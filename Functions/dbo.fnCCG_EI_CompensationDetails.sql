SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_CompensationDetails] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS @T TABLE 
(
	TopOrder	int,
	Descr		varchar(255),	-- Compensation
--  Amount					
	Value1		money,	
	HTMLValue1	varchar(25),
	Align1		varchar(6)
)
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select * from dbo.fnCCG_EI_CompensationDetails('2003005.xx',' ',' ')
*/
	insert into @T (TopOrder, Descr, Value1, HTMLValue1, Align1)
	select 1 as TopOrder, 'Labor', (Select IsNull(Fee,0) as Amount from PR where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3),'','Right'
	insert into @T (TopOrder, Descr, Value1, HTMLValue1, Align1)	
	select 2 as TopOrder, 'Consultant', (Select IsNull(ConsultFee,0) as Amount from PR where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3),'','Right'
	insert into @T (TopOrder, Descr, Value1, HTMLValue1, Align1)	
	select 3 as TopOrder, 'Expenses', (Select IsNull(ReimbAllow,0) as Amount from PR where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3),'','Right'
	
		-- Add grand total line:
	insert into @T (TopOrder, Descr, Value1, HTMLValue1, Align1) 
	select 90 as TopOrder, '<b>TOTAL</b>',sum(Value1),'','right' from @T 
	
	update @T set 
		HTMLValue1 = Case When Value1 is null Then '&nbsp;' Else '$' + Convert(varchar,Convert(money, Value1),1) End
		
		update @T set HTMLValue1='<b>'+HTMLValue1+'</b>' where TopOrder in (90)
	return
END
GO
