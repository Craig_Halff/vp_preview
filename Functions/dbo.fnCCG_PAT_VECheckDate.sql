SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_PAT_VECheckDate] (@PayableSeq int, @WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS datetime
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select dbo.fnCCG_PAT_VELastCheckDate(1234,'2003005.00',' ',' ')
	select dbo.fnCCG_PAT_VELastCheckDate(1234,'2003005.00','1PD',' ')
*/
    declare @paid datetime
		select @paid = pp.TransDate
		from CCG_PAT_Payable pat
		left join (select distinct PayableSeq,WBS1,WBS2,WBS3 from CCG_PAT_ProjectAmount ) pa on pat.Seq = pa.PayableSeq
		inner join LedgerAP ap on ap.Vendor=pat.Vendor and ap.WBS1=pa.WBS1 and ap.TransType='AP' and ap.Voucher=pat.Voucher
		inner join LedgerAP pp on pp.Vendor=ap.Vendor and pp.Voucher=ap.Voucher and pp.Line=ap.Line and pp.TransType='PP'
		left join VE on VE.Vendor=ap.Vendor
		--left join VO on VO.Vendor=ap.Vendor and vo.Voucher=ap.Voucher
		where (pat.Seq=@PayableSeq /*or pat.ParentSeq=@PayableSeq*/)  -- Match on single invoice or all invoices for a contract
		and (@WBS1 IS NULL or (ap.WBS1=@WBS1 and (ap.WBS2=@WBS2 or @WBS2 = ' ') and (ap.WBS3=@WBS3 or @WBS3 = ' ')))
	return @paid
END
GO
