SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnCCG_EI_JTDSpentDetailsByEmp] (@WBS1 varchar(max), @WBS2 varchar(7), @WBS3 varchar(7), @Key varchar(max))
RETURNS @T TABLE 
(
	TopOrder	int,
	Descr		varchar(max),
--	Trans Date					Hours						Billing						Comments
	Value1		datetime,		Value2		decimal(19,2),	Value3		money,			Value4		varchar(max),
	HTMLValue1	varchar(max),	HTMLValue2	varchar(max),	HTMLValue3	varchar(max),	HTMLValue4	varchar(max),
	Align1		varchar(max),	Align2		varchar(max),	Align3		varchar(max),	Align4		varchar(max)
)
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.
	
	select * from LD where wbs1='2003005.xx'
	select * from dbo.fnCCG_EI_JTDSpentDetailsByEmp('2003005.00',' ',' ','00011')
	select * from dbo.fnCCG_EI_JTDSpentDetailsByEmp('2003005.xx','1PD',' ')
*/
	declare @ThruPeriod int, @ThruDate datetime

	select @ThruPeriod=ThruPeriod, @ThruDate=ThruDate
	from CCG_EI_ConfigInvoiceGroups cig
	inner join ProjectCustomTabFields pctf on pctf.custInvoiceGroup=cig.InvoiceGroup
	where pctf.WBS1=@WBS1 and pctf.WBS2=' '

	if @ThruPeriod is null	set @ThruPeriod=999999
	if @ThruDate is null	set @ThruDate='12/31/2050'

	insert into @T (TopOrder, Descr, Value1, Value2, Value3, Value4, HTMLValue1, HTMLValue2, HTMLValue3, HTMLValue4, Align1, Align2, Align3, Align4)
	select 2 as TopOrder, ISNULL(LastName + ', ' + FirstName,''), TransDate, RegHrs + OvtHrs + SpecialOvtHrs, BillExt, dbo.fnCCG_ConvertHtml_LeaveNewLines(Comment), '', '', '','', 'right', 'right', 'right','left'
	from LD left join EM on EM.Employee=LD.Employee  
	where LD.Employee=@Key and WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' /* and BillStatus not in ('M','R')*/ and BillExt<>0  
	  and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
	--UNION ALL
	--select 2 as TopOrder, ISNULL(LastName + ', ' + FirstName,''), TransDate, RegHrs + OvtHrs + SpecialOvtHrs, BillExt,ISNULL(Comment,'') + '*' , '', '', '','', 'right', 'right','right', 'left'
	--from BILD LD left join EM on EM.Employee=LD.Employee  
	--where LD.Employee=@Key and WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus not in ('M','R') and BillExt<>0  
	--  and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
	
	-- Add grand total line:
	insert into @T (TopOrder, Descr, Value2, Value3, HTMLValue2, HTMLValue3, Align2, Align3)
	select 90 as TopOrder, '<b>Total</b>',sum(Value2),sum(Value3),'','', 'right', 'right' from @T where TopOrder in (2,3)
	
	update @T set 
		HTMLValue1 = Case When Value1 is null Then '&nbsp;' Else Convert(varchar(10), Value1, 101) End,
		HTMLValue2 = Convert(varchar(20),Value2),
		HTMLValue3 = Case When Value2 is null Then '&nbsp;' Else '$' + Convert(varchar,Convert(money, Value3),1) End,
		HTMLValue4 = IsNull(Value4, '&nbsp;')
		
	update @T set HTMLValue2='<b>'+HTMLValue2+'</b>', HTMLValue3='<b>'+HTMLValue3+'</b>' where TopOrder in (90)
	
	return
END
GO
