SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PM$tabHasECBaseline](
  @strPlanID varchar(32),
  @tiExpWBSLevel tinyint,
  @tiConWBSLevel tinyint
)

   RETURNS @tabResults TABLE
    (HasExpBaseline bit,
     HasConBaseline bit
    )

BEGIN

  DECLARE @strWBS1 Nvarchar(30)

  DECLARE @bitHasExpBaseline bit
  DECLARE @bitHasConBaseline bit

  DECLARE @bitExpWBSLevelChanged bit
  DECLARE @bitConWBSLevelChanged bit

  DECLARE @tabWBSTree TABLE(
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    WBSLevel tinyint,
    IsLeaf bit
    UNIQUE(WBS1, WBS2, WBS3, WBSLevel, IsLeaf)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @bitHasExpBaseline = 0
  SET @bitHasConBaseline = 0

  SET @bitExpWBSLevelChanged = 0
  SET @bitConWBSLevelChanged = 0

  SELECT
    @strWBS1 = P.WBS1,
    @bitExpWBSLevelChanged = CASE WHEN (@tiExpWBSLevel <> P.ExpWBSLevel) THEN 1 ELSE 0 END,
    @bitConWBSLevelChanged = CASE WHEN (@tiConWBSLevel <> P.ConWBSLevel) THEN 1 ELSE 0 END
    FROM PNPlan AS P
    WHERE P.PlanID = @strPlanID 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@bitExpWBSLevelChanged = 1 OR @bitConWBSLevelChanged = 1)
    BEGIN

      INSERT @tabWBSTree (
        WBS1,
        WBS2,
        WBS3,
        WBSLevel,
        IsLeaf
      )
        SELECT
          WBS1,
          WBS2,
          WBS3,
          WBSLevel,
          IsLeaf
          FROM
            (SELECT
               WBS1, WBS2, WBS3,
               CASE 
                 WHEN WBS2 = ' ' AND WBS3 = ' ' THEN 1
                 WHEN WBS2 != ' ' AND WBS3 = ' ' THEN 2
                 WHEN WBS2 != ' ' AND WBS3 != ' ' THEN 3
               END AS WBSLevel,
               CASE WHEN SubLevel = 'N' THEN 1 ELSE 0 END AS IsLeaf
               FROM PR
               WHERE WBS1 = @strWBS1
            ) AS X

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      IF (@bitExpWBSLevelChanged = 1)
        BEGIN

          SELECT @bitHasExpBaseline = 
            CASE 
              WHEN EXISTS(
                SELECT DISTINCT TPD.TimePhaseID
                  FROM PNBaselineExpenses AS TPD
                    INNER JOIN PNExpense AS E ON TPD.PlanID = E.PlanID AND TPD.TaskID = E.TaskID AND TPD.ExpenseID = E.ExpenseID
                    INNER JOIN @tabWBSTree AS WT ON WT.WBS1 = E.WBS1 AND WT.WBS2 = ISNULL(E.WBS2, ' ') AND WT.WBS3 = ISNULL(E.WBS3, ' ')
                  WHERE TPD.ExpenseID IS NOT NULL AND TPD.PlanID = @strPlanID AND 
                    (WT.WBSLevel > @tiExpWBSLevel OR (WT.WBSLevel < @tiExpWBSLevel AND WT.IsLeaf = 0))
              ) 
              THEN 1 ELSE 0 END

        END  /* IF (@bitExpWBSLevelChanged) */

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      IF (@bitConWBSLevelChanged = 1)
        BEGIN

          SELECT @bitHasConBaseline = 
            CASE 
              WHEN EXISTS(
                SELECT DISTINCT TPD.TimePhaseID
                  FROM PNBaselineConsultant AS TPD
                    INNER JOIN PNConsultant AS C ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID AND TPD.ConsultantID = C.ConsultantID
                    INNER JOIN @tabWBSTree AS WT ON WT.WBS1 = C.WBS1 AND WT.WBS2 = ISNULL(C.WBS2, ' ') AND WT.WBS3 = ISNULL(C.WBS3, ' ')
                  WHERE TPD.ConsultantID IS NOT NULL AND TPD.PlanID = @strPlanID AND 
                    (WT.WBSLevel > @tiConWBSLevel OR (WT.WBSLevel < @tiConWBSLevel AND WT.IsLeaf = 0))
              ) 
              THEN 1 ELSE 0 END

        END  /* IF (@bitConWBSLevelChanged) */

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    END /* IF (@bitExpWBSLevelChanged = 1 OR @bitConWBSLevelChanged = 1) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabResults
    (HasExpBaseline,
     HasConBaseline
    )
    SELECT
      @bitHasExpBaseline,
      @bitHasConBaseline

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


RETURN
END 

GO
