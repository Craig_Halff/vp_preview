SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnCCG_ConvertHtml](@htmlString varchar(max))
RETURNS varchar(max)
AS BEGIN

	declare @i int, @Start int, @End int, @Length int
	DECLARE @trimchars VARCHAR(10)
	SET @trimchars = CHAR(9)+CHAR(10)+CHAR(13)+CHAR(32)
	
	
	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '<br>', ' ')--char(13)+char(10))

	set @i = patindex('%<%>%', @htmlString)
	while @i > 0
	begin
		set @htmlString = stuff(@htmlString, @i, charindex('>', @htmlString, @i) - @i + 1, '')
		set @i = patindex('%<%>%', @htmlString)
	end

	
	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '&nbsp;', ' ')
	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '&amp;',  '&')
	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '&lt;',   '<')
	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '&gt;',   '>')
	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '&quot;', '"')
	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '&frasl;','/')
	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '&copy;', '@')

	set @htmlString = Replace(Replace(@htmlString,char(13),' '),char(10),' ')
	--remove leading spaces and line breaks
	IF @htmlString LIKE '[' + @trimchars + ']%' SET @htmlString = SUBSTRING(@htmlString, PATINDEX('%[^' + @trimchars + ']%', @htmlString), 8000)

	return @htmlString
END
GO
