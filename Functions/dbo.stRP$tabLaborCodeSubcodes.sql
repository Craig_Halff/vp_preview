SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabLaborCodeSubcodes](
  @strLaborCode nvarchar(14)
) 
  RETURNS @tabLaborCode TABLE (
    LC1Code nvarchar(14) COLLATE database_default,
    LC2Code nvarchar(14) COLLATE database_default,
    LC3Code nvarchar(14) COLLATE database_default,
    LC4Code nvarchar(14) COLLATE database_default,
    LC5Code nvarchar(14) COLLATE database_default,
    LC1Desc nvarchar(max) COLLATE database_default,
    LC2Desc nvarchar(max) COLLATE database_default,
    LC3Desc nvarchar(max) COLLATE database_default,
    LC4Desc nvarchar(max) COLLATE database_default,
    LC5Desc nvarchar(max) COLLATE database_default
)

BEGIN

  DECLARE @strTempLC nvarchar(14) = ''
  DECLARE @strLaborCodeDesc nvarchar(max) = ''
  DECLARE @strLC1Code nvarchar(14) = ''
  DECLARE @strLC2Code nvarchar(14) = ''
  DECLARE @strLC3Code nvarchar(14) = ''
  DECLARE @strLC4Code nvarchar(14) = ''
  DECLARE @strLC5Code nvarchar(14) = ''
  DECLARE @strLC1Desc nvarchar(max) = ''
  DECLARE @strLC2Desc nvarchar(max) = ''
  DECLARE @strLC3Desc nvarchar(max) = ''
  DECLARE @strLC4Desc nvarchar(max) = ''
  DECLARE @strLC5Desc nvarchar(max) = ''
  DECLARE @strLC1WildCard nvarchar(14) = ''
  DECLARE @strLC2WildCard nvarchar(14) = ''
  DECLARE @strLC3WildCard nvarchar(14) = ''
  DECLARE @strLC4WildCard nvarchar(14) = ''
  DECLARE @strLC5WildCard nvarchar(14) = ''
  DECLARE @strLCDelimiter varchar(1) = ''

  DECLARE @siLCLevels smallint = 0
  DECLARE @siLC1Start smallint = 0
  DECLARE @siLC2Start smallint = 0
  DECLARE @siLC3Start smallint = 0
  DECLARE @siLC4Start smallint = 0
  DECLARE @siLC5Start smallint = 0
  DECLARE @siLC1Length smallint = 0
  DECLARE @siLC2Length smallint = 0
  DECLARE @siLC3Length smallint = 0
  DECLARE @siLC4Length smallint = 0
  DECLARE @siLC5Length smallint = 0

  DECLARE @bitLC1IsValid bit = 0
  DECLARE @bitLC2IsValid bit = 0
  DECLARE @bitLC3IsValid bit = 0
  DECLARE @bitLC4IsValid bit = 0
  DECLARE @bitLC5IsValid bit = 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get LCDelimiter and other LC formats.
  
  SELECT
    @siLCLevels = LCLevels,
    @strLCDelimiter = ISNULL(LTRIM(RTRIM(LCDelimiter)), ''),
    @siLC1Start = LC1Start,
    @siLC2Start = LC2Start,
    @siLC3Start = LC3Start,
    @siLC4Start = LC4Start,
    @siLC5Start = LC5Start,
    @siLC1Length = LC1Length,
    @siLC2Length = LC2Length,
    @siLC3Length = LC3Length,
    @siLC4Length = LC4Length,
    @siLC5Length = LC5Length
    FROM CFGFormat

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Decompose Labor Code string into individual levels using the formatting information in CFGFormat.

  SELECT 
    @strLC1WildCard = REPLICATE('_', @siLC1Length),
    @strLC2WildCard = REPLICATE('_', @siLC2Length),
    @strLC3WildCard = REPLICATE('_', @siLC3Length),
    @strLC4WildCard = REPLICATE('_', @siLC4Length),
    @strLC5WildCard = REPLICATE('_', @siLC5Length)

  SELECT 
    @strLC1Code = LEFT(SUBSTRING(@strLaborCode, @siLC1Start, @siLC1Length) + REPLICATE('~', @siLC1Length), @siLC1Length),
    @strLC2Code = LEFT(SUBSTRING(@strLaborCode, @siLC2Start, @siLC2Length) + REPLICATE('~', @siLC2Length), @siLC2Length),
    @strLC3Code = LEFT(SUBSTRING(@strLaborCode, @siLC3Start, @siLC3Length) + REPLICATE('~', @siLC3Length), @siLC3Length),
    @strLC4Code = LEFT(SUBSTRING(@strLaborCode, @siLC4Start, @siLC4Length) + REPLICATE('~', @siLC4Length), @siLC4Length),
    @strLC5Code = LEFT(SUBSTRING(@strLaborCode, @siLC5Start, @siLC5Length) + REPLICATE('~', @siLC5Length), @siLC5Length)

  -- Reconstruct Labor Code string with delimiters to see if the input Labor Code string had the correct delimiters.

  SET @strTempLC = 
 
    @strLC1Code +

    CASE WHEN (@siLCLevels >= 2) 
      THEN ISNULL((ISNULL(@strLCDelimiter, '') + @strLC2Code), '') 
      ELSE ''
    END + 

    CASE WHEN (@siLCLevels >= 3) 
      THEN ISNULL((ISNULL(@strLCDelimiter, '') + @strLC3Code), '') 
      ELSE ''
    END + 

    CASE WHEN (@siLCLevels >= 4) 
      THEN ISNULL((ISNULL(@strLCDelimiter, '') + @strLC4Code), '') 
      ELSE ''
    END + 

    CASE WHEN (@siLCLevels >= 5) 
      THEN ISNULL((ISNULL(@strLCDelimiter, '') + @strLC5Code), '') 
      ELSE ''
    END 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- If the reconstructed Labor Code string @strTempLC is not the same as input Labor Code string @strTempLC then exit this UDF.
  -- I used MAX function to make sure that each select in the following section will return 1 row of result 
  -- when Length = 0 for a sub-Level.

  IF (@strLaborCode = @strTempLC)
    BEGIN

      SELECT 
        @strLC1Desc = 
          CASE
            WHEN @strLC1Code = @strLC1WildCard THEN @strLC1WildCard
            WHEN @strLC1Code <> @strLC1WildCard THEN ISNULL(MAX(LC.Label), '')
          END,
        @bitLC1IsValid = 
          CASE
            WHEN @siLC1Length = 0 THEN 1
            WHEN @siLC1Length > 0 AND @strLC1Code = @strLC1WildCard THEN 1
            WHEN @siLC1Length > 0 AND @strLC1Code <> @strLC1WildCard AND MAX(LC.Label) IS NOT NULL THEN 1
            ELSE 0
          END
        FROM CFGLCCodes AS LC
        WHERE LC.LCLevel = 1 AND LC.Code = @strLC1Code

      SELECT 
        @strLC2Desc = 
          CASE
            WHEN @strLC2Code = @strLC2WildCard THEN @strLC2WildCard
            WHEN @strLC2Code <> @strLC2WildCard THEN ISNULL(MAX(LC.Label), '')
          END,
        @bitLC2IsValid = 
          CASE
            WHEN @siLC2Length = 0 THEN 1
            WHEN @siLC2Length > 0 AND @strLC2Code = @strLC2WildCard THEN 1
            WHEN @siLC2Length > 0 AND @strLC2Code <> @strLC2WildCard AND MAX(LC.Label) IS NOT NULL THEN 1
            ELSE 0
          END
        FROM CFGLCCodes AS LC
        WHERE LC.LCLevel = 2 AND LC.Code = @strLC2Code

      SELECT 
        @strLC3Desc = 
          CASE
            WHEN @strLC3Code = @strLC3WildCard THEN @strLC3WildCard
            WHEN @strLC3Code <> @strLC3WildCard THEN ISNULL(MAX(LC.Label), '')
          END,
        @bitLC3IsValid = 
          CASE
            WHEN @siLC3Length = 0 THEN 1
            WHEN @siLC3Length > 0 AND @strLC3Code = @strLC3WildCard THEN 1
            WHEN @siLC3Length > 0 AND @strLC3Code <> @strLC3WildCard AND MAX(LC.Label) IS NOT NULL THEN 1
            ELSE 0
          END
        FROM CFGLCCodes AS LC
        WHERE LC.LCLevel = 3 AND LC.Code = @strLC3Code

      SELECT 
        @strLC4Desc = 
          CASE
            WHEN @strLC4Code = @strLC4WildCard THEN @strLC4WildCard
            WHEN @strLC4Code <> @strLC4WildCard THEN ISNULL(MAX(LC.Label), '')
          END,
        @bitLC4IsValid = 
          CASE
            WHEN @siLC4Length = 0 THEN 1
            WHEN @siLC4Length > 0 AND @strLC4Code = @strLC4WildCard THEN 1
            WHEN @siLC4Length > 0 AND @strLC4Code <> @strLC4WildCard AND MAX(LC.Label) IS NOT NULL THEN 1
            ELSE 0
          END
        FROM CFGLCCodes AS LC
        WHERE LC.LCLevel = 4 AND LC.Code = @strLC4Code

      SELECT 
        @strLC5Desc = 
          CASE
            WHEN @strLC5Code = @strLC5WildCard THEN @strLC5WildCard
            WHEN @strLC5Code <> @strLC5WildCard THEN ISNULL(MAX(LC.Label), '')
          END,
        @bitLC5IsValid = 
          CASE
            WHEN @siLC5Length = 0 THEN 1
            WHEN @siLC5Length > 0 AND @strLC5Code = @strLC5WildCard THEN 1
            WHEN @siLC5Length > 0 AND @strLC5Code <> @strLC5WildCard AND MAX(LC.Label) IS NOT NULL THEN 1
            ELSE 0
          END
        FROM CFGLCCodes AS LC
        WHERE LC.LCLevel = 5 AND LC.Code = @strLC5Code

    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF ((@bitLC1IsValid & @bitLC2IsValid & @bitLC3IsValid & @bitLC4IsValid & @bitLC5IsValid) = 1)
    BEGIN

      INSERT @tabLaborCode(
        LC1Code,
        LC2Code,
        LC3Code,
        LC4Code,
        LC5Code,
        LC1Desc,
        LC2Desc,
        LC3Desc,
        LC4Desc,
        LC5Desc
      )
        SELECT
          @strLC1Code,
          @strLC2Code,
          @strLC3Code,
          @strLC4Code,
          @strLC5Code,
          @strLC1Desc,
          @strLC2Desc,
          @strLC3Desc,
          @strLC4Desc,
          @strLC5Desc

    END /* IF ((@bitLC1IsValid & @bitLC2IsValid & @bitLC3IsValid & @bitLC4IsValid & @bitLC5IsValid) = 1) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN 

END -- stRP$tabLaborCodeSubcodes
GO
