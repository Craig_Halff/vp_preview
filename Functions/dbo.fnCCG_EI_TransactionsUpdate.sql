SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_TransactionsUpdate] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS @T TABLE
(
	ColValue	varchar(10),
	ColDetail	varchar(100)	-- Set background color to light red 
)
BEGIN 
/*
	Copyright (c) 2015 Central Consulting Group. All rights reserved.

	select dbo.fnCCG_EI_TransactionsUpdate('2003005.xx',' ',' ')
*/

	declare @PctUpdated varchar(10), @TransUpdated varchar(10), @res varchar(10), @detail varchar(100)
	set @PctUpdated=''
	set @TransUpdated=''

	--Check Pct Complete window for updates
	if exists (select 'x' from CCG_EI_FeePctCpl where WBS1=@WBS1)
		set @PctUpdated='Yes'
	
	----Check Transaction window for updates	
	--if exists (select 'x' from CCG_TR where OriginalWBS1=@WBS1)
	--	set @TransUpdated='Yes'

	--if @PctUpdated='Yes' and @TransUpdated='Yes'
	--	begin
	--		set @res='Both'
	--		set @detail = 'BackgroundColor=#F7FE2E' 
	--	end
	if @PctUpdated='Yes' and @TransUpdated=''
		begin
			set @res='Pct'
			set @detail = 'BackgroundColor=#F7FE2E' 
		end
		--else if @PctUpdated='' and @TransUpdated='Yes'
		--	begin
		--		set @res='Trans'
		--		set @detail = 'BackgroundColor=#F7FE2E' 
		--	end
			else
				begin
				set @res=''
				set @detail = '' 
			end
	
	insert into @T values (@res, @detail)
	return
END
GO
