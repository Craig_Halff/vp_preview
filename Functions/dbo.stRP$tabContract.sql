SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabContract](
  @strRowID nvarchar(255),
  @strSessionID varchar(32),
  @strMode varchar(1) = 'C', /* S = Self, C = Children */
  @bActiveWBSOnly bit = 0
)
  RETURNS @tabContract TABLE (

    RowID nvarchar(255) COLLATE database_default,
    ParentRowID nvarchar(255) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int,
    RowLevel int,
    Name nvarchar(255) COLLATE database_default,
    Org nvarchar(30) COLLATE database_default,
    OrgName nvarchar(100) COLLATE database_default,
    ProjMgr nvarchar(20) COLLATE database_default,
	UtilizationScheduleFlg varchar(1),
    PMFullName nvarchar(255) COLLATE database_default,
    ClientName nvarchar(100) COLLATE database_default,
    OpportunityID varchar(32) COLLATE database_default,
    RT_Status varchar(1) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    TopWBS1 nvarchar(30) COLLATE database_default,
    TopName nvarchar(255) COLLATE database_default,
    SecurityKey nvarchar(92) COLLATE database_default,
    HasChildren bit,
    HasNotes varchar(1) COLLATE database_default,
    LeafNode bit,
    
    ContractStartDate datetime,
    ContractEndDate datetime,

    ContractLaborCost decimal(19,4),
    ContractLaborBill decimal(19,4),
    ContractDirectExpCost decimal(19,4),
    ContractDirectExpBill decimal(19,4),
    ContractReimbExpCost decimal(19,4),
    ContractReimbExpBill decimal(19,4),
    ContractDirectConsultCost decimal(19,4),
    ContractDirectConsultBill decimal(19,4),
    ContractReimbConsultCost decimal(19,4),
    ContractReimbConsultBill decimal(19,4),
    ContractCompCost decimal(19,4),
    ContractCompBill decimal(19,4),
    ContractReimbAllowCost decimal(19,4),
    ContractReimbAllowBill decimal(19,4),
    ContractTotalCost decimal(19,4),
    ContractTotalBill decimal(19,4),
    ContractPctTotalCost decimal(19,4),
    ContractPctTotalBill decimal(19,4)

  )

BEGIN

/**************************************************************************************************************************/
--
-- Huge Assumptions:
--
-- 1. In Non-Vision worlds (e.g. Costpoint)
--    1.1 WBS structures will be imported into PR, RPTask, and PNTask tables.
--        1.1.1 PR table has rows with WBS1, WBS2, WBS3.
--        1.1.2 RPTask and PNTable tables also have WBS1, WBS2, WBS3, but can be indented using OutlineNumber.
--        1.1.3 RPTask and PNTable tables can have up to 15 indent levels (just like currently in ngRP).
--    1.2 WBS Numbers will be in WBS1, WBS2, WBS3 
--        (same rules as currently in Vision, such as WBS2 = <blank>, WBS3 = <blank> for top most WBS row)
--    1.3 WBS3 will cover WBS Level 3 through 15 (e.g. WBS3 = {1, 1.1, 1.1.1, 1.1.1.1).
--    1.4 JTD data will be at the lowest WBS level (e.g. JTD could be at level 15). 
--    1.5 JTD is matched using WBS1, WBS2, WBS3, Employee.
--    1.6 Assignments will be at lowest WBS level 
--        (e.g. Assignment could be at level 15, same level as in the case of JTD).
--
-- 2. In Vision world
--    2.1 PR, RPTask, and PNTask tables have only 3 levels.
--    2.2 JTD data will be at the lowest WBS level.
--    2.3 Assignments will be at lowest WBS level.
--
/**************************************************************************************************************************/

  DECLARE @_SectionSign nchar = NCHAR(167) -- N'§'
  DECLARE @_PipeSign nchar = N'|'

  DECLARE @strCompany nvarchar(14)
  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)
  DECLARE @strTaskID varchar(32)
  DECLARE @strPlanID varchar(32)
  DECLARE @strOpportunityID varchar(32)
  DECLARE @strTaskStatus varchar(1) = ''
  DECLARE @strTopWBS1 nvarchar(30)
  DECLARE @strTopName nvarchar(255)
  DECLARE @strInputType varchar(1) = ''
  DECLARE @strParentRowID nvarchar(255) = ''
  DECLARE @strWBS1 nvarchar(30)
  DECLARE @strWBS2 nvarchar(30) = ' '
  DECLARE @strWBS3 nvarchar(30) = ' '
  DECLARE @strWBS1WBS2WBS3 nvarchar(92)

  DECLARE @dtJTDDate datetime

  DECLARE @decContractCost decimal(19,4)
  DECLARE @decContractBill decimal(19,4)
 
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intMAXWBSLevel int

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint
  DECLARE @siWBS2Length smallint
  DECLARE @siWBS3Length smallint

  DECLARE @bitIsLeafTask bit

  -- Declare Temp tables.

  DECLARE @tabWBS TABLE (
    RowID int IDENTITY(1,1),
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    WBS1WBS2WBS3 nvarchar(92) COLLATE database_default,
    SecurityKey nvarchar(92) COLLATE database_default,
    Name nvarchar(255) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    NonLBCDChildrenCount int,
    OutlineLevel int,
    Org nvarchar(30) COLLATE database_default,
    ClientID varchar(32) COLLATE database_default,
    ProjMgr nvarchar(20) COLLATE database_default,
	UtilizationScheduleFlg varchar(1),
    Status varchar(1) COLLATE database_default,
    HasNotes varchar(1) COLLATE database_default,
    ContractStartDate datetime,
    ContractEndDate datetime,
    FeeLabCost decimal(19,4),
    FeeLabBill decimal(19,4),
    FeeDirExpCost decimal(19,4),
    FeeDirExpBill decimal(19,4),
    FeeReimExpCost decimal(19,4),
    FeeReimExpBill decimal(19,4),
    FeeDirConCost decimal(19,4),
    FeeDirConBill decimal(19,4),
    FeeReimConCost decimal(19,4),
    FeeReimConBill decimal(19,4)
    UNIQUE (RowID, WBS1, WBS2, WBS3, ParentOutlineNumber, OutlineNumber)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length,
    @siWBS2Length = WBS2Length,
    @siWBS3Length = WBS3Length,
    @intMAXWBSLevel = 
      CASE
        WHEN WBS2Length = 0 AND WBS3Length = 0 THEN 1
        WHEN WBS2Length > 0 AND WBS3Length = 0 THEN 2
        WHEN WBS2Length > 0 AND WBS3Length > 0 THEN 3
      END
    FROM CFGFormat

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Get RM Settings.
  
  SELECT 
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE())))
    FROM CFGRMSettings

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. For an Assignment row, @strRowID = 'E~<RPAssignment.ResourceID>|<RPTask.TaskID>
  --   2. For an Assignment row, @strRowID = 'G~<RPAssignment.GenericResourceID>|<RPTask.TaskID>'
  --   3. For a WBS row, @strRowID = '|<RPTask.TaskID>'
  --   4. For a WBS row, @strRowID = '|<PR.WBS1>§<PR.WBS2>§<PR.WBS3>'

  SET @strInputType = 
    CASE
      WHEN CHARINDEX(@_SectionSign, @strRowID) > 0
      THEN 'W'
      ELSE 'T'
    END

  IF (@strInputType ='T')
    BEGIN

      -- Parsing for TaskID.

      SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

      -- Setting various Plan parameter.

      SELECT 
        @strCompany = P.Company,
        @strPlanID = PT.PlanID,
        @strOpportunityID = P.OpportunityID,
        @strWBS1 = PT.WBS1,
        @strWBS2 = ISNULL(PT.WBS2, ' '),
        @strWBS3 = ISNULL(PT.WBS3, ' '),
        @strTaskStatus = PT.Status,
        @strTopWBS1 = MIN(ISNULL(TT.WBS1, '')),
        @strTopName = MIN(ISNULL(TT.Name, '')),
        @bitIsLeafTask =
          CASE
            WHEN COUNT(CT.TaskID) > 0
            THEN 0
            ELSE 1
          END
        FROM PNTask AS PT 
          LEFT JOIN PNTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
          LEFT JOIN PNTask AS TT ON PT.PlanID = TT.PlanID AND TT.WBSType = 'WBS1' AND TT.WBS1 <> '<none>'
          INNER JOIN PNPlan AS P ON PT.PlanID = P.PlanID
        WHERE PT.TaskID = @strTaskID
        GROUP BY PT.PlanID, PT.TaskID, PT.Status, PT.WBS1, PT.WBS2, PT.WBS3, P.Company, P.OpportunityID

    END
  ELSE IF (@strInputType = 'W')
    BEGIN

      SET @strWBS1WBS2WBS3 = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

      -- Get various Plan parameters.

      SELECT 
        @strCompany = 
          CASE 
            WHEN @strMultiCompanyEnabled = 'Y' 
            THEN ISNULL(SUBSTRING(TP.Org, @siOrg1Start, @siOrg1Length), ' ') 
            ELSE ' ' 
          END,
        @strPlanID = NULL,
        @strOpportunityID = NULL,
        @strTaskID = NULL,
        @strWBS1 = P.WBS1,
        @strWBS2 = P.WBS2,
        @strWBS3 = P.WBS3,
        @strTaskStatus = P.Status,
        @strTopWBS1 = ISNULL(TP.WBS1, ''),
        @strTopName = ISNULL(TP.Name, ''),
        @bitIsLeafTask =
          CASE
            WHEN P.SubLevel = 'Y'
            THEN 0
            ELSE 1
          END
        FROM PR AS P
          LEFT JOIN PR AS TP
            ON TP.WBS1 = P.WBS1 AND TP.WBS2 = ' ' AND TP.WBS3 = ' '
        WHERE P.WBS1 + @_SectionSign + P.WBS2 + @_SectionSign + P.WBS3 = @strWBS1WBS2WBS3

    END /* END ELSE IF (@strInputType IN ('W')) */

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Get decimal settings.
  
  SELECT
    @intAmtCostDecimals = AmtCostDecimals,
    @intAmtBillDecimals = AmtBillDecimals
    FROM dbo.stRP$tabPlanDecimals(@strPlanID)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Build WBS Structure.

  -- If the input Project currently has no Plan then save the entire Project away into @tabWBS.
  -- Otherwise, save away PNTask rows into @tabWBS.
      
  IF (@strInputType = 'W')
    BEGIN

      -- WBS1 Row.

      INSERT @tabWBS(
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        WBS1WBS2WBS3,
        SecurityKey,
        Name,
        ParentOutlineNumber,
        OutlineNumber,
        NonLBCDChildrenCount,
        OutlineLevel,
        Org,
        ClientID,
        ProjMgr,
		UtilizationScheduleFlg,
        Status,
        HasNotes,
        ContractStartDate,
        ContractEndDate,
        FeeLabCost,
        FeeLabBill,
        FeeDirExpCost,
        FeeDirExpBill,
        FeeReimExpCost,
        FeeReimExpBill,
        FeeDirConCost,
        FeeDirConBill,
        FeeReimConCost,
        FeeReimConBill
      )
        SELECT
          NULL AS PlanID,
          NULL AS TaskID,
          PR.WBS1 AS WBS1,
          PR.WBS2 AS WBS2,
          PR.WBS3 AS WBS3,
          PR.WBS1 + @_SectionSign + PR.WBS2 + @_SectionSign + PR.WBS3 AS WBS1WBS2WBS3,
          PR.WBS1 AS SecurityKey,
          PR.Name AS Name,
          NULL AS ParentOutlineNumber,
          '001' AS OutlineNumber,
          (SELECT COUNT(*) FROM PR WHERE WBS1 = @strWBS1 AND WBS2 <> ' ' AND WBS3 = ' ') AS NonLBCDChildrenCount,
          0 AS OutlineLevel,
          PR.Org AS Org,
          PR.ClientID AS ClientID,
          PR.ProjMgr AS ProjMgr,
		  PR.UtilizationScheduleFlg AS UtilizationScheduleFlg,
          PR.Status AS Status,
          'N' AS HasNotes,
          PR.StartDate AS ContractStartDate,
          COALESCE(PR.ActCompletionDate, PR.EstCompletionDate) AS ContractEndDate,
          PR.FeeDirLab AS FeeLabCost,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirLabBillingCurrency ELSE PR.FeeDirLab END) AS FeeLabBill,
          PR.FeeDirExp AS FeeDirExpCost,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirExpBillingCurrency ELSE PR.FeeDirExp END) AS FeeDirExpBill,
          PR.ReimbAllowExp AS FeeReimExpCost,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowExpBillingCurrency ELSE PR.ReimbAllowExp END) AS FeeReimExpBill,
          PR.ConsultFee AS FeeDirConCost,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ConsultFeeBillingCurrency ELSE PR.ConsultFee END) AS FeeDirConBill,
          PR.ReimbAllowCons AS FeeReimConCost,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowConsBillingCurrency ELSE PR.ReimbAllowCons END) AS FeeReimConBill
          FROM PR
          WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '

      -- WBS2 Rows.

      INSERT @tabWBS (
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        WBS1WBS2WBS3,
        SecurityKey,
        Name,
        ParentOutlineNumber,
        OutlineNumber,
        NonLBCDChildrenCount,
        OutlineLevel,
        Org,
        ClientID,
        ProjMgr,
		UtilizationScheduleFlg,
        Status,
        HasNotes,
        ContractStartDate,
        ContractEndDate,
        FeeLabCost,
        FeeLabBill,
        FeeDirExpCost,
        FeeDirExpBill,
        FeeReimExpCost,
        FeeReimExpBill,
        FeeDirConCost,
        FeeDirConBill,
        FeeReimConCost,
        FeeReimConBill
      )
        SELECT
          NULL AS PlanID,
          NULL AS TaskID,
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.WBS1 + @_SectionSign + X.WBS2 + @_SectionSign + X.WBS3 AS WBS1WBS2WBS3,
          X.WBS1 + @_PipeSign + X.WBS2 AS SecurityKey,
          X.Name,
          '001' AS ParentOutlineNumber,
          '001' + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
          (SELECT COUNT(*) FROM PR WHERE WBS1 = @strWBS1 AND WBS2 = X.WBS2 AND WBS3 <> ' ') AS NonLBCDChildrenCount,
          1 AS OutlineLevel,
          X.Org,
          X.ClientID,
          X.ProjMgr,
		  X.UtilizationScheduleFlg,
          X.Status,
          'N' AS HasNotes,
          X.ContractStartDate AS ContractStartDate,
          X.ContractEndDate AS ContractEndDate,
          X.FeeLabCost AS FeeLabCost,
          X.FeeLabBill AS FeeLabBill,
          X.FeeDirExpCost AS FeeDirExpCost,
          X.FeeDirExpBill AS FeeDirExpBill,
          X.FeeReimExpCost AS FeeReimExpCost,
          X.FeeReimExpBill AS FeeReimExpBill,
          X.FeeDirConCost AS FeeDirConCost,
          X.FeeDirConBill AS FeeDirConBill,
          X.FeeReimConCost AS FeeReimConCost,
          X.FeeReimConBill AS FeeReimConBill
          FROM (
            SELECT
              PR.WBS1 AS WBS1,
              PR.WBS2 AS WBS2,
              PR.WBS3 AS WBS3,
              PR.Name AS Name,
              PR.Org AS Org,
              PR.ClientID AS ClientID,
              PR.ProjMgr AS ProjMgr,
			  PR.UtilizationScheduleFlg AS UtilizationScheduleFlg,
              PR.Status AS Status,
              PR.StartDate AS ContractStartDate,
              COALESCE(PR.ActCompletionDate, PR.EstCompletionDate) AS ContractEndDate,
              PR.FeeDirLab AS FeeLabCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirLabBillingCurrency ELSE PR.FeeDirLab END) AS FeeLabBill,
              PR.FeeDirExp AS FeeDirExpCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirExpBillingCurrency ELSE PR.FeeDirExp END) AS FeeDirExpBill,
              PR.ReimbAllowExp AS FeeReimExpCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowExpBillingCurrency ELSE PR.ReimbAllowExp END) AS FeeReimExpBill,
              PR.ConsultFee AS FeeDirConCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ConsultFeeBillingCurrency ELSE PR.ConsultFee END) AS FeeDirConBill,
              PR.ReimbAllowCons AS FeeReimConCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowConsBillingCurrency ELSE PR.ReimbAllowCons END) AS FeeReimConBill,
              ROW_NUMBER() OVER (PARTITION BY PR.WBS1 ORDER BY PR.WBS1, PR.WBS2) AS RowID
              FROM PR 
              WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 <> ' ' AND PR.WBS3 = ' '
          ) AS X

      -- WBS3 Rows.

      INSERT @tabWBS (
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        WBS1WBS2WBS3,
        SecurityKey,
        Name,
        ParentOutlineNumber,
        OutlineNumber,
        NonLBCDChildrenCount,
        OutlineLevel,
        Org,
        ClientID,
        ProjMgr,
		UtilizationScheduleFlg,
        Status,
        HasNotes,
        ContractStartDate,
        ContractEndDate,
        FeeLabCost,
        FeeLabBill,
        FeeDirExpCost,
        FeeDirExpBill,
        FeeReimExpCost,
        FeeReimExpBill,
        FeeDirConCost,
        FeeDirConBill,
        FeeReimConCost,
        FeeReimConBill
      )
        SELECT
          NULL AS PlanID,
          NULL AS TaskID,
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.WBS1 + @_SectionSign + X.WBS2 + @_SectionSign + X.WBS3 AS WBS1WBS2WBS3,
          X.WBS1 + @_PipeSign + X.WBS2 + @_PipeSign + X.WBS3 AS SecurityKey,
          X.Name,
          PX.OutlineNumber AS ParentOutlineNumber,
          PX.OutlineNumber + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(X.RowID, 36)), 3) AS OutlineNumber,
          0 AS NonLBCDChildrenCount,
          PX.OutlineLevel + 1 AS OutlineLevel,
          X.Org,
          X.ClientID,
          X.ProjMgr,
		  X.UtilizationScheduleFlg,
          X.Status,
          'N' AS HasNotes,
          X.ContractStartDate AS ContractStartDate,
          X.ContractEndDate AS ContractEndDate,
          X.FeeLabCost AS FeeLabCost,
          X.FeeLabBill AS FeeLabBill,
          X.FeeDirExpCost AS FeeDirExpCost,
          X.FeeDirExpBill AS FeeDirExpBill,
          X.FeeReimExpCost AS FeeReimExpCost,
          X.FeeReimExpBill AS FeeReimExpBill,
          X.FeeDirConCost AS FeeDirConCost,
          X.FeeDirConBill AS FeeDirConBill,
          X.FeeReimConCost AS FeeReimConCost,
          X.FeeReimConBill AS FeeReimConBill
          FROM (
            SELECT
              PR.WBS1 AS WBS1,
              PR.WBS2 AS WBS2,
              PR.WBS3 AS WBS3,
              PR.Name AS Name,
              PR.Org AS Org,
              PR.ClientID AS ClientID,
              PR.ProjMgr AS ProjMgr,
			  PR.UtilizationScheduleFlg AS UtilizationScheduleFlg,
              PR.Status AS Status,
              PR.StartDate AS ContractStartDate,
              COALESCE(PR.ActCompletionDate, PR.EstCompletionDate) AS ContractEndDate,
              PR.FeeDirLab AS FeeLabCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirLabBillingCurrency ELSE PR.FeeDirLab END) AS FeeLabBill,
              PR.FeeDirExp AS FeeDirExpCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirExpBillingCurrency ELSE PR.FeeDirExp END) AS FeeDirExpBill,
              PR.ReimbAllowExp AS FeeReimExpCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowExpBillingCurrency ELSE PR.ReimbAllowExp END) AS FeeReimExpBill,
              PR.ConsultFee AS FeeDirConCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ConsultFeeBillingCurrency ELSE PR.ConsultFee END) AS FeeDirConBill,
              PR.ReimbAllowCons AS FeeReimConCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowConsBillingCurrency ELSE PR.ReimbAllowCons END) AS FeeReimConBill,
              ROW_NUMBER() OVER (PARTITION BY PR.WBS2 ORDER BY PR.WBS1, PR.WBS2, PR.WBS3) AS RowID
              FROM PR 
              WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 <> ' ' AND PR.WBS3 <> ' '
          ) AS X
            INNER JOIN @tabWBS AS PX ON X.WBS1 = PX.WBS1 AND X.WBS2 = PX.WBS2 AND PX.WBS3 = ' '
        
    END /* END IF (@strInputType = 'W') */
        
  ELSE IF (@strInputType = 'T')
    BEGIN
        
      INSERT @tabWBS(
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        WBS1WBS2WBS3,
        SecurityKey,
        Name,
        ParentOutlineNumber,
        OutlineNumber,
        NonLBCDChildrenCount,
        OutlineLevel,
        Org,
        ClientID,
        ProjMgr,
	    UtilizationScheduleFlg,
        Status,
        HasNotes,
        ContractStartDate,
        ContractEndDate,
        FeeLabCost,
        FeeLabBill,
        FeeDirExpCost,
        FeeDirExpBill,
        FeeReimExpCost,
        FeeReimExpBill,
        FeeDirConCost,
        FeeDirConBill,
        FeeReimConCost,
        FeeReimConBill
      )
        SELECT
          T.PlanID AS PlanID,
          T.TaskID AS TaskID,
          T.WBS1 AS WBS1,
          ISNULL(T.WBS2, ' ') AS WBS2,
          ISNULL(T.WBS3, ' ') AS WBS3,
          T.WBS1 + @_SectionSign + ISNULL(T.WBS2, ' ') + @_SectionSign + ISNULL(T.WBS3, ' ') AS WBS1WBS2WBS3,
          CASE
            WHEN (T.OutlineLevel + 1) <= @intMAXWBSLevel 
            THEN T.WBS1 + ISNULL((@_PipeSign + T.WBS2), '') + ISNULL((@_PipeSign + T.WBS3), '')
            ELSE BT.WBS1 + ISNULL((@_PipeSign + BT.WBS2), '') + ISNULL((@_PipeSign + BT.WBS3), '')
          END AS SecurityKey,
          T.Name AS Name,
          T.ParentOutlineNumber AS ParentOutlineNumber,
          T.OutlineNumber AS OutlineNumber,
          (SELECT COUNT(*) FROM PNTask AS XT WHERE XT.PlanID = T.PlanID AND XT.ParentOutlineNumber = T.OutlineNUmber AND XT.WBSType <> 'LBCD') AS NonLBCDChildrenCount,
          T.OutlineLevel AS OutlineLevel,
          T.Org AS Org,
          T.ClientID AS ClientID,
          T.ProjMgr AS ProjMgr,
		  PR.UtilizationScheduleFlg AS UtilizationScheduleFlg,
          T.Status AS Status,
          CASE WHEN T.Notes IS NOT NULL AND T.Notes <> '' THEN 'Y' ELSE 'N' END AS HasNotes,

          PR.StartDate AS ContractStartDate,
          COALESCE(PR.EndDate, PR.ActCompletionDate, PR.EstCompletionDate) AS ContractEndDate,

          COALESCE(PR.FeeDirLab, T.CompensationFeeDirLab, 0) AS FeeLabCost,
          CASE 
            WHEN @strReportAtBillingInBillingCurr = 'Y' 
            THEN COALESCE(PR.FeeDirLabBillingCurrency, T.CompensationFeeDirLabBill, 0) 
            ELSE COALESCE(PR.FeeDirLab, T.CompensationFeeDirLab, 0) 
          END AS FeeLabBill,

          COALESCE(PR.FeeDirExp, T.CompensationFeeDirExp, 0) AS FeeDirExpCost,
          CASE 
            WHEN @strReportAtBillingInBillingCurr = 'Y' 
            THEN COALESCE(PR.FeeDirExpBillingCurrency, T.CompensationFeeDirExpBill, 0)
            ELSE COALESCE(PR.FeeDirExp, T.CompensationFeeDirExp, 0) 
          END AS FeeDirExpBill,

          COALESCE(PR.ReimbAllowExp, T.ReimbAllowanceExp, 0) AS FeeReimExpCost,
          CASE 
            WHEN @strReportAtBillingInBillingCurr = 'Y' 
            THEN COALESCE(PR.ReimbAllowExpBillingCurrency, T.ReimbAllowanceExpBill, 0)
            ELSE COALESCE(PR.ReimbAllowExp, T.ReimbAllowanceExp, 0) 
          END AS FeeReimExpBill,

          COALESCE(PR.ConsultFee, T.ConsultantFee, 0) AS FeeDirConCost,
          CASE 
            WHEN @strReportAtBillingInBillingCurr = 'Y' 
            THEN COALESCE(PR.ConsultFeeBillingCurrency, T.ConsultantFeeBill, 0) 
            ELSE COALESCE(PR.ConsultFee, T.ConsultantFee, 0) 
          END AS FeeDirConBill,

          COALESCE(PR.ReimbAllowCons, T.ReimbAllowanceCon, 0) AS FeeReimConCost,
          CASE 
            WHEN @strReportAtBillingInBillingCurr = 'Y' 
            THEN COALESCE(PR.ReimbAllowConsBillingCurrency, T.ReimbAllowanceConBill, 0) 
            ELSE COALESCE(PR.ReimbAllowCons, T.ReimbAllowanceCon, 0) 
          END AS FeeReimConBill

          FROM PNTask AS T
            LEFT JOIN PNTask AS BT 
              ON T.PlanID = BT.PlanID AND T.OutlineNumber LIKE BT.OutlineNumber + '%' 
                AND (BT.OutlineLevel + 1) = @intMAXWBSLevel 
            LEFT JOIN PR
              ON T.WBS1 = PR.WBS1 AND ISNULL(T.WBS2, ' ') = PR.WBS2 AND ISNULL(T.WBS3, ' ') = PR.WBS3
          WHERE T.PlanID = @strPlanID AND T.WBSType <> 'LBCD'

    END /* END ELSE IF (@strInputType = 'T') */

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Determine ParentRowID based on @strMode, @strInputType.

  SELECT
    @strParentRowID = 
      CASE 
        WHEN @strMode = 'C' THEN @strRowID
        WHEN @strMode = 'S' THEN 
          CASE 
            WHEN (@strInputType = 'T') 
              THEN ISNULL(('|' + PT.TaskID), '')
            WHEN (@strInputType = 'W') 
              THEN ISNULL(('|' + PT.WBS1 + @_SectionSign + PT.WBS2 + @_SectionSign + PT.WBS3), '')
          END
      END 
    FROM @tabWBS AS CT
      LEFT JOIN @tabWBS AS PT ON PT.PlanID = CT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber
    WHERE CT.TaskID = @strTaskID

  -- Calculate Total Contract for Top WBS1.

  SELECT
    @decContractCost = (W.FeeLabCost + W.FeeDirExpCost + W.FeeReimExpCost + W.FeeDirConCost + W.FeeReimConCost),
    @decContractBill = (W.FeeLabBill + W.FeeDirExpBill + W.FeeReimExpBill + W.FeeDirConBill + W.FeeReimConBill)
    FROM @tabWBS AS W
    WHERE W.WBS1 = @strWBS1 AND W.WBS2 = ' ' AND W.WBS3 = ' '

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Insert final result for WBS rows.

  IF (@strMode = 'S')
    BEGIN

      INSERT @tabContract(
        RowID,
        ParentRowID,
        PlanID,
        TaskID,
        ParentOutlineNumber,
        OutlineNumber,
        OutlineLevel,
        RowLevel,
        Name,
        Org,
        OrgName,
        ProjMgr,
		UtilizationScheduleFlg,
        PMFullName,
        ClientName,
        OpportunityID,
        RT_Status,
        WBS1,
        WBS2,
        WBS3,
        TopWBS1,
        TopName,
        SecurityKey,
        HasChildren,
        HasNotes,
        LeafNode,
            
        ContractStartDate,
        ContractEndDate,

        ContractLaborCost,
        ContractLaborBill,
        ContractDirectExpCost,
        ContractDirectExpBill,
        ContractReimbExpCost,
        ContractReimbExpBill,
        ContractDirectConsultCost,
        ContractDirectConsultBill,
        ContractReimbConsultCost,
        ContractReimbConsultBill,
        ContractCompCost,
        ContractCompBill,
        ContractReimbAllowCost,
        ContractReimbAllowBill,
        ContractTotalCost,
        ContractTotalBill,
        ContractPctTotalCost,
        ContractPctTotalBill
      )
        SELECT
          '|' + 
          CASE 
            WHEN @strInputType = 'T' THEN YT.TaskID
            WHEN @strInputType = 'W' THEN YT.WBS1WBS2WBS3
          END AS RowID,
          @strParentRowID AS ParentRowID,
          YT.PlanID AS PlanID,
          YT.TaskID AS TaskID,
          YT.ParentOutlineNumber AS ParentOutlineNumber,
          YT.OutlineNumber AS OutlineNumber,
          YT.OutlineLevel AS OutlineLevel,
          YT.OutlineLevel AS RowLevel,
          YT.Name AS Name,
          ISNULL(YT.Org, '') AS Org,
          ISNULL(O.Name, '') AS OrgName,
          ISNULL(YT.ProjMgr, '') AS ProjMgr,
		  YT.UtilizationScheduleFlg AS UtilizationScheduleFlg,
          CONVERT(nvarchar(255), ISNULL(ISNULL(PM.PreferredName, PM.FirstName), '') + ISNULL((' ' + PM.LastName), '') + ISNULL(', ' + PMSuffix.Suffix, '')) AS PMFullName,
          ISNULL(CL.Name, '') AS ClientName,
          @strOpportunityID AS OpportunityID,
          YT.Status AS RT_Status,
          YT.WBS1 AS WBS1,
          YT.WBS2 AS WBS2,
          YT.WBS3 AS WBS3,
          @strTopWBS1 AS TopWBS1,
          @strTopName AS TopName,
          YT.SecurityKey AS SecurityKey,
          CASE 
            WHEN YT.NonLBCDChildrenCount > 0 
            THEN CONVERT(bit, 1) 
            ELSE CONVERT(bit, 0)
          END AS HasChildren,
          YT.HasNotes AS HasNotes,
          CASE WHEN YT.NonLBCDChildrenCount > 0 THEN CONVERT(bit, 0) ELSE CONVERT(bit,1) END AS LeafNode,
   
          YT.ContractStartDate AS ContractStartDate,
          YT.ContractEndDate AS ContractEndDate,

          ROUND(ISNULL(YT.FeeLabCost, 0.0000), @intAmtCostDecimals) AS ContractLaborCost,
          ROUND(ISNULL(YT.FeeLabBill, 0.0000), @intAmtCostDecimals) AS ContractLaborBill,
          ROUND(ISNULL(YT.FeeDirExpCost, 0.0000), @intAmtCostDecimals) AS ContractDirectExpCost,
          ROUND(ISNULL(YT.FeeDirExpBill, 0.0000), @intAmtCostDecimals) AS ContractDirectExpBill,
          ROUND(ISNULL(YT.FeeReimExpCost, 0.0000), @intAmtCostDecimals) AS ContractReimbExpCost,
          ROUND(ISNULL(YT.FeeReimExpBill, 0.0000), @intAmtCostDecimals) AS ContractReimbExpBill,
          ROUND(ISNULL(YT.FeeDirConCost, 0.0000), @intAmtCostDecimals) AS ContractDirectConsultCost,
          ROUND(ISNULL(YT.FeeDirConBill, 0.0000), @intAmtCostDecimals) AS ContractDirectConsultBill,
          ROUND(ISNULL(YT.FeeReimConCost, 0.0000), @intAmtCostDecimals) AS ContractReimbConsultCost,
          ROUND(ISNULL(YT.FeeReimConBill, 0.0000), @intAmtCostDecimals) AS ContractReimbConsultBill,
          ROUND(ISNULL((YT.FeeLabCost + YT.FeeDirExpCost), 0.0000), @intAmtCostDecimals) AS ContractCompCost,
          ROUND(ISNULL((YT.FeeLabBill + YT.FeeDirExpBill), 0.0000), @intAmtCostDecimals) AS ContractCompBill,
          ROUND(ISNULL((YT.FeeReimExpCost + YT.FeeReimConCost), 0.0000), @intAmtCostDecimals) AS ContractReimbAllowCost,
          ROUND(ISNULL((YT.FeeReimExpBill + YT.FeeReimConBill), 0.0000), @intAmtCostDecimals) AS ContractReimbAllowBill,
          ROUND(ISNULL((YT.FeeLabCost + YT.FeeDirExpCost + YT.FeeReimExpCost + YT.FeeDirConCost + YT.FeeReimConCost), 0.0000), @intAmtCostDecimals) AS ContractTotalCost,
          ROUND(ISNULL((YT.FeeLabBill + YT.FeeDirExpBill + YT.FeeReimExpBill + YT.FeeDirConBill + YT.FeeReimConBill), 0.0000), @intAmtCostDecimals) AS ContractTotalBill,

          COALESCE(
            (
             (YT.FeeLabCost + YT.FeeDirExpCost + YT.FeeReimExpCost + YT.FeeDirConCost + YT.FeeReimConCost)
             /
             NULLIF(@decContractCost, 0.0000)
            )
            , 0
          ) * 100.0000 AS ContractPctTotalCost,

          COALESCE(
            (
             (YT.FeeLabBill + YT.FeeDirExpBill + YT.FeeReimExpBill + YT.FeeDirConBill + YT.FeeReimConBill)
             /
             NULLIF(@decContractBill, 0.0000)
            )
            , 0
          ) * 100.0000 AS ContractPctTotalBill

        FROM ( /* AS YT */
          SELECT
            XT.PlanID,
            XT.TaskID,
            XT.ParentOutlineNumber,
            XT.OutlineNumber,
            XT.OutlineLevel,
            XT.NonLBCDChildrenCount,
            XT.Org,
            XT.ClientID,
            XT.ProjMgr,
			XT.UtilizationScheduleFlg,
            ISNULL(XT.Status, '') AS Status,
            XT.Name,
            XT.WBS1WBS2WBS3,
            XT.SecurityKey,
            XT.WBS1 AS WBS1,
            XT.WBS2 AS WBS2,
            XT.WBS3 AS WBS3,
            ISNULL(XT.HasNotes, 0) AS HasNotes,
            XT.ContractStartDate AS ContractStartDate,
            XT.ContractEndDate AS ContractEndDate,
            XT.FeeLabCost AS FeeLabCost,
            XT.FeeLabBill AS FeeLabBill,
            XT.FeeDirExpCost AS FeeDirExpCost,
            XT.FeeDirExpBill AS FeeDirExpBill,
            XT.FeeReimExpCost AS FeeReimExpCost,
            XT.FeeReimExpBill AS FeeReimExpBill,
            XT.FeeDirConCost AS FeeDirConCost,
            XT.FeeDirConBill AS FeeDirConBill,
            XT.FeeReimConCost AS FeeReimConCost,
            XT.FeeReimConBill AS FeeReimConBill
            FROM @tabWBS AS XT
              WHERE XT.WBS1 = @strWBS1 AND XT.WBS2 = @strWBS2 AND XT.WBS3 = @strWBS3 AND
                ISNULL(XT.Status, '') LIKE CASE WHEN @bActiveWBSOnly = 1 THEN 'A' ELSE '%' END
        ) AS YT
          LEFT JOIN CL ON YT.ClientID = CL.ClientID
          LEFT JOIN EM AS PM ON YT.ProjMgr = PM.Employee
		  LEFT JOIN CFGSuffix PMSuffix ON PMSuffix.Code = PM.Suffix
          LEFT JOIN Organization AS O ON YT.Org = O.Org
   
    END /* END IF (@strMode = 'S') */

  ELSE IF (@strMode = 'C')
    BEGIN
        
      INSERT @tabContract(
        RowID,
        ParentRowID,
        PlanID,
        TaskID,
        ParentOutlineNumber,
        OutlineNumber,
        OutlineLevel,
        RowLevel,
        Name,
        Org,
        OrgName,
        ProjMgr,
		UtilizationScheduleFlg,
        PMFullName,
        ClientName,
        OpportunityID,
        RT_Status,
        WBS1,
        WBS2,
        WBS3,
        TopWBS1,
        TopName,
        SecurityKey,
        HasChildren,
        HasNotes,
        LeafNode,
            
        ContractStartDate,
        ContractEndDate,

        ContractLaborCost,
        ContractLaborBill,
        ContractDirectExpCost,
        ContractDirectExpBill,
        ContractReimbExpCost,
        ContractReimbExpBill,
        ContractDirectConsultCost,
        ContractDirectConsultBill,
        ContractReimbConsultCost,
        ContractReimbConsultBill,
        ContractCompCost,
        ContractCompBill,
        ContractReimbAllowCost,
        ContractReimbAllowBill,
        ContractTotalCost,
        ContractTotalBill,
        ContractPctTotalCost,
        ContractPctTotalBill
      )
        SELECT
          '|' + 
          CASE 
            WHEN @strInputType = 'T' THEN YT.TaskID
            WHEN @strInputType = 'W' THEN YT.WBS1WBS2WBS3
          END AS RowID,
          @strParentRowID AS ParentRowID,
          YT.PlanID AS PlanID,
          YT.TaskID AS TaskID,
          YT.ParentOutlineNumber AS ParentOutlineNumber,
          YT.OutlineNumber AS OutlineNumber,
          YT.OutlineLevel AS OutlineLevel,
          YT.OutlineLevel AS RowLevel,
          YT.Name AS Name,
          ISNULL(YT.Org, '') AS Org,
          ISNULL(O.Name, '') AS OrgName,
          ISNULL(YT.ProjMgr, '') AS ProjMgr,
		  YT.UtilizationScheduleFlg AS UtilizationScheduleFlg,
          CONVERT(nvarchar(255), ISNULL(ISNULL(PM.PreferredName, PM.FirstName), '') + ISNULL((' ' + PM.LastName), '') + ISNULL(', ' + PMSuffix.Suffix, '')) AS PMFullName,
          ISNULL(CL.Name, '') AS ClientName,
          @strOpportunityID AS OpportunityID,
          YT.Status AS RT_Status,
          YT.WBS1 AS WBS1,
          YT.WBS2 AS WBS2,
          YT.WBS3 AS WBS3,
          @strTopWBS1 AS TopWBS1,
          @strTopName AS TopName,
          YT.SecurityKey AS SecurityKey,
          CASE 
            WHEN YT.NonLBCDChildrenCount > 0 
            THEN CONVERT(bit, 1) 
            ELSE CONVERT(bit, 0)
          END AS HasChildren,
          YT.HasNotes AS HasNotes,
          CASE WHEN YT.NonLBCDChildrenCount > 0 THEN CONVERT(bit, 0) ELSE CONVERT(bit,1) END AS LeafNode,
   
          YT.ContractStartDate AS ContractStartDate,
          YT.ContractEndDate AS ContractEndDate,

          ROUND(ISNULL(YT.FeeLabCost, 0.0000), @intAmtCostDecimals) AS ContractLaborCost,
          ROUND(ISNULL(YT.FeeLabBill, 0.0000), @intAmtCostDecimals) AS ContractLaborBill,
          ROUND(ISNULL(YT.FeeDirExpCost, 0.0000), @intAmtCostDecimals) AS ContractDirectExpCost,
          ROUND(ISNULL(YT.FeeDirExpBill, 0.0000), @intAmtCostDecimals) AS ContractDirectExpBill,
          ROUND(ISNULL(YT.FeeReimExpCost, 0.0000), @intAmtCostDecimals) AS ContractReimbExpCost,
          ROUND(ISNULL(YT.FeeReimExpBill, 0.0000), @intAmtCostDecimals) AS ContractReimbExpBill,
          ROUND(ISNULL(YT.FeeDirConCost, 0.0000), @intAmtCostDecimals) AS ContractDirectConsultCost,
          ROUND(ISNULL(YT.FeeDirConBill, 0.0000), @intAmtCostDecimals) AS ContractDirectConsultBill,
          ROUND(ISNULL(YT.FeeReimConCost, 0.0000), @intAmtCostDecimals) AS ContractReimbConsultCost,
          ROUND(ISNULL(YT.FeeReimConBill, 0.0000), @intAmtCostDecimals) AS ContractReimbConsultBill,
          ROUND(ISNULL((YT.FeeLabCost + YT.FeeDirExpCost), 0.0000), @intAmtCostDecimals) AS ContractCompCost,
          ROUND(ISNULL((YT.FeeLabBill + YT.FeeDirExpBill), 0.0000), @intAmtCostDecimals) AS ContractCompBill,
          ROUND(ISNULL((YT.FeeReimExpCost + YT.FeeReimConCost), 0.0000), @intAmtCostDecimals) AS ContractReimbAllowCost,
          ROUND(ISNULL((YT.FeeReimExpBill + YT.FeeReimConBill), 0.0000), @intAmtCostDecimals) AS ContractReimbAllowBill,
          ROUND(ISNULL((YT.FeeLabCost + YT.FeeDirExpCost + YT.FeeReimExpCost + YT.FeeDirConCost + YT.FeeReimConCost), 0.0000), @intAmtCostDecimals) AS ContractTotalCost,
          ROUND(ISNULL((YT.FeeLabBill + YT.FeeDirExpBill + YT.FeeReimExpBill + YT.FeeDirConBill + YT.FeeReimConBill), 0.0000), @intAmtCostDecimals) AS ContractTotalBill,

          COALESCE(
            (
             (YT.FeeLabCost + YT.FeeDirExpCost + YT.FeeReimExpCost + YT.FeeDirConCost + YT.FeeReimConCost)
             /
             NULLIF(@decContractCost, 0.0000)
            )
            , 0
          ) * 100.0000 AS ContractPctTotalCost,

          COALESCE(
            (
             (YT.FeeLabBill + YT.FeeDirExpBill + YT.FeeReimExpBill + YT.FeeDirConBill + YT.FeeReimConBill)
             /
             NULLIF(@decContractBill, 0.0000)
            )
            , 0
          ) * 100.0000 AS ContractPctTotalBill

        FROM ( /* AS YT */
          SELECT
            XT.PlanID,
            XT.TaskID,
            XT.ParentOutlineNumber,
            XT.OutlineNumber,
            XT.OutlineLevel,
            XT.NonLBCDChildrenCount,
            XT.Org,
            XT.ClientID,
            XT.ProjMgr,
			XT.UtilizationScheduleFlg,
            ISNULL(XT.Status, '') AS Status,
            XT.Name,
            XT.WBS1WBS2WBS3,
            XT.SecurityKey,
            XT.WBS1 AS WBS1,
            XT.WBS2 AS WBS2,
            XT.WBS3 AS WBS3,
            ISNULL(XT.HasNotes, 0) AS HasNotes,
            XT.ContractStartDate AS ContractStartDate,
            XT.ContractEndDate AS ContractEndDate,
            XT.FeeLabCost AS FeeLabCost,
            XT.FeeLabBill AS FeeLabBill,
            XT.FeeDirExpCost AS FeeDirExpCost,
            XT.FeeDirExpBill AS FeeDirExpBill,
            XT.FeeReimExpCost AS FeeReimExpCost,
            XT.FeeReimExpBill AS FeeReimExpBill,
            XT.FeeDirConCost AS FeeDirConCost,
            XT.FeeDirConBill AS FeeDirConBill,
            XT.FeeReimConCost AS FeeReimConCost,
            XT.FeeReimConBill AS FeeReimConBill
            FROM @tabWBS AS XT
              INNER JOIN @tabWBS AS PXT ON PXT.OutlineNumber = XT.ParentOutlineNumber
              WHERE PXT.WBS1 = @strWBS1 AND PXT.WBS2 = @strWBS2 AND PXT.WBS3 = @strWBS3 AND
                ISNULL(@strTaskStatus, '') LIKE CASE WHEN @bActiveWBSOnly = 1 THEN 'A' ELSE '%' END AND
                ISNULL(XT.Status, '') LIKE CASE WHEN @bActiveWBSOnly = 1 THEN 'A' ELSE '%' END
        ) AS YT
          LEFT JOIN CL ON YT.ClientID = CL.ClientID
          LEFT JOIN EM AS PM ON YT.ProjMgr = PM.Employee
		  LEFT JOIN CFGSuffix PMSuffix ON PMSuffix.Code = PM.Suffix
          LEFT JOIN Organization AS O ON YT.Org = O.Org

    END /* END ELSE IF (@strMode = 'C') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
