SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EIUDC_GetD] (@s varchar(max))
RETURNS varchar(max)
WITH ENCRYPTION
BEGIN
	return Convert(varchar(max),DecryptByPassPhrase('CCG_EIUDC_7150',Convert(varbinary(max), @s, 2)))
END
GO
