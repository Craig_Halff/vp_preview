SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabHasMissingJTDAssignments]
  (@strPlanID varchar(32))

  RETURNS @tabResults TABLE(
    HasMissingJTDLab varchar(1),
    HasMissingJTDExp varchar(1),
    HasMissingJTDCon varchar(1)
  )

BEGIN

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @strHasMissingJTDLab varchar(1) = 'N'
  DECLARE @strHasMissingJTDExp varchar(1) = 'N'
  DECLARE @strHasMissingJTDCon varchar(1) = 'N'
  DECLARE @strCompany nvarchar(14)
  DECLARE @strExpTab varchar(1) = 'N'
  DECLARE @strConTab varchar(1) = 'N'

  DECLARE @strLCLevel1Enabled varchar(1) = 'N'
  DECLARE @strLCLevel2Enabled varchar(1) = 'N'
  DECLARE @strLCLevel3Enabled varchar(1) = 'N'
  DECLARE @strLCLevel4Enabled varchar(1) = 'N'
  DECLARE @strLCLevel5Enabled varchar(1) = 'N'

  DECLARE @strLCDelimiter varchar(1) = ''

  DECLARE @strLC1WildCard nvarchar(14) = ''
  DECLARE @strLC2WildCard nvarchar(14) = ''
  DECLARE @strLC3WildCard nvarchar(14) = ''
  DECLARE @strLC4WildCard nvarchar(14) = ''
  DECLARE @strLC5WildCard nvarchar(14) = ''

  DECLARE @bitUseLaborCode bit = 0

  DECLARE @dtJTDDate datetime

  DECLARE @strWBS1 nvarchar(30)

  DECLARE @intExpWBSLevel int 
  DECLARE @intConWBSLevel int 

  DECLARE @siLCLevels smallint = 0
  DECLARE @siLC1Start smallint = 0
  DECLARE @siLC2Start smallint = 0
  DECLARE @siLC3Start smallint = 0
  DECLARE @siLC4Start smallint = 0
  DECLARE @siLC5Start smallint = 0
  DECLARE @siLC1Length smallint = 0
  DECLARE @siLC2Length smallint = 0
  DECLARE @siLC3Length smallint = 0
  DECLARE @siLC4Length smallint = 0
  DECLARE @siLC5Length smallint = 0

  DECLARE @tabTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int,
    ChildrenCount int,
    HasLBCD bit
    UNIQUE (PlanID, TaskID, WBS1, WBS2, WBS3, ParentOutlineNumber, OutlineNumber, HasLBCD)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save Format Settings to be used later.

  SELECT
    @siLCLevels = LCLevels,
    @strLCDelimiter = ISNULL(LTRIM(RTRIM(LCDelimiter)), ''),
    @siLC1Start = LC1Start,
    @siLC2Start = LC2Start,
    @siLC3Start = LC3Start,
    @siLC4Start = LC4Start,
    @siLC5Start = LC5Start,
    @siLC1Length = LC1Length,
    @siLC2Length = LC2Length,
    @siLC3Length = LC3Length,
    @siLC4Length = LC4Length,
    @siLC5Length = LC5Length
    FROM CFGFormat

  SELECT 
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE())))
    FROM CFGRMSettings

  SELECT
    @strCompany = P.Company,
    @strWBS1 = P.WBS1,
    @intExpWBSLevel = P.ExpWBSLevel,
    @intConWBSLevel = P.ConWBSLevel,
    @strLCLevel1Enabled = LCLevel1Enabled,
    @strLCLevel2Enabled = LCLevel2Enabled,
    @strLCLevel3Enabled = LCLevel3Enabled,
    @strLCLevel4Enabled = LCLevel4Enabled,
    @strLCLevel5Enabled = LCLevel5Enabled
    FROM PNPlan AS P
    WHERE P.PlanID = @strPlanID

  SELECT
    @strExpTab = ExpTab,
    @strConTab = ConTab
    FROM CFGResourcePlanning
    WHERE Company = @strCompany

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save PNTask rows from this Plan to be used later so that we don't have to walk through PNTask may times.
  -- Only need to grab the rows at levels WBS1/WBS2/WBS3, and not the Labor Code level rows.

  INSERT @tabTask(
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    ParentOutlineNumber,
    OutlineNumber,
    OutlineLevel,
    ChildrenCount,
    HasLBCD
  )
    SELECT
      T.PlanID AS PlanID,
      T.TaskID AS TaskID,
      T.WBS1 AS WBS1,
      ISNULL(T.WBS2, ' ') AS WBS2,
      ISNULL(T.WBS3, ' ') AS WBS3,
      T.ParentOutlineNumber AS ParentOutlineNumber,
      T.OutlineNumber AS OutlineNumber,
      T.OutlineLevel + 1 As OutLineLevel,
      T.ChildrenCount AS ChildrenCount,
      CASE
        WHEN (EXISTS(SELECT 'X' FROM PNTask AS LT WHERE LT.PlanID = @strPlanID AND T.OutlineNumber = LT.ParentOutlineNumber AND LT.WBSType = 'LBCD'))
        THEN 1
        ELSE 0
      END AS HasLBCD
      FROM PNTask AS T
      WHERE T.PlanID = @strPlanID AND T.WBSType IN ('WBS1', 'WBS2', 'WBS3')

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- Labor.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @tabRawJTDLab TABLE (
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    Employee nvarchar(20) COLLATE database_default
    UNIQUE (WBS1, WBS2, WBS3, LaborCode, Employee)
  )

  DECLARE @tabJTDLab TABLE (
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    Employee nvarchar(20) COLLATE database_default
    UNIQUE (WBS1, WBS2, WBS3, LaborCode, Employee)
  )

  DECLARE @tabAssignmentLab TABLE (
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    Employee nvarchar(20) COLLATE database_default
    UNIQUE (WBS1, WBS2, WBS3, LaborCode, Employee)
  )

  -- Only need to check for missing Labor JTD when there is JTD data.

  IF(
    EXISTS(SELECT 'X' FROM LD WITH (NOLOCK) WHERE WBS1 = @strWBS1 AND ProjectCost = 'Y' AND TransDate <= @dtJTDDate) OR
    EXISTS(SELECT 'X' FROM tkDetail AS TD WITH (NOLOCK) WHERE WBS1 = @strWBS1 AND TransDate <= @dtJTDDate) OR
    EXISTS(SELECT 'X' FROM tsDetail AS TD WITH (NOLOCK) WHERE WBS1 = @strWBS1 AND TD.TransDate <= @dtJTDDate)
  )
    BEGIN

      -- Determine whether various settings will lead to the need to deal with the Labor Code rows.

      SELECT @bitUseLaborCode = 
        CASE
          WHEN (
            @siLCLevels > 0 AND
            /* The Plan had enabled wildcard Labor Code */
            (
              @strLCLevel1Enabled = 'Y' OR 
              @strLCLevel2Enabled = 'Y' OR 
              @strLCLevel3Enabled = 'Y' OR 
              @strLCLevel4Enabled = 'Y' OR 
              @strLCLevel5Enabled = 'Y' 
            ) AND
            /* The Plan already had some Labor Code level existed */
            (EXISTS(SELECT 'X' FROM @tabTask WHERE HasLBCD = 1))
          )
          THEN 1
          ELSE 0
        END

      -- If the Plan has wildcards in Labor Code, then prepare for them.

      IF(@bitUseLaborCode = 1)
        BEGIN

          SELECT 
            @strLC1WildCard = REPLICATE('_', @siLC1Length),
            @strLC2WildCard = REPLICATE('_', @siLC2Length),
            @strLC3WildCard = REPLICATE('_', @siLC3Length),
            @strLC4WildCard = REPLICATE('_', @siLC4Length),
            @strLC5WildCard = REPLICATE('_', @siLC5Length)

        END /* END IF(@bitUseLaborCode = 1) */

      -- Save {WBS1, WBS2, WBS3, Employee} from LD table into @tabRawJTDLab.
      -- Only select combination of {WBS1, WBS2, WBS3, Employee} with non-zero JTDHrs.

      INSERT @tabRawJTDLab(
        WBS1,
        WBS2,
        WBS3,
        LaborCode,
        Employee
      ) 
        SELECT DISTINCT
          Y.WBS1 AS WBS1,
          Y.WBS2 AS WBS2,
          Y.WBS3 AS WBS3,
          Y.LaborCode AS LaborCode,
          Y.Employee AS Employee
          FROM ( /* Y */
            SELECT
              X.WBS1 AS WBS1,
              X.WBS2 AS WBS2,
              X.WBS3 AS WBS3,
              X.LaborCode AS LaborCode,
              SUM(X.JTDHours) AS JTDHours,
              X.Employee AS Employee
              FROM ( /* X */
                SELECT  
                  LD.WBS1,
                  LD.WBS2,
                  LD.WBS3,
                  LD.LaborCode,
                  SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDHours,
                  LD.Employee
                  FROM LD WITH (NOLOCK)
                  WHERE LD.WBS1 = @strWBS1 AND LD.Employee IS NOT NULL AND
                    LD.ProjectCost = 'Y' AND LD.TransDate <= @dtJTDDate
                  GROUP BY LD.WBS1, LD.WBS2, LD.WBS3, LD.LaborCode, LD.Employee
                  HAVING SUM(RegHrs + OvtHrs + SpecialOvtHrs) <> 0
                UNION
                SELECT  
                  TD.WBS1,
                  TD.WBS2,
                  TD.WBS3,
                  TD.LaborCode,
                  SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDHours,
                  TD.Employee
                  FROM tkDetail AS TD WITH (NOLOCK) -- TD table has entries at only the leaf level.
                    INNER JOIN EM  WITH (NOLOCK) ON TD.Employee = EM.Employee  
                    INNER JOIN tkMaster AS TM WITH (NOLOCK) ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
                  WHERE TD.WBS1 = @strWBS1 AND TD.Employee IS NOT NULL AND TD.TransDate <= @dtJTDDate  
                  GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.LaborCode, TD.Employee   
                  HAVING SUM(RegHrs + OvtHrs + SpecialOvtHrs) <> 0
                UNION
                SELECT  
                  TD.WBS1,
                  TD.WBS2,
                  TD.WBS3,
                  TD.LaborCode, 
                  SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDHours,
                  TD.Employee
                  FROM tsDetail AS TD  WITH (NOLOCK)
                    INNER JOIN tsControl AS TC  WITH (NOLOCK) ON TD.Batch = TC.Batch AND TC.Posted = 'N'
                  WHERE TD.WBS1 = @strWBS1 AND TD.Employee IS NOT NULL AND TD.TransDate <= @dtJTDDate  
                  GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.LaborCode, TD.Employee    
                  HAVING SUM(RegHrs + OvtHrs + SpecialOvtHrs) <> 0
              ) AS X
              GROUP BY X.WBS1, X.WBS2, X.WBS3, X.LaborCode, X.Employee
              HAVING SUM(X.JTDHours) <> 0
          ) AS Y

      -- Save {WBS1, WBS2, WBS3, Employee} from PNAssignment table into @tabAssignmentLab.

      INSERT @tabAssignmentLab(
        WBS1,
        WBS2,
        WBS3,
        LaborCode, 
        Employee
      ) 
        SELECT DISTINCT
          A.WBS1 AS WBS1,
          ISNULL(A.WBS2, ' ') AS WBS2,
          ISNULL(A.WBS3, ' ') AS WBS3,
          A.LaborCode AS LaborCode, 
          A.ResourceID AS Employee
          FROM PNAssignment AS A WITH (NOLOCK)
          WHERE A.PlanID = @strPlanID AND A.ResourceID IS NOT NULL

      -- Depending on whether Labor Code was in use or not, consolidate JTD Employees accordingly.

      IF(@bitUseLaborCode = 0)
        BEGIN

          INSERT @tabJTDLab(
            WBS1,
            WBS2,
            WBS3,
            LaborCode,
            Employee
          ) 
            SELECT DISTINCT
              J.WBS1 AS WBS1,
              J.WBS2 AS WBS2,
              J.WBS3 AS WBS3,
              NULL AS LaborCode,
              J.Employee AS Employee
              FROM @tabRawJTDLab AS J

        END /* END IF(@bitUseLaborCode = 0) */

      ELSE /* ELSE (@bitUseLaborCode = 1) */
        BEGIN

          INSERT @tabJTDLab(
            WBS1,
            WBS2,
            WBS3,
            LaborCode,
            Employee
          ) 
            SELECT DISTINCT
              J.WBS1 AS WBS1,
              J.WBS2 AS WBS2,
              J.WBS3 AS WBS3,

              /* Replace Labor Code string at each level with wildcard string,   */
              /* if the Plan's setting dictated that level contained wildcard    */
              
              CASE
                WHEN (J.LaborCode IS NULL OR T.HasLBCD = 0) 
                THEN NULL
                ELSE

                  /* Labor Code Level 1 */
                  CASE
                    WHEN @strLCLevel1Enabled = 'N' 
                    THEN @strLC1WildCard
                    ELSE SUBSTRING(J.LaborCode, @siLC1Start, @siLC1Length)
                  END +

                  /* Labor Code Level 2 */
                  CASE WHEN (@siLCLevels >= 2) 
                    THEN ISNULL((ISNULL(@strLCDelimiter, '') + 
                      CASE 
                        WHEN @strLCLevel2Enabled = 'N' 
                        THEN @strLC2WildCard 
                        ELSE SUBSTRING(J.LaborCode, @siLC2Start, @siLC2Length) 
                      END), '') 
                    ELSE ''
                  END + 

                  /* Labor Code Level 3 */
                  CASE WHEN (@siLCLevels >= 3) 
                    THEN ISNULL((ISNULL(@strLCDelimiter, '') + 
                      CASE 
                        WHEN @strLCLevel3Enabled = 'N' 
                        THEN @strLC3WildCard 
                        ELSE SUBSTRING(J.LaborCode, @siLC3Start, @siLC3Length) 
                      END), '') 
                    ELSE ''
                  END + 

                  /* Labor Code Level 4 */
                  CASE WHEN (@siLCLevels >= 4) 
                    THEN ISNULL((ISNULL(@strLCDelimiter, '') + 
                      CASE 
                        WHEN @strLCLevel4Enabled = 'N' 
                        THEN @strLC4WildCard 
                        ELSE SUBSTRING(J.LaborCode, @siLC4Start, @siLC4Length) 
                      END), '') 
                    ELSE ''
                  END + 

                  /* Labor Code Level 5 */
                  CASE WHEN (@siLCLevels >= 5) 
                    THEN ISNULL((ISNULL(@strLCDelimiter, '') + 
                      CASE 
                        WHEN @strLCLevel5Enabled = 'N' 
                        THEN @strLC5WildCard 
                        ELSE SUBSTRING(J.LaborCode, @siLC5Start, @siLC5Length) 
                      END), '') 
                    ELSE ''
                  END

              END AS LaborCode,

              J.Employee AS Employee
              FROM @tabRawJTDLab AS J
                INNER JOIN @tabTask AS T ON J.WBS1 = T.WBS1 AND J.WBS2 = T.WBS2 AND J.WBS3 = T.WBS3

        END /* END ELSE (@bitUseLaborCode = 1) */

      -- Compare @tabJTDLab against @tabAssignmentLab

      SELECT @strHasMissingJTDLab =
        CASE
          WHEN EXISTS(
            SELECT
              WBS1,
              WBS2,
              WBS3,
              LaborCode,
              Employee
              FROM @tabJTDLab
            EXCEPT
            SELECT
              WBS1,
              WBS2,
              WBS3,
              LaborCode,
              Employee
              FROM @tabAssignmentLab
          )
          THEN 'Y'
          ELSE 'N'
        END

    END /* END IF */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- Expense.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @tabRawJTDExp TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    Account nvarchar(13) COLLATE database_default,
    Vendor nvarchar(20) COLLATE database_default
    UNIQUE (RowID, WBS1, WBS2, WBS3, Account, Vendor)
  )

  DECLARE @tabJTDExp TABLE (
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    Account nvarchar(13) COLLATE database_default,
    Vendor nvarchar(20) COLLATE database_default
    UNIQUE (WBS1, WBS2, WBS3, Account, Vendor)
  )

  DECLARE @tabAssignmentExp TABLE (
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    Account nvarchar(13) COLLATE database_default,
    Vendor nvarchar(20) COLLATE database_default,
    UNIQUE (WBS1, WBS2, WBS3, Account, Vendor)
  )

  -- Only need to check for missing Expense JTD when there is JTD data.

  IF(
    @strExpTab = 'Y' AND (
      EXISTS(
        SELECT 'X' 
          FROM LedgerAP AS Ledger WITH (NOLOCK)
            INNER JOIN CA WITH (NOLOCK) ON Ledger.Account = CA.Account
          WHERE WBS1 = @strWBS1 AND ProjectCost = 'Y' AND TransDate <= @dtJTDDate AND CA.Type IN (5, 7, 9)
      ) OR
      EXISTS(
        SELECT 'X' 
          FROM LedgerAR AS Ledger WITH (NOLOCK) 
            INNER JOIN CA WITH (NOLOCK) ON Ledger.Account = CA.Account
          WHERE WBS1 = @strWBS1 AND ProjectCost = 'Y' AND TransDate <= @dtJTDDate AND CA.Type IN (5, 7, 9)
      ) OR
      EXISTS(
        SELECT 'X' 
          FROM LedgerEX AS Ledger WITH (NOLOCK) 
            INNER JOIN CA WITH (NOLOCK) ON Ledger.Account = CA.Account
          WHERE WBS1 = @strWBS1 AND ProjectCost = 'Y' AND TransDate <= @dtJTDDate AND CA.Type IN (5, 7, 9)
      ) OR
      EXISTS(
        SELECT 'X' 
          FROM LedgerMisc AS Ledger WITH (NOLOCK) 
            INNER JOIN CA WITH (NOLOCK) ON Ledger.Account = CA.Account
          WHERE WBS1 = @strWBS1 AND ProjectCost = 'Y' AND TransDate <= @dtJTDDate AND CA.Type IN (5, 7, 9)
      ) OR
      EXISTS( -- POC with no Change Orders.
        SELECT 'X' 
          FROM POCommitment AS POC WITH (NOLOCK) 
            INNER JOIN PODetail AS POD WITH (NOLOCK) ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
            INNER JOIN POMaster AS POM WITH (NOLOCK) ON POD.MasterPKey = POM.MasterPKey
            INNER JOIN CA WITH (NOLOCK) ON POC.Account = CA.Account
          WHERE POC.WBS1 = @strWBS1 AND POM.OrderDate <= @dtJTDDate AND CA.Type IN (5, 7, 9)  
      ) OR
      EXISTS( -- POC with Change Orders.
        SELECT 'X' 
          FROM POCommitment AS POC WITH (NOLOCK) 
            INNER JOIN PODetail AS POD WITH (NOLOCK) ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
            INNER JOIN POCOMaster AS POCOM WITH (NOLOCK) ON POD.COPKey = POCOM.PKey
            INNER JOIN POMaster AS POM WITH (NOLOCK) ON POCOM.MasterPKey = POM.MasterPKey
            INNER JOIN CA WITH (NOLOCK) ON POC.Account = CA.Account
          WHERE POC.WBS1 = @strWBS1 AND POCOM.OrderDate <= @dtJTDDate AND CA.Type IN (5, 7, 9) 
      )
    )
  )
    BEGIN

      -- Save {WBS1, WBS2, WBS3, Account, Vendor} from Ledger tables into @tabRawJTDExp.
      -- Only select combination of {WBS1, WBS2, WBS3, Account, Vendor} with non-zero JTDCost and JTDBill.

      INSERT @tabRawJTDExp(
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor
      )
        SELECT 
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.Account AS Account,
          X.Vendor AS Vendor
          FROM ( /* X */
            SELECT 
              XE.WBS1 AS WBS1,
              XE.WBS2 AS WBS2,
              XE.WBS3 AS WBS3,
              XE.Account AS Account,
              XE.Vendor AS Vendor,
              SUM(XE.JTDCost) AS JTDCost,
              SUM(XE.JTDBill) AS JTDBill
              FROM ( /* XE */
                SELECT
                  Ledger.WBS1 AS WBS1,
                  Ledger.WBS2 AS WBS2,
                  Ledger.WBS3 AS WBS3,
                  CA.Account AS Account,
                  Ledger.Vendor AS Vendor,
                  SUM(Ledger.AmountProjectCurrency) AS JTDCost,
                  SUM(Ledger.BillExt) AS JTDBill
                  FROM LedgerAP Ledger WITH (NOLOCK)
                    INNER JOIN CA WITH (NOLOCK) ON Ledger.Account = CA.Account 
                  WHERE Ledger.WBS1 = @strWBS1 
                    AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate
                    AND CA.Type IN (5, 7, 9)
                  GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, CA.Account, Ledger.Vendor
                  HAVING (SUM(Ledger.AmountProjectCurrency) <> 0 OR SUM(Ledger.BillExt) <> 0)
                UNION ALL
                SELECT
                  Ledger.WBS1 AS WBS1,
                  Ledger.WBS2 AS WBS2,
                  Ledger.WBS3 AS WBS3,
                  CA.Account AS Account,
                  Ledger.Vendor AS Vendor,
                  SUM(Ledger.AmountProjectCurrency) AS JTDCost,
                  SUM(Ledger.BillExt) AS JTDBill
                  FROM LedgerEX Ledger WITH (NOLOCK) 		   
                    INNER JOIN CA  WITH (NOLOCK) ON Ledger.Account = CA.Account 
                  WHERE Ledger.WBS1 = @strWBS1 
                    AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate
                    AND CA.Type IN (5, 7, 9)
                  GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, CA.Account, Ledger.Vendor
                  HAVING (SUM(Ledger.AmountProjectCurrency) <> 0 OR SUM(Ledger.BillExt) <> 0)
                UNION ALL
                SELECT
                  Ledger.WBS1 AS WBS1,
                  Ledger.WBS2 AS WBS2,
                  Ledger.WBS3 AS WBS3,
                  CA.Account AS Account,
                  Ledger.Vendor AS Vendor,
                  SUM(Ledger.AmountProjectCurrency) AS JTDCost,
                  SUM(Ledger.BillExt) AS JTDBill
                  FROM LedgerAR Ledger WITH (NOLOCK)
                    INNER JOIN CA WITH (NOLOCK) ON Ledger.Account = CA.Account 
                  WHERE Ledger.WBS1 = @strWBS1 
                    AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate
                    AND CA.Type IN (5, 7, 9)
                  GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, CA.Account, Ledger.Vendor
                  HAVING (SUM(Ledger.AmountProjectCurrency) <> 0 OR SUM(Ledger.BillExt) <> 0)
                UNION ALL
                SELECT
                  Ledger.WBS1 AS WBS1,
                  Ledger.WBS2 AS WBS2,
                  Ledger.WBS3 AS WBS3,
                  CA.Account AS Account,
                  Ledger.Vendor AS Vendor,
                  SUM(Ledger.AmountProjectCurrency) AS JTDCost,
                  SUM(Ledger.BillExt) AS JTDBill
                  FROM LedgerMisc Ledger WITH (NOLOCK) 		   
                    INNER JOIN CA WITH (NOLOCK) ON Ledger.Account = CA.Account 
                  WHERE Ledger.WBS1 = @strWBS1 
                    AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate
                    AND CA.Type IN (5, 7, 9)
                  GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, CA.Account, Ledger.Vendor
                  HAVING (SUM(Ledger.AmountProjectCurrency) <> 0 OR SUM(Ledger.BillExt) <> 0)
                UNION ALL
                SELECT -- POC with no Change Orders.
                  POC.WBS1 AS WBS1,
                  POC.WBS2 AS WBS2,
                  POC.WBS3 AS WBS3,
                  POC.Account AS Account,
                  POM.Vendor AS Vendor,
                  SUM(POC.AmountProjectCurrency) AS JTDCost,
                  SUM(POC.BillExt) AS JTDBill
                  FROM POCommitment AS POC WITH (NOLOCK) 
                    INNER JOIN PODetail AS POD WITH (NOLOCK) ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
                    INNER JOIN POMaster AS POM WITH (NOLOCK) ON POD.MasterPKey = POM.MasterPKey
                    INNER JOIN CA WITH (NOLOCK) ON POC.Account = CA.Account
                  WHERE POM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (5, 7, 9)  
                  GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
                  HAVING (SUM(POC.AmountProjectCurrency) <> 0 OR SUM(POC.BillExt) <> 0)
                UNION ALL
                SELECT -- POC with Change Orders.
                  POC.WBS1 AS WBS1,
                  POC.WBS2 AS WBS2,
                  POC.WBS3 AS WBS3,
                  POC.Account AS Account,
                  POM.Vendor AS Vendor,
                  SUM(POC.AmountProjectCurrency)  AS JTDCost,
                  SUM(POC.BillExt) AS JTDBill
                  FROM POCommitment AS POC WITH (NOLOCK) 
                    INNER JOIN PODetail AS POD WITH (NOLOCK) ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
                    INNER JOIN POCOMaster AS POCOM WITH (NOLOCK) ON POD.COPKey = POCOM.PKey
                    INNER JOIN POMaster AS POM WITH (NOLOCK) ON POCOM.MasterPKey = POM.MasterPKey
                    INNER JOIN CA WITH (NOLOCK) ON POC.Account = CA.Account
                  WHERE POCOM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (5, 7, 9) 
                  GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
                  HAVING (SUM(POC.AmountProjectCurrency) <> 0 OR SUM(POC.BillExt) <> 0)
              ) AS XE
              GROUP BY XE.WBS1, XE.WBS2, XE.WBS3, XE.Account, XE.Vendor
              HAVING (SUM(XE.JTDCost) <> 0 OR SUM(XE.JTDBill) <> 0)
          ) AS X

      -- Consolidate the JTD rows and line them up with PNTask rows which are less than or equal to @intExpWBSLevel.
      -- A PNTask Tree may have lesser levels than the @intExpWBSLevel.

      INSERT @tabJTDExp(
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor
      )
        SELECT DISTINCT
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.Account AS Account,
          X.Vendor AS Vendor
          FROM (
            SELECT DISTINCT
              W.WBS1 AS WBS1,
              W.WBS2 AS WBS2,
              W.WBS3 AS WBS3,
              RJE.Account AS Account,
              RJE.Vendor AS Vendor,
              CASE
                WHEN OutlineLevel < @intExpWBSLevel
                THEN
                  CASE
                    WHEN (ChildrenCount > 0 AND HasLBCD = 1) THEN 1
                    WHEN (ChildrenCount > 0 AND HasLBCD = 0) THEN 0
                    ELSE 1
                  END
                ELSE 1
              END AS IsLeaf
              FROM  @tabTask AS W
                INNER JOIN @tabRawJTDExp AS RJE 
                  ON RJE.WBS1 = W.WBS1 
                    AND RJE.WBS2 LIKE  CASE WHEN W.WBS2 = ' ' THEN '%' ELSE W.WBS2 + '%' END
                    AND RJE.WBS3 LIKE  CASE WHEN W.WBS3 = ' ' THEN '%' ELSE W.WBS3 + '%' END
                    AND W.OutlineLevel <= @intExpWBSLevel
          ) AS X
          WHERE X.IsLeaf = 1

      -- Save {WBS1, WBS2, WBS3, Account, Vendor} from PNExpense table into @tabAssignmentExp.

      INSERT @tabAssignmentExp(
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor
      )
        SELECT DISTINCT
          E.WBS1 AS WBS1,
          ISNULL(E.WBS2, ' ') AS WBS2,
          ISNULL(E.WBS3, ' ') AS WBS3,
          E.Account AS Account,
          E.Vendor AS Vendor
          FROM PNExpense AS E WITH (NOLOCK)
          WHERE E.PlanID = @strPlanID

      -- Compare @tabJTDExp against @tabAssignmentExp

      SELECT @strHasMissingJTDExp =
        CASE
          WHEN EXISTS(
            SELECT
              WBS1,
              WBS2,
              WBS3,
              Account,
              Vendor
              FROM @tabJTDExp
            EXCEPT
            SELECT
              WBS1,
              WBS2,
              WBS3,
              Account,
              Vendor
              FROM @tabAssignmentExp
          )
          THEN 'Y'
          ELSE 'N'
        END

    END /* END IF */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- Consultant.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @tabRawJTDCon TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    Account nvarchar(13) COLLATE database_default,
    Vendor nvarchar(20) COLLATE database_default
    UNIQUE (RowID, WBS1, WBS2, WBS3, Account, Vendor)
  )

  DECLARE @tabJTDCon TABLE (
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    Account nvarchar(13) COLLATE database_default,
    Vendor nvarchar(20) COLLATE database_default
    UNIQUE (WBS1, WBS2, WBS3, Account, Vendor)
  )

  DECLARE @tabAssignmentCon TABLE (
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    Account nvarchar(13) COLLATE database_default,
    Vendor nvarchar(20) COLLATE database_default,
    UNIQUE (WBS1, WBS2, WBS3, Account, Vendor)
  )

  -- Only need to check for missing Consultant JTD when there is JTD data.

  IF(
    @strConTab = 'Y' AND (
      EXISTS(
        SELECT 'X' 
          FROM LedgerAP AS Ledger WITH (NOLOCK)
            INNER JOIN CA WITH (NOLOCK) ON Ledger.Account = CA.Account
          WHERE WBS1 = @strWBS1 AND ProjectCost = 'Y' AND TransDate <= @dtJTDDate AND CA.Type IN (6, 8)
      ) OR
      EXISTS(
        SELECT 'X' 
          FROM LedgerAR AS Ledger WITH (NOLOCK) 
            INNER JOIN CA WITH (NOLOCK) ON Ledger.Account = CA.Account
          WHERE WBS1 = @strWBS1 AND ProjectCost = 'Y' AND TransDate <= @dtJTDDate AND CA.Type IN (6, 8)
      ) OR
      EXISTS(
        SELECT 'X' 
          FROM LedgerEX AS Ledger WITH (NOLOCK) 
            INNER JOIN CA WITH (NOLOCK) ON Ledger.Account = CA.Account
          WHERE WBS1 = @strWBS1 AND ProjectCost = 'Y' AND TransDate <= @dtJTDDate AND CA.Type IN (6, 8)
      ) OR
      EXISTS(
        SELECT 'X' 
          FROM LedgerMisc AS Ledger WITH (NOLOCK) 
            INNER JOIN CA WITH (NOLOCK) ON Ledger.Account = CA.Account
          WHERE WBS1 = @strWBS1 AND ProjectCost = 'Y' AND TransDate <= @dtJTDDate AND CA.Type IN (6, 8)
      ) OR
      EXISTS( -- POC with no Change Orders.
        SELECT 'X' 
          FROM POCommitment AS POC WITH (NOLOCK) 
            INNER JOIN PODetail AS POD WITH (NOLOCK) ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
            INNER JOIN POMaster AS POM WITH (NOLOCK) ON POD.MasterPKey = POM.MasterPKey
            INNER JOIN CA WITH (NOLOCK) ON POC.Account = CA.Account
          WHERE POC.WBS1 = @strWBS1 AND POM.OrderDate <= @dtJTDDate AND CA.Type IN (6, 8)  
      ) OR
      EXISTS( -- POC with Change Orders.
        SELECT 'X' 
          FROM POCommitment AS POC WITH (NOLOCK) 
            INNER JOIN PODetail AS POD WITH (NOLOCK) ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
            INNER JOIN POCOMaster AS POCOM WITH (NOLOCK) ON POD.COPKey = POCOM.PKey
            INNER JOIN POMaster AS POM WITH (NOLOCK) ON POCOM.MasterPKey = POM.MasterPKey
            INNER JOIN CA WITH (NOLOCK) ON POC.Account = CA.Account
          WHERE POC.WBS1 = @strWBS1 AND POCOM.OrderDate <= @dtJTDDate AND CA.Type IN (6, 8) 
      )
    )
  )
    BEGIN

      -- Save {WBS1, WBS2, WBS3, Account, Vendor} from Ledger tables into @tabRawJTDCon.
      -- Only select combination of {WBS1, WBS2, WBS3, Account, Vendor} with non-zero JTDCost and JTDBill.

      INSERT @tabRawJTDCon(
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor
      )
        SELECT 
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.Account AS Account,
          X.Vendor AS Vendor
          FROM ( /* X */
            SELECT 
              XE.WBS1 AS WBS1,
              XE.WBS2 AS WBS2,
              XE.WBS3 AS WBS3,
              XE.Account AS Account,
              XE.Vendor AS Vendor,
              SUM(XE.JTDCost) AS JTDCost,
              SUM(XE.JTDBill) AS JTDBill
              FROM ( /* XE */
                SELECT
                  Ledger.WBS1 AS WBS1,
                  Ledger.WBS2 AS WBS2,
                  Ledger.WBS3 AS WBS3,
                  CA.Account AS Account,
                  Ledger.Vendor AS Vendor,
                  SUM(Ledger.AmountProjectCurrency) AS JTDCost,
                  SUM(Ledger.BillExt) AS JTDBill
                  FROM LedgerAP Ledger WITH (NOLOCK)		   
                    INNER JOIN CA WITH (NOLOCK) ON Ledger.Account = CA.Account 
                  WHERE Ledger.WBS1 = @strWBS1 
                    AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate
                    AND CA.Type IN (6, 8)
                  GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, CA.Account, Ledger.Vendor
                  HAVING (SUM(Ledger.AmountProjectCurrency) <> 0 OR SUM(Ledger.BillExt) <> 0)
                UNION ALL
                SELECT
                  Ledger.WBS1 AS WBS1,
                  Ledger.WBS2 AS WBS2,
                  Ledger.WBS3 AS WBS3,
                  CA.Account AS Account,
                  Ledger.Vendor AS Vendor,
                  SUM(Ledger.AmountProjectCurrency) AS JTDCost,
                  SUM(Ledger.BillExt) AS JTDBill
                  FROM LedgerEX Ledger WITH (NOLOCK) 		   
                    INNER JOIN CA  WITH (NOLOCK) ON Ledger.Account = CA.Account 
                  WHERE Ledger.WBS1 = @strWBS1 
                    AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate
                    AND CA.Type IN (6, 8)
                  GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, CA.Account, Ledger.Vendor
                  HAVING (SUM(Ledger.AmountProjectCurrency) <> 0 OR SUM(Ledger.BillExt) <> 0)
                UNION ALL
                SELECT
                  Ledger.WBS1 AS WBS1,
                  Ledger.WBS2 AS WBS2,
                  Ledger.WBS3 AS WBS3,
                  CA.Account AS Account,
                  Ledger.Vendor AS Vendor,
                  SUM(Ledger.AmountProjectCurrency) AS JTDCost,
                  SUM(Ledger.BillExt) AS JTDBill
                  FROM LedgerAR Ledger WITH (NOLOCK)
                    INNER JOIN CA WITH (NOLOCK) ON Ledger.Account = CA.Account 
                  WHERE Ledger.WBS1 = @strWBS1 
                    AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate
                    AND CA.Type IN (6, 8)
                  GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, CA.Account, Ledger.Vendor
                  HAVING (SUM(Ledger.AmountProjectCurrency) <> 0 OR SUM(Ledger.BillExt) <> 0)
                UNION ALL
                SELECT
                  Ledger.WBS1 AS WBS1,
                  Ledger.WBS2 AS WBS2,
                  Ledger.WBS3 AS WBS3,
                  CA.Account AS Account,
                  Ledger.Vendor AS Vendor,
                  SUM(Ledger.AmountProjectCurrency) AS JTDCost,
                  SUM(Ledger.BillExt) AS JTDBill
                  FROM LedgerMisc Ledger WITH (NOLOCK) 		   
                    INNER JOIN CA WITH (NOLOCK) ON Ledger.Account = CA.Account 
                  WHERE Ledger.WBS1 = @strWBS1 
                    AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate
                    AND CA.Type IN (6, 8)
                  GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, CA.Account, Ledger.Vendor
                  HAVING (SUM(Ledger.AmountProjectCurrency) <> 0 OR SUM(Ledger.BillExt) <> 0)
                UNION ALL
                SELECT -- POC with no Change Orders.
                  POC.WBS1 AS WBS1,
                  POC.WBS2 AS WBS2,
                  POC.WBS3 AS WBS3,
                  POC.Account AS Account,
                  POM.Vendor AS Vendor,
                  SUM(POC.AmountProjectCurrency) AS JTDCost,
                  SUM(POC.BillExt) AS JTDBill
                  FROM POCommitment AS POC WITH (NOLOCK) 
                    INNER JOIN PODetail AS POD WITH (NOLOCK) ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
                    INNER JOIN POMaster AS POM WITH (NOLOCK) ON POD.MasterPKey = POM.MasterPKey
                    INNER JOIN CA WITH (NOLOCK) ON POC.Account = CA.Account
                  WHERE POM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (6, 8)  
                  GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
                  HAVING (SUM(POC.AmountProjectCurrency) <> 0 OR SUM(POC.BillExt) <> 0)
                UNION ALL
                SELECT -- POC with Change Orders.
                  POC.WBS1 AS WBS1,
                  POC.WBS2 AS WBS2,
                  POC.WBS3 AS WBS3,
                  POC.Account AS Account,
                  POM.Vendor AS Vendor,
                  SUM(POC.AmountProjectCurrency) AS JTDCost,
                  SUM(POC.BillExt) AS JTDBill
                  FROM POCommitment AS POC WITH (NOLOCK) 
                    INNER JOIN PODetail AS POD WITH (NOLOCK) ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
                    INNER JOIN POCOMaster AS POCOM WITH (NOLOCK) ON POD.COPKey = POCOM.PKey
                    INNER JOIN POMaster AS POM WITH (NOLOCK) ON POCOM.MasterPKey = POM.MasterPKey
                    INNER JOIN CA WITH (NOLOCK) ON POC.Account = CA.Account
                  WHERE POCOM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (6, 8) 
                  GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
                  HAVING (SUM(POC.AmountProjectCurrency) <> 0 OR SUM(POC.BillExt) <> 0)
              ) AS XE
              GROUP BY XE.WBS1, XE.WBS2, XE.WBS3, XE.Account, XE.Vendor
              HAVING (SUM(XE.JTDCost) <> 0 OR SUM(XE.JTDBill) <> 0)
          ) AS X

      -- Consolidate the JTD rows and line them up with PNTask rows which are less than or equal to @intConWBSLevel.
      -- A PNTask Tree may have lesser levels than the @intConWBSLevel.

      INSERT @tabJTDCon(
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor
      )
        SELECT DISTINCT
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.Account AS Account,
          X.Vendor AS Vendor
          FROM (
            SELECT DISTINCT
              W.WBS1 AS WBS1,
              W.WBS2 AS WBS2,
              W.WBS3 AS WBS3,
              RJE.Account AS Account,
              RJE.Vendor AS Vendor,
              CASE
                WHEN OutlineLevel < @intConWBSLevel
                THEN
                  CASE
                    WHEN (ChildrenCount > 0 AND HasLBCD = 1) THEN 1
                    WHEN (ChildrenCount > 0 AND HasLBCD = 0) THEN 0
                    ELSE 1
                  END
                ELSE 1
              END AS IsLeaf
              FROM  @tabTask AS W
                INNER JOIN @tabRawJTDCon AS RJE 
                  ON RJE.WBS1 = W.WBS1 
                    AND RJE.WBS2 LIKE  CASE WHEN W.WBS2 = ' ' THEN '%' ELSE W.WBS2 + '%' END
                    AND RJE.WBS3 LIKE  CASE WHEN W.WBS3 = ' ' THEN '%' ELSE W.WBS3 + '%' END
                    AND W.OutlineLevel <= @intConWBSLevel
          ) AS X
          WHERE X.IsLeaf = 1

      -- Save {WBS1, WBS2, WBS3, Account, Vendor} from PNExpense table into @tabAssignmentExp.

      INSERT @tabAssignmentCon(
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor
      )
        SELECT DISTINCT
          C.WBS1 AS WBS1,
          ISNULL(C.WBS2, ' ') AS WBS2,
          ISNULL(C.WBS3, ' ') AS WBS3,
          C.Account AS Account,
          C.Vendor AS Vendor
          FROM PNConsultant AS C WITH (NOLOCK)
          WHERE C.PlanID = @strPlanID

      -- Compare @tabJTDCon against @tabAssignmentCon

      SELECT @strHasMissingJTDCon =
        CASE
          WHEN EXISTS(
            SELECT
              WBS1,
              WBS2,
              WBS3,
              Account,
              Vendor
              FROM @tabJTDCon
            EXCEPT
            SELECT
              WBS1,
              WBS2,
              WBS3,
              Account,
              Vendor
              FROM @tabAssignmentCon
          )
          THEN 'Y'
          ELSE 'N'
        END

    END /* END IF */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabResults(
    HasMissingJTDLab,
    HasMissingJTDExp,
    HasMissingJTDCon
  )
    SELECT
      @strHasMissingJTDLab,
      @strHasMissingJTDExp,
      @strHasMissingJTDCon

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
