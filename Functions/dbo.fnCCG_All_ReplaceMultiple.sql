SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   FUNCTION [dbo].[fnCCG_All_ReplaceMultiple] (@text Nvarchar(max), @replaceParams Nvarchar(max), @delimiter varchar(20) = '|', @addQuotes bit)
RETURNS Nvarchar(max)
AS BEGIN
	-- Copyright (c) 2020 EleVia Software. All rights reserved. 
	declare @rtnVal Nvarchar(max) = @text, @tempText Nvarchar(1000)
	declare @index int, @param Nvarchar(100), @paramVal Nvarchar(900)
	set @index = -1

	while (LEN(@replaceParams) > 0)
	begin
		/* Find the first delimiter */
		set @index = CHARINDEX(@delimiter, @replaceParams)

		if @index = 0
			set @tempText = @replaceParams
		else if @index > 1
			set @tempText = LEFT(@replaceParams, @index - 1)

		set @param = left(@tempText, charindex('=', @tempText) - 1)
		set @paramVal = substring(@tempText, charindex('=', @tempText) + 1, 900)
		if @addQuotes = 1 set @paramVal = ''''+@paramVal+''''
		set @rtnVal = replace(@rtnVal, @param, @paramVal)

		if @index = 0 break
		else if @index > 1 set @replaceParams = RIGHT(@replaceParams, (LEN(@replaceParams) - @index + 1 - len(@delimiter)))
	end
	return @rtnVal
END
GO
