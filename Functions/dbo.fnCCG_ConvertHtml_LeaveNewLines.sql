SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnCCG_ConvertHtml_LeaveNewLines](@htmlString varchar(max))
RETURNS varchar(max)
AS BEGIN
-- PRINT dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar('<br>Test<br>Test2</p>', '<br>', char(13)+char(10))
	declare @i int, @Start int, @End int, @Length int
	DECLARE @trimchars VARCHAR(10)
	-- SET @trimchars = CHAR(9)+CHAR(10)+CHAR(13)+CHAR(32)
		
	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '<br>', char(13)+char(10))--char(13)+char(10))
	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '<br/>', char(13)+char(10))--char(13)+char(10))
	SET @htmlString = REPLACE(@htmlString, '</p>', CHAR(13)+CHAR(10))
	
	set @i = patindex('%<%>%', @htmlString)
	while @i > 0
	begin
		set @htmlString = stuff(@htmlString, @i, charindex('>', @htmlString, @i) - @i + 1, '')
		set @i = patindex('%<%>%', @htmlString)
	end
	
	SET @htmlString = REPLACE(@htmlString, CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10), CHAR(13)+CHAR(10))
    SET @htmlString = LEFT(@htmlString,LEN(@htmlString)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',REVERSE(@htmlString))+1)
	SET @htmlString = REPLACE(@htmlString, CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10), CHAR(13)+CHAR(10))
	SET @htmlString = RIGHT(@htmlString,LEN(@htmlString)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',@htmlString)+1)	

	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '&nbsp;', ' ')
	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '&amp;',  '&')
	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '&lt;',   '<')
	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '&gt;',   '>')
	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '&quot;', '"')
	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '&frasl;','/')
	set @htmlString = dbo.fnCCG_ConvertHtmlSpecialCharSpecialChar(@htmlString, '&copy;', '@')

	return @htmlString
END
GO
