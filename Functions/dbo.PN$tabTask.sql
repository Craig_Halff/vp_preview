SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$tabTask]
  (@strWBS1 Nvarchar(30),
   @strJTDDate VARCHAR(8)) -- Date must be in format: 'yyyymmdd' regardless of UI Culture.

  RETURNS @tabTask TABLE (
    TaskID varchar(32) COLLATE database_default, /* Only Plans, that were created from Navigator, have true TaskID */
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(30) COLLATE database_default,
    WBS3 Nvarchar(30) COLLATE database_default,
    Name Nvarchar(40) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    MinDate datetime,
    MaxDate datetime,
    WBSType varchar(4) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    BudgetType varchar(1) COLLATE database_default,
    CostCurrencyCode varchar(3) COLLATE database_default,
    BillingCurrencyCode varchar(3) COLLATE database_default,
    ChildrenCount int,
    OutlineLevel int,
    PlannedLabCost decimal(19,4),
    PlannedLabBill decimal(19,4),
    BaselineLabCost decimal(19,4),
    BaselineLabBill decimal(19,4),
    ETCLabCost decimal(19,4),
    ETCLabBill decimal(19,4),
    JTDLabCost decimal(19,4),
    JTDLabBill decimal(19,4),
    EACLabCost decimal(19,4),
    EACLabBill decimal(19,4),
    FeeLabCost decimal(19,4),
    FeeLabBill decimal(19,4),
    EACFeeLabCost decimal(19,4),
    EACFeeLabBill decimal(19,4),
    TargetMultCost decimal(19,4),
    EACMultCost decimal(19,4),
    EACProfit decimal(19,4),
    EACProfitPct decimal(19,4),
    NewParentOutlineNumber varchar(255) COLLATE database_default,
    NewOutlineNumber varchar(255) COLLATE database_default
  )

BEGIN

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
 
  DECLARE @strCompany Nvarchar(14)

  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)
  DECLARE @strCompEQLabDirExpFlg varchar(1)
  DECLARE @strChargeType varchar(1)
  DECLARE @strOHProcedure varchar(1)
  DECLARE @strOrgOHAllocMethod varchar(1)
  DECLARE @strSysOHAllocMethod varchar(1)
  
  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

  DECLARE @decTargetMultCost decimal(19,4)
  DECLARE @decBudOHRate decimal(19,4)
  DECLARE @decOrgOHRate decimal(19,4)
  DECLARE @decOrgOHProvRate decimal(19,4)
  DECLARE @decSysOHRate decimal(19,4)
  DECLARE @decSysOHProvRate decimal(19,4)
  DECLARE @decOHRate decimal(19,4)

  DECLARE @strVorN char(1)
  
  -- Declare Temp tables.

  DECLARE @tabMappedTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    BudgetType varchar(1) COLLATE database_default,
    CostCurrencyCode varchar(3) COLLATE database_default,
    BillingCurrencyCode varchar(3) COLLATE database_default,
    OverheadPct decimal(19,4),
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(30) COLLATE database_default,
    WBS3 Nvarchar(30) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    WBSType varchar(4) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChildrenCount int,
    OutlineLevel int,
    PlannedLabCost decimal(19,4),
    PlannedLabBill decimal(19,4),
    BaselineLabCost decimal(19,4),
    BaselineLabBill decimal(19,4),
    TargetMultCost decimal(19,4),
    PlanCreateDate datetime
    PRIMARY KEY(PlanID, TaskID)
  )

  DECLARE @tabTPD TABLE (
    RowID int IDENTITY(1,1),
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4)
    PRIMARY KEY(RowID, PlanID, TaskID, OutlineNumber)
  )
        
  DECLARE @tabTaskDates TABLE (
    RowID  int IDENTITY(1,1),
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(30) COLLATE database_default,
    WBS3 Nvarchar(30) COLLATE database_default,
    FirstEndDate datetime,
    LastStartDate datetime
    PRIMARY KEY(RowID, WBS1, WBS2, WBS3)
  )

  DECLARE @tabTaskETC TABLE (
    TaskID varchar(32) COLLATE database_default,
    ETCLabCost decimal(19,4),
    ETCLabBill decimal(19,4)
    PRIMARY KEY(TaskID)
  )

	DECLARE @tabLD TABLE (
    RowID  int IDENTITY(1,1),
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(30) COLLATE database_default,
    WBS3 Nvarchar(30) COLLATE database_default,
    TransDate datetime,
    JTDLabCost decimal(19,4),
    JTDLabBill decimal(19,4)
    PRIMARY KEY(RowID, WBS1, WBS2, WBS3)
  )

	DECLARE @tabOH TABLE (
    RowID  int IDENTITY(1,1),
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(30) COLLATE database_default,
    WBS3 Nvarchar(30) COLLATE database_default,
    MaxDate datetime,
    OHCost decimal(19,4),
    Flg bit
    UNIQUE(RowID, WBS1, WBS2, WBS3, Flg)
  )

	DECLARE @tabPR TABLE (
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(30) COLLATE database_default,
    WBS3 Nvarchar(30) COLLATE database_default,
    Name Nvarchar(40) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    FeeLabCost decimal(19,4),
    FeeLabBill decimal(19,4)
    PRIMARY KEY(WBS1, WBS2, WBS3)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Zero out OH Rates

  SET @decOHRate = 0.0000
  SET @decBudOHRate = 0.0000
  SET @decOrgOHRate = 0.0000
  SET @decSysOHRate = 0.0000
          
  -- Set JTD/ETC Dates.
  
  SET @dtJTDDate = CONVERT(datetime, @strJTDDate)
  SET @dtETCDate = DATEADD(d, 1, @dtJTDDate) 

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat     

  -- Save Company string for use later.
  
  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END,
    @strChargeType = PR.ChargeType,
    @decBudOHRate = PR.BudOHRate,
    @strOrgOHAllocMethod = O.OHAllocMethod,
    @decOrgOHRate = ISNULL(O.OHRate, 0.0000),
    @decOrgOHProvRate = ISNULL(O.OHProvisionalRate, 0.0000)
    FROM PR
      LEFT JOIN Organization AS O ON PR.Org = O.Org AND O.OHBasis IN ('TL', 'DL')
    WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' 
          
  -- Determine which set of tables need to be used.

  SET @strVorN = dbo.PN$PlanIs_VorN(@strWBS1)

  -- Get Default Plan Settings from CFGResourcePlanning

  SELECT
    @strCompEQLabDirExpFlg = CompEQLabDirExpFlg
    FROM CFGResourcePlanning WHERE Company = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN @strCompany ELSE ' ' END

  -- Determine Overhead Rate

  SELECT 
    @strOHProcedure = OHProcedure,
    @strSysOHAllocMethod = OHAllocMethod,
    @decSysOHRate = ISNULL(OHRate, 0.0000),
    @decSysOHProvRate = ISNULL(OHProvisionalRate, 0.0000)
    FROM CFGOHMain
    WHERE OHBasis IN ('TL', 'DL') AND Company = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN @strCompany ELSE ' ' END

  SELECT @decOHRate = 
    CASE @strOHProcedure
      WHEN 'N' THEN 0.0000
      WHEN 'P' THEN
        CASE
          WHEN @strOrgOHAllocMethod = 'P' THEN @decOrgOHProvRate
          ELSE
            CASE
              WHEN @decBudOHRate <> 0.0000 THEN @decBudOHRate 
              ELSE @decOrgOHRate 
            END
        END
      WHEN 'F' THEN
        CASE
          WHEN @strSysOHAllocMethod = 'P' THEN @decSysOHProvRate
          ELSE
            CASE
              WHEN @decBudOHRate <> 0.0000 THEN @decBudOHRate 
              ELSE @decSysOHRate 
            END
        END
    END / 100.0000

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
  -- Save RPTask/PNTask that are mapped to @strWBS1. There could be multiple Plans with UlilizationIncludeFlag = 'Y'
  -- Save TPD to be used later.

  IF (@strVorN = 'V')
    BEGIN

      INSERT @tabMappedTask (
        PlanID,
        TaskID,
        BudgetType,
        CostCurrencyCode,
        BillingCurrencyCode,
        OverheadPct,
        WBS1,
        WBS2,
        WBS3,
        StartDate,
        EndDate,
        WBSType,
        ParentOutlineNumber,
        OutlineNumber,
        ChildrenCount,
        OutlineLevel,
        PlannedLabCost,
        PlannedLabBill,
        BaselineLabCost,
        BaselineLabBill,
        TargetMultCost,
        PlanCreateDate
      )
        SELECT
          P.PlanID,
          T.TaskID,
          P.BudgetType,
          P.CostCurrencyCode,
          P.BillingCurrencyCode,
          P.OverheadPct,
          T.WBS1,
          ISNULL(T.WBS2, ' '),
          ISNULL(T.WBS3, ' '),
          T.StartDate,
          T.EndDate,
          T.WBSType,
          T.ParentOutlineNumber,
          T.OutlineNumber,
          T.ChildrenCount,
          T.OutlineLevel,
          T.PlannedLabCost,
          T.PlannedLabBill,
          T.BaselineLabCost,
          T.BaselineLabBill,
          P.TargetMultCost AS TargetMultCost,
          P.CreateDate AS PlanCreateDate
          FROM RPTask AS T
            INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND T.WBS1 = @strWBS1 AND
              ((T.WBSType = 'WBS1' AND T.WBS1 <> '<none>') OR
               (T.WBSType = 'WBS2' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>') OR
               (T.WBSType = 'WBS3' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>' AND T.WBS3 <> '<none>'))

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabTPD
        (PlanID,
         TaskID,
         AssignmentID,
         OutlineNumber,
         StartDate,
         EndDate,
         PeriodCost,
         PeriodBill
        )
        SELECT
          TL.PlanID,
          TL.TaskID,
          TL.AssignmentID,
          TL.OutlineNumber,
          TL.StartDate,
          TL.EndDate,
          TL.PeriodCost,
          TL.PeriodBill
          FROM
            (SELECT DISTINCT PT.PlanID, PT.TaskID, PT.OutlineNumber, TPD.AssignmentID, TPD.StartDate, TPD.EndDate, TPD.PeriodCost, TPD.PeriodBill
                FROM RPTask AS PT 
                  LEFT JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
                  LEFT JOIN RPAssignment AS A ON PT.PlanID = A.PlanID AND PT.TaskID = A.TaskID
                  LEFT JOIN RPPlannedLabor AS TPD ON PT.PlanID = TPD.PlanID AND PT.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL
                WHERE PT.WBS1 = @strWBS1 AND CT.TaskID IS NULL AND A.AssignmentID IS NULL AND TPD.PeriodHrs <> 0
              UNION ALL
              SELECT DISTINCT PT.PlanID, PT.TaskID, PT.OutlineNumber, TPD.AssignmentID, TPD.StartDate, TPD.EndDate, TPD.PeriodCost, TPD.PeriodBill
                FROM RPTask AS PT 
                  INNER JOIN RPAssignment AS A ON PT.PlanID = A.PlanID AND PT.TaskID = A.TaskID
                  LEFT JOIN RPPlannedLabor AS TPD ON PT.PlanID = TPD.PlanID AND PT.TaskID = TPD.TaskID AND TPD.AssignmentID IS NOT NULL
                WHERE PT.WBS1 = @strWBS1  AND TPD.PeriodHrs <> 0
            ) AS TL
          WHERE TL.PlanID IN (SELECT DISTINCT PlanID FROM @tabMappedTask)

    END /* End If (@strVorN = 'V') */
  ELSE IF (@strVorN = 'N')
    BEGIN

      INSERT @tabMappedTask (
        PlanID,
        TaskID,
        BudgetType,
        CostCurrencyCode,
        BillingCurrencyCode,
        OverheadPct,
        WBS1,
        WBS2,
        WBS3,
        StartDate,
        EndDate,
        WBSType,
        ParentOutlineNumber,
        OutlineNumber,
        ChildrenCount,
        OutlineLevel,
        PlannedLabCost,
        PlannedLabBill,
        BaselineLabCost,
        BaselineLabBill,
        TargetMultCost,
        PlanCreateDate
      )
        SELECT
          P.PlanID,
          T.TaskID,
          P.BudgetType,
          P.CostCurrencyCode,
          P.BillingCurrencyCode,
          P.OverheadPct,
          T.WBS1,
          ISNULL(T.WBS2, ' '),
          ISNULL(T.WBS3, ' '),
          T.StartDate,
          T.EndDate,
          T.WBSType,
          T.ParentOutlineNumber,
          T.OutlineNumber,
          T.ChildrenCount,
          T.OutlineLevel,
          T.PlannedLabCost,
          T.PlannedLabBill,
          T.BaselineLabCost,
          T.BaselineLabBill,
          P.TargetMultCost AS TargetMultCost,
          P.CreateDate AS PlanCreateDate
          FROM PNTask AS T
            INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND T.WBS1 = @strWBS1 AND
              ((T.WBSType = 'WBS1' AND T.WBS1 <> '<none>') OR
               (T.WBSType = 'WBS2' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>') OR
               (T.WBSType = 'WBS3' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>' AND T.WBS3 <> '<none>'))
 
      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabTPD
        (PlanID,
         TaskID,
         AssignmentID,
         OutlineNumber,
         StartDate,
         EndDate,
         PeriodCost,
         PeriodBill
        )
        SELECT
          TL.PlanID,
          TL.TaskID,
          TL.AssignmentID,
          TL.OutlineNumber,
          TL.StartDate,
          TL.EndDate,
          TL.PeriodCost,
          TL.PeriodBill
          FROM
            (SELECT DISTINCT PT.PlanID, PT.TaskID, PT.OutlineNumber, TPD.AssignmentID, TPD.StartDate, TPD.EndDate, TPD.PeriodCost, TPD.PeriodBill
                FROM PNTask AS PT 
                  LEFT JOIN PNTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
                  LEFT JOIN PNAssignment AS A ON PT.PlanID = A.PlanID AND PT.TaskID = A.TaskID
                  LEFT JOIN PNPlannedLabor AS TPD ON PT.PlanID = TPD.PlanID AND PT.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL
                WHERE PT.WBS1 = @strWBS1 AND CT.TaskID IS NULL AND A.AssignmentID IS NULL AND TPD.PeriodHrs <> 0
              UNION ALL
              SELECT DISTINCT PT.PlanID, PT.TaskID, PT.OutlineNumber, TPD.AssignmentID, TPD.StartDate, TPD.EndDate, TPD.PeriodCost, TPD.PeriodBill
                FROM PNTask AS PT 
                  INNER JOIN PNAssignment AS A ON PT.PlanID = A.PlanID AND PT.TaskID = A.TaskID
                  LEFT JOIN PNPlannedLabor AS TPD ON PT.PlanID = TPD.PlanID AND PT.TaskID = TPD.TaskID AND TPD.AssignmentID IS NOT NULL
                WHERE PT.WBS1 = @strWBS1 AND TPD.PeriodHrs <> 0
            ) AS TL
          WHERE TL.PlanID IN (SELECT DISTINCT PlanID FROM @tabMappedTask)

   END /* End If (@strVorN = 'N') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Building a table to hold the OutlineNumber for the WBS rows to be used in the case of not a Vision Plan.

  INSERT @tabPR(
    WBS1,
    WBS2,
    WBS3,
    Name,
    ParentOutlineNumber,
    OutlineNumber,
    FeeLabCost,
    FeeLabBill
  )
    SELECT
      WBS1,
      WBS2,
      WBS3,
      Name,
      NULL AS ParentOutlineNumber,
      '001' AS OutlineNumber,
      FeeDirLab AS FeeLabCost,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN FeeDirLabBillingCurrency ELSE FeeDirLab END) AS FeeLabBill
      FROM PR 
      WHERE WBS1 = @strWBS1 AND WBS2 = ' ' AND WBS3 = ' '

  INSERT @tabPR (
    WBS1,
    WBS2,
    WBS3,
    Name,
    ParentOutlineNumber,
    OutlineNumber,
    FeeLabCost,
    FeeLabBill
  )
    SELECT
      WBS1,
      WBS2,
      WBS3,
      Name,
      '001' AS ParentOutlineNumber,
      '001' + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
      FeeLabCost,
      FeeLabBill
      FROM (
        SELECT
          WBS1,
          WBS2,
          WBS3,
          Name,
          FeeDirLab AS FeeLabCost,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN FeeDirLabBillingCurrency ELSE FeeDirLab END) AS FeeLabBill,
          ROW_NUMBER() OVER (PARTITION BY WBS1 ORDER BY WBS1, WBS2) AS RowID
          FROM PR 
          WHERE WBS1 = @strWBS1 AND WBS2 <> ' ' AND WBS3 = ' '
      ) AS X

  INSERT @tabPR (
    WBS1,
    WBS2,
    WBS3,
    Name,
    ParentOutlineNumber,
    OutlineNumber,
    FeeLabCost,
    FeeLabBill
  )
    SELECT
      X.WBS1,
      X.WBS2,
      X.WBS3,
      X.Name,
      PX.OutlineNumber AS ParentOutlineNumber,
      PX.OutlineNumber + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
      X.FeeLabCost,
      X.FeeLabBill
      FROM (
        SELECT
          WBS1,
          WBS2,
          WBS3,
          Name,
          FeeDirLab AS FeeLabCost,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN FeeDirLabBillingCurrency ELSE FeeDirLab END) AS FeeLabBill,
          ROW_NUMBER() OVER (PARTITION BY WBS2 ORDER BY WBS1, WBS2, WBS3) AS RowID
          FROM PR 
          WHERE WBS1 = @strWBS1 AND WBS2 <> ' ' AND WBS3 <> ' '
      ) AS X
        INNER JOIN @tabPR AS PX ON X.WBS1 = PX.WBS1 AND X.WBS2 = PX.WBS2 AND PX.WBS3 = ' '

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine the Target Multipler by selecting the Plan with earliest CreateDate.

  SELECT @decTargetMultCost = TargetMultCost
    FROM @tabMappedTask WHERE PlanCreateDate = (SELECT MIN(PlanCreateDate) FROM @tabMappedTask)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	-- Save Labor JTD & Unposted Labor JTD for this Project into temp table.
  -- The JTD data are posted at the leaf node in the WBS Tree. 
  -- Therefore, we need to calculate JTD for the summary levels.

	INSERT @tabLD
    (WBS1,
     WBS2,
     WBS3,
     TransDate,
     JTDLabCost,
     JTDLabBill)      
		SELECT
      PR.WBS1,
			PR.WBS2,
			PR.WBS3,
      X.TransDate,
      SUM(ISNULL(X.JTDLabCost, 0.0000)) AS JTDLabCost,
      SUM(ISNULL(X.JTDLabBill, 0.0000)) AS JTDLabBill 
      FROM PR LEFT JOIN
        (SELECT
           LD.WBS1,
           LD.WBS2,
           LD.WBS3,
           LD.TransDate AS TransDate,
           SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS JTDLabCost,
           SUM(LD.BillExt) AS JTDLabBill
           FROM LD 
           WHERE LD.ProjectCost = 'Y' AND LD.TransDate <= @dtJTDDate AND LD.WBS1 = @strWBS1
           GROUP BY LD.WBS1, LD.WBS2, LD.WBS3, LD.TransDate
         UNION ALL SELECT
           TD.WBS1,
           TD.WBS2,
           TD.WBS3,
           TD.TransDate AS TransDate,
           SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS JTDLabCost,
           SUM(TD.BillExt) AS JTDLabBill
           FROM tkDetail AS TD
             INNER JOIN EM ON TD.Employee = EM.Employee  
             INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
           WHERE TD.TransDate <= @dtJTDDate AND TD.WBS1 = @strWBS1
           GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.TransDate
         UNION ALL SELECT
           TD.WBS1,
           TD.WBS2,
           TD.WBS3,
           TD.TransDate AS TransDate,
           SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS JTDLabCost,
           SUM(TD.BillExt) AS JTDLabBill
           FROM tsDetail AS TD
             INNER JOIN EM ON TD.Employee = EM.Employee  
             INNER JOIN tsControl AS TC ON (TC.Posted = 'N' AND TD.Batch = TC.Batch)
           WHERE TD.TransDate <= @dtJTDDate AND TD.WBS1 = @strWBS1
           GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.TransDate
        ) AS X
          ON PR.WBS1 = X.WBS1 AND  
            (PR.WBS2 LIKE CASE WHEN PR.WBS2 = ' ' THEN '%' ELSE X.WBS2 END) AND 
            (PR.WBS3 LIKE CASE WHEN PR.WBS3 = ' ' THEN '%' ELSE X.WBS3 END)
      WHERE PR.WBS1 = @strWBS1
      GROUP BY PR.WBS1, PR.WBS2, PR.WBS3, X.TransDate
 
  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Calculate JTD Overhead Allocation
  
	INSERT @tabOH (
    WBS1,
    WBS2,
    WBS3,
    MaxDate,
    OHCost,
    Flg
  )
		SELECT
      PR.WBS1,
			PR.WBS2,
			PR.WBS3,
      MAX(ISNULL(MaxDate, CONVERT(datetime, 0))) AS MaxDate,
      SUM(ISNULL(OHCost, 0.0000)) AS OHCost,
      0 AS Flg
      FROM PR
        LEFT JOIN (
		      SELECT
            PRF.WBS1,
			      PRF.WBS2,
			      PRF.WBS3,
            MAX(ISNULL(FD.AccountPdEnd, CONVERT(datetime, 0))) AS MaxDate,
            SUM(ISNULL(PRF.RegOHProjectCurrency, 0.0000)) AS OHCost
            FROM PRF
              LEFT JOIN CFGDates AS FD ON PRF.Period = FD.Period
            WHERE PRF.WBS1 = @strWBS1
            GROUP BY PRF.WBS1, PRF.WBS2, PRF.WBS3
        ) AS X 
          ON PR.WBS1 = X.WBS1 AND  
            (PR.WBS2 LIKE CASE WHEN PR.WBS2 = ' ' THEN '%' ELSE X.WBS2 END) AND 
            (PR.WBS3 LIKE CASE WHEN PR.WBS3 = ' ' THEN '%' ELSE X.WBS3 END)
      WHERE PR.WBS1 = @strWBS1
      GROUP BY PR.WBS1, PR.WBS2, PR.WBS3

	INSERT @tabOH (
    WBS1,
    WBS2,
    WBS3,
    MaxDate,
    OHCost,
    Flg
  )      
		SELECT
      OH.WBS1,
			OH.WBS2,
			OH.WBS3,
      MAX(ISNULL(X.TransDate, CONVERT(datetime, 0))) AS MaxDate,
      SUM(ISNULL(X.JTDLabCost, 0.0000)) * @decOHRate  AS OHCost,
      1 AS Flg
      FROM @tabOH AS OH
        LEFT JOIN @tabLD AS X ON OH.WBS1 = X.WBS1 AND OH.WBS2 = X.WBS2 AND OH.WBS3 = X.WBS3 AND OH.Flg = 0
        WHERE X.TransDate > OH.MaxDate
        GROUP BY OH.WBS1, OH.WBS2, OH.WBS3

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save Start and End Dates to be used later.

  INSERT @tabTaskDates (
    WBS1,
    WBS2,
    WBS3,
    FirstEndDate,
    LastStartDate
  )
    SELECT DISTINCT
      MT.WBS1,
      ISNULL(MT.WBS2, ' ') AS WBS2,
      ISNULL(MT.WBS3, ' ') AS WBS3,
      MIN(TPD.EndDate) OVER (PARTITION BY TPD.TaskID) FirstEndDate,
      MAX(TPD.StartDate) OVER (PARTITION BY TPD.TaskID) LastStartDate
      FROM @tabMappedTask AS MT
        LEFT JOIN @tabTPD AS TPD ON MT.PlanID = TPD.PlanID AND MT.TaskID = TPD.TaskID  AND TPD.AssignmentID IS NOT NULL

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Calculate Task ETC.

  INSERT @tabTaskETC
    (TaskID,
     ETCLabCost,
     ETCLabBill
    )
    SELECT TaskID AS TaskID,  
      SUM(CASE WHEN RType = 'L' THEN CASE WHEN X.StartDate >= @dtETCDate THEN PeriodCost
               ELSE PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END ELSE 0 END) AS ETCLabCost, 
      SUM(CASE WHEN RType = 'L' THEN CASE WHEN X.StartDate >= @dtETCDate THEN PeriodBill
               ELSE PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END ELSE 0 END) AS ETCLabBill 
      FROM
        (SELECT T.TaskID AS TaskID, 'L' AS RType,
           TPD.StartDate AS StartDate, TPD.EndDate as EndDate,
           ISNULL(PeriodCost, 0) AS PeriodCost,
           ISNULL(PeriodBill, 0) AS PeriodBill
           FROM @tabMappedTask AS T 
             INNER JOIN @tabTPD AS TPD ON (T.PlanID = TPD.PlanID AND TPD.OutlineNumber LIKE (T.OutlineNumber + '%') AND TPD.EndDate >= @dtETCDate) 
        ) AS X 
      GROUP BY X.TaskID 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Combine data for Task rows.
  -- ContractAmt and JTDLabBill come from Project, therefore should not be counted multiple times.
  -- ETCLabBill comes from all RPTask rows of Plans that were mapped to the Project.
  --   There could be more than one Plan that was mapped to the Project.
  --   Therefore, ETCLabBill need to be grouped by WBS1/WBS2/WBS3 for the collection of all RPTask rows. 

  INSERT @tabTask (
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    Name,
    StartDate,
    EndDate,
    MinDate,
    MaxDate,
    WBSType,
    ParentOutlineNumber,
    OutlineNumber,
    BudgetType,
    CostCurrencyCode,
    BillingCurrencyCode,
    ChildrenCount,
    OutlineLevel,
    PlannedLabCost,
    PlannedLabBill,
    BaselineLabCost,
    BaselineLabBill,
    ETCLabCost,
    ETCLabBill,
    JTDLabCost,
    JTDLabBill,
    EACLabCost,
    EACLabBill,
    FeeLabCost,
    FeeLabBill,
    EACFeeLabCost,
    EACFeeLabBill,
    TargetMultCost,
    EACMultCost,
    EACProfit,
    EACProfitPct,
    NewParentOutlineNumber,
    NewOutlineNumber
  )
    SELECT
      X.TaskID AS TaskID,
      PR.WBS1 AS WBS1,
      PR.WBS2 AS WBS2,
      PR.WBS3 AS WBS3,
      PR.Name AS Name,
      X.StartDate AS StartDate,
      X.EndDate AS EndDate,
      TD.FirstEndDate AS MinDate,
      TD.LastStartDate AS MaxDate,
      X.WBSType AS WBSType,
      X.ParentOutlineNumber AS ParentOutlineNumber,
      X.OutlineNumber AS OutlineNumber,
      X.BudgetType AS BudgetType,
      X.CostCurrencyCode AS CostCurrencyCode,
      X.BillingCurrencyCode AS BillingCurrencyCode,
      ISNULL(X.ChildrenCount, 0) AS ChildrenCount,
      ISNULL(X.OutlineLevel, 0) AS OutlineLevel,

      ISNULL(X.PlannedLabCost, 0.0000) AS PlannedLabCost,
      ISNULL(X.PlannedLabBill, 0.0000) AS PlannedLabBill,
      ISNULL(X.BaselineLabCost, 0.0000) AS BaselineLabCost,
      ISNULL(X.BaselineLabBill, 0.0000) AS BaselineLabBill,
      ISNULL(X.ETCLabCost, 0.0000) AS ETCLabCost,
      ISNULL(X.ETCLabBill, 0.0000) AS ETCLabBill,
      ISNULL(LD.JTDLabCost, 0.0000) AS JTDLabCost,
      ISNULL(LD.JTDLabBill, 0.0000) AS JTDLabBill,
      ISNULL(LD.JTDLabCost, 0.0000) + ISNULL(X.ETCLabCost, 0.0000) AS EACLabCost,
      ISNULL(LD.JTDLabBill, 0.0000) + ISNULL(X.ETCLabBill, 0.0000) AS EACLabBill,
      ISNULL(PR.FeeLabCost, 0.0000) AS FeeLabCost,
      ISNULL(PR.FeeLabBill, 0.0000) AS FeeLabBill,
      ISNULL(PR.FeeLabCost, 0.0000) - (ISNULL(LD.JTDLabCost, 0.0000) + ISNULL(X.ETCLabCost, 0.0000)) AS EACFeeLabCost,
      ISNULL(PR.FeeLabBill, 0.0000) - (ISNULL(LD.JTDLabBill, 0.0000) + ISNULL(X.ETCLabBill, 0.0000)) AS EACFeeLabBill,

      ISNULL(@decTargetMultCost, 0.0000) AS TargetMultCost,

      CASE
        WHEN @strVorN = 'V' AND BudgetType = 'B' THEN 0.0000
        ELSE
          COALESCE(((PR.FeeLabCost) / NULLIF((ISNULL(LD.JTDLabCost, 0.0000) + ISNULL(X.ETCLabCost, 0.0000)), 0)) , 0)
      END  AS EACMultCost,

      ISNULL(PR.FeeLabCost, 0.0000) - 
        (ISNULL(LD.JTDLabCost, 0.0000) + ISNULL(X.ETCLabCost, 0.0000) + 
         ISNULL(OH.JTDOHCost, 0.0000) + ISNULL(X.ETCOHCost, 0.0000)) AS EACProfit,

      COALESCE(
        (
         (
          ISNULL(PR.FeeLabCost, 0.0000) - 
            (ISNULL(LD.JTDLabCost, 0.0000) + ISNULL(X.ETCLabCost, 0.0000) + 
             ISNULL(OH.JTDOHCost, 0.0000) + ISNULL(X.ETCOHCost, 0.0000))
         )
         /
         NULLIF(PR.FeeLabCost, 0.0000)
        )
        , 0
      ) * 100.0000 AS EACProfitPct,

      PR.ParentOutlineNumber AS NewParentOutlineNumber,
      PR.OutlineNumber AS NewOutlineNumber

      FROM @tabPR AS PR LEFT JOIN
        (SELECT
           MIN(T.TaskID) AS TaskID,
           T.WBS1,
           T.WBS2,
           T.WBS3,
           MIN(T.StartDate) AS StartDate,
           MAX(T.EndDate) AS EndDate,
           MIN(T.WBSType) AS WBSType,
           MIN(T.ParentOutlineNumber) AS ParentOutlineNumber,
           MIN(T.OutlineNumber) AS OutlineNumber,
           MIN(T.BudgetType) AS BudgetType,
           MIN(T.CostCurrencyCode) AS CostCurrencyCode,
           MIN(T.BillingCurrencyCode) AS BillingCurrencyCode,
           MIN(T.ChildrenCount) AS ChildrenCount,
           MIN(T.OutlineLevel) AS OutlineLevel,
           SUM(ISNULL(T.PlannedLabCost, 0.0000)) AS PlannedLabCost,
           SUM(ISNULL(T.PlannedLabBill, 0.0000)) AS PlannedLabBill,
           SUM(ISNULL(T.BaselineLabCost, 0.0000)) AS BaselineLabCost,
           SUM(ISNULL(T.BaselineLabBill, 0.0000)) AS BaselineLabBill,
           SUM(ISNULL(ETC.ETCLabCost, 0.0000)) AS ETCLabCost,
           SUM(ISNULL(ETC.ETCLabBill, 0.0000)) AS ETCLabBill,
           SUM(ISNULL(ETC.ETCLabCost, 0.0000)) * (MIN(T.OverheadPct) / 100.0000) AS ETCOHCost
           FROM @tabMappedTask AS T
             LEFT JOIN @tabTaskETC AS ETC ON T.TaskID = ETC.TaskID
           GROUP BY T.WBS1, T.WBS2, T.WBS3
        ) AS X ON PR.WBS1 = X.WBS1 AND PR.WBS2 = X.WBS2 AND PR.WBS3 = X.WBS3
        LEFT JOIN @tabTaskDates AS TD ON PR.WBS1 = TD.WBS1 AND PR.WBS2 = TD.WBS2 AND PR.WBS3 = TD.WBS3
        LEFT JOIN (
          SELECT
            WBS1,
            WBS2,
            WBS3,
            SUM(ISNULL(JTDLabCost, 0.0000)) AS JTDLabCost,
            SUM(ISNULL(JTDLabBill, 0.0000)) AS JTDLabBill
            FROM @tabLD
            GROUP BY WBS1, WBS2, WBS3
        ) AS LD ON PR.WBS1 = LD.WBS1 AND PR.WBS2 = LD.WBS2 AND PR.WBS3 = LD.WBS3
        LEFT JOIN (
          SELECT
            WBS1,
            WBS2,
            WBS3,
            SUM(ISNULL(OHCost, 0.0000)) AS JTDOHCost
          FROM @tabOH
          GROUP BY WBS1, WBS2, WBS3
        ) AS OH ON PR.WBS1 = OH.WBS1 AND PR.WBS2 = OH.WBS2 AND PR.WBS3 = OH.WBS3

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
