SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RF$tabFiscalPeriodValidate](
  @strScopeStartDate varchar(8),
  @strScopeEndDate varchar(8)
)

  RETURNS @tabResults TABLE(
    HasFullRange bit,
    HasOverlap bit,
    HasGap bit
  )

BEGIN

  DECLARE @bitHasFullRange bit = 0
  DECLARE @bitHasOverlap bit = 0
  DECLARE @bitHasGap bit = 0

  DECLARE @dtScopeStartDate datetime = CONVERT(datetime, @strScopeStartDate)
  DECLARE @dtScopeEndDate datetime = CONVERT(datetime, @strScopeEndDate)

  DECLARE @dtMINStartDate datetime
  DECLARE @dtMAXEndDate datetime

  DECLARE @tabPeriod TABLE (
    Period int,
    AccountPDStart datetime,
    AccountPDEnd datetime
    UNIQUE(Period, AccountPDStart, AccountPDEnd)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Select from CFGDates rows that are within the input scope.

  INSERT @tabPeriod(
    Period,
    AccountPDStart,
    AccountPDEnd
  )
    SELECT 
      Period,
      AccountPDStart,
      AccountPDEnd
      FROM CFGDates
      WHERE AccountPDStart <= @dtScopeEndDate AND AccountPDEnd >= @dtScopeStartDate

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT
    @dtMINStartDate = MIN(AccountPDStart),
    @dtMAXEndDate = MAX(AccountPDEnd)
    FROM @tabPeriod

  SELECT @bitHasFullRange = 
    CASE
      WHEN (@dtMINStartDate <= @dtScopeStartDate AND @dtMAXEndDate >= @dtScopeEndDate)
      THEN 1 
      ELSE 0 
    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @bitHasOverlap = 
    CASE 
      WHEN 
        EXISTS(
          SELECT 'X'
            FROM @tabPeriod AS P1
              INNER JOIN @tabPeriod AS P2 ON P1.Period <> P2.Period 
                AND P2.AccountPDEnd >= P1.AccountPDStart AND P2.AccountPDStart <= P1.AccountPDEnd
        ) 
      THEN 1 
      ELSE 0 
    END
							
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @bitHasGap = 
    CASE 
      WHEN 
        EXISTS(
          SELECT 'X' 
            FROM (
              SELECT
                AccountPDEnd,
                LEAD(AccountPDStart, 1) OVER (ORDER BY Period) AS NextStart
                FROM @tabPeriod
            ) AS X
            WHERE DATEDIFF(day, AccountPDEnd, NextStart) > 1
        ) 
      THEN 1 
      ELSE 0 
    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabResults(
    HasFullRange,
    HasOverlap,
    HasGap
  )
    SELECT
      @bitHasFullRange,
      @bitHasOverlap,
      @bitHasGap

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
