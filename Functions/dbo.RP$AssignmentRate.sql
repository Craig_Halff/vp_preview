SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$AssignmentRate]
  (@strResourceID Nvarchar(20) = NULL,
   @siCategory smallint = 0,
   @strGRLBCD Nvarchar(14) = NULL,
   @strLaborCode Nvarchar(14) = NULL,
   @siRtMethod smallint = 0,
   @intRtTabNo int = 0,
   @siGRMethod smallint = 0,
   @intGRTabNo int = 0)
  RETURNS Nvarchar(max)
BEGIN

  /*--------------------------------------------------------------------------------------------------------------------
    This function encapsulates the complex rules that determine the Labor Rate for a given Employee or Generic Resource.
    This function will return a string that concatenates all effective rates for the given Employee/Generic Resources.
  */--------------------------------------------------------------------------------------------------------------------

  DECLARE @strRate Nvarchar(max)
  
  SET @strRate = NULL
  
  IF (@strResourceID IS NOT NULL) --> Employee
    BEGIN
    
      IF (@siRtMethod = 2) -- Labor Rate Table
        BEGIN
        
	        SELECT
		        @strRate = COALESCE(@strRate + ',', '') + 
			        '{eRate:"' + CASE WHEN StartDate = '19000101' AND EndDate = '99990101' THEN 'N' ELSE 'Y' END + '",' +
			        'start:"' + ISNULL(CONVERT(Nvarchar, StartDate, 126), '') + '",' +
			        'end:"' + ISNULL(CONVERT(Nvarchar, EndDate, 126), '') + '",' +
			        'rate:"' + LTRIM(STR(Rate, 19, 4)) + '"}'
		        FROM BTRRTEmpls
		        WHERE Employee = @strResourceID AND TableNo = @intRtTabNo
		        ORDER BY StartDate
                
        END -- IF (@siRtMethod = 2)
        
      ELSE IF (@siRtMethod = 3) -- Labor Category Table
        BEGIN
        
	        SELECT
		        @strRate = COALESCE(@strRate + ',', '') + 
			        '{eRate:"' + CASE WHEN R.StartDate = '19000101' AND R.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '",' +
			        'start:"' + ISNULL(CONVERT(Nvarchar, R.StartDate, 126), '') + '",' +
			        'end:"' + ISNULL(CONVERT(Nvarchar, R.EndDate, 126), '') + '",' +
			        'rate:"' + LTRIM(STR(R.Rate, 19, 4)) + '"}'
		        FROM EM
		          LEFT JOIN BTRCTEmpls AS OC ON EM.Employee = OC.Employee AND OC.TableNo = @intRtTabNo
		          INNER JOIN BTRCTCats AS R ON R.TableNo = @intRtTabNo AND R.Category = ISNULL(OC.Category, EM.BillingCategory)
		        WHERE EM.Employee = @strResourceID
		        ORDER BY StartDate
        
        END -- IF (@siRtMethod = 3)
        
      ELSE IF (@siRtMethod = 4) -- Labor Code Table
        BEGIN
        
	        SELECT
		        @strRate = COALESCE(@strRate + ',', '') + 
			        '{eRate:"' + CASE WHEN RA.StartDate = '19000101' AND RA.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '",' +
			        'start:"' + ISNULL(CONVERT(Nvarchar, RA.StartDate, 126), '') + '",' +
			        'end:"' + ISNULL(CONVERT(Nvarchar, RA.EndDate, 126), '') + '",' +
			        'rate:"' + LTRIM(STR(RA.Rate, 19, 4)) + '"}'
            FROM BTRLTCodes AS RA
              INNER JOIN
                (SELECT TOP 1 LaborCodeMask FROM BTRLTCodes, CFGFormat
                    WHERE TableNo = @intRtTabNo AND
                      (SUBSTRING(@strLaborCode, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
					            SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(@strLaborCode, LC1Start, LC1Length)) AND  
					            (SUBSTRING(@strLaborCode, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
					            SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(@strLaborCode, LC2Start, LC2Length)) AND  
					            (SUBSTRING(@strLaborCode, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
					            SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(@strLaborCode, LC3Start, LC3Length)) AND  
					            (SUBSTRING(@strLaborCode, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
					            SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(@strLaborCode, LC4Start, LC4Length)) AND  
					            (SUBSTRING(@strLaborCode, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
					            SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(@strLaborCode, LC5Start, LC5Length))) AS RB
					    ON RA.LaborCodeMask = RB.LaborCodeMask
            WHERE RA.TableNo = @intRtTabNo
		        ORDER BY RA.StartDate
        
        END -- IF (@siRtMethod = 4)
    
    END -- IF (@strResourceID IS NOT NULL) THEN
    
  ELSE --> Genric Resource
    BEGIN
    
      IF (@siGRMethod = 0) -- Labor Category
        BEGIN
        
	        SELECT
		        @strRate = COALESCE(@strRate + ',', '') + 
			        '{eRate:"' + CASE WHEN StartDate = '19000101' AND EndDate = '99990101' THEN 'N' ELSE 'Y' END + '",' +
			        'start:"' + ISNULL(CONVERT(Nvarchar, StartDate, 126), '') + '",' +
			        'end:"' + ISNULL(CONVERT(Nvarchar, EndDate, 126), '') + '",' +
			        'rate:"' + LTRIM(STR(Rate, 19, 4)) + '"}'
		        FROM BTRCTCats 
		        WHERE TableNo = @intGRTabNo AND Category = @siCategory
		        ORDER BY StartDate
        
        END -- IF (@siGRMethod = 0)
        
      ELSE IF (@siGRMethod = 1) -- Labor Code
        BEGIN
        
	        SELECT
		        @strRate = COALESCE(@strRate + ',', '') + 
			        '{eRate:"' + CASE WHEN RA.StartDate = '19000101' AND RA.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '",' +
			        'start:"' + ISNULL(CONVERT(Nvarchar, RA.StartDate, 126), '') + '",' +
			        'end:"' + ISNULL(CONVERT(Nvarchar, RA.EndDate, 126), '') + '",' +
			        'rate:"' + LTRIM(STR(RA.Rate, 19, 4)) + '"}'
            FROM BTRLTCodes AS RA
              INNER JOIN
                (SELECT TOP 1 LaborCodeMask FROM BTRLTCodes, CFGFormat
                    WHERE TableNo = @intGRTabNo AND
                      (SUBSTRING(@strGRLBCD, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
					            SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(@strGRLBCD, LC1Start, LC1Length)) AND  
					            (SUBSTRING(@strGRLBCD, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
					            SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(@strGRLBCD, LC2Start, LC2Length)) AND  
					            (SUBSTRING(@strGRLBCD, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
					            SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(@strGRLBCD, LC3Start, LC3Length)) AND  
					            (SUBSTRING(@strGRLBCD, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
					            SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(@strGRLBCD, LC4Start, LC4Length)) AND  
					            (SUBSTRING(@strGRLBCD, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
					            SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(@strGRLBCD, LC5Start, LC5Length))) AS RB
					    ON RA.LaborCodeMask = RB.LaborCodeMask
            WHERE RA.TableNo = @intGRTabNo
		        ORDER BY RA.StartDate
        
        END -- IF (@siGRMethod = 1)
    		            
    END -- IF (@strResourceID IS NOT NULL) ELSE
    
  -- If no rate was found, return a rate of zero and indicate no effective date was in effect.

  IF (@strRate IS NULL)
    BEGIN
      SET @strRate = '{eRate:"N",' +
			  'start:"1990-01-01T00:00:00.000",' +
			  'end:"9999-01-01T00:00:00.000",' +
			  'rate:"0.0000"}'
    END -- IF (@strRate IS NULL)

  RETURN(@strRate)

END -- fn_RP$AssignmentRate
GO
