SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[stRP$tabResourceJTDPlans](  
  @strResourceID nvarchar(20) 
)
  RETURNS @tabResourceJTDPlans TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default
  )

BEGIN

 DECLARE @dtJTDDate datetime
 DECLARE @dtETCDate datetime 
  --Retrieve JTD Date
  SELECT 
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE())))
    FROM CFGRMSettings
  
  SELECT @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)
	 
  -- Declare Temp tables.
  DECLARE @tabLD TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    Employee nvarchar(20) COLLATE database_default
    UNIQUE (RowID, WBS1, WBS2, WBS3, LaborCode, Employee)
  )

  INSERT @tabLD(
    WBS1,
    WBS2,
    WBS3,
    LaborCode,
    Employee
  ) 
    SELECT DISTINCT
      Y.WBS1 AS WBS1,
      Y.WBS2 AS WBS2,
      Y.WBS3 AS WBS3,
      Y.LaborCode AS LaborCode,
      Y.Employee AS Employee
      FROM ( /* Y */
        SELECT
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.LaborCode AS LaborCode,
          SUM(X.JTDHours) AS JTDHours,
          X.Employee AS Employee
          FROM ( /* X */
            SELECT  
              LD.WBS1,
              LD.WBS2,
              LD.WBS3,
              LD.LaborCode,
              SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDHours,
              LD.Employee
              FROM LD
              WHERE LD.Employee = @strResourceID AND LD.Employee IS NOT NULL AND
                LD.ProjectCost = 'Y' AND LD.TransDate <= @dtJTDDate
              GROUP BY LD.WBS1, LD.WBS2, LD.WBS3, LD.LaborCode, LD.Employee
              HAVING SUM(RegHrs + OvtHrs + SpecialOvtHrs) <> 0
            UNION
            SELECT  
              TD.WBS1,
              TD.WBS2,
              TD.WBS3,
              TD.LaborCode,
              SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDHours,
              TD.Employee
              FROM tkDetail AS TD -- TD table has entries at only the leaf level.
                INNER JOIN EM ON TD.Employee = EM.Employee  
                INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
              WHERE TD.Employee = @strResourceID AND TD.Employee IS NOT NULL AND TD.TransDate <= @dtJTDDate  
              GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.LaborCode, TD.Employee   
              HAVING SUM(RegHrs + OvtHrs + SpecialOvtHrs) <> 0
            UNION
            SELECT  
              TD.WBS1,
              TD.WBS2,
              TD.WBS3,
              TD.LaborCode,
              SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDHours,
              TD.Employee
              FROM tsDetail AS TD -- TD table has entries at only the leaf level.
              WHERE TD.Employee = @strResourceID AND TD.Employee IS NOT NULL AND TD.TransDate <= @dtJTDDate  
              GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.LaborCode, TD.Employee    
              HAVING SUM(RegHrs + OvtHrs + SpecialOvtHrs) <> 0
          ) AS X
          GROUP BY X.WBS1, X.WBS2, X.WBS3, X.LaborCode, X.Employee
          HAVING SUM(X.JTDHours) <> 0
      ) AS Y

	  

	 --if the resource is not involved any JTD, then return
	 if @@ROWCOUNT = 0  Return

	 --Now send back those JTD Plan ID and Top Task IDs, Those plan either has one WBS level or has missing JTD assginment
	 INSERT @tabResourceJTDPlans(
		 PlanID,
		 TaskID)
	        SELECT DISTINCT
				TopTask.PlanID,
				TopTask.TaskID
		    FROM PNTask JTDTask INNER JOIN @tabLD LD ON LD.WBS1 = JTDTask.WBS1 AND LD.WBS2 = ISNULL(JTDTask.WBS2, ' ')
				 AND LD.WBS3 = ISNULL(JTDTask.WBS3, ' ') AND ISNULL(LD.LaborCode, '%') LIKE ISNULL(JTDTask.LaborCode, '%') 
				 INNER JOIN PNTask TopTask ON TopTask.PlanID = JTDTask.PlanID AND TopTask.OutlineLevel =  0
				 LEFT JOIN PNAssignment Assginment ON Assginment.TaskID = JTDTask.TaskID AND Assginment.ResourceID = @strResourceID 
				 Where  JTDTask.ChildrenCount = 0 AND JTDTask.EndDate >= @dtETCDate AND (TopTask.ChildrenCount =  0  OR Assginment.AssignmentID IS NULL)
 
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
