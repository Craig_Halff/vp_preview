SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[UTCToLocalDate]
  (@dt AS datetime,@curDate as datetime, @curUTCDate as datetime)
  RETURNS datetime
BEGIN
DECLARE 
    @offset integer, 
    @sdt DATETIME, 
    @edt DATETIME,
	@curIsDST smallint,
	@dtIsDST smallint

    DECLARE @i integer 

	
		set @offset = dateDiff(HH,@curUTCDate,@curDate)

		if (MONTH(@dt) in (4,5,6,7,8,9,10) or (MONTH(@dt) = 3 and day(@dt) > 14))
			set @dtIsDST = 1
		else if MONTH(@dt) in (12,1,2)
			set @dtIsDST = 0
		else
			begin

			-- find second Sunday in March

			set @i = 8
				WHILE @i < 14 
				BEGIN
					IF @i < 10
					BEGIN
						SET @sdt = convert(datetime,RTRIM(YEAR(@dt))+'030'+RTRIM(@i))
					END
					ELSE
					BEGIN
						SET @sdt = convert(datetime,RTRIM(YEAR(@dt))+'03'+RTRIM(@i))
					END
				    IF DATEPART(weekday,@sdt)=1  
				    BEGIN 
				        SET @i = 14 
				    END 
				    SET @i = @i + 1 
				END 
				 
				-- find first Sunday in November
				 
				SET @i = 1 
				WHILE @i < 7
				BEGIN 
				    SET @edt = convert(datetime,RTRIM(YEAR(@dt))+'110'+RTRIM(@i))
				    IF DATEPART(weekday,@edt)=1  
				    BEGIN 
				        SET @i = 7
				    END 
				    SET @i = @i + 1 
				END 
				 
				-- add hour from offset if within DST or subtract 1 if in sdt
				 
				IF (@dt>=@sdt AND @dt<@edt) 
   	    		 set @dtIsDST = 1
				else
					  set @dtIsDST = 0
			end --dt date is dst

		if (MONTH(@curDate) in (4,5,6,7,8,9,10) or (MONTH(@curDate) = 3 and day(@curDate) > 14))
			set @curIsDST = 1
		else if MONTH(@curDate) in (12,1,2)
			set @curIsDST = 0
		else
			begin

			-- find second Sunday in March

			set @i = 8
				WHILE @i < 14 
				BEGIN
					IF @i < 10
					BEGIN
						SET @sdt = convert(datetime,RTRIM(YEAR(@curDate))+'030'+RTRIM(@i))
					END
					ELSE
					BEGIN
						SET @sdt = convert(datetime,RTRIM(YEAR(@curDate))+'03'+RTRIM(@i))
					END
				    IF DATEPART(weekday,@sdt)=1  
				    BEGIN 
				        SET @i = 14 
				    END 
				    SET @i = @i + 1 
				END 
				 
				-- find first Sunday in November
				 
				SET @i = 1 
				WHILE @i < 7
				BEGIN 
				    SET @edt = convert(datetime,RTRIM(YEAR(@curDate))+'110'+RTRIM(@i))
				    IF DATEPART(weekday,@edt)=1  
				    BEGIN 
				        SET @i = 7 
				    END 
				    SET @i = @i + 1 
				END 
				 
				-- add hour from offset if within DST or subtract 1 if in sdt
				 
				IF (@curDate>=@sdt AND @curDate<@edt) 
				    set @curIsDST = 1
				else
					 set @curIsDST = 0
			end --cur date is dst
		if (@curIsDST = @dtISdst)
			RETURN DATEadd(hh, @offset, @dt)
		else if (@curIsDST = 1)
			SET @OFFSET = @OFFSET -1
		else 
			SET @OFFSET = @OFFSET +1

		RETURN DATEadd(hh, @offset, @dt)
		    

END -- fn_UTCToLocalDate
GO
