SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_FeeToDate] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS @T TABLE 
(
       FeeToDate           money,
       PriorFeeBilled      money
)
AS BEGIN
/*
       Copyright (c) 2017 EleVia Software. All rights reserved.

*/
       declare @UserInputExists char(1), @res1 money, @res2 money, @res3 money, @res4 money, @res5 money, @PriorFeeBilled money, @PriorFeeBilledSet char(1)='Y'
       declare @res decimal(19,4), @FeeMeth int, @FeeBasis varchar(1), @BTLevel int declare @count int, @bt varchar(32)
       set @count=1

       insert into @T values (0.0, 0.0)        -- Easier below...

       -- Get the BT level first:
       select @BTLevel=IsNull(Case When SubLevelTerms='N' Then 1 Else 2 End,0) 
       from BT where BT.WBS1=@WBS1 and BT.WBS2=' '
       if @BTLevel=2
             select @BTLevel=IsNull(Case When SubLevelTerms='N' Then 2 Else 3 End,0) 
             from BT where BT.WBS1=@WBS1 and BT.WBS2=@WBS2 and @WBS2<>' ' and BT.WBS3=' '
       if @BTLevel=3
             select @BTLevel=IsNull(Case When SubLevelTerms='N' Then 3 Else 0 End,0) 
             from BT where BT.WBS1=@WBS1 and BT.WBS2=@WBS2 and BT.WBS3=@WBS3 and @WBS3<>' '
       --print '@BTLevel=' + Cast(@BTLevel as varchar(1))
       if IsNull(@BTLevel,0)=0          -- No billing terms
             return
       
       -- Now get the FeeMeth (0=No fee, 1=Overall % Complete, 2/3=Billing Phases, 4/5=other):
       if @BTLevel=1
             select @FeeMeth=IsNull(FeeMeth,0), @FeeBasis=IsNull(FeeBasis,'') from BT
             where WBS1=@WBS1 and WBS2=' ' and SubLevelTerms='N' and (FeeMeth in (1,4,5) or (FeeMeth in (2,3) and PostFeesByPhase='Y'))
       else if @BTLevel=2
	   begin
			-- ORIGINAL STUFF
             select @FeeMeth=IsNull(FeeMeth,0), @FeeBasis=IsNull(FeeBasis,'') from BT 
             where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=' ' and SubLevelTerms='N' 
			   and (FeeMeth in (1,4,5) or (FeeMeth in (2,3) and PostFeesByPhase='Y'))
               and not exists (select 'x' from BT where WBS1=@WBS1 and WBS2=' ' and SubLevelTerms='N')
			-- ADDED 2017-01-20 to fix posting to a phase that has no fee billing (from another phase)
			-- MODIFIED 2017-04-26 adding IsNull to the "if" and left joining BT in case there are NO billing terms on the receiving phase
			if IsNull(@FeeMeth,0) = 0
				 --select @FeeMeth=IsNull(FeeMeth,0), @FeeBasis=IsNull(FeeBasis,'') from BT inner join BTF on BTF.WBS1=BT.WBS1 and BTF.PostWBS2=BT.WBS2 
				 select @FeeMeth=IsNull(FeeMeth,0), @FeeBasis=IsNull(FeeBasis,'') from BTF left join BT on BTF.WBS1=BT.WBS1 and BT.WBS2=BTF.WBS2
				 where BT.WBS1=@WBS1 and PostWBS2=@WBS2 and BT.WBS3=' ' and SubLevelTerms='N' 
				   and FeeMeth in (2,3) and PostFeesByPhase='Y'
				   and not exists (select 'x' from BT where WBS1=@WBS1 and WBS2=' ' and SubLevelTerms='N')
       end
       else if @BTLevel=3
             select @FeeMeth=IsNull(FeeMeth,0), @FeeBasis=IsNull(FeeBasis,'') from BT 
             where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3 and (FeeMeth in (1,4,5) or (FeeMeth in (2,3) and PostFeesByPhase='Y'))
               and not exists (select 'x' from BT where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=' ' and SubLevelTerms='N')
       if @FeeMeth  is null set @FeeMeth=0
       if @FeeBasis is null set @FeeBasis=''
       -- print '@FeeMeth=' + Cast(@FeeMeth as varchar(1))
       -- print '@FeeBasis=' + @FeeBasis

       -- Step 1.  If there are no entries in CCG_EI_FeePctCpl at or below the passed-in level, take the value from the Draft invoice
       set @UserInputExists='N'
       if @WBS3<>' '
       begin
             if exists (select 'x' from CCG_EI_FeePctCpl where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3 and FeePctCpl is not null)
                    set @UserInputExists='Y'
             end else if @WBS2<>' ' and @FeeMeth <> 3
       begin
             if exists (select 'x' from CCG_EI_FeePctCpl where WBS1=@WBS1 and WBS2=@WBS2 and FeePctCpl is not null)
                    set @UserInputExists='Y'
       end else if @WBS2<>' ' --FIX HERE
       begin
             if exists (select 'x' from CCG_EI_FeePctCpl fpc inner join BTF on fpc.WBS1=BTF.WBS1 and fpc.WBS2=BTF.WBS2 and fpc.WBS3=BTF.WBS3 and fpc.Seq=BTF.Seq where BTF.WBS1=@WBS1 and BTF.POSTWBS2=@WBS2 and fpc.FeePctCpl is not null)
                    set @UserInputExists='Y'
       end else if @WBS2=' ' 
       begin
             if exists (select 'x' from CCG_EI_FeePctCpl where WBS1=@WBS1 and FeePctCpl is not null)
                    set @UserInputExists='Y'
       end
       if @UserInputExists='N'          -- Get the fee from the Draft invoice
       begin
             select @res=Sum(Case When Section = 'F' Then FinalAmount Else 0 End)
             from billInvSums
             where BillWBS1=@WBS1 and (BillWBS2=@WBS2 or @WBS2=' ') and (BillWBS3=@WBS3 or @WBS3=' ')
               and Invoice = '<Draft>'  and ArrayType = 'P' -- 'P' = posting locations, not invoice facing locations

             update @T set FeeToDate=IsNull(@res,0)
             return
       end 

       -- If BT are at level2, but we're at level1, get the sum of all the WBS2s:
       if @BTLevel=2 and @WBS2=' '
       begin
             set @res1=0.00
             set @PriorFeeBilled=0.00
             declare myCursor cursor fast_forward for
                    select distinct WBS2 from PR where WBS1=@WBS1 and WBS2<>' '
             open myCursor
             fetch next from myCursor into @WBS2
             while @@FETCH_STATUS = 0
             begin 
                    select @res1 = @res1 + FeeToDate, @PriorFeeBilled = @PriorFeeBilled + PriorFeeBilled
                           from dbo.fnCCG_EI_FeeToDate(@WBS1, @WBS2, ' ')
                    fetch next from myCursor into @WBS2
             end
             close myCursor
             deallocate myCursor
             update @T set FeeToDate=@res1, PriorFeeBilled=@PriorFeeBilled
             return
       end else if @BTLevel=3 and @WBS2<>' ' and @WBS3=' '
       begin
             -- If BT are at level3, but we're at level2, get the sum of all the WBS3s:
             set @res1=0.00
             set @PriorFeeBilled=0.00
             declare myCursor cursor fast_forward for
                    select distinct WBS3 from PR where WBS1=@WBS1 and WBS2=@WBS2 and WBS3<>' '
             open myCursor
             fetch next from myCursor into @WBS3
             while @@FETCH_STATUS = 0
             begin 
                    select @res1 = @res1 + FeeToDate, @PriorFeeBilled = @PriorFeeBilled + PriorFeeBilled
                           from dbo.fnCCG_EI_FeeToDate(@WBS1, @WBS2, @WBS3)
                    fetch next from myCursor into @WBS3
             end
             close myCursor
             deallocate myCursor
             update @T set FeeToDate=@res1, PriorFeeBilled=@PriorFeeBilled
             return
       end

       if @FeeMeth=0 and @BTLevel>1 and @WBS2<>' ' -- No fee at WBS2/3
             return
             
       if @FeeMeth = 1 and @FeeBasis<>'P' -- Overall % Complete, not percent of construction
             select @res1 = Sum(FeeFactor1 * IsNull(fpc.FeePctCpl,BT.FeePctCpl) / 100.0)
             from BT left join CCG_EI_FeePctCpl fpc on fpc.WBS1=BT.WBS1 and fpc.WBS2=BT.WBS2 and fpc.WBS3=BT.WBS3
             where BT.WBS1=@WBS1 and (@WBS2=' ' or BT.WBS2=@WBS2) and (@WBS3=' ' or Bt.WBS3=@WBS3)
               and FeeMeth=1 and SubLevelTerms='N' and IsNull(FeeBasis,'')<>'P'
       if @FeeMeth = 1 and @FeeBasis='P' -- Overall % Complete as percent of construction
             select @res1 = Sum(FeeFactor1 * FeeFactor2 / 100.0 * IsNull(fpc.FeePctCpl,BT.FeePctCpl) / 100.0)
             from BT left join CCG_EI_FeePctCpl fpc on fpc.WBS1=BT.WBS1 and fpc.WBS2=BT.WBS2 and fpc.WBS3=BT.WBS3
             where BT.WBS1=@WBS1 and (@WBS2=' ' or BT.WBS2=@WBS2) and (@WBS3=' ' or Bt.WBS3=@WBS3)
               and FeeMeth=1 and SubLevelTerms='N' and IsNull(FeeBasis,'')='P'
       if @FeeMeth = 2 and @FeeBasis<>'P' -- % Complete by Phase as % of Fee
	   begin
             select @res2 = Sum((IsNull(fpc.FeePctCpl,PctComplete) / 100.0) * (PctOfFee / 100.0) * FeeFactor1), @PriorFeeBilled=Sum(BilledJTD)
             from BT inner join BTF on BTF.WBS1=BT.WBS1 and BTF.WBS2=BT.WBS2 and BTF.WBS3=BT.WBS3
             left join CCG_EI_FeePctCpl fpc on fpc.WBS1=BTF.WBS1 and fpc.WBS2=BTF.WBS2 and fpc.WBS3=BTF.WBS3 and fpc.Seq=BTF.Seq
             where (PostWBS1=@WBS1 or (PostWBS1 is null and BTF.WBS1=@WBS1)) and (@WBS2=' ' or BTF.PostWBS2=@WBS2) and (@WBS3=' ' or BTF.PostWBS3=@WBS3)
               and FeeMeth=2 and SubLevelTerms='N' and IsNull(FeeBasis,'')<>'P'
			 set @PriorFeeBilledSet='Y'
       end
	   if @FeeMeth = 2 and @FeeBasis='P' -- % Complete by Phase as % of Fee; Fee is % of construction
       begin
	         select @res3 = Sum((IsNull(fpc.FeePctCpl,PctComplete) / 100.0) * (PctOfFee / 100.0) * FeeFactor1 * FeeFactor2 / 100.0), @PriorFeeBilled=Sum(BilledJTD)
             from BT inner join BTF on BTF.WBS1=BT.WBS1 and BTF.WBS2=BT.WBS2 and BTF.WBS3=BT.WBS3
             left join CCG_EI_FeePctCpl fpc on fpc.WBS1=BTF.WBS1 and fpc.WBS2=BTF.WBS2 and fpc.WBS3=BTF.WBS3 and fpc.Seq=BTF.Seq
             where (PostWBS1=@WBS1 or (PostWBS1 is null and BTF.WBS1=@WBS1)) and (@WBS2=' ' or BTF.PostWBS2=@WBS2) and (@WBS3=' ' or BTF.PostWBS3=@WBS3)
               and FeeMeth=2 and SubLevelTerms='N' and IsNull(FeeBasis,'')='P'
			 set @PriorFeeBilledSet='Y'
       end
	   if @FeeMeth = 3 -- % Complete by Phase as Fixed Fee   
	   begin 
             select @res4 = Sum((IsNull(fpc.FeePctCpl,PctComplete) / 100.0) * Fee), @PriorFeeBilled=Sum(BilledJTD)
             from BT inner join BTF on BTF.WBS1=BT.WBS1 and BTF.WBS2=BT.WBS2 and BTF.WBS3=BT.WBS3
             left join CCG_EI_FeePctCpl fpc on fpc.WBS1=BTF.WBS1 and fpc.WBS2=BTF.WBS2 and fpc.WBS3=BTF.WBS3 and fpc.Seq=BTF.Seq
             where (PostWBS1=@WBS1 or (PostWBS1 is null and BTF.WBS1=@WBS1)) and (@WBS2=' ' or BTF.PostWBS2=@WBS2) and (@WBS3=' ' or BTF.PostWBS3=@WBS3)
               and FeeMeth=3 and SubLevelTerms='N'
			 set @PriorFeeBilledSet='Y'
       end
	   if @FeeMeth > 3 -- Other fee (Cumulative or Current Unit or Fee)
             select @res5 = Sum(Case BT.FeeBasis When 'L' Then FeeFactor1 When 'P' Then FeeFactor1*IsNull(fpc.FeePctCpl,FeeFactor2) / 100.0 Else 0.0 End)
             from BT left join CCG_EI_FeePctCpl fpc on fpc.WBS1=BT.WBS1 and fpc.WBS2=BT.WBS2 and fpc.WBS3=BT.WBS3
             where BT.WBS1=@WBS1 and (@WBS2=' ' or BT.WBS2=@WBS2) and (@WBS3=' ' or BT.WBS3=@WBS3)
               and FeeMeth>3 and SubLevelTerms='N'

       update @T set FeeToDate = IsNull(@res1,0) + IsNull(@res2,0) + IsNull(@res3,0) + IsNull(@res4,0) + IsNull(@res5,0)
       if @FeeMeth in (1,2,3)
	   begin
			if @PriorFeeBilledSet='Y'
	             update @T set PriorFeeBilled = @PriorFeeBilled
			else
		         update @T set PriorFeeBilled = (select PriorFee from dbo.fnCCG_EI_PriorFeeBilled(@WBS1, @WBS2, @WBS3, @BTLevel, @FeeMeth))
	   end
       return 
END
GO
