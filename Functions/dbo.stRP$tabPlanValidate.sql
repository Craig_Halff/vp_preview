SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabPlanValidate](
  @strPlanID varchar(32)
)

  RETURNS @tabResults TABLE(
    MappedToMultipleProjs bit,
    HasMultipleScales bit,
    HasIllegalLevel bit,
    HasResourceAtNonLeaf bit,
    HasIllegalCalendarScale bit,
    HasDuplicateResources bit,
    HasOverflowedTPD bit,
    HasExpenseAtMultLevels bit,
    HasConsultantAtMultLevels bit,
    HasPlannedUnits bit,
    HasIllegalCurrency bit,
    HasUnmappedWBSRows bit,
    HasNegativeTPDAmount bit,
    HasMultipleTSKMappedtoPR bit, /* Multiple RPTask rows mapped to one WBS row in PR */
	HasMissingResourceAssignment bit /*Has Assignment Row but ResourceID and GenericResourceID are missing*/
  )

BEGIN

  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)

  DECLARE @bitMappedToMultipleProjs bit
  DECLARE @bitHasMultipleScales bit
  DECLARE @bitHasIllegalLevel bit
  DECLARE @bitHasResourceAtNonLeaf bit
  DECLARE @bitHasIllegalCalendarScale bit
  DECLARE @bitHasDuplicateResources bit
  DECLARE @bitHasOverflowedTPD bit
  DECLARE @bitHasPlannedExpenses bit
  DECLARE @bitHasExpenseAtMultLevels bit
  DECLARE @bitHasConsultantAtMultLevels bit
  DECLARE @bitHasPlannedUnits bit
  DECLARE @bitHasIllegalCurrency bit
  DECLARE @bitHasUnmappedWBSRows bit
  DECLARE @bitHasNegativeTPDAmount bit
  DECLARE @bitHasMultipleTSKMappedtoPR bit
  DECLARE @bitHasMissingResourceAssignment bit

  DECLARE @dtCIMaxEndDate datetime

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @bitMappedToMultipleProjs = 0
  SET @bitHasMultipleScales = 0
  SET @bitHasIllegalLevel = 0
  SET @bitHasResourceAtNonLeaf = 0
  SET @bitHasIllegalCalendarScale = 0
  SET @bitHasDuplicateResources = 0
  SET @bitHasOverflowedTPD = 0
  SET @bitHasPlannedExpenses = 0
  SET @bitHasExpenseAtMultLevels = 0
  SET @bitHasConsultantAtMultLevels = 0
  SET @bitHasPlannedUnits = 0
  SET @bitHasIllegalCurrency = 0
  SET @bitHasUnmappedWBSRows = 0
  SET @bitHasNegativeTPDAmount = 0
  SET @bitHasMultipleTSKMappedtoPR = 0
  SET @bitHasMissingResourceAssignment = 0

  -- Get the RABIBC flag

  SELECT 
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled
    FROM FW_CFGSystem

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine whether there are multiple RPTask rows with WBSType = 'WBS1' in this Plan.

  SELECT @bitMappedToMultipleProjs = CASE WHEN WBS1Count > 1 THEN 1 ELSE 0 END
    FROM (
      SELECT COUNT(XT.TaskID) AS WBS1Count
        FROM RPTask AS XT
        WHERE XT.WBSType = 'WBS1'
          AND XT.PlanID = @strPlanID
    ) AS X

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine whether the Plan has more than one Accordion Scales.

  SELECT @bitHasMultipleScales = CASE WHEN ScaleCount > 1 THEN 1 ELSE 0 END
    FROM
      (SELECT COUNT(AccordionFormatID) AS ScaleCount FROM RPAccordionFormat WHERE PlanID = @strPlanID) AS X

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine whether the Plan has illegal Mapping Levels.
  -- Legal Mapping Levels are 'WBS1', 'WBS2', and 'WBS3'.
  -- Illegal Mapping Level when WBSType is NULL.

  SELECT @bitHasIllegalLevel = CASE WHEN EXISTS(SELECT 'X' FROM RPTask WHERE PlanID = @strPlanID AND (WBSType IS NULL)) THEN 1 ELSE 0 END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine wheter the Assignments are at the Leaf rows of the Project.

  IF (@bitHasIllegalLevel = 0)
    BEGIN

      SELECT @bitHasResourceAtNonLeaf = 
        CASE WHEN 
          EXISTS(SELECT 'X' FROM RPAssignment AS A
                   INNER JOIN PR ON A.WBS1 = PR.WBS1 AND ISNULL(A.WBS2, ' ') = PR.WBS2 AND ISNULL(A.WBS3, ' ') = PR.WBS3
                   WHERE A.PlanID = @strPlanID AND PR.SubLevel = 'Y'
                ) 
             THEN 1 ELSE 0 END

    END -- END IF (@bitMappedToMultipleProjs = 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine whether Calendar Scale is illegal.

  SELECT @bitHasIllegalCalendarScale = CASE WHEN EXISTS(SELECT 'X' FROM RPAccordionFormat WHERE PlanID = @strPlanID AND MinorScale IN ('d', 'a', 'y')) THEN 1 ELSE 0 END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine whether there are multiple Resources under a Task in this Plan.

  SELECT @bitHasDuplicateResources = CASE WHEN AssignmentCount > 1 THEN 1 ELSE 0 END
    FROM
      (SELECT COUNT(AssignmentID) AssignmentCount
         FROM RPAssignment AS A
         WHERE PlanID = @strPlanID
         GROUP BY PlanID, TaskID, ResourceID, GenericResourceID
         HAVING COUNT(AssignmentID) > 1) AS X
      
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine whether there are time-phased data in the Overflow bucket.

  SELECT @dtCIMaxEndDate = MAX(EndDate) 
    FROM RPCalendarInterval WHERE PlanID = @strPlanID

  SELECT @bitHasOverflowedTPD = CASE WHEN EXISTS(SELECT MAX(StartDate) FROM RPPlannedLabor AS TPD WHERE PlanID = @strPlanID HAVING MAX(StartDate) > @dtCIMaxEndDate) THEN 1 ELSE 0 END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine whether there are Expense rows at multiple levels.

  SELECT @bitHasExpenseAtMultLevels = 
    CASE 
      WHEN EXISTS
        (
         SELECT DISTINCT PT.OutlineNumber AS POutlineNumber, CT.OutlineNumber AS COutlineNumber
           FROM RPExpense AS PC
             INNER JOIN RPTask AS PT ON PC.PlanID = PT.PlanID AND PC.TaskID = PT.TaskID
             INNER JOIN RPTask AS CT ON PC.PlanID = CT.PlanID AND CT.OutlineNumber LIKE (PT.OutlineNumber + '%') AND CT.OutlineNumber != PT.OutlineNumber
             INNER JOIN RPExpense AS EX ON PC.PlanID = EX.PlanID AND CT.TaskID = EX.TaskID
           WHERE PC.PlanID = @strPlanID
        ) 
      THEN 1 
      ELSE 0
    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine whether there are Consultant rows at multiple levels.

  SELECT @bitHasConsultantAtMultLevels = 
    CASE 
      WHEN EXISTS
        (
         SELECT DISTINCT PT.OutlineNumber AS POutlineNumber, CT.OutlineNumber AS COutlineNumber
           FROM RPConsultant AS PC
             INNER JOIN RPTask AS PT ON PC.PlanID = PT.PlanID AND PC.TaskID = PT.TaskID
             INNER JOIN RPTask AS CT ON PC.PlanID = CT.PlanID AND CT.OutlineNumber LIKE (PT.OutlineNumber + '%') AND CT.OutlineNumber != PT.OutlineNumber
             INNER JOIN RPConsultant AS CC ON PC.PlanID = CC.PlanID AND CT.TaskID = CC.TaskID
           WHERE PC.PlanID = @strPlanID
        ) 
      THEN 1 
      ELSE 0
    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
  -- Determine whether there are planned rows for units.

  SELECT @bitHasPlannedUnits = 
      CASE	
        WHEN exists (SELECT 'x' FROM RPUnit WHERE planid= @strPlanID) THEN 1 
        WHEN exists (SELECT 'x' FROM RPPlannedUnit WHERE planid= @strPlanID) THEN 1
        ELSE 0
      END
              
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine whether Currency Codes are legal.

  SELECT @bitHasIllegalCurrency = 
    CASE WHEN 
      EXISTS(SELECT 'X' 
               FROM RPPlan AS P
                 INNER JOIN PR ON P.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
               WHERE P.PlanID = @strPlanID AND
                 ((P.CostCurrencyCode <> PR.ProjectCurrencyCode) OR 
                  (P.BillingCurrencyCode <> CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.BillingCurrencyCode ELSE PR.ProjectCurrencyCode END)
                 )
            ) 
         THEN 1 ELSE 0 END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
  -- Determine whether there are planned rows for units.

  SELECT @bitHasUnmappedWBSRows = 
      CASE	
        WHEN EXISTS (
          SELECT 'x' 
            FROM RPTask 
            WHERE PlanID = @strPlanID AND
              ((WBSType = 'WBS1' AND (WBS1 = '<none>' OR WBS1 = NULL)) OR
               (WBSType = 'WBS2' AND (WBS2 = '<none>' OR WBS2 = NULL)) OR
               (WBSType = 'WBS3' AND (WBS3 = '<none>' OR WBS3 = NULL)) OR
               (WBSType = 'LBCD' AND (LaborCode = '<none>' OR LaborCode = NULL))
              )
        ) 
        THEN 1 
        ELSE 0
      END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
  -- Determine if the plan has negative amount on labor and baseline amounts.

  SELECT @bitHasNegativeTPDAmount = 
      CASE	
        WHEN EXISTS (
      SELECT PlanID FROM RPPlannedLabor WHERE (PeriodHrs < 0 or PeriodBill < 0 OR PeriodCost < 0) AND PlanID = @strPlanID
      UNION ALL
      SELECT PlanID FROM RPPlannedExpenses WHERE (PeriodCost < 0 OR PeriodBill < 0) AND PlanID = @strPlanID
      UNION ALL
      SELECT PlanID FROM RPPlannedConsultant WHERE (PeriodCost < 0 OR PeriodBill < 0) AND PlanID = @strPlanID
      UNION ALL
      SELECT PlanID FROM RPPlannedUnit WHERE PeriodQty < 0 AND PlanID = @strPlanID
      UNION ALL
      SELECT PlanID FROM RPBaselineLabor WHERE PeriodHrs < 0 AND PlanID = @strPlanID
      UNION ALL
      SELECT PlanID FROM RPBaselineExpenses WHERE (PeriodCost < 0 OR PeriodBill < 0) AND PlanID = @strPlanID
      UNION ALL
      SELECT PlanID FROM RPBaselineConsultant WHERE (PeriodCost < 0 OR PeriodBill < 0) AND PlanID = @strPlanID
      UNION ALL
      SELECT PlanID FROM  RPBaselineUnit WHERE PeriodQty < 0 AND PlanID = @strPlanID
      )
        THEN 1 
        ELSE 0
      END    

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine whether there are multiple RPTask rows mapped to one WBS row in PR.

  SELECT @bitHasMultipleTSKMappedtoPR = 
      CASE	
        WHEN EXISTS(
          SELECT 'X'
            FROM RPPlan AS RP
              LEFT JOIN PNPlan AS PN ON RP.PlanID = PN.PlanID
            WHERE RP.PlanID = @strPlanID
              AND PN.PlanID IS NULL /* Is a Vision Plan and not an iAccess Plan */
              AND EXISTS(
                SELECT 'X'
                  FROM ( /* Z */
                    SELECT
                      COUNT(T.TaskID) AS CountTask
                    FROM RPTask AS T
                      INNER JOIN PR on T.WBS1 = PR.WBS1 and ISNULL(T.WBS2, ' ') = PR.WBS2 and ISNULL(T.WBS3, ' ') = PR.WBS3
                    WHERE T.WBSType IN ('WBS1', 'WBS2', 'WBS3')
                      AND T.PlanID = RP.PlanID
                    GROUP BY T.WBS1, T.WBS2, T.WBS3, T.WBSType
                    HAVING COUNT(T.TaskID) > 1 
                  ) AS Z
              )
        ) 
        THEN 1 
        ELSE 0
      END

 -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine whether there are Assignment row but resourceID and GenericResourceID are missing.
  SELECT @bitHasMissingResourceAssignment = 
   CASE	
        WHEN EXISTS(
          SELECT 'X'
            FROM RPAssignment A               
            WHERE A.PlanID = @strPlanID 
			AND (A.ResourceID IS NULL AND A.GenericResourceID IS NULL)              
        ) 
        THEN 1 
        ELSE 0
      END
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabResults(
    MappedToMultipleProjs,
    HasMultipleScales,
    HasIllegalLevel,
    HasResourceAtNonLeaf,
    HasIllegalCalendarScale,
    HasDuplicateResources,
    HasOverflowedTPD,
    HasExpenseAtMultLevels,
    HasConsultantAtMultLevels,
    HasPlannedUnits,
    HasIllegalCurrency,
    HasUnmappedWBSRows,
    HasNegativeTPDAmount,
    HasMultipleTSKMappedtoPR,
	HasMissingResourceAssignment
  )
    SELECT
      @bitMappedToMultipleProjs,
      @bitHasMultipleScales,
      @bitHasIllegalLevel,
      @bitHasResourceAtNonLeaf,
      @bitHasIllegalCalendarScale,
      @bitHasDuplicateResources,
      @bitHasOverflowedTPD,
      @bitHasExpenseAtMultLevels,
      @bitHasConsultantAtMultLevels,
      @bitHasPlannedUnits,
      @bitHasIllegalCurrency,
      @bitHasUnmappedWBSRows,
      @bitHasNegativeTPDAmount,
      @bitHasMultipleTSKMappedtoPR,
	  @bitHasMissingResourceAssignment

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


RETURN
END 

GO
