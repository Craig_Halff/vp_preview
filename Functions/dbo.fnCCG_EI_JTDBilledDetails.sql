SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_JTDBilledDetails] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS 
@T TABLE 
(
	TopOrder	int,
	Descr		varchar(255),	-- Invoice
--  Invoice Date			 Total					 Labor					 Fee					 Consultant				 Expense				Other					
	Value1		datetime,	 Value2		money,		 Value3		money,		 Value4		money,		 Value5		money,		 Value6		money,		Value7		money,		
	HTMLValue1	varchar(25), HTMLValue2	varchar(25), HTMLValue3 varchar(25), HTMLValue4 varchar(25), HTMLValue5 varchar(25), HTMLValue6 varchar(25),HTMLValue7	varchar(25),
	Align1		varchar(6),	 Align2		varchar(6),	 Align3		varchar(6),	 Align4		varchar(6),	 Align5		varchar(6),	 Align6		varchar(6),	Align7		varchar(6)
)
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select * from dbo.fnCCG_EI_JTDBilledDetails('2003005.xx',' ',' ')
*/
	insert into @T (TopOrder, Descr, Value1, Value2, Value3, Value4, Value5, Value6, Value7,
		HTMLValue1, HTMLValue2, HTMLValue3, HTMLValue4, HTMLValue5, HTMLValue6, HTMLValue7,
		Align1, Align2, Align3, Align4, Align5, Align6, Align7)
	select 2 as TopOrder, 
		Invoice, InvoiceDate,
		Sum(Amount) as Total,
		Sum(Case When InvoiceSection='L' Then Amount Else 0 End) as AmountL,
		Sum(Case When InvoiceSection='F' Then Amount Else 0 End) as AmountF,
		Sum(Case When InvoiceSection='C' Then Amount Else 0 End) as AmountC,
		Sum(Case When InvoiceSection in ('E','U') Then Amount Else 0 End) as AmountE,
		Sum(Case When InvoiceSection not in ('L','F','C','E','U') Then Amount Else 0 End) as AmountOther,
		'','','','','','','','left','right','right','right','right','right','right'
	from (
		select LedgerAR.Invoice, InvoiceSection, InvoiceDate, Sum(-LedgerAR.Amount) as Amount
		from LedgerAR inner join AR on AR.WBS1=LedgerAR.WBS1 and AR.WBS2=LedgerAR.WBS2 and AR.WBS3=LedgerAR.WBS3 and AR.Invoice=LedgerAR.Invoice
		left join CA on CA.Account=LedgerAR.Account
		where LedgerAR.WBS1=@WBS1 and (@WBS2=' ' or @WBS2=LedgerAR.WBS2) and (@WBS3=' ' or @WBS3=LedgerAR.WBS3)
		  and (ledgerAR.TransType = 'IN' AND IsNull(SubType, ' ') not in ('I','R') AND AutoEntry = 'N')
		  and (CA.Type = 4 OR LedgerAR.Account IS NULL)
		group by LedgerAR.Invoice, InvoiceSection, InvoiceDate
	) as yaya
	group by Invoice, InvoiceDate
	
	delete from @T where Value1=0 and Value2=0 and Value3=0 and Value4=0 and Value5=0 and Value6=0 and Value7=0
	
		-- Add grand total line:
	insert into @T (TopOrder, Descr, Value1, Value2, Value3, Value4, Value5, Value6, Value7,
		HTMLValue1, HTMLValue2, HTMLValue3, HTMLValue4, HTMLValue5, HTMLValue6, HTMLValue7, 
		Align1, Align2, Align3, Align4, Align5, Align6, Align7) 
	select 90 as TopOrder, '<b>TOTAL</b>',null,sum(Value2),sum(Value3),sum(Value4),sum(Value5),sum(Value6),sum(Value7),
		'','','','','','','','left','right','right','right','right','right','right' from @T 
	
	update @T set 
		HTMLValue1 = Case When Value1 is null Then '&nbsp;' Else Convert(varchar(10), Value1, 101) End,
		HTMLValue2 = Case When Value2 is null Then '&nbsp;' Else '$' + Convert(varchar,Convert(money, Value2),1) End,
		HTMLValue3 = Case When Value3 is null Then '&nbsp;' Else '$' + Convert(varchar,Convert(money, Value3),1) End,
		HTMLValue4 = Case When Value4 is null Then '&nbsp;' Else '$' + Convert(varchar,Convert(money, Value4),1) End,
		HTMLValue5 = Case When Value5 is null Then '&nbsp;' Else '$' + Convert(varchar,Convert(money, Value5),1) End,
		HTMLValue6 = Case When Value6 is null Then '&nbsp;' Else '$' + Convert(varchar,Convert(money, Value6),1) End,
		HTMLValue7 = Case When Value7 is null Then '&nbsp;' Else '$' + Convert(varchar,Convert(money, Value7),1) End
		
		update @T set HTMLValue2='<b>'+HTMLValue2+'</b>' where TopOrder in (90)
		update @T set HTMLValue3='<b>'+HTMLValue3+'</b>' where TopOrder in (90)
		update @T set HTMLValue4='<b>'+HTMLValue4+'</b>' where TopOrder in (90)
		update @T set HTMLValue5='<b>'+HTMLValue5+'</b>' where TopOrder in (90)
		update @T set HTMLValue6='<b>'+HTMLValue6+'</b>' where TopOrder in (90)
		update @T set HTMLValue7='<b>'+HTMLValue7+'</b>' where TopOrder in (90)
	return
END
GO
