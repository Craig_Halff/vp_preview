SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabRevenue](
  @strRowID nvarchar(255),
  @strMode varchar(1) /* S = Self, C = Children, B = Both */
)
  RETURNS @tabRevenue TABLE (
    RowID nvarchar(255) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int,
    Name nvarchar(255) COLLATE database_default,
	Status varchar(1) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    RevenueStartDate datetime,
    RevenueEndDate datetime,
		Stage nvarchar(50) COLLATE database_default,
    ProjMgr nvarchar(20) COLLATE database_default,
    PMFullName nvarchar(255) COLLATE database_default,
    Org nvarchar(30) COLLATE database_default,
    OrgName nvarchar(100) COLLATE database_default,
		Revenue decimal(19,4),
		Probability smallint,
		WeightedRevenue decimal(19,4),
    ContractCost decimal(19,4),
    ContractBill decimal(19,4),
    HasNotes varchar(1) COLLATE database_default,
    HasChildren bit,
		RevenueCost decimal(19,4), --Set to zero in script populated by the calculator
		RevenueBill decimal(19,4), --Set to zero in script populated by the calculator
		ETCRevenueCost decimal(19,4), --Set to zero in script populated by the calculator
		ETCRevenueBill decimal(19,4), --Set to zero in script populated by the calculator
		EstFeeLessETCRevenueCost decimal(19,4), --Set to zero in script populated by the calculator
		WeightedFeeLessETCRevenueCost decimal(19,4) --Set to zero in script populated by the calculator
  )

 
 BEGIN

/**************************************************************************************************************************/
--
-- Huge Assumptions:
--
-- 1. In Non-Vision worlds (e.g. Costpoint)
--    1.1 WBS structures will be imported into PR, RPTask, and PNTask tables.
--        1.1.1 PR table has rows with WBS1, WBS2, WBS3.
--        1.1.2 RPTask and PNTable tables also have WBS1, WBS2, WBS3, but can be indented using OutlineNumber.
--        1.1.3 RPTask and PNTable tables can have up to 15 indent levels (just like currently in ngRP).
--    1.2 WBS Numbers will be in WBS1, WBS2, WBS3 
--        (same rules as currently in Vision, such as WBS2 = <blank>, WBS3 = <blank> for top most WBS row)
--    1.3 WBS3 will cover WBS Level 3 through 15 (e.g. WBS3 = {1, 1.1, 1.1.1, 1.1.1.1).
--    1.4 JTD data will be at the lowest WBS level (e.g. JTD could be at level 15). 
--    1.5 JTD is matched using WBS1, WBS2, WBS3, Employee.
--    1.6 Assignments will be at lowest WBS level 
--        (e.g. Assignment could be at level 15, same level as in the case of JTD).
--
-- 2. In Vision world
--    2.1 PR, RPTask, and PNTask tables have only 3 levels.
--    2.2 JTD data will be at the lowest WBS level.
--    2.3 Assignments will be at lowest WBS level.
--
/**************************************************************************************************************************/

  DECLARE @_SectionSign nchar = NCHAR(167) -- N'§'

  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strTaskID varchar(32)
  DECLARE @strPlanID varchar(32)
  DECLARE @strResourceType varchar(1)
  DECLARE @strTaskStatus varchar(1) = ''
  DECLARE @strWBS1 nvarchar(30)
  DECLARE @strWBS2 nvarchar(30) = ' '
  DECLARE @strWBS3 nvarchar(30) = ' '
  DECLARE @strWBS1WBS2WBS3 nvarchar(92)
	DECLARE @intRevenueCalcOption int

  -- Declare Temp tables.

  DECLARE @tabTask TABLE (
    RowID int IDENTITY(1,1),
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    WBS1WBS2WBS3 nvarchar(92) COLLATE database_default,
    Name nvarchar(255) COLLATE database_default,
	Status varchar(1) COLLATE database_default,
		StartDate datetime,
		EndDate datetime,
		RevenueStartDate datetime,
		RevenueEndDate datetime,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChildrenCount int,
    OutlineLevel int,
    HasNotes varchar(1) COLLATE database_default,
    UNIQUE (RowID, WBS1, WBS2, WBS3, ParentOutlineNumber, OutlineNumber)
  )

  DECLARE @tabWBS TABLE (
    RowID int IDENTITY(1,1),
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    WBS1WBS2WBS3 nvarchar(92) COLLATE database_default,
    Name nvarchar(255) COLLATE database_default,
	Status varchar(1) COLLATE database_default, 
		StartDate datetime,
		EndDate datetime,
		RevenueStartDate datetime,
		RevenueEndDate datetime,
		Stage nvarchar(50) COLLATE database_default,
    ProjMgr nvarchar(20) COLLATE database_default,
    PMFullName nvarchar(255) COLLATE database_default,
    Org nvarchar(30) COLLATE database_default,
    OrgName nvarchar(100) COLLATE database_default,
		Revenue decimal(19,4),
		Probability smallint,
		WeightedRevenue decimal(19,4),
    ContractCost decimal(19,4),
    ContractBill decimal(19,4),
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChildrenCount int,
    OutlineLevel int,
    HasNotes varchar(1) COLLATE database_default,
    UNIQUE (RowID, WBS1, WBS2, WBS3, ParentOutlineNumber, OutlineNumber)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END
    FROM FW_CFGSystem

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. For a WBS row, @strRowID = '|<RPTask.TaskID>'
  --   2. For a WBS row, @strRowID = '|<PR.WBS1>§<PR.WBS2>§<PR.WBS3>'

      -- Parsing for TaskID.

      SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

      -- Setting various Plan parameter.

      SELECT 
        @strPlanID = PT.PlanID,
        @strWBS1 = PT.WBS1,
        @strWBS2 = ISNULL(PT.WBS2, ' '),
        @strWBS3 = ISNULL(PT.WBS3, ' '),
        @strTaskStatus = PT.Status
        FROM RPTask AS PT 
          LEFT JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
          LEFT JOIN RPTask AS TT ON PT.PlanID = TT.PlanID AND TT.OutlineLevel = 0
          INNER JOIN PNPlan AS P ON PT.PlanID = P.PlanID
        WHERE PT.TaskID = @strTaskID
        GROUP BY PT.PlanID, PT.TaskID, PT.Status, PT.WBS1, PT.WBS2, PT.WBS3, P.Company

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		--Get Revenue calculation option
		-- 0 = Gross revenue
		-- 1 = Gross revenue - reimbursables
		-- 2 = Gross revenue - reimbursables - direct consultant

		SELECT
			@intRevenueCalcOption = RevenueCalculationsOption
		FROM CFGRMSettings

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find Task rows.  Only insert just those tasks to be returned.

  IF (@strMode = 'B' OR @strMode = 'S')  -- Get the Task for the TaskID passed in
		BEGIN
      INSERT @tabTask(
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        WBS1WBS2WBS3,
        Name,
		Status,
				StartDate,
				EndDate,
				RevenueStartDate,
				RevenueEndDate,
        ParentOutlineNumber,
        OutlineNumber,
        ChildrenCount,
        OutlineLevel,
				HasNotes
      )
        SELECT
          T.PlanID AS PlanID,
          T.TaskID AS TaskID,
          T.WBS1 AS WBS1,
          ISNULL(T.WBS2, ' ') AS WBS2,
          ISNULL(T.WBS3, ' ') AS WBS3,
          T.WBS1 + @_SectionSign + ISNULL(T.WBS2, ' ') + @_SectionSign + ISNULL(T.WBS3, ' ') AS WBS1WBS2WBS3,
          T.Name AS Name,
		  T.Status AS Status,
					T.StartDate AS StartDate,
					T.EndDate AS EndDate,
					T.RevenueStartDate AS RevenueStartDate,
					T.RevenueEndDate AS RevenueEndDate,
          T.ParentOutlineNumber AS ParentOutlineNumber,
          T.OutlineNumber AS OutlineNumber,
          T.ChildrenCount AS ChildrenCount,
          T.OutlineLevel AS OutlineLevel,
					CASE WHEN T.Notes IS NOT NULL AND T.Notes <> '' THEN 'Y' ELSE 'N' END AS HasNotes
        FROM RPTask AS T
        WHERE T.TaskID = @strTaskID 
		END

  IF (@strMode = 'B' OR @strMode = 'C')
		BEGIN
      INSERT @tabTask(
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        WBS1WBS2WBS3,
        Name,
		Status,
				StartDate,
				EndDate,
				RevenueStartDate,
				RevenueEndDate,
        ParentOutlineNumber,
        OutlineNumber,
        ChildrenCount,
        OutlineLevel,
				HasNotes
      )
        SELECT
          T.PlanID AS PlanID,
          T.TaskID AS TaskID,
          T.WBS1 AS WBS1,
          ISNULL(T.WBS2, ' ') AS WBS2,
          ISNULL(T.WBS3, ' ') AS WBS3,
          T.WBS1 + @_SectionSign + ISNULL(T.WBS2, ' ') + @_SectionSign + ISNULL(T.WBS3, ' ') AS WBS1WBS2WBS3,
          T.Name AS Name,
		  T.Status AS Status,
					T.StartDate AS StartDate,
					T.EndDate AS EndDate,
					T.RevenueStartDate AS RevenueStartDate,
					T.RevenueEndDate AS RevenueEndDate,
          T.ParentOutlineNumber AS ParentOutlineNumber,
          T.OutlineNumber AS OutlineNumber,
          T.ChildrenCount AS ChildrenCount,
          T.OutlineLevel AS OutlineLevel,
					CASE WHEN T.Notes IS NOT NULL AND T.Notes <> '' THEN 'Y' ELSE 'N' END AS HasNotes
          FROM RPTask AS T
						LEFT JOIN RPTask AS PT ON T.PlanID = PT.PlanID AND T.ParentOutlineNumber = PT.Outlinenumber
					WHERE PT.TaskID = @strTaskID AND T.TaskID <> @strTaskID AND T.WBSType <> 'LBCD'
		END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Build WBS Structure.

  -- If the input Project currently has no Plan then save the entire Project away into @tabWBS.
  -- Otherwise, save away RPTask rows into @tabWBS.
              
      INSERT @tabWBS(
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        WBS1WBS2WBS3,
        Name,
		Status,
				StartDate,
				EndDate,
				RevenueStartDate,
				RevenueEndDate,
				Stage,
				ProjMgr,
				PMFullName,
				Org,
				OrgName,
				Revenue,
				Probability,
				WeightedRevenue,
				ContractCost,
				ContractBill,
        ParentOutlineNumber,
        OutlineNumber,
        ChildrenCount,
        OutlineLevel,
				HasNotes
      )
        SELECT
          T.PlanID AS PlanID,
          T.TaskID AS TaskID,
          T.WBS1 AS WBS1,
          T.WBS2 AS WBS2,
          T.WBS3 AS WBS3,
          T.WBS1WBS2WBS3 AS WBS1WBS2WBS3,
          T.Name AS Name,
		  T.Status AS Status,
					T.StartDate AS StartDate,
					T.EndDate AS EndDate,
					T.RevenueStartDate AS RevenueStartDate,
					T.RevenueEndDate AS RevenueEndDate,
          ISNULL(STAGE.Description, '') AS Stage,
					PR.ProjMgr AS ProjMgr,
					CONVERT(nvarchar(255), ISNULL(ISNULL(PM.PreferredName, PM.FirstName), N'') + ISNULL((N' ' + PM.LastName), N'') + ISNULL(', ' + PMSuffix.Suffix, '')) AS PMFullName,
					PR.Org AS Org,
					ORGS.Name AS OrgName,
					PR.Revenue AS Revenue,
					PR.Probability AS Probability,
					PR.WeightedRevenue AS WeightedRevenue,
					CASE
						WHEN @intRevenueCalcOption = 0 THEN PR.FeeDirLab + PR.FeeDirExp + PR.ReimbAllowExp + PR.ConsultFee + PR.ReimbAllowCons
						WHEN @intRevenueCalcOption = 1 THEN PR.FeeDirLab + PR.FeeDirExp + PR.ConsultFee 
						WHEN @intRevenueCalcOption = 2 THEN PR.FeeDirLab + PR.FeeDirExp  
						WHEN @intRevenueCalcOption = 3 THEN PR.FeeDirLab 
					END AS ContractCost,
					CASE
						WHEN @intRevenueCalcOption = 0 THEN PR.FeeDirLabBillingCurrency + PR.FeeDirExpBillingCurrency + PR.ReimbAllowExpBillingCurrency + PR.ConsultFeeBillingCurrency + PR.ReimbAllowConsBillingCurrency
						WHEN @intRevenueCalcOption = 1 THEN PR.FeeDirLabBillingCurrency + PR.FeeDirExpBillingCurrency + PR.ConsultFeeBillingCurrency
						WHEN @intRevenueCalcOption = 2 THEN PR.FeeDirLabBillingCurrency + PR.FeeDirExpBillingCurrency
						WHEN @intRevenueCalcOption = 3 THEN PR.FeeDirLabBillingCurrency 
					END AS ContractBill,
          T.ParentOutlineNumber AS ParentOutlineNumber,
          T.OutlineNumber AS OutlineNumber,
          T.ChildrenCount AS ChildrenCount,
          T.OutlineLevel AS OutlineLevel,
					T.HasNotes
        FROM @tabTask AS T
          LEFT JOIN PR ON T.WBS1 = PR.WBS1 AND T.WBS2 = PR.WBS2 AND T.WBS3 = PR.WBS3
					LEFT JOIN CFGProjectStage AS STAGE ON STAGE.Code = PR.Stage
					LEFT JOIN EM AS PM ON PM.Employee = PR.ProjMgr
					LEFT JOIN CFGSuffix PMSuffix ON PMSuffix.Code = PM.Suffix
					LEFT JOIN Organization AS ORGS ON ORGS.Org = PR.Org

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Insert final result for WBS rows.
    INSERT @tabRevenue(
				RowID,
				PlanID,
				TaskID,
				ParentOutlineNumber,
				OutlineNumber,
				OutlineLevel,
				Name,
				Status,
				WBS1,
				WBS2,
				WBS3,
				StartDate,
				EndDate,
				RevenueStartDate,
				RevenueEndDate,
				Stage,
				ProjMgr,
				PMFullName,
				Org,
				OrgName,
				Revenue,
				Probability,
				WeightedRevenue,
				ContractCost,
				ContractBill,
				HasNotes,
        HasChildren,
				RevenueCost,
				RevenueBill,
				ETCRevenueCost,
				ETCRevenueBill,
				EstFeeLessETCRevenueCost,
				WeightedFeeLessETCRevenueCost
        )
        SELECT
          '|' + T.TaskID  AS RowID,
          T.PlanID AS PlanID,
          T.TaskID AS TaskID,
          T.ParentOutlineNumber AS ParentOutlineNumber,
          T.OutlineNumber AS OutlineNumber,
          T.OutlineLevel AS OutlineLevel,
          T.Name AS Name,
		  T.Status AS Status,
          ISNULL(T.WBS1, '') AS WBS1,
          ISNULL(T.WBS2, '') AS WBS2,
          ISNULL(T.WBS3, '') AS WBS3,
					T.StartDate AS StartDate,
					T.EndDate AS EndDate,
					T.RevenueStartDate AS RevenueStartDate,
					T.RevenueEndDate AS RevenueEndDate,
	        CASE WHEN T.OutlineLevel = 0 THEN T.Stage ELSE '' END AS Stage,
					T.ProjMgr AS ProjMgr,
					T.PMFullName AS PMFullName,
					T.Org AS Org,
					T.OrgName AS OrgName,
					CASE WHEN T.OutlineLevel = 0 THEN T.Revenue ELSE 0 END AS Revenue,
					CASE WHEN T.OutlineLevel = 0 THEN T.Probability ELSE 0 END AS Probability,
					CASE WHEN T.OutlineLevel = 0 THEN T.WeightedRevenue ELSE 0 END AS WeightedRevenue,
					T.ContractCost AS ContractCost,
					T.ContractBill AS ContractBill,
					T.HasNotes,
          CASE 
          WHEN T.ChildrenCount > 0 
						THEN
            CASE 
						WHEN EXISTS (SELECT 'X' FROM RPTask WHERE ParentOutlineNumber = T.OutlineNumber AND T.PlanID = PlanID AND WBSType <> 'LBCD') 
						THEN CONVERT(bit, 1) 
						ELSE CONVERT(bit, 0) 
              END 
          ELSE 
              CONVERT(bit, 0)
          END AS HasChildren,
					0 AS RevenueCost,
					0 AS RevenueBill,
					0 AS ETCRevenueCost,
					0 AS ETCRevenueBill,
					0 AS EstFeeLessETCRevenueCost,
					0 AS WeightedFeeLessETCRevenueCost
        FROM @tabWBS AS T
           
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
