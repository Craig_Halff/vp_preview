SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$rptNonBudgetEmpJTD]
  (@strPlanID varchar(32),
   @dtETCDate datetime,
   @strOutlineLevel int,
   @strTaskID varchar(32)=NULL,
   @strUnposted varchar(1) = 'N', 
   @strPlanAll varchar(1) = 'Y', 
   @strMatchWBS1Wildcard varchar(1) = 'N',
   @ReportAtBillingInBillingCurr varchar(1) = 'N')
  RETURNS @tabJTDLab TABLE
   (PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    Employee Nvarchar(20) COLLATE database_default,
    TransDate datetime,
    JTDCostCurrencyCode Nvarchar(3) COLLATE database_default,
    JTDBillCurrencyCode Nvarchar(3) COLLATE database_default,    
    PeriodHrs decimal(19,4),
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4),
    PostedFlg smallint)

/*
If @strPlanAll='Y', this function calculates the JTD numbers for ALL (both budgeted and non budgeted) employees of a plan.
If @strPlanAll='N', this function calculates the JTD numbers for ONLY non budgeted employees of a plan.
*/

BEGIN

  INSERT @tabJTDLab
    SELECT A.planID,
       A.taskID,  
       LD.employee,  
      TransDate,  
	  Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
      Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
      SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,  
      SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost,  
      SUM(BillExt) AS PeriodBill,  
      1 AS PostedFlg  
      FROM LD
        INNER JOIN PR on PR.WBS1=LD.WBS1 and PR.WBS2='' and PR.WBS3=''
        INNER JOIN rpTask AS A ON LD.WBS1 = A.WBS1 and A.outlineLevel=@strOutlineLevel
      WHERE ProjectCost = 'Y' AND A.PlanID = @strPlanID AND A.TaskID LIKE (ISNULL(@strTaskID,'%')) AND TransDate <= @dtETCDate  
        AND LD.WBS2 LIKE (ISNULL(A.WBS2, '%'))  
        AND LD.WBS3 LIKE (ISNULL(A.WBS3, '%'))  
        AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(A.LaborCode, '%'))  
        AND (@strPlanAll='Y' OR (@strPlanAll='N' AND NOT EXISTS (select 'X' from rpAssignment rpA 
          where rpA.PlanID = @strPlanID AND LD.WBS1 = rpA.WBS1 
          AND rpA.resourceID is not null and LD.employee=rpA.resourceID          
          AND LD.WBS2 LIKE (ISNULL(rpA.WBS2, '%'))  
          AND LD.WBS3 LIKE (ISNULL(rpA.WBS3, '%'))  
          AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(rpA.LaborCode, '%'))))) 
      GROUP BY A.planID,A.taskID,LD.employee, TransDate, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end 
    
  --> Unposted
  
  IF (@strUnposted = 'Y')
    BEGIN

      -- Get Unposted transactions from TK

      INSERT @tabJTDLab
        SELECT A.PlanID,
          A.TaskID,  
          TD.employee,  
          TransDate,  
	      Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
          Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
          SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,  
          SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost,  
          SUM(BillExt) AS PeriodBill,  
          -1 AS PostedFlg   
          FROM tkDetail AS TD 
          INNER JOIN PR on PR.WBS1=TD.WBS1 and PR.WBS2='' and PR.WBS3=''
          INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee  
              AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)  
          INNER JOIN rpTask AS A ON TD.WBS1 = A.WBS1 and A.outlineLevel=@strOutlineLevel
          WHERE A.PlanID = @strPlanID AND A.TaskID LIKE (ISNULL(@strTaskID,'%')) AND TransDate <= @dtETCDate  
            AND TD.WBS2 LIKE (ISNULL(A.WBS2, '%'))  
            AND TD.WBS3 LIKE (ISNULL(A.WBS3, '%'))  
            AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(A.LaborCode, '%'))  
            AND (@strPlanAll='Y' OR (@strPlanAll='N' AND NOT EXISTS (select 'X' from rpAssignment rpA 
              where rpA.PlanID = @strPlanID AND TD.WBS1 = rpA.WBS1 
              AND rpA.resourceID is not null and TD.employee=rpA.resourceID          
              AND TD.WBS2 LIKE (ISNULL(rpA.WBS2, '%'))  
              AND TD.WBS3 LIKE (ISNULL(rpA.WBS3, '%'))  
              AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(rpA.LaborCode, '%')))))
          GROUP BY A.PlanID, A.TaskID,TD.employee, TransDate, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end 

      -- Get Unposted transactions from TS

      -- ...For matching Named Resources.

      INSERT @tabJTDLab
        SELECT A.PlanID,
          A.TaskID,  
          TD.employee,  
          TransDate,  
	      Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
          Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
          SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,  
          SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost,  
          SUM(BillExt) AS PeriodBill,  
          -1 AS PostedFlg  
          FROM tsDetail AS TD  
          INNER JOIN PR on PR.WBS1=TD.WBS1 and PR.WBS2='' and PR.WBS3=''
          INNER JOIN tsControl AS TC ON (TC.Posted = 'N' AND TD.Batch = TC.Batch)  
          INNER JOIN rpTask AS A ON TD.WBS1 = A.WBS1 and A.outlineLevel=@strOutlineLevel
          WHERE A.PlanID = @strPlanID AND A.TaskID LIKE (ISNULL(@strTaskID,'%')) AND TransDate <= @dtETCDate  
            AND TD.WBS2 LIKE (ISNULL(A.WBS2, '%'))  
            AND TD.WBS3 LIKE (ISNULL(A.WBS3, '%'))  
            AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(A.LaborCode, '%'))  
            AND (@strPlanAll='Y' OR (@strPlanAll='N' AND NOT EXISTS (select 'X' from rpAssignment rpA 
              where rpA.PlanID = @strPlanID AND TD.WBS1 = rpA.WBS1 
              AND rpA.resourceID is not null and TD.employee=rpA.resourceID          
              AND TD.WBS2 LIKE (ISNULL(rpA.WBS2, '%'))  
              AND TD.WBS3 LIKE (ISNULL(rpA.WBS3, '%'))  
              AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(rpA.LaborCode, '%')))))
          GROUP BY A.PlanID, A.TaskID, TD.employee, TD.TransDate, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end 

    END -- If-Then
       
  RETURN

END -- 

GO
