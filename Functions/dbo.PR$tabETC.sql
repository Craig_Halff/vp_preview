SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PR$tabETC]
  (@strWBS1 Nvarchar(30),
   @strWBS2 Nvarchar(7),
   @strWBS3 Nvarchar(7),
   @strETCDate VARCHAR(8)) -- Date must be in format: 'yyyymmdd' regardless of UI Culture.

  RETURNS @tabETC TABLE (
     ETCLabCost decimal(19,4),
     ETCLabHrs decimal(19,4),
     ETCLabBill decimal(19,4)
  )

BEGIN

  DECLARE @dtETCDate datetime
 
  DECLARE @strPlanID varchar(32)
  DECLARE @strOutlineNumber varchar(255)

  DECLARE @strCompany Nvarchar(14)
  DECLARE @strMultiCompanyEnabled varchar(1)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

  DECLARE @decFutureLabCost decimal(19,4)
  DECLARE @decFutureLabHrs decimal(19,4)
  DECLARE @decFutureLabBill decimal(19,4)
  
  DECLARE @tabTPD TABLE(
    RowID int IDENTITY(1,1),
    TaskID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PeriodCost decimal(19,4),
    PeriodHrs decimal(19,4),
    PeriodBill decimal(19,4)
    PRIMARY KEY(RowID, TaskID)
  )
        
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled flag.
  
  SELECT
    @strMultiCompanyEnabled = MultiCompanyEnabled
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat     

  -- Save Company string for use later.
  
  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          
  -- Set ETC Dates.
  
  SET @dtETCDate = CONVERT(datetime, @strETCDate) 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find the RPTask row corresponds to the PR row.

  SET @strPlanID = NULL
  SET @strOutlineNumber = NULL

  SELECT @strPlanID = P.PlanID, @strOutlineNumber = MIN(PT.OutlineNumber)
    FROM PR
      LEFT JOIN RPTask AS PT ON PR.WBS1 = PT.WBS1 AND PR.WBS2 = ISNULL(PT.WBS2, ' ') AND PR.WBS3 = ISNULL(PT.WBS3, ' ') AND
        PT.WBSType = 
          CASE 
            WHEN PR.WBS2 = ' ' AND PR.WBS3 = ' ' THEN 'WBS1'
            WHEN PT.WBS2 <> ' ' AND PR.WBS3 = ' ' THEN 'WBS2'
            WHEN PT.WBS2 <> ' ' AND PR.WBS3 <> ' ' THEN 'WBS3'
          END
      LEFT JOIN RPPlan AS P ON PT.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y'
    WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = @strWBS2 AND PR.WBS3 = @strWBS3
    GROUP BY P.PlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Sum together the amounts that are in the future periods beyond the ETC Date boundary.

  SET @decFutureLabCost = 0.0000
  SET @decFutureLabHrs = 0.0000
  SET @decFutureLabBill = 0.0000

  SELECT
    @decFutureLabCost = SUM(TL.PeriodCost),
    @decFutureLabHrs = SUM(TL.PeriodHrs),
    @decFutureLabBill = SUM(TL.PeriodBill)
    FROM (
      /* Find TPD rows attached to leaf RPTask rows that are the descendants of the RPTask row that is mapped to the PR row in question */
      SELECT PT.TaskID, TPD.TimePhaseID, TPD.StartDate, TPD.EndDate, TPD.PeriodCost, TPD.PeriodHrs, TPD.PeriodBill
        FROM RPPlannedLabor AS TPD
          INNER JOIN RPTask AS PT ON PT.PlanID = TPD.PlanID AND PT.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL
          LEFT JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
          LEFT JOIN RPAssignment AS A ON PT.PlanID = A.PlanID AND PT.TaskID = A.TaskID
        WHERE PT.PlanID = @strPlanID AND PT.OutlineNumber LIKE (@strOutlineNumber + '%') AND 
          CT.TaskID IS NULL AND A.AssignmentID IS NULL AND TPD.PeriodHrs <> 0  AND TPD.StartDate >= @dtETCDate
      UNION ALL
      /* Find TPD rows attached to RPAssignment rows that are the descendants of the RPTask row that is mapped to the PR row in question */
      SELECT PT.TaskID, TPD.TimePhaseID, TPD.StartDate, TPD.EndDate, TPD.PeriodCost, TPD.PeriodHrs, TPD.PeriodBill
        FROM RPPlannedLabor AS TPD
          INNER JOIN RPTask AS PT ON PT.PlanID = TPD.PlanID AND PT.TaskID = TPD.TaskID AND TPD.AssignmentID IS NOT NULL
        WHERE PT.PlanID = @strPlanID AND PT.OutlineNumber LIKE (@strOutlineNumber + '%') AND TPD.PeriodHrs <> 0 AND TPD.StartDate >= @dtETCDate
    ) AS TL

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collect the TPD that straddle ETC Date.

  INSERT @tabTPD (
    TaskID,
    StartDate,
    EndDate,
    PeriodCost,
    PeriodHrs,
    PeriodBill
  )
    SELECT
      TL.TaskID,
      TL.StartDate,
      TL.EndDate,
      TL.PeriodCost,
      TL.PeriodHrs,
      TL.PeriodBill
      FROM (
        /* Find TPD rows attached to leaf RPTask rows that are the descendants of the RPTask row that is mapped to the PR row in question */
        SELECT PT.TaskID, TPD.TimePhaseID, TPD.StartDate, TPD.EndDate, TPD.PeriodCost, TPD.PeriodHrs,TPD.PeriodBill
          FROM RPPlannedLabor AS TPD
            INNER JOIN RPTask AS PT ON PT.PlanID = TPD.PlanID AND PT.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL
            LEFT JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
            LEFT JOIN RPAssignment AS A ON PT.PlanID = A.PlanID AND PT.TaskID = A.TaskID
          WHERE PT.PlanID = @strPlanID AND PT.OutlineNumber LIKE (@strOutlineNumber + '%') AND CT.TaskID IS NULL AND A.AssignmentID IS NULL 
            AND TPD.PeriodHrs <> 0  AND TPD.StartDate < @dtETCDate AND TPD.EndDate >= @dtETCDate
        UNION ALL
        /* Find TPD rows attached to RPAssignment rows that are the descendants of the RPTask row that is mapped to the PR row in question */
        SELECT PT.TaskID, TPD.TimePhaseID, TPD.StartDate, TPD.EndDate, TPD.PeriodCost, TPD.PeriodHrs, TPD.PeriodBill
          FROM RPPlannedLabor AS TPD
            INNER JOIN RPTask AS PT ON PT.PlanID = TPD.PlanID AND PT.TaskID = TPD.TaskID AND TPD.AssignmentID IS NOT NULL
          WHERE PT.PlanID = @strPlanID AND PT.OutlineNumber LIKE (@strOutlineNumber + '%') 
            AND TPD.PeriodHrs <> 0 AND TPD.StartDate < @dtETCDate AND TPD.EndDate >= @dtETCDate
      ) AS TL

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
  -- Calculate Task ETC.

  INSERT @tabETC (
    ETCLabCost,
    ETCLabHrs,
    ETCLabBill
  )
    SELECT  
      @decFutureLabCost + isNull(SUM(PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)),0) AS ETCLabCost, 
      @decFutureLabHrs + isNull(SUM(PeriodHrs * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)),0) AS ETCLabHrs, 
      @decFutureLabBill + isNull(SUM(PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)),0) AS ETCLabBill 
      FROM @tabTPD AS TPD

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
