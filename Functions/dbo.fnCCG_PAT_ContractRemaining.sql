SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_PAT_ContractRemaining] (
	@PayableSeq int,
	@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS @CustColData TABLE 
(
      ColValue money, --change this to appropriate type for column data
      ColDetail varchar(max)
)
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.
	select * from ccg_pat_payable where payablenumber = '0351485r' --4
	select * from  dbo.fnCCG_PAT_ContractRemaining(4,'R201204.40','001','024')
	select * from dbo.fnCCG_PAT_ContractRemaining(1,'R012779.02',' ',' ')
	select ColValue from dbo.fnCCG_PAT_JTDInvoicedFromVendor(1,'R012779.02',' ',' ')
*/
	declare @detail varchar(max),@contract decimal(19,5),@invoiced decimal(19,5),@remaining decimal(19,5), @type varchar(1)

	-- Get contract amount:
	select @type=PayableType from CCG_PAT_Payable pat where pat.Seq = @PayableSeq 



	If @type='C'
		Begin
			select @contract = ISNULL(Sum(pa.Amount),0) 
			from CCG_PAT_Payable pat
			left join CCG_PAT_ProjectAmount pa on pa.PayableSeq=pat.Seq
			where pat.Seq = @PayableSeq 
			  and (@WBS1 IS NULL or (pa.WBS1=@WBS1 and pa.WBS2=@WBS2 and pa.WBS3=@WBS3))
		End
	Else Begin
		select @contract = ISNULL(Sum(pa.Amount),0) 
		from CCG_PAT_Payable pat
		left join CCG_PAT_ProjectAmount pa on pa.PayableSeq=pat.ParentSeq
		where pat.Seq = @PayableSeq 
		  and (@WBS1 IS NULL or (pa.WBS1=@WBS1 and pa.WBS2=@WBS2 and pa.WBS3=@WBS3))
	End
			  
	select @invoiced= ColValue from dbo.fnCCG_PAT_JTDInvoicedFromVendor(@PayableSeq,@WBS1,@WBS2,@WBS3)
	set @remaining = isnull(@contract,0) - isnull(@invoiced,0)
	


	if @contract > 0
		begin
			if (@invoiced/@contract >= .8) and (@invoiced/@contract < 1)
				set @detail = 'BackgroundColor=#fffc33'
			else if @invoiced/@contract >= 1
				set @detail = 'BackgroundColor=#ffc0c0'
			else set @detail =  NULL
		end
	if @contract  = 0
		begin
		set @remaining = null 
		set @detail = NULL
		end
	INSERT INTO @CustColData select @remaining,@detail

	return 
ENd


GO
