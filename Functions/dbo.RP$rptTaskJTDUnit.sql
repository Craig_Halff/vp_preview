SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$rptTaskJTDUnit]
  (@strPlanID varchar(32),
   @strIncludeUnmapped varchar(1) = 'Y',
   @dtETCDate datetime,
   @ReportAtBillingInBillingCurr varchar(1) = 'N')
  RETURNS @taskJTDUnit TABLE
   (PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    StartDate datetime,
    JTDCostCurrencyCode Nvarchar(3) COLLATE database_default,
    JTDBillCurrencyCode Nvarchar(3) COLLATE database_default,    
    PeriodQty decimal(19,4),
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4))

BEGIN --outer

 INSERT @taskJTDUnit
  SELECT X.PlanID,
	   TaskID,
       TransDate as StartDate, 
       Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
       Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
       PeriodQty=ROUND(SUM(ISNULL(Ledger.UnitQuantity, 0)), 2),
	   PeriodCost=ROUND(SUM(ISNULL(Ledger.AmountProjectCurrency, 0)), 2),
	   PeriodBill=ROUND(SUM(ISNULL(Ledger.BillExt, 0)), 2)
  FROM LedgerMISC AS Ledger 
  INNER JOIN PR on PR.WBS1=Ledger.WBS1 and PR.WBS2='' and PR.WBS3=''
  INNER JOIN RPTask AS X ON (Ledger.WBS1 = X.WBS1 AND X.PlanID = @strPlanID AND Ledger.ProjectCost = 'Y' AND Ledger.TransType = 'UN') 
  INNER JOIN RPWBSLevelFormat AS F ON X.PlanID = F.PlanID AND X.WBSType = F.WBSType AND F.WBSMatch = 'Y'
  WHERE Ledger.WBS2 LIKE (ISNULL(X.WBS2, '%')) AND Ledger.WBS3 LIKE (ISNULL(X.WBS3, '%')) AND Ledger.TransDate <= @dtETCDate
  GROUP BY X.PlanID, X.TaskID, Ledger.TransDate,PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end

  if @strIncludeUnmapped = 'Y'
  begin

  DECLARE @strTaskID varchar(32) 
  DECLARE @strOutlineNumber varchar(255) 

  DECLARE csrTask CURSOR FOR 
    SELECT DISTINCT T.TaskID, T.OutlineNumber 
    FROM RPTask AS T LEFT JOIN @taskJTDUnit  AS TPD ON T.TaskID = TPD.TaskID 
    WHERE T.PlanID = @strPlanID  AND TPD.TaskID IS NULL
    ORDER BY T.OutlineNumber DESC 

  OPEN csrTask 

  FETCH NEXT FROM csrTask INTO @strTaskID, @strOutlineNumber 

  WHILE (@@FETCH_STATUS = 0) 
    BEGIN --while begin

    INSERT @taskJTDUnit 
    SELECT @strPlanID AS PlanID,
        @strTaskID AS TaskID, 
        StartDate AS StartDate, 
	    Max(JTDCostCurrencyCode) as JTDCostCurrencyCode,
        Max(JTDBillCurrencyCode) as JTDBillCurrencyCode,
        ROUND(SUM(ISNULL(PeriodQty, 0)), 2) AS PeriodQty, 
        ROUND(SUM(ISNULL(PeriodCost, 0)), 2) AS PeriodCost, 
        ROUND(SUM(ISNULL(PeriodBill, 0)), 2) AS PeriodBill
    FROM 
      (SELECT @strTaskID AS TaskID, 
        TPD.StartDate, 
	    Max(TPD.JTDCostCurrencyCode) as JTDCostCurrencyCode,
        Max(TPD.JTDBillCurrencyCode) as JTDBillCurrencyCode,
        ROUND(SUM(ISNULL(TPD.PeriodQty, 0)), 2) AS PeriodQty, 
        ROUND(SUM(ISNULL(TPD.PeriodCost, 0)), 2) AS PeriodCost, 
        ROUND(SUM(ISNULL(TPD.PeriodBill, 0)), 2) AS PeriodBill
      FROM RPTask AS CT 
      INNER JOIN @taskJTDUnit AS TPD ON TPD.TaskID = CT.TaskID 
      WHERE CT.PlanID = @strPlanID AND CT.ParentOutlineNumber = @strOutlineNumber 
      GROUP BY TPD.StartDate, TPD.JTDCostCurrencyCode, TPD.JTDBillCurrencyCode  
      UNION ALL SELECT @strTaskID AS TaskID, 
        TransDate AS StartDate, 
	    Max(JTDCostCurrencyCode) as JTDCostCurrencyCode,
        Max(JTDBillCurrencyCode) as JTDBillCurrencyCode, 
        ROUND(SUM(ISNULL(PeriodQty, 0)), 2) AS PeriodQty, 
        ROUND(SUM(ISNULL(PeriodCost, 0)), 2) AS PeriodCost, 
        ROUND(SUM(ISNULL(PeriodBill, 0)), 2) AS PeriodBill
      FROM dbo.RP$rptUnitJTD(@strPlanID,@dtETCDate,@strTaskID,@ReportAtBillingInBillingCurr)
      GROUP BY TransDate, JTDCostCurrencyCode, JTDBillCurrencyCode) AS XTPD 
      GROUP BY StartDate, JTDCostCurrencyCode, JTDBillCurrencyCode 

      FETCH NEXT FROM csrTask INTO @strTaskID, @strOutlineNumber
    END  --while end

  
  CLOSE csrTask
  DEALLOCATE csrTask
  END -- if @strIncludeUnmapped = 'Y' begin end

RETURN
END --outer

GO
