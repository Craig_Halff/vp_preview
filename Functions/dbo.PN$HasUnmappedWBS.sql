SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$HasUnmappedWBS]
  (@strWBS1 Nvarchar(30))
  RETURNS bit
BEGIN

  /*--------------------------------------------------------------------------------------------------------------------------------------------------*/
  /* This function is used to determine whether the given Vision Plan has unmapped WBS rows comparing with its Project.                               */
  /*   1. If the Project has Phases {A, B, C, D} and the Plan has Phases {A, B, C}, then the Plan is viewed as not having any unmapped row.           */
  /*   2. If the Project has Phases {A, B, C, D} and the Plan has Phases {A, B, C, E}, then the Plan is viewed as having unmapped row.                */
  /*   3. If the Project has Phases {A, B, C, D} and the Plan has Phases {A, B, <none>, D}, then the Plan is viewed as having unmapped row.           */
  /*--------------------------------------------------------------------------------------------------------------------------------------------------*/

  DECLARE @bitHasUnmappedWBS bit

  DECLARE @strPlanID varchar(32)
  DECLARE @dtCIMaxEndDate datetime

  SET @bitHasUnmappedWBS = 0

  SET @strPlanID = NULL

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strPlanID = PlanID 
    FROM RPPlan WHERE UtilizationIncludeFlg = 'Y' AND WBS1 = @strWBS1

  IF (@strPlanID IS NULL) RETURN(0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine wheter the given Vision Plan has unmapped WBS rows comparing with its Project.

  SELECT @bitHasUnmappedWBS = 
    CASE WHEN EXISTS
      (SELECT T.TaskID
         FROM RPTask AS T
           LEFT JOIN PR ON T.WBS1 = PR.WBS1 AND ISNULL(T.WBS2, ' ') = PR.WBS2 AND ISNULL(T.WBS3, ' ') = PR.WBS3
         WHERE T.PlanID = @strPlanID AND T.WBSType IS NOT NULL AND PR.Name IS NULL
      )
      THEN 1 ELSE 0 END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN @bitHasUnmappedWBS

END -- fn_PN$HasUnmappedWBS
GO
