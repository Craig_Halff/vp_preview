SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_JTDUnbilledBillingThru] (
    @WBS1 VARCHAR(30)
  , @WBS2 VARCHAR(7)
  , @WBS3 VARCHAR(7)
  , @ThruDate DATETIME
)
RETURNS MONEY
AS
    BEGIN
        /*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.
	- modified 12/7/2017 by J Gray to use @ThruDate; include/exclude BST transactions accordingly

	select dbo.fnCCG_EI_JTDUnbilledBillingThru('2003005.xx',' ',' ', CAST('2017-09-30' AS DATETIME))
	select dbo.fnCCG_EI_JTDUnbilledBillingThru('2003005.xx','1PD',' ', CAST('2017-09-30' AS DATETIME))

	20210511 - Craig H. Anderson
			   Added BilExt from BILD and BIED to account for partial billings

	20210520 - Craig H. Anderson
			   Removed W, X and D Bill statuses to ensure we are conservatively estimating revenue.

					**** IF THIS CODE IS CHANGED, THES CHANGES MUST ALSO BE MADE TO THE ****
						        REVENUE RECOGNITION SP WHICH USES THIS SAME LOGIC
	
	*/

        DECLARE @res1 MONEY
              , @res2 MONEY
              , @res3 MONEY;
        DECLARE @ThruPeriod INT;

        SET @ThruPeriod = DATEPART(yyyy, @ThruDate) * 100 + DATEPART(mm, @ThruDate);

        SELECT @res1 = SUM(Labor.BillExt)
            FROM (
                SELECT BillExt
                    FROM dbo.LD WITH (NOLOCK)
                    WHERE
                    WBS1 = @WBS1
                        AND (
                            WBS2 = @WBS2
                                OR @WBS2 = ' '
                        )
                        AND (
                            WBS3 = @WBS3
                                OR @WBS3 = ' '
                        )
                        AND ProjectCost = 'Y'
                        AND BillExt <> 0
                        AND Period <= @ThruPeriod
                        AND (
                            TransDate <= @ThruDate
                                OR TransDate IS NULL
                        )
                        AND (
                            (
                                -- removes all History-Loaded related transactions
                                BillStatus IN ( 'B', 'H', 'F' )
                                AND TransType <> 'HL'
                                AND (
                                    BilledPeriod > @ThruPeriod
                                        OR BilledPeriod = 0
                                )
                            )
                                OR (
                                    -- include History-Loaded Transactions
                                    BillStatus IN ( 'B', 'H' )
                                       AND TransType = 'HL'
                                       AND (
                                           BilledPeriod > @ThruPeriod
                                               OR BilledPeriod = 0
                                       )
                                )
                                OR (
                                    -- include History-Loaded Transactions
                                    BillStatus IN ( 'F' )
                                       AND TransType = 'HL'
                                       AND (BilledPeriod > @ThruPeriod)
                                )
                        )
            ) AS Labor;

        SELECT @res2 = SUM(Expenses.BillExt)
            FROM (
                SELECT L.BillExt
                    FROM dbo.LedgerMisc L WITH (NOLOCK)
                    WHERE
                    L.WBS1 = @WBS1
                        AND (
                            L.WBS2 = @WBS2
                                OR @WBS2 = ' '
                        )
                        AND (
                            L.WBS3 = @WBS3
                                OR @WBS3 = ' '
                        )
                        AND L.ProjectCost = 'Y'
                        AND L.BillExt <> 0
                        AND L.Period <= @ThruPeriod
                        AND (
                            L.TransDate <= @ThruDate
                                OR L.TransDate IS NULL
                        )
                        AND (
                            (
                                -- removes all History-Loaded related transactions
                                L.BillStatus IN ( 'B', 'H', 'F' )
                                AND L.TransType <> 'HE'
                                AND (
                                    L.BilledPeriod > @ThruPeriod
                                        OR L.BilledPeriod = 0
                                )
                            )
                                OR (
                                    -- include History-Loaded Transactions
                                    L.BillStatus IN ( 'B', 'H' )
                                       AND L.TransType = 'HE'
                                       AND (
                                           L.BilledPeriod > @ThruPeriod
                                               OR L.BilledPeriod = 0
                                       )
                                )
                                OR (
                                    -- include History-Loaded Transactions
                                    L.BillStatus IN ( 'F' )
                                       AND L.TransType = 'HE'
                                       AND (L.BilledPeriod > @ThruPeriod)
                                )
                        )
                UNION ALL
                SELECT L.BillExt
                    FROM dbo.LedgerAP L WITH (NOLOCK)
                    WHERE
                    L.WBS1 = @WBS1
                        AND (
                            L.WBS2 = @WBS2
                                OR @WBS2 = ' '
                        )
                        AND (
                            L.WBS3 = @WBS3
                                OR @WBS3 = ' '
                        )
                        AND L.ProjectCost = 'Y'
                        AND L.BillExt <> 0
                        AND L.Period <= @ThruPeriod
                        AND (
                            L.TransDate <= @ThruDate
                                OR L.TransDate IS NULL
                        )
                        AND (
                            (
                                -- removes all History-Loaded related transactions
                                L.BillStatus IN ( 'B', 'H', 'F' )
                                AND L.TransType <> 'HE'
                                AND (
                                    L.BilledPeriod > @ThruPeriod
                                        OR L.BilledPeriod = 0
                                )
                            )
                                OR (
                                    -- include History-Loaded Transactions
                                    L.BillStatus IN ( 'B', 'H' )
                                       AND L.TransType = 'HE'
                                       AND (
                                           L.BilledPeriod > @ThruPeriod
                                               OR L.BilledPeriod = 0
                                       )
                                )
                                OR (
                                    -- include History-Loaded Transactions
                                    L.BillStatus IN ( 'F' )
                                       AND L.TransType = 'HE'
                                       AND (L.BilledPeriod > @ThruPeriod)
                                )
                        )
                UNION ALL
                SELECT L.BillExt
                    FROM dbo.LedgerEX L WITH (NOLOCK)
                    WHERE
                    L.WBS1 = @WBS1
                        AND (
                            L.WBS2 = @WBS2
                                OR @WBS2 = ' '
                        )
                        AND (
                            L.WBS3 = @WBS3
                                OR @WBS3 = ' '
                        )
                        AND L.ProjectCost = 'Y'
                        AND L.BillExt <> 0
                        AND L.Period <= @ThruPeriod
                        AND (
                            L.TransDate <= @ThruDate
                                OR L.TransDate IS NULL
                        )
                        AND (
                            (
                                -- removes all History-Loaded related transactions
                                L.BillStatus IN ( 'B', 'H', 'F' )
                                AND L.TransType <> 'HE'
                                AND (
                                    L.BilledPeriod > @ThruPeriod
                                        OR L.BilledPeriod = 0
                                )
                            )
                                OR (
                                    -- include History-Loaded Transactions
                                    L.BillStatus IN ( 'B', 'H' )
                                       AND L.TransType = 'HE'
                                       AND (
                                           L.BilledPeriod > @ThruPeriod
                                               OR L.BilledPeriod = 0
                                       )
                                )
                                OR (
                                    -- include History-Loaded Transactions
                                    L.BillStatus IN ( 'F' )
                                       AND L.TransType = 'HE'
                                       AND (L.BilledPeriod > @ThruPeriod)
                                )
                        )
                UNION ALL
                SELECT L.BillExt
                    FROM dbo.LedgerAR L WITH (NOLOCK)
                    WHERE
                    L.WBS1 = @WBS1
                        AND (
                            L.WBS2 = @WBS2
                                OR @WBS2 = ' '
                        )
                        AND (
                            L.WBS3 = @WBS3
                                OR @WBS3 = ' '
                        )
                        AND L.ProjectCost = 'Y'
                        AND L.BillExt <> 0
                        AND L.Period <= @ThruPeriod
                        AND (
                            L.TransDate <= @ThruDate
                                OR L.TransDate IS NULL
                        )
                        AND (
                            (
                                -- removes all History-Loaded related transactions
                                L.BillStatus IN ( 'B', 'H', 'F' )
                                AND L.TransType <> 'HE'
                                AND (
                                    L.BilledPeriod > @ThruPeriod
                                        OR L.BilledPeriod = 0
                                )
                            )
                                OR (
                                    -- include History-Loaded Transactions
                                    L.BillStatus IN ( 'B', 'H' )
                                       AND L.TransType = 'HE'
                                       AND (
                                           L.BilledPeriod > @ThruPeriod
                                               OR L.BilledPeriod = 0
                                       )
                                )
                                OR (
                                    -- include History-Loaded Transactions
                                    L.BillStatus IN ( 'F' )
                                       AND L.TransType = 'HE'
                                       AND (L.BilledPeriod > @ThruPeriod)
                                )
                        )
            ) AS Expenses;

        ---------------------------------------------------
        -- Billing Tables
        ---------------------------------------------------
        SELECT @res3 = SUM(Billed.BillExt)
            FROM (
                SELECT L.BillExt
                    FROM dbo.BILD L WITH (NOLOCK)
                    WHERE
                    L.WBS1 = @WBS1
                        AND (
                            L.WBS2 = @WBS2
                                OR @WBS2 = ' '
                        )
                        AND (
                            L.WBS3 = @WBS3
                                OR @WBS3 = ' '
                        )
                        AND L.ProjectCost = 'Y'
                        AND L.BillExt <> 0
                        AND L.Period <= @ThruPeriod
                        AND (
                            L.TransDate <= @ThruDate
                                OR L.TransDate IS NULL
                        )
                        AND (
                            (
                                -- removes all History-Loaded related transactions
                                L.BillStatus IN ( 'B', 'H', 'F' )
                                AND L.TransType <> 'HL'
                                AND (
                                    L.BilledPeriod > @ThruPeriod
                                        OR L.BilledPeriod = 0
                                )
                            )
                                OR (
                                    -- include History-Loaded Transactions
                                    L.BillStatus IN ( 'B', 'H' )
                                       AND L.TransType = 'HL'
                                       AND (
                                           L.BilledPeriod > @ThruPeriod
                                               OR L.BilledPeriod = 0
                                       )
                                )
                                OR (
                                    -- include History-Loaded Transactions
                                    L.BillStatus IN ( 'F' )
                                       AND L.TransType = 'HL'
                                       AND (L.BilledPeriod > @ThruPeriod)
                                )
                        )
                UNION ALL
                SELECT L.BillExt
                    FROM dbo.BIED L WITH (NOLOCK)
                    WHERE
                    L.WBS1 = @WBS1
                        AND (
                            L.WBS2 = @WBS2
                                OR @WBS2 = ' '
                        )
                        AND (
                            L.WBS3 = @WBS3
                                OR @WBS3 = ' '
                        )
                        AND L.ProjectCost = 'Y'
                        AND L.BillExt <> 0
                        AND L.Period <= @ThruPeriod
                        AND (
                            L.TransDate <= @ThruDate
                                OR L.TransDate IS NULL
                        )
                        AND (
                            (
                                -- removes all History-Loaded related transactions
                                L.BillStatus IN ( 'B', 'H', 'F' )
                                AND L.TransType <> 'HE'
                                AND (
                                    L.BilledPeriod > @ThruPeriod
                                        OR L.BilledPeriod = 0
                                )
                            )
                                OR (
                                    -- include History-Loaded Transactions
                                    L.BillStatus IN ( 'B', 'H' )
                                       AND L.TransType = 'HE'
                                       AND (
                                           L.BilledPeriod > @ThruPeriod
                                               OR L.BilledPeriod = 0
                                       )
                                )
                                OR (
                                    -- include History-Loaded Transactions
                                    L.BillStatus IN ( 'F' )
                                       AND L.TransType = 'HE'
                                       AND (L.BilledPeriod > @ThruPeriod)
                                )
                        )
            ) AS Billed;

        SET @res1 = ISNULL(@res1, 0) + ISNULL(@res2, 0) + ISNULL(@res3, 0);
        IF @res1 = 0 SET @res1 = NULL;

        RETURN @res1;
    END;
GO
