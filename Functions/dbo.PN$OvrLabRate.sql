SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$OvrLabRate]
  (@strType varchar(1) = 'C',
   @strPlanID varchar(32),
   @strResourceID Nvarchar(20) = NULL,
   @siCategory smallint = 0,
   @strLaborCode Nvarchar(14) = NULL,
   @strGRLBCD Nvarchar(14) = NULL)
  RETURNS decimal(19,4)
    

BEGIN

  /*--------------------------------------------------------------------------------------------------------------------
    This function encapsulates the complex rules that determine the Labor Rate for a given Employee or Generic Resource.
    This function is used to get Labor Rate for each Assignment row when the Rate is a single rate (not composite rate).
  */--------------------------------------------------------------------------------------------------------------------

  DECLARE @strMultiCurrencyEnabled varchar(1)
  DECLARE @strCurrencyCode Nvarchar(3)

  DECLARE @siRtMethod smallint
  DECLARE @intRtTabNo int
  DECLARE @siGRMethod smallint
  DECLARE @intGRTabNo int

  DECLARE @decRate decimal(19,4)
 
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @decRate = 0.0000

  SELECT 
    @strMultiCurrencyEnabled = MultiCurrencyEnabled
    FROM FW_CFGSystem

  SELECT
    @siRtMethod = CASE WHEN @strType = 'C' THEN CostRtMethod ELSE BillingRtMethod END,
    @intRtTabNo = CASE WHEN @strType = 'C' THEN CostRtTableNo ELSE BillingRtTableNo END,
    @siGRMethod = GRMethod,
    @intGRTabNo = CASE WHEN @strType = 'C' THEN GenResTableNo ELSE GRBillTableNo END,
    @strCurrencyCode = CASE WHEN @strType = 'C' THEN CostCurrencyCode ELSE BillingCurrencyCode END
    FROM PNPlan WHERE PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- Determining Employee Rate
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strResourceID IS NOT NULL) --> Employee
    BEGIN
    
      IF (@siRtMethod = 1) -- Employee Provisional Rate
        BEGIN
        
	        SELECT @decRate = 
            ISNULL(
              CASE WHEN @strType = 'C' 
                THEN 
                  CASE WHEN @strMultiCurrencyEnabled <> 'Y'
                    THEN ProvCostRate 
                    ELSE
                      CASE WHEN ISNULL(MD.FunctionalCurrencyCode, N' ') = @strCurrencyCode
                        THEN ProvCostRate
                        ELSE 0.0000
                      END
                  END
                ELSE 
                  CASE WHEN @strMultiCurrencyEnabled <> 'Y'
                    THEN ProvBillRate 
                    ELSE
                      CASE WHEN ISNULL(MD.FunctionalCurrencyCode, N' ') = @strCurrencyCode
                        THEN ProvBillRate
                        ELSE 0.0000
                      END
                  END
              END, 
              0.0000
              )
            FROM EM
              LEFT JOIN CFGMainData AS MD ON ISNULL(EM.Org, N'%') LIKE CASE WHEN @strMultiCurrencyEnabled = 'Y' THEN MD.Company + '%' ELSE '%' END
            WHERE Employee = @strResourceID
               
        END -- IF (@siRtMethod = 1)

      ELSE IF (@siRtMethod = 2) -- Labor Rate Table
        BEGIN
        
	        SELECT
		        @decRate = ISNULL(R.Rate, 0.0000)
		        FROM BTRRTEmpls AS R
		        WHERE Employee = @strResourceID AND TableNo = @intRtTabNo AND
              R.StartDate = '19000101' AND R.EndDate = '99990101'
                
        END -- IF (@siRtMethod = 2)
        
      ELSE IF (@siRtMethod = 3) -- Labor Category Table
        BEGIN
        
	        SELECT
		        @decRate = ISNULL(R.Rate, 0.0000) 
		        FROM EM
		          LEFT JOIN BTRCTEmpls AS OC ON EM.Employee = OC.Employee AND OC.TableNo = @intRtTabNo
		          INNER JOIN BTRCTCats AS R ON R.TableNo = @intRtTabNo AND R.Category = ISNULL(OC.Category, EM.BillingCategory)
		        WHERE EM.Employee = @strResourceID AND
              R.StartDate = '19000101' AND R.EndDate = '99990101'
        
        END -- IF (@siRtMethod = 3)
        
      ELSE IF (@siRtMethod = 4) -- Labor Code Table
        BEGIN
        
	        SELECT
		        @decRate = ISNULL(RA.Rate, 0.0000) 
            FROM BTRLTCodes AS RA
              INNER JOIN
                (SELECT TOP 1 LaborCodeMask FROM BTRLTCodes, CFGFormat
                    WHERE TableNo = @intRtTabNo AND
                      (SUBSTRING(@strLaborCode, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
					            SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(@strLaborCode, LC1Start, LC1Length)) AND  
					            (SUBSTRING(@strLaborCode, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
					            SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(@strLaborCode, LC2Start, LC2Length)) AND  
					            (SUBSTRING(@strLaborCode, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
					            SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(@strLaborCode, LC3Start, LC3Length)) AND  
					            (SUBSTRING(@strLaborCode, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
					            SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(@strLaborCode, LC4Start, LC4Length)) AND  
					            (SUBSTRING(@strLaborCode, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
					            SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(@strLaborCode, LC5Start, LC5Length))) AS RB
					    ON RA.LaborCodeMask = RB.LaborCodeMask
            WHERE RA.TableNo = @intRtTabNo AND
              RA.StartDate = '19000101' AND RA.EndDate = '99990101'
        
        END -- IF (@siRtMethod = 4)
    
    END -- IF (@strResourceID IS NOT NULL) THEN
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- Determining Generic Resource Rate
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  ELSE --> Genric Resource
    BEGIN
    
      IF (@siGRMethod = 0) -- Labor Category
        BEGIN
        
	        SELECT
		        @decRate = ISNULL(R.Rate, 0.0000) 
		        FROM BTRCTCats AS R
		        WHERE TableNo = @intGRTabNo AND Category = @siCategory AND
              R.StartDate = '19000101' AND R.EndDate = '99990101'
        
        END -- IF (@siGRMethod = 0)
        
      ELSE IF (@siGRMethod = 1) -- Labor Code
        BEGIN
        
	        SELECT
		        @decRate = ISNULL(RA.Rate, 0.0000) 
            FROM BTRLTCodes AS RA
              INNER JOIN
                (SELECT TOP 1 LaborCodeMask FROM BTRLTCodes, CFGFormat
                    WHERE TableNo = @intGRTabNo AND
                      (SUBSTRING(@strGRLBCD, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
					            SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(@strGRLBCD, LC1Start, LC1Length)) AND  
					            (SUBSTRING(@strGRLBCD, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
					            SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(@strGRLBCD, LC2Start, LC2Length)) AND  
					            (SUBSTRING(@strGRLBCD, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
					            SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(@strGRLBCD, LC3Start, LC3Length)) AND  
					            (SUBSTRING(@strGRLBCD, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
					            SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(@strGRLBCD, LC4Start, LC4Length)) AND  
					            (SUBSTRING(@strGRLBCD, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
					            SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(@strGRLBCD, LC5Start, LC5Length))) AS RB
					    ON RA.LaborCodeMask = RB.LaborCodeMask
            WHERE RA.TableNo = @intGRTabNo AND
              RA.StartDate = '19000101' AND RA.EndDate = '99990101'
        
        END -- IF (@siGRMethod = 1)
    		            
    END -- IF (@strResourceID IS NOT NULL) ELSE
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN(@decRate)

END -- fn_PN$OvrLabRate
GO
