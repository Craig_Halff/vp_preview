SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabDecimals]
  (@strWBS1 nvarchar(30))

  RETURNS @tabDecimals TABLE(
    HrDecimals smallint,
    QtyDecimals smallint,
    AmtCostDecimals smallint,
    AmtBillDecimals smallint,
    LabRateDecimals smallint,
    LabRevDecimals smallint,
    ECURevDecimals smallint
  )

BEGIN

  /*--------------------------------------------------------------------------------------------------------------------
    This function encapsulates the rules for Decimal Settings in Resource Planning.
    This function will return the following columns:
      HrDecimals: Decimal Setting for Labor Hours.
      QtyDecimals: Decimal Setting for Unit Quantity.
      AmtCostDecimals: Decimal Setting for Cost Amount.
      AmtBillDecimals: Decimal Setting for Billing Amount.
      LabRateDecimals: Decimal Setting for Labor Rate.
      LabRevDecimals: Decimal Setting for Labor Revenue.
      ECURevDecimals: Decimal Setting for Expense/Consultant/Unit Revenue.
  */--------------------------------------------------------------------------------------------------------------------

  DECLARE @strCompany nvarchar(14)
  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)
  DECLARE @strReimbMethod varchar(1)
  DECLARE @strCostCurrencyCode nvarchar(3)
  DECLARE @strBillCurrencyCode nvarchar(3)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint
  DECLARE @siHrDecimals smallint
  DECLARE @siQtyDecimals smallint
  DECLARE @siAmtDecimals smallint
  DECLARE @siLabRateDecimals smallint
  DECLARE @siLabMultType smallint

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length
    FROM CFGFormat

  -- PR.ProjectCurrencyCode
  -- PR.BillingCurrencyCode
  -- PNPlan.CostCurrencyCode
  -- PNPlan.BillingCurrencyCode

  SELECT 
    @strCompany = 
      CASE 
        WHEN @strMultiCompanyEnabled = 'Y' 
        THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') 
        ELSE ' ' 
      END,
    @strCostCurrencyCode = PR.ProjectCurrencyCode,
    @strBillCurrencyCode = PR.BillingCurrencyCode,
    @siLabMultType = P.LabMultType,
    @strReimbMethod = P.ReimbMethod
    FROM PR
      LEFT JOIN PNPlan AS P ON PR.WBS1 = P.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
    WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '

   -- Get Decimal Settings from CFGRMSettings by Company.

  SELECT
    @siHrDecimals = HrDecimals,
    @siAmtDecimals = AmtDecimals
    FROM CFGRMSettings AS CRM
 
  -- Get Decimal Settings from CFGResourcePlanning by Company.

  SELECT
    @siQtyDecimals = QtyDecimals,
    @siLabRateDecimals = LabRateDecimals
    FROM CFGResourcePlanning AS CRP
    WHERE CRP.Company = @strCompany

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabDecimals (
    HrDecimals,
    QtyDecimals,
    AmtCostDecimals,
    AmtBillDecimals,
    LabRateDecimals,
    LabRevDecimals,
    ECURevDecimals
  )
    SELECT 
      @siHrDecimals AS HrDecimals,
      @siQtyDecimals AS QtyDecimals, 
      CASE WHEN @siAmtDecimals = -1 THEN CC.DecimalPlaces ELSE @siAmtDecimals END AS AmtCostDecimals, 
      CASE WHEN @siAmtDecimals = -1 THEN BC.DecimalPlaces ELSE @siAmtDecimals END AS AmtBillDecimals, 
      @siLabRateDecimals AS LabRateDecimals, 
      CASE 
        WHEN @siLabMultType < 2 
        THEN 
          CASE 
            WHEN @siAmtDecimals = -1 
            THEN CC.DecimalPlaces 
            ELSE @siAmtDecimals
          END 
        ELSE 
          CASE 
            WHEN @siAmtDecimals = -1 
            THEN BC.DecimalPlaces 
            ELSE @siAmtDecimals 
          END 
      END AS LabRevDecimals, 
      CASE 
        WHEN @strReimbMethod = 'C' 
        THEN 
          CASE 
            WHEN @siAmtDecimals = -1 
            THEN CC.DecimalPlaces 
            ELSE @siAmtDecimals 
          END 
        ELSE 
          CASE 
            WHEN @siAmtDecimals = -1 
            THEN BC.DecimalPlaces 
            ELSE @siAmtDecimals 
          END 
      END AS ECURevDecimals
      FROM FW_CFGCurrency AS CC, FW_CFGCurrency AS BC 
      WHERE CC.Code = @strCostCurrencyCode AND BC.Code = @strBillCurrencyCode 
      
  RETURN

END -- fn_stRP$tabDecimals
GO
