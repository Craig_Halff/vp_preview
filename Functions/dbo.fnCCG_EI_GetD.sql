SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnCCG_EI_GetD] (@s varchar(max))
RETURNS varchar(max)
WITH ENCRYPTION
BEGIN
	return Convert(varchar(max),DecryptByPassPhrase('CCG_EI_123',Convert(varbinary(max), @s, 2)))
END
GO
