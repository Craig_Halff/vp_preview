SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$DiffBaseline]
  (@strPlanID varchar(32)
  )
  RETURNS bit
BEGIN -- Function PN$DiffBaseline

  DECLARE @bitDiff bit

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

   SELECT @bitDiff = CASE WHEN SUM(DX) > 0 THEN 1 ELSE 0 END FROM
     (SELECT CASE WHEN EXISTS
        (SELECT MIN(VorN) AS VorN FROM
           (SELECT 
              0 AS VorN,
              BaselineLaborHrs, BaselineLabCost, BaselineLabBill, BaselineExpCost, BaselineExpBill, BaselineConCost, BaselineConBill,
              BaselineDirExpCost, BaselineDirExpBill, BaselineDirConCost, BaselineDirConBill, BaselineStart, BaselineFinish
              FROM PNPlan AS N WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS VorN,
              BaselineLaborHrs, BaselineLabCost, BaselineLabBill, BaselineExpCost, BaselineExpBill, BaselineConCost, BaselineConBill,
              BaselineDirExpCost, BaselineDirExpBill, BaselineDirConCost, BaselineDirConBill, BaselineStart, BaselineFinish
              FROM RPPlan AS V WHERE PlanID = @strPlanID
           ) ZP
           GROUP BY
             BaselineLaborHrs, BaselineLabCost, BaselineLabBill, BaselineExpCost, BaselineExpBill, BaselineConCost, BaselineConBill,
             BaselineDirExpCost, BaselineDirExpBill, BaselineDirConCost, BaselineDirConBill, BaselineStart, BaselineFinish
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Labor TPD */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(VorN) AS VorN FROM
           (SELECT 
              0 AS VorN,
              TaskID, PlanID, AssignmentID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodHrs, PeriodCost, PeriodBill, PeriodRev, CostRate, BillingRate
              FROM PNBaselineLabor AS N WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS VorN,
              TaskID, PlanID, AssignmentID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodHrs, PeriodCost, PeriodBill, PeriodRev, CostRate, BillingRate
              FROM RPBaselineLabor AS V WHERE PlanID = @strPlanID
           ) ZTPD
           GROUP BY
             TaskID, PlanID, AssignmentID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodHrs, PeriodCost, PeriodBill, PeriodRev, CostRate, BillingRate
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Expense TPD */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(VorN) AS VorN FROM
           (SELECT 
              0 AS VorN,
              TaskID, PlanID, ExpenseID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
              FROM PNBaselineExpenses AS N WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS VorN,
              TaskID, PlanID, ExpenseID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
              FROM RPBaselineExpenses AS V WHERE PlanID = @strPlanID
           ) ZTPD
           GROUP BY
             TaskID, PlanID, ExpenseID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Consultant TPD */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(VorN) AS VorN FROM
           (SELECT 
              0 AS VorN,
              TaskID, PlanID, ConsultantID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
              FROM PNBaselineConsultant AS N WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS VorN,
              TaskID, PlanID, ConsultantID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
              FROM RPBaselineConsultant AS V WHERE PlanID = @strPlanID
           ) ZTPD
           GROUP BY
             TaskID, PlanID, ConsultantID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
     ) AS Z

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN @bitDiff

END -- PN$DiffBaseline
GO
