SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_PAT_Source] (@PayableSeq int, @WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS varchar(20)
AS BEGIN
/*
	Copyright (c) 2018 Elevia Software Inc.  All rights reserved.

	select * from CCG_PAT_ProjectAmount where payableseq = 6
	select dbo.fnCCG_PAT_Source (5,NULL,NULL,NULL) 
	select top 30 * from CCG_PAT_Payable 
	select distinct sourcetype from CCG_PAT_Payable where createdate > getdate() - 300

*/
    declare @res varchar(20)

	select @res = 
		case when SourceType = 'SP' then 'Check Req' 
			when SourceType = 'U' then 'PO' 
			when SourceType = 'PD' then 'Per Diem' 
			when ISNULL(SourceType,'') ='' then 'Manual' 
			else SourceType 
		end
	from CCG_PAT_Payable pat 
	where pat.Seq=@PayableSeq

	return @res
END

GO
