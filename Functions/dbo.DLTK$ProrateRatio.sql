SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DLTK$ProrateRatio] (
  @dtStartDate1 AS datetime, 
  @dtEndDate1 AS datetime,
  @dtStartDate2 AS datetime, 
  @dtEndDate2 AS datetime, 
  @strCompany AS Nvarchar(14)
)
  RETURNS float
BEGIN

  -- WD = Work Days --> Calendar Days that are not weekend or holidays.
  -- CD = Calendar Days --> Calendar Days with regardless of weekend or holidays.
  
  DECLARE @flReturn AS float = 0.0

  DECLARE @flNumeratorWD AS float
  DECLARE @flTotalWD AS float
  
  DECLARE @flNumeratorCD AS float
  DECLARE @flTotalCD AS float
  
  SET @flNumeratorWD = dbo.DLTK$NumWorkingDays(@dtStartDate1, @dtEndDate1, @strCompany)
  SET @flTotalWD = dbo.DLTK$NumWorkingDays(@dtStartDate2, @dtEndDate2, @strCompany)
  
  IF (@flTotalWD = 0)
    BEGIN
      SET @flNumeratorCD = CONVERT(float, (DATEDIFF(dd, @dtStartDate1, @dtEndDate1) + 1))
      SET @flTotalCD = CONVERT(float, (DATEDIFF(dd, @dtStartDate2, @dtEndDate2) + 1))
    END /* END IF (@flNumeratorWD = 0) */

  SELECT
    @flReturn = 
      CASE 
        WHEN @flTotalWD != 0
        THEN @flNumeratorWD / @flTotalWD
        ELSE /* @flTotalWD = 0 */
          CASE 
            WHEN @flTotalCD != 0
            THEN @flNumeratorCD / @flTotalCD
            ELSE 0
          END
      END 

  RETURN @flReturn

END -- fn_DLTK$ProrateRatio
GO
