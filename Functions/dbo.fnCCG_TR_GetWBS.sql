SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnCCG_TR_GetWBS] (@TableName varchar(32), @Period int, @PostSeq int, @PKey varchar(32))
RETURNS varchar(64) AS
BEGIN

	declare @res varchar(64)
	if @TableName='LD'
		select @res = WBS1 + '/' + WBS2 + '/' + WBS3 from LD where Period=@Period and PostSeq=@PostSeq and PKey=@PKey
	else if @TableName='BILD'
		select @res = WBS1 + '/' + WBS2 + '/' + WBS3 from BILD where Period=@Period and PostSeq=@PostSeq and PKey=@PKey
	else if @TableName='BIED'
		select @res = WBS1 + '/' + WBS2 + '/' + WBS3 from BIED where Period=@Period and PostSeq=@PostSeq and PKey=@PKey
	else if @TableName='LedgerAP'
		select @res = WBS1 + '/' + WBS2 + '/' + WBS3 from LedgerAP where Period=@Period and PostSeq=@PostSeq and PKey=@PKey
	else if @TableName='LedgerEX'
		select @res = WBS1 + '/' + WBS2 + '/' + WBS3 from LedgerEX where Period=@Period and PostSeq=@PostSeq and PKey=@PKey
	else if @TableName='LedgerMisc'
		select @res = WBS1 + '/' + WBS2 + '/' + WBS3 from LedgerMisc where Period=@Period and PostSeq=@PostSeq and PKey=@PKey
	else if @TableName='LedgerAR'
		select @res = WBS1 + '/' + WBS2 + '/' + WBS3 from LedgerAR where Period=@Period and PostSeq=@PostSeq and PKey=@PKey
	else
		set @res=''
	return @res
END
GO
