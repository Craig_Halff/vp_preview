SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[UnbilledRoundedRate] (
    @BaseRate decimal(19,4),
    @BillingDecimals smallint,
    @LabMeth smallint,
    @Mult1 decimal(19,4),
    @Mult2 decimal(19,4),
    @Mult3 decimal(19,4))
RETURNS decimal(19,4)
AS
 BEGIN
  DECLARE @workMult1 decimal(19,4)
  DECLARE @workMult2 decimal(19,4)
  DECLARE @workMult3 decimal(19,4)
  DECLARE @precision int
  DECLARE @billRate decimal(19,4)
  SET @workMult1= CASE WHEN IsNull(@Mult1, 0) = 0 THEN 1 ELSE @Mult1 END
  SET @workMult2= CASE WHEN IsNull(@Mult2, 0) = 0 THEN 1 ELSE @Mult2 END
  SET @workMult3= CASE WHEN IsNull(@Mult3, 0) = 0 THEN 1 ELSE @Mult3 END

BEGIN
  IF (@BaseRate <> Round(@BaseRate, @BillingDecimals)) 
    BEGIN
        IF @BillingDecimals = 0 
           BEGIN
              SET @precision = (@BillingDecimals + (Len(left(@BaseRate,(CHARINDEX('.', @BaseRate)+ 0))) - Len(replace(rtrim(replace(Round(@BaseRate, @BillingDecimals),'0', ' ')),' ', '0'))))
           END
        ELSE
           BEGIN
              SET @precision = (@BillingDecimals + (Len(left(@BaseRate,(CHARINDEX('.', @BaseRate)+ 4))) - Len(replace(rtrim(replace(Round(@BaseRate, @BillingDecimals),'0', ' ')),' ', '0'))))
           END
     END
  ELSE -- BaseRate = Rounded Rate
    BEGIN
      SET @precision = @BillingDecimals
    END
 END 

  BEGIN
    IF @LabMeth = 1 OR (@workMult1 <> 1 OR @workMult2 <> 1 OR @workMult3 <> 1)
       BEGIN
          SET @precision = @BillingDecimals
        END 
  END

  BEGIN
     SET @billRate = Round(@BaseRate * @workMult1, @precision)
     SET @billRate = Round(@billRate * @workMult2, @precision)
     SET @billRate = Round(@billRate * @workMult3, @precision)
  END
RETURN @billRate
 END
GO
