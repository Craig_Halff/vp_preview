SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnCCG_EI_StatusChanged] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS @T TABLE 
(
       ColValue				Varchar,
       ColDetail			Varchar(255)
)
AS BEGIN
/*
	Copyright 2017 Elevia Software.  All rights reserved.
	
	select * from dbo.fnccg_ei_statuschanged( '031293.000', '00000', ' ')
*/
	declare @change varchar, @detail varchar(255), @eistatus varchar, @prstatus varchar, @modemp varchar(10) 
	set @change = 'N'
	set @detail = ' '
	if @WBS2=' ' and exists (
       select 'x' from PR inner join CCG_EI_CustomColumns eicc on PR.WBS1=eicc.WBS1 and PR.WBS2=eicc.WBS2 and PR.WBS3=eicc.WBS3  
       where PR.WBS1=@WBS1 and PR.WBS2<>' ' and eicc.Status_ModEmp is not null and ISNULL(eicc.Status,'') <> PR.Status  
)		set @change = 'Y'

	if @wbs2 <> ' ' and @WBS3 = ' ' and exists (
       select 'x' from PR inner join CCG_EI_CustomColumns eicc on PR.WBS1=eicc.WBS1 and PR.WBS2=eicc.WBS2 and PR.WBS3=eicc.WBS3  
       where PR.WBS1=@WBS1 and PR.WBS2=@wbs2 and pr.wbs3 <> ' ' and eicc.Status_ModEmp is not null and ISNULL(eicc.Status,'') <> PR.Status  
)		set @change = 'Y'

    select @eistatus = status, @modemp = Status_ModEmp from CCG_EI_CustomColumns where @wbs1 = Wbs1 and @wbs2 = wbs2 and @wbs3 = wbs3
	select @prstatus = status from pr where @wbs1 = Wbs1 and @wbs2 = wbs2 and @wbs3 = wbs3
	if isnull(@eistatus,' ') <> @prstatus and @ModEmp is not null set @change = 'Y'

	if isnull(@change,' ') = 'Y'
		set @detail = 'BackgroundColor=#ffc0c0'
	insert into @T values (@change, @detail)
	return 
END
GO
