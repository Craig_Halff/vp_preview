SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DW$tabEarnedValue]
  (@strCompany nvarchar(14) = ' ',
   @strScale varchar(1) = 'm')
  RETURNS @tabEV TABLE
    (PlanID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     WBS1 nvarchar(30) COLLATE database_default,
     WBS2 nvarchar(30) COLLATE database_default, 
     WBS3 nvarchar(30) COLLATE database_default,
     LaborCode nvarchar(14) COLLATE database_default,
     TransactionDate int,
     DEVPlannedCost decimal(19,4),
     DEVPlannedBill decimal(19,4),
     DEVBaselineCost decimal(19,4),
     DEVBaselineBill decimal(19,4),
     DEVFeesCost decimal(19,4),
     DEVFeesBill decimal(19,4),
     EVPct decimal(19,4)      
    )
BEGIN -- Function DW$tabEarnedValue
 
  DECLARE @dtStartDate datetime
  DECLARE @dtEndDate datetime
  DECLARE @dtIntervalEnd datetime
  
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
    
  DECLARE @intEVCount int
  DECLARE @intWkEndDay AS int
    
  -- Declare Temp tables.
  
  DECLARE @tabPlan
    TABLE (PlanID varchar(32) COLLATE database_default
           PRIMARY KEY(PlanID)) 
  
  DECLARE @tabTask
    TABLE (PlanID varchar(32) COLLATE database_default,
           TaskID varchar(32) COLLATE database_default,
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(30) COLLATE database_default,
           WBS3 Nvarchar(30) COLLATE database_default,
           LaborCode Nvarchar(14) COLLATE database_default,
           PlannedLabCost decimal(19, 4),
           PlannedLabBill decimal(19, 4),
           BaselineLabCost decimal(19, 4),
           BaselineLabBill decimal(19, 4),
           FeesCost decimal(19, 4),
           FeesBill decimal(19, 4)
           PRIMARY KEY(PlanID, TaskID)) 

  DECLARE @tabCalendarInterval
    TABLE(StartDate datetime,
          EndDate datetime
          PRIMARY KEY(StartDate, EndDate))
          
  DECLARE @tabEVTPD
	TABLE (RowID int identity(1, 1),
           CIStartDate datetime, 
           PlanID varchar(32) COLLATE database_default,
           TaskID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodPct decimal(19,4)
           PRIMARY KEY(RowID, PlanID, EndDate))
           
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Filter out only Plans with same Company
  
  INSERT @tabPlan(PlanID) SELECT PlanID FROM RPPlan WHERE Company = @strCompany
  
  SELECT
    @intWkEndDay = (CASE WHEN StartingDayOfWeek = 1 THEN 7 ELSE StartingDayOfWeek - 1 END)
    FROM CFGResourcePlanning
    WHERE CFGResourcePlanning.Company = @strCompany

  -- Get decimal settings.
  -- At this point, there is no way to know what was the basis used in calculation of Baseline Revenue numbers.
  -- Therefore, we decided to use the current basis to determine number of decimal digits to be used
  -- in the realignment of Baseline Revenue numbers.
  
  SELECT @intAmtCostDecimals = 2,
         @intAmtBillDecimals = 2
    
  -- Check to see if there is any time-phased data
  
    SELECT @intEVCount = COUNT(*) FROM RPEVT AS TPD INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID

  IF (@intEVCount > 0)
    BEGIN
      
      INSERT @tabTask
        (PlanID,
         TaskID,
         PlannedLabCost,
         PlannedLabBill,
         BaselineLabCost,
         BaselineLabBill,
         FeesCost,
         FeesBill) 
        SELECT DISTINCT 
          T.PlanID,
          T.TaskID,
          T.PlannedLabCost,
          T.PlannedLabBill,
          T.BaselineLabCost,
          T.BaselineLabBill,
 	      (T.CompensationFee + T.ConsultantFee + T.ReimbAllowance) AS FeesCost,
	      (T.CompensationFeeBill + T.ConsultantFeeBill + T.ReimbAllowanceBill) AS FeesBill        
          FROM RPEVT AS TPD
            INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID
            INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
            
      -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>      
    
      -- Find the MIN and MAX dates of all TPD.
  
      SELECT @dtStartDate = MIN(MINDate), @dtEndDate = MAX(MAXDate)
        FROM
          (SELECT MIN(TPD.StartDate) AS MINDate, MAX(TPD.EndDate) AS MAXDate FROM RPEVT AS TPD INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID
          ) AS X

      -- Save Calendar Intervals into a temp table and add an overflow Calendar Interval.
      
      WHILE (@dtStartDate <= @dtEndDate)
        BEGIN
        
          -- Compute End Date of interval.

          IF (@strScale = 'd') 
            SET @dtIntervalEnd = @dtStartDate
          ELSE
            SET @dtIntervalEnd = dbo.DLTK$IntervalEnd(@dtStartDate, @strScale, @intWkEndDay)
        
          IF (@dtIntervalEnd > @dtEndDate) 
            SET @dtIntervalEnd = @dtEndDate
            
          -- Insert new Calendar Interval record.
          
          INSERT @tabCalendarInterval(StartDate, EndDate)
            VALUES (@dtStartDate, @dtIntervalEnd)
          
          -- Set Start Date for next interval.
          
          SET @dtStartDate = DATEADD(d, 1, @dtIntervalEnd)
        
        END -- End While

    END -- IF (@intEVCount > 0)
      
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            
  IF (@intEVCount > 0)
    BEGIN
    
      -- Save EV Percent time-phased data rows.
      -- There should be only one Task row that has EVPct children.
      
      -- For EV Pct, pick the highest Pct for the common periods.
      --
      --   Old Scale: |   25  |  50  |
      --   New Scale: |      50      |
      --
      --   Old Scale:    |      25      |      75      |      90      
      --   New Scale: |   25  |  25  |  75  |  75  |  90  |

      INSERT @tabEVTPD
        (CIStartDate,
         PlanID, 
         TaskID,
         StartDate, 
         EndDate, 
         PeriodPct)
        SELECT
          CIStartDate AS CIStartDate,
          PlanID, 
          TaskID,
          MIN(StartDate) AS StartDate, 
          MAX(EndDate) AS EndDate, 
          ROUND(MAX(ISNULL(PeriodPct, 0)), 2) AS PeriodPct
          FROM
            (SELECT 
               CI.StartDate AS CIStartDate, 
               TPD.PlanID AS PlanID, 
               TPD.TaskID AS TaskID,
               CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
               CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
               PeriodPct AS PeriodPct
               FROM RPEVT AS TPD
                 INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID
                 INNER JOIN @tabCalendarInterval AS CI ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
            ) AS X
          GROUP BY CIStartDate, PlanID, TaskID
          
    END -- IF (@intEVCount > 0)
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT INTO @tabEV
    (PlanID,
     TaskID,
     WBS1,
     WBS2, 
     WBS3,
     LaborCode,
     TransactionDate,
     DEVPlannedCost,
     DEVPlannedBill,
     DEVBaselineCost,
     DEVBaselineBill,
     DEVFeesCost,
     DEVFeesBill,
     EVPct      
    )
    SELECT
      T.PlanID,
      T.TaskID,
      CASE WHEN T.WBS1 = '<none>' THEN '<empty>' ELSE ISNULL(T.WBS1, '<empty>') END AS WBS1,
      CASE WHEN T.WBS2 = '<none>' THEN '<empty>' ELSE ISNULL(T.WBS2, '<empty>') END AS WBS2,
      CASE WHEN T.WBS3 = '<none>' THEN '<empty>' ELSE ISNULL(T.WBS3, '<empty>') END AS WBS3,
      CASE WHEN T.LaborCode = '<none>' THEN '<empty>' ELSE ISNULL(T.LaborCode, '<empty>') END AS LaborCode,
      ISNULL(CONVERT(INT, TPD.StartDate), -1) AS TransactionDate,
      ((ISNULL(TPD.PeriodPct, 0) - ISNULL(MAX(TPDPrev.PeriodPct), 0)) * ISNULL(T.PlannedLabCost, 0) / 100) AS DEVPlannedCost,
      ((ISNULL(TPD.PeriodPct, 0) - ISNULL(MAX(TPDPrev.PeriodPct), 0)) * ISNULL(T.PlannedLabBill, 0) / 100) AS DEVPlannedBill,
      ((ISNULL(TPD.PeriodPct, 0) - ISNULL(MAX(TPDPrev.PeriodPct), 0)) * ISNULL(T.BaselineLabCost, 0) / 100) AS DEVBaselineCost,
      ((ISNULL(TPD.PeriodPct, 0) - ISNULL(MAX(TPDPrev.PeriodPct), 0)) * ISNULL(T.BaselineLabBill, 0) / 100) AS DEVBaselineBill,
      ((ISNULL(TPD.PeriodPct, 0) - ISNULL(MAX(TPDPrev.PeriodPct), 0)) * ISNULL(T.FeesCost, 0) / 100) AS DEVFeesCost,
      ((ISNULL(TPD.PeriodPct, 0) - ISNULL(MAX(TPDPrev.PeriodPct), 0)) * ISNULL(T.FeesBill, 0) / 100) AS DEVFeesBill,
      ISNULL(TPD.PeriodPct, 0)  AS EVPct
      FROM @tabEVTPD AS TPD
        INNER JOIN @tabTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
        LEFT JOIN @tabEVTPD AS TPDPrev ON TPD.PlanID = TPDPrev.PlanID AND TPD.TaskID = TPDPrev.TaskID AND TPDPrev.EndDate < TPD.StartDate
      GROUP BY T.PlanID, T.TaskID, T.WBS1, T.WBS2, T.WBS3, T.LaborCode, 
        T.PlannedLabCost, T.PlannedLabBill, T.BaselineLabCost, T.BaselineLabBill, T.FeesCost, T.FeesBill,
        TPD.PlanID, TPD.TaskID, TPD.StartDate, TPD.PeriodPct

  RETURN

END -- DW$tabEarnedValue
GO
