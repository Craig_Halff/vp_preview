SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PM$tabPlanHasNonNavData]
  (@strPlanID varchar(32))

   RETURNS @tabResults TABLE
    (HasCompFee bit,
     HasConsFee bit,
     HasReimFee bit,
     HasEVT bit,
     HasDependencies bit
    )

BEGIN

  INSERT @tabResults
    (HasCompFee,
     HasConsFee,
     HasReimFee,
     HasEVT,
     HasDependencies
    )
    SELECT
      CASE WHEN EXISTS(SELECT 'X' FROM RPCompensationFee WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END,
      CASE WHEN EXISTS(SELECT 'X' FROM RPConsultantFee WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END,
      CASE WHEN EXISTS(SELECT 'X' FROM RPReimbAllowance WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END,
      CASE WHEN EXISTS(SELECT 'X' FROM RPEVT WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END,
      CASE WHEN EXISTS(SELECT 'X' FROM RPDependency WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
