SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_BilledPercent] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS decimal(19,2)
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group. All rights reserved.

	exec dbo.fnCCG_EI_BilledPercent '031481.000',' ',' '
*/
     declare @Percent decimal(19,2), @contract money, @JTDBilled money, @draft money
	 --select @draft  = colvalue from dbo.fnCCG_EI_DraftInvoiceAmt(@wbs1,@wbs2,@wbs3)
     select @JTDBilled = dbo.fnCCG_EI_JTDBilled(@wbs1,@wbs2,@wbs3)
	 select @Contract = dbo.fnCCG_EI_Compensation(@wbs1,@wbs2,@wbs3)
	 set @Percent = (@JTDBilled)/@contract

     return @Percent
END
GO
