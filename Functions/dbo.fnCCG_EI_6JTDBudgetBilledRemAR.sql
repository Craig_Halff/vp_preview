SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create FUNCTION [dbo].[fnCCG_EI_6JTDBudgetBilledRemAR] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), @Username varchar(20))
RETURNS @T TABLE
(
       BudgetValue              money,               /* Change @budget=PR.Fee + PR.ConsultFee + PR.ReimbAllow to whatever is desired for budget */
       BudgetDetail				varchar(100),
       BudgetRemValue           money,
       BudgetRemDetail          varchar(100),
       JTDBilledValue           money,
       JTDBilledDetail          varchar(100),
       AROver60Value			money,
       AROver60Detail           varchar(100),
       AROver90Value			money,
       AROver90Detail           varchar(100),
	   ARValue					money,
       ARDetail					varchar(100),
	   draftValue				money,
	   draftdetail				varchar(100),
	   percentvalue				decimal(19,4),
	   percentdetail			varchar(100)
)
AS BEGIN
       -- select * from dbo.fnCCG_EI_6JTDBilledWithAR('2003005.00',' ',' ','')
       declare @budget money, @res61 money, @res91 money, @total money, @rest money, @Percent decimal(19,2), @BudgetRem money, @draft money
	   declare @ardetail varchar(50), @brdetail varchar(50), @BudgetRemdetail varchar(50)
       declare @ThruPeriod  int, @ThruDate datetime

       select @ThruPeriod=ThruPeriod, @ThruDate=ThruDate
       from CCG_EI_ConfigInvoiceGroups cig
       inner join ProjectCustomTabFields pctf on pctf.custInvoiceGroup=cig.InvoiceGroup
       where pctf.WBS1=@WBS1 and pctf.WBS2=' '

       if @ThruPeriod is null     set @ThruPeriod=999999
       if @ThruDate is null set @ThruDate='12/31/2050'

	select @budget=IsNull(Fee + ConsultFee + ReimbAllow,0) from PR where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3

	select @draft  = colvalue from dbo.fnCCG_EI_DraftInvoiceAmt(@wbs1,@wbs2,@wbs3)
	
	--LEFT JOIN BTBGSubs on PR.WBS1 = BTBGSubs.SubWBS1 
	--left join PR as MAINPR on BTBGSubs.MainWBS1=MAINPR.WBS1 AND MAINPR.WBS2=' ' AND MAINPR.WBS3=' '
	--where PR.WBS1=@WBS1 and (isnull(@WBS2,' ')=' ' or PR.WBS2=@WBS2) and (isnull(@WBS3,' ')=' ' or PR.WBS3=@WBS3)
	
	select @rest=Sum(Case When DaysOld >= 0 Then InvoiceBalance Else 0 End), @res61=Sum(Case When DaysOld >= 61 Then InvoiceBalance Else 0 End), @res91=Sum(Case When DaysOld >= 91 Then InvoiceBalance Else 0 End), @total=Sum(amount)
	from (
		select IsNull(LedgerAR.Invoice, '') As Invoice, AR.InvoiceDate,
			Sum(case when LedgerAR.TransType='IN'and isnull(subtype,' ') not in ('I','R') and AutoEntry = 'N' and (CA.Type = 4 OR LedgerAR.Account IS NULL) then -ledgerar.amount else 0 end) as amount, 
			IsNull(PR.WBS1, '') As WBS1, LedgerAR.WBS2, LedgerAR.WBS3,
			Min(case when AR.InvoiceDate IS NULL then 0 else datediff(day,AR.InvoiceDate,GETDATE()) end) AS DaysOld, 
			Sum(case when LedgerAR.TransType='IN' then -Amount else case when LedgerAR.TransType='CR' and LedgerAR.SubType='T' then -Amount else Amount end end ) AS InvoiceBalance
		from LedgerAR LEFT JOIN BTBGSubs on LedgerAR.WBS1 = BTBGSubs.SubWBS1 
		LEFT JOIN CA on CA.Account=LedgerAR.Account
		left join PR as MAINPR on BTBGSubs.MainWBS1=MAINPR.WBS1 AND MAINPR.WBS2=' ' AND MAINPR.WBS3=' '
		, AR , PR
		where
			LedgerAR.WBS1 = PR.WBS1 AND PR.WBS2 = /*N*/' ' AND PR.WBS3 = /*N*/' ' 
			 AND LedgerAR.WBS1 = AR.WBS1 AND LedgerAR.WBS2 = AR.WBS2 AND LedgerAR.WBS3 = AR.WBS3 AND LedgerAR.Invoice = AR.Invoice 
			AND LedgerAR.AutoEntry = 'N' 
			AND ((LedgerAR.TransType = /*N*/'IN' AND (LedgerAR.SubType <> 'X' OR LedgerAR.SubType Is Null)) 
			OR (LedgerAR.TransType = /*N*/'CR' AND LedgerAR.SubType IN ('R','T'))) 
			 AND ((PR.WBS1 = /*N*/@WBS1) OR (PR.WBS1 = BTBGSubs.subwbs1 AND (BTBGSubs.MAINWBS1 = /*N*/@WBS1) ))
		group by IsNull(LedgerAR.Invoice, ''), IsNull(PR.WBS1, ''), LedgerAR.WBS2, LedgerAR.WBS3, AR.InvoiceDate,
			--case when Min(AR.InvoiceDate) IS NULL then 0 else datediff(day,Min(AR.InvoiceDate),GETDATE()) end, 
			case when LedgerAR.TransType=/*N*/'CR' AND LedgerAR.SubType='R' then 'R' 
			 when LedgerAR.SubType='R' then 'E' 
			 when LedgerAR.TransType =/*N*/'CR' AND LedgerAR.SubType='T' then 'T' else ' ' end
	) as AROver90 where WBS1=@WBS1 and (@WBS2=' ' or WBS2=@WBS2) and (@WBS3=' ' or WBS3=@WBS3)

	if @res91 > 90 Set @ardetail = 'BackgroundColor=#ffc0c0'
	else if @res61 > 60 set @ardetail = 'BackgroundColor=#ffff00'
	else set @ardetail = ''
	if @rest = 0 set @rest = null
	
	if isNull(@budget,0) > 0
	set @Percent = (isnull(@total,0)+isnull(@draft,0))/@budget
	else set @Percent = null

	set @BudgetRem = ISNULL(@budget,0) - ISNULL(isnull(@total,0)+isnull(@draft,0),0)
	if isnull(@BudgetRem,0) < 0
	   set @BudgetRemdetail = 'BackgroundColor=#ffc0c0'
	if @BudgetRem = 0 set @BudgetRem = null
	
	set @brdetail = ' '


       insert into @T values (isnull(@budget,null), null, isnull(@budgetrem,0), @BudgetRemdetail, isnull(isnull(@total,0)+isnull(@draft,0),null), null, @res61, null, @res91, null, @rest, @ardetail, @draft, null, @percent, null)
       return
END
GO
