SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetMessage] 
	(@msgId varchar(50), 
     @param1 nvarchar(max) = N' ', 
	 @param2 nvarchar(max) = N' ', 
	 @param3 nvarchar(max) = N' ',
	 @param4 nvarchar(max) = N' ',
	 @param5 nvarchar(max) = N' ',
	 @param6 nvarchar(max) = N' ',
	 @param7 nvarchar(max) = N' ',
	 @param8 nvarchar(max) = N' ',
	 @param9 nvarchar(max) = N' '
)
RETURNS nvarchar(max) AS
BEGIN
	DECLARE @messageText nvarchar(max) 

	SELECT @messageText = MessageText FROM SPMessages 
		WHERE MsgId = @msgId AND 
			  UICultureName = dbo.FW_GetActiveCultureName()
	
	if (@param1 != null or @param1 != N' ') 
		set @messageText = replace(@messageText,'{0}',@param1) 
	
	if (@param2 != null or @param2 != N' ') 
		set @messageText = replace(@messageText,'{1}',@param2) 
	
	if (@param3 != null or @param3 != N' ') 
		set @messageText = replace(@messageText,'{2}',@param3) 

	if (@param4 != null or @param4 != N' ') 
		set @messageText = replace(@messageText,'{3}',@param4) 

	if (@param5 != null or @param5 != N' ') 
		set @messageText = replace(@messageText,'{4}',@param5) 

	if (@param6 != null or @param6 != N' ') 
		set @messageText = replace(@messageText,'{5}',@param6) 

	if (@param7 != null or @param7 != N' ') 
		set @messageText = replace(@messageText,'{6}',@param7) 

	if (@param8 != null or @param8 != N' ') 
		set @messageText = replace(@messageText,'{7}',@param8) 

	if (@param9 != null or @param9 != N' ') 
		set @messageText = replace(@messageText,'{8}',@param9) 

	RETURN @messageText	
		
END 
GO
