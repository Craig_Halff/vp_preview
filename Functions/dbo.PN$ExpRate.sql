SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$ExpRate]
  (@strPlanID varchar(32),
   @strAccount Nvarchar(13)
  )
  RETURNS decimal(19,4)
BEGIN

  /*--------------------------------------------------------------------------------------------------------------------
    This function returns an Expense Billing Multiplier for a given Account in a given Plan.
  */--------------------------------------------------------------------------------------------------------------------

  DECLARE @strCalcExpBillAmtFlg varchar(1)

  DECLARE @sintExpBillRtMethod smallint
  DECLARE @intExpBillRtTableNo int
  DECLARE @decExpBillMultiplier decimal(19,4)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get settings from PNPlan.

  SELECT 
    @strCalcExpBillAmtFlg = P.CalcExpBillAmtFlg,
    @sintExpBillRtMethod = P.ExpBillRtMethod,
    @intExpBillRtTableNo = P.ExpBillRtTableNo,
    @decExpBillMultiplier = P.ExpBillMultiplier
    FROM PNPlan AS P 
    WHERE P.PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN(
    CASE 
      WHEN @strCalcExpBillAmtFlg = 'Y' 
      THEN dbo.PN$ExpConMult(@strAccount, @sintExpBillRtMethod, @intExpBillRtTableNo, @decExpBillMultiplier)
      ELSE 1.0000
    END
  )

END -- fn_PN$ExpRate
GO
