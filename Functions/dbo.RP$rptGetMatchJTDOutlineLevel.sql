SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$rptGetMatchJTDOutlineLevel]
 (@strPlanID varchar(32)) 
  RETURNS int  AS
BEGIN 

Return (SELECT MIN(rpTask.OutlineLevel) AS OutlineLevel  
             FROM RPTask 
             INNER JOIN RPWBSLevelFormat AS F ON rpTask.PlanID = F.PlanID AND rpTask.WBSType = F.WBSType AND F.WBSMatch = 'Y' 
             WHERE rpTask.planID = @strPlanID
             GROUP by rpTask.planID)

End

GO
