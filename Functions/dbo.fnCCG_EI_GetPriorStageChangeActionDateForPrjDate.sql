SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_GetPriorStageChangeActionDateForPrjDate] (@WBS1 Nvarchar(32), @ActionDate datetime)
RETURNS datetime AS
BEGIN
	declare @res datetime
	select @res = Max(ActionDate) from CCG_EI_History
	where WBS1 = @WBS1 and datediff(second,@ActionDate,ActionDate)<0 and ActionTaken='Stage Change'
	return @res
END
GO
