SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_JTDUnbilledBilling] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS money
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select dbo.fnCCG_EI_JTDUnbilledBilling('2003005.xx',' ',' ')
	select dbo.fnCCG_EI_JTDUnbilledBilling('2003005.xx','1PD',' ')
*/
	declare @res1 money, @res2 money
	declare @ThruPeriod	int, @ThruDate datetime

	select @ThruPeriod=ThruPeriod, @ThruDate=ThruDate
	from CCG_EI_ConfigInvoiceGroups cig
	inner join ProjectCustomTabFields pctf on pctf.custInvoiceGroup=cig.InvoiceGroup
	where pctf.WBS1=@WBS1 and pctf.WBS2=' '

	if @ThruPeriod is null	set @ThruPeriod=999999
	if @ThruDate is null	set @ThruDate='12/31/2050'

	select @res1=Sum(BillExt)
	from (
		select BillExt From LD
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus in ('B','H') and BillExt<>0     
			and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
	) as Labor
	
	select @res2=Sum(BillExt)
	from (
		select BillExt from LedgerMisc L
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus in ('B','H') and BillExt<>0    
			and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
		UNION ALL
		select BillExt from LedgerAP L
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus in ('B','H') and BillExt<>0    
			and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
		UNION ALL
		select BillExt from LedgerEX L
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus in ('B','H') and BillExt<>0   
			 and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
		UNION ALL
		select BillExt from LedgerAR L
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus in ('B','H') and BillExt<>0   
			and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
	) as Expenses
	set @res1 = IsNull(@res1,0) + IsNull(@res2,0)
	if @res1 = 0
		set @res1 = null
	return @res1
END
GO
