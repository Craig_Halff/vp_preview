SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   FUNCTION [dbo].[fnCCG_PAT_GetPriorStageChangeActionDateForPayable] (@PayableSeq int, @ActionDate datetime)
RETURNS datetime AS
BEGIN
	declare @res datetime
	select @res = Max(ActionDate) from CCG_PAT_History
	where PayableSeq = @PayableSeq and ActionDate < @ActionDate and ActionTaken=N'Stage Change'
	return @res
END
GO
