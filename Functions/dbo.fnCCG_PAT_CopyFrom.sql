SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_PAT_CopyFrom] (@Vendor varchar(20))RETURNS @T TABLE(	Vendor varchar(20),	ID	varchar(255),	GroupDescription varchar(255),	SortOrder int,	WBS1 varchar(30),	WBS2 varchar(7),	WBS3 varchar(7),	GLAccount varchar(13),	[Description] varchar(80),	SuppressBill char(1),	ExpenseCode varchar(10),	Amount decimal(19,5),	LiabCode varchar(10),	BankCode varchar(10),	PayTerms varchar(7),	[Address] varchar(20),	Company varchar(14)) AS BEGIN	insert into @T (Vendor,ID,GroupDescription,SortOrder,		WBS1,WBS2,WBS3,GLAccount,[Description],SuppressBill,ExpenseCode,Amount)	select EB.Vendor, 'EB|' + WBS1, WBS1 + ' (Budget)' ,1000,EB.WBS1,EB.WBS2,EB.WBS3,EB.Account,		null as [description],'N',null as ExpenseCode,EB.AmtBud from EB where Vendor = @Vendor		insert into @T (Vendor,ID,GroupDescription,SortOrder,		WBS1,WBS2,WBS3,GLAccount,[Description],SuppressBill,ExpenseCode,Amount,LiabCode,BankCode,PayTerms,[Address],Company)		select p.Vendor, 'PAT|' + convert(varchar(12),p.seq),p.PayableNumber + ' ' + convert(varchar(12),p.PayableDate,101) +  ' (Prior)',2000 + p.counter,pa.WBS1,pa.WBS2,pa.WBS3,pa.GLAccount,			pa.[Description],pa.SuppressBill,pa.ExpenseCode,pa.Amount,			p.LiabCode,p.BankCode,case when p.PayTerms <> 'DATE' then p.PayTerms else null end as PayTerms,p.[Address],p.Company		from CCG_PAT_ProjectAmount pa 		inner join (select Seq,PayableType,Stage,Vendor,PayableNumber,PayableDate,LiabCode,BankCode,PayTerms,[Address],Company,ROW_NUMBER() over(order by PayableDate desc) as [counter] 			from CCG_PAT_Payable where vendor = @Vendor and PayableType in ('I','D') 		) p on p.seq = pa.PayableSeq and p.counter <= 5 --modify max number of payables to return 		--don't import amount	update @T set Amount = 0		insert into @T (Vendor,ID,GroupDescription,SortOrder,
        WBS1,WBS2,WBS3,GLAccount,[Description],SuppressBill,ExpenseCode,Amount)
        SELECT [CustVendor] as Vendor,'PO|' + PO.[UDIC_UID], PO.CustPONumber,0,
        ISNULL(PO.CustProject,Items.CustProject), null,null,null,Items.[CustDescription],'N',null,Items.CustTotal
        FROM [UDIC_PurchaseOrder] PO
        LEFT JOIN [UDIC_PurchaseOrder_Items] Items on PO.UDIC_UID = Items.UDIC_UID
        where CustVendor = @Vendor and CustStatus  in ('Ordered','Approved')--TODO add appropriate 
	return END
GO
