SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$rptTaskJTDLabor]
  (@strPlanID varchar(32),
   @dtETCDate datetime,
   @strUnposted varchar(1) = 'N',
   @strIncludeUnmapped varchar(1) = 'Y',
   @sintGRMethod smallint = 0,
   @ReportAtBillingInBillingCurr varchar(1) = 'N')
  RETURNS @taskJTDLab TABLE
   (planID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    OutlineLevel int,
    StartDate datetime,
    JTDCostCurrencyCode Nvarchar(3) COLLATE database_default,
    JTDBillCurrencyCode Nvarchar(3) COLLATE database_default,    
    PeriodHrs decimal(19,4),
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4),
    PostedFlg smallint)

BEGIN --outer

 INSERT @taskJTDLab 
  SELECT PlanID,
	   TaskID,
	   Max(OutlineLevel) as OutlineLevel,
       StartDate, 
	   Max(JTDCostCurrencyCode) as JTDCostCurrencyCode,
       Max(JTDBillCurrencyCode) as JTDBillCurrencyCode,
	   PeriodHrs=ROUND(SUM(ISNULL(PeriodHrs, 0)), 2),    
	   PeriodCost=ROUND(SUM(ISNULL(PeriodCost, 0)), 2),
	   PeriodBill=ROUND(SUM(ISNULL(PeriodBill, 0)), 2),
	   PostedFlg=SIGN(MIN(X.PostedFlg) + MAX(X.PostedFlg))
  FROM (
    SELECT T.PlanID AS PlanID, 
	     T.TaskID AS TaskID,
	     Max(T.OutlineLevel) AS OutlineLevel,
	      LD.TransDate AS StartDate,
		 Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		 Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
                   SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs, 
                   SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost, 
                   SUM(BillExt) AS PeriodBill, 
                   1 AS PostedFlg
    FROM LD 
    INNER JOIN PR on PR.WBS1=LD.WBS1 and PR.WBS2='' and PR.WBS3=''
    INNER JOIN RPTask AS T ON LD.WBS1 = T.WBS1 
    INNER JOIN RPWBSLevelFormat AS F ON T.PlanID = F.PlanID AND T.WBSType = F.WBSType AND F.WBSMatch = 'Y' 
    WHERE T.PlanID = @strPlanID AND LD.ProjectCost = 'Y' 
                    AND LD.TransDate <= @dtETCDate
                    AND LD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
                    AND LD.WBS3 LIKE (ISNULL(T.WBS3, '%')) 
                    AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%')) 
    GROUP BY T.TaskID, T.PlanID,LD.TransDate,PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end
    UNION ALL SELECT T.PlanID AS PlanID, 
	     T.TaskID AS TaskID,
	     Max(T.OutlineLevel) AS OutlineLevel,
         TD.TransDate AS StartDate,
		 Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		 Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
                   SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs, 
                   SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost, 
                   SUM(BillExt) AS PeriodBill, 
                   -1 AS PostedFlg
    FROM tkDetail AS TD 
    INNER JOIN PR on PR.WBS1=TD.WBS1 and PR.WBS2='' and PR.WBS3=''
    INNER JOIN RPTask AS T ON TD.WBS1 = T.WBS1
    INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
    INNER JOIN RPWBSLevelFormat AS F ON T.PlanID = F.PlanID AND T.WBSType = F.WBSType AND F.WBSMatch = 'Y' 
    WHERE @strUnposted = 'Y' AND T.PlanID = @strPlanID 
                    AND TD.TransDate <= @dtETCDate
                    AND TD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
                    AND TD.WBS3 LIKE (ISNULL(T.WBS3, '%')) 
                    AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%')) 
    GROUP BY T.TaskID, T.PlanID,TD.TransDate,PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end
    UNION ALL SELECT T.PlanID AS PlanID, 
	     T.TaskID AS TaskID,
	     Max(T.OutlineLevel) AS OutlineLevel,
         TD.TransDate AS StartDate,
		 Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		 Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
                   SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs, 
                   SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost, 
                   SUM(BillExt) AS PeriodBill, 
                   -1 AS PostedFlg
    FROM tsDetail AS TD 
    INNER JOIN PR on PR.WBS1=TD.WBS1 and PR.WBS2='' and PR.WBS3=''
    INNER JOIN RPTask AS T ON TD.WBS1 = T.WBS1
    INNER JOIN tsControl AS TC ON (TC.Posted = 'N' AND TD.Batch = TC.Batch) 
    INNER JOIN RPWBSLevelFormat AS F ON T.PlanID = F.PlanID AND T.WBSType = F.WBSType AND F.WBSMatch = 'Y' 
    WHERE @strUnposted = 'Y' AND T.PlanID = @strPlanID 
                    AND TD.TransDate <= @dtETCDate
                    AND TD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
                    AND TD.WBS3 LIKE (ISNULL(T.WBS3, '%')) 
                    AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%')) 
    GROUP BY T.TaskID, T.PlanID, TD.TransDate,PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end
  ) X
  GROUP BY PlanID,TaskID, StartDate, JTDCostCurrencyCode, JTDBillCurrencyCode

  if @strIncludeUnmapped='Y'
  begin

  DECLARE @strTaskID varchar(32) 
  DECLARE @strOutlineNumber varchar(255) 
  DECLARE @intOutlineLevel int

  DECLARE csrTask CURSOR FOR 
    SELECT DISTINCT T.TaskID, T.OutlineNumber, T.OutlineLevel
    FROM RPTask AS T LEFT JOIN @taskJTDLab  AS TPD ON T.TaskID = TPD.TaskID 
    WHERE T.PlanID = @strPlanID  AND TPD.TaskID IS NULL
    ORDER BY T.OutlineNumber DESC 

  OPEN csrTask 

  FETCH NEXT FROM csrTask INTO @strTaskID, @strOutlineNumber, @intOutlineLevel

  WHILE (@@FETCH_STATUS = 0) 
    BEGIN --while begin

  INSERT @taskJTDLab 
    SELECT @strPlanID AS PlanID, 
	     @strTaskID AS TaskID, 
      	 @intOutlineLevel AS OutlineLevel,
	     StartDate, 	
	     Max(JTDCostCurrencyCode) as JTDCostCurrencyCode,
         Max(JTDBillCurrencyCode) as JTDBillCurrencyCode,
         ROUND(SUM(ISNULL(PeriodHrs, 0)), 2) AS PeriodHrs, 
         ROUND(SUM(ISNULL(PeriodCost, 0)), 2) AS PeriodCost, 
         ROUND(SUM(ISNULL(PeriodBill, 0)), 2) AS PeriodBill, 
         SIGN(MIN(ISNULL(PostedFlg, 0)) + MAX(ISNULL(PostedFlg, 0))) AS PostedFlg 
    FROM 
      (SELECT @strTaskID AS TaskID, 
                      TPD.StartDate,
	                  Max(TPD.JTDCostCurrencyCode) as JTDCostCurrencyCode,
                      Max(TPD.JTDBillCurrencyCode) as JTDBillCurrencyCode,
                      ROUND(SUM(ISNULL(TPD.PeriodHrs, 0)), 2) AS PeriodHrs, 
                      ROUND(SUM(ISNULL(TPD.PeriodCost, 0)), 2) AS PeriodCost, 
                      ROUND(SUM(ISNULL(TPD.PeriodBill, 0)), 2) AS PeriodBill, 
                      SIGN(MIN(ISNULL(TPD.PostedFlg, 0)) + MAX(ISNULL(TPD.PostedFlg, 0))) AS PostedFlg 
      FROM RPTask AS CT 
      INNER JOIN @taskJTDLab AS TPD ON TPD.TaskID = CT.TaskID 
      WHERE CT.PlanID = @strPlanID AND CT.ParentOutlineNumber = @strOutlineNumber
      GROUP BY TPD.StartDate, TPD.JTDCostCurrencyCode, TPD.JTDBillCurrencyCode
      UNION ALL
      SELECT @strTaskID AS TaskID, 
	        TransDate AS StartDate,	
	                 Max(JTDCostCurrencyCode) as JTDCostCurrencyCode,
                     Max(JTDBillCurrencyCode) as JTDBillCurrencyCode,
                     ROUND(SUM(ISNULL(PeriodHrs, 0)), 2) AS PeriodHrs, 
                     ROUND(SUM(ISNULL(PeriodCost, 0)), 2) AS PeriodCost, 
                     ROUND(SUM(ISNULL(PeriodBill, 0)), 2) AS PeriodBill, 
                     SIGN(MIN(ISNULL(PostedFlg, 0)) + MAX(ISNULL(PostedFlg, 0))) AS PostedFlg 
      FROM dbo.RP$rptAssignJTD(@strPlanID,@dtETCDate,@strTaskID,@strUnposted,@sintGRMethod,'N',@ReportAtBillingInBillingCurr)
      GROUP BY TransDate, JTDCostCurrencyCode, JTDBillCurrencyCode
      ) AS XTPD 
    GROUP BY StartDate, JTDCostCurrencyCode, JTDBillCurrencyCode

      
      FETCH NEXT FROM csrTask INTO @strTaskID, @strOutlineNumber,@intOutlineLevel
    END  --while end
  
  CLOSE csrTask
  DEALLOCATE csrTask
  END -- if @strIncludeUnmapped='Y' begin end

RETURN
END --outer

GO
