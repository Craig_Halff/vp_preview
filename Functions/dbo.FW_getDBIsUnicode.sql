SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[FW_getDBIsUnicode]() RETURNS varchar(1)	
AS
BEGIN
   DECLARE @retIsUnicode AS varchar(1)

   SET @retIsUnicode = 'Y'

   RETURN (@retIsUnicode)
END -- FW_getDBIsUnicode

GO
