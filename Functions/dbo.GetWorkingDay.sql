SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetWorkingDay]
  (@dtStartDate AS datetime, @startOrEnd as int, @strCompany AS Nvarchar(14))
RETURNS datetime
BEGIN

		DECLARE @flNumWorkingDays as float
		DECLARE	@dtdate as datetime

		SET @dtdate = @dtStartDate
		SET @flNumWorkingDays = ABS(dbo.DLTK$NumWorkingDays(@dtStartDate, @dtStartDate, @strCompany))

		IF (@flNumWorkingDays = 0) 
			SET @dtdate = dbo.GetWorkingDay(DATEADD(d, @startOrEnd, @dtStartDate), @startOrEnd, @strCompany)

RETURN  @dtdate

END -- fn_GetWorkingDay
GO
