SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create FUNCTION [dbo].[fnCCG_PAT_LinkedPO] (@PayableSeq int, @WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS varchar(25)
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.
	select * from ve

*/
    declare @res varchar(25)

	SELECT 
		@res = PONumber + ' Linked'
		FROM CCG_PAT_Payable PAT
		WHERE pat.Seq = @PayableSeq 
	return @res
END
GO
