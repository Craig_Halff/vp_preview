SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$DiffPlanned] (
  @strPlanID varchar(32)
)
  RETURNS bit
BEGIN -- Function stRP$DiffPlanned

  DECLARE @bitDiff bit

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @bitDiff = CASE WHEN SUM(DX) > 0 THEN 1 ELSE 0 END FROM ( /* Z */

    SELECT CASE WHEN EXISTS ( /* DX */
      SELECT MIN(VorN) AS VorN FROM ( /* ZP */
        SELECT 
          0 AS VorN,
          PlanName, PlanNumber, ClientID, ProjMgr, Principal, Supervisor, Org, WBS1, StartDate, EndDate, ExpWBSLevel, ConWBSLevel,
          BudgetType, CostRtMethod, BillingRtMethod, CostRtTableNo, BillingRtTableNo, GRMethod, CostGRRtMethod, BillGRRtMethod, GenResTableNo, GRBillTableNo,
          CalcExpBillAmtFlg, CalcConBillAmtFlg, ExpBillRtMethod, ExpBillRtTableNo, ConBillRtMethod, ConBillRtTableNo, ExpBillMultiplier, ConBillMultiplier,
          StartingDayOfWeek, Probability, LabMultType, Multiplier, LabBillMultiplier, PctCompleteFormula, RevenueMethod, ReimbMethod, 
          Company, CostCurrencyCode, BillingCurrencyCode, EVFormula, PctComplByPeriodFlg, TargetMultCost, TargetMultBill, VersionID,
          Status, UtilizationIncludeFlg, AvailableFlg, UnPostedFlg, CommitmentFlg, ContingencyPct, ContingencyAmt, OverheadPct, ProjectedMultiplier, ProjectedRatio,
          CompensationFee, ConsultantFee, ReimbAllowance, CompensationFeeBill, ConsultantFeeBill, ReimbAllowanceBill,
          CompensationFeeDirLab, CompensationFeeDirLabBill, CompensationFeeDirExp, CompensationFeeDirExpBill,
          ReimbAllowanceExp, ReimbAllowanceExpBill, ReimbAllowanceCon, ReimbAllowanceConBill
          FROM PNPlan AS N WHERE PlanID = @strPlanID
        UNION ALL
        SELECT
          1 AS VorN,
          PlanName, PlanNumber, ClientID, ProjMgr, Principal, Supervisor, Org, WBS1, StartDate, EndDate, ExpWBSLevel, ConWBSLevel,
          BudgetType, CostRtMethod, BillingRtMethod, CostRtTableNo, BillingRtTableNo, GRMethod, CostGRRtMethod, BillGRRtMethod, GenResTableNo, GRBillTableNo,
          CalcExpBillAmtFlg, CalcConBillAmtFlg, ExpBillRtMethod, ExpBillRtTableNo, ConBillRtMethod, ConBillRtTableNo, ExpBillMultiplier, ConBillMultiplier,
          StartingDayOfWeek, Probability, LabMultType, Multiplier, LabBillMultiplier, PctCompleteFormula, RevenueMethod, ReimbMethod, 
          Company, CostCurrencyCode, BillingCurrencyCode, EVFormula, PctComplByPeriodFlg, TargetMultCost, TargetMultBill, VersionID,
          Status, UtilizationIncludeFlg, AvailableFlg, UnPostedFlg, CommitmentFlg, ContingencyPct, ContingencyAmt, OverheadPct, ProjectedMultiplier, ProjectedRatio,
          CompensationFee, ConsultantFee, ReimbAllowance, CompensationFeeBill, ConsultantFeeBill, ReimbAllowanceBill,
          CompensationFeeDirLab, CompensationFeeDirLabBill, CompensationFeeDirExp, CompensationFeeDirExpBill,
          ReimbAllowanceExp, ReimbAllowanceExpBill, ReimbAllowanceCon, ReimbAllowanceConBill
          FROM RPPlan AS V WHERE PlanID = @strPlanID
        ) AS ZP
        GROUP BY
          PlanName, PlanNumber, ClientID, ProjMgr, Principal, Supervisor, Org, WBS1, StartDate, EndDate, ExpWBSLevel, ConWBSLevel,
          BudgetType, CostRtMethod, BillingRtMethod, CostRtTableNo, BillingRtTableNo, GRMethod, CostGRRtMethod, BillGRRtMethod, GenResTableNo, GRBillTableNo,
          CalcExpBillAmtFlg, CalcConBillAmtFlg, ExpBillRtMethod, ExpBillRtTableNo, ConBillRtMethod, ConBillRtTableNo, ExpBillMultiplier, ConBillMultiplier,
          StartingDayOfWeek, Probability, LabMultType, Multiplier, LabBillMultiplier, PctCompleteFormula, RevenueMethod, ReimbMethod, 
          Company, CostCurrencyCode, BillingCurrencyCode, EVFormula, PctComplByPeriodFlg, TargetMultCost, TargetMultBill, VersionID,
          Status, UtilizationIncludeFlg, AvailableFlg, UnPostedFlg, CommitmentFlg, ContingencyPct, ContingencyAmt, OverheadPct, ProjectedMultiplier, ProjectedRatio,
          CompensationFee, ConsultantFee, ReimbAllowance, CompensationFeeBill, ConsultantFeeBill, ReimbAllowanceBill,
          CompensationFeeDirLab, CompensationFeeDirLabBill, CompensationFeeDirExp, CompensationFeeDirExpBill,
          ReimbAllowanceExp, ReimbAllowanceExpBill, ReimbAllowanceCon, ReimbAllowanceConBill
        HAVING COUNT(*) = 1
    ) THEN 1 ELSE 0 END AS DX

    UNION ALL /* Task */
    SELECT CASE WHEN EXISTS ( /* DX */
      SELECT MIN(VorN) AS VorN FROM ( /* ZT */
        SELECT 
          0 AS VorN,
          Name, WBS1, WBS2, WBS3, LaborCode, WBSType, ParentOutlineNumber, OutlineNumber, ChildrenCount, OutlineLevel, StartDate, EndDate, CostRate, BillingRate, ExpBillRate, ConBillRate,
          CompensationFee, ConsultantFee, ReimbAllowance, CompensationFeeBill, ConsultantFeeBill, ReimbAllowanceBill,
          CompensationFeeDirLab, CompensationFeeDirLabBill, CompensationFeeDirExp, CompensationFeeDirExpBill,
          ReimbAllowanceExp, ReimbAllowanceExpBill, ReimbAllowanceCon, ReimbAllowanceConBill,
          ProjMgr, ChargeType, ProjectType, ClientID, Status, Org, Notes
          FROM PNTask AS N WHERE PlanID = @strPlanID
        UNION ALL
        SELECT
          1 AS VorN,
          Name, WBS1, WBS2, WBS3, LaborCode, WBSType, ParentOutlineNumber, OutlineNumber, ChildrenCount, OutlineLevel, StartDate, EndDate, CostRate, BillingRate, ExpBillRate, ConBillRate,
          CompensationFee, ConsultantFee, ReimbAllowance, CompensationFeeBill, ConsultantFeeBill, ReimbAllowanceBill,
          CompensationFeeDirLab, CompensationFeeDirLabBill, CompensationFeeDirExp, CompensationFeeDirExpBill,
          ReimbAllowanceExp, ReimbAllowanceExpBill, ReimbAllowanceCon, ReimbAllowanceConBill,
          ProjMgr, ChargeType, ProjectType, ClientID, Status, Org, Notes
          FROM RPTask AS V WHERE PlanID = @strPlanID
        ) AS ZT
        GROUP BY
          Name, WBS1, WBS2, WBS3, LaborCode, WBSType, ParentOutlineNumber, OutlineNumber, ChildrenCount, OutlineLevel, StartDate, EndDate, CostRate, BillingRate, ExpBillRate, ConBillRate,
          CompensationFee, ConsultantFee, ReimbAllowance, CompensationFeeBill, ConsultantFeeBill, ReimbAllowanceBill,
          CompensationFeeDirLab, CompensationFeeDirLabBill, CompensationFeeDirExp, CompensationFeeDirExpBill,
          ReimbAllowanceExp, ReimbAllowanceExpBill, ReimbAllowanceCon, ReimbAllowanceConBill,
          ProjMgr, ChargeType, ProjectType, ClientID, Status, Org, Notes
        HAVING COUNT(*) = 1
      ) THEN 1 ELSE 0 END AS DX

    UNION ALL /* Assignment */
    SELECT CASE WHEN EXISTS ( /* DX */
      SELECT MIN(VorN) AS VorN FROM ( /* ZA */
        SELECT 
          0 AS VorN,
          TaskID, WBS1, WBS2, WBS3, LaborCode, ResourceID, GenericResourceID, Category, GRLBCD, ActivityID, StartDate, EndDate,
          CostRate, BillingRate, HardBooked, SortSeq
          FROM PNAssignment AS N WHERE PlanID = @strPlanID
        UNION ALL
        SELECT
          1 AS VorN,
          TaskID, WBS1, WBS2, WBS3, LaborCode, ResourceID, GenericResourceID, Category, GRLBCD, ActivityID, StartDate, EndDate,
          CostRate, BillingRate, HardBooked, SortSeq
          FROM RPAssignment AS V WHERE PlanID = @strPlanID
        ) AS ZA
        GROUP BY
          TaskID, WBS1, WBS2, WBS3, LaborCode, ResourceID, GenericResourceID, Category, GRLBCD, ActivityID, StartDate, EndDate,
          CostRate, BillingRate, HardBooked, SortSeq
        HAVING COUNT(*) = 1
      ) THEN 1 ELSE 0 END AS DX

    UNION ALL /* Expense */
    SELECT CASE WHEN EXISTS ( /* DX */
      SELECT MIN(VorN) AS VorN FROM ( /* ZE */
        SELECT 
          0 AS VorN,
          TaskID, WBS1, WBS2, WBS3, Account, Vendor, StartDate, EndDate, DirectAcctFlg, ExpBillRate, SortSeq
          FROM PNExpense AS N WHERE PlanID = @strPlanID
        UNION ALL
        SELECT
          1 AS VorN,
          TaskID, WBS1, WBS2, WBS3, Account, Vendor, StartDate, EndDate, DirectAcctFlg, ExpBillRate, SortSeq
          FROM RPExpense AS V WHERE PlanID = @strPlanID
        ) AS ZE
        GROUP BY
          TaskID, WBS1, WBS2, WBS3, Account, Vendor, StartDate, EndDate, DirectAcctFlg, ExpBillRate, SortSeq
        HAVING COUNT(*) = 1
      ) THEN 1 ELSE 0 END AS DX

    UNION ALL /* Consultant */
    SELECT CASE WHEN EXISTS ( /* DX */
      SELECT MIN(VorN) AS VorN FROM ( /* ZC */
        SELECT 
          0 AS VorN,
          TaskID, WBS1, WBS2, WBS3, Account, Vendor, StartDate, EndDate, DirectAcctFlg, ConBillRate, SortSeq
          FROM PNConsultant AS N WHERE PlanID = @strPlanID
        UNION ALL
        SELECT
          1 AS VorN,
          TaskID, WBS1, WBS2, WBS3, Account, Vendor, StartDate, EndDate, DirectAcctFlg, ConBillRate, SortSeq
          FROM RPConsultant AS V WHERE PlanID = @strPlanID
        ) AS ZC
        GROUP BY
          TaskID, WBS1, WBS2, WBS3, Account, Vendor, StartDate, EndDate, DirectAcctFlg, ConBillRate, SortSeq
        HAVING COUNT(*) = 1
      ) THEN 1 ELSE 0 END AS DX

    UNION ALL /* Labor TPD */
    SELECT CASE WHEN EXISTS ( /* DX */
      SELECT MIN(VorN) AS VorN FROM ( /* LTPD */
        SELECT 
          0 AS VorN,
          TaskID, AssignmentID, StartDate, EndDate, PeriodHrs, CostRate, BillingRate
          FROM PNPlannedLabor AS N WHERE PlanID = @strPlanID
        UNION ALL
        SELECT
          1 AS VorN,
          TaskID, AssignmentID, StartDate, EndDate, PeriodHrs, CostRate, BillingRate
          FROM RPPlannedLabor AS V WHERE PlanID = @strPlanID
        ) AS LTPD
        GROUP BY
          TaskID, AssignmentID, StartDate, EndDate, PeriodHrs, CostRate, BillingRate
        HAVING COUNT(*) = 1
      ) THEN 1 ELSE 0 END AS DX

    UNION ALL /* Expense TPD */
    SELECT CASE WHEN EXISTS ( /* DX */
      SELECT MIN(VorN) AS VorN FROM ( /* ETPD */
        SELECT 
          0 AS VorN,
          TaskID, ExpenseID, StartDate, EndDate, PeriodCost, PeriodBill
          FROM PNPlannedExpenses AS N WHERE PlanID = @strPlanID
        UNION ALL
        SELECT
          1 AS VorN,
          TaskID, ExpenseID, StartDate, EndDate, PeriodCost, PeriodBill
          FROM RPPlannedExpenses AS V WHERE PlanID = @strPlanID
        ) AS ETPD
        GROUP BY
          TaskID, ExpenseID, StartDate, EndDate, PeriodCost, PeriodBill
        HAVING COUNT(*) = 1
      ) THEN 1 ELSE 0 END AS DX

    UNION ALL /* Consultant TPD */
    SELECT CASE WHEN EXISTS ( /* DX */
      SELECT MIN(VorN) AS VorN FROM ( /* CTPD */
        SELECT 
            0 AS VorN,
            TaskID, ConsultantID, StartDate, EndDate, PeriodCost, PeriodBill
            FROM PNPlannedConsultant AS N WHERE PlanID = @strPlanID
          UNION ALL
          SELECT
            1 AS VorN,
            TaskID, ConsultantID, StartDate, EndDate, PeriodCost, PeriodBill
            FROM RPPlannedConsultant AS V WHERE PlanID = @strPlanID
          ) AS CTPD
          GROUP BY
            TaskID, ConsultantID, StartDate, EndDate, PeriodCost, PeriodBill
          HAVING COUNT(*) = 1
      ) THEN 1 ELSE 0 END AS DX

  ) AS Z

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN @bitDiff

END -- stRP$DiffPlanned
GO
