SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EIUDC_GetE] (@s varchar(max))
RETURNS varchar(max)
WITH ENCRYPTION
BEGIN
	return Convert(varchar(max),EncryptByPassPhrase('CCG_EIUDC_7150', @s),2)
END
GO
