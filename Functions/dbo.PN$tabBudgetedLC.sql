SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$tabBudgetedLC](
  @strWBS1 Nvarchar(30),
  @strWBS2 Nvarchar(7),
  @strWBS3 Nvarchar(7),
  @siLCLevel smallint,
  @strSelLC1 Nvarchar(14) = '', 
  @strSelLC2 Nvarchar(14) = '', 
  @strSelLC3 Nvarchar(14) = '', 
  @strSelLC4 Nvarchar(14) = '',
  @strStartDate varchar(8) = NULL, -- Date must be in format: 'yyyymmdd' regardless of UI Culture.
  @strEndDate varchar(8) = NULL, -- Date must be in format: 'yyyymmdd' regardless of UI Culture.
  @strEmployee Nvarchar(20) = NULL
) 

  RETURNS @tabBudgetedLC TABLE(
    Code Nvarchar(14) COLLATE database_default,
    LCLevel smallint,
    Label Nvarchar(MAX) COLLATE database_default
  )

BEGIN

  DECLARE @dtStartDate datetime
  DECLARE @dtEndDate datetime

  DECLARE @bLevelIsLimited bit

  DECLARE @bBudgetedLevel1 bit
  DECLARE @bBudgetedLevel2 bit
  DECLARE @bBudgetedLevel3 bit
  DECLARE @bBudgetedLevel4 bit
  DECLARE @bBudgetedLevel5 bit

  DECLARE @strBudgetSource varchar(1)
  DECLARE @strBudgetedFlag varchar(1)
  DECLARE @strBudgetLevel varchar(1)
  DECLARE @strBudgetedLevels varchar(5)
  DECLARE @strTKCheckRPDate varchar(1)
  DECLARE @strLCDelimiter varchar(1)
  DECLARE @strSelectedLC Nvarchar(14)

  DECLARE @siLCLevels smallint
  DECLARE @siLCStart smallint
  DECLARE @siLCLength smallint

  DECLARE @siLCLength1 smallint
  DECLARE @siLCLength2 smallint
  DECLARE @siLCLength3 smallint
  DECLARE @siLCLength4 smallint
  DECLARE @siLCLength5 smallint

  -- Declare Temp tables.

  DECLARE @tabSourceLC TABLE
    (Code Nvarchar(14) COLLATE database_default
     UNIQUE(Code))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get LCDelimiter and other LC formats.
  
  SELECT
    @siLCLevels = LCLevels,
    @strLCDelimiter = LCDelimiter,
    @siLCStart =
      CASE @siLCLevel
        WHEN 1 THEN LC1Start
        WHEN 2 THEN LC2Start
        WHEN 3 THEN LC3Start
        WHEN 4 THEN LC4Start
        WHEN 5 THEN LC5Start
      END,
    @siLCLength =
      CASE @siLCLevel
        WHEN 1 THEN LC1Length
        WHEN 2 THEN LC2Length
        WHEN 3 THEN LC3Length
        WHEN 4 THEN LC4Length
        WHEN 5 THEN LC5Length
      END,
    @siLCLength1 = LC1Length,
    @siLCLength2 = LC2Length,
    @siLCLength3 = LC3Length,
    @siLCLength4 = LC4Length,
    @siLCLength5 = LC5Length
    FROM CFGFormat

  -- Get BudgetSource from a specific WBS row.

  SELECT
    @strBudgetSource = BudgetSource,
    @strBudgetedFlag = BudgetedFlag,
    @strBudgetLevel = BudgetLevel,
    @strBudgetedLevels = BudgetedLevels,
    @strTKCheckRPDate = TKCheckRPDate,

    @bLevelIsLimited = 
      CASE 
        WHEN (BudgetedLevels IS NOT NULL) AND (CHARINDEX(CONVERT(VARCHAR, @siLCLevel), BudgetedLevels) <> 0)
        THEN 1
        ELSE 0
      END,

    @bBudgetedLevel1 =
      CASE 
        WHEN (BudgetedLevels IS NOT NULL) AND (CHARINDEX('1', BudgetedLevels) > 0)
        THEN 1
        ELSE 0
      END,

    @bBudgetedLevel2 =
      CASE 
        WHEN (BudgetedLevels IS NOT NULL) AND (CHARINDEX('2', BudgetedLevels) > 0)
        THEN 1
        ELSE 0
      END,

    @bBudgetedLevel3 =
      CASE 
        WHEN (BudgetedLevels IS NOT NULL) AND (CHARINDEX('3', BudgetedLevels) > 0)
        THEN 1
        ELSE 0
      END,

    @bBudgetedLevel4 =
      CASE 
        WHEN (BudgetedLevels IS NOT NULL) AND (CHARINDEX('4', BudgetedLevels) > 0)
        THEN 1
        ELSE 0
      END,

    @bBudgetedLevel5 =
      CASE 
        WHEN (BudgetedLevels IS NOT NULL) AND (CHARINDEX('5', BudgetedLevels) > 0)
        THEN 1
        ELSE 0
      END

    FROM PR
    WHERE WBS1 = @strWBS1 AND WBS2 = @strWBS2 AND WBS3 = @strWBS3

  -- Set Start/End Dates.
  
  SET @dtStartDate = CONVERT(datetime, @strStartDate)
  SET @dtEndDate = CONVERT(datetime, @strEndDate)

  -- Concatenate selected LC levels.
  -- When a LC Level is not budgeted, then the level will be wildcarded.

  SET @strSelectedLC = 

    CASE WHEN @bBudgetedLevel1 = 0
      THEN REPLICATE('_', @siLCLength1)
      ELSE ISNULL(@strSelLC1, '')
    END + 
 
    CASE WHEN @bBudgetedLevel2 = 0
      THEN 
	    CASE WHEN (@siLCLevel >= 2 AND @siLCLevel <= @siLCLevels) 
		  THEN ISNULL((ISNULL(@strLCDelimiter, '') + REPLICATE('_', @siLCLength2)), '')
          ELSE ''
        END
      ELSE 
        CASE WHEN (@siLCLevel >= 2 AND @siLCLevel <= @siLCLevels) 
          THEN ISNULL((ISNULL(@strLCDelimiter, '') + @strSelLC2), '') 
          ELSE ''
        END
    END + 

    CASE WHEN @bBudgetedLevel3 = 0
      THEN 
	    CASE WHEN (@siLCLevel >= 3 AND @siLCLevel <= @siLCLevels) 
		  THEN ISNULL((ISNULL(@strLCDelimiter, '') + REPLICATE('_', @siLCLength3)), '')
          ELSE ''
        END
      ELSE 
        CASE WHEN (@siLCLevel >= 3 AND @siLCLevel <= @siLCLevels) 
          THEN ISNULL((ISNULL(@strLCDelimiter, '') + @strSelLC3), '') 
          ELSE ''
        END
    END + 
 
    CASE WHEN @bBudgetedLevel4 = 0
      THEN 
 	    CASE WHEN (@siLCLevel >= 4 AND @siLCLevel <= @siLCLevels) 
		  THEN ISNULL((ISNULL(@strLCDelimiter, '') + REPLICATE('_', @siLCLength4)), '')
          ELSE ''
        END
     ELSE 
        CASE WHEN (@siLCLevel >= 4 AND @siLCLevel <= @siLCLevels) 
          THEN ISNULL((ISNULL(@strLCDelimiter, '') + @strSelLC4), '') 
          ELSE ''
        END
    END 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strBudgetedFlag <> 'N' AND @bLevelIsLimited = 1)
    BEGIN

      IF (@strBudgetSource = 'P')
        BEGIN
			--12/02/2019 By Dennis P: The query should always be from RPAssignment table instead RPTask.
			--It should check RPPLannedLabor for PeriodHrs instead of RPAssignment for PlannedLabCost and PlannedLabBill
              INSERT @tabSourceLC(
                Code
              )
                SELECT DISTINCT
                  ISNULL(SUBSTRING(LBS.LaborCode, @siLCStart, @siLCLength), REPLICATE('_', @siLCLength)) AS Code
                  FROM RPAssignment AS LBS
				    INNER JOIN RPTask on RPTask.PlanID = LBS.PlanID and RPTask.TaskID = LBS.TaskID
                    INNER JOIN RPPlan AS P ON LBS.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND 
                      LBS.WBS1 = @strWBS1 AND ISNULL(LBS.WBS2, ' ') = @strWBS2 AND ISNULL(LBS.WBS3, ' ') = @strWBS3
                  WHERE
					RPTask.WBSType = 'LBCD' AND 
                    (@strBudgetLevel = 'L' or LBS.ResourceID = @strEmployee) AND
                    LBS.LaborCode LIKE @strSelectedLC + '%' AND
                    ISNULL(LBS.LaborCode, '') <> '<none>' AND
                   ((@strTKCheckRPDate = 'N' AND Exists(
						select RPPL.PeriodHrs From RPPlannedLabor as RPPL 
						Where RPPL.PlanID =LBS.PlanID AND RPPL.TaskID =LBS.TaskID AND RPPL.AssignmentID =LBS.AssignmentID
						and RPPL.PeriodHrs > 0
						)   
					 ) 
					OR
                     (@strTKCheckRPDate = 'Y' AND (LBS.EndDate >= @dtStartDate AND LBS.StartDate <= @dtEndDate) AND Exists(
						select RPPL.PeriodHrs From RPPlannedLabor as RPPL 
						Where RPPL.PlanID =LBS.PlanID AND RPPL.TaskID =LBS.TaskID AND RPPL.AssignmentID =LBS.AssignmentID
						and RPPL.PeriodHrs > 0 AND (RPPL.EndDate >= @dtStartDate AND RPPL.StartDate <= @dtEndDate)
						)   
					 )
                    )

        END /* END IF (@strBudgetSource = 'P') */
      ELSE
        BEGIN

          INSERT @tabSourceLC(
            Code
          )
            SELECT DISTINCT
              SUBSTRING(LBS.LaborCode, @siLCStart, @siLCLength) AS Code
              FROM LB AS LBS
              WHERE 
                (LBS.HrsBud <> 0 OR LBS.AmtBud <> 0 OR LBS.EtcHrs <> 0 OR LBS.EtcAmt <> 0) AND 
                (LBS.WBS1 = @strWBS1 AND LBS.WBS2 = @strWBS2 AND LBS.WBS3 = @strWBS3) AND 
                LBS.LaborCode LIKE @strSelectedLC + '%'

        END /* END ELSE (@strBudgetSource <> 'P') */

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Need to eliminate unbudgeted labor code before returning result.

      INSERT @tabBudgetedLC(
        Code,
        LCLevel,
        Label
      )
        SELECT DISTINCT
          LC.Code,
          LC.LCLevel,
          LC.Label
          FROM CFGLCCodes AS LC
            INNER JOIN @tabSourceLC AS BLC ON LC.Code LIKE BLC.Code
          WHERE LC.LCLevel = @siLCLevel
          ORDER BY LC.Code

    END /* END IF (@strBudgetedFlag <> 'N' AND @bLevelIsLimited = 1) */
  ELSE
    BEGIN

      -- There was no restriction for budgeted Labor Code.
      -- Therefore, return all entries from CFGLCCodes for the requested level.
      
      INSERT @tabBudgetedLC(
        Code,
        LCLevel,
        Label
      )
        SELECT DISTINCT
          LC.Code,
          LC.LCLevel,
          LC.Label
          FROM CFGLCCodes AS LC
          WHERE LC.LCLevel = @siLCLevel
          ORDER BY LC.Code

    END /* END ESLE (@strBudgetedFlag = 'N' OR @bLevelIsLimited = 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
