SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   FUNCTION [dbo].[fnCCG_StripHtml](@htmlString Nvarchar(max))
RETURNS Nvarchar(max)
AS BEGIN
	/* Copyright (c) 2019 EleVia Software and Central Consulting Group.  All rights reserved. */
	/*
		select dbo.[fnCCG_StripHtml]('<html><body>Hello World &lt;tag&gt;, <font size="3">Random text &amp;!</font></body></html>')
	*/
	declare @i int, @Start int, @End int, @Length int
	DECLARE @trimchars VARCHAR(10)
	-- SET @trimchars = CHAR(9)+CHAR(10)+CHAR(13)+CHAR(32)
	
	set @htmlString = dbo.fnCCG_ReplaceHtmlSpecialChar(@htmlString, '<br>', char(13)+char(10))--char(13)+char(10))
	set @htmlString = dbo.fnCCG_ReplaceHtmlSpecialChar(@htmlString, '<br/>', char(13)+char(10))--char(13)+char(10))
	
	set @i = patindex('%<%>%', @htmlString)
	while @i > 0
	begin
		set @htmlString = stuff(@htmlString, @i, charindex('>', @htmlString, @i) - @i + 1, '')
		set @i = patindex('%<%>%', @htmlString)
	end
	
	SET @htmlString = REPLACE(@htmlString, CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10), CHAR(13)+CHAR(10))
    SET @htmlString = LEFT(@htmlString,LEN(@htmlString)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',REVERSE(@htmlString))+1)
	SET @htmlString = REPLACE(@htmlString, CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10), CHAR(13)+CHAR(10))
	SET @htmlString = RIGHT(@htmlString,LEN(@htmlString)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',@htmlString)+1)

	set @htmlString = dbo.fnCCG_ReplaceHtmlSpecialChar(@htmlString, '&nbsp;', ' ')
	set @htmlString = dbo.fnCCG_ReplaceHtmlSpecialChar(@htmlString, '&amp;',  '&')
	set @htmlString = dbo.fnCCG_ReplaceHtmlSpecialChar(@htmlString, '&#x0D;',   '')
	set @htmlString = dbo.fnCCG_ReplaceHtmlSpecialChar(@htmlString, 'edsp;',   ' ')
	set @htmlString = dbo.fnCCG_ReplaceHtmlSpecialChar(@htmlString, '&lt;',   '<')
	set @htmlString = dbo.fnCCG_ReplaceHtmlSpecialChar(@htmlString, '&gt;',   '>')
	set @htmlString = dbo.fnCCG_ReplaceHtmlSpecialChar(@htmlString, '&quot;', '"')
	set @htmlString = dbo.fnCCG_ReplaceHtmlSpecialChar(@htmlString, '&frasl;','/')
	set @htmlString = dbo.fnCCG_ReplaceHtmlSpecialChar(@htmlString, '&copy;', '@')

	return @htmlString
END
GO
