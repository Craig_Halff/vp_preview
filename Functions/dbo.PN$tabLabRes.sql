SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$tabLabRes]
  (@strWBS1 Nvarchar(30),
   @strWBS2 Nvarchar(7),
   @strWBS3 Nvarchar(7),
   @strJTDDate VARCHAR(8)) -- Date must be in format: 'yyyymmdd' regardless of UI Culture.

  RETURNS @tabLabRes TABLE
    (AssignmentID varchar(32) COLLATE database_default,
     Name Nvarchar(255) COLLATE database_default,
     SortName Nvarchar(255) COLLATE database_default,
     Employee Nvarchar(20) COLLATE database_default,
     GenericResourceID Nvarchar(20) COLLATE database_default,
     Category smallint,
     GRLBCD Nvarchar(14) COLLATE database_default,
     CostRate decimal(19,4),
     BillingRate decimal(19,4),
     BaselineLabHrs decimal(19,4),
     BaselineLabCost decimal(19,4),
     BaselineLabBill decimal(19,4),
     JTDLabHrs decimal(19,4),
     JTDLabCost decimal(19,4),
     JTDLabBill decimal(19,4),
     ETCLabHrs decimal(19,4),
     ETCLabCost decimal(19,4),
     ETCLabBill decimal(19,4),
     EACLabHrs decimal(19,4),
     EACLabCost decimal(19,4),
     EACLabBill decimal(19,4)
    )

BEGIN

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
  
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strMultiCompanyEnabled varchar(1)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint
 
  DECLARE @strVorN char(1)
 
  -- Declare Temp tables.

  DECLARE @tabAssignment TABLE
    (AssignmentID varchar(32) COLLATE database_default,
     WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(7) COLLATE database_default,
     WBS3 Nvarchar(7) COLLATE database_default,
     Employee Nvarchar(20) COLLATE database_default,
     GenericResourceID Nvarchar(20) COLLATE database_default,
     Category smallint,
     GRLBCD Nvarchar(14) COLLATE database_default,
     CostRate decimal(19,4),
     BillingRate decimal(19,4)
     UNIQUE(AssignmentID, Employee, GenericResourceID))

  DECLARE @tabRes TABLE
    (RowID  int IDENTITY(1,1),
     Name Nvarchar(255) COLLATE database_default,
     SortName Nvarchar(255) COLLATE database_default,
     Employee Nvarchar(20) COLLATE database_default,
     GenericResourceID Nvarchar(20) COLLATE database_default,
     Category smallint,
     GRLBCD Nvarchar(14) COLLATE database_default
     UNIQUE(RowID, Employee, GenericResourceID))
        
  DECLARE @tabLabETC TABLE
    (RowID  int IDENTITY(1,1),
     Employee Nvarchar(20) COLLATE database_default,
     GenericResourceID Nvarchar(20) COLLATE database_default,
     ETCLabHrs decimal(19,4),
     ETCLabCost decimal(19,4),
     ETCLabBill decimal(19,4)
     UNIQUE(RowID, Employee, GenericResourceID))
    
  DECLARE @tabLabBaseline TABLE
    (RowID  int IDENTITY(1,1),
     Employee Nvarchar(20) COLLATE database_default,
     GenericResourceID Nvarchar(20) COLLATE database_default,
     BaselineLabHrs decimal(19,4),
     BaselineLabCost decimal(19,4),
     BaselineLabBill decimal(19,4)
     UNIQUE(RowID, Employee, GenericResourceID))

 	DECLARE @tabLD TABLE 
    (RowID  int IDENTITY(1,1),
     Employee Nvarchar(20) COLLATE database_default,
     JTDLabHrs decimal(19,4),
     JTDLabCost decimal(19,4),
     JTDLabBill decimal(19,4)
     UNIQUE(RowID, Employee))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled flag.
  
  SELECT
    @strMultiCompanyEnabled = MultiCompanyEnabled
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat     

  -- Save Company string for use later.
  
  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          
  -- Set JTD/ETC Dates.
  
  SET @dtJTDDate = CONVERT(datetime, @strJTDDate)
  SET @dtETCDate = DATEADD(d, 1, @dtJTDDate) 

  -- Determine which set of tables need to be used.

  SET @strVorN = dbo.PN$PlanIs_VorN(@strWBS1)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Extract Assignment from Plans that are marked as "Included in Utilization".
  -- There could be multiple Plans with RPPlan.UlilizationIncludeFlag = 'Y'

  -- Calculate Resource ETC.
  -- Calculate Resource Baseline.
  -- It seems that having a SUM and GROUP BY in the following SQL would degrade performance substantially.

  IF (@strVorN = 'V')
    BEGIN

      INSERT @tabAssignment
        (AssignmentID,
         WBS1,
         WBS2,
         WBS3,
         Employee,
         GenericResourceID,
         Category,
         GRLBCD,
         CostRate,
         BillingRate)
        SELECT DISTINCT
          A.AssignmentID,
          A.WBS1,
          ISNULL(A.WBS2, ' '),
          ISNULL(A.WBS3, ' '),
          A.ResourceID AS Employee,
          A.GenericResourceID AS GenericResourceID,
          A.Category AS Category,
          A.GRLBCD AS GRLBCD,
          A.CostRate AS CostRate,
          A.BillingRate AS BillingRate
          FROM RPAssignment AS A
            INNER JOIN RPPlan AS P ON A.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND A.WBS1 = @strWBS1 AND 
              (ISNULL(A.WBS2, ' ') = @strWBS2) AND 
              (ISNULL(A.WBS3, ' ') = @strWBS3)

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabLabETC
        (Employee,
         GenericResourceID,
         ETCLabHrs,
         ETCLabCost,
         ETCLabBill
        )
        SELECT
          Employee AS Employee,
          GenericResourceID AS GenericResourceID,
          CASE WHEN X.StartDate >= @dtETCDate THEN PeriodHrs
	             ELSE PeriodHrs * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END AS ETCLabHrs,
          CASE WHEN X.StartDate >= @dtETCDate THEN PeriodCost
	             ELSE PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END AS ETCLabCost,
          CASE WHEN X.StartDate >= @dtETCDate THEN PeriodBill
	             ELSE PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END AS ETCLabBill
          FROM
            (SELECT
               A.Employee AS Employee,
               A.GenericResourceID AS GenericResourceID,
               TPD.StartDate AS StartDate, 
               TPD.EndDate as EndDate,
               ISNULL(PeriodHrs, 0) AS PeriodHrs,
               ISNULL(PeriodCost, 0) AS PeriodCost,
               ISNULL(PeriodBill, 0) AS PeriodBill
               FROM @tabAssignment AS A
                 INNER JOIN RPPlannedLabor AS TPD ON (A.AssignmentID = TPD.AssignmentID AND TPD.EndDate >= @dtETCDate) 
            ) AS X 

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabLabBaseline
        (Employee,
         GenericResourceID,
         BaselineLabHrs,
         BaselineLabCost,
         BaselineLabBill
        )
        SELECT
          A.Employee AS Employee,
          A.GenericResourceID AS GenericResourceID,
          ISNULL(PeriodHrs, 0) AS BaselineLabHrs,
          ISNULL(PeriodCost, 0) AS BaselineLabCost,
          ISNULL(PeriodBill, 0) AS BaselineLabBill
          FROM @tabAssignment AS A
            INNER JOIN RPBaselineLabor AS TPD ON A.AssignmentID = TPD.AssignmentID

    END /* End If (@strVorN = 'V') */
  ELSE IF (@strVorN = 'N')
    BEGIN

      INSERT @tabAssignment
        (AssignmentID,
         WBS1,
         WBS2,
         WBS3,
         Employee,
         GenericResourceID,
         Category,
         GRLBCD,
         CostRate,
         BillingRate)
        SELECT DISTINCT
          A.AssignmentID,
          A.WBS1,
          ISNULL(A.WBS2, ' '),
          ISNULL(A.WBS3, ' '),
          A.ResourceID AS Employee,
          A.GenericResourceID AS GenericResourceID,
          A.Category AS Category,
          A.GRLBCD AS GRLBCD,
          A.CostRate AS CostRate,
          A.BillingRate AS BillingRate
          FROM PNAssignment AS A
            INNER JOIN PNPlan AS P ON A.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND A.WBS1 = @strWBS1 AND 
              (ISNULL(A.WBS2, ' ') = @strWBS2) AND 
              (ISNULL(A.WBS3, ' ') = @strWBS3)

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabLabETC
        (Employee,
         GenericResourceID,
         ETCLabHrs,
         ETCLabCost,
         ETCLabBill
        )
        SELECT
          Employee AS Employee,
          GenericResourceID AS GenericResourceID,
          CASE WHEN X.StartDate >= @dtETCDate THEN PeriodHrs
	             ELSE PeriodHrs * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END AS ETCLabHrs,
          CASE WHEN X.StartDate >= @dtETCDate THEN PeriodCost
	             ELSE PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END AS ETCLabCost,
          CASE WHEN X.StartDate >= @dtETCDate THEN PeriodBill
	             ELSE PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END AS ETCLabBill
          FROM
            (SELECT
               A.Employee AS Employee,
               A.GenericResourceID AS GenericResourceID,
               TPD.StartDate AS StartDate, 
               TPD.EndDate as EndDate,
               ISNULL(PeriodHrs, 0) AS PeriodHrs,
               ISNULL(PeriodCost, 0) AS PeriodCost,
               ISNULL(PeriodBill, 0) AS PeriodBill
               FROM @tabAssignment AS A
                 INNER JOIN PNPlannedLabor AS TPD ON (A.AssignmentID = TPD.AssignmentID AND TPD.EndDate >= @dtETCDate) 
            ) AS X 

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabLabBaseline
        (Employee,
         GenericResourceID,
         BaselineLabHrs,
         BaselineLabCost,
         BaselineLabBill
        )
        SELECT
          A.Employee AS Employee,
          A.GenericResourceID AS GenericResourceID,
          ISNULL(PeriodHrs, 0) AS BaselineLabHrs,
          ISNULL(PeriodCost, 0) AS BaselineLabCost,
          ISNULL(PeriodBill, 0) AS BaselineLabBill
          FROM @tabAssignment AS A
            INNER JOIN PNBaselineLabor AS TPD ON A.AssignmentID = TPD.AssignmentID

    END /* End If (@strVorN = 'N') */
 
 -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save a list of Employees and Categories from Assignments for the input WBS1/WBS2/WBS3 combination.
  -- Also save a list of Employees from the Ledger table that do not have the same Categories as the Generic Resources in the Assignments.

  INSERT @tabRes
    (Name,
     SortName,
     Employee,
     GenericResourceID,
     Category,
     GRLBCD)
    SELECT DISTINCT
      CASE WHEN EM.Employee IS NOT NULL THEN CONVERT(Nvarchar(255), ISNULL(EM.FirstName, '') + ISNULL(' ' + EM.LastName, '')) 
           WHEN (EM.Employee IS NULL AND G.Code IS NOT NULL) THEN G.Name 
           ELSE '' END AS Name,
      CASE WHEN EM.Employee IS NOT NULL THEN CONVERT(Nvarchar(255), '1~' + ISNULL(EM.LastName, '') + ISNULL(', ' + EM.FirstName, '')) 
           WHEN (EM.Employee IS NULL AND G.Code IS NOT NULL) THEN CONVERT(Nvarchar(255), '0~' + ISNULL(G.Name, ''))  
           ELSE '' END AS SortName,
      X.Employee,
      X.GenericResourceID,
      X.Category,
      X.GRLBCD FROM
      (SELECT DISTINCT
         Employee,
         GenericResourceID,
         Category,
         GRLBCD
         FROM @tabAssignment AS A
       UNION SELECT DISTINCT
         Employee,
         NULL AS GenericResourceID,
         0 AS Category,
         NULL AS GRLBCD
         FROM LD -- LD table has entries at only the leaf level.
         WHERE LD.WBS1 = @strWBS1 AND 
           (LD.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
           (LD.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)   
       UNION SELECT DISTINCT
         Employee,
         NULL AS GenericResourceID,
         0 AS Category,
         NULL AS GRLBCD
         FROM tkDetail AS TD -- TD table has entries at only the leaf level.
         WHERE TD.WBS1 = @strWBS1 AND 
           (TD.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
           (TD.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END) AND
           (RegHrs > 0 OR OvtHrs > 0 OR SpecialOvtHrs > 0) 
       UNION SELECT DISTINCT
         Employee,
         NULL AS GenericResourceID,
         0 AS Category,
         NULL AS GRLBCD
         FROM tsDetail AS TD -- TD table has entries at only the leaf level.
         WHERE TD.WBS1 = @strWBS1 AND 
           (TD.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
           (TD.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END) AND
           (RegHrs > 0 OR OvtHrs > 0 OR SpecialOvtHrs > 0)
      ) AS X
        LEFT JOIN EM ON X.Employee = EM.Employee
        LEFT JOIN GR AS G ON X.GenericResourceID = G.Code 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	-- Save Labor JTD & Unposted Labor JTD for this Project into temp table.
  -- The JTD data are posted at the leaf node in the WBS Tree. 
  -- Therefore, we need to calculate JTD for the summary levels.

	INSERT @tabLD
    (Employee,
     JTDLabHrs,
     JTDLabCost,
     JTDLabBill
    )      
		SELECT
      X.Employee,
      SUM(ISNULL(JTDLabHrs, 0.0000)) AS JTDLabHrs,
      SUM(ISNULL(JTDLabCost, 0.0000)) AS JTDLabCost,
      SUM(ISNULL(JTDLabBill, 0.0000)) AS JTDLabBill 
      FROM
        (SELECT
           LD.Employee,
           SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDLabHrs,
           SUM(LD.RegAmtProjectCurrency + LD.OvtAmtProjectCurrency + LD.SpecialOvtAmtProjectCurrency) AS JTDLabCost,
           SUM(LD.BillExt) AS JTDLabBill
           FROM LD 
           WHERE LD.ProjectCost = 'Y' AND LD.TransDate <= @dtJTDDate AND LD.WBS1 = @strWBS1 AND
             (LD.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
             (LD.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
           GROUP BY LD.Employee
         UNION ALL SELECT
           TD.Employee,
           SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDLabHrs,
           SUM(TD.RegAmtProjectCurrency + TD.OvtAmtProjectCurrency + TD.SpecialOvtAmtProjectCurrency) AS JTDLabCost,
           SUM(TD.BillExt) AS JTDLabBill
           FROM tkDetail AS TD
             INNER JOIN EM ON TD.Employee = EM.Employee  
             INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
           WHERE TD.TransDate <= @dtJTDDate AND TD.WBS1 = @strWBS1 AND
             (TD.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
             (TD.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
           GROUP BY TD.Employee
         UNION ALL SELECT
           TD.Employee,
           SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDLabHrs,
           SUM(TD.RegAmtProjectCurrency + TD.OvtAmtProjectCurrency + TD.SpecialOvtAmtProjectCurrency) AS JTDLabCost,
           SUM(TD.BillExt) AS JTDLabBill
           FROM tsDetail AS TD
             INNER JOIN EM ON TD.Employee = EM.Employee  
             INNER JOIN tsControl AS TC ON (TC.Posted = 'N' AND TD.Batch = TC.Batch)
           WHERE TD.TransDate <= @dtJTDDate AND TD.WBS1 = @strWBS1 AND
             (TD.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
             (TD.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
           GROUP BY TD.Employee
        ) AS X
      GROUP BY X.Employee

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Combine data for Resource rows.

  INSERT @tabLabRes
    (AssignmentID,
     Name,
     SortName,
     Employee,
     GenericResourceID,
     Category,
     GRLBCD,
     CostRate,
     BillingRate,
     BaselineLabHrs,
     BaselineLabCost,
     BaselineLabBill,
     JTDLabHrs,
     JTDLabCost,
     JTDLabBill,
     ETCLabHrs,
     ETCLabCost,
     ETCLabBill,
     EACLabHrs,
     EACLabCost,
     EACLabBill
    )
    SELECT
      A.AssignmentID,
      R.Name,
      R.SortName,
      R.Employee,
      R.GenericResourceID,
      R.Category,
      R.GRLBCD,
      ISNULL(A.CostRate, 0.0000),
      ISNULL(A.BillingRate, 0.0000),
      ISNULL(BaselineLabHrs, 0.0000),
      ISNULL(BaselineLabCost, 0.0000),
      ISNULL(BaselineLabBill, 0.0000),
      ISNULL(JTDLabHrs, 0.0000),
      ISNULL(JTDLabCost, 0.0000),
      ISNULL(JTDLabBill, 0.0000),
      ISNULL(ETCLabHrs, 0.0000),
      ISNULL(ETCLabCost, 0.0000),
      ISNULL(ETCLabBill, 0.0000),
      ISNULL(JTDLabHrs, 0.0000) + ISNULL(ETCLabHrs, 0.0000) AS EACLabHrs,
      ISNULL(JTDLabCost, 0.0000) + ISNULL(ETCLabCost, 0.0000) AS EACLabCost,
      ISNULL(JTDLabBill, 0.0000) + ISNULL(ETCLabBill, 0.0000) AS EACLabBill
      FROM @tabRes AS R
        LEFT JOIN
          (SELECT MIN(AssignmentID) AS AssignmentID, Employee, GenericResourceID, MIN(CostRate) AS CostRate, MIN(BillingRate) AS BillingRate
             FROM @tabAssignment 
             GROUP BY Employee, GenericResourceID
          ) AS A ON ISNULL(R.Employee, '|') = ISNULL(A.Employee, '|') AND ISNULL(R.GenericResourceID, '|') = ISNULL(A.GenericResourceID, '|')
        LEFT JOIN
          (SELECT Employee, GenericResourceID, SUM(ISNULL(BaselineLabHrs, 0.0000)) AS BaselineLabHrs, SUM(ISNULL(BaselineLabCost, 0.0000)) AS BaselineLabCost, SUM(ISNULL(BaselineLabBill, 0.0000)) AS BaselineLabBill
             FROM @tabLabBaseline
             GROUP BY Employee, GenericResourceID
          ) AS B ON ISNULL(R.Employee, '|') = ISNULL(B.Employee, '|') AND ISNULL(R.GenericResourceID, '|') = ISNULL(B.GenericResourceID, '|')
        LEFT JOIN
          (SELECT Employee, GenericResourceID, SUM(ISNULL(ETCLabHrs, 0.0000)) AS ETCLabHrs, SUM(ISNULL(ETCLabCost, 0.0000)) AS ETCLabCost, SUM(ISNULL(ETCLabBill, 0.0000)) AS ETCLabBill
             FROM @tabLabETC
             GROUP BY Employee, GenericResourceID
          ) AS ETC ON ISNULL(R.Employee, '|') = ISNULL(ETC.Employee, '|') AND ISNULL(R.GenericResourceID, '|') = ISNULL(ETC.GenericResourceID, '|')
        LEFT JOIN @tabLD AS LD ON ISNULL(R.Employee, '|') = ISNULL(LD.Employee, '|') 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
