SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$BTRLTCodes]
  (@intTableNo AS int,
   @strLBCD AS Nvarchar(14),
   @dtStartDate AS datetime,
   @dtEndDate AS datetime)
  RETURNS float
BEGIN

  DECLARE @strCompany Nvarchar(14)
  DECLARE @fCompositeRate float
  DECLARE @fNWD float
  
  SET @strCompany = dbo.GetActiveCompany()
  SET @fNWD = ABS(dbo.DLTK$NumWorkingDays(@dtStartDate, @dtEndDate, @strCompany))
  SET @fCompositeRate = 0
  
  IF (@strLBCD <> '<none>')
    BEGIN
    
      IF (@fNWD <> 0)
        BEGIN
       
          -- Composite Rate = ((Rate_1 * NWD_1) + (Rate_2 * NWD_2) + ... + (Rate_n * NWD_n)) / NWD_total

          SELECT @fCompositeRate = ISNULL((SUM(ISNULL(Rate_n, 0)) / @fNWD), 0)
            FROM
              (SELECT ISNULL(Rate, 0) * ABS(dbo.DLTK$NumWorkingDays(CASE WHEN @dtStartDate > StartDate THEN @dtStartDate ELSE StartDate END,
                                                                    CASE WHEN @dtEndDate < EndDate THEN @dtEndDate ELSE EndDate END,
                                                                    @strCompany)) AS Rate_n

                 FROM BTRLTCodes AS RA
                   INNER JOIN
                     (SELECT TOP 1 LaborCodeMask FROM BTRLTCodes, CFGFormat
                         WHERE TableNo = @intTableNo AND
                           (SUBSTRING(@strLBCD, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
					                  SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(@strLBCD, LC1Start, LC1Length)) AND  
					                 (SUBSTRING(@strLBCD, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
					                  SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(@strLBCD, LC2Start, LC2Length)) AND  
					                 (SUBSTRING(@strLBCD, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
					                  SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(@strLBCD, LC3Start, LC3Length)) AND  
					                 (SUBSTRING(@strLBCD, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
					                  SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(@strLBCD, LC4Start, LC4Length)) AND  
					                 (SUBSTRING(@strLBCD, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
					                  SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(@strLBCD, LC5Start, LC5Length))) AS RB
					         ON RA.LaborCodeMask = RB.LaborCodeMask
                 WHERE RA.TableNo = @intTableNo AND RA.StartDate <= @dtEndDate AND RA.EndDate >= @dtStartDate) AS X
       
        END -- If-Then
      ELSE
        BEGIN
        
          -- If there is no working day between Start/End date then prorate the Rate using calendar days.

          SELECT @fCompositeRate = ISNULL((SUM(ISNULL(Rate_n, 0)) / (ABS(DATEDIFF(d, @dtStartDate, @dtEndDate)) + 1)), 0)
            FROM
              (SELECT ISNULL(Rate, 0) * (ABS(DATEDIFF(d,
                                                      CASE WHEN @dtStartDate > StartDate THEN @dtStartDate ELSE StartDate END,
                                                      CASE WHEN @dtEndDate < EndDate THEN @dtEndDate ELSE EndDate END)) + 1) AS Rate_n
                 FROM BTRLTCodes AS RA
                   INNER JOIN
                     (SELECT TOP 1 LaborCodeMask FROM BTRLTCodes, CFGFormat
                         WHERE TableNo = @intTableNo AND
                           (SUBSTRING(@strLBCD, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
					                  SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(@strLBCD, LC1Start, LC1Length)) AND  
					                 (SUBSTRING(@strLBCD, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
					                  SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(@strLBCD, LC2Start, LC2Length)) AND  
					                 (SUBSTRING(@strLBCD, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
					                  SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(@strLBCD, LC3Start, LC3Length)) AND  
					                 (SUBSTRING(@strLBCD, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
					                  SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(@strLBCD, LC4Start, LC4Length)) AND  
					                 (SUBSTRING(@strLBCD, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
					                  SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(@strLBCD, LC5Start, LC5Length))) AS RB
					         ON RA.LaborCodeMask = RB.LaborCodeMask
                 WHERE RA.TableNo = @intTableNo AND RA.StartDate <= @dtEndDate AND RA.EndDate >= @dtStartDate) AS X
        
        END -- If-Else
        
    END -- If-Then (@strLBCD <> '<none>')
  
    RETURN(@fCompositeRate)

END -- fn_RP$BTRLTCodes
GO
