SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RM$tabNavPlans]
  (@xmlPlanID XML)

  RETURNS @tabPlanIndicators TABLE
    (PlanID varchar(32) COLLATE database_default,
     PlanName Nvarchar(255) COLLATE database_default,
     PlanNumber Nvarchar(30) COLLATE database_default,
     CheckedOutID varchar(32) COLLATE database_default,
     CheckedOutDate datetime,
     CheckedOutUser Nvarchar(32) COLLATE database_default,
     LastPlanAction Nvarchar(30) COLLATE database_default,
     UnpublishedData varchar(1) COLLATE database_default
    )

BEGIN
 
  INSERT @tabPlanIndicators
    (PlanID,
     PlanName,
     PlanNumber,
     CheckedOutID,
     CheckedOutDate,
     CheckedOutUser,
     LastPlanAction,
     UnpublishedData
    )
    SELECT
      PN.PlanID AS PlanID,
      PN.PlanName AS PlanName,
      PN.PlanNumber AS PlanNumber,
      RP.CheckedOutID AS CheckedOutID,
      RP.CheckedOutDate AS CheckedOutDate,
      RP.CheckedOutUser AS CheckedOutUser,
      PN.LastPlanAction AS LastPlanAction,
      CASE WHEN PN.LastPlanAction IN ('SAVED', 'SAVEDBASELINE') THEN 'Y' ELSE 'N' END AS UnpublishedData
      FROM
        (SELECT Tbl.Col.value('PlanID[1]', 'varchar(32)') AS PlanID FROM @xmlPlanID.nodes('ROOT/PNPlan') AS Tbl(Col)) AS X
        INNER JOIN PNPlan AS PN ON X.PlanID = PN.PlanID
        INNER JOIN RPPlan AS RP ON PN.PlanID = RP.PlanID
      WHERE RP.CheckedOutUser IS NOT NULL OR RP.CheckedOutDate IS NOT NULL OR RP.CheckedOutID IS NOT NULL OR PN.LastPlanAction IN ('SAVED', 'SAVEDBASELINE')

RETURN
END 

GO
