SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[FW_GetActiveCultureName]() RETURNS varchar(10) AS 
BEGIN 
    DECLARE @UICulture varchar(10)

    SET @UICulture= CONVERT(varchar(10), SESSION_CONTEXT(N'Culture'))  
    -- determine an enabled culture when no session data set; allows for views to return data when run in SQL tool  

    IF (@UICulture is null)  
    SET @UICulture=(SELECT top 1 Language FROM FW_CFGEnabledLanguages where PrimaryLanguage='Y')  
  
    return ISNULL(@UICulture,'en-US') 
END
GO
