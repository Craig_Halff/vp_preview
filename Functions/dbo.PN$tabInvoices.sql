SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[PN$tabInvoices] (
  @strWBS1 nvarchar(30),
  @strWBS2 nvarchar(30),
  @strWBS3 nvarchar(30),
  @strJTDDate VARCHAR(8), -- Date must be in format: 'yyyymmdd' regardless of UI Culture.
  @strActivePeriod Varchar(6)
)

  RETURNS @tabInvoices TABLE (
    PKey varchar(32) COLLATE database_default,
	MainWBS1 nvarchar(30) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    InvoiceNumber nvarchar(12) COLLATE database_default,
    TransType varchar(2) COLLATE database_default,
    SubType varchar(1) COLLATE database_default,
    InvoiceSection varchar(1) COLLATE database_default,
    CreditMemoRefNo nvarchar(12) COLLATE database_default,
    TaxCode nvarchar(10) COLLATE database_default,
    LinkCompany nvarchar(14) COLLATE database_default,
    ICCompany nvarchar(14) COLLATE database_default,
    ICBillInvoice nvarchar(12) COLLATE database_default,
    TransDate datetime,
    InvoiceDate datetime,
    AmountBillingCurrency decimal(19,4),
    BilledAmt decimal(19,4),
    ReceivedAmt decimal(19,4),
    RetainageAmt decimal(19,4),
    RetainerAmt decimal(19,4),
    TaxAmt decimal(19,4),
    InterestAmt decimal(19,4),
    CreditMemoAmt decimal(19,4),
    InvoiceBalance decimal(19,4),
    DaysOld integer
    UNIQUE(Pkey, MainWBS1, WBS1, WBS2, WBS3, InvoiceNumber, TransDate, InvoiceDate, TransType, SubType, InvoiceSection, CreditMemoRefNo, TaxCode)
  )

BEGIN

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
 
  DECLARE @strCompany nvarchar(14)
  DECLARE @strMultiCompanyEnabled varchar(1)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
          
  -- Get MultiCompanyEnabled flag.
  
  SELECT
    @strMultiCompanyEnabled = MultiCompanyEnabled
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat     

  -- Save Company string for use later.
  
  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          
  -- Set JTD/ETC Dates.
  
  SET @dtJTDDate = CONVERT(datetime, NULLIF(@strJTDDate,''))
  SET @dtETCDate = DATEADD(d, 1, @dtJTDDate) 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT INTO @tabInvoices (
    PKey,
	MainWBS1,
    WBS1,
    WBS2,
    WBS3,
    InvoiceNumber,
    TransType,
    SubType,
    InvoiceSection,
    CreditMemoRefNo,
    TaxCode,
    LinkCompany,
    ICCompany,
    ICBillInvoice,
    TransDate,
    InvoiceDate,
    AmountBillingCurrency,
    BilledAmt,
    ReceivedAmt,
    RetainageAmt,
    RetainerAmt,
    TaxAmt,
    InterestAmt,
    CreditMemoAmt,
    InvoiceBalance,
    DaysOld
  )
    SELECT DISTINCT
      L.PKey,
	  IsNull(ICBillingWBS1, L.WBS1) AS MainWBS1,
      L.WBS1 AS WBS1,
      L.WBS2 AS WBS2,
      L.WBS3 AS WBS3,
      L.Invoice AS InvoiceNumber,
      L.TransType AS TransType,
      L.SubType AS SubType,
      L.InvoiceSection AS InvoiceSection,
      L.CreditMemoRefNo AS CreditMemoRefNo,
      L.TaxCode AS TaxCode,
      AR.LinkCompany AS LinkCompany,
      ICBillingWKSub.ICCompany AS ICCompany,
      ICBillingWKSub.ICBillInvoice AS ICBillInvoice,
      L.TransDate AS TransDate,
      AR.InvoiceDate AS InvoiceDate,
      
      L.AmountBillingCurrency AS AmountBillingCurrency,

      (
        CASE 
          WHEN L.TransType = 'IN' AND L.SubType IS NULL AND CreditMemoRefNo IS NULL THEN -AmountBillingCurrency
          WHEN L.TransType = 'IN' AND L.SubType = 'I' THEN -AmountBillingCurrency
          ELSE 0.0000
        END
      ) AS BilledAmt,

      (
        CASE
          WHEN ((L.TransType = 'CR' AND L.SubType <> 'T') OR (L.TransType = 'CR' AND L.SubType = 'T' AND L.Invoice IS NULL))
          THEN -AmountBillingCurrency
          ELSE 0.0000
        END
      ) AS ReceivedAmt,

      (
        CASE
          WHEN L.TransType= 'IN' AND L.SubType = 'R'
          THEN AmountBillingCurrency
          ELSE 0.0000
        END
      ) AS RetainageAmt,

      (
        CASE
          WHEN L.TransType= 'CR' AND SubType = 'T'
          THEN 
            CASE WHEN L.Invoice IS NULL THEN -AmountBillingCurrency ELSE AmountBillingCurrency END
          ELSE 0.0000
        END
      ) AS RetainerAmt,

      (
        CASE
          WHEN ISNULL(L.InvoiceSection, '') = 'T' OR (L.TaxCode IS NOT NULL AND L.TransType= 'CR') 
          THEN
            CASE WHEN L.TransType = 'IN' THEN -AmountBillingCurrency ELSE AmountBillingCurrency END
          ELSE 0.0000
        END
      ) AS TaxAmt,

      (
        CASE
          WHEN L.TransType = 'IN' AND L.SubType= 'I' 
          THEN -AmountBillingCurrency
          ELSE 0.0000
        END
      ) AS InterestAmt,

      (
        CASE 
          WHEN L.TransType = 'IN' AND (ISNULL(L.SubType, ' ') != 'R' AND ISNULL(L.SubType, ' ') != 'I') AND CreditMemoRefNo IS NOT NULL 
          THEN AmountBillingCurrency
          ELSE 0.0000
        END
      ) AS CreditMemoAmt,

      (
        CASE
          WHEN L.TransType = 'IN' THEN -AmountBillingCurrency
          WHEN L.TransType = 'CR' AND L.Invoice IS NOT NULL
          THEN
            CASE WHEN L.SubType = 'T' THEN -AmountBillingCurrency ELSE AmountBillingCurrency END
          ELSE 0.0000
        END
      ) AS InvoiceBalance,

      (
        CASE
          WHEN ((L.TransType= 'IN' AND ISNULL(L.SubType, ' ') <> 'X') OR (L.TransType= 'CR' AND (L.SubType= 'R' OR (L.SubType= 'T' AND ISNULL(L.Invoice, ' ') <> ' '))))
          THEN
            CASE
              WHEN AR.InvoiceDate IS NULL THEN 0
              ELSE
                CASE
                  WHEN AR.PaidPeriod >= 999999 
                  THEN DATEDIFF(day, AR.InvoiceDate, ISNULL(@dtJTDDate, GETDATE()))
                  ELSE 0
                END
            END
          ELSE 0
        END
      ) AS DaysOld

      FROM LedgerAR AS L
        LEFT JOIN AR ON L.WBS1 = AR.WBS1 AND L.WBS2 = AR.WBS2 AND L.WBS3 = AR.WBS3 AND L.Invoice = AR.Invoice
        LEFT JOIN CA ON L.Account = CA.Account

        LEFT JOIN (
          SELECT 
            ICBillingWK.InvoiceWBS1 AS WKWBS1, 
            ICBillingWK.InvoiceWBS2 AS WKWBS2, 
            ICBillingWK.InvoiceWBS3 AS WKWBS3, 
            ICBillInvMaster.MainWBS1 AS ICBillingWBS1, 
            ICBillInvMaster.Company AS ICCompany,
            ICBillInvMaster.Invoice AS ICBillInvoice
            FROM ICBillInvMaster
              LEFT JOIN ICBillingWK ON 
                ICBillInvMaster.Company = ICBillingWK.Company AND 
                ICBillInvMaster.Period = ICBillingWK.Period AND 
                ICBillInvMaster.RunSeq = ICBillingWK.RunSeq AND 
                ICBillInvMaster.Invoice = ICBillingWK.Invoice
            GROUP BY ICBillingWK.InvoiceWBS1, ICBillingWK.InvoiceWBS2, ICBillingWK.InvoiceWBS3, ICBillInvMaster.Company, ICBillInvMaster.MainWBS1, ICBillInvMaster.Invoice
        ) AS ICBillingWKSub
          ON AR.LinkCompany IS NOT NULL AND ICBillingWKSub.WKWBS1 = AR.WBS1 AND ICBillingWKSub.WKWBS2 = AR.WBS2 AND ICBillingWKSub.WKWBS3 = AR.WBS3 AND ICBillingWKSub.ICBillInvoice = AR.Invoice

      WHERE L.WBS1 = @strWBS1 AND 
        (L.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND
        (L.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END) AND
        L.AutoEntry = 'N' AND L.Period <= @strActivePeriod AND -- Use active period not JTD date (L.TransDate <= @dtJTDDate OR @dtJTDDate is NULL) AND
        ((TransType = 'IN' AND L.SubType IS NULL) OR
         (TransType = 'IN' AND L.SubType IN ('I', 'R')) OR
         (TransType = 'CR' AND L.SubType IN ('R', 'T'))
        )
        
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
