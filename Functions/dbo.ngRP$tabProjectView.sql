SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ngRP$tabProjectView](
  @strRowID nvarchar(255),
  @strMode varchar(1) = 'C', /* S = Self, C = Children */
  @bActiveWBSOnly bit = 0
)
  RETURNS @tabProjectView TABLE (

    RowID nvarchar(255) COLLATE database_default,
    ParentRowID nvarchar(255) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ResourceID nvarchar(20) COLLATE database_default,
    GenericResourceID nvarchar(20) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int,
    HasAssignments bit,
    HasChildren bit,
    LeafNode bit,
    RowLevel int,
    Name nvarchar(255) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PlannedHrs decimal(19,4),
    ETCHrs decimal(19,4),
    JTDHrs decimal(19,4),
    EACHrs decimal(19,4),
    HasPhoto bit,
	PhotoModDate datetime,
    Org nvarchar(30) COLLATE database_default,
    OrgName nvarchar(100) COLLATE database_default,
    ProjMgr nvarchar(20) COLLATE database_default,
    PMFullName nvarchar(255) COLLATE database_default,
    ClientName nvarchar(100) COLLATE database_default,
    RT_Status varchar(1) COLLATE database_default,
    HardBooked varchar(1) COLLATE database_default,
    ContractStartDate datetime,
    ContractEndDate datetime,
	UtilizationScheduleFlg varchar(1),
    MinASGDate datetime,
    MaxASGDate datetime,
    ResSortSeq tinyint,
    CalculatedPct decimal(19,4),
    TopWBS1 nvarchar(30) COLLATE database_default,
    TopName nvarchar(255) COLLATE database_default,
    VersionID varchar(32) COLLATE database_default,
    BaselineHrs decimal(19,4),
    BaselineCost decimal(19,4),
    BaselineBill decimal(19,4),
    PlannedCost decimal(19,4),
    PlannedBill decimal(19,4),
    JTDCost decimal(19,4),
    JTDBill decimal(19,4),
    ETCCost decimal(19,4),
    ETCBill decimal(19,4),
    EACCost decimal(19,4),
    EACBill decimal(19,4),
    PlannedLessJTDCost decimal(19,4),
    PlannedLessJTDBill decimal(19,4),
    PlannedLessEACCost decimal(19,4),
    PlannedLessEACBill decimal(19,4),
    ContractCost decimal(19,4),
    ContractBill decimal(19,4),
    ContractLessEACCost decimal(19,4),
    ContractLessEACBill decimal(19,4),
    ContractLessJTDCost decimal(19,4),
    ContractLessJTDBill decimal(19,4),
    EACOverheadCost decimal(19,4),
    EACOverheadBill decimal(19,4),
    EACProfitCost decimal(19,4),
    EACProfitBill decimal(19,4),
    EACProfitPctCost decimal(19,4),
    EACProfitPctBill decimal(19,4)
  )

BEGIN

  DECLARE @strCompany nvarchar(14)
  DECLARE @strTaskID varchar(32)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTopWBS1 nvarchar(30)
  DECLARE @strTopName nvarchar(255)
  DECLARE @strVersionID varchar(32)  
  DECLARE @strTopAssignmentTaskID varchar(32)
  DECLARE @strResourceType varchar(1)
  DECLARE @strIDPrefix nvarchar(255)
  DECLARE @strResourceID nvarchar(20)
  DECLARE @strGenericResourceID nvarchar(20)
  DECLARE @strParentRowID nvarchar(255) = ''
  DECLARE @strTaskStatus varchar(1) = ''

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime

  DECLARE @siHrDecimals int /* CFGRMSettings.HrDecimals */

  DECLARE @bitIsLeafTask bit

  -- Declare Temp tables.

  DECLARE @tabAssignment TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ResourceID varchar(32) COLLATE database_default,
    GenericResourceID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    HardBookedFlg int,
    OutlineNumber varchar(255) COLLATE database_default
    UNIQUE(PlanID, TaskID, AssignmentID)
  )

  DECLARE @tabLabTPD TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChargeType varchar(1) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PlannedHrs decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, OutlineNumber, StartDate, EndDate)
  )

  DECLARE @tabLabBaselineTPD TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PlannedHrs decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, OutlineNumber, StartDate, EndDate)
  )

  DECLARE @tabETCTPD TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PeriodHrs decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, OutlineNumber, StartDate, EndDate)
  )

  DECLARE @tabTotalETC TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ETCHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber, AssignmentID)
  )

  DECLARE @tabJTDLD TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    Employee nvarchar(20) COLLATE database_default,
    PeriodHrs decimal(19,4),
    JTDLabCost decimal(19,4),	
    JTDLabBill decimal(19,4)
    UNIQUE (RowID, WBS1, WBS2, WBS3,Employee)
  )

  DECLARE @tabLD TABLE (
    LDRowID  int IDENTITY(1,1),
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    PeriodHrs decimal(19,4),
    JTDLabCost decimal(19,4),	
    JTDLabBill decimal(19,4)
    PRIMARY KEY(LDRowID, PlanID, OutlineNumber)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Set Company to non-Multi-Company.
  
  SELECT @strCompany = dbo.GetActiveCompany()

  -- Get RM Settings.
  
  SELECT 
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE()))),
    @siHrDecimals = HrDecimals
    FROM CFGRMSettings

  SELECT @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)

  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. For an Assignment row, @strRowID = 'E~<RPAssignment.ResourceID>|<RPTask.TaskID>
  --   2. For an Assignment row, @strRowID = 'G~<RPAssignment.GenericResourceID>|<RPTask.TaskID>'
  --   3. For a WBS row, @strRowID = '|<RPTask.TaskID>'

  SET @strResourceType = 
    CASE 
      WHEN CHARINDEX('~', @strRowID) = 0 
      THEN ''
      ELSE SUBSTRING(@strRowID, 1, 1)
    END

  SET @strIDPrefix = LEFT(@strRowID, CHARINDEX('|', @strRowID) - 1)

  IF (@strResourceType = 'E')
    BEGIN
      SET @strResourceID = REPLACE(@strIDPrefix, 'E~', '')
      SET @strGenericResourceID = NULL
    END
  ELSE IF (@strResourceType = 'G')
    BEGIN
      SET @strGenericResourceID = REPLACE(@strIDPrefix, 'G~', '')
      SET @strResourceID = NULL
    END
  ELSE
    BEGIN
      SET @strResourceID = NULL
      SET @strGenericResourceID = NULL
    END

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

  -- Setting other data.

  SELECT 
    @strPlanID = PT.PlanID,
    @strTaskStatus = PT.Status,
    @bitIsLeafTask =
      CASE
        WHEN COUNT(CT.TaskID) > 0
        THEN 0
        ELSE 1
      END
    FROM RPTask AS PT 
      LEFT JOIN RPTask AS CT
        ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
    WHERE PT.TaskID = @strTaskID
    GROUP BY PT.PlanID, PT.TaskID, PT.Status

  -- Setting VesionID

  SELECT
    @strVersionID = VersionID
    FROM RPPlan 
    WHERE PlanID = @strPlanID

  IF (DATALENGTH(@strTaskID) <> 0)
    Begin
      Select @strTopWBS1 = PT.WBS1 FROM RPTask PT Where PlanID in (Select PlanID From RPTask Where TaskID = @strTaskID) and Outlinelevel = 0	
      Select @strTopName = PT.Name FROM RPTask PT Where PlanID in (Select PlanID From RPTask Where TaskID = @strTaskID) and Outlinelevel = 0	
    End

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Determine ParentRowID based on @strMode.

  SELECT
    @strParentRowID = 
      CASE 
        WHEN @strMode = 'C' THEN @strRowID
        WHEN @strMode = 'S' THEN 
          CASE 
            WHEN DATALENGTH(@strResourceType) > 0
            THEN ISNULL(('|' + CT.TaskID), '')
            ELSE ISNULL(('|' + PT.TaskID), '')
          END
      END 
    FROM RPTask AS CT
      LEFT JOIN RPTask AS PT ON PT.PlanID = CT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber
    WHERE CT.TaskID = @strTaskID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strMode = 'C')
    BEGIN

      INSERT @tabAssignment(
        PlanID,
        TaskID,
        AssignmentID,
        ResourceID,
        GenericResourceID,
        StartDate,
        EndDate,
        HardBookedFlg,
        OutlineNumber
      )
        SELECT DISTINCT
          A.PlanID,
          A.TaskID,
          A.AssignmentID,
          A.ResourceID,
          A.GenericResourceID,
          A.StartDate,
          A.EndDate,
          CASE HardBooked
            WHEN 'Y' THEN 1
            WHEN 'N' THEN -1
          END AS HardBookedFlg,
          AT.OutlineNumber AS OutlineNumber
          FROM RPAssignment AS A
            INNER JOIN RPPlan AS P ON P.PlanID = A.PlanID
            INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
            INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID AND AT.OutlineNumber LIKE PT.OutlineNumber + '%'
            LEFT JOIN RPTask AS CT ON A.PlanID = CT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber AND AT.OutlineNumber LIKE CT.OutlineNumber + '%'
          WHERE
            A.PlanID = @strPlanID AND PT.TaskID = @strTaskID

    END /* END IF (@strMode = 'C') */

  ELSE IF (@strMode = 'S')
    BEGIN

      IF (DATALENGTH(@strResourceType) = 0) /* @strRowID is pointing to a WBS row */
        BEGIN

          INSERT @tabAssignment(
            PlanID,
            TaskID,
            AssignmentID,
            ResourceID,
            GenericResourceID,
            StartDate,
            EndDate,
            HardBookedFlg,
            OutlineNumber
          )
            SELECT DISTINCT
              A.PlanID,
              A.TaskID,
              A.AssignmentID,
              A.ResourceID,
              A.GenericResourceID,
              A.StartDate,
              A.EndDate,
              CASE HardBooked
                WHEN 'Y' THEN 1
                WHEN 'N' THEN -1
              END AS HardBookedFlg,
              AT.OutlineNumber AS OutlineNumber
              FROM RPAssignment AS A
                INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
                INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID AND AT.OutlineNumber LIKE PT.OutlineNumber + '%'
              WHERE
                A.PlanID = @strPlanID AND PT.TaskID = @strTaskID

        END /* END IF (DATALENGTH(@strResourceType) = 0) */

      ELSE /* @strRowID is pointing to an Assignment row */
        BEGIN

          INSERT @tabAssignment(
            PlanID,
            TaskID,
            AssignmentID,
            ResourceID,
            GenericResourceID,
            StartDate,
            EndDate,
            HardBookedFlg,
            OutlineNumber
          )
            SELECT DISTINCT
              A.PlanID,
              A.TaskID,
              A.AssignmentID,
              A.ResourceID,
              A.GenericResourceID,
              A.StartDate,
              A.EndDate,
              CASE HardBooked
                WHEN 'Y' THEN 1
                WHEN 'N' THEN -1
              END AS HardBookedFlg,
              AT.OutlineNumber AS OutlineNumber
              FROM RPAssignment AS A
                INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
              WHERE
                A.PlanID = @strPlanID AND A.TaskID = @strTaskID AND
                ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') AND 
                ISNULL(A.GenericResourceID, '|') = ISNULL( @strGenericResourceID, '|')

        END /* END ELSE */

    END /* END ELSE IF (@strMode = 'S') */
    
--++++++++

  -- Collecting Labor Time-Phased Data to be used in subsequent calculations.
  -- The TPD records come from only the Assignments included in @tabAssignment.

  INSERT @tabLabTPD(
    TimePhaseID,
    PlanID,
    TaskID,
    AssignmentID,
    OutlineNumber,
    ChargeType,
    StartDate,
    EndDate,
    PlannedHrs
  )
    SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID,
      TPD.TaskID AS TaskID,
      TPD.AssignmentID AS AssignmentID,
      T.OutlineNumber AS OutlineNumber,
      T.ChargeType AS ChargeType,
      TPD.StartDate AS StartDate,
      TPD.EndDate AS EndDate,
      TPD.PeriodHrs AS PlannedHrs
      FROM @tabAssignment AS A
        INNER JOIN RPTask AS T ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
        INNER JOIN RPPlannedLabor AS TPD ON
          A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID AND
          TPD.PeriodHrs <> 0 AND TPD.AssignmentID IS NOT NULL 


--++++++++

  -- Collecting Baseline Labor Time-Phased Data to be used in subsequent calculations.
  -- The TPD records come from only the Assignments included in @tabAssignment.

  INSERT @tabLabBaselineTPD(
    TimePhaseID,
    PlanID,
    TaskID,
    AssignmentID,
    OutlineNumber,
    StartDate,
    EndDate,
    PlannedHrs
  )
    SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID,
      TPD.TaskID AS TaskID,
      TPD.AssignmentID AS AssignmentID,
      T.OutlineNumber AS OutlineNumber,
      TPD.StartDate AS StartDate,
      TPD.EndDate AS EndDate,
      TPD.PeriodHrs AS PlannedHrs
      FROM @tabAssignment AS A
        INNER JOIN RPTask AS T ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
        INNER JOIN RPBaselineLabor AS TPD ON
          A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID AND
          TPD.PeriodHrs <> 0 AND TPD.AssignmentID IS NOT NULL 


-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collecting ETC Time-Phased Data to be used in subsequent calculations.

  INSERT @tabETCTPD(
    TimePhaseID,
    PlanID,
    TaskID,
    AssignmentID,
    OutlineNumber,
    StartDate,
    EndDate,
    PeriodHrs
  )
    SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID,
      TPD.TaskID AS TaskID,
      TPD.AssignmentID AS AssignmentID,
      TPD.OutlineNumber AS OutlineNumber,
      CASE WHEN TPD.StartDate >= @dtETCDate THEN TPD.StartDate ELSE @dtETCDate END AS StartDate,
      TPD.EndDate AS EndDate,
      ISNULL(
        CASE 
          WHEN TPD.StartDate >= @dtETCDate 
          THEN TPD.PlannedHrs
          ELSE TPD.PlannedHrs * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) 
        END 
      , 0) AS PeriodHrs
      FROM @tabLabTPD AS TPD
      WHERE TPD.PlannedHrs <> 0 AND TPD.EndDate >= @dtETCDate 

--+++++

  -- Calculate Total ETC
  -- GROUP BY PlanID, OutlineNumber

  INSERT @tabTotalETC(
    PlanID,
    OutlineNumber,
    AssignmentID,
    ETCHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
      TPD.AssignmentID,
      ISNULL(SUM(TPD.PeriodHrs), 0) AS ETCHrs
      FROM @tabETCTPD AS TPD
      GROUP BY PlanID, OutlineNumber, AssignmentID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save Labor JTD & Unposted Labor JTD for this Resource into temp table.
  -- The JTD data are posted at the leaf node in the WBS Tree. 
  -- Therefore, we need to calculate JTD for the summary levels.
  
  INSERT @tabJTDLD(
    WBS1,
    WBS2,
    WBS3,
    LaborCode,
    Employee,
    PeriodHrs,
    JTDLabCost,
    JTDLabBill
  )               
    SELECT
      WBS1 AS WBS1,
      WBS2 AS WBS2,
      WBS3 AS WBS3,
      LaborCode AS LaborCode,
      Employee AS Employee, 
      SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,
      SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS JTDLabCost,
      SUM(LD.BillExt) AS JTDLabBill
      FROM LD
      WHERE LD.TransDate <= @dtJTDDate   AND LD.WBS1 = @strTopWBS1  
      GROUP BY WBS1, WBS2, WBS3, LaborCode, Employee 

    UNION ALL
    SELECT
      TD.WBS1 AS WBS1,
      TD.WBS2 AS WBS2,
      TD.WBS3 AS WBS3, 
      TD.LaborCode AS LaborCode,
      TD.Employee AS Employee, 
      SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,
      SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS JTDLabCost,
      SUM(TD.BillExt) AS JTDLabBill
      FROM tkDetail AS TD 
        INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate AND TD.EmployeeCompany = TM.EmployeeCompany)
      WHERE  TD.TransDate <= @dtJTDDate AND TD.WBS1 = @strTopWBS1  
      GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.LaborCode, TD.Employee 

    UNION ALL
    SELECT
      TD.WBS1 AS WBS1,
      TD.WBS2 AS WBS2,
      TD.WBS3 AS WBS3, 
      TD.LaborCode AS LaborCode,
      TD.Employee AS Employee, 
      SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,
      SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS JTDLabCost,
      SUM(TD.BillExt) AS JTDLabBill
      FROM tsDetail AS TD 
        INNER JOIN tsControl AS TC ON (TD.Batch = TC.Batch AND TC.Posted = 'N')  
      WHERE  TD.TransDate <= @dtJTDDate AND TD.WBS1 = @strTopWBS1  
      GROUP BY WBS1, WBS2, WBS3, LaborCode, Employee 


  INSERT @tabLD
    (PlanID,
     OutlineNumber,
     AssignmentID,
     PeriodHrs,
     JTDLabCost,
     JTDLabBill)      
    SELECT
      AT.PlanID AS PlanID,
      AT.OutlineNumber AS OutlineNumber,
      A.AssignmentID,
      SUM(PeriodHrs) AS PeriodHrs,
      SUM(LD.JTDLabCost) AS JTDLabCost,
      SUM(LD.JTDLabBill) AS JTDLabBill
    FROM @tabAssignment AS A
      INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
      INNER JOIN RPTask AS TT ON A.PlanID = TT.PlanID AND AT.WBS1 = TT.WBS1  AND TT.OutlineLevel = 0
      INNER JOIN @tabJTDLD LD 
      ON  LD.WBS1 = AT.WBS1 
        AND LD.WBS2 LIKE (ISNULL(AT.WBS2, '%'))
        AND LD.WBS3 LIKE (ISNULL(AT.WBS3 + '%', '%'))
        AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(AT.LaborCode, '%'))
     AND A.ResourceID = LD.Employee
    WHERE LD.WBS1 = TT.WBS1   
    GROUP BY AT.PlanID, AT.OutlineNumber, AT.WBS1, A.AssignmentID

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  IF ((@strMode = 'C' AND @bitIsLeafTask = 1) OR (@strMode = 'S' AND DATALENGTH(@strResourceType) > 0))
    BEGIN /* BEGIN Assignment Rows */
      INSERT @tabProjectView (
        RowID,
        ParentRowID,
        PlanID,
        TaskID,
        AssignmentID,	
        ResourceID,
        GenericResourceID,
        WBS1,
        WBS2,
        WBS3,
        LaborCode,
        ParentOutlineNumber,
        OutlineNumber,
        OutlineLevel,
        HasAssignments,
        HasChildren,
        LeafNode,
        RowLevel,
        Name,
        StartDate,
        EndDate,
        PlannedHrs,
        ETCHrs,
        JTDHrs,
        EACHrs,
        HasPhoto,
		PhotoModDate,
        Org,
        OrgName,
        ProjMgr,
        PMFullName,
        ClientName,
        RT_Status,
        HardBooked,
        ContractStartDate,
        ContractEndDate,
		UtilizationScheduleFlg,
        MinASGDate,
        MaxASGDate,
        ResSortSeq,
        CalculatedPct,
        TopWBS1,
        TopName,
        VersionID,
        BaselineHrs,
        BaselineCost,
        BaselineBill,
        PlannedCost,
        PlannedBill,
        JTDCost,
        JTDBill,
        ETCCost,
        ETCBill,
        EACCost,
        EACBill,
        PlannedLessJTDCost,
        PlannedLessJTDBill,
        PlannedLessEACCost,
        PlannedLessEACBill,
        ContractCost,
        ContractBill,
        ContractLessEACCost,
        ContractLessEACBill,
        ContractLessJTDCost,
        ContractLessJTDBill,
        EACOverheadCost,
        EACOverheadBill,
        EACProfitCost,
        EACProfitBill,
        EACProfitPctCost,
        EACProfitPctBill
      )
      SELECT
        CASE WHEN XT.ResourceID IS NULL THEN 'G~' + XT.GenericResourceID ELSE 'E~' + XT.ResourceID END + '|' + XT.TaskID AS RowID,
        @strParentRowID AS ParentRowID,
        P.PlanID AS PlanID,
        XT.TaskID AS TaskID,
        XT.AssignmentID AS AssignmentID,
        XT.ResourceID AS ResourceID,
        XT.GenericResourceID AS GenericResourceID,
        XT.WBS1 AS WBS1,
        XT.WBS2 AS WBS2,
        XT.WBS3 AS WBS3,
        XT.LaborCode AS LaborCode,
        XT.ParentOutlineNumber AS ParentOutlineNumber,
        XT.OutlineNumber AS OutlineNumber,
        XT.OutlineLevel AS OutlineLevel,
        0 AS HasAssignments,
        0 AS HasChildren,
        1 AS LeafNode,
        XT.OutlineLevel + 1 AS RowLevel,
        CASE 
          WHEN XT.ResourceID IS NULL 
          THEN GR.Name 
          ELSE CONVERT(nvarchar(255), ISNULL(ISNULL(EM.PreferredName, EM.FirstName), N'') + ISNULL((N' ' + EM.LastName), N'') + ISNULL(', ' + EMSuffix.Suffix, '')) 
        END AS Name,
        PlanStart AS StartDate,
        PlanEnd AS EndDate,
        XT.PlannedHrs AS PlannedHrs,
        XT.ETCHrs AS ETCHrs,
        XT.JTDHrs AS JTDHrs,
        XT.JTDHrs + XT.ETCHrs AS EACHrs,
        CASE WHEN XT.ResourceID IS NOT NULL AND EP.Photo IS NOT NULL THEN 1 ELSE 0 END AS HasPhoto,
		CASE WHEN XT.ResourceID IS NOT NULL and EP.Photo IS NOT NULL THEN EP.ModDate END as PhotoModDate,
        ISNULL(XT.Org, '') AS Org,
        ISNULL(O.Name, '') AS OrgName,
        ISNULL(XT.ProjMgr, '') AS ProjMgr,
        CONVERT(nvarchar(255), ISNULL(ISNULL(PM.PreferredName, PM.FirstName), '') + ISNULL((' ' + PM.LastName), '') + ISNULL(', ' + PMSuffix.Suffix, '')) AS PMFullName,
        ISNULL(CL.Name, '') AS ClientName,
        ISNULL((CASE WHEN XT.ResourceID IS NULL THEN GR.Status ELSE EM.Status END), '') AS RT_Status,
        XT.ASG_HardBooked AS HardBooked,
        NULL AS ContractStartDate,
        NULL AS ContractEndDate,
		NULL AS UtilizationScheduleFlg,
        XT.StartDate AS MinASGDate,
        XT.EndDate AS MaxASGDate,
        CASE
          WHEN XT.ResourceID IS NOT NULL THEN 1
          WHEN XT.GenericResourceID IS NOT NULL THEN 2
          ELSE 255
        END AS ResSortSeq,
        CASE WHEN XT.JTDHrs + XT.ETCHrs > 0 THEN ROUND((CONVERT(float,XT.JTDHrs)/CONVERT(float,XT.JTDHrs + XT.ETCHrs)) * 100,0) ELSE 0 END AS CalculatedPct,
        @strTopWBS1 AS TopWBS1,
        @strTopName AS TopName,
        @strVersionID AS VersionID,
        XT.BaselineHrs AS BaselineHrs,
        0 AS BaselineCost,
        0 AS BaselineBill,
        0 AS PlannedCost,
        0 AS PlannedBill,
        0 AS JTDCost,
        0 AS JTDBill,
        0 AS ETCCost,
        0 AS ETCBill,
        0 AS EACCost,
        0 AS EACBill,
        0 AS PlannedLessJTDCost,
        0 AS PlannedLessJTDBill,
        0 AS PlannedLessEACCost,
        0 AS PlannedLessEACBill,
        0 AS ContractCost,
        0 AS ContractBill,
        0 AS ContractLessEACCost,
        0 AS ContractLessEACBill,
        0 AS ContractLessJTDCost,
        0 AS ContractLessJTDBill,
        0 AS EACOverheadCost,
        0 AS EACOverheadBill,
        0 AS EACProfitCost,
        0 AS EACProfitBill,
        0 AS EACProfitPctCost,
        0 AS EACProfitPctBill
      FROM ( /* XT */
        SELECT
          T.PlanID,
          T.TaskID,
          T.WBS1,
          T.WBS2,
          T.WBS3,
          T.LaborCode,
          T.ParentOutlineNumber,
          T.OutlineNumber,
          T.OutlineLevel,
          T.ChildrenCount,
          T.Name,
          T.Org,
          T.ClientID,
          T.ProjMgr,
          T.StartDate AS PlanStart,
          T.EndDate AS PlanEnd,
          A.StartDate,
          A.EndDate,
          A.AssignmentID,
          A.ResourceID,
          A.GenericResourceID,
          CASE SIGN(MIN(A.HardBookedFlg) + MAX(A.HardBookedFlg))
            WHEN 0 THEN 'B'
            WHEN 1 THEN 'Y'
            WHEN -1 THEN 'N'
          END AS ASG_HardBooked,
          ROUND(ISNULL(SUM(ETCHrs), 0.0000), @siHrDecimals) AS ETCHrs,
          ROUND(ISNULL(SUM(PlannedHrs), 0.0000), @siHrDecimals) AS PlannedHrs,
          ROUND(ISNULL(SUM(JTDHrs), 0.0000), @siHrDecimals) AS JTDHrs,
          ROUND(ISNULL(SUM(BaselineHrs), 0.0000), @siHrDecimals) AS BaselineHrs
          FROM @tabAssignment A 
            INNER JOIN RPTask AS T ON T.TaskID = A.TaskID
            LEFT JOIN ( /* XE */
              SELECT
                PlanID AS PlanID,
                AssignmentID AS AssignmentID,
                ISNULL(SUM(TETCHrs), 0.0000) AS ETCHrs,
                ISNULL(SUM(PTPDHrs), 0.0000) AS PlannedHrs,
                ISNULL(SUM(JTDHrs), 0.0000) AS JTDHrs,
                ISNULL(SUM(BaselineHrs), 0.0000) AS BaselineHrs
                FROM ( /* ZE */
                  SELECT 
                    PlanID AS PlanID, AssignmentID AS AssignmentID,
                    ETCHrs AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
                    0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabTotalETC
                  UNION ALL
                  SELECT 
                    PlanID AS PlanID, AssignmentID AS AssignmentID,
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                    PlannedHrs AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
                    0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabLabTPD
                  UNION ALL
                  SELECT 
                    PlanID AS PlanID, AssignmentID AS AssignmentID,
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
                    0 AS JTDHrs, PlannedHrs AS BaselineHrs
                    FROM @tabLabBaselineTPD
                  UNION ALL
                  SELECT 
                    PlanID AS PlanID, AssignmentID AS AssignmentID,
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, PeriodHrs AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabLD
                ) AS ZE
                  GROUP BY ZE.PlanID, ZE.AssignmentID
            ) AS XE ON XE.PlanID = A.PlanID AND XE.AssignmentID = A.AssignmentID
          WHERE T.TaskID = @strTaskID AND
            ISNULL(T.Status,'') LIKE CASE WHEN @bActiveWBSOnly = 1 THEN 'A' ELSE '%' END
          GROUP BY T.PlanID, T.TaskID, T.WBS1, T.WBS2, T.WBS3, T.LaborCode, T.ParentOutlineNumber, T.OutlineNumber, T.OutlineLevel, T.ChildrenCount, T.Name, T.Org, T.ClientID,
            T.ProjMgr,T.StartDate,T.EndDate,A.StartDate,A.EndDate,A.AssignmentID,A.ResourceID,A.GenericResourceID
      ) AS XT
        INNER JOIN RPPlan AS P ON P.PlanID = XT.PlanID
        LEFT JOIN EM ON EM.Employee = XT.ResourceID
        LEFT JOIN CFGSuffix EMSuffix ON EMSuffix.Code = EM.Suffix
        LEFT JOIN EMPhoto EP ON EM.Employee = EP.Employee
        LEFT JOIN GR ON GR.Code = XT.GenericResourceID
        LEFT JOIN CL ON XT.ClientID = CL.ClientID
        LEFT JOIN EM AS PM ON XT.ProjMgr = PM.Employee
        LEFT JOIN CFGSuffix PMSuffix ON PMSuffix.Code = PM.Suffix
        LEFT JOIN Organization AS O ON XT.Org = O.Org
    END /* END Assignment Rows */

  ELSE /* WBS Rows */
    IF (@strMode = 'S')
      BEGIN
        INSERT @tabProjectView (
        RowID,
        ParentRowID,
        PlanID,
        TaskID,
        AssignmentID,
        ResourceID,
        GenericResourceID,
        WBS1,
        WBS3,
        WBS2,
        LaborCode,
        ParentOutlineNumber,
        OutlineNumber,
        OutlineLevel,
        HasAssignments,
        HasChildren,
        LeafNode,
        RowLevel,
        Name,
        StartDate,
        EndDate,
        PlannedHrs,
        ETCHrs,
        JTDHrs,
        EACHrs,
        HasPhoto,
		PhotoModDate,
        Org,
        OrgName,
        ProjMgr,
        PMFullName,
        ClientName,
        RT_Status,
        HardBooked,
        ContractStartDate,
        ContractEndDate,
		UtilizationScheduleFlg,
        MinASGDate,
        MaxASGDate,
        ResSortSeq,
        CalculatedPct,
        TopWBS1,
        TopName,
        VersionID,
        BaselineHrs,
        BaselineCost,
        BaselineBill,
        PlannedCost,
        PlannedBill,
        JTDCost,
        JTDBill,
        ETCCost,
        ETCBill,
        EACCost,
        EACBill,
        PlannedLessJTDCost,
        PlannedLessJTDBill,
        PlannedLessEACCost,
        PlannedLessEACBill,
        ContractCost,
        ContractBill,
        ContractLessEACCost,
        ContractLessEACBill,
        ContractLessJTDCost,
        ContractLessJTDBill,
        EACOverheadCost,
        EACOverheadBill,
        EACProfitCost,
        EACProfitBill,
        EACProfitPctCost,
        EACProfitPctBill
      )
        SELECT
          '|' + YT.TaskID AS RowID,
          @strParentRowID AS ParentRowID,
          YT.PlanID AS PlanID,
          YT.TaskID AS TaskID,
          null AS AssignmentID,
          null AS ResourceID,
          null AS GenericResourceID,
          YT.WBS1 AS WBS1,
          YT.WBS2 AS WBS2,
          YT.WBS3 AS WBS3,
          YT.LaborCode AS LaborCode,
          YT.ParentOutlineNumber AS ParentOutlineNumber,
          YT.OutlineNumber AS OutlineNumber,
          YT.OutlineLevel AS OutlineLevel,
          CASE
            WHEN MinAssignmentID IS NOT NULL
            THEN CONVERT(bit, 1)
            ELSE CONVERT(bit, 0)
          END AS HasAssignments,
          CASE WHEN YT.ChildrenCount > 0 THEN CONVERT(bit, 1) ELSE CASE WHEN EXISTS (SELECT 'X' FROM RPAssignment WHERE TaskID = YT.TaskID) THEN CONVERT(bit,1) ELSE CONVERT(bit, 0) END END AS HasChildren,
          CASE WHEN YT.ChildrenCount > 0 THEN CONVERT(bit, 0) ELSE CONVERT(bit,1) END AS LeafNode,
          YT.OutlineLevel AS RowLevel,
          YT.Name AS Name,
          YT.StartDate AS StartDate,
          YT.EndDate AS EndDate,
          YT.PlannedHrs AS PlannedHrs,
          YT.ETCHrs AS ETCHrs, 
          YT.JTDHrs AS JTDHrs,
          YT.ETCHrs + YT.JTDHrs AS EACHrs,
          CASE WHEN PRA.Photo IS NOT NULL THEN 1 ELSE 0 END AS HasPhoto,
		  CASE WHEN PRA.Photo IS NOT NULL THEN PRA.ModDate END AS PhotoModDate,
          ISNULL(YT.Org, '') AS Org,
          ISNULL(O.Name, '') AS OrgName,
          ISNULL(YT.ProjMgr, '') AS ProjMgr,
          CONVERT(nvarchar(255), ISNULL(ISNULL(PM.PreferredName, PM.FirstName), '') + ISNULL((' ' + PM.LastName), '') + ISNULL(', ' + PMSuffix.Suffix, '')) AS PMFullName,
          ISNULL(CL.Name, '') AS ClientName,
          YT.Status AS RT_Status,
          ASG_HardBooked AS HardBooked,
          PR.StartDate AS ContractStartDate,
          COALESCE(PR.EndDate, PR.ActCompletionDate, PR.EstCompletionDate) AS ContractEndDate,
		  PR.UtilizationscheduleFlg AS UtilizationscheduleFlg,
          MinASGDate,
          MaxASGDate,
          0 AS ResSortSeq,
          CASE WHEN YT.JTDHrs + YT.ETCHrs > 0 THEN ROUND((CONVERT(float,YT.JTDHrs)/CONVERT(float,YT.JTDHrs + YT.ETCHrs)) * 100,0) ELSE 0 END AS CalculatedPct,
          @strTopWBS1 AS TopWBS1,
          @strTopName AS TopName,
          @strVersionID AS VersionID,
          YT.BaselineHrs AS BaselineHrs,
          0 AS BaselineCost,
          0 AS BaselineBill,
          0 AS PlannedCost,
          0 AS PlannedBill,
          0 AS JTDCost,
          0 AS JTDBill,
          0 AS ETCCost,
          0 AS ETCBill,
          0 AS EACCost,
          0 AS EACBill,
          0 AS PlannedLessJTDCost,
          0 AS PlannedLessJTDBill,
          0 AS PlannedLessEACCost,
          0 AS PlannedLessEACBill,
          0 AS ContractCost,
          0 AS ContractBill,
          0 AS ContractLessEACCost,
          0 AS ContractLessEACBill,
          0 AS ContractLessJTDCost,
          0 AS ContractLessJTDBill,
          0 AS EACOverheadCost,
          0 AS EACOverheadBill,
          0 AS EACProfitCost,
          0 AS EACProfitBill,
          0 AS EACProfitPctCost,
          0 AS EACProfitPctBill
        FROM ( /* YT */
          SELECT
            XT.PlanID,
            XT.TaskID,
            XT.WBS1,
            XT.WBS2,
            XT.WBS3,
            XT.LaborCode,
            XT.ParentOutlineNumber,
            XT.OutlineNumber,
            XT.OutlineLevel,
            XT.ChildrenCount,
            XT.Name,
            XT.Org,
            XT.ClientID,
            XT.ProjMgr,
            XT.StartDate,
            XT.EndDate,
            XT.Status,
            MAX(ETCHrs) AS ETCHrs,
            MAX(PlannedHrs) AS PlannedHrs,
            MAX(JTDHrs) AS JTDHrs,
            MAX(BaselineHrs) AS BaselineHrs,
            CASE SIGN(MIN(A.HardBookedFlg) + MAX(A.HardBookedFlg))
              WHEN 0 THEN 'B'
              WHEN 1 THEN 'Y'
              WHEN -1 THEN 'N'
            END AS ASG_HardBooked,
            MIN(A.AssignmentID) AS MinAssignmentID,
            MIN(A.StartDate) AS MinASGDate,
            MAX(A.EndDate) AS MaxASGDate
            FROM ( /* XT */
              SELECT
                T.PlanID,
                T.TaskID,
                T.WBS1,
                T.WBS2,
                T.WBS3,
                T.LaborCode,
                T.ParentOutlineNumber,
                T.OutlineNumber,
                T.OutlineLevel,
                T.ChildrenCount,
                Max(CASE WHEN T.LaborCode IS NOT NULL THEN L.LaborCodeName ELSE T.NAME END) AS Name, 
				T.Org,
                T.ClientID,
                T.ProjMgr,
                T.StartDate,
                T.EndDate,
                ISNULL(T.Status, '') AS Status,
                ROUND(ISNULL(SUM(ETCHrs), 0.0000), @siHrDecimals) AS ETCHrs,
                ROUND(ISNULL(SUM(PlannedHrs), 0.0000), @siHrDecimals) AS PlannedHrs,
                ROUND(ISNULL(SUM(JTDHrs), 0.0000), @siHrDecimals) AS JTDHrs,
                ROUND(ISNULL(SUM(BaselineHrs), 0.0000), @siHrDecimals) AS BaselineHrs
                FROM RPTask AS T
                  LEFT JOIN ( /* XE */
                    SELECT
                      PlanID AS PlanID,
                      OutlineNumber AS OutlineNumber,
                      ISNULL(SUM(TETCHrs), 0.0000) AS ETCHrs,
                      ISNULL(SUM(PTPDHrs), 0.0000) AS PlannedHrs,
                      ISNULL(SUM(JTDHrs), 0.0000) AS JTDHrs,
                      ISNULL(SUM(BaselineHrs), 0.0000) AS BaselineHrs
                      FROM ( /* ZE */
                        SELECT 
                          PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                          ETCHrs AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                          0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
                          0 AS JTDHrs, 0 AS BaselineHrs
                          FROM @tabTotalETC
                        UNION ALL
                        SELECT 
                          PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                          0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                          PlannedHrs AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
                          0 AS JTDHrs, 0 AS BaselineHrs
                          FROM @tabLabTPD
                        UNION ALL
                        SELECT 
                          PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                          0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                          0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
                          0 AS JTDHrs, PlannedHrs AS BaselineHrs
                          FROM @tabLabBaselineTPD
                        UNION ALL
                        SELECT 
                          PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                          0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                          0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, PeriodHrs AS JTDHrs, 0 AS BaselineHrs
                          FROM @tabLD
                      ) AS ZE
                      GROUP BY ZE.PlanID, ZE.OutlineNumber
                  ) AS XE ON XE.PlanID = T.PlanID AND XE.OutlineNumber LIKE T.OutlineNumber + '%'
				  CROSS APPLY dbo.stRP$tabLaborCode(T.PlanID,T.LaborCode) AS L
                    WHERE T.TaskID = @strTaskID AND T.PlanID = @strPlanID AND
                      ISNULL(T.Status,'') LIKE CASE WHEN @bActiveWBSOnly = 1 THEN 'A' ELSE '%' END
                    GROUP BY T.PlanID, T.TaskID, T.WBS1, T.WBS2, T.WBS3, T.LaborCode, T.ParentOutlineNumber, T.OutlineNumber, T.OutlineLevel, T.ChildrenCount,
                      T.Name, T.Org, T.ClientID, T.ProjMgr, T.StartDate, T.EndDate, T.Status
            ) AS XT
              LEFT JOIN @tabAssignment A ON A.PlanID = XT.PlanID AND A.OutlineNumber LIKE XT.OutlineNumber + '%'
              GROUP BY XT.PlanID,XT.TaskID,XT.WBS1,XT.WBS2,XT.WBS3,XT.LaborCode,XT.ParentOutlineNumber,XT.OutlineNumber,XT.OutlineLevel,XT.ChildrenCount,XT.Name,XT.Org,XT.ClientID,XT.ProjMgr,XT.StartDate,XT.EndDate,XT.Status
        ) AS YT
          INNER JOIN RPPlan P ON P.PlanID = YT.PlanID
          LEFT JOIN CL ON YT.ClientID = CL.ClientID
          LEFT JOIN EM AS PM ON YT.ProjMgr = PM.Employee
          LEFT JOIN CFGSuffix PMSuffix ON PMSuffix.Code = PM.Suffix
          LEFT JOIN Organization AS O ON YT.Org = O.Org
          LEFT JOIN PR AS PR ON YT.WBS1 = PR.WBS1 AND ISNULL(YT.WBS2, ' ') = PR.WBS2 AND ISNULL(YT.WBS3, ' ') = PR.WBS3
		  LEFT JOIN PRAdditionalData PRA on PRA.WBS1=YT.WBS1 AND PRA.WBS2=' '  
      END /* END IF (@strMode = 'S') */

    ELSE IF (@strMode = 'C')
      BEGIN
        INSERT @tabProjectView (
          RowID,
          ParentRowID,
          PlanID,
          TaskID,
          AssignmentID,
          ResourceID,
          GenericResourceID,
          WBS1,
          WBS2,
          WBS3,
          LaborCode,
          ParentOutlineNumber,
          OutlineNumber,
          OutlineLevel,
          HasAssignments,
          HasChildren,
          LeafNode,
          RowLevel,
          Name,
          StartDate,
          EndDate,
          PlannedHrs,
          ETCHrs,
          JTDHrs,
          EACHrs,
          HasPhoto,
		  PhotoModDate,
          Org,
          OrgName,
          ProjMgr,
          PMFullName,
          ClientName,
          RT_Status,
          HardBooked,
          ContractStartDate,
          ContractEndDate,
		  UtilizationscheduleFlg,
          MinASGDate,
          MaxASGDate,
          ResSortSeq,
          CalculatedPct,
          TopWBS1,
          TopName,
          VersionID,
          BaselineHrs,
          BaselineCost,
          BaselineBill,
          PlannedCost,
          PlannedBill,
          JTDCost,
          JTDBill,
          ETCCost,
          ETCBill,
          EACCost,
          EACBill,
          PlannedLessJTDCost,
          PlannedLessJTDBill,
          PlannedLessEACCost,
          PlannedLessEACBill,
          ContractCost,
          ContractBill,
          ContractLessEACCost,
          ContractLessEACBill,
          ContractLessJTDCost,
          ContractLessJTDBill,
          EACOverheadCost,
          EACOverheadBill,
          EACProfitCost,
          EACProfitBill,
          EACProfitPctCost,
          EACProfitPctBill
        )
        SELECT
          '|' + YT.TaskID AS RowID,
          @strParentRowID AS ParentRowID,
          YT.PlanID AS PlanID,
          YT.TaskID AS TaskID,
          null AS AssignmentID,
          null AS ResourceID,
          null AS GenericResourceID,
          YT.WBS1 AS WBS1,
          YT.WBS2 AS WBS2,
          YT.WBS3 AS WBS3,
          YT.LaborCode AS LaborCode,
          YT.ParentOutlineNumber,
          YT.OutlineNumber AS OutlineNumber,
          YT.OutlineLevel AS OutlineLevel,
          CASE
            WHEN MinAssignmentID IS NOT NULL
            THEN CONVERT(bit, 1)
            ELSE CONVERT(bit, 0)
          END AS HasAssignments,
          CASE WHEN YT.ChildrenCount > 0 THEN CONVERT(bit, 1) ELSE CASE WHEN EXISTS (SELECT 'X' FROM RPAssignment WHERE TaskID = YT.TaskID) THEN CONVERT(bit,1) ELSE CONVERT(bit, 0) END END AS HasChildren,
          CASE WHEN YT.ChildrenCount > 0 THEN CONVERT(bit, 0) ELSE CONVERT(bit,1) END AS LeafNode,
          YT.OutlineLevel AS RowLevel,
          YT.Name AS Name,
          YT.StartDate AS StartDate,
          YT.EndDate AS EndDate,
          YT.PlannedHrs AS PlannedHrs,
          YT.ETCHrs AS ETCHrs, 
          YT.JTDHrs AS JTDHrs,
          YT.ETCHrs + YT.JTDHrs AS EACHrs,
          0 AS HasPhoto,
		  null as PhotoModDate,
          ISNULL(YT.Org, '') AS Org,
          ISNULL(O.Name, '') AS OrgName,
          ISNULL(YT.ProjMgr, '') AS ProjMgr,
          CONVERT(nvarchar(255), ISNULL(ISNULL(PM.PreferredName, PM.FirstName), '') + ISNULL((' ' + PM.LastName), '') + ISNULL(', ' + PMSuffix.Suffix, '')) AS PMFullName,
          ISNULL(CL.Name, '') AS ClientName,
          YT.Status AS RT_Status,
          ASG_HardBooked AS HardBooked,
          PR.StartDate AS ContractStartDate,
          COALESCE(PR.EndDate, PR.ActCompletionDate, PR.EstCompletionDate) AS ContractEndDate,
		  PR.UtilizationscheduleFlg AS UtilizationscheduleFlg,
          MinASGDate,
          MaxASGDate,
          0 AS ResSortSeq,
          CASE WHEN YT.JTDHrs + YT.ETCHrs > 0 THEN ROUND((CONVERT(float,YT.JTDHrs)/CONVERT(float,YT.JTDHrs + YT.ETCHrs)) * 100,0) ELSE 0 END AS CalculatedPct,
          @strTopWBS1 AS TopWBS1,
          @strTopName AS TopName,
          @strVersionID AS VersionID, 
          YT.BaselineHrs AS BaselineHrs,
          0 AS BaselineCost,
          0 AS BaselineBill,
          0 AS PlannedCost,
          0 AS PlannedBill,
          0 AS JTDCost,
          0 AS JTDBill,
          0 AS ETCCost,
          0 AS ETCBill,
          0 AS EACCost,
          0 AS EACBill,
          0 AS PlannedLessJTDCost,
          0 AS PlannedLessJTDBill,
          0 AS PlannedLessEACCost,
          0 AS PlannedLessEACBill,
          0 AS ContractCost,
          0 AS ContractBill,
          0 AS ContractLessEACCost,
          0 AS ContractLessEACBill,
          0 AS ContractLessJTDCost,
          0 AS ContractLessJTDBill,
          0 AS EACOverheadCost,
          0 AS EACOverheadBill,
          0 AS EACProfitCost,
          0 AS EACProfitBill,
          0 AS EACProfitPctCost,
          0 AS EACProfitPctBill
        FROM (SELECT
            XT.PlanID,
            XT.TaskID,
            XT.WBS1,
            XT.WBS2,
            XT.WBS3,
            XT.LaborCode,
            XT.ParentOutlineNumber,
            XT.OutlineNumber,
            XT.OutlineLevel,
            XT.ChildrenCount,
            XT.Name,
            XT.Org,
            XT.ClientID,
            XT.ProjMgr,
            XT.StartDate,
            XT.EndDate,
            XT.Status,
            MAX(ETCHrs) AS ETCHrs,
            MAX(PlannedHrs) AS PlannedHrs,
            MAX(JTDHrs) AS JTDHrs,
            MAX(BaselineHrs) AS BaselineHrs,
            CASE SIGN(MIN(A.HardBookedFlg) + MAX(A.HardBookedFlg))
              WHEN 0 THEN 'B'
              WHEN 1 THEN 'Y'
              WHEN -1 THEN 'N'
            END AS ASG_HardBooked,
            MIN(A.AssignmentID) AS MinAssignmentID,
            MIN(A.StartDate) AS MinASGDate,
            MAX(A.EndDate) AS MaxASGDate
        FROM (SELECT
            T.PlanID,
            T.TaskID,
            T.WBS1,
            T.WBS2,
            T.WBS3,
            T.LaborCode,
            T.ParentOutlineNumber,
            T.OutlineNumber,
            T.OutlineLevel,
            T.ChildrenCount,
            Max(CASE WHEN T.LaborCode IS NOT NULL THEN L.LaborCodeName ELSE T.NAME END) AS Name, 
            T.Org,
            T.ClientID,
            T.ProjMgr,
            T.StartDate,
            T.EndDate,
            ISNULL(T.Status, '') AS Status,
            ROUND(ISNULL(SUM(ETCHrs), 0.0000), @siHrDecimals) AS ETCHrs,
            ROUND(ISNULL(SUM(PlannedHrs), 0.0000), @siHrDecimals) AS PlannedHrs,
            ROUND(ISNULL(SUM(JTDHrs), 0.0000), @siHrDecimals) AS JTDHrs,
            ROUND(ISNULL(SUM(BaselineHrs), 0.0000), @siHrDecimals) AS BaselineHrs
        FROM RPTask AS T
          INNER JOIN RPTask AS PT ON PT.PlanID = T.PlanID AND PT.OutlineNumber = T.ParentOutlineNumber
          LEFT JOIN (
              SELECT
                PlanID AS PlanID,
                OutlineNumber AS OutlineNumber,
                ISNULL(SUM(TETCHrs), 0.0000) AS ETCHrs,
                ISNULL(SUM(PTPDHrs), 0.0000) AS PlannedHrs,
                ISNULL(SUM(JTDHrs), 0.0000) AS JTDHrs,
                ISNULL(SUM(BaselineHrs), 0.0000) AS BaselineHrs
                FROM (
                  SELECT 
                    PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                    ETCHrs AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
                    0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabTotalETC
                  UNION ALL
                  SELECT 
                    PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                    PlannedHrs AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
                    0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabLabTPD
                  UNION ALL
                  SELECT 
                    PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
                    0 AS JTDHrs, PlannedHrs AS BaselineHrs
                    FROM @tabLabBaselineTPD
                  UNION ALL
                  SELECT 
                    PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, PeriodHrs AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabLD
                ) AS ZE
                GROUP BY ZE.PlanID, ZE.OutlineNumber
            ) AS XE ON XE.PlanID = T.PlanID AND XE.OutlineNumber LIKE T.OutlineNumber + '%'
			CROSS APPLY dbo.stRP$tabLaborCode(T.PlanID,T.LaborCode) AS L
        WHERE PT.TaskID = @strTaskID AND PT.PlanID = @strPlanID AND
          ISNULL(@strTaskStatus,'') LIKE CASE WHEN @bActiveWBSOnly = 1 THEN 'A' ELSE '%' END AND
          ISNULL(T.Status,'') LIKE CASE WHEN @bActiveWBSOnly = 1 THEN 'A' ELSE '%' END
        GROUP BY T.PlanID,T.TaskID,T.WBS1,T.WBS2,T.WBS3, T.LaborCode, T.ParentOutlineNumber,T.OutlineNumber,T.OutlineLevel,T.ChildrenCount,T.Name,T.Org,T.ClientID,T.ProjMgr,T.StartDate,T.EndDate,T.Status) AS XT
          LEFT JOIN @tabAssignment A ON A.PlanID = XT.PlanID AND A.OutlineNumber LIKE XT.OutlineNumber + '%'
          GROUP BY XT.PlanID, XT.TaskID, XT.WBS1, XT.WBS2, XT.WBS3, XT.LaborCode, XT.ParentOutlineNumber, XT.OutlineNumber, XT.OutlineLevel, XT.ChildrenCount, XT.Name,
            XT.Org,XT.ClientID,XT.ProjMgr,XT.StartDate,XT.EndDate,XT.Status
        ) AS YT
          INNER JOIN RPPlan P ON P.PlanID = YT.PlanID
          LEFT JOIN CL ON YT.ClientID = CL.ClientID
          LEFT JOIN EM AS PM ON YT.ProjMgr = PM.Employee
          LEFT JOIN CFGSuffix PMSuffix ON PMSuffix.Code = PM.Suffix
          LEFT JOIN Organization AS O ON YT.Org = O.Org
          LEFT JOIN PR AS PR ON YT.WBS1 = PR.WBS1 AND ISNULL(YT.WBS2, ' ') = PR.WBS2 AND ISNULL(YT.WBS3, ' ') = PR.WBS3
      END /* END ELSE IF (@strMode = 'C') */
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
