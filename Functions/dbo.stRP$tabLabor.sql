SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabLabor](
  @strRowID nvarchar(255),
  @strMode varchar(1), /* S = Self, C = Children */
  @bActiveWBSOnly bit = 0
)
  RETURNS @tabLabor TABLE (

    RowID nvarchar(255) COLLATE database_default,
    ParentRowID nvarchar(255) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ResourceID nvarchar(20) COLLATE database_default,
    GenericResourceID nvarchar(20) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int,
    RowLevel int,
    Name nvarchar(255) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
		UtilizationScheduleFlg varchar(1),
    MinASGDate datetime,
    MaxASGDate datetime,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    ChargeType varchar(1) COLLATE database_default,
    ProjectType nvarchar(10) COLLATE database_default,
    WBSType varchar(4) COLLATE database_default,
    BudgetType varchar(1) COLLATE database_default,
    CostCurrencyCode varchar(3) COLLATE database_default,
    BillCurrencyCode varchar(3) COLLATE database_default,
    Org nvarchar(30) COLLATE database_default,
    OrgName nvarchar(100) COLLATE database_default,
    ProjMgr nvarchar(20) COLLATE database_default,
    PMFullName nvarchar(255) COLLATE database_default,
    ClientName nvarchar(100) COLLATE database_default,
    OpportunityID varchar(32) COLLATE database_default,
    RT_Status varchar(1) COLLATE database_default,
    TopWBS1 nvarchar(30) COLLATE database_default,
    TopName nvarchar(255) COLLATE database_default,
		HasPhoto bit,
		PhotoModDate datetime,
    HasNotes varchar(1) COLLATE database_default,
    HasAssignments bit,
    HasChildren bit,
    LeafNode bit,
    ResSortSeq tinyint,
    HardBooked varchar(1) COLLATE database_default,

    ContractStartDate datetime,
    ContractEndDate datetime,
    
    OverheadPct decimal(19,4),
    BaselineHrs decimal(19,4),
    BaselineETCHrs decimal(19,4),
    BaselineEACHrs decimal(19,4),
    BaselineCost decimal(19,4),
    BaselineBill decimal(19,4),
    JTDHrs decimal(19,4),
    JTDCost decimal(19,4),
    JTDBill decimal(19,4),
    JTDOHCost decimal(19,4),
    ContractCost decimal(19,4),
    ContractBill decimal(19,4),

    PlannedHrs decimal(19,4),
    PlannedCost decimal(19,4),
    PlannedBill decimal(19,4),
    ETCHrs decimal(19,4),
    ETCCost decimal(19,4),
    ETCBill decimal(19,4),
    EACHrs decimal(19,4),
    EACCost decimal(19,4),
    EACBill decimal(19,4),

    CalcPctComplCost decimal(19,4),
    CalcPctComplBill decimal(19,4),
    ContractLessJTDBill decimal(19,4),
    ContractLessEACBill decimal(19,4),

    PlannedLessJTDHrs decimal(19,4),
    PlannedLessJTDCost decimal(19,4),
    PlannedLessJTDBill decimal(19,4),
    PlannedLessEACHrs decimal(19,4),
    PlannedLessEACCost decimal(19,4),
    PlannedLessEACBill decimal(19,4),

    EACMultCost decimal(19,4),
    EACOverheadCost decimal(19,4),
    EACProfitCost decimal(19,4),
    EACProfitPctCost decimal(19,4)

  )

 
 BEGIN

/**************************************************************************************************************************/
--
-- Huge Assumptions:
--
-- 1. In Non-Vision worlds (e.g. Costpoint)
--    1.1 WBS structures will be imported into PR, RPTask, and PNTask tables.
--        1.1.1 PR table has rows with WBS1, WBS2, WBS3.
--        1.1.2 RPTask and PNTable tables also have WBS1, WBS2, WBS3, but can be indented using OutlineNumber.
--        1.1.3 RPTask and PNTable tables can have up to 15 indent levels (just like currently in ngRP).
--    1.2 WBS Numbers will be in WBS1, WBS2, WBS3 
--        (same rules as currently in Vision, such as WBS2 = <blank>, WBS3 = <blank> for top most WBS row)
--    1.3 WBS3 will cover WBS Level 3 through 15 (e.g. WBS3 = {1, 1.1, 1.1.1, 1.1.1.1).
--    1.4 JTD data will be at the lowest WBS level (e.g. JTD could be at level 15). 
--    1.5 JTD is matched using WBS1, WBS2, WBS3, Employee.
--    1.6 Assignments will be at lowest WBS level 
--        (e.g. Assignment could be at level 15, same level as in the case of JTD).
--
-- 2. In Vision world
--    2.1 PR, RPTask, and PNTask tables have only 3 levels.
--    2.2 JTD data will be at the lowest WBS level.
--    2.3 Assignments will be at lowest WBS level.
--
/**************************************************************************************************************************/

  DECLARE @_SectionSign nchar = NCHAR(167) -- N'§'

  DECLARE @strCompany nvarchar(14)
  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)
  DECLARE @strTaskID varchar(32)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTopWBS1 nvarchar(30)
  DECLARE @strTopName nvarchar(255)
  DECLARE @strInputType varchar(1) = ''
  DECLARE @strResourceType varchar(1)
  DECLARE @strIDPrefix nvarchar(255)
  DECLARE @strResourceID nvarchar(20)
  DECLARE @strGenericResourceID nvarchar(20)
  DECLARE @strParentRowID nvarchar(255) = ''
  DECLARE @strTaskStatus varchar(1) = ''
  DECLARE @strWBS1 nvarchar(30)
  DECLARE @strWBS2 nvarchar(30) = ' '
  DECLARE @strWBS3 nvarchar(30) = ' '
  DECLARE @strLaborCode nvarchar(14) = NULL
  DECLARE @strWBS1WBS2WBS3 nvarchar(92)
  DECLARE @strOHProcedure varchar(1)
  DECLARE @strOrgOHAllocMethod varchar(1)
  DECLARE @strSysOHAllocMethod varchar(1)
  DECLARE @strDefBudgetType varchar(1)

  DECLARE @decBudOHRate decimal(19,4)
  DECLARE @decOrgOHRate decimal(19,4)
  DECLARE @decOrgOHProvRate decimal(19,4)
  DECLARE @decSysOHRate decimal(19,4)
  DECLARE @decSysOHProvRate decimal(19,4)
  DECLARE @decOHRate decimal(19,4)

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
 
  DECLARE @intHrDecimals int /* CFGRMSettings.HrDecimals */
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

  DECLARE @bitIsLeafTask bit

  DECLARE @intActivePeriod int

  -- Declare Temp tables.

  DECLARE @tabWBS TABLE (
    RowID int IDENTITY(1,1),
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    WBS1WBS2WBS3 nvarchar(92) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    Name nvarchar(255) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChildrenCount int,
    OutlineLevel int,
    ChargeType varchar(1) COLLATE database_default,
    ProjectType nvarchar(10) COLLATE database_default,
    WBSType varchar(4) COLLATE database_default,
    BudgetType varchar(1) COLLATE database_default,
    CostCurrencyCode varchar(3) COLLATE database_default,
    BillCurrencyCode varchar(3) COLLATE database_default,
    Org nvarchar(30) COLLATE database_default,
    ClientID varchar(32) COLLATE database_default,
    ProjMgr nvarchar(20) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
		UtilizationScheduleFlg varchar(1),
    Status varchar(1) COLLATE database_default,
    HasNotes varchar(1) COLLATE database_default,
    ContractStartDate datetime,
    ContractEndDate datetime,
    OverheadPct decimal(19,4),
    BaselineLaborHrs decimal(19,4),
    BaselineLabCost decimal(19,4),
    BaselineLabBill decimal(19,4),
    FeeLabCost decimal(19,4),
    FeeLabBill decimal(19,4)
    UNIQUE (RowID, WBS1, WBS2, WBS3, ParentOutlineNumber, OutlineNumber, Status)
  )

  DECLARE @tabAssignment TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ResourceID varchar(32) COLLATE database_default,
    GenericResourceID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    CostRate decimal(19,4),
    BillRate decimal(19,4),
    HardBookedFlg int,
    OutlineNumber varchar(255) COLLATE database_default,
    BaselineHrs decimal(19,4),
    BaselineCost decimal(19,4),
    BaselineBill decimal(19,4)
    UNIQUE(PlanID, TaskID, AssignmentID, OutlineNumber, ResourceID, GenericResourceID)
  )

  DECLARE @tabRes TABLE (
    RowID int IDENTITY(1,1),
    OutlineNumber varchar(255) COLLATE database_default,
    Employee Nvarchar(20) COLLATE database_default,
    GenericResourceID Nvarchar(20) COLLATE database_default
    UNIQUE(RowID, OutlineNumber, Employee, GenericResourceID)
  )

  DECLARE @tabLD TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    Employee nvarchar(20) COLLATE database_default,
    TransDate datetime,
    JTDHrs decimal(19,4),
    JTDCost decimal(19,4),	
    JTDBill decimal(19,4)
    UNIQUE (RowID, WBS1, WBS2, WBS3, Employee)
  )

   DECLARE @tabLDTask TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    Employee nvarchar(20) COLLATE database_default,
    TransDate datetime,
    JTDHrs decimal(19,4),
    JTDCost decimal(19,4),	
    JTDBill decimal(19,4)
    UNIQUE (RowID, WBS1, WBS2, WBS3, OutlineNumber, Employee)
  )

  DECLARE @tabOH TABLE (
    RowID  int IDENTITY(1,1),
    OutlineNumber varchar(255) COLLATE database_default,
    MaxDate datetime,
    OHCost decimal(19,4),
    Flg bit
    UNIQUE(RowID, OutlineNumber, Flg)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length
    FROM CFGFormat

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Get RM Settings.
  
  SELECT 
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE())))
    FROM CFGRMSettings

  SELECT @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)
  
  select @intActivePeriod =  IsNULL(CurrentPeriod,0) from FW_CFGSystem  

  if @intActivePeriod = 0  
    begin
    select @intActivePeriod =  max( period) from cfgdates  
    end
  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. For an Assignment row, @strRowID = 'E~<RPAssignment.ResourceID>|<RPTask.TaskID>
  --   2. For an Assignment row, @strRowID = 'G~<RPAssignment.GenericResourceID>|<RPTask.TaskID>'
  --   3. For a WBS row, @strRowID = '|<RPTask.TaskID>'
  --   4. For a WBS row, @strRowID = '|<PR.WBS1>§<PR.WBS2>§<PR.WBS3>'

  SET @strInputType = 
    CASE
      WHEN CHARINDEX(@_SectionSign, @strRowID) > 0
      THEN 'W'
      ELSE 'T'
    END

  IF (@strInputType ='T')
    BEGIN

      -- Determining Resource Type.

      SET @strResourceType = 
        CASE 
          WHEN CHARINDEX('~', @strRowID) = 0 
          THEN ''
          ELSE SUBSTRING(@strRowID, 1, 1)
        END

      SET @strIDPrefix = LEFT(@strRowID, CHARINDEX('|', @strRowID) - 1)

      IF (@strResourceType = 'E')
        BEGIN
          SET @strResourceID = REPLACE(@strIDPrefix, 'E~', '')
          SET @strGenericResourceID = NULL
        END
      ELSE IF (@strResourceType = 'G')
        BEGIN
          SET @strGenericResourceID = REPLACE(@strIDPrefix, 'G~', '')
          SET @strResourceID = NULL
        END
      ELSE
        BEGIN
          SET @strResourceID = NULL
          SET @strGenericResourceID = NULL
        END

      -- Parsing for TaskID.

      SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

      -- Setting various Plan parameter.

      SELECT 
        @strCompany = P.Company,
        @strPlanID = PT.PlanID,
        @strWBS1 = PT.WBS1,
        @strWBS2 = ISNULL(PT.WBS2, ' '),
        @strWBS3 = ISNULL(PT.WBS3, ' '),
        @strLaborCode = PT.LaborCode,
        @strTaskStatus = PT.Status,
        @strTopWBS1 = MIN(ISNULL(TT.WBS1, '')),
        @strTopName = MIN(ISNULL(TT.Name, '')),
        @bitIsLeafTask =
          CASE
            WHEN COUNT(CT.TaskID) > 0
            THEN 0
            ELSE 1
          END
        FROM PNTask AS PT 
          LEFT JOIN PNTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
          LEFT JOIN PNTask AS TT ON PT.PlanID = TT.PlanID AND TT.OutlineLevel = 0
          INNER JOIN PNPlan AS P ON PT.PlanID = P.PlanID
        WHERE PT.TaskID = @strTaskID
        GROUP BY PT.PlanID, PT.TaskID, PT.Status, PT.WBS1, PT.WBS2, PT.WBS3, PT.LaborCode, P.Company

    END
  ELSE IF (@strInputType = 'W')
    BEGIN

      SET @strWBS1WBS2WBS3 = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

      -- Get various Plan parameters.

      SELECT 
        @strCompany = 
          CASE 
            WHEN @strMultiCompanyEnabled = 'Y' 
            THEN ISNULL(SUBSTRING(TP.Org, @siOrg1Start, @siOrg1Length), ' ') 
            ELSE ' ' 
          END,
        @strPlanID = NULL,
        @strTaskID = NULL,
        @strWBS1 = P.WBS1,
        @strWBS2 = P.WBS2,
        @strWBS3 = P.WBS3,
        @strTaskStatus = P.Status,
        @strTopWBS1 = ISNULL(TP.WBS1, ''),
        @strTopName = ISNULL(TP.Name, ''),
        @bitIsLeafTask =
          CASE
            WHEN P.SubLevel = 'Y'
            THEN 0
            ELSE 1
          END
        FROM PR AS P
          LEFT JOIN PR AS TP
            ON TP.WBS1 = P.WBS1 AND TP.WBS2 = ' ' AND TP.WBS3 = ' '
        WHERE P.WBS1 + @_SectionSign + P.WBS2 + @_SectionSign + P.WBS3 = @strWBS1WBS2WBS3

    END /* END ELSE IF (@strInputType IN ('W')) */

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Get decimal settings.
  
  SELECT 
    @intHrDecimals = HrDecimals,
    @intAmtCostDecimals = AmtCostDecimals,
    @intAmtBillDecimals = AmtBillDecimals
    FROM dbo.stRP$tabPlanDecimals(@strPlanID)

  -- Get other Company-dependent settings from CFGResourcePlanning.

  SELECT 
    @strDefBudgetType = BudgetType
    FROM CFGResourcePlanning AS CRP
    WHERE CRP.Company = @strCompany

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Build WBS Structure.

  -- If the input Project currently has no Plan then save the entire Project away into @tabWBS.
  -- Otherwise, save away PNTask rows into @tabWBS.
      
  IF (@strInputType = 'W')
    BEGIN

      -- WBS1 Row.

      INSERT @tabWBS(
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        WBS1WBS2WBS3,
        LaborCode,
        Name,
        ParentOutlineNumber,
        OutlineNumber,
        ChildrenCount,
        OutlineLevel,
        ChargeType,
        ProjectType,
        WBSType,
        BudgetType,
        CostCurrencyCode,
        BillCurrencyCode,
        Org,
        ClientID,
        ProjMgr,
        StartDate,
        EndDate,
				UtilizationScheduleFlg,
        Status,
        HasNotes,
        ContractStartDate,
        ContractEndDate,
        OverheadPct,
        BaselineLaborHrs,
        BaselineLabCost,
        BaselineLabBill,
        FeeLabCost,
        FeeLabBill
      )
        SELECT
          NULL AS PlanID,
          NULL AS TaskID,
          PR.WBS1 AS WBS1,
          PR.WBS2 AS WBS2,
          PR.WBS3 AS WBS3,
          PR.WBS1 + @_SectionSign + PR.WBS2 + @_SectionSign + PR.WBS3 AS WBS1WBS2WBS3,
          NULL AS LaborCode,
          PR.Name AS Name,
          NULL AS ParentOutlineNumber,
          '001' AS OutlineNumber,
          (SELECT COUNT(*) FROM PR WHERE WBS1 = @strWBS1 AND WBS2 <> ' ' AND WBS3 = ' ') AS ChildrenCount,
          0 AS OutlineLevel,
          PR.ChargeType AS ChargeType,
          PR.ProjectType AS ProjectType,
          'WBS1' AS WBSType,
          @strDefBudgetType AS BudgetType,
          PR.ProjectCurrencyCode AS CostCurrencyCode,
          PR.BillingCurrencyCode AS BillCurrencyCode,
          PR.Org AS Org,
          PR.ClientID AS ClientID,
          PR.ProjMgr AS ProjMgr,
          PR.StartDate AS StartDate,
          PR.EndDate AS EndDate,
					PR.UtilizationscheduleFlg AS UtilizationscheduleFlg,
          PR.Status AS Status,
          'N' AS HasNotes,
          PR.StartDate AS ContractStartDate,
          COALESCE(PR.EndDate, PR.ActCompletionDate, PR.EstCompletionDate) AS ContractEndDate,
          0 AS OverheadPct,
          0 AS BaselineLaborHrs,
          0 AS BaselineLabCost,
          0 AS BaselineLabBill,
          PR.FeeDirLab AS FeeLabCost,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirLabBillingCurrency ELSE PR.FeeDirLab END) AS FeeLabBill
          FROM PR
          WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '

      -- WBS2 Rows.

      INSERT @tabWBS (
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        WBS1WBS2WBS3,
        LaborCode,
        Name,
        ParentOutlineNumber,
        OutlineNumber,
        ChildrenCount,
        OutlineLevel,
        ChargeType,
        ProjectType,
        WBSType,
        BudgetType,
        CostCurrencyCode,
        BillCurrencyCode,
        Org,
        ClientID,
        ProjMgr,
        StartDate,
        EndDate,
				UtilizationScheduleFlg,
        Status,
        HasNotes,
        ContractStartDate,
        ContractEndDate,
        OverheadPct,
        BaselineLaborHrs,
        BaselineLabCost,
        BaselineLabBill,
        FeeLabCost,
        FeeLabBill
      )
        SELECT
          NULL AS PlanID,
          NULL AS TaskID,
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.WBS1 + @_SectionSign + X.WBS2 + @_SectionSign + X.WBS3 AS WBS1WBS2WBS3,
          NULL AS LaborCode,
          X.Name,
          '001' AS ParentOutlineNumber,
          '001' + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
          (SELECT COUNT(*) FROM PR WHERE WBS1 = @strWBS1 AND WBS2 = X.WBS2 AND WBS3 <> ' ') AS ChildrenCount,
          1 AS OutlineLevel,
          X.ChargeType AS ChargeType,
          X.ProjectType AS ProjectType,
          'WBS2' AS WBSType,
          @strDefBudgetType AS BudgetType,
          X.CostCurrencyCode AS CostCurrencyCode,
          X.BillCurrencyCode AS BillCurrencyCode,
          X.Org,
          X.ClientID,
          X.ProjMgr,
          X.StartDate,
          X.EndDate,
					X.UtilizationScheduleFlg AS UtilizationScheduleFlg, 
          X.Status,
          'N' AS HasNotes,
          X.ContractStartDate AS ContractStartDate,
          X.ContractEndDate AS ContractEndDate,
          0 AS OverheadPct,
          0 AS BaselineLaborHrs,
          0 AS BaselineLabCost,
          0 AS BaselineLabBill,
          X.FeeLabCost AS FeeLabCost,
          X.FeeLabBill AS FeeLabBill
          FROM (
            SELECT
              PR.WBS1 AS WBS1,
              PR.WBS2 AS WBS2,
              PR.WBS3 AS WBS3,
              PR.Name AS Name,
              PR.ChargeType AS ChargeType,
              PR.ProjectType AS ProjectType,
              PR.ProjectCurrencyCode AS CostCurrencyCode,
              PR.BillingCurrencyCode AS BillCurrencyCode,
              PR.Org AS Org,
              PR.ClientID AS ClientID,
              PR.ProjMgr AS ProjMgr,
              PR.StartDate AS StartDate,
              PR.EndDate AS EndDate,
							PR.UtilizationScheduleFlg AS UtilizationScheduleFlg, 
              PR.Status AS Status,
              PR.StartDate AS ContractStartDate,
              COALESCE(PR.EndDate, PR.ActCompletionDate, PR.EstCompletionDate) AS ContractEndDate,
              PR.FeeDirLab AS FeeLabCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirLabBillingCurrency ELSE PR.FeeDirLab END) AS FeeLabBill,
              ROW_NUMBER() OVER (PARTITION BY PR.WBS1 ORDER BY PR.WBS1, PR.WBS2) AS RowID
              FROM PR 
              WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 <> ' ' AND PR.WBS3 = ' '
          ) AS X

      -- WBS3 Rows.

      INSERT @tabWBS (
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        WBS1WBS2WBS3,
        LaborCode,
        Name,
        ParentOutlineNumber,
        OutlineNumber,
        ChildrenCount,
        OutlineLevel,
        ChargeType,
        ProjectType,
        WBSType,
        BudgetType,
        CostCurrencyCode,
        BillCurrencyCode,
        Org,
        ClientID,
        ProjMgr,
        StartDate,
        EndDate,
				UtilizationscheduleFlg,
        Status,
        HasNotes,
        ContractStartDate,
        ContractEndDate,
        OverheadPct,
        BaselineLaborHrs,
        BaselineLabCost,
        BaselineLabBill,
        FeeLabCost,
        FeeLabBill
      )
        SELECT
          NULL AS PlanID,
          NULL AS TaskID,
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.WBS1 + @_SectionSign + X.WBS2 + @_SectionSign + X.WBS3 AS WBS1WBS2WBS3,
          NULL AS LaborCode,
          X.Name,
          PX.OutlineNumber AS ParentOutlineNumber,
          PX.OutlineNumber + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(X.RowID, 36)), 3) AS OutlineNumber,
          0 AS ChildrenCount,
          PX.OutlineLevel + 1 AS OutlineLevel,
          X.ChargeType AS ChargeType,
          X.ProjectType AS ProjectType,
          'WBS3' AS WBSType,
          @strDefBudgetType AS BudgetType,
          X.CostCurrencyCode AS CostCurrencyCode,
          X.BillCurrencyCode AS BillCurrencyCode,
          X.Org,
          X.ClientID,
          X.ProjMgr,
          X.StartDate,
          X.EndDate,
					X.UtilizationscheduleFlg AS UtilizationscheduleFlg,
          X.Status,
          'N' AS HasNotes,
          X.ContractStartDate AS ContractStartDate,
          X.ContractEndDate AS ContractEndDate,
          0 AS OverheadPct,
          0 AS BaselineLaborHrs,
          0 AS BaselineLabCost,
          0 AS BaselineLabBill,
          X.FeeLabCost,
          X.FeeLabBill
          FROM (
            SELECT
              PR.WBS1 AS WBS1,
              PR.WBS2 AS WBS2,
              PR.WBS3 AS WBS3,
              PR.Name AS Name,
              PR.ChargeType AS ChargeType,
              PR.ProjectType AS ProjectType,
              PR.ProjectCurrencyCode AS CostCurrencyCode,
              PR.BillingCurrencyCode AS BillCurrencyCode,
              PR.Org AS Org,
              PR.ClientID AS ClientID,
              PR.ProjMgr AS ProjMgr,
              PR.StartDate AS StartDate,
              PR.EndDate AS EndDate,
							PR.UtilizationscheduleFlg AS UtilizationscheduleFlg,
              PR.Status AS Status,
              PR.StartDate AS ContractStartDate,
              COALESCE(PR.EndDate, PR.ActCompletionDate, PR.EstCompletionDate) AS ContractEndDate,
              PR.FeeDirLab AS FeeLabCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirLabBillingCurrency ELSE PR.FeeDirLab END) AS FeeLabBill,
              ROW_NUMBER() OVER (PARTITION BY PR.WBS2 ORDER BY PR.WBS1, PR.WBS2, PR.WBS3) AS RowID
              FROM PR 
              WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 <> ' ' AND PR.WBS3 <> ' '
          ) AS X
            INNER JOIN @tabWBS AS PX ON X.WBS1 = PX.WBS1 AND X.WBS2 = PX.WBS2 AND PX.WBS3 = ' '
        
    END /* END IF (@strInputType = 'W') */
        
  ELSE IF (@strInputType = 'T')
    BEGIN
        
      INSERT @tabWBS(
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        WBS1WBS2WBS3,
        LaborCode,
        Name,
        ParentOutlineNumber,
        OutlineNumber,
        ChildrenCount,
        OutlineLevel,
        ChargeType,
        ProjectType,
        WBSType,
        BudgetType,
        CostCurrencyCode,
        BillCurrencyCode,
        Org,
        ClientID,
        ProjMgr,
        StartDate,
        EndDate,
				UtilizationscheduleFlg,
        Status,
        HasNotes,
        ContractStartDate,
        ContractEndDate,
        OverheadPct,
        BaselineLaborHrs,
        BaselineLabCost,
        BaselineLabBill,
        FeeLabCost,
        FeeLabBill
      )
        SELECT
          T.PlanID AS PlanID,
          T.TaskID AS TaskID,
          T.WBS1 AS WBS1,
          ISNULL(T.WBS2, ' ') AS WBS2,
          ISNULL(T.WBS3, ' ') AS WBS3,
          T.WBS1 + @_SectionSign + ISNULL(T.WBS2, ' ') + @_SectionSign + ISNULL(T.WBS3, ' ') AS WBS1WBS2WBS3,
          T.LaborCode AS LaborCode,
          CASE WHEN T.LaborCode IS NOT NULL THEN L.LaborCodeName ELSE T.NAME END  AS Name,
          T.ParentOutlineNumber AS ParentOutlineNumber,
          T.OutlineNumber AS OutlineNumber,
          T.ChildrenCount AS ChildrenCount,
          T.OutlineLevel AS OutlineLevel,
          T.ChargeType AS ChargeType,
          T.ProjectType AS ProjectType,
          T.WBSType AS WBSType,
          P.BudgetType AS BudgetType,
          P.CostCurrencyCode AS CostCurrencyCode,
          P.BillingCurrencyCode AS BillCurrencyCode,
          T.Org AS Org,
          T.ClientID AS ClientID,
          T.ProjMgr AS ProjMgr,
          T.StartDate AS StartDate,
          T.EndDate AS EndDate,
					PR.UtilizationscheduleFlg AS UtilizationscheduleFlg,
          T.Status AS Status,
          CASE WHEN T.Notes IS NOT NULL AND T.Notes <> '' THEN 'Y' ELSE 'N' END AS HasNotes,
          PR.StartDate AS ContractStartDate,
          COALESCE(PR.EndDate, PR.ActCompletionDate, PR.EstCompletionDate) AS ContractEndDate,
          P.OverheadPct AS OverheadPct,
          T.BaselineLaborHrs,
          T.BaselineLabCost,
          T.BaselineLabBill,              
          COALESCE(PR.FeeDirLab, T.CompensationFeeDirLab, 0) AS FeeLabCost,
          CASE 
            WHEN @strReportAtBillingInBillingCurr = 'Y' 
            THEN COALESCE(PR.FeeDirLabBillingCurrency, T.CompensationFeeDirLabBill, 0) 
            ELSE COALESCE(PR.FeeDirLab, T.CompensationFeeDirLab, 0) 
          END AS FeeLabBill
          FROM PNTask AS T
            INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID
			CROSS APPLY dbo.stRP$tabLaborCode(T.PlanID,T.LaborCode) AS L
            LEFT JOIN PR
              ON T.WBS1 = PR.WBS1 AND ISNULL(T.WBS2, ' ') = PR.WBS2 AND ISNULL(T.WBS3, ' ') = PR.WBS3
          WHERE T.PlanID = @strPlanID

    END /* END ELSE IF (@strInputType = 'T') */

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Determine ParentRowID based on @strMode, @strInputType, @strResourceType.

  SELECT
    @strParentRowID = 
      CASE 
        WHEN @strMode = 'C' THEN @strRowID
        WHEN @strMode = 'S' THEN 
          CASE 
            WHEN (@strInputType = 'T' AND DATALENGTH(@strResourceType) > 0) 
              THEN ISNULL(('|' + CT.TaskID), '')
            WHEN (@strInputType = 'T' AND DATALENGTH(@strResourceType) = 0) 
              THEN ISNULL(('|' + PT.TaskID), '')
            WHEN (@strInputType = 'W' AND DATALENGTH(@strResourceType) > 0) 
              THEN ISNULL(('|' + CT.WBS1 + @_SectionSign + CT.WBS2 + @_SectionSign + CT.WBS3), '')
            WHEN (@strInputType = 'W' AND DATALENGTH(@strResourceType) = 0) 
              THEN ISNULL(('|' + PT.WBS1 + @_SectionSign + PT.WBS2 + @_SectionSign + PT.WBS3), '')
          END
      END 
    FROM @tabWBS AS CT
      LEFT JOIN @tabWBS AS PT ON PT.PlanID = CT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber
    WHERE CT.TaskID = @strTaskID

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save Labor JTD & Unposted Labor JTD.
  -- The JTD data are posted at the leaf node in the WBS Tree. 
  -- Therefore, we need to calculate JTD for the summary levels.

  INSERT @tabLD(
    WBS1,
    WBS2,
    WBS3,
    Employee,
    TransDate,
    JTDHrs,
    JTDCost,
    JTDBill
  )     
    SELECT
      LD.WBS1,
      LD.WBS2,
      LD.WBS3,
      LD.Employee,
      LD.TransDate,
      SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDHrs,
      SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS JTDCost,
      SUM(BillExt) AS JTDBill
      FROM LD
      WHERE LD.WBS1 = @strWBS1 AND
        (LD.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
        (LD.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 + '%' END) AND 
        LD.ProjectCost = 'Y' AND LD.TransDate <= @dtJTDDate
      GROUP BY LD.WBS1, LD.WBS2, LD.WBS3, LD.Employee, LD.TransDate 
    UNION ALL
    SELECT
      TD.WBS1,
      TD.WBS2,
      TD.WBS3,
      TD.Employee,
      TD.TransDate,
      SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDHrs,
      SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS JTDCost,
      SUM(BillExt) AS JTDBill
      FROM tkDetail AS TD -- TD table has entries at only the leaf level.
        INNER JOIN EM ON TD.Employee = EM.Employee  
        INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
      WHERE TD.WBS1 = @strWBS1 AND
        (TD.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
        (TD.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 + '%' END)
      GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.Employee, TD.TransDate
    UNION ALL
    SELECT
      TD.WBS1,
      TD.WBS2,
      TD.WBS3,
      TD.Employee,
      TD.TransDate,
      SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDHrs,
      SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS JTDCost,
      SUM(BillExt) AS JTDBill
      FROM tsDetail AS TD -- TD table has entries at only the leaf level.
        INNER JOIN tsControl AS tsC
          ON (tsC.Posted = 'N' AND TD.Batch = tsC.Batch)
      WHERE TD.WBS1 = @strWBS1 AND
        (TD.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
        (TD.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 + '%' END)
      GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.Employee, TD.TransDate   

    INSERT @tabLDTask(
      WBS1,
      WBS2,
      WBS3,
      OutlineNumber,
      Employee,
      TransDate,
      JTDHrs,
      JTDCost,
      JTDBill
    )
      SELECT
        XLD.WBS1 AS WBS1,
        XLD.WBS2 AS WBS2,
        XLD.WBS3 AS WBS3,
        W.OutlineNumber AS OutlineNumber,
        XLD.Employee AS Employee,
        XLD.TransDate AS TransDate,
        XLD.JTDHrs AS JTDHrs,
        XLD.JTDCost AS JTDCost,
        XLD.JTDBill AS JTDBill
        FROM  @tabLD  AS XLD
          INNER JOIN @tabWBS AS W 
            ON XLD.WBS1 = W.WBS1 AND XLD.WBS2 = W.WBS2 AND XLD.WBS3 = W.WBS3
 
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collect Assignments.

  IF (@strMode = 'C')
    BEGIN

      -- @strRowID is pointing to a Leaf WBS row.


      -- Insert both Planned and Un-Planned Resources into @tabAssignment table.

      INSERT @tabAssignment(
        PlanID,
        TaskID,
        AssignmentID,
        ResourceID,
        GenericResourceID,
        StartDate,
        EndDate,
        CostRate,
        BillRate,
        HardBookedFlg,
        OutlineNumber,
        BaselineHrs,
        BaselineCost,
        BaselineBill
      )
        SELECT DISTINCT
          A.PlanID AS PlanID,
          A.TaskID AS TaskID,
          A.AssignmentID AS AssignmentID,
          A.ResourceID AS ResourceID,
          A.GenericResourceID AS GenericResourceID,
          A.StartDate AS StartDate,
          A.EndDate AS EndDate,
          A.CostRate AS CostRate,
          A.BillingRate AS BillRate,
          CASE HardBooked
            WHEN 'Y' THEN 1
            WHEN 'N' THEN -1
          END AS HardBookedFlg,
          AT.OutlineNumber AS OutlineNumber,
          A.BaselineLaborHrs AS BaselineHrs,
          A.BaselineLabCost AS BaselineCost,
          A.BaselineLabBill AS BaselineBill
          FROM PNAssignment AS A
            INNER JOIN PNTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
            INNER JOIN PNTask AS PT ON A.PlanID = PT.PlanID AND AT.OutlineNumber LIKE PT.OutlineNumber + '%'
            LEFT JOIN PNTask AS CT ON A.PlanID = CT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber AND AT.OutlineNumber LIKE CT.OutlineNumber + '%'
          WHERE
            A.PlanID = @strPlanID AND PT.TaskID = @strTaskID

    END /* END IF (@strMode = 'C') */

  ELSE IF (@strMode = 'S')
    BEGIN

      -- @strRowID is pointing to an Assignment row.
      
      INSERT @tabAssignment(
        PlanID,
        TaskID,
        AssignmentID,
        ResourceID,
        GenericResourceID,
        StartDate,
        EndDate,
        CostRate,
        BillRate,
        HardBookedFlg,
        OutlineNumber,
        BaselineHrs,
        BaselineCost,
        BaselineBill
      )
        SELECT DISTINCT
          A.PlanID AS PlanID,
          A.TaskID AS TaskID,
          A.AssignmentID AS AssignmentID,
          A.ResourceID AS ResourceID,
          A.GenericResourceID AS GenericResourceID,
          A.StartDate AS StartDate,
          A.EndDate AS EndDate,
          A.CostRate AS CostRate,
          A.BillingRate AS BillRate,
          CASE HardBooked
            WHEN 'Y' THEN 1
            WHEN 'N' THEN -1
          END AS HardBookedFlg,
          AT.OutlineNumber AS OutlineNumber,
          A.BaselineLaborHrs AS BaselineHrs,
          A.BaselineLabCost AS BaselineCost,
          A.BaselineLabBill AS BaselineBill
          FROM PNAssignment AS A
            INNER JOIN PNTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
            INNER JOIN PNTask AS PT ON A.PlanID = PT.PlanID AND AT.OutlineNumber LIKE PT.OutlineNumber + '%'
          WHERE
            A.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
            ((ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') AND 
            ISNULL(A.GenericResourceID, '|') = ISNULL(@strGenericResourceID, '|'))
            OR (@strResourceID IS NULL AND @strGenericResourceID IS NULL))

    END /* END ELSE IF (@strMode = 'S') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF ((@strMode = 'C' AND @bitIsLeafTask = 1) OR (@strMode = 'S' AND DATALENGTH(@strResourceType) > 0))
    BEGIN /* BEGIN Assignment Rows */

      -- Save a list of Employees and Generic Resources from Assignments for the input WBS1/WBS2/WBS3 combination.
      -- Also save a list of Employees from the Ledger table that are not already in the list of Assignments.

      INSERT @tabRes(
        OutlineNumber,
        Employee,
        GenericResourceID
      )
        SELECT DISTINCT
          OutlineNumber,
          X.Employee AS Employee,
          X.GenericResourceID AS GenericResourceID
          FROM (
            SELECT DISTINCT
              A.OutlineNumber AS OutlineNumber,
              A.ResourceID AS Employee,
              A.GenericResourceID AS GenericResourceID
              FROM @tabAssignment AS A         
          ) AS X
          WHERE 
            ISNULL(X.Employee, '%') LIKE ISNULL(@strResourceID, '%') AND
            ISNULL(X.GenericResourceID, '%') LIKE ISNULL(@strGenericResourceID, '%')

      -- Insert final result.

      INSERT @tabLabor(
        RowID,
        ParentRowID,
        PlanID,
        TaskID,
        AssignmentID,
        ResourceID,
        GenericResourceID ,
        ParentOutlineNumber,
        OutlineNumber,
        OutlineLevel,
        RowLevel,
        Name,
        StartDate,
        EndDate,
				UtilizationScheduleFlg,
        MinASGDate,
        MaxASGDate,
        WBS1,
        WBS2,
        WBS3,
        LaborCode,
        ChargeType,
        ProjectType,
        WBSType,
        BudgetType,
        CostCurrencyCode,
        BillCurrencyCode,
        Org,
        OrgName,
        ProjMgr,
        PMFullName,
        ClientName,
        OpportunityID,
        RT_Status,
        TopWBS1,
        TopName,
        HasPhoto,
				PhotoModDate,
        HasNotes,
        HasAssignments,
        HasChildren,
        LeafNode,
        ResSortSeq,
        HardBooked,

        ContractStartDate,
        ContractEndDate,

        OverheadPct,
        BaselineHrs,
        BaselineETCHrs,
        BaselineEACHrs,
        BaselineCost,
        BaselineBill,
        JTDHrs,
        JTDCost,
        JTDBill,
        JTDOHCost,
        ContractCost,
        ContractBill,

        PlannedHrs,
        PlannedCost,
        PlannedBill,
        ETCHrs,
        ETCCost,
        ETCBill,
        EACHrs,
        EACCost,
        EACBill,

        CalcPctComplCost,
        CalcPctComplBill,
        ContractLessJTDBill,
        ContractLessEACBill,

        PlannedLessJTDHrs,
        PlannedLessJTDCost,
        PlannedLessJTDBill,
        PlannedLessEACHrs,
        PlannedLessEACCost,
        PlannedLessEACBill,

        EACMultCost,
        EACOverheadCost,
        EACProfitCost,
        EACProfitPctCost
      )
        SELECT
          CASE WHEN XT.ResourceID IS NULL THEN 'G~' + XT.GenericResourceID ELSE 'E~' + XT.ResourceID END + 
          '|' + 
          CASE 
            WHEN @strInputType = 'T' THEN XT.TaskID
            WHEN @strInputType = 'W' THEN XT.WBS1WBS2WBS3
          END AS RowID,
          @strParentRowID AS ParentRowID,
          @strPlanID AS PlanID,
          XT.TaskID AS TaskID,
          XT.AssignmentID AS AssignmentID,
          XT.ResourceID AS ResourceID,
          XT.GenericResourceID AS GenericResourceID,
          XT.ParentOutlineNumber AS ParentOutlineNumber,
          XT.OutlineNumber AS OutlineNumber,
          XT.OutlineLevel AS OutlineLevel,
          XT.OutlineLevel + 1 AS RowLevel,
          CASE 
            WHEN XT.ResourceID IS NULL 
            THEN GR.Name 
            ELSE CONVERT(nvarchar(255), ISNULL(ISNULL(EM.PreferredName, EM.FirstName), N'') + ISNULL((N' ' + EM.LastName), N'') + ISNULL(', ' + EMSuffix.Suffix, ''))
          END AS Name,
          PlanStart AS StartDate,
          PlanEnd AS EndDate,
					XT.UtilizationScheduleFlg AS UtilizationScheduleFlg,
          XT.StartDate AS MinASGDate,
          XT.EndDate AS MaxASGDate,
          ISNULL(XT.WBS1, '') AS WBS1,
          ISNULL(XT.WBS2, '') AS WBS2,
          ISNULL(XT.WBS3, '') AS WBS3,
          ISNULL(XT.LaborCode, '') AS LaborCode,
          ISNULL(XT.ChargeType, '') AS ChargeType,
          ISNULL(XT.ProjectType, '') AS ProjectType,
          ISNULL(XT.WBSType, '') AS WBSType,
          ISNULL(XT.BudgetType, '') AS BudgetType,
          ISNULL(XT.CostCurrencyCode, '') AS CostCurrencyCode,
          ISNULL(XT.BillCurrencyCode, '') AS BillCurrencyCode,
          ISNULL(XT.Org, '') AS Org,
          ISNULL(O.Name, '') AS OrgName,
          ISNULL(XT.ProjMgr, '') AS ProjMgr,
          CONVERT(nvarchar(255), ISNULL(ISNULL(PM.PreferredName, PM.FirstName), '') + ISNULL((' ' + PM.LastName), '') + ISNULL(', ' + PMSuffix.Suffix, '')) AS PMFullName,
          ISNULL(CL.Name, '') AS ClientName,
          NULL AS OpportunityID,
          ISNULL((CASE WHEN XT.ResourceID IS NULL THEN GR.Status ELSE EM.Status END), '') AS RT_Status,
          @strTopWBS1,
          @strTopName,
          CASE 
            WHEN XT.ResourceID IS NOT NULL 
            THEN 
              CASE 
                WHEN EP.Photo IS NULL 
                THEN 0 
                ELSE 1 
              END 
            ELSE 0
          END AS HasPhoto,
					CASE 
            WHEN XT.ResourceID IS NOT NULL 
            THEN 
              CASE 
                WHEN EP.Photo IS NULL 
                THEN NULL
                ELSE EP.ModDate 
              END 
            ELSE NULL
          END AS PhotoModDate,

          'N' AS HasNotes,
          0 AS HasAssignments,
          0 AS HasChildren,
          1 AS LeafNode,
          CASE
            WHEN XT.ResourceID IS NOT NULL THEN 1
            WHEN XT.GenericResourceID IS NOT NULL THEN 2
            ELSE 255
          END AS ResSortSeq,
          XT.ASG_HardBooked AS HardBooked,

          NULL AS ContractStartDate,
          NULL AS ContractEndDate,

          0 AS OverheadPct,
          XT.BaselineHrs AS BaselineHrs,
          0 AS BaselineETCHrs,
          0 AS BaselineEAChrs,
          XT.BaselineCost AS BaselineCost,
          XT.BaselineBill AS BaselineBill,
          XT.JTDHrs AS JTDHrs,
          XT.JTDCost AS JTDCost,
          XT.JTDBill AS JTDBill,
          0 AS JTDOHCost,
          0 AS ContractCost,
          0 AS ContractBill,

          0 AS PlannedHrs,
          0 AS PlannedCost,
          0 AS PlannedBill,
          0 AS ETCHrs,
          0 AS ETCCost,
          0 AS ETCBill,
          0 AS EACHrs,
          0 AS EACCost,
          0 AS EACBill,
   
          0 AS CalcPctComplCost,
          0 AS CalcPctComplBill,
          0 AS ContractLessJTDBill,
          0 AS ContractLessEACBill,

          0 AS PlannedLessJTDHrs,
          0 AS PlannedLessJTDCost,
          0 AS PlannedLessJTDBill,
          0 AS PlannedLessEACHrs,
          0 AS PlannedLessEACCost,
          0 AS PlannedLessEACBill,

          0 AS EACMultCost,
          0 AS EACOverheadCost,
          0 AS EACProfitCost,
          0 AS EACProfitPctCost
        FROM (
          SELECT
            T.PlanID AS PlanID,
            T.TaskID AS TaskID,
            T.ParentOutlineNumber AS ParentOutlineNumber,
            T.OutlineNumber AS OutlineNumber,
            T.OutlineLevel AS OutlineLevel,
            T.ChildrenCount AS ChildrenCount,
            T.Name AS Name,
            T.WBS1WBS2WBS3 AS WBS1WBS2WBS3,
            T.WBS1 AS WBS1,
            T.WBS2 AS WBS2,
            T.WBS3 AS WBS3,
            T.LaborCode AS LaborCode,
            T.ChargeType AS ChargeType,
            T.ProjectType AS ProjectType,
            T.WBSType AS WBSType,
            T.BudgetType AS BudgetType,
            T.CostCurrencyCode AS CostCurrencyCode,
            T.BillCurrencyCode AS BillCurrencyCode,
            T.Org AS Org,
            T.ClientID AS ClientID,
            T.ProjMgr AS ProjMgr,
            T.StartDate AS PlanStart,
            T.EndDate AS PlanEnd,
						T.UtilizationScheduleFlg AS UtilizationScheduleFlg,
            A.StartDate AS StartDate,
            A.EndDate AS EndDate,
            A.AssignmentID AS AssignmentID,
            R.Employee AS ResourceID,
            R.GenericResourceID AS GenericResourceID,
            ISNULL(A.CostRate, 0) AS CostRate,
            ISNULL(A.BillRate, 0) AS BillRate,
            CASE ISNULL(A.HardBookedFlg, -1)
              WHEN 1 THEN 'Y'
              WHEN -1 THEN 'N'
            END AS ASG_HardBooked,
            ROUND(ISNULL(A.BaselineHrs, 0.0000), @intHrDecimals) AS BaselineHrs,
            ROUND(ISNULL(A.BaselineCost, 0.0000), @intAmtCostDecimals) AS BaselineCost,
            ROUND(ISNULL(A.BaselineBill, 0.0000), @intAmtBillDecimals) AS BaselineBill,
            ROUND(ISNULL(XLD.JTDHrs, 0.0000), @intHrDecimals) AS JTDHrs,
            ROUND(ISNULL(XLD.JTDCost, 0.0000), @intAmtCostDecimals) AS JTDCost,
            ROUND(ISNULL(XLD.JTDBill, 0.0000), @intAmtBillDecimals) AS JTDBill
            FROM @tabRes AS R
              INNER JOIN @tabWBS AS T ON R.OutlineNumber = T.OutlineNumber
              LEFT JOIN @tabAssignment A 
                ON ISNULL(R.Employee, '|') = ISNULL(A.ResourceID, '|') AND
                  ISNULL(R.GenericResourceID, '|') = ISNULL(A.GenericResourceID, '|')
              LEFT JOIN ( /* AS XLD */
                SELECT
                  OutlineNumber AS OutlineNumber,
                  Employee AS Employee,
                  SUM(ISNULL(JTDHrs, 0.0000)) AS JTDHrs,
                  SUM(ISNULL(JTDCost, 0.0000)) AS JTDCost,
                  SUM(ISNULL(JTDBill, 0.0000)) AS JTDBill
                  FROM @tabLDTask AS ZLD
                  GROUP BY OutlineNumber, Employee
              ) AS XLD ON R.OutlineNumber = XLD.OutlineNumber AND R.Employee = XLD.Employee
            WHERE ISNULL(T.Status, '') LIKE CASE WHEN @bActiveWBSOnly = 1 THEN 'A' ELSE '%' END
        ) AS XT
        LEFT JOIN EM ON EM.Employee = XT.ResourceID
        LEFT JOIN CFGSuffix EMSuffix ON EMSuffix.Code = EM.Suffix
        LEFT JOIN EMPhoto EP ON EM.Employee = EP.Employee
        LEFT JOIN GR ON GR.Code = XT.GenericResourceID
        LEFT JOIN CL ON XT.ClientID = CL.ClientID
        LEFT JOIN EM AS PM ON XT.ProjMgr = PM.Employee
        LEFT JOIN CFGSuffix PMSuffix ON PMSuffix.Code = PM.Suffix
        LEFT JOIN Organization AS O ON XT.Org = O.Org

    END /* END Assignment Rows */

  ELSE /* WBS Rows */
    BEGIN /* BEGIN WBS Rows */

      -- Determine Overhead Rate

      SELECT 
        @strOHProcedure = OHProcedure,
        @strSysOHAllocMethod = OHAllocMethod,
        @decSysOHRate = ISNULL(OHRate, 0.0000),
        @decSysOHProvRate = ISNULL(OHProvisionalRate, 0.0000)
        FROM CFGOHMain
        WHERE OHBasis IN ('TL', 'DL') AND Company = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN @strCompany ELSE ' ' END

      SELECT @decOHRate = 
        CASE @strOHProcedure
          WHEN 'N' THEN 0.0000
          WHEN 'P' THEN
            CASE
              WHEN @strOrgOHAllocMethod = 'P' THEN @decOrgOHProvRate
              ELSE
                CASE
                  WHEN @decBudOHRate <> 0.0000 THEN @decBudOHRate 
                  ELSE @decOrgOHRate 
                END
            END
          WHEN 'F' THEN
            CASE
              WHEN @strSysOHAllocMethod = 'P' THEN @decSysOHProvRate
              ELSE
                CASE
                  WHEN @decBudOHRate <> 0.0000 THEN @decBudOHRate 
                  ELSE @decSysOHRate 
                END
            END
        END / 100.0000

      -- Calculate JTD Overhead Allocation
      
      INSERT @tabOH(
        OutlineNumber,
        MaxDate,
        OHCost,
        Flg
      )
        SELECT
          OutlineNumber AS OutlineNumber,
          MAX(ISNULL(MaxDate, CONVERT(datetime, 0))) AS MaxDate,
          SUM(ISNULL(OHCost, 0.0000)) AS OHCost,
          0 AS Flg
          FROM @tabWBS AS W
            LEFT JOIN (
              SELECT
                PRF.WBS1,
                PRF.WBS2,
                PRF.WBS3,
                MAX(ISNULL(FD.AccountPdEnd, CONVERT(datetime, 0))) AS MaxDate,
                SUM(ISNULL(PRF.RegOHProjectCurrency, 0.0000)) AS OHCost
                FROM PRF
                  LEFT JOIN CFGDates AS FD ON PRF.Period = FD.Period
                WHERE PRF.WBS1 = @strWBS1
									AND PRF.Period < @intActivePeriod
                GROUP BY PRF.WBS1, PRF.WBS2, PRF.WBS3
            ) AS X 
              ON W.WBS1 = X.WBS1 AND  W.WBS2 = X.WBS2 AND  W.WBS3 = X.WBS3 
          WHERE W.WBS1 = @strWBS1
          GROUP BY W.OutlineNumber

      INSERT @tabOH(
        OutlineNumber,
        MaxDate,
        OHCost,
        Flg
      )      
        SELECT
          OH.OutlineNumber AS OutlineNumber,
          MAX(ISNULL(X.TransDate, CONVERT(datetime, 0))) AS MaxDate,
          SUM(ISNULL(X.JTDCost, 0.0000)) * @decOHRate  AS OHCost,
          1 AS Flg
          FROM @tabOH AS OH
            LEFT JOIN @tabLDTask AS X ON OH.OutlineNumber = X.OutlineNumber AND OH.Flg = 0
            WHERE X.TransDate >= (SELECT AccountPdStart FROM CFGDates WHERE Period = @intActivePeriod)
            GROUP BY OH.OutlineNumber

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Insert final result for WBS rows.

      IF (@strMode = 'S')
        BEGIN

          INSERT @tabLabor(
            RowID,
            ParentRowID,
            PlanID,
            TaskID,
            AssignmentID,
            ResourceID,
            GenericResourceID ,
            ParentOutlineNumber,
            OutlineNumber,
            OutlineLevel,
            RowLevel,
            Name,
            StartDate,
            EndDate,
						UtilizationScheduleFlg,
            MinASGDate,
            MaxASGDate,
            WBS1,
            WBS2,
            WBS3,
            LaborCode,
            ChargeType,
            ProjectType,
            WBSType,
            BudgetType,
            CostCurrencyCode,
            BillCurrencyCode,
            Org,
            OrgName,
            ProjMgr,
            PMFullName,
            ClientName,
            OpportunityID,
            RT_Status,
            TopWBS1,
            TopName,
            HasPhoto,
						PhotoModDate,
            HasNotes,
            HasAssignments,
            HasChildren,
            LeafNode,
            ResSortSeq,
            HardBooked,
            
            ContractStartDate,
            ContractEndDate,

            OverheadPct,
            BaselineHrs,
            BaselineETCHrs,
            BaselineEACHrs,
            BaselineCost,
            BaselineBill,
            JTDHrs,
            JTDCost,
            JTDBill,
            JTDOHCost,
            ContractCost,
            ContractBill,

            PlannedHrs,
            PlannedCost,
            PlannedBill,
            ETCHrs,
            ETCCost,
            ETCBill,
            EACHrs,
            EACCost,
            EACBill,

            CalcPctComplCost,
            CalcPctComplBill,
            ContractLessJTDBill,
            ContractLessEACBill,

            PlannedLessJTDHrs,
            PlannedLessJTDCost,
            PlannedLessJTDBill,
            PlannedLessEACHrs,
            PlannedLessEACCost,
            PlannedLessEACBill,

            EACMultCost,
            EACOverheadCost,
            EACProfitCost,
            EACProfitPctCost
          )
            SELECT
              '|' + 
              CASE 
                WHEN @strInputType = 'T' THEN YT.TaskID
                WHEN @strInputType = 'W' THEN YT.WBS1WBS2WBS3
              END AS RowID,
              @strParentRowID AS ParentRowID,
              YT.PlanID AS PlanID,
              YT.TaskID AS TaskID,
              NULL AS AssignmentID,
              NULL AS ResourceID,
              NULL AS GenericResourceID,
              YT.ParentOutlineNumber AS ParentOutlineNumber,
              YT.OutlineNumber AS OutlineNumber,
              YT.OutlineLevel AS OutlineLevel,
              YT.OutlineLevel AS RowLevel,
              YT.Name AS Name,
              YT.StartDate AS StartDate,
              YT.EndDate AS EndDate,
							YT.UtilizationScheduleFlg AS UtilizationScheduleFlg,
              YT.MinASGDate,
              YT.MaxASGDate,
              ISNULL(YT.WBS1, '') AS WBS1,
              ISNULL(YT.WBS2, '') AS WBS2,
              ISNULL(YT.WBS3, '') AS WBS3,
              ISNULL(YT.LaborCode, '') AS LaborCode,
              ISNULL(YT.ChargeType, '') AS ChargeType,
              ISNULL(YT.ProjectType, '') AS ProjectType,
              ISNULL(YT.WBSType, '') AS WBSType,
              ISNULL(YT.BudgetType, '') AS BudgetType,
              ISNULL(YT.CostCurrencyCode, '') AS CostCurrencyCode,
              ISNULL(YT.BillCurrencyCode, '') AS BillCurrencyCode,
              ISNULL(YT.Org, '') AS Org,
              ISNULL(O.Name, '') AS OrgName,
              ISNULL(YT.ProjMgr, '') AS ProjMgr,
              CONVERT(nvarchar(255), ISNULL(ISNULL(PM.PreferredName, PM.FirstName), '') + ISNULL((' ' + PM.LastName), '') + ISNULL(', ' + PMSuffix.Suffix, '')) AS PMFullName,
              ISNULL(CL.Name, '') AS ClientName,
              NULL AS OpportunityID,
              YT.Status AS RT_Status,
              @strTopWBS1,
              @strTopName,
              0 as HasPhoto,
							NULL as PhotoModDate,
              YT.HasNotes AS HasNotes,
              CASE
                WHEN YT.MinAssignmentID IS NOT NULL
                THEN CONVERT(bit, 1)
                ELSE CONVERT(bit, 0)
              END AS HasAssignments,
              CASE 
                WHEN YT.ChildrenCount > 0 
                THEN CONVERT(bit, 1) 
                ELSE 
                  CASE 
                    WHEN EXISTS (SELECT 'X' FROM PNAssignment WHERE TaskID = YT.TaskID) 
                    THEN CONVERT(bit,1) 
                    ELSE CONVERT(bit, 0) 
                  END 
              END AS HasChildren,
              CASE WHEN YT.ChildrenCount > 0 THEN CONVERT(bit, 0) ELSE CONVERT(bit,1) END AS LeafNode,
              0 AS ResSortSeq,
              YT.ASG_HardBooked AS HardBooked,
   
              YT.ContractStartDate AS ContractStartDate,
              YT.ContractEndDate AS ContractEndDate,

              YT.OverheadPct AS OverheadPct,
              YT.BaselineHrs AS BaselineHrs,
              0 AS BaselineETCHrs,
              0 AS BaselineEAChrs,
              YT.BaselineCost AS BaselineCost,
              YT.BaselineBill AS BaselineBill,
              YT.JTDHrs AS JTDHrs,
              YT.JTDCost AS JTDCost,
              YT.JTDBill AS JTDBill,
              YT.JTDOHCost AS JTDOHCost,
              YT.FeeLabCost AS ContractCost,
              YT.FeeLabBill AS ContractBill,

              0 AS PlannedHrs,
              0 AS PlannedCost,
              0 AS PlannedBill,
              0 AS ETCHrs,
              0 AS ETCCost,
              0 AS ETCBill,
              0 AS EACHrs,
              0 AS EACCost,
              0 AS EACBill,
       
              0 AS CalcPctComplCost,
              0 AS CalcPctComplBill,
              0 AS ContractLessJTDBill,
              0 AS ContractLessEACBill,

              0 AS PlannedLessJTDHrs,
              0 AS PlannedLessJTDCost,
              0 AS PlannedLessJTDBill,
              0 AS PlannedLessEACHrs,
              0 AS PlannedLessEACCost,
              0 AS PlannedLessEACBill,

              0 AS EACMultCost,
              0 AS EACOverheadCost,
              0 AS EACProfitCost,
              0 AS EACProfitPctCost
            FROM ( /* AS YT */
              SELECT
                XT.PlanID,
                XT.TaskID,
                XT.ParentOutlineNumber,
                XT.OutlineNumber,
                XT.OutlineLevel,
                XT.ChildrenCount,
                XT.Name,
                XT.WBS1WBS2WBS3,
                XT.WBS1 AS WBS1,
                XT.WBS2 AS WBS2,
                XT.WBS3 AS WBS3,
                XT.LaborCode AS LaborCode,
                XT.ChargeType AS ChargeType,
                XT.ProjectType AS ProjectType,
                XT.WBSType AS WBSType,
                XT.BudgetType AS BudgetType,
                XT.CostCurrencyCode AS CostCurrencyCode,
                XT.BillCurrencyCode AS BillCurrencyCode,
                XT.Org,
                XT.ClientID,
                XT.ProjMgr,
                XT.StartDate,
                XT.EndDate,
								XT.UtilizationScheduleFlg AS UtilizationScheduleFlg,
                XT.Status,
                XT.HasNotes,
                XT.ContractStartDate AS ContractStartDate,
                XT.ContractEndDate AS ContractEndDate,
                XT.OverheadPct AS OverheadPct,
                XT.BaselineHrs AS BaselineHrs,
                XT.BaselineCost AS BaselineCost,
                XT.BaselineBill AS BaselineBill,
                XT.JTDHrs AS JTDHrs,
                XT.JTDCost AS JTDCost,
                XT.JTDBill AS JTDBill,
                XT.JTDOHCost AS JTDOHCost,
                XT.FeeLabCost AS FeeLabCost,
                XT.FeeLabBill AS FeeLabBill,
                XA.ASG_HardBooked AS ASG_HardBooked,
                XA.MinAssignmentID AS MinAssignmentID,
                XA.MinASGDate AS MinASGDate,
                XA.MaxASGDate AS MaxASGDate
                FROM ( /* AS XT */
                  SELECT
                    T.PlanID AS PlanID,
                    T.TaskID AS TaskID,
                    T.ParentOutlineNumber AS ParentOutlineNumber,
                    T.OutlineNumber AS OutlineNumber,
                    T.OutlineLevel AS OutlineLevel,
                    T.ChildrenCount AS ChildrenCount,
                    T.Name AS Name,
                    T.WBS1WBS2WBS3 AS WBS1WBS2WBS3,
                    T.WBS1 AS WBS1,
                    T.WBS2 AS WBS2,
                    T.WBS3 AS WBS3,
                    T.LaborCode AS LaborCode,
                    T.ChargeType AS ChargeType,
                    T.ProjectType AS ProjectType,
                    T.WBSType AS WBSType,
                    T.BudgetType AS BudgetType,
                    T.CostCurrencyCode AS CostCurrencyCode,
                    T.BillCurrencyCode AS BillCurrencyCode,
                    T.Org AS Org,
                    T.ClientID AS ClientID,
                    T.ProjMgr AS ProjMgr,
                    T.StartDate AS StartDate,
                    T.EndDate AS EndDate,
										T.UtilizationScheduleFlg AS UtilizationScheduleFlg,
                    ISNULL(T.Status, '') AS Status,
                    ISNULL(T.HasNotes, 'N') AS HasNotes,
                    T.ContractStartDate AS ContractStartDate,
                    T.ContractEndDate AS ContractEndDate,
                    ROUND(ISNULL(T.OverheadPct, 0.0000), 4) AS OverheadPct,
                    ROUND(ISNULL(T.BaselineLaborHrs, 0.0000), @intHrDecimals) AS BaselineHrs,
                    0 AS BaselineETCHrs,
                    0 AS BaselineEAChrs,
                    ROUND(ISNULL(T.BaselineLabCost, 0.0000), @intAmtCostDecimals) AS BaselineCost,
                    ROUND(ISNULL(T.BaselineLabBill, 0.0000), @intAmtBillDecimals) AS BaselineBill,
                    ROUND(SUM(ISNULL(XLD.JTDHrs, 0.0000)), @intHrDecimals) AS JTDHrs,
                    ROUND(SUM(ISNULL(XLD.JTDCost, 0.0000)), @intAmtCostDecimals) AS JTDCost,
                    ROUND(SUM(ISNULL(XLD.JTDBill, 0.0000)), @intAmtBillDecimals) AS JTDBill,
                    ROUND(SUM(ISNULL(XLD.JTDOHCost, 0.0000)), @intAmtCostDecimals) AS JTDOHCost,
                    ROUND(ISNULL(T.FeeLabCost, 0.0000), @intAmtCostDecimals) AS FeeLabCost,
                    ROUND(ISNULL(T.FeeLabBill, 0.0000), @intAmtBillDecimals) AS FeeLabBill
                    FROM @tabWBS AS T
                      LEFT JOIN ( /* AS XLD */
                        SELECT
                          OutlineNumber AS OutlineNumber,
                          SUM(ISNULL(JTDHrs, 0.0000)) AS JTDHrs,
                          SUM(ISNULL(JTDCost, 0.0000)) AS JTDCost,
                          SUM(ISNULL(JTDBill, 0.0000)) AS JTDBill,
                          SUM(ISNULL(JTDOHCost, 0.0000)) AS JTDOHCost
                          FROM ( /* AS YLD */
                            SELECT
                              OutlineNumber AS OutlineNumber,
                              SUM(ISNULL(JTDHrs, 0.0000)) AS JTDHrs,
                              SUM(ISNULL(JTDCost, 0.0000)) AS JTDCost,
                              SUM(ISNULL(JTDBill, 0.0000)) AS JTDBill,
                              0 AS JTDOHCost
                              FROM @tabLDTask AS ZLD
                              GROUP BY OutlineNumber
                            UNION ALL
                            SELECT
                              OutlineNumber AS OutlineNumber,
                              0 AS JTDHrs,
                              0 AS JTDCost,
                              0 AS JTDBill,
                              SUM(ISNULL(OHCost, 0.0000)) AS JTDOHCost
                              FROM @tabOH AS ZOH
                              GROUP BY OutlineNumber                          
                          ) AS YLD
                          GROUP BY OutlineNumber
                      ) AS XLD ON XLD.OutlineNumber LIKE T.OutlineNumber +  '%'  
                    WHERE T.WBS1 = @strWBS1 AND T.WBS2 = @strWBS2 AND T.WBS3 = @strWBS3 AND ISNULL(T.LaborCode, '|') = ISNULL(@strLaborCode, '|') AND
                      ISNULL(T.Status, '') LIKE CASE WHEN @bActiveWBSOnly = 1 THEN 'A' ELSE '%' END
                    GROUP BY
                      T.PlanID, T.TaskID, T.ParentOutlineNumber, T.OutlineNumber, T.OutlineLevel, T.ChildrenCount,
                      T.Name, T.WBS1WBS2WBS3, T.WBS1, T.WBS2, T.WBS3, T.LaborCode,
                      T.ChargeType, T.ProjectType, T.WBSType, T.BudgetType, T.CostCurrencyCode, T.BillCurrencyCode,
                      T.Org, T.ClientID, T.ProjMgr, T.StartDate, T.EndDate, T.UtilizationScheduleFlg,T.Status, T.HasNotes,
                      T.ContractStartDate, T.ContractEndDate, 
                      T.OverheadPct, T.BaselineLaborHrs, T.BaselineLabCost, T.BaselineLabBill, T.FeeLabCost, T.FeeLabBill
                ) AS XT
                LEFT JOIN (
                  SELECT
                    AT.PlanID AS PlanID,
                    AT.OutlineNumber AS OutlineNumber,
                    CASE SIGN(MIN(A.HardBookedFlg) + MAX(A.HardBookedFlg))
                      WHEN 0 THEN 'B'
                      WHEN 1 THEN 'Y'
                      WHEN -1 THEN 'N'
                    END AS ASG_HardBooked,
                    MIN(A.AssignmentID) AS MinAssignmentID,
                    MIN(A.StartDate) AS MinASGDate,
                    MAX(A.EndDate) AS MaxASGDate
                    FROM @tabWBS AS AT
                      LEFT JOIN @tabAssignment A ON AT.PlanID = A.PlanID AND A.OutlineNumber LIKE AT.OutlineNumber + '%'
                    GROUP BY AT.PlanID, AT.OutlineNumber
                ) AS XA ON XA.PlanID = XT.PlanID AND XA.OutlineNumber = XT.OutlineNumber
            ) AS YT
              LEFT JOIN CL ON YT.ClientID = CL.ClientID
              LEFT JOIN EM AS PM ON YT.ProjMgr = PM.Employee
        LEFT JOIN CFGSuffix PMSuffix ON PMSuffix.Code = PM.Suffix
              LEFT JOIN Organization AS O ON YT.Org = O.Org
   
        END /* END IF (@strMode = 'S') */

      ELSE IF (@strMode = 'C')
        BEGIN
        
          INSERT @tabLabor(
            RowID,
            ParentRowID,
            PlanID,
            TaskID,
            AssignmentID,
            ResourceID,
            GenericResourceID ,
            ParentOutlineNumber,
            OutlineNumber,
            OutlineLevel,
            RowLevel,
            Name,
            StartDate,
            EndDate,
						UtilizationScheduleFlg,
            MinASGDate,
            MaxASGDate,
            WBS1,
            WBS2,
            WBS3,
            LaborCode,
            ChargeType,
            ProjectType,
            WBSType,
            BudgetType,
            CostCurrencyCode,
            BillCurrencyCode,
            Org,
            OrgName,
            ProjMgr,
            PMFullName,
            ClientName,
            OpportunityID,
            RT_Status,
            TopWBS1,
            TopName,
            HasPhoto,
						PhotoModDate,
            HasNotes,
            HasAssignments,
            HasChildren,
            LeafNode,
            ResSortSeq,
            HardBooked,
            
            ContractStartDate,
            ContractEndDate,

            OverheadPct,
            BaselineHrs,
            BaselineETCHrs,
            BaselineEACHrs,
            BaselineCost,
            BaselineBill,
            JTDHrs,
            JTDCost,
            JTDBill,
            JTDOHCost,
            ContractCost,
            ContractBill,

            PlannedHrs,
            PlannedCost,
            PlannedBill,
            ETCHrs,
            ETCCost,
            ETCBill,
            EACHrs,
            EACCost,
            EACBill,

            CalcPctComplCost,
            CalcPctComplBill,
            ContractLessJTDBill,
            ContractLessEACBill,

            PlannedLessJTDHrs,
            PlannedLessJTDCost,
            PlannedLessJTDBill,
            PlannedLessEACHrs,
            PlannedLessEACCost,
            PlannedLessEACBill,

            EACMultCost,
            EACOverheadCost,
            EACProfitCost,
            EACProfitPctCost
          )
            SELECT
              '|' + 
              CASE 
                WHEN @strInputType = 'T' THEN YT.TaskID
                WHEN @strInputType = 'W' THEN YT.WBS1WBS2WBS3
              END AS RowID,
              @strParentRowID AS ParentRowID,
              YT.PlanID AS PlanID,
              YT.TaskID AS TaskID,
              NULL AS AssignmentID,
              NULL AS ResourceID,
              NULL AS GenericResourceID,
              YT.ParentOutlineNumber AS ParentOutlineNumber,
              YT.OutlineNumber AS OutlineNumber,
              YT.OutlineLevel AS OutlineLevel,
              YT.OutlineLevel AS RowLevel,
              YT.Name AS Name,
              YT.StartDate AS StartDate,
              YT.EndDate AS EndDate,
							YT.UtilizationScheduleFlg AS UtilizationScheduleFlg,
              YT.MinASGDate,
              YT.MaxASGDate,
              ISNULL(YT.WBS1, '') AS WBS1,
              ISNULL(YT.WBS2, '') AS WBS2,
              ISNULL(YT.WBS3, '') AS WBS3,
              ISNULL(YT.LaborCode, '') AS LaborCode,
              ISNULL(YT.ChargeType, '') AS ChargeType,
              ISNULL(YT.ProjectType, '') AS ProjectType,
              ISNULL(YT.WBSType, '') AS WBSType,
              ISNULL(YT.BudgetType, '') AS BudgetType,
              ISNULL(YT.CostCurrencyCode, '') AS CostCurrencyCode,
              ISNULL(YT.BillCurrencyCode, '') AS BillCurrencyCode,
              ISNULL(YT.Org, '') AS Org,
              ISNULL(O.Name, '') AS OrgName,
              ISNULL(YT.ProjMgr, '') AS ProjMgr,
              CONVERT(nvarchar(255), ISNULL(ISNULL(PM.PreferredName, PM.FirstName), '') + ISNULL((' ' + PM.LastName), '') + ISNULL(', ' + PMSuffix.Suffix, '')) AS PMFullName,
              ISNULL(CL.Name, '') AS ClientName,
              NULL AS OpportunityID,
              YT.Status AS RT_Status,
              @strTopWBS1,
              @strTopName,
              0 as HasPhoto,
							NULL as PhotoModDate,
              YT.HasNotes AS HasNotes,
              CASE
                WHEN YT.MinAssignmentID IS NOT NULL
                THEN CONVERT(bit, 1)
                ELSE CONVERT(bit, 0)
              END AS HasAssignments,
              CASE 
                WHEN YT.ChildrenCount > 0 
                THEN CONVERT(bit, 1) 
                ELSE 
                  CASE 
                    WHEN EXISTS (SELECT 'X' FROM PNAssignment WHERE TaskID = YT.TaskID) 
                    THEN CONVERT(bit,1) 
                    ELSE CONVERT(bit, 0) 
                  END 
              END AS HasChildren,
              CASE WHEN YT.ChildrenCount > 0 THEN CONVERT(bit, 0) ELSE CONVERT(bit,1) END AS LeafNode,
              0 AS ResSortSeq,
              YT.ASG_HardBooked AS HardBooked,
   
              YT.ContractStartDate AS ContractStartDate,
              YT.ContractEndDate AS ContractEndDate,

              YT.OverheadPct AS OverheadPct,
              YT.BaselineHrs AS BaselineHrs,
              0 AS BaselineETCHrs,
              0 AS BaselineEAChrs,
              YT.BaselineCost AS BaselineCost,
              YT.BaselineBill AS BaselineBill,
              YT.JTDHrs AS JTDHrs,
              YT.JTDCost AS JTDCost,
              YT.JTDBill AS JTDBill,
              YT.JTDOHCost AS JTDOHCost,
              YT.FeeLabCost AS ContractCost,
              YT.FeeLabBill AS ContractBill,

              0 AS PlannedHrs,
              0 AS PlannedCost,
              0 AS PlannedBill,
              0 AS ETCHrs,
              0 AS ETCCost,
              0 AS ETCBill,
              0 AS EACHrs,
              0 AS EACCost,
              0 AS EACBill,
       
              0 AS CalcPctComplCost,
              0 AS CalcPctComplBill,
              0 AS ContractLessJTDBill,
              0 AS ContractLessEACBill,

              0 AS PlannedLessJTDHrs,
              0 AS PlannedLessJTDCost,
              0 AS PlannedLessJTDBill,
              0 AS PlannedLessEACHrs,
              0 AS PlannedLessEACCost,
              0 AS PlannedLessEACBill,

              0 AS EACMultCost,
              0 AS EACOverheadCost,
              0 AS EACProfitCost,
              0 AS EACProfitPctCost
            FROM ( /* AS YT */
              SELECT
                XT.PlanID,
                XT.TaskID,
                XT.ParentOutlineNumber,
                XT.OutlineNumber,
                XT.OutlineLevel,
                XT.ChildrenCount,
                XT.Name,
                XT.WBS1WBS2WBS3,
                XT.WBS1 AS WBS1,
                XT.WBS2 AS WBS2,
                XT.WBS3 AS WBS3,
                XT.LaborCode AS LaborCode,
                XT.ChargeType AS ChargeType,
                XT.ProjectType AS ProjectType,
                XT.WBSType AS WBSType,
                XT.BudgetType AS BudgetType,
                XT.CostCurrencyCode AS CostCurrencyCode,
                XT.BillCurrencyCode AS BillCurrencyCode,
                XT.Org,
                XT.ClientID,
                XT.ProjMgr,
                XT.StartDate,
                XT.EndDate,
								XT.UtilizationScheduleFlg AS UtilizationScheduleFlg,
                XT.Status,
                XT.HasNotes,
                XT.ContractStartDate AS ContractStartDate,
                XT.ContractEndDate AS ContractEndDate,
                XT.OverheadPct AS OverheadPct,
                XT.BaselineHrs AS BaselineHrs,
                XT.BaselineCost AS BaselineCost,
                XT.BaselineBill AS BaselineBill,
                XT.JTDHrs AS JTDHrs,
                XT.JTDCost AS JTDCost,
                XT.JTDBill AS JTDBill,
                XT.JTDOHCost AS JTDOHCost,
                XT.FeeLabCost AS FeeLabCost,
                XT.FeeLabBill AS FeeLabBill,
                XA.ASG_HardBooked AS ASG_HardBooked,
                XA.MinAssignmentID AS MinAssignmentID,
                XA.MinASGDate AS MinASGDate,
                XA.MaxASGDate AS MaxASGDate
                FROM ( /* AS XT */
                  SELECT
                    T.PlanID AS PlanID,
                    T.TaskID AS TaskID,
                    T.ParentOutlineNumber AS ParentOutlineNumber,
                    T.OutlineNumber AS OutlineNumber,
                    T.OutlineLevel AS OutlineLevel,
                    T.ChildrenCount AS ChildrenCount,
                    T.Name AS Name,
                    T.WBS1WBS2WBS3 AS WBS1WBS2WBS3,
                    T.WBS1 AS WBS1,
                    T.WBS2 AS WBS2,
                    T.WBS3 AS WBS3,
                    T.LaborCode AS LaborCode,
                    T.ChargeType AS ChargeType,
                    T.ProjectType AS ProjectType,
                    T.WBSType AS WBSType,
                    T.BudgetType AS BudgetType,
                    T.CostCurrencyCode AS CostCurrencyCode,
                    T.BillCurrencyCode AS BillCurrencyCode,
                    T.Org AS Org,
                    T.ClientID AS ClientID,
                    T.ProjMgr AS ProjMgr,
                    T.StartDate AS StartDate,
                    T.EndDate AS EndDate,
										T.UtilizationScheduleFlg AS UtilizationScheduleFlg,
                    ISNULL(T.Status, '') AS Status,
                    ISNULL(T.HasNotes, 'N') AS HasNotes,
                    T.ContractStartDate AS ContractStartDate,
                    T.ContractEndDate AS ContractEndDate,
                    ROUND(ISNULL(T.OverheadPct, 0.0000), 4) AS OverheadPct,
                    ROUND(ISNULL(T.BaselineLaborHrs, 0.0000), @intHrDecimals) AS BaselineHrs,
                    ROUND(ISNULL(T.BaselineLabCost, 0.0000), @intAmtCostDecimals) AS BaselineCost,
                    ROUND(ISNULL(T.BaselineLabBill, 0.0000), @intAmtBillDecimals) AS BaselineBill,
                    ROUND(SUM(ISNULL(XLD.JTDHrs, 0.0000)), @intHrDecimals) AS JTDHrs,
                    ROUND(SUM(ISNULL(XLD.JTDCost, 0.0000)), @intAmtCostDecimals) AS JTDCost,
                    ROUND(SUM(ISNULL(XLD.JTDBill, 0.0000)), @intAmtBillDecimals) AS JTDBill,
                    ROUND(SUM(ISNULL(XLD.JTDOHCost, 0.0000)), @intAmtCostDecimals) AS JTDOHCost,
                    ROUND(ISNULL(T.FeeLabCost, 0.0000), @intAmtCostDecimals) AS FeeLabCost,
                    ROUND(ISNULL(T.FeeLabBill, 0.0000), @intAmtBillDecimals) AS FeeLabBill
                    FROM @tabWBS AS T
                      INNER JOIN @tabWBS AS PT ON PT.OutlineNumber = T.ParentOutlineNumber
                      LEFT JOIN ( /* AS XLD */
                        SELECT
                          OutlineNumber AS OutlineNumber,
                          SUM(ISNULL(JTDHrs, 0.0000)) AS JTDHrs,
                          SUM(ISNULL(JTDCost, 0.0000)) AS JTDCost,
                          SUM(ISNULL(JTDBill, 0.0000)) AS JTDBill,
                          SUM(ISNULL(JTDOHCost, 0.0000)) AS JTDOHCost
                          FROM ( /* AS YLD */
                            SELECT
                              OutlineNumber AS OutlineNumber,
                              SUM(ISNULL(JTDHrs, 0.0000)) AS JTDHrs,
                              SUM(ISNULL(JTDCost, 0.0000)) AS JTDCost,
                              SUM(ISNULL(JTDBill, 0.0000)) AS JTDBill,
                              0 AS JTDOHCost
                              FROM @tabLDTask AS ZLD
                              GROUP BY OutlineNumber
                            UNION ALL
                            SELECT
                              OutlineNumber AS OutlineNumber,
                              0 AS JTDHrs,
                              0 AS JTDCost,
                              0 AS JTDBill,
                              SUM(ISNULL(OHCost, 0.0000)) AS JTDOHCost
                              FROM @tabOH AS ZOH
                              GROUP BY OutlineNumber                          
                          ) AS YLD
                          GROUP BY OutlineNumber
                      ) AS XLD ON XLD.OutlineNumber LIKE T.OutlineNumber +  '%' 
                    WHERE PT.WBS1 = @strWBS1 AND PT.WBS2 = @strWBS2 AND PT.WBS3 = @strWBS3 AND
                      ISNULL(@strTaskStatus, '') LIKE CASE WHEN @bActiveWBSOnly = 1 THEN 'A' ELSE '%' END AND
                      ISNULL(T.Status, '') LIKE CASE WHEN @bActiveWBSOnly = 1 THEN 'A' ELSE '%' END
                    GROUP BY
                      T.PlanID, T.TaskID, T.ParentOutlineNumber, T.OutlineNumber, T.OutlineLevel, T.ChildrenCount,
                      T.Name, T.WBS1WBS2WBS3, T.WBS1, T.WBS2, T.WBS3, T.LaborCode,
                      T.ChargeType, T.ProjectType, T.WBSType, T.BudgetType, T.CostCurrencyCode, T.BillCurrencyCode,
                      T.Org, T.ClientID, T.ProjMgr, T.StartDate, T.EndDate, T.UtilizationScheduleFlg,T.Status, T.HasNotes,
                      T.ContractStartDate, T.ContractEndDate, 
                      T.OverheadPct, T.BaselineLaborHrs, T.BaselineLabCost, T.BaselineLabBill, T.FeeLabCost, T.FeeLabBill
                ) AS XT
                LEFT JOIN (
                  SELECT
                    AT.PlanID AS PlanID,
                    AT.OutlineNumber AS OutlineNumber,
                    CASE SIGN(MIN(A.HardBookedFlg) + MAX(A.HardBookedFlg))
                      WHEN 0 THEN 'B'
                      WHEN 1 THEN 'Y'
                      WHEN -1 THEN 'N'
                    END AS ASG_HardBooked,
                    MIN(A.AssignmentID) AS MinAssignmentID,
                    MIN(A.StartDate) AS MinASGDate,
                    MAX(A.EndDate) AS MaxASGDate
                    FROM @tabWBS AS AT
                      LEFT JOIN @tabAssignment A ON AT.PlanID = A.PlanID AND A.OutlineNumber LIKE AT.OutlineNumber + '%'
                    GROUP BY AT.PlanID, AT.OutlineNumber
                ) AS XA ON XA.PlanID = XT.PlanID AND XA.OutlineNumber = XT.OutlineNumber
            ) AS YT
              LEFT JOIN CL ON YT.ClientID = CL.ClientID
              LEFT JOIN EM AS PM ON YT.ProjMgr = PM.Employee
              LEFT JOIN CFGSuffix PMSuffix ON PMSuffix.Code = PM.Suffix
              LEFT JOIN Organization AS O ON YT.Org = O.Org

        END /* END ELSE IF (@strMode = 'C') */
    
    END /* END WBS Rows */
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
