SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION  [dbo].[GetVisionAuditingDetail] ()   RETURNS varchar(1) AS  
BEGIN 
  return CONVERT(varchar(1),(SELECT SESSION_CONTEXT(N'AuditDetail')))
END
GO
