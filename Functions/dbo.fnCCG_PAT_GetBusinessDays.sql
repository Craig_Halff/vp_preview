SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   FUNCTION [dbo].[fnCCG_PAT_GetBusinessDays](@StartDate datetime, @EndDate datetime, @Company AS Nvarchar(14))
RETURNS float
AS BEGIN
      DECLARE @Minutes float

      SELECT @Minutes =
            DATEDIFF(minute,@StartDate,@EndDate)
            - DATEDIFF(wk,@StartDate,@EndDate) * 2880
            - CASE
                  WHEN DATENAME(dw, @StartDate) = 'Friday'    AND DATENAME(dw, @EndDate) =  'Sunday'   THEN -1440
                  WHEN DATENAME(dw, @StartDate) <> 'Saturday' AND DATENAME(dw, @EndDate) =  'Saturday' THEN  1440
                  WHEN DATENAME(dw, @StartDate) = 'Saturday'  AND DATENAME(dw, @EndDate) <> 'Saturday' THEN -1440
                  ELSE 0
            END
            - (SELECT COUNT(*) * 1440 FROM CFGHoliday WHERE HolidayDate BETWEEN @StartDate AND @EndDate
                  AND DATENAME(dw, holidaydate) <> 'Saturday' AND DATENAME(dw, holidaydate) <> 'Sunday'
                  AND Company = @Company)

      RETURN @Minutes
END
GO
