SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$tabLabEAC]
  (@strWBS1 Nvarchar(30),
   @strWBS2 Nvarchar(7),
   @strWBS3 Nvarchar(7),
   @strJTDDate VARCHAR(8), /* Date must be in format: 'yyyymmdd' regardless of UI Culture. */
   @strScale varchar(1) = 'm')
  RETURNS @tabLabEAC TABLE
    (StartDate datetime,
     JEFlag varchar(1) COLLATE database_default,
     JTDLabCost decimal(19,4),
     JTDLabBill decimal(19,4),
     ETCLabCost decimal(19,4),
     ETCLabBill decimal(19,4),
     EACLabCost decimal(19,4),
     EACLabBill decimal(19,4)
    )
BEGIN -- Function PN$tabLabEAC
 
  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime

  DECLARE @strCompany Nvarchar(14)
  DECLARE @strMultiCompanyEnabled varchar(1)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

  DECLARE @dtStartDate datetime
  DECLARE @dtEndDate datetime
  DECLARE @dtIntervalEnd datetime
  
  DECLARE @intWkEndDay AS int
  
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int

  DECLARE @strVorN char(1)
  
  DECLARE @tabTask TABLE
    (TaskID varchar(32) COLLATE database_default
     PRIMARY KEY(TaskID))
       
	DECLARE @tabLD TABLE 
    (RowID int IDENTITY(1,1),
     TransDate datetime,
     JTDCost decimal(19,4),
     JTDBill decimal(19,4)
     PRIMARY KEY(RowID, TransDate))

	DECLARE @tabETC TABLE 
    (TimePhaseID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     StartDate datetime, 
     EndDate datetime, 
     PeriodCost decimal(19,4),
     PeriodBill decimal(19,4)
     PRIMARY KEY(TimePhaseID, StartDate, EndDate))

  DECLARE @tabCalendarInterval TABLE
    (StartDate datetime,
     EndDate datetime
     PRIMARY KEY(StartDate, EndDate))
          
  DECLARE @tabETCTPD TABLE 
    (RowID int identity(1, 1),
     TimePhaseID varchar(32) COLLATE database_default,
     CIStartDate datetime, 
     StartDate datetime, 
     EndDate datetime, 
     PeriodCost decimal(19,4),
     PeriodBill decimal(19,4)
     PRIMARY KEY(RowID, TimePhaseID)) 
          
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled flag.
  
  SELECT
    @strMultiCompanyEnabled = MultiCompanyEnabled 
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat     

  -- Save Company string for use later.
  
  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          
  -- Set JTD/ETC Dates.
  
  SET @dtJTDDate = CONVERT(datetime, @strJTDDate)
  SET @dtETCDate = DATEADD(d, 1, @dtJTDDate) 

  -- Set Start of Week = Monday, End of Week = Sunday.
  
  SELECT @intWkEndDay = 1
      
  -- Get decimal settings.
  
  SET @intAmtCostDecimals = 4
  SET @intAmtBillDecimals = 4
       
  -- Determine which set of tables need to be used.

  SET @strVorN = dbo.PN$PlanIs_VorN(@strWBS1)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Extract Tasks from Plans that are marked as "Included in Utilization".
  -- There could be multiple Plans with RPPlan.UlilizationIncludeFlag = 'Y'
  -- Extract TPD records after ETCDate.
  -- Prorate the TPD record that straddles ETCDate.

  IF (@strVorN = 'V')
    BEGIN

      INSERT @tabTask
        (TaskID)
        SELECT
          T.TaskID
          FROM RPTask AS T
            INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND T.WBS1 = @strWBS1 AND
              (ISNULL(T.WBS2, ' ') = @strWBS2) AND 
              (ISNULL(T.WBS3, ' ') = @strWBS3) AND
              (T.WBSType = 'WBS1' OR T.WBSType = 'WBS2' OR T.WBSType = 'WBS3') 

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	    INSERT @tabETC
        (TimePhaseID,
         TaskID,
         StartDate, 
         EndDate,
         PeriodCost,
         PeriodBill)
		    SELECT
          TPD.TimePhaseID,
          TPD.TaskID,
          CASE WHEN @dtETCDate > TPD.StartDate THEN @dtETCDate ELSE TPD.StartDate END AS StartDate, 
          TPD.EndDate AS EndDate,
          CASE WHEN TPD.StartDate >= @dtETCDate 
               THEN TPD.PeriodCost
	             ELSE TPD.PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END AS PeriodCost,
          CASE WHEN TPD.StartDate >= @dtETCDate 
               THEN TPD.PeriodBill
	             ELSE TPD.PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END AS PeriodBill
          FROM @tabTask AS T 
            INNER JOIN RPPlannedLabor AS TPD ON (T.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL AND TPD.EndDate >= @dtETCDate) 

     END /* End If (@strVorN = 'V') */
  ELSE IF (@strVorN = 'N')
    BEGIN

      INSERT @tabTask
        (TaskID)
        SELECT
          T.TaskID
          FROM PNTask AS T
            INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND T.WBS1 = @strWBS1 AND
              (ISNULL(T.WBS2, ' ') = @strWBS2) AND 
              (ISNULL(T.WBS3, ' ') = @strWBS3) AND
              (T.WBSType = 'WBS1' OR T.WBSType = 'WBS2' OR T.WBSType = 'WBS3') 

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	    INSERT @tabETC
        (TimePhaseID,
         TaskID,
         StartDate, 
         EndDate, 
         PeriodCost,
         PeriodBill)
		    SELECT
          TPD.TimePhaseID,
          TPD.TaskID,
          CASE WHEN @dtETCDate > TPD.StartDate THEN @dtETCDate ELSE TPD.StartDate END AS StartDate, 
          TPD.EndDate AS EndDate,
          CASE WHEN TPD.StartDate >= @dtETCDate 
               THEN TPD.PeriodCost
	             ELSE TPD.PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END AS PeriodCost,
          CASE WHEN TPD.StartDate >= @dtETCDate 
               THEN TPD.PeriodBill
	             ELSE TPD.PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END AS PeriodBill
          FROM @tabTask AS T 
            INNER JOIN PNPlannedLabor AS TPD ON (T.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL AND TPD.EndDate >= @dtETCDate) 

  END /* End If (@strVorN = 'N') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	-- Save Labor JTD & Unposted Labor JTD for this Project into temp table.
  -- The JTD data are posted at the leaf node in the WBS Tree. 
  -- Therefore, we need to calculate JTD for the summary levels.

	INSERT @tabLD
    (TransDate,
     JTDCost,
     JTDBill)      
		SELECT
      TransDate,
      SUM(ISNULL(JTDCost, 0.0000)) AS JTDCost,
      SUM(ISNULL(JTDBill, 0.0000)) AS JTDBill 
      FROM
        (SELECT
           LD.TransDate,
           SUM(LD.RegAmtProjectCurrency + LD.OvtAmtProjectCurrency + LD.SpecialOvtAmtProjectCurrency) AS JTDCost,
           SUM(LD.BillExt) AS JTDBill
           FROM LD 
           WHERE LD.ProjectCost = 'Y' AND LD.TransDate <= @dtJTDDate AND LD.WBS1 = @strWBS1 AND
             (LD.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
             (LD.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
           GROUP BY LD.TransDate
         UNION ALL SELECT
           TD.TransDate,
           SUM(TD.RegAmtProjectCurrency + TD.OvtAmtProjectCurrency + TD.SpecialOvtAmtProjectCurrency) AS JTDCost,
           SUM(TD.BillExt) AS JTDBill
           FROM tkDetail AS TD
             INNER JOIN EM ON TD.Employee = EM.Employee  
             INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
           WHERE TD.TransDate <= @dtJTDDate AND TD.WBS1 = @strWBS1 AND
             (TD.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
             (TD.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
           GROUP BY TD.TransDate
         UNION ALL SELECT
           TD.TransDate,
           SUM(TD.RegAmtProjectCurrency + TD.OvtAmtProjectCurrency + TD.SpecialOvtAmtProjectCurrency) AS JTDCost,
           SUM(TD.BillExt) AS JTDBill
           FROM tsDetail AS TD
             INNER JOIN EM ON TD.Employee = EM.Employee  
             INNER JOIN tsControl AS TC ON (TC.Posted = 'N' AND TD.Batch = TC.Batch)
           WHERE TD.TransDate <= @dtJTDDate AND TD.WBS1 = @strWBS1 AND
             (TD.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
             (TD.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
           GROUP BY TD.TransDate
        ) AS X
      GROUP BY X.TransDate

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find the MIN and MAX dates of all TPD.
  
  SELECT @dtStartDate = MIN(MINDate), @dtEndDate = MAX(MAXDate)
    FROM
      (SELECT 
         CASE WHEN @dtETCDate > TPD.StartDate THEN @dtETCDate ELSE TPD.StartDate END AS MINDate, 
         TPD.EndDate AS MAXDate 
         FROM @tabETC AS TPD 
       UNION SELECT
         TransDate AS MINDate,
         TransDate AS MAXDate
         FROM @tabLD
      ) AS X
  
  -- Contruct Calendar Intervals using the input scale.
  -- Note that in this algorithm, the first Calendar Interval starts with the first instance of a JTD transaction.
  -- There may be Planned data in RPPlannedLabor further back in the past, but those are not pick up
  -- because we only deal with Planned data after ETC date.
      
  WHILE (@dtStartDate <= @dtEndDate)
    BEGIN
        
      -- Compute End Date of interval.

      IF (@strScale = 'd') 
        SET @dtIntervalEnd = @dtStartDate
      ELSE
        SET @dtIntervalEnd = dbo.DLTK$IntervalEnd(@dtStartDate, @strScale, @intWkEndDay)
        
      IF (@dtIntervalEnd > @dtEndDate) 
        SET @dtIntervalEnd = @dtEndDate
            
      -- Insert new Calendar Interval record.
          
      INSERT @tabCalendarInterval(StartDate, EndDate)
        VALUES (@dtStartDate, @dtIntervalEnd)
          
      -- Set Start Date for next interval.
          
      SET @dtStartDate = DATEADD(d, 1, @dtIntervalEnd)
        
    END -- End While
       
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
              
  -- Put ETC TPD data into new calendar intervals.

  INSERT @tabETCTPD
    (TimePhaseID,
     CIStartDate,
     StartDate, 
     EndDate,
     PeriodCost,
     PeriodBill)
     SELECT
       TimePhaseID,
       CASE WHEN @dtETCDate > CIStartDate THEN @dtETCDate ELSE CIStartDate END AS CIStartDate,
       StartDate AS StartDate, 
       EndDate AS EndDate,
       ROUND(ISNULL(PeriodCost * ProrateRatio, 0), @intAmtCostDecimals) AS PeriodCost,
       ROUND(ISNULL(PeriodBill * ProrateRatio, 0), @intAmtBillDecimals) AS PeriodBill
     FROM 
       (SELECT
          TPD.TimePhaseID AS TimePhaseID,
          CI.StartDate AS CIStartDate,
          CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
          CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
          PeriodCost AS PeriodCost,
          PeriodBill AS PeriodBill,
          CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
               THEN dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                               THEN TPD.StartDate 
                                               ELSE CI.StartDate END, 
                                          CASE WHEN TPD.EndDate < CI.EndDate 
                                               THEN TPD.EndDate 
                                               ELSE CI.EndDate END, 
                                          TPD.StartDate, TPD.EndDate,
                                          @strCompany)
               ELSE 1 END AS ProrateRatio
          FROM @tabETC AS TPD
            INNER JOIN @tabCalendarInterval AS CI ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
       ) AS X
     WHERE (PeriodBill IS NOT NULL AND ROUND((PeriodBill * ProrateRatio), @intAmtBillDecimals) != 0)
                   
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT INTO @tabLabEAC
    (StartDate,
     JEFlag,
     JTDLabCost,
     JTDLabBill,
     ETCLabCost,
     ETCLabBill,
     EACLabCost,
     EACLabBill
    )
    SELECT
      CIStartDate AS StartDate,
      JEFlag,
      SUM(ISNULL(Z.JTDCost, 0)) AS JTDLabCost,
      SUM(ISNULL(Z.JTDBill, 0)) AS JTDLabBill,
      SUM(ISNULL(Z.ETCCost, 0)) AS ETCLabCost,
      SUM(ISNULL(Z.ETCBill, 0)) AS ETCLabBill,
      SUM(ISNULL(Z.JTDCost, 0)) +  SUM(ISNULL(Z.ETCCost, 0)) AS EACLabCost,
      SUM(ISNULL(Z.JTDBill, 0)) +  SUM(ISNULL(Z.ETCBill, 0)) AS EACLabBill
      FROM
        (SELECT
           CI.StartDate AS CIStartDate,
           'J' AS JEFlag,
           SUM(JTDCost) AS JTDCost,
           SUM(JTDBill) AS JTDBill,
           0 AS ETCCost,
           0 AS ETCBill
           FROM @tabLD AS LD
             INNER JOIN @tabCalendarInterval AS CI ON LD.TransDate <= CI.EndDate AND LD.TransDate >= CI.StartDate
           GROUP BY CI.StartDate
         UNION ALL SELECT
           TPD.CIStartDate AS CIStartDate,
           'E'  AS JEFlag,
           0 AS JTDCost,
           0 AS JTDBill,
           SUM(TPD.PeriodCost) AS ETCCost,
           SUM(TPD.PeriodBill) AS ETCBill
           FROM @tabETCTPD AS TPD
           GROUP BY CIStartDate
        ) AS Z
        GROUP BY Z.CIStartDate, Z.JEFlag

  RETURN

END -- PN$tabLabEAC
GO
