SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PR$tabConETC]
  (@strWBS1 Nvarchar(30),
   @strWBS2 Nvarchar(7),
   @strWBS3 Nvarchar(7),
   @strETCDate VARCHAR(8)) -- Date must be in format: 'yyyymmdd' regardless of UI CulturC.

  RETURNS @tabETC TABLE (
     ETCCost decimal(19,4),
     ETCBill decimal(19,4),
     ETCDirCost decimal(19,4),
     ETCDirBill decimal(19,4),
     ETCReimCost decimal(19,4),
     ETCReimBill decimal(19,4)
  )

BEGIN

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
 
  DECLARE @strPlanID varchar(32)
  DECLARE @strOutlineNumber varchar(255)
  DECLARE @strVorN varchar(1)

  DECLARE @strCompany Nvarchar(14)
  DECLARE @strMultiCompanyEnabled varchar(1)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int

  DECLARE @decFutureCost decimal(19,4)
  DECLARE @decFutureBill decimal(19,4)
  DECLARE @decFutureDirCost decimal(19,4)
  DECLARE @decFutureDirBill decimal(19,4)
  DECLARE @decFutureReimCost decimal(19,4)
  DECLARE @decFutureReimBill decimal(19,4)

  DECLARE @tabTPD TABLE(
    RowID int IDENTITY(1,1),
    ConsultantID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4),
    PeriodDirCost decimal(19,4),
    PeriodDirBill decimal(19,4),
    PeriodReimCost decimal(19,4),
    PeriodReimBill decimal(19,4)
    PRIMARY KEY(RowID, ConsultantID)
  )

  DECLARE @tabAcctVE TABLE(
    RowID  int IDENTITY(1,1),
    WBS1 Nvarchar(30),
    WBS2 Nvarchar(7),
    WBS3 Nvarchar(7),
    Account Nvarchar(13) COLLATE database_default,
    Vendor Nvarchar(30) COLLATE database_default,
    UNIQUE(RowID, WBS1, WBS2, WBS3, Account, Vendor)
  )

  DECLARE @tabPlanned TABLE(
    RowID  int IDENTITY(1,1),
    WBS1 Nvarchar(30),
    WBS2 Nvarchar(7),
    WBS3 Nvarchar(7),
    Account Nvarchar(13) COLLATE database_default,
    Vendor Nvarchar(30) COLLATE database_default,
    PlannedCost decimal(19,4),
    PlannedBill decimal(19,4),
    PlannedDirCost decimal(19,4),
    PlannedDirBill decimal(19,4),
    PlannedReimCost decimal(19,4),
    PlannedReimBill decimal(19,4)
    UNIQUE(RowID, WBS1, WBS2, WBS3, Account, Vendor)
  )
   
 	DECLARE @tabJTD TABLE(
    RowID  int IDENTITY(1,1),
    WBS1 Nvarchar(30),
    WBS2 Nvarchar(7),
    WBS3 Nvarchar(7),
    Account Nvarchar(13) COLLATE database_default,
    Vendor Nvarchar(30) COLLATE database_default,
    JTDCost decimal(19,4),
    JTDBill decimal(19,4),
    JTDDirCost decimal(19,4),
    JTDDirBill decimal(19,4),
    JTDReimCost decimal(19,4),
    JTDReimBill decimal(19,4)
    UNIQUE(RowID, WBS1, WBS2, WBS3, Account, Vendor)
  )
       
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled flag.
  
  SELECT
    @strMultiCompanyEnabled = MultiCompanyEnabled
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat     

  -- Save Company string for use later.
  
  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          
  -- Set ETC & JTD Dates.
  
  SET @dtETCDate = CONVERT(datetime, @strETCDate) 
  SET @dtJTDDate = DATEADD(d, -1, @dtETCDate) 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine whether the Project has a Navigator Plan.

  SET @strVorN = 
    CASE WHEN EXISTS 
      (SELECT 'X' 
         FROM PNTask AS T
         INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND 
         T.WBSType = 'WBS1' AND T.WBS1 = @strWBS1) 
      THEN 'N' ELSE 'V' END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find the RPTask row corresponds to the PR row.

  SET @strPlanID = NULL
  SET @strOutlineNumber = NULL

  SELECT @strPlanID = P.PlanID, @strOutlineNumber = MIN(PT.OutlineNumber)
    FROM PR
      LEFT JOIN RPTask AS PT ON PR.WBS1 = PT.WBS1 AND PR.WBS2 = ISNULL(PT.WBS2, ' ') AND PR.WBS3 = ISNULL(PT.WBS3, ' ') AND
        PT.WBSType = 
          CASE 
            WHEN PR.WBS2 = ' ' AND PR.WBS3 = ' ' THEN 'WBS1'
            WHEN PT.WBS2 <> ' ' AND PR.WBS3 = ' ' THEN 'WBS2'
            WHEN PT.WBS2 <> ' ' AND PR.WBS3 <> ' ' THEN 'WBS3'
          END
      LEFT JOIN RPPlan AS P ON PT.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y'
    WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = @strWBS2 AND PR.WBS3 = @strWBS3
    GROUP BY P.PlanID

  -- Get decimal settings.
  
  SELECT @intAmtCostDecimals = AmtCostDecimals,
         @intAmtBillDecimals = AmtBillDecimals
    FROM dbo.RP$tabDecimals(@strPlanID)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- If the Project has a Navigator Plan then calculate ETC by comparing Planned to JTD.
  -- Otherwise, calculate ETC as "remaining Planned" from tomorrow forward.

  IF (@strVorN = 'N')
    BEGIN

      INSERT @tabPlanned(
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor,
        PlannedCost,
        PlannedBill,
        PlannedDirCost,
        PlannedDirBill,
        PlannedReimCost,
        PlannedReimBill
      )
        SELECT
          TL.WBS1 AS WBS1,
          TL.WBS2 AS WBS2,
          TL.WBS3 AS WBS3,
          TL.Account AS Account,
          TL.Vendor AS Vendor,
          ROUND(ISNULL(SUM(TL.PeriodCost), 0.0000), @intAmtCostDecimals) AS PlannedCost,
          ROUND(ISNULL(SUM(TL.PeriodBill), 0.0000), @intAmtBillDecimals) AS PlannedBill,
          ROUND(ISNULL(SUM(TL.PeriodDirCost), 0.0000), @intAmtCostDecimals) AS PlannedDirCost,
          ROUND(ISNULL(SUM(TL.PeriodDirBill), 0.0000), @intAmtBillDecimals) AS PlannedDirBill,
          ROUND(ISNULL(SUM(TL.PeriodReimCost), 0.0000), @intAmtCostDecimals) AS PlannedReimCost,
          ROUND(ISNULL(SUM(TL.PeriodReimBill), 0.0000), @intAmtBillDecimals) AS PlannedReimBill
          FROM (
            /* Find TPD rows attached to RPConsultant rows that are the descendants of the RPTask row that is mapped to the PR row in question */
            SELECT DISTINCT 
              C.ConsultantID,
              C.WBS1 AS WBS1,
              C.WBS2 AS WBS2,
              C.WBS3 AS WBS3,
              C.Account AS Account,
              C.Vendor AS Vendor,
              TPD.StartDate, 
              TPD.EndDate, 
              ROUND(ISNULL(TPD.PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
              ROUND(ISNULL(TPD.PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill,
              ROUND(CASE WHEN CA.Type = 8 THEN ISNULL(TPD.PeriodCost, 0) ELSE 0 END, @intAmtCostDecimals) AS PeriodDirCost,
              ROUND(CASE WHEN CA.Type = 8 THEN ISNULL(TPD.PeriodBill, 0) ELSE 0 END, @intAmtBillDecimals) AS PeriodDirBill,
              ROUND(CASE WHEN CA.Type = 6 THEN ISNULL(TPD.PeriodCost, 0) ELSE 0 END, @intAmtCostDecimals) AS PeriodReimCost,
              ROUND(CASE WHEN CA.Type = 6 THEN ISNULL(TPD.PeriodBill, 0) ELSE 0 END, @intAmtBillDecimals) AS PeriodReimBill
              FROM RPPlannedConsultant AS TPD
                INNER JOIN RPConsultant AS C ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID AND TPD.ConsultantID = C.ConsultantID AND TPD.ConsultantID IS NOT NULL
                INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
                INNER JOIN CA ON C.Account = CA.Account
              WHERE TPD.PlanID = @strPlanID AND T.OutlineNumber LIKE (@strOutlineNumber + '%')
          ) AS TL
          GROUP BY WBS1, WBS2, WBS3, Account, Vendor

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabJTD(
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor,
        JTDCost,
        JTDBill,
        JTDDirCost,
        JTDDirBill,
        JTDReimCost,
        JTDReimBill
      )
		    SELECT
          X.WBS1,
          X.WBS2,
          X.WBS3,
          X.Account,
          X.Vendor,
          ROUND(ISNULL(SUM(JTDCost), 0.0000), 2) AS JTDCost,
          ROUND(ISNULL(SUM(JTDBill), 0.0000), 2) AS JTDBill,
          ROUND(ISNULL(SUM(JTDDirCost), 0.0000), 2) AS JTDDirCost,
          ROUND(ISNULL(SUM(JTDDirBill), 0.0000), 2) AS JTDDirBill,
          ROUND(ISNULL(SUM(JTDReimCost), 0.0000), 2) AS JTDReimCost,
          ROUND(ISNULL(SUM(JTDReimBill), 0.0000), 2) AS JTDReimBill
          FROM (
            SELECT
              LG.WBS1,
              LG.WBS2,
              LG.WBS3,
              LG.Account,
              LG.Vendor,
              ROUND(SUM(LG.AmountProjectCurrency), 2) AS JTDCost,  
              ROUND(SUM(LG.BillExt), 2) AS JTDBill,
              ROUND(SUM(CASE WHEN CA.Type = 8 THEN LG.AmountProjectCurrency ELSE 0 END), 2) AS JTDDirCost,
              ROUND(SUM(CASE WHEN CA.Type = 8 THEN LG.BillExt ELSE 0 END), 2) AS JTDDirBill,
              ROUND(SUM(CASE WHEN CA.Type = 6 THEN LG.AmountProjectCurrency ELSE 0 END), 2) AS JTDReimCost,
              ROUND(SUM(CASE WHEN CA.Type = 6 THEN LG.BillExt ELSE 0 END), 2) AS JTDReimBill
              FROM LedgerAR AS LG
                INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
              WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (8, 6) AND
                (LG.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
                (LG.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
              GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
            UNION ALL SELECT
              LG.WBS1,
              LG.WBS2,
              LG.WBS3,
              LG.Account,
              LG.Vendor,
              ROUND(SUM(LG.AmountProjectCurrency), 2) AS JTDCost,  
              ROUND(SUM(LG.BillExt), 2) AS JTDBill,
              ROUND(SUM(CASE WHEN CA.Type = 8 THEN LG.AmountProjectCurrency ELSE 0 END), 2) AS JTDDirCost,
              ROUND(SUM(CASE WHEN CA.Type = 8 THEN LG.BillExt ELSE 0 END), 2) AS JTDDirBill,
              ROUND(SUM(CASE WHEN CA.Type = 6 THEN LG.AmountProjectCurrency ELSE 0 END), 2) AS JTDReimCost,
              ROUND(SUM(CASE WHEN CA.Type = 6 THEN LG.BillExt ELSE 0 END), 2) AS JTDReimBill
              FROM LedgerAP AS LG
                INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
              WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (8, 6) AND
                (LG.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
                (LG.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
              GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
            UNION ALL SELECT
              LG.WBS1,
              LG.WBS2,
              LG.WBS3,
              LG.Account,
              LG.Vendor,
              ROUND(SUM(LG.AmountProjectCurrency), 2) AS JTDCost,  
              ROUND(SUM(LG.BillExt), 2) AS JTDBill,
              ROUND(SUM(CASE WHEN CA.Type = 8 THEN LG.AmountProjectCurrency ELSE 0 END), 2) AS JTDDirCost,
              ROUND(SUM(CASE WHEN CA.Type = 8 THEN LG.BillExt ELSE 0 END), 2) AS JTDDirBill,
              ROUND(SUM(CASE WHEN CA.Type = 6 THEN LG.AmountProjectCurrency ELSE 0 END), 2) AS JTDReimCost,
              ROUND(SUM(CASE WHEN CA.Type = 6 THEN LG.BillExt ELSE 0 END), 2) AS JTDReimBill
              FROM LedgerEX AS LG
                INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
              WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (8, 6) AND
                (LG.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
                (LG.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
              GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
            UNION ALL SELECT
              LG.WBS1,
              LG.WBS2,
              LG.WBS3,
              LG.Account,
              LG.Vendor,
              ROUND(SUM(LG.AmountProjectCurrency), 2) AS JTDCost,  
              ROUND(SUM(LG.BillExt), 2) AS JTDBill,
              ROUND(SUM(CASE WHEN CA.Type = 8 THEN LG.AmountProjectCurrency ELSE 0 END), 2) AS JTDDirCost,
              ROUND(SUM(CASE WHEN CA.Type = 8 THEN LG.BillExt ELSE 0 END), 2) AS JTDDirBill,
              ROUND(SUM(CASE WHEN CA.Type = 6 THEN LG.AmountProjectCurrency ELSE 0 END), 2) AS JTDReimCost,
              ROUND(SUM(CASE WHEN CA.Type = 6 THEN LG.BillExt ELSE 0 END), 2) AS JTDReimBill
              FROM LedgerMISC AS LG
                INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
              WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (8, 6) AND
                (LG.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
                (LG.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
              GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
            UNION ALL SELECT
              POC.WBS1,
              POC.WBS2,
              POC.WBS3,
              POC.Account,
              POM.Vendor,
              ROUND(SUM(AmountProjectCurrency), 2) AS JTDCost,  
              ROUND(SUM(BillExt), 2) AS JTDBill,
              ROUND(SUM(CASE WHEN CA.Type = 8 THEN AmountProjectCurrency ELSE 0 END), 2) AS JTDDirCost,
              ROUND(SUM(CASE WHEN CA.Type = 8 THEN BillExt ELSE 0 END), 2) AS JTDDirBill,
              ROUND(SUM(CASE WHEN CA.Type = 6 THEN AmountProjectCurrency ELSE 0 END), 2) AS JTDReimCost,
              ROUND(SUM(CASE WHEN CA.Type = 6 THEN BillExt ELSE 0 END), 2) AS JTDReimBill
              FROM POCommitment AS POC 
                INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
                INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey
                INNER JOIN CA ON POC.Account = CA.Account
              WHERE POM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (8, 6) AND 
                (AmountProjectCurrency != 0 AND BillExt != 0) AND
                (POC.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
                (POC.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
              GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
            UNION ALL SELECT
              POC.WBS1,
              POC.WBS2,
              POC.WBS3,
              POC.Account,
              POM.Vendor,
              ROUND(SUM(AmountProjectCurrency), 2) AS JTDCost,  
              ROUND(SUM(BillExt), 2) AS JTDBill,
              ROUND(SUM(CASE WHEN CA.Type = 8 THEN AmountProjectCurrency ELSE 0 END), 2) AS JTDDirCost,
              ROUND(SUM(CASE WHEN CA.Type = 8 THEN BillExt ELSE 0 END), 2) AS JTDDirBill,
              ROUND(SUM(CASE WHEN CA.Type = 6 THEN AmountProjectCurrency ELSE 0 END), 2) AS JTDReimCost,
              ROUND(SUM(CASE WHEN CA.Type = 6 THEN BillExt ELSE 0 END), 2) AS JTDReimBill
              FROM POCommitment AS POC 
                INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
                INNER JOIN POCOMaster AS POCOM ON POD.COPKey = POCOM.PKey
                INNER JOIN POMaster AS POM ON POCOM.MasterPKey = POM.MasterPKey
                INNER JOIN CA ON POC.Account = CA.Account
              WHERE POCOM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (8, 6) AND 
                (AmountProjectCurrency != 0 AND BillExt != 0) AND
                (POC.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
                (POC.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
              GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
          ) AS X
          GROUP BY X.WBS1, X.WBS2, X.WBS3, X.Account, X.Vendor
          HAVING ROUND(SUM(ISNULL(JTDCost, 0.0000)), 2) <> 0 OR ROUND(SUM(ISNULL(JTDBill, 0.0000)), 2) <> 0

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Collect all combination of Accounts and Vendor that have Planned data.
      -- We would not care about JTD rows that show up on a Navigator Plan but does not have Planned data.
      -- The reason for that is: ETC is zero when Planned is less than JTD. 
      -- If there is no Planned data then there is no need to calculate ETC for that row.

      INSERT @tabAcctVE (
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor
      )
        SELECT DISTINCT 
          WBS1,
          WBS2,
          WBS3,
          Account,
          Vendor 
          FROM @tabPlanned

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Calculate Task ETC.

      INSERT @tabETC (
        ETCCost,
        ETCBill,
        ETCDirCost,
        ETCDirBill,
        ETCReimCost,
        ETCReimBill
      )

        SELECT  
          ISNULL(SUM(ETCCost), 0.0000) AS ETCCost,
          ISNULL(SUM(ETCBill), 0.0000) AS ETCBill,
          ISNULL(SUM(ETCDirCost), 0.0000) AS ETCDirCost,
          ISNULL(SUM(ETCDirBill), 0.0000) AS ETCDirBill,
          ISNULL(SUM(ETCReimCost), 0.0000) AS ETCReimCost,
          ISNULL(SUM(ETCReimBill), 0.0000) AS ETCReimBill
          FROM (
            SELECT

              Y.Account AS Account,
              Y.Vendor AS Vendor,

              CASE
                WHEN ISNULL(Y.PlannedCost, 0.0000) > ISNULL(Y.JTDCost, 0.0000)
                THEN ISNULL(Y.PlannedCost, 0.0000) - ISNULL(Y.JTDCost, 0.0000)
                ELSE 0.0000
              END AS ETCCost,

              CASE
                WHEN ISNULL(Y.PlannedBill, 0.0000) > ISNULL(Y.JTDBill, 0.0000)
                THEN ISNULL(Y.PlannedBill, 0.0000) - ISNULL(Y.JTDBill, 0.0000)
                ELSE 0.0000
              END AS ETCBill,

              CASE
                WHEN ISNULL(Y.PlannedDirCost, 0.0000) > ISNULL(Y.JTDDirCost, 0.0000)
                THEN ISNULL(Y.PlannedDirCost, 0.0000) - ISNULL(Y.JTDDirCost, 0.0000)
                ELSE 0.0000
              END AS ETCDirCost,

              CASE
                WHEN ISNULL(Y.PlannedDirBill, 0.0000) > ISNULL(Y.JTDDirBill, 0.0000)
                THEN ISNULL(Y.PlannedDirBill, 0.0000) - ISNULL(Y.JTDDirBill, 0.0000)
                ELSE 0.0000
              END AS ETCDirBill,

              CASE
                WHEN ISNULL(Y.PlannedReimCost, 0.0000) > ISNULL(Y.JTDReimCost, 0.0000)
                THEN ISNULL(Y.PlannedReimCost, 0.0000) - ISNULL(Y.JTDReimCost, 0.0000)
                ELSE 0.0000
              END AS ETCReimCost,

              CASE
                WHEN ISNULL(Y.PlannedReimBill, 0.0000) > ISNULL(Y.JTDReimBill, 0.0000)
                THEN ISNULL(Y.PlannedReimBill, 0.0000) - ISNULL(Y.JTDReimBill, 0.0000)
                ELSE 0.0000
              END AS ETCReimBill

              FROM (
                SELECT
                  C.WBS1 AS WBS1,
                  C.WBS2 AS WBS2,
                  C.WBS3 AS WBS3,
                  C.Account AS Account,
                  C.Vendor AS Vendor,
                  ISNULL(SUM(PlannedCost), 0.0000) AS PlannedCost,
                  ISNULL(SUM(PlannedBill), 0.0000) AS PlannedBill,
                  ISNULL(SUM(PlannedDirCost), 0.0000) AS PlannedDirCost,
                  ISNULL(SUM(PlannedDirBill), 0.0000) AS PlannedDirBill,
                  ISNULL(SUM(PlannedReimCost), 0.0000) AS PlannedReimCost,
                  ISNULL(SUM(PlannedReimBill), 0.0000) AS PlannedReimBill,
                  ISNULL(SUM(JTDCost), 0.0000) AS JTDCost,
                  ISNULL(SUM(JTDBill), 0.0000) AS JTDBill,
                  ISNULL(SUM(JTDDirCost), 0.0000) AS JTDDirCost,
                  ISNULL(SUM(JTDDirBill), 0.0000) AS JTDDirBill,
                  ISNULL(SUM(JTDReimCost), 0.0000) AS JTDReimCost,
                  ISNULL(SUM(JTDReimBill), 0.0000) AS JTDReimBill
                  FROM @tabAcctVE AS C

                    LEFT JOIN (
                      SELECT
                        WBS1 AS WBS1,
                        WBS2 AS WBS2,
                        WBS3 AS WBS3,
                        Account AS Account,
                        Vendor AS Vendor,
                        SUM(PlannedCost) AS PlannedCost,
                        SUM(PlannedBill) AS PlannedBill,
                        SUM(PlannedDirCost) AS PlannedDirCost,
                        SUM(PlannedDirBill) AS PlannedDirBill,
                        SUM(PlannedReimCost) AS PlannedReimCost,
                        SUM(PlannedReimBill) AS PlannedReimBill
                        FROM @tabPlanned
                        GROUP BY WBS1, WBS2, WBS3, Account, Vendor
                    ) AS P
                      ON C.WBS1 = P.WBS1 AND ISNULL(C.WBS2, '|') = ISNULL(P.WBS2, '|') AND ISNULL(C.WBS3, '|') = ISNULL(P.WBS3, '|') AND
                        ISNULL(C.Account, '|') = ISNULL(P.Account, '|') AND ISNULL(C.Vendor, '|') = ISNULL(P.Vendor, '|')

                    LEFT JOIN (
                      SELECT
                        WTZ.WBS1 AS WBS1,
                        WTZ.WBS2 AS WBS2,
                        WTZ.WBS3 AS WBS3,
                        JZ.Account AS Account,
                        JZ.Vendor AS Vendor,
                        ISNULL(SUM(JZ.JTDCost), 0.0000) AS JTDCost,
                        ISNULL(SUM(JZ.JTDBill), 0.0000) AS JTDBill,
                        ISNULL(SUM(JZ.JTDDirCost), 0.0000) AS JTDDirCost,
                        ISNULL(SUM(JZ.JTDDirBill), 0.0000) AS JTDDirBill,
                        ISNULL(SUM(JZ.JTDReimCost), 0.0000) AS JTDReimCost,
                        ISNULL(SUM(JZ.JTDReimBill), 0.0000) AS JTDReimBill
                        FROM @tabAcctVE AS WTZ
                          OUTER APPLY (
                            SELECT
                              Account AS Account,
                              Vendor AS Vendor,
                              ISNULL(SUM(JTD.JTDCost), 0.0000) AS JTDCost,
                              ISNULL(SUM(JTD.JTDBill), 0.0000) AS JTDBill,
                              ISNULL(SUM(JTD.JTDDirCost), 0.0000) AS JTDDirCost,
                              ISNULL(SUM(JTD.JTDDirBill), 0.0000) AS JTDDirBill,
                              ISNULL(SUM(JTD.JTDReimCost), 0.0000) AS JTDReimCost,
                              ISNULL(SUM(JTD.JTDReimBill), 0.0000) AS JTDReimBill
                              FROM @tabJTD AS JTD
                              WHERE JTD.WBS1 = WTZ.WBS1 AND
                                (JTD.WBS2 LIKE CASE WHEN WTZ.WBS2 IS NULL THEN '%' ELSE WTZ.WBS2 END) AND
                                (JTD.WBS3 LIKE CASE WHEN WTZ.WBS3 IS NULL THEN '%' ELSE WTZ.WBS3 END) AND
                                ISNULL(JTD.Account, '|') = ISNULL(WTZ.Account, '|') AND ISNULL(JTD.Vendor, '|') = ISNULL(WTZ.Vendor, '|')
                              GROUP BY JTD.WBS1, JTD.WBS2, JTD.WBS3, JTD.Account, JTD.Vendor
                          ) AS JZ
                        GROUP BY WTZ.WBS1, WTZ.WBS2, WTZ.WBS3, JZ.Account, JZ.Vendor
                    ) AS J
                      ON C.WBS1 = J.WBS1 AND ISNULL(C.WBS2, '|') = ISNULL(J.WBS2, '|') AND ISNULL(C.WBS3, '|') = ISNULL(J.WBS3, '|') AND 
                        ISNULL(C.Account, '|') = ISNULL(J.Account, '|') AND ISNULL(C.Vendor, '|') = ISNULL(J.Vendor, '|')

                  GROUP BY C.WBS1, C.WBS2, C.WBS3, C.Account, C.Vendor

              ) AS Y

          ) AS X

    END /* End If (@strVorN = 'N') */

  ELSE
    BEGIN

      -- Sum together the amounts that are in the future periods beyond the ETC Date boundary.

      SET @decFutureCost = 0.0000
      SET @decFutureBill = 0.0000
      SET @decFutureDirCost = 0.0000
      SET @decFutureDirBill = 0.0000
      SET @decFutureReimCost = 0.0000
      SET @decFutureReimBill = 0.0000

      SELECT
        @decFutureCost = ISNULL(SUM(TL.PeriodCost), 0.0000),
        @decFutureBill = ISNULL(SUM(TL.PeriodBill), 0.0000),
        @decFutureDirCost = ISNULL(SUM(TL.PeriodDirCost), 0.0000),
        @decFutureDirBill = ISNULL(SUM(TL.PeriodDirBill), 0.0000),
        @decFutureReimCost = ISNULL(SUM(TL.PeriodReimCost), 0.0000),
        @decFutureReimBill = ISNULL(SUM(TL.PeriodReimBill), 0.0000)
        FROM (
          /* Find TPD rows attached to RPConsultant rows that are the descendants of the RPTask row that is mapped to the PR row in question */
          SELECT DISTINCT 
            C.ConsultantID, 
            TPD.StartDate, 
            TPD.EndDate, 
            ROUND(ISNULL(TPD.PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
            ROUND(ISNULL(TPD.PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill,
            ROUND(CASE WHEN CA.Type = 8 THEN ISNULL(TPD.PeriodCost, 0) ELSE 0 END, @intAmtCostDecimals) AS PeriodDirCost,
            ROUND(CASE WHEN CA.Type = 8 THEN ISNULL(TPD.PeriodBill, 0) ELSE 0 END, @intAmtBillDecimals) AS PeriodDirBill,
            ROUND(CASE WHEN CA.Type = 6 THEN ISNULL(TPD.PeriodCost, 0) ELSE 0 END, @intAmtCostDecimals) AS PeriodReimCost,
            ROUND(CASE WHEN CA.Type = 6 THEN ISNULL(TPD.PeriodBill, 0) ELSE 0 END, @intAmtBillDecimals) AS PeriodReimBill
            FROM RPPlannedConsultant AS TPD
              INNER JOIN RPConsultant AS C ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID AND TPD.ConsultantID = C.ConsultantID AND TPD.ConsultantID IS NOT NULL
              INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
              INNER JOIN CA ON C.Account = CA.Account
            WHERE TPD.PlanID = @strPlanID AND T.OutlineNumber LIKE (@strOutlineNumber + '%') AND TPD.StartDate >= @dtETCDate
        ) AS TL

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Collect the TPD that straddle ETC DatC.

      INSERT @tabTPD (
        ConsultantID,
        StartDate,
        EndDate,
        PeriodCost,
        PeriodBill,
        PeriodDirCost,
        PeriodDirBill,
        PeriodReimCost,
        PeriodReimBill
      )
        SELECT
          TL.ConsultantID,
          TL.StartDate,
          TL.EndDate,
          TL.PeriodCost,
          TL.PeriodBill,
          TL.PeriodDirCost,
          TL.PeriodDirBill,
          TL.PeriodReimCost,
          TL.PeriodReimBill
          FROM (
            /* Find TPD rows attached to RPAssignment rows that are the descendants of the RPTask row that is mapped to the PR row in question */
            SELECT DISTINCT 
              C.ConsultantID, 
              TPD.StartDate, 
              TPD.EndDate, 
              ROUND(ISNULL(TPD.PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
              ROUND(ISNULL(TPD.PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill,
              ROUND(CASE WHEN CA.Type = 8 THEN ISNULL(TPD.PeriodCost, 0) ELSE 0 END, @intAmtCostDecimals) AS PeriodDirCost,
              ROUND(CASE WHEN CA.Type = 8 THEN ISNULL(TPD.PeriodBill, 0) ELSE 0 END, @intAmtBillDecimals) AS PeriodDirBill,
              ROUND(CASE WHEN CA.Type = 6 THEN ISNULL(TPD.PeriodCost, 0) ELSE 0 END, @intAmtCostDecimals) AS PeriodReimCost,
              ROUND(CASE WHEN CA.Type = 6 THEN ISNULL(TPD.PeriodBill, 0) ELSE 0 END, @intAmtBillDecimals) AS PeriodReimBill
              FROM RPPlannedConsultant AS TPD
                INNER JOIN RPConsultant AS C ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID AND TPD.ConsultantID = C.ConsultantID AND TPD.ConsultantID IS NOT NULL
                INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
                INNER JOIN CA ON C.Account = CA.Account
              WHERE TPD.PlanID = @strPlanID AND T.OutlineNumber LIKE (@strOutlineNumber + '%') 
                AND TPD.StartDate < @dtETCDate AND TPD.EndDate >= @dtETCDate
          ) AS TL

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
      -- Calculate Task ETC.

      INSERT @tabETC (
        ETCCost,
        ETCBill,
        ETCDirCost,
        ETCDirBill,
        ETCReimCost,
        ETCReimBill
      )
        SELECT  
          ROUND(@decFutureCost + ISNULL(SUM(PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)),0), @intAmtCostDecimals) AS ETCCost, 
          ROUND(@decFutureBill + ISNULL(SUM(PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)),0), @intAmtBillDecimals) AS ETCBill, 
          ROUND(@decFutureDirCost + ISNULL(SUM(PeriodDirCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)),0), @intAmtCostDecimals) AS ETCDirCost, 
          ROUND(@decFutureDirBill + ISNULL(SUM(PeriodDirBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)),0), @intAmtBillDecimals) AS ETCDirBill, 
          ROUND(@decFutureReimCost + ISNULL(SUM(PeriodReimCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)),0), @intAmtCostDecimals) AS ETCReimCost, 
          ROUND(@decFutureReimBill + ISNULL(SUM(PeriodReimBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)),0), @intAmtBillDecimals) AS ETCReimBill 
          FROM @tabTPD AS TPD

    END /* End Else (@strVorN != 'N') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
