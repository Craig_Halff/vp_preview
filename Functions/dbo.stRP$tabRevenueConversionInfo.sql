SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabRevenueConversionInfo]
															  (@strPlanID varchar(32),
															   @strRevenueSetting varchar(1))

															   RETURNS @tabResults TABLE
																(RevenueFromFee varchar(1),
																 WhatCurrencyUsed varchar(1),
																 HasNegativeFee bit,
																 HasNegativePlannedRevenue bit
																)

															BEGIN

															  DECLARE @strProjectCurrencyCode nvarchar(3)
															  DECLARE @strBillingCurrencyCode nvarchar(3)
															  DECLARE @siPlannedRevenueMethod smallint
															  DECLARE @strReimbMethod char
															  DECLARE @strFeeByRowAndPeriod varchar(1)

															  DECLARE @strRevenueFromFee varchar(1)
															  DECLARE @strWhatCurrencyUsed varchar(1) = 'P'
															  DECLARE @bHasNegativeFee bit = 0
															  DECLARE @bHasNegativePlannedRevenue bit = 0

															  DECLARE @bHasNegativeCostFee bit = 0
															  DECLARE @bHasNegativeBillingFee bit = 0

															  DECLARE @strCompany nvarchar(14)
															  DECLARE @strMultiCurrencyEnabled varchar(1)
															  DECLARE @strReportAtBillingInBillingCurr varchar(1)

															  DECLARE @strOldPlanID varchar(32)

															  DECLARE @tabRevenue TABLE(
																PlanID varchar(32) COLLATE database_default,
																TimePhaseID varchar(32) COLLATE database_default,
																WBS1 nvarchar(30) COLLATE database_default,
																WBS2 nvarchar(30) COLLATE database_default,
																WBS3 nvarchar(30) COLLATE database_default,
																StartDate datetime,
																EndDate datetime,
																PeriodCost decimal,
																PeriodBill decimal,
																StoredRevenueCurrency varchar(1),
																RevenueFromFee varchar(1)
																UNIQUE(PlanID, TimePhaseID, WBS1, WBS2, WBS3 )
															  )
															  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
																SET @strOldPlanID = @strPlanID

																SELECT @strCompany = RP.Company,
																@strProjectCurrencyCode = PR.ProjectCurrencyCode,
																@strBillingCurrencyCode = PR.BillingCurrencyCode,
																@strReimbMethod = RP.ReimbMethod,
																@siPlannedRevenueMethod = RP.LabMultType
																FROM RPPlan RP INNER JOIN PR ON RP.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
																WHERE RP.PlanID = @strPlanID

																SELECT @strFeeByRowAndPeriod = FeesByPeriodFlg FROM CFGResourcePlanning WHERE Company = @strCompany

																 SELECT
																@strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
																@strMultiCurrencyEnabled = MultiCurrencyEnabled
																FROM FW_CFGSystem

															-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


															-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
																--------Determine where the revenue comes from
															  IF @strMultiCurrencyEnabled = 'Y' AND @strReportAtBillingInBillingCurr = 'Y' AND @strProjectCurrencyCode <> @strBillingCurrencyCode
															  BEGIN
																INSERT @tabRevenue (
																	PlanID,
																	TimePhaseID,
																	WBS1,
																	WBS2,
																	WBS3,
																	StartDate,
																	EndDate,
																	PeriodCost,
																	PeriodBill,
																	StoredRevenueCurrency,
																	RevenueFromFee
																)
																SELECT  PlanID,
																		TimePhaseID,
																		WBS1,
																		WBS2,
																		WBS3,
																		StartDate,
																		EndDate,
																		PeriodCost,
																		PeriodBill,
																		StoredRevenueCurrency,
																		RevenueFromFee
																		FROM dbo.stRP$tabConvertedRevenue (@strOldPlanID, @strRevenueSetting, @strFeeByRowAndPeriod,'Y', @siPlannedRevenueMethod, @strReimbMethod)

															  END

															  ELSE --IF @strMultiCurrencyEnabled<> 'Y' -->Single Currency
															  BEGIN
															  INSERT @tabRevenue (
																	PlanID,
																	TimePhaseID,
																	WBS1,
																	WBS2,
																	WBS3,
																	StartDate,
																	EndDate,
																	PeriodCost,
																	PeriodBill,
																	StoredRevenueCurrency,
																	RevenueFromFee
																)
																SELECT  PlanID,
																		TimePhaseID,
																		WBS1,
																		WBS2,
																		WBS3,
																		StartDate,
																		EndDate,
																		PeriodCost,
																		PeriodBill,
																		StoredRevenueCurrency,
																		RevenueFromFee
																		FROM dbo.stRP$tabConvertedRevenue (@strOldPlanID, @strRevenueSetting, @strFeeByRowAndPeriod,'N', @siPlannedRevenueMethod, @strReimbMethod)
															  END


															  --Check if there are revenue conveted
															  IF(SELECT COUNT(*) FROM @tabRevenue WHERE PeriodBill > 0 OR PeriodCost > 0) > 0  
																BEGIN
																	SELECT TOP 1 @strRevenueFromFee = RevenueFromFee, 
																					@strWhatCurrencyUsed = StoredRevenueCurrency,
																					@bHasNegativeFee = 0,
																					@bHasNegativePlannedRevenue = 0
																					FROM @tabRevenue
																END
															  ELSE -- No revenue converted, maybe there negative fee, negative Planned revenue, or no data to convert
																BEGIN
																	 SELECT @bHasNegativeCostFee = HasNegativeCostFee, @bHasNegativeBillingFee = HasNegativeBillingFee, @bHasNegativePlannedRevenue = HasNegativePlannedRevenue
																	 FROM dbo.stRP$tabRevenueNegativeValue(@strOldPlanID, @strRevenueSetting)

																	 SET @strRevenueFromFee = 'O'

																	 IF @strFeeByRowAndPeriod = 'Y' 
																	 BEGIN
																		--could be there is negative fee, negative planned revenue or no data to convert
																		IF @bHasNegativeCostFee = 1 OR @bHasNegativeBillingFee = 1-- No data converted because of negative fee
																	   BEGIN
																			SET @bHasNegativeFee = 1
																			SET @bHasNegativePlannedRevenue = 0
																		END
																		ELSE IF @bHasNegativePlannedRevenue = 1-- No data converted because of negative planned revenue
																		BEGIN
																			SET @bHasNegativeFee = 0
																			SET @bHasNegativePlannedRevenue = 1
																		END
																		ELSE -- No data converted
																		BEGIN
																			SET @bHasNegativeFee = 0
																			SET @bHasNegativePlannedRevenue = 0
																		END
																	END
																	ELSE -- --could be there is negative planned revenue or no data to convert
																	BEGIN
																		SET @bHasNegativeFee = 0
																	END
																END

															-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

																INSERT @tabResults(
																	RevenueFromFee,
																	WhatCurrencyUsed,
																	HasNegativeFee,
																	HasNegativePlannedRevenue
																)
																SELECT
																	@strRevenueFromFee,
																	@strWhatCurrencyUsed,
																	@bHasNegativeFee,
																	@bHasNegativePlannedRevenue
															RETURN
															END
GO
