SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DLTK$NumTimesheetDays]
  (@dtStartDate AS datetime, @dtEndDate AS datetime, @strCompany AS Nvarchar(14))
  RETURNS float
BEGIN

  DECLARE @flNumTimesheetDays float
  
  -- If the total number of days between Start & End Dates is not divisible by seven then we need to figure out 
  -- how many of the extra days are non-working day.
  -- The number of extra days is the remainder (mod) of Total Days divides by seven.
  
  -- I used MIN function on iNonWorkPerWeek and strNonWork so that I don't have to use GROUP BY.
  -- GROUP BY does not return any row when CFGNonWorkingDay is empty.

  SELECT @flNumTimesheetDays = iTotalDays - (iNonWorkPerWeek * (iTotalDays / 7)) - 
         LEN(REPLACE(SUBSTRING(strNonWork + strNonWork, iEndDay + 8 - (iTotalDays % 7), (iTotalDays % 7)), 'N', ''))
  FROM
    (SELECT 
       DATEDIFF(day, @dtStartDate, @dtEndDate) + 1 AS iTotalDays,
       DATEPART(dw, @dtEndDate) AS iEndDay,
       CASE WHEN COUNT(*) = 0 THEN 2 
            ELSE MIN(LEN(REPLACE(SundayInd + MondayInd  + TuesdayInd + WednesdayInd + ThursdayInd + FridayInd + SaturdayInd, 'N', ''))) END AS iNonWorkPerWeek, 
       CASE WHEN COUNT(*) = 0 THEN 'YNNNNNY'
            ELSE MIN((SundayInd + MondayInd  + TuesdayInd + WednesdayInd + ThursdayInd + FridayInd + SaturdayInd)) END AS strNonWork
       FROM CFGNonWorkingDay WHERE Company = @strCompany) AS NWD

  RETURN @flNumTimesheetDays

END -- fn_DLTK$NumTimesheetDays
GO
