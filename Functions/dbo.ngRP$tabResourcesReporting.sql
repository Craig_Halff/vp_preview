SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ngRP$tabResourcesReporting](
  @strRowID nvarchar(255),
  @strScopeStartDate varchar(8) /* Date must be in format: 'yyyymmdd' regardless of UI Culture */,
  @strScopeEndDate varchar(8) /* Date must be in format: 'yyyymmdd' regardless of UI Culture */,
  @strMode varchar(1) /* S = Self, C = Children */,
  @strResourceSummary varchar(1) /* Y/N */
)
  RETURNS @tabResourcesReporting TABLE (
    RowID nvarchar(255) COLLATE database_default,
    ParentRowID nvarchar(255) COLLATE database_default,
    RowLevel int,
    HasChildren bit,
    SelectFlag varchar(1) COLLATE database_default,
    Name nvarchar(255) COLLATE database_default,
    BillableFlg varchar(1) COLLATE database_default,
    MatchPct decimal(19,4),
    StartDate datetime,
    EndDate datetime,
    AvailableHrs decimal(19,4),
    PlannedHrs decimal(19,4),
    BaselineHrs decimal(19,4),
    SelectedPlannedHrs decimal(19,4),
    SelectedBillableHrs decimal(19,4),
    ScheduledPct decimal(19,4),
    UtilizationScheduleFlg varchar(1),
    TargetUtilization decimal(19,4),
    UtilizationPct decimal(19,4),
    SelectedETCHrs decimal(19,4),
    ETCHrs decimal(19,4),
    JTDHrs decimal(19,4),
    EACHrs decimal(19,4),
    HasPhoto bit,
	PhotoModDate datetime,
    BeforeETCHrs decimal(19,4),
    AfterETCHrs decimal(19,4),
    UtilRatioFlg varchar(1) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ResourceID nvarchar(20) COLLATE database_default,
    GenericResourceID nvarchar(20) COLLATE database_default,
    ProjectWBS varchar(max) COLLATE database_default,
    ProjectWBSName varchar(max) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    Company nvarchar(14),
    TaskName nvarchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    TopWBS1 nvarchar(30) COLLATE database_default,
    TopName nvarchar(255) COLLATE database_default,
    TopKonaSpace int,
    PMFullName nvarchar(255) COLLATE database_default,
    ClientName nvarchar(100) COLLATE database_default,
    HoursPerDay decimal(19,4),
    SumBeforeWD decimal(19,4),
    SumAfterWD decimal(19,4),
    ContractStartDate datetime,
    ContractEndDate datetime,
    MinASGDate datetime,
    MaxASGDate datetime,
    MinTPDDate datetime,
    MaxTPDDate datetime,
    LaborCat smallint,
    Org nvarchar(30) COLLATE database_default,
    OrgName varchar(100),
    OrgLevel1 nvarchar(30) COLLATE database_default,
    OrgLevel2 nvarchar(30) COLLATE database_default,
    OrgLevel3 nvarchar(30) COLLATE database_default,
    OrgLevel4 nvarchar(30) COLLATE database_default,
    OrgLevel5 nvarchar(30) COLLATE database_default,
    Title varchar(50),
    EMail varchar(50),
    HireDate datetime,
    TerminationDate datetime,
    [Status] varchar(30),
    R_Status varchar(1) COLLATE database_default,
    SupervisorName varchar(255),
    YearsOtherFirms int,
    PriorYearsFirm int,
    YearsWithFirm int,
    UtilizationRatio decimal(19,4),
    HardBooked varchar(1) COLLATE database_default,
    Location varchar(50) COLLATE database_default,
    OpportunityId varchar(32)
  )

BEGIN
/*
  When this UDF is executed, it will return different kinds of data depending on the input parameters @strRowID and @intOutlineLevel.

  1. To return data for a Resource row: @strMode = "S", @strRowID contains only ID of a Resource and no TaskID.

  2. To return data for the top most WBS rows: @strMode = "C", @strRowID contains only ID of a Resource and no TaskID.
  3. To return data for WBS Level 1 rows: @strMode = "C", @strRowID contains ID of a Resource and TaskID of a WBS Level 0 row.
  4. To return data for WBS Level 2 rows: @strMode = "C", @strRowID contains ID of a Resource and TaskID of a WBS Level 1 row.

  5. To return data for the top most WBS rows: @strMode = "S", @strRowID contains ID of a Resource and TaskID of a WBS Level 0 row.
  6. To return data for WBS Level 1 row: @strMode = "S", @strRowID contains ID of a Resource and TaskID of a WBS Level 1 row.
  7. To return data for WBS Level 2 row: @strMode = "S", @strRowID contains ID of a Resource and TaskID of a WBS Level 2 row.

  For WBS Level {1 .. n}, @strRowID will dictate which WBS or chidren rows being return using the TaskID embedded in @strRowID.
  @intOutlineLevel will be calculated from that TaskID. 

*/

  DECLARE @strCompany nvarchar(14)
  DECLARE @strResourceID nvarchar(20)
  DECLARE @strResourceName nvarchar(255)
  DECLARE @strGenericResourceID nvarchar(20)
  DECLARE @strSupervisorID nvarchar(20)
  DECLARE @strSupervisorName nvarchar(255)
  DECLARE @strTaskID varchar(32)
  DECLARE @strResourceType varchar(1)
  DECLARE @strIDPrefix nvarchar(255)
  DECLARE @bitHasPhoto bit
  DECLARE @dtPhotoModDate datetime
  DECLARE @strUtilRatioFlg varchar(1)
  DECLARE @strUseBookingForEmpHours varchar(1)
  DECLARE @strUseBookingForGenHours varchar(1)
  DECLARE @strIncludeSoftBookedHours varchar(1)

  DECLARE @strTopName nvarchar(255)

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
  DECLARE @dtScopeStartDate datetime
  DECLARE @dtScopeEndDate datetime
  DECLARE @dtScopeBeforeDate datetime
  DECLARE @dtScopeAfterDate datetime
 
  DECLARE @intOutlineLevel int
  DECLARE @intTaskCount int 

  DECLARE @siHrDecimals int /* CFGRMSettings.HrDecimals */
  DECLARE @siOverUtilOption smallint
  DECLARE @siEmpUtilPercent smallint
  DECLARE @siEmpUtilPlusPercent smallint
  DECLARE @siEmpUtilMorePercent smallint
  DECLARE @siUnderUtilPercent smallint
  DECLARE @siOverSchPercent smallint
  DECLARE @siUnderSchPercent smallint

  DECLARE @tiPercentOfUtilization tinyint = 1
  DECLARE @tiRatioPlusPercentage tinyint = 2
  DECLARE @tiGreaterThanPercentage tinyint = 3

  DECLARE @decAvailableHrs decimal(19,4)
  DECLARE @decHoursPerDay decimal(19,4)
  DECLARE @decUtilizationRatio decimal(19,4)

  DECLARE @decScheduledPct decimal(19,4)
  DECLARE @decUtilizationPct decimal(19,4)
  DECLARE @decURUnderLimit decimal(19,4)
  DECLARE @decUROverLimit decimal(19,4)
  
  -- Declare Temp tables.

  DECLARE @tabAssignment TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    BeforeWD decimal(19,4),
    AfterWD decimal(19,4),
    OutlineNumber varchar(255) COLLATE database_default,
  Hardbooked varchar(1),
  HardBookedFlg int,
    UNIQUE(PlanID, TaskID, AssignmentID)
  )

  DECLARE @tabTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    Name nvarchar(255) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
  WBS2 nvarchar(30) COLLATE database_default,
  WBS3 nvarchar(30) COLLATE database_default,
  LaborCode nvarchar(14) COLLATE database_default,
  Company nvarchar(14),
    StartDate datetime,
    EndDate datetime,
    ChargeType varchar(1) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int,
    ChildrenCount int,
    TopWBS1 nvarchar(30) COLLATE database_default,
    TopName nvarchar(255) COLLATE database_default,
    TopKonaSpace int,
    PMFullName nvarchar(255) COLLATE database_default,
    ClientName nvarchar(100) COLLATE database_default,
    ContractStartDate datetime,
    ContractEndDate datetime,
  UtilizationScheduleFlg varchar(1),
    ASG_BeforeWD decimal(19,4),
    ASG_AfterWD decimal(19,4),
    ASG_StartDate datetime,
    ASG_EndDate datetime
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabLabTPD TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChargeType varchar(1) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PlannedHrs decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, OutlineNumber, StartDate, EndDate)
  )

  DECLARE @tabLabBaselineTPD TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PlannedHrs decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, OutlineNumber, StartDate, EndDate)
  )

  DECLARE @tabSelectedTPD TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChargeType varchar(1) COLLATE database_default,
    PlannedHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabBillableTPD TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    PlannedHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabETCTPD TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PeriodHrs decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, OutlineNumber, StartDate, EndDate)
  )

  DECLARE @tabTotalETC TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ETCHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabBeforeETC TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ETCHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabAfterETC TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ETCHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabSelectedETC TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ETCHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabJTDLD TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    PeriodHrs decimal(19,4),
    JTDLabCost decimal(19,4),	
    JTDLabBill decimal(19,4)
    UNIQUE (RowID, WBS1, WBS2, WBS3)
  )

  DECLARE @tabLD TABLE (
    RowID  int IDENTITY(1,1),
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    PeriodHrs decimal(19,4),
    JTDLabCost decimal(19,4),	
    JTDLabBill decimal(19,4)
    PRIMARY KEY(RowID, PlanID, OutlineNumber)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Set Company to non-Multi-Company.
  
  SELECT @strCompany = dbo.GetActiveCompany()

  -- Get RM Settings.
  
  SELECT     
    @siOverUtilOption = OverUtilOption,
    @siEmpUtilPercent = EmpUtilPercent,
    @siEmpUtilPlusPercent = EmpUtilPlusPercent,
    @siEmpUtilMorePercent = EmpUtilMorePercent,
    @siUnderUtilPercent = UnderUtilPercent,
    @siOverSchPercent = OverSchPercent,
    @siUnderSchPercent = UnderSchPercent,
    @strUseBookingForEmpHours = UseBookingForEmpHours,
    @strUseBookingForGenHours = UseBookingForGenHours,

    @strIncludeSoftBookedHours = IncludeSoftBookedHours,
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE()))),
    @siHrDecimals = HrDecimals
    FROM CFGRMSettings

  -- Set Dates
  
  SELECT @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)
  SELECT @dtScopeStartDate = CONVERT(datetime, @strScopeStartDate)
  SELECT @dtScopeEndDate = CONVERT(datetime, @strScopeEndDate)
  SELECT @dtScopeBeforeDate = DATEADD(DAY, -1, @dtScopeStartDate)
  SELECT @dtScopeAfterDate = DATEADD(DAY, 1, @dtScopeEndDate)

  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. E~<RPAssignment.ResourceID>|<RPTask.TaskID>
  --   2. G~<RPAssignment.GenericResourceID>|<RPTask.TaskID>

  SET @strResourceType = SUBSTRING(@strRowID, 1, 1)
  SET @strIDPrefix = LEFT(@strRowID, CHARINDEX('|', @strRowID) - 1)

  SET @decAvailableHrs = 0
  SET @decHoursPerDay = 0

  IF (@strResourceType = 'E')
    BEGIN

      SET @strResourceID = REPLACE(@strIDPrefix, 'E~', '')
      SET @strGenericResourceID = NULL

      SELECT 
        @strResourceName = CONVERT(nvarchar(255), case when EM.PreferredName is Null then isnull(EM.firstName + ' ', N'') + EM.lastName else EM.PreferredName + ' ' + EM.lastname end + isnull(' ' + EM.Suffix, N'')),
        @bitHasPhoto = CASE WHEN EP.Photo IS NULL THEN 0 ELSE 1 END,
        @dtPhotoModDate = CASE WHEN EP.Photo IS NULL THEN NULL ELSE EP.ModDate END,
        @decUtilizationRatio = EM.UtilizationRatio,
        @decHoursPerDay = EM.HoursPerDay,
    @strSupervisorID = EM.Supervisor
        FROM EM 
          LEFT JOIN EMPhoto AS EP ON EM.Employee = EP.Employee
        WHERE EM.Employee = @strResourceID

      SELECT @decAvailableHrs = ROUND(@decHoursPerDay * dbo.DLTK$NumWorkingDays(@dtScopeStartDate, @dtScopeEndDate, @strCompany), @siHrDecimals)

      SET @decURUnderLimit = ROUND((@decUtilizationRatio * CONVERT(DECIMAL, @siUnderUtilPercent)), 2)

      SET @decUROverLimit = 
        CASE @siOverUtilOption
          WHEN @tiPercentOfUtilization
            THEN ROUND((@decUtilizationRatio * CONVERT(DECIMAL, @siEmpUtilPercent)), 2)
          WHEN @tiRatioPlusPercentage
            THEN ROUND((@decUtilizationRatio + CONVERT(DECIMAL, @siEmpUtilPlusPercent)), 2)
          WHEN @tiGreaterThanPercentage
            THEN ROUND(CONVERT(DECIMAL, @siEmpUtilMorePercent), 2)
        END

    END
  ELSE
    BEGIN
      SET @strGenericResourceID = REPLACE(@strIDPrefix, 'G~', '')
      SET @strResourceID = NULL
      SET @bitHasPhoto = 0
	  SET @dtJTDDate = NULL
      SET @decUtilizationRatio = 0
      SELECT @strResourceName = Name,
      @strSupervisorID = GR.Supervisor
    FROM GR WHERE GR.Code = @strGenericResourceID
    END

  SELECT @strSupervisorName = CONVERT(nvarchar(255), ISNULL(FirstName, '') + ISNULL(' ' + LastName, ''))
  FROM EM  WHERE EM.Employee = @strSupervisorID

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

  IF (DATALENGTH(@strTaskID) = 0)
    BEGIN
      SELECT 
        @intOutlineLevel = 
          CASE 
            WHEN @strMode = 'S' THEN 0
            WHEN @strMode = 'C' THEN 0
          END 
    END
  ELSE
    BEGIN
      SELECT 
        @intOutlineLevel = 
          CASE 
            WHEN @strMode = 'S' THEN OutlineLevel
            WHEN @strMode = 'C' THEN OutlineLevel + 1
          END 
        FROM RPTask 
        WHERE TaskID = @strTaskID
    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabAssignment(
    PlanID,
    TaskID,
    AssignmentID,
    StartDate,
    EndDate,
    BeforeWD,
    AfterWD,
    OutlineNumber,
  Hardbooked,
  HardBookedFlg
  )
    SELECT DISTINCT
      A.PlanID,
      A.TaskID,
      A.AssignmentID,
      A.StartDate,
      A.EndDate,
      CASE
        WHEN ((A.StartDate >= @dtScopeStartDate) OR (@dtScopeBeforeDate <= @dtETCDate))
        THEN 0
        ELSE
          dbo.DLTK$NumWorkingDays(
            CASE
              WHEN (A.StartDate BETWEEN @dtETCDate AND @dtScopeBeforeDate)
              THEN A.StartDate
              ELSE @dtETCDate
            END, 
            CASE
              WHEN (A.EndDate BETWEEN @dtETCDate AND @dtScopeBeforeDate)
              THEN A.EndDate
              ELSE @dtScopeBeforeDate
            END, 
            @strCompany
          )
      END AS BeforeWD,
      CASE
        WHEN (A.EndDate <= @dtScopeEndDate)
        THEN 0
        ELSE
          dbo.DLTK$NumWorkingDays(
            CASE
              WHEN (A.StartDate >= @dtScopeAfterDate)
              THEN A.StartDate
              ELSE @dtScopeAfterDate
            END, 
            A.EndDate,
            @strCompany
          )
      END AS AfterWD,
      AT.OutlineNumber AS OutlineNumber,
    A.HardBooked AS HardBooked,
    CASE HardBooked
    WHEN 'Y' THEN 1
    WHEN 'N' THEN -1
    END AS HardBookedFlg
      FROM RPAssignment AS A
        INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
      WHERE
        ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') AND 
        ISNULL(A.GenericResourceID, '|') = ISNULL( @strGenericResourceID, '|') AND
        A.EndDate >= @dtScopeStartDate AND
        dbo.DLTK$NumWorkingDays(@dtScopeStartDate, A.EndDate, @strCompany) > 0

    SELECT @intTaskCount = COUNT(DISTINCT TaskID) FROM @tabAssignment

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  IF @strMode <> 'P'
    BEGIN
      INSERT @tabTask(
        PlanID,
        TaskID,
        Name,
        WBS1,
    WBS2,
    WBS3,
	LaborCode,
    Company,
        StartDate,
        EndDate,
        ChargeType,
        OutlineNumber,
        OutlineLevel,
        ChildrenCount,
        TopWBS1,
        TopName,
        TopKonaSpace,
        PMFullName,
        ClientName,
    UtilizationScheduleFlg,
        ASG_BeforeWD,
        ASG_AfterWD,
        ASG_StartDate,
        ASG_EndDate
      )
        SELECT DISTINCT
          A.PlanID AS PlanID,
          CT.TaskID AS TaskID,
          CT.Name AS Name,
          CT.WBS1 AS WBS1,
      CT.WBS2 AS WBS2,
      CT.WBS3 AS WBS3,
	  CT.LaborCode as LaborCode,
      P.Company AS Company,
          CT.StartDate AS StartDate,
          CT.EndDate AS EndDate,
          CT.ChargeType AS ChargeType,
          CT.OutlineNumber AS OutlineNumber,
          CT.OutlineLevel AS OutlineLevel,
          CT.ChildrenCount AS ChildrenCount,
          PT.WBS1 AS TopWBS1,
          PT.Name AS TopName,
          ISNULL(PAD.KonaSpace, 0) AS TopKonaSpace,
          CONVERT(nvarchar(255), ISNULL(PM.FirstName, '') + ISNULL(' ' + PM.LastName, '')) AS PMFullName,
          ISNULL(CL.Name, '') AS ClientName,
      PR.UtilizationScheduleFlg AS UtilizationScheduleFlg,
          SUM(A.BeforeWD) AS ASG_BeforeWD,
          SUM(A.AfterWD) AS ASG_AfterWD,
          MIN(A.StartDate) AS ASG_StartDate,
          MAX(A.EndDate) AS ASG_EndDate
          FROM @tabAssignment AS A
            INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID
      INNER JOIN RPTask AS CT ON A.PlanID = CT.PlanID AND CT.TaskID = A.TaskID
      INNER JOIN RPPlan AS P ON P.PlanID = PT.PlanID
            LEFT JOIN EM AS PM ON PT.ProjMgr = PM.Employee
            LEFT JOIN CL ON PT.ClientID = CL.ClientID
            LEFT JOIN PRAdditionalData AS PAD ON PT.WBS1 = PAD.WBS1 AND ISNULL(PT.WBS2, ' ') = PAD.WBS2 AND ISNULL(PT.WBS3, ' ') = PAD.WBS3
      LEFT JOIN PR  ON CT.WBS1 = PR.WBS1 AND ISNULL(CT.WBS2, ' ') = PR.WBS2 AND ISNULL(CT.WBS3, ' ') = PR.WBS3
          WHERE CT.OutlineNumber LIKE PT.OutlineNumber + '%' AND PT.OutlineLevel = 0
          GROUP BY A.PlanID, CT.TaskID, CT.Name, CT.WBS1,CT.WBS2,CT.WBS3, CT.LaborCode, P.Company, CT.StartDate, CT.EndDate, CT.ChargeType, CT.OutlineNumber, CT.OutlineLevel, CT.ChildrenCount,
            PT.WBS1, PT.Name, PAD.KonaSpace, PM.FirstName, PM.LastName, CL.Name, PR.UtilizationScheduleFlg
    END
  ELSE
    BEGIN
      INSERT @tabTask(
        PlanID,
        TaskID,
        Name,
        WBS1,
    WBS2,
    WBS3,
	LaborCode,
    Company,
        StartDate,
        EndDate,
        ChargeType,
        OutlineNumber,
        OutlineLevel,
        ChildrenCount,
        TopWBS1,
        TopName,
        TopKonaSpace,
        PMFullName,
        ClientName,
    UtilizationScheduleFlg,
        ASG_BeforeWD,
        ASG_AfterWD,
        ASG_StartDate,
        ASG_EndDate
      )
        SELECT DISTINCT
          A.PlanID AS PlanID,
          CT.TaskID AS TaskID,
          CT.Name AS Name,
          CT.WBS1 AS WBS1,
      CT.WBS2 AS WBS2,
      CT.WBS3 AS WBS3,
	  CT.LaborCode AS LaborCode,
      P.Company AS Company,
          CT.StartDate AS StartDate,
          CT.EndDate AS EndDate,
          CT.ChargeType AS ChargeType,
          CT.OutlineNumber AS OutlineNumber,
          CT.OutlineLevel AS OutlineLevel,
          CT.ChildrenCount AS ChildrenCount,
          CT.WBS1 AS TopWBS1,
          CT.Name AS TopName,
          ISNULL(PAD.KonaSpace, 0) AS TopKonaSpace,
          CONVERT(nvarchar(255), ISNULL(PM.FirstName, '') + ISNULL(' ' + PM.LastName, '')) AS PMFullName,
          ISNULL(CL.Name, '') AS ClientName,
      PR.UtilizationScheduleFlg AS UtilizationScheduleFlg,
          SUM(A.BeforeWD) AS ASG_BeforeWD,
          SUM(A.AfterWD) AS ASG_AfterWD,
          MIN(A.StartDate) AS ASG_StartDate,
          MAX(A.EndDate) AS ASG_EndDate
          FROM @tabAssignment AS A
      INNER JOIN RPTask AS CT ON A.PlanID = CT.PlanID
      INNER JOIN RPPlan AS P ON P.PlanID = CT.PlanID
            LEFT JOIN EM AS PM ON CT.ProjMgr = PM.Employee
            LEFT JOIN CL ON CT.ClientID = CL.ClientID
            LEFT JOIN PRAdditionalData AS PAD ON CT.WBS1 = PAD.WBS1 AND ISNULL(CT.WBS2, ' ') = PAD.WBS2 AND ISNULL(CT.WBS3, ' ') = PAD.WBS3
            LEFT JOIN PR  ON CT.WBS1 = PR.WBS1 AND ISNULL(CT.WBS2, ' ') = PR.WBS2 AND ISNULL(CT.WBS3, ' ') = PR.WBS3
      WHERE ISNULL(CT.ParentOutlineNumber,'') = ''
          GROUP BY A.PlanID, CT.TaskID, CT.Name, CT.WBS1, CT.WBS2, CT.WBS3,CT.LaborCode, P.Company, CT.StartDate, CT.EndDate, CT.ChargeType, CT.OutlineNumber, CT.OutlineLevel, CT.ChildrenCount, 
            CT.WBS1, CT.Name, PAD.KonaSpace, PM.FirstName, PM.LastName, CL.Name, PR.UtilizationScheduleFlg
    END
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collecting Labor Time-Phased Data to be used in subsequent calculations.
  -- The TPD records come from only the Assignments included in @tabAssignment.

  INSERT @tabLabTPD(
    TimePhaseID,
    PlanID,
    TaskID,
    AssignmentID,
    OutlineNumber,
    ChargeType,
    StartDate,
    EndDate,
    PlannedHrs
  )
    SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID,
      TPD.TaskID AS TaskID,
      TPD.AssignmentID AS AssignmentID,
      T.OutlineNumber AS OutlineNumber,
      T.ChargeType AS ChargeType,
      TPD.StartDate AS StartDate,
      TPD.EndDate AS EndDate,
      TPD.PeriodHrs AS PlannedHrs
      FROM @tabAssignment AS A
        INNER JOIN RPTask AS T ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
        INNER JOIN RPPlannedLabor AS TPD ON
          A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID AND
          TPD.PeriodHrs <> 0 AND TPD.AssignmentID IS NOT NULL 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collecting Baseline Labor Time-Phased Data to be used in subsequent calculations.
  -- The TPD records come from only the Assignments included in @tabAssignment.

  INSERT @tabLabBaselineTPD(
    TimePhaseID,
    PlanID,
    TaskID,
    AssignmentID,
    OutlineNumber,
    StartDate,
    EndDate,
    PlannedHrs
  )
    SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID,
      TPD.TaskID AS TaskID,
      TPD.AssignmentID AS AssignmentID,
      T.OutlineNumber AS OutlineNumber,
      TPD.StartDate AS StartDate,
      TPD.EndDate AS EndDate,
      TPD.PeriodHrs AS PlannedHrs
      FROM @tabAssignment AS A
        INNER JOIN RPTask AS T ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
        INNER JOIN RPBaselineLabor AS TPD ON
          A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID AND
          TPD.PeriodHrs <> 0 AND TPD.AssignmentID IS NOT NULL 
   
--+++++

  -- Calculate Selected TPD (i.e. Planned Hrs in forecast range) 
  -- GROUP BY PlanID, OutlineNumber, ChargeType

  INSERT @tabSelectedTPD(
    PlanID,
    OutlineNumber,
    ChargeType,
    PlannedHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
      TPD.ChargeType,
      ROUND(ISNULL(SUM(
        CASE
          WHEN TPD.StartDate >= @dtScopeStartDate
          THEN 
            CASE
              WHEN TPD.EndDate <= @dtScopeEndDate
              THEN TPD.PlannedHrs
              ELSE TPD.PlannedHrs * dbo.DLTK$ProrateRatio(TPD.StartDate, @dtScopeEndDate, TPD.StartDate, TPD.EndDate, @strCompany)
            END
          ELSE
            CASE
              WHEN TPD.EndDate <= @dtScopeEndDate
              THEN TPD.PlannedHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)
              ELSE TPD.PlannedHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, @dtScopeEndDate, TPD.StartDate, TPD.EndDate, @strCompany)
            END
        END
      ), 0), @siHrDecimals) AS PlannedHrs
      FROM @tabLabTPD AS TPD
      WHERE TPD.StartDate <= @dtScopeEndDate AND TPD.EndDate >= @dtScopeStartDate
      GROUP BY PlanID, OutlineNumber, ChargeType

--+++++

  -- Calculate Billable TPD (i.e. Billable Hrs in forecast range) 
  -- GROUP BY PlanID, OutlineNumber

  INSERT @tabBillableTPD(
    PlanID,
    OutlineNumber,
    PlannedHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
      ISNULL(SUM(TPD.PlannedHrs), 0) AS PlannedHrs
      FROM @tabSelectedTPD AS TPD
      WHERE TPD.ChargeType = 'R'
      GROUP BY PlanID, OutlineNumber

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collecting ETC Time-Phased Data to be used in subsequent calculations.

  INSERT @tabETCTPD(
    TimePhaseID,
    PlanID,
    TaskID,
    AssignmentID,
    OutlineNumber,
    StartDate,
    EndDate,
    PeriodHrs
  )
    SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID,
      TPD.TaskID AS TaskID,
      TPD.AssignmentID AS AssignmentID,
      TPD.OutlineNumber AS OutlineNumber,
      CASE WHEN TPD.StartDate >= @dtETCDate THEN TPD.StartDate ELSE @dtETCDate END AS StartDate,
      TPD.EndDate AS EndDate,
      ROUND(ISNULL(
        CASE 
          WHEN TPD.StartDate >= @dtETCDate 
          THEN TPD.PlannedHrs
          ELSE TPD.PlannedHrs * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) 
        END 
      , 0), @siHrDecimals) AS PeriodHrs
      FROM @tabLabTPD AS TPD
      WHERE TPD.PlannedHrs <> 0 AND TPD.EndDate >= @dtETCDate 

--+++++

  -- Calculate Total ETC
  -- GROUP BY PlanID, OutlineNumber

  INSERT @tabTotalETC(
    PlanID,
    OutlineNumber,
    ETCHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
      ISNULL(SUM(TPD.PeriodHrs), 0) AS ETCHrs
      FROM @tabETCTPD AS TPD
      GROUP BY PlanID, OutlineNumber

--+++++

  -- Calculate Before ETC
  -- GROUP BY PlanID, OutlineNumber

  INSERT @tabBeforeETC(
    PlanID,
    OutlineNumber,
    ETCHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
      ROUND(ISNULL(SUM(
        CASE
          WHEN TPD.EndDate <= @dtScopeBeforeDate
          THEN TPD.PeriodHrs
          ELSE TPD.PeriodHrs * dbo.DLTK$ProrateRatio(TPD.StartDate, @dtScopeBeforeDate, TPD.StartDate, TPD.EndDate, @strCompany)
        END
      ), 0), @siHrDecimals) AS ETCHrs
      FROM @tabETCTPD AS TPD
      WHERE TPD.StartDate <= @dtScopeBeforeDate
      GROUP BY PlanID, OutlineNumber

--+++++

  -- Calculate After ETC
  -- GROUP BY PlanID, OutlineNumber

  INSERT @tabAfterETC(
    PlanID,
    OutlineNumber,
    ETCHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
      ROUND(ISNULL(SUM(
        CASE
          WHEN TPD.StartDate >= @dtScopeAfterDate
          THEN TPD.PeriodHrs
          ELSE TPD.PeriodHrs * dbo.DLTK$ProrateRatio(@dtScopeAfterDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)
        END
      ), 0), @siHrDecimals) AS ETCHrs
      FROM @tabETCTPD AS TPD
      WHERE TPD.EndDate >= @dtScopeAfterDate
      GROUP BY PlanID, OutlineNumber

--+++++

  -- Calculate Selected ETC
  -- GROUP BY PlanID, OutlineNumber

  INSERT @tabSelectedETC(
    PlanID,
    OutlineNumber,
    ETCHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
      ROUND(ISNULL(SUM(
        CASE
          WHEN TPD.StartDate >= @dtScopeStartDate
          THEN 
            CASE
              WHEN TPD.EndDate <= @dtScopeEndDate
              THEN TPD.PeriodHrs
              ELSE TPD.PeriodHrs * dbo.DLTK$ProrateRatio(TPD.StartDate, @dtScopeEndDate, TPD.StartDate, TPD.EndDate, @strCompany)
            END
          ELSE
            CASE
              WHEN TPD.EndDate <= @dtScopeEndDate
              THEN TPD.PeriodHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)
              ELSE TPD.PeriodHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, @dtScopeEndDate, TPD.StartDate, TPD.EndDate, @strCompany)
            END
        END
      ), 0), @siHrDecimals) AS ETCHrs
      FROM @tabETCTPD AS TPD
      WHERE TPD.StartDate <= @dtScopeEndDate AND TPD.EndDate >= @dtScopeStartDate
      GROUP BY PlanID, OutlineNumber
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Calculate @decScheduledPct, @decUtilizationPct, @strUtilRatioFlg for the Resource row.

  IF (@intOutlineLevel < 0 AND @strResourceType = 'E')
    BEGIN

      SELECT @decScheduledPct =
        CASE
          WHEN @decAvailableHrs > 0
          THEN ROUND(((XSP.SelectedPlannedHrs / @decAvailableHrs) * 100), @siHrDecimals) 
          ELSE 0
        END
        FROM (
          SELECT 
            ROUND(ISNULL(SUM(PlannedHrs), 0.0000), @siHrDecimals) AS SelectedPlannedHrs
            FROM @tabSelectedTPD
        ) AS XSP

      SELECT @decUtilizationPct =
        CASE
          WHEN @decAvailableHrs > 0
          THEN ROUND(((XSB.SelectedBillableHrs / @decAvailableHrs) * 100), @siHrDecimals) 
          ELSE 0
        END
        FROM (
          SELECT 
            ROUND(ISNULL(SUM(PlannedHrs), 0.0000), @siHrDecimals) AS SelectedBillableHrs
            FROM @tabBillableTPD
        ) AS XSB

      SELECT @strUtilRatioFlg = 
        CASE
          WHEN @decUtilizationPct <= @decURUnderLimit THEN 'U'
          WHEN @decUtilizationPct > @decUROverLimit THEN 'O'
          ELSE 'P'
        END

    END
  ELSE
    BEGIN
      SELECT @decScheduledPct = 0
      SELECT @decUtilizationPct = 0
      SELECT @strUtilRatioFlg = ''
    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save Labor JTD & Unposted Labor JTD for this Resource into temp table.
  -- The JTD data are posted at the leaf node in the WBS Tree. 
  -- Therefore, we need to calculate JTD for the summary levels.

      INSERT @tabJTDLD
    (WBS1,
     WBS2,
     WBS3,	  
     PeriodHrs,
     JTDLabCost,
     JTDLabBill)      
    SELECT
      WBS1,WBS2,WBS3,  
      SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,
      SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS JTDLabCost,
      SUM(LD.BillExt) AS JTDLabBill
    FROM LD
    WHERE LD.TransDate <= @dtJTDDate AND LD.Employee = @strResourceID
    GROUP BY WBS1,WBS2,WBS3 

    UNION ALL
     SELECT
      WBS1,WBS2,WBS3, 
      SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,
      SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS JTDLabCost,
      SUM(TD.BillExt) AS JTDLabBill
    FROM tkDetail AS TD 
    INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate AND TD.EmployeeCompany = TM.EmployeeCompany)
    WHERE  TD.Employee = @strResourceID 
         AND TD.TransDate <= @dtJTDDate AND TD.Employee = @strResourceID
    GROUP BY WBS1,WBS2,WBS3     
  
    UNION ALL
     SELECT
      WBS1,WBS2,WBS3, 
      SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,
      SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS JTDLabCost,
      SUM(TD.BillExt) AS JTDLabBill
    FROM tsDetail AS TD
    INNER JOIN tsControl AS TC ON (TD.Batch = TC.Batch AND TC.Posted = 'N')  
     WHERE  TD.Employee = @strResourceID
     AND TD.TransDate <= @dtJTDDate AND TD.Employee = @strResourceID
    GROUP BY WBS1,WBS2,WBS3    


  INSERT @tabLD
    (PlanID,
     OutlineNumber,
     PeriodHrs,
     JTDLabCost,
     JTDLabBill)      
    SELECT
      T.PlanID AS PlanID,
      T.OutlineNumber AS OutlineNumber,
    SUM(LD.PeriodHrs) AS PeriodHrs,
    SUM(LD.JTDLabCost) AS JTDLabCost,
    SUM(LD.JTDLabBill) AS JTDLabBill
    FROM @tabAssignment AS A
      INNER JOIN RPTask AS T
        ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
      INNER JOIN @tabJTDLD LD
        ON  LD.WBS1 = T.WBS1
        AND LD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
        AND LD.WBS3 LIKE (ISNULL(T.WBS3 + '%', '%')) 
    GROUP BY T.PlanID, T.OutlineNumber
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Combine various calculations and return the end result.
  -- CASE WHEN
  --   @strResourceSummary = 'Y' group by resource and return empty task and assignment specific columns
  --   @strResourceSummary = 'N' group by resource, assignment
  --   @strMode = 'P' group by resource, top-project

  If @strMode = 'P'
    BEGIN
      INSERT @tabResourcesReporting(
        RowID,
        ParentRowID,
        RowLevel,
        HasChildren,
        SelectFlag,
        Name,
        BillableFlg,
        MatchPct,
        StartDate,
        EndDate,
        AvailableHrs,
        PlannedHrs,
        BaselineHrs,
        SelectedPlannedHrs,
        SelectedBillableHrs,
        ScheduledPct,
        UtilizationScheduleFlg,
        TargetUtilization,
        UtilizationPct,
        SelectedETCHrs,
        ETCHrs,
        JTDHrs,
        EACHrs,
		HasPhoto,
		PhotoModDate,
        BeforeETCHrs,
        AfterETCHrs,
        UtilRatioFlg,
        PlanID,
        TaskID,
        AssignmentID,
        ResourceID,
        GenericResourceID,
        ProjectWBS,
        ProjectWBSName,
        WBS1,
        WBS2,
        WBS3,
        Company,
        TaskName,
        OutlineNumber,
        TopWBS1,
        TopName,
        TopKonaSpace,
        PMFullName,
        ClientName,
        HoursPerDay,
        SumBeforeWD,
        SumAfterWD,
        MinASGDate,
        MaxASGDate,
        MinTPDDate,
        MaxTPDDate,
        LaborCat,
        Org,
        OrgName,
        OrgLevel1,
        OrgLevel2,
        OrgLevel3,
        OrgLevel4,
        OrgLevel5,
        Title,
        Email,
        HireDate,
        TerminationDate,
        [Status],
        R_Status,
        SupervisorName,
        YearsOtherFirms,
        PriorYearsFirm,
        YearsWithFirm,
        UtilizationRatio,
        HardBooked,
        Location
      )
        SELECT
          @strIDPrefix + '|' + T.TaskID AS RowID,
          '' AS ParentRowID,
          0 AS RowLevel,
          CASE WHEN @intTaskCount > 0 THEN CONVERT(bit, 1) ELSE CONVERT(bit, 0) END AS HasChildren,
          'N' AS SelectFlag,
          @strResourceName AS Name,
          '' AS BillableFlg,
          0 AS MatchPct,
          T.StartDate AS StartDate,
          T.EndDate AS EndDate,
          @decAvailableHrs AS AvailableHrs,
          XE.PlannedHrs AS PlannedHrs,
          XE.BaselineHrs AS BaselineHrs,
          XE.SelectedPlannedHrs AS SelectedPlannedHrs,
          XE.SelectedBillableHrs AS SelectedBillableHrs,
          @decScheduledPct AS ScheduledPct,
          T.UtilizationScheduleFlg AS UtilizationScheduleFlg,
          @decUtilizationRatio AS TargetUtilization,
          @decUtilizationPct AS UtilizationPct,
          XE.SelectedETCHrs AS SelectedETCHrs,
          XE.ETCHrs AS ETCHrs,
          XE.JTDHrs AS JTDHrs,
          XE.JTDHrs + XE.ETCHrs AS EACHrs,
          @bitHasPhoto AS HasPhoto,
		  @dtPhotoModDate as PhotoModDate,
          XE.BeforeETCHrs AS BeforeETCHrs,
          XE.AfterETCHrs AS AfterETCHrs,
          @strUtilRatioFlg AS UtilRatioFlg,
          T.PlanID AS PlanID,
          T.TaskID AS TaskID,
          NULL AS AssignmentID,
          @strResourceID AS ResourceID,
          @strGenericResourceID AS GenericResourceID,
          dbo.ngRPGetConCatTaskWBS1(T.PlanID,T.TaskID) AS ProjectWBS,
          dbo.ngRPGetConCatTaskName(T.PlanID,T.TaskID) AS ProjectWBSName,
          T.WBS1 AS WBS1,
          T.WBS2 AS WBS2,
          T.WBS3 AS WBS3,
          T.Company AS Company,
          T.Name AS TaskName,
          T.OutlineNumber	AS OutlineNumber,
          T.TopWBS1 AS TopWBS1,
          T.TopName AS TopName,
          0 AS TopKonaSpace,
          NULL AS PMFullName,
          NULL AS ClientName,
          @decHoursPerDay AS HoursPerDay,
          0 AS SumBeforeWD,
          0 AS SumAfterWD,
          T.ASG_StartDate AS AssignmentStartDate,
          T.ASG_EndDate AS AssignmentEndDate,
          NULL AS MinTPDDate,
          NULL AS MaxTPDDate,
          ISNULL(EM.BillingCategory,ISNULL(GR.Category,0)) AS LaborCat,
          CASE WHEN @strResourceID IS NULL THEN GRORG.Org ELSE EMORG.Org END AS Org,
          CASE WHEN @strResourceID IS NULL THEN GRORG.Name ELSE EMORG.Name END AS OrgName,
          ISNULL(OL1.OrgCode,'') AS OrgLevel1,
          ISNULL(OL2.OrgCode,'') AS OrgLevel2,
          ISNULL(OL3.OrgCode,'') AS OrgLevel3,
          ISNULL(OL4.OrgCode,'') AS OrgLevel4,
          ISNULL(OL5.OrgCode,'') AS OrgLevel5,
          EM.Title AS Title,
          EM.EMail AS EMail,
          EM.HireDate AS HireDate,
          EM.TerminationDate AS TerminationDate,
          CASE WHEN @strResourceID IS NULL THEN CFGGS.Label ELSE CFGES.Label END AS [Status],
          ISNULL(EM.[Status],ISNULL(GR.[Status],'')) AS R_Status,
          @strSupervisorName AS SupervisorName,
          EM.YearsOtherFirms AS YearsOtherFirms,
          EM.PriorYearsFirm AS PriorYearsFirm,
          DATEDIFF(YEAR,EM.HireDate,GETDATE()) AS YearsWithFirm,
          EM.UtilizationRatio AS UtilizationRatio,
          T.HardBooked AS HardBooked,
          CFGL.Description AS Location
          FROM (SELECT 
                  T.PlanID, 
                  T.TaskID, 
                  T.OutlineNumber, 
				  Max(CASE WHEN T.LaborCode IS NOT NULL THEN L.LaborCodeName ELSE T.NAME END) AS NAME,
                  T.WBS1, 
                  T.WBS2, 
                  T.WBS3, 
                  T.Company,
                  T.TopName, 
                  T.TopWBS1, 
                  T.StartDate, 
                  T.EndDate, 
                  T.UtilizationScheduleFlg AS UtilizationScheduleFlg,
                  MIN(ASG_StartDate) AS ASG_StartDate, 
                  MAX(ASG_EndDate) AS ASG_EndDate,
                  CASE SIGN(MIN(A.HardBookedFlg) + MAX(A.HardBookedFlg))
                    WHEN 0 THEN 'B'
                    WHEN 1 THEN 'Y'
                    WHEN -1 THEN 'N'
                  END AS HardBooked
                FROM @tabTask T
                  INNER JOIN @tabAssignment A ON T.PlanID = A.PlanID
				  CROSS APPLY dbo.stRP$tabLaborCode(T.PlanID,T.LaborCode) AS L
                GROUP BY T.PlanID, T.TaskID, T.OutlineNumber, T.Name, T.WBS1, T.WBS2, T.WBS3, T.company, T.TopName, T.TopWBS1, T.StartDate, T.EndDate,T.UtilizationScheduleFlg) AS T
          LEFT JOIN EM ON EM.Employee = @strResourceID
          LEFT JOIN GR ON GR.Code = @strGenericResourceID
          LEFT JOIN Organization AS EMORG ON EMORG.Org = EM.Org
          LEFT JOIN Organization AS GRORG ON GRORG.Org = GR.Org
          LEFT JOIN CFGEmployeeStatus AS CFGES ON CFGES.[Status] = EM.[Status]
          LEFT JOIN CFGEmployeeStatus AS CFGGS ON CFGGS.[Status] = GR.[Status]
          LEFT JOIN CFGEMLocation AS CFGL ON CFGL.Code = EM.Location
          OUTER APPLY ngRP$tabOrgLevel(ISNULL(EMORG.Org,GRORG.Org),1) AS OL1
          OUTER APPLY ngRP$tabOrgLevel(ISNULL(EMORG.Org,GRORG.Org),2) AS OL2
          OUTER APPLY ngRP$tabOrgLevel(ISNULL(EMORG.Org,GRORG.Org),3) AS OL3
          OUTER APPLY ngRP$tabOrgLevel(ISNULL(EMORG.Org,GRORG.Org),4) AS OL4
          OUTER APPLY ngRP$tabOrgLevel(ISNULL(EMORG.Org,GRORG.Org),5) AS OL5
          LEFT JOIN (
            SELECT
              PlanID AS PlanID,
              ROUND(ISNULL(SUM(TETCHrs), 0.0000), @siHrDecimals) AS ETCHrs,
              ROUND(ISNULL(SUM(BETCHrs), 0.0000), @siHrDecimals) AS BeforeETCHrs,
              ROUND(ISNULL(SUM(AETCHrs), 0.0000), @siHrDecimals) AS AfterETCHrs,
              ROUND(ISNULL(SUM(SETCHrs), 0.0000), @siHrDecimals) AS SelectedETCHrs,
              ROUND(ISNULL(SUM(PTPDHrs), 0.0000), @siHrDecimals) AS PlannedHrs,
              ROUND(ISNULL(SUM(STPDHrs), 0.0000), @siHrDecimals) AS SelectedPlannedHrs,
              ROUND(ISNULL(SUM(BTPDHrs), 0.0000), @siHrDecimals) AS SelectedBillableHrs,
              ROUND(ISNULL(SUM(JTDHrs), 0.0000), @siHrDecimals) AS JTDHrs,
              ROUND(ISNULL(SUM(BaselineHrs), 0.0000), @siHrDecimals) AS BaselineHrs
              FROM (
                SELECT
                  PlanID AS PlanID, 
                  ETCHrs AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                  0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                  FROM @tabTotalETC
                UNION ALL
                SELECT 
                  PlanID AS PlanID, 
                  0 AS TETCHrs, ETCHrs AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                  0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                  FROM @tabBeforeETC
                UNION ALL
                SELECT 
                  PlanID AS PlanID, 
                  0 AS TETCHrs, 0 AS BETCHrs, ETCHrs AS AETCHrs, 0 AS SETCHrs, 
                  0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                  FROM @tabAfterETC
                UNION ALL
                SELECT 
                  PlanID AS PlanID, 
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, ETCHrs AS SETCHrs, 
                  0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                  FROM @tabSelectedETC
                UNION ALL
                SELECT 
                  PlanID AS PlanID, 
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                  PlannedHrs AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                  FROM @tabLabTPD
                UNION ALL
                SELECT 
                  PlanID AS PlanID, 
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                  0 AS PTPDHrs, PlannedHrs AS STPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                  FROM @tabSelectedTPD
                UNION ALL
                SELECT 
                  PlanID AS PlanID, 
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                  0 AS PTPDHrs, 0 AS STPDHrs, PlannedHrs AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                  FROM @tabBillableTPD
                UNION ALL
                SELECT 
                  PlanID AS PlanID, 
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                  0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, PeriodHrs as JTDHrs, 0 AS BaselineHrs
                  FROM @tabLD
                UNION ALL
                SELECT 
                  PlanID AS PlanID, 
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                  0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, 0 as JTDHrs, PlannedHrs AS BaselineHrs
                  FROM @tabLabBaselineTPD
              ) AS ZE
            GROUP BY ZE.PlanID
          ) AS XE
        ON XE.PlanID = T.PlanID
    END
  ELSE
    IF @strResourceSummary = 'Y'
      BEGIN
        INSERT @tabResourcesReporting(
          RowID,
          ParentRowID,
          RowLevel,
          HasChildren,
          SelectFlag,
          Name,
          BillableFlg,
          MatchPct,
          StartDate,
          EndDate,
          AvailableHrs,
          PlannedHrs,
          BaselineHrs,
          SelectedPlannedHrs,
          SelectedBillableHrs,
          ScheduledPct,		
          UtilizationScheduleFlg,			
          TargetUtilization,
          UtilizationPct,
          SelectedETCHrs,
          ETCHrs,
          JTDHrs,
          EACHrs,
		  HasPhoto,
	      PhotoModDate,
          BeforeETCHrs,
          AfterETCHrs,
          UtilRatioFlg,
          PlanID,
          TaskID,
          AssignmentID,
          ResourceID,
          GenericResourceID,
          ProjectWBS,
          ProjectWBSName,
          WBS1,
          WBS2,
          WBS3,					
          Company,
          TaskName,
          OutlineNumber,
          TopWBS1,
          TopName,
          TopKonaSpace,
          PMFullName,
          ClientName,
          HoursPerDay,
          SumBeforeWD,
          SumAfterWD,
          MinASGDate,
          MaxASGDate,
          MinTPDDate,
          MaxTPDDate,
          LaborCat,
          Org,
          OrgName,
          OrgLevel1,
          OrgLevel2,
          OrgLevel3,
          OrgLevel4,
          OrgLevel5,
          Title,
          Email,
          HireDate,
          TerminationDate,
          [Status],
          R_Status,
          SupervisorName,
          YearsOtherFirms,
          PriorYearsFirm,
          YearsWithFirm,
          UtilizationRatio,
          HardBooked,
          Location
        )
          SELECT
            @strRowID AS RowID,
            '' AS ParentRowID,
            0 AS RowLevel,
            CASE WHEN @intTaskCount > 0 THEN CONVERT(bit, 1) ELSE CONVERT(bit, 0) END AS HasChildren,
            'N' AS SelectFlag,
            @strResourceName AS Name,
            '' AS BillableFlg,
            0 AS MatchPct,
            NULL AS StartDate,
            NULL AS EndDate,
            @decAvailableHrs AS AvailableHrs,
            XE.PlannedHrs AS PlannedHrs,
            XE.BaselineHrs AS BaselineHrs,
            XE.SelectedPlannedHrs AS SelectedPlannedHrs,
            XE.SelectedBillableHrs AS SelectedBillableHrs,
            @decScheduledPct AS ScheduledPct,	
            NULL AS UtilizationScheduleFlg,					
            @decUtilizationRatio AS TargetUtilization,
            @decUtilizationPct AS UtilizationPct,
            XE.SelectedETCHrs AS SelectedETCHrs,
            XE.ETCHrs AS ETCHrs,
            XE.JTDHrs AS JTDHrs,
            XE.JTDHrs + XE.ETCHrs AS EACHrs,
            @bitHasPhoto AS HasPhoto,
		    @dtPhotoModDate as PhotoModDate,
            XE.BeforeETCHrs AS BeforeETCHrs,
            XE.AfterETCHrs AS AfterETCHrs,
            @strUtilRatioFlg AS UtilRatioFlg,
            NULL AS PlanID,
            NULL AS TaskID,
            NULL AS AssignmentID,
            @strResourceID AS ResourceID,
            @strGenericResourceID AS GenericResourceID,
            NULL AS ProjectWBS,
            NULL AS ProjectWBSName,
            NULL AS WBS1,
            NULL AS WBS2,
            NULL AS WBS3,						
            NULL AS Company,
            NULL AS TaskName,
            NULL	AS OutlineNumber,
            NULL AS TopWBS1,
            NULL AS TopName,
            0 AS TopKonaSpace,
            NULL AS PMFullName,
            NULL AS ClientName,
            @decHoursPerDay AS HoursPerDay,
            0 AS SumBeforeWD,
            0 AS SumAfterWD,
            NULL AS AssignmentStartDate,
            NULL AS AssignmentEndDate,
            NULL AS MinTPDDate,
            NULL AS MaxTPDDate,
            ISNULL(EM.BillingCategory,ISNULL(GR.Category,0)) AS LaborCat,
            CASE WHEN @strResourceID IS NULL THEN GRORG.Org ELSE EMORG.Org END AS Org,
            CASE WHEN @strResourceID IS NULL THEN GRORG.Name ELSE EMORG.Name END AS OrgName,
            ISNULL(OL1.OrgCode,'') AS OrgLevel1,
            ISNULL(OL2.OrgCode,'') AS OrgLevel2,
            ISNULL(OL3.OrgCode,'') AS OrgLevel3,
            ISNULL(OL4.OrgCode,'') AS OrgLevel4,
            ISNULL(OL5.OrgCode,'') AS OrgLevel5,
            EM.Title AS Title,
            EM.EMail AS EMail,
            EM.HireDate AS HireDate,
            EM.TerminationDate AS TerminationDate,
            CASE WHEN @strResourceID IS NULL THEN CFGGS.Label ELSE CFGES.Label END AS [Status],
            ISNULL(EM.[Status],ISNULL(GR.[Status],'')) AS R_Status,
            @strSupervisorName AS SupervisorName,
            EM.YearsOtherFirms AS YearsOtherFirms,
            EM.PriorYearsFirm AS PriorYearsFirm,
            DATEDIFF(YEAR,EM.HireDate,GETDATE()) AS YearsWithFirm,
            EM.UtilizationRatio AS UtilizationRatio,
            'N' AS HardBooked,
            CFGL.Description AS Location
            FROM (
              SELECT
                ROUND(ISNULL(SUM(TETCHrs), 0.0000), @siHrDecimals) AS ETCHrs,
                ROUND(ISNULL(SUM(BETCHrs), 0.0000), @siHrDecimals) AS BeforeETCHrs,
                ROUND(ISNULL(SUM(AETCHrs), 0.0000), @siHrDecimals) AS AfterETCHrs,
                ROUND(ISNULL(SUM(SETCHrs), 0.0000), @siHrDecimals) AS SelectedETCHrs,
                ROUND(ISNULL(SUM(PTPDHrs), 0.0000), @siHrDecimals) AS PlannedHrs,
                ROUND(ISNULL(SUM(STPDHrs), 0.0000), @siHrDecimals) AS SelectedPlannedHrs,
                ROUND(ISNULL(SUM(BTPDHrs), 0.0000), @siHrDecimals) AS SelectedBillableHrs,
                ROUND(ISNULL(SUM(JTDHrs), 0.0000), @siHrDecimals) AS JTDHrs,
                ROUND(ISNULL(SUM(BaselineHrs), 0.0000), @siHrDecimals) AS BaselineHrs
                FROM (
                  SELECT 
                    SUM(ETCHrs) AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
                    0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabTotalETC
                  UNION ALL
                  SELECT 
                    0 AS TETCHrs, SUM(ETCHrs) AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
                    0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabBeforeETC
                  UNION ALL
                  SELECT 
                    0 AS TETCHrs, 0 AS BETCHrs, SUM(ETCHrs) AS AETCHrs, 0 AS SETCHrs,
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
                    0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabAfterETC
                  UNION ALL
                  SELECT 
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, SUM(ETCHrs) AS SETCHrs,
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
                    0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabSelectedETC
                  UNION ALL
                  SELECT 
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                    SUM(PlannedHrs) AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
                    0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabLabTPD
                  UNION ALL
                  SELECT 
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                    0 AS PTPDHrs, SUM(PlannedHrs) AS STPDHrs, 0 AS BTPDHrs,
                    0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabSelectedTPD
                  UNION ALL
                  SELECT 
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                    0 AS PTPDHrs, 0 AS STPDHrs, SUM(PlannedHrs) AS BTPDHrs,
                    0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabBillableTPD
                  UNION ALL
                  SELECT 
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, SUM(PeriodHrs) AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabLD
                  UNION ALL
                  SELECT 
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, SUM(PlannedHrs) AS BaselineHrs
                    FROM @tabLabBaselineTPD
                ) AS YE
            ) AS XE
            LEFT JOIN EM ON EM.Employee = @strResourceID
            LEFT JOIN GR ON GR.Code = @strGenericResourceID
            LEFT JOIN Organization AS EMORG ON EMORG.Org = EM.Org
            LEFT JOIN Organization AS GRORG ON GRORG.Org = GR.Org
            LEFT JOIN CFGEmployeeStatus AS CFGES ON CFGES.[Status] = EM.[Status]
            LEFT JOIN CFGEmployeeStatus AS CFGGS ON CFGGS.[Status] = GR.[Status]
            LEFT JOIN CFGEMLocation AS CFGL ON CFGL.Code = EM.Location
            OUTER APPLY ngRP$tabOrgLevel(ISNULL(EMORG.Org,GRORG.Org),1) AS OL1
            OUTER APPLY ngRP$tabOrgLevel(ISNULL(EMORG.Org,GRORG.Org),2) AS OL2
            OUTER APPLY ngRP$tabOrgLevel(ISNULL(EMORG.Org,GRORG.Org),3) AS OL3
            OUTER APPLY ngRP$tabOrgLevel(ISNULL(EMORG.Org,GRORG.Org),4) AS OL4
            OUTER APPLY ngRP$tabOrgLevel(ISNULL(EMORG.Org,GRORG.Org),5) AS OL5
      END
    ELSE
      BEGIN
        INSERT @tabResourcesReporting(
          RowID,
          ParentRowID,
          RowLevel,
          HasChildren,
          SelectFlag,
          Name,
          BillableFlg,
          MatchPct,
          StartDate,
          EndDate,
          AvailableHrs,
          PlannedHrs,
          BaselineHrs,
          SelectedPlannedHrs,
          SelectedBillableHrs,
          ScheduledPct,
          UtilizationScheduleFlg,
          TargetUtilization,
          UtilizationPct,
          SelectedETCHrs,
          ETCHrs,
          JTDHrs,
          EACHrs,
          HasPhoto,
		  PhotoModDate,
          BeforeETCHrs,
          AfterETCHrs,
          UtilRatioFlg,
          PlanID,
          TaskID,
          AssignmentID,
          ResourceID,
          GenericResourceID,
          ProjectWBS,
          ProjectWBSName,
          WBS1,
          WBS2,
          WBS3,
          Company,
          TaskName,
          OutlineNumber,
          TopWBS1,
          TopName,
          TopKonaSpace,
          PMFullName,
          ClientName,
          HoursPerDay,
          SumBeforeWD,
          SumAfterWD,
          MinASGDate,
          MaxASGDate,
          MinTPDDate,
          MaxTPDDate,
          LaborCat,
          Org,
          OrgName,
          OrgLevel1,
          OrgLevel2,
          OrgLevel3,
          OrgLevel4,
          OrgLevel5,
          Title,
          Email,
          HireDate,
          TerminationDate,
          [Status],
          R_Status,
          SupervisorName,
          YearsOtherFirms,
          PriorYearsFirm,
          YearsWithFirm,
          UtilizationRatio,
          HardBooked,
          Location
        )
          SELECT
            @strIDPrefix + '|' + T.TaskID AS RowID,
            '' AS ParentRowID,
            0 AS RowLevel,
            CASE WHEN @intTaskCount > 0 THEN CONVERT(bit, 1) ELSE CONVERT(bit, 0) END AS HasChildren,
            'N' AS SelectFlag,
            @strResourceName AS Name,
            '' AS BillableFlg,
            0 AS MatchPct,
            T.StartDate AS StartDate,
            T.EndDate AS EndDate,
            @decAvailableHrs AS AvailableHrs,
            XE.PlannedHrs AS PlannedHrs,
            XE.BaselineHrs AS BaselineHrs,
            XE.SelectedPlannedHrs AS SelectedPlannedHrs,
            XE.SelectedBillableHrs AS SelectedBillableHrs,
            @decScheduledPct AS ScheduledPct,
            T.UtilizationScheduleFlg AS UtilizationScheduleFlg,
            @decUtilizationRatio AS TargetUtilization,
            @decUtilizationPct AS UtilizationPct,
            XE.SelectedETCHrs AS SelectedETCHrs,
            XE.ETCHrs AS ETCHrs,
            XE.JTDHrs AS JTDHrs,
            XE.JTDHrs + XE.ETCHrs AS EACHrs,
            @bitHasPhoto AS HasPhoto,
		    @dtPhotoModDate as PhotoModDate,
            XE.BeforeETCHrs AS BeforeETCHrs,
            XE.AfterETCHrs AS AfterETCHrs,
            @strUtilRatioFlg AS UtilRatioFlg,
            T.PlanID AS PlanID,
            T.TaskID AS TaskID,
            NULL AS AssignmentID,
            @strResourceID AS ResourceID,
            @strGenericResourceID AS GenericResourceID,
            dbo.ngRPGetConCatTaskWBS1(T.PlanID,T.TaskID) AS ProjectWBS,
            dbo.ngRPGetConCatTaskName(T.PlanID,T.TaskID) AS ProjectWBSName,
            T.WBS1 AS WBS1,
            T.WBS2 AS WBS2,
            T.WBS3 AS WBS3,
            T.Company AS Company,
            T.Name AS TaskName,
            T.OutlineNumber	AS OutlineNumber,
            T.TopWBS1 AS TopWBS1,
            T.TopName AS TopName,
            0 AS TopKonaSpace,
            NULL AS PMFullName,
            NULL AS ClientName,
            @decHoursPerDay AS HoursPerDay,
            0 AS SumBeforeWD,
            0 AS SumAfterWD,
            T.ASG_StartDate AS AssignmentStartDate,
            T.ASG_EndDate AS AssignmentEndDate,
            NULL AS MinTPDDate,
            NULL AS MaxTPDDate,
            ISNULL(EM.BillingCategory,ISNULL(GR.Category,0)) AS LaborCat,
            CASE WHEN @strResourceID IS NULL THEN GRORG.Org ELSE EMORG.Org END AS Org,
            CASE WHEN @strResourceID IS NULL THEN GRORG.Name ELSE EMORG.Name END AS OrgName,
            ISNULL(OL1.OrgCode,'') AS OrgLevel1,
            ISNULL(OL2.OrgCode,'') AS OrgLevel2,
            ISNULL(OL3.OrgCode,'') AS OrgLevel3,
            ISNULL(OL4.OrgCode,'') AS OrgLevel4,
            ISNULL(OL5.OrgCode,'') AS OrgLevel5,
            EM.Title AS Title,
            EM.EMail AS EMail,
            EM.HireDate AS HireDate,
            EM.TerminationDate AS TerminationDate,
            CASE WHEN @strResourceID IS NULL THEN CFGGS.Label ELSE CFGES.Label END AS [Status],
            ISNULL(EM.[Status],ISNULL(GR.[Status],'')) AS R_Status,
            @strSupervisorName AS SupervisorName,
            EM.YearsOtherFirms AS YearsOtherFirms,
            EM.PriorYearsFirm AS PriorYearsFirm,
            DATEDIFF(YEAR,EM.HireDate,GETDATE()) AS YearsWithFirm,
            EM.UtilizationRatio AS UtilizationRatio,
            T.HardBooked AS HardBooked,
            CFGL.Description AS Location
            FROM (SELECT 
                    T.PlanID, 
                    T.TaskID, 
                    T.OutlineNumber, 
                    CASE WHEN T.LaborCode IS NOT NULL THEN L.LaborCodeName ELSE T.NAME END AS NAME,  
                    T.WBS1, 
                    T.WBS2, 
                    T.WBS3, 
                    T.Company,
                    T.TopName, 
                    T.TopWBS1, 
                    T.StartDate, 
                    T.EndDate, 
                    T.UtilizationScheduleFlg AS UtilizationScheduleFlg,
                    ASG_StartDate, 
                    ASG_EndDate,
                    A.Hardbooked
                  FROM @tabTask T
                    INNER JOIN @tabAssignment A ON T.TaskID = A.TaskID
					CROSS APPLY dbo.stRP$tabLaborCode(T.PlanID,T.LaborCode) AS L ) AS T
            LEFT JOIN EM ON EM.Employee = @strResourceID
            LEFT JOIN GR ON GR.Code = @strGenericResourceID
            LEFT JOIN Organization AS EMORG ON EMORG.Org = EM.Org
            LEFT JOIN Organization AS GRORG ON GRORG.Org = GR.Org
            LEFT JOIN CFGEmployeeStatus AS CFGES ON CFGES.[Status] = EM.[Status]
            LEFT JOIN CFGEmployeeStatus AS CFGGS ON CFGGS.[Status] = GR.[Status]
            LEFT JOIN CFGEMLocation AS CFGL ON CFGL.Code = EM.Location
            OUTER APPLY ngRP$tabOrgLevel(ISNULL(EMORG.Org,GRORG.Org),1) AS OL1
            OUTER APPLY ngRP$tabOrgLevel(ISNULL(EMORG.Org,GRORG.Org),2) AS OL2
            OUTER APPLY ngRP$tabOrgLevel(ISNULL(EMORG.Org,GRORG.Org),3) AS OL3
            OUTER APPLY ngRP$tabOrgLevel(ISNULL(EMORG.Org,GRORG.Org),4) AS OL4
            OUTER APPLY ngRP$tabOrgLevel(ISNULL(EMORG.Org,GRORG.Org),5) AS OL5			
            LEFT JOIN (
              SELECT
                PlanID AS PlanID,
                OutlineNumber AS OutlineNumber,
                ROUND(ISNULL(SUM(TETCHrs), 0.0000), @siHrDecimals) AS ETCHrs,
                ROUND(ISNULL(SUM(BETCHrs), 0.0000), @siHrDecimals) AS BeforeETCHrs,
                ROUND(ISNULL(SUM(AETCHrs), 0.0000), @siHrDecimals) AS AfterETCHrs,
                ROUND(ISNULL(SUM(SETCHrs), 0.0000), @siHrDecimals) AS SelectedETCHrs,
                ROUND(ISNULL(SUM(PTPDHrs), 0.0000), @siHrDecimals) AS PlannedHrs,
                ROUND(ISNULL(SUM(STPDHrs), 0.0000), @siHrDecimals) AS SelectedPlannedHrs,
                ROUND(ISNULL(SUM(BTPDHrs), 0.0000), @siHrDecimals) AS SelectedBillableHrs,
                ROUND(ISNULL(SUM(JTDHrs), 0.0000), @siHrDecimals) AS JTDHrs,
                ROUND(ISNULL(SUM(BaselineHrs), 0.0000), @siHrDecimals) AS BaselineHrs
                FROM (
                  SELECT
                    PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                    ETCHrs AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabTotalETC
                  UNION ALL
                  SELECT 
                    PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                    0 AS TETCHrs, ETCHrs AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabBeforeETC
                  UNION ALL
                  SELECT 
                    PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                    0 AS TETCHrs, 0 AS BETCHrs, ETCHrs AS AETCHrs, 0 AS SETCHrs, 
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabAfterETC
                  UNION ALL
                  SELECT 
                    PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, ETCHrs AS SETCHrs, 
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabSelectedETC
                  UNION ALL
                  SELECT 
                    PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                    PlannedHrs AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabLabTPD
                  UNION ALL
                  SELECT 
                    PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                    0 AS PTPDHrs, PlannedHrs AS STPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabSelectedTPD
                  UNION ALL
                  SELECT 
                    PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                    0 AS PTPDHrs, 0 AS STPDHrs, PlannedHrs AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                    FROM @tabBillableTPD
                  UNION ALL
                  SELECT 
                    PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, PeriodHrs as JTDHrs, 0 AS BaselineHrs
                    FROM @tabLD
                  UNION ALL
                  SELECT 
                    PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                    0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                    0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, 0 as JTDHrs, PlannedHrs AS BaselineHrs
                    FROM @tabLabBaselineTPD
                ) AS ZE
              GROUP BY ZE.PlanID, ZE.OutlineNumber
            ) AS XE
          ON XE.PlanID = T.PlanID AND XE.OutlineNumber = T.OutlineNumber
      END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
