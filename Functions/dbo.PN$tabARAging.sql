SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[PN$tabARAging] (
  @strWBS1 nvarchar(30),
  @strJTDDate VARCHAR(8),
  @strActivePeriod Varchar(6)
) -- Date must be in format: 'yyyymmdd' regardless of UI Culture.

  RETURNS @tabARAging TABLE (
    BilledAmt decimal(19,4),
    ReceivedAmt decimal(19,4),
    RetainageAmt decimal(19,4),
    RetainerAmt decimal(19,4),
    TotalARAmt decimal(19,4),
    AROver30 decimal(19,4),
    AROver45 decimal(19,4),
    AROver60 decimal(19,4),
    AROver90 decimal(19,4),
    AROver120 decimal(19,4)
  )

BEGIN

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
 
  DECLARE @strCompany nvarchar(14)
  DECLARE @strMultiCompanyEnabled varchar(1)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

  DECLARE @tabLedgerAR TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    InvoiceNumber nvarchar(12) COLLATE database_default,
    InvoiceDate datetime,
    TransType varchar(2) COLLATE database_default,
    SubType varchar(1) COLLATE database_default,
    AcctType int,
    AmountBillingCurrency decimal(19,4)
    UNIQUE (RowID, WBS1)
  )

  DECLARE @tabCreditMemo TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    InvoiceNumber nvarchar(12) COLLATE database_default,
    InvoiceDate datetime,
    TransType varchar(2) COLLATE database_default,
    SubType varchar(1) COLLATE database_default,
    AcctType int,
    AmountBillingCurrency decimal(19,4)
    UNIQUE (RowID, WBS1)
  )

  DECLARE @tabRetainage TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    InvoiceNumber nvarchar(12) COLLATE database_default,
    InvoiceDate datetime,
    TransType varchar(2) COLLATE database_default,
    SubType varchar(1) COLLATE database_default,
    AcctType int,
    AmountBillingCurrency decimal(19,4)
    UNIQUE (RowID, WBS1)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
          
  -- Get MultiCompanyEnabled flag.
  
  SELECT
    @strMultiCompanyEnabled = MultiCompanyEnabled
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat     

  -- Save Company string for use later.
  
  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          
  -- Set JTD/ETC Dates.
  
  SET @dtJTDDate = CONVERT(datetime, @strJTDDate)
  SET @dtETCDate = DATEADD(d, 1, @dtJTDDate) 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find those Invoices with balances (AmountBillingCurrency <> 0)
  -- The result set is grouped by TransType so that I can get the Invoiced and Recieved AmountBillingCurrencys.

  INSERT INTO @tabLedgerAR (
    WBS1,
    InvoiceNumber,
    InvoiceDate,
    TransType,
    SubType,
    AcctType,
    AmountBillingCurrency
  )
    SELECT DISTINCT
      L.WBS1 AS WBS1, 
      L.Invoice AS InvoiceNumber,
      AR.InvoiceDate AS InvoiceDate,
      L.TransType,
      L.SubType,
      ISNULL(CA.Type, 99) AS AcctType,
      SUM(
        CASE 
          WHEN TransType = 'CR' AND SubType <> 'T' THEN AmountBillingCurrency 
          ELSE -AmountBillingCurrency 
        END
      ) AS AmountBillingCurrency
      FROM LedgerAR AS L
        LEFT JOIN AR ON L.WBS1 = AR.WBS1 AND L.WBS2 = AR.WBS2 AND L.WBS3 = AR.WBS3 AND L.Invoice = AR.Invoice
        LEFT JOIN CA ON L.Account = CA.Account
      WHERE L.WBS1 = @strWBS1 AND L.AutoEntry = 'N' AND L.CreditMemoRefNo IS NULL AND
        ((TransType = 'IN' AND L.SubType IS NULL) OR
         (TransType = 'IN' AND L.SubType IN ('I', 'R', 'X')) OR
         (TransType = 'CR' AND L.SubType IN ('R', 'X', 'T'))
        )
		AND L.Period <= @strActivePeriod
      GROUP BY L.WBS1, L.Invoice, AR.InvoiceDate, L.TransType, L.SubType, CA.Type
      ORDER BY WBS1, InvoiceNumber
 
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

 INSERT INTO @tabCreditMemo (
    WBS1,
    InvoiceNumber,
    InvoiceDate,
    TransType,
    SubType,
    AcctType,
    AmountBillingCurrency
  )
    SELECT DISTINCT
      L.WBS1 AS WBS1, 
      L.Invoice AS InvoiceNumber,
      AR.InvoiceDate AS InvoiceDate,
      L.TransType,
      L.SubType,
      ISNULL(CA.Type, 99) AS AcctType,
      SUM(-AmountBillingCurrency) AS AmountBillingCurrency
      FROM LedgerAR AS L
        LEFT JOIN AR ON L.WBS1 = AR.WBS1 AND L.WBS2 = AR.WBS2 AND L.WBS3 = AR.WBS3 AND L.Invoice = AR.Invoice
        LEFT JOIN CA ON L.Account = CA.Account
      WHERE L.WBS1 = @strWBS1 AND L.AutoEntry = 'N'
        AND L.TransType = 'IN' 
        AND (ISNULL(L.SubType, ' ') != 'R' AND ISNULL(L.SubType, ' ') != 'I') 
        AND CreditMemoRefNo IS NOT NULL
		AND L.Period <= @strActivePeriod
      GROUP BY L.WBS1, L.Invoice, AR.InvoiceDate, L.TransType, L.SubType, CA.Type
      ORDER BY WBS1, InvoiceNumber

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

 INSERT INTO @tabRetainage (
    WBS1,
    InvoiceNumber,
    InvoiceDate,
    TransType,
    SubType,
    AcctType,
    AmountBillingCurrency
  )
    SELECT DISTINCT
      L.WBS1 AS WBS1, 
      L.Invoice AS InvoiceNumber,
      AR.InvoiceDate AS InvoiceDate,
      L.TransType,
      L.SubType,
      ISNULL(CA.Type, 99) AS AcctType,
      SUM(AmountBillingCurrency) AS AmountBillingCurrency
      FROM LedgerAR AS L
        LEFT JOIN AR ON L.WBS1 = AR.WBS1 AND L.WBS2 = AR.WBS2 AND L.WBS3 = AR.WBS3 AND L.Invoice = AR.Invoice
        LEFT JOIN CA ON L.Account = CA.Account
      WHERE L.WBS1 = @strWBS1 AND L.AutoEntry = 'N' AND (TransType = 'IN' AND L.SubType IN ('R'))
      AND L.Period <= @strActivePeriod
	  GROUP BY L.WBS1, L.Invoice, AR.InvoiceDate, L.TransType, L.SubType, CA.Type
      ORDER BY WBS1, InvoiceNumber

 -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT INTO @tabARAging (
    BilledAmt,
    ReceivedAmt,
    RetainageAmt,
    RetainerAmt,
    TotalARAmt,
    AROver30,
    AROver45,
    AROver60,
    AROver90,
    AROver120
  )
    SELECT
      BilledAmt AS BilledAmt,
      ReceivedAmt AS ReceivedAmt,
      RetainageAmt AS RetainageAmt,
      RetainerAmt AS RetainerAmt,
      SUM(TotalARAmt) AS TotalARAmt,
      SUM(AROver30) AS AROver30,
      SUM(AROver45) AS AROver45,
      SUM(AROver60) AS AROver60,
      SUM(AROver90) AS AROver90,
      SUM(AROver120) AS AROver120
      FROM ( /* X */
        SELECT
          BilledAmt AS BilledAmt,
          ReceivedAmt AS ReceivedAmt,
          RetainageAmt AS RetainageAmt,
          RetainerAmt AS RetainerAmt,
          SUM(ARAmountBillingCurrency) AS TotalARAmt,
          CASE WHEN AgingBucket = 0 THEN SUM(ARAmountBillingCurrency) ELSE 0.0000 END AS AROver30,
          CASE WHEN AgingBucket = 1 THEN SUM(ARAmountBillingCurrency) ELSE 0.0000 END AS AROver45,
          CASE WHEN AgingBucket = 2 THEN SUM(ARAmountBillingCurrency) ELSE 0.0000 END AS AROver60,
          CASE WHEN AgingBucket = 3 THEN SUM(ARAmountBillingCurrency) ELSE 0.0000 END AS AROver90,
          CASE WHEN AgingBucket = 4 THEN SUM(ARAmountBillingCurrency) ELSE 0.0000 END AS AROver120
          FROM ( /* A */
            SELECT
              SUM(BilledAmt) AS BilledAmt,
              SUM(ReceivedAmt) AS ReceivedAmt,
              SUM(RetainageAmt) AS RetainageAmt,
              SUM(RetainerAmt) AS RetainerAmt
              FROM ( /* AA */
                SELECT /* Billed */
                  SUM(AmountBillingCurrency) AS BilledAmt,
                  0.0000 AS ReceivedAmt,
                  0.0000 AS RetainageAmt,
                  0.0000 AS RetainerAmt
                  FROM @tabLedgerAR
                  WHERE TransType = 'IN' AND (SubType IS NULL OR SubType = 'I')
                  GROUP BY TransType
                UNION ALL SELECT /* Credit Memo */
                  SUM(AmountBillingCurrency) AS BilledAmt,
                  0.0000 AS ReceivedAmt,
                  0.0000 AS RetainageAmt,
                  0.0000 AS RetainerAmt
                  FROM @tabCreditMemo
                  GROUP BY TransType
                UNION ALL SELECT 
                  0.0000 AS BilledAmt,
                  SUM(CASE WHEN SubType = 'T' THEN AmountBillingCurrency ELSE -AmountBillingCurrency END) AS ReceivedAmt,
                  0.0000 AS RetainageAmt,
                  0.0000 AS RetainerAmt
                  FROM @tabLedgerAR
                  WHERE ((TransType = 'CR' AND SubType <> 'T') OR (TransType = 'CR' AND SubType = 'T' AND InvoiceNumber IS NULL)) 
                  GROUP BY TransType
                UNION ALL SELECT  /* Retainage */
                  0.0000 AS BilledAmt,
                  0.0000 AS ReceivedAmt,
                  SUM(AmountBillingCurrency) AS RetainageAmt,
                  0.0000 AS RetainerAmt
                  FROM @tabRetainage
                  GROUP BY TransType
                UNION ALL SELECT
                  0.0000 AS BilledAmt,
                  0.0000 AS ReceivedAmt,
                  0.0000 AS RetainageAmt,
                  SUM(AmountBillingCurrency) AS RetainerAmt
                  FROM @tabLedgerAR
                  WHERE TransType= 'CR' AND SubType = 'T'
                  GROUP BY TransType
              ) AS AA
          ) AS A LEFT JOIN ( /* B */
            SELECT 
              SUM(AmountBillingCurrency) AS ARAmountBillingCurrency,
              CASE
                WHEN SUM(AmountBillingCurrency) = 0 THEN -1
                WHEN CONVERT(int, @dtJTDDate) - CONVERT(int, InvoiceDate) BETWEEN 31 AND 45 THEN 0 /* AR Over 30 */
                WHEN CONVERT(int, @dtJTDDate) - CONVERT(int, InvoiceDate) BETWEEN 46 AND 60 THEN 1 /* AR Over 45 */
                WHEN CONVERT(int, @dtJTDDate) - CONVERT(int, InvoiceDate) BETWEEN 61 AND 90 THEN 2 /* AR Over 60 */
                WHEN CONVERT(int, @dtJTDDate) - CONVERT(int, InvoiceDate) BETWEEN 91 AND 120 THEN 3 /* AR Over 90 */
                WHEN CONVERT(int, @dtJTDDate) - CONVERT(int, InvoiceDate) > 120 THEN 4 /* AR Over 120 */
                END As AgingBucket
              FROM @tabLedgerAR
              WHERE 
                ((TransType= 'IN' AND IsNull(SubType, ' ') <> 'X') 
                OR (TransType= 'CR' AND (SubType= 'R' OR (SubType= 'T' AND IsNull(InvoiceNumber, ' ') <> ' '))))
              GROUP BY InvoiceDate
            UNION ALL
            SELECT 
              SUM(AmountBillingCurrency) AS ARAmountBillingCurrency,
              CASE
                WHEN SUM(AmountBillingCurrency) = 0 THEN -1
                WHEN CONVERT(int, @dtJTDDate) - CONVERT(int, InvoiceDate) BETWEEN 31 AND 45 THEN 0 /* AR Over 30 */
                WHEN CONVERT(int, @dtJTDDate) - CONVERT(int, InvoiceDate) BETWEEN 46 AND 60 THEN 1 /* AR Over 45 */
                WHEN CONVERT(int, @dtJTDDate) - CONVERT(int, InvoiceDate) BETWEEN 61 AND 90 THEN 2 /* AR Over 60 */
                WHEN CONVERT(int, @dtJTDDate) - CONVERT(int, InvoiceDate) BETWEEN 91 AND 120 THEN 3 /* AR Over 90 */
                WHEN CONVERT(int, @dtJTDDate) - CONVERT(int, InvoiceDate) > 120 THEN 4 /* AR Over 120 */
                END As AgingBucket
              FROM @tabCreditMemo
              GROUP BY InvoiceDate
          ) AS B ON 1 = 1
          GROUP BY BilledAmt, ReceivedAmt, RetainageAmt, RetainerAmt, AgingBucket
      ) AS X
    GROUP BY BilledAmt, ReceivedAmt, RetainageAmt, RetainerAmt
                  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
