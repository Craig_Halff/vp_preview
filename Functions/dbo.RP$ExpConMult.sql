SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$ExpConMult]
  (@strAccount Nvarchar(13),
   @sintRtMethod smallint = 0,
   @intRtTabNo int = 0,
   @sintDecimals smallint = 2,
   @decDefMult decimal(19,4) = 0) --> RPPlan.ExpBillMultiplier or ConBillMultiplier.
  RETURNS decimal(19,4)
BEGIN

  /*--------------------------------------------------------------------------------------------------------------------
    This function encapsulates the complex rules that determine the Billing Multiplier for Expense & Consultant.
  */--------------------------------------------------------------------------------------------------------------------

  DECLARE @decMult decimal(19,4)
  DECLARE @decRetMult decimal(19,4)
  
  SET @decMult = NULL

  IF (@sintRtMethod = 2)
    SELECT @decMult = Multiplier
      FROM BTEAAccts AS R 
      WHERE R.Account = @strAccount AND R.TableNo = @intRtTabNo
  ELSE
    IF (@sintRtMethod = 3)
      SELECT @decMult = Multiplier
      FROM BTECAccts AS A
    	  INNER JOIN BTECCats AS C ON A.Category = C.Category AND A.TableNo = C.TableNo
    	WHERE A.Account = @strAccount AND A.TableNo = @intRtTabNo 
  
  SELECT @decRetMult = 
    ROUND(ISNULL((CASE WHEN (Type = 7 OR Type = 8) AND  @sintRtMethod = 1 THEN 1
                       WHEN (Type = 7 OR Type = 8) AND  @sintRtMethod <> 1 THEN ISNULL(@decMult, 1)
                       ELSE ISNULL(@decMult, @decDefMult) END), 0), @sintDecimals)   
    FROM CA WHERE Account = @strAccount
                  
  RETURN(@decRetMult)

END -- fn_RP$ExpConMult
GO
