SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabLaborCode](
  @strPlanID varchar(32),
  @strLaborCode nvarchar(14)
) 
  RETURNS @tabLaborCode TABLE (
    LaborCodeName nvarchar(255) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default
)

BEGIN

  DECLARE @strLaborCodeDesc nvarchar(255) = ''
  DECLARE @strTaskLaborCode nvarchar(14) = ''

  DECLARE @strLC1Code nvarchar(14) = ''
  DECLARE @strLC2Code nvarchar(14) = ''
  DECLARE @strLC3Code nvarchar(14) = ''
  DECLARE @strLC4Code nvarchar(14) = ''
  DECLARE @strLC5Code nvarchar(14) = ''
  DECLARE @strLC1Desc nvarchar(max) = ''
  DECLARE @strLC2Desc nvarchar(max) = ''
  DECLARE @strLC3Desc nvarchar(max) = ''
  DECLARE @strLC4Desc nvarchar(max) = ''
  DECLARE @strLC5Desc nvarchar(max) = ''
  DECLARE @strLC1WildCard nvarchar(14) = ''
  DECLARE @strLC2WildCard nvarchar(14) = ''
  DECLARE @strLC3WildCard nvarchar(14) = ''
  DECLARE @strLC4WildCard nvarchar(14) = ''
  DECLARE @strLC5WildCard nvarchar(14) = ''
  
  DECLARE @strLCDelimiter varchar(1) = ''
  DECLARE @siLCLevels smallint = 0
  DECLARE @siLC1Start smallint = 0
  DECLARE @siLC2Start smallint = 0
  DECLARE @siLC3Start smallint = 0
  DECLARE @siLC4Start smallint = 0
  DECLARE @siLC5Start smallint = 0
  DECLARE @siLC1Length smallint = 0
  DECLARE @siLC2Length smallint = 0
  DECLARE @siLC3Length smallint = 0
  DECLARE @siLC4Length smallint = 0
  DECLARE @siLC5Length smallint = 0

  DECLARE @strLCLevel1Enabled nvarchar(14) = 'Y'
  DECLARE @strLCLevel2Enabled nvarchar(14) = 'Y'
  DECLARE @strLCLevel3Enabled nvarchar(14) = 'Y'
  DECLARE @strLCLevel4Enabled nvarchar(14) = 'Y'
  DECLARE @strLCLevel5Enabled nvarchar(14) = 'Y'

  DECLARE @strLCDescSeparator nvarchar(3) = ' / '
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get LCDelimiter and other LC formats.
  
  SELECT
    @strLCLevel1Enabled = LCLevel1Enabled,
    @strLCLevel2Enabled = LCLevel2Enabled,
    @strLCLevel3Enabled = LCLevel3Enabled,
    @strLCLevel4Enabled = LCLevel4Enabled,
    @strLCLevel5Enabled = LCLevel5Enabled
    FROM PNPlan WHERE PlanID = @strPlanID

	SELECT
    @siLCLevels = LCLevels,
	@strLCDelimiter = ISNULL(LTRIM(RTRIM(LCDelimiter)), ''),
    @siLC1Start = LC1Start,
    @siLC2Start = LC2Start,
    @siLC3Start = LC3Start,
    @siLC4Start = LC4Start,
    @siLC5Start = LC5Start,
    @siLC1Length = LC1Length,
    @siLC2Length = LC2Length,
    @siLC3Length = LC3Length,
    @siLC4Length = LC4Length,
    @siLC5Length = LC5Length
    FROM CFGFormat

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Decompose Labor Code string into individual levels using the formatting information in CFGFormat.

  SELECT 
    @strLC1WildCard = REPLICATE('_', @siLC1Length),
    @strLC2WildCard = REPLICATE('_', @siLC2Length),
    @strLC3WildCard = REPLICATE('_', @siLC3Length),
    @strLC4WildCard = REPLICATE('_', @siLC4Length),
    @strLC5WildCard = REPLICATE('_', @siLC5Length)

  -- Set up code for each level based on the system level and planning labor code level enabled or not
  SELECT 
    @strLC1Code = 
        CASE
        WHEN @strLCLevel1Enabled = 'Y' THEN SUBSTRING(@strLaborCode, @siLC1Start, @siLC1Length)
        ELSE @strLC1WildCard
        END
  
  IF @siLCLevels >=2 
  BEGIN
	SELECT 
	@strLC2Code = 
	CASE
	WHEN @strLCLevel2Enabled = 'Y' THEN SUBSTRING(@strLaborCode, @siLC2Start, @siLC2Length)
	ELSE @strLC2WildCard
	END
  END --IF @siLCLevels >=2

  IF @siLCLevels >=3 
  BEGIN
	SELECT 
	@strLC3Code = 
	CASE
	WHEN @strLCLevel3Enabled = 'Y' THEN SUBSTRING(@strLaborCode, @siLC3Start, @siLC3Length)
	ELSE @strLC3WildCard
	END
  END --IF @siLCLevels >=3

  IF @siLCLevels >=4 
  BEGIN
	SELECT 
	@strLC4Code = 
	CASE
	WHEN @strLCLevel4Enabled = 'Y' THEN SUBSTRING(@strLaborCode, @siLC4Start, @siLC4Length)
	ELSE @strLC4WildCard
	END
  END --IF @siLCLevels >=4

  IF @siLCLevels = 5 
  BEGIN
	SELECT 
	@strLC5Code = 
	CASE
	WHEN @strLCLevel5Enabled = 'Y' THEN SUBSTRING(@strLaborCode, @siLC5Start, @siLC5Length)
	ELSE @strLC5WildCard
	END
  END --IF @siLCLevels >=5

 -- Set up code description for each level based on the system level 
 -- and planning labor code level enabled or not

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    SELECT 
    @strLC1Desc = 
        CASE
        WHEN @strLC1Code = @strLC1WildCard THEN @strLC1WildCard
        WHEN @strLC1Code <> @strLC1WildCard THEN ISNULL(MAX(LC.Label), '')
        END
    FROM CFGLCCodes AS LC
    WHERE LC.LCLevel = 1 AND LC.Code = @strLC1Code

	IF @siLCLevels >=2 
	BEGIN
		SELECT 
		@strLC2Desc = 
			CASE
			WHEN @strLC2Code = @strLC2WildCard THEN @strLC2WildCard
			WHEN @strLC2Code <> @strLC2WildCard THEN ISNULL(MAX(LC.Label), '')
			END
		FROM CFGLCCodes AS LC
		WHERE LC.LCLevel = 2 AND LC.Code = @strLC2Code
		
		SET @strLC2Desc = @strLCDescSeparator + @strLC2Desc
	END --IF @siLCLevels >=2 

	IF @siLCLevels >=3
	BEGIN
		SELECT 
		@strLC3Desc = 
			CASE
			WHEN @strLC3Code = @strLC3WildCard THEN @strLC3WildCard
			WHEN @strLC3Code <> @strLC3WildCard THEN ISNULL(MAX(LC.Label), '')
			END
		FROM CFGLCCodes AS LC
		WHERE LC.LCLevel = 3 AND LC.Code = @strLC3Code

		SET @strLC3Desc = @strLCDescSeparator + @strLC3Desc
	END --IF @siLCLevels >=3

	IF @siLCLevels >=4
	BEGIN
		SELECT 
		@strLC4Desc = 
			CASE
			WHEN @strLC4Code = @strLC4WildCard THEN @strLC4WildCard
			WHEN @strLC4Code <> @strLC4WildCard THEN ISNULL(MAX(LC.Label), '')
			END
		FROM CFGLCCodes AS LC
		WHERE LC.LCLevel = 4 AND LC.Code = @strLC4Code

		SET @strLC4Desc = @strLCDescSeparator + @strLC4Desc
	END

	IF @siLCLevels >=5
	BEGIN
		SELECT 
		@strLC5Desc = 
			CASE
			WHEN @strLC5Code = @strLC5WildCard THEN @strLC5WildCard
			WHEN @strLC5Code <> @strLC5WildCard THEN ISNULL(MAX(LC.Label), '')
			END
		FROM CFGLCCodes AS LC
		WHERE LC.LCLevel = 5 AND LC.Code = @strLC5Code

		SET @strLC5Desc = @strLCDescSeparator + @strLC5Desc
	END
	 
	SET @strTaskLaborCode = @strLC1Code  
	IF @strLC2Code <> '' SET @strTaskLaborCode = @strTaskLaborCode + @strLCDelimiter + @strLC2Code
	IF @strLC3Code <> '' SET @strTaskLaborCode = @strTaskLaborCode + @strLCDelimiter + @strLC3Code
	IF @strLC4Code <> '' SET @strTaskLaborCode = @strTaskLaborCode + @strLCDelimiter + @strLC4Code
	IF @strLC5Code <> '' SET @strTaskLaborCode = @strTaskLaborCode + @strLCDelimiter + @strLC5Code

	SET @strLaborCodeDesc = @strLC1Desc + @strLC2Desc + @strLC3Desc + @strLC4Desc + @strLC5Desc
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      INSERT @tabLaborCode(
        LaborCodeName,
        LaborCode
      )
        SELECT
          @strLaborCodeDesc,
          @strTaskLaborCode
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN 

END -- stRP$tabLaborCode
GO
