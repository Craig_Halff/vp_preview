SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[RM$GetFullWBSForTask]
  (@planid varchar(32) ,
   @outlineNumber Nvarchar(255))
RETURNS Nvarchar(max)

BEGIN

/*--------------------------------------------------------------------------------------------------------------------
This function returns a string of the *real* WBS levels assigned to given task in RPTask. Used in RM generic resources
Given task outline, resolve all *real* wbs levels. So, say my plan wbs is like this
Project ABC
   Phase XYZ
      Fake level 1
         Task 123
            Fake Level 2
               Labor Code A7
                  Fake Level 3
                     Resource Assignment

I want back:
Project ABC, Phase XYZ, Task 123, LC A7
*/--------------------------------------------------------------------------------------------------------------------

DECLARE @strRet AS Nvarchar(max), @prefix AS Nvarchar(35)
DECLARE @levelname AS Nvarchar(100), @parentOutlineNumber AS Nvarchar(255), @WBSType as Nvarchar(4)

SET @strRet = ''

-- level name to have number and name
SELECT @levelName = CASE 
						WHEN WBSType = 'WBS1' THEN WBS1 + ':'
						WHEN WBSType = 'WBS2' THEN WBS2 + ':'
						WHEN WBSType = 'WBS3' THEN WBS3 + ':'
						WHEN WBSType = 'LBCD' THEN LaborCode + ':'
						ELSE ''
					END + 
					CASE WHEN WBSType <> 'LBCD' then Name 
					ELSE (SELECT 
							LC1Desc  + 
							CASE WHEN LC2Desc>'' THEN ' / ' + LC2Desc ELSE  '' END  + 
							CASE WHEN LC3Desc>'' THEN ' / ' + LC3Desc ELSE  '' END  + 
							CASE WHEN LC4Desc>'' THEN ' / ' + LC4Desc ELSE  '' END  + 
							CASE WHEN LC5Desc>'' THEN ' / ' + LC5Desc ELSE  '' END 
						FROM dbo.stRP$tabLaborCodeSubcodes(LaborCode)) 
					end, 
		@parentOutlineNumber = parentOutlineNumber, @wbsType = wbsType
   FROM RPTask 
   WHERE RPTask.PlanID = @planID AND outlineNumber = @outlineNumber

IF @levelname IS NULL 
     RETURN ''

IF @WBSType IS NOT NULL 
	SET @strRet =  @levelname + '|'

-- recurse if have parent
IF @parentOutlineNumber IS NOT NULL 
   SET @strRet = dbo.RM$GetFullWBSForTask( @planId, @parentOutlineNumber) + @strRet

RETURN(@strRet)
  
END
GO
