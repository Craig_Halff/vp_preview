SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_PAT_JTDPaidStatusCL] (@PayableSeq int, @WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS varchar(20)
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select * from  dbo.fnCCG_PAT_JTDPaidCL(5284,null,' ',' ')
	select * from  dbo.fnCCG_PAT_JTDPaidCL(5284,'PATPWP0.02','001',' ')
	
*/
    declare @prevpay money, @paidCL money, @status varchar(20), @billed varchar(20), @res varchar(20) 
	
	SELECT 
		--Sum(CASE WHEN (LedgerAP.TransType= /*N*/'AP' AND LedgerAP.SubType= 'L') OR (LedgerAP.TransType= /*N*/'PP' AND LedgerAP.SubType<>'X') THEN Case When LedgerAP.TransType= /*N*/'AP' Then LedgerAP.amountsourcecurrency else -LedgerAP.amountsourcecurrency end ELSE 0 END) AS Balance, 
		--@prevpay = Sum(Case When LedgerAP.TransType= /*N*/'PP' Then LedgerAP.amountsourcecurrency else 0 end), --prevpay
		--@paidCL = SUM(CASE WHEN CLPaidPeriod IS NOT null then Amount /*or BillExt*/ else 0 END),
		@status = AVG(CASE WHEN CLPaidPeriod IS NOT null then 1 WHEN INInvoice IS NOT NULL then 0 else NULL END)
	FROM (CCG_PAT_Payable PAT
	INNER JOIN VO on pat.Vendor=vo.Vendor and pat.Voucher=vo.Voucher 
		INNER JOIN LedgerAP ON (VO.Vendor= LedgerAP.Vendor) AND (VO.Voucher= LedgerAP.Voucher)) 
		INNER JOIN VE ON VO.Vendor= VE.Vendor INNER JOIN VEAccounting ON VO.Vendor= VEAccounting.Vendor AND VO.Company= VEAccounting.Company 
		INNER JOIN (
			Select AR.WBS1 as INWBS1, AR.WBS2 as INWBS2, AR.WBS3 as INWBS3, AR.Invoice as INInvoice, Max(AR.PaidPeriod) as CLPaidPeriod 
			From AR WHERE PaidPeriod <= 999900/*@ThruPeriod*/ AND PaidPeriod is not null Group BY AR.WBS1, AR.WBS2,AR.WBS3,AR.Invoice 
		) PaidAR on BilledWBS1 = INWBS1 and BilledWBS2 = INWBS2 and BilledWBS3 = INWBS3 and BilledInvoice = INInvoice
	WHERE (pat.Seq = @PayableSeq or pat.ParentSeq = @PayableSeq) and --match on single invoice or all invoices for a contract
		 (@WBS1 IS NULL or (LedgerAP.BilledWBS1=@WBS1 and LedgerAP.BilledWBS2=@WBS2 and LedgerAP.BilledWBS3=@WBS3 )) and
		  ((LedgerAP.TransType= /*N*/'AP' AND LedgerAP.SubType= 'L') OR (LedgerAP.TransType= /*N*/'PP' AND LedgerAP.SubType <> 'X')) 
			--AND VO.PayTerms= /*N*/'PWP' 
			--AND VO.PaidPeriod > @ThruPeriod AND LedgerAP.Period <= @ThruPeriod --And VO.Company= /*N*/' '  AND VE.Vendor= '0000000001'

	SELECT 
		@billed = MAX(BilledInvoice)
	FROM CCG_PAT_Payable PAT
		INNER JOIN LedgerAP vo ON pat.Vendor=vo.Vendor and pat.Voucher=vo.Voucher 
	WHERE (pat.Seq = @PayableSeq or pat.ParentSeq = @PayableSeq) and --match on single invoice or all invoices for a contract
		 (@WBS1 IS NULL or (BilledWBS1=@WBS1 and BilledWBS2=@WBS2 and BilledWBS3=@WBS3 )) and
		 ((TransType= /*N*/'AP' AND SubType= 'L') OR (TransType= /*N*/'PP' AND SubType <> 'X')) 

	if @status = 1 set @res = 'Paid' 
	if @status > 0 and @status <> 1 set @res = 'Partial' 
	if @status = 0 set @res = 'No'

	return @res
END
GO
