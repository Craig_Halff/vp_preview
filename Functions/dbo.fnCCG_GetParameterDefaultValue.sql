SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   FUNCTION [dbo].[fnCCG_GetParameterDefaultValue]
(
	@pSPName varchar(1000),
	@pParameterName varchar(100),
	@pType bit = 0					-- 0 for Stored Procedure and 1 for Function
)
RETURNS nvarchar(1000)
AS
BEGIN
	declare @vSPtext nvarchar(4000), @str nvarchar(4000)
	declare @idx int, @idx2 int, @idx3 int, @idx4 int, @idx5 int, @idx6 int, @idx7 int, @tries int = 0

	-- Get the text from syscomments
	if @pType = 0
	begin
		SELECT @vSPtext = (SELECT text FROM syscomments WHERE id = object_id(@pSPName) and colid=1 and number = 1)
		Set @vSPtext = SubString(@vSPtext, CharIndex('CREATE PROCEDURE', @vSPtext), 4000)
	end
	else begin
		SELECT @vSPtext = (SELECT text FROM syscomments WHERE id = object_id(@pSPName) and colid=1 and number = 0)
		Set @vSPtext = SubString(@vSPtext, CharIndex('CREATE OR ALTER FUNCTION', @vSPtext), 4000)
	end

	if IsNull(@vSPtext,'') = ''
	begin
		RETURN ''
	end

	while 1=1
	begin
		set @idx = PatIndex('%'+@pParameterName+'%', @vSPText)
		set @vSPText = SUBSTRING(@vSPText, @idx + len(@pParameterName), 4000)

		-- Do we have a winner?
		if ltrim(replace(left(@vSPText,1), char(9),'')) = ''
		begin
			set @vSPText = ltrim(replace(@vSPText, char(9),''))
			set @idx7 = CharIndex('=', @vSPText)

			set @idx = isnull(nullif(CharIndex(',', @vSPText), 0), 4000)
			set @idx2 = isnull(isnull(nullif(PatIndex('%AS['+char(13)+char(10)+']%', @vSPText), 0), nullif(PatIndex('%AS BEGIN%', @vSPText), 0)), 4000)
			set @idx3 = isnull(isnull(nullif(CharIndex(char(13), @vSPText), 0), nullif(CharIndex(char(10), @vSPText), 0)), 4000)
			set @idx4 = isnull(nullif(CharIndex('--', @vSPText), 0), 4000)
			set @idx5 = isnull(nullif(CharIndex(')', @vSPText, @idx7+1), 0), 4000)
			set @idx6 = isnull(nullif(CharIndex('/*', @vSPText, @idx7+3), 0), 4000)
			if @idx2 < @idx set @idx = @idx2
			if @idx3 < @idx set @idx = @idx3
			if @idx4 < @idx set @idx = @idx4
			if @idx5 < @idx set @idx = @idx5
			if @idx6 < @idx set @idx = @idx6

			set @vSPText = rtrim(SUBSTRING(@vSPText, 0, @idx))

			-- Get part after = sign
			set @idx7 = CharIndex('=', @vSPText)
			if @idx7 = 0 begin
				-- No default
				RETURN ''
			end

			set @vSPText = ltrim(rtrim(SUBSTRING(@vSPText, @idx7, 1000)))
			if right(@vSPText, 1) = ')' set @vSPText = rtrim(left(@vSPText, len(@vSPText)-1))
			break
		end
		set @tries += 1
		if @tries > 20 return ''
	end

	return @vSPText
END
GO
