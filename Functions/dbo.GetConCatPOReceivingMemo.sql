SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetConCatPOReceivingMemo]
 (@strMasterPKey varchar(32)) 
  RETURNS Nvarchar(max)  AS
BEGIN

	DECLARE @strConcatMemo Nvarchar(max)
	DECLARE @ReceiptMemo Nvarchar(max)

	  -- Get Memo of current PO receiving
	SET @strConcatMemo = ''  
	declare Receiving cursor for 
	 SELECT Memo FROM ReceiveMaster WHERE MasterPKey = @strMasterPKey 

	  -- Combine multiple receipt memo 

	open Receiving 
	fetch next from Receiving into @ReceiptMemo
	while @@fetch_Status=0
		begin
			if (@ReceiptMemo is not null)
				begin			
					if (@strConcatMemo<>'')
							SET @strConcatMemo = @strConcatMemo + char(10) + char(13) 
					SET @strConcatMemo = @strConcatMemo + @ReceiptMemo
				end
			FETCH NEXT FROM Receiving INTO @ReceiptMemo
		end

	close Receiving 
	deallocate Receiving 
	RETURN @strConcatMemo 

END -- GetConCatTaskName
GO
