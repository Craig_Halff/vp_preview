SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   FUNCTION [dbo].[fnCCG_All_GetTableColumns] (@tableName Nvarchar(200))
RETURNS @T TABLE (ColName varchar(1000), DataType varchar(200), IsNullable varchar(10), MaxLen int,
	IsIdentityColumn varchar(10), IsUnique int, ColDefault varchar(2000))
AS BEGIN
	INSERT INTO @T (ColName, DataType, IsNullable, MaxLen, IsIdentityColumn, IsUnique, ColDefault)
	SELECT ColName, C.DATA_TYPE, C.IS_NULLABLE,
			C.CHARACTER_MAXIMUM_LENGTH as MAXLEN, o.is_identity, o.is_unique, o.ColDefault
		FROM (SELECT 'Column' as objType, obj.name as tableName, '1' as TopLevel, '' as TabName, col.name as ColName, obj.type,
					id_col.is_identity, idx.is_unique, in_col.COLUMN_DEFAULT as ColDefault, idx.index_id
				FROM sys.all_objects obj
					INNER JOIN sys.all_columns col ON col.object_id = obj.object_id
					INNER JOIN INFORMATION_SCHEMA.COLUMNS in_col ON in_col.COLUMN_NAME = col.name and in_col.TABLE_NAME = obj.name
					LEFT JOIN sys.identity_columns id_col ON id_col.object_id = obj.object_id and id_col.column_id = col.column_id
					LEFT JOIN (select idx_col.object_id, idx_col.column_id, idx.is_unique, idx.index_id
						from sys.index_columns idx_col
							INNER JOIN sys.indexes idx ON idx.object_id = idx_col.object_id and idx.index_id = idx_col.index_id
						where idx.is_unique = 1
					) idx ON idx.object_id = obj.object_id and idx.column_id = col.column_id
				WHERE obj.name = @tableName AND obj.type = 'U'
			) o
			LEFT JOIN INFORMATION_SCHEMA.COLUMNS C ON C.TABLE_NAME = o.tableName and C.COLUMN_NAME = o.ColName
		Order By Index_id, TopLevel, o.is_identity desc, ColName
	RETURN
END
GO
