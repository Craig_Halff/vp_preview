SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DLTK$AddBusinessDays](
  @dtFromDate AS datetime,
  @iDaysToAdd int,
  @strCompany AS Nvarchar(14)
)
  RETURNS datetime
BEGIN
  
  DECLARE @dtResultDate datetime
  DECLARE @iStep int = 0

  DECLARE @bitFromDateIsWD bit = 
    CASE 
      WHEN dbo.DLTK$NumWorkingDays(@dtFromDate, @dtFromDate, @strCompany) <> 0
      THEN 1
      ELSE 0
    END

  SET @iDaysToAdd = 
    CASE 
      WHEN (@bitFromDateIsWD = 0 AND @iDaysToAdd > 0) THEN @iDaysToAdd + 1
      WHEN (@bitFromDateIsWD = 0 AND @iDaysToAdd < 0) THEN @iDaysToAdd - 1
      ELSE @iDaysToAdd
    END

  SET @iStep = SIGN(@iDaysToAdd)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	DECLARE @tabNWD TABLE (
		DayOfWeek	int
    UNIQUE(DayOfWeek)
	)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabNWD(
    DayOfWeek
  )
    SELECT
      1 AS DayOfWeek
      FROM CFGNonWorkingDay
      WHERE Company = @strCompany AND SundayInd = 'Y'
    UNION ALL
    SELECT
      2 AS DayOfWeek
      FROM CFGNonWorkingDay
      WHERE Company = @strCompany AND MondayInd = 'Y'
    UNION ALL
    SELECT
      3 AS DayOfWeek
      FROM CFGNonWorkingDay
      WHERE Company = @strCompany AND TuesdayInd = 'Y'
    UNION ALL
    SELECT
      4 AS DayOfWeek
      FROM CFGNonWorkingDay
      WHERE Company = @strCompany AND WednesdayInd = 'Y'
    UNION ALL
    SELECT
      5 AS DayOfWeek
      FROM CFGNonWorkingDay
      WHERE Company = @strCompany AND ThursdayInd = 'Y'
    UNION ALL
    SELECT
      6 AS DayOfWeek
      FROM CFGNonWorkingDay
      WHERE Company = @strCompany AND FridayInd = 'Y'
    UNION ALL
    SELECT
      7 AS DayOfWeek
      FROM CFGNonWorkingDay
      WHERE Company = @strCompany AND SaturdayInd = 'Y'
  ; -- Terminate INSERT @tabNWD

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  WITH GetDateWD AS (
    SELECT 
      @dtFromDate AS DateWD, 
      0 AS DayNo 
    UNION ALL
    SELECT 
      (DateWD + @iStep) AS DateWD, 
      CASE 
        WHEN DATEPART(DW, (DateWD + @iStep)) IN (SELECT DayOfWeek FROM @tabNWD) THEN DayNo
        WHEN EXISTS(SELECT 'X' FROM CFGHoliday WHERE Company = @strCompany AND HolidayDate = (DateWD + @iStep)) THEN DayNo
        ELSE (DayNo + @iStep)
      END AS DayNo
    FROM GetDateWD
    WHERE @iStep * (DayNo - @iDaysToAdd) < 0
  )

  SELECT 
    @dtResultDate = 
      CASE 
        WHEN @iStep > 0 
        THEN MAX(DateWD)
        ELSE MIN(DateWD)
      END
    FROM GetDateWD
    OPTION (MAXRECURSION 0);

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


  RETURN @dtResultDate

END -- fn_DLTK$AddBusinessDays
GO
