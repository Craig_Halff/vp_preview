SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[Greatest](@v1 SQL_VARIANT, @v2 SQL_VARIANT) 
RETURNS SQL_VARIANT AS
/*
Copyright (c) 2018 Central Consulting Group. All rights reserved.
04/16/2018 David Springer
           Return greatest of two or three variables
SELECT dbo.greatest(GETDATE(), GETDATE()-2, GETDATE()+3)
*/
BEGIN
	DECLARE @return SQL_VARIANT
	SELECT TOP 1 @return = VALUE 
	FROM (SELECT @v1 value UNION ALL SELECT @v2) a
	ORDER BY value DESC
	RETURN @return
END


GO
