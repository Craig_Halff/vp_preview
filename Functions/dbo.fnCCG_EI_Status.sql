SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_Status] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS varchar 
AS BEGIN
/*
	Copyright 2016 Central Consulting Group.  All rights reserved.
	select dbo.fnCCG_EI_Status('2003005.xx',' ',' ')
	select dbo.fnCCG_EI_Status('2003005.xx','1PD',' ')
	
*/
	declare @res varchar
	--select @res=Status from PR where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3
	select @res = custbillingstatus from ProjectCustomTabFields where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3
	if isnull(@res,' ') = 'D' set @res = 'C'
	return @res
END
GO
