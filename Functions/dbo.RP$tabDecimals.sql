SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$tabDecimals]
  (@strPlanID varchar(32))
  RETURNS @tabDecimals TABLE
   (HrDecimals smallint,
    QtyDecimals smallint,
    AmtCostDecimals smallint,
    AmtBillDecimals smallint,
    RtCostDecimals smallint,
    RtBillDecimals smallint,
    LabRevDecimals smallint,
    ECURevDecimals smallint)
BEGIN

  /*--------------------------------------------------------------------------------------------------------------------
    This function encapsulates the rules for Decimal Settings in Resource Planning.
    This function will return the following columns:
      HrDecimals: Decimal Setting for Labor Hours.
      QtyDecimals: Decimal Setting for Unit Quantity.
      AmtCostDecimals: Decimal Setting for Cost Amount.
      AmtBillDecimals: Decimal Setting for Billing Amount.
      RtCostDecimals: Decimal Setting for Cost Rate.
      RtBillDecimals: Decimal Setting for Billing Rate.
      LabRevDecimals: Decimal Setting for Labor Revenue.
      ECURevDecimals: Decimal Setting for Expense/Consultant/Unit Revenue.
  */--------------------------------------------------------------------------------------------------------------------

  INSERT @tabDecimals
    SELECT 
      HrDecimals,
      QtyDecimals, 
      CASE WHEN AmtDecimals = -1 THEN CC.DecimalPlaces ELSE AmtDecimals END AS AmtCostDecimals, 
      CASE WHEN AmtDecimals = -1 THEN BC.DecimalPlaces ELSE AmtDecimals END AS AmtBillDecimals, 
      4 AS RtCostDecimals, 
      4 AS RtBillDecimals, 
      CASE WHEN P.LabMultType < 2 
           THEN CASE WHEN AmtDecimals = -1 THEN CC.DecimalPlaces ELSE AmtDecimals END 
           ELSE CASE WHEN AmtDecimals = -1 THEN BC.DecimalPlaces ELSE AmtDecimals END 
           END AS LabRevDecimals, 
      CASE WHEN P.ReimbMethod = 'C' 
           THEN CASE WHEN AmtDecimals = -1 THEN CC.DecimalPlaces ELSE AmtDecimals END 
           ELSE CASE WHEN AmtDecimals = -1 THEN BC.DecimalPlaces ELSE AmtDecimals END 
           END AS ECURevDecimals
      FROM RPPlan AS P, CFGResourcePlanning, FW_CFGCurrency AS CC, FW_CFGCurrency AS BC 
      WHERE P.PlanID = @strPlanID AND CFGResourcePlanning.Company = P.Company AND
            CC.Code = P.CostCurrencyCode AND BC.Code = P.BillingCurrencyCode 
      
  RETURN

END -- fn_RP$tabDecimals
GO
