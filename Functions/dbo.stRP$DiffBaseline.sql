SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$DiffBaseline] (
  @strPlanID varchar(32)
)
  RETURNS bit
BEGIN -- Function stRP$DiffBaseline

  DECLARE @bitDiff bit

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @bitDiff = CASE WHEN SUM(DX) > 0 THEN 1 ELSE 0 END FROM ( /* Z */

    /* Labor TPD */
    SELECT CASE WHEN EXISTS ( /* DX */
      SELECT MIN(VorN) AS VorN FROM ( /* LTPD */
        SELECT 
          0 AS VorN,
          TaskID, AssignmentID, StartDate, EndDate, PeriodHrs, CostRate, BillingRate
          FROM PNBaselineLabor AS N WHERE PlanID = @strPlanID
        UNION ALL
        SELECT
          1 AS VorN,
          TaskID, AssignmentID, StartDate, EndDate, PeriodHrs, CostRate, BillingRate
          FROM RPBaselineLabor AS V WHERE PlanID = @strPlanID
        ) AS LTPD
        GROUP BY
          TaskID, AssignmentID, StartDate, EndDate, PeriodHrs, CostRate, BillingRate
        HAVING COUNT(*) = 1
      ) THEN 1 ELSE 0 END AS DX

    UNION ALL /* Expense TPD */
    SELECT CASE WHEN EXISTS ( /* DX */
      SELECT MIN(VorN) AS VorN FROM ( /* ETPD */
        SELECT 
          0 AS VorN,
          TaskID, ExpenseID, StartDate, EndDate, PeriodCost, PeriodBill
          FROM PNBaselineExpenses AS N WHERE PlanID = @strPlanID
        UNION ALL
        SELECT
          1 AS VorN,
          TaskID, ExpenseID, StartDate, EndDate, PeriodCost, PeriodBill
          FROM RPBaselineExpenses AS V WHERE PlanID = @strPlanID
        ) AS ETPD
        GROUP BY
          TaskID, ExpenseID, StartDate, EndDate, PeriodCost, PeriodBill
        HAVING COUNT(*) = 1
      ) THEN 1 ELSE 0 END AS DX

    UNION ALL /* Consultant TPD */
    SELECT CASE WHEN EXISTS ( /* DX */
      SELECT MIN(VorN) AS VorN FROM ( /* CTPD */
        SELECT 
            0 AS VorN,
            TaskID, ConsultantID, StartDate, EndDate, PeriodCost, PeriodBill
            FROM PNBaselineConsultant AS N WHERE PlanID = @strPlanID
          UNION ALL
          SELECT
            1 AS VorN,
            TaskID, ConsultantID, StartDate, EndDate, PeriodCost, PeriodBill
            FROM RPBaselineConsultant AS V WHERE PlanID = @strPlanID
          ) AS CTPD
          GROUP BY
            TaskID, ConsultantID, StartDate, EndDate, PeriodCost, PeriodBill
          HAVING COUNT(*) = 1
      ) THEN 1 ELSE 0 END AS DX

  ) AS Z

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN @bitDiff

END -- stRP$DiffBaseline
GO
