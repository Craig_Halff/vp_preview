SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$rptTaskJTDExpCon]
  (@strPlanID varchar(32),
   @dtETCDate datetime,
   @strCommitment varchar(1),
   @strExcludeUNFlg varchar(1) = 'N',
   @strIncludeUnmapped varchar(1) = 'Y',
   @strECType varchar(1),
   @ReportAtBillingInBillingCurr varchar(1) = 'N')
  RETURNS @taskJTDExpCon TABLE
   (planID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    StartDate datetime,
    JTDCostCurrencyCode Nvarchar(3) COLLATE database_default,
    JTDBillCurrencyCode Nvarchar(3) COLLATE database_default,    
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4),
    PostedFlg smallint)

BEGIN --outer

 INSERT @taskJTDExpCon
  SELECT PlanID,
	   TaskID,
       StartDate, 
	   Max(JTDCostCurrencyCode) as JTDCostCurrencyCode,
       Max(JTDBillCurrencyCode) as JTDBillCurrencyCode,
	   PeriodCost=ROUND(SUM(ISNULL(PeriodCost, 0)), 2),
	   PeriodBill=ROUND(SUM(ISNULL(PeriodBill, 0)), 2),
	   PostedFlg=SIGN(MIN(PostedFlg) + MAX(PostedFlg))
  FROM (
    SELECT X.PlanID AS PlanID, 
                   X.TaskID AS TaskID,
                   TransDate AS StartDate, 
   		           Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		           Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
                   SUM(AmountProjectCurrency) AS PeriodCost, 
                   SUM(BillExt) AS PeriodBill, 
                   1 AS PostedFlg 
    FROM LedgerAR AS Ledger 
    INNER JOIN PR on PR.WBS1=Ledger.WBS1 and PR.WBS2='' and PR.WBS3=''
    INNER JOIN RPTask AS X ON (Ledger.WBS1 = X.WBS1 AND X.PlanID = @strPlanID AND Ledger.ProjectCost = 'Y') 
    INNER JOIN RPWBSLevelFormat AS F ON X.PlanID = F.PlanID AND X.WBSType = F.WBSType AND F.WBSMatch = 'Y'
    INNER JOIN CA ON Ledger.Account = CA.Account
    WHERE Ledger.WBS2 LIKE (ISNULL(X.WBS2, '%')) AND Ledger.WBS3 LIKE (ISNULL(X.WBS3, '%')) 
                   AND (
                   (@strECType='E' AND CA.Type IN (5, 7))
                   OR
                   (@strECType='C' AND CA.Type IN (6, 8))
                   )
                   AND Ledger.TransDate <= @dtETCDate AND Ledger.TransType != (CASE WHEN @strExcludeUNFlg = 'Y' THEN 'UN' ELSE '*' END)
    GROUP BY Ledger.TransDate, X.TaskID, X.PlanID,PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end
    UNION ALL SELECT X.PlanID AS PlanID, 
                   X.TaskID AS TaskID,
                   TransDate AS StartDate, 
   		           Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		           Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
                   SUM(AmountProjectCurrency) AS PeriodCost, 
                   SUM(BillExt) AS PeriodBill, 
                   1 AS PostedFlg 
    FROM LedgerAP AS Ledger 
    INNER JOIN PR on PR.WBS1=Ledger.WBS1 and PR.WBS2='' and PR.WBS3=''
    INNER JOIN RPTask AS X ON (Ledger.WBS1 = X.WBS1 AND X.PlanID = @strPlanID AND Ledger.ProjectCost = 'Y') 
    INNER JOIN RPWBSLevelFormat AS F ON X.PlanID = F.PlanID AND X.WBSType = F.WBSType AND F.WBSMatch = 'Y'
    INNER JOIN CA ON Ledger.Account = CA.Account
    WHERE Ledger.WBS2 LIKE (ISNULL(X.WBS2, '%')) AND Ledger.WBS3 LIKE (ISNULL(X.WBS3, '%')) 
                   AND (
                   (@strECType='E' AND CA.Type IN (5, 7))
                   OR
                   (@strECType='C' AND CA.Type IN (6, 8))
                   )
                   AND Ledger.TransDate <= @dtETCDate AND Ledger.TransType != (CASE WHEN @strExcludeUNFlg = 'Y' THEN 'UN' ELSE '*' END)
    GROUP BY Ledger.TransDate, X.TaskID, X.PlanID,PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end
    UNION ALL SELECT X.PlanID AS PlanID, 
                   X.TaskID AS TaskID,
                   TransDate AS StartDate, 
   		           Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		           Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
                   SUM(AmountProjectCurrency) AS PeriodCost, 
                   SUM(BillExt) AS PeriodBill, 
                   1 AS PostedFlg 
    FROM LedgerEX AS Ledger 
    INNER JOIN PR on PR.WBS1=Ledger.WBS1 and PR.WBS2='' and PR.WBS3=''
    INNER JOIN RPTask AS X ON (Ledger.WBS1 = X.WBS1 AND X.PlanID = @strPlanID AND Ledger.ProjectCost = 'Y') 
    INNER JOIN RPWBSLevelFormat AS F ON X.PlanID = F.PlanID AND X.WBSType = F.WBSType AND F.WBSMatch = 'Y'
    INNER JOIN CA ON Ledger.Account = CA.Account
    WHERE Ledger.WBS2 LIKE (ISNULL(X.WBS2, '%')) AND Ledger.WBS3 LIKE (ISNULL(X.WBS3, '%')) 
                   AND (
                   (@strECType='E' AND CA.Type IN (5, 7))
                   OR
                   (@strECType='C' AND CA.Type IN (6, 8))
                   ) 
                   AND Ledger.TransDate <= @dtETCDate AND Ledger.TransType != (CASE WHEN @strExcludeUNFlg = 'Y' THEN 'UN' ELSE '*' END)
    GROUP BY Ledger.TransDate, X.TaskID, X.PlanID,PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end
    UNION ALL SELECT X.PlanID AS PlanID, 
                   X.TaskID AS TaskID,
                   TransDate AS StartDate, 
   		           Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		           Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
                   SUM(AmountProjectCurrency) AS PeriodCost, 
                   SUM(BillExt) AS PeriodBill, 
                   1 AS PostedFlg 
    FROM LedgerMISC AS Ledger 
    INNER JOIN PR on PR.WBS1=Ledger.WBS1 and PR.WBS2='' and PR.WBS3=''
    INNER JOIN RPTask AS X ON (Ledger.WBS1 = X.WBS1 AND X.PlanID = @strPlanID AND Ledger.ProjectCost = 'Y') 
    INNER JOIN RPWBSLevelFormat AS F ON X.PlanID = F.PlanID AND X.WBSType = F.WBSType AND F.WBSMatch = 'Y'
    INNER JOIN CA ON Ledger.Account = CA.Account
    WHERE Ledger.WBS2 LIKE (ISNULL(X.WBS2, '%')) AND Ledger.WBS3 LIKE (ISNULL(X.WBS3, '%')) 
                   AND (
                   (@strECType='E' AND CA.Type IN (5, 7))
                   OR
                   (@strECType='C' AND CA.Type IN (6, 8))
                   )
                   AND Ledger.TransDate <= @dtETCDate AND Ledger.TransType != (CASE WHEN @strExcludeUNFlg = 'Y' THEN 'UN' ELSE '*' END)
    GROUP BY Ledger.TransDate, X.TaskID, X.PlanID,PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end
    UNION ALL SELECT X.PlanID AS PlanID, 
                   X.TaskID AS TaskID, 
                   POM.OrderDate AS StartDate, 
   		           Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		           Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
                   SUM(AmountProjectCurrency) AS PeriodCost, 
                   SUM(BillExt) AS PeriodBill, 
                   -1 AS PostedFlg 
    FROM POCommitment AS POC 
    INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL) 
    INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey 
    INNER JOIN PR on PR.WBS1=POC.WBS1 and PR.WBS2='' and PR.WBS3=''
    INNER JOIN RPTask AS X ON (POC.WBS1 = X.WBS1 AND X.PlanID = @strPlanID)
    INNER JOIN RPWBSLevelFormat AS F ON X.PlanID = F.PlanID AND X.WBSType = F.WBSType AND F.WBSMatch = 'Y' 
    INNER JOIN CA ON POC.Account = CA.Account
    WHERE @strCommitment='Y' AND POC.WBS2 LIKE (ISNULL(X.WBS2, '%')) AND POC.WBS3 LIKE (ISNULL(X.WBS3, '%')) 
                    AND (
                    (@strECType='E' AND CA.Type IN (5, 7))
                    OR
                    (@strECType='C' AND CA.Type IN (6, 8))
                    )
                    AND POM.OrderDate <= @dtETCDate AND AmountProjectCurrency != 0 AND BillExt != 0 
    GROUP BY POM.OrderDate, X.TaskID, X.PlanID,PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end
    UNION ALL SELECT X.PlanID AS PlanID, 
                    X.TaskID AS TaskID, 
                    POCOM.OrderDate AS StartDate, 
   		            Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		            Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
                    SUM(AmountProjectCurrency) AS PeriodCost, 
                    SUM(BillExt) AS PeriodBill, 
                    -1 AS PostedFlg 
    FROM POCommitment AS POC 
    INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL) 
    INNER JOIN POCOMaster AS POCOM ON POD.COPKey = POCOM.PKey 
    INNER JOIN POMaster AS POM ON POCOM.MasterPKey = POM.MasterPKey 
    INNER JOIN PR on PR.WBS1=POC.WBS1 and PR.WBS2='' and PR.WBS3=''
    INNER JOIN RPTask AS X ON (POC.WBS1 = X.WBS1 AND X.PlanID = @strPlanID) 
    INNER JOIN RPWBSLevelFormat AS F ON X.PlanID = F.PlanID AND X.WBSType = F.WBSType AND F.WBSMatch = 'Y' 
    INNER JOIN CA ON POC.Account = CA.Account
    WHERE @strCommitment='Y' AND POC.WBS2 LIKE (ISNULL(X.WBS2, '%')) AND POC.WBS3 LIKE (ISNULL(X.WBS3, '%')) 
                    AND (
                    (@strECType='E' AND CA.Type IN (5, 7))
                    OR
                    (@strECType='C' AND CA.Type IN (6, 8))
                    )
                    AND POCOM.OrderDate <= @dtETCDate  AND AmountProjectCurrency != 0 AND BillExt != 0 
    GROUP BY POCOM.OrderDate, X.TaskID, X.PlanID,PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end 
             ) X
  GROUP BY PlanID,TaskID, StartDate, JTDCostCurrencyCode, JTDBillCurrencyCode

  if @strIncludeUnmapped='Y'
  begin

  DECLARE @strTaskID varchar(32) 
  DECLARE @strOutlineNumber varchar(255) 

  DECLARE csrTask CURSOR FOR 
    SELECT DISTINCT T.TaskID, T.OutlineNumber 
    FROM RPTask AS T LEFT JOIN @taskJTDExpCon  AS TPD ON T.TaskID = TPD.TaskID 
    WHERE T.PlanID = @strPlanID  AND TPD.TaskID IS NULL
    ORDER BY T.OutlineNumber DESC 

  OPEN csrTask 

  FETCH NEXT FROM csrTask INTO @strTaskID, @strOutlineNumber 

  WHILE (@@FETCH_STATUS = 0) 
    BEGIN --while begin

    INSERT @taskJTDExpCon 
    SELECT @strPlanID AS PlanID,
        @strTaskID AS TaskID, 
        StartDate AS StartDate, 
	    Max(JTDCostCurrencyCode) as JTDCostCurrencyCode,
        Max(JTDBillCurrencyCode) as JTDBillCurrencyCode,
        ROUND(SUM(ISNULL(PeriodCost, 0)), 2) AS PeriodCost, 
        ROUND(SUM(ISNULL(PeriodBill, 0)), 2) AS PeriodBill, 
        SIGN(MIN(ISNULL(PostedFlg, 0)) + MAX(ISNULL(PostedFlg, 0))) AS PostedFlg 
    FROM 
      (SELECT @strTaskID AS TaskID, 
        TPD.StartDate, 
	    Max(TPD.JTDCostCurrencyCode) as JTDCostCurrencyCode,
        Max(TPD.JTDBillCurrencyCode) as JTDBillCurrencyCode,
        ROUND(SUM(ISNULL(TPD.PeriodCost, 0)), 2) AS PeriodCost, 
        ROUND(SUM(ISNULL(TPD.PeriodBill, 0)), 2) AS PeriodBill, 
        SIGN(MIN(ISNULL(TPD.PostedFlg, 0)) + MAX(ISNULL(TPD.PostedFlg, 0))) AS PostedFlg 
      FROM RPTask AS CT 
      INNER JOIN @taskJTDExpCon AS TPD ON TPD.TaskID = CT.TaskID 
      WHERE CT.PlanID = @strPlanID AND CT.ParentOutlineNumber = @strOutlineNumber 
      GROUP BY TPD.StartDate, TPD.JTDCostCurrencyCode, TPD.JTDBillCurrencyCode 
      UNION ALL SELECT @strTaskID AS TaskID, 
        TransDate AS StartDate,
	    Max(JTDCostCurrencyCode) as JTDCostCurrencyCode,
        Max(JTDBillCurrencyCode) as JTDBillCurrencyCode, 
        ROUND(SUM(ISNULL(PeriodCost, 0)), 2) AS PeriodCost, 
        ROUND(SUM(ISNULL(PeriodBill, 0)), 2) AS PeriodBill, 
        SIGN(MIN(ISNULL(PostedFlg, 0)) + MAX(ISNULL(PostedFlg, 0))) AS PostedFlg 
      FROM dbo.RP$rptExpConJTD(@strPlanID,@dtETCDate,@strTaskID,@strCommitment,'N',@strExcludeUNFlg,@strECType,@ReportAtBillingInBillingCurr)
      GROUP BY TransDate, JTDCostCurrencyCode, JTDBillCurrencyCode) AS XTPD 
    GROUP BY StartDate, JTDCostCurrencyCode, JTDBillCurrencyCode 
   
      FETCH NEXT FROM csrTask INTO @strTaskID, @strOutlineNumber
    END  --while end
  
  CLOSE csrTask
  DEALLOCATE csrTask
  END --  if @strIncludeUnmapped='Y' begin/end

RETURN
END --outer

GO
