SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DLTK$IntervalEnd]
  (@dtCurrDate AS datetime, 
   @strScale AS char,
   @intWkEndDay AS int)
  RETURNS datetime
BEGIN

  IF (@strScale = 'b')
    BEGIN
      DECLARE @dtDatum datetime
      SET @dtDatum = dbo.DLTK$NextDay('19000101', @intWkEndDay)
    END

  -- When period scale and periods per year is not 13 then use months, else use 28 day periods
  If (@strScale = 'p')
	BEGIN
	  IF ((SELECT PeriodsPerYear FROM FW_CFGSystem) <> 13) 
		SET @strScale = 'm'
	END

  RETURN(
  
    CASE
    
      -- Week.
      WHEN @strScale = 'w' THEN dbo.DLTK$NextDay(@dtCurrDate, @intWkEndDay)
      
      -- Bi-Weekly. Must offset by muitiple of 14 days from the end-day of the first week of SQL Sever Calendar.
      WHEN @strScale = 'b' THEN DATEADD(wk, CEILING(DATEDIFF(dd, @dtDatum, @dtCurrDate) / 14.0) * 2.0, @dtDatum)
      
      -- Semi-Monthly.
      WHEN @strScale = 's' THEN 
      
        CASE
          WHEN DAY(@dtCurrDate) <= 15 
            THEN CONVERT(datetime, CONVERT(varchar, YEAR(@dtCurrDate)) + '-' + CONVERT(varchar, MONTH(@dtCurrDate)) + '-15')
            ELSE DATEADD(day, -1, DATEADD(month, 1, CONVERT(datetime, CONVERT(varchar, YEAR(@dtCurrDate)) + '-' + CONVERT(varchar, MONTH(@dtCurrDate)) + '-01')))
        END -- End Case
      
      -- Month.
      WHEN @strScale = 'm' 
        THEN DATEADD(day, -1, DATEADD(month, 1, CONVERT(datetime, CONVERT(varchar, YEAR(@dtCurrDate)) + '-' + CONVERT(varchar, MONTH(@dtCurrDate)) + '-01')))

      -- Quarter.
      WHEN @strScale = 'q' 
        THEN DATEADD(day, -1, DATEADD(month, 1, CONVERT(datetime, CONVERT(varchar, YEAR(@dtCurrDate)) + '-' + CONVERT(varchar, (DATEPART(q, @dtCurrDate) * 3)) + '-01')))

      -- Year.
      WHEN @strScale = 'y' 
        THEN DATEADD(day, -1, CONVERT(datetime, CONVERT(varchar, (YEAR(@dtCurrDate) + 1)) + '-01-01'))

      -- Fiscal period with periods per year = 13 then 28 day periods, which would end 27 days past the start date provided
      WHEN @strScale = 'p'
        THEN DATEADD(day, 27, @dtCurrDate)

    END -- End Case
  
  ) -- End Return

END -- fn_DLTK$IntervalEnd
GO
