SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ngRP$tabProjectViewPeriod](
  @strRowID Nvarchar(255),
  @strMultiScales varchar(14), /* e.g. "x,20160601,2,m" */
  @strMode varchar(1) /* S = Self, C = Children */
)
  RETURNS @tabProjectViewPeriod TABLE (
    SeqID int,
    RowID Nvarchar(255),
    AssignmentID Nvarchar(32) COLLATE database_default,
    TaskID varchar( 32),
    StartDate datetime,
    PeriodHrs decimal(19,4),
    NumWorkingDays int,
    ETC varchar(1)
)

BEGIN


-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @strCompany Nvarchar(14)
  DECLARE @strResourceID Nvarchar(20)
  DECLARE @strResourceName Nvarchar(255)
  DECLARE @strGenericResourceID Nvarchar(20)
  DECLARE @strTaskID varchar(32)
  DECLARE @strResourceType varchar(1)
  DECLARE @strIDPrefix Nvarchar(255)
  DECLARE @strPlanID varchar(32)
  DECLARE @strPeriodScale varchar(1)

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
  DECLARE @dtScopeStartDate datetime
  DECLARE @dtScopeEndDate datetime
  DECLARE @dtScopeBeforeDate datetime
  DECLARE @dtScopeAfterDate datetime
  DECLARE @dtMinEndDate datetime
  DECLARE @dtStart datetime
  DECLARE @dtEnd datetime
 
  DECLARE @intHrDecimals int /* CFGRMSettings.HrDecimals */
  DECLARE @intTaskCount int 
  DECLARE @intOutlineLevel int
  DECLARE @intWorkingHoursPerDay int

  DECLARE @bitIsLeafTask bit
 
  -- Declare Temp tables.

  DECLARE @tabCalendar TABLE (
    SeqID int,
    StartDate	datetime,
    EndDate	datetime,
    PeriodScale	varchar(1) COLLATE database_default,
    NumWorkingDays int,
    ETC varchar(1)
    UNIQUE(StartDate,EndDate,ETC)
  )

  DECLARE @tabAssignment TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    AT_OutlineNumber varchar(255) COLLATE database_default,
    AT_ChargeType varchar(1) COLLATE database_default
    UNIQUE(PlanID, TaskID, AssignmentID)
  )

  DECLARE @tabSelectedETC TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    AT_OutlineNumber varchar(255) COLLATE database_default,
    AT_ChargeType varchar(1) COLLATE database_default,
    StartDate datetime,	
    EndDate datetime,
    PeriodHrs decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, AT_OutlineNumber, AT_ChargeType, StartDate, EndDate)
  )

  DECLARE @tabPLabTPD TABLE (
    RowID int identity,
    SeqID int,
    TimePhaseID varchar(32) COLLATE database_default,
    CIStartDate datetime, 
    CIEndDate datetime, 
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    AT_OutlineNumber varchar(255) COLLATE database_default,
    AT_ChargeType varchar(1) COLLATE database_default,
    StartDate datetime, 
    EndDate datetime, 
    PeriodHrs decimal(19,4), 
    BillableHrs decimal(19,4), 
    CINumWorkingDays int,
    ETC varchar(1)
    UNIQUE (RowID, SeqID, TimePhaseID, CIStartDate, AssignmentID, PlanID, TaskID, AT_OutlineNumber, AT_ChargeType, StartDate, EndDate, ETC)
  )

  DECLARE @tabRoundedTPD TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    RowNo int,
    RndValue decimal(19,4)
    UNIQUE (RowNo, TimePhaseID)
  )

  DECLARE @tabLD TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    PeriodHrs decimal(19,4),
    TransDate datetime
    UNIQUE (PlanID,OutlineNumber,TransDate)
  )
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Set Company to non-Multi-Company.
  
  SELECT @strCompany = dbo.GetActiveCompany()

  -- Get RM Settings.  
 SELECT 
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE()))),
    @intHrDecimals = HrDecimals
    FROM CFGRMSettings

  -- Extract @strScale from @strMultiScales

  SET @strPeriodScale = SUBSTRING(@strMultiScales, 1, 1)

  -- Set Dates
  
  SELECT @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)

  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. For an Assignment row, @strRowID = 'E~<RPAssignment.ResourceID>|<RPTask.TaskID>
  --   2. For an Assignment row, @strRowID = 'G~<RPAssignment.GenericResourceID>|<RPTask.TaskID>'
  --   3. For a WBS row, @strRowID = '|<RPTask.TaskID>'

  SET @strResourceType = 
    CASE 
      WHEN CHARINDEX('~', @strRowID) = 0 
      THEN ''
      ELSE SUBSTRING(@strRowID, 1, 1)
    END

  SET @strIDPrefix = LEFT(@strRowID, CHARINDEX('|', @strRowID) - 1)

  IF (@strResourceType = 'E')
    BEGIN
      SET @strResourceID = REPLACE(@strIDPrefix, 'E~', '')
      SET @strGenericResourceID = NULL
    END
  ELSE IF (@strResourceType = 'G')
    BEGIN
      SET @strGenericResourceID = REPLACE(@strIDPrefix, 'G~', '')
      SET @strResourceID = NULL
    END
  ELSE
    BEGIN
      SET @strResourceID = NULL
      SET @strGenericResourceID = NULL
    END

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

  -- Setting other data.

  SELECT 
    @strPlanID = PT.PlanID,
    @dtStart = TT.StartDate,
    @dtEnd = TT.EndDate,
    @bitIsLeafTask =
      CASE
        WHEN EXISTS(
          SELECT 'X' 
            FROM RPTask AS CT 
            WHERE 
              CT.PlanID = PT.PlanID 
            AND PT.OutlineNumber = CT.ParentOutlineNumber
        )
        THEN 0
        ELSE 1
      END
    FROM RPTask AS PT
      INNER JOIN RPTask AS TT ON PT.PlanID = TT.PlanID AND TT.OutlineLevel = 0
    WHERE PT.TaskID = @strTaskID

  -- Calculate calendar

  INSERT @tabCalendar(
    SeqID,
    StartDate,
    EndDate,
    PeriodScale,
    NumWorkingDays,
    ETC
  )	
    SELECT
      SeqID, 
      StartDate,
      EndDate,
      Scale,
      NumWorkingDays,
      'N' 
      FROM dbo.ngRP$tabCalendarMultiScales(CONVERT(varchar(8),@dtStart,112),CONVERT(varchar(8),@dtEnd,112), @strMultiScales)

  SELECT
    @dtScopeStartDate = MIN(StartDate),
    @dtScopeEndDate = MAX(EndDate),
    @dtMinEndDate = MIN(EndDate)
    FROM @tabCalendar

  -- Insert one special calendar row with StartDate = ETCDate 
  IF (@dtETCDate >= @dtScopeStartDate AND @dtETCDate <= @dtScopeEndDate)
    BEGIN

      INSERT @tabCalendar(
        SeqID,
        StartDate,
        EndDate,
        PeriodScale,
        NumWorkingDays,
        ETC
      )	
        SELECT
          SeqID, 
          @dtETCDate,
          EndDate,
          @strPeriodScale,
          dbo.DLTK$NumWorkingDays(@dtETCDate, EndDate, @strCompany),
          'Y'
        FROM @tabCalendar where @dtETCDate >= StartDate AND @dtETCDate <= EndDate
    END
    
  SELECT @dtScopeBeforeDate = DATEADD(DAY, -1, @dtScopeStartDate)
  SELECT @dtScopeAfterDate = DATEADD(DAY, 1, @dtScopeEndDate)

  SELECT @intWorkingHoursPerDay = 8

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strMode = 'C')
    BEGIN

      INSERT @tabAssignment(
        PlanID,
        TaskID,
        AssignmentID,
        StartDate,
        EndDate,
        AT_OutlineNumber,
        AT_ChargeType
      )
        SELECT DISTINCT
          A.PlanID,
          A.TaskID,
          A.AssignmentID,
          A.StartDate,
          A.EndDate,
          AT.OutlineNumber AS AT_OutlineNumber,
          AT.ChargeType AS AT_ChargeType
          FROM RPAssignment AS A
            INNER JOIN RPPlan AS P ON P.PlanID = A.PlanID
            INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
            INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID AND AT.OutlineNumber LIKE PT.OutlineNumber + '%'
            LEFT JOIN RPTask AS CT ON A.PlanID = CT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber AND AT.OutlineNumber LIKE CT.OutlineNumber + '%'
          WHERE
            A.PlanID = @strPlanID AND PT.TaskID = @strTaskID

    END /* END IF (@strMode = 'C') */

  ELSE IF (@strMode = 'S')
    BEGIN

      IF (DATALENGTH(@strResourceType) = 0) /* @strRowID is pointing to a WBS row */
        BEGIN

          INSERT @tabAssignment(
            PlanID,
            TaskID,
            AssignmentID,
            StartDate,
            EndDate,
            AT_OutlineNumber,
            AT_ChargeType
          )
            SELECT DISTINCT
              A.PlanID,
              A.TaskID,
              A.AssignmentID,
              A.StartDate,
              A.EndDate,
              AT.OutlineNumber AS AT_OutlineNumber,
              AT.ChargeType AS AT_ChargeType
              FROM RPAssignment AS A
                INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
                INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID AND AT.OutlineNumber LIKE PT.OutlineNumber + '%'
              WHERE
                A.PlanID = @strPlanID AND PT.TaskID = @strTaskID

        END /* END IF (DATALENGTH(@strResourceType) = 0) */

      ELSE /* @strRowID is pointing to an Assignment row */
        BEGIN

          INSERT @tabAssignment(
            PlanID,
            TaskID,
            AssignmentID,
            StartDate,
            EndDate,
            AT_OutlineNumber,
            AT_ChargeType
          )
            SELECT DISTINCT
              A.PlanID,
              A.TaskID,
              A.AssignmentID,
              A.StartDate,
              A.EndDate,
              AT.OutlineNumber AS AT_OutlineNumber,
              AT.ChargeType AS AT_ChargeType
              FROM RPAssignment AS A
                INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
              WHERE
                A.PlanID = @strPlanID AND A.TaskID = @strTaskID AND
                ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') AND 
                ISNULL(A.GenericResourceID, '|') = ISNULL( @strGenericResourceID, '|')

        END /* END ELSE */

    END /* END ELSE IF (@strMode = 'S') */

    SELECT @intTaskCount = COUNT(DISTINCT TaskID) FROM @tabAssignment

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collecting Labor Time-Phased Data to be used in subsequent calculations.
  -- The TPD records come from only the Assignments included in @tabAssignment.

  -- Calculate Selected ETC

  INSERT @tabSelectedETC(
    TimePhaseID,
    PlanID,
    TaskID,
    AssignmentID,
    AT_OutlineNumber,
    AT_ChargeType,
    StartDate,
    EndDate,
    PeriodHrs
  )
    SELECT  /* Keep only TPD in the Selected ETC range */
      STPD.TimePhaseID AS TimePhaseID,
      STPD.PlanID AS PlanID,
      STPD.TaskID AS TaskID,
      STPD.AssignmentID AS AssignmentID,
      STPD.AT_OutlineNumber AS AT_OutlineNumber,
      STPD.AT_ChargeType AS AT_ChargeType,
      STPD.StartDate AS StartDate,
      STPD.EndDate AS EndDate,
      STPD.PeriodHrs AS PeriodHrs
      FROM (
        SELECT /* Select only TPD in the ETC range */
          ETPD.TimePhaseID AS TimePhaseID,
          ETPD.PlanID AS PlanID,
          ETPD.TaskID AS TaskID,
          ETPD.AssignmentID AS AssignmentID,
          ETPD.AT_OutlineNumber AS AT_OutlineNumber,
          ETPD.AT_ChargeType AS AT_ChargeType,
          ETPD.StartDate AS StartDate,
          ETPD.EndDate AS EndDate,
          ETPD.PeriodHrs AS PeriodHrs
          FROM (
            SELECT /* TPD for Assignment rows in @tabAssignment */
              TPD.TimePhaseID AS TimePhaseID,
              TPD.PlanID AS PlanID,
              TPD.TaskID AS TaskID,
              TPD.AssignmentID AS AssignmentID,
              A.AT_OutlineNumber AS AT_OutlineNumber,
              A.AT_ChargeType AS AT_ChargeType,
              TPD.StartDate AS StartDate,
              TPD.EndDate AS EndDate,
              TPD.PeriodHrs AS PeriodHrs
              FROM @tabAssignment AS A
                INNER JOIN RPPlannedLabor AS TPD ON
                  A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID AND
                  TPD.PeriodHrs <> 0 AND TPD.AssignmentID IS NOT NULL 
          ) AS ETPD
          WHERE ETPD.PeriodHrs <> 0
      ) AS STPD

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Split up TPD in @tabSelectedETC to match with Calendar Intervals.

  INSERT @tabPLabTPD (
    SeqID,
    TimePhaseID,
    CIStartDate,
    CIEndDate,
    PlanID, 
    TaskID,
    AssignmentID,
    AT_OutlineNumber,
    AT_ChargeType,
    StartDate, 
    EndDate, 
    PeriodHrs,
    BillableHrs,
    CINumWorkingDays,
    ETC
   )
     SELECT
      SeqID AS SeqID,
      TimePhaseID AS TimePhaseID,
      CIStartDate AS CIStartDate,
      CIEndDate AS CIEndDate,
      PlanID AS PlanID, 
      TaskID AS TaskID,
      AssignmentID AS AssignmentID,
      AT_OutlineNumber AS AT_OutlineNumber,
      AT_ChargeType AS AT_ChargeType,
      StartDate AS StartDate, 
      EndDate AS EndDate, 
      ISNULL(PeriodHrs, 0) AS PeriodHrs,
      ISNULL(CASE WHEN AT_ChargeType = 'R' THEN PeriodHrs ELSE 0 END, 0) AS BillableHrs,
      CINumWorkingDays,
      ETC
    FROM (
      SELECT -- For Assignment Rows.
        CI.SeqID AS SeqID,
        CI.StartDate AS CIStartDate, 
        CI.EndDate AS CIEndDate, 
        TPD.TimePhaseID AS TimePhaseID,
        TPD.PlanID AS PlanID, 
        TPD.TaskID AS TaskID,
        TPD.AssignmentID AS AssignmentID,
        TPD.AT_OutlineNumber AS AT_OutlineNumber,
        TPD.AT_ChargeType AS AT_ChargeType,
        CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
        CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
        CASE 
          WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
          THEN TPD.PeriodHrs * 
            dbo.DLTK$ProrateRatio(
              CASE 
                WHEN TPD.StartDate > CI.StartDate 
                THEN TPD.StartDate 
                ELSE CI.StartDate 
              END, 
              CASE 
                WHEN TPD.EndDate < CI.EndDate 
                THEN TPD.EndDate 
                ELSE CI.EndDate 
              END, 
              TPD.StartDate, TPD.EndDate,
              @strCompany)             
          ELSE PeriodHrs 
        END AS PeriodHrs,
        CI.NumWorkingDays AS CINumWorkingDays,
        CI.ETC AS ETC
        FROM @tabCalendar AS CI 
          INNER JOIN @tabSelectedETC AS TPD 
            ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
    ) AS X

-- Once everything is done run the periodhrs through the rounding function and update with the rounded values.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabRoundedTPD(
    TimePhaseID,
    RowNo,
    RndValue
  )
    SELECT 
      TimePhaseID,
      H.RowNo, 
      H.RndValue
    FROM (
      SELECT 
        TimePhaseID,
        CONVERT(xml,
          '<root>' + (
            SELECT 
              ISNULL(PeriodHrs, 0) AS UnRndValue, 
              SeqID AS RowNo 
              FROM @tabPLabTPD P 
              WHERE P.TimePhaseID = ETC.TimePhaseID AND P.ETC = 'N' AND P.PeriodHrs <> 0 
              FOR XML RAW
          ) + '</root>') AS HrsXML
        FROM (SELECT DISTINCT TimePhaseID FROM @tabSelectedETC) AS ETC
      ) AS A 
      CROSS APPLY dbo.DLTK$tabRound(HrsXML,@intHrDecimals) AS H 
      WHERE A.HrsXML IS NOT NULL
                  
  UPDATE @tabPLabTPD SET 
    PeriodHrs = #R1.RndValue
  FROM @tabPLabTPD P 
    INNER JOIN @tabRoundedTPD AS #R1
    ON P.TimePhaseID = #R1.TimePhaseID AND P.SeqID = #R1.RowNo

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- After rouding process was applied, PeriodHrs in a calendar period could be changed.
  -- For example, when scale is "Daily" and the total Hrs for a TPD chunk is small, we could get zero and one as PeriodHrs.
  -- The rouding process could redistribute the zero'es and one's around in a different pattern.
  -- Therefore, we need to recalculate PeriodHrs for the ETC row.
  -- We need to re-porate PeriodHrs using NumWorkingDays between Start/End Dates and cannot rely on NumWorkingDays of the Calendar Periods.
  -- Note that the Start/End Dates range of TPD could be less than Start/End Dates range of a Calendar Period.

  UPDATE @tabPLabTPD SET
    PeriodHrs = ROUND(XETC.PeriodHrs, @intHrDecimals)

  FROM @tabPLabTPD TPD, (
    SELECT 
      ROUND(
        NonETC.PeriodHrs *
        CASE WHEN NonETC.NumWorkingDays = 0 THEN 0 
        ELSE 
          CONVERT(decimal(19, 4), ETC.NumWorkingDays) / CONVERT(decimal(19, 4), NonETC.NumWorkingDays) 
        END, 
        @intHrDecimals
      ) AS PeriodHrs, 
      ETC.SeqID, 
      ETC.TimePhaseID 

      FROM (
        SELECT 
          SeqID, 
          TimePhaseID, 
          dbo.DLTK$NumWorkingDays(StartDate, EndDate, @strCompany) AS NumWorkingDays
          FROM @tabPLabTPD XTPD
          WHERE ETC = 'Y'
      ) AS ETC
        INNER JOIN (
          SELECT 
            SeqID, 
            TimePhaseID, 
            PeriodHrs,
            dbo.DLTK$NumWorkingDays(StartDate, EndDate, @strCompany) AS NumWorkingDays
            FROM @tabPLabTPD YTPD 
            WHERE ETC = 'N'
        ) AS NonETC
          ON ETC.SeqID = NonETC.SeqID AND ETC.TimePhaseID = NonETC.TimePhaseID
  ) AS XETC

  WHERE TPD.SeqID = XETC.SeqID AND TPD.TimePhaseID = XETC.TimePhaseID AND TPD.ETC = 'Y'

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     
IF ((@strMode = 'C' AND @bitIsLeafTask = 1) OR (@strMode = 'S' AND DATALENGTH(@strResourceType) > 0))
  BEGIN
    INSERT @tabProjectViewPeriod(
      SeqID,
      RowID,
      AssignmentID,
      TaskID,
      StartDate,
      PeriodHrs,
      NumWorkingDays,
      ETC
    )
    SELECT 
        SeqID,
        @strIDPrefix + '|' + @strTaskID AS RowID,
        AssignmentID, 
        @strTaskID as TaskID, 
        CIStartDate,
        SUM(ISNULL(ETC.PeriodHrs,0)) AS PeriodHrs,
        0 AS NumWorkingDays, 
        ETC
    FROM @tabPLabTPD AS ETC
    GROUP BY SeqID, AssignmentID, CIStartDate, ETC
  END /* END Assignment Rows */

ELSE /* WBS Rows */
  BEGIN
    IF (@strMode = 'S')
    BEGIN
      INSERT @tabProjectViewPeriod(
        SeqID,
        RowID,
        AssignmentID,
        TaskID,
        StartDate,
        PeriodHrs,
        NumWorkingDays,
        ETC
      )
      SELECT 
        ETC.SeqID,
        @strRowID AS RowID,
        null AS AssignmentID, 
        @strTaskID as TaskID, 
        ETC.CIStartDate, 
        MAX(ISNULL(PeriodHrs,0)) AS PeriodHrs, 
        0 AS NumWorkingDays, 
        ETC.ETC
      FROM 
        (SELECT 
          SeqID,
          SUM(ISNULL(ETC.PeriodHrs,0)) AS PeriodHrs, 
          CIStartDate, 
          0 AS NumWorkingDays,
          ETC
          FROM @tabPLabTPD AS ETC
          GROUP BY SeqID, CIStartDate, ETC
        ) AS ETC
      GROUP BY ETC.SeqID, ETC.CIStartDate, ETC.ETC
      END /* END IF (@strMode = 'S') */

      ELSE IF (@strMode = 'C')
      BEGIN
        INSERT @tabProjectViewPeriod(
          SeqID,
          RowID,
          AssignmentID,
          TaskID,
          StartDate,
          PeriodHrs,
          NumWorkingDays,
          ETC
        )
          SELECT 
            ETC.SeqID,
            @strIDPrefix + '|' + CT.TaskID AS RowID,
            NULL AS AssignmentID, 
            CT.TaskID, 
            ETC.CIStartDate, 
            SUM(ISNULL(PeriodHrs,0)) AS PeriodHrs, 
            0 AS NumWorkingDays, 
            ETC.ETC
          FROM 
            (SELECT 
              SeqID,
              SUM(ISNULL(ETC.PeriodHrs,0)) AS PeriodHrs,
              TaskID, 
              AssignmentID,
              CIStartDate,
              AT_OutlineNumber,
              0 AS NumWorkingDays, 
              ETC
             FROM @tabPLabTPD AS ETC
             GROUP BY SeqID, TaskID, AssignmentID, AT_OutlineNumber, CIStartDate, ETC
            ) AS ETC
            INNER JOIN RPTask CT ON ETC.AT_OutlineNumber LIKE CT.OutlineNumber + '%'
            INNER JOIN RPTask PT ON PT.Outlinenumber = CT.ParentOutlineNumber AND PT.PlanID = CT.PlanID
          WHERE PT.TaskID = @strTaskID
          GROUP BY ETC.SeqID, CT.TaskID, ETC.CIStartDate, ETC.ETC
      END
    END

RETURN
END

GO
