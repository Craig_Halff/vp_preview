SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[stRP$tabNeedUpgradeLaborCode](
  @strPlanID varchar(32) 
) 
  RETURNS @planLaborCodeSetup TABLE (
    bNeedUpdateLaborCode bit ,
	bHasLaborCode bit,
	bHasAllWildCardLaborCode bit ,
	bHasInconsistentLaborCode bit ,
    LCLevel1Enabled varchar (1) COLLATE database_default,
    LCLevel2Enabled varchar (1) COLLATE database_default,
    LCLevel3Enabled varchar (1) COLLATE database_default,
    LCLevel4Enabled varchar (1) COLLATE database_default,
    LCLevel5Enabled varchar (1) COLLATE database_default
)

BEGIN

  DECLARE @strTaskLaborCode nvarchar(14) = ''

  DECLARE @strLC1Code nvarchar(14) = ''
  DECLARE @strLC2Code nvarchar(14) = ''
  DECLARE @strLC3Code nvarchar(14) = ''
  DECLARE @strLC4Code nvarchar(14) = ''
  DECLARE @strLC5Code nvarchar(14) = ''

  DECLARE @strLC1WildCard nvarchar(14) = ''
  DECLARE @strLC2WildCard nvarchar(14) = ''
  DECLARE @strLC3WildCard nvarchar(14) = ''
  DECLARE @strLC4WildCard nvarchar(14) = ''
  DECLARE @strLC5WildCard nvarchar(14) = ''
  
  DECLARE @strLCDelimiter varchar(1) = ''
  DECLARE @siLCLevels smallint = 0
  DECLARE @siLC1Start smallint = 0
  DECLARE @siLC2Start smallint = 0
  DECLARE @siLC3Start smallint = 0
  DECLARE @siLC4Start smallint = 0
  DECLARE @siLC5Start smallint = 0
  DECLARE @siLC1Length smallint = 0
  DECLARE @siLC2Length smallint = 0
  DECLARE @siLC3Length smallint = 0
  DECLARE @siLC4Length smallint = 0
  DECLARE @siLC5Length smallint = 0

  DECLARE @strLCLevel1Enabled nvarchar(14) = 'N'
  DECLARE @strLCLevel2Enabled nvarchar(14) = 'N'
  DECLARE @strLCLevel3Enabled nvarchar(14) = 'N'
  DECLARE @strLCLevel4Enabled nvarchar(14) = 'N'
  DECLARE @strLCLevel5Enabled nvarchar(14) = 'N'

  DECLARE @strPrevious_LCLevel1Enabled nvarchar(14) = 'N'
  DECLARE @strPrevious_LCLevel2Enabled nvarchar(14) = 'N'
  DECLARE @strPrevious_LCLevel3Enabled nvarchar(14) = 'N'
  DECLARE @strPrevious_LCLevel4Enabled nvarchar(14) = 'N'
  DECLARE @strPrevious_LCLevel5Enabled nvarchar(14) = 'N'

  DECLARE @strLCDescSeparator nvarchar(3) = ' / '

  DECLARE @strLaborCode nvarchar(14)

  DECLARE @indexLoop int = 1
  DECLARE @bNeedUpdateLaborCode bit = 1
  DECLARE @bHasAllWildCardLaborCode bit = 0
  DECLARE @bHasInconsistentLaborCode bit = 0
  DECLARE @bHasLaborCode bit = 0
  DECLARE @cntLaborCodeTask int = 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get LCDelimiter and other LC formats.
  
	SELECT
    @siLCLevels = LCLevels,
	@strLCDelimiter = ISNULL(LTRIM(RTRIM(LCDelimiter)), ''),
    @siLC1Start = LC1Start,
    @siLC2Start = LC2Start,
    @siLC3Start = LC3Start,
    @siLC4Start = LC4Start,
    @siLC5Start = LC5Start,
    @siLC1Length = LC1Length,
    @siLC2Length = LC2Length,
    @siLC3Length = LC3Length,
    @siLC4Length = LC4Length,
    @siLC5Length = LC5Length
    FROM CFGFormat

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Decompose Labor Code string into individual levels using the formatting information in CFGFormat.

  SELECT 
    @strLC1WildCard = REPLICATE('_', @siLC1Length),
    @strLC2WildCard = REPLICATE('_', @siLC2Length),
    @strLC3WildCard = REPLICATE('_', @siLC3Length),
    @strLC4WildCard = REPLICATE('_', @siLC4Length),
    @strLC5WildCard = REPLICATE('_', @siLC5Length)

-- Loop through all labor code inside the plan 
-- to check the consisten of the level that the labor code wildcard appears
	
	SELECT @cntLaborCodeTask = count (*) FROM RPTask WHERE WBSType ='LBCD'  AND WBS1 <> '<none>' 
	AND ISNULL(LaborCode, ' ') <> '<none>' 
	AND ISNULL(WBS2, ' ') <> '<none>'  AND ISNULL(WBS3, ' ') <> '<none>'  AND PlanID = @strPlanID
	 
	DECLARE laborCodeTaskCursor CURSOR FOR 
	SELECT DISTINCT LaborCode FROM RPTask WHERE WBSType ='LBCD'  AND WBS1 <> '<none>' 
	AND ISNULL(LaborCode, ' ') <> '<none>' 
	AND ISNULL(WBS2, ' ') <> '<none>'  AND ISNULL(WBS3, ' ') <> '<none>'  AND PlanID = @strPlanID

	OPEN laborCodeTaskCursor
	FETCH NEXT FROM laborCodeTaskCursor INTO @strLaborCode 
	WHILE @@FETCH_STATUS = 0 
	BEGIN	    
		--retrieve level 1 labor code
		SELECT 
	    @strLC1Code = SUBSTRING(@strLaborCode, @siLC1Start, @siLC1Length)

		IF @indexLoop = 1 --first labor code in the loop
		BEGIN
			IF @strLC1Code = @strLC1WildCard  
			begin
				SET @strPrevious_LCLevel1Enabled = 'N'
				SET @strLCLevel1Enabled = 'N'
			end
			else
			begin
				SET @strPrevious_LCLevel1Enabled = 'Y'		
				SET @strLCLevel1Enabled = 'Y'		
			end
		END --IF @indexLoop = 1
		ELSE -- labor code after the first one in the loop
		BEGIN
			IF @strLC1Code = @strLC1WildCard  
			begin
				SET @strLCLevel1Enabled = 'N'
			end
			else
			begin
				SET @strLCLevel1Enabled = 'Y'		
			end
			--check if the labor code wildcard breaks the consistence
			IF @strLCLevel1Enabled <> @strPrevious_LCLevel1Enabled 
			BEGIN
				SET @bNeedUpdateLaborCode = 0
				SET @bHasInconsistentLaborCode = 1
				BREAK
			END
		END --ELSE IF @indexLoop = 1

		IF @siLCLevels >=2 
		BEGIN
			--retrieve level 2 labor code
			SELECT 
			@strLC2Code = SUBSTRING(@strLaborCode, @siLC2Start, @siLC2Length)

			IF @indexLoop = 1 --first labor code in the loop
			BEGIN
				IF @strLC2Code = @strLC2WildCard  
				begin
					SET @strPrevious_LCLevel2Enabled = 'N'
					SET @strLCLevel2Enabled = 'N'
				end
				else
				begin
					SET @strPrevious_LCLevel2Enabled = 'Y'		
					SET @strLCLevel2Enabled = 'Y'		
				end
			END --IF @indexLoop = 1
			ELSE -- labor code after the first one in the loop
			BEGIN
				IF @strLC2Code = @strLC2WildCard  
				begin
					SET @strLCLevel2Enabled = 'N'
				end
				else
				begin
					SET @strLCLevel2Enabled = 'Y'		
				end
				--check if the labor code wildcard breaks the consistence
				IF @strLCLevel2Enabled <> @strPrevious_LCLevel2Enabled 
				BEGIN
					SET @bNeedUpdateLaborCode = 0
					SET @bHasInconsistentLaborCode = 1
					BREAK
				END
			END --ELSE IF @indexLoop = 1
		END --IF @siLCLevels >=2	
		
		IF @siLCLevels >=3 
		BEGIN
			--retrieve level 3 labor code
			SELECT 
			@strLC3Code = SUBSTRING(@strLaborCode, @siLC3Start, @siLC3Length)

			IF @indexLoop = 1 --first labor code in the loop
			BEGIN
				IF @strLC3Code = @strLC3WildCard  
				begin
					SET @strPrevious_LCLevel3Enabled = 'N'
					SET @strLCLevel3Enabled = 'N'
				end
				else
				begin
					SET @strPrevious_LCLevel3Enabled = 'Y'		
					SET @strLCLevel3Enabled = 'Y'		
				end
			END --IF @indexLoop = 1
			ELSE -- labor code after the first one in the loop
			BEGIN
				IF @strLC3Code = @strLC3WildCard  
				begin
					SET @strLCLevel3Enabled = 'N'
				end
				else
				begin
					SET @strLCLevel3Enabled = 'Y'		
				end
				--check if the labor code wildcard breaks the consistence
				IF @strLCLevel3Enabled <> @strPrevious_LCLevel3Enabled 
				BEGIN
					SET @bNeedUpdateLaborCode = 0
					SET @bHasInconsistentLaborCode = 1
					BREAK
				END
			END --ELSE IF @indexLoop = 1
		END --IF @siLCLevels >=3	
		
		IF @siLCLevels >=4 
		BEGIN
			--retrieve level 4 labor code
			SELECT 
			@strLC4Code = SUBSTRING(@strLaborCode, @siLC4Start, @siLC4Length)

			IF @indexLoop = 1 --first labor code in the loop
			BEGIN
				IF @strLC4Code = @strLC4WildCard  
				begin
					SET @strPrevious_LCLevel4Enabled = 'N'
					SET @strLCLevel4Enabled = 'N'
				end
				else
				begin
					SET @strPrevious_LCLevel4Enabled = 'Y'		
					SET @strLCLevel4Enabled = 'Y'		
				end
			END --IF @indexLoop = 1
			ELSE -- labor code after the first one in the loop
			BEGIN
				IF @strLC4Code = @strLC4WildCard  
				begin
					SET @strLCLevel4Enabled = 'N'
				end
				else
				begin
					SET @strLCLevel4Enabled = 'Y'		
				end
				--check if the labor code wildcard breaks the consistence
				IF @strLCLevel4Enabled <> @strPrevious_LCLevel4Enabled 
				BEGIN
					SET @bNeedUpdateLaborCode = 0
					SET @bHasInconsistentLaborCode = 1
					BREAK
				END
			END --ELSE IF @indexLoop = 1
		END --IF @siLCLevels >=4

		IF @siLCLevels >=5 
		BEGIN
			--retrieve level 5 labor code
			SELECT 
			@strLC5Code = SUBSTRING(@strLaborCode, @siLC5Start, @siLC5Length)

			IF @indexLoop = 1 --first labor code in the loop
			BEGIN
				IF @strLC5Code = @strLC5WildCard  
				begin
					SET @strPrevious_LCLevel5Enabled = 'N'
					SET @strLCLevel5Enabled = 'N'
				end
				else
				begin
					SET @strPrevious_LCLevel5Enabled = 'Y'		
					SET @strLCLevel5Enabled = 'Y'		
				end
			END --IF @indexLoop = 1
			ELSE -- labor code after the first one in the loop
			BEGIN
				IF @strLC5Code = @strLC5WildCard  
				begin
					SET @strLCLevel5Enabled = 'N'
				end
				else
				begin
					SET @strLCLevel5Enabled = 'Y'		
				end
				--check if the labor code wildcard breaks the consistence
				IF @strLCLevel5Enabled <> @strPrevious_LCLevel5Enabled 
				BEGIN
					SET @bNeedUpdateLaborCode = 0
					SET @bHasInconsistentLaborCode = 1
					BREAK
				END
			END --ELSE IF @indexLoop = 1
		END --IF @siLCLevels >=5
		
		SET @indexLoop = @indexLoop + 1
		
		FETCH NEXT FROM laborCodeTaskCursor INTO @strLaborCode 
    END
    CLOSE laborCodeTaskCursor
    DEALLOCATE laborCodeTaskCursor
  
	IF @cntLaborCodeTask = 0 -- no labor code level tasks	 
	BEGIN
		SET @bNeedUpdateLaborCode = 0
	END

	IF @cntLaborCodeTask > 0  AND @bNeedUpdateLaborCode = 1 
	AND
	(@strLCLevel1Enabled = 'N' AND 
	 @strLCLevel2Enabled = 'N' AND 
	 @strLCLevel3Enabled = 'N' AND 
	 @strLCLevel4Enabled = 'N' AND 
	 @strLCLevel5Enabled = 'N' ) 
	BEGIN
		SET @bHasAllWildCardLaborCode = 1 -- all labor code are consistent set as wild card
		SET @bNeedUpdateLaborCode = 0
	END

    INSERT @planLaborCodeSetup(
    bNeedUpdateLaborCode,
	bHasLaborCode,
	bHasAllWildCardLaborCode,
	bHasInconsistentLaborCode,
    LCLevel1Enabled,
    LCLevel2Enabled,
    LCLevel3Enabled,
    LCLevel4Enabled,
    LCLevel5Enabled
    )
    SELECT
        @bNeedUpdateLaborCode,
		CASE WHEN @cntLaborCodeTask > 0 THEN 1 ELSE 0 END,
		@bHasAllWildCardLaborCode,
		@bHasInconsistentLaborCode,
        @strLCLevel1Enabled,
  		@strLCLevel2Enabled,
        @strLCLevel3Enabled,
  		@strLCLevel4Enabled,
        @strLCLevel5Enabled

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN 

END -- stRP$tabNeedUpgradeLaborCode
GO
