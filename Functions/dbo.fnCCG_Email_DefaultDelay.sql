SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_Email_DefaultDelay]()  RETURNS int  AS BEGIN  	declare @retval int  	select @retval = max(interval) from CCG_Email_Config  	return isnull(@retval,15)  END 
GO
