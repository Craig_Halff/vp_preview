SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_JTDUnbilledBillingDetails] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS @T TABLE 
(
	TopOrder	int,
	Descr		varchar(255),
--	Hours					Billing						Employee/URL				E(mployee) or A(ccount)
	Value1 decimal(19,2),	Value2		money,			Value3		varchar(32),	Which char(1),
	HTMLValue1 varchar(50),	HTMLValue2	varchar(50),	HTMLValue3	varchar(512),
	Align1 varchar(10),		Align2		varchar(10),	Align3		varchar(10)
)
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select * from dbo.fnCCG_EI_JTDUnbilledBillingDetails('2003005.xx',' ',' ')
	select * from dbo.fnCCG_EI_JTDUnbilledBillingDetails('2003005.xx','1PD',' ')
*/
	declare @ThruPeriod int, @ThruDate datetime

	select @ThruPeriod=ThruPeriod, @ThruDate=ThruDate
	from CCG_EI_ConfigInvoiceGroups cig
	inner join ProjectCustomTabFields pctf on pctf.custInvoiceGroup=cig.InvoiceGroup
	where pctf.WBS1=@WBS1 and pctf.WBS2=' '

	if @ThruPeriod is null			set @ThruPeriod=999999
	if @ThruDate is null	set @ThruDate='12/31/2050'

	insert into @T (TopOrder, Descr, Value1, Value2, Value3, HTMLValue1, HTMLValue2, HTMLValue3, Align1, Align2, Align3, Which)
	select 1 as TopOrder, '<b>LABOR</b>', null, null, null, '', '', '', 'left', 'left', 'center', null
	UNION ALL
	select 2 as TopOrder, '&nbsp;&nbsp;' + ISNULL(LastName + ', ' + FirstName,''), SUM(Hrs), Sum(BillExt), Labor.Employee, '', '', '', 'right', 'right', 'center', 'E'
	From(
		select LD.Employee, RegHrs+OvtHrs+SpecialOvtHrs as Hrs, BillExt
		from LD left join EM on EM.Employee=LD.Employee  
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus in ('B','H') and BillExt<>0   
			and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
	) as Labor left join EM on EM.Employee=Labor.Employee group by Labor.Employee, LastName, FirstName 
	UNION ALL
	select 30 as TopOrder, '<b>EXPENSE</b>', null, null, null, '', '', '', 'left', 'left', 'center', null
	UNION ALL
	select 40 as TopOrder, '&nbsp;&nbsp;' + CA.Name, null, Sum(BillExt), Expenses.Account, '', '', '', 'left', 'right', 'center', 'A'
	from (
		select Account, BillExt
		from LedgerMisc L --left join CA  on CA.Account=L.Account
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus in ('B','H') and BillExt<>0   
		  and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
		UNION ALL
		select Account, BillExt
		from LedgerAP L --left join CA  on CA.Account=L.Account
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus in ('B','H') and BillExt<>0  
		  and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
		UNION ALL
		select Account, BillExt
		from LedgerEX L --left join CA  on CA.Account=L.Account
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus in ('B','H') and BillExt<>0  
		  and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
		UNION ALL
		select Account, BillExt
		from LedgerAR L --left join CA  on CA.Account=L.Account
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus in ('B','H') and BillExt<>0  
		  and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
	) as Expenses left join CA on CA.Account=Expenses.Account group by Expenses.Account, CA.Name
	
	-- Add labor total line:
	insert into @T (TopOrder, Descr, Value1, Value2, HTMLValue1, HTMLValue2, Align1, Align2)
	select 9 as TopOrder, '<b>Labor Total</b>', (select sum(Value1) from @T where TopOrder in (2)), (select sum(Value2) from @T where TopOrder in (2)),   '', '', 'right', 'right'
	-- Add expense total line:
	insert into @T (TopOrder, Descr, Value1, Value2, HTMLValue1, HTMLValue2, Align1, Align2)
	select 49 as TopOrder, '<b>Expense Total</b>', null, (select sum(Value2) from @T where TopOrder in (40)), '', '', 'right', 'right'
	-- Add grand total line:
	insert into @T (TopOrder, Descr, Value1, Value2, HTMLValue1, HTMLValue2, Align1, Align2)
	select 90 as TopOrder, '<b>Grand Total</b>', null, (select sum(Value2) from @T where TopOrder in (2,40)), '', '', 'right', 'right'
	
	update @T set
		HTMLValue1 = Case When Value1 is null Then '&nbsp;' Else Convert(varchar(20),Value1) End,
		HTMLValue2 = Case When Value2 is null Then '&nbsp;' Else '$' + Convert(varchar,Convert(money, Value2),1) End,
		HTMLValue3 = Case When Value3 is null Then '&nbsp;' Else '<a href="' + dbo.fnCCG_GetWebServerURLPrefix() + 'EI_Popup.aspx?WBS1=' + @WBS1 + '&WBS2=' + RTrim(@WBS2) + '&WBS3=' + RTRim(@WBS3) +
			'&DbParams=%2C%27' + Value3 + '%27' + 
			'&T=Unbilled%20at%20Billing' + --%20-%20' + Value2 +
			'&NV=4' +
			Case Which When 'E' Then '&HD=Employee' Else '&HD=Account' End +
			'&H1=Date' +
			Case Which When 'E' Then '&H2=Hours' Else '&H2=Vendor' End +
			'&H3=Billing' +
			'&H4A=Left' +
			Case Which When 'E' Then '&H4=Comment' Else '&H4=Description' End +
			Case Which When 'E' Then '&FN=fnCCG_EI_JTDUnbilledBillingDetailsByEmp' Else '&FN=fnCCG_EI_JTDUnbilledBillingDetailsByAcct' End +
			'&DS=' + @@SERVERNAME + '&DB=' + DB_NAME() + '">Show&nbsp;Details</a>'
			End
	from @T, CFGSystem
	
	update @T set HTMLValue1='<b>'+HTMLValue1+'</b>' where TopOrder in (9,49,90)
	update @T set HTMLValue2='<b>'+HTMLValue2+'</b>' where TopOrder in (9,49,90)
	
	return
END
GO
