SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnCCG_EI_JTDUnbilledBillingThru_Old] (@WBS1 VARCHAR(30), @WBS2 VARCHAR(7), @WBS3 VARCHAR(7), @ThruDate DATETIME)
RETURNS MONEY
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.
	- modified 12/7/2017 by J Gray to use @ThruDate; include/exclude BST transactions accordingly

	select dbo.fnCCG_EI_JTDUnbilledBillingThru('2003005.xx',' ',' ', CAST('2017-09-30' AS DATETIME))
	select dbo.fnCCG_EI_JTDUnbilledBillingThru('2003005.xx','1PD',' ', CAST('2017-09-30' AS DATETIME))

	-- CHA - This need to be evaulated fully, especially considering future history loads from acquisitions.
*/

	declare @res1 money, @res2 money
	Declare @ThruPeriod	int 

	Set @ThruPeriod = DATEPART(yyyy, @ThruDate) * 100 + DATEPART(mm, @ThruDate)

	select @res1=Sum(BillExt)
	from (
		select BillExt From LD
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') 
		and ProjectCost='Y' 
		and BillExt<>0     
		and Period <= @ThruPeriod 
		and (TransDate <= @ThruDate or TransDate is null) 
		and
		(
			(	
				-- removes all BST related transactions
				BillStatus in ('B','H','F') 
				and TransType <> 'HL'	
				and (BilledPeriod > @ThruPeriod or BilledPeriod = 0)
			)
			or
			(
				-- include BST transactions
				BillStatus in ('B','H') 
				and TransType = 'HL'
			)
		)
	) as Labor
	
	select @res2=Sum(BillExt)
	from (
		select BillExt from LedgerMisc L
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') 
		and ProjectCost='Y' 
		and BillExt<>0     
		and Period <= @ThruPeriod 
		and (TransDate <= @ThruDate or TransDate is null) 
		and
		(
			(	
				-- removes all BST related transactions
				BillStatus in ('B','H','F') 
				and TransType <> 'HE'	
				and (BilledPeriod > @ThruPeriod or BilledPeriod = 0)
			)
			or
			(
				-- include BST transactions
				BillStatus in ('B','H') 
				and TransType = 'HE'
			)
		)

		UNION ALL

		select BillExt from LedgerAP L
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') 
		and ProjectCost='Y' 
		and BillExt<>0     
		and Period <= @ThruPeriod 
		and (TransDate <= @ThruDate or TransDate is null) 
		and
		(
			(	
				-- removes all BST related transactions
				BillStatus in ('B','H','F')  
				and TransType <> 'HE'	
				and (BilledPeriod > @ThruPeriod or BilledPeriod = 0)
			)
			or
			(
				-- include BST transactions
				BillStatus in ('B','H') 
				and TransType = 'HE'
			)
		)

		UNION ALL

		select BillExt from LedgerEX L
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') 
		and ProjectCost='Y' 
		and BillExt<>0     
		and Period <= @ThruPeriod 
		and (TransDate <= @ThruDate or TransDate is null) 
		and
		(
			(	
				-- removes all BST related transactions
				BillStatus in ('B','H','F') 
				and TransType <> 'HE'	
				and (BilledPeriod > @ThruPeriod or BilledPeriod = 0)
			)
			or
			(
				-- include BST transactions
				BillStatus in ('B','H') 
				and TransType = 'HE'
			)
		)

		UNION ALL
		
		select BillExt from LedgerAR L
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') 
		and ProjectCost='Y' 
		and BillExt<>0     
		and Period <= @ThruPeriod 
		and (TransDate <= @ThruDate or TransDate is null) 
		and
		(
			(	
				-- removes all BST related transactions
				BillStatus in ('B','H','F') 
				and TransType <> 'HE'	
				and (BilledPeriod > @ThruPeriod or BilledPeriod = 0)
			)
			or
			(
				-- include BST transactions
				BillStatus in ('B','H') 
				and TransType = 'HE'
			)
		)
	) as Expenses

	SET @res1 = ISNULL(@res1,0) + ISNULL(@res2,0)

	IF @res1 = 0
		SET @res1 = NULL

	RETURN @res1





	--declare @res1 money, @res2 money
	--Declare @ThruPeriod	int 

	--Set @ThruPeriod = DATEPART(yyyy, @ThruDate) * 100 + DATEPART(mm, @ThruDate)

	--select @res1=Sum(BillExt)
	--from (
	--	select BillExt From LD
	--	where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus not in ('X','R','T') and BillExt<>0     
	--		and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null) and (BilledPeriod > @ThruPeriod or BilledPeriod = 0)
	--) as Labor
	
	--select @res2=Sum(BillExt)
	--from (
	--	select BillExt from LedgerMisc L
	--	where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus not in ('X','R','T') and BillExt<>0    
	--		and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null) and (BilledPeriod > @ThruPeriod or BilledPeriod = 0)
	--	UNION ALL
	--	select BillExt from LedgerAP L
	--	where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus not in ('X','R','T') and BillExt<>0    
	--		and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null) and (BilledPeriod > @ThruPeriod or BilledPeriod = 0)
	--	UNION ALL
	--	select BillExt from LedgerEX L
	--	where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus not in ('X','R','T') and BillExt<>0   
	--			and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null) and (BilledPeriod > @ThruPeriod or BilledPeriod = 0)
	--	UNION ALL
	--	select BillExt from LedgerAR L
	--	where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus not in ('X','R','T') and BillExt<>0   
	--		and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null) and (BilledPeriod > @ThruPeriod or BilledPeriod = 0)
	--) as Expenses

	--set @res1 = IsNull(@res1,0) + IsNull(@res2,0)

	--if @res1 = 0
	--	set @res1 = null

	--return @res1
END
GO
