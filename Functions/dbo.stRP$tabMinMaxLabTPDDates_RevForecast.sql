SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabMinMaxLabTPDDates_RevForecast](
  @strRowID nvarchar(255),
  @bitCalledFromRM bit = 0
)
  RETURNS @tabMinMaxDates TABLE (
    StartDate datetime,
    EndDate datetime
  )

BEGIN

  DECLARE @strPlanID varchar(32) = NULL
  DECLARE @strTaskID varchar(32) = NULL
  DECLARE @strAssignmentID varchar(32) = NULL

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
  -- Parse @strRowID

  SELECT
    @strPlanID = PlanID,
    @strTaskID = TaskID
    FROM dbo.stRP$tabParseRowID(@strRowID)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@bitCalledFromRM = 0)
    BEGIN -- Need to return PN data.

      INSERT @tabMinMaxDates(
        StartDate,
        EndDate
      )
        SELECT
          MIN(TPD.StartDate) AS StartDate,
          MAX(TPD.EndDate) AS EndDate
          FROM PNPlannedRevenueLabor AS TPD
            INNER JOIN PNTask AS CT ON TPD.PlanID = CT.PlanID AND TPD.TaskID = CT.TaskID 
            INNER JOIN PNTask AS PT ON CT.PlanID = PT.PlanID AND CT.OutlineNumber LIKE PT.OutlineNumber + '%'
          WHERE TPD.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND (TPD.PeriodBill > 0 OR TPD.PeriodCost > 0 ) 

    END /* END IF (@bitCalledFromRM = 0) */

  ELSE
    BEGIN -- Need to return RP data.

      INSERT @tabMinMaxDates(
        StartDate,
        EndDate
      )
        SELECT
          MIN(TPD.StartDate) AS StartDate,
          MAX(TPD.EndDate) AS EndDate
          FROM RPPlannedRevenueLabor AS TPD
            INNER JOIN RPTask AS CT ON TPD.PlanID = CT.PlanID AND TPD.TaskID = CT.TaskID
            INNER JOIN RPTask AS PT ON CT.PlanID = PT.PlanID AND CT.OutlineNumber LIKE PT.OutlineNumber + '%'
          WHERE TPD.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND (TPD.PeriodBill > 0 OR TPD.PeriodCost > 0 ) 
             

    END /* END ELSE (@bitCalledFromRM = 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
