SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DW$tabPlan]()
  RETURNS @tabPlan TABLE
    (PlanID varchar(32) COLLATE database_default,
     Principal nvarchar(20) COLLATE database_default,
     ProjMgr nvarchar(20) COLLATE database_default,
     Supervisor nvarchar(20) COLLATE database_default,
     Org nvarchar(14) COLLATE database_default,
     ClientID varchar(32) COLLATE database_default,
     OpportunityID varchar(32) COLLATE database_default,
     PlanName nvarchar(255) COLLATE database_default,
     PlanNumber nvarchar(30) COLLATE database_default,
     Status varchar(1) COLLATE database_default,
     StatusDesc nvarchar(50) COLLATE database_default,
     PctCompleteFormula smallint,
     PctCompleteFormulaDesc nvarchar(50) COLLATE database_default,
     LabMultType smallint,
     LabMultTypeDesc nvarchar(50) COLLATE database_default,
     ReimbMethod varchar(1) COLLATE database_default,
     ReimbMethodDesc nvarchar(50) COLLATE database_default,
     Probability smallint,
     ProjectedMultiplier decimal(19, 4),
     ProjectedRatio decimal(19, 4),
     TargetMultCost decimal(19, 4),
     TargetMultBill decimal(19, 4),
     UtilizationIncludeFlg varchar(1) COLLATE database_default,
     CompensationFeeCost decimal(19, 4),
     ConsultantFeeCost decimal(19, 4),
     ReimbAllowanceCost decimal(19, 4),
     CompensationFeeBill decimal(19, 4),
     ConsultantFeeBill decimal(19, 4),
     ReimbAllowanceBill decimal(19, 4),
     SK_StartDateID int,
     SK_EndDateID int,
     CostCurrencyCode nvarchar(3) COLLATE database_default,
	 BillingCurrencyCode nvarchar(3)  COLLATE database_default
    )
BEGIN -- Function DW$tabPlan

  DECLARE @tabPctCompleteFormula
    TABLE (Code smallint,
           Description nvarchar(50) COLLATE database_default
           PRIMARY KEY(Code))
           
  DECLARE @tabLabMultType
    TABLE (Code smallint,
           Description nvarchar(50) COLLATE database_default
           PRIMARY KEY(Code))

  DECLARE @tabReimbMethod
    TABLE (Code varchar(1) COLLATE database_default,
           Description nvarchar(50) COLLATE database_default
           PRIMARY KEY(Code))


-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Insert Code & Description for PctCompleteFormula
  
  INSERT INTO @tabPctCompleteFormula(Code, Description) VALUES(0, N'User-Entered % Complete') --My.Resources.DDPctComplFormulaUserEnter
  INSERT INTO @tabPctCompleteFormula(Code, Description) VALUES(1, N'(Planned - ETC) / Planned') --My.Resources.DDPctComplFormulaPlannedETC
  INSERT INTO @tabPctCompleteFormula(Code, Description) VALUES(2, N'JTD / (JTD + ETC)') --My.Resources.DDPctComplFormulaJTDETC
  INSERT INTO @tabPctCompleteFormula(Code, Description) VALUES(3, N'(Baseline - ETC) / Baseline') --My.Resources.DDPctComplFormulaBaselineETC

  -- Insert Code & Description for LabMultType
  
  INSERT INTO @tabLabMultType(Code, Description) VALUES(0, N'User Entered (Cost)') --My.Resources.DDLabMultTypeUsrEnteredCost
  INSERT INTO @tabLabMultType(Code, Description) VALUES(1, N'Planned Multiplier') --My.Resources.DDLabMultTypePlannedMultiplier
  INSERT INTO @tabLabMultType(Code, Description) VALUES(2, N'User Entered (Billing)') --My.Resources.DDLabMultTypeUsrEnteredBilling
  INSERT INTO @tabLabMultType(Code, Description) VALUES(3, N'Planned Ratio') --My.Resources.DDLabMultTypePlannedRatio

  -- Insert Code & Description for ReimbMethod
  
  INSERT INTO @tabReimbMethod(Code, Description) VALUES('C', N'Cost') --My.Resources.DDReimbMethodCost
  INSERT INTO @tabReimbMethod(Code, Description) VALUES('B', N'Billing') --My.Resources.DDReimbMethodBilling

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Insert records from RPPlan into @tabPlan

  INSERT INTO @tabPlan
    (PlanID,
     Principal,
     ProjMgr,
     Supervisor,
     Org,
     ClientID,
     OpportunityID,
     PlanName,
     PlanNumber,
     Status,
     StatusDesc,
     PctCompleteFormula,
     PctCompleteFormulaDesc,
     LabMultType,
     LabMultTypeDesc,
     ReimbMethod,
     ReimbMethodDesc,
     Probability,
     ProjectedMultiplier,
     ProjectedRatio,
     TargetMultCost,
     TargetMultBill,
     UtilizationIncludeFlg,
     CompensationFeeCost,
     ConsultantFeeCost,
     ReimbAllowanceCost,
     CompensationFeeBill,
     ConsultantFeeBill,
     ReimbAllowanceBill,
     SK_StartDateID,
     SK_EndDateID,
     CostCurrencyCode,
     BillingCurrencyCode
	)
    SELECT
      P.PlanID AS PlanID,
      CONVERT(NVARCHAR(20),P.Principal) Principal, 
      CONVERT(NVARCHAR(20),P.ProjMgr) ProjMgr, 
      CONVERT(NVARCHAR(20),P.Supervisor) Supervisor, 
      CONVERT(NVARCHAR(14),P.Org) Org, 
      P.ClientID AS ClientID,
      P.OpportunityID AS OpportunityID,
      CONVERT(NVARCHAR(255),P.PlanName) AS PlanName,
      CONVERT(NVARCHAR(30),ISNULL(P.PlanNumber, '<empty>')) AS PlanNumber,
      ISNULL(P.Status, '*') AS Status,
      CONVERT(NVARCHAR(50),ISNULL(S.Description, '<empty>')) StatusDesc, 
      ISNULL(P.PctCompleteFormula, '0') AS PctCompleteFormula,
      ISNULL(PCF.Description, '<empty>') PctCompleteFormulaDesc, 
      ISNULL(P.LabMultType, '0') AS LabMultType,
      ISNULL(LMT.Description, '<empty>') LabMultTypeDesc, 
      ISNULL(P.ReimbMethod, '*') AS ReimbMethod,
      ISNULL(RM.Description, '<empty>') ReimbMethodDesc, 
      P.Probability AS Probability,
      P.ProjectedMultiplier AS ProjectedMultiplier,
      P.ProjectedRatio AS ProjectedRatio,
      P.TargetMultCost AS TargetMultCost,
      P.TargetMultBill AS TargetMultBill,
      P.UtilizationIncludeFlg AS UtilizationIncludeFlg,
      P.CompensationFee AS CompensationFeeCost,
      P.ConsultantFee AS ConsultantFeeCost,
      P.ReimbAllowance AS ReimbAllowanceCost,
      P.CompensationFeeBill AS CompensationFeeBill,
      P.ConsultantFeeBill AS ConsultantFeeBill,
      P.ReimbAllowanceBill AS ReimbAllowanceBill,

      convert(int,CASE WHEN P.StartDate is null THEN -1 ELSE P.StartDate END) AS SK_StartDateID,
      convert(int,CASE WHEN P.EndDate is null THEN -1 ELSE P.EndDate END) AS SK_EndDateID,
      P.CostCurrencyCode AS CostCurrencyCode,
      P.BillingCurrencyCode AS BillingCurrencyCode
	  

      FROM RPPlan AS P
	    LEFT JOIN CFGProjectStatus AS S ON P.Status = S.Code
        LEFT JOIN @tabPctCompleteFormula AS PCF ON P.PctCompleteFormula = PCF.Code
        LEFT JOIN @tabLabMultType AS LMT ON P.LabMultType = LMT.Code
        LEFT JOIN @tabReimbMethod AS RM ON P.ReimbMethod = RM.Code

  RETURN

END -- DW$tabPlan

GO
