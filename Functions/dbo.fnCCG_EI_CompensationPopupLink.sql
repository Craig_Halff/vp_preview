SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_CompensationPopupLink] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS varchar(255)
AS BEGIN
/*
	Copyright (c) 2020 EleVia Software. All rights reserved.

	select dbo.fnCCG_EI_CompensationPopupLink('2003005.xx', ' ', ' ')
*/
	declare @res varchar(255)
	select @res = 'value@spCCG_EI_PopWin_CompensationDetails'
	return @res
END
GO
