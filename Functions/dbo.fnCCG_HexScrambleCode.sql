SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnCCG_HexScrambleCode] (
	@valueToScramble		varchar(max)
)
returns varchar(max)
as
begin
	-- select dbo.fnCCG_HexScrambleCode('ADMIN')
	declare @sHex varchar(max) = ''
	if isnull(@valueToScramble, '') <> ''
	begin
		declare @pos int = 0
		declare @data varchar(max) = @valueToScramble
		declare @sValue varchar(10) = ''
		declare @letter int
		declare @shift int = len(@valueToScramble) + 1
		declare @i int = 1

		while @i <= len(@valueToScramble)
		begin
			set @letter = ascii(SUBSTRING(@valueToScramble, @i, 1))
			set @letter = @letter - @shift - (@pos % 5)
			set @sValue = FORMAT(@letter,'X')
			if len(@sValue) = 1 set @sValue = '0'+@sValue
			set @sHex = @sHex + @sValue
			set @i = @i + 1
			set @pos = @pos + 1
		end
	end
	return @sHex
end
GO
