SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetConCatTaskName_Timesheet]
 (@strPlanID varchar(32), 
  @strTaskID varchar(32),
  @option varchar(1)) 
  RETURNS Nvarchar(max)  AS
BEGIN

  DECLARE @strConcatName Nvarchar(max)
  DECLARE @strParentOutlineNumber varchar(255)
  DECLARE @WBS1 Nvarchar(30), @WBS2 Nvarchar(7), @WBS3 Nvarchar(7), @WBSType varchar(4), @LaborCode Nvarchar(14)
  DECLARE @StartDate datetime, @EndDate datetime
  DECLARE @Done varchar(1)
  DECLARE @ReturnValue Nvarchar(max) 

  SELECT @Done = 'N'

  -- Get Name of current Task
  
  SELECT 
    @strConcatName = Name,
    @strParentOutlineNumber = ParentOutlineNumber,
	@WBS1 = WBS1, @WBS2 = WBS2, @WBS3 = WBS3, @LaborCode = LaborCode, @WBSType = WBSType,
	@StartDate = StartDate, @EndDate = EndDate
    FROM RPTask WHERE PlanID = @strPlanID AND TaskID = @strTaskID

  -- Loop upward to find parent until getting to the top

	  IF @WBSType = 'LBCD' AND (@LaborCode IS NOT NULL AND @LaborCode <> '<none>') SET @Done = 'Y' 
	  IF @WBSType = 'WBS3' AND (@WBS3 IS NOT NULL AND @WBS3 <> '<none>') SET @Done = 'Y' 
	  IF @WBSType = 'WBS2' AND (@WBS2 IS NOT NULL AND @WBS2 <> '<none>') SET @Done = 'Y' 

  WHILE NOT (@strParentOutlineNumber IS NULL) AND @Done = 'N'
    BEGIN

      SELECT 
        @strConcatName = Name,
        @strParentOutlineNumber = ParentOutlineNumber,
		@WBS1 = WBS1, @WBS2 = WBS2, @WBS3 = WBS3, @LaborCode = LaborCode, @WBSType = WBSType,
		@StartDate = StartDate, @EndDate = EndDate
        FROM RPTask WHERE PlanID = @strPlanID AND OutlineNumber = @strParentOutlineNumber 

      IF @@ROWCOUNT = 0 SET @Done = 'Y'

	  IF @WBSType = 'LBCD' AND (@LaborCode IS NOT NULL AND @LaborCode <> '<none>') SET @Done = 'Y' 
	  IF @WBSType = 'WBS3' AND (@WBS3 IS NOT NULL AND @WBS3 <> '<none>') SET @Done = 'Y' 
	  IF @WBSType = 'WBS2' AND (@WBS2 IS NOT NULL AND @WBS2 <> '<none>') SET @Done = 'Y' 

    END -- WHILE

	IF @option = 'D' SET @ReturnValue = @strConcatName 
	IF @option = 'S' SET @ReturnValue = Replace(CONVERT(VARCHAR, @StartDate, 121), ' ', 'T') 
	IF @option = 'E' SET @ReturnValue = Replace(CONVERT(VARCHAR, @EndDate, 121), ' ', 'T') 
  RETURN @ReturnValue 

END -- GetConCatTaskName_Timesheet
GO
