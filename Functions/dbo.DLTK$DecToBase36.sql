SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[DLTK$DecToBase36]
  (@val as BigInt, -- Max: 2^63–1 (9,223,372,036,854,775,807). 
   @base as tinyint)
  RETURNS varchar(63)
  
BEGIN

    -- Check if we get the valid base.

    IF (@val < 0) OR (@base < 2) OR (@base > 36) RETURN NULL

    -- Variable to hold final answer.
    -- At Base 2, there will be maximumly 63 characters.
    -- At Base 36, there will be maximumly 13 characters.

    DECLARE @strAnswer as varchar(63)
    SET @strAnswer = ''

    -- Variable contains all possible alpha numeric letters for base up to 36. 

    DECLARE @strAllDigits as varchar(36)
    SET @strAllDigits = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    -- Loop until your source value is greater than 0

    WHILE (@val > 0)
      BEGIN

        SET @strAnswer = SUBSTRING(@strAllDigits, @val % @base + 1, 1) + @strAnswer
        SET @val = @val / @base

      END

    -- Return the final answer.

    RETURN @strAnswer

END -- fn_DLTK$DecToBase36
GO
