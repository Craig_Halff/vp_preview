SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   FUNCTION [dbo].[fnCCG_EI_GetPriorActionDate] (@Seq int)
RETURNS datetime AS
BEGIN

/* Copyright (c) 2019 EleVia Software.  All rights reserved.*/

	declare @res		datetime
	declare @WBS1		Nvarchar(32)
	declare @ActionDate	datetime

	select @WBS1=WBS1, @ActionDate=ActionDate from CCG_EI_History where Seq=@Seq

	select @res = Max(ActionDate)
	from CCG_EI_History
	where WBS1 = @WBS1 and datediff(second,@ActionDate,ActionDate)<0 and ActionTaken='Stage Change'
	return @res
END
GO
