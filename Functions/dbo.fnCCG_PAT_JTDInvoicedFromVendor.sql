SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_PAT_JTDInvoicedFromVendor] (
	@PayableSeq int,
	@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS @T TABLE
(
	ColValue	money,
	ColDetail	varchar(100)	-- Set background color 
)
AS BEGIN
/*
	Copyright (c) 2017 EleVia Software. All rights reserved.

	select * from  dbo.fnCCG_PAT_JTDInvoicedFromVendor(5262,NULL,' ',' ')
	select * from  dbo.fnCCG_PAT_JTDInvoicedFromVendor(5311,NULL,' ',' ')
	select * from  dbo.fnCCG_PAT_JTDInvoicedFromVendor(5311,'2003005.00',' ',' ')
	-- 20210617 - CHA - Added WITH (NOLOCK) hints
*/
    declare @res1 decimal(19,5), @res2 decimal(19,5), @res3 decimal(19,5)

		-- Records from PAT not yet in Vision:
		select @res1 = sum(pa.NetAmount)
            from CCG_PAT_Payable pat WITH (NOLOCK)
            inner join CCG_PAT_ProjectAmount pa  WITH (NOLOCK) ON pat.Seq = pa.PayableSeq
            inner join (
				select distinct Vendor,pat.Seq,WBS1 
				from CCG_PAT_Payable pat  WITH (NOLOCK)
				inner join CCG_PAT_ProjectAmount pa  WITH (NOLOCK) ON pat.Seq = pa.PayableSeq 
				where pat.Seq = @PayableSeq and (@WBS1 is null or WBS1 = @WBS1)
				) thispa on pat.Vendor = thispa.Vendor and pa.WBS1 = thispa.WBS1
            inner join VE  WITH (NOLOCK) ON VE.Vendor=pat.Vendor
            where 
              PayableType = 'I' and ISNULL(pat.Voucher,'') = '' and (@WBS1 is null or (pa.WBS1 = @WBS1 and pa.WBS2 = @WBS2 and pa.WBS3 = @WBS3 ))

		-- Unposted records in Vision
		select @res2 = sum(apd.NetAmount)
		from CCG_PAT_Payable pat WITH (NOLOCK)
		inner join (select distinct PayableSeq,WBS1/*,WBS2,WBS3*/ from CCG_PAT_ProjectAmount WITH (NOLOCK) ) pa on pat.Seq = pa.PayableSeq
		inner join apMaster ap  WITH (NOLOCK) ON pat.Vendor=ap.Vendor --and pat.Voucher=ap.Voucher
		inner join apDetail apd  WITH (NOLOCK) ON apd.Batch=ap.Batch and apd.MasterPKey=ap.MasterPKey
		left join VE  WITH (NOLOCK) ON VE.Vendor=ap.Vendor
		where (pat.Seq = @PayableSeq /*or pat.ParentSeq = @PayableSeq*/)	-- Match on single invoice or all invoices for a contract
		  and (@WBS1 IS NULL or (apd.WBS1=@WBS1 and apd.WBS2=@WBS2 and apd.WBS3=@WBS3))
		  and ap.Posted='N'

		  if @WBS1 IS NULL or @WBS1 = ' '
		  BEGIN
		--Records from Vision
			select @res3 = sum(ap.amount)
			from CCG_PAT_Payable pat  WITH (NOLOCK)
			left join (select distinct PayableSeq,WBS1/*,WBS2,WBS3*/ from CCG_PAT_ProjectAmount WITH (NOLOCK) ) pa on pat.Seq = pa.PayableSeq
			left join LedgerAP ap WITH (NOLOCK) on pat.Vendor=ap.Vendor and pa.WBS1=ap.WBS1 /*and (pa.WBS2=ap.WBS2 or pa.WBS2='')*/
			--left join VE on VE.Vendor=ap.Vendor
			--left join VendorCustomTabFields vctf on vctf.Vendor = ap.Vendor
			--left join VO on VO.Vendor=ap.Vendor and VO.Voucher=ap.Voucher
			where (pat.Seq = @PayableSeq /*or pat.ParentSeq = @PayableSeq*/)	-- Match on single invoice or all invoices for a contract
			  and (@WBS1 IS NULL or (ap.WBS1=@WBS1 /*and (ap.WBS2=@WBS2 or @WBS2 = ' ') and (ap.WBS3=@WBS3 or @WBS3 = ' ')*/ ))
			  and TransType = 'AP'  and PostSeq  <> '0'		
		END
		else
		BEGIN
		--Records from Vision
			select @res3 = sum(ap.amount)
			from CCG_PAT_Payable pat WITH (NOLOCK)
			left join (select distinct PayableSeq,WBS1 from CCG_PAT_ProjectAmount WITH (NOLOCK) ) pa on pat.Seq = pa.PayableSeq
			left join LedgerAP ap WITH (NOLOCK) on pat.Vendor=ap.Vendor and pa.WBS1=ap.WBS1 
			--left join VE on VE.Vendor=ap.Vendor
			--left join VendorCustomTabFields vctf on vctf.Vendor = ap.Vendor
			--left join VO on VO.Vendor=ap.Vendor and VO.Voucher=ap.Voucher
			where (pat.Seq = @PayableSeq /*or pat.ParentSeq = @PayableSeq*/)	-- Match on single invoice or all invoices for a contract
			  and  (ap.WBS1=@WBS1 and ap.WBS2=@WBS2  and ap.WBS3=@WBS3 )
			  and TransType = 'AP'  and PostSeq  <> '0'	
		END
	set @res1 = IsNull(@res1,0)+IsNull(@res2,0)+IsNull(@res3,0)	
	if @res1 = 0
		set @res1 = null
    insert into @T values (@res1, '')
	return
END

GO
