SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$xmlAssignmentAlert]
  (@strPlanID varchar(32)
  )
  RETURNS xml
BEGIN -- Function PN$xmlAssignmentAlert
 
  DECLARE  @tabEmployees TABLE
    (SupervisorNo Nvarchar(20) COLLATE database_default,
     SupervisorEMail Nvarchar(50) COLLATE database_default,
     EmployeeNo Nvarchar(20) COLLATE database_default,
     EmployeeName Nvarchar(56) COLLATE database_default,
     TaskName Nvarchar(100) COLLATE database_default,
	 LaborCode Nvarchar(100) COLLATE database_default,
     VorN bit
    )

  DECLARE @xmlAssignmentAlert xml

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT INTO @tabEmployees
    (SupervisorNo,
     SupervisorEMail,
     EmployeeNo,
     EmployeeName,
     TaskName,
	 LaborCode,
     VorN
    )
     SELECT
      ISNULL(SP.Employee, N'') AS SupervisorNo,
      ISNULL(SP.EMail, N'') AS SupervisorEMail,
      DF.ResourceID AS EmployeeNo,
      ISNULL(EE.FirstName, N'') + ISNULL(N' ' + EE.LastName, N'') AS EmployeeName,
      CASE WHEN DF.LaborCode IS NULL THEN CASE WHEN DF.VorN = 0 THEN ISNULL(NT.Name, N'') 
           WHEN DF.VorN = 1 THEN ISNULL(VT.Name, N'')
           END 
		   ELSE DF.TaskName END
		   AS TaskName,
      CASE WHEN DF.LaborCode IS NOT NULL THEN CASE WHEN DF.VorN = 0 THEN ISNULL(NT.Name, N'') 
           WHEN DF.VorN = 1 THEN ISNULL(VT.Name, N'')
           END 
		   ELSE N'' END
		   AS LaborCode,
      VorN
      FROM
        (SELECT MIN(VorN) as VorN, PlanID, TaskID, TaskName, LaborCode, ResourceID
           FROM
             (SELECT 0 AS VorN, N.PlanID, N.TaskID, TaskName = (select Name from PNTask WHERE PNTask.WBS1=N.WBS1 AND ISNULL(PNTask.WBS2, N'')= ISNULL(N.WBS2, N'') AND  ISNULL(PNTask.WBS3, N'')= ISNULL(N.WBS3, N'') AND WBSType <> 'LBCD'), LaborCode, N.ResourceID FROM PNAssignment AS N WHERE PlanID = @strPlanID
              UNION ALL
              SELECT 1 AS VorN, V.PlanID, V.TaskID, TaskName = (select Name from PNTask WHERE PNTask.WBS1=V.WBS1 AND ISNULL(PNTask.WBS2, N'')= ISNULL(V.WBS2, N'') AND  ISNULL(PNTask.WBS3, N'')= ISNULL(V.WBS3, N'') AND WBSType <> 'LBCD'), LaborCode, V.ResourceID FROM RPAssignment AS V WHERE PlanID = @strPlanID
             ) ZZ
           GROUP BY PlanID, TaskID,TaskName, LaborCode, ResourceID
           HAVING COUNT(*) = 1
        ) AS DF
        LEFT JOIN EM AS EE ON DF.ResourceID = EE.Employee
        LEFT JOIN EM AS SP ON EE.Supervisor = SP.Employee
        LEFT JOIN PNTask AS NT ON DF.TaskID = NT.TaskID AND DF.VorN = 0
        LEFT JOIN RPTask AS VT ON DF.TaskID = VT.TaskID AND DF.VorN = 1

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @xmlAssignmentAlert = 
    (
     SELECT
       P.PlanID AS "@PlanID",
       P.PlanName AS "@PlanName",
       P.UtilizationIncludeFlg AS "@UtilizationInclude",
       ISNULL(PA.Employee, N'') AS "@PlanAuthorNo",
       ISNULL(PA.FirstName, N'') + ISNULL(N' ' + PA.LastName, N'') AS "@PlanAuthorName",
       ISNULL(PA.EMail, N'') AS "@PlanAuthorEmail",
       (
        SELECT
          SP.SupervisorNo AS "@ID",
          SP.SupervisorEMail AS "@Email",
          (
           SELECT
             EE.EmployeeNo AS "@ID",
             EE.EmployeeName AS "@Name",
             EE.TaskName AS "@TaskName",
			 EE.LaborCode AS "@LaborCode",
             CASE WHEN VorN = 0 THEN 'I' 
                  WHEN VorN = 1 THEN 'D'
                  END AS "@Action"
             FROM @tabEmployees AS EE
             WHERE EE.SupervisorNo = SP.SupervisorNo
             FOR XML PATH('Employee'), TYPE
          )
          FROM @tabEmployees AS SP
          GROUP BY SP.SupervisorNo, SP.SupervisorEMail
          FOR XML PATH('Supervisor'), TYPE
       )
       FROM PNPlan AS P
         LEFT JOIN SEUser AS SU ON P.ModUser = SU.UserName
         LEFT JOIN EM AS PA ON SU.Employee = PA.Employee
       WHERE P.PlanID = @strPlanID
       FOR XML PATH('Plan')    
    )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN @xmlAssignmentAlert

END -- PN$xmlAssignmentAlert
GO
