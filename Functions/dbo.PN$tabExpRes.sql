SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$tabExpRes]
  (@strWBS1 Nvarchar(30),
   @strWBS2 Nvarchar(7),
   @strWBS3 Nvarchar(7),
   @strJTDDate VARCHAR(8)) -- Date must be in format: 'yyyymmdd' regardless of UI Culture.

  RETURNS @tabExpRes TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    ExpenseID varchar(32) COLLATE database_default,
    WBS1 Nvarchar(30),
    WBS2 Nvarchar(7),
    WBS3 Nvarchar(7),
    Vendor Nvarchar(30) COLLATE database_default,
    VendorName Nvarchar(100) COLLATE database_default,
    Account Nvarchar(13) COLLATE database_default,
    AccountName Nvarchar(40) COLLATE database_default,
    AccountTypeCode smallint,
    AccountType Nvarchar(255) COLLATE database_default,
    ExpBillRate decimal(19,4),
    StartDate datetime,
    EndDate datetime,
    PlannedExpCost decimal(19,4),
    PlannedReimExpCost decimal(19,4),
    PlannedDirExpCost decimal(19,4),
    PlannedIndirExpCost decimal(19,4),
    PlannedExpBill decimal(19,4),
    PlannedReimExpBill decimal(19,4),
    PlannedDirExpBill decimal(19,4),
    PlannedIndirExpBill decimal(19,4),
    BaselineExpCost decimal(19,4),
    BaselineExpBill decimal(19,4),
    JTDExpCost decimal(19,4),
    JTDReimExpCost decimal(19,4),
    JTDDirExpCost decimal(19,4),
    JTDIndirExpCost decimal(19,4),
    JTDExpBill decimal(19,4),
    JTDReimExpBill decimal(19,4),
    JTDDirExpBill decimal(19,4),
    JTDIndirExpBill decimal(19,4),
    ETCExpCost decimal(19,4),
    ETCExpBill decimal(19,4)
  )

BEGIN

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
  
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint
 
  DECLARE @strVorN char(1)
 
  -- Declare Temp tables.

  DECLARE @tabMappedTask TABLE
    (PlanID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(7) COLLATE database_default,
     WBS3 Nvarchar(7) COLLATE database_default,
     WBSType varchar(4) COLLATE database_default,
     ParentOutlineNumber varchar(255) COLLATE database_default,
     OutlineNumber varchar(255) COLLATE database_default,
     OutlineLevel int
     UNIQUE(PlanID, TaskID))

  DECLARE @tabExpense TABLE
    (PlanID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     ExpenseID varchar(32) COLLATE database_default,
     WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(7) COLLATE database_default,
     WBS3 Nvarchar(7) COLLATE database_default,
     Account Nvarchar(13) COLLATE database_default,
     Vendor Nvarchar(30) COLLATE database_default,
     ExpBillRate decimal(19,4),
     BaselineExpCost decimal(19,4),
     BaselineExpBill decimal(19,4)
     UNIQUE(PlanID, TaskID, ExpenseID, Account, Vendor, WBS1, WBS2, WBS3))

  DECLARE @tabRes TABLE
    (RowID  int IDENTITY(1,1),
     AccountName Nvarchar(40) COLLATE database_default,
     VendorName Nvarchar(100) COLLATE database_default,
     Account Nvarchar(13) COLLATE database_default,
     AccountTypeCode smallint,
     AccountType Nvarchar(255) COLLATE database_default,
     Vendor Nvarchar(30) COLLATE database_default
     UNIQUE(RowID, Account, Vendor))
        
  DECLARE @tabPExp TABLE
    (RowID  int IDENTITY(1,1),
     WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(7) COLLATE database_default,
     WBS3 Nvarchar(7) COLLATE database_default,
     Account Nvarchar(13) COLLATE database_default,
     Vendor Nvarchar(30) COLLATE database_default,
     StartDate datetime,
     EndDate datetime,
     PlannedExpCost decimal(19,4),
     PlannedReimExpCost decimal(19,4),
     PlannedDirExpCost decimal(19,4),
     PlannedIndirExpCost decimal(19,4),
     PlannedExpBill decimal(19,4),
     PlannedReimExpBill decimal(19,4),
     PlannedDirExpBill decimal(19,4),
     PlannedIndirExpBill decimal(19,4)
     UNIQUE(RowID, WBS1, WBS2, WBS3, Account, Vendor))
    
 	DECLARE @tabLedger TABLE 
    (RowID  int IDENTITY(1,1),
     WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(7) COLLATE database_default,
     WBS3 Nvarchar(7) COLLATE database_default,
     Account Nvarchar(13) COLLATE database_default,
     Vendor Nvarchar(30) COLLATE database_default,
     JTDExpCost decimal(19,4),
     JTDReimExpCost decimal(19,4),
     JTDDirExpCost decimal(19,4),
     JTDIndirExpCost decimal(19,4),
     JTDExpBill decimal(19,4),
     JTDReimExpBill decimal(19,4),
     JTDDirExpBill decimal(19,4),
     JTDIndirExpBill decimal(19,4)
     UNIQUE(RowID, WBS1, WBS2, WBS3, Account, Vendor))

 	DECLARE @tabETC TABLE (
    RowID  int IDENTITY(1,1),
    Account Nvarchar(13) COLLATE database_default,
    Vendor Nvarchar(30) COLLATE database_default,
    ETCExpCost decimal(19,4),
    ETCExpBill decimal(19,4)
    UNIQUE(RowID, Account, Vendor)
  )


-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = ReportAtBillingInBillingCurr,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat     

  -- Save Company string for use later.
  
  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          
  -- Set JTD/ETC Dates.
  
  SET @dtJTDDate = CONVERT(datetime, @strJTDDate)
  SET @dtETCDate = DATEADD(d, 1, @dtJTDDate) 

  -- Determine which set of tables need to be used.

  SET @strVorN = dbo.PN$PlanIs_VorN(@strWBS1)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	-- Save Expense JTD & Unposted Expense JTD for this Project into temp table.
  -- The JTD data are posted at the leaf node in the WBS Tree. 
  -- Therefore, we need to calculate JTD for the summary levels.

	INSERT @tabLedger(
    WBS1,
    WBS2,
    WBS3,
    Account,
    Vendor,
    JTDExpCost,
    JTDReimExpCost,
    JTDDirExpCost,
    JTDIndirExpCost,
    JTDExpBill,
    JTDReimExpBill,
    JTDDirExpBill,
    JTDIndirExpBill
  )      
		SELECT
      X.WBS1,
      X.WBS2,
      X.WBS3,
      X.Account,
      X.Vendor,
      SUM(ISNULL(JTDExpCost, 0.0000)) AS JTDExpCost,
      SUM(ISNULL(JTDReimExpCost, 0.0000)) AS JTDReimExpCost,
      SUM(ISNULL(JTDDirExpCost, 0.0000)) AS JTDDirExpCost,
      SUM(ISNULL(JTDIndirExpCost, 0.0000)) AS JTDIndirExpCost,
      SUM(ISNULL(JTDExpBill, 0.0000)) AS JTDExpBill,
      SUM(ISNULL(JTDReimExpBill, 0.0000)) AS JTDReimExpBill,
      SUM(ISNULL(JTDDirExpBill, 0.0000)) AS JTDDirExpBill,
      SUM(ISNULL(JTDIndirExpBill, 0.0000)) AS JTDIndirExpBill
      FROM (
        SELECT
          LG.WBS1,
          LG.WBS2,
          LG.WBS3,
          LG.Account,
          LG.Vendor,
          SUM(LG.AmountProjectCurrency) AS JTDExpCost,  
          SUM(CASE WHEN CA.Type = 5 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimExpCost,
          SUM(CASE WHEN CA.Type = 7 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDDirExpCost,
          SUM(CASE WHEN CA.Type = 9 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDIndirExpCost,
          SUM(LG.BillExt) AS JTDExpBill,
          SUM(CASE WHEN CA.Type = 5 THEN LG.BillExt ELSE 0 END) AS JTDReimExpBill,
          SUM(CASE WHEN CA.Type = 7 THEN LG.BillExt ELSE 0 END) AS JTDDirExpBill,
          SUM(CASE WHEN CA.Type = 9 THEN LG.BillExt ELSE 0 END) AS JTDIndirExpBill
          FROM LedgerAR AS LG
            INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
          WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (5, 7, 9) AND
            (LG.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
            (LG.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
          GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
        UNION ALL SELECT
          LG.WBS1,
          LG.WBS2,
          LG.WBS3,
          LG.Account,
          LG.Vendor,
          SUM(LG.AmountProjectCurrency) AS JTDExpCost,  
          SUM(CASE WHEN CA.Type = 5 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimExpCost,
          SUM(CASE WHEN CA.Type = 7 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDDirExpCost,
          SUM(CASE WHEN CA.Type = 9 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDIndirExpCost,
          SUM(LG.BillExt) AS JTDExpBill,
          SUM(CASE WHEN CA.Type = 5 THEN LG.BillExt ELSE 0 END) AS JTDReimExpBill,
          SUM(CASE WHEN CA.Type = 7 THEN LG.BillExt ELSE 0 END) AS JTDDirExpBill,
          SUM(CASE WHEN CA.Type = 9 THEN LG.BillExt ELSE 0 END) AS JTDIndirExpBill
          FROM LedgerAP AS LG
            INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
          WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (5, 7, 9) AND
            (LG.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
            (LG.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
          GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
        UNION ALL SELECT
          LG.WBS1,
          LG.WBS2,
          LG.WBS3,
          LG.Account,
          LG.Vendor,
          SUM(LG.AmountProjectCurrency) AS JTDExpCost,  
          SUM(CASE WHEN CA.Type = 5 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimExpCost,
          SUM(CASE WHEN CA.Type = 7 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDDirExpCost,
          SUM(CASE WHEN CA.Type = 9 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDIndirExpCost,
          SUM(LG.BillExt) AS JTDExpBill,
          SUM(CASE WHEN CA.Type = 5 THEN LG.BillExt ELSE 0 END) AS JTDReimExpBill,
          SUM(CASE WHEN CA.Type = 7 THEN LG.BillExt ELSE 0 END) AS JTDDirExpBill,
          SUM(CASE WHEN CA.Type = 9 THEN LG.BillExt ELSE 0 END) AS JTDIndirExpBill
          FROM LedgerEX AS LG
            INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
          WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (5, 7, 9) AND
            (LG.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
            (LG.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
          GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
        UNION ALL SELECT
          LG.WBS1,
          LG.WBS2,
          LG.WBS3,
          LG.Account,
          LG.Vendor,
          SUM(LG.AmountProjectCurrency) AS JTDExpCost,  
          SUM(CASE WHEN CA.Type = 5 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimExpCost,
          SUM(CASE WHEN CA.Type = 7 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDDirExpCost,
          SUM(CASE WHEN CA.Type = 9 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDIndirExpCost,
          SUM(LG.BillExt) AS JTDExpBill,
          SUM(CASE WHEN CA.Type = 5 THEN LG.BillExt ELSE 0 END) AS JTDReimExpBill,
          SUM(CASE WHEN CA.Type = 7 THEN LG.BillExt ELSE 0 END) AS JTDDirExpBill,
          SUM(CASE WHEN CA.Type = 9 THEN LG.BillExt ELSE 0 END) AS JTDIndirExpBill
          FROM LedgerMISC AS LG
            INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
          WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (5, 7, 9) AND
            (LG.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
            (LG.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
          GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
        UNION ALL SELECT
          POC.WBS1,
          POC.WBS2,
          POC.WBS3,
          POC.Account,
          POM.Vendor,
          SUM(AmountProjectCurrency) AS JTDExpCost,  
          SUM(CASE WHEN CA.Type = 5 THEN AmountProjectCurrency ELSE 0 END) AS JTDReimExpCost,
          SUM(CASE WHEN CA.Type = 7 THEN AmountProjectCurrency ELSE 0 END) AS JTDDirExpCost,
          SUM(CASE WHEN CA.Type = 9 THEN AmountProjectCurrency ELSE 0 END) AS JTDIndirExpCost,
          SUM(BillExt) AS JTDExpBill,
          SUM(CASE WHEN CA.Type = 5 THEN BillExt ELSE 0 END) AS JTDReimExpBill,
          SUM(CASE WHEN CA.Type = 7 THEN BillExt ELSE 0 END) AS JTDDirExpBill,
          SUM(CASE WHEN CA.Type = 9 THEN BillExt ELSE 0 END) AS JTDIndirExpBill
          FROM POCommitment AS POC 
            INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
            INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey
            INNER JOIN CA ON POC.Account = CA.Account
          WHERE POM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (5, 7, 9) AND 
            (AmountProjectCurrency != 0 AND BillExt != 0) AND
            (POC.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
            (POC.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
          GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
        UNION ALL SELECT
          POC.WBS1,
          POC.WBS2,
          POC.WBS3,
          POC.Account,
          POM.Vendor,
          SUM(AmountProjectCurrency) AS JTDExpCost,  
          SUM(CASE WHEN CA.Type = 5 THEN AmountProjectCurrency ELSE 0 END) AS JTDReimExpCost,
          SUM(CASE WHEN CA.Type = 7 THEN AmountProjectCurrency ELSE 0 END) AS JTDDirExpCost,
          SUM(CASE WHEN CA.Type = 9 THEN AmountProjectCurrency ELSE 0 END) AS JTDIndirExpCost,
          SUM(BillExt) AS JTDExpBill,
          SUM(CASE WHEN CA.Type = 5 THEN BillExt ELSE 0 END) AS JTDReimExpBill,
          SUM(CASE WHEN CA.Type = 7 THEN BillExt ELSE 0 END) AS JTDDirExpBill,
          SUM(CASE WHEN CA.Type = 9 THEN BillExt ELSE 0 END) AS JTDIndirExpBill
          FROM POCommitment AS POC 
            INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
            INNER JOIN POCOMaster AS POCOM ON POD.COPKey = POCOM.PKey
            INNER JOIN POMaster AS POM ON POCOM.MasterPKey = POM.MasterPKey
            INNER JOIN CA ON POC.Account = CA.Account
          WHERE POCOM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (5, 7, 9) AND 
            (AmountProjectCurrency != 0 AND BillExt != 0) AND
            (POC.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
            (POC.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
          GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
      ) AS X
      GROUP BY X.WBS1, X.WBS2, X.WBS3, X.Account, X.Vendor
      HAVING SUM(ISNULL(JTDExpCost, 0.0000)) <> 0 OR SUM(ISNULL(JTDExpBill, 0.0000)) <> 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Extract Expense from Plans that are marked as "Included in Utilization".
  -- There could be multiple Plans with RPPlan.UlilizationIncludeFlag = 'Y'

  -- Calculate Resource Planned.
  -- Calculate Resource Baseline.
  -- It seems that having a SUM and GROUP BY in the following SQL would degrade performance substantially.

  IF (@strVorN = 'V')
    BEGIN

      INSERT @tabMappedTask
        (PlanID,
         TaskID,
         WBS1,
         WBS2,
         WBS3,
         WBSType,
         ParentOutlineNumber,
         OutlineNumber,
         OutlineLevel
        )
        SELECT
          P.PlanID,
          T.TaskID,
          T.WBS1,
          ISNULL(T.WBS2, ' '),
          ISNULL(T.WBS3, ' '),
          T.WBSType,
          T.ParentOutlineNumber,
          T.OutlineNumber,
          T.OutlineLevel
          FROM RPTask AS T
            INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND 
              T.WBS1 = @strWBS1 AND ISNULL(T.WBS2, ' ') = @strWBS2 AND ISNULL(T.WBS3, ' ') = @strWBS3

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabExpense
        (PlanID,
         TaskID,
         ExpenseID,
         WBS1,
         WBS2,
         WBS3,
         Account,
         Vendor,
         ExpBillRate,
         BaselineExpCost,
         BaselineExpBill
        )
        SELECT DISTINCT
          E.PlanID,
          E.TaskID,
          E.ExpenseID,
          E.WBS1,
          ISNULL(E.WBS2, ' '),
          ISNULL(E.WBS3, ' '),
          E.Account AS Account,
          E.Vendor AS Vendor,
          E.ExpBillRate AS ExpBillRate,
          E.BaselineExpCost AS BaselineExpCost,
          E.BaselineExpBill AS BaselineExpBill
          FROM RPExpense AS E
            INNER JOIN RPTask AS T ON E.PlanID = T.PlanID AND E.TaskID = T.TaskID
            INNER JOIN @tabMappedTask AS MT ON E.PlanID = MT.PlanID AND T.OutlineNumber LIKE (MT.OutlineNumber + '%')

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabPExp (
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor,
        StartDate,
        EndDate,
        PlannedExpCost,
        PlannedReimExpCost,
        PlannedDirExpCost,
        PlannedIndirExpCost,
        PlannedExpBill,
        PlannedReimExpBill,
        PlannedDirExpBill,
        PlannedIndirExpBill
      )
        SELECT
          X.WBS1,
          X.WBS2,
          X.WBS3,
          X.Account AS Account,
          X.Vendor AS Vendor,
          X.StartDate AS StartDate,
          X.EndDate AS EndDate,
          X.PeriodCost AS PlannedExpCost,
          X.PeriodReimCost AS PlannedReimExpCost,
          X.PeriodDirCost AS PlannedDirExpCost,
          X.PeriodIndirCost AS PlannedIndirExpCost,
          X.PeriodBill AS PlannedExpBill,
          X.PeriodReimBill AS PlannedReimExpBill,
          X.PeriodDirBill AS PlannedDirExpBill,
          X.PeriodIndirBill AS PlannedIndirExpBill
          FROM (
            SELECT
              E.WBS1 AS WBS1,
              E.WBS2 AS WBS2,
              E.WBS3 AS WBS3,
              E.Account AS Account,
              E.Vendor AS Vendor,
              TPD.StartDate, 
              TPD.EndDate, 
              ISNULL(TPD.PeriodCost, 0) AS PeriodCost,
              CASE WHEN CA.Type = 5 THEN ISNULL(TPD.PeriodCost, 0) ELSE 0 END AS PeriodReimCost,
              CASE WHEN CA.Type = 7 THEN ISNULL(TPD.PeriodCost, 0) ELSE 0 END AS PeriodDirCost,
              CASE WHEN CA.Type = 9 THEN ISNULL(TPD.PeriodCost, 0) ELSE 0 END AS PeriodIndirCost,
              ISNULL(TPD.PeriodBill, 0) AS PeriodBill,
              CASE WHEN CA.Type = 5 THEN ISNULL(TPD.PeriodBill, 0) ELSE 0 END AS PeriodReimBill,
              CASE WHEN CA.Type = 7 THEN ISNULL(TPD.PeriodBill, 0) ELSE 0 END AS PeriodDirBill,
              CASE WHEN CA.Type = 9 THEN ISNULL(TPD.PeriodBill, 0) ELSE 0 END AS PeriodIndirBill
              FROM @tabExpense AS E
                INNER JOIN RPPlannedExpenses AS TPD ON E.PlanID = TPD.PlanID AND E.TaskID = TPD.TaskID AND E.ExpenseID = TPD.ExpenseID
                INNER JOIN CA ON E.Account = CA.Account
          ) AS X 

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabETC (
        Account,
        Vendor,
        ETCExpCost,
        ETCExpBill
      )
        SELECT
          Account AS Account,
          Vendor AS Vendor,
          ETCExpCost AS ETCExpCost,
          ETCExpBill AS ETCExpBill
          FROM (
            SELECT
              Account AS Account,
              Vendor AS Vendor,
              SUM(ETCExpCost) AS ETCExpCost,
              SUM(ETCExpBill) AS ETCExpBill
              FROM (
                SELECT
                  Account,
                  Vendor,
                  CASE WHEN StartDate >= @dtETCDate THEN PlannedExpCost
	                     ELSE PlannedExpCost * dbo.DLTK$ProrateRatio(@dtETCDate, EndDate, StartDate, EndDate, @strCompany) END AS ETCExpCost,
                  CASE WHEN StartDate >= @dtETCDate THEN PlannedExpBill
	                     ELSE PlannedExpBill * dbo.DLTK$ProrateRatio(@dtETCDate, EndDate, StartDate, EndDate, @strCompany) END AS ETCExpBill
                  FROM @tabPExp AS TPD
                  WHERE TPD.EndDate >= @dtETCDate
              ) AS XA
              GROUP BY Account, Vendor
          ) AS X
 
    END /* End If (@strVorN = 'V') */
  ELSE IF (@strVorN = 'N')
    BEGIN

      INSERT @tabMappedTask
        (PlanID,
         TaskID,
         WBS1,
         WBS2,
         WBS3,
         WBSType,
         ParentOutlineNumber,
         OutlineNumber,
         OutlineLevel
        )
        SELECT
          P.PlanID,
          T.TaskID,
          T.WBS1,
          ISNULL(T.WBS2, ' '),
          ISNULL(T.WBS3, ' '),
          T.WBSType,
          T.ParentOutlineNumber,
          T.OutlineNumber,
          T.OutlineLevel
          FROM PNTask AS T
            INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND 
              T.WBS1 = @strWBS1 AND ISNULL(T.WBS2, ' ') = @strWBS2 AND ISNULL(T.WBS3, ' ') = @strWBS3

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabExpense
        (PlanID,
         TaskID,
         ExpenseID,
         WBS1,
         WBS2,
         WBS3,
         Account,
         Vendor,
         ExpBillRate,
         BaselineExpCost,
         BaselineExpBill
        )
        SELECT DISTINCT
          E.PlanID,
          E.TaskID,
          E.ExpenseID,
          E.WBS1,
          ISNULL(E.WBS2, ' '),
          ISNULL(E.WBS3, ' '),
          E.Account AS Account,
          E.Vendor AS Vendor,
          E.ExpBillRate AS ExpBillRate,
          E.BaselineExpCost AS BaselineExpCost,
          E.BaselineExpBill AS BaselineExpBill
          FROM PNExpense AS E
            INNER JOIN PNTask AS T ON E.PlanID = T.PlanID AND E.TaskID = T.TaskID
            INNER JOIN @tabMappedTask AS MT ON E.PlanID = MT.PlanID AND T.OutlineNumber LIKE (MT.OutlineNumber + '%')

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabPExp (
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor,
        StartDate,
        EndDate,
        PlannedExpCost,
        PlannedReimExpCost,
        PlannedDirExpCost,
        PlannedIndirExpCost,
        PlannedExpBill,
        PlannedReimExpBill,
        PlannedDirExpBill,
        PlannedIndirExpBill
      )
        SELECT
          X.WBS1,
          X.WBS2,
          X.WBS3,
          X.Account AS Account,
          X.Vendor AS Vendor,
          X.StartDate AS StartDate,
          X.EndDate AS EndDate,
          X.PeriodCost AS PlannedExpCost,
          X.PeriodReimCost AS PlannedReimExpCost,
          X.PeriodDirCost AS PlannedDirExpCost,
          X.PeriodIndirCost AS PlannedIndirExpCost,
          X.PeriodBill AS PlannedExpBill,
          X.PeriodReimBill AS PlannedReimExpBill,
          X.PeriodDirBill AS PlannedDirExpBill,
          X.PeriodIndirBill AS PlannedIndirExpBill
          FROM (
            SELECT
              E.WBS1 AS WBS1,
              E.WBS2 AS WBS2,
              E.WBS3 AS WBS3,
              E.Account AS Account,
              E.Vendor AS Vendor,
              TPD.StartDate, 
              TPD.EndDate, 
              ISNULL(TPD.PeriodCost, 0) AS PeriodCost,
              CASE WHEN CA.Type = 5 THEN ISNULL(TPD.PeriodCost, 0) ELSE 0 END AS PeriodReimCost,
              CASE WHEN CA.Type = 7 THEN ISNULL(TPD.PeriodCost, 0) ELSE 0 END AS PeriodDirCost,
              CASE WHEN CA.Type = 9 THEN ISNULL(TPD.PeriodCost, 0) ELSE 0 END AS PeriodIndirCost,
              ISNULL(TPD.PeriodBill, 0) AS PeriodBill,
              CASE WHEN CA.Type = 5 THEN ISNULL(TPD.PeriodBill, 0) ELSE 0 END AS PeriodReimBill,
              CASE WHEN CA.Type = 7 THEN ISNULL(TPD.PeriodBill, 0) ELSE 0 END AS PeriodDirBill,
              CASE WHEN CA.Type = 9 THEN ISNULL(TPD.PeriodBill, 0) ELSE 0 END AS PeriodIndirBill
              FROM @tabExpense AS E
                INNER JOIN PNPlannedExpenses AS TPD ON E.PlanID = TPD.PlanID AND E.TaskID = TPD.TaskID AND E.ExpenseID = TPD.ExpenseID 
                INNER JOIN CA ON E.Account = CA.Account
          ) AS X 

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabETC (
        Account,
        Vendor,
        ETCExpCost,
        ETCExpBill
      )
        SELECT
          Account AS Account,
          Vendor AS Vendor,
          SUM(ISNULL(ETCCost, 0.0000)) AS ETCExpCost,
          SUM(ISNULL(ETCBill, 0.0000)) AS ETCExpBill
          FROM (
            SELECT
              R.WBS1 AS WBS1,
              R.WBS2 AS WBS2,
              R.WBS3 AS WBS3,
              R.Account AS Account,
              R.Vendor AS Vendor,
              CASE
                WHEN ISNULL(TPD.PlannedCost, 0.0000) > ISNULL(JTD.JTDCost, 0.0000)
                THEN ISNULL(TPD.PlannedCost, 0.0000) - ISNULL(JTD.JTDCost, 0.0000)
                ELSE 0.0000
              END AS ETCCost,
              CASE
                WHEN ISNULL(TPD.PlannedBill, 0.0000) > ISNULL(JTD.JTDBill, 0.0000)
                THEN ISNULL(TPD.PlannedBill, 0.0000) - ISNULL(JTD.JTDBill, 0.0000)
                ELSE 0.0000
              END AS ETCBill

              FROM (
                SELECT DISTINCT
                  WBS1 AS WBS1,
                  ISNULL(WBS2, ' ') AS WBS2,
                  ISNULL(WBS3, ' ') AS WBS3,
                  Account AS Account,
                  Vendor AS Vendor
                  FROM @tabExpense
              ) AS R

                LEFT JOIN (
                  SELECT
                    WBS1 AS WBS1,
                    ISNULL(WBS2, ' ') AS WBS2,
                    ISNULL(WBS3, ' ') AS WBS3,
                    Account AS Account,
                    Vendor AS Vendor,
                    SUM(PlannedExpCost) AS PlannedCost,
                    SUM(PlannedExpBill) AS PlannedBill
                    FROM @tabPExp
                    GROUP BY WBS1, WBS2, WBS3, Account, Vendor
                ) AS TPD
                  ON R.WBS1 = TPD.WBS1 AND R.WBS2 = TPD.WBS2 AND R.WBS3 = TPD.WBS3 AND R.Account = TPD.Account AND ISNULL(R.Vendor, '|') = ISNULL(TPD.Vendor, '|')

                LEFT JOIN (
                  SELECT
                    WTZ.WBS1 AS WBS1,
                    WTZ.WBS2 AS WBS2,
                    WTZ.WBS3 AS WBS3,
                    JZ.Account AS Account,
                    JZ.Vendor AS Vendor,
                    ISNULL(SUM(JZ.JTDCost), 0.0000) AS JTDCost,
                    ISNULL(SUM(JZ.JTDBill), 0.0000) AS JTDBill
                    FROM @tabExpense AS WTZ
                      OUTER APPLY (
                        SELECT
                          WBS1 AS WBS1,
                          WBS2 AS WBS2,
                          WBS3 AS WBS3,
                          Account AS Account,
                          Vendor AS Vendor,
                          ISNULL(SUM(TPD.JTDExpCost), 0.0000) AS JTDCost,
                          ISNULL(SUM(TPD.JTDExpBill), 0.0000) AS JTDBill
                          FROM @tabLedger AS TPD
                          WHERE TPD.WBS1 = WTZ.WBS1 AND
                            (ISNULL(TPD.WBS2, '|') LIKE CASE WHEN WTZ.WBS2 = ' ' THEN '%' ELSE WTZ.WBS2 END) AND
                            (ISNULL(TPD.WBS3, '|') LIKE CASE WHEN WTZ.WBS3 = ' ' THEN '%' ELSE WTZ.WBS3 END) AND
                            ISNULL(WTZ.Account, '|') = ISNULL(TPD.Account, '|') AND ISNULL(WTZ.Vendor, '|') = ISNULL(TPD.Vendor, '|') 
                          GROUP BY TPD.WBS1, TPD.WBS2, TPD.WBS3, TPD.Account, TPD.Vendor
                      ) AS JZ
                    GROUP BY WTZ.WBS1, WTZ.WBS2, WTZ.WBS3, JZ.Account, JZ.Vendor
                ) AS JTD 
                  ON R.WBS1 = JTD.WBS1 AND R.WBS2 = JTD.WBS2 AND R.WBS3 = JTD.WBS3 AND R.Account = JTD.Account AND ISNULL(R.Vendor, '|') = ISNULL(JTD.Vendor, '|')

          ) AS ETC
            GROUP BY Account, Vendor

    END /* End If (@strVorN = 'N') */
 
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save PlanID and TaskID to be used later

  SELECT @strPlanID = MIN(PlanID), @strTaskID = MIN(TaskID) FROM @tabMappedTask

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save a list of Accounts and Vendors from Expenses for the input WBS1/WBS2/WBS3 combination.
  -- Also save a list of Accounts and Vendors from the Ledger tables.
  -- Need to do this step after populate @tabLedger because we don't want JTD rows with zero Cost and Bill amounts.

  INSERT @tabRes
    (AccountName,
     VendorName,
     Account,
     AccountTypeCode,
     AccountType,
     Vendor)
    SELECT DISTINCT
      X.AccountName,
      X.VendorName,
      X.Account,
      X.AccountTypeCode,
      X.AccountType,
      X.Vendor FROM
      (SELECT DISTINCT
         CA.Name AS AccountName,
         VE.Name AS VendorName,
         C.Account,
         CA.Type AS AccountTypeCode,
         CASE
           WHEN CA.Type = 5 THEN 'Reimb'
           WHEN CA.Type = 7 THEN 'Direct'
           WHEN CA.Type = 9 THEN 'Indirect'
         END AS AccountType,
         C.Vendor
         FROM @tabExpense AS C
           INNER JOIN CA ON C.Account = CA.Account
           LEFT JOIN VE ON C.Vendor = VE.Vendor
       UNION SELECT DISTINCT
         CA.Name AS AccountName,
         VE.Name AS VendorName,
         LG.Account,
         CA.Type AS AccountTypeCode,
         CASE
           WHEN CA.Type = 5 THEN 'Reimb'
           WHEN CA.Type = 7 THEN 'Direct'
           WHEN CA.Type = 9 THEN 'Indirect'
         END AS AccountType,
         LG.Vendor
         FROM @tabLedger AS LG
           INNER JOIN CA ON LG.Account = CA.Account
           LEFT JOIN VE ON LG.Vendor = VE.Vendor
       ) AS X

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Combine data for Resource rows.

  INSERT @tabExpRes
    (PlanID,
     TaskID,
     ExpenseID,
     WBS1,
     WBS2,
     WBS3,
     Vendor,
     VendorName,
     Account,
     AccountName,
     AccountTypeCode,
     AccountType,
     ExpBillRate,
     StartDate,
     EndDate,
     PlannedExpCost,
     PlannedReimExpCost,
     PlannedDirExpCost,
     PlannedIndirExpCost,
     PlannedExpBill,
     PlannedReimExpBill,
     PlannedDirExpBill,
     PlannedIndirExpBill,
     BaselineExpCost,
     BaselineExpBill,
     JTDExpCost,
     JTDReimExpCost,
     JTDDirExpCost,
     JTDIndirExpCost,
     JTDExpBill,
     JTDReimExpBill,
     JTDDirExpBill,
     JTDIndirExpBill,
     ETCExpCost,
     ETCExpBill
    )
    SELECT
      ISNULL(E.PlanID, @strPlanID) AS PlanID,
      ISNULL(E.TaskID, @strTaskID) AS TaskID,
      E.ExpenseID,
      @strWBS1 AS WBS1,
      @strWBS2 AS WBS2,
      @strWBS3 AS WBS3,
      R.Vendor,
      R.VendorName,
      R.Account,
      R.AccountName,
      R.AccountTypeCode,
      R.AccountType,
      E.ExpBillRate,
      PE.StartDate AS StartDate,
      PE.EndDate AS EndDate,
      ISNULL(PE.PlannedExpCost, 0.0000) AS PlannedExpCost,
      ISNULL(PE.PlannedReimExpCost, 0.0000) AS PlannedReimExpCost,
      ISNULL(PE.PlannedDirExpCost, 0.0000) AS PlannedDirExpCost,
      ISNULL(PE.PlannedIndirExpCost, 0.0000) AS PlannedIndirExpCost,
      ISNULL(PE.PlannedExpBill, 0.0000) AS PlannedExpBill,
      ISNULL(PE.PlannedReimExpBill, 0.0000) AS PlannedReimExpBill,
      ISNULL(PE.PlannedDirExpBill, 0.0000) AS PlannedDirExpBill,
      ISNULL(PE.PlannedIndirExpBill, 0.0000) AS PlannedIndirExpBill,
      ISNULL(E.BaselineExpCost, 0.0000) AS BaselineExpCost,
      ISNULL(E.BaselineExpBill, 0.0000) AS BaselineExpBill,
      ISNULL(JTD.JTDExpCost, 0.0000) AS JTDExpCost,
      ISNULL(JTD.JTDReimExpCost, 0.0000) AS JTDReimExpCost,
      ISNULL(JTD.JTDDirExpCost, 0.0000) AS JTDDirExpCost,
      ISNULL(JTD.JTDIndirExpCost, 0.0000) AS JTDIndirExpCost,
      ISNULL(JTD.JTDExpBill, 0.0000) AS JTDExpBill,
      ISNULL(JTD.JTDReimExpBill, 0.0000) AS JTDReimExpBill,
      ISNULL(JTD.JTDDirExpBill, 0.0000) AS JTDDirExpBill,
      ISNULL(JTD.JTDIndirExpBill, 0.0000) AS JTDIndirExpBill,

      ISNULL(ETC.ETCExpCost, 0.0000) AS ETCExpCost,
      ISNULL(ETC.ETCExpBill, 0.0000) AS ETCExpBill

      FROM @tabRes AS R

        LEFT JOIN
          (SELECT 
             MIN(PlanID) AS PlanID,
             MIN(TaskID) AS TaskID,
             MIN(ExpenseID) AS ExpenseID,
             Account, 
             Vendor,
             MIN(ExpBillRate) AS ExpBillRate,
             SUM(ISNULL(BaselineExpCost, 0.0000)) AS BaselineExpCost,
             SUM(ISNULL(BaselineExpBill, 0.0000)) AS BaselineExpBill
             FROM @tabExpense 
             GROUP BY Account, Vendor
          ) AS E ON ISNULL(R.Account, '|') = ISNULL(E.Account, '|') AND ISNULL(R.Vendor, '|') = ISNULL(E.Vendor, '|')

        LEFT JOIN
          (SELECT 
             Account, 
             Vendor, 
             MIN(PX.StartDate) AS StartDate,
             MAX(PX.EndDate) AS EndDate,
             SUM(ISNULL(PX.PlannedExpCost, 0.0000)) AS PlannedExpCost,
             SUM(ISNULL(PX.PlannedReimExpCost, 0.0000)) AS PlannedReimExpCost,
             SUM(ISNULL(PX.PlannedDirExpCost, 0.0000)) AS PlannedDirExpCost,
             SUM(ISNULL(PX.PlannedIndirExpCost, 0.0000)) AS PlannedIndirExpCost,
             SUM(ISNULL(PX.PlannedExpBill, 0.0000)) AS PlannedExpBill,
             SUM(ISNULL(PX.PlannedReimExpBill, 0.0000)) AS PlannedReimExpBill,
             SUM(ISNULL(PX.PlannedDirExpBill, 0.0000)) AS PlannedDirExpBill,
             SUM(ISNULL(PX.PlannedIndirExpBill, 0.0000)) AS PlannedIndirExpBill
             FROM @tabPExp AS PX
             GROUP BY Account, Vendor
          ) AS PE ON ISNULL(R.Account, '|') = ISNULL(PE.Account, '|') AND ISNULL(R.Vendor, '|') = ISNULL(PE.Vendor, '|')

        LEFT JOIN (
          SELECT
            Account, 
            Vendor,
            ISNULL(SUM(JX.JTDExpCost), 0.0000) AS JTDExpCost,
            ISNULL(SUM(JX.JTDReimExpCost), 0.0000) AS JTDReimExpCost,
            ISNULL(SUM(JX.JTDDirExpCost), 0.0000) AS JTDDirExpCost,
            ISNULL(SUM(JX.JTDIndirExpCost), 0.0000) AS JTDIndirExpCost,
            ISNULL(SUM(JX.JTDExpBill), 0.0000) AS JTDExpBill,
            ISNULL(SUM(JX.JTDReimExpBill), 0.0000) AS JTDReimExpBill,
            ISNULL(SUM(JX.JTDDirExpBill), 0.0000) AS JTDDirExpBill,
            ISNULL(SUM(JX.JTDIndirExpBill), 0.0000) AS JTDIndirExpBill
            FROM @tabLedger AS JX
            GROUP BY Account, Vendor
        ) AS JTD ON ISNULL(R.Account, '|') = ISNULL(JTD.Account, '|') AND ISNULL(R.Vendor, '|') = ISNULL(JTD.Vendor, '|')

        LEFT JOIN @tabETC AS ETC ON ISNULL(R.Account, '|') = ISNULL(ETC.Account, '|') AND ISNULL(R.Vendor, '|') = ISNULL(ETC.Vendor, '|')

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
