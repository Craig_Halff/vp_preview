SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_JTDSpentLink] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS varchar(300)
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select dbo.fnCCG_EI_SpentLink('2003005.xx', ' ', ' ')
*/
	declare @res varchar(300)
	select @res = '@' + Replace(AppURL,'VisionClient','Vision')  + '/EI_NValue.aspx?WBS1=' + @WBS1 + '&WBS2=' + @WBS2 + '&WBS3=' + @WBS3 + 
		'&T=JTD%20Details%20at%20Billing' +
		'&NV=3' +
		'&HD=Employee/Account' +
		'&H1=Hours' +
		'&H2=Billing' +
		'&H3=Details' +
		'&FN=fnCCG_EI_JTDSpentDetails' +
		'&DS=' + @@SERVERNAME + '&DB=' + DB_NAME()
		from CFGSystem
	return @res
END
GO
