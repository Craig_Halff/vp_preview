SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Function [dbo].[fnCCG_UserEmployeeNo] (@User varchar(32)) RETURNS varchar(150)
/*
Copyright 2019 (c) Central Consulting Group, Inc. All rights reserved.
11/19/2019	David Springer
			Return the employee's number associated with a user account.
*/
BEGIN
DECLARE @EmployeeNo varchar (32)

  Select @EmployeeNo = u.Employee
  From SEUser u
  Where u.Username = @User

  RETURN @EmployeeNo

-- Proper syntax
-- (Select dbo.fnCCG_UserEmployeeNo ('JVONDRA'))
END
GO
