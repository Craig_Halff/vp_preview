SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_ProgressReportEditDate] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), @USERNAME varchar(32))
RETURNS datetime
AS BEGIN
/*
	Copyright (c) 2020 EleVia Software.  All rights reserved.
*/
	declare @res datetime
	select @res = CustProgressReportEditDate from ProjectCustomTabFields where wbs1 = @wbs1 and wbs2 = @wbs2 and wbs3 = @wbs3
	return @res
END
GO
