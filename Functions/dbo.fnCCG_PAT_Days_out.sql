SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_PAT_Days_out] (@PayableSeq int, @WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS @T TABLE 
(
      ColValue int, 
      ColDetail varchar(100)
)
AS BEGIN

	declare @dt date, @res1 varchar(10), @detail varchar(100), @days int

	SELECT @dt =  min(dt)
		FROM CCG_PAT_Payable as pat
		left join (SELECT createdatetime as dt, PayableSeq as pseq  FROM CCG_PAT_Pending ) pcs on pat.seq = pcs.pseq
		where dt is not null and pat.seq = @PayableSeq
	
	set @days = datediff(day,@dt,getdate())


	if @days  <= 0
		begin
			set @days = null 
			set @detail = NULL
		end
	else
		begin
			if @days > 7
				set @detail = 'BackgroundColor=#ffc0c0'

		end
				

	insert into @T values (@days,@detail)
	return 
end
GO
