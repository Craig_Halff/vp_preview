SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   FUNCTION [dbo].[fnCCG_PAT_RegexMatch]
(
	@pattern		Nvarchar(2000),
    @matchstring	Nvarchar(max)
)
RETURNS INT
AS BEGIN
	DECLARE @match BIT;
	SET @match = 1;
    RETURN @match;

END;
GO
