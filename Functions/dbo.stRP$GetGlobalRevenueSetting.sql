SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$GetGlobalRevenueSetting] (
)
  RETURNS  varchar(1)
  
  BEGIN -- Function stRP$GetGlobalRevenueSetting

  DECLARE @strPlanID varchar(1)
  DECLARE @strRevenueSetting varchar(1) -- G = Gross Revenue, I = Net Revenue Include Direct Consultants, E = Net Revenue Exclude Direct Consultants, L = Labor Revenue
  DECLARE @strCustGlobalRevenueSetting varchar(20) = 'NotDefined' -- Gross = Gross Revenue, incl or with = Net Revenue Include Direct Consultants, excl or without = Net Revenue Exclude Direct Consultants, Labor = Labor Revenue, NotDefined = the custom field is not defined 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  ---Note that in Vision the UDF field is defined in ProjectPlan, but after conversion, it is transferred to Projects. 
  ---Since the function is used for Vision when getting conversion information, also it is used during conversion process.

  SELECT @strCustGlobalRevenueSetting = DefaultValue, 
  @strRevenueSetting = CASE WHEN DefaultValue like '%incl%' OR (DefaultValue like '%with%' AND DefaultValue  NOT like '%without%')  THEN 'I'
					   WHEN DefaultValue like '%excl%' OR DefaultValue like '%without%' THEN 'E'
					   WHEN DefaultValue like '%Labor%' THEN 'L'
					   ELSE 'G'
					   END 
  FROM FW_CustomColumnsData WHERE (InfocenterArea = 'ProjectPlan' OR InfocenterArea = 'Projects') AND NAME = 'CustGlobalRevenueSetting'

  IF @strCustGlobalRevenueSetting = 'NotDefined' -- means the custom field is not setup by the client
	  BEGIN
		-- (If expense, consultant, and units planning tabs disabled and plan compensation values by row and period is off in every company)
		-- or (If expense, consultant, and units planning tabs disabled for every company 
		------ and plan compensation values by row and period is on in any company 
		------ and FeeDirLab or FeeDirLab Bill columns are only Fee... columns set to show in Configuration>Planning Grids. )
		SET @strRevenueSetting = 'N'
		Select @strRevenueSetting = 'L' FROM CFGResourcePlanning
			WHERE ( EXISTS (SELECT * FROM CFGResourcePlanning WHERE ExpTab = 'N' AND ConTab = 'N' AND UntTab = 'N')
					AND NOT EXISTS (SELECT * FROM CFGResourcePlanning WHERE FeesByPeriodFlg = 'Y') )
				OR ( NOT EXISTS (SELECT * FROM CFGResourcePlanning WHERE ExpTab = 'Y' OR ConTab = 'Y' OR UntTab = 'Y')
				AND EXISTS (SELECT * FROM CFGResourcePlanning WHERE FeesByPeriodFlg = 'Y') 
				AND EXISTS (SELECT * FROM RPGridCfg WHERE Employee = '<Default>' 
				AND (CompDirLabCost = 'Y' OR CompDirLabBill = 'Y')
				AND CompDirExpCost <> 'Y' AND CompDirExpBill <> 'Y'
				AND ReimConCost <> 'Y' AND ReimConBill <> 'Y'
				AND ReimExpCost <> 'Y' AND ReimExpBill <> 'Y'
				AND ConsCost <> 'Y' AND ConsBill <> 'Y') )
	  END
  IF @strRevenueSetting = 'N' -- means above sql returns no record
	BEGIN
		SET @strRevenueSetting = 'G'
	END
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN @strRevenueSetting

END -- stRP$GetGlobalRevenueSetting
GO
