SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_GetBusinessDays](@StartDate datetime, @EndDate datetime, @Company AS Nvarchar(14))
RETURNS float
AS BEGIN
/* OLD WAY - Didn't handle weekend start/end points correctly
	declare @Minutes float
	select @Minutes =
		DATEDIFF(minute,@StartDate,@EndDate)
		- DATEDIFF(wk,@StartDate,@EndDate) * 2880
		- case
			when DATENAME(dw, @StartDate) = 'Friday'    AND DATENAME(dw, @EndDate) =  'Sunday'   then -1440
			when DATENAME(dw, @StartDate) <> 'Saturday' AND DATENAME(dw, @EndDate) =  'Saturday' then  1440
			when DATENAME(dw, @StartDate) = 'Saturday'  AND DATENAME(dw, @EndDate) <> 'Saturday' then -1440
			else 0
		END
		- (select COUNT(*) * 1440 from CFGHoliday where HolidayDate BETWEEN @StartDate AND @EndDate
			AND DATENAME(dw, holidaydate) <> 'Saturday' AND DATENAME(dw, holidaydate) <> 'Sunday'
			AND Company = @Company)
*/
	Declare @WorkMin int = 0   -- Initialize counter
	Declare @Reverse bit       -- Flag to hold if direction is reverse
	Declare @StartHour int = 0   -- Start of business hours (can be supplied as an argument if needed)
	Declare @EndHour int = 24    -- End of business hours (can be supplied as an argument if needed)
	Declare @Holidays Table (HDate DateTime)   --  Table variable to hold holidays

	-- if dates are in reverse order, switch them and set flag
	if @StartDate>@EndDate
	Begin
		Declare @TempDate DateTime=@StartDate
		Set @StartDate=@EndDate
		Set @EndDate=@TempDate
		Set @Reverse=1
	End
	Else Set @Reverse = 0

	-- Get country holidays from table based on the country code
	insert into @Holidays (HDate) select HolidayDate as HDate from CFGHoliday Where Company=@Company and HolidayDate>=DateAdd(dd, DateDiff(dd,0,@StartDate), 0)

	if DatePart(HH, @StartDate)<@StartHour Set @StartDate = DateAdd(hour, @StartHour, DateDiff(DAY, 0, @StartDate))  -- if Start time is less than start hour, set it to start hour
	if DatePart(HH, @StartDate)>=@EndHour+1 Set @StartDate = DateAdd(hour, @StartHour+24, DateDiff(DAY, 0, @StartDate)) -- if Start time is after end hour, set it to start hour of next day
	if DatePart(HH, @EndDate)>=@EndHour+1 Set @EndDate = DateAdd(hour, @EndHour, DateDiff(DAY, 0, @EndDate)) -- if End time is after end hour, set it to end hour
	if DatePart(HH, @EndDate)<@StartHour Set @EndDate = DateAdd(hour, @EndHour-24, DateDiff(DAY, 0, @EndDate)) -- if End time is before start hour, set it to end hour of previous day

	if @StartDate>@EndDate Return 0

	-- if Start and End is on same day
	if DateDiff(Day,@StartDate,@EndDate) <= 0
	Begin
		if Datepart(dw,@StartDate)>1 And DATEPART(dw,@StartDate)<7  -- if day is between sunday and saturday
			if (select Count(*) From @Holidays Where HDATE=DateAdd(dd, DateDiff(dd,0,@StartDate), 0)) = 0  -- if day is not a holiday
				if @EndDate<@StartDate Return 0 Else Set @WorkMin=DATEDIFF(MI, @StartDate, @EndDate) -- Calculate difference
			Else Return 0
		Else Return 0
	End
	Else Begin
		Declare @Partial int=1   -- Set partial day flag
		While DateDiff(Day,@StartDate,@EndDate) > 0   -- While start and end days are different
		Begin
			if Datepart(dw,@StartDate)>1 And DATEPART(dw,@StartDate)<7    --  if this is a weekday
			Begin
				if (select Count(*) From @Holidays Where HDATE=DateAdd(dd, DateDiff(dd,0,@StartDate), 0)) = 0  -- if this is not a holiday
				Begin
					if @Partial=1  -- if this is the first iteration, calculate partial time
					Begin
						Set @WorkMin=@WorkMin + DATEDIFF(MI, @StartDate, DateAdd(hour, @EndHour, DateDiff(DAY, 0, @StartDate)))
						Set @StartDate=DateAdd(hour, @StartHour+24, DateDiff(DAY, 0, @StartDate))
						Set @Partial=0
					End
					Else Begin      -- if this is a full day, add full minutes
						Set @WorkMin=@WorkMin + (@EndHour-@StartHour)*60
						Set @StartDate = DATEADD(DD,1,@StartDate)
					End
				End
				Else Set @StartDate = DATEADD(DD,1,@StartDate)
			End
			Else Set @StartDate = DATEADD(DD,1,@StartDate)
		End
		if Datepart(dw,@StartDate)>1 And DATEPART(dw,@StartDate)<7  -- if last day is a weekday
			if (select Count(*) From @Holidays Where HDATE=DateAdd(dd, DateDiff(dd,0,@StartDate), 0)) = 0   -- And it is not a holiday
				if @Partial=0 Set @WorkMin=@WorkMin + DATEDIFF(MI, @StartDate, @EndDate) Else Set @WorkMin=@WorkMin + DATEDIFF(MI, DateAdd(hour, @StartHour, DateDiff(DAY, 0, @StartDate)), @EndDate)
	End
	if @Reverse=1 Set @WorkMin=-@WorkMin
	return @WorkMin
END
GO
