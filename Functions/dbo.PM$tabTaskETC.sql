SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PM$tabTaskETC](
  @strPlanID varchar(32),
  @strETCDate VARCHAR(8) -- Date must be in format: 'yyyymmdd' regardless of UI Culture.
)
  RETURNS @tabTaskETC TABLE (
    TaskID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ETCLabHrs decimal(19,4),
    ETCLabCost decimal(19,4),
    ETCLabBill decimal(19,4),
    ETCExpCost decimal(19,4),
    ETCExpBill decimal(19,4),
    ETCDirExpCost decimal(19,4),
    ETCDirExpBill decimal(19,4),
    ETCReimExpCost decimal(19,4),
    ETCReimExpBill decimal(19,4),
    ETCConCost decimal(19,4),
    ETCConBill decimal(19,4),
    ETCDirConCost decimal(19,4),
    ETCDirConBill decimal(19,4),
    ETCReimConCost decimal(19,4),
    ETCReimConBill decimal(19,4),
    ETCUntQty decimal(19,4),
    ETCUntCost decimal(19,4),
    ETCUntBill decimal(19,4)
  )

BEGIN

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
 
  DECLARE @strOutlineNumber varchar(255)

  DECLARE @strCompany Nvarchar(14)
  DECLARE @strWBS1 Nvarchar(30)
  DECLARE @strVorN varchar(1)

  DECLARE @strExpTab  varchar(1)
  DECLARE @strConTab  varchar(1)
  DECLARE @strUntTab varchar(1)

  DECLARE @intHrDecimals int
  DECLARE @intQtyDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  
  DECLARE @intLabTPDCount int
  DECLARE @intExpTPDCount int
  DECLARE @intConTPDCount int
  DECLARE @intUntTPDCount int

  DECLARE @tabLabETC TABLE(
    TaskID varchar(32) COLLATE database_default,
    ETCLabHrs decimal(19,4),
    ETCLabCost decimal(19,4),
    ETCLabBill decimal(19,4)
    PRIMARY KEY(TaskID)
  )

  DECLARE @tabExpETC TABLE(
    TaskID varchar(32) COLLATE database_default,
    ETCExpCost decimal(19,4),
    ETCExpBill decimal(19,4),
    ETCDirExpCost decimal(19,4),
    ETCDirExpBill decimal(19,4),
    ETCReimExpCost decimal(19,4),
    ETCReimExpBill decimal(19,4)
    PRIMARY KEY(TaskID)
  )    

  DECLARE @tabConETC TABLE(
    TaskID varchar(32) COLLATE database_default,
    ETCConCost decimal(19,4),
    ETCConBill decimal(19,4),
    ETCDirConCost decimal(19,4),
    ETCDirConBill decimal(19,4),
    ETCReimConCost decimal(19,4),
    ETCReimConBill decimal(19,4)
    PRIMARY KEY(TaskID)
  )    

  DECLARE @tabUntETC TABLE(
    TaskID varchar(32) COLLATE database_default,
    ETCUntQty decimal(19,4),
    ETCUntCost decimal(19,4),
    ETCUntBill decimal(19,4)
    PRIMARY KEY(TaskID)
  )    

 	DECLARE @tabExpJTD TABLE(
    RowID int IDENTITY(1,1),
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    Account Nvarchar(13) COLLATE database_default,
    Vendor Nvarchar(30) COLLATE database_default,
    JTDExpCost decimal(19,4),
    JTDExpBill decimal(19,4),
    JTDDirExpCost decimal(19,4),
    JTDDirExpBill decimal(19,4),
    JTDReimExpCost decimal(19,4),
    JTDReimExpBill decimal(19,4)
    UNIQUE(RowID, WBS1, WBS2, WBS3, Account, Vendor)
  )

  DECLARE @tabConJTD TABLE(
    RowID int IDENTITY(1,1),
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    Account Nvarchar(13) COLLATE database_default,
    Vendor Nvarchar(30) COLLATE database_default,
    JTDConCost decimal(19,4),
    JTDConBill decimal(19,4),
    JTDDirConCost decimal(19,4),
    JTDDirConBill decimal(19,4),
    JTDReimConCost decimal(19,4),
    JTDReimConBill decimal(19,4)
    UNIQUE(RowID, WBS1, WBS2, WBS3, Account, Vendor)
  )

 	DECLARE @tabLeafNavExpETC TABLE(
    RowID int IDENTITY(1,1),
    OutlineNumber varchar(255) COLLATE database_default,
    ETCExpCost decimal(19,4),
    ETCExpBill decimal(19,4),
    ETCDirExpCost decimal(19,4),
    ETCDirExpBill decimal(19,4),
    ETCReimExpCost decimal(19,4),
    ETCReimExpBill decimal(19,4)
    UNIQUE(RowID, OutlineNumber)
  )
       
 	DECLARE @tabLeafNavConETC TABLE(
    RowID int IDENTITY(1,1),
    OutlineNumber varchar(255) COLLATE database_default,
    ETCConCost decimal(19,4),
    ETCConBill decimal(19,4),
    ETCDirConCost decimal(19,4),
    ETCDirConBill decimal(19,4),
    ETCReimConCost decimal(19,4),
    ETCReimConBill decimal(19,4)
    UNIQUE(RowID, OutlineNumber)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Set ETC Date
  
  SET @dtETCDate = CONVERT(datetime, @strETCDate) 
  SET @dtJTDDate = DATEADD(d, -1, @dtETCDate)

  -- Get decimal settings.

/*
  SELECT 
    @intHrDecimals = HrDecimals,
    @intQtyDecimals = QtyDecimals,
    @intAmtCostDecimals = AmtCostDecimals,
    @intAmtBillDecimals = AmtBillDecimals
    FROM dbo.RP$tabDecimals(@strPlanID)
*/

  SELECT 
    @intHrDecimals = 2,
    @intQtyDecimals = 2,
    @intAmtCostDecimals = 2,
    @intAmtBillDecimals = 2

  -- Get Plan Company

  SELECT 
    @strCompany = Company,
    @strWBS1 = WBS1
    FROM RPPlan WHERE PlanID = @strPlanID
    
  -- Determine whether the Project has a Navigator Plan.

  SET @strVorN = 
    CASE 
      WHEN EXISTS(
        SELECT 'X' 
          FROM RPPlan AS V
            INNER JOIN PNPlan AS N ON V.PlanID = N.PlanID AND V.WBS1 = N.WBS1 
          WHERE V.PlanID = @strPlanID AND V.UtilizationIncludeFlg = 'Y'
      ) 
      THEN 'N' ELSE 'V' 
    END

  -- Get flags to determine which features are being used.
  
  SELECT
    @strExpTab = ExpTab,
    @strConTab = ConTab,
    @strUntTab = UntTab
    FROM CFGResourcePlanning
    WHERE CFGResourcePlanning.Company = @strCompany

  -- Determine what kinds of TPD data that the Plan has.

  SET @intExpTPDCount = 0 
  SET @intConTPDCount = 0
  SET @intUntTPDCount = 0

  SELECT @intLabTPDCount = COUNT(*) FROM RPPlannedLabor WHERE PlanID = @strPlanID
  IF (@strExpTab = 'Y') SELECT @intExpTPDCount = COUNT(*) FROM RPPlannedExpenses WHERE PlanID = @strPlanID
  IF (@strConTab = 'Y') SELECT @intConTPDCount = COUNT(*) FROM RPPlannedConsultant WHERE PlanID = @strPlanID
  IF (@strUntTab = 'Y') SELECT @intUntTPDCount = COUNT(*) FROM RPPlannedUnit WHERE PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Calculate Labor ETC.

  IF (@intLabTPDCount > 0)
    BEGIN

      INSERT @tabLabETC(
        TaskID,
        ETCLabHrs,
        ETCLabCost,
        ETCLabBill
      )
        SELECT
          X.TaskID AS TaskID,
          SUM(X.ETCLabHrs) AS ETCLabHrs,
          SUM(X.ETCLabCost) AS ETCLabCost,
          SUM(X.ETCLabBill) AS ETCLabBill
          FROM (
            SELECT
              TZ.TaskID AS TaskID,
              Z.ETCLabHrs AS ETCLabHrs,
              Z.ETCLabCost AS ETCLabCost,
              Z.ETCLabBill AS ETCLabBill
              FROM RPTask AS TZ
                INNER JOIN (
                  /* Find TPD rows attached to leaf RPTask rows */
                  SELECT  
                    PT.OutlineNumber AS OutlineNumber, 
                    ROUND(ISNULL(SUM(
                      CASE 
                        WHEN TPD.StartDate >= @dtETCDate 
                        THEN PeriodHrs 
                        ELSE PeriodHrs * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) 
                      END
                    ), 0), @intHrDecimals) AS ETCLabHrs, 
                    ROUND(ISNULL(SUM(
                      CASE 
                        WHEN TPD.StartDate >= @dtETCDate 
                        THEN PeriodCost
                        ELSE PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END
                    ), 0), @intAmtCostDecimals) AS ETCLabCost, 
                    ROUND(ISNULL(SUM(
                      CASE 
                        WHEN TPD.StartDate >= @dtETCDate 
                        THEN PeriodBill
                        ELSE PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END
                    ), 0), @intAmtBillDecimals) AS ETCLabBill 
                    FROM RPTask AS PT 
                      LEFT JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
                      LEFT JOIN RPAssignment AS A ON PT.PlanID = A.PlanID AND PT.TaskID = A.TaskID
                      INNER JOIN RPPlannedLabor AS TPD ON PT.PlanID = TPD.PlanID AND PT.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL
                    WHERE PT.PlanID = @strPlanID AND CT.TaskID IS NULL AND A.AssignmentID IS NULL 
                      AND TPD.PeriodHrs <> 0 AND TPD.EndDate >= @dtETCDate
                    GROUP BY PT.OutlineNumber
                  UNION ALL
                  /* Find TPD rows attached to RPAssignment rows */
                  SELECT  
                    PT.OutlineNumber AS OutlineNumber, 
                    ROUND(ISNULL(SUM(
                      CASE 
                        WHEN TPD.StartDate >= @dtETCDate 
                        THEN PeriodHrs 
                        ELSE PeriodHrs * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) 
                      END
                    ), 0), @intHrDecimals) AS ETCLabHrs, 
                    ROUND(ISNULL(SUM(
                      CASE 
                        WHEN TPD.StartDate >= @dtETCDate 
                        THEN PeriodCost
                        ELSE PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END
                    ), 0), @intAmtCostDecimals) AS ETCLabCost, 
                    ROUND(ISNULL(SUM(
                      CASE 
                        WHEN TPD.StartDate >= @dtETCDate 
                        THEN PeriodBill
                        ELSE PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END
                    ), 0), @intAmtBillDecimals) AS ETCLabBill 
                    FROM RPTask AS PT 
                      INNER JOIN RPAssignment AS A ON PT.PlanID = A.PlanID AND PT.TaskID = A.TaskID
                      INNER JOIN RPPlannedLabor AS TPD 
                        ON A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID AND TPD.AssignmentID IS NOT NULL
                    WHERE PT.PlanID = @strPlanID AND TPD.EndDate >= @dtETCDate AND TPD.PeriodHrs <> 0
                    GROUP BY PT.OutlineNumber
                ) AS Z ON Z.OutlineNumber LIKE (TZ.OutlineNumber + '%')
              WHERE TZ.PlanID = @strPlanID
          ) AS X
          GROUP BY X.TaskID

    END /* IF (@intLabTPDCount > 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Calculate Expense ETC for Vision Plan.

  IF (@strExpTab = 'Y' AND @strVorN = 'V' AND @intExpTPDCount > 0)
    BEGIN

      INSERT @tabExpETC(
        TaskID,
        ETCExpCost,
        ETCExpBill,
        ETCDirExpCost,
        ETCDirExpBill,
        ETCReimExpCost,
        ETCReimExpBill
      )
        SELECT
          X.TaskID AS TaskID,
          SUM(X.ETCExpCost) AS ETCExpCost,
          SUM(X.ETCExpBill) AS ETCExpBill,
          SUM(X.ETCDirExpCost) AS ETCDirExpCost,
          SUM(X.ETCDirExpBill) AS ETCDirExpBill,
          SUM(X.ETCReimExpCost) AS ETCReimExpCost,
          SUM(X.ETCReimExpBill) AS ETCReimExpBill
          FROM (
            SELECT
              TY.TaskID AS TaskID,
              Y.ETCExpCost AS ETCExpCost,
              Y.ETCExpBill AS ETCExpBill,
              Y.ETCDirExpCost AS ETCDirExpCost,
              Y.ETCDirExpBill AS ETCDirExpBill,
              Y.ETCReimExpCost AS ETCReimExpCost,
              Y.ETCReimExpBill AS ETCReimExpBill
              FROM RPTask AS TY
                INNER JOIN (
                  SELECT  
                    Z.OutlineNumber AS OutlineNumber,
                    SUM(Z.ETCExpCost) AS ETCExpCost,
                    SUM(Z.ETCExpBill) AS ETCExpBill,
                    SUM(CASE WHEN CA.Type = 7 THEN Z.ETCExpCost ELSE 0 END) AS ETCDirExpCost,
                    SUM(CASE WHEN CA.Type = 7 THEN Z.ETCExpBill ELSE 0 END) AS ETCDirExpBill,
                    SUM(CASE WHEN CA.Type = 5 THEN Z.ETCExpCost ELSE 0 END) AS ETCReimExpCost,
                    SUM(CASE WHEN CA.Type = 5 THEN Z.ETCExpBill ELSE 0 END) AS ETCReimExpBill
                    FROM (
                      /* Find TPD rows attached to RPExpense rows */
                      SELECT  
                        PT.OutlineNumber AS OutlineNumber,
                        E.Account AS Account,
                        ROUND(ISNULL(SUM(
                          CASE 
                            WHEN TPD.StartDate >= @dtETCDate 
                            THEN PeriodCost
                            ELSE PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END
                        ), 0), @intAmtCostDecimals) AS ETCExpCost, 
                        ROUND(ISNULL(SUM(
                          CASE 
                            WHEN TPD.StartDate >= @dtETCDate 
                            THEN PeriodBill
                            ELSE PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END
                        ), 0), @intAmtBillDecimals) AS ETCExpBill 
                        FROM RPTask AS PT 
                          INNER JOIN RPExpense AS E ON PT.PlanID = E.PlanID AND PT.TaskID = E.TaskID
                          INNER JOIN RPPlannedExpenses AS TPD 
                            ON E.PlanID = TPD.PlanID AND E.TaskID = TPD.TaskID AND E.ExpenseID = TPD.ExpenseID AND TPD.ExpenseID IS NOT NULL
                        WHERE PT.PlanID = @strPlanID AND TPD.EndDate >= @dtETCDate AND (TPD.PeriodCost <> 0 OR TPD.PeriodBill <> 0)
                        GROUP BY PT.OutlineNumber, E.Account
                    ) AS Z
                      INNER JOIN CA ON Z.Account = CA.Account
                    GROUP BY Z.OutlineNumber
                ) AS Y ON Y.OutlineNumber LIKE (TY.OutlineNumber + '%')
              WHERE TY.PlanID = @strPlanID
          ) AS X
          GROUP BY X.TaskID

    END /* IF (@strExpTab = 'Y' AND @strVorN = 'V' AND @intExpTPDCount > 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Calculate Expense ETC for iAccess Plan.

  IF (@strExpTab = 'Y' AND @strVorN = 'N' AND @intExpTPDCount > 0)
    BEGIN

      -- Calculate Expense JTD for Navigator Plan.
      -- These are JTD records at the leaf-level in the Project WBS Tree.

      INSERT @tabExpJTD(
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor,
        JTDExpCost,
        JTDExpBill,
        JTDDirExpCost,
        JTDDirExpBill,
        JTDReimExpCost,
        JTDReimExpBill
      )
		    SELECT
          X.WBS1,
          X.WBS2,
          X.WBS3,
          X.Account,
          X.Vendor,
          ISNULL(SUM(JTDCost), 0.0000) AS JTDExpCost,
          ISNULL(SUM(JTDBill), 0.0000) AS JTDExpBill,
          ISNULL(SUM(JTDDirCost), 0.0000) AS JTDDirExpCost,
          ISNULL(SUM(JTDDirBill), 0.0000) AS JTDDirExpBill,
          ISNULL(SUM(JTDReimCost), 0.0000) AS JTDReimExpCost,
          ISNULL(SUM(JTDReimBill), 0.0000) AS JTDReimExpBill
          FROM (
            SELECT
              LG.WBS1,
              LG.WBS2,
              LG.WBS3,
              LG.Account,
              LG.Vendor,
              SUM(LG.AmountProjectCurrency) AS JTDCost,  
              SUM(LG.BillExt) AS JTDBill,
              SUM(CASE WHEN CA.Type = 7 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
              SUM(CASE WHEN CA.Type = 7 THEN LG.BillExt ELSE 0 END) AS JTDDirBill,
              SUM(CASE WHEN CA.Type = 5 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
              SUM(CASE WHEN CA.Type = 5 THEN LG.BillExt ELSE 0 END) AS JTDReimBill
              FROM LedgerAR AS LG
                INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
              WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (5, 7)
              GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
            UNION ALL SELECT
              LG.WBS1,
              LG.WBS2,
              LG.WBS3,
              LG.Account,
              LG.Vendor,
              SUM(LG.AmountProjectCurrency) AS JTDCost,  
              SUM(LG.BillExt) AS JTDBill,
              SUM(CASE WHEN CA.Type = 7 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
              SUM(CASE WHEN CA.Type = 7 THEN LG.BillExt ELSE 0 END) AS JTDDirBill,
              SUM(CASE WHEN CA.Type = 5 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
              SUM(CASE WHEN CA.Type = 5 THEN LG.BillExt ELSE 0 END) AS JTDReimBill
              FROM LedgerAP AS LG
                INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
              WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (5, 7)
              GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
            UNION ALL SELECT
              LG.WBS1,
              LG.WBS2,
              LG.WBS3,
              LG.Account,
              LG.Vendor,
              SUM(LG.AmountProjectCurrency) AS JTDCost,  
              SUM(LG.BillExt) AS JTDBill,
              SUM(CASE WHEN CA.Type = 7 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
              SUM(CASE WHEN CA.Type = 7 THEN LG.BillExt ELSE 0 END) AS JTDDirBill,
              SUM(CASE WHEN CA.Type = 5 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
              SUM(CASE WHEN CA.Type = 5 THEN LG.BillExt ELSE 0 END) AS JTDReimBill
              FROM LedgerEX AS LG
                INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
              WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (5, 7)
              GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
            UNION ALL SELECT
              LG.WBS1,
              LG.WBS2,
              LG.WBS3,
              LG.Account,
              LG.Vendor,
              SUM(LG.AmountProjectCurrency) AS JTDCost,  
              SUM(LG.BillExt) AS JTDBill,
              SUM(CASE WHEN CA.Type = 7 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
              SUM(CASE WHEN CA.Type = 7 THEN LG.BillExt ELSE 0 END) AS JTDDirBill,
              SUM(CASE WHEN CA.Type = 5 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
              SUM(CASE WHEN CA.Type = 5 THEN LG.BillExt ELSE 0 END) AS JTDReimBill
              FROM LedgerMISC AS LG
                INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
              WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (5, 7)
              GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
            UNION ALL SELECT
              POC.WBS1,
              POC.WBS2,
              POC.WBS3,
              POC.Account,
              POM.Vendor,
              SUM(AmountProjectCurrency) AS JTDCost,  
              SUM(BillExt) AS JTDBill,
              SUM(CASE WHEN CA.Type = 7 THEN AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
              SUM(CASE WHEN CA.Type = 7 THEN BillExt ELSE 0 END) AS JTDDirBill,
              SUM(CASE WHEN CA.Type = 5 THEN AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
              SUM(CASE WHEN CA.Type = 5 THEN BillExt ELSE 0 END) AS JTDReimBill
              FROM POCommitment AS POC 
                INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
                INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey
                INNER JOIN CA ON POC.Account = CA.Account
              WHERE POM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (5, 7) AND 
                (AmountProjectCurrency != 0 AND BillExt != 0)
              GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
            UNION ALL SELECT
              POC.WBS1,
              POC.WBS2,
              POC.WBS3,
              POC.Account,
              POM.Vendor,
              SUM(AmountProjectCurrency) AS JTDCost,  
              SUM(BillExt) AS JTDBill,
              SUM(CASE WHEN CA.Type = 7 THEN AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
              SUM(CASE WHEN CA.Type = 7 THEN BillExt ELSE 0 END) AS JTDDirBill,
              SUM(CASE WHEN CA.Type = 5 THEN AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
              SUM(CASE WHEN CA.Type = 5 THEN BillExt ELSE 0 END) AS JTDReimBill
              FROM POCommitment AS POC 
                INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
                INNER JOIN POCOMaster AS POCOM ON POD.COPKey = POCOM.PKey
                INNER JOIN POMaster AS POM ON POCOM.MasterPKey = POM.MasterPKey
                INNER JOIN CA ON POC.Account = CA.Account
              WHERE POCOM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (5, 7) AND 
                (AmountProjectCurrency != 0 AND BillExt != 0)
              GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
          ) AS X
          GROUP BY X.WBS1, X.WBS2, X.WBS3, X.Account, X.Vendor
          HAVING SUM(ISNULL(JTDCost, 0.0000)) <> 0 OR SUM(ISNULL(JTDBill, 0.0000)) <> 0

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Calculate Expense ETC for each Leaf OutlineNumber.

      -- Using RP tables instead of PN tables in the calculation of ETC
      -- since this script will be used in Reports and other Applications to show ETC for published iAccess Plans.

      INSERT @tabLeafNavExpETC(
        OutlineNumber,
        ETCExpCost,
        ETCExpBill,
        ETCDirExpCost,
        ETCDirExpBill,
        ETCReimExpCost,
        ETCReimExpBill
      )
        /* Collection of ETC GROUP BY OutlineNumber */
        SELECT
          OutlineNumber AS OutlineNumber,
          ISNULL(SUM(ETCExpCost), 0.0000) AS ETCExpCost,
          ISNULL(SUM(ETCExpBill), 0.0000) AS ETCExpBill,
          ISNULL(SUM(ETCDirExpCost), 0.0000) AS ETCDirExpCost,
          ISNULL(SUM(ETCDirExpBill), 0.0000) AS ETCDirExpBill,
          ISNULL(SUM(ETCReimExpCost), 0.0000) AS ETCReimExpCost,
          ISNULL(SUM(ETCReimExpBill), 0.0000) AS ETCReimExpBill
          FROM (
            /* Collection of ETC GROUP BY ExpenseID, OutlineNumber, WBS1, WBS2, WBS3, Account, Vendor */
            /* Need to do this first to align Planned vs. JTD data for each Planned row */
            SELECT
              PLD.ExpenseID AS ExpenseID,
              PLD.OutlineNumber AS OutlineNumber,
              PLD.WBS1,
              PLD.WBS2,
              PLD.WBS3,
              PLD.Account,
              PLD.Vendor,
              CASE
                WHEN ISNULL(SUM(PLD.PlannedExpCost), 0.0000) > ISNULL(SUM(JTD.JTDExpCost), 0.0000)
                THEN ISNULL(SUM(PLD.PlannedExpCost), 0.0000) - ISNULL(SUM(JTD.JTDExpCost), 0.0000)
                ELSE 0.0000
              END AS ETCExpCost,
              CASE
                WHEN ISNULL(SUM(PLD.PlannedExpBill), 0.0000) > ISNULL(SUM(JTD.JTDExpBill), 0.0000)
                THEN ISNULL(SUM(PLD.PlannedExpBill), 0.0000) - ISNULL(SUM(JTD.JTDExpBill), 0.0000)
                ELSE 0.0000
              END AS ETCExpBill,
              CASE
                WHEN ISNULL(SUM(PLD.PlannedDirExpCost), 0.0000) > ISNULL(SUM(JTD.JTDDirExpCost), 0.0000)
                THEN ISNULL(SUM(PLD.PlannedDirExpCost), 0.0000) - ISNULL(SUM(JTD.JTDDirExpCost), 0.0000)
                ELSE 0.0000
              END AS ETCDirExpCost,
              CASE
                WHEN ISNULL(SUM(PLD.PlannedDirExpBill), 0.0000) > ISNULL(SUM(JTD.JTDDirExpBill), 0.0000)
                THEN ISNULL(SUM(PLD.PlannedDirExpBill), 0.0000) - ISNULL(SUM(JTD.JTDDirExpBill), 0.0000)
                ELSE 0.0000
              END AS ETCDirExpBill,
              CASE
                WHEN ISNULL(SUM(PLD.PlannedReimExpCost), 0.0000) > ISNULL(SUM(JTD.JTDReimExpCost), 0.0000)
                THEN ISNULL(SUM(PLD.PlannedReimExpCost), 0.0000) - ISNULL(SUM(JTD.JTDReimExpCost), 0.0000)
                ELSE 0.0000
              END AS ETCReimExpCost,
              CASE
                WHEN ISNULL(SUM(PLD.PlannedReimExpBill), 0.0000) > ISNULL(SUM(JTD.JTDReimExpBill), 0.0000)
                THEN ISNULL(SUM(PLD.PlannedReimExpBill), 0.0000) - ISNULL(SUM(JTD.JTDReimExpBill), 0.0000)
                ELSE 0.0000
              END AS ETCReimExpBill
              FROM (
                SELECT
                  ZE.OutlineNumber AS OutlineNumber,
                  ZE.ExpenseID AS ExpenseID,
                  ZE.WBS1,
                  ZE.WBS2,
                  ZE.WBS3,
                  ZE.Account,
                  ZE.Vendor,
                  ZE.PeriodCost AS PlannedExpCost, 
                  ZE.PeriodBill AS PlannedExpBill,
                  CASE WHEN CA.Type = 7 THEN ZE.PeriodCost ELSE 0 END AS PlannedDirExpCost,
                  CASE WHEN CA.Type = 7 THEN ZE.PeriodBill ELSE 0 END AS PlannedDirExpBill,
                  CASE WHEN CA.Type = 5 THEN ZE.PeriodCost ELSE 0 END AS PlannedReimExpCost,
                  CASE WHEN CA.Type = 5 THEN ZE.PeriodBill ELSE 0 END AS PlannedReimExpBill
                  FROM (
                    SELECT
                      T.OutlineNumber AS OutlineNumber,
                      E.ExpenseID AS ExpenseID,
                      E.WBS1 AS WBS1,
                      ISNULL(E.WBS2, ' ') AS WBS2,
                      ISNULL(E.WBS3, ' ') AS WBS3,
                      E.Account AS Account,
                      E.Vendor AS Vendor,
                      ISNULL(SUM(TPD.PeriodCost), 0) AS PeriodCost, 
                      ISNULL(SUM(TPD.PeriodBill), 0) AS PeriodBill
                      FROM RPPlannedExpenses AS TPD
                        INNER JOIN RPExpense AS E ON TPD.PlanID = E.PlanID AND TPD.TaskID = E.TaskID AND TPD.ExpenseID = E.ExpenseID AND TPD.ExpenseID IS NOT NULL
                        INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
                      WHERE TPD.PlanID = @strPlanID
                      GROUP BY E.ExpenseID, T.OutlineNumber, E.WBS1, E.WBS2, E.WBS3, E.Account, E.Vendor
                  ) AS ZE
                    INNER JOIN CA ON ZE.Account = CA.Account
              ) AS PLD
                LEFT JOIN @tabExpJTD AS JTD ON
                  JTD.WBS1 = PLD.WBS1 AND 
                  (JTD.WBS2 LIKE CASE WHEN PLD.WBS2 = ' ' THEN '%' ELSE PLD.WBS2 END) AND 
                  (JTD.WBS3 LIKE CASE WHEN PLD.WBS3 = ' ' THEN '%' ELSE PLD.WBS3 END) AND 
                  JTD.Account = PLD.Account AND 
                  ISNULL(JTD.Vendor, '|') = ISNULL(PLD.Vendor, '|')
              GROUP BY PLD.ExpenseID, PLD.OutlineNumber, PLD.WBS1, PLD.WBS2, PLD.WBS3, PLD.Account, PLD.Vendor
          ) AS X
          GROUP BY OutlineNumber

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Calculate ETC for each RPTask row

      INSERT @tabExpETC(
        TaskID,
        ETCExpCost,
        ETCExpBill,
        ETCDirExpCost,
        ETCDirExpBill,
        ETCReimExpCost,
        ETCReimExpBill
      )
        /* Collection of ETC GROUP BY TaskID */
        SELECT
          TY.TaskID AS TaskID,
          ISNULL(SUM(ETCExpCost), 0.0000) AS ETCExpCost,
          ISNULL(SUM(ETCExpBill), 0.0000) AS ETCExpBill,
          ISNULL(SUM(ETCDirExpCost), 0.0000) AS ETCDirExpCost,
          ISNULL(SUM(ETCDirExpBill), 0.0000) AS ETCDirExpBill,
          ISNULL(SUM(ETCReimExpCost), 0.0000) AS ETCReimExpCost,
          ISNULL(SUM(ETCReimExpBill), 0.0000) AS ETCReimExpBill
          FROM RPTask AS TY
            INNER JOIN @tabLeafNavExpETC AS ETC
              ON ETC.OutlineNumber LIKE (TY.OutlineNumber + '%')
          WHERE TY.PlanID = @strPlanID
          GROUP BY TY.TaskID

    END /* IF (@strExpTab = 'Y' AND @strVorN = 'V' AND @intExpTPDCount > 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Calculate Consultant ETC for Vision Plan.

  IF (@strConTab = 'Y' AND @strVorN = 'V' AND @intConTPDCount > 0)
    BEGIN

      INSERT @tabConETC(
        TaskID,
        ETCConCost,
        ETCConBill,
        ETCDirConCost,
        ETCDirConBill,
        ETCReimConCost,
        ETCReimConBill
      )
        SELECT
          X.TaskID AS TaskID,
          SUM(X.ETCConCost) AS ETCConCost,
          SUM(X.ETCConBill) AS ETCConBill,
          SUM(X.ETCDirConCost) AS ETCDirConCost,
          SUM(X.ETCDirConBill) AS ETCDirConBill,
          SUM(X.ETCReimConCost) AS ETCReimConCost,
          SUM(X.ETCReimConBill) AS ETCReimConBill
          FROM (
            SELECT
              TY.TaskID AS TaskID,
              Y.ETCConCost AS ETCConCost,
              Y.ETCConBill AS ETCConBill,
              Y.ETCDirConCost AS ETCDirConCost,
              Y.ETCDirConBill AS ETCDirConBill,
              Y.ETCReimConCost AS ETCReimConCost,
              Y.ETCReimConBill AS ETCReimConBill
              FROM RPTask AS TY
                INNER JOIN (
                  SELECT  
                    Z.OutlineNumber AS OutlineNumber,
                    SUM(Z.ETCConCost) AS ETCConCost,
                    SUM(Z.ETCConBill) AS ETCConBill,
                    SUM(CASE WHEN CA.Type = 8 THEN Z.ETCConCost ELSE 0 END) AS ETCDirConCost,
                    SUM(CASE WHEN CA.Type = 8 THEN Z.ETCConBill ELSE 0 END) AS ETCDirConBill,
                    SUM(CASE WHEN CA.Type = 6 THEN Z.ETCConCost ELSE 0 END) AS ETCReimConCost,
                    SUM(CASE WHEN CA.Type = 6 THEN Z.ETCConBill ELSE 0 END) AS ETCReimConBill
                    FROM (
                      /* Find TPD rows attached to RPConsultant rows */
                      SELECT  
                        PT.OutlineNumber AS OutlineNumber,
                        C.Account AS Account,
                        ROUND(ISNULL(SUM(
                          CASE 
                            WHEN TPD.StartDate >= @dtETCDate 
                            THEN PeriodCost
                            ELSE PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END
                        ), 0), @intAmtCostDecimals) AS ETCConCost, 
                        ROUND(ISNULL(SUM(
                          CASE 
                            WHEN TPD.StartDate >= @dtETCDate 
                            THEN PeriodBill
                            ELSE PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END
                        ), 0), @intAmtBillDecimals) AS ETCConBill 
                        FROM RPTask AS PT 
                          INNER JOIN RPConsultant AS C ON PT.PlanID = C.PlanID AND PT.TaskID = C.TaskID
                          INNER JOIN RPPlannedConsultant AS TPD 
                            ON C.PlanID = TPD.PlanID AND C.TaskID = TPD.TaskID AND C.ConsultantID = TPD.ConsultantID AND TPD.ConsultantID IS NOT NULL
                        WHERE PT.PlanID = @strPlanID AND TPD.EndDate >= @dtETCDate AND (TPD.PeriodCost <> 0 OR TPD.PeriodBill <> 0)
                        GROUP BY PT.OutlineNumber, C.Account
                    ) AS Z
                      INNER JOIN CA ON Z.Account = CA.Account
                    GROUP BY Z.OutlineNumber
                ) AS Y ON Y.OutlineNumber LIKE (TY.OutlineNumber + '%')
              WHERE TY.PlanID = @strPlanID
          ) AS X
          GROUP BY X.TaskID

    END /* IF (@strConTab = 'Y' AND @strVorN = 'V' AND @intConTPDCount > 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Calculate Consultant ETC for iAccess Plan.

  IF (@strConTab = 'Y' AND @strVorN = 'N' AND @intConTPDCount > 0)
    BEGIN

      -- Calculate Consultant JTD for Navigator Plan.
      -- These are JTD records at the leaf-level in the Project WBS Tree.

      INSERT @tabConJTD(
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor,
        JTDConCost,
        JTDConBill,
        JTDDirConCost,
        JTDDirConBill,
        JTDReimConCost,
        JTDReimConBill
      )
		    SELECT
          X.WBS1,
          X.WBS2,
          X.WBS3,
          X.Account,
          X.Vendor,
          ISNULL(SUM(JTDCost), 0.0000) AS JTDConCost,
          ISNULL(SUM(JTDBill), 0.0000) AS JTDConBill,
          ISNULL(SUM(JTDDirCost), 0.0000) AS JTDDirConCost,
          ISNULL(SUM(JTDDirBill), 0.0000) AS JTDDirConBill,
          ISNULL(SUM(JTDReimCost), 0.0000) AS JTDReimConCost,
          ISNULL(SUM(JTDReimBill), 0.0000) AS JTDReimConBill
          FROM (
            SELECT
              LG.WBS1,
              LG.WBS2,
              LG.WBS3,
              LG.Account,
              LG.Vendor,
              SUM(LG.AmountProjectCurrency) AS JTDCost,  
              SUM(LG.BillExt) AS JTDBill,
              SUM(CASE WHEN CA.Type = 8 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
              SUM(CASE WHEN CA.Type = 8 THEN LG.BillExt ELSE 0 END) AS JTDDirBill,
              SUM(CASE WHEN CA.Type = 6 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
              SUM(CASE WHEN CA.Type = 6 THEN LG.BillExt ELSE 0 END) AS JTDReimBill
              FROM LedgerAR AS LG
                INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
              WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (6, 8)
              GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
            UNION ALL SELECT
              LG.WBS1,
              LG.WBS2,
              LG.WBS3,
              LG.Account,
              LG.Vendor,
              SUM(LG.AmountProjectCurrency) AS JTDCost,  
              SUM(LG.BillExt) AS JTDBill,
              SUM(CASE WHEN CA.Type = 8 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
              SUM(CASE WHEN CA.Type = 8 THEN LG.BillExt ELSE 0 END) AS JTDDirBill,
              SUM(CASE WHEN CA.Type = 6 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
              SUM(CASE WHEN CA.Type = 6 THEN LG.BillExt ELSE 0 END) AS JTDReimBill
              FROM LedgerAP AS LG
                INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
              WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (6, 8)
              GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
            UNION ALL SELECT
              LG.WBS1,
              LG.WBS2,
              LG.WBS3,
              LG.Account,
              LG.Vendor,
              SUM(LG.AmountProjectCurrency) AS JTDCost,  
              SUM(LG.BillExt) AS JTDBill,
              SUM(CASE WHEN CA.Type = 8 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
              SUM(CASE WHEN CA.Type = 8 THEN LG.BillExt ELSE 0 END) AS JTDDirBill,
              SUM(CASE WHEN CA.Type = 6 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
              SUM(CASE WHEN CA.Type = 6 THEN LG.BillExt ELSE 0 END) AS JTDReimBill
              FROM LedgerEX AS LG
                INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
              WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (6, 8)
              GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
            UNION ALL SELECT
              LG.WBS1,
              LG.WBS2,
              LG.WBS3,
              LG.Account,
              LG.Vendor,
              SUM(LG.AmountProjectCurrency) AS JTDCost,  
              SUM(LG.BillExt) AS JTDBill,
              SUM(CASE WHEN CA.Type = 8 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
              SUM(CASE WHEN CA.Type = 8 THEN LG.BillExt ELSE 0 END) AS JTDDirBill,
              SUM(CASE WHEN CA.Type = 6 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
              SUM(CASE WHEN CA.Type = 6 THEN LG.BillExt ELSE 0 END) AS JTDReimBill
              FROM LedgerMISC AS LG
                INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
              WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (6, 8)
              GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
            UNION ALL SELECT
              POC.WBS1,
              POC.WBS2,
              POC.WBS3,
              POC.Account,
              POM.Vendor,
              SUM(AmountProjectCurrency) AS JTDCost,  
              SUM(BillExt) AS JTDBill,
              SUM(CASE WHEN CA.Type = 8 THEN AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
              SUM(CASE WHEN CA.Type = 8 THEN BillExt ELSE 0 END) AS JTDDirBill,
              SUM(CASE WHEN CA.Type = 6 THEN AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
              SUM(CASE WHEN CA.Type = 6 THEN BillExt ELSE 0 END) AS JTDReimBill
              FROM POCommitment AS POC 
                INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
                INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey
                INNER JOIN CA ON POC.Account = CA.Account
              WHERE POM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (6, 8) AND 
                (AmountProjectCurrency != 0 AND BillExt != 0)
              GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
            UNION ALL SELECT
              POC.WBS1,
              POC.WBS2,
              POC.WBS3,
              POC.Account,
              POM.Vendor,
              SUM(AmountProjectCurrency) AS JTDCost,  
              SUM(BillExt) AS JTDBill,
              SUM(CASE WHEN CA.Type = 8 THEN AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
              SUM(CASE WHEN CA.Type = 8 THEN BillExt ELSE 0 END) AS JTDDirBill,
              SUM(CASE WHEN CA.Type = 6 THEN AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
              SUM(CASE WHEN CA.Type = 6 THEN BillExt ELSE 0 END) AS JTDReimBill
              FROM POCommitment AS POC 
                INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
                INNER JOIN POCOMaster AS POCOM ON POD.COPKey = POCOM.PKey
                INNER JOIN POMaster AS POM ON POCOM.MasterPKey = POM.MasterPKey
                INNER JOIN CA ON POC.Account = CA.Account
              WHERE POCOM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (6, 8) AND 
                (AmountProjectCurrency != 0 AND BillExt != 0)
              GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
          ) AS X
          GROUP BY X.WBS1, X.WBS2, X.WBS3, X.Account, X.Vendor
          HAVING SUM(ISNULL(JTDCost, 0.0000)) <> 0 OR SUM(ISNULL(JTDBill, 0.0000)) <> 0

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Calculate Consultant ETC for each Leaf OutlineNumber.

      -- Using RP tables instead of PN tables in the calculation of ETC
      -- since this script will be used in Reports and other Applications to show ETC for published iAccess Plans.

      INSERT @tabLeafNavConETC(
        OutlineNumber,
        ETCConCost,
        ETCConBill,
        ETCDirConCost,
        ETCDirConBill,
        ETCReimConCost,
        ETCReimConBill
      )
        /* Collection of ETC GROUP BY OutlineNumber */
        SELECT
          OutlineNumber AS OutlineNumber,
          ISNULL(SUM(ETCConCost), 0.0000) AS ETCConCost,
          ISNULL(SUM(ETCConBill), 0.0000) AS ETCConBill,
          ISNULL(SUM(ETCDirConCost), 0.0000) AS ETCDirConCost,
          ISNULL(SUM(ETCDirConBill), 0.0000) AS ETCDirConBill,
          ISNULL(SUM(ETCReimConCost), 0.0000) AS ETCReimConCost,
          ISNULL(SUM(ETCReimConBill), 0.0000) AS ETCReimConBill
          FROM (
            /* Collection of ETC GROUP BY ConsultantID, OutlineNumber, WBS1, WBS2, WBS3, Account, Vendor */
            /* Need to do this first to align Planned vs. JTD data for each Planned row */
            SELECT
              PLD.ConsultantID AS ConsultantID,
              PLD.OutlineNumber AS OutlineNumber,
              PLD.WBS1,
              PLD.WBS2,
              PLD.WBS3,
              PLD.Account,
              PLD.Vendor,
              CASE
                WHEN ISNULL(SUM(PLD.PlannedConCost), 0.0000) > ISNULL(SUM(JTD.JTDConCost), 0.0000)
                THEN ISNULL(SUM(PLD.PlannedConCost), 0.0000) - ISNULL(SUM(JTD.JTDConCost), 0.0000)
                ELSE 0.0000
              END AS ETCConCost,
              CASE
                WHEN ISNULL(SUM(PLD.PlannedConBill), 0.0000) > ISNULL(SUM(JTD.JTDConBill), 0.0000)
                THEN ISNULL(SUM(PLD.PlannedConBill), 0.0000) - ISNULL(SUM(JTD.JTDConBill), 0.0000)
                ELSE 0.0000
              END AS ETCConBill,
              CASE
                WHEN ISNULL(SUM(PLD.PlannedDirConCost), 0.0000) > ISNULL(SUM(JTD.JTDDirConCost), 0.0000)
                THEN ISNULL(SUM(PLD.PlannedDirConCost), 0.0000) - ISNULL(SUM(JTD.JTDDirConCost), 0.0000)
                ELSE 0.0000
              END AS ETCDirConCost,
              CASE
                WHEN ISNULL(SUM(PLD.PlannedDirConBill), 0.0000) > ISNULL(SUM(JTD.JTDDirConBill), 0.0000)
                THEN ISNULL(SUM(PLD.PlannedDirConBill), 0.0000) - ISNULL(SUM(JTD.JTDDirConBill), 0.0000)
                ELSE 0.0000
              END AS ETCDirConBill,
              CASE
                WHEN ISNULL(SUM(PLD.PlannedReimConCost), 0.0000) > ISNULL(SUM(JTD.JTDReimConCost), 0.0000)
                THEN ISNULL(SUM(PLD.PlannedReimConCost), 0.0000) - ISNULL(SUM(JTD.JTDReimConCost), 0.0000)
                ELSE 0.0000
              END AS ETCReimConCost,
              CASE
                WHEN ISNULL(SUM(PLD.PlannedReimConBill), 0.0000) > ISNULL(SUM(JTD.JTDReimConBill), 0.0000)
                THEN ISNULL(SUM(PLD.PlannedReimConBill), 0.0000) - ISNULL(SUM(JTD.JTDReimConBill), 0.0000)
                ELSE 0.0000
              END AS ETCReimConBill
              FROM (
                SELECT
                  ZE.OutlineNumber AS OutlineNumber,
                  ZE.ConsultantID AS ConsultantID,
                  ZE.WBS1,
                  ZE.WBS2,
                  ZE.WBS3,
                  ZE.Account,
                  ZE.Vendor,
                  ZE.PeriodCost AS PlannedConCost, 
                  ZE.PeriodBill AS PlannedConBill,
                  CASE WHEN CA.Type = 8 THEN ZE.PeriodCost ELSE 0 END AS PlannedDirConCost,
                  CASE WHEN CA.Type = 8 THEN ZE.PeriodBill ELSE 0 END AS PlannedDirConBill,
                  CASE WHEN CA.Type = 6 THEN ZE.PeriodCost ELSE 0 END AS PlannedReimConCost,
                  CASE WHEN CA.Type = 6 THEN ZE.PeriodBill ELSE 0 END AS PlannedReimConBill
                  FROM (
                    SELECT
                      T.OutlineNumber AS OutlineNumber,
                      E.ConsultantID AS ConsultantID,
                      E.WBS1 AS WBS1,
                      ISNULL(E.WBS2, ' ') AS WBS2,
                      ISNULL(E.WBS3, ' ') AS WBS3,
                      E.Account AS Account,
                      E.Vendor AS Vendor,
                      ISNULL(SUM(TPD.PeriodCost), 0) AS PeriodCost, 
                      ISNULL(SUM(TPD.PeriodBill), 0) AS PeriodBill
                      FROM RPPlannedConsultant AS TPD
                        INNER JOIN RPConsultant AS E ON TPD.PlanID = E.PlanID AND TPD.TaskID = E.TaskID AND TPD.ConsultantID = E.ConsultantID AND TPD.ConsultantID IS NOT NULL
                        INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
                      WHERE TPD.PlanID = @strPlanID
                      GROUP BY E.ConsultantID, T.OutlineNumber, E.WBS1, E.WBS2, E.WBS3, E.Account, E.Vendor
                  ) AS ZE
                    INNER JOIN CA ON ZE.Account = CA.Account
              ) AS PLD
                LEFT JOIN @tabConJTD AS JTD ON
                  JTD.WBS1 = PLD.WBS1 AND 
                  (JTD.WBS2 LIKE CASE WHEN PLD.WBS2 = ' ' THEN '%' ELSE PLD.WBS2 END) AND 
                  (JTD.WBS3 LIKE CASE WHEN PLD.WBS3 = ' ' THEN '%' ELSE PLD.WBS3 END) AND 
                  JTD.Account = PLD.Account AND 
                  ISNULL(JTD.Vendor, '|') = ISNULL(PLD.Vendor, '|')
              GROUP BY PLD.ConsultantID, PLD.OutlineNumber, PLD.WBS1, PLD.WBS2, PLD.WBS3, PLD.Account, PLD.Vendor
          ) AS X
          GROUP BY OutlineNumber

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Calculate ETC for each RPTask row

      INSERT @tabConETC(
        TaskID,
        ETCConCost,
        ETCConBill,
        ETCDirConCost,
        ETCDirConBill,
        ETCReimConCost,
        ETCReimConBill
      )
        /* Collection of ETC GROUP BY TaskID */
        SELECT
          TY.TaskID AS TaskID,
          ISNULL(SUM(ETCConCost), 0.0000) AS ETCConCost,
          ISNULL(SUM(ETCConBill), 0.0000) AS ETCConBill,
          ISNULL(SUM(ETCDirConCost), 0.0000) AS ETCDirConCost,
          ISNULL(SUM(ETCDirConBill), 0.0000) AS ETCDirConBill,
          ISNULL(SUM(ETCReimConCost), 0.0000) AS ETCReimConCost,
          ISNULL(SUM(ETCReimConBill), 0.0000) AS ETCReimConBill
          FROM RPTask AS TY
            INNER JOIN @tabLeafNavConETC AS ETC
              ON ETC.OutlineNumber LIKE (TY.OutlineNumber + '%')
          WHERE TY.PlanID = @strPlanID
          GROUP BY TY.TaskID

    END /* IF (@strConTab = 'Y' AND @strVorN = 'V' AND @intConTPDCount > 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Calculate Unit ETC.

  IF (@strUntTab = 'Y' AND @intUntTPDCount > 0)
    BEGIN

      INSERT @tabUntETC(
        TaskID,
        ETCUntQty,
        ETCUntCost,
        ETCUntBill
      )
        SELECT
          X.TaskID AS TaskID,
          SUM(X.ETCUntQty) AS ETCUntQty,
          SUM(X.ETCUntCost) AS ETCUntCost,
          SUM(X.ETCUntBill) AS ETCUntBill
          FROM (
            SELECT
              TZ.TaskID AS TaskID,
              Z.ETCUntQty AS ETCUntQty,
              Z.ETCUntCost AS ETCUntCost,
              Z.ETCUntBill AS ETCUntBill
              FROM RPTask AS TZ
                INNER JOIN (
                  /* Find TPD rows attached to RPUnit rows */
                  SELECT  
                    PT.OutlineNumber AS OutlineNumber, 
                    ROUND(ISNULL(SUM(
                      CASE 
                        WHEN TPD.StartDate >= @dtETCDate 
                        THEN PeriodQty 
                        ELSE PeriodQty * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) 
                      END
                    ), 0), @intQtyDecimals) AS ETCUntQty, 
                    ROUND(ISNULL(SUM(
                      CASE 
                        WHEN TPD.StartDate >= @dtETCDate 
                        THEN PeriodCost
                        ELSE PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END
                    ), 0), @intAmtCostDecimals) AS ETCUntCost, 
                    ROUND(ISNULL(SUM(
                      CASE 
                        WHEN TPD.StartDate >= @dtETCDate 
                        THEN PeriodBill
                        ELSE PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END
                    ), 0), @intAmtBillDecimals) AS ETCUntBill 
                    FROM RPTask AS PT 
                      INNER JOIN RPUnit AS U ON PT.PlanID = U.PlanID AND PT.TaskID = U.TaskID
                      INNER JOIN RPPlannedUnit AS TPD 
                        ON U.PlanID = TPD.PlanID AND U.TaskID = TPD.TaskID AND U.UnitID = TPD.UnitID AND TPD.UnitID IS NOT NULL
                    WHERE PT.PlanID = @strPlanID AND TPD.EndDate >= @dtETCDate AND TPD.PeriodQty <> 0
                    GROUP BY PT.OutlineNumber
                ) AS Z ON Z.OutlineNumber LIKE (TZ.OutlineNumber + '%')
              WHERE TZ.PlanID = @strPlanID
          ) AS X
          GROUP BY X.TaskID

    END /* IF (@strUntTab = 'Y' AND @intUntTPDCount > 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Combine Labor, Expense, Consultant, Unit ETC for each RPTask row.

  INSERT @tabTaskETC(
    TaskID,
    OutlineNumber,
    ETCLabHrs,
    ETCLabCost,
    ETCLabBill,
    ETCExpCost,
    ETCExpBill,
    ETCDirExpCost,
    ETCDirExpBill,
    ETCReimExpCost,
    ETCReimExpBill,
    ETCConCost,
    ETCConBill,
    ETCDirConCost,
    ETCDirConBill,
    ETCReimConCost,
    ETCReimConBill,
    ETCUntQty,
    ETCUntCost,
    ETCUntBill
  )
    SELECT
      T.TaskID AS TaskID,
      T.OutlineNumber AS OutlineNumber,
      ISNULL(LE.ETCLabHrs, 0) AS ETCLabHrs,
      ISNULL(LE.ETCLabCost, 0) AS ETCLabCost,
      ISNULL(LE.ETCLabBill, 0) AS ETCLabBill,
      ISNULL(EE.ETCExpCost, 0) AS ETCExpCost,
      ISNULL(EE.ETCExpBill, 0) AS ETCExpBill,
      ISNULL(EE.ETCDirExpCost, 0) AS ETCDirExpCost,
      ISNULL(EE.ETCDirExpBill, 0) AS ETCDirExpBill,
      ISNULL(EE.ETCReimExpCost, 0) AS ETCReimExpCost,
      ISNULL(EE.ETCReimExpBill, 0) AS ETCReimExpBill,
      ISNULL(CE.ETCConCost, 0) AS ETCConCost,
      ISNULL(CE.ETCConBill, 0) AS ETCConBill,
      ISNULL(CE.ETCDirConCost, 0) AS ETCDirConCost,
      ISNULL(CE.ETCDirConBill, 0) AS ETCDirConBill,
      ISNULL(CE.ETCReimConCost, 0) AS ETCReimConCost,
      ISNULL(CE.ETCReimConBill, 0) AS ETCReimConBill,
      ISNULL(UE.ETCUntQty, 0) AS ETCUntQty,
      ISNULL(UE.ETCUntCost, 0) AS ETCUntCost,
      ISNULL(UE.ETCUntBill, 0) AS ETCUntBill
      FROM RPTask AS T
        LEFT JOIN @tabLabETC AS LE ON T.TaskID = LE.TaskID
        LEFT JOIN @tabExpETC AS EE ON T.TaskID = EE.TaskID
        LEFT JOIN @tabConETC AS CE ON T.TaskID = CE.TaskID
        LEFT JOIN @tabUntETC AS UE ON T.TaskID = UE.TaskID
      WHERE T.PlanID = @strPlanID
        AND (ETCLabHrs <> 0 OR ETCExpCost <> 0 OR ETCExpBill <> 0 OR ETCConCost <> 0 OR ETCConBill <> 0 OR ETCUntQty <> 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
