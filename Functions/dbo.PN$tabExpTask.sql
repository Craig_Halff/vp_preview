SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$tabExpTask]
  (@strWBS1 Nvarchar(30),
   @strJTDDate VARCHAR(8)) -- Date must be in format: 'yyyymmdd' regardless of UI Culture.

  RETURNS @tabExpTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default, /* Only Plans, that were created from Navigator, have true TaskID */
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    Name Nvarchar(40) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    MinDate datetime,
    MaxDate datetime,
    WBSType varchar(4) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int,
    PlannedExpCost decimal(19,4),
    PlannedReimExpCost decimal(19,4),
    PlannedDirExpCost decimal(19,4),
    PlannedIndirExpCost decimal(19,4),
    PlannedExpBill decimal(19,4),
    PlannedReimExpBill decimal(19,4),
    PlannedDirExpBill decimal(19,4),
    PlannedIndirExpBill decimal(19,4),
    BaselineExpCost decimal(19,4),
    BaselineExpBill decimal(19,4),
    JTDExpCost decimal(19,4),
    JTDReimExpCost decimal(19,4),
    JTDDirExpCost decimal(19,4),
    JTDIndirExpCost decimal(19,4),
    JTDExpBill decimal(19,4),
    JTDReimExpBill decimal(19,4),
    JTDDirExpBill decimal(19,4),
    JTDIndirExpBill decimal(19,4),
    FeeExpCost decimal(19,4),
    FeeExpBill decimal(19,4),
    FeeDirExpCost decimal(19,4),
    FeeDirExpBill decimal(19,4),
    FeeReimExpCost decimal(19,4),
    FeeReimExpBill decimal(19,4),
    MarkupExpCost decimal(19,4),
    FeeJTDExpBill decimal(19,4),
    ETCExpCost decimal(19,4),
    ETCExpBill decimal(19,4)
  )

BEGIN

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
 
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strPlanID varchar(32)

  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)
  DECLARE @strCompEQLabDirExpFlg varchar(1)
  
  DECLARE @strExpTab varchar(1)

  DECLARE @strVorN char(1)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

  DECLARE @tiDefExpWBSLevel tinyint
  DECLARE @tiMinExpWBSLevel tinyint
  
  -- Declare Temp tables.

  DECLARE @tabWBSTree TABLE (
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    Name Nvarchar(40) COLLATE database_default,
    FeeExpCost decimal(19,4),
    FeeExpBill decimal(19,4),
    FeeDirExpCost decimal(19,4),
    FeeDirExpBill decimal(19,4),
    FeeReimExpCost decimal(19,4),
    FeeReimExpBill decimal(19,4),
    WBSLevel tinyint,
    IsLeaf bit
    UNIQUE(WBS1, WBS2, WBS3, WBSLevel, IsLeaf)
  )

  DECLARE @tabMappedTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    WBSType varchar(4) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChildrenCount int,
    OutlineLevel int,
    BaselineExpCost decimal(19,4),
    BaselineExpBill decimal(19,4)
    UNIQUE(PlanID, TaskID)
  )

  DECLARE @tabPlanned TABLE(
    RowID int IDENTITY(1,1),
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    Account Nvarchar(13) COLLATE database_default,
    Vendor Nvarchar(30) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PlannedCost decimal(19,4),
    PlannedBill decimal(19,4),
    PlannedDirCost decimal(19,4),
    PlannedDirBill decimal(19,4),
    PlannedReimCost decimal(19,4),
    PlannedReimBill decimal(19,4),
    PlannedIndirCost decimal(19,4),
    PlannedIndirBill decimal(19,4),
    ETCCost decimal(19,4),
    ETCBill decimal(19,4)
    UNIQUE(RowID, WBS1, WBS2, WBS3, Account, Vendor)
  )
    
 	DECLARE @tabJTD TABLE(
    RowID int IDENTITY(1,1),
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    Account Nvarchar(13) COLLATE database_default,
    Vendor Nvarchar(30) COLLATE database_default,
    JTDCost decimal(19,4),
    JTDBill decimal(19,4),
    JTDDirCost decimal(19,4),
    JTDDirBill decimal(19,4),
    JTDReimCost decimal(19,4),
    JTDReimBill decimal(19,4),
    JTDIndirCost decimal(19,4),
    JTDIndirBill decimal(19,4)
    UNIQUE(RowID, WBS1, WBS2, WBS3, Account, Vendor)
  )

  DECLARE @tabETC TABLE (
    RowID  int IDENTITY(1,1),
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    ETCCost decimal(19,4),
    ETCBill decimal(19,4)
    UNIQUE(RowID, WBS1, WBS2, WBS3)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @tiMinExpWBSLevel = 3 -- Leaf Level

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
          
  -- Set JTD/ETC Dates.
  
  SET @dtJTDDate = CONVERT(datetime, @strJTDDate)
  SET @dtETCDate = DATEADD(d, 1, @dtJTDDate) 

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat     

  -- Save Company string for use later.
  
  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          
  -- Determine which set of tables need to be used.

  SET @strVorN = dbo.PN$PlanIs_VorN(@strWBS1)

  -- Get Default Plan Settings from CFGResourcePlanning

  SELECT
    @tiDefExpWBSLevel = ExpWBSLevel,
    @strCompEQLabDirExpFlg = CompEQLabDirExpFlg
    FROM CFGResourcePlanning WHERE Company = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN @strCompany ELSE ' ' END

  -- Initialize @strPlanID

  SET @strPlanID = NULL

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine the minimum WBS level where Consultant rows need to be attached.

  IF (@strVorN = 'V')
    BEGIN

      -- Start with the default ExpWBSLevel. If there is no Consultant row in the Vision Plan then @tiMinExpWBSLevel = ExpWBSLevel.
      -- If the Vision Plan has Consultant rows, then the highest level found will be used as ExpWBSLevel.

      SELECT @tiMinExpWBSLevel = ISNULL(MIN(WBSLevel), @tiDefExpWBSLevel) FROM
        (SELECT 1 AS WBSLevel
           FROM RPExpense AS C
             INNER JOIN PR ON C.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND C.WBS2 IS NULL AND C.WBS3 IS NULL AND PR.SubLevel = 'Y'
             INNER JOIN RPPlan AS P ON C.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y'
           WHERE C.WBS1 = @strWBS1
         UNION
         SELECT 2 AS WBSLevel
           FROM RPExpense AS C
             INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND PR.WBS3 = ' ' AND C.WBS3 IS NULL AND PR.SubLevel = 'Y'
             INNER JOIN RPPlan AS P ON C.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y'
           WHERE C.WBS1 = @strWBS1
         UNION
         SELECT 3 AS WBSLevel
           FROM RPExpense AS C
             INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND PR.WBS3 = ' ' AND C.WBS3 IS NULL AND PR.SubLevel = 'N'
             INNER JOIN RPPlan AS P ON C.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y'
          WHERE C.WBS1 = @strWBS1
         UNION
         SELECT 3 AS WBSLevel
           FROM RPExpense AS C
             INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND C.WBS3 = PR.WBS3
             INNER JOIN RPPlan AS P ON C.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y'
           WHERE C.WBS1 = @strWBS1
        ) AS X

    END /* End If (@strVorN = 'V') */
  ELSE IF (@strVorN = 'N')
    BEGIN

      -- For Navigator Plans, @tiMinExpWBSLevel = ExpWBSLevel from PNPlan

      SELECT @tiMinExpWBSLevel = ExpWBSLevel
        FROM PNPlan AS P 
        WHERE P.WBS1 = @strWBS1

    END 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabWBSTree (
    WBS1,
    WBS2,
    WBS3,
    Name,
    FeeExpCost,
    FeeExpBill,
    FeeDirExpCost,
    FeeDirExpBill,
    FeeReimExpCost,
    FeeReimExpBill,
    WBSLevel,
    IsLeaf
  )
    SELECT
      WBS1,
      WBS2,
      WBS3,
      Name,
      (FeeDirExpCost + FeeReimExpCost) AS FeeExpCost,
      (FeeDirExpBill + FeeReimExpBill) AS FeeExpBill,
      FeeDirExpCost,
      FeeDirExpBill,
      FeeReimExpCost,
      FeeReimExpBill,
      WBSLevel,
      IsLeaf
      FROM
        (SELECT
           WBS1, 
           WBS2, 
           WBS3,
           Name, 
           FeeDirExp AS FeeDirExpCost,
           (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN FeeDirExpBillingCurrency ELSE FeeDirExp END) AS FeeDirExpBill,
           ReimbAllowExp AS FeeReimExpCost,
           (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN ReimbAllowExpBillingCurrency ELSE ReimbAllowExp END) AS FeeReimExpBill,
           CASE 
             WHEN WBS2 = ' ' AND WBS3 = ' ' THEN 1
             WHEN WBS2 != ' ' AND WBS3 = ' ' THEN 2
             WHEN WBS2 != ' ' AND WBS3 != ' ' THEN 3
           END AS WBSLevel,
           CASE WHEN SubLevel = 'N' THEN 1 ELSE 0 END AS IsLeaf
           FROM PR
           WHERE WBS1 = @strWBS1
        ) AS X
      WHERE WBSLevel <= @tiMinExpWBSLevel

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabJTD (
    WBS1,
    WBS2,
    WBS3,
    Account,
    Vendor,
    JTDCost,
    JTDBill,
    JTDDirCost,
    JTDDirBill,
    JTDReimCost,
    JTDReimBill,
    JTDIndirCost,
    JTDIndirBill
  )
		SELECT
      X.WBS1,
      X.WBS2,
      X.WBS3,
      X.Account,
      X.Vendor,
      ISNULL(SUM(JTDCost), 0.0000) AS JTDCost,
      ISNULL(SUM(JTDBill), 0.0000) AS JTDBill,
      ISNULL(SUM(JTDDirCost), 0.0000) AS JTDDirCost,
      ISNULL(SUM(JTDDirBill), 0.0000) AS JTDDirBill,
      ISNULL(SUM(JTDReimCost), 0.0000) AS JTDReimCost,
      ISNULL(SUM(JTDReimBill), 0.0000) AS JTDReimBill,
      ISNULL(SUM(JTDIndirCost), 0.0000) AS JTDIndirCost,
      ISNULL(SUM(JTDIndirBill), 0.0000) AS JTDIndirBill
      FROM (
        SELECT
          LG.WBS1,
          LG.WBS2,
          LG.WBS3,
          LG.Account,
          LG.Vendor,
          SUM(LG.AmountProjectCurrency) AS JTDCost,  
          SUM(LG.BillExt) AS JTDBill,
          SUM(CASE WHEN CA.Type = 7 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
          SUM(CASE WHEN CA.Type = 7 THEN LG.BillExt ELSE 0 END) AS JTDDirBill,
          SUM(CASE WHEN CA.Type = 5 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
          SUM(CASE WHEN CA.Type = 5 THEN LG.BillExt ELSE 0 END) AS JTDReimBill,
          SUM(CASE WHEN CA.Type = 9 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDIndirCost,
          SUM(CASE WHEN CA.Type = 9 THEN LG.BillExt ELSE 0 END) AS JTDIndirBill
          FROM LedgerAR AS LG
            INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
          WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (5, 7, 9)
          GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
        UNION ALL SELECT
          LG.WBS1,
          LG.WBS2,
          LG.WBS3,
          LG.Account,
          LG.Vendor,
          SUM(LG.AmountProjectCurrency) AS JTDCost,  
          SUM(LG.BillExt) AS JTDBill,
          SUM(CASE WHEN CA.Type = 7 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
          SUM(CASE WHEN CA.Type = 7 THEN LG.BillExt ELSE 0 END) AS JTDDirBill,
          SUM(CASE WHEN CA.Type = 5 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
          SUM(CASE WHEN CA.Type = 5 THEN LG.BillExt ELSE 0 END) AS JTDReimBill,
          SUM(CASE WHEN CA.Type = 9 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDIndirCost,
          SUM(CASE WHEN CA.Type = 9 THEN LG.BillExt ELSE 0 END) AS JTDIndirBill
          FROM LedgerAP AS LG
            INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
          WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (5, 7, 9)
          GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
        UNION ALL SELECT
          LG.WBS1,
          LG.WBS2,
          LG.WBS3,
          LG.Account,
          LG.Vendor,
          SUM(LG.AmountProjectCurrency) AS JTDCost,  
          SUM(LG.BillExt) AS JTDBill,
          SUM(CASE WHEN CA.Type = 7 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
          SUM(CASE WHEN CA.Type = 7 THEN LG.BillExt ELSE 0 END) AS JTDDirBill,
          SUM(CASE WHEN CA.Type = 5 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
          SUM(CASE WHEN CA.Type = 5 THEN LG.BillExt ELSE 0 END) AS JTDReimBill,
          SUM(CASE WHEN CA.Type = 9 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDIndirCost,
          SUM(CASE WHEN CA.Type = 9 THEN LG.BillExt ELSE 0 END) AS JTDIndirBill
          FROM LedgerEX AS LG
            INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
          WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (5, 7, 9)
          GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
        UNION ALL SELECT
          LG.WBS1,
          LG.WBS2,
          LG.WBS3,
          LG.Account,
          LG.Vendor,
          SUM(LG.AmountProjectCurrency) AS JTDCost,  
          SUM(LG.BillExt) AS JTDBill,
          SUM(CASE WHEN CA.Type = 7 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
          SUM(CASE WHEN CA.Type = 7 THEN LG.BillExt ELSE 0 END) AS JTDDirBill,
          SUM(CASE WHEN CA.Type = 5 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
          SUM(CASE WHEN CA.Type = 5 THEN LG.BillExt ELSE 0 END) AS JTDReimBill,
          SUM(CASE WHEN CA.Type = 9 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDIndirCost,
          SUM(CASE WHEN CA.Type = 9 THEN LG.BillExt ELSE 0 END) AS JTDIndirBill
          FROM LedgerMISC AS LG
            INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
          WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (5, 7, 9)
          GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
        UNION ALL SELECT
          POC.WBS1,
          POC.WBS2,
          POC.WBS3,
          POC.Account,
          POM.Vendor,
          SUM(AmountProjectCurrency) AS JTDCost,  
          SUM(BillExt) AS JTDBill,
          SUM(CASE WHEN CA.Type = 7 THEN AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
          SUM(CASE WHEN CA.Type = 7 THEN BillExt ELSE 0 END) AS JTDDirBill,
          SUM(CASE WHEN CA.Type = 5 THEN AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
          SUM(CASE WHEN CA.Type = 5 THEN BillExt ELSE 0 END) AS JTDReimBill,
          SUM(CASE WHEN CA.Type = 9 THEN AmountProjectCurrency ELSE 0 END) AS JTDIndirCost,
          SUM(CASE WHEN CA.Type = 9 THEN BillExt ELSE 0 END) AS JTDIndirBill
          FROM POCommitment AS POC 
            INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
            INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey
            INNER JOIN CA ON POC.Account = CA.Account
          WHERE POM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (5, 7, 9) AND 
            (AmountProjectCurrency != 0 AND BillExt != 0)
          GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
        UNION ALL SELECT
          POC.WBS1,
          POC.WBS2,
          POC.WBS3,
          POC.Account,
          POM.Vendor,
          SUM(AmountProjectCurrency) AS JTDCost,  
          SUM(BillExt) AS JTDBill,
          SUM(CASE WHEN CA.Type = 7 THEN AmountProjectCurrency ELSE 0 END) AS JTDDirCost,
          SUM(CASE WHEN CA.Type = 7 THEN BillExt ELSE 0 END) AS JTDDirBill,
          SUM(CASE WHEN CA.Type = 5 THEN AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
          SUM(CASE WHEN CA.Type = 5 THEN BillExt ELSE 0 END) AS JTDReimBill,
          SUM(CASE WHEN CA.Type = 9 THEN AmountProjectCurrency ELSE 0 END) AS JTDIndirCost,
          SUM(CASE WHEN CA.Type = 9 THEN BillExt ELSE 0 END) AS JTDIndirBill
          FROM POCommitment AS POC 
            INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
            INNER JOIN POCOMaster AS POCOM ON POD.COPKey = POCOM.PKey
            INNER JOIN POMaster AS POM ON POCOM.MasterPKey = POM.MasterPKey
            INNER JOIN CA ON POC.Account = CA.Account
          WHERE POCOM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (5, 7, 9) AND 
            (AmountProjectCurrency != 0 AND BillExt != 0)
          GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
      ) AS X
      GROUP BY X.WBS1, X.WBS2, X.WBS3, X.Account, X.Vendor
      HAVING SUM(ISNULL(JTDCost, 0.0000)) <> 0 OR SUM(ISNULL(JTDBill, 0.0000)) <> 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save RPTask/PNTask that are mapped to @strWBS1. There could be multiple Plans with UlilizationIncludeFlag = 'Y'
  -- Save TPD to be used later.

  IF (@strVorN = 'V')
    BEGIN

      INSERT @tabMappedTask
        (PlanID,
         TaskID,
         WBS1,
         WBS2,
         WBS3,
         StartDate,
         EndDate,
         WBSType,
         ParentOutlineNumber,
         OutlineNumber,
         ChildrenCount,
         OutlineLevel,
         BaselineExpCost,
         BaselineExpBill
        )
        SELECT
          P.PlanID,
          T.TaskID,
          T.WBS1,
          ISNULL(T.WBS2, ' '),
          ISNULL(T.WBS3, ' '),
          T.StartDate,
          T.EndDate,
          T.WBSType,
          T.ParentOutlineNumber,
          T.OutlineNumber,
          T.ChildrenCount,
          T.OutlineLevel,
          T.BaselineExpCost,
          T.BaselineExpBill
          FROM RPTask AS T
            INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND T.WBS1 = @strWBS1 AND
              ((T.WBSType = 'WBS1' AND T.WBS1 <> '<none>') OR
               (T.WBSType = 'WBS2' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>') OR
               (T.WBSType = 'WBS3' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>' AND T.WBS3 <> '<none>'))
            INNER JOIN @tabWBSTree AS WT ON
              T.WBS1 = WT.WBS1 AND ISNULL(T.WBS2, ' ') = WT.WBS2 AND ISNULL(T.WBS3, ' ') = WT.WBS3

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Find Plan.

      SELECT @strPlanID = P.PlanID
        FROM RPPlan AS P
        WHERE P.UtilizationIncludeFlg = 'Y' AND P.WBS1 = @strWBS1

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabPlanned (
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor,
        StartDate,
        EndDate,
        PlannedCost,
        PlannedBill,
        PlannedDirCost,
        PlannedDirBill,
        PlannedReimCost,
        PlannedReimBill,
        PlannedIndirCost,
        PlannedIndirBill,
        ETCCost,
        ETCBill
      )
        SELECT
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.Account AS Account,
          X.Vendor AS Vendor,
          MIN(X.StartDate) AS StartDate,
          MAX(X.EndDate) AS EndDate,
          ISNULL(SUM(X.PlannedCost), 0.0000) AS PlannedCost,
          ISNULL(SUM(X.PlannedBill), 0.0000) AS PlannedBill,
          ISNULL(SUM(X.PlannedDirCost), 0.0000) AS PlannedDirCost,
          ISNULL(SUM(X.PlannedDirBill), 0.0000) AS PlannedDirBill,
          ISNULL(SUM(X.PlannedReimCost), 0.0000) AS PlannedReimCost,
          ISNULL(SUM(X.PlannedReimBill), 0.0000) AS PlannedReimBill,
          ISNULL(SUM(X.PlannedIndirCost), 0.0000) AS PlannedIndirCost,
          ISNULL(SUM(X.PlannedIndirBill), 0.0000) AS PlannedIndirBill,
          ISNULL(SUM(X.ETCCost), 0.0000) AS ETCCost,
          ISNULL(SUM(X.ETCBill), 0.0000) AS ETCBill
          FROM (
            /* Find TPD rows attached to RPExpense rows for Plan that is mapped to Project in question */
            SELECT
              E.WBS1 AS WBS1,
              E.WBS2 AS WBS2,
              E.WBS3 AS WBS3,
              E.Account AS Account,
              E.Vendor AS Vendor,
              TPD.StartDate AS StartDate,
              TPD.EndDate AS EndDate,
              ISNULL(TPD.PeriodCost, 0) AS PlannedCost, 
              ISNULL(TPD.PeriodBill, 0) AS PlannedBill,
              CASE WHEN CA.Type = 7 THEN ISNULL(TPD.PeriodCost, 0) ELSE 0 END AS PlannedDirCost,
              CASE WHEN CA.Type = 7 THEN ISNULL(TPD.PeriodBill, 0) ELSE 0 END AS PlannedDirBill,
              CASE WHEN CA.Type = 5 THEN ISNULL(TPD.PeriodCost, 0) ELSE 0 END AS PlannedReimCost,
              CASE WHEN CA.Type = 5 THEN ISNULL(TPD.PeriodBill, 0) ELSE 0 END AS PlannedReimBill,
              CASE WHEN CA.Type = 9 THEN ISNULL(TPD.PeriodCost, 0) ELSE 0 END AS PlannedIndirCost,
              CASE WHEN CA.Type = 9 THEN ISNULL(TPD.PeriodBill, 0) ELSE 0 END AS PlannedIndirBill,
              CASE 
                WHEN (TPD.StartDate >= @dtETCDate AND TPD.EndDate >= @dtETCDate) THEN TPD.PeriodCost
	              WHEN (TPD.StartDate < @dtETCDate AND TPD.EndDate >= @dtETCDate) THEN TPD.PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) 
                ELSE 0
              END AS ETCCost,
              CASE 
                WHEN (TPD.StartDate >= @dtETCDate AND TPD.EndDate >= @dtETCDate) THEN TPD.PeriodBill
	              WHEN (TPD.StartDate < @dtETCDate AND TPD.EndDate >= @dtETCDate) THEN TPD.PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) 
                ELSE 0
              END AS ETCBill
              FROM RPPlannedExpenses AS TPD
                INNER JOIN RPExpense AS E ON TPD.PlanID = E.PlanID AND TPD.TaskID = E.TaskID AND TPD.ExpenseID = E.ExpenseID AND TPD.ExpenseID IS NOT NULL
                INNER JOIN CA ON E.Account = CA.Account
              WHERE TPD.PlanID = @strPlanID
          ) AS X
          GROUP BY WBS1, WBS2, WBS3, Account, Vendor

    END /* End If (@strVorN = 'V') */

  ELSE IF (@strVorN = 'N')
    BEGIN

      INSERT @tabMappedTask
        (PlanID,
         TaskID,
         WBS1,
         WBS2,
         WBS3,
         StartDate,
         EndDate,
         WBSType,
         ParentOutlineNumber,
         OutlineNumber,
         ChildrenCount,
         OutlineLevel,
         BaselineExpCost,
         BaselineExpBill
        )
        SELECT
          P.PlanID,
          T.TaskID,
          T.WBS1,
          ISNULL(T.WBS2, ' '),
          ISNULL(T.WBS3, ' '),
          T.StartDate,
          T.EndDate,
          T.WBSType,
          T.ParentOutlineNumber,
          T.OutlineNumber,
          T.ChildrenCount,
          T.OutlineLevel,
          T.BaselineExpCost,
          T.BaselineExpBill
          FROM PNTask AS T
            INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND T.WBS1 = @strWBS1 AND
              ((T.WBSType = 'WBS1' AND T.WBS1 <> '<none>') OR
               (T.WBSType = 'WBS2' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>') OR
               (T.WBSType = 'WBS3' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>' AND T.WBS3 <> '<none>'))
            INNER JOIN @tabWBSTree AS WT ON
              T.WBS1 = WT.WBS1 AND ISNULL(T.WBS2, ' ') = WT.WBS2 AND ISNULL(T.WBS3, ' ') = WT.WBS3

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Find Plan.

      SELECT @strPlanID = P.PlanID
        FROM PNPlan AS P
        WHERE P.UtilizationIncludeFlg = 'Y' AND P.WBS1 = @strWBS1

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabPlanned (
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor,
        StartDate,
        EndDate,
        PlannedCost,
        PlannedBill,
        PlannedDirCost,
        PlannedDirBill,
        PlannedReimCost,
        PlannedReimBill,
        PlannedIndirCost,
        PlannedIndirBill,
        ETCCost,
        ETCBill
      )
        SELECT
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.Account AS Account,
          X.Vendor AS Vendor,
          MIN(X.StartDate) AS StartDate,
          MAX(X.EndDate) AS EndDate,
          ISNULL(SUM(X.PlannedCost), 0.0000) AS PlannedCost,
          ISNULL(SUM(X.PlannedBill), 0.0000) AS PlannedBill,
          ISNULL(SUM(X.PlannedDirCost), 0.0000) AS PlannedDirCost,
          ISNULL(SUM(X.PlannedDirBill), 0.0000) AS PlannedDirBill,
          ISNULL(SUM(X.PlannedReimCost), 0.0000) AS PlannedReimCost,
          ISNULL(SUM(X.PlannedReimBill), 0.0000) AS PlannedReimBill,
          ISNULL(SUM(X.PlannedIndirCost), 0.0000) AS PlannedIndirCost,
          ISNULL(SUM(X.PlannedIndirBill), 0.0000) AS PlannedIndirBill,
          ISNULL(SUM(X.ETCCost), 0.0000) AS ETCCost,
          ISNULL(SUM(X.ETCBill), 0.0000) AS ETCBill
          FROM (
            /* For Navigator Plan, use Planned Cost and Bill Amounts from PNExpense rows.  */
            /* There is no need to sum up from the TPD rows.                               */
            SELECT 
              E.WBS1 AS WBS1,
              E.WBS2 AS WBS2,
              E.WBS3 AS WBS3,
              E.Account AS Account,
              E.Vendor AS Vendor,
              E.StartDate AS StartDate,
              E.EndDate AS EndDate,
              ISNULL(E.PlannedExpCost, 0) AS PlannedCost, 
              ISNULL(E.PlannedExpBill, 0) AS PlannedBill,
              CASE WHEN CA.Type = 7 THEN ISNULL(E.PlannedExpCost, 0) ELSE 0 END AS PlannedDirCost,
              CASE WHEN CA.Type = 7 THEN ISNULL(E.PlannedExpBill, 0) ELSE 0 END AS PlannedDirBill,
              CASE WHEN CA.Type = 5 THEN ISNULL(E.PlannedExpCost, 0) ELSE 0 END AS PlannedReimCost,
              CASE WHEN CA.Type = 5 THEN ISNULL(E.PlannedExpBill, 0) ELSE 0 END AS PlannedReimBill,
              CASE WHEN CA.Type = 9 THEN ISNULL(E.PlannedExpCost, 0) ELSE 0 END AS PlannedIndirCost,
              CASE WHEN CA.Type = 9 THEN ISNULL(E.PlannedExpBill, 0) ELSE 0 END AS PlannedIndirBill,
              0.0000 ETCCost,
              0.0000 ETCBill
              FROM PNExpense AS E
                INNER JOIN CA ON E.Account = CA.Account
              WHERE E.PlanID = @strPlanID
          ) AS X
          GROUP BY WBS1, WBS2, WBS3, Account, Vendor

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabETC (
        WBS1,
        WBS2,
        WBS3,
        ETCCost,
        ETCBill
      )
        SELECT
          WBS1 AS WBS1,
          WBS2 AS WBS2,
          WBS3 AS WBS3,
          SUM(ISNULL(ETCCost, 0.0000)) AS ETCCost,
          SUM(ISNULL(ETCBill, 0.0000)) AS ETCBill
          FROM (
            SELECT
              R.WBS1 AS WBS1,
              R.WBS2 AS WBS2,
              R.WBS3 AS WBS3,
              R.Account AS Account,
              R.Vendor AS Vendor,
              CASE
                WHEN ISNULL(TPD.PlannedCost, 0.0000) > ISNULL(JTD.JTDCost, 0.0000)
                THEN ISNULL(TPD.PlannedCost, 0.0000) - ISNULL(JTD.JTDCost, 0.0000)
                ELSE 0.0000
              END AS ETCCost,
              CASE
                WHEN ISNULL(TPD.PlannedBill, 0.0000) > ISNULL(JTD.JTDBill, 0.0000)
                THEN ISNULL(TPD.PlannedBill, 0.0000) - ISNULL(JTD.JTDBill, 0.0000)
                ELSE 0.0000
              END AS ETCBill

              FROM (
                SELECT DISTINCT
                  WBS1 AS WBS1,
                  ISNULL(WBS2, ' ') AS WBS2,
                  ISNULL(WBS3, ' ') AS WBS3,
                  Account AS Account,
                  Vendor AS Vendor
                  FROM PNExpense
                  WHERE PlanID = @strPlanID
              ) AS R

                LEFT JOIN (
                  SELECT
                    WBS1 AS WBS1,
                    ISNULL(WBS2, ' ') AS WBS2,
                    ISNULL(WBS3, ' ') AS WBS3,
                    Account AS Account,
                    Vendor AS Vendor,
                    SUM(PlannedCost) AS PlannedCost,
                    SUM(PlannedBill) AS PlannedBill
                    FROM @tabPlanned
                    GROUP BY WBS1, WBS2, WBS3, Account, Vendor
                ) AS TPD
                  ON R.WBS1 = TPD.WBS1 AND R.WBS2 = TPD.WBS2 AND R.WBS3 = TPD.WBS3 AND R.Account = TPD.Account AND ISNULL(R.Vendor, '|') = ISNULL(TPD.Vendor, '|')

                LEFT JOIN (
                  SELECT
                    WTZ.WBS1 AS WBS1,
                    WTZ.WBS2 AS WBS2,
                    WTZ.WBS3 AS WBS3,
                    Account AS Account,
                    Vendor AS Vendor,
                    ISNULL(SUM(JZ.JTDCost), 0.0000) AS JTDCost,
                    ISNULL(SUM(JZ.JTDBill), 0.0000) AS JTDBill
                    FROM @tabWBSTree AS WTZ
                      OUTER APPLY (
                        SELECT
                          Account AS Account,
                          Vendor AS Vendor,
                          ISNULL(SUM(TPD.JTDCost), 0.0000) AS JTDCost,
                          ISNULL(SUM(TPD.JTDBill), 0.0000) AS JTDBill
                          FROM @tabJTD AS TPD
                          WHERE TPD.WBS1 = WTZ.WBS1 AND
                            (ISNULL(TPD.WBS2, '|') LIKE CASE WHEN WTZ.WBS2 = ' ' THEN '%' ELSE WTZ.WBS2 END) AND
                            (ISNULL(TPD.WBS3, '|') LIKE CASE WHEN WTZ.WBS3 = ' ' THEN '%' ELSE WTZ.WBS3 END) 
                          GROUP BY TPD.WBS1, TPD.WBS2, TPD.WBS3, TPD.Account, TPD.Vendor
                      ) AS JZ
                    GROUP BY WTZ.WBS1, WTZ.WBS2, WTZ.WBS3, JZ.Account, JZ.Vendor
                ) AS JTD 
                  ON R.WBS1 = JTD.WBS1 AND R.WBS2 = JTD.WBS2 AND R.WBS3 = JTD.WBS3 AND R.Account = JTD.Account AND ISNULL(R.Vendor, '|') = ISNULL(JTD.Vendor, '|')

          ) AS ETC
            GROUP BY WBS1, WBS2, WBS3

   END /* End If (@strVorN = 'N') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Combine data for Task rows.
  -- ContractAmt and JTDExpBill come from Project, therefore should not be counted multiple times.
  -- PlannedExpBill comes from all RPTask rows of Plans that were mapped to the Project.
  --   There could be more than one Plan that was mapped to the Project.
  --   Therefore, PlannedExpBill need to be grouped by WBS1/WBS2/WBS3 for the collection of all RPTask rows. 

  INSERT @tabExpTask (
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    Name,
    StartDate,
    EndDate,
    MinDate,
    MaxDate,
    WBSType,
    ParentOutlineNumber,
    OutlineNumber,
    OutlineLevel,
    PlannedExpCost,
    PlannedReimExpCost,
    PlannedDirExpCost,
    PlannedIndirExpCost,
    PlannedExpBill,
    PlannedReimExpBill,
    PlannedDirExpBill,
    PlannedIndirExpBill,
    BaselineExpCost,
    BaselineExpBill,
    JTDExpCost,
    JTDReimExpCost,
    JTDDirExpCost,
    JTDIndirExpCost,
    JTDExpBill,
    JTDReimExpBill,
    JTDDirExpBill,
    JTDIndirExpBill,
    FeeExpCost,
    FeeExpBill,
    FeeDirExpCost,
    FeeDirExpBill,
    FeeReimExpCost,
    FeeReimExpBill,
    MarkupExpCost,
    FeeJTDExpBill,
    ETCExpCost,
    ETCExpBill
  )
    SELECT
      X.PlanID AS PlanID,
      X.TaskID AS TaskID,
      WT.WBS1 AS WBS1,
      WT.WBS2 AS WBS2,
      WT.WBS3 AS WBS3,
      WT.Name AS Name,
      X.StartDate AS StartDate,
      X.EndDate AS EndDate,
      PX.StartDate AS MinDate,
      PX.EndDate AS MaxDate,
      X.WBSType AS WBSType,
      X.ParentOutlineNumber AS ParentOutlineNumber,
      X.OutlineNumber AS OutlineNumber,
      X.OutlineLevel AS OutlineLevel,
      ISNULL(PX.PlannedExpCost, 0.0000) AS PlannedExpCost,
      ISNULL(PX.PlannedReimExpCost, 0.0000) AS PlannedReimExpCost,
      ISNULL(PX.PlannedDirExpCost, 0.0000) AS PlannedDirExpCost,
      ISNULL(PX.PlannedIndirExpCost, 0.0000) AS PlannedIndirExpCost,
      ISNULL(PX.PlannedExpBill, 0.0000) AS PlannedExpBill,
      ISNULL(PX.PlannedReimExpBill, 0.0000) AS PlannedReimExpBill,
      ISNULL(PX.PlannedDirExpBill, 0.0000) AS PlannedDirExpBill,
      ISNULL(PX.PlannedIndirExpBill, 0.0000) AS PlannedIndirExpBill,
      ISNULL(X.BaselineExpCost, 0.0000) AS BaselineExpCost,
      ISNULL(X.BaselineExpBill, 0.0000) AS BaselineExpBill,
      ISNULL(JX.JTDExpCost, 0.0000) AS JTDExpCost,
      ISNULL(JX.JTDReimExpCost, 0.0000) AS JTDReimExpCost,
      ISNULL(JX.JTDDirExpCost, 0.0000) AS JTDDirExpCost,
      ISNULL(JX.JTDIndirExpCost, 0.0000) AS JTDIndirExpCost,
      ISNULL(JX.JTDExpBill, 0.0000) AS JTDExpBill,
      ISNULL(JX.JTDReimExpBill, 0.0000) AS JTDReimExpBill,
      ISNULL(JX.JTDDirExpBill, 0.0000) AS JTDDirExpBill,
      ISNULL(JX.JTDIndirExpBill, 0.0000) AS JTDIndirExpBill,
      ISNULL(WT.FeeExpCost, 0.0000) AS FeeExpCost,
      ISNULL(WT.FeeExpBill, 0.0000) AS FeeExpBill,
      ISNULL(WT.FeeDirExpCost, 0.0000) AS FeeDirExpCost,
      ISNULL(WT.FeeDirExpBill, 0.0000) AS FeeDirExpBill,
      ISNULL(WT.FeeReimExpCost, 0.0000) AS FeeReimExpCost,
      ISNULL(WT.FeeReimExpBill, 0.0000) AS FeeReimExpBill,
      (ISNULL(WT.FeeExpCost, 0.0000) - ISNULL(PX.PlannedExpCost, 0.0000)) AS MarkupExpCost,
      (ISNULL(WT.FeeExpBill, 0.0000) - ISNULL(JX.JTDExpBill, 0.0000)) AS FeeJTDExpBill,

      CASE
        WHEN @strVorN = 'V' THEN ISNULL(PX.ETCCost, 0.0000)
        WHEN @strVorN = 'N' THEN ISNULL(EX.ETCCost, 0.0000)
        ELSE 0.00000
      END AS ETCExpCost,

      CASE
        WHEN @strVorN = 'V' THEN ISNULL(PX.ETCBill, 0.0000)
        WHEN @strVorN = 'N' THEN ISNULL(EX.ETCBill, 0.0000)
        ELSE 0.00000
      END AS ETCExpBill

      FROM @tabWBSTree AS WT

        LEFT JOIN (
          SELECT
            MIN(MT.PlanID) AS PlanID,
            MIN(MT.TaskID) AS TaskID,
            MT.WBS1,
            MT.WBS2,
            MT.WBS3,
            MIN(MT.StartDate) AS StartDate,
            MAX(MT.EndDate) AS EndDate,
            MIN(MT.WBSType) AS WBSType,
            MIN(MT.ParentOutlineNumber) AS ParentOutlineNumber,
            MIN(MT.OutlineNumber) AS OutlineNumber,
            MIN(MT.OutlineLevel) AS OutlineLevel,
            SUM(MT.BaselineExpCost) AS BaselineExpCost,
            SUM(MT.BaselineExpBill) AS BaselineExpBill
            FROM @tabMappedTask AS MT
            GROUP BY MT.WBS1, MT.WBS2, MT.WBS3
        ) AS X ON WT.WBS1 = X.WBS1 AND WT.WBS2 = X.WBS2 AND WT.WBS3 = X.WBS3

        LEFT JOIN (
          SELECT
            WTZ.WBS1 AS WBS1,
            WTZ.WBS2 AS WBS2,
            WTZ.WBS3 AS WBS3,
            MIN(PZ.StartDate) AS StartDate,
            MAX(PZ.EndDate) AS EndDate,
            ISNULL(SUM(PZ.PlannedExpCost), 0.0000) AS PlannedExpCost,
            ISNULL(SUM(PZ.PlannedExpBill), 0.0000) AS PlannedExpBill,
            ISNULL(SUM(PZ.PlannedDirExpCost), 0.0000) AS PlannedDirExpCost,
            ISNULL(SUM(PZ.PlannedDirExpBill), 0.0000) AS PlannedDirExpBill,
            ISNULL(SUM(PZ.PlannedReimExpCost), 0.0000) AS PlannedReimExpCost,
            ISNULL(SUM(PZ.PlannedReimExpBill), 0.0000) AS PlannedReimExpBill,
            ISNULL(SUM(PZ.PlannedIndirExpCost), 0.0000) AS PlannedIndirExpCost,
            ISNULL(SUM(PZ.PlannedIndirExpBill), 0.0000) AS PlannedIndirExpBill,
            ISNULL(SUM(PZ.ETCCost), 0.0000) AS ETCCost,
            ISNULL(SUM(PZ.ETCBill), 0.0000) AS ETCBill
            FROM @tabWBSTree AS WTZ
              OUTER APPLY (
                SELECT
                  MIN(TPD.StartDate) AS StartDate,
                  MAX(TPD.EndDate) AS EndDate,
                  ISNULL(SUM(TPD.PlannedCost), 0.0000) AS PlannedExpCost,
                  ISNULL(SUM(TPD.PlannedBill), 0.0000) AS PlannedExpBill,
                  ISNULL(SUM(TPD.PlannedDirCost), 0.0000) AS PlannedDirExpCost,
                  ISNULL(SUM(TPD.PlannedDirBill), 0.0000) AS PlannedDirExpBill,
                  ISNULL(SUM(TPD.PlannedReimCost), 0.0000) AS PlannedReimExpCost,
                  ISNULL(SUM(TPD.PlannedReimBill), 0.0000) AS PlannedReimExpBill,
                  ISNULL(SUM(TPD.PlannedIndirCost), 0.0000) AS PlannedIndirExpCost,
                  ISNULL(SUM(TPD.PlannedIndirBill), 0.0000) AS PlannedIndirExpBill,
                  ISNULL(SUM(TPD.ETCCost), 0.0000) AS ETCCost,
                  ISNULL(SUM(TPD.ETCBill), 0.0000) AS ETCBill
                  FROM @tabPlanned AS TPD
                  WHERE TPD.WBS1 = WTZ.WBS1 AND
                    (ISNULL(TPD.WBS2, '|') LIKE CASE WHEN WTZ.WBS2 = ' ' THEN '%' ELSE WTZ.WBS2 END) AND
                    (ISNULL(TPD.WBS3, '|') LIKE CASE WHEN WTZ.WBS3 = ' ' THEN '%' ELSE WTZ.WBS3 END) 
                  GROUP BY TPD.WBS1, TPD.WBS2, TPD.WBS3
              ) AS PZ
            GROUP BY WTZ.WBS1, WTZ.WBS2, WTZ.WBS3
        ) AS PX ON WT.WBS1 = PX.WBS1 AND WT.WBS2 = PX.WBS2 AND WT.WBS3 = PX.WBS3

        LEFT JOIN (
          SELECT
            WTZ.WBS1 AS WBS1,
            WTZ.WBS2 AS WBS2,
            WTZ.WBS3 AS WBS3,
            ISNULL(SUM(JZ.JTDExpCost), 0.0000) AS JTDExpCost,
            ISNULL(SUM(JZ.JTDExpBill), 0.0000) AS JTDExpBill,
            ISNULL(SUM(JZ.JTDDirExpCost), 0.0000) AS JTDDirExpCost,
            ISNULL(SUM(JZ.JTDDirExpBill), 0.0000) AS JTDDirExpBill,
            ISNULL(SUM(JZ.JTDReimExpCost), 0.0000) AS JTDReimExpCost,
            ISNULL(SUM(JZ.JTDReimExpBill), 0.0000) AS JTDReimExpBill,
            ISNULL(SUM(JZ.JTDIndirExpCost), 0.0000) AS JTDIndirExpCost,
            ISNULL(SUM(JZ.JTDIndirExpBill), 0.0000) AS JTDIndirExpBill
            FROM @tabWBSTree AS WTZ
              OUTER APPLY (
                SELECT
                  ISNULL(SUM(TPD.JTDCost), 0.0000) AS JTDExpCost,
                  ISNULL(SUM(TPD.JTDBill), 0.0000) AS JTDExpBill,
                  ISNULL(SUM(TPD.JTDDirCost), 0.0000) AS JTDDirExpCost,
                  ISNULL(SUM(TPD.JTDDirBill), 0.0000) AS JTDDirExpBill,
                  ISNULL(SUM(TPD.JTDReimCost), 0.0000) AS JTDReimExpCost,
                  ISNULL(SUM(TPD.JTDReimBill), 0.0000) AS JTDReimExpBill,
                  ISNULL(SUM(TPD.JTDIndirCost), 0.0000) AS JTDIndirExpCost,
                  ISNULL(SUM(TPD.JTDIndirBill), 0.0000) AS JTDIndirExpBill
                  FROM @tabJTD AS TPD
                  WHERE TPD.WBS1 = WTZ.WBS1 AND
                    (ISNULL(TPD.WBS2, '|') LIKE CASE WHEN WTZ.WBS2 = ' ' THEN '%' ELSE WTZ.WBS2 END) AND
                    (ISNULL(TPD.WBS3, '|') LIKE CASE WHEN WTZ.WBS3 = ' ' THEN '%' ELSE WTZ.WBS3 END) 
                  GROUP BY TPD.WBS1, TPD.WBS2, TPD.WBS3
              ) AS JZ
            GROUP BY WTZ.WBS1, WTZ.WBS2, WTZ.WBS3
        ) AS JX ON WT.WBS1 = JX.WBS1 AND WT.WBS2 = JX.WBS2 AND WT.WBS3 = JX.WBS3

        LEFT JOIN (
          SELECT
            WTZ.WBS1 AS WBS1,
            WTZ.WBS2 AS WBS2,
            WTZ.WBS3 AS WBS3,
            ISNULL(SUM(EZ.ETCCost), 0.0000) AS ETCCost,
            ISNULL(SUM(EZ.ETCBill), 0.0000) AS ETCBill
            FROM @tabWBSTree AS WTZ
              OUTER APPLY (
                SELECT
                  ISNULL(SUM(TPD.ETCCost), 0.0000) AS ETCCost,
                  ISNULL(SUM(TPD.ETCBill), 0.0000) AS ETCBill
                  FROM @tabETC AS TPD
                  WHERE TPD.WBS1 = WTZ.WBS1 AND
                    (ISNULL(TPD.WBS2, '|') LIKE CASE WHEN WTZ.WBS2 = ' ' THEN '%' ELSE WTZ.WBS2 END) AND
                    (ISNULL(TPD.WBS3, '|') LIKE CASE WHEN WTZ.WBS3 = ' ' THEN '%' ELSE WTZ.WBS3 END) 
                  GROUP BY TPD.WBS1, TPD.WBS2, TPD.WBS3
              ) AS EZ
            GROUP BY WTZ.WBS1, WTZ.WBS2, WTZ.WBS3
        ) AS EX ON WT.WBS1 = EX.WBS1 AND WT.WBS2 = EX.WBS2 AND WT.WBS3 = EX.WBS3

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
