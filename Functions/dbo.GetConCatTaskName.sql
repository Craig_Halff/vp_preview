SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetConCatTaskName]
 (@strPlanID varchar(32), 
  @strTaskID varchar(32)) 
  RETURNS Nvarchar(max)  AS
BEGIN

  DECLARE @strConcatName Nvarchar(max)

-- SQL to concatenate names of ancestors.
	SELECT
	@strConcatName = (
    ISNULL(
      REVERSE(STUFF(REVERSE((
        SELECT
          case 
			when LaborCode is null then XT.Name 
			else (
					  SELECT 
						LC1Desc  + 
						CASE WHEN LC2Desc>'' THEN ' / ' + LC2Desc ELSE  '' END  + 
						CASE WHEN LC3Desc>'' THEN ' / ' + LC3Desc ELSE  '' END  + 
						CASE WHEN LC4Desc>'' THEN ' / ' + LC4Desc ELSE  '' END  + 
						CASE WHEN LC5Desc>'' THEN ' / ' + LC5Desc ELSE  '' END 
					  FROM dbo.stRP$tabLaborCodeSubcodes(XT.LaborCode)
				  )  
		  end + '/'
          FROM rpTask AS XT 
          WHERE XT.PlanID = CT.PlanID AND CT.OutlineNUmber LIKE XT.OutlineNumber + '%'
          ORDER BY XT.OutlineNUmber 
          FOR XML PATH ('')
      )), 1, 1, ''))
    , '')
  ) 
  FROM rpTask AS CT
  WHERE CT.PlanID = @strPlanID and CT.TaskID = @strTaskID
  
  RETURN @strConcatName 

END -- GetConCatTaskName
GO
