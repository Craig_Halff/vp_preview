SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$ResRate]
  (@strPlanID varchar(32),
   @strResourceID Nvarchar(20) = NULL,
   @strGenericResourceID Nvarchar(20) = NULL,
   @dtStartDate datetime,
   @dtEndDate datetime
  )
  RETURNS decimal(19,4)

BEGIN

  DECLARE @strMultiCurrencyEnabled varchar(1)
  DECLARE @strBillingCurrencyCode Nvarchar(3)

  DECLARE @siRtMethod smallint
  DECLARE @intRtTabNo int
  DECLARE @siGRMethod smallint
  DECLARE @intGRTabNo int

  DECLARE @decRate decimal(19,4)
  DECLARE @decProvBillRate decimal(19,4)
  DECLARE @decLabBillMult decimal(19,4)

  DECLARE @siRtBillDecimals smallint

  DECLARE @siCategory smallint
  DECLARE @strGRLBCD Nvarchar(14)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT 
    @strMultiCurrencyEnabled = MultiCurrencyEnabled
    FROM FW_CFGSystem

  SET @decRate = 0.0000
  SET @siCategory = 0
  SET @strGRLBCD = NULL
  SET @decProvBillRate = 0.0000
  SET @siRtBillDecimals = 4

  SELECT
    @siRtMethod = BillingRtMethod,
    @intRtTabNo = BillingRtTableNo,
    @siGRMethod = GRMethod,
    @intGRTabNo = GRBillTableNo,
    @decLabBillMult = LabBillMultiplier,
    @strBillingCurrencyCode = BillingCurrencyCode
    FROM PNPlan WHERE PlanID = @strPlanID

  SELECT @decProvBillRate = 
    CASE WHEN @strMultiCurrencyEnabled <> 'Y'
      THEN EM.ProvBillRate 
      ELSE
        CASE WHEN ISNULL(MD.FunctionalCurrencyCode, N' ') = @strBillingCurrencyCode
          THEN EM.ProvBillRate
          ELSE 0.0000
        END
    END
    FROM EM 
      LEFT JOIN CFGMainData AS MD ON ISNULL(EM.Org, N'%') LIKE CASE WHEN @strMultiCurrencyEnabled = 'Y' THEN MD.Company + '%' ELSE '%' END
    WHERE Employee = @strResourceID

  SELECT
    @siCategory = Category,
    @strGRLBCD = LaborCode
    FROM GR WHERE Code = @strGenericResourceID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @decRate =
    dbo.RP$LabRate
      (@strResourceID,
       @siCategory,
       @strGRLBCD,
       NULL,
       @siRtMethod,
       @intRtTabNo,
       @siGRMethod,
       @intGRTabNo,
       @dtStartDate,
       @dtEndDate,
       @siRtBillDecimals,
       @decProvBillRate,
       0.0000)
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN(@decRate * @decLabBillMult)

END -- fn_PN$ResRate
GO
