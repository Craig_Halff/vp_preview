SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DLTK$NumWorkingDays]
  (@dtStartDate AS datetime, @dtEndDate AS datetime, @strCompany AS Nvarchar(14))
  RETURNS float
BEGIN

  DECLARE @flNumWorkingDays float
  DECLARE @flMultiplier float
  DECLARE @dtTemp datetime
  
  -- If Start Date is greater than End Date then swap the dates.
  -- The return value from this function will then be a negative number.
  
  SET @flMultiplier = 1.0
  
  IF (DATEDIFF(day, @dtStartDate, @dtEndDate) < 0)
    BEGIN
    
      SET @flMultiplier = -1.0
      
      SET @dtTemp = @dtStartDate
      SET @dtStartDate = @dtEndDate
      SET @dtEndDate = @dtTemp
    
    END
    
  -- If the total number of days between Start & End Dates is not divisible by seven then we need to figure out 
  -- how many of the extra days are non-working day.
  -- The number of extra days is the remainder (mod) of Total Days divides by seven.
  
  -- I used MIN function on iNonWorkPerWeek and strNonWork so that I don't have to use GROUP BY.
  -- GROUP BY does not return any row when CFGNonWorkingDay is empty.

  SELECT @flNumWorkingDays =
    (iTotalDays - 
     (iNonWorkPerWeek * (iTotalDays / 7)) - 
	   LEN(REPLACE(SUBSTRING(strNonWork + strNonWork, iEndDay + 8 - (iTotalDays % 7), (iTotalDays % 7)), 'N', '')) -
     (SELECT COUNT(*) FROM CFGHoliday 
		    WHERE (HolidayDate BETWEEN @dtStartDate AND @dtEndDate) 
		      AND SUBSTRING(strNonWork, DATEPART(dw, HolidayDate), 1) = 'N'
		      AND Company = @strCompany)) * @flMultiplier
  FROM
    (SELECT 
       DATEDIFF(day, @dtStartDate, @dtEndDate) + 1 AS iTotalDays,
       DATEPART(dw, @dtEndDate) AS iEndDay,
       CASE WHEN COUNT(*) = 0 THEN 2 
            ELSE MIN(LEN(REPLACE(SundayInd + MondayInd  + TuesdayInd + WednesdayInd + ThursdayInd + FridayInd + SaturdayInd, 'N', ''))) END AS iNonWorkPerWeek, 
       CASE WHEN COUNT(*) = 0 THEN 'YNNNNNY'
            ELSE MIN((SundayInd + MondayInd  + TuesdayInd + WednesdayInd + ThursdayInd + FridayInd + SaturdayInd)) END AS strNonWork
       FROM CFGNonWorkingDay WHERE Company = @strCompany) AS NWD

  RETURN @flNumWorkingDays

END -- fn_DLTK$NumWorkingDays
GO
