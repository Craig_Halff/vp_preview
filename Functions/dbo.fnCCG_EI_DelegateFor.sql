SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   FUNCTION [dbo].[fnCCG_EI_DelegateFor] (@employee Nvarchar(32), @wbs1 Nvarchar(30))
RETURNS @T TABLE (Employee Nvarchar(32))
AS BEGIN
	declare @ApprovalRequired varchar(1), @Delegation varchar(1)
	select @ApprovalRequired = DelegationApproval, @Delegation = Delegation from CCG_EI_Config

	if @Delegation = 'Y'
		INSERT INTO @T (Employee)
		SELECT distinct d1.Employee
			FROM PR
				left join CCG_EI_Delegation d1 on d1.Delegate = @employee AND (isnull( d1.ApprovedBy, '') <> '' or @ApprovalRequired = 'N') 
					AND GETDATE() between d1.FromDate and dateadd(d, 1, d1.ToDate) AND isnull(d1.ForWBS1,'') = ''		and isnull(d1.ForClientId, '') = ''
				left join CCG_EI_Delegation d2 on d2.Delegate = @employee AND (isnull( d2.ApprovedBy, '') <> '' or @ApprovalRequired = 'N') 
					AND GETDATE() between d2.FromDate and dateadd(d, 1, d2.ToDate) AND isnull(d2.ForWBS1,'') = ''		and isnull(d2.ForClientId, '') = PR.BillingClientID
				left join CCG_EI_Delegation d3 on d3.Delegate = @employee AND (isnull( d3.ApprovedBy, '') <> '' or @ApprovalRequired = 'N') 
					AND GETDATE() between d3.FromDate and dateadd(d, 1, d3.ToDate) AND isnull(d3.ForWBS1,'') = PR.WBS1	and isnull(d3.ForClientId, '') = ''
			WHERE PR.WBS1 = @wbs1 and PR.WBS2 = N' ' and PR.WBS3 = N' '
	RETURN
END
GO
