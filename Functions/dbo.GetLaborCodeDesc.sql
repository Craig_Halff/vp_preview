SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetLaborCodeDesc](
  @strLaborCode Nvarchar(14)
) 
  RETURNS Nvarchar(max) AS
BEGIN

  DECLARE @strTempLC Nvarchar(14) = ''
  DECLARE @strLaborCodeDesc Nvarchar(max) = ''
  DECLARE @strLC1Code Nvarchar(14) = ''
  DECLARE @strLC2Code Nvarchar(14) = ''
  DECLARE @strLC3Code Nvarchar(14) = ''
  DECLARE @strLC4Code Nvarchar(14) = ''
  DECLARE @strLC5Code Nvarchar(14) = ''
  DECLARE @strLC1Desc Nvarchar(max) = ''
  DECLARE @strLC2Desc Nvarchar(max) = ''
  DECLARE @strLC3Desc Nvarchar(max) = ''
  DECLARE @strLC4Desc Nvarchar(max) = ''
  DECLARE @strLC5Desc Nvarchar(max) = ''
  DECLARE @strLCDelimiter varchar(1) = ''

  DECLARE @siLCLevels smallint = 0
  DECLARE @siLC1Start smallint = 0
  DECLARE @siLC2Start smallint = 0
  DECLARE @siLC3Start smallint = 0
  DECLARE @siLC4Start smallint = 0
  DECLARE @siLC5Start smallint = 0
  DECLARE @siLC1Length smallint = 0
  DECLARE @siLC2Length smallint = 0
  DECLARE @siLC3Length smallint = 0
  DECLARE @siLC4Length smallint = 0
  DECLARE @siLC5Length smallint = 0

  DECLARE @bitLC1IsValid bit = 0
  DECLARE @bitLC2IsValid bit = 0
  DECLARE @bitLC3IsValid bit = 0
  DECLARE @bitLC4IsValid bit = 0
  DECLARE @bitLC5IsValid bit = 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get LCDelimiter and other LC formats.
  
  SELECT
    @siLCLevels = LCLevels,
    @strLCDelimiter = ISNULL(LTRIM(RTRIM(LCDelimiter)), ''),
    @siLC1Start = LC1Start,
    @siLC2Start = LC2Start,
    @siLC3Start = LC3Start,
    @siLC4Start = LC4Start,
    @siLC5Start = LC5Start,
    @siLC1Length = LC1Length,
    @siLC2Length = LC2Length,
    @siLC3Length = LC3Length,
    @siLC4Length = LC4Length,
    @siLC5Length = LC5Length
    FROM CFGFormat

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Decompose Labor Code string into individual levels using the formatting information in CFGFormat.

  SELECT 
    @strLC1Code = LEFT(SUBSTRING(@strLaborCode, @siLC1Start, @siLC1Length) + REPLICATE('~', @siLC1Length), @siLC1Length),
    @strLC2Code = LEFT(SUBSTRING(@strLaborCode, @siLC2Start, @siLC2Length) + REPLICATE('~', @siLC2Length), @siLC2Length),
    @strLC3Code = LEFT(SUBSTRING(@strLaborCode, @siLC3Start, @siLC3Length) + REPLICATE('~', @siLC3Length), @siLC3Length),
    @strLC4Code = LEFT(SUBSTRING(@strLaborCode, @siLC4Start, @siLC4Length) + REPLICATE('~', @siLC4Length), @siLC4Length),
    @strLC5Code = LEFT(SUBSTRING(@strLaborCode, @siLC5Start, @siLC5Length) + REPLICATE('~', @siLC5Length), @siLC5Length)

  -- Reconstruct Labor Code string with delimiters to see if the input Labor Code string had the correct delimiters.

  SET @strTempLC = 
 
    @strLC1Code +

    CASE WHEN (@siLCLevels >= 2) 
      THEN ISNULL((ISNULL(@strLCDelimiter, '') + @strLC2Code), '') 
      ELSE ''
    END + 

    CASE WHEN (@siLCLevels >= 3) 
      THEN ISNULL((ISNULL(@strLCDelimiter, '') + @strLC3Code), '') 
      ELSE ''
    END + 

    CASE WHEN (@siLCLevels >= 4) 
      THEN ISNULL((ISNULL(@strLCDelimiter, '') + @strLC4Code), '') 
      ELSE ''
    END + 

    CASE WHEN (@siLCLevels >= 5) 
      THEN ISNULL((ISNULL(@strLCDelimiter, '') + @strLC5Code), '') 
      ELSE ''
    END 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- If the reconstructed Labor Code string @strTempLC is not the same as input Labor Code string @strTempLC then exit this UDF.
  -- I used MAX function to make sure that each select in the following section will return 1 row of result 
  -- when Length = 0 for a sub-Level.

  IF (@strLaborCode = @strTempLC)
    BEGIN

      SELECT 
        @strLC1Desc = ISNULL(MAX(LC.Label), ''),
        @bitLC1IsValid = 
          CASE
            WHEN @siLC1Length = 0 THEN 1
            WHEN @siLC1Length > 0 AND MAX(LC.Label) IS NOT NULL THEN 1
            ELSE 0
          END
        FROM CFGLCCodes AS LC
        WHERE LC.LCLevel = 1 AND LC.Code = @strLC1Code

      SELECT 
        @strLC2Desc = ISNULL(MAX('~~~' + LC.Label), ''),
        @bitLC2IsValid =  
          CASE
            WHEN @siLC2Length = 0 THEN 1
            WHEN @siLC2Length > 0 AND MAX(LC.Label) IS NOT NULL THEN 1
            ELSE 0
          END
        FROM CFGLCCodes AS LC
        WHERE LC.LCLevel = 2 AND LC.Code = @strLC2Code

      SELECT 
        @strLC3Desc = ISNULL(MAX('~~~' + LC.Label), ''),
        @bitLC3IsValid = 
          CASE
            WHEN @siLC3Length = 0 THEN 1
            WHEN @siLC3Length > 0 AND MAX(LC.Label) IS NOT NULL THEN 1
            ELSE 0
          END
        FROM CFGLCCodes AS LC
        WHERE LC.LCLevel = 3 AND LC.Code = @strLC3Code

      SELECT 
        @strLC4Desc =ISNULL(MAX('~~~' + LC.Label), ''),
        @bitLC4IsValid = 
          CASE
            WHEN @siLC4Length = 0 THEN 1
            WHEN @siLC4Length > 0 AND MAX(LC.Label) IS NOT NULL THEN 1
            ELSE 0
          END
        FROM CFGLCCodes AS LC
        WHERE LC.LCLevel = 4 AND LC.Code = @strLC4Code

      SELECT 
        @strLC5Desc = ISNULL(MAX('~~~' + LC.Label), ''),
        @bitLC5IsValid = 
          CASE
            WHEN @siLC5Length = 0 THEN 1
            WHEN @siLC5Length > 0 AND MAX(LC.Label) IS NOT NULL THEN 1
            ELSE 0
          END
        FROM CFGLCCodes AS LC
        WHERE LC.LCLevel = 5 AND LC.Code = @strLC5Code

      SET @strLaborCodeDesc = 
        CASE
          WHEN (@bitLC1IsValid & @bitLC2IsValid & @bitLC3IsValid & @bitLC4IsValid & @bitLC5IsValid) = 1
          THEN @strLC1Desc + @strLC2Desc + @strLC3Desc + @strLC4Desc + @strLC5Desc
          ELSE ''
        END

    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN @strLaborCodeDesc 

END -- GetLaborCodeDesc
GO
