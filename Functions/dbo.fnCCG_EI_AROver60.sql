SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_AROver60] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS money
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select dbo.fnCCG_EI_AROver60('2003005.xx',' ',' ')
	select dbo.fnCCG_EI_AROver60('2003005.xx','1PD',' ')
	select dbo.fnCCG_EI_AROver60('2003005.xx','1PD','COD')
*/
	declare @res61 money, @total money

	select @res61=Sum(Case When DaysOld >= 61 Then InvoiceBalance Else 0 End), @total=Sum(InvoiceBalance)
	from (
		select IsNull(LedgerAR.Invoice, '') As Invoice, AR.InvoiceDate, 
			IsNull(PR.WBS1, '') As WBS1, LedgerAR.WBS2, LedgerAR.WBS3,
			Min(case when AR.InvoiceDate IS NULL then 0 else datediff(day,AR.InvoiceDate,GETDATE()) end) AS DaysOld, 
			Sum(case when LedgerAR.TransType='IN' then -Amount else case when LedgerAR.TransType='CR' and LedgerAR.SubType='T' then -Amount else Amount end end ) AS InvoiceBalance
		from LedgerAR LEFT JOIN BTBGSubs on LedgerAR.WBS1 = BTBGSubs.SubWBS1 
		left join PR as MAINPR on BTBGSubs.MainWBS1=MAINPR.WBS1 AND MAINPR.WBS2=' ' AND MAINPR.WBS3=' '
		, AR , PR 
		where
			LedgerAR.WBS1 = PR.WBS1 AND PR.WBS2 = /*N*/' ' AND PR.WBS3 = /*N*/' ' 
			 AND LedgerAR.WBS1 = AR.WBS1 AND LedgerAR.WBS2 = AR.WBS2 AND LedgerAR.WBS3 = AR.WBS3 AND LedgerAR.Invoice = AR.Invoice 
			AND LedgerAR.AutoEntry = 'N' 
			AND ((LedgerAR.TransType = /*N*/'IN' AND (LedgerAR.SubType <> 'X' OR LedgerAR.SubType Is Null)) 
			OR (LedgerAR.TransType = /*N*/'CR' AND LedgerAR.SubType IN ('R','T'))) 
			 AND ((PR.WBS1 = /*N*/@WBS1) OR (PR.WBS1 = BTBGSubs.subwbs1 AND (BTBGSubs.MAINWBS1 = /*N*/@WBS1) ))
		group by IsNull(LedgerAR.Invoice, ''), IsNull(PR.WBS1, ''), LedgerAR.WBS2, LedgerAR.WBS3, AR.InvoiceDate,
			--case when Min(AR.InvoiceDate) IS NULL then 0 else datediff(day,Min(AR.InvoiceDate),GETDATE()) end, 
			case when LedgerAR.TransType=/*N*/'CR' AND LedgerAR.SubType='R' then 'R' 
			 when LedgerAR.SubType='R' then 'E' 
			 when LedgerAR.TransType =/*N*/'CR' AND LedgerAR.SubType='T' then 'T' else ' ' end
	) as AROver60 where WBS1=@WBS1 and (@WBS2=' ' or WBS2=@WBS2) and (@WBS3=' ' or WBS3=@WBS3)
	if @total=0
		set @res61=null
	return @res61
END
GO
