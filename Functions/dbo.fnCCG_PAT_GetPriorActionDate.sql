SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   FUNCTION [dbo].[fnCCG_PAT_GetPriorActionDate] (@Seq int)
RETURNS datetime AS
BEGIN
      declare @res            datetime
      declare @PayableSeq     int
      declare @ActionDate     datetime

      --select @PayableSeq=PayableSeq, @ActionDate=ActionDate from CCG_PAT_History where Seq=@Seq

      select @res = Max(ActionDate)
      from CCG_PAT_History
      where PayableSeq = @PayableSeq and ActionDate < @ActionDate and ActionTaken=N'Stage Change'
      return @res
END
GO
