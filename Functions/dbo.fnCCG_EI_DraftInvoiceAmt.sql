SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_DraftInvoiceAmt] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS @T TABLE
(
	ColValue	money,
	ColDetail	varchar(100)	-- Set background color 
)
AS BEGIN
/*
	Copyright (c) 2017 EleVia Software.  All rights reserved.

*/
	declare @DraftNonFee money, @DraftFee money, @res money
	
	select @DraftNonFee = IsNull(Sum(Case When Section <> 'F' Then FinalAmount Else 0 End), 0)
	from billInvSums s left join BTBGSubs subs on subs.SubWBS1=s.BillWBS1	-- SPS change 3/15/2017 to disregard bad ghost entries left by Deltek
	where (s.MainWBS1=IsNull(subs.MainWBS1,@WBS1) and BillWBS1=@WBS1) and (@WBS2=' ' or @WBS2=BillWBS2) and (@WBS3=' ' or @WBS3=BillWBS3)
	  and Invoice = '<Draft>' and ArrayType = 'P'	-- SPS change 8/8/2014 from 'I'
	  and (s.MainWBS1=@WBS1 or s.MainWBS1=IsNull(subs.MainWBS1,''))

	select @DraftFee = (FeeToDate - IsNull(PriorFeeBilled,0.0)) from dbo.fnCCG_EI_FeeToDate(@WBS1, @WBS2, @WBS3)

	set @res = IsNull(@DraftNonFee,0) + IsNull(@DraftFee,0)
	insert into @T values (@res, '')
	return
END
GO
