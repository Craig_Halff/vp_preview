SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_BillRemain] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS @T TABLE
(
	ColValue	money,
	ColDetail	varchar(100)	-- Set background color to light red if spent exceeds Compensation and Compensation > 0
) 
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select * from dbo.fnCCG_EI_BillRemain('032025.00','','')

*/
	declare @res money, @billed decimal(19,2), @Compensation decimal(19,2),  @Detail varchar(100)  
	set @detail = ' '

	select @billed = dbo.fnCCG_EI_JTDBilled(@WBS1, @WBS2, @WBS3)
	select @Compensation = dbo.fnCCG_EI_Compensation(@WBS1, @WBS2, @WBS3)
	

	set @res = ISNULL(@Compensation,0) - ISNULL(@billed,0)
	if isnull(@res,0) < 0
		set @detail = 'BackgroundColor=#ffc0c0'
	if @res = 0 set @res = null

	insert into @T values (@res, @detail)
		
	return
END
GO
