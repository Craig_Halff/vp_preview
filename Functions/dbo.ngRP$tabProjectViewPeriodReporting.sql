SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ngRP$tabProjectViewPeriodReporting](
  @strRowID Nvarchar(255),
  @strScopeStartDate varchar(8) /* Date must be in format: 'yyyymmdd' regardless of UI Culture */,
	@numPeriods int,
	@strPeriodScale varchar(1),
  @strMode varchar(1) /* S = Self, C = Children */
)
  RETURNS @tabProjectViewPeriodReporting TABLE (
		SeqID int,
    AssignmentID Nvarchar(32) COLLATE database_default,
		TaskID varchar(32),
		ResourceID varchar(32),
		GenericResourceID varchar(20),
		StartDate datetime,
		PeriodHrs decimal(19,4),
		JTDHrs decimal(19,4),
		NumWorkingDays int,
		ETC varchar(1)
)

BEGIN


-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @strCompany Nvarchar(14)
  DECLARE @strResourceID Nvarchar(20)
  DECLARE @strResourceName Nvarchar(255)
  DECLARE @strGenericResourceID Nvarchar(20)
  DECLARE @strTaskID varchar(32)
  DECLARE @strResourceType varchar(1)
  DECLARE @strIDPrefix Nvarchar(255)
	DECLARE @strPlanID varchar(32)

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
  DECLARE @dtScopeStartDate datetime
  DECLARE @dtScopeEndDate datetime
  DECLARE @dtScopeBeforeDate datetime
  DECLARE @dtScopeAfterDate datetime
	DECLARE @dtMinEndDate datetime
	DECLARE @dtStart datetime
	DECLARE @dtEnd datetime
 
  DECLARE @siHrDecimals int /* CFGRMSettings.HrDecimals */

  DECLARE @intTaskCount int 
	DECLARE @intOutlineLevel int
	DECLARE @intWorkingHoursPerDay int

  DECLARE @bitIsLeafTask bit
 
  -- Declare Temp tables.

	DECLARE @tabCalendar TABLE (
		SeqID int,
		StartDate	datetime,
		EndDate	datetime,
		PeriodScale	varchar(1) COLLATE database_default,
		NumWorkingDays int,
		ETC varchar(1)
		UNIQUE(StartDate,EndDate,ETC)
	)

  DECLARE @tabAssignment TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ResourceID varchar(32) COLLATE database_default,
    GenericResourceID varchar(20) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    OutlineNumber varchar(255) COLLATE database_default,
		ChargeType varchar(1) COLLATE database_default
    UNIQUE(PlanID, TaskID, AssignmentID)
  )

  DECLARE @tabTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    Name Nvarchar(255) COLLATE database_default,
    WBS1 Nvarchar(30) COLLATE database_default,
    MA_StartDate datetime,
    MA_EndDate datetime,
    ChargeType varchar(1) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int,
    ChildrenCount int
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabSelectedETC TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    StartDate datetime,	
    EndDate datetime,
    PeriodHrs decimal(19,4),
    BillableHrs decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, OutlineNumber, StartDate, EndDate)
  )

	DECLARE @tabPLabTPD TABLE (
		RowID int identity,
    TimePhaseID varchar(32) COLLATE database_default,
    CIStartDate datetime, 
    CIEndDate datetime, 
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    StartDate datetime, 
    EndDate datetime, 
    PeriodHrs decimal(19,4), 
    BillableHrs decimal(19,4), 
		CINumWorkingDays int,
		ETC varchar(1)
    UNIQUE (CIStartDate, AssignmentID, RowID, PlanID, TaskID,OutlineNumber, StartDate, EndDate, ETC)
  )

DECLARE @tabLD TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ResourceID varchar(32) COLLATE database_default,
		PeriodHrs decimal(19,4),
		TransDate datetime
		UNIQUE (PlanID,OutlineNumber,ResourceID,TransDate)
)
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Set Company to non-Multi-Company.
  
  SELECT @strCompany = dbo.GetActiveCompany()

  -- Get RM Settings.
  
  SELECT 
		@dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE()))),
    @siHrDecimals = HrDecimals
    FROM CFGRMSettings

  -- Set Dates
  
  SELECT @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)

  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. For an Assignment row, @strRowID = 'E~<RPAssignment.ResourceID>|<RPTask.TaskID>
  --   2. For an Assignment row, @strRowID = 'G~<RPAssignment.GenericResourceID>|<RPTask.TaskID>'
  --   3. For a WBS row, @strRowID = '|<RPTask.TaskID>'

  SET @strResourceType = 
    CASE 
      WHEN CHARINDEX('~', @strRowID) = 0 
      THEN ''
      ELSE SUBSTRING(@strRowID, 1, 1)
    END

  SET @strIDPrefix = LEFT(@strRowID, CHARINDEX('|', @strRowID) - 1)

  IF (@strResourceType = 'E')
    BEGIN
      SET @strResourceID = REPLACE(@strIDPrefix, 'E~', '')
      SET @strGenericResourceID = NULL
    END
  ELSE IF (@strResourceType = 'G')
    BEGIN
      SET @strGenericResourceID = REPLACE(@strIDPrefix, 'G~', '')
      SET @strResourceID = NULL
    END
  ELSE
    BEGIN
      SET @strResourceID = NULL
      SET @strGenericResourceID = NULL
    END

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

  -- Setting other data.

  SELECT 
    @strPlanID = PT.PlanID,
    @bitIsLeafTask =
      CASE
        WHEN EXISTS(
          SELECT 'X' 
            FROM RPTask AS CT 
            WHERE 
              CT.PlanID = PT.PlanID 
            AND PT.OutlineNumber = CT.ParentOutlineNumber
        )
        THEN 0
        ELSE 1
      END
    FROM RPTask AS PT
      INNER JOIN RPTask AS TT ON PT.PlanID = TT.PlanID AND TT.OutlineLevel = 0
    WHERE PT.TaskID = @strTaskID

  -- Calculate calendar

	INSERT @tabCalendar(
		SeqID,
	  StartDate,
	  EndDate,
	  PeriodScale,
	  NumWorkingDays,
		ETC
	)	
		SELECT
			SeqID, 
		  StartDate,
		  EndDate,
		  Scale,
		  NumWorkingDays,
			'N' 
		  FROM dbo.ngRP$tabCalendarInterval(@strScopeStartDate, @strPeriodScale, @numPeriods)

  SELECT
    @dtScopeStartDate = MIN(StartDate),
    @dtScopeEndDate = MAX(EndDate),
		@dtMinEndDate = MIN(EndDate)
    FROM @tabCalendar

  -- Insert one special calendar row with StartDate = ETCDate 
	IF (@dtETCDate >= @dtScopeStartDate AND @dtETCDate <= @dtScopeEndDate)
    BEGIN

			INSERT @tabCalendar(
				SeqID,
				StartDate,
				EndDate,
				PeriodScale,
				NumWorkingDays,
				ETC
			)	
				SELECT
					SeqID, 
					@dtETCDate,
					EndDate,
					@strPeriodScale,
					dbo.DLTK$NumWorkingDays(@dtETCDate, EndDate, @strCompany),
					'Y'
				FROM @tabCalendar where @dtETCDate >= StartDate AND @dtETCDate <= EndDate
		END
	  
  SELECT @dtScopeBeforeDate = DATEADD(DAY, -1, @dtScopeStartDate)
  SELECT @dtScopeAfterDate = DATEADD(DAY, 1, @dtScopeEndDate)

	SELECT @intWorkingHoursPerDay = 8

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabAssignment(
    PlanID,
    TaskID,
    AssignmentID,
		ResourceID,
		GenericResourceID,
    StartDate,
    EndDate,
    OutlineNumber,
		ChargeType
  )
    SELECT DISTINCT
      A.PlanID,
      A.TaskID,
      A.AssignmentID,
			A.ResourceID,
			A.GenericResourceID,
      A.StartDate,
      A.EndDate,
      AT.OutlineNumber AS OutlineNumber,
      AT.ChargeType AS ChargeType
      FROM RPAssignment AS A
        INNER JOIN RPPlan AS P ON P.PlanID = A.PlanID
        INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
        INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID AND AT.OutlineNumber LIKE PT.OutlineNumber + '%'
        LEFT JOIN RPTask AS CT ON A.PlanID = CT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber AND AT.OutlineNumber LIKE CT.OutlineNumber + '%'
      WHERE
        A.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
				A.EndDate > @dtScopeStartDate


    SELECT @intTaskCount = COUNT(DISTINCT TaskID) FROM @tabAssignment

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collecting Labor Time-Phased Data to be used in subsequent calculations.
  -- The TPD records come from only the Assignments included in @tabAssignment.

  -- Calculate Selected ETC

  INSERT @tabSelectedETC(
    TimePhaseID,
    PlanID,
    TaskID,
    AssignmentID,
    OutlineNumber,
    StartDate,
    EndDate,
    PeriodHrs,
		BillableHrs
  )
    SELECT  /* Keep only TPD in the Selected ETC range */
      STPD.TimePhaseID AS TimePhaseID,
      STPD.PlanID AS PlanID,
      STPD.TaskID AS TaskID,
      STPD.AssignmentID AS AssignmentID,
      STPD.OutlineNumber AS OutlineNumber,
      STPD.StartDate AS StartDate,
      STPD.EndDate AS EndDate,
			STPD.PeriodHrs AS PeriodHrs,
			CASE WHEN STPD.BillableHrs = 0 THEN 0 ELSE STPD.PeriodHrs END AS BillableHrs
      FROM (
        SELECT /* Select only TPD in the ETC range */
          ETPD.TimePhaseID AS TimePhaseID,
          ETPD.PlanID AS PlanID,
          ETPD.TaskID AS TaskID,
          ETPD.AssignmentID AS AssignmentID,
          ETPD.OutlineNumber AS OutlineNumber,
          ETPD.StartDate AS StartDate,
          ETPD.EndDate AS EndDate,
					ETPD.PeriodHrs AS PeriodHrs,
					BillableHrs
          FROM (
            SELECT /* TPD for Assignment rows in @tabAssignment */
              TPD.TimePhaseID AS TimePhaseID,
              TPD.PlanID AS PlanID,
              TPD.TaskID AS TaskID,
              TPD.AssignmentID AS AssignmentID,
              A.OutlineNumber AS OutlineNumber,
              TPD.StartDate AS StartDate,
              TPD.EndDate AS EndDate,
              TPD.PeriodHrs AS PeriodHrs,
							CASE WHEN A.ChargeType = 'R' THEN TPD.PeriodHrs ELSE 0 END AS BillableHrs
              FROM @tabAssignment AS A
                INNER JOIN RPPlannedLabor AS TPD ON
                  A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID AND
                  TPD.PeriodHrs <> 0 AND TPD.AssignmentID IS NOT NULL 
          ) AS ETPD
          WHERE ETPD.PeriodHrs <> 0
      ) AS STPD

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Split up TPD in @tabSelectedETC to match with Calendar Intervals.

  INSERT @tabPLabTPD (
    TimePhaseID,
    CIStartDate,
		CIEndDate,
    PlanID, 
    TaskID,
    AssignmentID,
    OutlineNumber,
    StartDate, 
    EndDate, 
    PeriodHrs,
		BillableHrs,
		CINumWorkingDays,
		ETC
   )
     SELECT
      TimePhaseID AS TimePhaseID,
      CIStartDate AS CIStartDate,
      CIEndDate AS CIEndDate,
      PlanID AS PlanID, 
      TaskID AS TaskID,
      AssignmentID AS AssignmentID,
      OutlineNumber AS OutlineNumber,
      StartDate AS StartDate, 
      EndDate AS EndDate, 
      ROUND(ISNULL(PeriodHrs, 0), @siHrDecimals) AS PeriodHrs,
      ROUND(ISNULL(BillableHrs, 0), @siHrDecimals) AS BillableHrs,
			CINumWorkingDays,
			ETC
    FROM (
      SELECT -- For Assignment Rows.
        CI.StartDate AS CIStartDate, 
        CI.EndDate AS CIEndDate, 
        TPD.TimePhaseID AS TimePhaseID,
        TPD.PlanID AS PlanID, 
        TPD.TaskID AS TaskID,
        TPD.AssignmentID AS AssignmentID,
        TPD.OutlineNumber AS OutlineNumber,
        CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
        CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
        CASE 
          WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
          THEN TPD.PeriodHrs * 
            dbo.DLTK$ProrateRatio(
              CASE 
                WHEN TPD.StartDate > CI.StartDate 
                THEN TPD.StartDate 
                ELSE CI.StartDate 
              END, 
              CASE 
                WHEN TPD.EndDate < CI.EndDate 
                THEN TPD.EndDate 
                ELSE CI.EndDate 
              END, 
              TPD.StartDate, TPD.EndDate,
              @strCompany)             
          ELSE PeriodHrs 
        END AS PeriodHrs,
				CASE WHEN TPD.BillableHrs = 0 THEN 0 ELSE PeriodHrs END AS BillableHrs,
				CI.NumWorkingDays as CINumWorkingDays,
				CI.ETC AS ETC
        FROM @tabCalendar AS CI 
          INNER JOIN @tabSelectedETC AS TPD 
            ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
    ) AS X

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	-- Save Labor JTD & Unposted Labor JTD for this Resource into temp table.
  -- The JTD data are posted at the leaf node in the WBS Tree. 
  -- Therefore, we need to calculate JTD for the summary levels.

		INSERT @tabLD
			(PlanID,
			 OutlineNumber,
			 ResourceID,
			 TransDate,
			 PeriodHrs)      
			SELECT
				T.PlanID AS PlanID,
				T.OutlineNumber AS OutlineNumber,
				A.ResourceID AS ResourceID,
				TransDate,
				SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs
			FROM @tabAssignment AS A
				INNER JOIN RPTask AS T
					ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
				INNER JOIN LD
					ON LD.WBS1 = T.WBS1 
					 AND LD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
				     AND LD.WBS3 LIKE (ISNULL(T.WBS3 + '%', '%'))
					AND A.ResourceID = LD.Employee
			WHERE LD.TransDate <= @dtJTDDate
			GROUP BY T.PlanID, T.OutlineNumber, A.ResourceID, LD.TransDate

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

IF @strMode <> 'S'
	IF @strMode = 'C' /* Return data at the Assignment level */
		BEGIN
			INSERT @tabProjectViewPeriodReporting(
				SeqID,
				AssignmentID,
				TaskID,
				ResourceID,
				GenericResourceID,
				StartDate,
				PeriodHrs,
				JTDHrs,
				NumWorkingDays,
				ETC
			)
				SELECT 
					A.SeqID,
					A.AssignmentID AS AssignmentID, 
					A.TaskID as TaskID, 
					A.ResourceID,
					A.GenericResourceID,
					A.StartDate, 
					MAX(ISNULL(ETC.PeriodHrs,0)) AS PeriodHrs, 
					SUM(ISNULL(LD.PeriodHrs,0)) AS PeriodHrs, 
					SUM(NumWorkingDays), 
					A.ETC
				FROM 
					(SELECT 
						C.SeqID,
						A.TaskID,
						A.AssignmentID,
						A.ResourceID,
						A.GenericResourceID,
						A.OutlineNumber,
						C.StartDate, 
						C.EndDate, 
						A.StartDate AS AStart, 
						A.EndDate AS AEnd, 
						SUM(ISNULL(CASE
							WHEN (A.StartDate <= C.StartDate AND A.EndDate >= C.EndDate)
							THEN NumWorkingDays
							ELSE dbo.DLTK$NumWorkingDays(
								CASE 
									WHEN A.StartDate > C.StartDate 
									THEN A.StartDate 
									ELSE C.StartDate 
								END, 
								CASE 
									WHEN A.EndDate < C.EndDate 
									THEN A.EndDate 
									ELSE C.EndDate 
								END,
							@strCompany)
						END,0)) AS NumWorkingDays,  
						ETC
					FROM @tabCalendar AS C
						LEFT JOIN @tabAssignment AS A ON 1 = 1
					GROUP BY C.SeqID, A.TaskID, A.AssignmentID, A.OutlineNumber, A.ResourceID, A.GenericResourceID, C.StartDate, C.EndDate, A.StartDate, A.EndDate, C.ETC
					) AS A
				LEFT JOIN 
					(SELECT 
						SUM(ISNULL(ETC.PeriodHrs,0)) AS PeriodHrs,
						AssignmentID, 
						CIStartDate, 
						ETC
						FROM @tabPLabTPD AS ETC
						GROUP BY AssignmentID, CIStartDate, ETC
					) AS ETC ON ETC.CIStartDate = A.StartDate AND ETC.ETC = A.ETC AND ETC.AssignmentID = A.AssignmentID
				LEFT JOIN
					(SELECT
						SUM(ISNULL(LD.PeriodHrs,0)) AS PeriodHrs,
						TransDate,
						OutlineNumber,
						ResourceID  
					FROM @tabLD AS LD
					GROUP BY OutlineNumber, ResourceID, TransDate
					) AS LD ON LD.ResourceID = A.ResourceID AND A.OutlineNumber = LD.OutlineNumber AND A.StartDate <= LD.TransDate AND A.EndDate >= LD.TransDate AND A.ETC = 'N'
				GROUP BY A.SeqID, A.TaskID, A.AssignmentID,A.ResourceID, A.GenericResourceID, LD.ResourceID, A.StartDate, A.ETC
		END /* END Assignment Rows */
	ELSE	/*  @strMode = 'P'  */ /* Return data at the top-task, resource level */
		BEGIN
			INSERT @tabProjectViewPeriodReporting(
				SeqID,
				AssignmentID,
				TaskID,
				ResourceID,
				GenericResourceID,
				StartDate,
				PeriodHrs,
				JTDHrs,
				NumWorkingDays,
				ETC
			)
				SELECT 
					A.SeqID,
					null AS AssignmentID, 
					@strTaskID as TaskID, 
					A.ResourceID,
					A.GenericResourceID,
					A.StartDate, 
					SUM(ISNULL(ETC.PeriodHrs,0)) AS PeriodHrs, 
					SUM(ISNULL(LD.PeriodHrs,0)) AS PeriodHrs, 
					SUM(NumWorkingDays), 
					A.ETC
				FROM 
					(SELECT 
						C.SeqID, 
						A.ResourceID,
						A.GenericResourceID,
						C.StartDate, 
						C.EndDate, 
						SUM(ISNULL(CASE
							WHEN (A.StartDate <= C.StartDate AND A.EndDate >= C.EndDate)
							THEN NumWorkingDays
							ELSE dbo.DLTK$NumWorkingDays(
								CASE 
									WHEN A.StartDate > C.StartDate 
									THEN A.StartDate 
									ELSE C.StartDate 
								END, 
								CASE 
									WHEN A.EndDate < C.EndDate 
									THEN A.EndDate 
									ELSE C.EndDate 
								END,
							@strCompany)
						END,0)) AS NumWorkingDays,  
						ETC
					FROM @tabCalendar AS C
						LEFT JOIN @tabAssignment AS A ON 1 = 1
					GROUP BY C.SeqID, A.ResourceID, A.GenericResourceID, C.StartDate, C.EndDate, C.ETC
					) AS A
				LEFT JOIN 
					(SELECT 
						SUM(ISNULL(ETC.PeriodHrs,0)) AS PeriodHrs,
						A.ResourceID,
						A.GenericResourceID,
						CIStartDate, 
						ETC
						FROM @tabPLabTPD AS ETC
							INNER JOIN @tabAssignment A ON A.AssignmentID = ETC.AssignmentID
						GROUP BY ResourceID, GenericResourceID, CIStartDate, ETC
					) AS ETC ON ETC.CIStartDate = A.StartDate AND ETC.ETC = A.ETC AND ISNULL(ETC.ResourceID,'|') = ISNULL(A.ResourceID,'|') AND ISNULL(ETC.GenericResourceID,'|') = ISNULL(A.GenericResourceID,'|')
				LEFT JOIN
					(SELECT
						SUM(ISNULL(LD.PeriodHrs,0)) AS PeriodHrs,
						TransDate,
						ResourceID
					FROM @tabLD AS LD
					GROUP BY ResourceID, TransDate
					) AS LD ON LD.TransDate >= A.StartDate AND LD.TransDate <= A.EndDate AND A.ResourceID = LD.ResourceID AND A.ETC = 'N'
				GROUP BY A.SeqID, A.ResourceID, A.GenericResourceID, LD.ResourceID, A.StartDate, A.ETC
		END
ELSE /* Sum data at the project level */
		BEGIN
			INSERT @tabProjectViewPeriodReporting(
				SeqID,
				AssignmentID,
				TaskID,
				ResourceID,
				GenericResourceID,
				StartDate,
				PeriodHrs,
				JTDHrs,
				NumWorkingDays,
				ETC
			)
			SELECT 
				A.SeqID,
				null AS AssignmentID, 
				@strTaskID as TaskID, 
				null AS ResourceID,
				null AS GenericResourceID,
				A.StartDate, 
				MAX(ISNULL(ETC.PeriodHrs,0)) AS PeriodHrs, 
				SUM(ISNULL(LD.PeriodHrs,0)) AS JTDHrs,
				0 AS NumWorkingDays, 
				A.ETC
			FROM 
				(SELECT 
					C.SeqID, 
					C.StartDate, 
					C.EndDate, 
					ETC
				FROM @tabCalendar AS C
					LEFT JOIN @tabAssignment AS A ON 1 = 1
				GROUP BY C.SeqID, C.StartDate, C.EndDate, C.ETC
				) AS A
			LEFT JOIN 
				(SELECT 
					SUM(ISNULL(ETC.PeriodHrs,0)) AS PeriodHrs, 
					CIStartDate, 
					ETC
					FROM @tabPLabTPD AS ETC
					GROUP BY CIStartDate, ETC
				) AS ETC ON ETC.CIStartDate = A.StartDate AND ETC.ETC = A.ETC
			LEFT JOIN
				(SELECT
					SUM(ISNULL(LD.PeriodHrs,0)) AS PeriodHrs,
					TransDate
				FROM @tabLD AS LD
				GROUP BY TransDate
				) AS LD ON LD.TransDate >= A.StartDate AND LD.TransDate <= A.EndDate AND A.ETC = 'N'
			GROUP BY A.SeqID, A.StartDate, A.ETC
		END /* END IF (@strMode = 'S') */

RETURN
END
GO
