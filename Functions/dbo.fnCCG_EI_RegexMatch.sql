SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_RegexMatch]
(
	@pattern		VARCHAR(2000),
    @matchstring	VARCHAR(max)
)
RETURNS INT
AS BEGIN
	DECLARE @match BIT;
	SET @match = 1;
    RETURN @match;

END;
GO
