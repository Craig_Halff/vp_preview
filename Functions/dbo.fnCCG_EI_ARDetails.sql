SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_ARDetails] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS 
@T TABLE 
(	-- Invoice, Invoice Date, 0, 30, 60, 90, 120+ (6 values) 
	TopOrder	int,
	Descr		varchar(255),
	Value1		datetime,	 Value2		money,		 Value3		money,		 Value4		money,		 Value5		money,		 Value6		money,
	HTMLValue1	varchar(20), HTMLValue2	varchar(20), HTMLValue3 varchar(20), HTMLValue4 varchar(20), HTMLValue5 varchar(20), HTMLValue6 varchar(20),
	Align1		varchar(6),	 Align2		varchar(6),	 Align3		varchar(6),	 Align4		varchar(6),	 Align5		varchar(6),	 Align6		varchar(6) 
)
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select * from dbo.fnCCG_EI_ARDetails('2003005.xx',' ',' ')
	select * from dbo.fnCCG_EI_ARDetails('2000002.xx',' ',' ')
*/
	declare @today datetime, @MainWBS1 varchar(30)
    set @today=getdate()

	select @MainWBS1=MainWBS1 from BTBGSubs where SubWBS1=@WBS1
	if @MainWBS1 is not null 
		set @WBS1=@MainWBS1
		
	insert into @T (TopOrder, Descr, Value1, Value2, Value3, Value4, Value5, Value6, 
		HTMLValue1, HTMLValue2, HTMLValue3, HTMLValue4, HTMLValue5, HTMLValue6,
		Align1, Align2, Align3, Align4, Align5, Align6)
	select row_number() over(order by InvoiceDate desc, Invoice desc), Invoice, InvoiceDate,
		Sum(Case When					DaysOld < 31 Then InvoiceBalance Else 0 End) as Inv_0,
		Sum(Case When DaysOld >= 31 and DaysOld < 61 Then InvoiceBalance Else 0 End) as Inv_31_61,
		Sum(Case When DaysOld >= 61 and DaysOld < 91 Then InvoiceBalance Else 0 End) as Inv_61_91,
		Sum(Case When DaysOld >= 91 and DaysOld < 121 Then InvoiceBalance Else 0 End) as Inv_91_121,	
		Sum(Case When DaysOld >= 121 Then InvoiceBalance Else 0 End) as Inv_121,
		'','','','','','','left','right','right','right','right','right'
	from ( 
		select IsNull(LedgerAR.Invoice, '') As Invoice, AR.InvoiceDate, IsNull(PR.WBS1, '') As WBS1,
			Min(case when AR.InvoiceDate IS NULL then 0 else datediff(day,AR.InvoiceDate,GETDATE()) end) AS DaysOld, 
			Sum(case when LedgerAR.TransType='IN' then -Amount else case when LedgerAR.TransType='CR' and LedgerAR.SubType='T' then -Amount else Amount end end ) AS InvoiceBalance
		from LedgerAR LEFT JOIN BTBGSubs on LedgerAR.WBS1 = BTBGSubs.SubWBS1 
		left join PR as MAINPR on BTBGSubs.MainWBS1=MAINPR.WBS1 AND MAINPR.WBS2=' ' AND MAINPR.WBS3=' '
		, AR , PR 
		where
			LedgerAR.WBS1 = PR.WBS1 AND PR.WBS2 = /*N*/' ' AND PR.WBS3 = /*N*/' ' 
			 AND LedgerAR.WBS1 = AR.WBS1 AND LedgerAR.WBS2 = AR.WBS2 AND LedgerAR.WBS3 = AR.WBS3 AND LedgerAR.Invoice = AR.Invoice 
			AND LedgerAR.AutoEntry = 'N' 
			AND ((LedgerAR.TransType = /*N*/'IN' AND (LedgerAR.SubType <> 'X' OR LedgerAR.SubType Is Null)) 
			OR (LedgerAR.TransType = /*N*/'CR' AND LedgerAR.SubType IN ('R','T'))) 
			 AND ((PR.WBS1 = /*N*/@WBS1) OR (PR.WBS1 = BTBGSubs.subwbs1 AND (BTBGSubs.MAINWBS1 = /*N*/@WBS1) ))
		group by IsNull(LedgerAR.Invoice, ''), IsNull(PR.WBS1, ''), AR.InvoiceDate,
			--case when Min(AR.InvoiceDate) IS NULL then 0 else datediff(day,Min(AR.InvoiceDate),GETDATE()) end, 
			case when LedgerAR.TransType=/*N*/'CR' AND LedgerAR.SubType='R' then 'R' 
			 when LedgerAR.SubType='R' then 'E' 
			 when LedgerAR.TransType =/*N*/'CR' AND LedgerAR.SubType='T' then 'T' else ' ' end
	) ARAgingQuery
	group by Invoice, InvoiceDate
	delete from @T where IsNull(Value2,0)=0 and IsNull(Value3,0)=0 and IsNull(Value4,0)=0 and IsNull(Value5,0)=0 and IsNull(Value6,0)=0  
	
	-- Add grand total line:
	insert into @T (TopOrder, Descr, Value1, Value2, Value3, Value4, Value5, Value6,
		HTMLValue1, HTMLValue2, HTMLValue3, HTMLValue4, HTMLValue5, HTMLValue6,
		Align1, Align2, Align3, Align4, Align5, Align6)
	select 99 as TopOrder, '<b>Total</b>', null, sum(value2), sum(value3), sum(value4), sum(value5), sum(value6), 
		'','','','','','','left','right','right','right','right','right' from @T
	
	update @T set 
		HTMLValue1 = Case When Value1 is null Then '' Else Convert(varchar(10), Value1, 101) End,
		HTMLValue2 = Case When Value2 is null Then '&nbsp;' Else '$' + Convert(varchar,Convert(money, Value2),1) End,
		HTMLValue3 = Case When Value3 is null Then '&nbsp;' Else '$' + Convert(varchar,Convert(money, Value3),1) End,
		HTMLValue4 = Case When Value4 is null Then '&nbsp;' Else '$' + Convert(varchar,Convert(money, Value4),1) End,
		HTMLValue5 = Case When Value5 is null Then '&nbsp;' Else '$' + Convert(varchar,Convert(money, Value5),1) End,
		HTMLValue6 = Case When Value6 is null Then '&nbsp;' Else '$' + Convert(varchar,Convert(money, Value6),1) End
		
	update @T set HTMLValue2='<b>'+HTMLValue2+'</b>' where TopOrder in (99)
	update @T set HTMLValue3='<b>'+HTMLValue3+'</b>' where TopOrder in (99)
	update @T set HTMLValue4='<b>'+HTMLValue4+'</b>' where TopOrder in (99)
	update @T set HTMLValue5='<b>'+HTMLValue5+'</b>' where TopOrder in (99)
	update @T set HTMLValue6='<b>'+HTMLValue6+'</b>' where TopOrder in (99)
	return
END
GO
