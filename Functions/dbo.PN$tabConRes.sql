SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$tabConRes]
  (@strWBS1 Nvarchar(30),
   @strWBS2 Nvarchar(7),
   @strWBS3 Nvarchar(7),
   @strJTDDate VARCHAR(8)) -- Date must be in format: 'yyyymmdd' regardless of UI Culture.

  RETURNS @tabConRes TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    ConsultantID varchar(32) COLLATE database_default,
    WBS1 Nvarchar(30),
    WBS2 Nvarchar(7),
    WBS3 Nvarchar(7),
    Vendor Nvarchar(30) COLLATE database_default,
    VendorName Nvarchar(100) COLLATE database_default,
    Account Nvarchar(13) COLLATE database_default,
    AccountName Nvarchar(40) COLLATE database_default,
    AccountTypeCode smallint,
    AccountType Nvarchar(255) COLLATE database_default,
    ConBillRate decimal(19,4),
    StartDate datetime,
    EndDate datetime,
    PlannedConCost decimal(19,4),
    PlannedReimConCost decimal(19,4),
    PlannedConBill decimal(19,4),
    PlannedReimConBill decimal(19,4),
    BaselineConCost decimal(19,4),
    BaselineConBill decimal(19,4),
    JTDConCost decimal(19,4),
    JTDReimConCost decimal(19,4),
    JTDConBill decimal(19,4),
    JTDReimConBill decimal(19,4),
    ETCConCost decimal(19,4),
    ETCConBill decimal(19,4)
  )

BEGIN

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
  
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint
 
  DECLARE @strVorN char(1)
 
  -- Declare Temp tables.

  DECLARE @tabMappedTask TABLE
    (PlanID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(7) COLLATE database_default,
     WBS3 Nvarchar(7) COLLATE database_default,
     WBSType varchar(4) COLLATE database_default,
     ParentOutlineNumber varchar(255) COLLATE database_default,
     OutlineNumber varchar(255) COLLATE database_default,
     OutlineLevel int
     UNIQUE(PlanID, TaskID))

  DECLARE @tabConsultant TABLE
    (PlanID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     ConsultantID varchar(32) COLLATE database_default,
     WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(7) COLLATE database_default,
     WBS3 Nvarchar(7) COLLATE database_default,
     Account Nvarchar(13) COLLATE database_default,
     Vendor Nvarchar(30) COLLATE database_default,
     ConBillRate decimal(19,4),
     BaselineConCost decimal(19,4),
     BaselineConBill decimal(19,4)
     UNIQUE(PlanID, TaskID, ConsultantID, Account, Vendor, WBS1, WBS2, WBS3))

  DECLARE @tabRes TABLE
    (RowID  int IDENTITY(1,1),
     AccountName Nvarchar(40) COLLATE database_default,
     VendorName Nvarchar(100) COLLATE database_default,
     Account Nvarchar(13) COLLATE database_default,
     AccountTypeCode smallint,
     AccountType Nvarchar(255) COLLATE database_default,
     Vendor Nvarchar(30) COLLATE database_default
     UNIQUE(RowID, Account, Vendor))
        
  DECLARE @tabPCon TABLE
    (RowID  int IDENTITY(1,1),
     WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(7) COLLATE database_default,
     WBS3 Nvarchar(7) COLLATE database_default,
     Account Nvarchar(13) COLLATE database_default,
     Vendor Nvarchar(30) COLLATE database_default,
     StartDate datetime,
     EndDate datetime,
     PlannedConCost decimal(19,4),
     PlannedReimConCost decimal(19,4),
     PlannedConBill decimal(19,4),
     PlannedReimConBill decimal(19,4)
     UNIQUE(RowID, WBS1, WBS2, WBS3, Account, Vendor))
    
 	DECLARE @tabLedger TABLE 
    (RowID  int IDENTITY(1,1),
     WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(7) COLLATE database_default,
     WBS3 Nvarchar(7) COLLATE database_default,
     Account Nvarchar(13) COLLATE database_default,
     Vendor Nvarchar(30) COLLATE database_default,
     JTDConCost decimal(19,4),
     JTDReimConCost decimal(19,4),
     JTDConBill decimal(19,4),
     JTDReimConBill decimal(19,4)
     UNIQUE(RowID, WBS1, WBS2, WBS3, Account, Vendor))

  DECLARE @tabETC TABLE (
    RowID  int IDENTITY(1,1),
    Account Nvarchar(13) COLLATE database_default,
    Vendor Nvarchar(30) COLLATE database_default,
    ETCConCost decimal(19,4),
    ETCConBill decimal(19,4)
    UNIQUE(RowID, Account, Vendor)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = ReportAtBillingInBillingCurr,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat     

  -- Save Company string for use later.
  
  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          
  -- Set JTD/ETC Dates.
  
  SET @dtJTDDate = CONVERT(datetime, @strJTDDate)
  SET @dtETCDate = DATEADD(d, 1, @dtJTDDate) 

  -- Determine which set of tables need to be used.

  SET @strVorN = dbo.PN$PlanIs_VorN(@strWBS1)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	-- Save Consultant JTD & Unposted Consultant JTD for this Project into temp table.
  -- The JTD data are posted at the leaf node in the WBS Tree. 
  -- Therefore, we need to calculate JTD for the summary levels.

	INSERT @tabLedger(
    WBS1,
    WBS2,
    WBS3,
    Account,
    Vendor,
    JTDConCost,
    JTDReimConCost,
    JTDConBill,
    JTDReimConBill
  )      
		SELECT
      X.WBS1,
      X.WBS2,
      X.WBS3,
      X.Account,
      X.Vendor,
      SUM(ISNULL(JTDConCost, 0.0000)) AS JTDConCost,
      SUM(ISNULL(JTDReimConCost, 0.0000)) AS JTDReimConCost,
      SUM(ISNULL(JTDConBill, 0.0000)) AS JTDConBill,
      SUM(ISNULL(JTDReimConBill, 0.0000)) AS JTDReimConBill
      FROM (
        SELECT
          LG.WBS1,
          LG.WBS2,
          LG.WBS3,
          LG.Account,
          LG.Vendor,
          SUM(LG.AmountProjectCurrency) AS JTDConCost,  
          SUM(CASE WHEN CA.Type = 6 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimConCost,
          SUM(LG.BillExt) AS JTDConBill,
          SUM(CASE WHEN CA.Type = 6 THEN LG.BillExt ELSE 0 END) AS JTDReimConBill
          FROM LedgerAR AS LG
            INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
          WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (6, 8) AND
            (LG.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
            (LG.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
          GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
        UNION ALL SELECT
          LG.WBS1,
          LG.WBS2,
          LG.WBS3,
          LG.Account,
          LG.Vendor,
          SUM(LG.AmountProjectCurrency) AS JTDConCost,  
          SUM(CASE WHEN CA.Type = 6 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimConCost,
          SUM(LG.BillExt) AS JTDConBill,
          SUM(CASE WHEN CA.Type = 6 THEN LG.BillExt ELSE 0 END) AS JTDReimConBill
          FROM LedgerAP AS LG
            INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
          WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (6, 8) AND
            (LG.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
            (LG.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
          GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
        UNION ALL SELECT
          LG.WBS1,
          LG.WBS2,
          LG.WBS3,
          LG.Account,
          LG.Vendor,
          SUM(LG.AmountProjectCurrency) AS JTDConCost,  
          SUM(CASE WHEN CA.Type = 6 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimConCost,
          SUM(LG.BillExt) AS JTDConBill,
          SUM(CASE WHEN CA.Type = 6 THEN LG.BillExt ELSE 0 END) AS JTDReimConBill
          FROM LedgerEX AS LG
            INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
          WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (6, 8) AND
            (LG.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
            (LG.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
          GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
        UNION ALL SELECT
          LG.WBS1,
          LG.WBS2,
          LG.WBS3,
          LG.Account,
          LG.Vendor,
          SUM(LG.AmountProjectCurrency) AS JTDConCost,  
          SUM(CASE WHEN CA.Type = 6 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimConCost,
          SUM(LG.BillExt) AS JTDConBill,
          SUM(CASE WHEN CA.Type = 6 THEN LG.BillExt ELSE 0 END) AS JTDReimConBill
          FROM LedgerMISC AS LG
            INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
          WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (6, 8) AND
            (LG.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
            (LG.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
          GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
        UNION ALL SELECT
          POC.WBS1,
          POC.WBS2,
          POC.WBS3,
          POC.Account,
          POM.Vendor,
          SUM(AmountProjectCurrency) AS JTDConCost,  
          SUM(CASE WHEN CA.Type = 6 THEN AmountProjectCurrency ELSE 0 END) AS JTDReimConCost,
          SUM(BillExt) AS JTDConBill,
          SUM(CASE WHEN CA.Type = 6 THEN BillExt ELSE 0 END) AS JTDReimConBill
          FROM POCommitment AS POC 
            INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
            INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey
            INNER JOIN CA ON POC.Account = CA.Account
          WHERE POM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (6, 8) AND 
            (AmountProjectCurrency != 0 AND BillExt != 0) AND
            (POC.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
            (POC.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
          GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
        UNION ALL SELECT
          POC.WBS1,
          POC.WBS2,
          POC.WBS3,
          POC.Account,
          POM.Vendor,
          SUM(AmountProjectCurrency) AS JTDConCost,  
          SUM(CASE WHEN CA.Type = 6 THEN AmountProjectCurrency ELSE 0 END) AS JTDReimConCost,
          SUM(BillExt) AS JTDConBill,
          SUM(CASE WHEN CA.Type = 6 THEN BillExt ELSE 0 END) AS JTDReimConBill
          FROM POCommitment AS POC 
            INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
            INNER JOIN POCOMaster AS POCOM ON POD.COPKey = POCOM.PKey
            INNER JOIN POMaster AS POM ON POCOM.MasterPKey = POM.MasterPKey
            INNER JOIN CA ON POC.Account = CA.Account
          WHERE POCOM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (6, 8) AND 
            (AmountProjectCurrency != 0 AND BillExt != 0) AND
            (POC.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
            (POC.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
          GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
      ) AS X
      GROUP BY X.WBS1, X.WBS2, X.WBS3, X.Account, X.Vendor
      HAVING SUM(ISNULL(JTDConCost, 0.0000)) <> 0 OR SUM(ISNULL(JTDConBill, 0.0000)) <> 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Extract Consultant from Plans that are marked as "Included in Utilization".
  -- There could be multiple Plans with RPPlan.UlilizationIncludeFlag = 'Y'

  -- Calculate Resource Planned.
  -- Calculate Resource Baseline.
  -- It seems that having a SUM and GROUP BY in the following SQL would degrade performance substantially.

  IF (@strVorN = 'V')
    BEGIN

      INSERT @tabMappedTask
        (PlanID,
         TaskID,
         WBS1,
         WBS2,
         WBS3,
         WBSType,
         ParentOutlineNumber,
         OutlineNumber,
         OutlineLevel
        )
        SELECT
          P.PlanID,
          T.TaskID,
          T.WBS1,
          ISNULL(T.WBS2, ' '),
          ISNULL(T.WBS3, ' '),
          T.WBSType,
          T.ParentOutlineNumber,
          T.OutlineNumber,
          T.OutlineLevel
          FROM RPTask AS T
            INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND 
              T.WBS1 = @strWBS1 AND ISNULL(T.WBS2, ' ') = @strWBS2 AND ISNULL(T.WBS3, ' ') = @strWBS3

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabConsultant
        (PlanID,
         TaskID,
         ConsultantID,
         WBS1,
         WBS2,
         WBS3,
         Account,
         Vendor,
         ConBillRate,
         BaselineConCost,
         BaselineConBill
        )
        SELECT DISTINCT
          C.PlanID,
          C.TaskID,
          C.ConsultantID,
          C.WBS1,
          ISNULL(C.WBS2, ' '),
          ISNULL(C.WBS3, ' '),
          C.Account AS Account,
          C.Vendor AS Vendor,
          C.ConBillRate AS ConBillRate,
          C.BaselineConCost AS BaselineConCost,
          C.BaselineConBill AS BaselineConBill
          FROM RPConsultant AS C
            INNER JOIN RPTask AS T ON C.PlanID = T.PlanID AND C.TaskID = T.TaskID
            INNER JOIN @tabMappedTask AS MT ON C.PlanID = MT.PlanID AND T.OutlineNumber LIKE (MT.OutlineNumber + '%')

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabPCon (
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor,
        StartDate,
        EndDate,
        PlannedConCost,
        PlannedReimConCost,
        PlannedConBill,
        PlannedReimConBill
      )
        SELECT
          X.WBS1,
          X.WBS2,
          X.WBS3,
          X.Account AS Account,
          X.Vendor AS Vendor,
          X.StartDate AS StartDate,
          X.EndDate AS EndDate,
          X.PeriodCost AS PlannedConCost,
          X.PeriodReimCost AS PlannedReimConCost,
          X.PeriodBill AS PlannedConBill,
          X.PeriodReimBill AS PlannedReimConBill
          FROM (
            SELECT
              C.WBS1 AS WBS1,
              C.WBS2 AS WBS2,
              C.WBS3 AS WBS3,
              C.Account AS Account,
              C.Vendor AS Vendor,
              TPD.StartDate, 
              TPD.EndDate, 
              TPD.PeriodCost, 
              CASE WHEN CA.Type = 6 THEN TPD.PeriodCost ELSE 0 END AS PeriodReimCost,
              TPD.PeriodBill,
              CASE WHEN CA.Type = 6 THEN TPD.PeriodBill ELSE 0 END AS PeriodReimBill
              FROM @tabConsultant AS C
                INNER JOIN RPPlannedConsultant AS TPD ON C.PlanID = TPD.PlanID AND C.TaskID = TPD.TaskID AND C.ConsultantID = TPD.ConsultantID
                INNER JOIN CA ON C.Account = CA.Account
          ) AS X 

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabETC (
        Account,
        Vendor,
        ETCConCost,
        ETCConBill
      )
        SELECT
          Account AS Account,
          Vendor AS Vendor,
          ETCConCost AS ETCConCost,
          ETCConBill AS ETCConBill
          FROM (
            SELECT
              Account AS Account,
              Vendor AS Vendor,
              SUM(ETCConCost) AS ETCConCost,
              SUM(ETCConBill) AS ETCConBill
              FROM (
                SELECT
                  Account,
                  Vendor,
                  CASE WHEN StartDate >= @dtETCDate THEN PlannedConCost
	                     ELSE PlannedConCost * dbo.DLTK$ProrateRatio(@dtETCDate, EndDate, StartDate, EndDate, @strCompany) END AS ETCConCost,
                  CASE WHEN StartDate >= @dtETCDate THEN PlannedConBill
	                     ELSE PlannedConBill * dbo.DLTK$ProrateRatio(@dtETCDate, EndDate, StartDate, EndDate, @strCompany) END AS ETCConBill
                  FROM @tabPCon AS TPD
                  WHERE TPD.EndDate >= @dtETCDate
              ) AS XA
              GROUP BY Account, Vendor
          ) AS X
 
    END /* End If (@strVorN = 'V') */
  ELSE IF (@strVorN = 'N')
    BEGIN

      INSERT @tabMappedTask
        (PlanID,
         TaskID,
         WBS1,
         WBS2,
         WBS3,
         WBSType,
         ParentOutlineNumber,
         OutlineNumber,
         OutlineLevel
        )
        SELECT
          P.PlanID,
          T.TaskID,
          T.WBS1,
          ISNULL(T.WBS2, ' '),
          ISNULL(T.WBS3, ' '),
          T.WBSType,
          T.ParentOutlineNumber,
          T.OutlineNumber,
          T.OutlineLevel
          FROM PNTask AS T
            INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND 
              T.WBS1 = @strWBS1 AND ISNULL(T.WBS2, ' ') = @strWBS2 AND ISNULL(T.WBS3, ' ') = @strWBS3

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabConsultant
        (PlanID,
         TaskID,
         ConsultantID,
         WBS1,
         WBS2,
         WBS3,
         Account,
         Vendor,
         ConBillRate,
         BaselineConCost,
         BaselineConBill
        )
        SELECT DISTINCT
          C.PlanID,
          C.TaskID,
          C.ConsultantID,
          C.WBS1,
          ISNULL(C.WBS2, ' '),
          ISNULL(C.WBS3, ' '),
          C.Account AS Account,
          C.Vendor AS Vendor,
          C.ConBillRate AS ConBillRate,
          C.BaselineConCost AS BaselineConCost,
          C.BaselineConBill AS BaselineConBill
          FROM PNConsultant AS C
            INNER JOIN PNTask AS T ON C.PlanID = T.PlanID AND C.TaskID = T.TaskID
            INNER JOIN @tabMappedTask AS MT ON C.PlanID = MT.PlanID AND T.OutlineNumber LIKE (MT.OutlineNumber + '%')

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabPCon (
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor,
        StartDate,
        EndDate,
        PlannedConCost,
        PlannedReimConCost,
        PlannedConBill,
        PlannedReimConBill
      )
        SELECT
          X.WBS1,
          X.WBS2,
          X.WBS3,
          X.Account AS Account,
          X.Vendor AS Vendor,
          X.StartDate AS StartDate,
          X.EndDate AS EndDate,
          X.PeriodCost AS PlannedConCost,
          X.PeriodReimCost AS PlannedReimConCost,
          X.PeriodBill AS PlannedConBill,
          X.PeriodReimBill AS PlannedReimConBill
          FROM (
            SELECT
              C.WBS1 AS WBS1,
              C.WBS2 AS WBS2,
              C.WBS3 AS WBS3,
              C.Account AS Account,
              C.Vendor AS Vendor,
              TPD.StartDate, 
              TPD.EndDate, 
              TPD.PeriodCost, 
              CASE WHEN CA.Type = 6 THEN TPD.PeriodCost ELSE 0 END AS PeriodReimCost,
              TPD.PeriodBill,
              CASE WHEN CA.Type = 6 THEN TPD.PeriodBill ELSE 0 END AS PeriodReimBill
              FROM @tabConsultant AS C
                INNER JOIN PNPlannedConsultant AS TPD ON C.PlanID = TPD.PlanID AND C.TaskID = TPD.TaskID AND C.ConsultantID = TPD.ConsultantID 
                INNER JOIN CA ON C.Account = CA.Account
          ) AS X 

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabETC (
        Account,
        Vendor,
        ETCConCost,
        ETCConBill
      )
        SELECT
          Account AS Account,
          Vendor AS Vendor,
          SUM(ISNULL(ETCCost, 0.0000)) AS ETCConCost,
          SUM(ISNULL(ETCBill, 0.0000)) AS ETCConBill
          FROM (
            SELECT
              R.WBS1 AS WBS1,
              R.WBS2 AS WBS2,
              R.WBS3 AS WBS3,
              R.Account AS Account,
              R.Vendor AS Vendor,
              CASE
                WHEN ISNULL(TPD.PlannedCost, 0.0000) > ISNULL(JTD.JTDCost, 0.0000)
                THEN ISNULL(TPD.PlannedCost, 0.0000) - ISNULL(JTD.JTDCost, 0.0000)
                ELSE 0.0000
              END AS ETCCost,
              CASE
                WHEN ISNULL(TPD.PlannedBill, 0.0000) > ISNULL(JTD.JTDBill, 0.0000)
                THEN ISNULL(TPD.PlannedBill, 0.0000) - ISNULL(JTD.JTDBill, 0.0000)
                ELSE 0.0000
              END AS ETCBill

              FROM (
                SELECT DISTINCT
                  WBS1 AS WBS1,
                  ISNULL(WBS2, ' ') AS WBS2,
                  ISNULL(WBS3, ' ') AS WBS3,
                  Account AS Account,
                  Vendor AS Vendor
                  FROM @tabConsultant
              ) AS R

                LEFT JOIN (
                  SELECT
                    WBS1 AS WBS1,
                    ISNULL(WBS2, ' ') AS WBS2,
                    ISNULL(WBS3, ' ') AS WBS3,
                    Account AS Account,
                    Vendor AS Vendor,
                    SUM(PlannedConCost) AS PlannedCost,
                    SUM(PlannedConBill) AS PlannedBill
                    FROM @tabPCon
                    GROUP BY WBS1, WBS2, WBS3, Account, Vendor
                ) AS TPD
                  ON R.WBS1 = TPD.WBS1 AND R.WBS2 = TPD.WBS2 AND R.WBS3 = TPD.WBS3 AND R.Account = TPD.Account AND ISNULL(R.Vendor, '|') = ISNULL(TPD.Vendor, '|')

                LEFT JOIN (
                  SELECT
                    WTZ.WBS1 AS WBS1,
                    WTZ.WBS2 AS WBS2,
                    WTZ.WBS3 AS WBS3,
                    JZ.Account AS Account,
                    JZ.Vendor AS Vendor,
                    ISNULL(SUM(JZ.JTDCost), 0.0000) AS JTDCost,
                    ISNULL(SUM(JZ.JTDBill), 0.0000) AS JTDBill
                    FROM @tabConsultant AS WTZ
                      OUTER APPLY (
                        SELECT
                          WBS1 AS WBS1,
                          WBS2 AS WBS2,
                          WBS3 AS WBS3,
                          Account AS Account,
                          Vendor AS Vendor,
                          ISNULL(SUM(TPD.JTDConCost), 0.0000) AS JTDCost,
                          ISNULL(SUM(TPD.JTDConBill), 0.0000) AS JTDBill
                          FROM @tabLedger AS TPD
                          WHERE TPD.WBS1 = WTZ.WBS1 AND
                            (ISNULL(TPD.WBS2, '|') LIKE CASE WHEN WTZ.WBS2 = ' ' THEN '%' ELSE WTZ.WBS2 END) AND
                            (ISNULL(TPD.WBS3, '|') LIKE CASE WHEN WTZ.WBS3 = ' ' THEN '%' ELSE WTZ.WBS3 END) AND
                            ISNULL(WTZ.Account, '|') = ISNULL(TPD.Account, '|') AND ISNULL(WTZ.Vendor, '|') = ISNULL(TPD.Vendor, '|') 
                          GROUP BY TPD.WBS1, TPD.WBS2, TPD.WBS3, TPD.Account, TPD.Vendor
                      ) AS JZ
                    GROUP BY WTZ.WBS1, WTZ.WBS2, WTZ.WBS3, JZ.Account, JZ.Vendor
                ) AS JTD 
                  ON R.WBS1 = JTD.WBS1 AND R.WBS2 = JTD.WBS2 AND R.WBS3 = JTD.WBS3 AND R.Account = JTD.Account AND ISNULL(R.Vendor, '|') = ISNULL(JTD.Vendor, '|')

          ) AS ETC
            GROUP BY Account, Vendor

    END /* End If (@strVorN = 'N') */
 
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save PlanID and TaskID to be used later

  SELECT @strPlanID = MIN(PlanID), @strTaskID = MIN(TaskID) FROM @tabMappedTask

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save a list of Accounts and Vendors from Consultants for the input WBS1/WBS2/WBS3 combination.
  -- Also save a list of Accounts and Vendors from the Ledger tables.
  -- Need to do this step after populate @tabLedger because we don't want JTD rows with zero Cost and Bill amounts.

  INSERT @tabRes
    (AccountName,
     VendorName,
     Account,
     AccountTypeCode,
     AccountType,
     Vendor)
    SELECT DISTINCT
      X.AccountName,
      X.VendorName,
      X.Account,
      X.AccountTypeCode,
      X.AccountType,
      X.Vendor FROM
      (SELECT DISTINCT
         CA.Name AS AccountName,
         VE.Name AS VendorName,
         C.Account,
         CA.Type AS AccountTypeCode,
         CASE
           WHEN CA.Type = 6 THEN 'Reimb'
           WHEN CA.Type = 8 THEN 'Direct'
         END AS AccountType,
         C.Vendor
         FROM @tabConsultant AS C
           INNER JOIN CA ON C.Account = CA.Account
           LEFT JOIN VE ON C.Vendor = VE.Vendor
       UNION SELECT DISTINCT
         CA.Name AS AccountName,
         VE.Name AS VendorName,
         LG.Account,
         CA.Type AS AccountTypeCode,
         CASE
           WHEN CA.Type = 6 THEN 'Reimb'
           WHEN CA.Type = 8 THEN 'Direct'
         END AS AccountType,
         LG.Vendor
         FROM @tabLedger AS LG
           INNER JOIN CA ON LG.Account = CA.Account
           LEFT JOIN VE ON LG.Vendor = VE.Vendor
      ) AS X

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Combine data for Resource rows.

  INSERT @tabConRes
    (PlanID,
     TaskID,
     ConsultantID,
     WBS1,
     WBS2,
     WBS3,
     Vendor,
     VendorName,
     Account,
     AccountName,
     AccountTypeCode,
     AccountType,
     ConBillRate,
     StartDate,
     EndDate,
     PlannedConCost,
     PlannedReimConCost,
     PlannedConBill,
     PlannedReimConBill,
     BaselineConCost,
     BaselineConBill,
     JTDConCost,
     JTDReimConCost,
     JTDConBill,
     JTDReimConBill,
     ETCConCost,
     ETCConBill
    )
    SELECT
      ISNULL(C.PlanID, @strPlanID) AS PlanID,
      ISNULL(C.TaskID, @strTaskID) AS TaskID,
      C.ConsultantID,
      @strWBS1 AS WBS1,
      @strWBS2 AS WBS2,
      @strWBS3 AS WBS3,
      R.Vendor,
      R.VendorName,
      R.Account,
      R.AccountName,
      R.AccountTypeCode,
      R.AccountType,
      C.ConBillRate,
      PC.StartDate AS StartDate,
      PC.EndDate AS EndDate,
      ISNULL(PC.PlannedConCost, 0.0000) AS PlannedConCost,
      ISNULL(PC.PlannedReimConCost, 0.0000) AS PlannedReimConCost,
      ISNULL(PC.PlannedConBill, 0.0000) AS PlannedConBill,
      ISNULL(PC.PlannedReimConBill, 0.0000) AS PlannedReimConBill,
      ISNULL(C.BaselineConCost, 0.0000) AS BaselineConCost,
      ISNULL(C.BaselineConBill, 0.0000) AS BaselineConBill,
      ISNULL(JTD.JTDConCost, 0.0000) AS JTDConCost,
      ISNULL(JTD.JTDReimConCost, 0.0000) AS JTDReimConCost,
      ISNULL(JTD.JTDConBill, 0.0000) AS JTDConBill,
      ISNULL(JTD.JTDReimConBill, 0.0000) AS JTDReimConBill,

      ISNULL(ETC.ETCConCost, 0.0000) AS ETCConCost,
      ISNULL(ETC.ETCConBill, 0.0000) AS ETCConBill

      FROM @tabRes AS R

        LEFT JOIN (
          SELECT 
            MIN(PlanID) AS PlanID,
            MIN(TaskID) AS TaskID,
            MIN(ConsultantID) AS ConsultantID,
            Account, 
            Vendor,
            MIN(ConBillRate) AS ConBillRate,
            SUM(ISNULL(BaselineConCost, 0.0000)) AS BaselineConCost,
            SUM(ISNULL(BaselineConBill, 0.0000)) AS BaselineConBill
            FROM @tabConsultant 
            GROUP BY Account, Vendor
        ) AS C ON ISNULL(R.Account, '|') = ISNULL(C.Account, '|') AND ISNULL(R.Vendor, '|') = ISNULL(C.Vendor, '|')

        LEFT JOIN (
          SELECT 
            Account, 
            Vendor,
            MIN(PX.StartDate) AS StartDate,
            MAX(PX.EndDate) AS EndDate,
            SUM(ISNULL(PX.PlannedConCost, 0.0000)) AS PlannedConCost,
            SUM(ISNULL(PX.PlannedReimConCost, 0.0000)) AS PlannedReimConCost,
            SUM(ISNULL(PX.PlannedConBill, 0.0000)) AS PlannedConBill,
            SUM(ISNULL(PX.PlannedReimConBill, 0.0000)) AS PlannedReimConBill
            FROM @tabPCon AS PX
            GROUP BY Account, Vendor
        ) AS PC ON ISNULL(R.Account, '|') = ISNULL(PC.Account, '|') AND ISNULL(R.Vendor, '|') = ISNULL(PC.Vendor, '|')

        LEFT JOIN (
          SELECT
            Account, 
            Vendor,
            ISNULL(SUM(JX.JTDConCost), 0.0000) AS JTDConCost,
            ISNULL(SUM(JX.JTDConBill), 0.0000) AS JTDConBill,
            ISNULL(SUM(JX.JTDReimConCost), 0.0000) AS JTDReimConCost,
            ISNULL(SUM(JX.JTDReimConBill), 0.0000) AS JTDReimConBill
            FROM @tabLedger AS JX
            GROUP BY Account, Vendor
        ) AS JTD ON ISNULL(R.Account, '|') = ISNULL(JTD.Account, '|') AND ISNULL(R.Vendor, '|') = ISNULL(JTD.Vendor, '|')

        LEFT JOIN @tabETC AS ETC ON ISNULL(R.Account, '|') = ISNULL(ETC.Account, '|') AND ISNULL(R.Vendor, '|') = ISNULL(ETC.Vendor, '|')

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
