SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabGantt](
  @strRowID nvarchar(255) /* PNTask.TaskID of top most row */
)
  RETURNS @tabGantt TABLE (

    id varchar(32) COLLATE database_default, /* PNTask.TaskID */
    text nvarchar(255) COLLATE database_default, /* PNTask.Name */
    start_date datetime, /* PNTask.StartDate */
    PlanEndDate datetime, /* PNTask.EndDate */
    parent varchar(32) COLLATE database_default, /* PNTask.TaskID of parent row */
    HasChildren bit, /* Flag to indicate whether the row has WBS rows undeneath */
    OutlineNumber varchar(255) COLLATE database_default /* PNTask.OutlineNumber */

  )
 
 BEGIN

  DECLARE @strTaskID varchar(32)
  DECLARE @strPlanID varchar(32)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Parsing for TaskID.

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

  -- Determine PlanID.

  SELECT 
    @strPlanID = PT.PlanID
    FROM PNTask AS PT 
    WHERE PT.TaskID = @strTaskID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabGantt(
    id,
    text, 
    start_date,
    PlanEndDate,
    parent,
    HasChildren,
    OutlineNumber
  )
    SELECT
      T.TaskID AS id,
      T.Name AS text,
      T.StartDate start_date,
      T.EndDate AS PlanEndDate,
      CASE
        WHEN T.ParentOutlineNumber IS NOT NULL
        THEN PT.TaskID
        ELSE NULL
      END AS parent,
      CASE WHEN T.ChildrenCount > 0 THEN CONVERT(bit, 1) ELSE CONVERT(bit, 0) END AS HasChildren,
      T.OutlineNumber AS OutlineNumber
      FROM PNTask AS T
        LEFT JOIN PNTask AS PT ON T.PlanID = PT.PlanID AND PT.OutlineNumber = T.ParentOutlineNumber 
      WHERE T.PlanID = @strPlanID
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
