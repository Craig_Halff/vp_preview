SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetVisionAuditSource]() RETURNS nvarchar(3) AS 
BEGIN 
-- Leave NVARCHAR alone since converting binary and need to know position for parsing and nvarchar is double byte
  RETURN CONVERT(nvarchar(3),(SELECT SESSION_CONTEXT(N'AuditSource')))
END
GO
