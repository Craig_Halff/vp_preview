SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PM$tabGR]()
  RETURNS @tabGR TABLE
   (Code Nvarchar(20),
    Name Nvarchar(50),
    Category smallint,
    LaborCode Nvarchar(14))
BEGIN

  /*-----------------------------------------------------------------------------------------------------------------------------
    This function is used in the Import process of Plan from Microsoft Project.
    This function would be called from the lookup process while importing an Assignment row.
    I had to use a UDF because I need to return either Category or Labor Code depending on Generic Method in CFGResourcePlanning.
  */-----------------------------------------------------------------------------------------------------------------------------
  
  DECLARE @strActiveCompany Nvarchar(14)
  DECLARE @sintGRMethod smallint
  
  -- Save Active Company string for use later.
  
  SET @strActiveCompany = dbo.GetActiveCompany()

  -- Get flags to determine which Generic Method is being used.
  
  SELECT
     @sintGRMethod = GRMethod
     FROM CFGResourcePlanning
     WHERE CFGResourcePlanning.Company = @strActiveCompany

  INSERT @tabGR
    SELECT 
      Code,
      Name,
      CASE WHEN @sintGRMethod = 0 THEN Category ELSE 0 END AS Category, -- Labor Category
      CASE WHEN @sintGRMethod = 1 THEN LaborCode ELSE NULL END AS LaborCode -- Labor Code
      FROM GR 
      
  RETURN

END -- fn_PM$tabGR
GO
