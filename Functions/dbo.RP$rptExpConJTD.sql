SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$rptExpConJTD]
  (@strPlanID varchar(32),
   @dtETCDate datetime,
   @strTaskID varchar(32)=NULL,
   @strCommitmentFlg varchar(1) = 'N', 
   @strMatchWBS1Wildcard varchar(1) = 'N',
   @strExcludeUNFlg varchar(1) = 'N',
   @strType varchar(1) = 'E',
   @ReportAtBillingInBillingCurr varchar(1) = 'N')
  RETURNS @tabJTDExpCon TABLE
   (PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    RowID varchar(32) COLLATE database_default,
    TransDate datetime,
    JTDCostCurrencyCode Nvarchar(3) COLLATE database_default,
    JTDBillCurrencyCode Nvarchar(3) COLLATE database_default,     
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4),
    PostedFlg smallint)
BEGIN

  /*--------------------------------------------------------------------------------------------------------------------
This function does the same thing as UD function RP$tabJTDExpCon.  Only difference
is that this function has an optional taskID parameter.  Also, @dtETCDate is the ETC date user enters.
  */--------------------------------------------------------------------------------------------------------------------

  DECLARE @tabExpCon TABLE
    (PlanID varchar(32) COLLATE database_default,     
     TaskID varchar(32) COLLATE database_default,
     RowID varchar(32) COLLATE database_default,
     WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(30) COLLATE database_default,
     WBS3 Nvarchar(30) COLLATE database_default,
     Account Nvarchar(13) COLLATE database_default,
     Vendor Nvarchar(30) COLLATE database_default)
     
  IF (@strType = 'E')
    INSERT @tabExpCon
      SELECT PlanID,TaskID,ExpenseID AS RowID, WBS1, WBS2, WBS3, Account, Vendor
        FROM RPExpense WHERE PlanID = @strPlanID AND TaskID LIKE (ISNULL(@strTaskID,'%'))
  ELSE
    INSERT @tabExpCon
      SELECT PlanID,TaskID, ConsultantID AS RowID, WBS1, WBS2, WBS3, Account, Vendor
        FROM RPConsultant WHERE PlanID = @strPlanID AND TaskID LIKE (ISNULL(@strTaskID,'%'))
  
  -- Insert JTD into returned table
  
  INSERT @tabJTDExpCon
    SELECT 
      PlanID,
      TaskID,  
      RowID,  
      TransDate, 
	  Max(JTDCostCurrencyCode) as JTDCostCurrencyCode,
      Max(JTDBillCurrencyCode) as JTDBillCurrencyCode, 
      SUM(PeriodCost) AS PeriodCost,  
      SUM(PeriodBill) AS PeriodBill,  
      1 AS PostedFlg
      FROM
        (SELECT 
           EC.PlanID AS PlanID,  
           EC.TaskID AS TaskID,  
           EC.RowID AS RowID,  
           TransDate AS TransDate,
	       Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
           Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,  
           SUM(AmountProjectCurrency) AS PeriodCost,  
           SUM(BillExt) AS PeriodBill  
           FROM LedgerAR AS Ledger
             INNER JOIN PR on PR.WBS1=Ledger.WBS1 and PR.WBS2='' and PR.WBS3=''
             INNER JOIN @tabExpCon AS EC
               ON (Ledger.WBS1 = EC.WBS1
                   AND Ledger.Account = EC.Account AND Ledger.ProjectCost = 'Y')  
             INNER JOIN (SELECT MIN(RowID) AS RowID FROM @tabExpCon  
               GROUP BY TaskID, Account, Vendor) AS X1 ON EC.RowID = X1.RowID  
           WHERE TransDate <= @dtETCDate
             AND Ledger.TransType != (CASE WHEN @strExcludeUNFlg = 'Y' THEN 'UN' ELSE '*' END)
             AND Ledger.WBS2 LIKE (ISNULL(EC.WBS2, '%'))  
             AND Ledger.WBS3 LIKE (ISNULL(EC.WBS3, '%'))  
             AND (ISNULL(Ledger.Vendor, '%')) LIKE (ISNULL(EC.Vendor, '%'))  
           GROUP BY EC.PlanID,EC.TaskID, TransDate, EC.RowID, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end
         UNION ALL
         SELECT 
           EC.PlanID AS PlanID,  
           EC.TaskID AS TaskID,  
           EC.RowID AS RowID,  
           TransDate AS TransDate,  
	       Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
           Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,  
           SUM(AmountProjectCurrency) AS PeriodCost,  
           SUM(BillExt) AS PeriodBill  
           FROM LedgerAP AS Ledger
             INNER JOIN PR on PR.WBS1=Ledger.WBS1 and PR.WBS2='' and PR.WBS3=''
             INNER JOIN @tabExpCon AS EC
               ON (Ledger.WBS1 = EC.WBS1
                   AND Ledger.Account = EC.Account AND Ledger.ProjectCost = 'Y')  
             INNER JOIN (SELECT MIN(RowID) AS RowID FROM @tabExpCon
               GROUP BY TaskID, Account, Vendor) AS X1 ON EC.RowID = X1.RowID  
           WHERE TransDate <= @dtETCDate
             AND Ledger.TransType != (CASE WHEN @strExcludeUNFlg = 'Y' THEN 'UN' ELSE '*' END)
             AND Ledger.WBS2 LIKE (ISNULL(EC.WBS2, '%'))  
             AND Ledger.WBS3 LIKE (ISNULL(EC.WBS3, '%'))  
             AND (ISNULL(Ledger.Vendor, '%')) LIKE (ISNULL(EC.Vendor, '%'))  
           GROUP BY EC.PlanID,EC.TaskID, TransDate, EC.RowID, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end
         UNION ALL
         SELECT 
           EC.PlanID AS PlanID,  
           EC.TaskID AS TaskID,  
           EC.RowID AS RowID,  
           TransDate AS TransDate,  
	       Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
           Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,  
           SUM(AmountProjectCurrency) AS PeriodCost,  
           SUM(BillExt) AS PeriodBill  
           FROM LedgerEX AS Ledger
             INNER JOIN PR on PR.WBS1=Ledger.WBS1 and PR.WBS2='' and PR.WBS3=''
             INNER JOIN @tabExpCon AS EC
               ON (Ledger.WBS1 = EC.WBS1
                   AND Ledger.Account = EC.Account AND Ledger.ProjectCost = 'Y')  
             INNER JOIN (SELECT MIN(RowID) AS RowID FROM @tabExpCon  
               GROUP BY TaskID, Account, Vendor) AS X1 ON EC.RowID = X1.RowID  
           WHERE TransDate <= @dtETCDate
             AND Ledger.TransType != (CASE WHEN @strExcludeUNFlg = 'Y' THEN 'UN' ELSE '*' END)
             AND Ledger.WBS2 LIKE (ISNULL(EC.WBS2, '%'))  
             AND Ledger.WBS3 LIKE (ISNULL(EC.WBS3, '%'))  
             AND (ISNULL(Ledger.Vendor, '%')) LIKE (ISNULL(EC.Vendor, '%'))  
           GROUP BY EC.PlanID,EC.TaskID, TransDate, EC.RowID, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end
         UNION ALL
         SELECT 
           EC.PlanID AS PlanID,  
           EC.TaskID AS TaskID, 
           EC.RowID AS RowID,   
           TransDate AS TransDate,  
	       Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
           Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,  
           SUM(AmountProjectCurrency) AS PeriodCost,  
           SUM(BillExt) AS PeriodBill  
           FROM LedgerMISC AS Ledger
             INNER JOIN PR on PR.WBS1=Ledger.WBS1 and PR.WBS2='' and PR.WBS3=''
             INNER JOIN @tabExpCon AS EC
               ON (Ledger.WBS1 = EC.WBS1
                   AND Ledger.Account = EC.Account AND Ledger.ProjectCost = 'Y')  
             INNER JOIN (SELECT MIN(RowID) AS RowID FROM @tabExpCon
               GROUP BY TaskID, Account, Vendor) AS X1 ON EC.RowID = X1.RowID  
           WHERE TransDate <= @dtETCDate
             AND Ledger.TransType != (CASE WHEN @strExcludeUNFlg = 'Y' THEN 'UN' ELSE '*' END)
             AND Ledger.WBS2 LIKE (ISNULL(EC.WBS2, '%'))  
             AND Ledger.WBS3 LIKE (ISNULL(EC.WBS3, '%'))  
             AND (ISNULL(Ledger.Vendor, '%')) LIKE (ISNULL(EC.Vendor, '%'))  
           GROUP BY EC.PlanID,EC.TaskID, TransDate, EC.RowID, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end
           ) AS Z
    GROUP BY PlanID,RowID, TaskID, TransDate, JTDCostCurrencyCode, JTDBillCurrencyCode
    
  --> PO Commitments.
  
  IF (@strCommitmentFlg = 'Y')
    BEGIN

      -- POC with no Change Orders.

      INSERT @tabJTDExpCon
        SELECT 
          EC.PlanID AS PlanID,  
          EC.TaskID AS TaskID, 
          EC.RowID AS RowID,   
          POM.OrderDate AS TransDate,  
	      Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
          Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,  
          SUM(AmountProjectCurrency) AS PeriodCost,  
          SUM(BillExt) AS PeriodBill,  
          -1 AS PostedFlg
          FROM POCommitment AS POC 
            INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
            INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey
            INNER JOIN PR on PR.WBS1=POC.WBS1 and PR.WBS2='' and PR.WBS3=''
            INNER JOIN @tabExpCon AS EC
              ON (POC.WBS1 = EC.WBS1
                  AND POC.Account = EC.Account)  
            INNER JOIN (SELECT MIN(RowID) AS RowID FROM @tabExpCon 
              GROUP BY TaskID, Account, Vendor) AS X1 ON EC.RowID = X1.RowID  
          WHERE POM.OrderDate <= @dtETCDate
            AND AmountProjectCurrency != 0 AND BillExt != 0
            AND POC.WBS2 LIKE (ISNULL(EC.WBS2, '%'))  
            AND POC.WBS3 LIKE (ISNULL(EC.WBS3, '%'))  
            AND POM.Vendor LIKE (ISNULL(EC.Vendor, '%'))  
          GROUP BY EC.PlanID,EC.TaskID, POM.OrderDate, EC.RowID, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end
            
      -- POC with Change Orders.

      INSERT @tabJTDExpCon
        SELECT 
          EC.PlanID AS PlanID,  
          EC.TaskID AS TaskID, 
          EC.RowID AS RowID,   
          POCOM.OrderDate AS TransDate,  
          Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
          Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,  
          SUM(AmountProjectCurrency) AS PeriodCost,  
          SUM(BillExt) AS PeriodBill,  
          -1 AS PostedFlg
          FROM POCommitment AS POC 
            INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
            INNER JOIN POCOMaster AS POCOM ON POD.COPKey = POCOM.PKey
            INNER JOIN POMaster AS POM ON POCOM.MasterPKey = POM.MasterPKey
            INNER JOIN PR on PR.WBS1=POC.WBS1 and PR.WBS2='' and PR.WBS3=''
            INNER JOIN @tabExpCon AS EC
              ON (POC.WBS1 = EC.WBS1
                  AND POC.Account = EC.Account)  
            INNER JOIN (SELECT MIN(RowID) AS RowID FROM @tabExpCon 
              GROUP BY TaskID, Account, Vendor) AS X1 ON EC.RowID = X1.RowID  
          WHERE POCOM.OrderDate <= @dtETCDate
            AND AmountProjectCurrency != 0 AND BillExt != 0
            AND POC.WBS2 LIKE (ISNULL(EC.WBS2, '%'))  
            AND POC.WBS3 LIKE (ISNULL(EC.WBS3, '%'))  
            AND POM.Vendor LIKE (ISNULL(EC.Vendor, '%'))  
          GROUP BY EC.PlanID,EC.TaskID, POCOM.OrderDate, EC.RowID, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end

    END -- If-Then
       
  RETURN

END -- fn_RP$rptExpConJTD
GO
