SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create FUNCTION [dbo].[fnCCG_PAT_JTDInvoicedFromVendor_Details] (@PayableSeq int, @WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS @T TABLE
(
	TopOrder	int,
	Descr		varchar(255),	-- Vendor
--	Date					 Invoice				 Voucher					Amount
	Value1		datetime,	 Value2		varchar(20), Value3		varchar(20),	Value4		money,
	HTMLValue1	varchar(20), HTMLValue2	varchar(20), HTMLValue3 varchar(20),	HTMLValue4 varchar(20),
	Align1		varchar(6),	 Align2		varchar(6),	 Align3		varchar(6),		Align4		varchar(6)
)
AS BEGIN
/*
	Copyright (c) 2017 EleVia Software.  All rights reserved.

	select * from dbo.fnCCG_PAT_JTDInvoicedFromVendor_Details(1,'','','')
	select * from dbo.fnCCG_PAT_JTDInvoicedFromVendor_Details(4216,'2003005.00',' ',' ')
	select * from dbo.fnCCG_PAT_JTDInvoicedFromVendor_Details(5265,'2003005.00','1PD',' ')
*/
	set @WBS1 = nullif(@WBS1,'')
	set @WBS2 = nullif(@WBS2,'')
	set @WBS3 = nullif(@WBS3,'')
	
	insert into @T (TopOrder, Descr, Value1, Value2, Value3, Value4,
		HTMLValue1, HTMLValue2, HTMLValue3, HTMLValue4,
		Align1, Align2, Align3, Align4)
	select 1, VendorName, TransDate, InvoiceNumber, Voucher, Sum(Amount),
		'','','','','center','right','right','right'
	from (
		-- Records from PAT not yet in Vision:
		select VE.Name as VendorName, PayableDate as TransDate, pat.PayableNumber as InvoiceNumber, Voucher as Voucher, IsNull(pa.NetAmount,0) as Amount
            from CCG_PAT_Payable pat
            inner join CCG_PAT_ProjectAmount pa on pat.Seq = pa.PayableSeq
            inner join (
				select distinct Vendor,pat.Seq,WBS1 
				from CCG_PAT_Payable pat 
				inner join CCG_PAT_ProjectAmount pa on pat.Seq = pa.PayableSeq 
				where pat.Seq = @PayableSeq and (@WBS1 is null or WBS1 = @WBS1)
				) thispa on pat.Vendor = thispa.Vendor and pa.WBS1 = thispa.WBS1
            inner join VE on VE.Vendor=pat.Vendor
            where 
              PayableType = 'I' and ISNULL(pat.Voucher,'') = '' and (@WBS1 is null or (pa.WBS1 = @WBS1 and pa.WBS2 = @WBS2 and pa.WBS3 = @WBS3 ))
		UNION ALL
		-- Unposted records in Vision
		select VE.Name as VendorName, ap.TransDate, ap.Invoice as InvoiceNumber, ap.Voucher as Voucher,  apd.NetAmount
		from CCG_PAT_Payable pat
		inner join (select distinct PayableSeq,WBS1/*,WBS2,WBS3*/ from CCG_PAT_ProjectAmount ) pa on pat.Seq = pa.PayableSeq
		inner join apMaster ap on pat.Vendor=ap.Vendor --and pat.Voucher=ap.Voucher
		inner join apDetail apd on apd.Batch=ap.Batch and apd.MasterPKey=ap.MasterPKey
		left join VE on VE.Vendor=ap.Vendor
		where (pat.Seq = @PayableSeq /*or pat.ParentSeq = @PayableSeq*/)	-- Match on single invoice or all invoices for a contract
		  and (@WBS1 IS NULL or (apd.WBS1=@WBS1 and apd.WBS2=@WBS2 and apd.WBS3=@WBS3))
		  and ap.Posted='N'
		UNION ALL
		--Records from Vision
		select VE.Name as VendorName, ap.TransDate, VO.Invoice as InvoiceNumber, ap.Voucher, ap.amount
		from CCG_PAT_Payable pat
		left join (select distinct PayableSeq,WBS1/*,WBS2,WBS3*/ from CCG_PAT_ProjectAmount ) pa on pat.Seq = pa.PayableSeq
		left join LedgerAP ap on pat.Vendor=ap.Vendor and pa.WBS1=ap.WBS1 /*and (pa.WBS2=ap.WBS2 or pa.WBS2='')*/
		left join VE on VE.Vendor=ap.Vendor
		--left join VendorCustomTabFields vctf on vctf.Vendor = ap.Vendor
		left join VO on VO.Vendor=ap.Vendor and VO.Voucher=ap.Voucher
		where (pat.Seq = @PayableSeq /*or pat.ParentSeq = @PayableSeq*/)	-- Match on single invoice or all invoices for a contract
		  and (@WBS1 IS NULL or (ap.WBS1=@WBS1 and ap.WBS2=@WBS2  and ap.WBS3=@WBS3 ))
		  and TransType = 'AP'  and PostSeq  <> '0'
	) as yaya group by VendorName, TransDate, Voucher, InvoiceNumber order by 1,2,3,4
	delete from @T where Value4=0
	
	-- Add grand total line:
	insert into @T (TopOrder, Descr, Value1, Value2, Value3, Value4,
		HTMLValue1, HTMLValue2, HTMLValue3, HTMLValue4,
		Align1, Align2, Align3, Align4)
	select 99 as TopOrder, '<b>Total</b>', null, null, null, sum(Value4), 
		'','','','','left','right','right','right' from @T
	
	update @T set 
		HTMLValue1 = Case When Value1 is null Then '&nbsp;' Else Convert(varchar(10), Value1, 101) End,
		HTMLValue2 = Case When Value2 is null Then '&nbsp;' Else Value2 End,
		HTMLValue3 = Case When Value3 is null Then '&nbsp;' Else Value3 End,
		HTMLValue4 = Case When Value4 is null Then '&nbsp;' Else '$' + Convert(varchar,Convert(money, Value4),1) End
	update @T set HTMLValue4='<b>'+HTMLValue4+'</b>' where TopOrder in (99)
	return
END

GO
