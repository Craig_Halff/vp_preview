SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DW$tabTask]()
  RETURNS @tabTask TABLE
    (PlanID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     ParentTaskID varchar(32) COLLATE database_default,
     WBS1 nvarchar(30) COLLATE database_default,
     WBS2 nvarchar(30) COLLATE database_default,
     WBS3 nvarchar(30) COLLATE database_default,
     LaborCode nvarchar(14) COLLATE database_default,
     Employee nvarchar(20) COLLATE database_default,
     LaborCategory smallint,
     Account nvarchar(13) COLLATE database_default,
     Vendor nvarchar(20) COLLATE database_default,
     Unit nvarchar(30) COLLATE database_default,
     UnitTable nvarchar(30) COLLATE database_default,
     TaskName nvarchar(255) COLLATE database_default,
     OutlineNumber varchar(255) COLLATE database_default,
     OutlineLevel int,
     WBSType varchar(4) COLLATE database_default,
     WBSTypeDesc nvarchar(50) COLLATE database_default,
     PlannedLaborHrs decimal(19, 4),
     PlannedLabCost decimal(19, 4),
     PlannedLabBill decimal(19, 4),
     PlannedExpCost decimal(19, 4),
     PlannedExpBill decimal(19, 4),
     PlannedDirExpCost decimal(19, 4),
     PlannedDirExpBill decimal(19, 4),
     PlannedConCost decimal(19, 4),
     PlannedConBill decimal(19, 4),
     PlannedDirConCost decimal(19, 4),
     PlannedDirConBill decimal(19, 4),
     PlannedUntQty decimal(19, 4),
     PlannedUntCost decimal(19, 4),
     PlannedUntBill decimal(19, 4),
     PlannedDirUntCost decimal(19, 4),
     PlannedDirUntBill decimal(19, 4),
     BaselineLaborHrs decimal(19, 4),
     BaselineLabCost decimal(19, 4),
     BaselineLabBill decimal(19, 4),
     BaselineExpCost decimal(19, 4),
     BaselineExpBill decimal(19, 4),
     BaselineDirExpCost decimal(19, 4),
     BaselineDirExpBill decimal(19, 4),
     BaselineConCost decimal(19, 4),
     BaselineConBill decimal(19, 4),
     BaselineDirConCost decimal(19, 4),
     BaselineDirConBill decimal(19, 4),
     BaselineUntQty decimal(19, 4),
     BaselineUntCost decimal(19, 4),
     BaselineUntBill decimal(19, 4),
     BaselineDirUntCost decimal(19, 4),
     BaselineDirUntBill decimal(19, 4),
     WeightedLabCost decimal(19, 4),
     WeightedLabBill decimal(19, 4),
     WeightedExpCost decimal(19, 4),
     WeightedExpBill decimal(19, 4),
     WeightedConCost decimal(19, 4),
     WeightedConBill decimal(19, 4),
     WeightedUntCost decimal(19, 4),
     WeightedUntBill decimal(19, 4)	 
    )
BEGIN -- Function DW$tabTask

  DECLARE @WBS1Label AS nvarchar(50)
  DECLARE @WBS2Label AS nvarchar(50)
  DECLARE @WBS3Label AS nvarchar(50)
  DECLARE @LBCDLabel AS nvarchar(50)
  
  SELECT @WBS1Label = CONVERT(NVARCHAR(50), LabelValue) FROM FW_CFGLabels WHERE LabelName = 'wbs1Label'
  SELECT @WBS2Label = CONVERT(NVARCHAR(50), LabelValue) FROM FW_CFGLabels WHERE LabelName = 'wbs2Label'
  SELECT @WBS3Label = CONVERT(NVARCHAR(50), LabelValue) FROM FW_CFGLabels WHERE LabelName = 'wbs3Label'
  SELECT @LBCDLabel = CONVERT(NVARCHAR(50), LabelValue) FROM FW_CFGLabels WHERE LabelName = 'labcdLabel'

  DECLARE @tabLedgerWBS2
    TABLE (WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(30) COLLATE database_default
           PRIMARY KEY(WBS1, WBS2))
           
  DECLARE @tabLedgerWBS3
    TABLE (WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(30) COLLATE database_default,
           WBS3 Nvarchar(30) COLLATE database_default
           PRIMARY KEY(WBS1, WBS2, WBS3))

  DECLARE @tabLedgerLBCD
    TABLE (WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(30) COLLATE database_default,
           WBS3 Nvarchar(30) COLLATE database_default,
           LaborCode Nvarchar(14) COLLATE database_default
           PRIMARY KEY(WBS1, WBS2, WBS3, LaborCode))

  DECLARE @tabLedgerEM
    TABLE (WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(30) COLLATE database_default,
           WBS3 Nvarchar(30) COLLATE database_default,
           LaborCode Nvarchar(14) COLLATE database_default,
           Employee Nvarchar(20) COLLATE database_default
           UNIQUE(WBS1, WBS2, WBS3, LaborCode, Employee))

  DECLARE @tabWBS1
    TABLE (PlanID varchar(32) COLLATE database_default,
           TaskID varchar(32) COLLATE database_default,
           OutlineNumber varchar(255) COLLATE database_default,
           OutlineLevel int,
           WBS1 Nvarchar(30) COLLATE database_default,
		   Operation varchar(1)
           PRIMARY KEY(PlanID, OutlineNumber, WBS1))

  DECLARE @tabMP_WBS2
    TABLE (RowID int IDENTITY,
           PlanID varchar(32) COLLATE database_default,
           TaskID varchar(32) COLLATE database_default,
           OutlineNumber varchar(255) COLLATE database_default,
           OutlineLevel int,
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(30) COLLATE database_default,
		   Operation varchar(30)
           PRIMARY KEY(RowID, PlanID, TaskID, OutlineNumber, WBS1, WBS2))
           
  DECLARE @tabMP_WBS3
    TABLE (RowID int IDENTITY,
           PlanID varchar(32) COLLATE database_default,
           TaskID varchar(32) COLLATE database_default,
           OutlineNumber varchar(255) COLLATE database_default,
           OutlineLevel int,
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(30) COLLATE database_default,
           WBS3 Nvarchar(30) COLLATE database_default,
		   Operation varchar(1)
           PRIMARY KEY(RowID, PlanID, TaskID, OutlineNumber, WBS1))

  DECLARE @tabUM_WBS2
    TABLE (PlanID varchar(32) COLLATE database_default,
           TaskID varchar(32) COLLATE database_default,
           ParentTaskID varchar(32) COLLATE database_default,
           ParentOutlineNumber varchar(255) COLLATE database_default,
           ParentOutlineLevel int,
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(30) COLLATE database_default,
           RowID int,
		   Operation varchar(1)
           PRIMARY KEY(PlanID, ParentTaskID, ParentOutlineNumber, WBS1, WBS2, RowID))
           
  DECLARE @tabUM_WBS3
    TABLE (PlanID varchar(32) COLLATE database_default,
           TaskID varchar(32) COLLATE database_default,
           ParentTaskID varchar(32) COLLATE database_default,
           ParentOutlineNumber varchar(255) COLLATE database_default,
           ParentOutlineLevel int,          
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(30) COLLATE database_default,
           WBS3 Nvarchar(30) COLLATE database_default,
           RowID int,
		   Operation varchar(1)
           PRIMARY KEY(PlanID, ParentTaskID, ParentOutlineNumber, WBS1, RowID))

  DECLARE @tabUM_LBCD
    TABLE (PlanID varchar(32) COLLATE database_default,
           TaskID varchar(32) COLLATE database_default,
           ParentTaskID varchar(32) COLLATE database_default,
           ParentOutlineNumber varchar(255) COLLATE database_default,
           ParentOutlineLevel int,
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(30) COLLATE database_default,
           WBS3 Nvarchar(30) COLLATE database_default,
           LaborCode Nvarchar(14) COLLATE database_default,
           RowID int,
		   Operation varchar(1)
           PRIMARY KEY(PlanID, ParentTaskID, ParentOutlineNumber, WBS1, RowID))
		              
  DECLARE @tabWBSTree
    TABLE (RowID int IDENTITY,
           PlanID varchar(32) COLLATE database_default,
           TaskID varchar(32) COLLATE database_default,
           ParentTaskID varchar(32) COLLATE database_default,
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(30) COLLATE database_default,
           WBS3 Nvarchar(30) COLLATE database_default,
           LaborCode Nvarchar(14) COLLATE database_default,
           OutlineNumber varchar(255) COLLATE database_default,
           OutlineLevel int,
           WBSType varchar(4) COLLATE database_default,
		   Operation varchar(1)
           PRIMARY KEY(RowID, PlanID, TaskID, WBS1, WBS2, WBS3, LaborCode, OutlineNumber, WBSType))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find all WBS1 that are in both Ledger tables and in RPTask.

  INSERT @tabWBS1
    (PlanID,
     TaskID,
     OutlineNumber,
     OutlineLevel,
     WBS1)
    SELECT DISTINCT
      T.PlanID,
      T.TaskID,
      ISNULL(T.OutlineNumber,''),
      T.OutlineLevel,
      T.WBS1
      FROM 
        (SELECT DISTINCT WBS1 FROM LD
         UNION
         SELECT DISTINCT WBS1 FROM LedgerAR
         UNION
         SELECT DISTINCT WBS1 FROM LedgerAP
         UNION
         SELECT DISTINCT WBS1 FROM LedgerEX
         UNION
         SELECT DISTINCT WBS1 FROM LedgerMISC) AS LD 
        INNER JOIN RPTask AS T ON LD.WBS1 = T.WBS1 AND T.WBSType = 'WBS1' AND T.WBS1 != '<none>'		

  INSERT @tabLedgerWBS2
    (WBS1,
     WBS2)	
    SELECT DISTINCT LD.WBS1, LD.WBS2
      FROM 
        (SELECT DISTINCT WBS1, WBS2 FROM LD
         UNION
         SELECT DISTINCT WBS1, WBS2 FROM LedgerAR
         UNION
         SELECT DISTINCT WBS1, WBS2 FROM LedgerAP
         UNION
         SELECT DISTINCT WBS1, WBS2 FROM LedgerEX
         UNION
         SELECT DISTINCT WBS1, WBS2 FROM LedgerMISC) AS LD
        INNER JOIN RPTask AS T ON LD.WBS1 = T.WBS1 AND T.WBSType = 'WBS1' AND T.WBS1 != '<none>'		
      WHERE LD.WBS2 IS NOT NULL AND LD.WBS2 != ' '
 
  INSERT @tabLedgerWBS3
    (WBS1,
     WBS2,
     WBS3)	
    SELECT DISTINCT LD.WBS1, LD.WBS2, LD.WBS3
      FROM 
        (SELECT DISTINCT WBS1, WBS2, WBS3 FROM LD
         UNION
         SELECT DISTINCT WBS1, WBS2, WBS3 FROM LedgerAR
         UNION
         SELECT DISTINCT WBS1, WBS2, WBS3 FROM LedgerAP
         UNION
         SELECT DISTINCT WBS1, WBS2, WBS3 FROM LedgerEX
         UNION
         SELECT DISTINCT WBS1, WBS2, WBS3 FROM LedgerMISC) AS LD
        INNER JOIN RPTask AS T ON LD.WBS1 = T.WBS1 AND T.WBSType = 'WBS1' AND T.WBS1 != '<none>'		
      WHERE LD.WBS2 IS NOT NULL AND LD.WBS2 != ' ' AND LD.WBS3 IS NOT NULL AND LD.WBS3 != ' '
       
  INSERT @tabLedgerLBCD
    (WBS1,
     WBS2,
     WBS3,
     LaborCode)	
    SELECT DISTINCT
      LD.WBS1 AS WBS1, 
      LD.WBS2 AS WBS2, -- WBS2 could be ' '
      LD.WBS3 AS WBS3, -- WBS3 could be ' '
      LD.LaborCode AS LaborCode 
      FROM LD
        INNER JOIN RPTask AS T ON LD.WBS1 = T.WBS1 AND T.WBSType = 'WBS1' AND T.WBS1 != '<none>'		
        WHERE LD.LaborCode IS NOT NULL

  INSERT @tabLedgerEM
    (WBS1,
     WBS2,
     WBS3,
     LaborCode,
     Employee)	
    SELECT DISTINCT
      LD.WBS1 AS WBS1, 
      LD.WBS2 AS WBS2, -- WBS2 could be ' '
      LD.WBS3 AS WBS3, -- WBS3 could be ' '
      LD.LaborCode AS LaborCode,
      LD.Employee AS Employee
      FROM LD
        
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       
  -- Find WBS2 that are in RPTask and the Ledger tables --> Mapped WBS2.
  -- This will be used later to figure out the Unmapped WBS3 and LaborCode.
  
  INSERT @tabMP_WBS2
    (PlanID,
     TaskID,
     OutlineNumber,
     OutlineLevel,
     WBS1,
     WBS2)
    SELECT DISTINCT
      T.PlanID,
      T.TaskID,
      T.OutlineNumber,
      T.OutlineLevel,
      T.WBS1,
      T.WBS2
      FROM RPTask AS T
        INNER JOIN @tabLedgerWBS2 AS LD 
          ON T.WBS1 = LD.WBS1 AND T.WBS2 = LD.WBS2 AND T.WBSType = 'WBS2' AND T.WBS1 != '<none>' AND T.WBS2 != '<none>'		  
        
  -- Find WBS3 that are in RPTask and the Ledger tables --> Mapped WBS3.
  -- This will be used later to figure out the Unmapped LaborCode.
  
  INSERT @tabMP_WBS3
    (PlanID,
     TaskID,
     OutlineNumber,
     OutlineLevel,
     WBS1,
     WBS2,
     WBS3)
    SELECT DISTINCT
      T.PlanID,
      T.TaskID,
      T.OutlineNumber,
      T.OutlineLevel,
      T.WBS1,
      T.WBS2,
      T.WBS3
      FROM RPTask AS T	  
        INNER JOIN @tabLedgerWBS3 AS LD 
          ON T.WBS1 = LD.WBS1 AND T.WBS2 = LD.WBS2 AND T.WBS3 = LD.WBS3 
            AND T.WBSType = 'WBS3' AND T.WBS1 != '<none>' AND T.WBS2 != '<none>' AND T.WBS3 != '<none>'

           
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  /*
  
  The general algorithm to find Unmapped entities is:
  
  1. Prepare a list of parent task rows under which the unmapped entities need to be hung.
  
  2. Prepare a list of entities in the ledger table.
  
  3. Compare the list from #2 against the mapped entities. Identify the unmapped entities.
  
  */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find WBS2 that are in the Ledger tables but are not in RPTask --> Unmapped WBS2.
  
  INSERT @tabUM_WBS2
    (PlanID,
     TaskID,
     ParentTaskID,
     ParentOutlineNumber,
     ParentOutlineLevel,
     WBS1,
     WBS2,
     RowID)
    SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
      PlanID AS PlanID,
      (SELECT REPLACE(CAST(NEWID AS VARCHAR(36)), '-', '') FROM dbo.NewID) AS TaskID, -- Compute TaskID for the new row here so that the id can be propagated downward.
      TaskID AS ParentTaskID, -- This is the TaskID of the WBS1 row.
      OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the WBS1 row.
      OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the WBS1 row.
      WBS1 AS WBS1,
      WBS2 AS WBS2,
      ROW_NUMBER() OVER (PARTITION BY PlanID, OutlineNumber, WBS1 ORDER BY PlanID, OutlineNumber) AS RowID	  
      FROM
        (SELECT DISTINCT
           T1.PlanID,
           T1.TaskID,
           T1.OutlineNumber,
           T1.OutlineLevel,
           T1.WBS1,
           LD2.WBS2
           FROM @tabLedgerWBS2 AS LD2
             INNER JOIN @tabWBS1 AS T1 ON LD2.WBS1 = T1.WBS1
             LEFT JOIN RPTask AS T2 ON T1.PlanID = T2.PlanID AND LD2.WBS1 = T2.WBS1 AND LD2.WBS2 = T2.WBS2 			 
               AND T2.WBSType = 'WBS2' AND T2.WBS2 != '<none>'
           WHERE T2.WBS1 IS NULL) as TZ       
 
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find WBS3 that are in the Ledger tables but are not in RPTask --> Unmapped WBS3.
  
  INSERT @tabUM_WBS3 -->>> Unmapped WBS3 for Mapped WBS2.
    (PlanID,
     TaskID,
     ParentTaskID,
     ParentOutlineNumber,
     ParentOutlineLevel,
     WBS1,
     WBS2,
     WBS3,
     RowID)
    SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
      PlanID AS PlanID,
      (SELECT REPLACE(CAST(NEWID AS VARCHAR(36)), '-', '') FROM dbo.NewID) AS TaskID, -- Compute TaskID for the new row here so that the id can be propagated downward.
      TaskID AS ParentTaskID, -- This is the TaskID of the WBS2 row.
      OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the WBS2 row.
      OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the WBS2 row.
      WBS1 AS WBS1,
      WBS2 AS WBS2,
      WBS3 AS WBS3,
      ROW_NUMBER() OVER (PARTITION BY PlanID, OutlineNumber, WBS1, WBS2 ORDER BY PlanID, OutlineNumber) AS RowID
      FROM
        (SELECT
           TM.PlanID AS PlanID,
           TM.TaskID AS TaskID,
           TM.OutlineNumber AS OutlineNumber,
           TM.OutlineLevel AS OutlineLevel,
           TM.WBS1 AS WBS1,
           TM.WBS2 AS WBS2,
           LD3.WBS3 AS WBS3
           FROM @tabLedgerWBS3 AS LD3 
             INNER JOIN @tabMP_WBS2 AS TM ON LD3.WBS1 = TM.WBS1 AND LD3.WBS2 = TM.WBS2
         EXCEPT
         SELECT
           TM.PlanID AS PlanID,
           TM.TaskID AS TaskID,
           TM.OutlineNumber AS OutlineNumber,
           TM.OutlineLevel AS OutlineLevel,
           TM.WBS1 AS WBS1,
           TM.WBS2 AS WBS2,
           T3.WBS3 AS WBS3		   
           FROM RPTask AS T3 		   
             INNER JOIN @tabMP_WBS2 AS TM ON TM.PlanID = T3.PlanID AND T3.ParentOutlineNumber = TM.OutlineNumber AND TM.WBS1 = T3.WBS1 AND TM.WBS2 = T3.WBS2 AND
               T3.WBSType = 'WBS3' AND T3.WBS2 != '<none>' AND T3.WBS3 != '<none>') AS TZ
             
  INSERT @tabUM_WBS3 -->>> Unmapped WBS3 for Unmapped WBS2.
    (PlanID,
     TaskID,
     ParentTaskID,
     ParentOutlineNumber,
     ParentOutlineLevel,
     WBS1,
     WBS2,
     WBS3,
     RowID)
    SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
      PlanID AS PlanID,
      (SELECT REPLACE(CAST(NEWID AS VARCHAR(36)), '-', '') FROM dbo.NewID) AS TaskID, -- Compute TaskID for the new row here so that the id can be propagated downward.
      TaskID AS ParentTaskID, -- This is the TaskID of the WBS2 row.
      OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the WBS2 row.
      OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the WBS2 row.
      WBS1 AS WBS1,
      WBS2 AS WBS2,
      WBS3 AS WBS3,
      ROW_NUMBER() OVER (PARTITION BY PlanID, OutlineNumber, WBS1, WBS2 ORDER BY PlanID, OutlineNumber) AS RowID
      FROM
        (SELECT DISTINCT -- Get the Unmapped WBS2 rows.
           TU.PlanID AS PlanID,
           TU.TaskID AS TaskID, -- This is the TaskID of the Unmapped WBS2 row.
           TU.ParentOutlineNumber + '.X' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
           TU.ParentOutlineLevel + 1 AS OutlineLevel,
           TU.WBS1 AS WBS1,
           TU.WBS2 AS WBS2,
           LDX.WBS3 AS WBS3		   
           FROM @tabUM_WBS2 AS TU
             INNER JOIN @tabLedgerWBS3 AS LDX ON LDX.WBS1 = TU.WBS1 AND LDX.WBS2 = TU.WBS2) AS TZ

--LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
--L Unmapped Labor Code.                                                                                                                                           L
--LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL

  --LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
  --L LBCD1. Unmapped Labor Code hung under WBS1 rows.                                                                                                             L
  --LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
  
  INSERT @tabUM_LBCD -->>> LBCD1. Unmapped Labor Code hung under WBS1 rows. 
    (PlanID,
     TaskID,
     ParentTaskID,
     ParentOutlineNumber,
     ParentOutlineLevel,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     RowID)
    SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
      PlanID AS PlanID,
      (SELECT REPLACE(CAST(NEWID AS VARCHAR(36)), '-', '') FROM dbo.NewID) AS TaskID, -- Compute TaskID for the new row here so that the id can be propagated downward.
      TaskID AS ParentTaskID, -- This is the TaskID of the WBS1 row.
      OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the WBS1 row.
      OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the WBS1 row.
      WBS1 AS WBS1,
      WBS2 AS WBS2,
      WBS3 AS WBS3,
      LaborCode AS LaborCode,
      ROW_NUMBER() OVER (PARTITION BY PlanID, OutlineNumber, WBS1 ORDER BY PlanID, OutlineNumber) AS RowID
      FROM
        (SELECT DISTINCT
           T1.PlanID AS PlanID,
           T1.TaskID AS TaskID,
           T1.OutlineNumber AS OutlineNumber,
           T1.OutlineLevel AS OutlineLevel,
           T1.WBS1 AS WBS1,
           NULL AS WBS2,
           NULL AS WBS3,
           LD4.LaborCode AS LaborCode
           FROM @tabLedgerLBCD AS LD4
             INNER JOIN @tabWBS1 AS T1 ON LD4.WBS1 = T1.WBS1
             WHERE LD4.WBS2 = ' ' AND LD4.WBS3 = ' '
         EXCEPT
         SELECT
           T1.PlanID AS PlanID,
           T1.TaskID AS TaskID,
           T1.OutlineNumber AS OutlineNumber,
           T1.OutlineLevel AS OutlineLevel,
           T1.WBS1 AS WBS1,
           NULL AS WBS2,
           NULL AS WBS3,
           T4.LaborCode AS LaborCode
			FROM RPTask AS T4		   
             INNER JOIN @tabWBS1 AS T1 ON T1.PlanID = T4.PlanID AND T4.ParentOutlineNumber = T1.OutlineNumber AND T1.WBS1 = T4.WBS1 AND
               T4.WBSType = 'LBCD' AND T4.WBS2 IS NULL AND T4.WBS3 IS NULL AND T4.LaborCode != '<none>') AS TZ

  --LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
  --L LBCD2.1. Unmapped Labor Code hung under Mapped WBS2 rows.                                                                                                    L
  --LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
           
  INSERT @tabUM_LBCD -->>> LBCD2.1. Unmapped Labor Code hung under Mapped WBS2 rows.
    (PlanID,
     TaskID,
     ParentTaskID,
     ParentOutlineNumber,
     ParentOutlineLevel,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     RowID)
    SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
      PlanID AS PlanID,
      (SELECT REPLACE(CAST(NEWID AS VARCHAR(36)), '-', '') FROM dbo.NewID) AS TaskID, -- Compute TaskID for the new row here so that the id can be propagated downward.
      TaskID AS ParentTaskID, -- This is the TaskID of the WBS2 row.
      OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the WBS2 row.
      OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the WBS2 row.
      WBS1 AS WBS1,
      WBS2 AS WBS2,
      WBS3 AS WBS3,
      LaborCode AS LaborCode,
      ROW_NUMBER() OVER (PARTITION BY PlanID, OutlineNumber, WBS1, WBS2 ORDER BY PlanID, OutlineNumber) AS RowID
      FROM
        (SELECT
           TM.PlanID AS PlanID,
           TM.TaskID AS TaskID,
           TM.OutlineNumber AS OutlineNumber,
           TM.OutlineLevel AS OutlineLevel,
           TM.WBS1 AS WBS1,
           TM.WBS2 AS WBS2,
           NULL AS WBS3,
           LD4.LaborCode AS LaborCode
           FROM @tabLedgerLBCD AS LD4
             INNER JOIN @tabMP_WBS2 AS TM ON LD4.WBS1 = TM.WBS1 AND LD4.WBS2 = TM.WBS2
             WHERE LD4.WBS2 != ' ' AND LD4.WBS3 = ' '
         EXCEPT
         SELECT
           TM.PlanID AS PlanID,
           TM.TaskID AS TaskID,
           TM.OutlineNumber AS OutlineNumber,
           TM.OutlineLevel AS OutlineLevel,
           TM.WBS1 AS WBS1,
           TM.WBS2 AS WBS2,
           NULL AS WBS3,
           T4.LaborCode AS LaborCode
           FROM RPTask AS T4		   
             INNER JOIN @tabMP_WBS2 AS TM ON TM.PlanID = T4.PlanID AND T4.ParentOutlineNumber = TM.OutlineNumber AND TM.WBS1 = T4.WBS1 AND TM.WBS2 = T4.WBS2 AND
               T4.WBSType = 'LBCD' AND T4.WBS2 != '<none>' AND T4.WBS3 IS NULL AND T4.LaborCode != '<none>') AS TZ
      
  --LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
  --L LBCD2.2. Unmapped Labor Code hung under Unmapped WBS2 rows.                                                                                                  L
  --LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL

  INSERT @tabUM_LBCD -->>> LBCD2.2. Unmapped Labor Code hung under Unmapped WBS2 rows.
    (PlanID,
     TaskID,
     ParentTaskID,
     ParentOutlineNumber,
     ParentOutlineLevel,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     RowID)
    SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
      PlanID AS PlanID,
      (SELECT REPLACE(CAST(NEWID AS VARCHAR(36)), '-', '') FROM dbo.NewID) AS TaskID, -- Compute TaskID for the new row here so that the id can be propagated downward.
      TaskID AS ParentTaskID, -- This is the TaskID of the WBS2 row.
      OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the WBS2 row.
      OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the WBS2 row.
      WBS1 AS WBS1,
      WBS2 AS WBS2,
      WBS3 AS WBS3,
      LaborCode AS LaborCode,
      ROW_NUMBER() OVER (PARTITION BY PlanID, OutlineNumber, WBS1, WBS2 ORDER BY PlanID, OutlineNumber) AS RowID
      FROM
        (SELECT DISTINCT -- Get the Unmapped WBS2 rows.
           TU.PlanID AS PlanID,
           TU.TaskID AS TaskID, -- This is the TaskID of the Unmapped WBS2 row.
           TU.ParentOutlineNumber + '.X' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
           TU.ParentOutlineLevel + 1 AS OutlineLevel,
           TU.WBS1 AS WBS1,
           TU.WBS2 AS WBS2,
           NULL AS WBS3,
           LD4.LaborCode AS LaborCode
           FROM @tabUM_WBS2 AS TU
             INNER JOIN @tabLedgerLBCD AS LD4 ON LD4.WBS1 = TU.WBS1 AND LD4.WBS2 = TU.WBS2
             WHERE LD4.WBS2 != ' ' AND LD4.WBS3 = ' '
        ) AS TZ      

  --LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
  --L LBCD3.1. Unmapped Labor Code hung under Mapped WBS3 rows.                                                                                                    L
  --LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
           
  INSERT @tabUM_LBCD -->>> LBCD3.1. Unmapped Labor Code hung under Mapped WBS3 rows.
    (PlanID,
     TaskID,
     ParentTaskID,
     ParentOutlineNumber,
     ParentOutlineLevel,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     RowID)
    SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
      PlanID AS PlanID,
      (SELECT REPLACE(CAST(NEWID AS VARCHAR(36)), '-', '') FROM dbo.NewID) AS TaskID, -- Compute TaskID for the new row here so that the id can be propagated downward.
      TaskID AS ParentTaskID, -- This is the TaskID of the WBS3 row.
      OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the WBS3 row.
      OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the WBS3 row.
      WBS1 AS WBS1,
      WBS2 AS WBS2,
      WBS3 AS WBS3,
      LaborCode AS LaborCode,
      ROW_NUMBER() OVER (PARTITION BY PlanID, OutlineNumber, WBS1, WBS2 ORDER BY PlanID, OutlineNumber) AS RowID
      FROM
        (SELECT
           TM.PlanID AS PlanID,
           TM.TaskID AS TaskID,
           TM.OutlineNumber AS OutlineNumber,
           TM.OutlineLevel AS OutlineLevel,
           TM.WBS1 AS WBS1,
           TM.WBS2 AS WBS2,
           TM.WBS3 AS WBS3,
           LD4.LaborCode AS LaborCode
           FROM @tabLedgerLBCD AS LD4
             INNER JOIN @tabMP_WBS3 AS TM ON LD4.WBS1 = TM.WBS1 AND LD4.WBS2 = TM.WBS2 AND LD4.WBS3 = TM.WBS3
             WHERE LD4.WBS2 != ' ' AND LD4.WBS3 != ' '
         EXCEPT
         SELECT
           TM.PlanID AS PlanID,
           TM.TaskID AS TaskID,
           TM.OutlineNumber AS OutlineNumber,
           TM.OutlineLevel AS OutlineLevel,
           TM.WBS1 AS WBS1,
           TM.WBS2 AS WBS2,
           TM.WBS3 AS WBS3,
           T4.LaborCode AS LaborCode
           FROM RPTask AS T4		   
             INNER JOIN @tabMP_WBS3 AS TM ON TM.PlanID = T4.PlanID AND T4.ParentOutlineNumber = TM.OutlineNumber AND 
               TM.WBS1 = T4.WBS1 AND TM.WBS2 = T4.WBS2 AND TM.WBS3 = T4.WBS3 AND
               T4.WBSType = 'LBCD' AND T4.WBS2 != '<none>' AND T4.WBS3 != '<none>' AND T4.LaborCode != '<none>') AS TZ

  --LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
  --L LBCD3.2. Unmapped Labor Code hung under Unmapped WBS3 rows.                                                                                                  L
  --LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL

  INSERT @tabUM_LBCD -->>> LBCD3.2. Unmapped Labor Code hung under Unmapped WBS3 rows.
    (PlanID,
     TaskID,
     ParentTaskID,
     ParentOutlineNumber,
     ParentOutlineLevel,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     RowID,
	 Operation)
    SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
      PlanID AS PlanID,
      (SELECT REPLACE(CAST(NEWID AS VARCHAR(36)), '-', '') FROM dbo.NewID) AS TaskID, -- Compute TaskID for the new row here so that the id can be propagated downward.
      TaskID AS ParentTaskID, -- This is the TaskID of the WBS3 row.
      OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the WBS3 row.
      OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the WBS3 row.
      WBS1 AS WBS1,
      WBS2 AS WBS2,
      WBS3 AS WBS3,
      LaborCode AS LaborCode,
      ROW_NUMBER() OVER (PARTITION BY PlanID, OutlineNumber, WBS1, WBS2 ORDER BY PlanID, OutlineNumber) AS RowID,
	  Operation
      FROM
        (SELECT DISTINCT -- Get the Unmapped WBS2 rows.
           TU.PlanID AS PlanID,
           TU.TaskID AS TaskID, -- This is the TaskID of the Unmapped WBS2 row.
           TU.ParentOutlineNumber + '.Y' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
           TU.ParentOutlineLevel + 1 AS OutlineLevel,
           TU.WBS1 AS WBS1,
           TU.WBS2 AS WBS2,
           TU.WBS3 AS WBS3,
           LD4.LaborCode AS LaborCode,
		   TU.Operation
           FROM @tabUM_WBS3 AS TU
             INNER JOIN @tabLedgerLBCD AS LD4 ON LD4.WBS1 = TU.WBS1 AND LD4.WBS2 = TU.WBS2 AND LD4.WBS3 = TU.WBS3
             WHERE LD4.WBS2 != ' ' AND LD4.WBS3 != ' '
        ) AS TZ      

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Insert records from RPTask into @tabTask

  INSERT INTO @tabTask
    (PlanID,
     TaskID,
     ParentTaskID,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     Employee,
     LaborCategory,
     Account,
     Vendor,
     Unit,
     UnitTable,
     TaskName,
     OutlineNumber,
     OutlineLevel,
	 WBSType,
	 WBSTypeDesc,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     PlannedExpCost,
     PlannedExpBill,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PlannedConCost,
     PlannedConBill,
     PlannedDirConCost,
     PlannedDirConBill,
     PlannedUntQty,
     PlannedUntCost,
     PlannedUntBill,
     PlannedDirUntCost,
     PlannedDirUntBill,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineExpCost,
     BaselineExpBill,
     BaselineDirExpCost,
     BaselineDirExpBill,
     BaselineConCost,
     BaselineConBill,
     BaselineDirConCost,
     BaselineDirConBill,
     BaselineUntQty,
     BaselineUntCost,
     BaselineUntBill,
     BaselineDirUntCost,
     BaselineDirUntBill,
     WeightedLabCost,
     WeightedLabBill,
     WeightedExpCost,
     WeightedExpBill,
     WeightedConCost,
     WeightedConBill,
     WeightedUntCost,
     WeightedUntBill
    )
    SELECT
      CT.PlanID AS PlanID,
      CT.TaskID AS TaskID,
      PT.TaskID AS ParentTaskID,
      CONVERT(NVARCHAR(30), ISNULL(CT.WBS1, '<empty>')) AS WBS1,
      CONVERT(NVARCHAR(30), ISNULL(CT.WBS2, ' ')) AS WBS2,
      CONVERT(NVARCHAR(30), ISNULL(CT.WBS3, ' ')) AS WBS3,
      CONVERT(NVARCHAR(14), CASE WHEN CT.LaborCode = '<none>' THEN '<empty>' ELSE ISNULL(CT.LaborCode, '<empty>') END) AS LaborCode,
      CONVERT(NVARCHAR(20), '<empty>') AS Employee,
      -1 AS LaborCategory,
      CONVERT(NVARCHAR(13), '<empty>') AS Account,
      CONVERT(NVARCHAR(20), '<empty>') AS Vendor,
      CONVERT(NVARCHAR(30), '<empty>') AS Unit,
      CONVERT(NVARCHAR(30), '<empty>') AS UnitTable,
      CONVERT(NVARCHAR(255), ISNULL(CT.Name, '<empty>')) AS TaskName,
      CT.OutlineNumber AS OutlineNumber, -- Needed for sorting the leaf tasks.
      CT.OutlineLevel AS OutlineLevel,
      ISNULL(CT.WBSType, ' ') AS WBSType,
      CONVERT(NVARCHAR(50), ISNULL(CASE WHEN CT.WBSType = 'WBS1' THEN @WBS1Label
                                        WHEN CT.WBSType = 'WBS2' THEN @WBS2Label
                                        WHEN CT.WBSType = 'WBS3' THEN @WBS3Label
                                        WHEN CT.WBSType = 'LBCD' THEN @LBCDLabel
		                                END, ' ')) AS WBSTypeDesc,
      CT.PlannedLaborHrs AS PlannedLaborHrs,
      CT.PlannedLabCost AS PlannedLabCost,
      CT.PlannedLabBill AS PlannedLabBill,
      CT.PlannedExpCost AS PlannedExpCost,
      CT.PlannedExpBill AS PlannedExpBill,
      CT.PlannedDirExpCost AS PlannedDirExpCost,
      CT.PlannedDirExpBill AS PlannedDirExpBill,
      CT.PlannedConCost AS PlannedConCost,
      CT.PlannedConBill AS PlannedConBill,
      CT.PlannedDirConCost AS PlannedDirConCost,
      CT.PlannedDirConBill AS PlannedDirConBill,
      CT.PlannedUntQty AS PlannedUntQty,
      CT.PlannedUntCost AS PlannedUntCost,
      CT.PlannedUntBill AS PlannedUntBill,
      CT.PlannedDirUntCost AS PlannedDirUntCost,
      CT.PlannedDirUntBill AS PlannedDirUntBill,
      CT.BaselineLaborHrs AS BaselineLaborHrs,
      CT.BaselineLabCost AS BaselineLabCost,
      CT.BaselineLabBill AS BaselineLabBill,
      CT.BaselineExpCost AS BaselineExpCost,
      CT.BaselineExpBill AS BaselineExpBill,
      CT.BaselineDirExpCost AS BaselineDirExpCost,
      CT.BaselineDirExpBill AS BaselineDirExpBill,
      CT.BaselineConCost AS BaselineConCost,
      CT.BaselineConBill AS BaselineConBill,
      CT.BaselineDirConCost AS BaselineDirConCost,
      CT.BaselineDirConBill AS BaselineDirConBill,
      CT.BaselineUntQty AS BaselineUntQty,
      CT.BaselineUntCost AS BaselineUntCost,
      CT.BaselineUntBill AS BaselineUntBill,
      CT.BaselineDirUntCost AS BaselineDirUntCost,
      CT.BaselineDirUntBill AS BaselineDirUntBill,
      CT.WeightedLabCost AS WeightedLabCost,
      CT.WeightedLabBill AS WeightedLabBill,
      CT.WeightedExpCost AS WeightedExpCost,
      CT.WeightedExpBill AS WeightedExpBill,
      CT.WeightedConCost AS WeightedConCost,
      CT.WeightedConBill AS WeightedConBill,
      CT.WeightedUntCost AS WeightedUntCost,
      CT.WeightedUntBill AS WeightedUntBill	 
      FROM RPTask AS CT	  
        INNER JOIN RPPlan AS P ON CT.PlanID = P.PlanID
        LEFT JOIN RPTask AS PT ON CT.PlanID = PT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Add the unmapped WBS2 records into @tabTask

  INSERT INTO @tabTask
    (PlanID,
     TaskID,
     ParentTaskID,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     Employee,
     LaborCategory,
     Account,
     Vendor,
     Unit,
     UnitTable,
     TaskName,
     OutlineNumber,
     OutlineLevel,
	 WBSType,
	 WBSTypeDesc,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     PlannedExpCost,
     PlannedExpBill,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PlannedConCost,
     PlannedConBill,
     PlannedDirConCost,
     PlannedDirConBill,
     PlannedUntQty,
     PlannedUntCost,
     PlannedUntBill,
     PlannedDirUntCost,
     PlannedDirUntBill,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineExpCost,
     BaselineExpBill,
     BaselineDirExpCost,
     BaselineDirExpBill,
     BaselineConCost,
     BaselineConBill,
     BaselineDirConCost,
     BaselineDirConBill,
     BaselineUntQty,
     BaselineUntCost,
     BaselineUntBill,
     BaselineDirUntCost,
     BaselineDirUntBill,
     WeightedLabCost,
     WeightedLabBill,
     WeightedExpCost,
     WeightedExpBill,
     WeightedConCost,
     WeightedConBill,
     WeightedUntCost,
     WeightedUntBill
    )
    SELECT
      CT.PlanID AS PlanID,
      CT.TaskID AS TaskID,
      CT.ParentTaskID AS ParentTaskID,
      CONVERT(NVARCHAR(30), CT.WBS1) AS WBS1,
      CONVERT(NVARCHAR(30), CT.WBS2) AS WBS2,
      CONVERT(NVARCHAR(30), ' ') AS WBS3,
      CONVERT(NVARCHAR(14), '<empty>') AS LaborCode,
      CONVERT(NVARCHAR(20), '<empty>') AS Employee,
      -1 AS LaborCategory,
      CONVERT(NVARCHAR(13), '<empty>') AS Account,
      CONVERT(NVARCHAR(20), '<empty>') AS Vendor,
      CONVERT(NVARCHAR(30), '<empty>') AS Unit,
      CONVERT(NVARCHAR(30), '<empty>') AS UnitTable,
      CONVERT(NVARCHAR(255), '*' + P.Name) AS TaskName,
      CT.ParentOutlineNumber + '.X' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
      CT.ParentOutlineLevel + 1 AS OutlineLevel,
      'WBS2' AS WBSType,
      @WBS2Label AS WBSTypeDesc,
      0 AS PlannedLaborHrs,
      0 AS PlannedLabCost,
      0 AS PlannedLabBill,
      0 AS PlannedExpCost,
      0 AS PlannedExpBill,
      0 AS PlannedDirExpCost,
      0 AS PlannedDirExpBill,
      0 AS PlannedConCost,
      0 AS PlannedConBill,
      0 AS PlannedDirConCost,
      0 AS PlannedDirConBill,
      0 AS PlannedUntQty,
      0 AS PlannedUntCost,
      0 AS PlannedUntBill,
      0 AS PlannedDirUntCost,
      0 AS PlannedDirUntBill,
      0 AS BaselineLaborHrs,
      0 AS BaselineLabCost,
      0 AS BaselineLabBill,
      0 AS BaselineExpCost,
      0 AS BaselineExpBill,
      0 AS BaselineDirExpCost,
      0 AS BaselineDirExpBill,
      0 AS BaselineConCost,
      0 AS BaselineConBill,
      0 AS BaselineDirConCost,
      0 AS BaselineDirConBill,
      0 AS BaselineUntQty,
      0 AS BaselineUntCost,
      0 AS BaselineUntBill,
      0 AS BaselineDirUntCost,
      0 AS BaselineDirUntBill,
      0 AS WeightedLabCost,
      0 AS WeightedLabBill,
      0 AS WeightedExpCost,
      0 AS WeightedExpBill,
      0 AS WeightedConCost,
      0 AS WeightedConBill,
      0 AS WeightedUntCost,
      0 AS WeightedUntBill
      FROM @tabUM_WBS2 AS CT
        INNER JOIN PR AS P ON CT.WBS1 = P.WBS1 AND CT.WBS2 = P.WBS2 AND P.WBS3 = ' '
        
  -- Add the unmapped WBS3 records into @tabTask
  
  INSERT INTO @tabTask
    (PlanID,
     TaskID,
     ParentTaskID,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     Employee,
     LaborCategory,
     Account,
     Vendor,
     Unit,
     UnitTable,
     TaskName,
     OutlineNumber,
     OutlineLevel,
	 WBSType,
	 WBSTypeDesc,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     PlannedExpCost,
     PlannedExpBill,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PlannedConCost,
     PlannedConBill,
     PlannedDirConCost,
     PlannedDirConBill,
     PlannedUntQty,
     PlannedUntCost,
     PlannedUntBill,
     PlannedDirUntCost,
     PlannedDirUntBill,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineExpCost,
     BaselineExpBill,
     BaselineDirExpCost,
     BaselineDirExpBill,
     BaselineConCost,
     BaselineConBill,
     BaselineDirConCost,
     BaselineDirConBill,
     BaselineUntQty,
     BaselineUntCost,
     BaselineUntBill,
     BaselineDirUntCost,
     BaselineDirUntBill,
     WeightedLabCost,
     WeightedLabBill,
     WeightedExpCost,
     WeightedExpBill,
     WeightedConCost,
     WeightedConBill,
     WeightedUntCost,
     WeightedUntBill
    )
    SELECT
      CT.PlanID AS PlanID,
      CT.TaskID AS TaskID,
      CT.ParentTaskID AS ParentTaskID,
      CONVERT(NVARCHAR(30), CT.WBS1) AS WBS1,
      CONVERT(NVARCHAR(30), CT.WBS2) AS WBS2,
      CONVERT(NVARCHAR(30), CT.WBS3) AS WBS3,
      CONVERT(NVARCHAR(14), '<empty>') AS LaborCode,
      CONVERT(NVARCHAR(20), '<empty>') AS Employee,
      -1 AS LaborCategory,
      CONVERT(NVARCHAR(13), '<empty>') AS Account,
      CONVERT(NVARCHAR(20), '<empty>') AS Vendor,
      CONVERT(NVARCHAR(30), '<empty>') AS Unit,
      CONVERT(NVARCHAR(30), '<empty>') AS UnitTable,
      CONVERT(NVARCHAR(255), '*' + P.Name) AS TaskName,
      CT.ParentOutlineNumber + '.Y' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
      CT.ParentOutlineLevel + 1 AS OutlineLevel,
      'WBS3' AS WBSType,
      @WBS3Label AS WBSTypeDesc,
      0 AS PlannedLaborHrs,
      0 AS PlannedLabCost,
      0 AS PlannedLabBill,
      0 AS PlannedExpCost,
      0 AS PlannedExpBill,
      0 AS PlannedDirExpCost,
      0 AS PlannedDirExpBill,
      0 AS PlannedConCost,
      0 AS PlannedConBill,
      0 AS PlannedDirConCost,
      0 AS PlannedDirConBill,
      0 AS PlannedUntQty,
      0 AS PlannedUntCost,
      0 AS PlannedUntBill,
      0 AS PlannedDirUntCost,
      0 AS PlannedDirUntBill,
      0 AS BaselineLaborHrs,
      0 AS BaselineLabCost,
      0 AS BaselineLabBill,
      0 AS BaselineExpCost,
      0 AS BaselineExpBill,
      0 AS BaselineDirExpCost,
      0 AS BaselineDirExpBill,
      0 AS BaselineConCost,
      0 AS BaselineConBill,
      0 AS BaselineDirConCost,
      0 AS BaselineDirConBill,
      0 AS BaselineUntQty,
      0 AS BaselineUntCost,
      0 AS BaselineUntBill,
      0 AS BaselineDirUntCost,
      0 AS BaselineDirUntBill,
      0 AS WeightedLabCost,
      0 AS WeightedLabBill,
      0 AS WeightedExpCost,
      0 AS WeightedExpBill,
      0 AS WeightedConCost,
      0 AS WeightedConBill,
      0 AS WeightedUntCost,
      0 AS WeightedUntBill	  
      FROM @tabUM_WBS3 AS CT
        INNER JOIN PR AS P ON CT.WBS1 = P.WBS1 AND CT.WBS2 = P.WBS2 AND CT.WBS3 = P.WBS3

  -- Add the unmapped Labor Code records into @tabTask
  
  INSERT INTO @tabTask
    (PlanID,
     TaskID,
     ParentTaskID,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     Employee,
     LaborCategory,
     Account,
     Vendor,
     Unit,
     UnitTable,
     TaskName,
     OutlineNumber,
     OutlineLevel,
	 WBSType,
	 WBSTypeDesc,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     PlannedExpCost,
     PlannedExpBill,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PlannedConCost,
     PlannedConBill,
     PlannedDirConCost,
     PlannedDirConBill,
     PlannedUntQty,
     PlannedUntCost,
     PlannedUntBill,
     PlannedDirUntCost,
     PlannedDirUntBill,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineExpCost,
     BaselineExpBill,
     BaselineDirExpCost,
     BaselineDirExpBill,
     BaselineConCost,
     BaselineConBill,
     BaselineDirConCost,
     BaselineDirConBill,
     BaselineUntQty,
     BaselineUntCost,
     BaselineUntBill,
     BaselineDirUntCost,
     BaselineDirUntBill,
     WeightedLabCost,
     WeightedLabBill,
     WeightedExpCost,
     WeightedExpBill,
     WeightedConCost,
     WeightedConBill,
     WeightedUntCost,
     WeightedUntBill
    )
    SELECT
      CT.PlanID AS PlanID,
      CT.TaskID AS TaskID,
      CT.ParentTaskID AS ParentTaskID,
      CONVERT(NVARCHAR(30), CT.WBS1) AS WBS1,
      CONVERT(NVARCHAR(30), ISNULL(CT.WBS2, ' ')) AS WBS2,
      CONVERT(NVARCHAR(30), ISNULL(CT.WBS3, ' ')) AS WBS3,
      CONVERT(NVARCHAR(14), CT.LaborCode) AS LaborCode,
      CONVERT(NVARCHAR(20), '<empty>') AS Employee,
      -1 AS LaborCategory,
      CONVERT(NVARCHAR(13), '<empty>') AS Account,
      CONVERT(NVARCHAR(20), '<empty>') AS Vendor,
      CONVERT(NVARCHAR(30), '<empty>') AS Unit,
      CONVERT(NVARCHAR(30), '<empty>') AS UnitTable,
      CONVERT(NVARCHAR(255), '*' + @LBCDLabel + ' - ' + CT.LaborCode) AS TaskName,
      CT.ParentOutlineNumber + '.Z' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
      CT.ParentOutlineLevel + 1 AS OutlineLevel,
      'LBCD' AS WBSType,
      @LBCDLabel AS WBSTypeDesc,
      0 AS PlannedLaborHrs,
      0 AS PlannedLabCost,
      0 AS PlannedLabBill,
      0 AS PlannedExpCost,
      0 AS PlannedExpBill,
      0 AS PlannedDirExpCost,
      0 AS PlannedDirExpBill,
      0 AS PlannedConCost,
      0 AS PlannedConBill,
      0 AS PlannedDirConCost,
      0 AS PlannedDirConBill,
      0 AS PlannedUntQty,
      0 AS PlannedUntCost,
      0 AS PlannedUntBill,
      0 AS PlannedDirUntCost,
      0 AS PlannedDirUntBill,
      0 AS BaselineLaborHrs,
      0 AS BaselineLabCost,
      0 AS BaselineLabBill,
      0 AS BaselineExpCost,
      0 AS BaselineExpBill,
      0 AS BaselineDirExpCost,
      0 AS BaselineDirExpBill,
      0 AS BaselineConCost,
      0 AS BaselineConBill,
      0 AS BaselineDirConCost,
      0 AS BaselineDirConBill,
      0 AS BaselineUntQty,
      0 AS BaselineUntCost,
      0 AS BaselineUntBill,
      0 AS BaselineDirUntCost,
      0 AS BaselineDirUntBill,
      0 AS WeightedLabCost,
      0 AS WeightedLabBill,
      0 AS WeightedExpCost,
      0 AS WeightedExpBill,
      0 AS WeightedConCost,
      0 AS WeightedConBill,
      0 AS WeightedUntCost,
      0 AS WeightedUntBill
      FROM @tabUM_LBCD AS CT

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- At this point, all of the mapped and unmapped Tasks (WBS1, WBS2, WBS3, LBCD) have already been inserted into @tabTask.
-- Save these rows into a separate table (which has indexes) so that performance is not suffer.
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  INSERT INTO @tabWBSTree
    (PlanID,
     TaskID,
     ParentTaskID,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     OutlineNumber,
     OutlineLevel,
     WBSType
    )
    SELECT
      PlanID,
      TaskID,
      ParentTaskID,
      WBS1,
      WBS2,
      WBS3,
      LaborCode,
      OutlineNumber,
      OutlineLevel,
      WBSType
      FROM @tabTask
      ORDER BY PlanID, OutlineNumber
    
--AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
--A Assignment.                                                                                                                                                    A
--AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

  --AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
  --A A1. Insert all Employees and Generic Resources from RPAssignment into @tabTask.                                                                              A
  --AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

  INSERT INTO @tabTask -->>> A1. Insert all Employees and Generic Resources from RPAssignment into @tabTask.
    (PlanID,
     TaskID,
     ParentTaskID,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     Employee,
     LaborCategory,
     Account,
     Vendor,
     Unit,
     UnitTable,
     TaskName,
     OutlineNumber,
     OutlineLevel,
     WBSType,
     WBSTypeDesc,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     PlannedExpCost,
     PlannedExpBill,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PlannedConCost,
     PlannedConBill,
     PlannedDirConCost,
     PlannedDirConBill,
     PlannedUntQty,
     PlannedUntCost,
     PlannedUntBill,
     PlannedDirUntCost,
     PlannedDirUntBill,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineExpCost,
     BaselineExpBill,
     BaselineDirExpCost,
     BaselineDirExpBill,
     BaselineConCost,
     BaselineConBill,
     BaselineDirConCost,
     BaselineDirConBill,
     BaselineUntQty,
     BaselineUntCost,
     BaselineUntBill,
     BaselineDirUntCost,
     BaselineDirUntBill,
     WeightedLabCost,
     WeightedLabBill,
     WeightedExpCost,
     WeightedExpBill,
     WeightedConCost,
     WeightedConBill,
     WeightedUntCost,
     WeightedUntBill
    )
    SELECT
      AZ.PlanID AS PlanID,
      AZ.TaskID AS TaskID,
      AZ.ParentTaskID AS ParentTaskID,
      CONVERT(NVARCHAR(30), AZ.WBS1) AS WBS1,
      CONVERT(NVARCHAR(30), ISNULL(AZ.WBS2, ' ')) AS WBS2,
      CONVERT(NVARCHAR(30), ISNULL(AZ.WBS3, ' ')) AS WBS3,
      CONVERT(NVARCHAR(14), CASE WHEN AZ.LaborCode = '<none>' THEN '<empty>' ELSE ISNULL(AZ.LaborCode, '<empty>') END) AS LaborCode,
      CONVERT(NVARCHAR(20), ISNULL(AZ.Employee, '<empty>')) AS Employee,
      CASE WHEN AZ.Employee IS NULL THEN ISNULL(AZ.Category, -1) ELSE -1 END AS LaborCategory,
      CONVERT(NVARCHAR(13), '<empty>') AS Account,
      CONVERT(NVARCHAR(20), '<empty>') AS Vendor,
      CONVERT(NVARCHAR(30), '<empty>') AS Unit,
      CONVERT(NVARCHAR(30), '<empty>') AS UnitTable,
      CONVERT(NVARCHAR(255), ISNULL(CASE WHEN AZ.Employee IS NOT NULL 
                                         THEN ISNULL(E.Employee, '') + ' :: ' + ISNULL(E.FirstName, '') + ISNULL(' ' + LEFT(E.MiddleName, 1), '') + ISNULL(' ' + E.LastName, '')
                                         WHEN AZ.GenericResourceID IS NOT NULL THEN
                                           CASE WHEN AZ.Category != 0 THEN ISNULL(CONVERT(NVARCHAR(5), GR.Category), '') + ' - ' + ISNULL(GR.Name, '<empty>')
                                                WHEN AZ.GRLBCD IS NOT NULL THEN ISNULL(GR.LaborCode, '') + ' - ' + ISNULL(GR.Name, '<empty>')
                                                ELSE '<empty> - ' + ISNULL(GR.Name, '<empty>')
                                           END
                                         END, '<empty>')) AS TaskName,
      AZ.ParentOutlineNumber + CASE WHEN AZ.Employee IS NOT NULL THEN '.AE' ELSE '.AG' END + 
        RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(AZ.RowID, 36)), 3) AS OutlineNumber, -- Needed for sorting the leaf tasks.
      AZ.ParentOutlineLevel + 1 AS OutlineLevel,
      CASE WHEN AZ.Employee IS NOT NULL THEN CONVERT(VARCHAR(4), 'EMPL') ELSE CONVERT(VARCHAR(4), 'CATG') END AS WBSType,
      CONVERT(NVARCHAR(50), ' ') AS WBSTypeDesc,
      AZ.PlannedLaborHrs AS PlannedLaborHrs,
      AZ.PlannedLabCost AS PlannedLabCost,
      AZ.PlannedLabBill AS PlannedLabBill,
      0 AS PlannedExpCost,
      0 AS PlannedExpBill,
      0 AS PlannedDirExpCost,
      0 AS PlannedDirExpBill,
      0 AS PlannedConCost,
      0 AS PlannedConBill,
      0 AS PlannedDirConCost,
      0 AS PlannedDirConBill,
      0 AS PlannedUntQty,
      0 AS PlannedUntCost,
      0 AS PlannedUntBill,
      0 AS PlannedDirUntCost,
      0 AS PlannedDirUntBill,
      AZ.BaselineLaborHrs AS BaselineLaborHrs,
      AZ.BaselineLabCost AS BaselineLabCost,
      AZ.BaselineLabBill AS BaselineLabBill,
      0 AS BaselineExpCost,
      0 AS BaselineExpBill,
      0 AS BaselineDirExpCost,
      0 AS BaselineDirExpBill,
      0 AS BaselineConCost,
      0 AS BaselineConBill,
      0 AS BaselineDirConCost,
      0 AS BaselineDirConBill,
      0 AS BaselineUntQty,
      0 AS BaselineUntCost,
      0 AS BaselineUntBill,
      0 AS BaselineDirUntCost,
      0 AS BaselineDirUntBill,
      AZ.WeightedLabCost AS WeightedLabCost,
      AZ.WeightedLabBill AS WeightedLabBill,
      0 AS WeightedExpCost,
      0 AS WeightedExpBill,
      0 AS WeightedConCost,
      0 AS WeightedConBill,
      0 AS WeightedUntCost,
      0 AS WeightedUntBill
      FROM 
        (SELECT DISTINCT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
           A.PlanID AS PlanID,
           A.AssignmentID AS TaskID,
           A.TaskID AS ParentTaskID, -- This is the TaskID of the parent row.
           PT.OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the parent row.
           PT.OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the parent row.
           A.WBS1 AS WBS1,
           A.WBS2 AS WBS2,
           A.WBS3 AS WBS3,
           A.LaborCode AS LaborCode,
           A.ResourceID AS Employee,
           A.GenericResourceID AS GenericResourceID,
           A.Category AS Category,
           A.GRLBCD AS GRLBCD,
           A.PlannedLaborHrs AS PlannedLaborHrs,
           A.PlannedLabCost AS PlannedLabCost,
           A.PlannedLabBill AS PlannedLabBill,
           A.BaselineLaborHrs AS BaselineLaborHrs,
           A.BaselineLabCost AS BaselineLabCost,
           A.BaselineLabBill AS BaselineLabBill,
           A.WeightedLabCost AS WeightedLabCost,
           A.WeightedLabBill AS WeightedLabBill,
           ROW_NUMBER() OVER (PARTITION BY A.PlanID, PT.ParentOutlineNumber, A.WBS1, A.WBS2, A.WBS3, A.LaborCode ORDER BY A.PlanID, PT.OutlineNumber) AS RowID
           FROM RPAssignment AS A
             INNER JOIN RPPlan AS P ON A.PlanID = P.PlanID
             INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID AND A.TaskID = PT.TaskID			 
        ) AS AZ      
        LEFT JOIN EM AS E ON AZ.Employee = E.Employee
        LEFT JOIN GR ON AZ.GenericResourceID = GR.Code

  --AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
  --A A2. Insert what left of the unmatched Employees from Ledger table into @tabTask.                                                                             A
  --AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

  INSERT INTO @tabTask -->>> A2. Insert what left of the unmatched Employees from Ledger table into @tabTask.
    (PlanID,
     TaskID,
     ParentTaskID,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     Employee,
     LaborCategory,
     Account,
     Vendor,
     Unit,
     UnitTable,
     TaskName,
     OutlineNumber,
     OutlineLevel,
     WBSType,
     WBSTypeDesc,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     PlannedExpCost,
     PlannedExpBill,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PlannedConCost,
     PlannedConBill,
     PlannedDirConCost,
     PlannedDirConBill,
     PlannedUntQty,
     PlannedUntCost,
     PlannedUntBill,
     PlannedDirUntCost,
     PlannedDirUntBill,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineExpCost,
     BaselineExpBill,
     BaselineDirExpCost,
     BaselineDirExpBill,
     BaselineConCost,
     BaselineConBill,
     BaselineDirConCost,
     BaselineDirConBill,
     BaselineUntQty,
     BaselineUntCost,
     BaselineUntBill,
     BaselineDirUntCost,
     BaselineDirUntBill,
     WeightedLabCost,
     WeightedLabBill,
     WeightedExpCost,
     WeightedExpBill,
     WeightedConCost,
     WeightedConBill,
     WeightedUntCost,
     WeightedUntBill
    )
    SELECT
      AZ.PlanID AS PlanID,
      AZ.TaskID AS TaskID,
      AZ.ParentTaskID AS ParentTaskID,
      CONVERT(NVARCHAR(30), AZ.WBS1) AS WBS1,
      CONVERT(NVARCHAR(30), ISNULL(AZ.WBS2, ' ')) AS WBS2,
      CONVERT(NVARCHAR(30), ISNULL(AZ.WBS3, ' ')) AS WBS3,
      CONVERT(NVARCHAR(14), CASE WHEN AZ.LaborCode = '<none>' THEN '<empty>' ELSE ISNULL(AZ.LaborCode, '<empty>') END) AS LaborCode,
      CONVERT(NVARCHAR(20), ISNULL(AZ.Employee, '<empty>')) AS Employee,
      -1 AS LaborCategory,
      CONVERT(NVARCHAR(13), '<empty>') AS Account,
      CONVERT(NVARCHAR(20), '<empty>') AS Vendor,
      CONVERT(NVARCHAR(30), '<empty>') AS Unit,
      CONVERT(NVARCHAR(30), '<empty>') AS UnitTable,
      CONVERT(NVARCHAR(255), '*' + ISNULL(E.Employee, '') + ' :: ' + ISNULL(E.FirstName, '') + ISNULL(' ' + LEFT(E.MiddleName, 1), '') + ISNULL(' ' + E.LastName, '')) AS TaskName,
      AZ.ParentOutlineNumber + '.AE9' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(AZ.RowID, 36)), 3) AS OutlineNumber, -- Needed for sorting the leaf tasks.
      AZ.ParentOutlineLevel + 1 AS OutlineLevel,
      CONVERT(VARCHAR(4), 'EMPL') AS WBSType,
      CONVERT(NVARCHAR(50), ' ') AS WBSTypeDesc,
      0 AS PlannedLaborHrs,
      0 AS PlannedLabCost,
      0 AS PlannedLabBill,
      0 AS PlannedExpCost,
      0 AS PlannedExpBill,
      0 AS PlannedDirExpCost,
      0 AS PlannedDirExpBill,
      0 AS PlannedConCost,
      0 AS PlannedConBill,
      0 AS PlannedDirConCost,
      0 AS PlannedDirConBill,
      0 AS PlannedUntQty,
      0 AS PlannedUntCost,
      0 AS PlannedUntBill,
      0 AS PlannedDirUntCost,
      0 AS PlannedDirUntBill,
      0 AS BaselineLaborHrs,
      0 AS BaselineLabCost,
      0 AS BaselineLabBill,
      0 AS BaselineExpCost,
      0 AS BaselineExpBill,
      0 AS BaselineDirExpCost,
      0 AS BaselineDirExpBill,
      0 AS BaselineConCost,
      0 AS BaselineConBill,
      0 AS BaselineDirConCost,
      0 AS BaselineDirConBill,
      0 AS BaselineUntQty,
      0 AS BaselineUntCost,
      0 AS BaselineUntBill,
      0 AS BaselineDirUntCost,
      0 AS BaselineDirUntBill,
      0 AS WeightedLabCost,
      0 AS WeightedLabBill,
      0 AS WeightedExpCost,
      0 AS WeightedExpBill,
      0 AS WeightedConCost,
      0 AS WeightedConBill,
      0 AS WeightedUntCost,
      0 AS WeightedUntBill
      FROM
        (SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
           PlanID AS PlanID,
           (SELECT REPLACE(CAST(NEWID AS VARCHAR(36)), '-', '') FROM dbo.NewID) AS TaskID, -- Compute TaskID for the new row here so that the id can be propagated downward.
           TaskID AS ParentTaskID, -- This is the TaskID of the parent row.
           OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the parent row.
           OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the parent row.
           WBS1 AS WBS1,
           WBS2 AS WBS2,
           WBS3 AS WBS3,
           LaborCode AS LaborCode,
           Employee AS Employee,
           ROW_NUMBER() OVER (PARTITION BY PlanID, OutlineNumber, WBS1, WBS2, WBS3, LaborCode ORDER BY PlanID, OutlineNumber) AS RowID
           FROM
             (SELECT DISTINCT
                PT.PlanID AS PlanID,
                PT.TaskID AS TaskID,
                PT.OutlineNumber AS OutlineNumber,
                PT.OutlineLevel AS OutlineLevel,
                PT.WBS1 AS WBS1,
                PT.WBS2 AS WBS2,
                PT.WBS3 AS WBS3,
                PT.LaborCode AS LaborCode,
                LD.Employee AS Employee
                FROM 
                  @tabLedgerEM AS LD
                  INNER JOIN 
                    (SELECT
                       PlanID AS PlanID,
                       TaskID AS TaskID,
                       WBS1 AS WBS1,
                       WBS2 AS WBS2,
                       WBS3 AS WBS3,
                       CASE WHEN LaborCode = '<empty>' THEN NULL ELSE LaborCode END AS LaborCode,
                       OutlineNumber AS OutlineNumber,
                       OutlineLevel AS OutlineLevel,
                       WBSType AS WBSType
                       FROM @tabWBSTree WHERE WBSType IN ('WBS1', 'WBS2', 'WBS3', 'LBCD')
                    ) AS PT ON LD.WBS1 = PT.WBS1
                      AND LD.WBS2 = PT.WBS2
                      AND LD.WBS3 = PT.WBS3
                      AND ISNULL(LD.LaborCode, -999) = ISNULL(PT.LaborCode, -999)
                      AND PT.WBSType = CASE WHEN LD.WBS1 != ' ' AND LD.WBS2 = ' ' AND LD.WBS3 = ' ' AND LD.LaborCode IS NULL THEN 'WBS1'
                                            WHEN LD.WBS1 != ' ' AND LD.WBS2 != ' ' AND LD.WBS3 = ' ' AND LD.LaborCode IS NULL THEN 'WBS2'
                                            WHEN LD.WBS1 != ' ' AND LD.WBS2 != ' ' AND LD.WBS3 != ' ' AND LD.LaborCode IS NULL THEN 'WBS3'
                                            WHEN LD.LaborCode IS NOT NULL THEN 'LBCD' END
                  LEFT JOIN RPAssignment AS A
                    ON PT.PlanID = A.PlanID AND PT.TaskID = A.TaskID 
                        AND LD.WBS1 = A.WBS1
                        AND LD.WBS2 = ISNULL(A.WBS2, ' ')
                        AND LD.WBS3 = ISNULL(A.WBS3, ' ')
                        AND ISNULL(LD.LaborCode, -999) = ISNULL(A.LaborCode, -999)
                        AND LD.Employee = A.ResourceID AND A.ResourceID IS NOT NULL
                WHERE A.AssignmentID IS NULL
             ) AS AX
        ) AS AZ
        LEFT JOIN EM AS E ON AZ.Employee = E.Employee
      
--EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
--E Expense.                                                                                                                                                       E
--EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE

  --EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
  --E E1. Insert detail rows for Account and Vendor combination from RPExpense into @tabTask.                                                                      E
  --EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE

  INSERT INTO @tabTask -->>> E1. Insert detail rows for Account and Vendor combination from RPExpense into @tabTask.
    (PlanID,
     TaskID,
     ParentTaskID,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     Employee,
     LaborCategory,
     Account,
     Vendor,
     Unit,
     UnitTable,
     TaskName,
     OutlineNumber,
     OutlineLevel,
     WBSType,
     WBSTypeDesc,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     PlannedExpCost,
     PlannedExpBill,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PlannedConCost,
     PlannedConBill,
     PlannedDirConCost,
     PlannedDirConBill,
     PlannedUntQty,
     PlannedUntCost,
     PlannedUntBill,
     PlannedDirUntCost,
     PlannedDirUntBill,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineExpCost,
     BaselineExpBill,
     BaselineDirExpCost,
     BaselineDirExpBill,
     BaselineConCost,
     BaselineConBill,
     BaselineDirConCost,
     BaselineDirConBill,
     BaselineUntQty,
     BaselineUntCost,
     BaselineUntBill,
     BaselineDirUntCost,
     BaselineDirUntBill,
     WeightedLabCost,
     WeightedLabBill,
     WeightedExpCost,
     WeightedExpBill,
     WeightedConCost,
     WeightedConBill,
     WeightedUntCost,
     WeightedUntBill
    )
    SELECT
      EZ.PlanID AS PlanID,
      EZ.TaskID AS TaskID,
      EZ.ParentTaskID AS ParentTaskID,
      CONVERT(NVARCHAR(30), EZ.WBS1) AS WBS1,
      CONVERT(NVARCHAR(30), ISNULL(EZ.WBS2, ' ')) AS WBS2,
      CONVERT(NVARCHAR(30), ISNULL(EZ.WBS3, ' ')) AS WBS3,
      CONVERT(NVARCHAR(14), '<empty>') AS LaborCode,
      CONVERT(NVARCHAR(20), '<empty>') AS Employee,
      -1 AS LaborCategory,
      CONVERT(NVARCHAR(13), ISNULL(EZ.Account, '<empty>')) AS Account,
      CONVERT(NVARCHAR(20), ISNULL(EZ.Vendor, '<empty>')) AS Vendor,
      CONVERT(NVARCHAR(30), '<empty>') AS Unit,
      CONVERT(NVARCHAR(30), '<empty>') AS UnitTable,
      CONVERT(NVARCHAR(255), ISNULL(EZ.Account + ' - ' + CA.Name, '<empty>') +
        CASE WHEN EZ.Vendor IS NOT NULL THEN ' (' + ISNULL(EZ.Vendor, '') + ' - ' + ISNULL(VE.Name, '<empty>') + ')' ELSE '' END) AS TaskName,
      EZ.ParentOutlineNumber + '.B' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(EZ.RowID, 36)), 3) AS OutlineNumber, -- Needed for sorting the leaf tasks.
      EZ.ParentOutlineLevel + 1 AS OutlineLevel,
      CONVERT(VARCHAR(4), 'CAVE') AS WBSType,
      CONVERT(NVARCHAR(50), ' ') AS WBSTypeDesc,
      0 AS PlannedLaborHrs,
      0 AS PlannedLabCost,
      0 AS PlannedLabBill,
      EZ.PlannedExpCost AS PlannedExpCost,
      EZ.PlannedExpBill AS PlannedExpBill,
      EZ.PlannedDirExpCost AS PlannedDirExpCost,
      EZ.PlannedDirExpBill AS PlannedDirExpBill,
      0 AS PlannedConCost,
      0 AS PlannedConBill,
      0 AS PlannedDirConCost,
      0 AS PlannedDirConBill,
      0 AS PlannedUntQty,
      0 AS PlannedUntCost,
      0 AS PlannedUntBill,
      0 AS PlannedDirUntCost,
      0 AS PlannedDirUntBill,
      0 AS BaselineLaborHrs,
      0 AS BaselineLabCost,
      0 AS BaselineLabBill,
      EZ.BaselineExpCost AS BaselineExpCost,
      EZ.BaselineExpBill AS BaselineExpBill,
      EZ.BaselineDirExpCost AS BaselineDirExpCost,
      EZ.BaselineDirExpBill AS BaselineDirExpBill,
      0 AS BaselineConCost,
      0 AS BaselineConBill,
      0 AS BaselineDirConCost,
      0 AS BaselineDirConBill,
      0 AS BaselineUntQty,
      0 AS BaselineUntCost,
      0 AS BaselineUntBill,
      0 AS BaselineDirUntCost,
      0 AS BaselineDirUntBill,
      0 AS WeightedLabCost,
      0 AS WeightedLabBill,
      EZ.WeightedExpCost AS WeightedExpCost,
      EZ.WeightedExpBill AS WeightedExpBill,
      0 AS WeightedConCost,
      0 AS WeightedConBill,
      0 AS WeightedUntCost,
      0 AS WeightedUntBill
      FROM 
        (SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
         E.PlanID AS PlanID,
         E.ExpenseID AS TaskID,
         PT.TaskID AS ParentTaskID, -- This is the TaskID of the parent row.
         PT.OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the parent row.
         PT.OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the parent row.
         E.WBS1 AS WBS1,
         E.WBS2 AS WBS2,
         E.WBS3 AS WBS3,
         E.Account AS Account,
         E.Vendor AS Vendor,
         E.PlannedExpCost AS PlannedExpCost,
         E.PlannedExpBill AS PlannedExpBill,
         E.PlannedDirExpCost AS PlannedDirExpCost,
         E.PlannedDirExpBill AS PlannedDirExpBill,
         E.BaselineExpCost AS BaselineExpCost,
         E.BaselineExpBill AS BaselineExpBill,
         E.BaselineDirExpCost AS BaselineDirExpCost,
         E.BaselineDirExpBill AS BaselineDirExpBill,
         E.WeightedExpCost AS WeightedExpCost,
         E.WeightedExpBill AS WeightedExpBill,
         ROW_NUMBER() OVER (PARTITION BY E.PlanID, PT.ParentOutlineNumber, E.WBS1, E.WBS2, E.WBS3 ORDER BY E.PlanID, PT.ParentOutlineNumber, E.Account) AS RowID		
           FROM RPExpense AS E
             INNER JOIN RPTask AS PT ON E.PlanID = PT.PlanID AND E.TaskID = PT.TaskID			 
        ) AS EZ
        LEFT JOIN CA ON EZ.Account = CA.Account
        LEFT JOIN VE ON EZ.Vendor = VE.Vendor

  --EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
  --E E2. Insert detail rows for Account and Vendor combination from Ledger tables into @tabTask.                                                                  E
  --EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE

  INSERT INTO @tabTask -->>> E2. Insert detail rows for Account and Vendor combination from Ledger tables into @tabTask.
    (PlanID,
     TaskID,
     ParentTaskID,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     Employee,
     LaborCategory,
     Account,
     Vendor,
     Unit,
     UnitTable,
     TaskName,
     OutlineNumber,
     OutlineLevel,
     WBSType,
     WBSTypeDesc,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     PlannedExpCost,
     PlannedExpBill,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PlannedConCost,
     PlannedConBill,
     PlannedDirConCost,
     PlannedDirConBill,
     PlannedUntQty,
     PlannedUntCost,
     PlannedUntBill,
     PlannedDirUntCost,
     PlannedDirUntBill,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineExpCost,
     BaselineExpBill,
     BaselineDirExpCost,
     BaselineDirExpBill,
     BaselineConCost,
     BaselineConBill,
     BaselineDirConCost,
     BaselineDirConBill,
     BaselineUntQty,
     BaselineUntCost,
     BaselineUntBill,
     BaselineDirUntCost,
     BaselineDirUntBill,
     WeightedLabCost,
     WeightedLabBill,
     WeightedExpCost,
     WeightedExpBill,
     WeightedConCost,
     WeightedConBill,
     WeightedUntCost,
     WeightedUntBill
    )
    SELECT
      EZ.PlanID AS PlanID,
      EZ.TaskID AS TaskID,
      EZ.ParentTaskID AS ParentTaskID,
      CONVERT(NVARCHAR(30), EZ.WBS1) AS WBS1,
      CONVERT(NVARCHAR(30), ISNULL(EZ.WBS2, ' ')) AS WBS2,
      CONVERT(NVARCHAR(30), ISNULL(EZ.WBS3, ' ')) AS WBS3,
      CONVERT(NVARCHAR(14), '<empty>') AS LaborCode,
      CONVERT(NVARCHAR(20), '<empty>') AS Employee,
      -1 AS LaborCategory,
      CONVERT(NVARCHAR(13), ISNULL(EZ.Account, '<empty>')) AS Account,
      CONVERT(NVARCHAR(20), ISNULL(EZ.Vendor, '<empty>')) AS Vendor,
      CONVERT(NVARCHAR(30), '<empty>') AS Unit,
      CONVERT(NVARCHAR(30), '<empty>') AS UnitTable,
      CONVERT(NVARCHAR(255), '*' + ISNULL(EZ.Account + ' - ' + EZ.AccountName, '<empty>') +
        CASE WHEN EZ.Vendor IS NOT NULL THEN ' (' + ISNULL(EZ.Vendor, '') + ' - ' + ISNULL(VE.Name, '<empty>') + ')' ELSE '' END) AS TaskName,
      EZ.ParentOutlineNumber + '.B8' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(EZ.RowID, 36)), 3) AS OutlineNumber, -- Needed for sorting the leaf tasks.
      EZ.ParentOutlineLevel + 1 AS OutlineLevel,
      CONVERT(VARCHAR(4), 'CAVE') AS WBSType,
      CONVERT(NVARCHAR(50), ' ') AS WBSTypeDesc,
      0 AS PlannedLaborHrs,
      0 AS PlannedLabCost,
      0 AS PlannedLabBill,
      0 AS PlannedExpCost,
      0 AS PlannedExpBill,
      0 AS PlannedDirExpCost,
      0 AS PlannedDirExpBill,
      0 AS PlannedConCost,
      0 AS PlannedConBill,
      0 AS PlannedDirConCost,
      0 AS PlannedDirConBill,
      0 AS PlannedUntQty,
      0 AS PlannedUntCost,
      0 AS PlannedUntBill,
      0 AS PlannedDirUntCost,
      0 AS PlannedDirUntBill,
      0 AS BaselineLaborHrs,
      0 AS BaselineLabCost,
      0 AS BaselineLabBill,
      0 AS BaselineExpCost,
      0 AS BaselineExpBill,
      0 AS BaselineDirExpCost,
      0 AS BaselineDirExpBill,
      0 AS BaselineConCost,
      0 AS BaselineConBill,
      0 AS BaselineDirConCost,
      0 AS BaselineDirConBill,
      0 AS BaselineUntQty,
      0 AS BaselineUntCost,
      0 AS BaselineUntBill,
      0 AS BaselineDirUntCost,
      0 AS BaselineDirUntBill,
      0 AS WeightedLabCost,
      0 AS WeightedLabBill,
      0 AS WeightedExpCost,
      0 AS WeightedExpBill,
      0 AS WeightedConCost,
      0 AS WeightedConBill,
      0 AS WeightedUntCost,
      0 AS WeightedUntBill
      FROM 
        (SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
           PlanID AS PlanID,
           (SELECT REPLACE(CAST(NEWID AS VARCHAR(36)), '-', '') FROM dbo.NewID) AS TaskID, -- Compute TaskID for the new row here so that the id can be propagated downward.
           TaskID AS ParentTaskID, -- This is the TaskID of the parent row.
           OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the parent row.
           OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the parent row.
           WBS1 AS WBS1,
           WBS2 AS WBS2,
           WBS3 AS WBS3,
           Account AS Account,
           AccountName AS AccountName,
           Vendor AS Vendor,
           ROW_NUMBER() OVER (PARTITION BY PlanID, OutlineNumber, WBS1, WBS2, WBS3 ORDER BY PlanID, OutlineNumber, Account, Vendor) AS RowID
           FROM
             (SELECT DISTINCT
                PT.PlanID AS PlanID,
                PT.TaskID AS TaskID,
                PT.OutlineNumber AS OutlineNumber,
                PT.OutlineLevel AS OutlineLevel,
                PT.WBS1 AS WBS1,
                PT.WBS2 AS WBS2,
                PT.WBS3 AS WBS3,
                LD.Account AS Account,
                LD.AccountName AS AccountName,
                LD.Vendor AS Vendor
                FROM
                  (SELECT DISTINCT LDX.WBS1, LDX.WBS2, LDX.WBS3, LDX.Account, LDX.Vendor, CA.Name AS AccountName 
                     FROM LedgerAR AS LDX INNER JOIN CA ON LDX.Account = CA.Account AND CA.Type IN (5, 7) AND ProjectCost = 'Y' AND TransType != 'UN' AND Employee IS NULL
                   UNION
                   SELECT DISTINCT LDX.WBS1, LDX.WBS2, LDX.WBS3, LDX.Account, LDX.Vendor, CA.Name AS AccountName  
                     FROM LedgerAP AS LDX INNER JOIN CA ON LDX.Account = CA.Account AND CA.Type IN (5, 7) AND ProjectCost = 'Y' AND TransType != 'UN' AND Employee IS NULL
                   UNION
                   SELECT DISTINCT LDX.WBS1, LDX.WBS2, LDX.WBS3, LDX.Account, LDX.Vendor, CA.Name AS AccountName  
                     FROM LedgerEX AS LDX INNER JOIN CA ON LDX.Account = CA.Account AND CA.Type IN (5, 7) AND ProjectCost = 'Y' AND TransType != 'UN' AND Employee IS NULL
                   UNION
                   SELECT DISTINCT LDX.WBS1, LDX.WBS2, LDX.WBS3, LDX.Account, LDX.Vendor, CA.Name AS AccountName  
                     FROM LedgerMISC AS LDX INNER JOIN CA ON LDX.Account = CA.Account AND CA.Type IN (5, 7) AND ProjectCost = 'Y' AND TransType != 'UN' AND Employee IS NULL
                  ) AS LD
                  INNER JOIN 
                    (SELECT
                       PlanID AS PlanID,
                       TaskID AS TaskID,
                       WBS1 AS WBS1,
                       WBS2 AS WBS2,
                       WBS3 AS WBS3,
                       OutlineNumber AS OutlineNumber,
                       OutlineLevel AS OutlineLevel,
                       WBSType AS WBSType
                       FROM @tabWBSTree WHERE WBSType IN ('WBS1', 'WBS2', 'WBS3')
                    ) AS PT ON LD.WBS1 = PT.WBS1
                      AND LD.WBS2 = PT.WBS2
                      AND LD.WBS3 = PT.WBS3
                      AND PT.WBSType = CASE WHEN LD.WBS1 != ' ' AND LD.WBS2 = ' ' AND LD.WBS3 = ' ' THEN 'WBS1'
                                            WHEN LD.WBS1 != ' ' AND LD.WBS2 != ' ' AND LD.WBS3 = ' ' THEN 'WBS2'
                                            WHEN LD.WBS1 != ' ' AND LD.WBS2 != ' ' AND LD.WBS3 != ' ' THEN 'WBS3' END
                  LEFT JOIN RPExpense AS E
                    ON PT.PlanID = E.PlanID 
                      AND LD.WBS1 = E.WBS1
                      AND LD.WBS2 = ISNULL(E.WBS2, ' ')
                      AND LD.WBS3 = ISNULL(E.WBS3, ' ')
                      AND LD.Account = E.Account
                      AND ISNULL(LD.Vendor, -999) = ISNULL(E.Vendor, -999)
                WHERE E.ExpenseID IS NULL
             ) AS EY
        ) AS EZ
        LEFT JOIN VE ON EZ.Vendor = VE.Vendor
        
  --EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
  --E E3. Insert detail rows for Account and Employee combination from Ledger tables into @tabTask.                                                                  E
  --EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE

  INSERT INTO @tabTask -->>> E3. Insert detail rows for Account and Employee combination from Ledger tables into @tabTask.
    (PlanID,
     TaskID,
     ParentTaskID,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     Employee,
     LaborCategory,
     Account,
     Vendor,
     Unit,
     UnitTable,
     TaskName,
     OutlineNumber,
     OutlineLevel,
     WBSType,
     WBSTypeDesc,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     PlannedExpCost,
     PlannedExpBill,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PlannedConCost,
     PlannedConBill,
     PlannedDirConCost,
     PlannedDirConBill,
     PlannedUntQty,
     PlannedUntCost,
     PlannedUntBill,
     PlannedDirUntCost,
     PlannedDirUntBill,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineExpCost,
     BaselineExpBill,
     BaselineDirExpCost,
     BaselineDirExpBill,
     BaselineConCost,
     BaselineConBill,
     BaselineDirConCost,
     BaselineDirConBill,
     BaselineUntQty,
     BaselineUntCost,
     BaselineUntBill,
     BaselineDirUntCost,
     BaselineDirUntBill,
     WeightedLabCost,
     WeightedLabBill,
     WeightedExpCost,
     WeightedExpBill,
     WeightedConCost,
     WeightedConBill,
     WeightedUntCost,
     WeightedUntBill
    )
    SELECT
      EZ.PlanID AS PlanID,
      EZ.TaskID AS TaskID,
      EZ.ParentTaskID AS ParentTaskID,
      CONVERT(NVARCHAR(30), EZ.WBS1) AS WBS1,
      CONVERT(NVARCHAR(30), ISNULL(EZ.WBS2, ' ')) AS WBS2,
      CONVERT(NVARCHAR(30), ISNULL(EZ.WBS3, ' ')) AS WBS3,
      CONVERT(NVARCHAR(14), '<empty>') AS LaborCode,
      CONVERT(NVARCHAR(20), ISNULL(EZ.Employee, '<empty>')) AS Employee,
      -1 AS LaborCategory,
      CONVERT(NVARCHAR(13), ISNULL(EZ.Account, '<empty>')) AS Account,
      CONVERT(NVARCHAR(20), '<empty>') AS Vendor,
      CONVERT(NVARCHAR(30), '<empty>') AS Unit,
      CONVERT(NVARCHAR(30), '<empty>') AS UnitTable,
      CONVERT(NVARCHAR(255), '+' + ISNULL(EZ.Account + ' - ' + EZ.AccountName, '<empty>') +
        ' (' + ISNULL(EM.Employee, '') + ' :: ' + ISNULL(EM.FirstName, '') + ISNULL(' ' + LEFT(EM.MiddleName, 1), '') + ISNULL(' ' + EM.LastName, '') + ')') AS TaskName,
      EZ.ParentOutlineNumber + '.B9' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(EZ.RowID, 36)), 3) AS OutlineNumber, -- Needed for sorting the leaf tasks.
      EZ.ParentOutlineLevel + 1 AS OutlineLevel,
      CONVERT(VARCHAR(4), 'CAVE') AS WBSType,
      CONVERT(NVARCHAR(50), ' ') AS WBSTypeDesc,
      0 AS PlannedLaborHrs,
      0 AS PlannedLabCost,
      0 AS PlannedLabBill,
      0 AS PlannedExpCost,
      0 AS PlannedExpBill,
      0 AS PlannedDirExpCost,
      0 AS PlannedDirExpBill,
      0 AS PlannedConCost,
      0 AS PlannedConBill,
      0 AS PlannedDirConCost,
      0 AS PlannedDirConBill,
      0 AS PlannedUntQty,
      0 AS PlannedUntCost,
      0 AS PlannedUntBill,
      0 AS PlannedDirUntCost,
      0 AS PlannedDirUntBill,
      0 AS BaselineLaborHrs,
      0 AS BaselineLabCost,
      0 AS BaselineLabBill,
      0 AS BaselineExpCost,
      0 AS BaselineExpBill,
      0 AS BaselineDirExpCost,
      0 AS BaselineDirExpBill,
      0 AS BaselineConCost,
      0 AS BaselineConBill,
      0 AS BaselineDirConCost,
      0 AS BaselineDirConBill,
      0 AS BaselineUntQty,
      0 AS BaselineUntCost,
      0 AS BaselineUntBill,
      0 AS BaselineDirUntCost,
      0 AS BaselineDirUntBill,
      0 AS WeightedLabCost,
      0 AS WeightedLabBill,
      0 AS WeightedExpCost,
      0 AS WeightedExpBill,
      0 AS WeightedConCost,
      0 AS WeightedConBill,
      0 AS WeightedUntCost,
      0 AS WeightedUntBill
      FROM 
        (SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
           PlanID AS PlanID,
           (SELECT REPLACE(CAST(NEWID AS VARCHAR(36)), '-', '') FROM dbo.NewID) AS TaskID, -- Compute TaskID for the new row here so that the id can be propagated downward.
           TaskID AS ParentTaskID, -- This is the TaskID of the parent row.
           OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the parent row.
           OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the parent row.
           WBS1 AS WBS1,
           WBS2 AS WBS2,
           WBS3 AS WBS3,
           Account AS Account,
           AccountName AS AccountName,
           Employee AS Employee,
           ROW_NUMBER() OVER (PARTITION BY PlanID, OutlineNumber, WBS1, WBS2, WBS3 ORDER BY PlanID, OutlineNumber, Account, Employee) AS RowID
           FROM
             (SELECT DISTINCT
                PT.PlanID AS PlanID,
                PT.TaskID AS TaskID,
                PT.OutlineNumber AS OutlineNumber,
                PT.OutlineLevel AS OutlineLevel,
                PT.WBS1 AS WBS1,
                PT.WBS2 AS WBS2,
                PT.WBS3 AS WBS3,
                LD.Account AS Account,
                LD.AccountName AS AccountName,
                LD.Employee AS Employee
                FROM
                  (SELECT DISTINCT LDX.WBS1, LDX.WBS2, LDX.WBS3, LDX.Account, LDX.Employee, CA.Name AS AccountName 
                     FROM LedgerAR AS LDX INNER JOIN CA ON LDX.Account = CA.Account AND CA.Type IN (5, 7) AND ProjectCost = 'Y' AND TransType != 'UN' AND Employee IS NOT NULL
                   UNION
                   SELECT DISTINCT LDX.WBS1, LDX.WBS2, LDX.WBS3, LDX.Account, LDX.Employee, CA.Name AS AccountName  
                     FROM LedgerAP AS LDX INNER JOIN CA ON LDX.Account = CA.Account AND CA.Type IN (5, 7) AND ProjectCost = 'Y' AND TransType != 'UN' AND Employee IS NOT NULL
                   UNION
                   SELECT DISTINCT LDX.WBS1, LDX.WBS2, LDX.WBS3, LDX.Account, LDX.Employee, CA.Name AS AccountName  
                     FROM LedgerEX AS LDX INNER JOIN CA ON LDX.Account = CA.Account AND CA.Type IN (5, 7) AND ProjectCost = 'Y' AND TransType != 'UN' AND Employee IS NOT NULL
                   UNION
                   SELECT DISTINCT LDX.WBS1, LDX.WBS2, LDX.WBS3, LDX.Account, LDX.Employee, CA.Name AS AccountName  
                     FROM LedgerMISC AS LDX INNER JOIN CA ON LDX.Account = CA.Account AND CA.Type IN (5, 7) AND ProjectCost = 'Y' AND TransType != 'UN' AND Employee IS NOT NULL
                  ) AS LD
                  INNER JOIN 
                    (SELECT
                       PlanID AS PlanID,
                       TaskID AS TaskID,
                       WBS1 AS WBS1,
                       WBS2 AS WBS2,
                       WBS3 AS WBS3,
                       OutlineNumber AS OutlineNumber,
                       OutlineLevel AS OutlineLevel,
                       WBSType AS WBSType
                       FROM @tabWBSTree WHERE WBSType IN ('WBS1', 'WBS2', 'WBS3')
                    ) AS PT ON LD.WBS1 = PT.WBS1
                      AND LD.WBS2 = PT.WBS2
                      AND LD.WBS3 = PT.WBS3
                      AND PT.WBSType = CASE WHEN LD.WBS1 != ' ' AND LD.WBS2 = ' ' AND LD.WBS3 = ' ' THEN 'WBS1'
                                            WHEN LD.WBS1 != ' ' AND LD.WBS2 != ' ' AND LD.WBS3 = ' ' THEN 'WBS2'
                                            WHEN LD.WBS1 != ' ' AND LD.WBS2 != ' ' AND LD.WBS3 != ' ' THEN 'WBS3' END
             ) AS EY
        ) AS EZ
        LEFT JOIN EM ON EZ.Employee = EM.Employee

--CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
--C Consultant.                                                                                                                                                    C
--CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

  --CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
  --C C1. Insert detail rows for Account and Vendor combination from RPConsultant into @tabTask.                                                                   C
  --CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

  INSERT INTO @tabTask -->>> C1. Insert detail rows for Account and Vendor combination from RPConsultant into @tabTask.
    (PlanID,
     TaskID,
     ParentTaskID,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     Employee,
     LaborCategory,
     Account,
     Vendor,
     Unit,
     UnitTable,
     TaskName,
     OutlineNumber,
     OutlineLevel,
     WBSType,
     WBSTypeDesc,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     PlannedExpCost,
     PlannedExpBill,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PlannedConCost,
     PlannedConBill,
     PlannedDirConCost,
     PlannedDirConBill,
     PlannedUntQty,
     PlannedUntCost,
     PlannedUntBill,
     PlannedDirUntCost,
     PlannedDirUntBill,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineExpCost,
     BaselineExpBill,
     BaselineDirExpCost,
     BaselineDirExpBill,
     BaselineConCost,
     BaselineConBill,
     BaselineDirConCost,
     BaselineDirConBill,
     BaselineUntQty,
     BaselineUntCost,
     BaselineUntBill,
     BaselineDirUntCost,
     BaselineDirUntBill,
     WeightedLabCost,
     WeightedLabBill,
     WeightedExpCost,
     WeightedExpBill,
     WeightedConCost,
     WeightedConBill,
     WeightedUntCost,
     WeightedUntBill
    )
    SELECT
      CZ.PlanID AS PlanID,
      CZ.TaskID AS TaskID,
      CZ.ParentTaskID AS ParentTaskID,
      CONVERT(NVARCHAR(30), CZ.WBS1) AS WBS1,
      CONVERT(NVARCHAR(30), ISNULL(CZ.WBS2, ' ')) AS WBS2,
      CONVERT(NVARCHAR(30), ISNULL(CZ.WBS3, ' ')) AS WBS3,
      CONVERT(NVARCHAR(14), '<empty>') AS LaborCode,
      CONVERT(NVARCHAR(20), '<empty>') AS Employee,
      -1 AS LaborCategory,
      CONVERT(NVARCHAR(13), ISNULL(CZ.Account, '<empty>')) AS Account,
      CONVERT(NVARCHAR(20), ISNULL(CZ.Vendor, '<empty>')) AS Vendor,
      CONVERT(NVARCHAR(30), '<empty>') AS Unit,
      CONVERT(NVARCHAR(30), '<empty>') AS UnitTable,
      CONVERT(NVARCHAR(255), ISNULL(CZ.Account + ' - ' + CA.Name, '<empty>') +
        CASE WHEN CZ.Vendor IS NOT NULL THEN ' (' + ISNULL(CZ.Vendor, '') + ' - ' + ISNULL(VE.Name, '<empty>') + ')' ELSE '' END) AS TaskName,
      CZ.ParentOutlineNumber + '.C' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(CZ.RowID, 36)), 3) AS OutlineNumber, -- Needed for sorting the leaf tasks.
      CZ.ParentOutlineLevel + 1 AS OutlineLevel,
      CONVERT(VARCHAR(4), 'CAVE') AS WBSType,
      CONVERT(NVARCHAR(50), ' ') AS WBSTypeDesc,
      0 AS PlannedLaborHrs,
      0 AS PlannedLabCost,
      0 AS PlannedLabBill,
      0 AS PlannedExpCost,
      0 AS PlannedExpBill,
      0 AS PlannedDirExpCost,
      0 AS PlannedDirExpBill,
      CZ.PlannedConCost AS PlannedConCost,
      CZ.PlannedConBill AS PlannedConBill,
      CZ.PlannedDirConCost AS PlannedDirConCost,
      CZ.PlannedDirConBill AS PlannedDirConBill,
      0 AS PlannedUntQty,
      0 AS PlannedUntCost,
      0 AS PlannedUntBill,
      0 AS PlannedDirUntCost,
      0 AS PlannedDirUntBill,
      0 AS BaselineLaborHrs,
      0 AS BaselineLabCost,
      0 AS BaselineLabBill,
      0 AS BaselineExpCost,
      0 AS BaselineExpBill,
      0 AS BaselineDirExpCost,
      0 AS BaselineDirExpBill,
      CZ.BaselineConCost AS BaselineConCost,
      CZ.BaselineConBill AS BaselineConBill,
      CZ.BaselineDirConCost AS BaselineDirConCost,
      CZ.BaselineDirConBill AS BaselineDirConBill,
      0 AS BaselineUntQty,
      0 AS BaselineUntCost,
      0 AS BaselineUntBill,
      0 AS BaselineDirUntCost,
      0 AS BaselineDirUntBill,
      0 AS WeightedLabCost,
      0 AS WeightedLabBill,
      0 AS WeightedExpCost,
      0 AS WeightedExpBill,
      CZ.WeightedConCost AS WeightedConCost,
      CZ.WeightedConBill AS WeightedConBill,
      0 AS WeightedUntCost,
      0 AS WeightedUntBill
      FROM 
        (SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
         C.PlanID AS PlanID,
         C.ConsultantID AS TaskID,
         C.TaskID AS ParentTaskID, -- This is the TaskID of the parent row.
         PT.OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the parent row.
         PT.OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the parent row.
         C.WBS1 AS WBS1,
         C.WBS2 AS WBS2,
         C.WBS3 AS WBS3,
         C.Account AS Account,
         C.Vendor AS Vendor,
         C.PlannedConCost AS PlannedConCost,
         C.PlannedConBill AS PlannedConBill,
         C.PlannedDirConCost AS PlannedDirConCost,
         C.PlannedDirConBill AS PlannedDirConBill,
         C.BaselineConCost AS BaselineConCost,
         C.BaselineConBill AS BaselineConBill,
         C.BaselineDirConCost AS BaselineDirConCost,
         C.BaselineDirConBill AS BaselineDirConBill,
         C.WeightedConCost AS WeightedConCost,
         C.WeightedConBill AS WeightedConBill,
         ROW_NUMBER() OVER (PARTITION BY C.PlanID, PT.ParentOutlineNumber, C.WBS1, C.WBS2, C.WBS3 ORDER BY C.PlanID, PT.ParentOutlineNumber, C.Account) AS RowID		 
           FROM RPConsultant AS C
             INNER JOIN RPTask AS PT ON C.PlanID = PT.PlanID AND C.TaskID = PT.TaskID			 
        ) AS CZ
        LEFT JOIN CA ON CZ.Account = CA.Account
        LEFT JOIN VE ON CZ.Vendor = VE.Vendor

  --CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
  --C C2. Insert detail rows for Account and Vendor combination from Ledger tables into @tabTask.                                                                  C
  --CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

  INSERT INTO @tabTask -->>> C2. Insert detail rows for Account and Vendor combination from Ledger tables into @tabTask.
    (PlanID,
     TaskID,
     ParentTaskID,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     Employee,
     LaborCategory,
     Account,
     Vendor,
     Unit,
     UnitTable,
     TaskName,
     OutlineNumber,
     OutlineLevel,
     WBSType,
     WBSTypeDesc,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     PlannedExpCost,
     PlannedExpBill,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PlannedConCost,
     PlannedConBill,
     PlannedDirConCost,
     PlannedDirConBill,
     PlannedUntQty,
     PlannedUntCost,
     PlannedUntBill,
     PlannedDirUntCost,
     PlannedDirUntBill,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineExpCost,
     BaselineExpBill,
     BaselineDirExpCost,
     BaselineDirExpBill,
     BaselineConCost,
     BaselineConBill,
     BaselineDirConCost,
     BaselineDirConBill,
     BaselineUntQty,
     BaselineUntCost,
     BaselineUntBill,
     BaselineDirUntCost,
     BaselineDirUntBill,
     WeightedLabCost,
     WeightedLabBill,
     WeightedExpCost,
     WeightedExpBill,
     WeightedConCost,
     WeightedConBill,
     WeightedUntCost,
     WeightedUntBill
    )
    SELECT
      CZ.PlanID AS PlanID,
      CZ.TaskID AS TaskID,
      CZ.ParentTaskID AS ParentTaskID,
      CONVERT(NVARCHAR(30), CZ.WBS1) AS WBS1,
      CONVERT(NVARCHAR(30), ISNULL(CZ.WBS2, ' ')) AS WBS2,
      CONVERT(NVARCHAR(30), ISNULL(CZ.WBS3, ' ')) AS WBS3,
      CONVERT(NVARCHAR(14), '<empty>') AS LaborCode,
      CONVERT(NVARCHAR(20), '<empty>') AS Employee,
      -1 AS LaborCategory,
      CONVERT(NVARCHAR(13), ISNULL(CZ.Account, '<empty>')) AS Account,
      CONVERT(NVARCHAR(20), ISNULL(CZ.Vendor, '<empty>')) AS Vendor,
      CONVERT(NVARCHAR(30), '<empty>') AS Unit,
      CONVERT(NVARCHAR(30), '<empty>') AS UnitTable,
      CONVERT(NVARCHAR(255), '*' + ISNULL(CZ.Account + ' - ' + CZ.AccountName, '<empty>') +
        CASE WHEN CZ.Vendor IS NOT NULL THEN ' (' + ISNULL(CZ.Vendor, '') + ' - ' + ISNULL(VE.Name, '<empty>') + ')' ELSE '' END) AS TaskName,
      CZ.ParentOutlineNumber + '.C8' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(CZ.RowID, 36)), 3) AS OutlineNumber, -- Needed for sorting the leaf tasks.
      CZ.ParentOutlineLevel + 1 AS OutlineLevel,
      CONVERT(VARCHAR(4), 'CAVE') AS WBSType,
      CONVERT(NVARCHAR(50), ' ') AS WBSTypeDesc,
      0 AS PlannedLaborHrs,
      0 AS PlannedLabCost,
      0 AS PlannedLabBill,
      0 AS PlannedExpCost,
      0 AS PlannedExpBill,
      0 AS PlannedDirExpCost,
      0 AS PlannedDirExpBill,
      0 AS PlannedConCost,
      0 AS PlannedConBill,
      0 AS PlannedDirConCost,
      0 AS PlannedDirConBill,
      0 AS PlannedUntQty,
      0 AS PlannedUntCost,
      0 AS PlannedUntBill,
      0 AS PlannedDirUntCost,
      0 AS PlannedDirUntBill,
      0 AS BaselineLaborHrs,
      0 AS BaselineLabCost,
      0 AS BaselineLabBill,
      0 AS BaselineExpCost,
      0 AS BaselineExpBill,
      0 AS BaselineDirExpCost,
      0 AS BaselineDirExpBill,
      0 AS BaselineConCost,
      0 AS BaselineConBill,
      0 AS BaselineDirConCost,
      0 AS BaselineDirConBill,
      0 AS BaselineUntQty,
      0 AS BaselineUntCost,
      0 AS BaselineUntBill,
      0 AS BaselineDirUntCost,
      0 AS BaselineDirUntBill,
      0 AS WeightedLabCost,
      0 AS WeightedLabBill,
      0 AS WeightedExpCost,
      0 AS WeightedExpBill,
      0 AS WeightedConCost,
      0 AS WeightedConBill,
      0 AS WeightedUntCost,
      0 AS WeightedUntBill
      FROM 
        (SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
           PlanID AS PlanID,
           (SELECT REPLACE(CAST(NEWID AS VARCHAR(36)), '-', '') FROM dbo.NewID) AS TaskID, -- Compute TaskID for the new row here so that the id can be propagated downward.
           TaskID AS ParentTaskID, -- This is the TaskID of the parent row.
           OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the parent row.
           OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the parent row.
           WBS1 AS WBS1,
           WBS2 AS WBS2,
           WBS3 AS WBS3,
           Account AS Account,
           AccountName AS AccountName,
           Vendor AS Vendor,
           ROW_NUMBER() OVER (PARTITION BY PlanID, OutlineNumber, WBS1, WBS2, WBS3 ORDER BY PlanID, OutlineNumber, Account, Vendor) AS RowID
           FROM
             (SELECT DISTINCT
                PT.PlanID AS PlanID,
                PT.TaskID AS TaskID,
                PT.OutlineNumber AS OutlineNumber,
                PT.OutlineLevel AS OutlineLevel,
                PT.WBS1 AS WBS1,
                PT.WBS2 AS WBS2,
                PT.WBS3 AS WBS3,
                LD.Account AS Account,
                LD.AccountName AS AccountName,
                LD.Vendor AS Vendor
                FROM
                  (SELECT DISTINCT LDX.WBS1, LDX.WBS2, LDX.WBS3, LDX.Account, LDX.Vendor, CA.Name AS AccountName 
                     FROM LedgerAR AS LDX INNER JOIN CA ON LDX.Account = CA.Account AND CA.Type IN (6, 8) AND ProjectCost = 'Y' AND TransType != 'UN' AND Employee IS NULL
                   UNION
                   SELECT DISTINCT LDX.WBS1, LDX.WBS2, LDX.WBS3, LDX.Account, LDX.Vendor, CA.Name AS AccountName  
                     FROM LedgerAP AS LDX INNER JOIN CA ON LDX.Account = CA.Account AND CA.Type IN (6, 8) AND ProjectCost = 'Y' AND TransType != 'UN' AND Employee IS NULL
                   UNION
                   SELECT DISTINCT LDX.WBS1, LDX.WBS2, LDX.WBS3, LDX.Account, LDX.Vendor, CA.Name AS AccountName  
                     FROM LedgerEX AS LDX INNER JOIN CA ON LDX.Account = CA.Account AND CA.Type IN (6, 8) AND ProjectCost = 'Y' AND TransType != 'UN' AND Employee IS NULL
                   UNION
                   SELECT DISTINCT LDX.WBS1, LDX.WBS2, LDX.WBS3, LDX.Account, LDX.Vendor, CA.Name AS AccountName  
                     FROM LedgerMISC AS LDX INNER JOIN CA ON LDX.Account = CA.Account AND CA.Type IN (6, 8) AND ProjectCost = 'Y' AND TransType != 'UN' AND Employee IS NULL
                  ) AS LD
                  INNER JOIN 
                    (SELECT
                       PlanID AS PlanID,
                       TaskID AS TaskID,
                       WBS1 AS WBS1,
                       WBS2 AS WBS2,
                       WBS3 AS WBS3,
                       OutlineNumber AS OutlineNumber,
                       OutlineLevel AS OutlineLevel,
                       WBSType AS WBSType,
					   Operation
                       FROM @tabWBSTree WHERE WBSType IN ('WBS1', 'WBS2', 'WBS3')
                    ) AS PT ON LD.WBS1 = PT.WBS1
                      AND LD.WBS2 = PT.WBS2
                      AND LD.WBS3 = PT.WBS3
                      AND PT.WBSType = CASE WHEN LD.WBS1 != ' ' AND LD.WBS2 = ' ' AND LD.WBS3 = ' ' THEN 'WBS1'
                                            WHEN LD.WBS1 != ' ' AND LD.WBS2 != ' ' AND LD.WBS3 = ' ' THEN 'WBS2'
                                            WHEN LD.WBS1 != ' ' AND LD.WBS2 != ' ' AND LD.WBS3 != ' ' THEN 'WBS3' END
                  LEFT JOIN RPConsultant AS C
                    ON PT.PlanID = C.PlanID 
                      AND LD.WBS1 = C.WBS1
                      AND LD.WBS2 = ISNULL(C.WBS2, ' ')
                      AND LD.WBS3 = ISNULL(C.WBS3, ' ')
                      AND LD.Account = C.Account
                      AND ISNULL(LD.Vendor, -999) = ISNULL(C.Vendor, -999)
                WHERE C.ConsultantID IS NULL
             ) AS CY
        ) AS CZ
        LEFT JOIN VE ON CZ.Vendor = VE.Vendor
        
  --CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
  --C C3. Insert detail rows for Account and Employee combination from Ledger tables into @tabTask.                                                                C
  --CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

  INSERT INTO @tabTask -->>> C3. Insert detail rows for Account and Employee combination from Ledger tables into @tabTask.
    (PlanID,
     TaskID,
     ParentTaskID,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     Employee,
     LaborCategory,
     Account,
     Vendor,
     Unit,
     UnitTable,
     TaskName,
     OutlineNumber,
     OutlineLevel,
     WBSType,
     WBSTypeDesc,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     PlannedExpCost,
     PlannedExpBill,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PlannedConCost,
     PlannedConBill,
     PlannedDirConCost,
     PlannedDirConBill,
     PlannedUntQty,
     PlannedUntCost,
     PlannedUntBill,
     PlannedDirUntCost,
     PlannedDirUntBill,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineExpCost,
     BaselineExpBill,
     BaselineDirExpCost,
     BaselineDirExpBill,
     BaselineConCost,
     BaselineConBill,
     BaselineDirConCost,
     BaselineDirConBill,
     BaselineUntQty,
     BaselineUntCost,
     BaselineUntBill,
     BaselineDirUntCost,
     BaselineDirUntBill,
     WeightedLabCost,
     WeightedLabBill,
     WeightedExpCost,
     WeightedExpBill,
     WeightedConCost,
     WeightedConBill,
     WeightedUntCost,
     WeightedUntBill
    )
    SELECT
      CZ.PlanID AS PlanID,
      CZ.TaskID AS TaskID,
      CZ.ParentTaskID AS ParentTaskID,
      CONVERT(NVARCHAR(30), CZ.WBS1) AS WBS1,
      CONVERT(NVARCHAR(30), ISNULL(CZ.WBS2, ' ')) AS WBS2,
      CONVERT(NVARCHAR(30), ISNULL(CZ.WBS3, ' ')) AS WBS3,
      CONVERT(NVARCHAR(14), '<empty>') AS LaborCode,
      CONVERT(NVARCHAR(20), ISNULL(CZ.Employee, '<empty>')) AS Employee,
      -1 AS LaborCategory,
      CONVERT(NVARCHAR(13), ISNULL(CZ.Account, '<empty>')) AS Account,
      CONVERT(NVARCHAR(20), '<empty>') AS Vendor,
      CONVERT(NVARCHAR(30), '<empty>') AS Unit,
      CONVERT(NVARCHAR(30), '<empty>') AS UnitTable,
      CONVERT(NVARCHAR(255), '+' + ISNULL(CZ.Account + ' - ' + CZ.AccountName, '<empty>') +
        ' (' + ISNULL(EM.Employee, '') + ' :: ' + ISNULL(EM.FirstName, '') + ISNULL(' ' + LEFT(EM.MiddleName, 1), '') + ISNULL(' ' + EM.LastName, '') + ')') AS TaskName,
      CZ.ParentOutlineNumber + '.B9' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(CZ.RowID, 36)), 3) AS OutlineNumber, -- Needed for sorting the leaf tasks.
      CZ.ParentOutlineLevel + 1 AS OutlineLevel,
      CONVERT(VARCHAR(4), 'CAVE') AS WBSType,
      CONVERT(NVARCHAR(50), ' ') AS WBSTypeDesc,
      0 AS PlannedLaborHrs,
      0 AS PlannedLabCost,
      0 AS PlannedLabBill,
      0 AS PlannedExpCost,
      0 AS PlannedExpBill,
      0 AS PlannedDirExpCost,
      0 AS PlannedDirExpBill,
      0 AS PlannedConCost,
      0 AS PlannedConBill,
      0 AS PlannedDirConCost,
      0 AS PlannedDirConBill,
      0 AS PlannedUntQty,
      0 AS PlannedUntCost,
      0 AS PlannedUntBill,
      0 AS PlannedDirUntCost,
      0 AS PlannedDirUntBill,
      0 AS BaselineLaborHrs,
      0 AS BaselineLabCost,
      0 AS BaselineLabBill,
      0 AS BaselineExpCost,
      0 AS BaselineExpBill,
      0 AS BaselineDirExpCost,
      0 AS BaselineDirExpBill,
      0 AS BaselineConCost,
      0 AS BaselineConBill,
      0 AS BaselineDirConCost,
      0 AS BaselineDirConBill,
      0 AS BaselineUntQty,
      0 AS BaselineUntCost,
      0 AS BaselineUntBill,
      0 AS BaselineDirUntCost,
      0 AS BaselineDirUntBill,
      0 AS WeightedLabCost,
      0 AS WeightedLabBill,
      0 AS WeightedExpCost,
      0 AS WeightedExpBill,
      0 AS WeightedConCost,
      0 AS WeightedConBill,
      0 AS WeightedUntCost,
      0 AS WeightedUntBill
      FROM 
        (SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
           PlanID AS PlanID,
           (SELECT REPLACE(CAST(NEWID AS VARCHAR(36)), '-', '') FROM dbo.NewID) AS TaskID, -- Compute TaskID for the new row here so that the id can be propagated downward.
           TaskID AS ParentTaskID, -- This is the TaskID of the parent row.
           OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the parent row.
           OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the parent row.
           WBS1 AS WBS1,
           WBS2 AS WBS2,
           WBS3 AS WBS3,
           Account AS Account,
           AccountName AS AccountName,
           Employee AS Employee,
           ROW_NUMBER() OVER (PARTITION BY PlanID, OutlineNumber, WBS1, WBS2, WBS3 ORDER BY PlanID, OutlineNumber, Account, Employee) AS RowID
           FROM
             (SELECT DISTINCT
                PT.PlanID AS PlanID,
                PT.TaskID AS TaskID,
                PT.OutlineNumber AS OutlineNumber,
                PT.OutlineLevel AS OutlineLevel,
                PT.WBS1 AS WBS1,
                PT.WBS2 AS WBS2,
                PT.WBS3 AS WBS3,
                LD.Account AS Account,
                LD.AccountName AS AccountName,
                LD.Employee AS Employee
                FROM
                  (SELECT DISTINCT LDX.WBS1, LDX.WBS2, LDX.WBS3, LDX.Account, LDX.Employee, CA.Name AS AccountName 
                     FROM LedgerAR AS LDX INNER JOIN CA ON LDX.Account = CA.Account AND CA.Type IN (6, 8) AND ProjectCost = 'Y' AND TransType != 'UN' AND Employee IS NOT NULL
                   UNION
                   SELECT DISTINCT LDX.WBS1, LDX.WBS2, LDX.WBS3, LDX.Account, LDX.Employee, CA.Name AS AccountName  
                     FROM LedgerAP AS LDX INNER JOIN CA ON LDX.Account = CA.Account AND CA.Type IN (6, 8) AND ProjectCost = 'Y' AND TransType != 'UN' AND Employee IS NOT NULL
                   UNION
                   SELECT DISTINCT LDX.WBS1, LDX.WBS2, LDX.WBS3, LDX.Account, LDX.Employee, CA.Name AS AccountName  
                     FROM LedgerEX AS LDX INNER JOIN CA ON LDX.Account = CA.Account AND CA.Type IN (6, 8) AND ProjectCost = 'Y' AND TransType != 'UN' AND Employee IS NOT NULL
                   UNION
                   SELECT DISTINCT LDX.WBS1, LDX.WBS2, LDX.WBS3, LDX.Account, LDX.Employee, CA.Name AS AccountName  
                     FROM LedgerMISC AS LDX INNER JOIN CA ON LDX.Account = CA.Account AND CA.Type IN (6, 8) AND ProjectCost = 'Y' AND TransType != 'UN' AND Employee IS NOT NULL
                  ) AS LD
                  INNER JOIN 
                    (SELECT
                       PlanID AS PlanID,
                       TaskID AS TaskID,
                       WBS1 AS WBS1,
                       WBS2 AS WBS2,
                       WBS3 AS WBS3,
                       OutlineNumber AS OutlineNumber,
                       OutlineLevel AS OutlineLevel,
                       WBSType AS WBSType
                       FROM @tabWBSTree WHERE WBSType IN ('WBS1', 'WBS2', 'WBS3')
                    ) AS PT ON LD.WBS1 = PT.WBS1
                      AND LD.WBS2 = PT.WBS2
                      AND LD.WBS3 = PT.WBS3
                      AND PT.WBSType = CASE WHEN LD.WBS1 != ' ' AND LD.WBS2 = ' ' AND LD.WBS3 = ' ' THEN 'WBS1'
                                            WHEN LD.WBS1 != ' ' AND LD.WBS2 != ' ' AND LD.WBS3 = ' ' THEN 'WBS2'
                                            WHEN LD.WBS1 != ' ' AND LD.WBS2 != ' ' AND LD.WBS3 != ' ' THEN 'WBS3' END
             ) AS CY
        ) AS CZ
        LEFT JOIN EM ON CZ.Employee = EM.Employee

--UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
--U Unit.                                                                                                                                                          U
--UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

  --UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
  --U U1. Insert records from RPUnit into @tabTask.                                                                                                                U
  --UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

  INSERT INTO @tabTask -->>> U1. Insert records from RPUnit into @tabTask.
    (PlanID,
     TaskID,
     ParentTaskID,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     Employee,
     LaborCategory,
     Account,
     Vendor,
     Unit,
     UnitTable,
     TaskName,
     OutlineNumber,
     OutlineLevel,
     WBSType,
     WBSTypeDesc,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     PlannedExpCost,
     PlannedExpBill,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PlannedConCost,
     PlannedConBill,
     PlannedDirConCost,
     PlannedDirConBill,
     PlannedUntQty,
     PlannedUntCost,
     PlannedUntBill,
     PlannedDirUntCost,
     PlannedDirUntBill,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineExpCost,
     BaselineExpBill,
     BaselineDirExpCost,
     BaselineDirExpBill,
     BaselineConCost,
     BaselineConBill,
     BaselineDirConCost,
     BaselineDirConBill,
     BaselineUntQty,
     BaselineUntCost,
     BaselineUntBill,
     BaselineDirUntCost,
     BaselineDirUntBill,
     WeightedLabCost,
     WeightedLabBill,
     WeightedExpCost,
     WeightedExpBill,
     WeightedConCost,
     WeightedConBill,
     WeightedUntCost,
     WeightedUntBill
    )
    SELECT
      UZ.PlanID AS PlanID,
      UZ.TaskID AS TaskID,
      UZ.ParentTaskID AS ParentTaskID,
      CONVERT(NVARCHAR(30), UZ.WBS1) AS WBS1,
      CONVERT(NVARCHAR(30), ISNULL(UZ.WBS2, ' ')) AS WBS2,
      CONVERT(NVARCHAR(30), ISNULL(UZ.WBS3, ' ')) AS WBS3,
      CONVERT(NVARCHAR(14), '<empty>') AS LaborCode,
      CONVERT(NVARCHAR(20), '<empty>') AS Employee,
      -1 AS LaborCategory,
      CONVERT(NVARCHAR(13), ISNULL(UZ.Account, '<empty>')) AS Account,
      CONVERT(NVARCHAR(20), '<empty>') AS Vendor,
      CONVERT(NVARCHAR(30), ISNULL(UZ.Unit, '<empty>')) AS Unit,
      CONVERT(NVARCHAR(30), ISNULL(UZ.UnitTable, '<empty>')) AS UnitTable,
      CONVERT(NVARCHAR(255), ISNULL(UN.Unit + ' - ' + UN.Name, '<empty>') +
        CASE WHEN UZ.Account IS NOT NULL THEN ' (' + ISNULL(CA.Account + ' - ' + CA.Name, '<empty>') + ')' ELSE '' END) AS TaskName,
      UZ.ParentOutlineNumber + '.D' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(UZ.SortSeq, 36)), 3) AS OutlineNumber, -- Needed for sorting the leaf tasks.
      UZ.ParentOutlineLevel + 1 AS OutlineLevel,
      CONVERT(VARCHAR(4), 'UNT') AS WBSType,
      CONVERT(NVARCHAR(50), ' ') AS WBSTypeDesc,
      0 AS PlannedLaborHrs,
      0 AS PlannedLabCost,
      0 AS PlannedLabBill,
      0 AS PlannedExpCost,
      0 AS PlannedExpBill,
      0 AS PlannedDirExpCost,
      0 AS PlannedDirExpBill,
      0 AS PlannedConCost,
      0 AS PlannedConBill,
      0 AS PlannedDirConCost,
      0 AS PlannedDirConBill,
      UZ.PlannedUntQty AS PlannedUntQty,
      UZ.PlannedUntCost AS PlannedUntCost,
      UZ.PlannedUntBill AS PlannedUntBill,
      UZ.PlannedDirUntCost AS PlannedDirUntCost,
      UZ.PlannedDirUntBill AS PlannedDirUntBill,
      0 AS BaselineLaborHrs,
      0 AS BaselineLabCost,
      0 AS BaselineLabBill,
      0 AS BaselineExpCost,
      0 AS BaselineExpBill,
      0 AS BaselineDirExpCost,
      0 AS BaselineDirExpBill,
      0 AS BaselineConCost,
      0 AS BaselineConBill,
      0 AS BaselineDirConCost,
      0 AS BaselineDirConBill,
      UZ.BaselineUntQty AS BaselineUntQty,
      UZ.BaselineUntCost AS BaselineUntCost,
      UZ.BaselineUntBill AS BaselineUntBill,
      UZ.BaselineDirUntCost AS BaselineDirUntCost,
      UZ.BaselineDirUntBill AS BaselineDirUntBill,
      0 AS WeightedLabCost,
      0 AS WeightedLabBill,
      0 AS WeightedExpCost,
      0 AS WeightedExpBill,
      0 AS WeightedConCost,
      0 AS WeightedConBill,
      UZ.WeightedUntCost AS WeightedUntCost,
      UZ.WeightedUntBill AS WeightedUntBill
      FROM 
        (SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
           U.PlanID AS PlanID,
           U.UnitID AS TaskID,
           U.TaskID AS ParentTaskID, -- This is the TaskID of the parent row.
           PT.OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the parent row.
           PT.OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the parent row.
           U.WBS1 AS WBS1,
           U.WBS2 AS WBS2,
           U.WBS3 AS WBS3,
           U.Unit AS Unit,
           U.UnitTable AS UnitTable,
           U.Account AS Account,
           P.Company AS Company,
           U.PlannedUntQty AS PlannedUntQty,
           U.PlannedUntCost AS PlannedUntCost,
           U.PlannedUntBill AS PlannedUntBill,
           U.PlannedDirUntCost AS PlannedDirUntCost,
           U.PlannedDirUntBill AS PlannedDirUntBill,
           U.BaselineUntQty AS BaselineUntQty,
           U.BaselineUntCost AS BaselineUntCost,
           U.BaselineUntBill AS BaselineUntBill,
           U.BaselineDirUntCost AS BaselineDirUntCost,
           U.BaselineDirUntBill AS BaselineDirUntBill,
           U.WeightedUntCost AS WeightedUntCost,
           U.WeightedUntBill AS WeightedUntBill,
           U.SortSeq		  
           FROM RPUnit AS U
             INNER JOIN RPPlan AS P ON U.PlanID = P.PlanID
             INNER JOIN RPTask AS PT ON U.PlanID = PT.PlanID AND U.TaskID = PT.TaskID			 
        ) AS UZ
        LEFT JOIN CA ON UZ.Account = CA.Account
        LEFT JOIN UN ON UZ.Unit = UN.Unit AND UZ.UnitTable = UN.UnitTable AND UN.Company = UZ.Company

  --UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
  --U U2. Insert unmatched records from LedgerMISC into @tabTask.                                                                                                  U
  --UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU

  INSERT INTO @tabTask -->>> U2. Insert unmatched records from LedgerMISC into @tabTask. 
    (PlanID,
     TaskID,
     ParentTaskID,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     Employee,
     LaborCategory,
     Account,
     Vendor,
     Unit,
     UnitTable,
     TaskName,
     OutlineNumber,
     OutlineLevel,
     WBSType,
     WBSTypeDesc,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     PlannedExpCost,
     PlannedExpBill,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PlannedConCost,
     PlannedConBill,
     PlannedDirConCost,
     PlannedDirConBill,
     PlannedUntQty,
     PlannedUntCost,
     PlannedUntBill,
     PlannedDirUntCost,
     PlannedDirUntBill,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineExpCost,
     BaselineExpBill,
     BaselineDirExpCost,
     BaselineDirExpBill,
     BaselineConCost,
     BaselineConBill,
     BaselineDirConCost,
     BaselineDirConBill,
     BaselineUntQty,
     BaselineUntCost,
     BaselineUntBill,
     BaselineDirUntCost,
     BaselineDirUntBill,
     WeightedLabCost,
     WeightedLabBill,
     WeightedExpCost,
     WeightedExpBill,
     WeightedConCost,
     WeightedConBill,
     WeightedUntCost,
     WeightedUntBill
    )
    SELECT
      UZ.PlanID AS PlanID,
      UZ.TaskID AS TaskID,
      UZ.ParentTaskID AS ParentTaskID,
      CONVERT(NVARCHAR(30), UZ.WBS1) AS WBS1,
      CONVERT(NVARCHAR(30), ISNULL(UZ.WBS2, ' ')) AS WBS2,
      CONVERT(NVARCHAR(30), ISNULL(UZ.WBS3, ' ')) AS WBS3,
      CONVERT(NVARCHAR(14), '<empty>') AS LaborCode,
      CONVERT(NVARCHAR(20), ISNULL(UZ.Employee, '<empty>')) AS Employee,
      -1 AS LaborCategory,
      CONVERT(NVARCHAR(13), ISNULL(UZ.Account, '<empty>')) AS Account,
      CONVERT(NVARCHAR(20), '<empty>') AS Vendor,
      CONVERT(NVARCHAR(30), ISNULL(UZ.Unit, '<empty>')) AS Unit,
      CONVERT(NVARCHAR(30), ISNULL(UZ.UnitTable, '<empty>')) AS UnitTable,
      CONVERT(NVARCHAR(255), '*' + ISNULL(UZ.Unit + ' - ' + UZ.UnitName, '<empty>') +
      CASE WHEN UZ.Account IS NOT NULL THEN ' (' + ISNULL(UZ.Account + ' - ' + UZ.AccountName, '<empty>') + ')' ELSE '' END) AS TaskName,
      UZ.ParentOutlineNumber + '.D9' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(UZ.RowID, 36)), 3) AS OutlineNumber, -- Needed for sorting the leaf tasks.
      UZ.ParentOutlineLevel + 1 AS OutlineLevel,
      CONVERT(VARCHAR(4), 'UNT') AS WBSType,
      CONVERT(NVARCHAR(50), ' ') AS WBSTypeDesc,
      0 AS PlannedLaborHrs,
      0 AS PlannedLabCost,
      0 AS PlannedLabBill,
      0 AS PlannedExpCost,
      0 AS PlannedExpBill,
      0 AS PlannedDirExpCost,
      0 AS PlannedDirExpBill,
      0 AS PlannedConCost,
      0 AS PlannedConBill,
      0 AS PlannedDirConCost,
      0 AS PlannedDirConBill,
      0 AS PlannedUntQty,
      0 AS PlannedUntCost,
      0 AS PlannedUntBill,
      0 AS PlannedDirUntCost,
      0 AS PlannedDirUntBill,
      0 AS BaselineLaborHrs,
      0 AS BaselineLabCost,
      0 AS BaselineLabBill,
      0 AS BaselineExpCost,
      0 AS BaselineExpBill,
      0 AS BaselineDirExpCost,
      0 AS BaselineDirExpBill,
      0 AS BaselineConCost,
      0 AS BaselineConBill,
      0 AS BaselineDirConCost,
      0 AS BaselineDirConBill,
      0 AS BaselineUntQty,
      0 AS BaselineUntCost,
      0 AS BaselineUntBill,
      0 AS BaselineDirUntCost,
      0 AS BaselineDirUntBill,
      0 AS WeightedLabCost,
      0 AS WeightedLabBill,
      0 AS WeightedExpCost,
      0 AS WeightedExpBill,
      0 AS WeightedConCost,
      0 AS WeightedConBill,
      0 AS WeightedUntCost,
      0 AS WeightedUntBill
      FROM
        (SELECT -- Need to have this outer SELECT so that the ROW_NUMBER can aggegrate over the DISTINCT.
           PlanID AS PlanID,
           (SELECT REPLACE(CAST(NEWID AS VARCHAR(36)), '-', '') FROM dbo.NewID) AS TaskID, -- Compute TaskID for the new row here so that the id can be propagated downward.
           TaskID AS ParentTaskID, -- This is the TaskID of the parent row.
           OutlineNumber AS ParentOutlineNumber, -- This is the OutlineNumber of the parent row.
           OutlineLevel AS ParentOutlineLevel, -- This is the OutlineLevel of the parent row.
           WBS1 AS WBS1,
           WBS2 AS WBS2,
           WBS3 AS WBS3,
           Account AS Account,
           Employee AS Employee,
           Unit AS Unit,
           UnitTable AS UnitTable,
           AccountName AS AccountName,
           UnitName AS UnitName,
           ROW_NUMBER() OVER (PARTITION BY PlanID, OutlineNumber, WBS1, WBS2, WBS3 ORDER BY PlanID, OutlineNumber) AS RowID
           FROM
             (SELECT DISTINCT
                PT.PlanID AS PlanID,
                PT.TaskID AS TaskID,
                PT.OutlineNumber AS OutlineNumber,
                PT.OutlineLevel AS OutlineLevel,
                PT.WBS1 AS WBS1,
                PT.WBS2 AS WBS2,
                PT.WBS3 AS WBS3,
                LD.Account AS Account,
                LD.Employee AS Employee,
                LD.Unit AS Unit,
                LD.UnitTable AS UnitTable,
                LD.AccountName AS AccountName,
                LD.UnitName AS UnitName
                FROM
                  (SELECT DISTINCT 
                     LDX.WBS1, 
                     LDX.WBS2, 
                     LDX.WBS3, 
                     LDX.Account, 
                     LDX.Employee, 
                     LDX.Unit, 
                     LDX.UnitTable,
                     CA.Name AS AccountName,
                     UN.Name AS UnitName
                     FROM LedgerMISC AS LDX
                     INNER JOIN CA ON LDX.Account = CA.Account
                     INNER JOIN UN ON LDX.Unit = UN.Unit AND LDX.UnitTable = UN.UnitTable 
                     WHERE LDX.ProjectCost = 'Y' AND LDX.TransType = 'UN' AND LDX.Unit IS NOT NULL AND LDX.UnitTable IS NOT NULL
                  ) AS LD
                  INNER JOIN 
                    (SELECT
                       PlanID AS PlanID,
                       TaskID AS TaskID,
                       WBS1 AS WBS1,
                       WBS2 AS WBS2,
                       WBS3 AS WBS3,
                       OutlineNumber AS OutlineNumber,
                       OutlineLevel AS OutlineLevel,
                       WBSType AS WBSType
                       FROM @tabWBSTree WHERE WBSType IN ('WBS1', 'WBS2', 'WBS3')
                    ) AS PT ON LD.WBS1 = PT.WBS1
                      AND LD.WBS2 = PT.WBS2
                      AND LD.WBS3 = PT.WBS3
                      AND PT.WBSType = CASE WHEN LD.WBS1 != ' ' AND LD.WBS2 = ' ' AND LD.WBS3 = ' ' THEN 'WBS1'
                                            WHEN LD.WBS1 != ' ' AND LD.WBS2 != ' ' AND LD.WBS3 = ' ' THEN 'WBS2'
                                            WHEN LD.WBS1 != ' ' AND LD.WBS2 != ' ' AND LD.WBS3 != ' ' THEN 'WBS3' END
                  LEFT JOIN RPUnit AS U
                    ON PT.PlanID = U.PlanID AND PT.TaskID = U.TaskID
                      AND LD.WBS1 = U.WBS1
                      AND LD.WBS2 = ISNULL(U.WBS2, ' ')
                      AND LD.WBS3 = ISNULL(U.WBS3, ' ')
                      AND ISNULL(LD.Account, -999) = ISNULL(U.Account, -999)
                      AND ISNULL(LD.Unit, -999) = ISNULL(U.Unit, -999)
                      AND ISNULL(LD.UnitTable, -999) = ISNULL(U.UnitTable, -999)
                WHERE U.UnitID IS NULL
             ) AS UY
        ) AS UZ         
		LEFT JOIN EM ON UZ.Employee = EM.Employee
                
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        
  RETURN

END -- DW$tabTask

GO
