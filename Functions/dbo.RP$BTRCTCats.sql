SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$BTRCTCats]
  (@intTableNo AS int,
   @sintCategory AS smallint,
   @dtStartDate AS datetime,
   @dtEndDate AS datetime,
   @strEmployee AS Nvarchar(20) = NULL)
  RETURNS float
BEGIN

  DECLARE @strCompany Nvarchar(14)
  DECLARE @fCompositeRate float
  DECLARE @fNWD float
  
  SET @strCompany = dbo.GetActiveCompany()
  SET @fNWD = ABS(dbo.DLTK$NumWorkingDays(@dtStartDate, @dtEndDate, @strCompany))
  SET @fCompositeRate = 0
  
  -- If the @strEmployee parameter is not NULL then compute the overriding Category.
  
  IF (@strEmployee IS NOT NULL)
    BEGIN
      SELECT @sintCategory = ISNULL(OC.Category, EM.BillingCategory)
        FROM EM LEFT JOIN BTRCTEmpls AS OC ON EM.Employee = OC.Employee AND OC.TableNo = @intTableNo
        WHERE EM.Employee = @strEmployee
    END -- If
      
  IF (@fNWD <> 0)
    BEGIN
  
      -- Composite Rate = ((Rate_1 * NWD_1) + (Rate_2 * NWD_2) + ... + (Rate_n * NWD_n)) / NWD_total

      SELECT @fCompositeRate = ISNULL((SUM(ISNULL(Rate_n, 0)) / @fNWD), 0)
        FROM
          (SELECT ISNULL(Rate, 0) * ABS(dbo.DLTK$NumWorkingDays(CASE WHEN @dtStartDate > StartDate THEN @dtStartDate ELSE StartDate END,
                                                                CASE WHEN @dtEndDate < EndDate THEN @dtEndDate ELSE EndDate END,
                                                                @strCompany)) AS Rate_n
             FROM BTRCTCats
             WHERE TableNo = @intTableNo AND Category = @sintCategory AND StartDate <= @dtEndDate AND EndDate >= @dtStartDate) AS X
   
    END -- If-Then
  ELSE
    BEGIN
    
      -- If there is no working day between Start/End date then prorate the Rate using calendar days.

      SELECT @fCompositeRate = ISNULL((SUM(ISNULL(Rate_n, 0)) / (ABS(DATEDIFF(d, @dtStartDate, @dtEndDate)) + 1)), 0)
        FROM
          (SELECT ISNULL(Rate, 0) * (ABS(DATEDIFF(d,
                                                  CASE WHEN @dtStartDate > StartDate THEN @dtStartDate ELSE StartDate END,
                                                  CASE WHEN @dtEndDate < EndDate THEN @dtEndDate ELSE EndDate END)) + 1) AS Rate_n
             FROM BTRCTCats
             WHERE TableNo = @intTableNo AND Category = @sintCategory AND StartDate <= @dtEndDate AND EndDate >= @dtStartDate) AS X
    
    END -- If Else

  RETURN(@fCompositeRate)

END -- fn_RP$BTRCTCats
GO
