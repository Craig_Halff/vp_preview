SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   FUNCTION [dbo].[fnCCG_ReplaceHtmlSpecialChar](@html Nvarchar(max), @htmlChar Nvarchar(10), @normalChar Nvarchar(10))
RETURNS Nvarchar(max)
AS BEGIN
	/* Copyright (c) 2019 EleVia Software and Central Consulting Group.  All rights reserved. */
	declare @Start int, @End int, @Length int

	set @Start = CHARINDEX(@htmlChar, @html)
	set @End = @Start + Len(@htmlChar) - 1
	set @Length = (@End - @Start) + 1
	while (@Start > 0 AND @End > 0 AND @Length > 0)
	begin
		set @html   = STUFF(@html, @Start, @Length, @normalChar)
		set @Start  = CHARINDEX(@htmlChar, @html)
		set @End    = @Start + Len(@htmlChar) - 1
		set @Length = (@End - @Start) + 1
	end
	return @html
END
GO
