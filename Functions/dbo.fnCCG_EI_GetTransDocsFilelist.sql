SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_GetTransDocsFilelist](
	@Username		Nvarchar(32), 
	@CorE			char(1), 
	@Invoice		Nvarchar(12), 
	@WBS1			Nvarchar(30)
)
RETURNS @T TABLE
(
	Seq				int identity(1,1),  /* For updates */
	MenuFolder      varchar(255),       /* Includes cascades - e.g. Billed>2014 */
	MenuLabel       varchar(255),       /* Last menu label after cascades - e.g. Invoice: AJ Carl Engineers 0938 3/19/2013 */
	OnInvoice       char(1),            /* Indicator if the item is on the passed in invoice - e.g. * or blank */
	OrigInvoice     Nvarchar(32),
	Description1	Nvarchar(100),
	Description2	Nvarchar(100),
	FileName        varchar(255),
	TransDate       datetime,
	FileID          uniqueidentifier,
	SourceTable     varchar(32),
	Invoice         Nvarchar(32),
	PayableType     varchar(32),
	TopOrder        smallint	
)
AS BEGIN
/*
	Copyright (c) 2017 Central Consulting Group.  All rights reserved.

	select * from ledgerap where wbs1='2003005.00' and transtype='ap' and transdate='11/30/12'
	select * from VO where vendor='00DREWTILE' and voucher='0000769'
	select * from billcondetail where mainwbs1='2003005.00'  and transdate='11/30/12'

	select * from dbo.fnCCG_EI_GetTransDocsFilelist('ADMIN','C','0001000','2003005.00')
	select * from dbo.fnCCG_EI_GetTransDocsFilelist('ADMIN','E','0001000','2003005.00')
	select * from dbo.fnCCG_EI_GetTransDocsFilelist('ADMIN','E','<Draft>','2003005.00')
*/
	declare @ShowUnbilledOnlyInCEMenus varchar(1)
	select @ShowUnbilledOnlyInCEMenus=ShowUnbilledOnlyInCEMenus from CCG_EI_Config
	if @CorE = 'C'
		insert into @T (MenuFolder,MenuLabel,OnInvoice,OrigInvoice,Description1,Description2,
			FileName,TransDate,FileID,SourceTable,Invoice,PayableType,TopOrder)
		select '', '', Max(Case When IsNull(l.BilledInvoice,N'<Draft>') = @Invoice Then '*' Else ' ' End), '',
			Max(IsNull(l.Desc1,'')), Max(IsNull(l.Desc2,'')), fwf.FileName, Max(l.TransDate), fwf.FileID, 'LedgerAP', Max(l.BilledInvoice),
			'Invoice', 1
		from LedgerAP l
		inner join VO on VO.Vendor=l.Vendor and VO.Voucher=l.Voucher
		inner join CA on CA.Account=l.Account
		inner join LedgerDocuments docs on docs.Period=l.Period and docs.PostSeq=l.PostSeq and docs.PKey=l.PKey and docs.TableName='LedgerAP'
		inner join FW_Files fwf on fwf.FileID=docs.FileID
		left join BTBGSubs subs on subs.SubWBS1=l.WBS1
		where (l.WBS1=@WBS1 or (subs.MainWBS1=@WBS1 and subs.MainWBS1<>subs.SubWBS1)) and (@ShowUnbilledOnlyInCEMenus='N' or l.BilledInvoice is null) and CA.Type in (6,8) and l.BillStatus<>'T' and l.TransType='AP'
		group by VO.Invoice, fwf.FileName, fwf.FileID
		UNION ALL
		select '', '', Max(Case When IsNull(l.BilledInvoice,N'<Draft>') = @Invoice Then '*' Else ' ' End), '',
			Max(IsNull(l.Desc1,N'')), Max(IsNull(l.Desc2,N'')), fwf.FileName, Max(l.TransDate), fwf.FileID, 'LedgerEX', Max(l.BilledInvoice),
			'Invoice', 1
		from LedgerEX l
		inner join CA on CA.Account=l.Account
		inner join LedgerDocuments docs on docs.Period=l.Period and docs.PostSeq=l.PostSeq and docs.PKey=l.PKey and docs.TableName='LedgerEX'
		inner join FW_Files fwf on fwf.FileID=docs.FileID
		left join BTBGSubs subs on subs.SubWBS1=l.WBS1
		where (l.WBS1=@WBS1 or (subs.MainWBS1=@WBS1 and subs.MainWBS1<>subs.SubWBS1)) and (@ShowUnbilledOnlyInCEMenus='N' or l.BilledInvoice is null) and CA.Type in (6,8) and l.BillStatus<>'T'
		group by fwf.FileName, fwf.FileID
		UNION ALL
		select '', '', Max(Case When IsNull(l.BilledInvoice,N'<Draft>') = @Invoice Then '*' Else ' ' End), '',
			Max(IsNull(l.Desc1,N'')), Max(IsNull(l.Desc2,N'')), fwf.FileName, Max(l.TransDate), fwf.FileID, 'LedgerMisc', Max(l.BilledInvoice),
			'Invoice', 1
		from LedgerMisc l
		inner join CA on CA.Account=l.Account
		inner join LedgerDocuments docs on docs.Period=l.Period and docs.PostSeq=l.PostSeq and docs.PKey=l.PKey and docs.TableName='LedgerMisc'
		inner join FW_Files fwf on fwf.FileID=docs.FileID
		left join BTBGSubs subs on subs.SubWBS1=l.WBS1
		left join billConDetail d on d.OriginalPeriod=docs.Period and d.OriginalPostSeq=docs.PostSeq and d.OriginalPKey=docs.PKey
		where (l.WBS1=@WBS1 or (subs.MainWBS1=@WBS1 and subs.MainWBS1<>subs.SubWBS1)) and (@ShowUnbilledOnlyInCEMenus='N' or l.BilledInvoice is null) and CA.Type in (6,8) and l.BillStatus<>'T'
		group by fwf.FileName, fwf.FileID
		UNION ALL
		select '', '', Max(Case When IsNull(l.BilledInvoice,N'<Draft>') = @Invoice Then '*' Else ' ' End), '',
			Max(IsNull(l.Desc1,N'')), Max(IsNull(l.Desc2,N'')), fwf.FileName, Max(l.TransDate), fwf.FileID, 'BIED', Max(l.BilledInvoice),
			'Invoice', 1
		from BIED l
		inner join CA on CA.Account=l.Account
		inner join LedgerDocuments docs on docs.Period=l.Period and docs.PostSeq=l.PostSeq and docs.PKey=l.PKey and docs.TableName='BIED'
		inner join FW_Files fwf on fwf.FileID=docs.FileID
		left join BTBGSubs subs on subs.SubWBS1=l.WBS1
		where (l.WBS1=@WBS1 or (subs.MainWBS1=@WBS1 and subs.MainWBS1<>subs.SubWBS1)) and (@ShowUnbilledOnlyInCEMenus='N' or l.BilledInvoice is null) and CA.Type in (6,8) and l.BillStatus<>'T'
		group by fwf.FileName, fwf.FileID

		order by 8 desc /* TransDate */
	else
		insert into @T (MenuFolder,MenuLabel,OnInvoice,OrigInvoice,Description1,Description2,
			FileName,TransDate,FileID,SourceTable,Invoice,PayableType,TopOrder)
		select '', '', Max(Case When IsNull(l.BilledInvoice,N'<Draft>') = @Invoice Then '*' Else ' ' End), '',
			Max(IsNull(l.Desc1,N'')), Max(IsNull(l.Desc2,N'')), fwf.FileName, Max(l.TransDate), fwf.FileID, 'LedgerAP', Max(l.BilledInvoice),
			'Invoice', 1
		from LedgerAP l
		inner join CA on CA.Account=l.Account
		inner join LedgerDocuments docs on docs.Period=l.Period and docs.PostSeq=l.PostSeq and docs.PKey=l.PKey and docs.TableName='LedgerAP'
		inner join FW_Files fwf on fwf.FileID=docs.FileID
		left join BTBGSubs subs on subs.SubWBS1=l.WBS1
		where (l.WBS1=@WBS1 or (subs.MainWBS1=@WBS1 and subs.MainWBS1<>subs.SubWBS1)) and (@ShowUnbilledOnlyInCEMenus='N' or l.BilledInvoice is null) and CA.Type in (5,7) and l.BillStatus<>'T' and l.TransType='AP'
		group by fwf.FileName, fwf.FileID
		UNION ALL
		select '', '', Max(Case When IsNull(l.BilledInvoice,N'<Draft>') = @Invoice Then '*' Else ' ' End), '',
			Max(IsNull(l.Desc1,N'')), Max(IsNull(l.Desc2,N'')), fwf.FileName, Max(l.TransDate), fwf.FileID, 'LedgerEX', Max(l.BilledInvoice),
			'Invoice', 1
		from LedgerEX l
		inner join CA on CA.Account=l.Account
		inner join LedgerDocuments docs on docs.Period=l.Period and docs.PostSeq=l.PostSeq and docs.PKey=l.PKey and docs.TableName='LedgerEX'
		inner join FW_Files fwf on fwf.FileID=docs.FileID
		left join BTBGSubs subs on subs.SubWBS1=l.WBS1
		where (l.WBS1=@WBS1 or (subs.MainWBS1=@WBS1 and subs.MainWBS1<>subs.SubWBS1)) and (@ShowUnbilledOnlyInCEMenus='N' or l.BilledInvoice is null) and CA.Type in (5,7) and l.BillStatus<>'T'
		group by fwf.FileName, fwf.FileID
		UNION ALL
		select '', '', Max(Case When IsNull(l.BilledInvoice,N'<Draft>') = @Invoice Then '*' Else ' ' End), '',
			Max(IsNull(l.Desc1,N'')), Max(IsNull(l.Desc2,N'')), fwf.FileName, Max(l.TransDate), fwf.FileID, 'LedgerMisc', Max(l.BilledInvoice),
			'Invoice', 1
		from LedgerMisc l
		inner join CA on CA.Account=l.Account
		inner join LedgerDocuments docs on docs.Period=l.Period and docs.PostSeq=l.PostSeq and docs.PKey=l.PKey and docs.TableName='LedgerMisc'
		inner join FW_Files fwf on fwf.FileID=docs.FileID
		left join BTBGSubs subs on subs.SubWBS1=l.WBS1
		where (l.WBS1=@WBS1 or (subs.MainWBS1=@WBS1 and subs.MainWBS1<>subs.SubWBS1)) and (@ShowUnbilledOnlyInCEMenus='N' or l.BilledInvoice is null) and CA.Type in (5,7) and l.BillStatus<>'T'
		group by fwf.FileName, fwf.FileID
		UNION ALL
		select '', '', Max(Case When IsNull(l.BilledInvoice,N'<Draft>') = @Invoice Then '*' Else ' ' End), '',
			Max(IsNull(l.Desc1,N'')), Max(IsNull(l.Desc2,N'')), fwf.FileName, Max(l.TransDate), fwf.FileID, 'BIED', Max(l.BilledInvoice),
			'Invoice', 1
		from BIED l
		inner join CA on CA.Account=l.Account
		inner join LedgerDocuments docs on docs.Period=l.Period and docs.PostSeq=l.PostSeq and docs.PKey=l.PKey and docs.TableName='BIED'
		inner join FW_Files fwf on fwf.FileID=docs.FileID
		left join BTBGSubs subs on subs.SubWBS1=l.WBS1
		where (l.WBS1=@WBS1 or (subs.MainWBS1=@WBS1 and subs.MainWBS1<>subs.SubWBS1)) and (@ShowUnbilledOnlyInCEMenus='N' or l.BilledInvoice is null) and CA.Type in (5,7) and l.BillStatus<>'T'
		group by fwf.FileName, fwf.FileID

		order by 8 desc /* TransDate */

	return
END
GO
