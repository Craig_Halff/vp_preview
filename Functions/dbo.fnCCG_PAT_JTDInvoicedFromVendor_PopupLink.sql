SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_PAT_JTDInvoicedFromVendor_PopupLink] (@PayableSeq int, @WBS1 /*N*/varchar(30), @WBS2 /*N*/varchar(7), @WBS3 /*N*/varchar(7))
RETURNS varchar(500)
AS BEGIN
/*
	Copyright (c) 2021 EleVia Software. All rights reserved.
*/
	return 'value@spCCG_PAT_JTDInvoicedFromVendor_Details_PopupWin'
END
GO
