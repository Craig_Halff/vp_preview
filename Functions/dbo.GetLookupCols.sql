SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetLookupCols] (
	@TableName			VARCHAR(100),
	@ColName			VARCHAR(100),
	@ColDataType		VARCHAR(256)
)
RETURNS @LookupCols TABLE (
	LookupTable		VARCHAR(100),
	LookupValueColumn	VARCHAR(100),
	LookupJoinColumn	VARCHAR(100)
)
AS
BEGIN
	DECLARE
		@LookupTable		VARCHAR(100),
		@LookupValueColumn	VARCHAR(100),
		@LookupJoinColumn	VARCHAR(100)

	IF (@TableName = 'ClientCustomTabFields' AND @ColName = 'ClientID') OR @ColDataType = 'client' BEGIN
		SET @LookupTable = 'CL'
		SET @LookupValueColumn = '{0}.Name'
		SET @LookupJoinColumn = 'ClientID'
	END
	ELSE IF (@TableName = 'ContactCustomTabFields' AND @ColName = 'ContactID') OR @ColDataType = 'contact' BEGIN
		SET @LookupTable = 'Contacts'
		SET @LookupValueColumn = 'ISNULL({0}.LastName, '''') + ISNULL('', '' + {0}.FirstName, '''')'
		SET @LookupJoinColumn = 'ContactID'
	END
	ELSE IF (@TableName = 'EmployeeCustomTabFields' AND @ColName = 'Employee') OR @ColDataType = 'employee' BEGIN
		SET @LookupTable = 'EM'
		SET @LookupValueColumn = 'ISNULL({0}.LastName, '''') + ISNULL('', '' + {0}.FirstName, '''')'
		SET @LookupJoinColumn = 'Employee'
	END
	ELSE IF (@TableName = 'VendorCustomTabFields' AND @ColName = 'Vendor') OR @ColDataType = 'vendor' BEGIN
		SET @LookupTable = 'VE'
		SET @LookupValueColumn = '{0}.Name'
		SET @LookupJoinColumn = 'Vendor'
	END
	ELSE IF @ColDataType = 'wbs1' BEGIN
		SET @LookupTable = 'PR'
		SET @LookupValueColumn = '{0}.Name'
		SET @LookupJoinColumn = 'WBS1'
	END
	ELSE IF @ColDataType = 'account' BEGIN
		SET @LookupTable = 'CA'
		SET @LookupValueColumn = '{0}.Name'
		SET @LookupJoinColumn = 'Account'
	END
	ELSE IF (@TableName = 'MktCampaignCustomTabFields' AND @ColName = 'CampaignID') OR @ColDataType = 'mkt' BEGIN
		SET @LookupTable = 'MktCampaign'
		SET @LookupValueColumn = '{0}.Name'
		SET @LookupJoinColumn = 'CampaignID'
	END
	ELSE IF (@TableName = 'LeadCustomTabFields' AND @ColName = 'LeadID') OR @ColDataType = 'lead' BEGIN
		SET @LookupTable = 'Leads'
		SET @LookupValueColumn = 'ISNULL({0}.LastName, '''') + ISNULL('', '' + {0}.FirstName, '''')'
		SET @LookupJoinColumn = 'LeadID'
	END
	ELSE IF @ColDataType = 'equipment' BEGIN
		SET @LookupTable = 'Equipment'
		SET @LookupValueColumn = 'ISNULL({0}.Item, '''') + ISNULL('', '' + {0}.EquipmentNumber, '''')'
		SET @LookupJoinColumn = 'EquipmentID'
	END
	ELSE IF @ColDataType = 'org' BEGIN
		SET @LookupTable = 'Organization'
		SET @LookupValueColumn = '{0}.Name'
		SET @LookupJoinColumn = 'Org'
	END
	ELSE BEGIN
		SET @LookupTable = ''
	END

	INSERT INTO @LookupCols
	SELECT @LookupTable, @LookupValueColumn, @LookupJoinColumn

	RETURN
END
GO
