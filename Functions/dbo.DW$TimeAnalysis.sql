SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[DW$TimeAnalysis]()
   RETURNS @TimeAnalysis TABLE
    (Employee nvarchar(20) COLLATE database_default,
     StartDate datetime,
     EndDate datetime,
     NumWorkingDays int,
     NumWorkingDaysIncHol int,
	 HoursPerDay decimal(19,4),
	 Rate decimal(19,4)
    )
BEGIN -- Function DW$TimeAnalysis


	declare @strCompany  Nvarchar(14)
	declare @dtStartDate datetime
	declare @dtEndDate datetime
	declare @dtIntervalEnd datetime
	declare @HoursPerDay decimal(19,4)
	declare @Rate decimal(19,4)
	declare @NumWorkingDays integer
	declare @KeysFetch integer
	declare @NumWorkingDaysIncHol integer
     

	declare KeysCursor cursor for
		SELECT min(startDate) as startDate, Case When max(endDate) > '2050-12-31' Then '2050-12-31' Else max(endDate) End as endDate, Company
		FROM
		(
			SELECT myInner.Employee, 
			convert(datetime, convert(varchar(4), CASE WHEN datepart(yyyy,min(startdate)) > '1990' THEN datepart(yyyy, min(startDate)) ELSE '1990' END) + '-01-01') startDate, 
			convert(datetime, convert(varchar(4), datepart(yyyy, CASE WHEN min(startDate) > max(endDate) THEN min(startDate) ELSE max(endDate) END)) + '-12-31') endDate,
			(SELECT CASE WHEN (SELECT MulticompanyEnabled FROM FW_CFGSystem) = 'Y' THEN subString(max(EM.Org),(SELECT org1Start FROM CFGFormat),(SELECT org1Length FROM CFGFormat)) ELSE ' ' END) Company
			FROM (
				SELECT min(transDate) startDate, max(transDate) endDate, Employee FROM LD GROUP BY Employee
				UNION ALL
				SELECT min(RPPlannedLabor.startDate) startDate, max(RPPlannedLabor.EndDate) endDate, ResourceID FROM RPPlannedLabor 
                left join RPAssignment on RPPlannedLabor.AssignmentID = RPAssignment.AssignmentID GROUP BY ResourceID
				UNION ALL
				SELECT IsNull(HireDate, '2009-01-01') startDate, IsNull(TerminationDate, '2010-12-31') endDate, Employee 	FROM EM
			) as myInner
			inner join EM on myInner.Employee = EM.Employee
			GROUP BY myInner.Employee
		) myInnerMain
		Group by Company
       
     
	open KeysCursor
	fetch next from KeysCursor into 
		@dtStartDate,
		@dtEndDate,
		@strCompany

	set @KeysFetch = @@Fetch_Status

	While (@KeysFetch = 0)
		begin
		  WHILE (@dtStartDate <= @dtEndDate)
			BEGIN
	        
			  -- Compute End Date of interval.

			  SET @dtIntervalEnd = dbo.DLTK$IntervalEnd(@dtStartDate, 'm', 0)
	        
			  IF (@dtIntervalEnd > @dtEndDate) 
				SET @dtIntervalEnd = @dtEndDate
	            
--- This logic calculates number of NumWorkingDaysIncHol.
--	declare @strCompany nvarchar(14)
--	declare @dtStartDate datetime
--	declare @dtEndDate datetime

--	SELECT @strCompany = ' ', @dtStartDate='1990-01-01 00:00:00.000', @dtEndDate='1990-01-31 00:00:00.000'

			  SELECT @NumWorkingDays =
				(TotalDays - 
				 (iNonWorkPerWeek * (TotalDays / 7)) - 
				   LEN(REPLACE(SUBSTRING(strNonWork + strNonWork, iEndDay + 8 - (TotalDays % 7), (TotalDays % 7)), 'N', ''))), 
			  @NumWorkingDaysIncHol = 
				(TotalDays - 
				 (iNonWorkPerWeek * (TotalDays / 7)) - 
				   LEN(REPLACE(SUBSTRING(strNonWork + strNonWork, iEndDay + 8 - (TotalDays % 7), (TotalDays % 7)), 'N', ''))) -
			     (SELECT COUNT(*) FROM CFGHoliday 
					WHERE (HolidayDate BETWEEN @dtStartDate AND @dtIntervalEnd) 
					AND SUBSTRING(strNonWork, DATEPART(dw, HolidayDate), 1) = 'N'
					AND Company = @strCompany)

			  FROM
				(SELECT 
				   DATEDIFF(day, @dtStartDate, @dtIntervalEnd) + 1 AS TotalDays,
				   DATEPART(dw, @dtIntervalEnd) AS iEndDay,
				   CASE WHEN COUNT(*) = 0 THEN 2 
						ELSE MIN(LEN(REPLACE(SundayInd + MondayInd  + TuesdayInd + WednesdayInd + ThursdayInd + FridayInd + SaturdayInd, 'N', ''))) END AS iNonWorkPerWeek, 
				   CASE WHEN COUNT(*) = 0 THEN 'YNNNNNY'
						ELSE MIN((SundayInd + MondayInd  + TuesdayInd + WednesdayInd + ThursdayInd + FridayInd + SaturdayInd)) END AS strNonWork
				   FROM CFGNonWorkingDay WHERE Company = @strCompany) AS NWD

			  -- Insert new Calendar Interval record.

			  INSERT INTO @TimeAnalysis
				(Employee,
				 StartDate,
				 EndDate,
				 NumWorkingDays,
				 NumWorkingDaysIncHol,
				 HoursPerDay,
				 Rate
				)
				Select convert(nvarchar(20),Employee) As Employee, @dtStartDate, @dtIntervalEnd, @NumWorkingDays, @NumWorkingDaysIncHol, 
				EM.HoursPerDay
				,CASE WHEN (CFGMainData.JobCostAtSalary)='N' THEN (EM.JobCostRate) ELSE (CASE WHEN (EM.JobCostType) = 'H' Then (EM.JobCostRate) ELSE (EM.JobCostRate) /((CASE (CFGMainData.JobCostFrequency) WHEN 'B' THEN 260 / 26  WHEN 'S' THEN 260 / 24 WHEN 'M' THEN 260 / 12 ELSE 260 / 52 END )*(CASE WHEN (EM.HoursPerDay) = 0 or (EM.HoursPerDay) is null Then 8 Else (EM.HoursPerDay) END )) END) END Rate
				From EM, CFGMainData
				Where (SELECT CASE WHEN (SELECT MulticompanyEnabled FROM FW_CFGSystem) = 'Y' THEN subString((EM.Org),(SELECT org1Start FROM CFGFormat),(SELECT org1Length FROM CFGFormat)) ELSE ' ' END) = @strCompany
				and CFGMainData.Company = @strCompany
				and IsNull(EM.HireDate, '1900-01-01') <= @dtIntervalEnd
				and IsNull(EM.TerminationDate, '2999-12-31') >= @dtStartDate

			  -- Set Start Date for next interval.
	          
			  SET @dtStartDate = DATEADD(d, 1, @dtIntervalEnd)
	        
			END -- End While

			fetch next from KeysCursor into 
				@dtStartDate,
				@dtEndDate,
				@strCompany
			set @KeysFetch = @@Fetch_Status
		end

	close keysCursor
	deallocate keysCursor

  RETURN

END -- DW$TimeAnalysis

GO
