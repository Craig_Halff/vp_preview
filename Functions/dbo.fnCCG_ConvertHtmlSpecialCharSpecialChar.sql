SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnCCG_ConvertHtmlSpecialCharSpecialChar](@html varchar(max), @htmlChar varchar(10), @normalChar varchar(10))
RETURNS varchar(max)
AS BEGIN

	declare @Start int, @End int, @Length int

	set @Start = CHARINDEX(@htmlChar, @html)
	set @End = @Start + Len(@htmlChar) - 1
	set @Length = (@End - @Start) + 1
	while (@Start > 0 AND @End > 0 AND @Length > 0) 
	begin
		set @html   = STUFF(@html, @Start, @Length, @normalChar)
		set @Start  = CHARINDEX(@htmlChar, @html)
		set @End    = @Start + Len(@htmlChar) - 1
		set @Length = (@End - @Start) + 1
	end
	return @html
END
GO
