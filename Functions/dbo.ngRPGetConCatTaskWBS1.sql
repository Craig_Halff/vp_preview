SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ngRPGetConCatTaskWBS1]
 (@strPlanID varchar(32), 
  @strTaskID varchar(32)) 
  RETURNS Nvarchar(max)  AS
BEGIN

  DECLARE @strConcatName Nvarchar(max)
  DECLARE @strWBS1 nvarchar(30)
  DECLARE @strWBS2 nvarchar(30)
  DECLARE @strWBS3 nvarchar(30)
  DECLARE @strLaborCode nvarchar(14)
    
  SELECT 
    @strWBS1 = WBS1,
    @strWBS2 = ISNULL(WBS2,' '),
    @strWBS3 = ISNULL(WBS3,' '),
	@strLaborCode = ISNULL(LaborCode,' ')
    FROM RPTask WHERE PlanID = @strPlanID AND TaskID = @strTaskID

  -- Loop upward to find parent until getting to the top

  SET @strConcatName =
	CASE
		WHEN @strLaborCode = ' ' 
		THEN
		  CASE
		  WHEN @strWBS3 = ' '
		  THEN
			CASE 
			  WHEN @strWBS2 = ' '
			  THEN @strWBS1
			  ELSE @strWBS1 + ' / ' + @strWBS2
		  END
		ELSE @strWBS1 + ' / ' + @strWBS2 + ' / ' + @strWBS3
		END
		ELSE @strWBS1 
		+ CASE WHEN @strWBS2 <> ' ' THEN ' / ' + @strWBS2 ELSE '' END  
		+ CASE WHEN @strWBS3 <> ' ' THEN ' / ' + @strWBS3 ELSE '' END  		
		+ ' / ' + @strLaborCode
  END

  RETURN @strConcatName 

END -- ngRPGetConCatTaskWBS1
GO
