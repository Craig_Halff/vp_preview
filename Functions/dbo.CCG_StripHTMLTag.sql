SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[CCG_StripHTMLTag] (@HTMLText VARCHAR(MAX), @Tag varchar(12))
RETURNS VARCHAR(MAX) 
AS BEGIN
	declare @Start int, @End int, @Length int
	if @Tag='' set @Tag='<'
	set @Start = CHARINDEX(@Tag,@HTMLText)
	set @End = CHARINDEX('>',@HTMLText,CHARINDEX(@Tag,@HTMLText))
	set @Length = (@End - @Start) + 1
	while @Start > 0 and @End > 0 AND @Length > 0
	begin
		set @HTMLText = STUFF(@HTMLText,@Start,@Length,' ')
		set @Start = CHARINDEX(@Tag,@HTMLText)
		set @End = CHARINDEX('>',@HTMLText,CHARINDEX(@Tag,@HTMLText))
		set @Length = (@End - @Start) + 1
	end
	return LTRIM(RTRIM(@HTMLText))
END

GO
