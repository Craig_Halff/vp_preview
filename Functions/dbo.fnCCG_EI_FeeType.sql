SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_FeeType] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS varchar(50)
AS BEGIN
/*
	Copyright (c) 2017 Elevia Software.  All rights reserved.

	select dbo.fnCCG_EI_TotalComp('2003005.xx',' ',' ')
	select dbo.fnCCG_EI_TotalComp('2003005.xx','1PD',' ')
	select dbo.fnCCG_EI_TotalComp('2003005.xx','1PD','COD')
*/
	declare @res varchar(50)
	select @res = CustFeeType from ProjectCustomTabFields where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3
	
	return @res
END
GO
