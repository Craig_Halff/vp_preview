SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DW$tabBaselineLabor]
  (@strCompany nvarchar(14) = ' ',
   @strScale varchar(1) = 'm')
  RETURNS @tabBaselineLabor TABLE
    (PlanID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     WBS1 nvarchar(30) COLLATE database_default,
     WBS2 nvarchar(30) COLLATE database_default, 
     WBS3 nvarchar(30) COLLATE database_default,
     LaborCode nvarchar(14) COLLATE database_default,
     Employee nvarchar(20) COLLATE database_default,
     LaborCategory smallint,
     TransactionDate int,
     Hours decimal(19,4),
     CostAmount decimal(19,4),
     BillAmount decimal(19,4),
     RevenueAmount decimal(19,4)
    )
BEGIN -- Function DW$tabBaselineLabor
 
  DECLARE @dtStartDate datetime
  DECLARE @dtEndDate datetime
  DECLARE @dtIntervalEnd datetime
  
  DECLARE @intWkEndDay AS int
  
  DECLARE @intHrDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intLabRevDecimals int
    
  DECLARE @intBaselineLabCount int
    
  -- Declare Temp tables.

  DECLARE @tabPlan
    TABLE (PlanID varchar(32) COLLATE database_default
           PRIMARY KEY(PlanID)) 
  
  DECLARE @tabCalendarInterval
    TABLE(StartDate datetime,
          EndDate datetime
          PRIMARY KEY(StartDate, EndDate))
          
  DECLARE @tabBLabTPD
    TABLE (RowID int identity(1, 1),
	       TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	       PlanID varchar(32) COLLATE database_default,
	       TaskID varchar(32) COLLATE database_default,
	       AssignmentID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodHrs decimal(19,4),
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PeriodRev decimal(19,4)
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Filter out only Plans with same Company
  
  INSERT @tabPlan(PlanID) SELECT PlanID FROM RPPlan WHERE Company = @strCompany

  -- Get flags to determine which features are being used.
  
  SELECT
     @intWkEndDay = (CASE WHEN StartingDayOfWeek = 1 THEN 7 ELSE StartingDayOfWeek - 1 END)
     FROM CFGResourcePlanning
     WHERE CFGResourcePlanning.Company = @strCompany
      
  -- Get decimal settings.
  -- At this point, there is no way to know what was the basis used in calculation of Baseline Revenue numbers.
  -- Therefore, we decided to use the current basis to determine number of decimal digits to be used
  -- in the realignment of Baseline Revenue numbers.
  
  SELECT @intHrDecimals = 2,
         @intAmtCostDecimals = 2,
         @intAmtBillDecimals = 2,
         @intLabRevDecimals = 2
    
  -- Check to see if there is any time-phased data
  
    SELECT @intBaselineLabCount = COUNT(*) FROM RPBaselineLabor AS TPD INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID  
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find the MIN and MAX dates of all TPD.
  
  SELECT @dtStartDate = MIN(MINDate), @dtEndDate = MAX(MAXDate)
    FROM
      (SELECT MIN(StartDate) AS MINDate, MAX(EndDate) AS MAXDate FROM RPBaselineLabor AS TPD INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID  
      ) AS X
  
  -- Save Calendar Intervals into a temp table and add an overflow Calendar Interval.
      
  WHILE (@dtStartDate <= @dtEndDate)
    BEGIN
        
      -- Compute End Date of interval.

      IF (@strScale = 'd') 
        SET @dtIntervalEnd = @dtStartDate
      ELSE
        SET @dtIntervalEnd = dbo.DLTK$IntervalEnd(@dtStartDate, @strScale, @intWkEndDay)
        
      IF (@dtIntervalEnd > @dtEndDate) 
        SET @dtIntervalEnd = @dtEndDate
            
      -- Insert new Calendar Interval record.
          
      INSERT @tabCalendarInterval(StartDate, EndDate)
        VALUES (@dtStartDate, @dtIntervalEnd)
          
      -- Set Start Date for next interval.
          
      SET @dtStartDate = DATEADD(d, 1, @dtIntervalEnd)
        
    END -- End While
       
       
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
              
  IF (@intBaselineLabCount > 0)
    BEGIN
    
      -- Save Baseline Labor time-phased data rows.

      INSERT @tabBLabTPD
        (TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         AssignmentID,
         StartDate, 
         EndDate, 
         PeriodHrs,
         PeriodCost, 
         PeriodBill,
         PeriodRev)
         SELECT
           TimePhaseID AS TimePhaseID,
           CIStartDate AS CIStartDate,
           PlanID AS PlanID, 
           TaskID AS TaskID,
           AssignmentID AS AssignmentID,
           StartDate AS StartDate, 
           EndDate AS EndDate, 
           ROUND(ISNULL(PeriodHrs * ProrateRatio, 0), @intHrDecimals) AS PeriodHrs,
           ROUND(ISNULL(PeriodCost * ProrateRatio, 0), @intAmtCostDecimals) AS PeriodCost, 
           ROUND(ISNULL(PeriodBill * ProrateRatio, 0), @intAmtBillDecimals) AS PeriodBill, 
           ROUND(ISNULL(PeriodRev * ProrateRatio, 0), @intLabRevDecimals) AS PeriodRev
         FROM (SELECT -- For Childless Task Rows.
                 CI.StartDate AS CIStartDate,
                 TPD.TimePhaseID AS TimePhaseID,
                 T.PlanID AS PlanID, 
                 T.TaskID AS TaskID,
                 NULL AS AssignmentID, 
                 CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                 CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                 PeriodHrs AS PeriodHrs,
                 PeriodCost AS PeriodCost,
                 PeriodBill AS PeriodBill,
                 PeriodRev AS PeriodRev,
                 CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                      THEN dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                      THEN TPD.StartDate 
                                                      ELSE CI.StartDate END, 
                                                 CASE WHEN TPD.EndDate < CI.EndDate 
                                                      THEN TPD.EndDate 
                                                      ELSE CI.EndDate END, 
                                                 TPD.StartDate, TPD.EndDate,
                                                 @strCompany)
                      ELSE 1 END AS ProrateRatio
                 FROM RPBaselineLabor AS TPD				   
                   INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID
                   INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                     AND TPD.AssignmentID IS NULL AND T.ChildrenCount = 0 AND T.LabParentState = 'N'
                   INNER JOIN @tabCalendarInterval AS CI ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate				
               UNION ALL
               SELECT -- For Assignment Rows.
                 CI.StartDate AS CIStartDate, 
                 TPD.TimePhaseID AS TimePhaseID,
                 A.PlanID AS PlanID, 
                 A.TaskID AS TaskID,
                 A.AssignmentID AS AssignmentID, 
                 CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                 CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                 PeriodHrs AS PeriodHrs,
                 PeriodCost AS PeriodCost,
                 PeriodBill AS PeriodBill,
                 PeriodRev AS PeriodRev,
                 CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                      THEN dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                      THEN TPD.StartDate 
                                                      ELSE CI.StartDate END, 
                                                 CASE WHEN TPD.EndDate < CI.EndDate 
                                                      THEN TPD.EndDate 
                                                      ELSE CI.EndDate END, 
                                                 TPD.StartDate, TPD.EndDate,
                                                 @strCompany)
                      ELSE 1 END AS ProrateRatio
                 FROM RPBaselineLabor AS TPD				   
                   INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID
                   INNER JOIN RPAssignment AS A ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND TPD.AssignmentID = A.AssignmentID
                   INNER JOIN @tabCalendarInterval AS CI ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
              ) AS X
         WHERE (PeriodHrs IS NOT NULL AND ROUND((PeriodHrs * ProrateRatio), @intHrDecimals) != 0)
     
      -- Adjust Baseline Labor time-phased data to compensate for rounding errors.
     
      UPDATE @tabBLabTPD 
        SET 
          PeriodHrs = (TPD.PeriodHrs + D.DeltaHrs),
          PeriodCost = (TPD.PeriodCost + D.DeltaCost),
          PeriodBill = (TPD.PeriodBill + D.DeltaBill),
          PeriodRev = (TPD.PeriodRev + D.DeltaRev)
        FROM @tabBLabTPD AS TPD INNER JOIN 
          (SELECT 
             YTPD.TimePhaseID AS TimePhaseID, 
             ROUND((YTPD.PeriodHrs - SUM(ISNULL(XTPD.PeriodHrs, 0))), @intHrDecimals) AS DeltaHrs,
             ROUND((YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))), @intAmtCostDecimals) AS DeltaCost,
             ROUND((YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))), @intAmtBillDecimals) AS DeltaBill,
             ROUND((YTPD.PeriodRev - SUM(ISNULL(XTPD.PeriodRev, 0))), @intLabRevDecimals) AS DeltaRev            
             FROM @tabBLabTPD AS XTPD INNER JOIN RPBaselineLabor AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
             GROUP BY YTPD.TimePhaseID, YTPD.PeriodHrs, YTPD.PeriodCost, YTPD.PeriodBill, YTPD.PeriodRev) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
        WHERE (D.DeltaHrs != 0 OR D.DeltaCost != 0 OR D.DeltaBill != 0 OR D.DeltaRev != 0)
          AND RowID IN
            (SELECT RowID FROM @tabBLabTPD AS ATPD INNER JOIN
               (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabBLabTPD GROUP BY TimePhaseID) AS BTPD
                  ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
               
    END -- IF (@intBaselineLabCount > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT INTO @tabBaselineLabor
    (PlanID,
     TaskID,
     WBS1,
     WBS2, 
     WBS3,
     LaborCode,
     Employee,
     LaborCategory,
     TransactionDate,
     Hours,
     CostAmount,
     BillAmount,
     RevenueAmount
    )
    SELECT
      Z.PlanID,
      Z.TaskID,
      CASE WHEN T.WBS1 = '<none>' THEN '<empty>' ELSE ISNULL(T.WBS1, '<empty>') END AS WBS1,
      CASE WHEN T.WBS2 = '<none>' THEN '<empty>' ELSE ISNULL(T.WBS2, '<empty>') END AS WBS2,
      CASE WHEN T.WBS3 = '<none>' THEN '<empty>' ELSE ISNULL(T.WBS3, '<empty>') END AS WBS3,
      CASE WHEN T.LaborCode = '<none>' THEN '<empty>' ELSE ISNULL(T.LaborCode, '<empty>') END AS LaborCode,
      A.ResourceID As Employee,
      A.Category AS LaborCategory,
      ISNULL(CONVERT(INT, MIN(Z.StartDate)), -1) AS TransactionDate,
      ISNULL(SUM(Z.Hours), 0) AS Hours,
      ISNULL(SUM(Z.CostAmount), 0) AS CostAmount,
      ISNULL(SUM(Z.BillAmount), 0) AS BillAmount,
      ISNULL(SUM(Z.RevenueAmount), 0) AS RevenueAmount
      FROM
        (SELECT
           TPD.CIStartDate AS CIStartDate,
           TPD.PlanID AS PlanID,
           TPD.TaskID AS TaskID,
           TPD.AssignmentID AS AssignmentID,
           TPD.StartDate,
           TPD.EndDate,
           TPD.PeriodHrs AS Hours,
           TPD.PeriodCost AS CostAmount,
           TPD.PeriodBill AS BillAmount,
           TPD.PeriodRev AS RevenueAmount
           FROM @tabBLabTPD AS TPD
        ) AS Z
        INNER JOIN RPTask AS T ON Z.PlanID = T.PlanID AND Z.TaskID = T.TaskID
        LEFT JOIN RPAssignment AS A ON Z.PlanID = A.PlanID AND Z.TaskID = A.TaskID AND Z.AssignmentID = A.AssignmentID
        GROUP BY Z.PlanID, Z.TaskID, Z.AssignmentID, Z.CIStartDate, T.WBS1, T.WBS2, T.WBS3, T.LaborCode, A.ResourceID, A.Category

  RETURN

END -- DW$tabBaselineLabor

GO
