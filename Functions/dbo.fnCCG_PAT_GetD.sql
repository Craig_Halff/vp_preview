SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   FUNCTION [dbo].[fnCCG_PAT_GetD] (@s Nvarchar(max))
RETURNS Nvarchar(max)
WITH ENCRYPTION
BEGIN
	RETURN Convert(Nvarchar(max),DecryptByPassPhrase(N'CCG_PAT_123',Convert(varbinary(max), @s, 2)))
END
GO
