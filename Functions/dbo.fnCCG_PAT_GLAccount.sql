SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_PAT_GLAccount] (@PayableSeq int, @WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS varchar(20)
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select * from CCG_PAT_ProjectAmount where payableseq = 6
	select dbo.fnCCG_PAT_GLAccount (5,NULL,NULL,NULL) 
	select dbo.fnCCG_PAT_GLAccount (5, '1993041.00', ' ', ' ') 
	select dbo.fnCCG_PAT_GLAccount (6,NULL,NULL,NULL) 
*/
    declare @res varchar(20)

	select @res = case when MIN(pat.GLAccount) <> MAX(pat.GLAccount) then '<MULT>' else MIN(pat.GLAccount) end
	from CCG_PAT_ProjectAmount pat
	where (pat.PayableSeq=@PayableSeq )

	return @res
END
GO
