SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabCalendarMultiScales](
  @strScopeStartDate varchar(8) /* Date must be in format: 'yyyymmdd' regardless of UI Culture */,
  @strScopeEndDate varchar(8) /* Date must be in format: 'yyyymmdd' regardless of UI Culture */,
  @strMultiScales varchar(14) /* e.g. "x,20160601,2,m" */,
  @strCompany Nvarchar(14)
)
  RETURNS @tabCalendarInterval TABLE(
    SeqID int IDENTITY(1,1),
    StartDate datetime,
    EndDate datetime,
    Scale varchar(1) COLLATE database_default, 
    NumWorkingDays integer,
    Heading varchar(255) COLLATE database_default
  )

BEGIN

  DECLARE @strScale varchar(1) /* {d, w, m, x} */
  DECLARE @strXStartDate varchar(8) /* Date must be in format: 'yyyymmdd' regardless of UI Culture */
  DECLARE @intXMonthCount integer
  DECLARE @strXSecondaryScale varchar(1) = 'm' /* {w, m} */

  DECLARE @dtScopeStartDate datetime
	DECLARE @dtScopeEndDate datetime

	DECLARE @dtXStartDate datetime

  DECLARE @dtBeforeDate datetime
  DECLARE @dtAfterDate datetime

  DECLARE @dtChunk1StartDate datetime
	DECLARE @dtChunk1EndDate datetime
  DECLARE @dtChunk2StartDate datetime
	DECLARE @dtChunk2EndDate datetime
  DECLARE @dtChunk3StartDate datetime
	DECLARE @dtChunk3EndDate datetime

  DECLARE @dtCIStartDate datetime
  DECLARE @dtCIEndDate datetime
 
  DECLARE @intWkEndDay AS integer

  DECLARE @decBeforeWD decimal(19,4) = 0
  DECLARE @decAfterWD decimal(19,4) = 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Extract @strScale from @strMultiScales

  SET @strScale = SUBSTRING(@strMultiScales, 1, 1)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strScale IN ('d', 'w', 'm', 'p'))
    BEGIN

	    INSERT @tabCalendarInterval(
        StartDate,
        EndDate,
        Scale, 
        NumWorkingDays,
        Heading
	    )	
		    SELECT
          StartDate,
          EndDate,
          Scale, 
          NumWorkingDays,
          Heading
		      FROM dbo.stRP$tabCalendarStartEndDate(@strScopeStartDate, @strScopeEndDate, @strScale, @strCompany)

    END /* END IF (@strScale = 'w' OR @strScale = 'm' */

  ELSE IF (@strScale = 'x')
    BEGIN


      -- Get StartingDayOfWeek from Configuration Settings.

      SELECT
        @intWkEndDay = (CASE WHEN StartingDayOfWeek = 1 THEN 7 ELSE StartingDayOfWeek - 1 END)
        FROM CFGResourcePlanning Where Company = @strCompany

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Parse @strMultiScales to get the rest of the multi-scale settings.

      SET @strXStartDate = SUBSTRING(@strMultiScales, 3, 8)
      SET @intXMonthCount = SUBSTRING(@strMultiScales, 12, 1)
      SET @strXSecondaryScale = SUBSTRING(@strMultiScales, 14, 1)

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Set Dates.
  
      SELECT 
        @dtScopeStartDate = CONVERT(datetime, @strScopeStartDate),
        @dtScopeEndDate = CONVERT(datetime, @strScopeEndDate),
        @dtXStartDate = CONVERT(datetime, @strXStartDate)

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Determine Start and End Dates for Chunk #2

      SELECT
        @dtChunk2StartDate = 
          CASE @strXSecondaryScale
            WHEN 'w' THEN DATEADD(DAY, -6, dbo.DLTK$NextDay(@dtXStartDate, @intWkEndDay))
            WHEN 'm' THEN DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtXStartDate), 0)
          END,
        @dtChunk2EndDate = 
          dbo.DLTK$IntervalEnd(
            DATEADD(MONTH, (@intXMonthCount - 1), @dtXStartDate), 
            @strXSecondaryScale, 
            @intWkEndDay
          )
        
      -- Ensure that Chunk #2 does not go pass the Scope Start/End Dates.

      SELECT
        @dtChunk2StartDate = 
          CASE 
            WHEN @dtScopeStartDate BETWEEN @dtChunk2StartDate AND @dtChunk2EndDate
            THEN @dtScopeStartDate
            ELSE @dtChunk2StartDate
          END,
        @dtChunk2EndDate = 
          CASE 
            WHEN @dtScopeEndDate BETWEEN @dtChunk2StartDate AND @dtChunk2EndDate
            THEN @dtScopeEndDate
            ELSE @dtChunk2EndDate
          END  

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      SET @dtBeforeDate = DATEADD(DAY, -1, @dtChunk2StartDate)
      SET @dtAfterDate = DATEADD(DAY, 1, @dtChunk2EndDate)

      SET @decBeforeWD = dbo.DLTK$NumWorkingDays(@dtScopeStartDate, @dtBeforeDate, @strCompany)
      SET @decAfterWD = dbo.DLTK$NumWorkingDays(@dtAfterDate, @dtScopeEndDate, @strCompany)

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Build Calendar.
      -- Calendar has at most 3 chunks {Before, Main, After}.

      IF (@decBeforeWD > 0)
        BEGIN

	        INSERT @tabCalendarInterval(
            StartDate,
            EndDate ,
            Scale, 
            NumWorkingDays,
            Heading
	        )	
		        SELECT 
              StartDate,
              EndDate ,
              Scale, 
              NumWorkingDays,
              Heading
              FROM 
                dbo.stRP$tabCalendarStartEndDate(
                  @strScopeStartDate, 
                  REPLACE(CONVERT(varchar(10), @dtBeforeDate, 121), '-', ''), 
                  @strXSecondaryScale,
				  @strCompany
                )

        END

	    INSERT @tabCalendarInterval(
        StartDate,
        EndDate ,
        Scale, 
        NumWorkingDays,
        Heading
	    )	
		    SELECT 
          StartDate,
          EndDate ,
          Scale, 
          NumWorkingDays,
          Heading
          FROM 
            dbo.stRP$tabCalendarStartEndDate(
              REPLACE(CONVERT(varchar(10), @dtChunk2StartDate, 121), '-', ''), 
              REPLACE(CONVERT(varchar(10), @dtChunk2EndDate, 121), '-', ''), 
              'd',
			  @strCompany
            )

      IF (@decAfterWD > 0)
        BEGIN

	        INSERT @tabCalendarInterval(
            StartDate,
            EndDate ,
            Scale, 
            NumWorkingDays,
            Heading
	        )	
		        SELECT 
              StartDate,
              EndDate ,
              Scale, 
              NumWorkingDays,
              Heading
              FROM 
                dbo.stRP$tabCalendarStartEndDate(
                  REPLACE(CONVERT(varchar(10), @dtAfterDate, 121), '-', ''), 
                  @strScopeEndDate, 
                  @strXSecondaryScale,
				  @strCompany
                )

        END       

    END /* END ELSE IF (@strScale = 'd') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
