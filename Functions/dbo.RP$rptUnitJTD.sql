SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$rptUnitJTD]
  (@strPlanID varchar(32),
   @dtETCDate datetime,
   @strTaskID varchar(32)=NULL,
   @ReportAtBillingInBillingCurr varchar(1) = 'N')
  RETURNS @tabJTDUnit TABLE
   (PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    UnitID varchar(32) COLLATE database_default,
    TransDate datetime,
    JTDCostCurrencyCode Nvarchar(3) COLLATE database_default,
    JTDBillCurrencyCode Nvarchar(3) COLLATE database_default,    
    PeriodQty decimal(19,4),
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4))
BEGIN

  INSERT @tabJTDUnit
    SELECT 
      PlanID,
      TaskID,  
      UnitID,  
      TransDate,  
	  Max(JTDCostCurrencyCode) as JTDCostCurrencyCode,
      Max(JTDBillCurrencyCode) as JTDBillCurrencyCode,
      SUM(PeriodQty) AS PeriodQty,
      SUM(PeriodCost) AS PeriodCost,  
      SUM(PeriodBill) AS PeriodBill
      FROM
        (SELECT X.PlanID as PlanID, 
                    X.TaskID as TaskID,  
                    X.UnitID AS UnitID, 
                    Ledger.TransDate AS TransDate, 
	                Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
                    Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
                    SUM(Ledger.UnitQuantity) AS PeriodQty, 
                    SUM(Ledger.AmountProjectCurrency) AS PeriodCost, 
                    SUM(Ledger.BillExt) AS PeriodBill 
          FROM LedgerMISC AS Ledger 
          INNER JOIN PR on PR.WBS1=Ledger.WBS1 and PR.WBS2='' and PR.WBS3=''
          INNER JOIN RPUnit AS X ON (Ledger.WBS1 = X.WBS1 AND X.PlanID = @strPlanID AND X.TaskID like ISNULL(@strTaskID,'%') AND Ledger.ProjectCost = 'Y' 
                    AND Ledger.TransType = 'UN' AND Ledger.Unit = X.Unit AND Ledger.UnitTable = X.UnitTable AND Ledger.Account = X.Account) 
          INNER JOIN (SELECT MIN(UnitID) AS UnitID FROM RPUnit WHERE PlanID = @strPlanID AND TaskID like ISNULL(@strTaskID,'%') GROUP BY TaskID, Unit, Account) AS X1 ON X.UnitID = X1.UnitID 
          WHERE Ledger.WBS2 LIKE (ISNULL(X.WBS2, '%')) AND Ledger.WBS3 LIKE (ISNULL(X.WBS3, '%')) AND Ledger.TransDate <= @dtETCDate
          GROUP BY X.PlanID, X.TaskID, Ledger.TransDate, X.UnitID, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) AS Z
    GROUP BY UnitID, TaskID,PlanID, TransDate, JTDCostCurrencyCode, JTDBillCurrencyCode

  RETURN

END 

GO
