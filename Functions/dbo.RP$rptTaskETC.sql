SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$rptTaskETC]
  (@strPlanID varchar(32),
  @dtETCDate datetime)
  RETURNS @taskETC TABLE
   (planID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
	OutlineNumber varchar(255) COLLATE database_default,
    ETCLabHrs decimal(19,4),
    ETCLabCost decimal(19,4),
    ETCLabBill decimal(19,4),
    ETCExpCost decimal(19,4),
    ETCExpBill decimal(19,4),
    ETCDirExpCost decimal(19,4),
    ETCDirExpBill decimal(19,4),
    ETCReimExpCost decimal(19,4),
    ETCReimExpBill decimal(19,4),
    ETCConCost decimal(19,4),
    ETCConBill decimal(19,4),
    ETCDirConCost decimal(19,4),
    ETCDirConBill decimal(19,4),
    ETCReimConCost decimal(19,4),
    ETCReimConBill decimal(19,4),
    ETCUntQty decimal(19,4),
    ETCUntCost decimal(19,4),
    ETCUntBill decimal(19,4))

BEGIN --outer

  DECLARE @strETCDate VARCHAR(8)
  SELECT @strETCDate = CONVERT(char(4),YEAR(@dtETCDate))+CONVERT(char(2),RIGHT('0' + RTRIM(MONTH(@dtETCDate)), 2))+CONVERT(char(2),RIGHT('0' + RTRIM(DAY(@dtETCDate)), 2))

  INSERT @taskETC
    SELECT @strPlanID,* from dbo.PM$tabTaskETC(@strPlanID,@strETCDate) 

  DECLARE @strTaskID varchar(32) 
  DECLARE @strOutlineNumber varchar(255) 
  DECLARE csrTask CURSOR FOR 
    SELECT DISTINCT T.TaskID, T.OutlineNumber
    FROM RPTask AS T LEFT JOIN @taskETC AS TPD ON T.PlanID = TPD.PlanID and T.TaskID = TPD.TaskID 
    WHERE T.PlanID = @strPlanID  AND TPD.TaskID IS NULL
    ORDER BY T.OutlineNumber DESC 
  OPEN csrTask 
  FETCH NEXT FROM csrTask INTO @strTaskID, @strOutlineNumber
    WHILE (@@FETCH_STATUS = 0) 
      BEGIN --csrTask while begin

      INSERT @taskETC
      SELECT @strPlanID AS PlanID, 
	          @strTaskID AS TaskID, 
			  @strOutlineNumber,
              SUM(ETCLabHrs) as ETCLabHrs,
              SUM(ETCLabCost) as ETCLabCost,
              SUM(ETCLabBill) as ETCLabBill,
              SUM(ETCExpCost) as ETCExpCost,
              SUM(ETCExpBill) as ETCExpBill,
              SUM(ETCDirExpCost) as ETCDirExpCost,
              SUM(ETCDirExpBill) as ETCDirExpBill,
              SUM(ETCReimExpCost) as ETCReimExpCost,
              SUM(ETCReimExpBill) as ETCReimExpBill,
              SUM(ETCConCost) as ETCConCost,
              SUM(ETCConBill) as ETCConBill,
              SUM(ETCDirConCost) as ETCDirConCost,
              SUM(ETCDirConBill) as ETCDirConBill,
              SUM(ETCReimConCost) as ETCReimConCost,
              SUM(ETCReimConBill) as ETCReimConBill,
              SUM(ETCUntQty) as ETCUntQty,
              SUM(ETCUntCost) as ETCUntCost,
              SUM(ETCUntBill) as ETCUntBill   
      FROM RPTask AS CT 
      INNER JOIN @taskETC AS TPD ON TPD.PlanID = CT.PlanID AND TPD.TaskID = CT.TaskID 
      WHERE CT.PlanID = @strPlanID AND CT.ParentOutlineNumber = @strOutlineNumber
      
      FETCH NEXT FROM csrTask INTO @strTaskID, @strOutlineNumber
    END  --csrTask while end
  
  CLOSE csrTask
  DEALLOCATE csrTask

RETURN
END --outer

GO
