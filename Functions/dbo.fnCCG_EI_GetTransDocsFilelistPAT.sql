SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_GetTransDocsFilelistPAT](@Username Nvarchar(32), @CorE char(1), @Invoice Nvarchar(12), @WBS1 Nvarchar(30))
RETURNS @T TABLE
(
	Seq                 int identity(1,1),  /* For updates */
	MenuFolder          varchar(255),       /* Includes cascades - e.g. Billed>2014 */
	MenuLabel           varchar(255),       /* Last menu label after cascades - e.g. Invoice: AJ Carl Engineers 0938 3/19/2013 */
	OnInvoice           char(1),                   /* Indicator if the item is on the passed in invoice - e.g. * or blank */
	OrigInvoice         Nvarchar(32),
	Description1		Nvarchar(100),
	Description2		Nvarchar(100),
	FileName            varchar(255),
	TransDate           datetime,
	FileID              uniqueidentifier,
	SourceTable         varchar(32),
	Invoice             Nvarchar(32),
	PayableType         varchar(32),
	TopOrder            smallint
)
AS BEGIN
/*
	   Copyright (c) 2017 Central Consulting Group.  All rights reserved.

	   select * from dbo.fnCCG_EI_GetTransDocsFilelistPAT('ADMIN','C','0001000','2003005.00')

*/
	declare @ShowUnbilledOnlyInCEMenus varchar(1)
	select @ShowUnbilledOnlyInCEMenus=ShowUnbilledOnlyInCEMenus from CCG_EI_Config

	insert into @T (MenuFolder,MenuLabel,OnInvoice,OrigInvoice,Description1,Description2,
			FileName,TransDate,FileID,SourceTable,Invoice,PayableType,TopOrder)
		select '' as MenuFolder, '' as MenuLabel, IsNull(ap.OnInvoice,' ') as OnInvoice, IsNull(ap.OrigInvoice, pay.PayableNumber) as OrigInvoice,
				Max(Case When IsNull(ap.Description1,'')='' Then IsNull(pa.Description,VE.Name) Else IsNull(ap.Description1,'') End) as Description1,
				Max(Case When IsNull(ap.Description2,'')='' Then pa.Description Else IsNull(ap.Description2,'') End) as Description2,
				pay.PayableFileName as FileName, pay.PayableDate as TransDate, null as FileID, 'PAT' as SourceTable, ap.Invoice,
				case When pay.PayableType='C' Then 'Contract' Else 'Invoice' End as PayableType, 1 as TopOrder
			from CCG_PAT_ProjectAmount pa
				inner join CCG_PAT_Payable pay on pay.Seq=pa.PayableSeq
				left join CCG_PAT_ConfigStages stage on pay.Stage=stage.Stage and stage.SubType<>'Exported'
				left join BTBGSubs subs on subs.SubWBS1=pa.WBS1
				left join VE on VE.Vendor=pay.Vendor
				left join (
					select l.vendor, l.voucher, BilledInvoice, Max(Case When d.Invoice=@Invoice Then '*' Else ' ' End) as OnInvoice, Max(l.Invoice) as OrigInvoice, Max(d.Invoice) as Invoice,
							Max(IsNull(d.Description1,l.Desc1)) as Description1, Max(IsNull(d.Description2,l.Desc2)) as Description2,
							Sum(l.amount) as VisionAmount, --count(distinct l.WBS1) as VisionWBS1Count, min(l.WBS1) as VisionMinWBS1,
							Min(Cast(l.Period as varchar)+Cast(l.PostSeq as varchar)+l.PKey) as MinPeriodPostSeqPKey
						from LedgerAP l inner join ve on ve.vendor=l.vendor
							left join billConDetail d on d.OriginalPeriod=l.Period and d.OriginalPostSeq=l.PostSeq and d.OriginalPKey=l.PKey and d.OriginalTable='LedgerAP'
						where l.WBS1 = @WBS1 and l.TransType = 'AP' and SubType = 'L' and l.PostSeq <> 0
						group by BilledInvoice, l.vendor, l.voucher
				) ap on ap.Vendor=pay.Vendor and ap.Voucher=pay.Voucher
				left join LedgerDocuments docs on Cast(docs.Period as varchar)+Cast(docs.PostSeq as varchar)+docs.PKey=ap.MinPeriodPostSeqPKey and docs.TableName='LedgerAP'
			where (pa.WBS1=@WBS1 or subs.MainWBS1 = @WBS1 or IsNull(ap.VisionAmount,0) > 0)
				/* Either not exported or exported but no TDM: */
				and (stage.Stage is null or docs.Period is null)
				and (@ShowUnbilledOnlyInCEMenus='N' or ap.BilledInvoice is null)
			group by IsNull(ap.OnInvoice,' '), IsNull(ap.OrigInvoice, pay.PayableNumber), pay.PayableFileName, pay.PayableDate, ap.Invoice,
					case When pay.PayableType='C' Then 'Contract' Else 'Invoice' End
			order by 1 desc, pay.PayableDate desc
	return
END
GO
