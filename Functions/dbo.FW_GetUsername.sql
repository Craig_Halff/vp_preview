SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[FW_GetUsername]() RETURNS nvarchar(32) AS  
BEGIN 
-- Leave NVARCHAR alone since converting binary and need to know position for parsing and nvarchar is double byte
  RETURN CONVERT(nvarchar(32),(SELECT ISNULL(SESSION_CONTEXT(N'Username'),'')))
END
GO
