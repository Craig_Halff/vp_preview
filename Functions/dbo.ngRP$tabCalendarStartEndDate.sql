SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ngRP$tabCalendarStartEndDate](
  @strScopeStartDate varchar(8) /* Date must be in format: 'yyyymmdd' regardless of UI Culture */,
  @strScopeEndDate varchar(8) /* Date must be in format: 'yyyymmdd' regardless of UI Culture */,
  @strScale varchar(1) /* {d, w, m, p} */
)
  RETURNS @tabCalendarInterval TABLE(
    SeqID int IDENTITY(1,1),
    StartDate datetime,
    EndDate datetime,
    Scale varchar(1) COLLATE database_default, 
    NumWorkingDays integer,
    Heading varchar(255) COLLATE database_default
  )

BEGIN

  DECLARE @dtScopeStartDate datetime
  DECLARE @dtScopeEndDate datetime


  DECLARE @dtCIStartDate datetime
  DECLARE @dtCIEndDate datetime

  DECLARE @dtFiscalStartDate datetime
  DECLARE @dtFiscalEndDate datetime
 
  DECLARE @strCompany Nvarchar(14)

  DECLARE @intWkEndDay AS integer

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Set Company to non-Multi-Company.
  
  SELECT @strCompany = dbo.GetActiveCompany()

  -- Get StartingDayOfWeek from Configuration Settings.

    SELECT
      @intWkEndDay = (CASE WHEN StartingDayOfWeek = 1 THEN 7 ELSE StartingDayOfWeek - 1 END)
      FROM CFGResourcePlanning Where Company = @strCompany

  -- Set Dates.
  
  SELECT @dtScopeStartDate = CONVERT(datetime, @strScopeStartDate)
  SELECT @dtScopeEndDate = CONVERT(datetime, @strScopeEndDate)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	SET @dtCIStartDate = @dtScopeStartDate

  --If scale is fiscal period then first copy over any existing periods into the calendar
  IF (@strScale = 'p')
  BEGIN
	INSERT @tabCalendarInterval(
	StartDate, 
	EndDate, 
	Scale, 
	NumWorkingDays,
	Heading
	)
	SELECT
		AccountPdStart,
		AccountPdEnd,
		'p',
		dbo.DLTK$NumWorkingDays(AccountPdStart, AccountPdEnd, @strCompany) AS NumWorkingDays,
		CONVERT(varchar,Period % 100) + '/' + CONVERT(varchar,Period/100) AS Heading
	FROM CFGDates
	WHERE AccountPdStart <= @dtScopeEndDate AND AccountPdEnd >= @dtScopeStartDate

	--Retrieve last enddate to determine if there are more calendar intervals to insert
	SELECT @dtFiscalEndDate = MAX(EndDate),
		   @dtFiscalStartDate = MIN(StartDate)
	FROM @tabCalendarInterval
  END

  DECLARE @intCount integer = 1;

  WHILE @dtCIStartDate <= @dtScopeEndDate
    BEGIN

	  IF (@strScale = 'p' AND @dtCIStartDate >= @dtFiscalStartDate AND @dtCIStartDate <= @dtFiscalEndDate)
		SET @dtCIStartDate = DATEADD(day,1,@dtFiscalEndDate)

      IF (@strScale = 'd') 
        SET @dtCIEndDate = @dtCIStartDate
      ELSE
        SET @dtCIEndDate = dbo.DLTK$IntervalEnd(@dtCIStartDate, @strScale, @intWkEndDay)
        
			IF (@dtCIEndDate > @dtScopeEndDate)
				SET @dtCIEndDate = @dtScopeEndDate

      INSERT @tabCalendarInterval(
        StartDate, 
        EndDate, 
        Scale, 
        NumWorkingDays,
        Heading
      )
        SELECT
          @dtCIStartDate AS StartDate,
          @dtCIEndDate AS EndDate,
          @strScale AS Scale,
          dbo.DLTK$NumWorkingDays(@dtCIStartDate, @dtCIEndDate, @strCompany) AS NumWorkingDays,
          CASE 
            WHEN YEAR(@dtCIStartDate) = YEAR(@dtCIEndDate) 
              THEN DATENAME(yyyy, @dtCIStartDate) + '<br />' + CONVERT(VARCHAR(5), @dtCIStartDate, 101)  + ' - ' +  CONVERT(VARCHAR(5), @dtCIEndDate, 101) 
            WHEN YEAR(@dtCIStartDate) != YEAR(@dtCIEndDate) 
              THEN DATENAME(yyyy, @dtCIStartDate) + ' - ' + DATENAME(yyyy, @dtCIEndDate) + '<br />' + CONVERT(VARCHAR(5), @dtCIStartDate, 101)  + ' - ' +  CONVERT(VARCHAR(5), @dtCIEndDate, 101) 
          END AS Heading
          
      -- Set @dtStartDate and @intCount for next interval.
          
      SET @dtCIStartDate = DATEADD(d, 1, @dtCIEndDate)
      SET @intCount = @intCount + 1;

    END;

/* "2016<br />3/1 - 3/31" */

RETURN
END

GO
