SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_JTDBilledLink] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS varchar(255)
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select dbo.fnCCG_EI_JTDBilledLink('25299', ' ', ' ')
*/
	declare @res varchar(255)
	select @res = '@' + dbo.fnCCG_GetWebServerURLPrefix() +  'EI_NValue.aspx?WBS1=' + @WBS1 + '&WBS2=' + @WBS2 + '&WBS3=' + @WBS3 + 
		'&T=JTD%20Invoiced' +
		'&NV=7' +
		'&HD=Invoice' +
		'&H1=Inv Date' +
		'&H2=Total' +
		'&H3=Labor' +
		'&H4=Fee' +
		'&H5=Consultant' +
		'&H6=Expense' +
		'&H7=Other' +
		'&FN=fnCCG_EI_JTDBilledDetails' +
		'&DS=' + @@SERVERNAME + '&DB=' + DB_NAME()
		from CFGSystem
	return @res
END
GO
