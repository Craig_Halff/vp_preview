SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[rpProjDetExp]
  (
   @company as Nvarchar(4),@ETCDate as datetime,@rptWBSListKey as varchar(32),
   @includeFlags as smallint,
   @wbs1Activity varchar(1) = 'Y',
   @wbs2Activity varchar(1) = 'Y',
   @wbs3Activity varchar(1) = 'Y'
  )
returns @t table
/* 
NOTE: @ETCDate should be the client date entered. down below, 1 day is added to the @ETCDate to get the actual start of etc calculations.
So, if the client types 12/13/06 into the ETC date basis selection, 12/13/06 should be passed in here. From here on, it gets changed to 12/14/06.
*/

--declare @t table
(
wbs1 Nvarchar(30) COLLATE database_default,
wbs2 Nvarchar(7) COLLATE database_default,
wbs3 Nvarchar(7) COLLATE database_default,
laborcode Nvarchar(14) COLLATE database_default,
recordtype char(1) COLLATE database_default,
subtype char(1) COLLATE database_default,
account Nvarchar(13) COLLATE database_default,
vendor Nvarchar(20) COLLATE database_default,
unit Nvarchar(30) COLLATE database_default,
pctcompleteformula smallint,
targetmultcost decimal(19,4),
targetmultbill decimal(19,4),
plannedrevenue decimal(19,4),
baselinerevenue decimal(19,4),
plannedhrs decimal (19,4),
baselinehrs decimal(19,4),
plannedcost decimal(19,4),
plannedbill decimal (19,4),
baselinecost decimal (19,4),
baselinebill decimal (19,4),
pctcmpplannedcost decimal (19,4),
pctcmpplannedbill decimal (19,4),
pctcmpbaselinecost decimal (19,4),
pctcmpbaselinebill decimal (19,4),
pctcmpplanhrscost decimal (19,4),
pctcmpplanhrsbill decimal (19,4),
pctcmpbasehrscost decimal (19,4),
pctcmpbasehrsbill decimal (19,4),
etcrev decimal (19,4),
etchrs decimal (19,4),
etccost decimal (19,4),
etcbill decimal (19,4)
primary key (wbs1,wbs2,wbs3,laborcode,recordtype,subtype,account,vendor,unit)
)
begin 

/*
declare @company as Nvarchar(4);
declare @etcDate as datetime;
declare @rptWBSListKey as varchar(32);
declare @includeFlags as smallint
set @company = ' ';
set @etcDate = getDate();
set @rptWBSListKey = 'test';
set @includeFlags = 127;
*/
set @etcDate = dateadd(d,1,@etcDate)
declare @rpPrintDirects as smallint,@rpPrintDirectCons as smallint,@rpPrintReimbs As smallint,@rpPrintReimbCons as smallint,@rpPrintIndirects as smallint
declare @rpPrintUnits as smallint;
declare @expTab as smallint,@conTab as smallint,@untTab as smallint;

select 
@expTab = case when expTab = 'Y' then 1 else 0 end, 
@conTab = case when conTab = 'Y' then 1 else 0 end, 
@untTab = case when untTab = 'Y' then 1 else 0 end 
from cfgresourceplanning where company = @company;

select @rpPrintDirects = 0,@rpPrintDirectCons = 0,@rpPrintReimbs = 0,@rpPrintReimbCons = 0,@rpPrintIndirects = 0;
if (@includeFlags >= 16) 
 begin
 set @rpPrintIndirects = 1
 set @includeFlags = @includeFlags - 16
 end
if (@includeFlags >= 8)
 begin
 set @rpPrintReimbCons = 1
 set @includeFlags = @includeFlags - 8
 end
if (@includeFlags >= 4)
 begin
 set @rpPrintReimbs = 1
 set @includeFlags = @includeFlags - 4
 end
if (@includeFlags >= 2)
 begin
 set @rpPrintDirectCons = 1
 set @includeFlags = @includeFlags - 2
 end
if (@includeFlags >= 1)
 begin
 set @rpPrintDirects = 1
 set @includeFlags = @includeFlags - 1
 end
--@includeFlags should be 0 at this point

declare @wbsActivityLevel as smallint
set @wbsActivityLevel = 3
if (@wbs1Activity = 'Y')
  begin
  set @wbsActivityLevel = 1
  end

if (@wbs2Activity = 'Y')
  begin
  set @wbsActivityLevel = 2
  end

if (@wbs3Activity = 'Y')
  begin
  set @wbsActivityLevel = 3
  end

if (@rpPrintReimbs > 0) set @rpPrintReimbs = @expTab
if (@rpPrintDirects > 0) set @rpPrintDirects = @expTab
if (@rpPrintIndirects > 0) set @rpPrintIndirects = @expTab
if (@rpPrintReimbCons > 0) set @rpPrintReimbCons = @conTab
if (@rpPrintDirectCons > 0) set @rpPrintDirectCons = @conTab
set @rpPrintUnits = 0
if (@rpPrintReimbs + @rpPrintDirects + @rpPrintIndirects + @rpPRintReimbCons + @rpPRintDirectCons > 0) set @rpPrintUnits = @untTab

insert into @t
select wbs1,
case when wbs2 = '<none>' then '' else wbs2 end wbs2,
case when wbs3 = '<none>' then '' else wbs3 end wbs3,laborcode,
recordtype,subtype,account,vendor,unit,max(pctcompleteformula) pctcompleteformula,
max(targetmultcost) targetmultcost,max(targetmultbill) targetmultbill,
sum(case when recordtype = '3' then case when reimbmethod = 'C' then plancost else planbill end else 0 end) planrevenue,
sum(case when recordtype = '3' then case when reimbmethod = 'C' then basecost else basebill end else 0 end) baserevenue,
sum(planhrs) planhrs,sum(basehrs) basehrs,
sum(plancost) plancost,sum(planbill) planbill,
sum(basecost) basecost,sum(basebill) basebill,
sum(pctcmpplancost) pctcmpplancost, sum(pctcmpplanbill) pctcmpplanbill,
sum(pctcmpbasecost) pctcmpbasecost, sum(pctcmpbasebill) pctcmpbasebill,
sum(pctcmpplanhrscost) pctcmpplanhrscost, sum(pctcmpplanhrsbill) pctcmpplanhrsbill,
sum(pctcmpbasehrscost) pctcmpbasehrscost, sum(pctcmpbasehrsbill) pctcmpbasehrsbill,
sum(case when recordtype = '3' then case when reimbmethod = 'C' then etcplancost else etcplanbill end else 0 end) etcrev,
sum(etcplanhrs) etcplanhrs,
sum(etcplancost) etcplancost,
sum(etcplanbill) etcplanbill
from (

--expense, Direct, Cons, tasks w/exp resources (cons tab)
select isnull(t.wbs1,'') wbs1,isnull(level2.wbs2,'') wbs2,isnull(level3.wbs3,'') wbs3,isnull(t.laborcode,'') laborcode,
case when res.directacctflg <> 'Y' then '5' else '3' end recordtype,'0' subtype,
isnull(res.account,'') account,isnull(res.vendor,'') vendor,'' unit,
max(p.PctcompleteFormula) PctcompleteFormula,
max(p.targetmultcost) targetmultcost,
max(p.targetmultbill) targetmultbill,
max(p.reimbmethod) reimbmethod,
0 planhrs,0 basehrs,
sum(tpd.periodcost * tpd.periodcount) plancost, 
sum(tpd.periodbill * tpd.periodcount) planbill, 
0 basecost, 
0 basebill, 
sum(tpd.periodcost * tpd.periodcount * res.pctcompleteconcost) pctcmpplancost, 
sum(tpd.periodbill * tpd.periodcount * res.pctcompleteconbill) pctcmpplanbill, 
0 pctcmpbasecost, 
0 pctcmpbasebill,
0 pctcmpplanhrscost,0 pctcmpplanhrsbill,0 pctcmpbasehrscost,0 pctcmpbasehrsbill,0 etcplanhrs,
sum(TPD.PeriodCost * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplancost,
sum(TPD.PeriodBill * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplanbill
from rpplan p, {oj {oj rptask t left outer join pr as level2 on t.wbs1 = level2.wbs1 and t.wbs2 = level2.wbs2 and level2.wbs3 = ''}
left outer join pr as level3 on t.wbs1 = level3.wbs1 and t.wbs2 = level3.wbs2 and t.wbs3 = level3.wbs3}
, rpconsultant res, reportWBSList wbs,
rpplannedconsultant tpd
where p.planid = t.planid
and p.utilizationincludeflg = 'Y'
and t.planid = res.planid and t.taskid = res.taskid
and res.consultantid is not null
and tpd.planid = t.planid and tpd.taskid = t.taskid
and tpd.consultantid = res.consultantid
and isnull(t.wbs1,'') > ''
and ((res.directacctflg = 'Y' and @rpPrintDirectCons <> 0) or (res.directacctflg <> 'Y' and @rpPrintReimbCons <> 0))
and ((@wbsActivityLevel=3 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and isnull(level3.wbs3,'') = wbs.wbs3)
or (@wbsActivityLevel=2 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and wbs.wbs3='')
or (@wbsActivityLevel=1 and isnull(t.wbs1,'') = wbs.wbs1 and wbs.wbs2='' and wbs.wbs3=''))
and wbs.sessionId = @rptWBSListKey
group by isnull(t.wbs1,''),isnull(level2.wbs2,''),isnull(level3.wbs3,''),isnull(t.laborcode,''),isnull(res.account,''),isnull(res.vendor,'')
,case when res.directacctflg <> 'Y' then '5' else '3' end
union all
select isnull(t.wbs1,'') wbs1,isnull(level2.wbs2,'') wbs2,isnull(level3.wbs3,'') wbs3,isnull(t.laborcode,'') laborcode,
case when res.directacctflg <> 'Y' then '5' else '3' end recordtype,'0' subtype,
isnull(res.account,'') account,isnull(res.vendor,'') vendor,'' unit,
max(p.PctcompleteFormula) PctcompleteFormula,
max(p.targetmultcost) targetmultcost,
max(p.targetmultbill) targetmultbill,
max(p.reimbmethod) reimbmethod,
0 planhrs,0 basehrs,
0 plancost, 
0 planbill, 
sum(res.baselineconcost) basecost, 
sum(res.baselineconbill) basebill, 
0 pctcmpplancost, 
0 pctcmpplanbill,
sum(res.baselineconcost * res.pctcompleteconcost) pctcmpbasecost, 
sum(res.baselineconbill * res.pctcompleteconbill) pctcmpbasebill, 
0 pctcmpplanhrscost,0 pctcmpplanhrsbill,0 pctcmpbasehrscost,0 pctcmpbasehrsbill,0 etcplanhrs,
0 etcplancost,
0 etcplanbill
from rpplan p, {oj {oj rptask t left outer join pr as level2 on t.wbs1 = level2.wbs1 and t.wbs2 = level2.wbs2 and level2.wbs3 = ''}
left outer join pr as level3 on t.wbs1 = level3.wbs1 and t.wbs2 = level3.wbs2 and t.wbs3 = level3.wbs3}
,rpconsultant res, reportWBSList wbs
where p.planid = t.planid
and p.utilizationincludeflg = 'Y'
and t.planid = res.planid and t.taskid = res.taskid
and res.consultantid is not null
and isnull(t.wbs1,'') > ''
and ((res.directacctflg = 'Y' and @rpPrintDirectCons <> 0) or (res.directacctflg <> 'Y' and @rpPrintReimbCons <> 0))
and ((@wbsActivityLevel=3 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and isnull(level3.wbs3,'') = wbs.wbs3)
or (@wbsActivityLevel=2 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and wbs.wbs3='')
or (@wbsActivityLevel=1 and isnull(t.wbs1,'') = wbs.wbs1 and wbs.wbs2='' and wbs.wbs3=''))
and wbs.sessionId = @rptWBSListKey
group by isnull(t.wbs1,''),isnull(level2.wbs2,''),isnull(level3.wbs3,''),isnull(t.laborcode,''),isnull(res.account,''),isnull(res.vendor,'')
,case when res.directacctflg <> 'Y' then '5' else '3' end

union all
--expense, Direct, Non-Cons, tasks w/exp resources (exp tab)
select isnull(t.wbs1,'') wbs1,isnull(level2.wbs2,'') wbs2,isnull(level3.wbs3,'') wbs3,isnull(t.laborcode,'') laborcode,
case when res.directacctflg <> 'Y' then '5' else '3' end recordtype,'1' subtype,
isnull(res.account,'') account,isnull(res.vendor,'') vendor,'' unit,
max(p.PctcompleteFormula) PctcompleteFormula,
max(p.targetmultcost) targetmultcost,
max(p.targetmultbill) targetmultbill,
max(p.reimbmethod) reimbmethod,
0 planhrs,0 basehrs,
sum(tpd.periodcost * tpd.periodcount) plancost, 
sum(tpd.periodbill * tpd.periodcount) planbill, 
0 basecost, 
0 basebill, 
sum(tpd.periodcost * tpd.periodcount * res.pctcompleteexpcost) pctcmpplancost, 
sum(tpd.periodbill * tpd.periodcount * res.pctcompleteexpbill) pctcmpplanbill, 
0 pctcmpbasecost, 
0 pctcmpbasebill,
0 pctcmpplanhrscost,0 pctcmpplanhrsbill,0 pctcmpbasehrscost,0 pctcmpbasehrsbill,0 etcplanhrs,
sum(TPD.PeriodCost * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplancost,
sum(TPD.PeriodBill * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplanbill
from rpplan p, {oj {oj rptask t left outer join pr as level2 on t.wbs1 = level2.wbs1 and t.wbs2 = level2.wbs2 and level2.wbs3 = ''}
left outer join pr as level3 on t.wbs1 = level3.wbs1 and t.wbs2 = level3.wbs2 and t.wbs3 = level3.wbs3}
, rpexpense res, reportWBSList wbs,
rpplannedexpenses tpd
where p.planid = t.planid
and p.utilizationincludeflg = 'Y'
and t.planid = res.planid and t.taskid = res.taskid
and res.expenseid is not null
and tpd.planid = t.planid and tpd.taskid = t.taskid
and tpd.expenseid = res.expenseid
and isnull(t.wbs1,'') > ''
and ((@wbsActivityLevel=3 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and isnull(level3.wbs3,'') = wbs.wbs3)
or (@wbsActivityLevel=2 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and wbs.wbs3='')
or (@wbsActivityLevel=1 and isnull(t.wbs1,'') = wbs.wbs1 and wbs.wbs2='' and wbs.wbs3=''))
and ((res.directacctflg = 'Y' and (@rpPrintDirects <> 0 or @rpPrintIndirects <> 0)) or (res.directacctflg <> 'Y' and (@rpPrintReimbs <> 0)))
and wbs.sessionId = @rptWBSListKey
group by isnull(t.wbs1,''),isnull(level2.wbs2,''),isnull(level3.wbs3,''),isnull(t.laborcode,''),isnull(res.account,''),isnull(res.vendor,'')
,case when res.directacctflg <> 'Y' then '5' else '3' end
union all
select isnull(t.wbs1,'') wbs1,isnull(level2.wbs2,'') wbs2,isnull(level3.wbs3,'') wbs3,isnull(t.laborcode,'') laborcode,
case when res.directacctflg <> 'Y' then '5' else '3' end recordtype,'1' subtype,
isnull(res.account,'') account,isnull(res.vendor,'') vendor,'' unit,
max(p.PctcompleteFormula) PctcompleteFormula,
max(p.targetmultcost) targetmultcost,
max(p.targetmultbill) targetmultbill,
max(p.reimbmethod) reimbmethod,
0 planhrs,0 basehrs,
0 plancost, 
0 planbill, 
sum(res.baselineexpcost) basecost, 
sum(res.baselineexpbill) basebill, 
0 pctcmpplancost, 
0 pctcmpplanbill,
sum(res.baselineexpcost * res.pctcompleteexpcost) pctcmpbasecost, 
sum(res.baselineexpbill * res.pctcompleteexpbill) pctcmpbasebill, 
0 pctcmpplanhrscost,0 pctcmpplanhrsbill,0 pctcmpbasehrscost,0 pctcmpbasehrsbill,0 etcplanhrs,
0 etcplancost,
0 etcplanbill
from rpplan p, {oj {oj rptask t left outer join pr as level2 on t.wbs1 = level2.wbs1 and t.wbs2 = level2.wbs2 and level2.wbs3 = ''}
left outer join pr as level3 on t.wbs1 = level3.wbs1 and t.wbs2 = level3.wbs2 and t.wbs3 = level3.wbs3}
, rpexpense res, reportWBSList wbs
where p.planid = t.planid
and p.utilizationincludeflg = 'Y'
and t.planid = res.planid and t.taskid = res.taskid
and res.expenseid is not null
and isnull(t.wbs1,'') > ''
and ((@wbsActivityLevel=3 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and isnull(level3.wbs3,'') = wbs.wbs3)
or (@wbsActivityLevel=2 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and wbs.wbs3='')
or (@wbsActivityLevel=1 and isnull(t.wbs1,'') = wbs.wbs1 and wbs.wbs2='' and wbs.wbs3=''))
and ((res.directacctflg = 'Y' and (@rpPrintDirects <> 0 or @rpPrintIndirects <> 0)) or (res.directacctflg <> 'Y' and (@rpPrintReimbs <> 0)))
and wbs.sessionId = @rptWBSListKey
group by isnull(t.wbs1,''),isnull(level2.wbs2,''),isnull(level3.wbs3,''),isnull(t.laborcode,''),isnull(res.account,''),isnull(res.vendor,'')
,case when res.directacctflg <> 'Y' then '5' else '3' end

union all
--expense, Direct, Non-Cons, tasks w/exp resources (unt tab)
select isnull(t.wbs1,'') wbs1,isnull(level2.wbs2,'') wbs2,isnull(level3.wbs3,'') wbs3,isnull(t.laborcode,'') laborcode,
case when res.directacctflg <> 'Y' then '5' else '3' end recordtype,'1' subtype,
isnull(res.account,'') account,'' vendor,isnull(res.unit,'') unit,
max(p.PctcompleteFormula) PctcompleteFormula,
max(p.targetmultcost) targetmultcost,
max(p.targetmultbill) targetmultbill,
max(p.reimbmethod) reimbmethod,
0 planhrs,0 basehrs,
sum(tpd.periodcost * tpd.periodcount) plancost, 
sum(tpd.periodbill * tpd.periodcount) planbill, 
0 basecost, 
0 basebill, 
sum(tpd.periodcost * tpd.periodcount * res.pctcompleteuntcost) pctcmpplancost, 
sum(tpd.periodbill * tpd.periodcount * res.pctcompleteuntbill) pctcmpplanbill, 
0 pctcmpbasecost, 
0 pctcmpbasebill,
0 pctcmpplanhrscost,0 pctcmpplanhrsbill,0 pctcmpbasehrscost,0 pctcmpbasehrsbill,0 etcplanhrs,
sum(TPD.PeriodCost * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplancost,
sum(TPD.PeriodBill * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplanbill
from rpplan p, {oj {oj rptask t left outer join pr as level2 on t.wbs1 = level2.wbs1 and t.wbs2 = level2.wbs2 and level2.wbs3 = ''}
left outer join pr as level3 on t.wbs1 = level3.wbs1 and t.wbs2 = level3.wbs2 and t.wbs3 = level3.wbs3}
, rpunit res, reportWBSList wbs,
rpplannedunit tpd
where p.planid = t.planid
and p.utilizationincludeflg = 'Y'
and t.planid = res.planid and t.taskid = res.taskid
and res.unitid is not null
and tpd.planid = t.planid and tpd.taskid = t.taskid
and tpd.unitid = res.unitid
and isnull(t.wbs1,'') > ''
and ((@wbsActivityLevel=3 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and isnull(level3.wbs3,'') = wbs.wbs3)
or (@wbsActivityLevel=2 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and wbs.wbs3='')
or (@wbsActivityLevel=1 and isnull(t.wbs1,'') = wbs.wbs1 and wbs.wbs2='' and wbs.wbs3=''))
and @rpPrintUnits <> 0
and wbs.sessionId = @rptWBSListKey
group by isnull(t.wbs1,''),isnull(level2.wbs2,''),isnull(level3.wbs3,''),isnull(t.laborcode,''),isnull(res.account,''),isnull(res.unit,'')
,case when res.directacctflg <> 'Y' then '5' else '3' end
union all
select isnull(t.wbs1,'') wbs1,isnull(level2.wbs2,'') wbs2,isnull(level3.wbs3,'') wbs3,isnull(t.laborcode,'') laborcode,
case when res.directacctflg <> 'Y' then '5' else '3' end recordtype,'1' subtype,
isnull(res.account,'') account,'' vendor,isnull(res.unit,'') unit,
max(p.PctcompleteFormula) PctcompleteFormula,
max(p.targetmultcost) targetmultcost,
max(p.targetmultbill) targetmultbill,
max(p.reimbmethod) reimbmethod,
0 planhrs,0 basehrs,
0 plancost, 
0 planbill, 
sum(res.baselineuntcost) basecost, 
sum(res.baselineuntbill) basebill, 
0 pctcmpplancost, 
0 pctcmpplanbill,
sum(res.baselineuntcost * res.pctcompleteuntcost) pctcmpbasecost, 
sum(res.baselineuntbill * res.pctcompleteuntbill) pctcmpbasebill, 
0 pctcmpplanhrscost,0 pctcmpplanhrsbill,0 pctcmpbasehrscost,0 pctcmpbasehrsbill,0 etcplanhrs,
0 etcplancost,
0 etcplanbill
from rpplan p, {oj {oj rptask t left outer join pr as level2 on t.wbs1 = level2.wbs1 and t.wbs2 = level2.wbs2 and level2.wbs3 = ''}
left outer join pr as level3 on t.wbs1 = level3.wbs1 and t.wbs2 = level3.wbs2 and t.wbs3 = level3.wbs3}
, rpunit res, reportWBSList wbs
where p.planid = t.planid
and p.utilizationincludeflg = 'Y'
and t.planid = res.planid and t.taskid = res.taskid
and res.unitid is not null
and isnull(t.wbs1,'') > ''
and ((@wbsActivityLevel=3 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and isnull(level3.wbs3,'') = wbs.wbs3)
or (@wbsActivityLevel=2 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and wbs.wbs3='')
or (@wbsActivityLevel=1 and isnull(t.wbs1,'') = wbs.wbs1 and wbs.wbs2='' and wbs.wbs3=''))
and @rpPrintUnits <> 0
and wbs.sessionId = @rptWBSListKey
group by isnull(t.wbs1,''),isnull(level2.wbs2,''),isnull(level3.wbs3,''),isnull(t.laborcode,''),isnull(res.account,''),isnull(res.unit,'')
,case when res.directacctflg <> 'Y' then '5' else '3' end

union all
--expense, Direct, Cons, tasks w/o resources (cons tab)
select isnull(t.wbs1,'') wbs1,isnull(level2.wbs2,'') wbs2,isnull(level3.wbs3,'') wbs3,isnull(t.laborcode,'') laborcode,
'3' recordType,'0' subtype,
'' account,'' vendor,'' unit,
max(p.PctcompleteFormula) PctcompleteFormula,
max(p.targetmultcost) targetmultcost,
max(p.targetmultbill) targetmultbill,
max(p.reimbmethod) reimbmethod,
0 planhrs,0 basehrs,
sum(tpd.periodcost * tpd.periodcount) plancost, 
sum(tpd.periodbill * tpd.periodcount) planbill, 
0 basecost, 
0 basebill, 
sum(tpd.periodcost * tpd.periodcount * t.pctcompleteconcost) pctcmpplancost, 
sum(tpd.periodbill * tpd.periodcount * t.pctcompleteconbill) pctcmpplanbill, 
0 pctcmpbasecost, 
0 pctcmpbasebill,
0 pctcmpplanhrscost,0 pctcmpplanhrsbill,0 pctcmpbasehrscost,0 pctcmpbasehrsbill,0 etcplanhrs,
sum(TPD.PeriodCost * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplancost,
sum(TPD.PeriodBill * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplanbill
from rpplan p, {oj {oj rptask t left outer join pr as level2 on t.wbs1 = level2.wbs1 and t.wbs2 = level2.wbs2 and level2.wbs3 = ''}
left outer join pr as level3 on t.wbs1 = level3.wbs1 and t.wbs2 = level3.wbs2 and t.wbs3 = level3.wbs3}
, reportWBSList wbs,
rpplannedconsultant tpd
where p.planid = t.planid
and p.utilizationincludeflg = 'Y'
and t.conparentstate = 'N'
and tpd.planid = t.planid and tpd.taskid = t.taskid
and isnull(t.wbs1,'') > ''
and @rpPrintDirectCons <> 0
and ((@wbsActivityLevel=3 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and isnull(level3.wbs3,'') = wbs.wbs3)
or (@wbsActivityLevel=2 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and wbs.wbs3='')
or (@wbsActivityLevel=1 and isnull(t.wbs1,'') = wbs.wbs1 and wbs.wbs2='' and wbs.wbs3=''))
and wbs.sessionId = @rptWBSListKey
group by isnull(t.wbs1,''),isnull(level2.wbs2,''),isnull(level3.wbs3,''),isnull(t.laborcode,'')
union all
select isnull(t.wbs1,'') wbs1,isnull(level2.wbs2,'') wbs2,isnull(level3.wbs3,'') wbs3,isnull(t.laborcode,'') laborcode,
'3' recordType,'0' subtype,
'' account,'' vendor,'' unit,
max(p.PctcompleteFormula) PctcompleteFormula,
max(p.targetmultcost) targetmultcost,
max(p.targetmultbill) targetmultbill,
max(p.reimbmethod) reimbmethod,
0 planhrs,0 basehrs,
0 plancost, 
0 planbill, 
sum(t.baselineconcost) basecost, 
sum(t.baselineconbill) basebill, 
0 pctcmpplancost, 
0 pctcmpplanbill,
sum(t.baselineconcost * t.pctcompleteconcost) pctcmpbasecost, 
sum(t.baselineconbill * t.pctcompleteconbill) pctcmpbasebill, 
0 pctcmpplanhrscost,0 pctcmpplanhrsbill,0 pctcmpbasehrscost,0 pctcmpbasehrsbill,0 etcplanhrs,
0 etcplancost,
0 etcplanbill
from rpplan p, {oj {oj rptask t left outer join pr as level2 on t.wbs1 = level2.wbs1 and t.wbs2 = level2.wbs2 and level2.wbs3 = ''}
left outer join pr as level3 on t.wbs1 = level3.wbs1 and t.wbs2 = level3.wbs2 and t.wbs3 = level3.wbs3}
, reportWBSList wbs
where p.planid = t.planid
and p.utilizationincludeflg = 'Y'
and t.conparentstate = 'N'
and isnull(t.wbs1,'') > ''
and @rpPrintDirectCons <> 0
and ((@wbsActivityLevel=3 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and isnull(level3.wbs3,'') = wbs.wbs3)
or (@wbsActivityLevel=2 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and wbs.wbs3='')
or (@wbsActivityLevel=1 and isnull(t.wbs1,'') = wbs.wbs1 and wbs.wbs2='' and wbs.wbs3=''))
and wbs.sessionId = @rptWBSListKey
group by isnull(t.wbs1,''),isnull(level2.wbs2,''),isnull(level3.wbs3,''),isnull(t.laborcode,'')

union all
--expense, Direct, Non-Cons, tasks w/o exp resources (exp tab)
select isnull(t.wbs1,'') wbs1,isnull(level2.wbs2,'') wbs2,isnull(level3.wbs3,'') wbs3,isnull(t.laborcode,'') laborcode,
'3' recordType, '1' subtype,
'' account,'' vendor,'' unit,
max(p.PctcompleteFormula) PctcompleteFormula,
max(p.targetmultcost) targetmultcost,
max(p.targetmultbill) targetmultbill,
max(p.reimbmethod) reimbmethod,
0 planhrs,0 basehrs,
sum(tpd.periodcost * tpd.periodcount) plancost, 
sum(tpd.periodbill * tpd.periodcount) planbill, 
0 basecost, 
0 basebill, 
sum(tpd.periodcost * tpd.periodcount * t.pctcompleteexpcost) pctcmpplancost, 
sum(tpd.periodbill * tpd.periodcount * t.pctcompleteexpbill) pctcmpplanbill, 
0 pctcmpbasecost, 
0 pctcmpbasebill,
0 pctcmpplanhrscost,0 pctcmpplanhrsbill,0 pctcmpbasehrscost,0 pctcmpbasehrsbill,0 etcplanhrs,
sum(TPD.PeriodCost * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplancost,
sum(TPD.PeriodBill * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplanbill
from rpplan p, {oj {oj rptask t left outer join pr as level2 on t.wbs1 = level2.wbs1 and t.wbs2 = level2.wbs2 and level2.wbs3 = ''}
left outer join pr as level3 on t.wbs1 = level3.wbs1 and t.wbs2 = level3.wbs2 and t.wbs3 = level3.wbs3}
, reportWBSList wbs,
rpplannedexpenses tpd
where p.planid = t.planid
and p.utilizationincludeflg = 'Y'
and t.expparentstate = 'N'
and tpd.planid = t.planid and tpd.taskid = t.taskid
and isnull(t.wbs1,'') > ''
and ((@wbsActivityLevel=3 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and isnull(level3.wbs3,'') = wbs.wbs3)
or (@wbsActivityLevel=2 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and wbs.wbs3='')
or (@wbsActivityLevel=1 and isnull(t.wbs1,'') = wbs.wbs1 and wbs.wbs2='' and wbs.wbs3=''))
and (@rpPrintDirects <> 0 or @rpPrintIndirects <> 0)
and wbs.sessionId = @rptWBSListKey
group by isnull(t.wbs1,''),isnull(level2.wbs2,''),isnull(level3.wbs3,''),isnull(t.laborcode,'')
union all
select isnull(t.wbs1,'') wbs1,isnull(level2.wbs2,'') wbs2,isnull(level3.wbs3,'') wbs3,isnull(t.laborcode,'') laborcode,
'3' recordType, '1' subtype,
'' account,'' vendor,'' unit,
max(p.PctcompleteFormula) PctcompleteFormula,
max(p.targetmultcost) targetmultcost,
max(p.targetmultbill) targetmultbill,
max(p.reimbmethod) reimbmethod,
0 planhrs,0 basehrs,
0 plancost, 
0 planbill, 
sum(t.baselineexpcost) basecost, 
sum(t.baselineexpbill) basebill, 
0 pctcmpplancost, 
0 pctcmpplanbill,
sum(t.baselineexpcost * t.pctcompleteexpcost) pctcmpbasecost, 
sum(t.baselineexpbill * t.pctcompleteexpbill) pctcmpbasebill, 
0 pctcmpplanhrscost,0 pctcmpplanhrsbill,0 pctcmpbasehrscost,0 pctcmpbasehrsbill,0 etcplanhrs,
0 etcplancost,
0 etcplanbill
from rpplan p, {oj {oj rptask t left outer join pr as level2 on t.wbs1 = level2.wbs1 and t.wbs2 = level2.wbs2 and level2.wbs3 = ''}
left outer join pr as level3 on t.wbs1 = level3.wbs1 and t.wbs2 = level3.wbs2 and t.wbs3 = level3.wbs3}
, reportWBSList wbs
where p.planid = t.planid
and p.utilizationincludeflg = 'Y'
and t.expparentstate = 'N'
and isnull(t.wbs1,'') > ''
and ((@wbsActivityLevel=3 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and isnull(level3.wbs3,'') = wbs.wbs3)
or (@wbsActivityLevel=2 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and wbs.wbs3='')
or (@wbsActivityLevel=1 and isnull(t.wbs1,'') = wbs.wbs1 and wbs.wbs2='' and wbs.wbs3=''))
and (@rpPrintDirects <> 0 or @rpPrintIndirects <> 0)
and wbs.sessionId = @rptWBSListKey
group by isnull(t.wbs1,''),isnull(level2.wbs2,''),isnull(level3.wbs3,''),isnull(t.laborcode,'')

union all
--expense, Direct, Non-Cons, tasks w/o exp resources (unt tab)
select isnull(t.wbs1,'') wbs1,isnull(level2.wbs2,'') wbs2,isnull(level3.wbs3,'') wbs3,isnull(t.laborcode,'') laborcode,
'3' recordType, '1' subtype,
'' account,'' vendor,'' unit,
max(p.PctcompleteFormula) PctcompleteFormula,
max(p.targetmultcost) targetmultcost,
max(p.targetmultbill) targetmultbill,
max(p.reimbmethod) reimbmethod,
0 planhrs,0 basehrs,
sum(tpd.periodcost * tpd.periodcount) plancost, 
sum(tpd.periodbill * tpd.periodcount) planbill, 
0 basecost, 
0 basebill, 
sum(tpd.periodcost * tpd.periodcount * t.pctcompleteuntcost) pctcmpplancost, 
sum(tpd.periodbill * tpd.periodcount * t.pctcompleteuntbill) pctcmpplanbill, 
0 pctcmpbasecost, 
0 pctcmpbasebill,
0 pctcmpplanhrscost,0 pctcmpplanhrsbill,0 pctcmpbasehrscost,0 pctcmpbasehrsbill,0 etcplanhrs,
sum(TPD.PeriodCost * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplancost,
sum(TPD.PeriodBill * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplanbill
from rpplan p, {oj {oj rptask t left outer join pr as level2 on t.wbs1 = level2.wbs1 and t.wbs2 = level2.wbs2 and level2.wbs3 = ''}
left outer join pr as level3 on t.wbs1 = level3.wbs1 and t.wbs2 = level3.wbs2 and t.wbs3 = level3.wbs3}
, reportWBSList wbs,
rpplannedunit tpd
where p.planid = t.planid
and p.utilizationincludeflg = 'Y'
and t.untparentstate = 'N'
and tpd.planid = t.planid and tpd.taskid = t.taskid
and isnull(t.wbs1,'') > ''
and ((@wbsActivityLevel=3 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and isnull(level3.wbs3,'') = wbs.wbs3)
or (@wbsActivityLevel=2 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and wbs.wbs3='')
or (@wbsActivityLevel=1 and isnull(t.wbs1,'') = wbs.wbs1 and wbs.wbs2='' and wbs.wbs3=''))
and @rpPrintUnits <> 0
and wbs.sessionId = @rptWBSListKey
group by isnull(t.wbs1,''),isnull(level2.wbs2,''),isnull(level3.wbs3,''),isnull(t.laborcode,'')
union all
select isnull(t.wbs1,'') wbs1,isnull(level2.wbs2,'') wbs2,isnull(level3.wbs3,'') wbs3,isnull(t.laborcode,'') laborcode,
'3' recordType, '1' subtype,
'' account,'' vendor,'' unit,
max(p.PctcompleteFormula) PctcompleteFormula,
max(p.targetmultcost) targetmultcost,
max(p.targetmultbill) targetmultbill,
max(p.reimbmethod) reimbmethod,
0 planhrs,0 basehrs,
0 plancost, 
0 planbill, 
sum(t.baselineuntcost) basecost, 
sum(t.baselineuntbill) basebill, 
0 pctcmpplancost, 
0 pctcmpplanbill,
sum(t.baselineuntcost * t.pctcompleteuntcost) pctcmpbasecost, 
sum(t.baselineuntbill * t.pctcompleteuntbill) pctcmpbasebill, 
0 pctcmpplanhrscost,0 pctcmpplanhrsbill,0 pctcmpbasehrscost,0 pctcmpbasehrsbill,0 etcplanhrs,
0 etcplancost,
0 etcplanbill
from rpplan p, {oj {oj rptask t left outer join pr as level2 on t.wbs1 = level2.wbs1 and t.wbs2 = level2.wbs2 and level2.wbs3 = ''}
left outer join pr as level3 on t.wbs1 = level3.wbs1 and t.wbs2 = level3.wbs2 and t.wbs3 = level3.wbs3}
, reportWBSList wbs
where p.planid = t.planid
and p.utilizationincludeflg = 'Y'
and t.untparentstate = 'N'
and isnull(t.wbs1,'') > ''
and ((@wbsActivityLevel=3 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and isnull(level3.wbs3,'') = wbs.wbs3)
or (@wbsActivityLevel=2 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and wbs.wbs3='')
or (@wbsActivityLevel=1 and isnull(t.wbs1,'') = wbs.wbs1 and wbs.wbs2='' and wbs.wbs3=''))
and @rpPrintUnits <> 0
and wbs.sessionId = @rptWBSListKey
group by isnull(t.wbs1,''),isnull(level2.wbs2,''),isnull(level3.wbs3,''),isnull(t.laborcode,'')

) subquery
group by wbs1,
case when wbs2 = '<none>' then '' else wbs2 end,
case when wbs3 = '<none>' then '' else wbs3 end,laborcode,recordtype,subtype,account,vendor,unit


/*
select * from @t
order by wbs1,wbs2,wbs3
*/

return
end
GO
