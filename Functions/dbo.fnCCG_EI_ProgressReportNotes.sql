SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_ProgressReportNotes] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), @USERNAME varchar(32))
RETURNS varchar(512)
AS BEGIN
/*
	Copyright (c) 2020 EleVia Software.  All rights reserved.
	select dbo.[fnCCG_EI_ProgressReportNotes]('2003005.00', ' ', ' ','GRACEC')
*/
	declare @res varchar(512), @emp varchar(32), @txt varchar(max)
	select @emp=Employee from SEUser where Username=@USERNAME
	select @txt = left(dbo.fnCCG_ConvertHtml_LeaveNewLines(CustProgressReportNotes), 200) from ProjectCustomTabFields where wbs1 = @wbs1 and wbs2 = @wbs2 and wbs3 = @wbs3
	if isnull(@txt, '') = '' set @txt = '<None>'

	select @res = @txt+'@?T=ProjectCustomTabFields&FFL=Progress Report Notes&SP_PARAMS=1&SP=ProgressReportNotesUpdate' +
		'&F=CustProgressReportNotes' +
		'&SA=T' +
		'&SH=ALL' +
		'&E=' + @emp +
		'&WBS1=' + @WBS1 + '&WBS2=' + @WBS2 + '&WBS3=' + @WBS3
		from CFGSystem
	return @res
END
GO
