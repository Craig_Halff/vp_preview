SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$tabContract]
  (@strWBS1 Nvarchar(30))

  RETURNS @tabContract TABLE (
    TaskID varchar(32) COLLATE database_default, /* Only Plans, that were created from Navigator, have true TaskID */
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    Name Nvarchar(40) COLLATE database_default,
    FeeLabCost decimal(19,4),
    FeeLabBill decimal(19,4),
    FeeExpCost decimal(19,4),
    FeeExpBill decimal(19,4),
    FeeDirExpCost decimal(19,4),
    FeeDirExpBill decimal(19,4),
    FeeReimExpCost decimal(19,4),
    FeeReimExpBill decimal(19,4),
    FeeConCost decimal(19,4),
    FeeConBill decimal(19,4),
    FeeDirConCost decimal(19,4),
    FeeDirConBill decimal(19,4),
    FeeReimConCost decimal(19,4),
    FeeReimConBill decimal(19,4),
    FeeCompCost decimal(19,4),
    FeeCompBill decimal(19,4),
    FeeReimCost decimal(19,4),
    FeeReimBill decimal(19,4),
    FeeTotalCost decimal(19,4),
    FeeTotalBill decimal(19,4)
  )

BEGIN

  DECLARE @strCompany Nvarchar(14)

  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)

  DECLARE @strVorN char(1)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

  -- Declare Temp tables.

  DECLARE @tabWBSTree TABLE (
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    Name Nvarchar(40) COLLATE database_default,
    FeeLabCost decimal(19,4),
    FeeLabBill decimal(19,4),
    FeeDirExpCost decimal(19,4),
    FeeDirExpBill decimal(19,4),
    FeeReimExpCost decimal(19,4),
    FeeReimExpBill decimal(19,4),
    FeeDirConCost decimal(19,4),
    FeeDirConBill decimal(19,4),
    FeeReimConCost decimal(19,4),
    FeeReimConBill decimal(19,4)
    UNIQUE(WBS1, WBS2, WBS3)
  )

  DECLARE @tabMappedTask TABLE
    (PlanID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(7) COLLATE database_default,
     WBS3 Nvarchar(7) COLLATE database_default
     UNIQUE(PlanID, TaskID))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, AutoSumComp, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat     

  -- Save Company string for use later.
  
  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          
  -- Determine which set of tables need to be used.

  SET @strVorN = dbo.PN$PlanIs_VorN(@strWBS1)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabWBSTree (
    WBS1,
    WBS2,
    WBS3,
    Name,
    FeeLabCost,
    FeeLabBill,
    FeeDirExpCost,
    FeeDirExpBill,
    FeeReimExpCost,
    FeeReimExpBill,
    FeeDirConCost,
    FeeDirConBill,
    FeeReimConCost,
    FeeReimConBill
  )
    SELECT
      WBS1,
      WBS2,
      WBS3,
      Name,
      FeeDirLab AS FeeLabCost,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN FeeDirLabBillingCurrency ELSE FeeDirLab END) AS FeeLabBill,
      FeeDirExp AS FeeDirExpCost,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN FeeDirExpBillingCurrency ELSE FeeDirExp END) AS FeeDirExpBill,
      ReimbAllowExp AS FeeReimExpCost,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN ReimbAllowExpBillingCurrency ELSE ReimbAllowExp END) AS FeeReimExpBill,
      ConsultFee AS FeeDirConCost,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN ConsultFeeBillingCurrency ELSE ConsultFee END) AS FeeDirConBill,
      ReimbAllowCons AS FeeReimConCost,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN ReimbAllowConsBillingCurrency ELSE ReimbAllowCons END) AS FeeReimConBill
      FROM PR
      WHERE WBS1 = @strWBS1

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save RPTask/PNTask that are mapped to @strWBS1. There could be multiple Plans with UlilizationIncludeFlag = 'Y'

  IF (@strVorN = 'V')
    BEGIN

      INSERT @tabMappedTask (
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3
      )
        SELECT
          P.PlanID,
          T.TaskID,
          T.WBS1,
          ISNULL(T.WBS2, ' '),
          ISNULL(T.WBS3, ' ')
          FROM RPTask AS T
            INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND T.WBS1 = @strWBS1 AND
              ((T.WBSType = 'WBS1' AND T.WBS1 <> '<none>') OR
               (T.WBSType = 'WBS2' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>') OR
               (T.WBSType = 'WBS3' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>' AND T.WBS3 <> '<none>'))
            INNER JOIN @tabWBSTree AS WT ON
              T.WBS1 = WT.WBS1 AND ISNULL(T.WBS2, ' ') = WT.WBS2 AND ISNULL(T.WBS3, ' ') = WT.WBS3

    END /* End If (@strVorN = 'V') */
  ELSE IF (@strVorN = 'N')
    BEGIN

      INSERT @tabMappedTask (
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3
      )
        SELECT
          P.PlanID,
          T.TaskID,
          T.WBS1,
          ISNULL(T.WBS2, ' '),
          ISNULL(T.WBS3, ' ')
          FROM PNTask AS T
            INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND T.WBS1 = @strWBS1 AND
              ((T.WBSType = 'WBS1' AND T.WBS1 <> '<none>') OR
               (T.WBSType = 'WBS2' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>') OR
               (T.WBSType = 'WBS3' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>' AND T.WBS3 <> '<none>'))
            INNER JOIN @tabWBSTree AS WT ON
              T.WBS1 = WT.WBS1 AND ISNULL(T.WBS2, ' ') = WT.WBS2 AND ISNULL(T.WBS3, ' ') = WT.WBS3
 
   END /* End If (@strVorN = 'N') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabContract (
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    Name,
    FeeLabCost,
    FeeLabBill,
    FeeExpCost,
    FeeExpBill,
    FeeDirExpCost,
    FeeDirExpBill,
    FeeReimExpCost,
    FeeReimExpBill,
    FeeConCost,
    FeeConBill,
    FeeDirConCost,
    FeeDirConBill,
    FeeReimConCost,
    FeeReimConBill,
    FeeCompCost,
    FeeCompBill,
    FeeReimCost,
    FeeReimBill,
    FeeTotalCost,
    FeeTotalBill
  )
    SELECT
      X.TaskID AS TaskID,
      WT.WBS1 AS WBS1,
      WT.WBS2 AS WBS2,
      WT.WBS3 AS WBS3,
      WT.Name AS Name,
      WT.FeeLabCost AS FeeLabCost,
      WT.FeeLabBill AS FeeLabBill,
      (WT.FeeDirExpCost + WT.FeeReimExpCost) AS FeeExpCost,
      (WT.FeeDirExpBill + WT.FeeReimExpBill) AS FeeExpBill,
      WT.FeeDirExpCost AS FeeDirExpCost,
      WT.FeeDirExpBill AS FeeDirExpBill,
      WT.FeeReimExpCost AS FeeReimExpCost,
      WT.FeeReimExpBill AS FeeReimExpBill,
      (WT.FeeDirConCost + WT.FeeReimConCost) AS FeeConCost,
      (WT.FeeDirConBill + WT.FeeReimConBill) AS FeeConBill,
      WT.FeeDirConCost AS FeeDirConCost,
      WT.FeeDirConBill AS FeeDirConBill,
      WT.FeeReimConCost AS FeeReimConCost,
      WT.FeeReimConBill AS FeeReimConBill,
      (WT.FeeLabCost + WT.FeeDirExpCost) AS FeeCompCost,
      (WT.FeeLabBill + WT.FeeDirExpBill) AS FeeCompBill,
      (WT.FeeReimExpCost + WT.FeeReimConCost) AS FeeReimCost,
      (WT.FeeReimExpBill + WT.FeeReimConBill) AS FeeReimBill,
      (WT.FeeLabCost + WT.FeeDirExpCost + WT.FeeReimExpCost + WT.FeeDirConCost + WT.FeeReimConCost) AS FeeTotalCost,
      (WT.FeeLabBill + WT.FeeDirExpBill + WT.FeeReimExpBill + WT.FeeDirConBill + WT.FeeReimConBill) AS FeeTotalBill
      FROM @tabWBSTree AS WT LEFT JOIN
        (SELECT
           MIN(MT.TaskID) AS TaskID,
           MT.WBS1 AS WBS1,
           MT.WBS2 AS WBS2,
           MT.WBS3 AS WBS3
           FROM @tabMappedTask AS MT
           GROUP BY MT.WBS1, MT.WBS2, MT.WBS3
        ) AS X ON WT.WBS1 = X.WBS1 AND WT.WBS2 = X.WBS2 AND WT.WBS3 = X.WBS3

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
