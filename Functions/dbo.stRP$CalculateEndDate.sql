SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$CalculateEndDate](
  @strNewStartDate varchar(8),
  @strStartDate varchar(8),
  @strEndDate varchar(8),
  @strCompany nvarchar(14)
)
  RETURNS datetime

BEGIN

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
--
-- This Stored Procedure will determine the EndDate of a date range where the range is started at @strNewStartDate
-- and has a same duration (in number of work-days) as in the range of {@strStartDate, @strEndDate}.
--
-- Note that the range of {@strStartDate, @strEndDate} could have zero work-day, 1 work-day, more than 1 work-days, or negative work-days.
-- For example, 
--   1. A range of {'20190615', '20190616'} has zero work-day.
--   2. A range of {'20190615', '20190615'} has zero work-day.
--   3. A range of {'20190618', '20190618'} has one work-day.
--   4. A range of {'20190617', '20190618'} has two work-days.
--   5. A range of {'20190618', '20190617'} has negative-two work-days.
--
-- If @strNewStartDate is a non-work-day, for example '2019-06-15', then, the new EndDate is calculated as the following:
--   1. If @iNumWorkDays = 0, then @dtNewEndDate = 2019-06-15 00:00:00.000
--   2. If @iNumWorkDays = 1, then @dtNewEndDate = 2019-06-15 00:00:00.000
--   3. If @iNumWorkDays = 2, then @dtNewEndDate = 2019-06-18 00:00:00.000
--   4. If @iNumWorkDays = 6, then @dtNewEndDate = 2019-06-24 00:00:00.000
--   5. If @iNumWorkDays = -2, then @dtNewEndDate = 2019-06-13 00:00:00.000
--
-- If @strNewStartDate is a work-day, for example '2019-06-17', then, the new EndDate is calculated as the following:
--   1. If @iNumWorkDays = 0, then @dtNewEndDate = 2019-06-17 00:00:00.000
--   2. If @iNumWorkDays = 1, then @dtNewEndDate = 2019-06-17 00:00:00.000
--   3. If @iNumWorkDays = 2, then @dtNewEndDate = 2019-06-18 00:00:00.000
--   4. If @iNumWorkDays = 6, then @dtNewEndDate = 2019-06-24 00:00:00.000
--   5. If @iNumWorkDays = -2, then @dtNewEndDate = 2019-06-14 00:00:00.000
--
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @dtNewEndDate datetime

  DECLARE @iNumWorkDays int = dbo.DLTK$NumWorkingDays(@strStartDate, @strEndDate, @strCompany)

  SELECT 
    @dtNewEndDate = 
      CASE
        WHEN @iNumWorkDays = 0 THEN @strNewStartDate
        WHEN @iNumWorkDays < 0 THEN dbo.DLTK$AddBusinessDays(@strNewStartDate, @iNumWorkDays + 1, @strCompany)
        ELSE dbo.DLTK$AddBusinessDays(@strNewStartDate, @iNumWorkDays - 1, @strCompany)
      END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN(@dtNewEndDate)

END /* END FUNCTION stRP$CalculateEndDate */

GO
