SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[rpProjDetLab]
  (
   @company as Nvarchar(4),@ETCDate as datetime,@rptWBSListKey as varchar(32),
   @includeFlags as smallint,
   @wbs1Activity varchar(1) = 'Y',
   @wbs2Activity varchar(1) = 'Y',
   @wbs3Activity varchar(1) = 'Y'
  )
returns @t table
/* 
NOTE: @ETCDate should be the client date entered. down below, 1 day is added to the @ETCDate to get the actual start of etc calculations.
So, if the client types 12/13/06 into the ETC date basis selection, 12/13/06 should be passed in here. From here on, it gets changed to 12/14/06.
*/
--declare @t table
(
wbs1 Nvarchar(30) COLLATE database_default,
wbs2 Nvarchar(7) COLLATE database_default,
wbs3 Nvarchar(7) COLLATE database_default,
laborcode Nvarchar(14) COLLATE database_default,
datatype smallint,
recordtype char(1) COLLATE database_default,
subtype char(1) COLLATE database_default,
resourceid Nvarchar(32) COLLATE database_default,
category smallint,
pctcompleteformula smallint,
targetmultcost decimal(19,4),
targetmultbill decimal(19,4),
plannnedrevenue decimal(19,4),
baselinerevenue decimal(19,4),
plannedhrs decimal (19,4),
baselinehrs decimal(19,4),
plannedcost decimal(19,4),
plannedbill decimal (19,4),
baselinecost decimal (19,4),
baselinebill decimal (19,4),
pctcmpplannedcost decimal (19,4),
pctcmpplannedbill decimal (19,4),
pctcmpbaselinecost decimal (19,4),
pctcmpbaselinebill decimal (19,4),
pctcmpplanhrscost decimal (19,4),
pctcmpplanhrsbill decimal (19,4),
pctcmpbasehrscost decimal (19,4),
pctcmpbasehrsbill decimal (19,4),
etcrev decimal (19,4),
etchrs decimal (19,4),
etccost decimal (19,4),
etcbill decimal (19,4)
primary key (wbs1,wbs2,wbs3,laborcode,datatype,recordtype,subtype,resourceid,category)
)
begin 

/*
declare @company as Nvarchar(4);
declare @etcDate as datetime;
declare @rptWBSListKey as varchar(32);
declare @includeFlags as smallint
set @company = ' ';
set @etcDate = getDate();
set @rptWBSListKey = 'ADMIN5908200603385478575';
set @includeFlags = 1;
*/

set @etcDate = dateadd(d,1,@etcDate)
declare @seecostrates as smallint;
select @seecostrates=0;
if (@includeFlags >= 1) 
 begin
 set @seecostrates = 1
 set @includeFlags = @includeFlags - 1
 end
--@includeFlags should be 0 at this point

declare @wbsActivityLevel as smallint
set @wbsActivityLevel = 3
if (@wbs1Activity = 'Y')
  begin
  set @wbsActivityLevel = 1
  end

if (@wbs2Activity = 'Y')
  begin
  set @wbsActivityLevel = 2
  end

if (@wbs3Activity = 'Y')
  begin
  set @wbsActivityLevel = 3
  end

insert into @t
select wbs1,
case when wbs2 = '<none>' then '' else wbs2 end,
case when wbs3 = '<none>' then '' else wbs3 end,laborcode,
1 datatype,
recordtype,subtype,resourceid,category,max(pctcompleteformula) pctcompleteformula,
max(targetmultcost) targetmultcost,max(targetmultbill) targetmultbill,
sum(plancost * multiplier) planrevenue,sum(basecost * multiplier) baserevenue,
sum(planhrs) planhrs,sum(basehrs) basehrs,
sum(plancost) plancost,sum(planbill) planbill,
sum(basecost) basecost,sum(basebill) basebill,
sum(pctcmpplancost) pctcmpplancost, sum(pctcmpplanbill) pctcmpplanbill,
sum(pctcmpbasecost) pctcmpbasecost, sum(pctcmpbasebill) pctcmpbasebill,
sum(pctcmpplanhrscost) pctcmpplanhrscost, sum(pctcmpplanhrsbill) pctcmpplanhrsbill,
sum(pctcmpbasehrscost) pctcmpbasehrscost, sum(pctcmpbasehrsbill) pctcmpbasehrsbill,
sum(etcplancost * multiplier) etcrev,
sum(etcplanhrs) etcplanhrs,
sum(etcplancost) etcplancost,
sum(etcplanbill) etcplanbill
from (

--labor, tasks wits labor resources
select isnull(t.wbs1,'') wbs1,isnull(level2.wbs2,'') wbs2,isnull(level3.wbs3,'') wbs3,isnull(t.laborcode,'') laborcode,
'1' recordtype,' ' subtype, 
isnull(res.resourceid,'') resourceid,res.category,
max(p.PctcompleteFormula) PctcompleteFormula,
max(p.targetmultcost) targetmultcost,
max(p.targetmultbill) targetmultbill,
max(p.multiplier) multiplier,
sum(tpd.periodhrs * tpd.periodcount) planhrs,
0 basehrs,
sum(@seecostrates * tpd.periodcost * tpd.periodcount) plancost, 
sum(tpd.periodbill * tpd.periodcount) planbill, 
0 basecost, 
0 basebill, 
sum(@seecostrates * tpd.periodcost * tpd.periodcount * res.pctcompletelabcost) pctcmpplancost, 
sum(tpd.periodbill * tpd.periodcount * res.pctcompletelabbill) pctcmpplanbill, 
0 pctcmpbasecost, 
0 pctcmpbasebill,
sum(tpd.periodhrs * tpd.periodcount * res.pctcompletelabcost) pctcmpplanhrscost, 
sum(tpd.periodhrs * tpd.periodcount * res.pctcompletelabbill) pctcmpplanhrsbill, 
0 pctcmpbasehrscost, 
0 pctcmpbasehrsbill, 
sum(TPD.PeriodHrs  * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplanhrs,
sum(TPD.PeriodCost * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplancost,
sum(TPD.PeriodBill * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplanbill
from rpplan p, {oj {oj rptask t left outer join pr as level2 on t.wbs1 = level2.wbs1 and t.wbs2 = level2.wbs2 and level2.wbs3 = ''}
left outer join pr as level3 on t.wbs1 = level3.wbs1 and t.wbs2 = level3.wbs2 and t.wbs3 = level3.wbs3}
, rpassignment res, reportWBSList wbs,
rpplannedlabor tpd
where p.planid = t.planid
and p.utilizationincludeflg = 'Y'
and t.planid = res.planid and t.taskid = res.taskid
and res.assignmentid is not null
and tpd.planid = t.planid and tpd.taskid = t.taskid
and tpd.assignmentid = res.assignmentid
and isnull(t.wbs1,'') > ''
and ((@wbsActivityLevel=3 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and isnull(level3.wbs3,'') = wbs.wbs3)
or (@wbsActivityLevel=2 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and wbs.wbs3='')
or (@wbsActivityLevel=1 and isnull(t.wbs1,'') = wbs.wbs1 and wbs.wbs2='' and wbs.wbs3=''))
and wbs.sessionId = @rptWBSListKey
group by isnull(t.wbs1,''),isnull(level2.wbs2,''),isnull(level3.wbs3,''),isnull(t.laborcode,''),isnull(res.resourceid,''),res.category
union all
select isnull(t.wbs1,'') wbs1,isnull(level2.wbs2,'') wbs2,isnull(level3.wbs3,'') wbs3,isnull(t.laborcode,'') laborcode,
'1' recordtype,' ' subtype, 
isnull(res.resourceid,'') resourceid,res.category,
max(p.PctcompleteFormula) PctcompleteFormula,
max(p.targetmultcost) targetmultcost,
max(p.targetmultbill) targetmultbill,
max(p.multiplier) multiplier,
0 planhrs,
sum(res.baselinelaborhrs) basehrs,
0 plancost, 
0 planbill, 
sum(@seecostrates * res.baselinelabcost) basecost, 
sum(res.baselinelabbill) basebill, 
0 pctcmpplancost, 
0 pctcmpplanbill,
sum(@seecostrates * res.baselinelabcost * res.pctcompletelabcost) pctcmpbasecost, 
sum(res.baselinelabbill * res.pctcompletelabbill) pctcmpbasebill, 
0 pctcmpplanhrscost, 
0 pctcmpplanhrsbill, 
sum(res.baselinelaborhrs * res.pctcompletelabcost) pctcmpbasehrscost, 
sum(res.baselinelaborhrs * res.pctcompletelabbill) pctcmpbasehrsbill, 
0 etcplanhrs,
0 etcplancost,
0 etcplanbill
from rpplan p, {oj {oj rptask t left outer join pr as level2 on t.wbs1 = level2.wbs1 and t.wbs2 = level2.wbs2 and level2.wbs3 = ''}
left outer join pr as level3 on t.wbs1 = level3.wbs1 and t.wbs2 = level3.wbs2 and t.wbs3 = level3.wbs3}
, rpassignment res, reportWBSList wbs
where p.planid = t.planid
and p.utilizationincludeflg = 'Y'
and t.planid = res.planid and t.taskid = res.taskid
and res.assignmentid is not null
and isnull(t.wbs1,'') > ''
and ((@wbsActivityLevel=3 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and isnull(level3.wbs3,'') = wbs.wbs3)
or (@wbsActivityLevel=2 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and wbs.wbs3='')
or (@wbsActivityLevel=1 and isnull(t.wbs1,'') = wbs.wbs1 and wbs.wbs2='' and wbs.wbs3=''))
and wbs.sessionId = @rptWBSListKey
group by isnull(t.wbs1,''),isnull(level2.wbs2,''),isnull(level3.wbs3,''),isnull(t.laborcode,''),isnull(res.resourceid,''),res.category

union all
--labor, tasks w/o labor resources
select isnull(t.wbs1,'') wbs1,isnull(level2.wbs2,'') wbs2,isnull(level3.wbs3,'') wbs3,isnull(t.laborcode,'') laborcode,
'1' recordtype,' ' subtype, 
'' resourceid,0 category,
max(p.PctcompleteFormula) PctcompleteFormula,
max(p.targetmultcost) targetmultcost,
max(p.targetmultbill) targetmultbill,
max(p.multiplier) multiplier,
sum(tpd.periodhrs * tpd.periodcount) planhrs,
0 basehrs,
sum(@seecostrates * tpd.periodcost * tpd.periodcount) plancost, 
sum(tpd.periodbill * tpd.periodcount) planbill, 
0 basecost, 
0 basebill, 
sum(@seecostrates * tpd.periodcost * tpd.periodcount * t.pctcompletelabcost) pctcmpplancost, 
sum(tpd.periodbill * tpd.periodcount * t.pctcompletelabbill) pctcmpplanbill, 
0 pctcmpbasecost, 
0 pctcmpbasebill,
sum(tpd.periodhrs * tpd.periodcount * t.pctcompletelabcost) pctcmpplanhrscost, 
sum(tpd.periodhrs * tpd.periodcount * t.pctcompletelabbill) pctcmpplanhrsbill, 
0 pctcmpbasehrscost, 
0 pctcmpbasehrsbill, 
sum(TPD.PeriodHrs  * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplanhrs,
sum(TPD.PeriodCost * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplancost,
sum(TPD.PeriodBill * TPD.PeriodCount * case when TPD.enddate < @etcDate then 0 when TPD.startDate > @etcDate then 1 else dbo.DLTK$ProrateRatio(@etcDate,TPD.enddate,TPD.startdate,TPD.enddate,@company) end) etcplanbill
from rpplan p, {oj {oj rptask t left outer join pr as level2 on t.wbs1 = level2.wbs1 and t.wbs2 = level2.wbs2 and level2.wbs3 = ''}
left outer join pr as level3 on t.wbs1 = level3.wbs1 and t.wbs2 = level3.wbs2 and t.wbs3 = level3.wbs3}
, reportWBSList wbs,
rpplannedlabor tpd
where p.planid = t.planid
and p.utilizationincludeflg = 'Y'
and t.labparentstate = 'N'
and tpd.planid = t.planid and tpd.taskid = t.taskid
and isnull(t.wbs1,'') > ''
and ((@wbsActivityLevel=3 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and isnull(level3.wbs3,'') = wbs.wbs3)
or (@wbsActivityLevel=2 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and wbs.wbs3='')
or (@wbsActivityLevel=1 and isnull(t.wbs1,'') = wbs.wbs1 and wbs.wbs2='' and wbs.wbs3=''))
and wbs.sessionId = @rptWBSListKey
group by isnull(t.wbs1,''),isnull(level2.wbs2,''),isnull(level3.wbs3,''),isnull(t.laborcode,'')
union all
select isnull(t.wbs1,'') wbs1,isnull(level2.wbs2,'') wbs2,isnull(level3.wbs3,'') wbs3,isnull(t.laborcode,'') laborcode,
'1' recordtype,' ' subtype, 
'' resourceid,0 category,
max(p.PctcompleteFormula) PctcompleteFormula,
max(p.targetmultcost) targetmultcost,
max(p.targetmultbill) targetmultbill,
max(p.multiplier) multiplier,
0 planhrs,
sum(t.baselinelaborhrs) basehrs,
0 plancost, 
0 planbill, 
sum(@seecostrates * t.baselinelabcost) basecost, 
sum(t.baselinelabbill) basebill, 
0 pctcmpplancost, 
0 pctcmpplanbill,
sum(@seecostrates * t.baselinelabcost * t.pctcompletelabcost) pctcmpbasecost, 
sum(t.baselinelabbill * t.pctcompletelabbill) pctcmpbasebill, 
0 pctcmpplanhrscost, 
0 pctcmpplanhrsbill, 
sum(t.baselinelaborhrs * t.pctcompletelabcost) pctcmpbasehrscost, 
sum(t.baselinelaborhrs * t.pctcompletelabbill) pctcmpbasehrsbill, 
0 etcplanhrs,
0 etcplancost,
0 etcplanbill
from rpplan p, {oj {oj rptask t left outer join pr as level2 on t.wbs1 = level2.wbs1 and t.wbs2 = level2.wbs2 and level2.wbs3 = ''}
left outer join pr as level3 on t.wbs1 = level3.wbs1 and t.wbs2 = level3.wbs2 and t.wbs3 = level3.wbs3}
, reportWBSList wbs
where p.planid = t.planid
and p.utilizationincludeflg = 'Y'
and t.labparentstate = 'N'
and isnull(t.wbs1,'') > ''
and ((@wbsActivityLevel=3 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and isnull(level3.wbs3,'') = wbs.wbs3)
or (@wbsActivityLevel=2 and isnull(t.wbs1,'') = wbs.wbs1 and isnull(level2.wbs2,'') = wbs.wbs2 and wbs.wbs3='')
or (@wbsActivityLevel=1 and isnull(t.wbs1,'') = wbs.wbs1 and wbs.wbs2='' and wbs.wbs3=''))
and wbs.sessionId = @rptWBSListKey
group by isnull(t.wbs1,''),isnull(level2.wbs2,''),isnull(level3.wbs3,''),isnull(t.laborcode,'')

) subquery
group by wbs1,
case when wbs2 = '<none>' then '' else wbs2 end,
case when wbs3 = '<none>' then '' else wbs3 end,laborcode,recordtype,subtype,resourceid,category

/*
select * from @t
order by wbs1,wbs2,wbs3
*/

return
end
GO
