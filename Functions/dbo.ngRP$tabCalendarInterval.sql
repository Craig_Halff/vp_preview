SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ngRP$tabCalendarInterval](
  @strScopeStartDate varchar(8) /* Date must be in format: 'yyyymmdd' regardless of UI Culture */,
  @strScale varchar(1) /* {d, w, m} */,
  @intIntervalCount integer
)
  RETURNS @tabCalendarInterval TABLE(
    SeqID int IDENTITY(1,1),
    StartDate datetime,
    EndDate datetime,
    Scale varchar(1) COLLATE database_default, 
    NumWorkingDays integer,
    Heading varchar(255) COLLATE database_default
  )

BEGIN

  DECLARE @dtScopeStartDate datetime

  DECLARE @dtCIStartDate datetime
  DECLARE @dtCIEndDate datetime
 
  DECLARE @strCompany Nvarchar(14)

  DECLARE @intWkEndDay AS integer

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Validating @strScale and @strScopeStartDate.

  IF (@strScale IS NULL OR DATALENGTH(@strScale) > 1 OR (NOT(UPPER(@strScale) IN ('D', 'W', 'M')))) RETURN

  IF (@strScopeStartDate IS NULL OR (ISDATE(@strScopeStartDate) = 0)) RETURN

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Set Company to non-Multi-Company.
  
  SELECT @strCompany = dbo.GetActiveCompany()

  -- Get StartingDayOfWeek from Configuration Settings.

    SELECT
      @intWkEndDay = (CASE WHEN StartingDayOfWeek = 1 THEN 7 ELSE StartingDayOfWeek - 1 END)
      FROM CFGResourcePlanning Where Company = @strCompany

  -- Set Dates.
  
  SELECT @dtScopeStartDate = CONVERT(datetime, @strScopeStartDate)

  -- Calculate StartDate and EndDate of the Calendar.

  SELECT @dtCIStartDate = 
    CASE @strScale
      WHEN 'd' THEN @dtScopeStartDate
      WHEN 'w' THEN DATEADD(DAY, -6, dbo.DLTK$NextDay(@dtScopeStartDate, @intWkEndDay))
      WHEN 'm' THEN DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtScopeStartDate), 0)
    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @intCount integer = 1;

  WHILE @intCount <= @intIntervalCount
    BEGIN

      IF (@strScale = 'd') 
        SET @dtCIEndDate = @dtCIStartDate
      ELSE
        SET @dtCIEndDate = dbo.DLTK$IntervalEnd(@dtCIStartDate, @strScale, @intWkEndDay)
        
      INSERT @tabCalendarInterval(
        StartDate, 
        EndDate, 
        Scale, 
        NumWorkingDays,
        Heading
      )
        SELECT
          @dtCIStartDate AS StartDate,
          @dtCIEndDate AS EndDate,
          @strScale AS Scale,
          dbo.DLTK$NumWorkingDays(@dtCIStartDate, @dtCIEndDate, @strCompany) AS NumWorkingDays,
          CASE 
            WHEN YEAR(@dtCIStartDate) = YEAR(@dtCIEndDate) 
              THEN DATENAME(yyyy, @dtCIStartDate) + '<br />' + CONVERT(VARCHAR(5), @dtCIStartDate, 101)  + ' - ' +  CONVERT(VARCHAR(5), @dtCIEndDate, 101) 
            WHEN YEAR(@dtCIStartDate) != YEAR(@dtCIEndDate) 
              THEN DATENAME(yyyy, @dtCIStartDate) + ' - ' + DATENAME(yyyy, @dtCIEndDate) + '<br />' + CONVERT(VARCHAR(5), @dtCIStartDate, 101)  + ' - ' +  CONVERT(VARCHAR(5), @dtCIEndDate, 101) 
          END AS Heading
          
      -- Set @dtStartDate and @intCount for next interval.
          
      SET @dtCIStartDate = DATEADD(d, 1, @dtCIEndDate)
      SET @intCount = @intCount + 1;

    END;

/* "2016<br />3/1 - 3/31" */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
