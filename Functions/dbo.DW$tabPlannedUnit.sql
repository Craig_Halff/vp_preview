SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DW$tabPlannedUnit]
  (@strCompany nvarchar(14) = ' ',
   @strScale varchar(1) = 'm')
  RETURNS @tabPlannedUnit TABLE
    (PlanID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     WBS1 nvarchar(30) COLLATE database_default,
     WBS2 nvarchar(30) COLLATE database_default, 
     WBS3 nvarchar(30) COLLATE database_default,
     Account nvarchar(13) COLLATE database_default,
     Unit nvarchar(30) COLLATE database_default,
     UnitTable nvarchar(30) COLLATE database_default,
     TransactionDate int,
     Quantity decimal(19,4),
     CostAmount decimal(19,4),
     BillAmount decimal(19,4),
     RevenueAmount decimal(19,4)
    )
BEGIN -- Function DW$tabPlannedUnit
 
  DECLARE @dtStartDate datetime
  DECLARE @dtEndDate datetime
  DECLARE @dtIntervalEnd datetime
  
  DECLARE @intWkEndDay AS int
  
  DECLARE @intQtyDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intUntRevDecimals int
    
  DECLARE @intPlannedUntCount int
  
  DECLARE @strUntTab varchar(1)
    
  -- Declare Temp tables.

  DECLARE @tabPlan
    TABLE (PlanID varchar(32) COLLATE database_default
           PRIMARY KEY(PlanID)) 
  
  DECLARE @tabCalendarInterval
    TABLE(StartDate datetime,
          EndDate datetime
          PRIMARY KEY(StartDate, EndDate))
          
  DECLARE @tabPUntTPD
    TABLE (RowID int identity(1, 1),
	       TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	       PlanID varchar(32) COLLATE database_default,
	       TaskID varchar(32) COLLATE database_default,
	       UnitID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodQty decimal(19,4),
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PeriodRev decimal(19,4)
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Filter out only Plans with same Company
  
  INSERT @tabPlan(PlanID) SELECT PlanID FROM RPPlan WHERE Company = @strCompany

  -- Get flags to determine which features are being used.
  
  SELECT
     @strUntTab = UntTab,
     @intWkEndDay = (CASE WHEN StartingDayOfWeek = 1 THEN 7 ELSE StartingDayOfWeek - 1 END)
     FROM CFGResourcePlanning
     WHERE CFGResourcePlanning.Company = @strCompany
      
  -- Get decimal settings.
  -- At this point, there is no way to know what was the basis used in calculation of Baseline Revenue numbers.
  -- Therefore, we decided to use the current basis to determine number of decimal digits to be used
  -- in the realignment of Baseline Revenue numbers.
  
  SELECT @intQtyDecimals = 4,
         @intAmtCostDecimals = 2,
         @intAmtBillDecimals = 2,
         @intUntRevDecimals = 2
    
  -- Check to see if there is any time-phased data
  
  IF (@strUntTab = 'Y')
    BEGIN
      SELECT @intPlannedUntCount = COUNT(*) FROM RPPlannedUnit AS TPD INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID
    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find the MIN and MAX dates of all TPD.
  
  SELECT @dtStartDate = MIN(MINDate), @dtEndDate = MAX(MAXDate)
    FROM
      (SELECT MIN(StartDate) AS MINDate, MAX(EndDate) AS MAXDate FROM RPPlannedUnit AS TPD INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID
      ) AS X
  
  -- Save Calendar Intervals into a temp table and add an overflow Calendar Interval.
      
  WHILE (@dtStartDate <= @dtEndDate)
    BEGIN
        
      -- Compute End Date of interval.

      IF (@strScale = 'd') 
        SET @dtIntervalEnd = @dtStartDate
      ELSE
        SET @dtIntervalEnd = dbo.DLTK$IntervalEnd(@dtStartDate, @strScale, @intWkEndDay)
        
      IF (@dtIntervalEnd > @dtEndDate) 
        SET @dtIntervalEnd = @dtEndDate
            
      -- Insert new Calendar Interval record.
          
      INSERT @tabCalendarInterval(StartDate, EndDate)
        VALUES (@dtStartDate, @dtIntervalEnd)
          
      -- Set Start Date for next interval.
          
      SET @dtStartDate = DATEADD(d, 1, @dtIntervalEnd)
        
    END -- End While
       
       
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
              
  IF (@intPlannedUntCount > 0)
    BEGIN
    
      -- Save Planned Labor time-phased data rows.

      INSERT @tabPUntTPD
        (TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         UnitID,
         StartDate, 
         EndDate, 
         PeriodQty,
         PeriodCost, 
         PeriodBill,
         PeriodRev)
         SELECT
           TimePhaseID AS TimePhaseID,
           CIStartDate AS CIStartDate,
           PlanID AS PlanID, 
           TaskID AS TaskID,
           UnitID AS UnitID,
           StartDate AS StartDate, 
           EndDate AS EndDate, 
           ROUND(ISNULL(PeriodQty * ProrateRatio, 0), @intQtyDecimals) AS PeriodQty,
           ROUND(ISNULL(PeriodCost * ProrateRatio, 0), @intAmtCostDecimals) AS PeriodCost, 
           ROUND(ISNULL(PeriodBill * ProrateRatio, 0), @intAmtBillDecimals) AS PeriodBill, 
           ROUND(ISNULL(PeriodRev * ProrateRatio, 0), @intUntRevDecimals) AS PeriodRev
         FROM (SELECT -- For Unit Rows.
                 CI.StartDate AS CIStartDate, 
                 TPD.TimePhaseID AS TimePhaseID,
                 U.PlanID AS PlanID, 
                 U.TaskID AS TaskID,
                 U.UnitID AS UnitID, 
                 CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                 CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                 PeriodQty AS PeriodQty,
                 PeriodCost AS PeriodCost,
                 PeriodBill AS PeriodBill,
                 PeriodRev AS PeriodRev,
                 CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                      THEN dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                      THEN TPD.StartDate 
                                                      ELSE CI.StartDate END, 
                                                 CASE WHEN TPD.EndDate < CI.EndDate 
                                                      THEN TPD.EndDate 
                                                      ELSE CI.EndDate END, 
                                                 TPD.StartDate, TPD.EndDate,
                                                 @strCompany)
                      ELSE 1 END AS ProrateRatio
                 FROM RPPlannedUnit AS TPD
                   INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID
                   INNER JOIN RPUnit AS U ON TPD.PlanID = U.PlanID AND TPD.TaskID = U.TaskID AND TPD.UnitID = U.UnitID
                   INNER JOIN @tabCalendarInterval AS CI ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
              ) AS X
         WHERE (PeriodQty IS NOT NULL AND ROUND((PeriodQty * ProrateRatio), @intQtyDecimals) != 0)
     
      -- Adjust Planned Unit time-phased data to compensate for rounding errors.
     
      UPDATE @tabPUntTPD 
        SET 
          PeriodQty = (TPD.PeriodQty + D.DeltaHrs),
          PeriodCost = (TPD.PeriodCost + D.DeltaCost),
          PeriodBill = (TPD.PeriodBill + D.DeltaBill),
          PeriodRev = (TPD.PeriodRev + D.DeltaRev)
        FROM @tabPUntTPD AS TPD INNER JOIN 
          (SELECT 
             YTPD.TimePhaseID AS TimePhaseID, 
             ROUND((YTPD.PeriodQty - SUM(ISNULL(XTPD.PeriodQty, 0))), @intQtyDecimals) AS DeltaHrs,
             ROUND((YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))), @intAmtCostDecimals) AS DeltaCost,
             ROUND((YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))), @intAmtBillDecimals) AS DeltaBill,
             ROUND((YTPD.PeriodRev - SUM(ISNULL(XTPD.PeriodRev, 0))), @intUntRevDecimals) AS DeltaRev            
             FROM @tabPUntTPD AS XTPD INNER JOIN RPPlannedUnit AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
             GROUP BY YTPD.TimePhaseID, YTPD.PeriodQty, YTPD.PeriodCost, YTPD.PeriodBill, YTPD.PeriodRev) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
        WHERE (D.DeltaHrs != 0 OR D.DeltaCost != 0 OR D.DeltaBill != 0 OR D.DeltaRev != 0)
          AND RowID IN
            (SELECT RowID FROM @tabPUntTPD AS ATPD INNER JOIN
               (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabPUntTPD GROUP BY TimePhaseID) AS BTPD
                  ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
               
    END -- IF (@intPlannedUntCount > 0)
   
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT INTO @tabPlannedUnit
    (PlanID,
     TaskID,
     WBS1,
     WBS2, 
     WBS3,
     Account,
     Unit,
     UnitTable,
     TransactionDate,
     Quantity,
     CostAmount,
     BillAmount,
     RevenueAmount
    )
    SELECT
      Z.PlanID,
      ISNULL(Z.UnitID, Z.TaskID) AS TaskID,
      ISNULL(T.WBS1, ' ') AS WBS1,
      ISNULL(T.WBS2, ' ') AS WBS2,
      ISNULL(T.WBS3, ' ') AS WBS3,
      U.Account AS Account,
      U.Unit As Unit,
      U.UnitTable AS UnitTable,
      ISNULL(CONVERT(INT, MIN(Z.StartDate)), -1) AS TransactionDate,
      ISNULL(SUM(Z.Quantity), 0) AS Quantity,
      ISNULL(SUM(Z.CostAmount), 0) AS CostAmount,
      ISNULL(SUM(Z.BillAmount), 0) AS BillAmount,
      ISNULL(SUM(Z.RevenueAmount), 0) AS RevenueAmount
      FROM
        (SELECT
           TPD.CIStartDate AS CIStartDate,
           TPD.PlanID AS PlanID,
           TPD.TaskID AS TaskID,
           TPD.UnitID AS UnitID,
           TPD.StartDate,
           TPD.EndDate,
           TPD.PeriodQty AS Quantity,
           TPD.PeriodCost AS CostAmount,
           TPD.PeriodBill AS BillAmount,
           TPD.PeriodRev AS RevenueAmount
           FROM @tabPUntTPD AS TPD
        ) AS Z
        INNER JOIN RPTask AS T ON Z.PlanID = T.PlanID AND Z.TaskID = T.TaskID
        INNER JOIN RPUnit AS U ON Z.PlanID = U.PlanID AND Z.TaskID = U.TaskID AND Z.UnitID = U.UnitID
        GROUP BY Z.PlanID, Z.TaskID, Z.UnitID, Z.CIStartDate, T.WBS1, T.WBS2, T.WBS3, U.Account, U.Unit, U.UnitTable

  RETURN

END -- DW$tabPlannedUnit
GO
