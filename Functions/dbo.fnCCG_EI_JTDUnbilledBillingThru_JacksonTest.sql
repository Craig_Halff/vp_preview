SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_JTDUnbilledBillingThru_JacksonTest] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), @ThruDate datetime)
RETURNS money
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select dbo.fnCCG_EI_JTDUnbilledBillingThru('2003005.xx',' ',' ', CAST('2017-09-30' AS DATETIME))
	select dbo.fnCCG_EI_JTDUnbilledBillingThru('2003005.xx','1PD',' ', CAST('2017-09-30' AS DATETIME))
*/
	declare @res1 money, @res2 money
	Declare @ThruPeriod	int 

	Set @ThruPeriod = DATEPART(yyyy, @ThruDate) * 100 + DATEPART(mm, @ThruDate)

	select @res1=Sum(BillExt)
	from (
		select BillExt From LD
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus in ('B','H') and BillExt<>0     
			and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
	) as Labor
	
	select @res2=Sum(BillExt)
	from (
		select BillExt from LedgerMisc L
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus in ('B','H') and BillExt<>0    
			and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
		UNION ALL
		select BillExt from LedgerAP L
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus in ('B','H') and BillExt<>0    
			and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
		UNION ALL
		select BillExt from LedgerEX L
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus in ('B','H') and BillExt<>0   
				and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
		UNION ALL
		select BillExt from LedgerAR L
		where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus in ('B','H') and BillExt<>0   
			and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
	) as Expenses

	set @res1 = IsNull(@res1,0) + IsNull(@res2,0)

	if @res1 = 0
		set @res1 = null

	return @res1
END
GO
