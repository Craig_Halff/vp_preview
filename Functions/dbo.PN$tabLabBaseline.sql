SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$tabLabBaseline]
  (@strWBS1 Nvarchar(30),
   @strWBS2 Nvarchar(7),
   @strWBS3 Nvarchar(7),
   @strScale varchar(1) = 'm')
  RETURNS @tabLabBaseline TABLE
    (StartDate datetime,
     BaselineLabCost decimal(19,4),
     BaselineLabBill decimal(19,4)
    )
BEGIN -- Function PN$tabLabBaseline
 
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strMultiCompanyEnabled varchar(1)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

  DECLARE @dtStartDate datetime
  DECLARE @dtEndDate datetime
  DECLARE @dtIntervalEnd datetime
  
  DECLARE @intWkEndDay AS int
  
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int

  DECLARE @strVorN char(1)
  
  DECLARE @tabTask TABLE
    (TaskID varchar(32) COLLATE database_default
     PRIMARY KEY(TaskID))
       
  DECLARE @tabTPD TABLE
    (TimePhaseID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     StartDate datetime,
     EndDate datetime,
     PeriodCost decimal(19,4),
     PeriodBill decimal(19,4)
     PRIMARY KEY(TimePhaseID, TaskID))

  DECLARE @tabCalendarInterval TABLE
    (StartDate datetime,
     EndDate datetime
     PRIMARY KEY(StartDate, EndDate))
          
  DECLARE @tabPLabTPD TABLE 
    (RowID int identity(1, 1),
     TimePhaseID varchar(32) COLLATE database_default,
     CIStartDate datetime, 
     StartDate datetime, 
     EndDate datetime, 
     PeriodCost decimal(19,4),
     PeriodBill decimal(19,4)
     PRIMARY KEY(RowID, TimePhaseID)) 
           
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled flag.
  
  SELECT
    @strMultiCompanyEnabled = MultiCompanyEnabled
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat     

  -- Save Company string for use later.
  
  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          
  -- Set Start of Week = Monday, End of Week = Sunday.
  
  SELECT @intWkEndDay = 1
      
  -- Get decimal settings.
  
  SET @intAmtCostDecimals = 4
  SET @intAmtBillDecimals = 4

  -- Determine whether we should use the Vision or Navigator tables.
  
  SET @strVorN = dbo.PN$PlanIs_VorN(@strWBS1)
       
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Extract Tasks from Plans that are marked as "Included in Utilization".
  -- There could be multiple Plans with RPPlan.UlilizationIncludeFlag = 'Y'

  IF (@strVorN = 'V')
    BEGIN

      INSERT @tabTask
        (TaskID)
        SELECT
          T.TaskID
          FROM RPTask AS T
            INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND T.WBS1 = @strWBS1 AND
              (ISNULL(T.WBS2, ' ') = @strWBS2) AND 
              (ISNULL(T.WBS3, ' ') = @strWBS3) AND
              (T.WBSType = 'WBS1' OR T.WBSType = 'WBS2' OR T.WBSType = 'WBS3') 

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabTPD
        (TimePhaseID,
         TaskID,
         StartDate,
         EndDate,
         PeriodCost,
         PeriodBill
        )
        SELECT
          TPD.TimePhaseID,
          TPD.TaskID,
          TPD.StartDate,
          TPD.EndDate,
          TPD.PeriodCost,
          TPD.PeriodBill
          FROM @tabTask AS T
            INNER JOIN RPBaselineLabor AS TPD ON T.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL

    END /* End If (@strVorN = 'V') */
  ELSE IF (@strVorN = 'N')
    BEGIN

      INSERT @tabTask
        (TaskID)
        SELECT
          T.TaskID
          FROM PNTask AS T
            INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND T.WBS1 = @strWBS1 AND
              (ISNULL(T.WBS2, ' ') = @strWBS2) AND 
              (ISNULL(T.WBS3, ' ') = @strWBS3) AND
              (T.WBSType = 'WBS1' OR T.WBSType = 'WBS2' OR T.WBSType = 'WBS3') 

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabTPD
        (TimePhaseID,
         TaskID,
         StartDate,
         EndDate,
         PeriodCost,
         PeriodBill
        )
        SELECT
          TPD.TimePhaseID,
          TPD.TaskID,
          TPD.StartDate,
          TPD.EndDate,
          TPD.PeriodCost,
          TPD.PeriodBill
          FROM @tabTask AS T
            INNER JOIN PNBaselineLabor AS TPD ON T.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL

   END /* End If (@strVorN = 'N') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find the MIN and MAX dates of all TPD.
  
  SELECT @dtStartDate = MIN(MINDate), @dtEndDate = MAX(MAXDate)
    FROM
      (SELECT MIN(TPD.StartDate) AS MINDate, MAX(TPD.EndDate) AS MAXDate 
         FROM @tabTPD AS TPD INNER JOIN @tabTask AS T ON TPD.TaskID = T.TaskID
      ) AS X

  -- Save Calendar Intervals into a temp table.
      
  WHILE (@dtStartDate <= @dtEndDate)
    BEGIN
        
      -- Compute End Date of interval.

      IF (@strScale = 'd') 
        SET @dtIntervalEnd = @dtStartDate
      ELSE
        SET @dtIntervalEnd = dbo.DLTK$IntervalEnd(@dtStartDate, @strScale, @intWkEndDay)
        
      IF (@dtIntervalEnd > @dtEndDate) 
        SET @dtIntervalEnd = @dtEndDate
            
      -- Insert new Calendar Interval record.
          
      INSERT @tabCalendarInterval(StartDate, EndDate)
        VALUES (@dtStartDate, @dtIntervalEnd)
          
      -- Set Start Date for next interval.
          
      SET @dtStartDate = DATEADD(d, 1, @dtIntervalEnd)
        
    END -- End While
       
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
              
  -- Save Baseline Labor time-phased data rows.

  INSERT @tabPLabTPD
    (TimePhaseID,
     CIStartDate,
     StartDate, 
     EndDate,
     PeriodCost, 
     PeriodBill)
     SELECT
       TimePhaseID,
       CIStartDate AS CIStartDate,
       StartDate AS StartDate, 
       EndDate AS EndDate,
       ROUND(ISNULL(PeriodCost * ProrateRatio, 0), @intAmtCostDecimals) AS PeriodCost,
       ROUND(ISNULL(PeriodBill * ProrateRatio, 0), @intAmtBillDecimals) AS PeriodBill
     FROM 
       (SELECT
          TPD.TimePhaseID AS TimePhaseID,
          CI.StartDate AS CIStartDate,
          CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
          CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
          PeriodCost AS PeriodCost,
          PeriodBill AS PeriodBill,
          CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
               THEN dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                               THEN TPD.StartDate 
                                               ELSE CI.StartDate END, 
                                          CASE WHEN TPD.EndDate < CI.EndDate 
                                               THEN TPD.EndDate 
                                               ELSE CI.EndDate END, 
                                          TPD.StartDate, TPD.EndDate,
                                          @strCompany)
               ELSE 1 END AS ProrateRatio
          FROM @tabTPD AS TPD
               INNER JOIN @tabTask AS T ON TPD.TaskID = T.TaskID
               INNER JOIN @tabCalendarInterval AS CI ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
       ) AS X
     WHERE (
      (PeriodCost IS NOT NULL AND ROUND((PeriodCost * ProrateRatio), @intAmtCostDecimals) != 0) OR
      (PeriodBill IS NOT NULL AND ROUND((PeriodBill * ProrateRatio), @intAmtBillDecimals) != 0)
    )
              
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT INTO @tabLabBaseline
    (StartDate,
     BaselineLabCost,
     BaselineLabBill
    )
    SELECT
      MIN(Z.StartDate) AS StartDate,
      ISNULL(SUM(Z.CostAmount), 0) AS BaselineLabCost,
      ISNULL(SUM(Z.BillAmount), 0) AS BaselineLabBill
      FROM
        (SELECT
           TPD.CIStartDate AS CIStartDate,
           TPD.StartDate,
           TPD.PeriodCost AS CostAmount,
           TPD.PeriodBill AS BillAmount
           FROM @tabPLabTPD AS TPD
        ) AS Z
        GROUP BY Z.CIStartDate

  RETURN

END -- PN$tabLabBaseline
GO
