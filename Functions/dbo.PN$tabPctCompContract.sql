SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$tabPctCompContract]
  (@strWBS1 Nvarchar(30),
   @strWBS2 Nvarchar(7),
   @strWBS3 Nvarchar(7),
   @strJTDDate VARCHAR(8), /* Date must be in format: 'yyyymmdd' regardless of UI Culture. */
   @strScale varchar(1) = 'm')
  RETURNS @tabPctCompContract TABLE
    (StartDate datetime,
     PctCompContractCost decimal(19,4),
     PctCompContractBill decimal(19,4)
    )
BEGIN -- Function PN$tabPctCompContract
 
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strReportAtBillingInBillingCurr varchar(1)

  DECLARE @dtJTDDate datetime
  DECLARE @dtStartDate datetime
  DECLARE @dtEndDate datetime
  DECLARE @dtIntervalEnd datetime
  
  DECLARE @intWkEndDay AS int
  
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int

  DECLARE @sintPctCompleteFormula smallint

  DECLARE @decFeeLabCost decimal(19,4)
  DECLARE @decFeeLabBill decimal(19,4)
  DECLARE @decTotalPlannedCost decimal(19,4)
  DECLARE @decTotalPlannedBill decimal(19,4)
  DECLARE @decTotalBaselineCost decimal(19,4)
  DECLARE @decTotalBaselineBill decimal(19,4)
  
  DECLARE @strVorN char(1)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

  DECLARE @tabTask TABLE
    (TaskID varchar(32) COLLATE database_default,
     PlanID varchar(32) COLLATE database_default,
     PlanCreateDate datetime,
     PctCompleteFormula smallint
     PRIMARY KEY(TaskID))
       
  DECLARE @tabTPD TABLE (
    RowID int IDENTITY(1,1),
    TaskID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4)
    PRIMARY KEY(RowID, TaskID)
  )

	DECLARE @tabLD TABLE (
    RowID int IDENTITY(1,1),
    TransDate datetime,
    JTDCost decimal(19,4),
    JTDBill decimal(19,4)
    PRIMARY KEY(RowID, TransDate)
  )

  DECLARE @tabCalendarInterval TABLE (
    StartDate datetime,
    EndDate datetime
    PRIMARY KEY(StartDate, EndDate)
  )
          
  DECLARE @tabPLabTPD TABLE (
    RowID int identity(1, 1),
    CIStartDate datetime, 
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4)
    PRIMARY KEY(RowID)
  )

  DECLARE @tabPLabCum TABLE (
    RowID int identity(1, 1),
    CIStartDate datetime, 
    CumPeriodCost decimal(19,4),
    CumPeriodBill decimal(19,4)
    PRIMARY KEY(RowID)
  )

  DECLARE @tabBLabTPD TABLE (
    RowID int identity(1, 1),
    CIStartDate datetime, 
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4)
    PRIMARY KEY(RowID)
  )

  DECLARE @tabBLabCum TABLE (
    RowID int identity(1, 1),
    CIStartDate datetime, 
    CumPeriodCost decimal(19,4),
    CumPeriodBill decimal(19,4)
    PRIMARY KEY(RowID)
  ) 

  DECLARE @tabJLabTPD TABLE (
    RowID int identity(1, 1),
    CIStartDate datetime, 
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4)
    PRIMARY KEY(RowID)
  )

  DECLARE @tabJLabCum TABLE (
    RowID int identity(1, 1),
    CIStartDate datetime, 
    CumPeriodCost decimal(19,4),
    CumPeriodBill decimal(19,4)
    PRIMARY KEY(RowID)
  )
           
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Set JTD Date

  SET @dtJTDDate = CONVERT(datetime, @strJTDDate)

  -- Set Start of Week = Monday, End of Week = Sunday.
  
  SELECT @intWkEndDay = 1
      
  -- Get decimal settings.
  
  SET @intAmtCostDecimals = 4
  SET @intAmtBillDecimals = 4

  -- Get the RABIBC flag

  SELECT 
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END
    FROM FW_CFGSystem
       
  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat     

  -- Save Company string for use later.
  
  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '

  -- Determine Labor Fee Amount for the WBS1/WBS2/WBS3 row.

  SELECT 
    @decFeeLabCost = PR.Fee,
    @decFeeLabBill = (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeBillingCurrency ELSE PR.Fee END)
    FROM PR WHERE WBS1 = @strWBS1 AND WBS2 = @strWBS2 AND WBS3 = @strWBS3
 
  -- Determine which set of tables need to be used.

  SET @strVorN = dbo.PN$PlanIs_VorN(@strWBS1)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Extract Tasks from Plans that are marked as "Included in Utilization".
  -- There could be multiple Plans with RPPlan.UlilizationIncludeFlag = 'Y'

  IF (@strVorN = 'V')
    BEGIN

      INSERT @tabTask
        (TaskID,
         PlanID,
         PlanCreateDate,
         PctCompleteFormula
        )
        SELECT
          T.TaskID AS TaskID,
          T.PlanID AS PlanID,
          P.CreateDate AS PlanCreateDate,
          P.PctCompleteFormula
          FROM RPTask AS T
            INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND T.WBS1 = @strWBS1 AND
              (ISNULL(T.WBS2, ' ') = @strWBS2) AND 
              (ISNULL(T.WBS3, ' ') = @strWBS3) AND
              (T.WBSType = 'WBS1' OR T.WBSType = 'WBS2' OR T.WBSType = 'WBS3')

    END /* End If (@strVorN = 'V') */
  ELSE IF (@strVorN = 'N')
    BEGIN

      INSERT @tabTask
        (TaskID,
         PlanID,
         PlanCreateDate,
         PctCompleteFormula
        )
        SELECT
          T.TaskID AS TaskID,
          T.PlanID AS PlanID,
          P.CreateDate AS PlanCreateDate,
          P.PctCompleteFormula
          FROM PNTask AS T
            INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND T.WBS1 = @strWBS1 AND
              (ISNULL(T.WBS2, ' ') = @strWBS2) AND 
              (ISNULL(T.WBS3, ' ') = @strWBS3) AND
              (T.WBSType = 'WBS1' OR T.WBSType = 'WBS2' OR T.WBSType = 'WBS3')

   END /* End If (@strVorN = 'N') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine the Pct Complete Formula.

  SELECT @sintPctCompleteFormula = PctCompleteFormula
    FROM @tabTask WHERE PlanCreateDate = (SELECT MIN(PlanCreateDate) FROM @tabTask)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- These are 3 Pct Complete Formulae:
-- PctCompleteFormula = 1 --> PctComplete = 100.00 * ((PlannedAmt - ETCAmt) / PlannedAmt)
-- PctCompleteFormula = 2 --> PctComplete = 100.00 * (JTDAmt / (JTDAmt + ETCAmt))
-- PctCompleteFormula = 3 --> PctComplete = 100.00 * ((BaselineAmt - ETCAmt) / BaselineAmt)
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@sintPctCompleteFormula = 1)
    BEGIN

      -- Save TPD into a table to address the issue of 2 set of tables.

      IF (@strVorN = 'V')
        BEGIN

          INSERT @tabTPD
            (TaskID,
             StartDate,
             EndDate,
             PeriodCost,
             PeriodBill
            )
            SELECT
              TPD.TaskID,
              TPD.StartDate,
              TPD.EndDate,
              TPD.PeriodCost,
              TPD.PeriodBill
              FROM @tabTask AS T
                INNER JOIN RPPlannedLabor AS TPD ON T.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL

        END /* End If (@strVorN = 'V') */
      ELSE IF (@strVorN = 'N')
        BEGIN      
      
          INSERT @tabTPD
            (TaskID,
             StartDate,
             EndDate,
             PeriodCost,
             PeriodBill
            )
            SELECT
              TPD.TaskID,
              TPD.StartDate,
              TPD.EndDate,
              TPD.PeriodCost,
              TPD.PeriodBill
              FROM @tabTask AS T
                INNER JOIN PNPlannedLabor AS TPD ON T.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL

        END /* End If (@strVorN = 'N') */

      -- Find the MIN and MAX dates of all TPD. Also, calculate the Total Planned Bill Amounts.
  
      SELECT 
        @dtStartDate = MIN(MINDate), 
        @dtEndDate = MAX(MAXDate), 
        @decTotalPlannedCost = ISNULL(SUM(PeriodCost), 0),
        @decTotalPlannedBill = ISNULL(SUM(PeriodBill), 0)
        FROM
          (SELECT 
             MIN(TPD.StartDate) AS MINDate, 
             MAX(TPD.EndDate) AS MAXDate, 
             SUM(PeriodCost) AS PeriodCost,
             SUM(PeriodBill) AS PeriodBill 
             FROM @tabTPD AS TPD 
          ) AS X

      /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
      /* If Total Planned Amounts is zero then exit the UDF.      */
      /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

      IF (@decTotalPlannedCost = 0 AND @decTotalPlannedBill = 0) RETURN

      -- Stop the Calendar Intervals at JTD Date.

      SELECT @dtEndDate = CASE WHEN @dtEndDate > @dtJTDDate THEN @dtJTDDate ELSE @dtEndDate END
  
      -- Save Calendar Intervals into a temp table.
      
      WHILE (@dtStartDate <= @dtEndDate)
        BEGIN
        
          -- Compute End Date of interval.

          IF (@strScale = 'd') 
            SET @dtIntervalEnd = @dtStartDate
          ELSE
            SET @dtIntervalEnd = dbo.DLTK$IntervalEnd(@dtStartDate, @strScale, @intWkEndDay)
        
          IF (@dtIntervalEnd > @dtEndDate) 
            SET @dtIntervalEnd = @dtEndDate
            
          -- Insert new Calendar Interval record.
          
          INSERT @tabCalendarInterval(StartDate, EndDate)
            VALUES (@dtStartDate, @dtIntervalEnd)
          
          -- Set Start Date for next interval.
          
          SET @dtStartDate = DATEADD(d, 1, @dtIntervalEnd)
        
        END -- End While
       
      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
              
      -- Save Planned Labor time-phased data rows.
      -- Need to generate rows for all calendar periods even if there is no data in that period.
      -- This is necessary to calculate the cumulative amounts later.

      INSERT @tabPLabTPD
        (CIStartDate,
         PeriodCost,
         PeriodBill)
         SELECT
           CI.StartDate AS CIStartDate,
           SUM(ISNULL(PeriodCost * ProrateRatio, 0)) AS PeriodCost,
           SUM(ISNULL(PeriodBill * ProrateRatio, 0)) AS PeriodBill
         FROM @tabCalendarInterval AS CI
           LEFT JOIN
             (SELECT
                CIX.StartDate AS StartDate,
                PeriodCost AS PeriodCost,
                PeriodBill AS PeriodBill,
                CASE WHEN (TPD.StartDate < CIX.StartDate OR TPD.EndDate > CIX.EndDate)
                     THEN dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CIX.StartDate 
                                                     THEN TPD.StartDate 
                                                     ELSE CIX.StartDate END, 
                                                CASE WHEN TPD.EndDate < CIX.EndDate 
                                                     THEN TPD.EndDate 
                                                     ELSE CIX.EndDate END, 
                                                TPD.StartDate, TPD.EndDate,
                                                @strCompany)
                     ELSE 1 END AS ProrateRatio
                FROM @tabCalendarInterval AS CIX
                  LEFT JOIN @tabTPD AS TPD ON TPD.StartDate <= CIX.EndDate AND TPD.EndDate >= CIX.StartDate
             ) AS X
             ON CI.StartDate = X.StartDate
         GROUP BY CI.StartDate

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Calculate Cummulative Planned Bill Amounts.

      INSERT @tabPLabCum
        (CIStartDate,
         CumPeriodCost,
         CumPeriodBill)
         SELECT
           CIStartDate AS CIStartDate,
           (SELECT SUM(PeriodCost) FROM @tabPLabTPD AS X WHERE Y.RowID >= X.RowID) AS CumPeriodCost,
           (SELECT SUM(PeriodBill) FROM @tabPLabTPD AS X WHERE Y.RowID >= X.RowID) AS CumPeriodBill
           FROM @tabPLabTPD AS Y

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Calculate Pct Complete * Compensation

      INSERT @tabPctCompContract 
        (StartDate,
         PctCompContractCost,
         PctCompContractBill
        )
        SELECT
          CIStartDate AS StartDate,
          (COALESCE((CumPeriodCost / NULLIF(@decTotalPlannedCost, 0)), 0) * @decFeeLabCost) AS PctCompContractCost,
          (COALESCE((CumPeriodBill / NULLIF(@decTotalPlannedBill, 0)), 0) * @decFeeLabBill) AS PctCompContractBill
          FROM @tabPLabCum

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      RETURN

    END /* IF (@sintPctCompleteFormula = 1) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

  IF (@sintPctCompleteFormula = 2)
    BEGIN

	    -- Save Labor JTD & Unposted Labor JTD for this Project into temp table.
      -- The JTD data are posted at the leaf node in the WBS Tree. 
      -- Therefore, we need to calculate JTD for the summary levels.

	    INSERT @tabLD
        (TransDate,
         JTDCost,
         JTDBill)      
		    SELECT
          TransDate,
          SUM(ISNULL(JTDCost, 0.0000)) AS JTDCost,
          SUM(ISNULL(JTDBill, 0.0000)) AS JTDBill 
          FROM
            (SELECT
               LD.TransDate,
               SUM(LD.RegAmtProjectCurrency + LD.OvtAmtProjectCurrency + LD.SpecialOvtAmtProjectCurrency) AS JTDCost,
               SUM(LD.BillExt) AS JTDBill
               FROM LD 
               WHERE LD.ProjectCost = 'Y' AND LD.TransDate <= @dtJTDDate AND LD.WBS1 = @strWBS1 AND
                 (LD.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
                 (LD.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
               GROUP BY LD.TransDate
             UNION ALL SELECT
               TD.TransDate,
               SUM(TD.RegAmtProjectCurrency + TD.OvtAmtProjectCurrency + TD.SpecialOvtAmtProjectCurrency) AS JTDCost,
               SUM(TD.BillExt) AS JTDBill
               FROM tkDetail AS TD
                 INNER JOIN EM ON TD.Employee = EM.Employee  
                 INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
               WHERE TD.TransDate <= @dtJTDDate AND TD.WBS1 = @strWBS1 AND
                 (TD.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
                 (TD.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
               GROUP BY TD.TransDate
             UNION ALL SELECT
               TD.TransDate,
               SUM(TD.RegAmtProjectCurrency + TD.OvtAmtProjectCurrency + TD.SpecialOvtAmtProjectCurrency) AS JTDCost,
               SUM(TD.BillExt) AS JTDBill
               FROM tsDetail AS TD
                 INNER JOIN EM ON TD.Employee = EM.Employee  
                 INNER JOIN tsControl AS TC ON (TC.Posted = 'N' AND TD.Batch = TC.Batch)
               WHERE TD.TransDate <= @dtJTDDate AND TD.WBS1 = @strWBS1 AND
                 (TD.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
                 (TD.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 END)
               GROUP BY TD.TransDate
            ) AS X
          GROUP BY X.TransDate

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Save TPD into a table to address the issue of 2 set of tables.

      IF (@strVorN = 'V')
        BEGIN

          INSERT @tabTPD
            (TaskID,
             StartDate,
             EndDate,
             PeriodCost,
             PeriodBill
            )
            SELECT
              TPD.TaskID,
              TPD.StartDate,
              TPD.EndDate,
              TPD.PeriodCost,
              TPD.PeriodBill
              FROM @tabTask AS T
                INNER JOIN RPPlannedLabor AS TPD ON T.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL

        END /* End If (@strVorN = 'V') */
      ELSE IF (@strVorN = 'N')
        BEGIN      
      
          INSERT @tabTPD
            (TaskID,
             StartDate,
             EndDate,
             PeriodCost,
             PeriodBill
            )
            SELECT
              TPD.TaskID,
              TPD.StartDate,
              TPD.EndDate,
              TPD.PeriodCost,
              TPD.PeriodBill
              FROM @tabTask AS T
                INNER JOIN PNPlannedLabor AS TPD ON T.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL

        END /* End If (@strVorN = 'N') */

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Calculate the Total Planned Cost & Bill Amounts.

      SELECT 
        @decTotalPlannedCost = ISNULL(SUM(PeriodCost), 0),
        @decTotalPlannedBill = ISNULL(SUM(PeriodBill), 0)
        FROM @tabTPD
      
      -- Find the MIN and MAX dates of all TPD.
  
      SELECT @dtStartDate = MIN(MINDate), @dtEndDate = MAX(MAXDate)
        FROM
          (SELECT MIN(TPD.StartDate) AS MINDate, MAX(TPD.EndDate) AS MAXDate 
             FROM @tabTPD AS TPD 
             WHERE TPD.StartDate <= @dtJTDDate
           UNION SELECT
             TransDate AS MINDate,
             TransDate AS MAXDate
             FROM @tabLD
          ) AS X

      -- Stop the Calendar Intervals at JTD Date.

      SELECT @dtEndDate = CASE WHEN @dtEndDate > @dtJTDDate THEN @dtJTDDate ELSE @dtEndDate END
  
      -- Save Calendar Intervals into a temp table.
      
      WHILE (@dtStartDate <= @dtEndDate)
        BEGIN
        
          -- Compute End Date of interval.

          IF (@strScale = 'd') 
            SET @dtIntervalEnd = @dtStartDate
          ELSE
            SET @dtIntervalEnd = dbo.DLTK$IntervalEnd(@dtStartDate, @strScale, @intWkEndDay)
        
          IF (@dtIntervalEnd > @dtEndDate) 
            SET @dtIntervalEnd = @dtEndDate
            
          -- Insert new Calendar Interval record.
          
          INSERT @tabCalendarInterval(StartDate, EndDate)
            VALUES (@dtStartDate, @dtIntervalEnd)
          
          -- Set Start Date for next interval.
          
          SET @dtStartDate = DATEADD(d, 1, @dtIntervalEnd)
        
        END -- End While
       
      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
              
      -- Save Planned Labor time-phased data rows.
      -- Need to generate rows for all calendar periods even if there is no data in that period.
      -- This is necessary to calculate the cumulative amounts later.

      INSERT @tabPLabTPD
        (CIStartDate,
         PeriodCost,
         PeriodBill)
         SELECT
           CI.StartDate AS CIStartDate,
           SUM(ISNULL(PeriodCost * ProrateRatio, 0)) AS PeriodCost,
           SUM(ISNULL(PeriodBill * ProrateRatio, 0)) AS PeriodBill
         FROM @tabCalendarInterval AS CI
           LEFT JOIN
             (SELECT
                CIX.StartDate AS StartDate,
                PeriodCost AS PeriodCost,
                PeriodBill AS PeriodBill,
                CASE WHEN (TPD.StartDate < CIX.StartDate OR TPD.EndDate > CIX.EndDate)
                     THEN dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CIX.StartDate 
                                                     THEN TPD.StartDate 
                                                     ELSE CIX.StartDate END, 
                                                CASE WHEN TPD.EndDate < CIX.EndDate 
                                                     THEN TPD.EndDate 
                                                     ELSE CIX.EndDate END, 
                                                TPD.StartDate, TPD.EndDate,
                                                @strCompany)
                     ELSE 1 END AS ProrateRatio
                FROM @tabCalendarInterval AS CIX
                  LEFT JOIN @tabTPD AS TPD ON TPD.StartDate <= CIX.EndDate AND TPD.EndDate >= CIX.StartDate
             ) AS X
             ON CI.StartDate = X.StartDate
         GROUP BY CI.StartDate

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Calculate Cummulative Planned Bill Amounts.

      INSERT @tabPLabCum
        (CIStartDate,
         CumPeriodCost,
         CumPeriodBill)
         SELECT
           CIStartDate AS CIStartDate,
           (SELECT SUM(PeriodCost) FROM @tabPLabTPD AS X WHERE Y.RowID >= X.RowID) AS CumPeriodCost,
           (SELECT SUM(PeriodBill) FROM @tabPLabTPD AS X WHERE Y.RowID >= X.RowID) AS CumPeriodBill
           FROM @tabPLabTPD AS Y

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Calculate JTD time-phased data rows.

      INSERT INTO @tabJLabTPD
        (CIStartDate,
         PeriodCost,
         PeriodBill
        )
        SELECT
          CI.StartDate AS CIStartDate,
          SUM(ISNULL(LD.JTDCost, 0)) AS JTDLabCost,
          SUM(ISNULL(LD.JTDBill, 0)) AS JTDLabBill
          FROM @tabCalendarInterval AS CI
            LEFT JOIN @tabLD AS LD ON LD.TransDate <= CI.EndDate AND LD.TransDate >= CI.StartDate
          GROUP BY CI.StartDate

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Calculate Cummulative JTD Bill Amounts.

      INSERT @tabJLabCum
        (CIStartDate,
         CumPeriodCost,
         CumPeriodBill)
         SELECT
           CIStartDate AS CIStartDate,
           (SELECT SUM(PeriodCost) FROM @tabJLabTPD AS X WHERE Y.RowID >= X.RowID) AS CumPeriodCost,
           (SELECT SUM(PeriodBill) FROM @tabJLabTPD AS X WHERE Y.RowID >= X.RowID) AS CumPeriodBill
           FROM @tabJLabTPD AS Y

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      -- Pct Complete = Cum JTD / EAC
      -- EAC = (Total Planned - Cum Planned) + Cum JTD
      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Calculate Pct Complete * Compensation

      INSERT @tabPctCompContract 
        (StartDate,
         PctCompContractCost,
         PctCompContractBill
        )
        SELECT
          CI.StartDate AS StartDate,
          CASE WHEN ((@decTotalPlannedCost - ISNULL(PTPD.CumPeriodCost, 0)) + ISNULL(JTPD.CumPeriodCost, 0)) = 0
               THEN 0
               ELSE (ISNULL(JTPD.CumPeriodCost, 0) / ((@decTotalPlannedCost - ISNULL(PTPD.CumPeriodCost, 0)) + ISNULL(JTPD.CumPeriodCost, 0))) * @decFeeLabCost
               END AS PctCompContractCost,
          CASE WHEN ((@decTotalPlannedBill - ISNULL(PTPD.CumPeriodBill, 0)) + ISNULL(JTPD.CumPeriodBill, 0)) = 0
               THEN 0
               ELSE (ISNULL(JTPD.CumPeriodBill, 0) / ((@decTotalPlannedBill - ISNULL(PTPD.CumPeriodBill, 0)) + ISNULL(JTPD.CumPeriodBill, 0))) * @decFeeLabBill
               END AS PctCompContractBill
          FROM @tabCalendarInterval AS CI
            LEFT JOIN @tabJLabCum AS JTPD ON CI.StartDate = JTPD.CIStartDate
            LEFT JOIN @tabPLabCum AS PTPD ON CI.StartDate = PTPD.CIStartDate

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      RETURN

    END /* IF (@sintPctCompleteFormula = 2) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

  IF (@sintPctCompleteFormula = 3)
    BEGIN

      -- Save TPD into a table to address the issue of 2 set of tables.

      IF (@strVorN = 'V')
        BEGIN

          INSERT @tabTPD
            (TaskID,
             StartDate,
             EndDate,
             PeriodCost,
             PeriodBill
            )
            SELECT
              TPD.TaskID,
              TPD.StartDate,
              TPD.EndDate,
              TPD.PeriodCost,
              TPD.PeriodBill
              FROM @tabTask AS T
                INNER JOIN RPBaselineLabor AS TPD ON T.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL

        END /* End If (@strVorN = 'V') */
      ELSE IF (@strVorN = 'N')
        BEGIN      
      
          INSERT @tabTPD
            (TaskID,
             StartDate,
             EndDate,
             PeriodCost,
             PeriodBill
            )
            SELECT
              TPD.TaskID,
              TPD.StartDate,
              TPD.EndDate,
              TPD.PeriodCost,
              TPD.PeriodBill
              FROM @tabTask AS T
                INNER JOIN PNBaselineLabor AS TPD ON T.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL

        END /* End If (@strVorN = 'N') */

      -- Find the MIN and MAX dates of all TPD. Also, calculate the Total Baseline Bill Amounts.
  
      SELECT 
        @dtStartDate = MIN(MINDate), 
        @dtEndDate = MAX(MAXDate), 
        @decTotalBaselineBill = ISNULL(SUM(PeriodCost), 0),
        @decTotalBaselineBill = ISNULL(SUM(PeriodBill), 0)
        FROM
          (SELECT 
             MIN(TPD.StartDate) AS MINDate, 
             MAX(TPD.EndDate) AS MAXDate, 
             SUM(PeriodCost) AS PeriodCost,
             SUM(PeriodBill) AS PeriodBill 
             FROM @tabTPD AS TPD
          ) AS X

      /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
      /* If Total Baseline Bill Amounts is zero then exit the UDF. */
      /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

      IF (@decTotalBaselineCost = 0 AND @decTotalBaselineBill = 0) RETURN

      -- Stop the Calendar Intervals at JTD Date.

      SELECT @dtEndDate = CASE WHEN @dtEndDate > @dtJTDDate THEN @dtJTDDate ELSE @dtEndDate END
  
      -- Save Calendar Intervals into a temp table.
      
      WHILE (@dtStartDate <= @dtEndDate)
        BEGIN
        
          -- Compute End Date of interval.

          IF (@strScale = 'd') 
            SET @dtIntervalEnd = @dtStartDate
          ELSE
            SET @dtIntervalEnd = dbo.DLTK$IntervalEnd(@dtStartDate, @strScale, @intWkEndDay)
        
          IF (@dtIntervalEnd > @dtEndDate) 
            SET @dtIntervalEnd = @dtEndDate
            
          -- Insert new Calendar Interval record.
          
          INSERT @tabCalendarInterval(StartDate, EndDate)
            VALUES (@dtStartDate, @dtIntervalEnd)
          
          -- Set Start Date for next interval.
          
          SET @dtStartDate = DATEADD(d, 1, @dtIntervalEnd)
        
        END -- End While
       
      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
              
      -- Save Baseline Labor time-phased data rows.
      -- Need to generate rows for all calendar periods even if there is no data in that period.
      -- This is necessary to calculate the cumulative amounts later.

      INSERT @tabBLabTPD
        (CIStartDate,
         PeriodCost,
         PeriodBill)
         SELECT
           CI.StartDate AS CIStartDate,
           SUM(ISNULL(PeriodCost * ProrateRatio, 0)) AS PeriodCost,
           SUM(ISNULL(PeriodBill * ProrateRatio, 0)) AS PeriodBill
         FROM @tabCalendarInterval AS CI
           LEFT JOIN
             (SELECT
                CIX.StartDate AS StartDate,
                PeriodCost AS PeriodCost,
                PeriodBill AS PeriodBill,
                CASE WHEN (TPD.StartDate < CIX.StartDate OR TPD.EndDate > CIX.EndDate)
                     THEN dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CIX.StartDate 
                                                     THEN TPD.StartDate 
                                                     ELSE CIX.StartDate END, 
                                                CASE WHEN TPD.EndDate < CIX.EndDate 
                                                     THEN TPD.EndDate 
                                                     ELSE CIX.EndDate END, 
                                                TPD.StartDate, TPD.EndDate,
                                                @strCompany)
                     ELSE 1 END AS ProrateRatio
                FROM @tabCalendarInterval AS CIX
                  LEFT JOIN @tabTPD AS TPD ON TPD.StartDate <= CIX.EndDate AND TPD.EndDate >= CIX.StartDate
             ) AS X
             ON CI.StartDate = X.StartDate
         GROUP BY CI.StartDate

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Calculate Cummulative Baseline Bill Amounts.

      INSERT @tabBLabCum
        (CIStartDate,
         CumPeriodCost,
         CumPeriodBill)
         SELECT
           CIStartDate AS CIStartDate,
           (SELECT SUM(PeriodCost) FROM @tabBLabTPD AS X WHERE Y.RowID >= X.RowID) AS CumPeriodCost,
           (SELECT SUM(PeriodBill) FROM @tabBLabTPD AS X WHERE Y.RowID >= X.RowID) AS CumPeriodBill
           FROM @tabBLabTPD AS Y

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Calculate Pct Complete * Compensation

      INSERT @tabPctCompContract 
        (StartDate,
         PctCompContractCost,
         PctCompContractBill
        )
        SELECT
          CIStartDate AS StartDate,
          (COALESCE((CumPeriodCost / NULLIF(@decTotalBaselineCost, 0)), 0) * @decFeeLabCost) AS PctCompContractCost,
          (COALESCE((CumPeriodBill / NULLIF(@decTotalBaselineBill, 0)), 0) * @decFeeLabBill) AS PctCompContractBill
          FROM @tabBLabCum

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      RETURN

    END /* IF (@sintPctCompleteFormula = 3) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN

END -- PN$tabPctCompContract
GO
