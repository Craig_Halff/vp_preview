SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[stRP$tabPlanningTaskInfo](  
  @strTaskID varchar(32),
  @strMode varchar(4) 
)
  RETURNS @tabResourceJTDPlans TABLE (
    TaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
	LaborCode nvarchar(14) COLLATE database_default,
	Name nvarchar(255) COLLATE database_default,
	OutlineNumber varchar(255) COLLATE database_default,
	ParentOutlineNumber varchar(255) COLLATE database_default,
	OutlineLevel int,
	LeafNode bit,
	HasChildren bit,
	HasAssignment bit,
	HasJTD bit,
	HasPlannedHours bit
  )

BEGIN

 DECLARE @dtJTDDate datetime
 DECLARE @dtETCDate datetime 
 DECLARE @strWBS1 nvarchar(30)
 DECLARE @strPlanID varchar(32)
 DECLARE @strOutlineNumber varchar(32)

  --Retrieve JTD Date
  SELECT 
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE())))
    FROM CFGRMSettings
  
  SELECT @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)

  SELECT @strWBS1 = WBS1, @strPlanID = PlanID, @strOutlineNumber = OutlineNumber 
    FROM PNTask Where TaskID = @strTaskID
	 
  -- Declare Temp tables.
  DECLARE @tabLD TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    Employee nvarchar(20) COLLATE database_default
    UNIQUE (RowID, WBS1, WBS2, WBS3, LaborCode, Employee)
  )

  DECLARE @tabTaskHasChildren TABLE (
    TaskID varchar(32) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default
    UNIQUE (TaskID, LaborCode)
  )

  DECLARE @tabTaskHasAssignment TABLE (
    TaskID varchar(32) COLLATE database_default,
    UNIQUE (TaskID)
  )

  DECLARE @tabTaskHasPlannedHours TABLE (
    TaskID varchar(32) COLLATE database_default,
    UNIQUE (TaskID)
  )

  DECLARE @tabAllTasks TABLE (
    TaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
	LaborCode nvarchar(14) COLLATE database_default,
	Name nvarchar(255) COLLATE database_default,
	OutlineNumber varchar(255) COLLATE database_default,
	ParentOutlineNumber varchar(255) COLLATE database_default,
	OutlineLevel int
    UNIQUE (TaskID)
  )

  --retrieve all tasks with children in the given plan
  INSERT @tabTaskHasChildren(
    TaskID,
    LaborCode
  ) 
  SELECT DISTINCT Parent.TaskID AS TaskID,
    Child.LaborCode AS LaborCode 
	From PNTask Parent INNER JOIN PNTask Child  
		ON Parent.OutlineNumber = Child.ParentOutlineNumber 
		AND Parent.PlanID = Child.PlanID 
		WHERE Parent.PlanID = @strPlanID

  --retrieve all tasks with assignments in the given plan
  INSERT @tabTaskHasAssignment(
    TaskID
  ) 
  SELECT DISTINCT TaskID From PNAssignment 
   WHERE PlanID = @strPlanID

-----------
--retrieve all leaf tasks with planned hours in the given plan
  INSERT @tabTaskHasPlannedHours(
    TaskID
  ) 
  SELECT DISTINCT P.TaskID From PNPlannedLabor P
  INNER JOIN PNTask T ON T.PlanID = P.PlanID --AND T.WBSType = 'LBCD'
   WHERE T.PlanID = @strPlanID AND PeriodHrs > 0 AND P.EndDate >= @dtETCDate

---
  IF @strMode = 'self'
	  BEGIN
	   INSERT @tabAllTasks(
		TaskID,
	    WBS1, 
		WBS2, 
		WBS3,  
		LaborCode, 
		Name,  
		OutlineNumber, 
		ParentOutlineNumber,  
		OutlineLevel
	   ) 
	   SELECT 
		TaskID,
	    WBS1, 
		WBS2, 
		WBS3,  
		PNTask.LaborCode AS LaborCode, 
		CASE WHEN PNTask.LaborCode IS NOT NULL THEN L.LaborCodeName ELSE PNTask.NAME END AS Name,  
		OutlineNumber, 
		ParentOutlineNumber,  
		OutlineLevel	    
	   From PNTask 
	   CROSS APPLY dbo.stRP$tabLaborCode(PNTask.PlanID,PNTask.LaborCode) AS L
	   WHERE TaskID = @strTaskID AND PlanId = @strPlanID
	  END
  ELSE   
	  BEGIN
	   INSERT @tabAllTasks(
		TaskID,
	    WBS1, 
		WBS2, 
		WBS3,  
		LaborCode, 
		Name,  
		OutlineNumber, 
		ParentOutlineNumber,  
		OutlineLevel
	   ) 
	   SELECT 
		TaskID,
	    WBS1, 
		WBS2, 
		WBS3,  
		PNTask.LaborCode AS LaborCode, 
		CASE WHEN PNTask.LaborCode IS NOT NULL THEN L.LaborCodeName ELSE PNTask.NAME END AS Name,  
		OutlineNumber, 
		ParentOutlineNumber,  
		OutlineLevel	    
	   From PNTask 
	   CROSS APPLY dbo.stRP$tabLaborCode(PNTask.PlanID,PNTask.LaborCode) AS L
		WHERE  ParentOutlineNumber = @strOutlineNumber AND PlanId = @strPlanID
	    ORDER BY OutlineNumber
      END 
  --END if @strMode = 'self'

  --retrieve all JTD related to the Plan
  INSERT @tabLD(
    WBS1,
    WBS2,
    WBS3,
    LaborCode,
    Employee
  ) 
    SELECT DISTINCT
      Y.WBS1 AS WBS1,
      Y.WBS2 AS WBS2,
      Y.WBS3 AS WBS3,
      Y.LaborCode AS LaborCode,
      Y.Employee AS Employee
      FROM ( /* Y */
        SELECT
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.LaborCode AS LaborCode,
          SUM(X.JTDHours) AS JTDHours,
          X.Employee AS Employee
          FROM ( /* X */
            SELECT  
              LD.WBS1,
              LD.WBS2,
              LD.WBS3,
              LD.LaborCode,
              SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDHours,
              LD.Employee
              FROM LD
              WHERE LD.WBS1 = @strWBS1 AND LD.Employee IS NOT NULL AND
                LD.ProjectCost = 'Y' AND LD.TransDate <= @dtJTDDate
              GROUP BY LD.WBS1, LD.WBS2, LD.WBS3, LD.LaborCode, LD.Employee
              HAVING SUM(RegHrs + OvtHrs + SpecialOvtHrs) <> 0
            UNION
            SELECT  
              TD.WBS1,
              TD.WBS2,
              TD.WBS3,
              TD.LaborCode,
              SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDHours,
              TD.Employee
              FROM tkDetail AS TD -- TD table has entries at only the leaf level.
                INNER JOIN EM ON TD.Employee = EM.Employee  
                INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
              WHERE TD.WBS1 = @strWBS1 AND TD.Employee IS NOT NULL AND TD.TransDate <= @dtJTDDate  
              GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.LaborCode, TD.Employee   
              HAVING SUM(RegHrs + OvtHrs + SpecialOvtHrs) <> 0
            UNION
            SELECT  
              TD.WBS1,
              TD.WBS2,
              TD.WBS3,
              TD.LaborCode,
              SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDHours,
              TD.Employee
              FROM tsDetail AS TD -- TD table has entries at only the leaf level.
              WHERE TD.WBS1 = @strWBS1 AND TD.Employee IS NOT NULL AND TD.TransDate <= @dtJTDDate  
              GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.LaborCode, TD.Employee    
              HAVING SUM(RegHrs + OvtHrs + SpecialOvtHrs) <> 0
          ) AS X
          GROUP BY X.WBS1, X.WBS2, X.WBS3, X.LaborCode, X.Employee
          HAVING SUM(X.JTDHours) <> 0
      ) AS Y

	 
	 INSERT @tabResourceJTDPlans  (
		TaskID,
		WBS1,
		WBS2,
		WBS3,
		LaborCode,
		Name,
		OutlineNumber,
		ParentOutlineNumber,
		OutlineLevel,
		LeafNode,
		HasChildren,
		HasAssignment,
		HasJTD,
		HasPlannedHours  
  )
	SELECT DISTINCT AllPlanTasks.TaskID AS TaskID, 
		AllPlanTasks.WBS1,
		AllPlanTasks.WBS2,
		AllPlanTasks.WBS3, 
		AllPlanTasks.LaborCode AS LaborCode,
		Name,
		OutlineNumber,
		ParentOutlineNumber, 
		OutlineLevel,
		CASE WHEN TaskHasChildren.TaskID IS NOT NULL THEN   
		CASE WHEN TaskHasChildren.LaborCode IS NOT NULL Then 1 ELSE 0 END 
		ELSE 1 END  As LeafNode, 
		CASE WHEN TaskHasChildren.TaskID IS NOT Null Then 1 Else 0 END As HasChildren, 
		CASE WHEN TaskHasAssignment.TaskID IS NOT Null Then 1 Else 0 END As HasAssignment,
		CASE WHEN LD.WBS1 IS NOT Null Then 1 Else 0 END As HasJTD,
		CASE WHEN TaskHasPlannedHours.TaskID IS NOT Null Then 1 Else 0 END As HasPlannedHours
		FROM @tabAllTasks AS AllPlanTasks 
		LEFT JOIN @tabTaskHasAssignment AS TaskHasAssignment ON TaskHasAssignment.TaskID = AllPlanTasks.TaskID 
		LEFT JOIN @tabTaskHasChildren AS TaskHasChildren ON TaskHasChildren.TaskID = AllPlanTasks.TaskID
		LEFT JOIN @tabLD AS LD ON LD.WBS1 = AllPlanTasks.WBS1 
								  AND LD.WBS2 LIKE (ISNULL(AllPlanTasks.WBS2, '%'))
								  AND LD.WBS3 LIKE (ISNULL(AllPlanTasks.WBS3 + '%', '%'))							
								  AND ISNULL(LD.LaborCode, '%') LIKE ISNULL(AllPlanTasks.LaborCode, '%')
		LEFT JOIN @tabTaskHasPlannedHours TaskHasPlannedHours ON TaskHasPlannedHours.TaskID = AllPlanTasks.TaskID 			                           
				                             
 -- stRP$tabPlanningTaskInfo
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
