SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_PAT_GetGroups] (@Employee VARCHAR(20),@Context VARCHAR(20))
RETURNS @T TABLE
(
	PKey varchar(50),--PKey Value Globally unique
	Code varchar(50),--Code Value, unique per type
	GroupName varchar(100),--Display Value
	CascadeMenu varchar(100),--optional if should be under a cascade menu
	--GroupDescription varchar(255),
	FilterType varchar(100),--PayableGroup (Default when null), indicates value will be in CCG_PAT_Payable.PayableGroup field.  Otherwise 'function' is also valid here to use dynamic groups provided by a sql function
	FilterDetail varchar(500),--function name or other detail
	DefaultGroup varchar(1)
)
 AS BEGIN
--select * from dbo.fnCCG_PAT_GetGroups('00001',null)


	declare @AcctRoles varchar(1024)
	declare @VisionRole varchar(255)
	declare @PATRoleLabel varchar(255)
	declare @IsAcct varchar(1)
	declare @IsAdmin varchar(1)--NS specific
	declare @MyOrg varchar(20)


	insert into @T (PKey,CascadeMenu,Code,GroupName,FilterType,DefaultGroup)
	select distinct '[ALL]',null, '[ALL]','All','[ALL]','Y'

		insert into @T (PKey,CascadeMenu,Code,GroupName,FilterType,DefaultGroup)
	select distinct 'MY',null, @Employee,'My records','CreateUser','N'

	insert into @T (PKey,CascadeMenu,Code,GroupName,FilterType,FilterDetail)
                              select 'PENDING_MY',null, @Employee,'My Next In Route','function','dbo.fnCCG_PAT_GroupNextRoute'	
--Optional, Records of other users
--select distinct CreateUser from CCG_PAT_Payable
	insert into @T (PKey,CascadeMenu,Code,GroupName,FilterType)
	select distinct 'CB' + p.CreateUser,'Created By', p.CreateUser,EM.LastName + ', ' + EM.FirstName,'CreateUser' from CCG_PAT_Payable P inner join EM on p.CreateUser = EM.Employee
		
	return
 END

GO
