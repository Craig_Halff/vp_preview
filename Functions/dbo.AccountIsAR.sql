SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[AccountIsAR]
 (@Account varchar(13), 
  @Company varchar(14)) 
  RETURNS Nvarchar(1)  AS
BEGIN
	DECLARE @count as int
	DECLARE @retYN as varchar(1)
	
	Select @count = COUNT(*) From (
	Select Account from CFGARMap where Account = @Account and Company = @Company 
	Union Select ARAccount from CFGInvMap where ARAccount = @Account and Company = @Company) myAR
            
    if @count>0	
		SET @retYN = 'Y'
	else
		SET @retYN = 'N'
	RETURN @retYN
	
END -- AccountIsAR
GO
