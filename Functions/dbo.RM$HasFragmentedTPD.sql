SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RM$HasFragmentedTPD]
  (@strPlanID VARCHAR(32))
  RETURNS tinyint
BEGIN

  /*--------------------------------------------------------------------------------------------------------------------------------------------------*/
  /*  This function is used to determine whether the given Plan has multiple rows of Time-Phased Data (TPD) for any calendar period.                  */
  /*  Resource Management (RM) could generate fragmented TPD when RM has scale (e.g. Weekly) that is smaller the scale in Plans (e.g. Monthly).       */
  /*--------------------------------------------------------------------------------------------------------------------------------------------------*/

  DECLARE @tabCalendarInterval
    TABLE(PlanID varchar(32) COLLATE database_default,
          StartDate datetime,
          EndDate datetime,
          PeriodScale varchar(1) COLLATE database_default
          PRIMARY KEY(PlanID, StartDate, EndDate))

  DECLARE @tiHasFragmentedTPD tinyint

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


  INSERT @tabCalendarInterval(PlanID, StartDate, EndDate, PeriodScale)
  SELECT PlanID, StartDate, EndDate, PeriodScale FROM
    (SELECT 
       PlanID AS PlanID, 
       CAST('19900101' AS datetime) AS StartDate,
       DATEADD(d, -1, MIN(StartDate)) AS EndDate,
       'o' AS PeriodScale
       FROM RPCalendarInterval WHERE PlanID = @strPlanID
       GROUP BY PlanID
     UNION ALL
     SELECT 
       PlanID AS PlanID, 
       StartDate AS StartDate, 
       EndDate AS EndDate, 
       PeriodScale AS PeriodScale
       FROM RPCalendarInterval WHERE PlanID = @strPlanID
     UNION ALL
     SELECT 
       PlanID AS PlanID, 
       DATEADD(d, 1, MAX(EndDate)) AS StartDate,
       DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate,
       'o' AS PeriodScale
       FROM RPCalendarInterval WHERE PlanID = @strPlanID
       GROUP BY PlanID) AS CI

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Resource Management only deals with Time-Phased Data rows hung under Assignment rows, and not under "Childless Task" rows.

  SELECT @tiHasFragmentedTPD = 
    CASE WHEN EXISTS
      (SELECT 
         COUNT(TPD.TimePhaseID) AS XCount
         FROM @tabCalendarInterval AS CI 
           LEFT JOIN RPAssignment AS A ON CI.PlanID = A.PlanID
           INNER JOIN RPPlannedLabor AS TPD ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID 
             AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
             AND TPD.AssignmentID = A.AssignmentID
         WHERE TPD.TimePhaseID IS NOT NULL
         GROUP BY CI.StartDate, A.AssignmentID
         HAVING COUNT(TPD.TimePhaseID) > 1)
      THEN 1 ELSE 0 END

  RETURN(@tiHasFragmentedTPD)

END -- fn_RM$HasFragmentedTPD
GO
