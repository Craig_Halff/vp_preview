SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$tabConTask]
  (@strWBS1 Nvarchar(30),
   @strJTDDate VARCHAR(8)) -- Date must be in format: 'yyyymmdd' regardless of UI Culture.

  RETURNS @tabConTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default, /* Only Plans, that were created from Navigator, have true TaskID */
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    Name Nvarchar(40) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    MinDate datetime,
    MaxDate datetime,
    WBSType varchar(4) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int,
    PlannedConCost decimal(19,4),
    PlannedConBill decimal(19,4),
    PlannedReimConCost decimal(19,4),
    PlannedReimConBill decimal(19,4),
    BaselineConCost decimal(19,4),
    BaselineConBill decimal(19,4),
    JTDConCost decimal(19,4),
    JTDConBill decimal(19,4),
    JTDReimConCost decimal(19,4),
    JTDReimConBill decimal(19,4),
    FeeConCost decimal(19,4),
    FeeConBill decimal(19,4),
    FeeDirConCost decimal(19,4),
    FeeDirConBill decimal(19,4),
    FeeReimConCost decimal(19,4),
    FeeReimConBill decimal(19,4),
    MarkupConCost decimal(19,4),
    FeeJTDConBill decimal(19,4),
    ETCConCost decimal(19,4),
    ETCConBill decimal(19,4)
  )

BEGIN

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
 
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strPlanID varchar(32)

  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)
  
  DECLARE @strExpTab varchar(1)
  DECLARE @strConTab varchar(1)

  DECLARE @strVorN char(1)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

  DECLARE @tiDefConWBSLevel tinyint
  DECLARE @tiMinConWBSLevel tinyint
  
  -- Declare Temp tables.

  DECLARE @tabWBSTree TABLE (
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    Name Nvarchar(40) COLLATE database_default,
    FeeConCost decimal(19,4),
    FeeConBill decimal(19,4),
    FeeDirConCost decimal(19,4),
    FeeDirConBill decimal(19,4),
    FeeReimConCost decimal(19,4),
    FeeReimConBill decimal(19,4),
    WBSLevel tinyint,
    IsLeaf bit
    UNIQUE(WBS1, WBS2, WBS3, WBSLevel, IsLeaf)
  )

  DECLARE @tabMappedTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    WBSType varchar(4) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChildrenCount int,
    OutlineLevel int,
    BaselineConCost decimal(19,4),
    BaselineConBill decimal(19,4)
    UNIQUE(PlanID, TaskID)
  )

  DECLARE @tabPlanned TABLE(
    RowID  int IDENTITY(1,1),
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    Account Nvarchar(13) COLLATE database_default,
    Vendor Nvarchar(30) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PlannedCost decimal(19,4),
    PlannedBill decimal(19,4),
    PlannedReimCost decimal(19,4),
    PlannedReimBill decimal(19,4),
    ETCCost decimal(19,4),
    ETCBill decimal(19,4)
    UNIQUE(RowID, WBS1, WBS2, WBS3, Account, Vendor)
  )
    
 	DECLARE @tabJTD TABLE(
    RowID  int IDENTITY(1,1),
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    Account Nvarchar(13) COLLATE database_default,
    Vendor Nvarchar(30) COLLATE database_default,
    JTDCost decimal(19,4),
    JTDBill decimal(19,4),
    JTDReimCost decimal(19,4),
    JTDReimBill decimal(19,4)
    UNIQUE(RowID, WBS1, WBS2, WBS3, Account, Vendor)
  )

  DECLARE @tabETC TABLE (
    RowID  int IDENTITY(1,1),
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    ETCCost decimal(19,4),
    ETCBill decimal(19,4)
    UNIQUE(RowID, WBS1, WBS2, WBS3)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @tiMinConWBSLevel = 3 -- Leaf Level

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
          
  -- Set JTD/ETC Dates.
  
  SET @dtJTDDate = CONVERT(datetime, @strJTDDate)
  SET @dtETCDate = DATEADD(d, 1, @dtJTDDate) 

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat     

  -- Save Company string for use later.
  
  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          
  -- Determine which set of tables need to be used.

  SET @strVorN = dbo.PN$PlanIs_VorN(@strWBS1)

  -- Get Default Plan Settings from CFGResourcePlanning

  SELECT
    @tiDefConWBSLevel = ConWBSLevel
    FROM CFGResourcePlanning WHERE Company = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN @strCompany ELSE ' ' END

  -- Initialize @strPlanID

  SET @strPlanID = NULL

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine the minimum WBS level where Consultant rows need to be attached.

  IF (@strVorN = 'V')
    BEGIN

      -- Start with the default ConWBSLevel. If there is no Consultant row in the Vision Plan then @tiMinConWBSLevel = ConWBSLevel.
      -- If the Vision Plan has Consultant rows, then the highest level found will be used as ConWBSLevel.

      SELECT @tiMinConWBSLevel = ISNULL(MIN(WBSLevel), @tiDefConWBSLevel) FROM
        (SELECT 1 AS WBSLevel
           FROM RPConsultant AS C
             INNER JOIN PR ON C.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND C.WBS2 IS NULL AND C.WBS3 IS NULL AND PR.SubLevel = 'Y'
             INNER JOIN RPPlan AS P ON C.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y'
           WHERE C.WBS1 = @strWBS1
         UNION
         SELECT 2 AS WBSLevel
           FROM RPConsultant AS C
             INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND PR.WBS3 = ' ' AND C.WBS3 IS NULL AND PR.SubLevel = 'Y'
             INNER JOIN RPPlan AS P ON C.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y'
           WHERE C.WBS1 = @strWBS1
         UNION
         SELECT 3 AS WBSLevel
           FROM RPConsultant AS C
             INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND PR.WBS3 = ' ' AND C.WBS3 IS NULL AND PR.SubLevel = 'N'
             INNER JOIN RPPlan AS P ON C.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y'
          WHERE C.WBS1 = @strWBS1
         UNION
         SELECT 3 AS WBSLevel
           FROM RPConsultant AS C
             INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND C.WBS3 = PR.WBS3
             INNER JOIN RPPlan AS P ON C.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y'
           WHERE C.WBS1 = @strWBS1
        ) AS X

    END /* End If (@strVorN = 'V') */
  ELSE IF (@strVorN = 'N')
    BEGIN

      -- For Navigator Plans, @tiMinConWBSLevel = ConWBSLevel from PNPlan

      SELECT @tiMinConWBSLevel = ConWBSLevel
        FROM PNPlan AS P 
        WHERE P.WBS1 = @strWBS1

    END 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabWBSTree (
    WBS1,
    WBS2,
    WBS3,
    Name,
    FeeConCost,
    FeeConBill,
    FeeDirConCost,
    FeeDirConBill,
    FeeReimConCost,
    FeeReimConBill,
    WBSLevel,
    IsLeaf
  )
    SELECT
      WBS1,
      WBS2,
      WBS3,
      Name,
      (FeeDirConCost + FeeReimConCost) AS FeeConCost,
      (FeeDirConBill + FeeReimConBill) AS FeeConBill,
      FeeDirConCost,
      FeeDirConBill,
      FeeReimConCost,
      FeeReimConBill,
      WBSLevel,
      IsLeaf
      FROM
        (SELECT
           WBS1, 
           WBS2, 
           WBS3,
           Name, 
           ConsultFee AS FeeDirConCost,
           (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN ConsultFeeBillingCurrency ELSE ConsultFee END) AS FeeDirConBill,
           ReimbAllowCons AS FeeReimConCost,
           (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN ReimbAllowConsBillingCurrency ELSE ReimbAllowCons END) AS FeeReimConBill,
           CASE 
             WHEN WBS2 = ' ' AND WBS3 = ' ' THEN 1
             WHEN WBS2 != ' ' AND WBS3 = ' ' THEN 2
             WHEN WBS2 != ' ' AND WBS3 != ' ' THEN 3
           END AS WBSLevel,
           CASE WHEN SubLevel = 'N' THEN 1 ELSE 0 END AS IsLeaf
           FROM PR
           WHERE WBS1 = @strWBS1
        ) AS X
      WHERE WBSLevel <= @tiMinConWBSLevel

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabJTD(
    WBS1,
    WBS2,
    WBS3,
    Account,
    Vendor,
    JTDCost,
    JTDBill,
    JTDReimCost,
    JTDReimBill
  )
		SELECT
      X.WBS1,
      X.WBS2,
      X.WBS3,
      X.Account,
      X.Vendor,
      ISNULL(SUM(JTDCost), 0.0000) AS JTDCost,
      ISNULL(SUM(JTDBill), 0.0000) AS JTDBill,
      ISNULL(SUM(JTDReimCost), 0.0000) AS JTDReimCost,
      ISNULL(SUM(JTDReimBill), 0.0000) AS JTDReimBill
      FROM (
        SELECT
          LG.WBS1,
          LG.WBS2,
          LG.WBS3,
          LG.Account,
          LG.Vendor,
          SUM(LG.AmountProjectCurrency) AS JTDCost,  
          SUM(LG.BillExt) AS JTDBill,
          SUM(CASE WHEN CA.Type = 6 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
          SUM(CASE WHEN CA.Type = 6 THEN LG.BillExt ELSE 0 END) AS JTDReimBill
          FROM LedgerAR AS LG
            INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
          WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (6, 8)
          GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
        UNION ALL SELECT
          LG.WBS1,
          LG.WBS2,
          LG.WBS3,
          LG.Account,
          LG.Vendor,
          SUM(LG.AmountProjectCurrency) AS JTDCost,  
          SUM(LG.BillExt) AS JTDBill,
          SUM(CASE WHEN CA.Type = 6 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
          SUM(CASE WHEN CA.Type = 6 THEN LG.BillExt ELSE 0 END) AS JTDReimBill
          FROM LedgerAP AS LG
            INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
          WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (6, 8)
          GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
        UNION ALL SELECT
          LG.WBS1,
          LG.WBS2,
          LG.WBS3,
          LG.Account,
          LG.Vendor,
          SUM(LG.AmountProjectCurrency) AS JTDCost,  
          SUM(LG.BillExt) AS JTDBill,
          SUM(CASE WHEN CA.Type = 6 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
          SUM(CASE WHEN CA.Type = 6 THEN LG.BillExt ELSE 0 END) AS JTDReimBill
          FROM LedgerEX AS LG
            INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
          WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (6, 8)
          GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
        UNION ALL SELECT
          LG.WBS1,
          LG.WBS2,
          LG.WBS3,
          LG.Account,
          LG.Vendor,
          SUM(LG.AmountProjectCurrency) AS JTDCost,  
          SUM(LG.BillExt) AS JTDBill,
          SUM(CASE WHEN CA.Type = 6 THEN LG.AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
          SUM(CASE WHEN CA.Type = 6 THEN LG.BillExt ELSE 0 END) AS JTDReimBill
          FROM LedgerMISC AS LG
            INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
          WHERE LG.ProjectCost = 'Y' AND LG.TransDate <= @dtJTDDate AND LG.WBS1 = @strWBS1 AND CA.Type IN (6, 8)
          GROUP BY LG.WBS1, LG.WBS2, LG.WBS3, LG.Account, LG.Vendor
        UNION ALL SELECT
          POC.WBS1,
          POC.WBS2,
          POC.WBS3,
          POC.Account,
          POM.Vendor,
          SUM(AmountProjectCurrency) AS JTDCost,  
          SUM(BillExt) AS JTDBill,
          SUM(CASE WHEN CA.Type = 6 THEN AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
          SUM(CASE WHEN CA.Type = 6 THEN BillExt ELSE 0 END) AS JTDReimBill
          FROM POCommitment AS POC 
            INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
            INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey
            INNER JOIN CA ON POC.Account = CA.Account
          WHERE POM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (6, 8) AND 
            (AmountProjectCurrency != 0 AND BillExt != 0)
          GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
        UNION ALL SELECT
          POC.WBS1,
          POC.WBS2,
          POC.WBS3,
          POC.Account,
          POM.Vendor,
          SUM(AmountProjectCurrency) AS JTDCost,  
          SUM(BillExt) AS JTDBill,
          SUM(CASE WHEN CA.Type = 6 THEN AmountProjectCurrency ELSE 0 END) AS JTDReimCost,
          SUM(CASE WHEN CA.Type = 6 THEN BillExt ELSE 0 END) AS JTDReimBill
          FROM POCommitment AS POC 
            INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
            INNER JOIN POCOMaster AS POCOM ON POD.COPKey = POCOM.PKey
            INNER JOIN POMaster AS POM ON POCOM.MasterPKey = POM.MasterPKey
            INNER JOIN CA ON POC.Account = CA.Account
          WHERE POCOM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (6, 8) AND 
            (AmountProjectCurrency != 0 AND BillExt != 0)
          GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor
      ) AS X
      GROUP BY X.WBS1, X.WBS2, X.WBS3, X.Account, X.Vendor
      HAVING SUM(ISNULL(JTDCost, 0.0000)) <> 0 OR SUM(ISNULL(JTDBill, 0.0000)) <> 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save RPTask/PNTask that are mapped to @strWBS1. There could be multiple Plans with UlilizationIncludeFlag = 'Y'
  -- Save TPD to be used later.

  IF (@strVorN = 'V')
    BEGIN

      INSERT @tabMappedTask
        (PlanID,
         TaskID,
         WBS1,
         WBS2,
         WBS3,
         StartDate,
         EndDate,
         WBSType,
         ParentOutlineNumber,
         OutlineNumber,
         ChildrenCount,
         OutlineLevel,
         BaselineConCost,
         BaselineConBill
        )
        SELECT
          P.PlanID,
          T.TaskID,
          T.WBS1,
          ISNULL(T.WBS2, ' '),
          ISNULL(T.WBS3, ' '),
          T.StartDate,
          T.EndDate,
          T.WBSType,
          T.ParentOutlineNumber,
          T.OutlineNumber,
          T.ChildrenCount,
          T.OutlineLevel,
          T.BaselineConCost,
          T.BaselineConBill
          FROM RPTask AS T
            INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND T.WBS1 = @strWBS1 AND
              ((T.WBSType = 'WBS1' AND T.WBS1 <> '<none>') OR
               (T.WBSType = 'WBS2' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>') OR
               (T.WBSType = 'WBS3' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>' AND T.WBS3 <> '<none>'))
            INNER JOIN @tabWBSTree AS WT ON
              T.WBS1 = WT.WBS1 AND ISNULL(T.WBS2, ' ') = WT.WBS2 AND ISNULL(T.WBS3, ' ') = WT.WBS3
 
    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Find Plan.

      SELECT @strPlanID = P.PlanID
        FROM RPPlan AS P
        WHERE P.UtilizationIncludeFlg = 'Y' AND P.WBS1 = @strWBS1

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabPlanned (
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor,
        StartDate,
        EndDate,
        PlannedCost,
        PlannedBill,
        PlannedReimCost,
        PlannedReimBill,
        ETCCost,
        ETCBill
      )
        SELECT
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.Account AS Account,
          X.Vendor AS Vendor,
          MIN(X.StartDate) AS StartDate,
          MAX(X.EndDate) AS EndDate,
          ISNULL(SUM(X.PlannedCost), 0.0000) AS PlannedCost,
          ISNULL(SUM(X.PlannedBill), 0.0000) AS PlannedBill,
          ISNULL(SUM(X.PlannedReimCost), 0.0000) AS PlannedReimCost,
          ISNULL(SUM(X.PlannedReimBill), 0.0000) AS PlannedReimBill,
          ISNULL(SUM(X.ETCCost), 0.0000) AS ETCCost,
          ISNULL(SUM(X.ETCBill), 0.0000) AS ETCBill
          FROM (
            /* Find TPD rows attached to RPConsultant rows for Plan that is mapped to Project in question */
            SELECT 
              C.WBS1 AS WBS1,
              C.WBS2 AS WBS2,
              C.WBS3 AS WBS3,
              C.Account AS Account,
              C.Vendor AS Vendor,
              TPD.StartDate AS StartDate,
              TPD.EndDate AS EndDate,
              ISNULL(TPD.PeriodCost, 0) AS PlannedCost, 
              ISNULL(TPD.PeriodBill, 0) AS PlannedBill,
              CASE WHEN CA.Type = 6 THEN ISNULL(TPD.PeriodCost, 0) ELSE 0 END AS PlannedReimCost,
              CASE WHEN CA.Type = 6 THEN ISNULL(TPD.PeriodBill, 0) ELSE 0 END AS PlannedReimBill,
              CASE 
                WHEN (TPD.StartDate >= @dtETCDate AND TPD.EndDate >= @dtETCDate) THEN TPD.PeriodCost
	              WHEN (TPD.StartDate < @dtETCDate AND TPD.EndDate >= @dtETCDate) THEN TPD.PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) 
                ELSE 0
              END AS ETCCost,
              CASE 
                WHEN (TPD.StartDate >= @dtETCDate AND TPD.EndDate >= @dtETCDate) THEN TPD.PeriodBill
	              WHEN (TPD.StartDate < @dtETCDate AND TPD.EndDate >= @dtETCDate) THEN TPD.PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) 
                ELSE 0
              END AS ETCBill
              FROM RPPlannedConsultant AS TPD
                INNER JOIN RPConsultant AS C ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID AND TPD.ConsultantID = C.ConsultantID AND TPD.ConsultantID IS NOT NULL
                INNER JOIN CA ON C.Account = CA.Account
              WHERE TPD.PlanID = @strPlanID
          ) AS X
          GROUP BY WBS1, WBS2, WBS3, Account, Vendor

    END /* End If (@strVorN = 'V') */

  ELSE IF (@strVorN = 'N')
    BEGIN

      INSERT @tabMappedTask
        (PlanID,
         TaskID,
         WBS1,
         WBS2,
         WBS3,
         StartDate,
         EndDate,
         WBSType,
         ParentOutlineNumber,
         OutlineNumber,
         ChildrenCount,
         OutlineLevel,
         BaselineConCost,
         BaselineConBill
        )
        SELECT
          P.PlanID,
          T.TaskID,
          T.WBS1,
          ISNULL(T.WBS2, ' '),
          ISNULL(T.WBS3, ' '),
          T.StartDate,
          T.EndDate,
          T.WBSType,
          T.ParentOutlineNumber,
          T.OutlineNumber,
          T.ChildrenCount,
          T.OutlineLevel,
          T.BaselineConCost,
          T.BaselineConBill
          FROM PNTask AS T
            INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND T.WBS1 = @strWBS1 AND
              ((T.WBSType = 'WBS1' AND T.WBS1 <> '<none>') OR
               (T.WBSType = 'WBS2' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>') OR
               (T.WBSType = 'WBS3' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>' AND T.WBS3 <> '<none>'))
            INNER JOIN @tabWBSTree AS WT ON
              T.WBS1 = WT.WBS1 AND ISNULL(T.WBS2, ' ') = WT.WBS2 AND ISNULL(T.WBS3, ' ') = WT.WBS3
 
    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Find Plan.

      SELECT @strPlanID = P.PlanID
        FROM PNPlan AS P
        WHERE P.UtilizationIncludeFlg = 'Y' AND P.WBS1 = @strWBS1

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabPlanned (
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor,
        StartDate,
        EndDate,
        PlannedCost,
        PlannedBill,
        PlannedReimCost,
        PlannedReimBill,
        ETCCost,
        ETCBill
      )
        SELECT
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.Account AS Account,
          X.Vendor AS Vendor,
          MIN(X.StartDate) AS StartDate,
          MAX(X.EndDate) AS EndDate,
          ISNULL(SUM(X.PlannedCost), 0.0000) AS PlannedCost,
          ISNULL(SUM(X.PlannedBill), 0.0000) AS PlannedBill,
          ISNULL(SUM(X.PlannedReimCost), 0.0000) AS PlannedReimCost,
          ISNULL(SUM(X.PlannedReimBill), 0.0000) AS PlannedReimBill,
          ISNULL(SUM(X.ETCCost), 0.0000) AS ETCCost,
          ISNULL(SUM(X.ETCBill), 0.0000) AS ETCBill
          FROM (
            /* For Navigator Plan, use Planned Cost and Bill Amounts from PNConsultant rows.  */
            /* There is no need to sum up from the TPD rows.                               */
            SELECT 
              C.WBS1 AS WBS1,
              C.WBS2 AS WBS2,
              C.WBS3 AS WBS3,
              C.Account AS Account,
              C.Vendor AS Vendor,
              C.StartDate AS StartDate,
              C.EndDate AS EndDate,
              ISNULL(C.PlannedConCost, 0) AS PlannedCost, 
              ISNULL(C.PlannedConBill, 0) AS PlannedBill,
              CASE WHEN CA.Type = 6 THEN ISNULL(C.PlannedConCost, 0) ELSE 0 END AS PlannedReimCost,
              CASE WHEN CA.Type = 6 THEN ISNULL(C.PlannedConBill, 0) ELSE 0 END AS PlannedReimBill,
              0.0000 ETCCost,
              0.0000 ETCBill
              FROM PNConsultant AS C
                INNER JOIN CA ON C.Account = CA.Account
              WHERE C.PlanID = @strPlanID
          ) AS X
          GROUP BY WBS1, WBS2, WBS3, Account, Vendor

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabETC (
        WBS1,
        WBS2,
        WBS3,
        ETCCost,
        ETCBill
      )
        SELECT
          WBS1 AS WBS1,
          WBS2 AS WBS2,
          WBS3 AS WBS3,
          SUM(ISNULL(ETCCost, 0.0000)) AS ETCCost,
          SUM(ISNULL(ETCBill, 0.0000)) AS ETCBill
          FROM (
            SELECT
              R.WBS1 AS WBS1,
              R.WBS2 AS WBS2,
              R.WBS3 AS WBS3,
              R.Account AS Account,
              R.Vendor AS Vendor,
              CASE
                WHEN ISNULL(TPD.PlannedCost, 0.0000) > ISNULL(JTD.JTDCost, 0.0000)
                THEN ISNULL(TPD.PlannedCost, 0.0000) - ISNULL(JTD.JTDCost, 0.0000)
                ELSE 0.0000
              END AS ETCCost,
              CASE
                WHEN ISNULL(TPD.PlannedBill, 0.0000) > ISNULL(JTD.JTDBill, 0.0000)
                THEN ISNULL(TPD.PlannedBill, 0.0000) - ISNULL(JTD.JTDBill, 0.0000)
                ELSE 0.0000
              END AS ETCBill

              FROM (
                SELECT DISTINCT
                  WBS1 AS WBS1,
                  ISNULL(WBS2, ' ') AS WBS2,
                  ISNULL(WBS3, ' ') AS WBS3,
                  Account AS Account,
                  Vendor AS Vendor
                  FROM PNConsultant
                  WHERE PlanID = @strPlanID
              ) AS R

                LEFT JOIN (
                  SELECT
                    WBS1 AS WBS1,
                    ISNULL(WBS2, ' ') AS WBS2,
                    ISNULL(WBS3, ' ') AS WBS3,
                    Account AS Account,
                    Vendor AS Vendor,
                    SUM(PlannedCost) AS PlannedCost,
                    SUM(PlannedBill) AS PlannedBill
                    FROM @tabPlanned
                    GROUP BY WBS1, WBS2, WBS3, Account, Vendor
                ) AS TPD
                  ON R.WBS1 = TPD.WBS1 AND R.WBS2 = TPD.WBS2 AND R.WBS3 = TPD.WBS3 AND R.Account = TPD.Account AND ISNULL(R.Vendor, '|') = ISNULL(TPD.Vendor, '|')

                LEFT JOIN (
                  SELECT
                    WTZ.WBS1 AS WBS1,
                    WTZ.WBS2 AS WBS2,
                    WTZ.WBS3 AS WBS3,
                    Account AS Account,
                    Vendor AS Vendor,
                    ISNULL(SUM(JZ.JTDCost), 0.0000) AS JTDCost,
                    ISNULL(SUM(JZ.JTDBill), 0.0000) AS JTDBill
                    FROM @tabWBSTree AS WTZ
                      OUTER APPLY (
                        SELECT
                          Account AS Account,
                          Vendor AS Vendor,
                          ISNULL(SUM(TPD.JTDCost), 0.0000) AS JTDCost,
                          ISNULL(SUM(TPD.JTDBill), 0.0000) AS JTDBill
                          FROM @tabJTD AS TPD
                          WHERE TPD.WBS1 = WTZ.WBS1 AND
                            (ISNULL(TPD.WBS2, '|') LIKE CASE WHEN WTZ.WBS2 = ' ' THEN '%' ELSE WTZ.WBS2 END) AND
                            (ISNULL(TPD.WBS3, '|') LIKE CASE WHEN WTZ.WBS3 = ' ' THEN '%' ELSE WTZ.WBS3 END) 
                          GROUP BY TPD.WBS1, TPD.WBS2, TPD.WBS3, TPD.Account, TPD.Vendor
                      ) AS JZ
                    GROUP BY WTZ.WBS1, WTZ.WBS2, WTZ.WBS3, JZ.Account, JZ.Vendor
                ) AS JTD 
                  ON R.WBS1 = JTD.WBS1 AND R.WBS2 = JTD.WBS2 AND R.WBS3 = JTD.WBS3 AND R.Account = JTD.Account AND ISNULL(R.Vendor, '|') = ISNULL(JTD.Vendor, '|')

          ) AS ETC
            GROUP BY WBS1, WBS2, WBS3

   END /* End If (@strVorN = 'N') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Combine data for Task rows.
  -- ContractAmt and JTDConBill come from Project, therefore should not be counted multiple times.
  -- PlannedConBill comes from all RPTask rows of Plans that were mapped to the Project.
  --   There could be more than one Plan that was mapped to the Project.
  --   Therefore, PlannedConBill need to be grouped by WBS1/WBS2/WBS3 for the collection of all RPTask rows. 

  INSERT @tabConTask (
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    Name,
    StartDate,
    EndDate,
    MinDate,
    MaxDate,
    WBSType,
    ParentOutlineNumber,
    OutlineNumber,
    OutlineLevel,
    PlannedConCost,
    PlannedConBill,
    PlannedReimConCost,
    PlannedReimConBill,
    BaselineConCost,
    BaselineConBill,
    JTDConCost,
    JTDConBill,
    JTDReimConCost,
    JTDReimConBill,
    FeeConCost,
    FeeConBill,
    FeeDirConCost,
    FeeDirConBill,
    FeeReimConCost,
    FeeReimConBill,
    MarkupConCost,
    FeeJTDConBill,
    ETCConCost,
    ETCConBill
  )
    SELECT
      X.PlanID AS PlanID,
      X.TaskID AS TaskID,
      WT.WBS1 AS WBS1,
      WT.WBS2 AS WBS2,
      WT.WBS3 AS WBS3,
      WT.Name AS Name,
      X.StartDate AS StartDate,
      X.EndDate AS EndDate,
      PX.StartDate AS MinDate,
      PX.EndDate AS MaxDate,
      X.WBSType AS WBSType,
      X.ParentOutlineNumber AS ParentOutlineNumber,
      X.OutlineNumber AS OutlineNumber,
      X.OutlineLevel AS OutlineLevel,
      ISNULL(PX.PlannedConCost, 0.0000) AS PlannedConCost,
      ISNULL(PX.PlannedConBill, 0.0000) AS PlannedConBill,
      ISNULL(PX.PlannedReimConCost, 0.0000) AS PlannedReimConCost,
      ISNULL(PX.PlannedReimConBill, 0.0000) AS PlannedReimConBill,
      ISNULL(X.BaselineConCost, 0.0000) AS BaselineConCost,
      ISNULL(X.BaselineConBill, 0.0000) AS BaselineConBill,
      ISNULL(JX.JTDConCost, 0.0000) AS JTDConCost,
      ISNULL(JX.JTDConBill, 0.0000) AS JTDConBill,
      ISNULL(JX.JTDReimConCost, 0.0000) AS JTDReimConCost,
      ISNULL(JX.JTDReimConBill, 0.0000) AS JTDReimConBill,
      ISNULL(WT.FeeConCost, 0.0000) AS FeeConCost,
      ISNULL(WT.FeeConBill, 0.0000) AS FeeConBill,
      ISNULL(WT.FeeDirConCost, 0.0000) AS FeeDirConCost,
      ISNULL(WT.FeeDirConBill, 0.0000) AS FeeDirConBill,
      ISNULL(WT.FeeReimConCost, 0.0000) AS FeeReimConCost,
      ISNULL(WT.FeeReimConBill, 0.0000) AS FeeReimConBill,
      (ISNULL(WT.FeeConCost, 0.0000) - ISNULL(PX.PlannedConCost, 0.0000)) AS MarkupConCost,
      (ISNULL(WT.FeeConBill, 0.0000) - ISNULL(JX.JTDConBill, 0.0000)) AS FeeJTDConBill,

      CASE
        WHEN @strVorN = 'V' THEN ISNULL(PX.ETCCost, 0.0000)
        WHEN @strVorN = 'N' THEN ISNULL(CX.ETCCost, 0.0000)
        ELSE 0.00000
      END AS ETCConCost,

      CASE
        WHEN @strVorN = 'V' THEN ISNULL(PX.ETCBill, 0.0000)
        WHEN @strVorN = 'N' THEN ISNULL(CX.ETCBill, 0.0000)
        ELSE 0.00000
      END AS ETCConBill

      FROM @tabWBSTree AS WT

        LEFT JOIN (
          SELECT
            MIN(MT.PlanID) AS PlanID,
            MIN(MT.TaskID) AS TaskID,
            MT.WBS1,
            MT.WBS2,
            MT.WBS3,
            MIN(MT.StartDate) AS StartDate,
            MAX(MT.EndDate) AS EndDate,
            MIN(MT.WBSType) AS WBSType,
            MIN(MT.ParentOutlineNumber) AS ParentOutlineNumber,
            MIN(MT.OutlineNumber) AS OutlineNumber,
            MIN(MT.OutlineLevel) AS OutlineLevel,
            SUM(MT.BaselineConCost) AS BaselineConCost,
            SUM(MT.BaselineConBill) AS BaselineConBill
            FROM @tabMappedTask AS MT
            GROUP BY MT.WBS1, MT.WBS2, MT.WBS3
        ) AS X ON WT.WBS1 = X.WBS1 AND WT.WBS2 = X.WBS2 AND WT.WBS3 = X.WBS3

        LEFT JOIN (
          SELECT
            WTZ.WBS1 AS WBS1,
            WTZ.WBS2 AS WBS2,
            WTZ.WBS3 AS WBS3,
            MIN(PZ.StartDate) AS StartDate,
            MAX(PZ.EndDate) AS EndDate,
            ISNULL(SUM(PZ.PlannedConCost), 0.0000) AS PlannedConCost,
            ISNULL(SUM(PZ.PlannedConBill), 0.0000) AS PlannedConBill,
            ISNULL(SUM(PZ.PlannedReimConCost), 0.0000) AS PlannedReimConCost,
            ISNULL(SUM(PZ.PlannedReimConBill), 0.0000) AS PlannedReimConBill,
            ISNULL(SUM(PZ.ETCCost), 0.0000) AS ETCCost,
            ISNULL(SUM(PZ.ETCBill), 0.0000) AS ETCBill
            FROM @tabWBSTree AS WTZ
              OUTER APPLY (
                SELECT
                  MIN(TPD.StartDate) AS StartDate,
                  MAX(TPD.EndDate) AS EndDate,
                  ISNULL(SUM(TPD.PlannedCost), 0.0000) AS PlannedConCost,
                  ISNULL(SUM(TPD.PlannedBill), 0.0000) AS PlannedConBill,
                  ISNULL(SUM(TPD.PlannedReimCost), 0.0000) AS PlannedReimConCost,
                  ISNULL(SUM(TPD.PlannedReimBill), 0.0000) AS PlannedReimConBill,
                  ISNULL(SUM(TPD.ETCCost), 0.0000) AS ETCCost,
                  ISNULL(SUM(TPD.ETCBill), 0.0000) AS ETCBill
                  FROM @tabPlanned AS TPD
                  WHERE TPD.WBS1 = WTZ.WBS1 AND
                    (ISNULL(TPD.WBS2, '|') LIKE CASE WHEN WTZ.WBS2 = ' ' THEN '%' ELSE WTZ.WBS2 END) AND
                    (ISNULL(TPD.WBS3, '|') LIKE CASE WHEN WTZ.WBS3 = ' ' THEN '%' ELSE WTZ.WBS3 END) 
                  GROUP BY TPD.WBS1, TPD.WBS2, TPD.WBS3
              ) AS PZ
            GROUP BY WTZ.WBS1, WTZ.WBS2, WTZ.WBS3
        ) AS PX ON WT.WBS1 = PX.WBS1 AND WT.WBS2 = PX.WBS2 AND WT.WBS3 = PX.WBS3

        LEFT JOIN (
          SELECT
            WTZ.WBS1 AS WBS1,
            WTZ.WBS2 AS WBS2,
            WTZ.WBS3 AS WBS3,
            ISNULL(SUM(JZ.JTDConCost), 0.0000) AS JTDConCost,
            ISNULL(SUM(JZ.JTDConBill), 0.0000) AS JTDConBill,
            ISNULL(SUM(JZ.JTDReimConCost), 0.0000) AS JTDReimConCost,
            ISNULL(SUM(JZ.JTDReimConBill), 0.0000) AS JTDReimConBill
            FROM @tabWBSTree AS WTZ
              OUTER APPLY (
                SELECT
                  ISNULL(SUM(TPD.JTDCost), 0.0000) AS JTDConCost,
                  ISNULL(SUM(TPD.JTDBill), 0.0000) AS JTDConBill,
                  ISNULL(SUM(TPD.JTDReimCost), 0.0000) AS JTDReimConCost,
                  ISNULL(SUM(TPD.JTDReimBill), 0.0000) AS JTDReimConBill
                  FROM @tabJTD AS TPD
                  WHERE TPD.WBS1 = WTZ.WBS1 AND
                    (ISNULL(TPD.WBS2, '|') LIKE CASE WHEN WTZ.WBS2 = ' ' THEN '%' ELSE WTZ.WBS2 END) AND
                    (ISNULL(TPD.WBS3, '|') LIKE CASE WHEN WTZ.WBS3 = ' ' THEN '%' ELSE WTZ.WBS3 END) 
                  GROUP BY TPD.WBS1, TPD.WBS2, TPD.WBS3
              ) AS JZ
            GROUP BY WTZ.WBS1, WTZ.WBS2, WTZ.WBS3
        ) AS JX ON WT.WBS1 = JX.WBS1 AND WT.WBS2 = JX.WBS2 AND WT.WBS3 = JX.WBS3

        LEFT JOIN (
          SELECT
            WTZ.WBS1 AS WBS1,
            WTZ.WBS2 AS WBS2,
            WTZ.WBS3 AS WBS3,
            ISNULL(SUM(CZ.ETCCost), 0.0000) AS ETCCost,
            ISNULL(SUM(CZ.ETCBill), 0.0000) AS ETCBill
            FROM @tabWBSTree AS WTZ
              OUTER APPLY (
                SELECT
                  ISNULL(SUM(TPD.ETCCost), 0.0000) AS ETCCost,
                  ISNULL(SUM(TPD.ETCBill), 0.0000) AS ETCBill
                  FROM @tabETC AS TPD
                  WHERE TPD.WBS1 = WTZ.WBS1 AND
                    (ISNULL(TPD.WBS2, '|') LIKE CASE WHEN WTZ.WBS2 = ' ' THEN '%' ELSE WTZ.WBS2 END) AND
                    (ISNULL(TPD.WBS3, '|') LIKE CASE WHEN WTZ.WBS3 = ' ' THEN '%' ELSE WTZ.WBS3 END) 
                  GROUP BY TPD.WBS1, TPD.WBS2, TPD.WBS3
              ) AS CZ
            GROUP BY WTZ.WBS1, WTZ.WBS2, WTZ.WBS3
        ) AS CX ON WT.WBS1 = CX.WBS1 AND WT.WBS2 = CX.WBS2 AND WT.WBS3 = CX.WBS3

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
RETURN
END 

GO
