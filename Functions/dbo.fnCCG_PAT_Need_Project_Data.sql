SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_PAT_Need_Project_Data] (@PayableSeq int, @WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS @CustColData TABLE 
(
      ColValue varchar(20), --change this to appropriate type for column data
      ColDetail varchar(max)
)
AS BEGIN


--select * from CCG_PAT_projectamount where seq = 1170
--select * from CCG_PAT_Payable
   
    declare @Need_Project_Data varchar(20)
	--declare @PayableSeq varchar(20)
	--set @payableSeq = 1170
	--declare @PayableType varchar(10)
	declare @detail varchar(max)
	if not exists (select 'y' from CCG_PAT_ProjectAmount where PayableSeq = @PayableSeq and ISNULL(WBS1,'') <> '')
		set @Need_Project_Data = 'Y' --missing


    if /*@PayableType = 'I' and */exists(
		select 'y' from CCG_PAT_ProjectAmount pa
        left join PR on pa.WBS1 = PR.WBS1 and pa.WBS2 = PR.WBS2 and pa.WBS3 = PR.WBS3 LEFT JOIN CA on CA.Account = pa.GLAccount
        WHERE pa.PayableSeq = @PayableSeq and ISNULL(CA.Type,5) between 5 and 9 and  ISNULL(PR.SubLevel,'Y') = 'Y'--missing, invalid, or not lowest			
/*	if /*@PayableType = 'I' and*/ exists(
		select 'y' from CCG_PAT_ProjectAmount pa
		left join PR on pa.WBS1 = PR.WBS1 and pa.WBS2 = PR.WBS2 and pa.WBS3 = PR.WBS3
		WHERE pa.PayableSeq = @PayableSeq and  ISNULL(PR.SubLevel,'Y') = 'Y'-- invalid, or not lowest*/
	)
		set @Need_Project_Data = 'Y' --'Projects must be at the lowest level.'
     else 
		set  @Need_Project_Data = 'N'			

	if @Need_Project_Data = 'Y'
		set @detail = 'BackgroundColor=#ffc0c0'
	else
		set @detail = NULL

	INSERT INTO @CustColData select @Need_Project_Data,@detail

	return
END
GO
