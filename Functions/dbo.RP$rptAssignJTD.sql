SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$rptAssignJTD]
  (@strPlanID varchar(32),
   @dtETCDate datetime,
   @strTaskID varchar(32)=NULL,
   @strUnposted varchar(1) = 'N', 
   @sintGRMethod smallint = 0,
   @strMatchWBS1Wildcard varchar(1) = 'N',
   @ReportAtBillingInBillingCurr varchar(1) = 'N')
  RETURNS @tabJTDLab TABLE
   (PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    TransDate datetime,
    JTDCostCurrencyCode Nvarchar(3) COLLATE database_default,
    JTDBillCurrencyCode Nvarchar(3) COLLATE database_default,    
    PeriodHrs decimal(19,4),
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4),
    PostedFlg smallint)

/*
This function does the same thing as UD function RP$tabJTDALab.  Only difference
is that this function has an optional taskID parameter. Also, @dtETCDate is the ETC date user enters.

*/

BEGIN

  INSERT @tabJTDLab
    SELECT A.PlanID,
      A.TaskID,  
      A.AssignmentID,  
      TransDate,  
	  Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
      Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
      SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,  
      SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost,  
      SUM(BillExt) AS PeriodBill,  
      1 AS PostedFlg  
      FROM LD
        INNER JOIN PR on PR.WBS1=LD.WBS1 and PR.WBS2='' and PR.WBS3=''
        INNER JOIN RPAssignment AS A 
          ON (LD.WBS1 = A.WBS1
              AND LD.Employee = A.ResourceID)  
        INNER JOIN (SELECT MIN(AssignmentID) AS AssignmentID FROM RPAssignment  
          WHERE PlanID = @strPlanID AND TaskID LIKE (ISNULL(@strTaskID,'%')) AND ResourceID IS NOT NULL  
          GROUP BY TaskID, ResourceID) AS A1 ON A.AssignmentID = A1.AssignmentID  
      WHERE ProjectCost = 'Y' AND A.PlanID = @strPlanID AND A.TaskID LIKE (ISNULL(@strTaskID,'%')) AND TransDate <= @dtETCDate  
        AND LD.WBS2 LIKE (ISNULL(A.WBS2, '%'))  
        AND LD.WBS3 LIKE (ISNULL(A.WBS3, '%'))  
        AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(A.LaborCode, '%'))  
      GROUP BY A.AssignmentID, A.PlanID, A.TaskID, TransDate, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end

  --> Generic Resources.

  IF (@sintGRMethod = 0)
    BEGIN

      -- Match Actual to first occurrence of a Category for Generic Resources.
      -- Exclude Actual values that have already matched to Named Resources.

      INSERT @tabJTDLab
        SELECT A.PlanID,
          A.TaskID,  
          A.AssignmentID,  
          TransDate,  
		  Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		  Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
          SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,  
          SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost,  
          SUM(BillExt) AS PeriodBill,  
          1 AS PostedFlg  
          FROM LD
            INNER JOIN PR on PR.WBS1=LD.WBS1 and PR.WBS2='' and PR.WBS3=''
            INNER JOIN RPAssignment AS A 
              ON (LD.WBS1 = A.WBS1
                  AND LD.Category = A.Category)  
            INNER JOIN (SELECT MIN(AssignmentID) AS AssignmentID FROM RPAssignment  
              WHERE PlanID = @strPlanID AND TaskID LIKE (ISNULL(@strTaskID,'%')) AND ResourceID IS NULL AND GRLBCD IS NULL  
              GROUP BY TaskID, Category) AS A1 ON A.AssignmentID = A1.AssignmentID  
          WHERE ProjectCost = 'Y' AND A.PlanID = @strPlanID AND A.TaskID LIKE (ISNULL(@strTaskID,'%')) AND TransDate <= @dtETCDate  
            AND A.ResourceID IS NULL AND A.GRLBCD IS NULL  
            AND LD.WBS2 LIKE (ISNULL(A.WBS2, '%'))  
            AND LD.WBS3 LIKE (ISNULL(A.WBS3, '%'))  
            AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(A.LaborCode, '%'))  
            AND (LD.Employee NOT IN (SELECT A2.ResourceID FROM RPAssignment AS A2  
              WHERE A2.TaskID = A.TaskID AND A2.ResourceID IS NOT NULL))  
          GROUP BY A.AssignmentID, A.PlanID, A.TaskID, TransDate, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end

    END -- If-Then
  ELSE
    BEGIN

      -- Match Actual to first occurrence of a Labor Code for Generic Resources.
      -- Exclude Actual values that have already matched to Named Resources.

      INSERT @tabJTDLab
        SELECT A.PlanID,
          A.TaskID,  
          A.AssignmentID,  
          TransDate,  
		  Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		  Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
          SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,  
          SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost,  
          SUM(BillExt) AS PeriodBill,  
          1 AS PostedFlg  
          FROM LD
            INNER JOIN PR on PR.WBS1=LD.WBS1 and PR.WBS2='' and PR.WBS3=''
            INNER JOIN RPAssignment AS A 
              ON (LD.WBS1 = A.WBS1
                  AND ISNULL(LD.LaborCode, '%') LIKE A.GRLBCD)  
            INNER JOIN (SELECT MIN(AssignmentID) AS AssignmentID FROM RPAssignment  
              WHERE PlanID = @strPlanID AND TaskID LIKE (ISNULL(@strTaskID,'%')) AND ResourceID IS NULL AND Category = 0  
              GROUP BY TaskID, GRLBCD) AS A1 ON A.AssignmentID = A1.AssignmentID  
          WHERE ProjectCost = 'Y' AND A.PlanID = @strPlanID AND A.TaskID LIKE (ISNULL(@strTaskID,'%')) AND TransDate <= @dtETCDate  
            AND A.ResourceID IS NULL AND A.Category = 0  
            AND LD.WBS2 LIKE (ISNULL(A.WBS2, '%'))  
            AND LD.WBS3 LIKE (ISNULL(A.WBS3, '%'))  
            AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(A.LaborCode, '%'))  
            AND (LD.Employee NOT IN (SELECT A2.ResourceID FROM RPAssignment AS A2  
              WHERE A2.TaskID = A.TaskID AND A2.ResourceID IS NOT NULL))  
          GROUP BY A.AssignmentID, A.PlanID, A.TaskID, TransDate, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end 

    END -- Else
    
  --> Unposted
  
  IF (@strUnposted = 'Y')
    BEGIN

      -- Get Unposted transactions from TK

      -- ...For matching Named Resources.

      INSERT @tabJTDLab
        SELECT A.PlanID,
          A.TaskID,  
          A.AssignmentID,  
          TransDate,  
		  Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		  Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
          SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,  
          SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost,  
          SUM(BillExt) AS PeriodBill,  
          -1 AS PostedFlg   
          FROM tkDetail AS TD 
             INNER JOIN PR on PR.WBS1=TD.WBS1 and PR.WBS2='' and PR.WBS3=''
             INNER JOIN RPAssignment AS A 
              ON (TD.WBS1 = A.WBS1
                  AND TD.Employee = A.ResourceID)
            INNER JOIN EM ON TD.Employee = EM.Employee  
            INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee  
              AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)  
            INNER JOIN (SELECT MIN(AssignmentID) AS AssignmentID FROM RPAssignment  
              WHERE PlanID = @strPlanID AND TaskID LIKE (ISNULL(@strTaskID,'%')) AND NOT ResourceID IS NULL  
              GROUP BY TaskID, ResourceID) AS A1 ON A.AssignmentID = A1.AssignmentID  
          WHERE A.PlanID = @strPlanID AND A.TaskID LIKE (ISNULL(@strTaskID,'%')) AND TransDate <= @dtETCDate  
            AND TD.WBS2 LIKE (ISNULL(A.WBS2, '%'))  
            AND TD.WBS3 LIKE (ISNULL(A.WBS3, '%'))  
            AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(A.LaborCode, '%'))  
          GROUP BY A.AssignmentID, A.PlanID, A.TaskID, TransDate, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end 

      -- ...For matching Generic Resources.

      IF (@sintGRMethod = 0)
        BEGIN -- Category As Generic Resources

          INSERT @tabJTDLab
            SELECT A.PlanID,
              A.TaskID,  
              A.AssignmentID,  
              TransDate,  
		      Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		      Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
              SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,  
              SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost,  
              SUM(BillExt) AS PeriodBill,  
              -1 AS PostedFlg  
              FROM tkDetail AS TD 
                INNER JOIN PR on PR.WBS1=TD.WBS1 and PR.WBS2='' and PR.WBS3=''
                INNER JOIN RPAssignment AS A 
                  ON (TD.WBS1 = A.WBS1
                      AND TD.BillCategory = A.Category)
                INNER JOIN EM ON TD.Employee = EM.Employee  
                INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee  
                  AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)  
                INNER JOIN (SELECT MIN(AssignmentID) AS AssignmentID FROM RPAssignment  
                  WHERE PlanID = @strPlanID AND TaskID LIKE (ISNULL(@strTaskID,'%')) AND ResourceID IS NULL AND GRLBCD IS NULL  
                  GROUP BY TaskID, Category) AS A1 ON A.AssignmentID = A1.AssignmentID  
              WHERE A.PlanID = @strPlanID AND A.TaskID LIKE (ISNULL(@strTaskID,'%')) AND TransDate <= @dtETCDate  
                AND A.ResourceID IS NULL AND A.GRLBCD IS NULL  
                AND TD.WBS2 LIKE (ISNULL(A.WBS2, '%'))  
                AND TD.WBS3 LIKE (ISNULL(A.WBS3, '%'))  
                AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(A.LaborCode, '%'))  
                AND (TD.Employee NOT IN (SELECT A2.ResourceID FROM RPAssignment AS A2  
                  WHERE A2.TaskID = A.TaskID AND A2.ResourceID IS NOT NULL))  
              GROUP BY A.AssignmentID, A.PlanID, A.TaskID, TransDate, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end 
              
        END -- If-Then
      ELSE
        BEGIN -- Labor Code As Generic Resources

          INSERT @tabJTDLab
            SELECT A.PlanID,
              A.TaskID,  
              A.AssignmentID,  
              TransDate,  
		      Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		      Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
              SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,  
              SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost,  
              SUM(BillExt) AS PeriodBill,  
              -1 AS PostedFlg  
              FROM tkDetail AS TD 
                INNER JOIN PR on PR.WBS1=TD.WBS1 and PR.WBS2='' and PR.WBS3=''
                INNER JOIN RPAssignment AS A 
                  ON (TD.WBS1 = A.WBS1
                      AND ISNULL(TD.LaborCode, '%') LIKE A.GRLBCD)  
                INNER JOIN EM ON TD.Employee = EM.Employee  
                INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee  
                  AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)  
                INNER JOIN (SELECT MIN(AssignmentID) AS AssignmentID FROM RPAssignment  
                  WHERE PlanID = @strPlanID AND TaskID LIKE (ISNULL(@strTaskID,'%')) AND ResourceID IS NULL AND Category = 0  
                  GROUP BY TaskID, Category) AS A1 ON A.AssignmentID = A1.AssignmentID  
              WHERE A.PlanID = @strPlanID AND TaskID LIKE (ISNULL(@strTaskID,'%')) AND TransDate <= @dtETCDate  
                AND A.ResourceID IS NULL AND A.Category = 0   
                AND TD.WBS2 LIKE (ISNULL(A.WBS2, '%'))  
                AND TD.WBS3 LIKE (ISNULL(A.WBS3, '%'))  
                AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(A.LaborCode, '%'))  
                AND (TD.Employee NOT IN (SELECT A2.ResourceID FROM RPAssignment AS A2  
                  WHERE A2.TaskID = A.TaskID AND A2.ResourceID IS NOT NULL))  
              GROUP BY A.AssignmentID, A.PlanID, A.TaskID, TransDate, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end 

        END -- If-Then

      -- Get Unposted transactions from TS

      -- ...For matching Named Resources.

      INSERT @tabJTDLab
        SELECT A.PlanID,
          A.TaskID,  
          A.AssignmentID,  
          TransDate,  
		  Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		  Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
          SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,  
          SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost,  
          SUM(BillExt) AS PeriodBill,  
          -1 AS PostedFlg  
          FROM tsDetail AS TD  
            INNER JOIN PR on PR.WBS1=TD.WBS1 and PR.WBS2='' and PR.WBS3=''
            INNER JOIN RPAssignment AS A 
              ON (TD.WBS1 = A.WBS1
                  AND TD.Employee = A.ResourceID) 
            INNER JOIN EM ON TD.Employee = EM.Employee  
            INNER JOIN tsControl AS TC ON (TC.Posted = 'N' AND TD.Batch = TC.Batch)  
            INNER JOIN (SELECT MIN(AssignmentID) AS AssignmentID FROM RPAssignment  
              WHERE PlanID = @strPlanID AND TaskID LIKE (ISNULL(@strTaskID,'%')) AND NOT ResourceID IS NULL  
              GROUP BY TaskID, ResourceID) AS A1 ON A.AssignmentID = A1.AssignmentID   
          WHERE A.PlanID = @strPlanID AND A.TaskID LIKE (ISNULL(@strTaskID,'%')) AND TransDate <= @dtETCDate  
            AND TD.WBS2 LIKE (ISNULL(A.WBS2, '%'))  
            AND TD.WBS3 LIKE (ISNULL(A.WBS3, '%'))  
            AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(A.LaborCode, '%'))  
          GROUP BY A.AssignmentID, A.PlanID, A.TaskID, TD.TransDate, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end 

      -- ...For matching Generic Resources.
      
      IF (@sintGRMethod = 0)
        BEGIN -- Category As Generic Resources

          INSERT @tabJTDLab
            SELECT A.PlanID,
              A.TaskID,  
              A.AssignmentID,  
              TD.TransDate,  
 		      Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		      Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
              SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,  
              SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost,  
              SUM(BillExt) AS PeriodBill,  
              -1 AS PostedFlg  
              FROM tsDetail AS TD 
                INNER JOIN PR on PR.WBS1=TD.WBS1 and PR.WBS2='' and PR.WBS3=''
                INNER JOIN RPAssignment AS A 
                  ON (TD.WBS1 = A.WBS1
                      AND TD.BillCategory = A.Category) 
                INNER JOIN EM ON TD.Employee = EM.Employee  
                INNER JOIN tsControl AS TC ON (TC.Posted = 'N' AND TD.Batch = TC.Batch)  
                INNER JOIN (SELECT MIN(AssignmentID) AS AssignmentID FROM RPAssignment  
                  WHERE PlanID = @strPlanID AND TaskID LIKE (ISNULL(@strTaskID,'%')) AND ResourceID IS NULL  
                  GROUP BY TaskID, Category) AS A1 ON A.AssignmentID = A1.AssignmentID  
              WHERE A.PlanID = @strPlanID AND A.TaskID LIKE (ISNULL(@strTaskID,'%')) AND TransDate <= @dtETCDate  
                AND A.ResourceID IS NULL AND A.GRLBCD IS NULL  
                AND TD.WBS2 LIKE (ISNULL(A.WBS2, '%'))  
                AND TD.WBS3 LIKE (ISNULL(A.WBS3, '%'))  
                AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(A.LaborCode, '%'))  
                AND (TD.Employee NOT IN (SELECT A2.ResourceID FROM RPAssignment AS A2  
                  WHERE A2.TaskID = A.TaskID AND A2.ResourceID IS NOT NULL))  
              GROUP BY A.AssignmentID, A.PlanID, A.TaskID, TD.TransDate, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end 
              
        END -- If-Then
      ELSE
        BEGIN -- Labor Code As Generic Resources

          INSERT @tabJTDLab
            SELECT A.PlanID,
              A.TaskID,  
              A.AssignmentID,  
              TransDate,  
		      Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
		      Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,
              SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,  
              SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost,  
              SUM(BillExt) AS PeriodBill,  
              -1 AS PostedFlg  
              FROM tsDetail AS TD 
                INNER JOIN PR on PR.WBS1=TD.WBS1 and PR.WBS2='' and PR.WBS3=''
                INNER JOIN RPAssignment AS A 
                  ON (TD.WBS1 = A.WBS1
                      AND ISNULL(TD.LaborCode, '%') LIKE A.GRLBCD)  
                INNER JOIN EM ON TD.Employee = EM.Employee  
                INNER JOIN tsControl AS TC ON (TC.Posted = 'N' AND TD.Batch = TC.Batch)  
                INNER JOIN (SELECT MIN(AssignmentID) AS AssignmentID FROM RPAssignment  
                  WHERE PlanID = @strPlanID AND TaskID LIKE (ISNULL(@strTaskID,'%')) AND ResourceID IS NULL  
                  GROUP BY TaskID, Category) AS A1 ON A.AssignmentID = A1.AssignmentID
              WHERE A.PlanID = @strPlanID AND A.TaskID LIKE (ISNULL(@strTaskID,'%')) AND TransDate <= @dtETCDate  
                AND A.ResourceID IS NULL AND A.Category = 0   
                AND TD.WBS2 LIKE (ISNULL(A.WBS2, '%'))  
                AND TD.WBS3 LIKE (ISNULL(A.WBS3, '%'))  
                AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(A.LaborCode, '%'))  
                AND (TD.Employee NOT IN (SELECT A2.ResourceID FROM RPAssignment AS A2  
                  WHERE A2.TaskID = A.TaskID AND A2.ResourceID IS NOT NULL))  
              GROUP BY A.AssignmentID, A.PlanID, A.TaskID, TD.TransDate, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end 

        END -- If-Then

    END -- If-Then
       
  RETURN

END 
GO
