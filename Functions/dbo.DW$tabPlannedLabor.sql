SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DW$tabPlannedLabor]
  (@strCompany nvarchar(14) = ' ',
   @strScale varchar(1) = 'm')
  RETURNS @tabPlannedLabor TABLE
    (PlanID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     WBS1 nvarchar(30) COLLATE database_default,
     WBS2 nvarchar(30) COLLATE database_default, 
     WBS3 nvarchar(30) COLLATE database_default,
     LaborCode nvarchar(14) COLLATE database_default,
     Employee nvarchar(20) COLLATE database_default,
     LaborCategory smallint,
     TransactionDate int,
     Hours decimal(19,4),
     CostAmount decimal(19,4),
     BillAmount decimal(19,4),
     RevenueAmount decimal(19,4),
     CostCompensationFeeAmount decimal(19,4),
     BillCompensationFeeAmount decimal(19,4),
     CostConsultantFeeAmount decimal(19,4),
     BillConsultantFeeAmount decimal(19,4),
     CostReimbAllowanceAmount decimal(19,4),
     BillReimbAllowanceAmount decimal(19,4),
     EVPct decimal(19,4)
    )
BEGIN -- Function DW$tabPlannedLabor
 
  DECLARE @dtStartDate datetime
  DECLARE @dtEndDate datetime
  DECLARE @dtIntervalEnd datetime
  
  DECLARE @intWkEndDay AS int
  
  DECLARE @intHrDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intLabRevDecimals int
    
  DECLARE @intPlannedLabCount int
  
  DECLARE @intCompCount int
  DECLARE @intConsCount int
  DECLARE @intReimCount int
  DECLARE @intEVCount int
   
  DECLARE @strFeesByPeriod varchar(1)
  
  -- Declare Temp tables.
  
   DECLARE @tabPlan
    TABLE (PlanID varchar(32) COLLATE database_default,		   
		   Company NVARCHAR(14),
		   CostRtMethod smallint,
		   CostRtTableNo int,
		   GenResTableNo int,		   
		   BillingRtMethod smallint,
		   BillingRtTableNo int,
		   LabBillMultiplier decimal(19,4),
           PRIMARY KEY(PlanID)) 
  
  DECLARE @tabCalendarInterval
    TABLE(StartDate datetime,
          EndDate datetime
          PRIMARY KEY(StartDate, EndDate))
          
  DECLARE @tabPLabTPD TABLE(
    RowID int identity(1, 1),
    TimePhaseID varchar(32) COLLATE database_default,
    CIStartDate datetime, 
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    StartDate datetime, 
    EndDate datetime, 
    PeriodHrs decimal(19,4),
    PeriodCost decimal(19,4), 
    PeriodBill decimal(19,4), 
    PeriodRev decimal(19,4)
    UNIQUE(RowID, TimePhaseID, PlanID, TaskID, AssignmentID, StartDate, EndDate, PeriodHrs)
  ) 
    
  DECLARE @tabEVTPD
	TABLE (RowID int identity(1, 1),
           CIStartDate datetime, 
           PlanID varchar(32) COLLATE database_default,
           TaskID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodPct decimal(19,4)
           PRIMARY KEY(RowID, PlanID, EndDate))
           
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Filter out only Plans with same Company
  
  INSERT @tabPlan(PlanID, Company, CostRtMethod, CostRtTableNo, GenResTableNo, BillingRtMethod, BillingRtTableNo, LabBillMultiplier) 
	SELECT PlanID, Company, CostRtMethod, CostRtTableNo, GenResTableNo, BillingRtMethod, BillingRtTableNo, LabBillMultiplier
	FROM RPPlan WHERE Company = @strCompany 

  -- Get flags to determine which features are being used.
  
  SELECT
     @intWkEndDay = (CASE WHEN StartingDayOfWeek = 1 THEN 7 ELSE StartingDayOfWeek - 1 END), 
     @strFeesByPeriod = FeesByPeriodFlg
     FROM CFGResourcePlanning
     WHERE CFGResourcePlanning.Company = @strCompany
      
  -- Get decimal settings.
  -- At this point, there is no way to know what was the basis used in calculation of Baseline Revenue numbers.
  -- Therefore, we decided to use the current basis to determine number of decimal digits to be used
  -- in the realignment of Baseline Revenue numbers.
  
  SELECT @intHrDecimals = 2,
         @intAmtCostDecimals = 2,
         @intAmtBillDecimals = 2,
         @intLabRevDecimals = 2
    
  -- Check to see if there is any time-phased data
  
    SELECT @intPlannedLabCount = COUNT(*) FROM RPPlannedLabor AS TPD INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID 
   
    SELECT @intEVCount = COUNT(*) FROM RPEVT AS TPD INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find the MIN and MAX dates of all TPD.
  
  SELECT @dtStartDate = MIN(MINDate), @dtEndDate = MAX(MAXDate)
    FROM
      (SELECT MIN(TPD.StartDate) AS MINDate, MAX(TPD.EndDate) AS MAXDate FROM RPPlannedLabor AS TPD INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID 
       UNION
       SELECT MIN(TPD.StartDate) AS MINDate, MAX(TPD.EndDate) AS MAXDate FROM RPEVT AS TPD INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID
      ) AS X
  
  -- Save Calendar Intervals into a temp table and add an overflow Calendar Interval.
      
  WHILE (@dtStartDate <= @dtEndDate)
    BEGIN
        
      -- Compute End Date of interval.

      IF (@strScale = 'd') 
        SET @dtIntervalEnd = @dtStartDate
      ELSE
        SET @dtIntervalEnd = dbo.DLTK$IntervalEnd(@dtStartDate, @strScale, @intWkEndDay)
        
      IF (@dtIntervalEnd > @dtEndDate) 
        SET @dtIntervalEnd = @dtEndDate
            
      -- Insert new Calendar Interval record.
          
      INSERT @tabCalendarInterval(StartDate, EndDate)
        VALUES (@dtStartDate, @dtIntervalEnd)
          
      -- Set Start Date for next interval.
          
      SET @dtStartDate = DATEADD(d, 1, @dtIntervalEnd)
        
    END -- End While
       
       
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
              
  IF (@intPlannedLabCount > 0)
    BEGIN
    
      -- Save Planned Labor time-phased data rows.

      INSERT @tabPLabTPD
        (TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         AssignmentID,
         StartDate, 
         EndDate, 
         PeriodHrs,
         PeriodCost, 
         PeriodBill,
         PeriodRev)
         SELECT
           TimePhaseID AS TimePhaseID,
           CIStartDate AS CIStartDate,
           PlanID AS PlanID, 
           TaskID AS TaskID,
           AssignmentID AS AssignmentID,
           StartDate AS StartDate, 
           EndDate AS EndDate, 
           ROUND(ISNULL(PeriodHrs, 0), @intHrDecimals) AS PeriodHrs,
           ROUND(ISNULL(PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
           ROUND(ISNULL(PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill, 
           ROUND(ISNULL(PeriodRev, 0), @intLabRevDecimals) AS PeriodRev
         FROM (SELECT -- For Assignment Rows.
                 CI.StartDate AS CIStartDate, 
                 TPD.TimePhaseID AS TimePhaseID,
                 A.PlanID AS PlanID, 
                 A.TaskID AS TaskID,
                 A.AssignmentID AS AssignmentID, 
                 CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                 CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
               
			     (dbo.dltk$prorateratio(
				 CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END,
				 CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END,TPD.StartDate,TPD.EndDate,P.Company) 
				 * TPD.PeriodHrs) AS PeriodHrs,

				 TPD.PeriodCost * dbo.dltk$prorateratio(CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END,
				 CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END,TPD.StartDate,TPD.EndDate,P.Company) AS PeriodCost,

				 TPD.PeriodBill * dbo.dltk$prorateratio(CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END,
				 CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END,TPD.StartDate,TPD.EndDate,P.Company)  AS PeriodBill,

                 PeriodRev AS PeriodRev,
                 CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                      THEN dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                      THEN TPD.StartDate 
                                                      ELSE CI.StartDate END, 
                                                 CASE WHEN TPD.EndDate < CI.EndDate 
                                                      THEN TPD.EndDate 
                                                      ELSE CI.EndDate END, 
                                                 TPD.StartDate, TPD.EndDate,
                                                 @strCompany)
                      ELSE 1 END AS ProrateRatio
                 FROM RPPlannedLabor AS TPD
				   INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID
                   INNER JOIN RPAssignment AS A ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND TPD.AssignmentID = A.AssignmentID
				   INNER JOIN @tabCalendarInterval AS CI ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
				    LEFT JOIN EM ON EM.Employee = A.ResourceID
				   LEFT JOIN GR ON GR.Code = A.GenericResourceID
              ) AS X
         WHERE (PeriodHrs IS NOT NULL AND ROUND((PeriodHrs * ProrateRatio), @intHrDecimals) != 0)
     
      -- Adjust Planned Labor time-phased data to compensate for rounding errors.
     
		UPDATE @tabPLabTPD 
		SET 
			PeriodHrs = (TPD.PeriodHrs + D.DeltaHrs),
			PeriodCost = (TPD.PeriodCost + D.DeltaCost),
			PeriodBill = (TPD.PeriodBill + D.DeltaBill),
			PeriodRev = (TPD.PeriodRev + D.DeltaRev)
		FROM @tabPLabTPD AS TPD INNER JOIN 
			(SELECT 
			YTPD.TimePhaseID AS TimePhaseID,
			ROUND((YTPD.PeriodHrs - SUM(ISNULL(XTPD.PeriodHrs, 0))), @intHrDecimals) AS DeltaHrs,
			ROUND((YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))), @intAmtCostDecimals) AS DeltaCost,
			ROUND((YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))), @intAmtBillDecimals) AS DeltaBill,
			ROUND((YTPD.PeriodRev - SUM(ISNULL(XTPD.PeriodRev, 0))), @intLabRevDecimals) AS DeltaRev
			FROM @tabPLabTPD AS XTPD
				inner join (
				SELECT YTPD.TimePhaseID, YTPD.PlanID, YTPD.TaskID, YTPD.PeriodRev AS PeriodRev,
				YTPD.PeriodHrs AS PeriodHrs,
				YTPD.PeriodCost AS PeriodCost,
				YTPD.PeriodBill AS PeriodBill
				FROM RPPlannedLabor AS YTPD
				INNER JOIN @tabPlan AS P ON YTPD.PlanID = P.PlanID
				INNER JOIN RPAssignment AS A ON YTPD.PlanID = A.PlanID AND YTPD.TaskID = A.TaskID AND YTPD.AssignmentID = A.AssignmentID
				LEFT JOIN EM ON EM.Employee = A.ResourceID
				LEFT JOIN GR ON GR.Code = A.GenericResourceID
				) AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID AND XTPD.PlanID = YTPD.PlanID AND XTPD.TaskID = YTPD.TaskID
				GROUP BY YTPD.TimePhaseID, YTPD.PeriodHrs, YTPD.PeriodCost, YTPD.PeriodBill, YTPD.PeriodRev) AS D
			ON TPD.TimePhaseID = D.TimePhaseID
			WHERE (D.DeltaHrs != 0 OR D.DeltaCost != 0 OR D.DeltaBill != 0 OR D.DeltaRev != 0)
				AND RowID IN
				(SELECT RowID FROM @tabPLabTPD AS ATPD INNER JOIN
				(SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabPLabTPD GROUP BY TimePhaseID) AS BTPD
				ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
               
    END -- IF (@intPlannedLabCount > 0)

            
  IF (@intEVCount > 0)
    BEGIN
    
      -- Save EV Percent time-phased data rows.
      -- There should be only one Task row that has EVPct children.
      
      -- For EV Pct, pick the highest Pct for the common periods.
      --
      --   Old Scale: |   25  |  50  |
      --   New Scale: |      50      |
      --
      --   Old Scale:    |      25      |      75      |      90      
      --   New Scale: |   25  |  25  |  75  |  75  |  90  |

      INSERT @tabEVTPD
        (CIStartDate,
         PlanID, 
         TaskID,
         StartDate, 
         EndDate, 
         PeriodPct)
        SELECT
          CIStartDate AS CIStartDate,
          PlanID, 
          TaskID,
          MIN(StartDate) AS StartDate, 
          MAX(EndDate) AS EndDate, 
          ROUND(MAX(ISNULL(PeriodPct, 0)), 2) AS PeriodPct
          FROM
            (SELECT 
               CI.StartDate AS CIStartDate, 
               TPD.PlanID AS PlanID, 
               TPD.TaskID AS TaskID,
               CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
               CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
               PeriodPct AS PeriodPct
               FROM RPEVT AS TPD
                 INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID
                 INNER JOIN @tabCalendarInterval AS CI ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
            ) AS X
          GROUP BY CIStartDate, PlanID, TaskID
                
    END -- IF (@intEVCount > 0)
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT INTO @tabPlannedLabor
    (PlanID,
     TaskID,
     WBS1,
     WBS2, 
     WBS3,
     LaborCode,
     Employee,
     LaborCategory,
     TransactionDate,
     Hours,
     CostAmount,
     BillAmount,
     RevenueAmount,
     CostCompensationFeeAmount,
     BillCompensationFeeAmount,
     CostConsultantFeeAmount,
     BillConsultantFeeAmount,
     CostReimbAllowanceAmount,
     BillReimbAllowanceAmount,
     EVPct
    )
    SELECT
      Z.PlanID,
      ISNULL(Z.AssignmentID, Z.TaskID),
      ISNULL(T.WBS1, ' ') AS WBS1,
      ISNULL(T.WBS2, ' ') AS WBS2,
      ISNULL(T.WBS3, ' ') AS WBS3,
      CASE WHEN T.LaborCode = '<none>' THEN '<empty>' ELSE ISNULL(T.LaborCode, '<empty>') END AS LaborCode,
      A.ResourceID As Employee,
      CASE WHEN A.ResourceID IS NULL THEN ISNULL(A.Category, 0)
           ELSE ISNULL(E.BillingCategory, 0) END AS LaborCategory,
      ISNULL(CONVERT(INT, MIN(Z.StartDate)), -1) AS TransactionDate,
      ISNULL(SUM(Z.Hours), 0) AS Hours,
      ISNULL(SUM(Z.CostAmount), 0) AS CostAmount,
      ISNULL(SUM(Z.BillAmount), 0) AS BillAmount,
      ISNULL(SUM(Z.RevenueAmount), 0) AS RevenueAmount,
      ISNULL(SUM(Z.CostCompensationFeeAmount), 0) AS CostCompensationFeeAmount,
      ISNULL(SUM(Z.BillCompensationFeeAmount), 0) AS BillCompensationFeeAmount,
      ISNULL(SUM(Z.CostConsultantFeeAmount), 0) AS CostConsultantFeeAmount,
      ISNULL(SUM(Z.BillConsultantFeeAmount), 0) AS BillConsultantFeeAmount,
      ISNULL(SUM(Z.CostReimbAllowanceAmount), 0) AS CostReimbAllowanceAmount,
      ISNULL(SUM(Z.BillReimbAllowanceAmount), 0) AS BillReimbAllowanceAmount,
      ISNULL(SUM(Z.EVPct), 0)  AS EVPct
      FROM
        (SELECT
           TPD.CIStartDate AS CIStartDate,
           TPD.PlanID AS PlanID,
           TPD.TaskID AS TaskID,
           TPD.AssignmentID AS AssignmentID,
           TPD.StartDate,
           TPD.EndDate,
           TPD.PeriodHrs AS Hours,
           TPD.PeriodCost AS CostAmount,
           TPD.PeriodBill AS BillAmount,
           TPD.PeriodRev AS RevenueAmount,
           0 AS CostCompensationFeeAmount,
           0 AS BillCompensationFeeAmount,
           0 AS CostConsultantFeeAmount,
           0 AS BillConsultantFeeAmount,
           0 AS CostReimbAllowanceAmount,
           0 AS BillReimbAllowanceAmount,
           0 AS EVPct
           FROM @tabPLabTPD AS TPD        
         UNION ALL
         SELECT
           TPD.CIStartDate AS CIStartDate,
           TPD.PlanID AS PlanID,
           TPD.TaskID AS TaskID,
           NULL AS AssignmentID,
           TPD.StartDate,
           TPD.EndDate,
           0 AS Hours,
           0 AS CostAmount,
           0 AS BillAmount,
           0 AS RevenueAmount,
           0 AS CostCompensationFeeAmount,
           0 AS BillCompensationFeeAmount,
           0 AS CostConsultantFeeAmount,
           0 AS BillConsultantFeeAmount,
           0 AS CostReimbAllowanceAmount,
           0 AS BillReimbAllowanceAmount,
           TPD.PeriodPct AS EVPct
           FROM @tabEVTPD AS TPD
        ) AS Z
        INNER JOIN RPTask AS T ON Z.PlanID = T.PlanID AND Z.TaskID = T.TaskID
        LEFT JOIN RPAssignment AS A ON Z.PlanID = A.PlanID AND Z.TaskID = A.TaskID AND Z.AssignmentID = A.AssignmentID
        LEFT JOIN EM AS E ON A.ResourceID = E.Employee
        GROUP BY Z.PlanID, Z.TaskID, Z.AssignmentID, Z.CIStartDate, T.WBS1, T.WBS2, T.WBS3, T.LaborCode, A.ResourceID, A.Category, E.BillingCategory

  RETURN

END -- DW$tabPlannedLabor

GO
