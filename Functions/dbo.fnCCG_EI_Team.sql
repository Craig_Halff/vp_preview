SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_Team] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
returns varchar(20)
AS BEGIN
/*
	Copyright (c) 2017 Elevia Sotfware. All rights reserved.

	select dbo.fnCCG_EI_Team('2003005.xx',' ',' ')
*/
     declare @t varchar(20)
     select @t = right(org,2) from PR where WBS1=@WBS1 and WBS2=' ' 
     return @t
END
GO
