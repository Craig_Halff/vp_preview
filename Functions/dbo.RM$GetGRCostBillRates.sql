SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RM$GetGRCostBillRates]
  (@PlanID varchar(32), 
   @GenericResourceID Nvarchar(20) = NULL,
   @StartDate DateTime, 
   @EndDate DateTime)
  RETURNS @tabRates TABLE
   (CostRate decimal(19,4),
    BillingRate decimal(19,4))
BEGIN

  /*--------------------------------------------------------------------------------------------------------------------
    This function encapsulates the rules for getting labor billing/cost rates for an employee
    This function will return the following columns:
      CostRate: Cost rate
      BillRate: Bill rate
  */--------------------------------------------------------------------------------------------------------------------

-- Decimal stuff
DECLARE @RtCostDecimals int
DECLARE @RtBillDecimals int

DECLARE @sintCategory smallint 
DECLARE @strGRLBCD varchar(14)  

SELECT	@RtCostDecimals = RtCostDecimals,
		@RtBillDecimals = RtBillDecimals
   FROM dbo.RP$tabDecimals(@PlanID)

SELECT @sintCategory = Category, 
       @strGRLBCD = LaborCode 
   FROM GR WHERE Code = @GenericResourceID
   
  INSERT @tabRates
    SELECT 
       dbo.RP$LabRate
          (NULL,
           @sintCategory, -- Category 
           @strGRLBCD, -- RPAssignment.GRLBCD
           NULL,
           P.CostRtMethod,
           P.CostRtTableNo,
           P.GRMethod,
           P.GenResTableNo,
           @StartDate,
           @EndDate,
           @RtCostDecimals,
           0, 
           0) AS CostRate , -- CostRate override
       dbo.RP$LabRate
          (NULL,
           @sintCategory, -- Category 
           @strGRLBCD, -- RPAssignment.GRLBCD
           NULL,
           P.BillingRtMethod,
           P.BillingRtTableNo,
           P.GRMethod,
           P.GRBillTableNo,
           @StartDate,
           @EndDate,
           @RtBillDecimals,
           0, 
           0) AS BillingRate  -- BillRate override
   FROM RPPlan AS P
      WHERE P.PlanID = @PlanID 
      
  RETURN

END -- fn_RM$GetGRCostBillRates
GO
