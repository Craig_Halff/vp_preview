SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$MultiCompanyConfig]()
  RETURNS varchar(max)

BEGIN

  DECLARE @strMultiCompanyConfig varchar(max)

  DECLARE @strCompany nvarchar(14)
  DECLARE @strMultiCompanyEnabled varchar(1)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

  DECLARE @tabCompany TABLE(
    Company nvarchar(14) COLLATE database_default
    UNIQUE(Company)
  )

  DECLARE @tabHoliday TABLE(
    RowID int IDENTITY(1,1),
    Company nvarchar(14) COLLATE database_default,
    JSONHolidayDates varchar(max) COLLATE database_default,
    bitHasHolidayDate bit
    UNIQUE(RowID, Company)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled.
  
  SELECT
    @strMultiCompanyEnabled = MultiCompanyEnabled
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length
    FROM CFGFormat

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabCompany(
    Company
  )
    SELECT DISTINCT
      CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(O.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END AS Company
      FROM Organization AS O
    UNION /* Case where Org is not being used */
    SELECT ' ' AS Company
      WHERE @strMultiCompanyEnabled = 'N'

  INSERT @tabHoliday(
    Company,
    JSONHolidayDates,
    bitHasHolidayDate
  )
    SELECT 
      Company AS Company, 
      '"Holidays": [' +
      ISNULL(
        REVERSE(STUFF(REVERSE((
          SELECT '"' + REPLACE(CONVERT(VARCHAR(23), H.HolidayDate, 121), ' ', 'T') + '",' 
            FROM CFGHoliday AS H 
            WHERE H.Company = C.Company 
            ORDER BY H.HolidayDate 
            FOR XML PATH ('')
        )), 1, 1, ''))
      , '') + 
      ']' AS JSONHolidayDates,
      CASE WHEN EXISTS(SELECT 'X' FROM CFGHoliday AS HX WHERE HX.Company = C.Company) THEN 1 ELSE 0 END AS bitHasHolidayDate
      FROM @tabCompany AS C
      ORDER BY C.Company

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strMultiCompanyConfig = NULL

  SELECT @strMultiCompanyConfig = COALESCE(@strMultiCompanyConfig + ',', '[') +
    '{' +
    REVERSE(STUFF(REVERSE((
      '"Company": "' + C.Company + '",' +
      ISNULL(('"NonWorkingDays": "' + NWD.SundayInd + NWD.MondayInd + NWD.TuesdayInd + NWD.WednesdayInd + NWD.ThursdayInd + NWD.FridayInd + NWD.SaturdayInd + '",'), '') +
      ISNULL(H.JSONHolidayDates, '')
    )), 1, 0, '')) +
    '}'
    FROM @tabCompany AS C
      LEFT JOIN @tabHoliday AS H ON C.Company = H.Company
      LEFT JOIN CFGNonWorkingDay AS NWD ON C.Company = NWD.Company
    WHERE 
      (NWD.SundayInd + NWD.MondayInd + NWD.TuesdayInd + NWD.WednesdayInd + NWD.ThursdayInd + NWD.FridayInd + NWD.SaturdayInd) IS NOT NULL OR
      H.bitHasHolidayDate = 1

  SELECT @strMultiCompanyConfig = COALESCE(@strMultiCompanyConfig + ']', '')

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN(@strMultiCompanyConfig)

END /* END FUNCTION stRP$MultiCompanyConfig */

GO
