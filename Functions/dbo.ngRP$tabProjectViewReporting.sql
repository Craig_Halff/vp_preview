SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ngRP$tabProjectViewReporting](
  @strRowID Nvarchar(255),
  @strScopeStartDate varchar(8) /* Date must be in format: 'yyyymmdd' regardless of UI Culture */,
  @strScopeEndDate varchar(8) /* Date must be in format: 'yyyymmdd' regardless of UI Culture */,
  @strMode varchar(1) /* S = Self, C = Children */
)
  RETURNS @tabProjectView TABLE (
    RowID Nvarchar(255) COLLATE database_default,
    ParentRowID Nvarchar(255) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ResourceID Nvarchar(20) COLLATE database_default,
	ResourceType Nvarchar(1) COLLATE database_default,
    GenericResourceID Nvarchar(20) COLLATE database_default,
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(30) COLLATE database_default,
    WBS3 Nvarchar(30) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    HasChildren bit,
    LeafNode bit,
    RowLevel int,
    Name Nvarchar(255) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PlannedHrs decimal(19,4),
    ETCHrs decimal(19,4),
    JTDHrs decimal(19,4),
    EACHrs decimal(19,4),
	HasPhoto bit,
	PhotoModDate datetime,
    Org Nvarchar(30) COLLATE database_default,
    OrgName Nvarchar(100) COLLATE database_default,
		OrgLevel1 Nvarchar(30) COLLATE database_default,
		OrgLevel2 Nvarchar(30) COLLATE database_default,
		OrgLevel3 Nvarchar(30) COLLATE database_default,
		OrgLevel4 Nvarchar(30) COLLATE database_default,
		OrgLevel5 Nvarchar(30) COLLATE database_default,
    ProjMgr Nvarchar(20) COLLATE database_default,
    PMFullName Nvarchar(255) COLLATE database_default,
    ClientName Nvarchar(100) COLLATE database_default,
    HardBooked varchar(1) COLLATE database_default,
    MinASGDate datetime,
    MaxASGDate datetime,
		TopName Nvarchar(255) COLLATE database_default,
		TopWBS1 Nvarchar(255) COLLATE database_default,
		ProjectWBSName Nvarchar(max) COLLATE database_default,
		ProjectWBS Nvarchar(max) COLLATE database_default,
		ProjectType Nvarchar(10) COLLATE database_default,
    SelectedPlannedHrs decimal(19,4),
    SelectedETCHrs decimal(19,4),
    AvailableHrs decimal(19,4),
    ScheduledPct decimal(19,4),
	UtilizationScheduleFlg varchar(1),
    TargetUtilization decimal(19,4),
    UtilizationPct decimal(19,4)
  )

BEGIN


  DECLARE @strCompany Nvarchar(14)
  DECLARE @strTaskID varchar(32)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTopWBS1 Nvarchar(30)
  DECLARE @strTopName Nvarchar(255)
  DECLARE @strTopAssignmentTaskID varchar(32)
  DECLARE @strResourceType varchar(1)
  DECLARE @strIDPrefix Nvarchar(255)
  DECLARE @strResourceID Nvarchar(20)
  DECLARE @strGenericResourceID Nvarchar(20)

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
 
  DECLARE @siHrDecimals int /* CFGRMSettings.HrDecimals */

  DECLARE @intTaskCount int 

  DECLARE @bitIsLeafTask bit
  DECLARE @dtScopeStartDate datetime
  DECLARE @dtScopeEndDate datetime
	
  DECLARE @decAvailableHrs decimal(19,4)
  DECLARE @decHoursPerDay decimal(19,4)
  DECLARE @decUtilizationRatio decimal(19,4)

  DECLARE @decScheduledPct decimal(19,4)
  DECLARE @decUtilizationPct decimal(19,4)
	DECLARE @intScopeWorkingDays int


  -- Declare Temp tables.

  DECLARE @tabAssignment TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ResourceID varchar(32) COLLATE database_default,
    GenericResourceID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    HardBookedFlg int,
    OutlineNumber varchar(255) COLLATE database_default
    UNIQUE(PlanID, TaskID, AssignmentID)
  )

  DECLARE @tabLabTPD TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChargeType varchar(1) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PlannedHrs decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, OutlineNumber, StartDate, EndDate)
  )

  DECLARE @tabSelectedTPD TABLE (
    PlanID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChargeType varchar(1) COLLATE database_default,
    PlannedHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber, AssignmentID)
  )

  DECLARE @tabBillableTPD TABLE (
    PlanID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    PlannedHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber, AssignmentID)
  )

  DECLARE @tabETCTPD TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PeriodHrs decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, OutlineNumber, StartDate, EndDate)
  )

  DECLARE @tabTotalETC TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ETCHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber, AssignmentID)
  )

  DECLARE @tabSelectedETC TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ETCHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber, AssignmentID)
  )

  DECLARE @tabLD TABLE (
    LDRowID  int IDENTITY(1,1),
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    PeriodHrs decimal(19,4),
    JTDLabCost decimal(19,4),	
    JTDLabBill decimal(19,4)
    PRIMARY KEY(LDRowID, PlanID, OutlineNumber)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Set Company to non-Multi-Company.
  
  SELECT @strCompany = dbo.GetActiveCompany()

  -- Get RM Settings.
  
  SELECT 
		@dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE()))),
    @siHrDecimals = HrDecimals
    FROM CFGRMSettings

  SELECT @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)
  SELECT @dtScopeStartDate = CONVERT(datetime, @strScopeStartDate)
  SELECT @dtScopeEndDate = CONVERT(datetime, @strScopeEndDate)

	-- Get total working hours for selected range to use for available calcs

  SET @decAvailableHrs = 0
  SET @decHoursPerDay = 0

	SELECT @intScopeWorkingDays = dbo.DLTK$NumWorkingDays(@dtScopeStartDate, @dtScopeEndDate, @strCompany)
 
  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. For an Assignment row, @strRowID = 'E~<RPAssignment.ResourceID>|<RPTask.TaskID>
  --   2. For an Assignment row, @strRowID = 'G~<RPAssignment.GenericResourceID>|<RPTask.TaskID>'
  --   3. For a WBS row, @strRowID = '|<RPTask.TaskID>'

  SET @strResourceType = 
    CASE 
      WHEN CHARINDEX('~', @strRowID) = 0 
      THEN ''
      ELSE SUBSTRING(@strRowID, 1, 1)
    END

  SET @strIDPrefix = LEFT(@strRowID, CHARINDEX('|', @strRowID) - 1)

  IF (@strResourceType = 'E')
    BEGIN
      SET @strResourceID = REPLACE(@strIDPrefix, 'E~', '')
      SET @strGenericResourceID = NULL
    END
  ELSE IF (@strResourceType = 'G')
    BEGIN
      SET @strGenericResourceID = REPLACE(@strIDPrefix, 'G~', '')
      SET @strResourceID = NULL
    END
  ELSE
    BEGIN
      SET @strResourceID = NULL
      SET @strGenericResourceID = NULL
    END

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

  -- Setting other data.

  SELECT 
    @strPlanID = PT.PlanID,
    @bitIsLeafTask =
      CASE
        WHEN COUNT(CT.TaskID) > 0
        THEN 0
        ELSE 1
      END
    FROM RPTask AS PT 
      LEFT JOIN RPTask AS CT
        ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
    WHERE PT.TaskID = @strTaskID
    GROUP BY PT.PlanID, PT.TaskID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabAssignment(
    PlanID,
    TaskID,
    AssignmentID,
    ResourceID,
    GenericResourceID,
    StartDate,
    EndDate,
    HardBookedFlg,
    OutlineNumber
  )
    SELECT DISTINCT
      A.PlanID,
      A.TaskID,
      A.AssignmentID,
      A.ResourceID,
      A.GenericResourceID,
      A.StartDate,
      A.EndDate,
      CASE HardBooked
        WHEN 'Y' THEN 1
        WHEN 'N' THEN -1
      END AS HardBookedFlg,
      AT.OutlineNumber AS OutlineNumber
      FROM RPAssignment AS A
        INNER JOIN RPPlan AS P ON P.PlanID = A.PlanID
        INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
        INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID AND AT.OutlineNumber LIKE PT.OutlineNumber + '%'
        LEFT JOIN RPTask AS CT ON A.PlanID = CT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber AND AT.OutlineNumber LIKE CT.OutlineNumber + '%'
      WHERE
        A.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
				A.EndDate >= @dtScopeStartDate


    SELECT @intTaskCount = COUNT(DISTINCT TaskID) FROM @tabAssignment

--++++++++

  -- Collecting Labor Time-Phased Data to be used in subsequent calculations.
  -- The TPD records come from only the Assignments included in @tabAssignment.

  INSERT @tabLabTPD(
    TimePhaseID,
    PlanID,
    TaskID,
    AssignmentID,
    OutlineNumber,
    ChargeType,
    StartDate,
    EndDate,
    PlannedHrs
  )
    SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID,
      TPD.TaskID AS TaskID,
      TPD.AssignmentID AS AssignmentID,
      T.OutlineNumber AS OutlineNumber,
      T.ChargeType AS ChargeType,
      TPD.StartDate AS StartDate,
      TPD.EndDate AS EndDate,
      TPD.PeriodHrs AS PlannedHrs
      FROM @tabAssignment AS A
        INNER JOIN RPTask AS T ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
        INNER JOIN RPPlannedLabor AS TPD ON
          A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID AND
          TPD.PeriodHrs <> 0 AND TPD.AssignmentID IS NOT NULL 


-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collecting ETC Time-Phased Data to be used in subsequent calculations.

  INSERT @tabETCTPD(
    TimePhaseID,
    PlanID,
    TaskID,
    AssignmentID,
    OutlineNumber,
    StartDate,
    EndDate,
    PeriodHrs
  )
    SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID,
      TPD.TaskID AS TaskID,
      TPD.AssignmentID AS AssignmentID,
      TPD.OutlineNumber AS OutlineNumber,
      CASE WHEN TPD.StartDate >= @dtETCDate THEN TPD.StartDate ELSE @dtETCDate END AS StartDate,
      TPD.EndDate AS EndDate,
      ISNULL(
        CASE 
          WHEN TPD.StartDate >= @dtETCDate 
          THEN TPD.PlannedHrs
          ELSE TPD.PlannedHrs * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) 
        END 
      , 0) AS PeriodHrs
      FROM @tabLabTPD AS TPD
      WHERE TPD.PlannedHrs <> 0 AND TPD.EndDate >= @dtETCDate 

--+++++

  -- Calculate Total ETC
  -- GROUP BY PlanID, OutlineNumber

  INSERT @tabTotalETC(
    PlanID,
    OutlineNumber,
    AssignmentID,
    ETCHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
      TPD.AssignmentID,
      ISNULL(SUM(TPD.PeriodHrs), 0) AS ETCHrs
      FROM @tabETCTPD AS TPD
      GROUP BY PlanID, OutlineNumber, AssignmentID

--+++++

  -- Calculate Selected TPD (i.e. Planned Hrs in forecast range) 
  -- GROUP BY PlanID, OutlineNumber, ChargeType

  INSERT @tabSelectedTPD(
    PlanID,
    OutlineNumber,
		AssignmentID,
    ChargeType,
    PlannedHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
			TPD.AssignmentID,
      TPD.ChargeType,
      ISNULL(SUM(
        CASE
          WHEN TPD.StartDate >= @dtScopeStartDate
          THEN 
            CASE
              WHEN TPD.EndDate <= @dtScopeEndDate
              THEN TPD.PlannedHrs
              ELSE TPD.PlannedHrs * dbo.DLTK$ProrateRatio(TPD.StartDate, @dtScopeEndDate, TPD.StartDate, TPD.EndDate, @strCompany)
            END
          ELSE
            CASE
              WHEN TPD.EndDate <= @dtScopeEndDate
              THEN TPD.PlannedHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)
              ELSE TPD.PlannedHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, @dtScopeEndDate, TPD.StartDate, TPD.EndDate, @strCompany)
            END
        END
      ), 0) AS PlannedHrs
      FROM @tabLabTPD AS TPD
      WHERE TPD.StartDate <= @dtScopeEndDate AND TPD.EndDate >= @dtScopeStartDate
      GROUP BY PlanID, OutlineNumber, AssignmentID, ChargeType

--+++++

  -- Calculate Billable TPD (i.e. Billable Hrs in forecast range) 
  -- GROUP BY PlanID, OutlineNumber

  INSERT @tabBillableTPD(
    PlanID,
		AssignmentID,
    OutlineNumber,
    PlannedHrs
  )
    SELECT
      TPD.PlanID,
			TPD.AssignmentID,
      TPD.OutlineNumber,
      ISNULL(SUM(TPD.PlannedHrs), 0) AS PlannedHrs
      FROM @tabSelectedTPD AS TPD
      WHERE TPD.ChargeType = 'R'
      GROUP BY PlanID, AssignmentID, OutlineNumber

--+++++

  -- Calculate Selected ETC
  -- GROUP BY PlanID, OutlineNumber

  INSERT @tabSelectedETC(
    PlanID,
    OutlineNumber,
		AssignmentID,
    ETCHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
			TPD.AssignmentID,
      ISNULL(SUM(
        CASE
          WHEN TPD.StartDate >= @dtScopeStartDate
          THEN 
            CASE
              WHEN TPD.EndDate <= @dtScopeEndDate
              THEN TPD.PeriodHrs
              ELSE TPD.PeriodHrs * dbo.DLTK$ProrateRatio(TPD.StartDate, @dtScopeEndDate, TPD.StartDate, TPD.EndDate, @strCompany)
            END
          ELSE
            CASE
              WHEN TPD.EndDate <= @dtScopeEndDate
              THEN TPD.PeriodHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)
              ELSE TPD.PeriodHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, @dtScopeEndDate, TPD.StartDate, TPD.EndDate, @strCompany)
            END
        END
      ), 0) AS ETCHrs
      FROM @tabETCTPD AS TPD
      WHERE TPD.StartDate <= @dtScopeEndDate AND TPD.EndDate >= @dtScopeStartDate
      GROUP BY PlanID, OutlineNumber, AssignmentID


-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save Labor JTD & Unposted Labor JTD for this Resource into temp table.
  -- The JTD data are posted at the leaf node in the WBS Tree. 
  -- Therefore, we need to calculate JTD for the summary levels.

  INSERT @tabLD
    (PlanID,
     OutlineNumber,
     AssignmentID,
     PeriodHrs,
     JTDLabCost,
     JTDLabBill)      
    SELECT
      T.PlanID AS PlanID,
      T.OutlineNumber AS OutlineNumber,
      A.AssignmentID,
      SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,
      SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS JTDLabCost,
      SUM(LD.BillExt) AS JTDLabBill
    FROM @tabAssignment AS A
      INNER JOIN RPTask AS T
        ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
      INNER JOIN LD
        ON LD.WBS1 = T.WBS1  
		AND LD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
		AND LD.WBS3 LIKE (ISNULL(T.WBS3 + '%', '%'))
		AND A.ResourceID = LD.Employee
    WHERE LD.TransDate <= @dtJTDDate
    GROUP BY T.PlanID,T.OutlineNumber,A.AssignmentID

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  IF @strMode = 'C'
    BEGIN
      INSERT @tabProjectView (
        RowID,
        ParentRowID,
        PlanID,
        TaskID,
        AssignmentID,	
        ResourceID,
		ResourceType,
        GenericResourceID,
        WBS1,
        WBS2,
        WBS3,
        OutlineNumber,
        HasChildren,
        LeafNode,
        RowLevel,
        Name,
        StartDate,
        EndDate,
        PlannedHrs,
        ETCHrs,
        JTDHrs,
        EACHrs,
        HasPhoto,
		PhotoModDate,
        Org,
        OrgName,
				OrgLevel1,
				OrgLevel2,
				OrgLevel3,
				OrgLevel4,
				OrgLevel5,
        ProjMgr,
        PMFullName,
        ClientName,
        HardBooked,
        MinASGDate,
        MaxASGDate,
				TopName,
				TopWBS1,
				ProjectWBSName,
				ProjectWBS,
				ProjectType,
				SelectedPlannedHrs,
				SelectedETCHrs,
				AvailableHrs,
				ScheduledPct,
				UtilizationScheduleFlg,
				UtilizationPct,
				TargetUtilization
      )
      SELECT
        CASE WHEN XT.ResourceID IS NULL THEN 'G~' + ISNULL(XT.GenericResourceID,'') ELSE 'E~' + ISNULL(XT.ResourceID,'') END + '|' + ISNULL(XT.TaskID,'') AS RowID,
        @strTaskID AS ParentRowID,
        P.PlanID AS PlanID,
        XT.TaskID AS TaskID,
        XT.AssignmentID AS AssignmentID,
        XT.ResourceID AS ResourceID,
		XT.ResourceType as ResourceType,
        XT.GenericResourceID AS GenericResourceID,
        XT.WBS1 AS WBS1,
        XT.WBS2 AS WBS2,
        XT.WBS3 AS WBS3,
        XT.OutlineNumber AS OutlineNumber,
        0 AS HasChildren,
        1 AS LeafNode,
        XT.OutlineLevel + 1 AS RowLevel,
        CASE 
          WHEN XT.ResourceID IS NULL 
          THEN GR.Name 
          ELSE CONVERT(Nvarchar(255), ISNULL(ISNULL(EM.PreferredName, EM.FirstName), N'') + ISNULL((N' ' + EM.LastName), N'')) 
        END AS Name,
        PlanStart AS StartDate,
        PlanEnd AS EndDate,
        XT.PlannedHrs AS PlannedHrs,
        XT.ETCHrs AS ETCHrs,
        XT.JTDHrs AS JTDHrs,
        XT.JTDHrs + XT.ETCHrs AS EACHrs,
        CASE WHEN XT.ResourceID IS NOT NULL THEN CASE WHEN EP.Photo IS NULL THEN 0 ELSE 1 END ELSE 0 END AS HasPhoto,
        CASE WHEN XT.ResourceID IS NOT NULL THEN CASE WHEN EP.Photo IS NULL THEN NULL ELSE EP.ModDate END ELSE NULL END AS PhotoModDate,
	    ISNULL(XT.Org, '') AS Org,
        ISNULL(O.Name, '') AS OrgName,
				ISNULL(OL1.OrgCode,'') AS OrgLevel1,
				ISNULL(OL2.OrgCode,'') AS OrgLevel2,
				ISNULL(OL3.OrgCode,'') AS OrgLevel3,
				ISNULL(OL4.OrgCode,'') AS OrgLevel4,
				ISNULL(OL5.OrgCode,'') AS OrgLevel5,
        ISNULL(XT.ProjMgr, '') AS ProjMgr,
        CONVERT(Nvarchar(255), ISNULL(PM.FirstName, '') + ISNULL(' ' + PM.LastName, '')) AS PMFullName,
        ISNULL(CL.Name, '') AS ClientName,
        XT.ASG_HardBooked AS HardBooked,
        XT.StartDate AS MinASGDate,
        XT.EndDate AS MaxASGDate,
				XT.TopName AS TopName,
				XT.TopWBS1 AS TopWBS1,
				dbo.ngRPGetConCatTaskName(XT.PlanID,XT.TaskID) AS ProjectWBSName,
				dbo.ngRPGetConCatTaskWBS1(XT.PlanID,XT.TaskID) AS ProjectWBS,
				XT.ProjectType AS ProjectType,
				XT.SelectedPlannedHrs,
				XT.SelectedETCHrs,
				ROUND(EM.HoursPerDay * @intScopeWorkingDays, @siHrDecimals) AS AvailableHrs,
				CASE WHEN EM.HoursPerDay = 0 OR @intScopeWorkingDays = 0 THEN 0 ELSE ROUND(((XT.SelectedPlannedHrs / (EM.HoursPerDay * @intScopeWorkingDays)) * 100), @siHrDecimals) END AS ScheduledPct,
				PR.UtilizationScheduleFlg AS UtilizationScheduleFlg,
				CASE WHEN EM.HoursPerDay = 0 OR @intScopeWorkingDays = 0 THEN 0 ELSE ROUND(((XT.SelectedBillableHrs / (EM.HoursPerDay * @intScopeWorkingDays)) * 100), @siHrDecimals) END AS UtilizationPct,
				EM.UtilizationRatio AS TargetUtilization
      FROM (SELECT
          T.PlanID,
          T.TaskID,
          T.WBS1,
          T.WBS2,
          T.WBS3,
          T.OutlineNumber,
          T.OutlineLevel,
          T.ChildrenCount,
          MAX(CASE WHEN T.LaborCode IS NOT NULL THEN L.LaborCodeName ELSE T.NAME END) AS Name,
          T.Org,
          T.ClientID,
          T.ProjMgr,
					T.StartDate AS PlanStart,
					T.EndDate AS PlanEnd,
          A.StartDate,
          A.EndDate,
          A.AssignmentID,
          A.ResourceID,
		  Case isnull(A.ResourceID,'') when '' then 'G' Else 'E' end as Resourcetype,
          A.GenericResourceID,
          CASE SIGN(MIN(A.HardBookedFlg) + MAX(A.HardBookedFlg))
            WHEN 0 THEN 'B'
            WHEN 1 THEN 'Y'
            WHEN -1 THEN 'N'
          END AS ASG_HardBooked,
          ROUND(ISNULL(SUM(ETCHrs), 0.0000), @siHrDecimals) AS ETCHrs,
          ROUND(ISNULL(SUM(PlannedHrs), 0.0000), @siHrDecimals) AS PlannedHrs,
          ROUND(ISNULL(SUM(JTDHrs), 0.0000), @siHrDecimals) AS JTDHrs,
					ROUND(ISNULL(SUM(SelectedPlannedHrs), 0.0000), @siHrDecimals) AS SelectedPlannedHrs,
					ROUND(ISNULL(SUM(SelectedETCHrs), 0.0000), @siHrDecimals) AS SelectedETCHrs,
					ROUND(ISNULL(SUM(SelectBillableHrs), 0.0000), @siHrDecimals) AS SelectedBillableHrs,
					PT.Name AS TopName,
					PT.WBS1 AS TopWBS1,					
					T.ProjectType AS ProjectType
      FROM @tabAssignment A 
        INNER JOIN RPTask AS T ON T.TaskID = A.TaskID
		CROSS APPLY dbo.stRP$tabLaborCode(T.PlanID,T.LaborCode) AS L
        LEFT JOIN (
            SELECT
              PlanID AS PlanID,
              AssignmentID AS AssignmentID,
							ISNULL(SUM(TETCHrs), 0.0000) AS ETCHrs,
							ISNULL(SUM(PTPDHrs), 0.0000) AS PlannedHrs,
              ISNULL(SUM(JTDHrs), 0.0000) AS JTDHrs,
							ISNULL(SUM(STPDHrs), 0.0000) AS SelectedPlannedHrs,
							ISNULL(SUM(SETCHrs), 0.0000) AS SelectedETCHrs,
							ISNULL(SUM(PTPDHrs), 0.0000) AS SelectBillableHrs
              FROM (
                SELECT 
                  PlanID AS PlanID, AssignmentID AS AssignmentID,
                  ETCHrs AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                  0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
                  0 AS JTDHrs
                  FROM @tabTotalETC
                UNION ALL
                SELECT 
                  PlanID AS PlanID, AssignmentID AS AssignmentID,
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                  PlannedHrs AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
                  0 AS JTDHrs
                  FROM @tabLabTPD
                UNION ALL
                SELECT 
                  PlanID AS PlanID, AssignmentID AS AssignmentID,
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                  0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, PeriodHrs AS JTDHrs
                  FROM @tabLD
                UNION ALL
                SELECT 
                  PlanID AS PlanID, AssignmentID AS AssignmentID,
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                  0 AS PTPDHrs, PlannedHrs AS STPDHrs, 0 AS BTPDHrs,
									0 AS JTDHrs
                  FROM @tabSelectedTPD
                UNION ALL
                SELECT 
                  PlanID AS PlanID, AssignmentID AS AssignmentID,
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, ETCHrs AS SETCHrs,
                  0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
									0 AS JTDHrs
                  FROM @tabSelectedETC
                UNION ALL
                SELECT 
                  PlanID AS PlanID, AssignmentID AS AssignmentID,
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                  0 AS PTPDHrs, 0 AS STPDHrs, PlannedHrs AS BTPDHrs,
									0 AS JTDHrs
                  FROM @tabBillableTPD
              ) AS ZE
              GROUP BY ZE.PlanID, ZE.AssignmentID
          ) AS XE ON XE.PlanID = A.PlanID AND XE.AssignmentID = A.AssignmentID
			INNER JOIN RPTask PT ON PT.PlanID = T.PlanID AND T.OutlineNumber LIKE PT.OutlineNumber + '%'
      WHERE PT.TaskID = @strTaskID
      GROUP BY T.PlanID,T.TaskID,T.WBS1,T.WBS2,T.WBS3,T.OutlineNumber,T.OutlineLevel,T.ChildrenCount,T.Name,T.Org,T.ClientID,T.ProjMgr,T.StartDate,T.EndDate,A.StartDate,A.EndDate,A.AssignmentID,A.ResourceID,A.GenericResourceID,PT.Name,PT.WBS1,T.ProjectType) AS XT
      INNER JOIN RPPlan AS P ON P.PlanID = XT.PlanID
	  LEFT JOIN PR  ON XT.WBS1 = PR.WBS1 AND ISNULL(XT.WBS2, ' ') = PR.WBS2 AND ISNULL(XT.WBS3, ' ') = PR.WBS3
      LEFT JOIN EM ON EM.Employee = XT.ResourceID
      LEFT JOIN EMPhoto EP ON EM.Employee = EP.Employee
      LEFT JOIN GR ON GR.Code = XT.GenericResourceID
      LEFT JOIN CL ON XT.ClientID = CL.ClientID
      LEFT JOIN EM AS PM ON XT.ProjMgr = PM.Employee
      LEFT JOIN Organization AS O ON XT.Org = O.Org
			OUTER APPLY ngRP$tabOrgLevel(O.Org,1) AS OL1
			OUTER APPLY ngRP$tabOrgLevel(O.Org,2) AS OL2
			OUTER APPLY ngRP$tabOrgLevel(O.Org,3) AS OL3
			OUTER APPLY ngRP$tabOrgLevel(O.Org,4) AS OL4
			OUTER APPLY ngRP$tabOrgLevel(O.Org,5) AS OL5
    END /* END Assignment Rows */

  ELSE
		IF @strMode = 'S'  /* (@strMode = 'S') */
			BEGIN
				INSERT @tabProjectView (
				RowID,
				ParentRowID,
				PlanID,
				TaskID,
				AssignmentID,
				ResourceID,		
				ResourceType,		
				GenericResourceID,
				WBS1,
				WBS2,
				WBS3,
				OutlineNumber,
				HasChildren,
				LeafNode,
				RowLevel,
				Name,
				StartDate,
				EndDate,
				PlannedHrs,
				ETCHrs,
				JTDHrs,
				EACHrs,
				HasPhoto,
				PhotoModDate,
				Org,
				OrgName,
				OrgLevel1,
				OrgLevel2,
				OrgLevel3,
				OrgLevel4,
				OrgLevel5,
				ProjMgr,
				PMFullName,
				ClientName,
				HardBooked,
				MinASGDate,
				MaxASGDate,
				TopName,
				TopWBS1,
				ProjectWBSName,
				ProjectWBS,
				ProjectType,
				SelectedPlannedHrs,
				SelectedETCHrs,
				AvailableHrs,
				ScheduledPct,
				UtilizationScheduleFlg,
				TargetUtilization,
				UtilizationPct
			)
				SELECT
					'|' + ISNULL(YT.TaskID,'') AS RowID,
					@strRowID AS ParentRowID,
					YT.PlanID AS PlanID,
					YT.TaskID AS TaskID,
					null AS AssignmentID,
					null AS ResourceID,
					null As ResourceType,
					null AS GenericResourceID,
					YT.WBS1 AS WBS1,
					YT.WBS2 AS WBS2,
					YT.WBS3 AS WBS3,
					YT.OutlineNumber AS OutlineNumber,
					CASE WHEN YT.ChildrenCount > 0 THEN CONVERT(bit, 1) ELSE CASE WHEN EXISTS (SELECT 'X' FROM RPAssignment WHERE TaskID = YT.TaskID) THEN CONVERT(bit,1) ELSE CONVERT(bit, 0) END END AS HasChildren,
					CASE WHEN YT.ChildrenCount > 0 THEN CONVERT(bit, 0) ELSE CONVERT(bit,1) END AS LeafNode,
					YT.OutlineLevel AS RowLevel,
					YT.Name AS Name,
					YT.StartDate AS StartDate,
					YT.EndDate AS EndDate,
					YT.PlannedHrs AS PlannedHrs,
					YT.ETCHrs AS ETCHrs, 
					YT.JTDHrs AS JTDHrs,
					YT.ETCHrs + YT.JTDHrs AS EACHrs,
					0 as HasPhoto,
					NULL as PhotoModDate,
					ISNULL(YT.Org, '') AS Org,
					ISNULL(O.Name, '') AS OrgName,
					ISNULL(OL1.OrgCode,'') AS OrgLevel1,
					ISNULL(OL2.OrgCode,'') AS OrgLevel2,
					ISNULL(OL3.OrgCode,'') AS OrgLevel3,
					ISNULL(OL4.OrgCode,'') AS OrgLevel4,
					ISNULL(OL5.OrgCode,'') AS OrgLevel5,
					ISNULL(YT.ProjMgr, '') AS ProjMgr,
					CONVERT(Nvarchar(255), ISNULL(PM.FirstName, '') + ISNULL(' ' + PM.LastName, '')) AS PMFullName,
					ISNULL(CL.Name, '') AS ClientName,
					ASG_HardBooked AS HardBooked,
					MinASGDate,
					MaxASGDate,
					YT.Name AS TopName,
					YT.WBS1 AS TopWBS1,
					dbo.ngRPGetConCatTaskName(YT.PlanID,YT.TaskID) AS ProjectWBSName,
					dbo.ngRPGetConCatTaskWBS1(YT.PlanID,YT.TaskID) AS ProjectWBS,
					YT.ProjectType AS ProjectType,
					YT.SelectedPlannedHrs,
					YT.SelectedETCHrs,
					0 AS AvailableHrs,
					0 AS ScheduledPct,
					PR.UtilizationScheduleFlg AS UtilizationScheduleFlg,
					0 AS UtilizationPct,
					0 AS TargetUtilization
				FROM (SELECT
						XT.PlanID,
						XT.TaskID,
						XT.WBS1,
						XT.WBS2,
						XT.WBS3,
						XT.OutlineNumber,
						XT.OutlineLevel,
						XT.ChildrenCount,
						XT.Name,
						XT.Org,
						XT.ClientID,
						XT.ProjMgr,
						XT.StartDate,
						XT.EndDate,
						MAX(ETCHrs) AS ETCHrs,
						MAX(PlannedHrs) AS PlannedHrs,
						MAX(JTDHrs) AS JTDHrs,
						CASE SIGN(MIN(A.HardBookedFlg) + MAX(A.HardBookedFlg))
							WHEN 0 THEN 'B'
							WHEN 1 THEN 'Y'
							WHEN -1 THEN 'N'
						END AS ASG_HardBooked,
						MIN(A.StartDate) AS MinASGDate,
						MAX(A.EndDate) AS MaxASGDate,
						XT.ProjectType,
						MAX(SelectedPlannedHrs) AS SelectedPlannedHrs,
						MAX(SelectedETCHrs) AS SelectedETCHrs
				FROM (SELECT
						T.PlanID,
						T.TaskID,
						T.WBS1,
						T.WBS2,
						T.WBS3,
						T.OutlineNumber,
						T.OutlineLevel,
						T.ChildrenCount,
						MAX(CASE WHEN T.LaborCode IS NOT NULL THEN L.LaborCodeName ELSE T.NAME END) AS Name,
						T.Org,
						T.ClientID,
						T.ProjMgr,
						T.StartDate,
						T.EndDate,
						ROUND(ISNULL(SUM(ETCHrs), 0.0000), @siHrDecimals) AS ETCHrs,
						ROUND(ISNULL(SUM(PlannedHrs), 0.0000), @siHrDecimals) AS PlannedHrs,
						ROUND(ISNULL(SUM(JTDHrs), 0.0000), @siHrDecimals) AS JTDHrs,
						ROUND(ISNULL(SUM(SelectedPlannedHrs), 0.0000), @siHrDecimals) AS SelectedPlannedHrs,
						ROUND(ISNULL(SUM(SelectedETCHrs), 0.0000), @siHrDecimals) AS SelectedETCHrs,
						T.ProjectType
				FROM 
					RPTask AS T  
					CROSS APPLY dbo.stRP$tabLaborCode(T.PlanID,T.LaborCode) AS L
					LEFT JOIN (
							SELECT
								PlanID AS PlanID,
								OutlineNumber AS OutlineNumber,
								ISNULL(SUM(TETCHrs), 0.0000) AS ETCHrs,
								ISNULL(SUM(PTPDHrs), 0.0000) AS PlannedHrs,
								ISNULL(SUM(JTDHrs), 0.0000) AS JTDHrs,
								ISNULL(SUM(STPDHrs), 0.0000) AS SelectedPlannedHrs,
								ISNULL(SUM(SETCHrs), 0.0000) AS SelectedETCHrs
								FROM (
									SELECT 
										PlanID AS PlanID, OutlineNumber AS OutlineNumber,
										ETCHrs AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
										0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
										0 AS JTDHrs
										FROM @tabTotalETC
									UNION ALL
									SELECT 
										PlanID AS PlanID, OutlineNumber AS OutlineNumber,
										0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
										PlannedHrs AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
										0 AS JTDHrs
										FROM @tabLabTPD
									UNION ALL
									SELECT 
										PlanID AS PlanID, OutlineNumber AS OutlineNumber,
										0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
										0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, PeriodHrs AS JTDHrs
										FROM @tabLD
									UNION ALL
									SELECT 
										PlanID AS PlanID, OutlineNumber AS OutlineNumber,
										0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
										0 AS PTPDHrs, PlannedHrs AS STPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs
										FROM @tabSelectedTPD
									UNION ALL
									SELECT 
										PlanID AS PlanID, OutlineNumber AS OutlineNumber,
										0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, ETCHrs AS SETCHrs, 
										0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs
										FROM @tabSelectedETC
								) AS ZE
								GROUP BY ZE.PlanID, ZE.OutlineNumber
						) AS XE ON XE.PlanID = T.PlanID AND XE.OutlineNumber LIKE T.OutlineNumber + '%'
				WHERE T.TaskID = @strTaskID AND T.PlanID = @strPlanID
				GROUP BY T.PlanID,T.TaskID,T.WBS1,T.WBS2,T.WBS3,T.OutlineNumber,T.OutlineLevel,T.ChildrenCount,T.Name,T.Org,T.ClientID,T.ProjMgr,T.StartDate,T.EndDate,T.ProjectType) AS XT
					LEFT JOIN @tabAssignment A ON A.PlanID = XT.PlanID AND A.OutlineNumber LIKE XT.OutlineNumber + '%'
					GROUP BY XT.PlanID,XT.TaskID,XT.WBS1,XT.WBS2,XT.WBS3,XT.OutlineNumber,XT.OutlineLevel,XT.ChildrenCount,XT.Name,XT.Org,XT.ClientID,XT.ProjMgr,XT.StartDate,XT.EndDate,XT.ProjectType) AS YT
					INNER JOIN RPPlan P ON P.PlanID = YT.PlanID
			  	    LEFT JOIN PR  ON YT.WBS1 = PR.WBS1 AND ISNULL(YT.WBS2, ' ') = PR.WBS2 AND ISNULL(YT.WBS3, ' ') = PR.WBS3
					LEFT JOIN CL ON YT.ClientID = CL.ClientID
					LEFT JOIN EM AS PM ON YT.ProjMgr = PM.Employee
					LEFT JOIN Organization AS O ON YT.Org = O.Org
					OUTER APPLY ngRP$tabOrgLevel(O.Org,1) AS OL1
					OUTER APPLY ngRP$tabOrgLevel(O.Org,2) AS OL2
					OUTER APPLY ngRP$tabOrgLevel(O.Org,3) AS OL3
					OUTER APPLY ngRP$tabOrgLevel(O.Org,4) AS OL4
					OUTER APPLY ngRP$tabOrgLevel(O.Org,5) AS OL5
			END /* END IF (@strMode = 'P') */
		ELSE   /* (@strMode = 'P') */
			BEGIN
				INSERT @tabProjectView (
					RowID,
					ParentRowID,
					PlanID,
					TaskID,
					AssignmentID,	
					ResourceID,
					ResourceType,
					GenericResourceID,
					WBS1,
					WBS2,
					WBS3,
					OutlineNumber,
					HasChildren,
					LeafNode,
					RowLevel,
					Name,
					StartDate,
					EndDate,
					PlannedHrs,
					ETCHrs,
					JTDHrs,
					EACHrs,
					HasPhoto,
					PhotoModDate,
					Org,
					OrgName,
					OrgLevel1,
					OrgLevel2,
					OrgLevel3,
					OrgLevel4,
					OrgLevel5,
					ProjMgr,
					PMFullName,
					ClientName,
					HardBooked,
					MinASGDate,
					MaxASGDate,
					TopName,
					TopWBS1,
					ProjectWBSName,
					ProjectWBS,
					ProjectType,
					SelectedPlannedHrs,
					SelectedETCHrs,
					AvailableHrs,
					ScheduledPct,
					UtilizationScheduleFlg,
					UtilizationPct,
					TargetUtilization
				)
				SELECT
					CASE WHEN XT.ResourceID IS NULL THEN 'G~' + ISNULL(XT.GenericResourceID,'') ELSE 'E~' + ISNULL(XT.ResourceID,'') END + '|' + @strTaskID AS RowID,
					@strTaskID AS ParentRowID,
					P.PlanID AS PlanID,
					@strTaskID AS TaskID,
					XT.AssignmentID AS AssignmentID,
					XT.ResourceID AS ResourceID,
					XT.ResourceType as ResourceType,
					XT.GenericResourceID AS GenericResourceID,
					XT.WBS1 AS WBS1,
					XT.WBS2 AS WBS2,
					XT.WBS3 AS WBS3,
					XT.OutlineNumber AS OutlineNumber,
					0 AS HasChildren,
					1 AS LeafNode,
					XT.OutlineLevel + 1 AS RowLevel,
					CASE 
          WHEN XT.ResourceID IS NULL 
          THEN GR.Name 
          ELSE CONVERT(Nvarchar(255), ISNULL(ISNULL(EM.PreferredName, EM.FirstName), N'') + ISNULL((N' ' + EM.LastName), N'')) 
        END AS Name,
					XT.PlanStart AS StartDate,
					XT.PlanEnd AS EndDate,
					XT.PlannedHrs AS PlannedHrs,
					XT.ETCHrs AS ETCHrs,
					XT.JTDHrs AS JTDHrs,
					XT.JTDHrs + XT.ETCHrs AS EACHrs,
					CASE WHEN XT.ResourceID IS NOT NULL THEN CASE WHEN EP.Photo IS NULL THEN 0 ELSE 1 END ELSE 0 END AS HasPhoto,
					CASE WHEN XT.ResourceID IS NOT NULL THEN CASE WHEN EP.Photo IS NULL THEN NULL ELSE EP.ModDate END ELSE NULL END AS PhotoModDate,
					ISNULL(XT.Org, '') AS Org,
					ISNULL(O.Name, '') AS OrgName,
					ISNULL(OL1.OrgCode,'') AS OrgLevel1,
					ISNULL(OL2.OrgCode,'') AS OrgLevel2,
					ISNULL(OL3.OrgCode,'') AS OrgLevel3,
					ISNULL(OL4.OrgCode,'') AS OrgLevel4,
					ISNULL(OL5.OrgCode,'') AS OrgLevel5,
					ISNULL(XT.ProjMgr, '') AS ProjMgr,
					CONVERT(Nvarchar(255), ISNULL(PM.FirstName, '') + ISNULL(' ' + PM.LastName, '')) AS PMFullName,
					ISNULL(CL.Name, '') AS ClientName,
					XT.ASG_HardBooked AS HardBooked,
					XT.StartDate AS MinASGDate,
					XT.EndDate AS MaxASGDate,
					XT.TopName AS TopName,
					XT.TopWBS1 AS TopWBS1,
					dbo.ngRPGetConCatTaskName(XT.PlanID,XT.TaskID) AS ProjectWBSName,
					dbo.ngRPGetConCatTaskWBS1(XT.PlanID,XT.TaskID) AS ProjectWBS,
					XT.ProjectType AS ProjectType,
					XT.SelectedPlannedHrs,
					XT.SelectedETCHrs,
					ROUND(EM.HoursPerDay * @intScopeWorkingDays, @siHrDecimals) AS AvailableHrs,
					CASE WHEN EM.HoursPerDay = 0 OR @intScopeWorkingDays = 0 THEN 0 ELSE ROUND(((XT.SelectedPlannedHrs / (EM.HoursPerDay * @intScopeWorkingDays)) * 100), @siHrDecimals) END AS ScheduledPct,
					PR.UtilizationScheduleFlg AS UtilizationScheduleFlg,
					CASE WHEN EM.HoursPerDay = 0 OR @intScopeWorkingDays = 0 THEN 0 ELSE ROUND(((XT.SelectedBillableHrs / (EM.HoursPerDay * @intScopeWorkingDays)) * 100), @siHrDecimals) END AS UtilizationPct,
					EM.UtilizationRatio AS TargetUtilization
				FROM (SELECT
						T.PlanID,
						NULL AS TaskID,
						T.WBS1,
						T.WBS2,
						T.WBS3,
						NULL AS OutlineNumber,
						NULL AS OutlineLevel,
						NULL AS ChildrenCount,
						MAX(CASE WHEN T.LaborCode IS NOT NULL THEN L.LaborCodeName ELSE T.NAME END) AS Name,
						T.Org,
						T.ClientID,
						T.ProjMgr,
						T.StartDate AS PlanStart,
						T.EndDate AS PlanEnd,
						MIN(A.StartDate) AS StartDate,
						MAX(A.EndDate) AS EndDate,
						NULL AS AssignmentID,
						A.ResourceID,
						Case isnull(A.ResourceID,'') when '' then 'G' Else 'E' end as Resourcetype,
						A.GenericResourceID,
						CASE SIGN(MIN(A.HardBookedFlg) + MAX(A.HardBookedFlg))
							WHEN 0 THEN 'B'
							WHEN 1 THEN 'Y'
							WHEN -1 THEN 'N'
						END AS ASG_HardBooked,
						ROUND(ISNULL(SUM(ETCHrs), 0.0000), @siHrDecimals) AS ETCHrs,
						ROUND(ISNULL(SUM(PlannedHrs), 0.0000), @siHrDecimals) AS PlannedHrs,
						ROUND(ISNULL(SUM(JTDHrs), 0.0000), @siHrDecimals) AS JTDHrs,
						ROUND(ISNULL(SUM(SelectedPlannedHrs), 0.0000), @siHrDecimals) AS SelectedPlannedHrs,
						ROUND(ISNULL(SUM(SelectedETCHrs), 0.0000), @siHrDecimals) AS SelectedETCHrs,
						ROUND(ISNULL(SUM(SelectBillableHrs), 0.0000), @siHrDecimals) AS SelectedBillableHrs,
						T.Name AS TopName,
						T.WBS1 AS TopWBS1,
						T.ProjectType AS ProjectType
					FROM RPTask T 
					INNER JOIN @tabAssignment AS A ON  T.PlanID = A.PlanID
					CROSS APPLY dbo.stRP$tabLaborCode(T.PlanID,T.LaborCode) AS L
					LEFT JOIN (
							SELECT
								PlanID AS PlanID,
								AssignmentID AS AssignmentID,
								ISNULL(SUM(TETCHrs), 0.0000) AS ETCHrs,
								ISNULL(SUM(PTPDHrs), 0.0000) AS PlannedHrs,
								ISNULL(SUM(JTDHrs), 0.0000) AS JTDHrs,
								ISNULL(SUM(STPDHrs), 0.0000) AS SelectedPlannedHrs,
								ISNULL(SUM(SETCHrs), 0.0000) AS SelectedETCHrs,
								ISNULL(SUM(PTPDHrs), 0.0000) AS SelectBillableHrs
								FROM (
									SELECT 
										PlanID AS PlanID, AssignmentID AS AssignmentID,
										ETCHrs AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
										0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
										0 AS JTDHrs
										FROM @tabTotalETC
									UNION ALL
									SELECT 
										PlanID AS PlanID, AssignmentID AS AssignmentID,
										0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
										PlannedHrs AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
										0 AS JTDHrs
										FROM @tabLabTPD
									UNION ALL
									SELECT 
										PlanID AS PlanID, AssignmentID AS AssignmentID,
										0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
										0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs, PeriodHrs AS JTDHrs
										FROM @tabLD
									UNION ALL
									SELECT 
										PlanID AS PlanID, AssignmentID AS AssignmentID,
										0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
										0 AS PTPDHrs, PlannedHrs AS STPDHrs, 0 AS BTPDHrs,
										0 AS JTDHrs
										FROM @tabSelectedTPD
									UNION ALL
									SELECT 
										PlanID AS PlanID, AssignmentID AS AssignmentID,
										0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, ETCHrs AS SETCHrs,
										0 AS PTPDHrs, 0 AS STPDHrs, 0 AS BTPDHrs,
										0 AS JTDHrs
										FROM @tabSelectedETC
									UNION ALL
									SELECT 
										PlanID AS PlanID, AssignmentID AS AssignmentID,
										0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
										0 AS PTPDHrs, 0 AS STPDHrs, PlannedHrs AS BTPDHrs,
										0 AS JTDHrs
										FROM @tabBillableTPD
								) AS ZE
								GROUP BY ZE.PlanID, ZE.AssignmentID
						) AS XE ON XE.PlanID = A.PlanID AND XE.AssignmentID = A.AssignmentID
					WHERE T.TaskID = @strTaskID AND T.PlanID = @strPlanID
				GROUP BY T.PlanID,T.WBS1,T.WBS2,T.WBS3,T.Name,T.Org,T.ClientID,T.ProjMgr,T.StartDate,T.EndDate,A.ResourceID,A.GenericResourceID,T.ProjectType) AS XT
				INNER JOIN RPPlan AS P ON P.PlanID = XT.PlanID
			  	    LEFT JOIN PR  ON XT.WBS1 = PR.WBS1 AND ISNULL(XT.WBS2, ' ') = PR.WBS2 AND ISNULL(XT.WBS3, ' ') = PR.WBS3
				LEFT JOIN EM ON EM.Employee = XT.ResourceID
				LEFT JOIN EMPhoto EP ON EM.Employee = EP.Employee
				LEFT JOIN GR ON GR.Code = XT.GenericResourceID
				LEFT JOIN CL ON XT.ClientID = CL.ClientID
				LEFT JOIN EM AS PM ON XT.ProjMgr = PM.Employee
				LEFT JOIN Organization AS O ON XT.Org = O.Org
				OUTER APPLY ngRP$tabOrgLevel(O.Org,1) AS OL1
				OUTER APPLY ngRP$tabOrgLevel(O.Org,2) AS OL2
				OUTER APPLY ngRP$tabOrgLevel(O.Org,3) AS OL3
				OUTER APPLY ngRP$tabOrgLevel(O.Org,4) AS OL4
				OUTER APPLY ngRP$tabOrgLevel(O.Org,5) AS OL5
			END
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
