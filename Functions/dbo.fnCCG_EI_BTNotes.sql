SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_BTNotes] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS varchar(max)
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.
	
	select dbo.fnCCG_EI_BTNotes('2003005.00',' ',' ')
*/
	declare @res varchar(max)
	select @res=Notes from BT where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3
	return @res
END
GO
