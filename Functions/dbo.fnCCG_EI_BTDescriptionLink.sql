SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_BTDescriptionLink] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), @USERNAME varchar(32))
RETURNS varchar(512)
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select dbo.fnCCG_EI_BTDescriptionLink('2003005.00', ' ', ' ','GRACEC')
*/
	declare @res varchar(512), @emp varchar(32)
	select @emp=Employee from SEUser where Username=@USERNAME
	select @res = 'BT Descr@' + Replace(dbo.fnCCG_GetWebServerURLPrefix(), '/Vision/', '') + '/EI_UpdateMemoField/EI_UpdateMemoField.aspx' +
		'?enc=fYawwoi46nbl28a90apsodifw09g55qlj209' +
		'&FL=Billing%20Description' +
		'&obs=920446587188924376' +
		'&T=BT' +
		'&tbl=CfgTemplateData' +
		'&F=Description' +
		'&EIF=' +
		'&SA=T' +
		'&SH=ALL' +
		'&E=' + @emp +
		'&WBS1=' + @WBS1 + '&WBS2=' + @WBS2 + '&WBS3=' + @WBS3
		from CFGSystem
	return @res
END
GO
