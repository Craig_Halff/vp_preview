SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$PlanIs_VorN]
  (@strWBS1 Nvarchar(30)) 
  RETURNS char(1)

BEGIN

  /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  /* The purpose of this UDF is to determine whether the input Project is mapped to a Navigator Plan                   */
  /* or to a Resource Planning Plan.                                                                                   */
  /* If there is no mapped Plan for the Project, then assume that the Project is mapped to a Navigator Plan.           */
  /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

  DECLARE @strRet char(1)

  SET @strRet = ''

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @strRet = 
    CASE WHEN EXISTS 
      (SELECT 'X' 
         FROM PNTask AS T
         INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND 
         T.WBSType = 'WBS1' AND T.WBS1 = @strWBS1) 
      THEN 'N' ELSE '' END

  IF (@strRet = 'N') RETURN(@strRet)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @strRet = 
    CASE WHEN EXISTS 
      (SELECT 'X' 
         FROM RPTask AS T
         INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND 
         T.WBSType = 'WBS1' AND T.WBS1 = @strWBS1) 
      THEN 'V' ELSE '' END

  IF (@strRet = 'V') RETURN(@strRet)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN(@strRet)

END 

GO
