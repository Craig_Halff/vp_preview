SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ngRP$tabOrgLevel](
  @strOrg Nvarchar(30),
  @siOrgLevel smallint
)
  RETURNS @tabOrgLevel TABLE (
    OrgCode Nvarchar(30) COLLATE database_default,
    OrgName Nvarchar(100) COLLATE database_default,
    OrgLike Nvarchar(30) COLLATE database_default
  )

BEGIN

  DECLARE @siOrgLevels smallint
  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint
  DECLARE @siOrg2Start smallint
  DECLARE @siOrg2Length smallint
  DECLARE @siOrg3Start smallint
  DECLARE @siOrg3Length smallint
  DECLARE @siOrg4Start smallint
  DECLARE @siOrg4Length smallint
  DECLARE @siOrg5Start smallint
  DECLARE @siOrg5Length smallint

  DECLARE @strOrgDelimiter varchar(1)
  DECLARE @strVariableOrgLevels varchar(1)
  DECLARE @strOrgCode Nvarchar(30)
  DECLARE @strName Nvarchar(100)
  DECLARE @strOrgLike Nvarchar(30)

  DECLARE @iOrgLength integer

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @iOrgLength = LEN(@strOrg)

  SELECT
    @strVariableOrgLevels = VariableOrgLevels,
    @strOrgDelimiter = OrgDelimiter,
    @siOrgLevels = OrgLevels,
    @siOrg1Start = Org1Start,
    @siOrg1Length = Org1Length,
    @siOrg2Start = Org2Start,
    @siOrg2Length = Org2Length,
    @siOrg3Start = Org3Start,
    @siOrg3Length = Org3Length,
    @siOrg4Start = Org4Start,
    @siOrg4Length = Org4Length,
    @siOrg5Start = Org5Start,
    @siOrg5Length = Org5Length
    FROM CFGFormat

  IF (@strVariableOrgLevels = 'N' AND @siOrgLevel > @siOrgLevels) RETURN

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strVariableOrgLevels = 'N')
    BEGIN

      SELECT @strOrgCode = 
        CASE @siOrgLevel
          WHEN 1 THEN SUBSTRING(@strOrg, @siOrg1Start, @siOrg1Length)
          WHEN 2 THEN SUBSTRING(@strOrg, @siOrg2Start, @siOrg2Length)
          WHEN 3 THEN SUBSTRING(@strOrg, @siOrg3Start, @siOrg3Length)
          WHEN 4 THEN SUBSTRING(@strOrg, @siOrg4Start, @siOrg4Length)
          WHEN 5 THEN SUBSTRING(@strOrg, @siOrg5Start, @siOrg5Length)
        END

      SELECT @strName = Label
        FROM CFGOrgCodes 
        WHERE Status = 'A' AND Code = @strOrgCode AND OrgLevel = @siOrgLevel

      SELECT @strOrgLike = @strOrgCode

    END /* (@strVariableOrgLevels = 'N') */

  ELSE /* (@strVariableOrgLevels = 'Y') */
    BEGIN

      ;
      WITH OrgLevel(Level, StartID, EndID) AS (
        SELECT 1, 1, CHARINDEX(@strOrgDelimiter, @strOrg, 0)
        UNION ALL
        SELECT Level + 1, EndID + 1, CHARINDEX(@strOrgDelimiter, @strOrg, EndID + 1)
          FROM OrgLevel
          WHERE EndID > 0
      )
      SELECT
        @strOrgCode = SUBSTRING(@strOrg, 1, CASE WHEN EndID > 0 THEN EndID - 1 ELSE @iOrgLength END),
        @strOrgLike = SUBSTRING(@strOrg, 1, CASE WHEN EndID > 0 THEN EndID - 1 ELSE @iOrgLength END) + '%'
        FROM OrgLevel
        WHERE Level = @siOrgLevel

      SELECT @strName = Name
        FROM Organization 
        WHERE Org = @strOrgCode

    END /* (@strVariableOrgLevels = 'Y') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabOrgLevel(
    OrgCode,
    OrgName,
    OrgLike
  )
    SELECT
      @strOrgCode AS OrgCode,
      @strName AS OrgName,
      @strOrgLike AS OrgLike

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
