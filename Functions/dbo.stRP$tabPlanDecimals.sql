SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabPlanDecimals]
  (@strPlanID varchar(32))

  RETURNS @tabDecimals TABLE(
    HrDecimals smallint,
    AmtCostDecimals smallint,
    AmtBillDecimals smallint,
    LabRateDecimals smallint
  )

BEGIN

  /*--------------------------------------------------------------------------------------------------------------------
    This function encapsulates the rules for Decimal Settings in Resource Planning.
    This function will return the following columns:
      HrDecimals: Decimal Setting for Labor Hours.
      AmtCostDecimals: Decimal Setting for Cost Amount.
      AmtBillDecimals: Decimal Setting for Billing Amount.
      LabRateDecimals: Decimal Setting for Labor Rate.
  */--------------------------------------------------------------------------------------------------------------------

  DECLARE @strCompany nvarchar(14)
  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)
  DECLARE @strReimbMethod varchar(1)
  DECLARE @strCostCurrencyCode nvarchar(3)
  DECLARE @strBillCurrencyCode nvarchar(3)

  DECLARE @siHrDecimals smallint
  DECLARE @siAmtDecimals smallint
  DECLARE @siAmtCostDecimals smallint
  DECLARE @siAmtBillDecimals smallint
  DECLARE @siLabRateDecimals smallint

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

  -- PNPlan.CostCurrencyCode
  -- PNPlan.BillingCurrencyCode

  SELECT 
    @strCompany = 
      CASE 
        WHEN @strMultiCompanyEnabled = 'Y' 
        THEN Company 
        ELSE ' ' 
      END,
    @strCostCurrencyCode = P.CostCurrencyCode,
    @strBillCurrencyCode = P.BillingCurrencyCode
    FROM PNPlan AS P
    WHERE P.PlanID = @strPlanID

   -- Get Decimal Settings from CFGRMSettings by Company.

  SELECT
    @siHrDecimals = HrDecimals,
    @siAmtDecimals = AmtDecimals
    FROM CFGRMSettings AS CRM
 
  -- Get Decimal Settings from CFGResourcePlanning by Company.

  SELECT
    @siLabRateDecimals = LabRateDecimals
    FROM CFGResourcePlanning AS CRP
    WHERE CRP.Company = @strCompany

  -- Compute @siAmtCostDecimals and @siAmtBillDecimals

  SELECT 
    @siAmtCostDecimals = ISNULL(MIN(CASE WHEN @siAmtDecimals <> 0 THEN CC.DecimalPlaces ELSE 0 END), 0), 
    @siAmtBillDecimals = ISNULL(MIN(CASE WHEN @siAmtDecimals <> 0 THEN BC.DecimalPlaces ELSE 0 END), 0)
    FROM FW_CFGCurrency AS CC, FW_CFGCurrency AS BC 
    WHERE CC.Code = @strCostCurrencyCode AND BC.Code = @strBillCurrencyCode 

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabDecimals (
    HrDecimals,
    AmtCostDecimals,
    AmtBillDecimals,
    LabRateDecimals
  )
    SELECT 
      @siHrDecimals AS HrDecimals,
      @siAmtCostDecimals AS AmtCostDecimals, 
      @siAmtBillDecimals AS AmtBillDecimals, 
      ISNULL(@siLabRateDecimals, 0) AS LabRateDecimals
      
  RETURN

END -- fn_stRP$tabPlanDecimals
GO
