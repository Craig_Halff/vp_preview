SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabConvertedRevenue]
  (@strPlanID varchar(32),
   @strRevenueSetting varchar(1),
   @strFeeByRowAndPeriod varchar(1),
   @strMultiCurrencyEnabled varchar(1),
   @siPlannedRevenueMethod smallint,
   @strReimbMethod varchar (1)
   )

  RETURNS @tabRevenue TABLE (
    PlanID varchar(32) COLLATE database_default,
    TimePhaseID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PeriodCost decimal(19, 4),
    PeriodBill decimal(19, 4),
	StoredRevenueCurrency varchar (1),
	RevenueFromFee varchar (1)
    UNIQUE(PlanID, TimePhaseID, WBS1, WBS2, WBS3 )
  )
    
BEGIN

DECLARE @bHasNegativeCostFee bit = 0
	DECLARE @bHasNegativeBillingFee bit = 0
	DECLARE @bHasNegativePlannedRevenue bit = 0

	DECLARE @strOldPlanID varchar(32) 

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	SET @strOldPlanID = @strPlanID
	
	IF @strRevenueSetting = 'G' 
	BEGIN
		-- Check if there negative value to be used for conversion
		SELECT @bHasNegativeCostFee = HasNegativeCostFee, @bHasNegativeBillingFee = HasNegativeBillingFee, @bHasNegativePlannedRevenue = HasNegativePlannedRevenue
		FROM dbo.stRP$tabRevenueNegativeValue (@strOldPlanID,'G')

		IF (SELECT COUNT(*) FROM (
				SELECT TimePhaseID FROM RPCompensationFee WHERE PeriodCost > 0 AND PlanID = @strOldPlanID
				UNION ALL
				SELECT TimePhaseID FROM RPReimbAllowance WHERE PeriodCost > 0  AND PlanID = @strOldPlanID
				UNION ALL
				SELECT TimePhaseID FROM RPConsultantFee WHERE PeriodCost > 0   AND PlanID = @strOldPlanID
			) AS G ) > 0 AND @bHasNegativeCostFee = 0 AND @strFeeByRowAndPeriod = 'Y'
		BEGIN
			INSERT @tabRevenue (
				PlanID ,
				TimePhaseID ,
				WBS1,
				WBS2,
				WBS3,
				StartDate,
				EndDate,
				PeriodCost,
				PeriodBill,
				StoredRevenueCurrency,
				RevenueFromFee
			)
			SELECT F.PlanID,
					TimePhaseID,
					T.WBS1,
					T.WBS2,
					T.WBS3,
					F.StartDate,
					F.EndDate,
					F.PeriodCost,
					0,
					'P',
					'Y'
					FROM RPCompensationFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
					WHERE PeriodCost > 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
			UNION ALL
			SELECT F.PlanID,
					TimePhaseID,
					T.WBS1,
					T.WBS2,
					T.WBS3,
					F.StartDate,
					F.EndDate,
					F.PeriodCost,
					0,
					'P',
					'Y'
					FROM RPReimbAllowance F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
					WHERE PeriodCost > 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
			UNION ALL
			SELECT F.PlanID,
					TimePhaseID,
					T.WBS1,
					T.WBS2,
					T.WBS3,
					F.StartDate,
					F.EndDate,
					F.PeriodCost,
					0,
					'P',
					'Y'
					FROM RPConsultantFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
					WHERE PeriodCost > 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
			END  
		ELSE IF (SELECT COUNT(*) FROM (
				SELECT TimePhaseID FROM RPCompensationFee WHERE PeriodBill > 0 AND PlanID = @strOldPlanID
				UNION ALL
				SELECT TimePhaseID FROM RPReimbAllowance WHERE PeriodBill > 0  AND PlanID = @strOldPlanID
				UNION ALL
				SELECT TimePhaseID FROM RPConsultantFee WHERE PeriodBill > 0   AND PlanID = @strOldPlanID
			) AS G ) > 0 AND @bHasNegativeCostFee = 0 AND @bHasNegativeBillingFee = 0 AND @strFeeByRowAndPeriod = 'Y'
			BEGIN
				INSERT @tabRevenue (
					PlanID ,
					TimePhaseID ,
					WBS1,
					WBS2,
					WBS3,
					StartDate,
					EndDate,
					PeriodCost,
					PeriodBill,
					StoredRevenueCurrency,
					RevenueFromFee
				)
				SELECT F.PlanID,
						TimePhaseID,
						T.WBS1,
						T.WBS2,
						T.WBS3,
						F.StartDate,
						F.EndDate,
						0,
						F.PeriodBill,
						'B',
						'Y'
						FROM RPCompensationFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
						WHERE PeriodBill > 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
				UNION ALL
				SELECT F.PlanID,
						TimePhaseID,
						T.WBS1,
						T.WBS2,
						T.WBS3,
						F.StartDate,
						F.EndDate,
						0,
						F.PeriodBill,
						'B',
						'Y'
						FROM RPReimbAllowance F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
						WHERE PeriodBill > 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
				UNION ALL
				SELECT F.PlanID,
						TimePhaseID,
						T.WBS1,
						T.WBS2,
						T.WBS3,
						F.StartDate,
						F.EndDate,
						0,
						F.PeriodBill,
						'B',
						'Y'
						FROM RPConsultantFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
						WHERE PeriodBill > 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
			END -- If From billing fee
			ELSE IF @bHasNegativeCostFee = 0 AND @bHasNegativeBillingFee = 0 AND @bHasNegativePlannedRevenue = 0 
			---- From planned revenue
			BEGIN 
		  	INSERT @tabRevenue (
				PlanID ,
				TimePhaseID ,
				WBS1,
				WBS2,
				WBS3,
				StartDate,
				EndDate,
				PeriodCost,
				PeriodBill,
				StoredRevenueCurrency,
				RevenueFromFee
			)
			SELECT F.PlanID,
					TimePhaseID,
					T.WBS1,
					T.WBS2,
					T.WBS3,
					F.StartDate,
					F.EndDate,
					CASE WHEN @siPlannedRevenueMethod < 2 OR @strMultiCurrencyEnabled = 'N' THEN PeriodRev ELSE 0 END AS PeriodCost,
					CASE WHEN @siPlannedRevenueMethod > 1 AND @strMultiCurrencyEnabled = 'Y' THEN PeriodRev ELSE 0 END AS PeriodBill,
					CASE WHEN @siPlannedRevenueMethod < 2 OR @strMultiCurrencyEnabled = 'N' THEN 'P' ELSE 'B' END AS StoredRevenueCurrency,'N'					
					FROM RPPlannedLabor F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
					WHERE PeriodRev > 0 	AND AssignmentID IS NOT NULL AND F.PlanID = @strOldPlanID		
			UNION ALL
			SELECT F.PlanID,
					TimePhaseID,
					T.WBS1,
					T.WBS2,
					T.WBS3,
					F.StartDate,
					F.EndDate,
					CASE WHEN @strReimbMethod = 'C' OR @strMultiCurrencyEnabled = 'N' THEN PeriodRev ELSE 0 END AS PeriodCost,
					CASE WHEN @strReimbMethod = 'B' AND @strMultiCurrencyEnabled = 'Y' THEN PeriodRev ELSE 0 END AS PeriodBill,
					CASE WHEN @siPlannedRevenueMethod < 2 OR @strMultiCurrencyEnabled = 'N' THEN 'P' ELSE 'B' END AS StoredRevenueCurrency,'N'	
					FROM RPPlannedExpenses F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
					INNER JOIN RPExpense E ON E.PlanID = F.PlanID AND E.TaskID = F.TaskID AND E.ExpenseID = F.ExpenseID
					INNER JOIN CA ON CA.Account = E.Account
					WHERE PeriodRev > 0 	AND F.ExpenseID IS NOT NULL AND CA.Type = '5' AND F.PlanID = @strOldPlanID	
			UNION ALL
			SELECT F.PlanID,
					TimePhaseID,
					T.WBS1,
					T.WBS2,
					T.WBS3,
					F.StartDate,
					F.EndDate,
					CASE WHEN @strReimbMethod = 'C' OR @strMultiCurrencyEnabled = 'N' THEN PeriodRev ELSE 0 END AS PeriodCost,
					CASE WHEN @strReimbMethod = 'B' AND @strMultiCurrencyEnabled = 'Y' THEN PeriodRev ELSE 0 END AS PeriodBill,
					CASE WHEN @siPlannedRevenueMethod < 2 OR @strMultiCurrencyEnabled = 'N' THEN 'P' ELSE 'B' END AS StoredRevenueCurrency,'N'	
					FROM RPPlannedConsultant F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
					INNER JOIN RPConsultant C ON C.PlanID = F.PlanID AND C.TaskID = F.TaskID AND C.ConsultantID = F.ConsultantID
					INNER JOIN CA ON CA.Account = C.Account
					WHERE PeriodRev > 0 	AND F.ConsultantID IS NOT NULL AND CA.Type = '6' AND F.PlanID = @strOldPlanID								   	
			UNION ALL
				SELECT F.PlanID,
					TimePhaseID,
					T.WBS1,
					T.WBS2,
					T.WBS3,
					F.StartDate,
					F.EndDate,
					CASE WHEN @strReimbMethod = 'C' OR @strMultiCurrencyEnabled = 'N' THEN PeriodRev ELSE 0 END AS PeriodCost,
					CASE WHEN @strReimbMethod = 'B' AND @strMultiCurrencyEnabled = 'Y' THEN PeriodRev ELSE 0 END AS PeriodBill,
					CASE WHEN @siPlannedRevenueMethod < 2 OR @strMultiCurrencyEnabled = 'N' THEN 'P' ELSE 'B' END AS StoredRevenueCurrency,'N'	
					FROM RPPlannedUnit F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
					INNER JOIN RPUnit U ON U.PlanID = F.PlanID AND U.TaskID = F.TaskID AND U.UnitID = F.UnitID
					INNER JOIN CA ON CA.Account = U.Account
					WHERE PeriodRev > 0 	AND F.UnitID IS NOT NULL AND (CA.Type = '5' OR CA.Type = '6') AND F.PlanID = @strOldPlanID	
		END
	END --IF @strRevenueSetting = 'G'

	IF @strRevenueSetting = 'I' 
	BEGIN
		-- Check if there negative value to be used for conversion
		SELECT @bHasNegativeCostFee = HasNegativeCostFee, @bHasNegativeBillingFee = HasNegativeBillingFee, @bHasNegativePlannedRevenue = HasNegativePlannedRevenue
		FROM dbo.stRP$tabRevenueNegativeValue (@strOldPlanID,'G')

		IF (SELECT COUNT(*) FROM (
				SELECT TimePhaseID FROM RPCompensationFee WHERE PeriodCost > 0 AND PlanID = @strOldPlanID
				UNION ALL
				SELECT TimePhaseID FROM RPConsultantFee WHERE PeriodCost > 0   AND PlanID = @strOldPlanID
			) AS G ) > 0 AND @bHasNegativeCostFee = 0 AND @strFeeByRowAndPeriod = 'Y'
		BEGIN
			INSERT @tabRevenue (
				PlanID ,
				TimePhaseID ,
				WBS1,
				WBS2,
				WBS3,
				StartDate,
				EndDate,
				PeriodCost,
				PeriodBill,
				StoredRevenueCurrency,
				RevenueFromFee
			)
			SELECT F.PlanID,
					TimePhaseID,
					T.WBS1,
					T.WBS2,
					T.WBS3,
					F.StartDate,
					F.EndDate,
					F.PeriodCost,
					0,
					'P',
					'Y'
					FROM RPCompensationFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
					WHERE PeriodCost > 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
			UNION ALL
			SELECT F.PlanID,
					TimePhaseID,
					T.WBS1,
					T.WBS2,
					T.WBS3,
					F.StartDate,
					F.EndDate,
					F.PeriodCost,
					0,
					'P',
					'Y'
					FROM RPConsultantFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
					WHERE PeriodCost > 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
			END  
		ELSE IF (SELECT COUNT(*) FROM (
				SELECT TimePhaseID FROM RPCompensationFee WHERE PeriodBill > 0 AND PlanID = @strOldPlanID
				UNION ALL
				SELECT TimePhaseID FROM RPConsultantFee WHERE PeriodBill > 0   AND PlanID = @strOldPlanID
			) AS G ) > 0 AND @bHasNegativeCostFee = 0 AND @bHasNegativeBillingFee = 0 AND @strFeeByRowAndPeriod = 'Y'
			BEGIN
				INSERT @tabRevenue (
					PlanID ,
					TimePhaseID ,
					WBS1,
					WBS2,
					WBS3,
					StartDate,
					EndDate,
					PeriodCost,
					PeriodBill,
					StoredRevenueCurrency,
					RevenueFromFee
				)
				SELECT F.PlanID,
						TimePhaseID,
						T.WBS1,
						T.WBS2,
						T.WBS3,
						F.StartDate,
						F.EndDate,
						0,
						F.PeriodBill,
						'B',
						'Y'
						FROM RPCompensationFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
						WHERE PeriodBill > 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
				UNION ALL
				SELECT F.PlanID,
						TimePhaseID,
						T.WBS1,
						T.WBS2,
						T.WBS3,
						F.StartDate,
						F.EndDate,
						0,
						F.PeriodBill,
						'B',
						'Y'
						FROM RPConsultantFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
						WHERE PeriodBill > 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
			END -- If From billing fee
			ELSE IF @bHasNegativeCostFee = 0 AND @bHasNegativeBillingFee = 0 AND @bHasNegativePlannedRevenue = 0 
			---- From planned revenue
			BEGIN 
		  	INSERT @tabRevenue (
				PlanID ,
				TimePhaseID ,
				WBS1,
				WBS2,
				WBS3,
				StartDate,
				EndDate,
				PeriodCost,
				PeriodBill,
				StoredRevenueCurrency,
				RevenueFromFee
			)
			SELECT F.PlanID,
					TimePhaseID,
					T.WBS1,
					T.WBS2,
					T.WBS3,
					F.StartDate,
					F.EndDate,
					CASE WHEN @siPlannedRevenueMethod < 2 OR @strMultiCurrencyEnabled = 'N' THEN PeriodRev ELSE 0 END AS PeriodCost,
					CASE WHEN @siPlannedRevenueMethod > 1 AND @strMultiCurrencyEnabled = 'Y' THEN PeriodRev ELSE 0 END AS PeriodBill,
					CASE WHEN @siPlannedRevenueMethod < 2 OR @strMultiCurrencyEnabled = 'N' THEN 'P' ELSE 'B' END AS StoredRevenueCurrency,'N'
					FROM RPPlannedLabor F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
					WHERE PeriodRev > 0 	AND AssignmentID IS NOT NULL AND F.PlanID = @strOldPlanID		
		END
	END --IF @strRevenueSetting = 'I'

	IF @strRevenueSetting = 'E' 
	BEGIN
		-- Check if there negative value to be used for conversion
		SELECT @bHasNegativeCostFee = HasNegativeCostFee, @bHasNegativeBillingFee = HasNegativeBillingFee, @bHasNegativePlannedRevenue = HasNegativePlannedRevenue
		FROM dbo.stRP$tabRevenueNegativeValue (@strOldPlanID,'G')

		IF (SELECT COUNT(*) FROM (
				SELECT TimePhaseID FROM RPCompensationFee WHERE PeriodCost > 0 AND PlanID = @strOldPlanID
			) AS G ) > 0 AND @bHasNegativeCostFee = 0 AND @strFeeByRowAndPeriod = 'Y'
		BEGIN
			INSERT @tabRevenue (
				PlanID ,
				TimePhaseID ,
				WBS1,
				WBS2,
				WBS3,
				StartDate,
				EndDate,
				PeriodCost,
				PeriodBill,
				StoredRevenueCurrency,
				RevenueFromFee
			)
			SELECT F.PlanID,
					TimePhaseID,
					T.WBS1,
					T.WBS2,
					T.WBS3,
					F.StartDate,
					F.EndDate,
					F.PeriodCost,
					0,
					'P',
					'Y'
					FROM RPCompensationFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
					WHERE PeriodCost > 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
			END  
		ELSE IF (SELECT COUNT(*) FROM (
				SELECT TimePhaseID FROM RPCompensationFee WHERE PeriodBill > 0 AND PlanID = @strOldPlanID
			) AS G ) > 0 AND @bHasNegativeCostFee = 0 AND @bHasNegativeBillingFee = 0 AND @strFeeByRowAndPeriod = 'Y'
			BEGIN
				INSERT @tabRevenue (
					PlanID ,
					TimePhaseID ,
					WBS1,
					WBS2,
					WBS3,
					StartDate,
					EndDate,
					PeriodCost,
					PeriodBill,
					StoredRevenueCurrency,
					RevenueFromFee
				)
				SELECT F.PlanID,
						TimePhaseID,
						T.WBS1,
						T.WBS2,
						T.WBS3,
						F.StartDate,
						F.EndDate,
						0,
						F.PeriodBill,
						'B',
						'Y'
						FROM RPCompensationFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
						WHERE PeriodBill > 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
			END -- If From billing fee
			ELSE IF @bHasNegativeCostFee = 0 AND @bHasNegativeBillingFee = 0 AND @bHasNegativePlannedRevenue = 0 
			---- From planned revenue
			BEGIN 
		  	INSERT @tabRevenue (
				PlanID ,
				TimePhaseID ,
				WBS1,
				WBS2,
				WBS3,
				StartDate,
				EndDate,
				PeriodCost,
				PeriodBill,
				StoredRevenueCurrency,
				RevenueFromFee
			)
			SELECT F.PlanID,
					TimePhaseID,
					T.WBS1,
					T.WBS2,
					T.WBS3,
					F.StartDate,
					F.EndDate,
					CASE WHEN @siPlannedRevenueMethod < 2 OR @strMultiCurrencyEnabled = 'N' THEN PeriodRev ELSE 0 END AS PeriodCost,
					CASE WHEN @siPlannedRevenueMethod > 1 AND @strMultiCurrencyEnabled = 'Y' THEN PeriodRev ELSE 0 END AS PeriodBill,
					CASE WHEN @siPlannedRevenueMethod < 2 OR @strMultiCurrencyEnabled = 'N' THEN 'P' ELSE 'B' END AS StoredRevenueCurrency,'N'
					FROM RPPlannedLabor F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
					WHERE PeriodRev > 0 	AND AssignmentID IS NOT NULL AND F.PlanID = @strOldPlanID		
		END
	END --IF @strRevenueSetting = 'E'

	IF @strRevenueSetting = 'L' 
	BEGIN
		-- Check if there negative value to be used for conversion
		SELECT @bHasNegativeCostFee = HasNegativeCostFee, @bHasNegativeBillingFee = HasNegativeBillingFee, @bHasNegativePlannedRevenue = HasNegativePlannedRevenue
		FROM dbo.stRP$tabRevenueNegativeValue (@strOldPlanID,'G')

		IF (SELECT COUNT(*) FROM (
				SELECT TimePhaseID FROM RPCompensationFee WHERE PeriodDirLabCost > 0 AND PlanID = @strOldPlanID
			) AS G ) > 0 AND @bHasNegativeCostFee = 0 AND @strFeeByRowAndPeriod = 'Y'
		BEGIN
			INSERT @tabRevenue (
				PlanID ,
				TimePhaseID ,
				WBS1,
				WBS2,
				WBS3,
				StartDate,
				EndDate,
				PeriodCost,
				PeriodBill,
				StoredRevenueCurrency,
				RevenueFromFee
			)
			SELECT F.PlanID,
					TimePhaseID,
					T.WBS1,
					T.WBS2,
					T.WBS3,
					F.StartDate,
					F.EndDate,
					F.PeriodDirLabCost,
					0,
					'P',
					'Y'
					FROM RPCompensationFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
					WHERE PeriodDirLabCost > 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
			END  
		ELSE IF (SELECT COUNT(*) FROM (
				SELECT TimePhaseID FROM RPCompensationFee WHERE PeriodDirLabBill > 0 AND PlanID = @strOldPlanID
			) AS G ) > 0 AND @bHasNegativeCostFee = 0 AND @bHasNegativeBillingFee = 0  AND @strFeeByRowAndPeriod = 'Y'
			BEGIN
				INSERT @tabRevenue (
					PlanID ,
					TimePhaseID ,
					WBS1,
					WBS2,
					WBS3,
					StartDate,
					EndDate,
					PeriodCost,
					PeriodBill,
					StoredRevenueCurrency,
					RevenueFromFee
				)
				SELECT F.PlanID,
						TimePhaseID,
						T.WBS1,
						T.WBS2,
						T.WBS3,
						F.StartDate,
						F.EndDate,
						0,
						F.PeriodDirLabBill,
						'B',
						'Y'
						FROM RPCompensationFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
						WHERE PeriodDirLabBill > 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
			END -- If From billing fee
			ELSE IF @bHasNegativeCostFee = 0 AND @bHasNegativeBillingFee = 0 AND @bHasNegativePlannedRevenue = 0 
			---- From planned revenue
			BEGIN 
		  	INSERT @tabRevenue (
				PlanID ,
				TimePhaseID ,
				WBS1,
				WBS2,
				WBS3,
				StartDate,
				EndDate,
				PeriodCost,
				PeriodBill,
				StoredRevenueCurrency,
				RevenueFromFee
			)
			SELECT F.PlanID,
					TimePhaseID,
					T.WBS1,
					T.WBS2,
					T.WBS3,
					F.StartDate,
					F.EndDate,
					CASE WHEN @siPlannedRevenueMethod < 2 OR @strMultiCurrencyEnabled = 'N' THEN PeriodRev ELSE 0 END AS PeriodCost,
					CASE WHEN @siPlannedRevenueMethod > 1 AND @strMultiCurrencyEnabled = 'Y' THEN PeriodRev ELSE 0 END AS PeriodBill,
					CASE WHEN @siPlannedRevenueMethod < 2 OR @strMultiCurrencyEnabled = 'N' THEN 'P' ELSE 'B' END AS StoredRevenueCurrency,'N'
					FROM RPPlannedLabor F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
					WHERE PeriodRev > 0 	AND AssignmentID IS NOT NULL AND F.PlanID = @strOldPlanID		
		END
	END --IF @strRevenueSetting = 'E'

 RETURN
END 

GO
