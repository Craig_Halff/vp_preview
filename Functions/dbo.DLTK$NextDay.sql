SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DLTK$NextDay]
  (@dtCurrDate AS datetime, @intDWCode AS int)
  RETURNS datetime
BEGIN

  DECLARE @intCurrWkDay AS int
  DECLARE @intNumDays AS int

  -- Calculate weekday. 
  -- Need to take into consideration that @@DATEFIRST may not be set to default value.

  SET @intCurrWkDay = ((DATEPART(dw, @dtCurrDate) + @@DATEFIRST) % 7)

  -- Calculate number of days that needs to be added to the input date
  -- to arrive at the next closest date of the given weekday.

  SET @intNumDays = @intDWCode - @intCurrWkDay +( CASE WHEN @intCurrWkDay <= @intDWCode THEN 0 ELSE 7 END)

  RETURN DATEADD(day, @intNumDays, @dtCurrDate)

END -- fn_DLTK$NextDay
GO
