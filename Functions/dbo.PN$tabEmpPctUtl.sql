SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$tabEmpPctUtl]
  (@strStartDate VARCHAR(8),
   @strEndDate VARCHAR(8)) -- Date must be in format: 'yyyymmdd' regardless of UI Culture.

  RETURNS @tabEM TABLE
    (Employee Nvarchar(20) COLLATE database_default,
     PctUtilized decimal(19,4)
     UNIQUE(Employee)
    )

BEGIN

  DECLARE @strMultiCompanyEnabled VARCHAR(1)
  DECLARE @strUseBookingForEmpHours VARCHAR(1)
  DECLARE @strIncludeSoftBookedHours VARCHAR(1)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

  DECLARE @dtStartDate datetime
  DECLARE @dtEndDate datetime
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Set Start/End Dates.
  
  SET @dtStartDate = CONVERT(datetime, @strStartDate)
  SET @dtEndDate = CONVERT(datetime, @strEndDate) 

  -- Get MultiCompanyEnabled Flag.
  
  SELECT @strMultiCompanyEnabled = MultiCompanyEnabled FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat

  -- Get Flags for RM Settings.

  SELECT 
    @strUseBookingForEmpHours = UseBookingForEmpHours,
    @strIncludeSoftBookedHours = IncludeSoftBookedHours
    FROM CFGRMSettings

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabEM (
    Employee,
    PctUtilized
  )
    SELECT
      Employee AS Employee,
      PctUtilized AS PctUtilized
      FROM (
        SELECT 
          Employee AS Employee,
          ISNULL((SUM(PeriodHrs) / NULLIF(HoursPerDay * dbo.DLTK$NumWorkingDays(@dtStartDate, @dtEndDate, Company), 0.0000)), 0.0000) * 100 AS PctUtilized
          FROM (
            SELECT
              E.Employee AS Employee,
              E.HoursPerDay AS HoursPerDay,
              CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(E.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END AS Company,
              TPD.StartDate AS StartDate, 
              TPD.EndDate AS EndDate,
              CASE
                WHEN (TPD.StartDate >= @dtStartDate AND TPD.EndDate <= @dtEndDate) THEN PeriodHrs
                WHEN (TPD.StartDate < @dtStartDate AND TPD.EndDate <= @dtEndDate) THEN
                  PeriodHrs * dbo.DLTK$ProrateRatio(@dtStartDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, P.Company)
                WHEN (TPD.StartDate >= @dtStartDate AND TPD.EndDate > @dtEndDate) THEN
                  PeriodHrs * dbo.DLTK$ProrateRatio(TPD.StartDate, @dtEndDate, TPD.StartDate, TPD.EndDate, P.Company)
                WHEN (TPD.StartDate < @dtStartDate AND TPD.EndDate > @dtEndDate) THEN
                  PeriodHrs * dbo.DLTK$ProrateRatio(@dtStartDate, @dtEndDate, TPD.StartDate, TPD.EndDate, P.Company)
                END AS PeriodHrs
              FROM EM AS E
                INNER JOIN RPAssignment AS A ON E.Employee = A.ResourceID
                INNER JOIN RPPlan AS P ON A.PlanID = P.PlanID
                INNER JOIN RPPlannedLabor AS TPD ON TPD.AssignmentID = A.AssignmentID AND TPD.PlanID = A.PlanID
              WHERE E.Status = 'A' AND
                TPD.StartDate <= @dtEndDate AND TPD.EndDate >= @dtStartDate AND P.UtilizationIncludeFlg = 'Y' AND
                ((TPD.HardBooked = 'Y') OR
                 (TPD.HardBooked = 'N' AND @strUseBookingForEmpHours = 'N') OR
                 (TPD.HardBooked = 'N' AND @strIncludeSoftBookedHours = 'Y')
                )
          ) AS X
          GROUP BY Employee, HoursPerDay, Company
      ) AS Z
      WHERE PctUtilized > 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
