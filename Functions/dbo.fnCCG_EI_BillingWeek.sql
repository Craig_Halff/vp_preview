SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnCCG_EI_BillingWeek] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS varchar(30) 
AS BEGIN
/*
	Copyright 2016 Central Consulting Group.  All rights reserved.
	select dbo.fnCCG_EI_Status('2003005.xx',' ',' ')
	select dbo.fnCCG_EI_Status('2003005.xx','1PD',' ')
	
*/
	declare @res varchar(30)
	select @res = custbillingweek from ProjectCustomTabFields where @wbs1 = wbs1 and wbs2 = ' '
	

	return @res
END
GO
