SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabConsultant](
  @strRowID nvarchar(255),
  @strSessionID varchar(32),
  @strMode varchar(1), /* S = Self, C = Children */
  @bActiveWBSOnly bit = 0
)
  RETURNS @tabConsultants TABLE (

    RowID nvarchar(255) COLLATE database_default,
    ParentRowID nvarchar(255) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    ConsultantID varchar(32) COLLATE database_default,

    Account nvarchar(13) COLLATE database_default,
    AccountName nvarchar(40) COLLATE database_default,
    AccountTypeCode smallint,
    AccountType nvarchar(255) COLLATE database_default,
    Vendor nvarchar(30) COLLATE database_default,
    VendorName nvarchar(100) COLLATE database_default,

    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int,
    RowLevel int,
    Name nvarchar(255) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    MinASGDate datetime,
    MaxASGDate datetime,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    Org nvarchar(30) COLLATE database_default,
    OrgName nvarchar(100) COLLATE database_default,
    ProjMgr nvarchar(20) COLLATE database_default,
    UtilizationScheduleFlg varchar(1),
    PMFullName nvarchar(255) COLLATE database_default,
    ClientName nvarchar(100) COLLATE database_default,
    RT_Status varchar(1) COLLATE database_default,
    TopWBS1 nvarchar(30) COLLATE database_default,
    TopName nvarchar(255) COLLATE database_default,
    HasNotes varchar(1) COLLATE database_default,
    HasAssignments bit,
    HasChildren bit,
    LeafNode bit,
    
    ContractStartDate datetime,
    ContractEndDate datetime,

    BaselineCost decimal(19,4),
    BaselineBill decimal(19,4),

    JTDCost decimal(19,4),
    JTDReimCost decimal(19,4),
    JTDBill decimal(19,4),
    JTDReimBill decimal(19,4),

    ContractCost decimal(19,4),
    ContractDirCost decimal(19,4),
    ContractReimCost decimal(19,4),
    ContractBill decimal(19,4),
    ContractDirBill decimal(19,4),
    ContractReimBill decimal(19,4),

    PlannedCost decimal(19,4),
    PlannedReimCost decimal(19,4),
    PlannedBill decimal(19,4),
    PlannedReimBill decimal(19,4),

    ETCCost decimal(19,4),
    ETCBill decimal(19,4),
    EACCost decimal(19,4),
    EACBill decimal(19,4),
    MarkupCost decimal(19,4),
    ContractLessJTDBill decimal(19,4),

    CalcPctComplCost decimal(19,4),
    CalcPctComplBill decimal(19,4),
    PlannedLessJTDCost decimal(19,4),
    PlannedLessJTDBill decimal(19,4),
    PlannedLessEACCost decimal(19,4),
    PlannedLessEACBill decimal(19,4),
    ContractLessEACCost decimal(19,4),
    ContractLessEACBill decimal(19,4),
    ContractLessJTDCost decimal(19,4)
  )

BEGIN

/**************************************************************************************************************************/
--
-- Huge Assumptions:
--
-- 1. In Non-Vision worlds (e.g. Costpoint)
--    1.1 WBS structures will be imported into PR, RPTask, and PNTask tables.
--        1.1.1 PR table has rows with WBS1, WBS2, WBS3.
--        1.1.2 RPTask and PNTable tables also have WBS1, WBS2, WBS3, but can be indented using OutlineNumber.
--        1.1.3 RPTask and PNTable tables can have up to 15 indent levels (just like currently in ngRP).
--    1.2 WBS Numbers will be in WBS1, WBS2, WBS3 
--        (same rules as currently in Vision, such as WBS2 = <blank>, WBS3 = <blank> for top most WBS row)
--    1.3 WBS3 will cover WBS Level 3 through 15 (e.g. WBS3 = {1, 1.1, 1.1.1, 1.1.1.1).
--    1.4 JTD data will be at the lowest WBS level (e.g. JTD could be at level 15). 
--    1.5 JTD is matched using WBS1, WBS2, WBS3, Employee.
--    1.6 Assignments will be at lowest WBS level 
--        (e.g. Assignment could be at level 15, same level as in the case of JTD).
--
-- 2. In Vision world
--    2.1 PR, RPTask, and PNTask tables have only 3 levels.
--    2.2 JTD data will be at the lowest WBS level.
--    2.3 Assignments will be at lowest WBS level.
--
/**************************************************************************************************************************/

  DECLARE @_SectionSign nchar = NCHAR(167) -- N'§'
  DECLARE @_AccountVendorSign nchar = NCHAR(10132) -- N'➔'

  DECLARE @strCompany nvarchar(14)
  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)
  DECLARE @strTaskID varchar(32)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTopWBS1 nvarchar(30)
  DECLARE @strTopName nvarchar(255)
  DECLARE @strInputType varchar(1) = ''
  DECLARE @strIDPrefix nvarchar(255)
  DECLARE @strAccount nvarchar(26)
  DECLARE @strVendor nvarchar(40)
  DECLARE @strParentRowID nvarchar(255) = ''
  DECLARE @strTaskStatus varchar(1) = ''
  DECLARE @strWBS1 nvarchar(30)
  DECLARE @strWBS2 nvarchar(30) = ' '
  DECLARE @strWBS3 nvarchar(30) = ' '
  DECLARE @strWBS1WBS2WBS3 nvarchar(92)
  DECLARE @strOHProcedure varchar(1)
  DECLARE @strOrgOHAllocMethod varchar(1)
  DECLARE @strSysOHAllocMethod varchar(1)
  DECLARE @strUICultureName varchar(10)

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
  DECLARE @dtToday datetime 
  DECLARE @dtTomorrow datetime
  DECLARE @dtWBS1MinDate datetime
 
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intAccountVendorIndex int
  DECLARE @intRowIDSeperatorIndex int

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

  DECLARE @tiMinConWBSLevel tinyint

  DECLARE @bitIsLeafTask bit

  -- Declare Temp tables.

  DECLARE @tabWBS TABLE (
    RowID int IDENTITY(1,1),
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    WBS1WBS2WBS3 nvarchar(92) COLLATE database_default,
    Name nvarchar(255) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    NonLBCDChildrenCount int,
    OutlineLevel int,
    Org nvarchar(30) COLLATE database_default,
    ClientID varchar(32) COLLATE database_default,
    ProjMgr nvarchar(20) COLLATE database_default,
    UtilizationScheduleFlg varchar(1),
    StartDate datetime,
    EndDate datetime,
    Status varchar(1) COLLATE database_default,
    HasNotes varchar(1) COLLATE database_default,
    ContractStartDate datetime,
    ContractEndDate datetime,
    BaselineExpCost decimal(19,4),
    BaselineExpBill decimal(19,4),
    FeeCost decimal(19,4),
    FeeBill decimal(19,4),
    FeeDirCost decimal(19,4),
    FeeDirBill decimal(19,4),
    FeeReimCost decimal(19,4),
    FeeReimBill decimal(19,4)
    UNIQUE (RowID, WBS1, WBS2, WBS3, ParentOutlineNumber, OutlineNumber)
  )

  DECLARE @tabConsultant TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    ConsultantID varchar(32) COLLATE database_default,
    Account nvarchar(13) COLLATE database_default,
    AccountName nvarchar(40) COLLATE database_default,
    AccountTypeCode smallint,
    AccountType nvarchar(255) COLLATE database_default,
    Vendor nvarchar(30) COLLATE database_default,
    VendorName nvarchar(100) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    OutlineNumber varchar(255) COLLATE database_default,
    BaselineCost decimal(19,4),
    BaselineBill decimal(19,4)
    UNIQUE(PlanID, TaskID, ConsultantID, OutlineNumber, Account, Vendor)
  )

  DECLARE @tabCATypeDescriptions TABLE (
    Type smallint,
    Description nvarchar(200) COLLATE database_default
    UNIQUE(Type, Description)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get UICultureName.

  SELECT @strUICultureName = dbo.FW_GetActiveCultureName()

  -- Load Chart of Account (CA) Type Descriptions for the given UICultureName.

  INSERT @tabCATypeDescriptions(
    Type,
    Description
  )
    SELECT
      CONVERT(smallint, LEFT(Code, CHARINDEX('-', Code) - 1)) AS Type, 
      COALESCE(Description, TNT.LocalizedValue) AS Description
      FROM CFGCubeTranslationDescriptions AS CTD
        LEFT JOIN FW_TextNotTranslated AS TNT ON TNT.UICultureName = @strUICultureName
      WHERE CTD.Usage = 'AccountType' AND CTD.Code NOT LIKE '%Unknown' AND CTD.UICultureName = @strUICultureName

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length
    FROM CFGFormat

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Get RM Settings.
  
  SELECT 
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE())))
    FROM CFGRMSettings

  SELECT @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)
  
  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. For an Expense row, @strRowID = '<PNConsultant.Account>➔<PNConsultant.Vendor>|<RPTask.TaskID>
  --   2. For an Expense row, @strRowID = '<PNConsultant.Account>➔|<RPTask.TaskID>
  --   3. For a WBS row, @strRowID = '|<PNTask.TaskID>'
  --   4. For a WBS row, @strRowID = '|<PR.WBS1>§<PR.WBS2>§<PR.WBS3>'

  SET @strInputType = 
    CASE  
      WHEN CHARINDEX(@_SectionSign, @strRowID) > 0
      THEN 'W'
      ELSE 'T'
    END

  IF (@strInputType ='T')
    BEGIN

      -- Determining Account, Vendor and TaskID.

      SET @intAccountVendorIndex = CHARINDEX(@_AccountVendorSign,@strRowID)
      SET @intRowIDSeperatorIndex = CHARINDEX('|',@strRowID)

      IF @intAccountVendorIndex > 0
        BEGIN
          SET @strAccount = LEFT(@strRowID,@intAccountVendorIndex - 1)
        END

      SET @strVendor =
        CASE
          WHEN @intRowIDSeperatorIndex = @intAccountVendorIndex + 1
          THEN NULL
          ELSE SUBSTRING(@strRowID,@intAccountVendorIndex + 1,@intRowIDSeperatorIndex - @intAccountVendorIndex-1)
        END
      SET @strTaskID = SUBSTRING(@strRowID,@intRowIDSeperatorIndex + 1,LEN(@strRowID) - @intRowIDSeperatorIndex)

      -- Setting various Plan parameter.
      -- Get MIN Consultant Level before loading WBS structure.
      -- We only need it for Projects with Plans

      SELECT 
        @strCompany = P.Company,
        @strPlanID = PT.PlanID,
        @strWBS1 = PT.WBS1,
        @strWBS2 = ISNULL(PT.WBS2, ' '),
        @strWBS3 = ISNULL(PT.WBS3, ' '),
        @strTaskStatus = PT.Status,
        @strTopWBS1 = MIN(ISNULL(TT.WBS1, '')),
        @strTopName = MIN(ISNULL(TT.Name, '')),
        @tiMinConWBSLevel = P.ConWBSLevel,
        @bitIsLeafTask =
          CASE
            WHEN PT.OutlineLevel < (P.ConWBSLevel - 1)
            THEN
              CASE
                WHEN COUNT(CT.TaskID) > 0
                THEN 0
                ELSE 1
              END
            ELSE 1
          END
        FROM PNTask AS PT 
          LEFT JOIN PNTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber AND CT.WBSType <> 'LBCD'
          LEFT JOIN PNTask AS TT ON PT.PlanID = TT.PlanID AND TT.WBSType = 'WBS1' AND TT.WBS1 <> '<none>'
          INNER JOIN PNPlan AS P ON PT.PlanID = P.PlanID
        WHERE PT.TaskID = @strTaskID
        GROUP BY PT.PlanID, PT.TaskID, PT.Status, PT.WBS1, PT.WBS2, PT.WBS3, PT.OutlineLevel, P.Company, P.ConWBSLevel

    END
  ELSE IF (@strInputType = 'W')
    BEGIN

      SET @strWBS1WBS2WBS3 = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

      -- Get the current date in current local and not the UTC date.
  
      SET @dtToday = DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)
      SET @dtTomorrow = DATEADD(dd, 1, @dtToday)  

      -- Get various Plan parameters.

      SELECT 
        @strCompany = 
          CASE 
            WHEN @strMultiCompanyEnabled = 'Y' 
            THEN ISNULL(SUBSTRING(TP.Org, @siOrg1Start, @siOrg1Length), ' ') 
            ELSE ' ' 
          END,
        @strPlanID = NULL,
        @strTaskID = NULL,
        @strWBS1 = P.WBS1,
        @strWBS2 = P.WBS2,
        @strWBS3 = P.WBS3,
        @strTaskStatus = P.Status,
        @strTopWBS1 = ISNULL(TP.WBS1, ''),
        @strTopName = ISNULL(TP.Name, ''),
        @dtWBS1MinDate = COALESCE(TP.StartDate, TP.EstCompletionDate, @dtTomorrow),
        @bitIsLeafTask =
          CASE
            WHEN P.SubLevel = 'Y'
            THEN 0
            ELSE 1
          END
        FROM PR AS P
          LEFT JOIN PR AS TP
            ON TP.WBS1 = P.WBS1 AND TP.WBS2 = ' ' AND TP.WBS3 = ' '
        WHERE P.WBS1 + @_SectionSign + P.WBS2 + @_SectionSign + P.WBS3 = @strWBS1WBS2WBS3

    END /* END ELSE IF (@strInputType IN ('W')) */

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Get decimal settings.
  
  SELECT 
    @intAmtCostDecimals = AmtCostDecimals,
    @intAmtBillDecimals = AmtBillDecimals
    FROM dbo.stRP$tabPlanDecimals(@strPlanID)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Build WBS Structure.

  -- If the input Project currently has no Plan then save the entire Project away into @tabWBS.
  -- Otherwise, save away PNTask rows into @tabWBS.
      
  IF (@strInputType = 'W')
    BEGIN

      -- WBS1 Row.

      INSERT @tabWBS(
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        WBS1WBS2WBS3,
        Name,
        ParentOutlineNumber,
        OutlineNumber,
        NonLBCDChildrenCount,
        OutlineLevel,
        Org,
        ClientID,
        ProjMgr,
    UtilizationScheduleFlg,
        StartDate,
        EndDate,
        Status,
        HasNotes,
        ContractStartDate,
        ContractEndDate,
        BaselineExpCost,
        BaselineExpBill,
        FeeCost,
        FeeBill,
        FeeDirCost,
        FeeDirBill,
        FeeReimCost,
        FeeReimBill
      )
        SELECT
          X.PlanID AS PlanID,
          X.TaskID AS TaskID,
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.WBS1WBS2WBS3 AS WBS1WBS2WBS3,
          X.Name AS Name,
          X.ParentOutlineNumber AS ParentOutlineNumber,
          X.OutlineNumber AS OutlineNumber,
          X.NonLBCDChildrenCount AS NonLBCDChildrenCount,
          X.OutlineLevel AS OutlineLevel,
          X.Org AS Org,
          X.ClientID AS ClientID,
          X.ProjMgr AS ProjMgr,
      X.UtilizationScheduleFlg As UtilizationScheduleFlg,
          X.StartDate AS StartDate,
          X.EndDate AS EndDate,
          X.Status AS Status,
          'N' AS HasNotes,
          X.ContractStartDate AS ContractStartDate,
          X.ContractEndDate AS ContractEndDate,
          X.BaselineExpCost AS BaselineExpCost,
          X.BaselineExpBill AS BaselineExpBill,
          (X.FeeDirCost + X.FeeReimCost) AS FeeCost,
          (X.FeeDirBill + X.FeeReimBill) AS FeeBill,
          X.FeeDirCost AS FeeDirCost,
          X.FeeDirBill AS FeeDirBill,
          X.FeeReimCost AS FeeReimCost,
          X.FeeReimBill AS FeeReimBill
          FROM (
            SELECT
              NULL AS PlanID,
              NULL AS TaskID,
              PR.WBS1 AS WBS1,
              PR.WBS2 AS WBS2,
              PR.WBS3 AS WBS3,
              PR.WBS1 + @_SectionSign + PR.WBS2 + @_SectionSign + PR.WBS3 AS WBS1WBS2WBS3,
              PR.Name AS Name,
              NULL AS ParentOutlineNumber,
              '001' AS OutlineNumber,
              (SELECT COUNT(*) FROM PR WHERE WBS1 = @strWBS1 AND WBS2 <> ' ' AND WBS3 = ' ') AS NonLBCDChildrenCount,
              0 AS OutlineLevel,
              PR.Org AS Org,
              PR.ClientID AS ClientID,
              PR.ProjMgr AS ProjMgr,
        PR.UtilizationScheduleFlg AS UtilizationScheduleFlg,
              COALESCE(PR.StartDate, @dtWBS1MinDate) AS StartDate,
              CASE
                WHEN (PR.EndDate IS NOT NULL AND PR.EndDate >= COALESCE(PR.StartDate, @dtWBS1MinDate))
                THEN PR.EndDate
                ELSE COALESCE(PR.StartDate, @dtWBS1MinDate)
              END AS EndDate,
              PR.Status AS Status,
              PR.StartDate AS ContractStartDate,
              COALESCE(PR.EndDate, PR.ActCompletionDate, PR.EstCompletionDate) AS ContractEndDate,
              0 AS BaselineExpCost,
              0 AS BaselineExpBill,
              ConsultFee AS FeeDirCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN ConsultFeeBillingCurrency ELSE ConsultFee END) AS FeeDirBill,
              ReimbAllowCons AS FeeReimCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN ReimbAllowConsBillingCurrency ELSE ReimbAllowCons END) AS FeeReimBill
              FROM PR
                LEFT JOIN PRAdditionalData AS PAD ON PR.WBS1 = PAD.WBS1 AND PR.WBS2 = PAD.WBS2 AND PR.WBS3 = PAD.WBS3
              WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          ) AS X 

      -- WBS2 Rows.

      INSERT @tabWBS (
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        WBS1WBS2WBS3,
        Name,
        ParentOutlineNumber,
        OutlineNumber,
        NonLBCDChildrenCount,
        OutlineLevel,
        Org,
        ClientID,
        ProjMgr,
    UtilizationScheduleFlg,
        StartDate,
        EndDate,
        Status,
        HasNotes,
        ContractStartDate,
        ContractEndDate,
        BaselineExpCost,
        BaselineExpBill,
        FeeCost,
        FeeBill,
        FeeDirCost,
        FeeDirBill,
        FeeReimCost,
        FeeReimBill
      )
        SELECT
          NULL AS PlanID,
          NULL AS TaskID,
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.WBS1 + @_SectionSign + X.WBS2 + @_SectionSign + X.WBS3 AS WBS1WBS2WBS3,
          X.Name,
          '001' AS ParentOutlineNumber,
          '001' + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
          (SELECT COUNT(*) FROM PR WHERE WBS1 = @strWBS1 AND WBS2 = X.WBS2 AND WBS3 <> ' ') AS NonLBCDChildrenCount,
          1 AS OutlineLevel,
          X.Org,
          X.ClientID,
          X.ProjMgr,
      X.UtilizationScheduleFlg AS UtilizationScheduleFlg,
          X.StartDate,
          X.EndDate,
          X.Status,
          'N' AS HasNotes,
          X.ContractStartDate AS ContractStartDate,
          X.ContractEndDate AS ContractEndDate,
          0 AS BaselineLabCost,
          0 AS BaselineLabBill,
          (X.FeeDirCost + X.FeeReimCost) AS FeeCost,
          (X.FeeDirBill + X.FeeReimBill) AS FeeBill,
          X.FeeDirCost AS FeeDirCost,
          X.FeeDirBill AS FeeDirBill,
          X.FeeReimCost AS FeeReimCost,
          X.FeeReimBill AS FeeReimBill
          FROM (
            SELECT
              PR.WBS1 AS WBS1,
              PR.WBS2 AS WBS2,
              PR.WBS3 AS WBS3,
              PR.Name AS Name,
              PR.Org AS Org,
              PR.ClientID AS ClientID,
              PR.ProjMgr AS ProjMgr,
        PR.UtilizationScheduleFlg As UtilizationScheduleFlg,
              COALESCE(PR.StartDate, @dtWBS1MinDate) AS StartDate,
              CASE
                WHEN (PR.EndDate IS NOT NULL AND PR.EndDate >= COALESCE(PR.StartDate, @dtWBS1MinDate))
                THEN PR.EndDate
                ELSE COALESCE(PR.StartDate, @dtWBS1MinDate)
              END AS EndDate,
              PR.Status AS Status,
              PR.StartDate AS ContractStartDate,
              COALESCE(PR.EndDate, PR.ActCompletionDate, PR.EstCompletionDate) AS ContractEndDate,
              ConsultFee AS FeeDirCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN ConsultFeeBillingCurrency ELSE ConsultFee END) AS FeeDirBill,
              ReimbAllowCons AS FeeReimCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN ReimbAllowConsBillingCurrency ELSE ReimbAllowCons END) AS FeeReimBill,
              ROW_NUMBER() OVER (PARTITION BY PR.WBS1 ORDER BY PR.WBS1, PR.WBS2) AS RowID
              FROM PR 
                LEFT JOIN PRAdditionalData AS PAD ON PR.WBS1 = PAD.WBS1 AND PR.WBS2 = PAD.WBS2 AND PR.WBS3 = PAD.WBS3
              WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 <> ' ' AND PR.WBS3 = ' '
          ) AS X

      -- WBS3 Rows.

      INSERT @tabWBS (
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        WBS1WBS2WBS3,
        Name,
        ParentOutlineNumber,
        OutlineNumber,
        NonLBCDChildrenCount,
        OutlineLevel,
        Org,
        ClientID,
        ProjMgr,
    UtilizationScheduleFlg,
        StartDate,
        EndDate,
        Status,
        HasNotes,
        ContractStartDate,
        ContractEndDate,
        BaselineExpCost,
        BaselineExpBill,
        FeeCost,
        FeeBill,
        FeeDirCost,
        FeeDirBill,
        FeeReimCost,
        FeeReimBill
      )
        SELECT
          NULL AS PlanID,
          NULL AS TaskID,
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.WBS1 + @_SectionSign + X.WBS2 + @_SectionSign + X.WBS3 AS WBS1WBS2WBS3,
          X.Name,
          PX.OutlineNumber AS ParentOutlineNumber,
          PX.OutlineNumber + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(X.RowID, 36)), 3) AS OutlineNumber,
          0 AS NonLBCDChildrenCount,
          PX.OutlineLevel + 1 AS OutlineLevel,
          X.Org,
          X.ClientID,
          X.ProjMgr,
      X.UtilizationScheduleFlg AS UtilizationScheduleFlg,
          X.StartDate,
          X.EndDate,
          X.Status,
          'N' AS HasNotes,
          X.ContractStartDate AS ContractStartDate,
          X.ContractEndDate AS ContractEndDate,
          0 AS BaselineLabCost,
          0 AS BaselineLabBill,
          (X.FeeDirCost + X.FeeReimCost) AS FeeCost,
          (X.FeeDirBill + X.FeeReimBill) AS FeeBill,
          X.FeeDirCost AS FeeDirCost,
          X.FeeDirBill AS FeeDirBill,
          X.FeeReimCost AS FeeReimCost,
          X.FeeReimBill AS FeeReimBill
          FROM (
            SELECT
              PR.WBS1 AS WBS1,
              PR.WBS2 AS WBS2,
              PR.WBS3 AS WBS3,
              PR.Name AS Name,
              PR.Org AS Org,
              PR.ClientID AS ClientID,
              PR.ProjMgr AS ProjMgr,
        PR.UtilizationScheduleFlg AS UtilizationScheduleFlg,
              COALESCE(PR.StartDate, @dtWBS1MinDate) AS StartDate,
              CASE
                WHEN (PR.EndDate IS NOT NULL AND PR.EndDate >= COALESCE(PR.StartDate, @dtWBS1MinDate))
                THEN PR.EndDate
                ELSE COALESCE(PR.StartDate, @dtWBS1MinDate)
              END AS EndDate,
              PR.Status AS Status,
              PR.StartDate AS ContractStartDate,
              COALESCE(PR.EndDate, PR.ActCompletionDate, PR.EstCompletionDate) AS ContractEndDate,
              ConsultFee AS FeeDirCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN ConsultFeeBillingCurrency ELSE ConsultFee END) AS FeeDirBill,
              ReimbAllowCons AS FeeReimCost,
              (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN ReimbAllowConsBillingCurrency ELSE ReimbAllowCons END) AS FeeReimBill,
              ROW_NUMBER() OVER (PARTITION BY PR.WBS2 ORDER BY PR.WBS1, PR.WBS2, PR.WBS3) AS RowID
              FROM PR 
                LEFT JOIN PRAdditionalData AS PAD ON PR.WBS1 = PAD.WBS1 AND PR.WBS2 = PAD.WBS2 AND PR.WBS3 = PAD.WBS3
              WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 <> ' ' AND PR.WBS3 <> ' '
          ) AS X
            INNER JOIN @tabWBS AS PX ON X.WBS1 = PX.WBS1 AND X.WBS2 = PX.WBS2 AND PX.WBS3 = ' '
        
    END /* END IF (@strInputType = 'W') */
        
  ELSE IF (@strInputType = 'T')
    BEGIN
        
      INSERT @tabWBS(
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        WBS1WBS2WBS3,
        Name,
        ParentOutlineNumber,
        OutlineNumber,
        NonLBCDChildrenCount,
        OutlineLevel,
        Org,
        ClientID,
        ProjMgr,
    UtilizationScheduleFlg,
        StartDate,
        EndDate,
        Status,
        HasNotes,
        ContractStartDate,
        ContractEndDate,
        BaselineExpCost,
        BaselineExpBill,
        FeeCost,
        FeeBill,
        FeeDirCost,
        FeeDirBill,
        FeeReimCost,
        FeeReimBill
      )
        SELECT
          X.PlanID AS PlanID,
          X.TaskID AS TaskID,
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.WBS1WBS2WBS3 AS WBS1WBS2WBS3,
          X.Name AS Name,
          X.ParentOutlineNumber AS ParentOutlineNumber,
          X.OutlineNumber AS OutlineNumber,
          X.NonLBCDChildrenCount AS NonLBCDChildrenCount,
          X.OutlineLevel AS OutlineLevel,
          X.Org AS Org,
          X.ClientID AS ClientID,
          X.ProjMgr AS ProjMgr,
          X.UtilizationScheduleFlg AS UtilizationScheduleFlg,
      X.StartDate AS StartDate,
          X.EndDate AS EndDate,
          X.Status AS Status,
          X.HasNotes AS HasNotes,
          X.ContractStartDate AS ContractStartDate,
          X.ContractEndDate AS ContractEndDate,
          X.BaselineExpCost AS BaselineExpCost,
          X.BaselineExpBill AS BaselineExpBill,
          (X.FeeDirCost + X.FeeReimCost) AS FeeCost,
          (X.FeeDirBill + X.FeeReimBill) AS FeeBill,
          X.FeeDirCost AS FeeDirCost,
          X.FeeDirBill AS FeeDirBill,
          X.FeeReimCost AS FeeReimCost,
          X.FeeReimBill AS FeeReimBill
          FROM (
            SELECT
              T.PlanID AS PlanID,
              T.TaskID AS TaskID,
              T.WBS1 AS WBS1,
              ISNULL(T.WBS2, ' ') AS WBS2,
              ISNULL(T.WBS3, ' ') AS WBS3,
              T.WBS1 + @_SectionSign + ISNULL(T.WBS2, ' ') + @_SectionSign + ISNULL(T.WBS3, ' ') AS WBS1WBS2WBS3,
              T.Name AS Name,
              T.ParentOutlineNumber AS ParentOutlineNumber,
              T.OutlineNumber AS OutlineNumber,
              (SELECT COUNT(*) FROM PNTask AS XT WHERE XT.PlanID = T.PlanID AND XT.ParentOutlineNumber = T.OutlineNUmber AND XT.WBSType <> 'LBCD') AS NonLBCDChildrenCount,
              T.OutlineLevel AS OutlineLevel,
              T.Org AS Org,
              T.ClientID AS ClientID,
              T.ProjMgr AS ProjMgr,
          PR.UtilizationScheduleFlg AS UtilizationScheduleFlg,
              T.StartDate AS StartDate,
              T.EndDate AS EndDate,
              T.Status AS Status,
              CASE WHEN T.Notes IS NOT NULL AND T.Notes <> '' THEN 'Y' ELSE 'N' END AS HasNotes,
              PR.StartDate AS ContractStartDate,
              COALESCE(PR.EndDate, PR.ActCompletionDate, PR.EstCompletionDate) AS ContractEndDate,
              T.BaselineExpCost AS BaselineExpCost,
              T.BaselineExpBill AS BaselineExpBill,              
              COALESCE(PR.ConsultFee, T.ConsultantFee, 0) AS FeeDirCost,
              CASE 
                WHEN @strReportAtBillingInBillingCurr = 'Y' 
                THEN COALESCE(PR.ConsultFeeBillingCurrency, T.ConsultantFeeBill, 0) 
                ELSE COALESCE(PR.ConsultFee, T.ConsultantFee, 0) 
              END AS FeeDirBill,
              COALESCE(PR.ReimbAllowCons, T.ReimbAllowanceCon, 0) AS FeeReimCost,
              CASE 
                WHEN @strReportAtBillingInBillingCurr = 'Y' 
                THEN COALESCE(PR.ReimbAllowConsBillingCurrency, T.ReimbAllowanceConBill, 0) 
                ELSE COALESCE(PR.ReimbAllowCons, T.ReimbAllowanceCon, 0) 
              END AS FeeReimBill,
              (T.OutlineLevel + 1) AS WBSLevel
              FROM PNTask AS T
                LEFT JOIN PR
                  ON T.WBS1 = PR.WBS1 AND ISNULL(T.WBS2, ' ') = PR.WBS2 AND ISNULL(T.WBS3, ' ') = PR.WBS3
              WHERE T.PlanID = @strPlanID AND T.WBSType <> 'LBCD'
          ) AS X
        WHERE X.WBSLevel <= @tiMinConWBSLevel

    END /* END ELSE IF (@strInputType = 'T') */

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Determine ParentRowID based on @strMode, @strInputType, @strResourceType.

  SELECT
    @strParentRowID = 
      CASE 
        WHEN @strMode = 'C' THEN @strRowID
        WHEN @strMode = 'S' THEN 
          CASE 
            WHEN (@strInputType = 'T' AND DATALENGTH(@strAccount) > 0) 
              THEN ISNULL(('|' + CT.TaskID), '')
            WHEN (@strInputType = 'T' AND DATALENGTH(@strAccount) = 0) 
              THEN ISNULL(('|' + PT.TaskID), '')
            WHEN (@strInputType = 'W' AND DATALENGTH(@strAccount) > 0) 
              THEN ISNULL(('|' + CT.WBS1 + @_SectionSign + CT.WBS2 + @_SectionSign + CT.WBS3), '')
            WHEN (@strInputType = 'W' AND DATALENGTH(@strAccount) = 0) 
              THEN ISNULL(('|' + PT.WBS1 + @_SectionSign + PT.WBS2 + @_SectionSign + PT.WBS3), '')
          END
      END 
    FROM @tabWBS AS CT
      LEFT JOIN @tabWBS AS PT ON PT.PlanID = CT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber
    WHERE CT.TaskID = @strTaskID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collect Expenses and Un-Planned Resources.

  IF (@strMode = 'C')
    BEGIN

      -- @strRowID is pointing to a Leaf WBS row.

      INSERT @tabConsultant(
        PlanID,
        TaskID,
        ConsultantID,
        Account,
        AccountName,
        AccountTypeCode,
        AccountType,
        Vendor,
        VendorName,
        StartDate,
        EndDate,
        OutlineNumber,
        BaselineCost,
        BaselineBill
      )
        SELECT DISTINCT
          A.PlanID AS PlanID,
          A.TaskID AS TaskID,
          A.ConsultantID AS ConsultantID,
          A.Account AS Account,
          CA.Name AS AccountName,
          CA.Type AS AccountTypeCode,
          CATD.Description AS AccountType,
          A.Vendor AS Vendor,
          VE.Name AS VendorName,
          A.StartDate AS StartDate,
          A.EndDate AS EndDate,
          AT.OutlineNumber AS OutlineNumber,
          A.BaselineConCost AS BaselineCost,
          A.BaselineConBill AS BaselineBill
          FROM PNConsultant AS A
            INNER JOIN PNTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
            INNER JOIN PNTask AS PT ON A.PlanID = PT.PlanID AND AT.OutlineNumber LIKE PT.OutlineNumber + '%'
            INNER JOIN CA ON CA.Account = A.Account
            LEFT JOIN PNTask AS CT ON A.PlanID = CT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber AND AT.OutlineNumber LIKE CT.OutlineNumber + '%'
            LEFT JOIN VE ON A.Vendor = VE.Vendor
            LEFT JOIN @tabCATypeDescriptions AS CATD ON CA.Type = CATD.Type
          WHERE
            A.PlanID = @strPlanID AND PT.TaskID = @strTaskID

    END /* END IF (@strMode = 'C') */

  ELSE IF (@strMode = 'S')
    BEGIN

      -- @strRowID is pointing to an Expense row.
      
      INSERT @tabConsultant(
        PlanID,
        TaskID,
        ConsultantID,
        Account,
        AccountName,
        AccountTypeCode,
        AccountType,
        Vendor,
        VendorName,
        StartDate,
        EndDate,
        OutlineNumber,
        BaselineCost,
        BaselineBill
      )
        SELECT DISTINCT
          A.PlanID AS PlanID,
          A.TaskID AS TaskID,
          A.ConsultantID AS ConsultantID,
          CA.Account AS Account,
          CA.Name AS AccountName,
          CA.Type AS AccountTypeCode,
          CATD.Description AS AccountType,
          VE.Vendor AS Vendor,
          VE.Name AS VendorName,
          A.StartDate AS StartDate,
          A.EndDate AS EndDate,
          AT.OutlineNumber AS OutlineNumber,
          A.BaselineConCost AS BaselineCost,
          A.BaselineConBill AS BaselineBill
          FROM PNConsultant AS A
            INNER JOIN PNTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
            INNER JOIN PNTask AS PT ON A.PlanID = PT.PlanID AND AT.OutlineNumber LIKE PT.OutlineNumber + '%'
            INNER JOIN CA ON CA.Account = A.Account
            LEFT JOIN VE ON A.Vendor = VE.Vendor
            LEFT JOIN @tabCATypeDescriptions AS CATD ON CA.Type = CATD.Type
          WHERE
            A.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
            ((@strAccount IS NOT NULL AND A.Account = @strAccount AND
            ISNULL(A.Vendor, '|') = ISNULL(@strVendor, '|'))
            OR @strAccount IS NULL)

    END /* END ELSE IF (@strMode = 'S') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  IF ((@strMode = 'C' AND @bitIsLeafTask = 1) OR (@strMode = 'S' AND DATALENGTH(@strAccount) > 0))
    BEGIN /* BEGIN Assignment Rows */

      -- Insert final result.

      INSERT @tabConsultants(
        RowID,
        ParentRowID,
        PlanID,
        TaskID,
        ConsultantID,
        Account,
        AccountName,
        AccountTypeCode,
        AccountType,
        Vendor,
        VendorName,
        ParentOutlineNumber,
        OutlineNumber,
        OutlineLevel,
        RowLevel,
        Name,
        StartDate,
        EndDate,
        MinASGDate,
        MaxASGDate,
        WBS1,
        WBS2,
        WBS3,
        Org,
        OrgName,
        ProjMgr,
    UtilizationScheduleFlg,
        PMFullName,
        ClientName,
        RT_Status,
        TopWBS1,
        TopName,
        HasNotes,
        HasAssignments,
        HasChildren,
        LeafNode,
        
        ContractStartDate,
        ContractEndDate,

        BaselineCost,
        BaselineBill,

        JTDCost,
        JTDReimCost,
        JTDBill,
        JTDReimBill,

        ContractCost,
        ContractDirCost,
        ContractReimCost,
        ContractBill,
        ContractDirBill,
        ContractReimBill,

        PlannedCost,
        PlannedReimCost,
        PlannedBill,
        PlannedReimBill,

        ETCCost,
        ETCBill,
        EACCost,
        EACBill,
        MarkupCost,
        ContractLessJTDBill,

        CalcPctComplCost,
        CalcPctComplBill,
        PlannedLessJTDCost,
        PlannedLessJTDBill,
        PlannedLessEACCost,
        PlannedLessEACBill,
        ContractLessEACCost,
        ContractLessEACBill,
        ContractLessJTDCost
      )
        SELECT
          XT.Account + @_AccountVendorSign + ISNULL(XT.Vendor,'') + 
          '|' + 
          CASE 
            WHEN @strInputType = 'T' THEN XT.TaskID
            WHEN @strInputType = 'W' THEN XT.WBS1WBS2WBS3
          END AS RowID,
          @strParentRowID AS ParentRowID,
          @strPlanID AS PlanID,
          XT.TaskID AS TaskID,
          XT.ConsultantID AS ConsultantID,
          XT.Account AS Account,
          XT.AccountName AS AccountName,
          XT.AccountTypeCode AS AccountTypeCode,
          XT.AccountType AS AccountType,
          XT.Vendor AS Vendor,
          XT.VendorName AS VendorName,
          XT.ParentOutlineNumber AS ParentOutlineNumber,
          XT.OutlineNumber AS OutlineNumber,
          XT.OutlineLevel AS OutlineLevel,
          XT.OutlineLevel + 1 AS RowLevel,
          XT.AccountName AS Name,
          PlanStart AS StartDate,
          PlanEnd AS EndDate,
          XT.StartDate AS MinASGDate,
          XT.EndDate AS MaxASGDate,
          ISNULL(XT.WBS1, '') AS WBS1,
          ISNULL(XT.WBS2, '') AS WBS2,
          ISNULL(XT.WBS3, '') AS WBS3,
          ISNULL(XT.Org, '') AS Org,
          ISNULL(O.Name, '') AS OrgName,
          ISNULL(XT.ProjMgr, '') AS ProjMgr,
      XT.UtilizationScheduleFlg AS UtilizationScheduleFlg,
          CONVERT(nvarchar(255), ISNULL(ISNULL(PM.PreferredName, PM.FirstName), '') + ISNULL((' ' + PM.LastName), '') + ISNULL(', ' + PMSuffix.Suffix, '')) AS PMFullName,
          ISNULL(CL.Name, '') AS ClientName,
          '' AS RT_Status,
          @strTopWBS1,
          @strTopName,
          'N' AS HasNotes,
          0 AS HasAssignments,
          0 AS HasChildren,
          1 AS LeafNode,

          NULL AS ContractStartDate,
          NULL AS ContractEndDate,

          XT.BaselineCost AS BaselineCost,
          XT.BaselineBill AS BaselineBill,

          XT.JTDCost AS JTDCost,
          XT.JTDReimCost AS JTDReimCost,
          XT.JTDBill AS JTDBill,
          XT.JTDReimBill AS JTDReimBill,

          XT.ContractCost AS ContractCost,
          XT.ContractDirCost AS ContractDirCost,
          XT.ContractReimCost AS ContractReimCost,
          XT.ContractBill AS ContractBill,
          XT.ContractDirBill AS ContractDirBill,
          XT.ContractReimBill AS ContractReimBill,

          0 AS PlannedCost,
          0 AS PlannedReimCost,
          0 AS PlannedBill,
          0 AS PlannedReimBill,

          0 AS ETCCost,
          0 AS ETCBill,
          0 AS EACCost,
          0 AS EACBill,
          0 AS MarkupCost,
          0 AS ContractLessJTDBill,

          0 AS CalcPctComplCost,
          0 AS CalcPctComplBill,
          0 AS PlannedLessJTDCost,
          0 AS PlannedLessJTDBill,
          0 AS PlannedLessEACCost,
          0 AS PlannedLessEACBill,
          0 AS ContractLessEACCost,
          0 AS ContractLessEACBill,
          0 AS ContractLessJTDCost
        FROM (
          SELECT
            T.PlanID AS PlanID,
            T.TaskID AS TaskID,
            T.ParentOutlineNumber AS ParentOutlineNumber,
            T.OutlineNumber AS OutlineNumber,
            T.OutlineLevel AS OutlineLevel,
            T.NonLBCDChildrenCount AS NonLBCDChildrenCount,
            T.Name AS Name,
            T.WBS1WBS2WBS3 AS WBS1WBS2WBS3,
            T.WBS1 AS WBS1,
            T.WBS2 AS WBS2,
            T.WBS3 AS WBS3,
            T.Org AS Org,
            T.ClientID AS ClientID,
            T.ProjMgr AS ProjMgr,
            T.UtilizationScheduleFlg AS UtilizationScheduleFlg,
        T.StartDate AS PlanStart,
            T.EndDate AS PlanEnd,
            A.StartDate AS StartDate,
            A.EndDate AS EndDate,
            A.ConsultantID AS ConsultantID,
            A.Account AS Account,
            A.AccountName AS AccountName,
            A.AccountTypeCode AS AccountTypeCode,
            A.AccountType AS AccountType,
            A.Vendor AS Vendor,
            A.VendorName AS VendorName,
            ROUND(ISNULL(A.BaselineCost, 0.0000), @intAmtCostDecimals) AS BaselineCost,
            ROUND(ISNULL(A.BaselineBill, 0.0000), @intAmtBillDecimals) AS BaselineBill,
            0 AS JTDCost,
            0 AS JTDReimCost,
            0 AS JTDBill,
            0 AS JTDReimBill,
            ROUND(ISNULL(T.FeeCost, 0.0000), @intAmtCostDecimals) AS ContractCost,
            ROUND(ISNULL(T.FeeDirCost, 0.0000), @intAmtCostDecimals) AS ContractDirCost,
            ROUND(ISNULL(T.FeeReimCost, 0.0000), @intAmtCostDecimals) AS ContractReimCost,
            ROUND(ISNULL(T.FeeBill, 0.0000), @intAmtBillDecimals) AS ContractBill,
            ROUND(ISNULL(T.FeeDirBill, 0.0000), @intAmtBillDecimals) AS ContractDirBill,
            ROUND(ISNULL(T.FeeReimBill, 0.0000), @intAmtBillDecimals) AS ContractReimBill
            FROM @tabWBS AS T
              INNER JOIN @tabConsultant A ON A.OutlineNumber = T.OutlineNumber              
            WHERE ISNULL(T.Status, '') LIKE CASE WHEN @bActiveWBSOnly = 1 THEN 'A' ELSE '%' END
        ) AS XT
        LEFT JOIN CL ON XT.ClientID = CL.ClientID
        LEFT JOIN EM AS PM ON XT.ProjMgr = PM.Employee
    LEFT JOIN CFGSuffix PMSuffix ON PMSuffix.Code = PM.Suffix
        LEFT JOIN Organization AS O ON XT.Org = O.Org
        LEFT JOIN PNPlan AS P ON P.PlanID = XT.PlanID

    END /* END Expense Rows */

  ELSE /* WBS Rows */
    BEGIN /* BEGIN WBS Rows */

      -- Insert final result for WBS rows.

      IF (@strMode = 'S')
        BEGIN

          INSERT @tabConsultants(
            RowID,
            ParentRowID,
            PlanID,
            TaskID,
            ConsultantID,
            Account,
            AccountName,
            AccountTypeCode,
            AccountType,
            Vendor,
            VendorName,
            ParentOutlineNumber,
            OutlineNumber,
            OutlineLevel,
            RowLevel,
            Name,
            StartDate,
            EndDate,
            MinASGDate,
            MaxASGDate,
            WBS1,
            WBS2,
            WBS3,
            Org,
            OrgName,
            ProjMgr,
      UtilizationScheduleFlg,
            PMFullName,
            ClientName,
            RT_Status,
            TopWBS1,
            TopName,
            HasNotes,
            HasAssignments,
            HasChildren,
            LeafNode,
        
            ContractStartDate,
            ContractEndDate,

            BaselineCost,
            BaselineBill,

            JTDCost,
            JTDReimCost,
            JTDBill,
            JTDReimBill,

            ContractCost,
            ContractDirCost,
            ContractReimCost,
            ContractBill,
            ContractDirBill,
            ContractReimBill,

            PlannedCost,
            PlannedReimCost,
            PlannedBill,
            PlannedReimBill,

            ETCCost,
            ETCBill,
            EACCost,
            EACBill,
            MarkupCost,
            ContractLessJTDBill,

            CalcPctComplCost,
            CalcPctComplBill,
            PlannedLessJTDCost,
            PlannedLessJTDBill,
            PlannedLessEACCost,
            PlannedLessEACBill,
            ContractLessEACCost,
            ContractLessEACBill,
            ContractLessJTDCost
          )
            SELECT
              '|' + 
              CASE 
                WHEN @strInputType = 'T' THEN YT.TaskID
                WHEN @strInputType = 'W' THEN YT.WBS1WBS2WBS3
              END AS RowID,
              @strParentRowID AS ParentRowID,
              YT.PlanID AS PlanID,
              YT.TaskID AS TaskID,
              NULL AS ConsultantID,
              NULL AS Account,
              NULL AS AccountName,
              NULL AS AccountTypeCode,
              NULL AS AccountType,
              NULL AS Vendor,
              NULL AS VendorName,
              YT.ParentOutlineNumber AS ParentOutlineNumber,
              YT.OutlineNumber AS OutlineNumber,
              YT.OutlineLevel AS OutlineLevel,
              YT.OutlineLevel AS RowLevel,
              YT.Name AS Name,
              YT.StartDate AS StartDate,
              YT.EndDate AS EndDate,
              YT.MinASGDate,
              YT.MaxASGDate,
              ISNULL(YT.WBS1, '') AS WBS1,
              ISNULL(YT.WBS2, '') AS WBS2,
              ISNULL(YT.WBS3, '') AS WBS3,
              ISNULL(YT.Org, '') AS Org,
              ISNULL(O.Name, '') AS OrgName,
              ISNULL(YT.ProjMgr, '') AS ProjMgr,
            YT.UtilizationScheduleFlg AS UtilizationScheduleFlg,
              CONVERT(nvarchar(255), ISNULL(ISNULL(PM.PreferredName, PM.FirstName), '') + ISNULL((' ' + PM.LastName), '') + ISNULL(', ' + PMSuffix.Suffix, '')) AS PMFullName,
              ISNULL(CL.Name, '') AS ClientName,
              ISNULL(YT.Status, '') AS RT_Status, 
              @strTopWBS1,
              @strTopName,
              YT.HasNotes AS HasNotes,
              CASE
                WHEN YT.MinConsultantID IS NOT NULL
                THEN CONVERT(bit, 1)
                ELSE CONVERT(bit, 0)
              END AS HasAssignments,
              CASE 
                WHEN YT.NonLBCDChildrenCount > 0 
                THEN CONVERT(bit, 1) 
                ELSE 
                  CASE 
                    WHEN EXISTS (SELECT 'X' FROM PNConsultant WHERE TaskID = YT.TaskID) 
                    THEN CONVERT(bit,1) 
                    ELSE CONVERT(bit, 0) 
                  END 
              END AS HasChildren,
              CASE WHEN YT.NonLBCDChildrenCount > 0 THEN CONVERT(bit, 0) ELSE CONVERT(bit,1) END AS LeafNode,
   
              YT.ContractStartDate AS ContractStartDate,
              YT.ContractEndDate AS ContractEndDate,

              YT.BaselineCost AS BaselineCost,
              YT.BaselineBill AS BaselineBill,

              YT.JTDCost AS JTDCost,
              YT.JTDReimCost AS JTDReimCost,
              YT.JTDBill AS JTDBill,
              YT.JTDReimBill AS JTDReimBill,

              YT.ContractCost AS ContractCost,
              YT.ContractDirCost AS ContractDirCost,
              YT.ContractReimCost AS ContractReimCost,
              YT.ContractBill AS ContractBill,
              YT.ContractDirBill AS ContractDirBill,
              YT.ContractReimBill AS ContractReimBill,

              0 AS PlannedCost,
              0 AS PlannedReimCost,
              0 AS PlannedBill,
              0 AS PlannedReimBill,

              0 AS ETCCost,
              0 AS ETCBill,
              0 AS EACCost,
              0 AS EACBill,
              0 AS MarkupCost,
              0 AS ContractLessJTDBill,

              0 AS CalcPctComplCost,
              0 AS CalcPctComplBill,
              0 AS PlannedLessJTDCost,
              0 AS PlannedLessJTDBill,
              0 AS PlannedLessEACCost,
              0 AS PlannedLessEACBill,
              0 AS ContractLessEACCost,
              0 AS ContractLessEACBill,
              0 AS ContractLessJTDCost
            FROM ( /* AS YT */
              SELECT
                XT.PlanID,
                XT.TaskID,
                XT.ParentOutlineNumber,
                XT.OutlineNumber,
                XT.OutlineLevel,
                XT.NonLBCDChildrenCount,
                XT.Name,
                XT.WBS1WBS2WBS3,
                XT.WBS1 AS WBS1,
                XT.WBS2 AS WBS2,
                XT.WBS3 AS WBS3,
                XT.Org,
                XT.ClientID,
                XT.ProjMgr,
                XT.UtilizationScheduleFlg AS UtilizationScheduleFlg,
                XT.StartDate,
                XT.EndDate,
                XT.Status,
                XT.HasNotes,
                XT.ContractStartDate AS ContractStartDate,
                XT.ContractEndDate AS ContractEndDate,
                XT.BaselineCost AS BaselineCost,
                XT.BaselineBill AS BaselineBill,
                XT.JTDCost AS JTDCost,
                XT.JTDReimCost AS JTDReimCost,
                XT.JTDBill AS JTDBill,
                XT.JTDReimBill AS JTDReimBill,
                XT.ContractCost AS ContractCost,
                XT.ContractDirCost AS ContractDirCost,
                XT.ContractReimCost AS ContractReimCost,
                XT.ContractBill AS ContractBill,
                XT.ContractDirBill AS ContractDirBill,
                XT.ContractReimBill AS ContractReimBill,
                XA.MinConsultantID AS MinConsultantID,
                XA.MinASGDate AS MinASGDate,
                XA.MaxASGDate AS MaxASGDate
                FROM ( /* AS XT */
                  SELECT
                    T.PlanID AS PlanID,
                    T.TaskID AS TaskID,
                    T.ParentOutlineNumber AS ParentOutlineNumber,
                    T.OutlineNumber AS OutlineNumber,
                    T.OutlineLevel AS OutlineLevel,
                    CASE WHEN T.OutlineLevel < @tiMinConWBSLevel - 1 THEN T.NonLBCDChildrenCount ELSE 0 END AS NonLBCDChildrenCount,
                    T.Name AS Name,
                    T.WBS1WBS2WBS3 AS WBS1WBS2WBS3,
                    T.WBS1 AS WBS1,
                    T.WBS2 AS WBS2,
                    T.WBS3 AS WBS3,
                    T.Org AS Org,
                    T.ClientID AS ClientID,
                    T.ProjMgr AS ProjMgr,
          T.UtilizationScheduleFlg AS UtilizationScheduleFlg,
                    T.StartDate AS StartDate,
                    T.EndDate AS EndDate,
                    ISNULL(T.Status, '') AS Status,
                    ISNULL(T.HasNotes, 'N') AS HasNotes,
                    T.ContractStartDate AS ContractStartDate,
                    T.ContractEndDate AS ContractEndDate,
                    ROUND(ISNULL(T.BaselineExpCost, 0.0000), @intAmtCostDecimals) AS BaselineCost,
                    ROUND(ISNULL(T.BaselineExpBill, 0.0000), @intAmtBillDecimals) AS BaselineBill,
                    0 AS JTDCost,
                    0 AS JTDReimCost,
                    0 AS JTDBill,
                    0 AS JTDReimBill,
                    ROUND(ISNULL(T.FeeCost, 0.0000), @intAmtCostDecimals) AS ContractCost,
                    ROUND(ISNULL(T.FeeDirCost, 0.0000), @intAmtCostDecimals) AS ContractDirCost,
                    ROUND(ISNULL(T.FeeReimCost, 0.0000), @intAmtCostDecimals) AS ContractReimCost,
                    ROUND(ISNULL(T.FeeBill, 0.0000), @intAmtBillDecimals) AS ContractBill,
                    ROUND(ISNULL(T.FeeDirBill, 0.0000), @intAmtBillDecimals) AS ContractDirBill,
                    ROUND(ISNULL(T.FeeReimBill, 0.0000), @intAmtBillDecimals) AS ContractReimBill
                    FROM @tabWBS AS T                      
                    WHERE T.WBS1 = @strWBS1 AND T.WBS2 = @strWBS2 AND T.WBS3 = @strWBS3 AND
                      ISNULL(T.Status, '') LIKE CASE WHEN @bActiveWBSOnly = 1 THEN 'A' ELSE '%' END
                    GROUP BY
                      T.PlanID, T.TaskID, T.ParentOutlineNumber, T.OutlineNumber, T.OutlineLevel, T.NonLBCDChildrenCount,
                      T.Name, T.WBS1WBS2WBS3, T.WBS1, T.WBS2, T.WBS3, T.Org, T.ClientID, T.ProjMgr, T.UtilizationScheduleFlg,T.StartDate, T.EndDate, T.Status, T.HasNotes,
                      T.ContractStartDate, T.ContractEndDate, 
                      T.BaselineExpCost, T.BaselineExpBill, T.FeeCost, T.FeeBill, T.FeeDirCost, T.FeeDirBill, T.FeeReimCost,
                      T.FeeReimBill
                ) AS XT
                LEFT JOIN (
                  SELECT
                    AT.PlanID AS PlanID,
                    AT.OutlineNumber AS OutlineNumber,
                    MIN(A.ConsultantID) AS MinConsultantID,
                    MIN(A.StartDate) AS MinASGDate,
                    MAX(A.EndDate) AS MaxASGDate
                    FROM @tabWBS AS AT
                      LEFT JOIN @tabConsultant A ON AT.PlanID = A.PlanID AND A.OutlineNumber LIKE AT.OutlineNumber + '%'
                    GROUP BY AT.PlanID, AT.OutlineNumber
                ) AS XA ON XA.PlanID = XT.PlanID AND XA.OutlineNumber = XT.OutlineNumber
            ) AS YT
              LEFT JOIN CL ON YT.ClientID = CL.ClientID
              LEFT JOIN EM AS PM ON YT.ProjMgr = PM.Employee
        LEFT JOIN CFGSuffix PMSuffix ON PMSuffix.Code = PM.Suffix
              LEFT JOIN Organization AS O ON YT.Org = O.Org
              LEFT JOIN PNPlan AS P ON P.PlanID = YT.PlanID
   
        END /* END IF (@strMode = 'S') */

      ELSE IF (@strMode = 'C')
        BEGIN
        
          INSERT @tabConsultants(
            RowID,
            ParentRowID,
            PlanID,
            TaskID,
            ConsultantID,
            Account,
            AccountName,
            AccountTypeCode,
            AccountType,
            Vendor,
            VendorName,
            ParentOutlineNumber,
            OutlineNumber,
            OutlineLevel,
            RowLevel,
            Name,
            StartDate,
            EndDate,
            MinASGDate,
            MaxASGDate,
            WBS1,
            WBS2,
            WBS3,
            Org,
            OrgName,
            ProjMgr,
        UtilizationScheduleFlg,
            PMFullName,
            ClientName,
            RT_Status,
            TopWBS1,
            TopName,
            HasNotes,
            HasAssignments,
            HasChildren,
            LeafNode,
        
            ContractStartDate,
            ContractEndDate,

            BaselineCost,
            BaselineBill,

            JTDCost,
            JTDReimCost,
            JTDBill,
            JTDReimBill,

            ContractCost,
            ContractDirCost,
            ContractReimCost,
            ContractBill,
            ContractDirBill,
            ContractReimBill,

            PlannedCost,
            PlannedReimCost,
            PlannedBill,
            PlannedReimBill,

            ETCCost,
            ETCBill,
            EACCost,
            EACBill,
            MarkupCost,
            ContractLessJTDBill,

            CalcPctComplCost,
            CalcPctComplBill,
            PlannedLessJTDCost,
            PlannedLessJTDBill,
            PlannedLessEACCost,
            PlannedLessEACBill,
            ContractLessEACCost,
            ContractLessEACBill,
            ContractLessJTDCost
          )
            SELECT
              '|' + 
              CASE 
                WHEN @strInputType = 'T' THEN YT.TaskID
                WHEN @strInputType = 'W' THEN YT.WBS1WBS2WBS3
              END AS RowID,
              @strParentRowID AS ParentRowID,
              YT.PlanID AS PlanID,
              YT.TaskID AS TaskID,
              NULL AS ConsultantID,
              NULL AS Account,
              NULL AS AccountName,
              NULL AS AccountTypeCode,
              NULL AS AccountType,
              NULL AS Vendor,
              NULL AS VendorName,
              YT.ParentOutlineNumber AS ParentOutlineNumber,
              YT.OutlineNumber AS OutlineNumber,
              YT.OutlineLevel AS OutlineLevel,
              YT.OutlineLevel AS RowLevel,
              YT.Name AS Name,
              YT.StartDate AS StartDate,
              YT.EndDate AS EndDate,
              YT.MinASGDate,
              YT.MaxASGDate,
              ISNULL(YT.WBS1, '') AS WBS1,
              ISNULL(YT.WBS2, '') AS WBS2,
              ISNULL(YT.WBS3, '') AS WBS3,
              ISNULL(YT.Org, '') AS Org,
              ISNULL(O.Name, '') AS OrgName,
              ISNULL(YT.ProjMgr, '') AS ProjMgr,
            YT.UtilizationScheduleFlg AS UtilizationScheduleFlg,
              CONVERT(nvarchar(255), ISNULL(ISNULL(PM.PreferredName, PM.FirstName), '') + ISNULL((' ' + PM.LastName), '') + ISNULL(', ' + PMSuffix.Suffix, '')) AS PMFullName,
              ISNULL(CL.Name, '') AS ClientName,
              YT.Status AS RT_Status,
              @strTopWBS1,
              @strTopName,
              YT.HasNotes AS HasNotes,
              CASE
                WHEN YT.MinConsultantID IS NOT NULL
                THEN CONVERT(bit, 1)
                ELSE CONVERT(bit, 0)
              END AS HasAssignments,
              CASE 
                WHEN YT.NonLBCDChildrenCount > 0 
                THEN CONVERT(bit, 1) 
                ELSE 
                  CASE 
                    WHEN EXISTS (SELECT 'X' FROM PNConsultant WHERE TaskID = YT.TaskID) 
                    THEN CONVERT(bit,1) 
                    ELSE CONVERT(bit, 0) 
                  END 
              END AS HasChildren,
              CASE WHEN YT.NonLBCDChildrenCount > 0 THEN CONVERT(bit, 0) ELSE CONVERT(bit,1) END AS LeafNode,
   
              YT.ContractStartDate AS ContractStartDate,
              YT.ContractEndDate AS ContractEndDate,

              YT.BaselineCost AS BaselineCost,
              YT.BaselineBill AS BaselineBill,

              YT.JTDCost AS JTDCost,
              YT.JTDReimCost AS JTDReimCost,
              YT.JTDBill AS JTDBill,
              YT.JTDReimBill AS JTDReimBill,

              YT.ContractCost AS ContractCost,
              YT.ContractDirCost AS ContractDirCost,
              YT.ContractReimCost AS ContractReimCost,
              YT.ContractBill AS ContractBill,
              YT.ContractDirBill AS ContractDirBill,
              YT.ContractReimBill AS ContractReimBill,

              0 AS PlannedCost,
              0 AS PlannedReimCost,
              0 AS PlannedBill,
              0 AS PlannedReimBill,

              0 AS ETCCost,
              0 AS ETCBill,
              0 AS EACCost,
              0 AS EACBill,
              0 AS MarkupCost,
              0 AS ContractLessJTDBill,

              0 AS CalcPctComplCost,
              0 AS CalcPctComplBill,
              0 AS PlannedLessJTDCost,
              0 AS PlannedLessJTDBill,
              0 AS PlannedLessEACCost,
              0 AS PlannedLessEACBill,
              0 AS ContractLessEACCost,
              0 AS ContractLessEACBill,
              0 AS ContractLessJTDCost
            FROM ( /* AS YT */
              SELECT
                XT.PlanID,
                XT.TaskID,
                XT.ParentOutlineNumber,
                XT.OutlineNumber,
                XT.OutlineLevel,
                XT.NonLBCDChildrenCount,
                XT.Name,
                XT.WBS1WBS2WBS3,
                XT.WBS1 AS WBS1,
                XT.WBS2 AS WBS2,
                XT.WBS3 AS WBS3,
                XT.Org,
                XT.ClientID,
                XT.ProjMgr,
        XT.UtilizationScheduleFlg AS UtilizationScheduleFlg,
                XT.StartDate,
                XT.EndDate,
                XT.Status,
                XT.HasNotes,
                XT.ContractStartDate AS ContractStartDate,
                XT.ContractEndDate AS ContractEndDate,
                XT.BaselineCost AS BaselineCost,
                XT.BaselineBill AS BaselineBill,
                XT.JTDCost AS JTDCost,
                XT.JTDReimCost AS JTDReimCost,
                XT.JTDBill AS JTDBill,
                XT.JTDReimBill AS JTDReimBill,
                XT.ContractCost AS ContractCost,
                XT.ContractDirCost AS ContractDirCost,
                XT.ContractReimCost AS ContractReimCost,
                XT.ContractBill AS ContractBill,
                XT.ContractDirBill AS ContractDirBill,
                XT.ContractReimBill AS ContractReimBill,
                XA.MinConsultantID AS MinConsultantID,
                XA.MinASGDate AS MinASGDate,
                XA.MaxASGDate AS MaxASGDate
                FROM ( /* AS XT */
                  SELECT
                    T.PlanID AS PlanID,
                    T.TaskID AS TaskID,
                    T.ParentOutlineNumber AS ParentOutlineNumber,
                    T.OutlineNumber AS OutlineNumber,
                    T.OutlineLevel AS OutlineLevel,
                    CASE WHEN T.OutlineLevel < @tiMinConWBSLevel - 1 THEN T.NonLBCDChildrenCount ELSE 0 END AS NonLBCDChildrenCount,
                    T.Name AS Name,
                    T.WBS1WBS2WBS3 AS WBS1WBS2WBS3,
                    T.WBS1 AS WBS1,
                    T.WBS2 AS WBS2,
                    T.WBS3 AS WBS3,
                    T.Org AS Org,
                    T.ClientID AS ClientID,
                    T.ProjMgr AS ProjMgr,
          T.UtilizationScheduleFlg AS UtilizationScheduleFlg,
                    T.StartDate AS StartDate,
                    T.EndDate AS EndDate,
                    T.ContractStartDate AS ContractStartDate,
                    T.ContractEndDate AS ContractEndDate,
                    ISNULL(T.Status, '') AS Status,
                    ISNULL(T.HasNotes, 'N') AS HasNotes,
                    ROUND(ISNULL(T.BaselineExpCost, 0.0000), @intAmtCostDecimals) AS BaselineCost,
                    ROUND(ISNULL(T.BaselineExpBill, 0.0000), @intAmtBillDecimals) AS BaselineBill,
                    0 AS JTDCost,
                    0 AS JTDReimCost,
                    0 AS JTDBill,
                    0 AS JTDReimBill,
                    ROUND(ISNULL(T.FeeCost, 0.0000), @intAmtCostDecimals) AS ContractCost,
                    ROUND(ISNULL(T.FeeDirCost, 0.0000), @intAmtCostDecimals) AS ContractDirCost,
                    ROUND(ISNULL(T.FeeReimCost, 0.0000), @intAmtCostDecimals) AS ContractReimCost,
                    ROUND(ISNULL(T.FeeBill, 0.0000), @intAmtBillDecimals) AS ContractBill,
                    ROUND(ISNULL(T.FeeDirBill, 0.0000), @intAmtBillDecimals) AS ContractDirBill,
                    ROUND(ISNULL(T.FeeReimBill, 0.0000), @intAmtBillDecimals) AS ContractReimBill
                    FROM @tabWBS AS T
                      INNER JOIN @tabWBS AS PT ON PT.OutlineNumber = T.ParentOutlineNumber
                    WHERE PT.WBS1 = @strWBS1 AND PT.WBS2 = @strWBS2 AND PT.WBS3 = @strWBS3 AND
                      ISNULL(@strTaskStatus, '') LIKE CASE WHEN @bActiveWBSOnly = 1 THEN 'A' ELSE '%' END AND
                      ISNULL(T.Status, '') LIKE CASE WHEN @bActiveWBSOnly = 1 THEN 'A' ELSE '%' END
                    GROUP BY
                      T.PlanID, T.TaskID, T.ParentOutlineNumber, T.OutlineNumber, T.OutlineLevel, T.NonLBCDChildrenCount,
                      T.Name, T.WBS1WBS2WBS3, T.WBS1, T.WBS2, T.WBS3, T.Org, T.ClientID, T.ProjMgr, T.UtilizationScheduleFlg, T.StartDate, T.EndDate, T.Status, T.HasNotes,
                      T.ContractStartDate, T.ContractEndDate, 
                      T.BaselineExpCost, T.BaselineExpBill, T.FeeCost, T.FeeBill, T.FeeDirCost, T.FeeDirBill, T.FeeReimCost,
                      T.FeeReimBill
                ) AS XT
                LEFT JOIN (
                  SELECT
                    AT.PlanID AS PlanID,
                    AT.OutlineNumber AS OutlineNumber,
                    MIN(A.ConsultantID) AS MinConsultantID,
                    MIN(A.StartDate) AS MinASGDate,
                    MAX(A.EndDate) AS MaxASGDate
                    FROM @tabWBS AS AT
                      LEFT JOIN @tabConsultant A ON AT.PlanID = A.PlanID AND A.OutlineNumber LIKE AT.OutlineNumber + '%'
                    GROUP BY AT.PlanID, AT.OutlineNumber
                ) AS XA ON XA.PlanID = XT.PlanID AND XA.OutlineNumber = XT.OutlineNumber
            ) AS YT
              LEFT JOIN CL ON YT.ClientID = CL.ClientID
              LEFT JOIN EM AS PM ON YT.ProjMgr = PM.Employee
        LEFT JOIN CFGSuffix PMSuffix ON PMSuffix.Code = PM.Suffix
              LEFT JOIN Organization AS O ON YT.Org = O.Org
              LEFT JOIN PNPlan AS P ON P.PlanID = YT.PlanID

        END /* END ELSE IF (@strMode = 'C') */
    
    END /* END WBS Rows */
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
