SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DW$ARAging]()
  RETURNS @ARAging TABLE
    (
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    Org  nvarchar(14) COLLATE database_default, 
    InvoiceNumber nvarchar(12) COLLATE database_default,
    Period int,
    PeriodEndDate int,
    InvoiceBalance decimal (19,4),
    InvoiceBalanceProjectCurrency decimal (19,4),
    InvoiceBalanceBillingCurrency decimal (19,4),
    AgingBucket int
    )
BEGIN -- Function DW$ARAging

  /**************************************************************************************************************************/
  /* The purpose of this UDF is to return a table which contains AR Trending data.                                          */
  /* Data from LedgerAR are combined with data from AR and CFGDates to derive AR Trending data.                             */
  /* Since data from LedgerAR need to be inserted into @ARAging by WBS1, WBS2, WBS3, InvoiceNumber,                         */
  /* and InvoiceBalance need to be calculated sequentially by Period, Cursor is used here to ensure                         */
  /* that data are being processed in correct sequence.                                                                     */
  /* I have found the following anomalies in those tables mentioned above, and tried to compensate                          */
  /* for the conditions whenever possible:                                                                                  */
  /*   1. LedgerAR and AR could have Period = 0. Therefore, the join to CFGDates is based on AR.InvoiceDate                 */
  /*      to find the correct Accounting Period when LedgerAR.Period = 0.                                                   */
  /*   2. LedgerAR could have different Orgs for a given combination of WBS1/WBS2/WBS3/Invoice.                             */
  /*   3. CFGDates in one of the QE DB had 2 records for 2 different Periods but with overlapping Start/End Dates.          */
  /*      Shirley indicated that was an error condition in data of CFGDates, and no action is needed                        */
  /*      with respect to AR Trending.                                                                                      */
  /*   4. AR.PaidPeriod cannot be trusted. I need to use LedgerAR to determine whether an Invoice is still unpaid.          */
  /*   5. Sometimes, entries in LedgerAR does not have a corresponding Invoice in AR.                                       */
  /*      I had to use LEFT JOIN between LedgerAR and AR to handle this issue.                                              */
  /*      Also, I needed to use LedgerAR.Invoice instead of relying on AR.                                                  */
  /**************************************************************************************************************************/

  DECLARE @WBS1 nvarchar(60)
  DECLARE @WBS2 nvarchar(14)
  DECLARE @WBS3 nvarchar(14)
  DECLARE @Org nvarchar(14)
  DECLARE @InvoiceNumber nvarchar(24)
  DECLARE @InvoiceDate datetime
  DECLARE @PeriodEndDate datetime
  DECLARE @Period int

  DECLARE @Amount decimal(19,4)
  DECLARE @AmountProjectCurrency decimal(19,4)
  DECLARE @AmountBillingCurrency decimal(19,4)
  DECLARE @InvoiceBalance decimal(19,4)
  DECLARE @InvoiceBalanceProjectCurrency decimal(19,4)
  DECLARE @InvoiceBalanceBillingCurrency decimal(19,4)

  DECLARE @lastWBS1 nvarchar(60)
  DECLARE @lastWBS2 nvarchar(14)
  DECLARE @lastWBS3 nvarchar(14)
  DECLARE @lastOrg nvarchar(14)
  DECLARE @lastInvoiceNumber nvarchar(24)
  DECLARE @lastInvoiceDate datetime
  DECLARE @lastPeriod int

  DECLARE @AgingBucket int

  DECLARE @MaxPeriod int
  
  SELECT @MaxPeriod = MAX(Period) FROM CFGDates

  /* I need to use a cursor to ensure that the Aging records can be traversed in a particular sequence */

  DECLARE curAR CURSOR FOR
    SELECT DISTINCT
      WBS1, 
      WBS2, 
      WBS3,
      MIN(Org),
      InvoiceNumber,
      InvoiceDate, 
      Period,
      PeriodEndDate,
      SUM(Amount) AS Amount,
      SUM(AmountProjectCurrency) AS AmountProjectCurrency,
      SUM(AmountBillingCurrency) AS AmountBillingCurrency
      FROM (

        SELECT
          L.WBS1 AS WBS1, 
          L.WBS2 AS WBS2, 
          L.WBS3 AS WBS3,
          MIN(L.Org) AS Org,
          L.Invoice AS InvoiceNumber,
          AR.InvoiceDate AS InvoiceDate, 
          L.Period AS Period,
          CONVERT(int, (ISNULL(D.AccountPdEnd, AR.InvoiceDate))) AS PeriodEndDate,
          SUM(CASE WHEN TransType = 'CR' AND SubType = 'R' THEN Amount ELSE -Amount END) AS Amount,
          SUM(CASE WHEN TransType = 'CR' AND SubType = 'R' THEN AmountProjectCurrency ELSE -AmountProjectCurrency END) AS AmountProjectCurrency,
          SUM(CASE WHEN TransType = 'CR' AND SubType = 'R' THEN AmountBillingCurrency ELSE -AmountBillingCurrency END) AS AmountBillingCurrency
          FROM LedgerAR AS L
            INNER JOIN AR ON L.WBS1 = AR.WBS1 AND L.WBS2 = AR.WBS2 AND L.WBS3 = AR.WBS3 AND L.Invoice = AR.Invoice
            LEFT JOIN CFGDates AS D ON AR.InvoiceDate BETWEEN D.AccountPdStart AND D.AccountPdEnd
          WHERE L.Period <> 0 AND
            ((TransType = 'IN' AND L.AutoEntry = 'N' AND ISNULL(L.SubType, N' ') <> 'X') OR
             (TransType = 'CR' AND L.SubType = 'R') OR
             (TransType = 'CR' AND L.SubType = 'T' AND ISNULL(L.Invoice, N' ') <> ' ')
            ) AND L.Org is not null 
          GROUP BY L.WBS1, L.WBS2, L.WBS3, L.Invoice, AR.InvoiceDate, D.AccountPdEnd, L.Period
          HAVING (SUM(CASE WHEN TransType = 'CR' AND SubType = 'R' THEN Amount ELSE -Amount END) <> 0)
        UNION ALL
        SELECT
          L.WBS1 AS WBS1, 
          L.WBS2 AS WBS2, 
          L.WBS3 AS WBS3,
          MIN(L.Org) AS Org,
          L.Invoice AS InvoiceNumber,
          AR.InvoiceDate AS InvoiceDate, 
          D.Period AS Period,
          CONVERT(int, (ISNULL(D.AccountPdEnd, AR.InvoiceDate))) AS PeriodEndDate,
          SUM(CASE WHEN TransType = 'CR' AND SubType = 'R' THEN Amount ELSE -Amount END) AS Amount,
          SUM(CASE WHEN TransType = 'CR' AND SubType = 'R' THEN AmountProjectCurrency ELSE -AmountProjectCurrency END) AS AmountProjectCurrency,
          SUM(CASE WHEN TransType = 'CR' AND SubType = 'R' THEN AmountBillingCurrency ELSE -AmountBillingCurrency END) AS AmountBillingCurrency
          FROM LedgerAR AS L
            INNER JOIN AR ON L.WBS1 = AR.WBS1 AND L.WBS2 = AR.WBS2 AND L.WBS3 = AR.WBS3 AND L.Invoice = AR.Invoice
            LEFT JOIN CFGDates AS D ON AR.InvoiceDate BETWEEN D.AccountPdStart AND D.AccountPdEnd
          WHERE L.Period = 0 AND
            ((TransType = 'IN' AND L.AutoEntry = 'N' AND ISNULL(L.SubType, N' ') <> 'X') OR
             (TransType = 'CR' AND L.SubType = 'R') OR
             (TransType = 'CR' AND L.SubType = 'T' AND ISNULL(L.Invoice, N' ') <> ' ')
            ) AND L.Org is not null 
          GROUP BY L.WBS1, L.WBS2, L.WBS3, L.Invoice, AR.InvoiceDate, D.AccountPdEnd, D.Period
          HAVING (SUM(CASE WHEN TransType = 'CR' AND SubType = 'R' THEN Amount ELSE -Amount END) <> 0)

      ) AS X
      GROUP BY WBS1, WBS2, WBS3, InvoiceNumber, InvoiceDate, PeriodEndDate, Period
      ORDER BY WBS1, WBS2, WBS3, InvoiceNumber, Period

  SET @lastWBS1 = NULL
  SET @lastWBS2 = NULL
  SET @lastWBS3 = NULL
  SET @lastInvoiceNumber = NULL
  SET @lastInvoiceDate = NULL
  SET @lastPeriod = NULL
  SET @InvoiceBalance = 0
  SET @InvoiceBalanceProjectCurrency = 0
  SET @InvoiceBalanceBillingCurrency = 0
  SET @AgingBucket = 0

  OPEN curAR

  FETCH NEXT FROM curAR INTO 
    @WBS1,
    @WBS2,
    @WBS3,
    @Org,
    @InvoiceNumber,
    @InvoiceDate,
    @Period,
    @PeriodEndDate,
    @Amount,
    @AmountProjectCurrency,
    @AmountBillingCurrency

  WHILE (@@FETCH_STATUS = 0)
    BEGIN /* WHILE */

      IF (@lastWBS1 = @WBS1 AND @lastWBS2 = @WBS2 AND @lastWBS3 = @WBS3 AND @lastInvoiceNumber = @InvoiceNumber) 
        BEGIN /* IF */

          /* Fill in the rest of the periods prior to the current period. */

          IF (@InvoiceBalance <> 0)
            INSERT INTO @ARAging (
              WBS1,
              WBS2,
              WBS3,
              Org, 
              InvoiceNumber,
              Period,
              PeriodEndDate,
              InvoiceBalance,
              InvoiceBalanceProjectCurrency,
              InvoiceBalanceBillingCurrency,
              AgingBucket
              )
              SELECT
                @lastWBS1,
                @lastWBS2,
                @lastWBS3,
                @lastOrg, 
                @lastInvoiceNumber,
                Period,
                CONVERT(int, AccountPdEnd) as PeriodEndDate,
                @InvoiceBalance,
                @InvoiceBalanceProjectCurrency,
                @InvoiceBalanceBillingCurrency,
                CASE
                  WHEN @InvoiceBalance = 0 THEN 0
                  WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @lastInvoiceDate) < 31 THEN 0
                  WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @lastInvoiceDate) < 46 THEN 1
                  WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @lastInvoiceDate) < 61 THEN 2
                  WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @lastInvoiceDate) < 91 THEN 3
                  WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @lastInvoiceDate) < 121 THEN 4
                  ELSE 5
                  END As AgingBucket
                FROM CFGDates
                WHERE Period > @lastPeriod AND Period < @Period

          /* Fill in data for the current period. */

          SET @InvoiceBalance = @InvoiceBalance + @Amount
          SET @InvoiceBalanceProjectCurrency = @InvoiceBalanceProjectCurrency + @AmountProjectCurrency
          SET @InvoiceBalanceBillingCurrency = @InvoiceBalanceBillingCurrency + @AmountBillingCurrency

          INSERT INTO @ARAging (
            WBS1,
            WBS2,
            WBS3,
            Org, 
            InvoiceNumber,
            Period,
            PeriodEndDate,
            InvoiceBalance,
            InvoiceBalanceProjectCurrency,
            InvoiceBalanceBillingCurrency,
            AgingBucket
            )
            SELECT
              @WBS1,
              @WBS2,
              @WBS3,
              @Org, 
              @InvoiceNumber,
              Period,
              CONVERT(int, AccountPdEnd) as PeriodEndDate,
              @InvoiceBalance,
              @InvoiceBalanceProjectCurrency,
              @InvoiceBalanceBillingCurrency,
              CASE
                WHEN @InvoiceBalance = 0 THEN 0
                WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @InvoiceDate) < 31 THEN 0
                WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @InvoiceDate) < 46 THEN 1
                WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @InvoiceDate) < 61 THEN 2
                WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @InvoiceDate) < 91 THEN 3
                WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @InvoiceDate) < 121 THEN 4
                ELSE 5
                END As AgingBucket
              FROM CFGDates
              WHERE Period = @Period

        END /* IF */
      ELSE
        BEGIN /* ELSE */

          /************************************************************************************************************************************/
          /* When we get to this point, the combination of WBS1/WBS2/WBS3/Invoice was just changed with the last read from the Cursor.        */
          /* Before inserting data for the new invoice, we need to flush out the data for the previous invoice.                               */
          /* --> If the previous invoice had a balance, insert one record for each Accounting Period until the end of the Fiscal Calendar.    */
          /************************************************************************************************************************************/

          IF (@InvoiceBalance <> 0)
            BEGIN /* IF */
              INSERT INTO @ARAging (
                WBS1,
                WBS2,
                WBS3,
                Org, 
                InvoiceNumber,
                Period,
                PeriodEndDate,
                InvoiceBalance,
                InvoiceBalanceProjectCurrency,
                InvoiceBalanceBillingCurrency,
                AgingBucket
                )
                SELECT
                  @lastWBS1,
                  @lastWBS2,
                  @lastWBS3,
                  @lastOrg, 
                  @lastInvoiceNumber,
                  Period,
                  CONVERT(int, AccountPdEnd) as PeriodEndDate,
                  @InvoiceBalance,
                  @InvoiceBalanceProjectCurrency,
                  @InvoiceBalanceBillingCurrency,
                  CASE
                    WHEN @InvoiceBalance = 0 THEN 0
                    WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @lastInvoiceDate) < 31 THEN 0
                    WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @lastInvoiceDate) < 46 THEN 1
                    WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @lastInvoiceDate) < 61 THEN 2
                    WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @lastInvoiceDate) < 91 THEN 3
                    WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @lastInvoiceDate) < 121 THEN 4
                    ELSE 5
                    END As AgingBucket
                  FROM CFGDates
                  WHERE Period > @lastPeriod AND Period <= @MaxPeriod
            END /* IF */

          /* Starting a new Invoice. */

          SET @InvoiceBalance = @Amount
          SET @InvoiceBalanceProjectCurrency = @AmountProjectCurrency
          SET @InvoiceBalanceBillingCurrency = @AmountBillingCurrency

          INSERT INTO @ARAging (
            WBS1,
            WBS2,
            WBS3,
            Org, 
            InvoiceNumber,
            Period,
            PeriodEndDate,
            InvoiceBalance,
            InvoiceBalanceProjectCurrency,
            InvoiceBalanceBillingCurrency,
            AgingBucket
            )
            SELECT
              @WBS1,
              @WBS2,
              @WBS3,
              @Org, 
              @InvoiceNumber,
              Period,
              CONVERT(int, AccountPdEnd) as PeriodEndDate,
              @InvoiceBalance,
              @InvoiceBalanceProjectCurrency,
              @InvoiceBalanceBillingCurrency,
              CASE
                WHEN @InvoiceBalance = 0 THEN 0
                WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @InvoiceDate) < 31 THEN 0
                WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @InvoiceDate) < 46 THEN 1
                WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @InvoiceDate) < 61 THEN 2
                WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @InvoiceDate) < 91 THEN 3
                WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @InvoiceDate) < 121 THEN 4
                ELSE 5
                END As AgingBucket
              FROM CFGDates
              WHERE Period = @Period

        END /* ELSE */

      /* Keeping track of the data from the last pass in the loop. */

      SET @lastWBS1 = @WBS1
      SET @lastWBS2 = @WBS2
      SET @lastWBS3 = @WBS3
      SET @lastOrg = @Org
      SET @lastInvoiceNumber = @InvoiceNumber
      SET @lastInvoiceDate = @InvoiceDate
      SET @lastPeriod = @Period

      FETCH NEXT FROM curAR INTO 
        @WBS1,
        @WBS2,
        @WBS3,
        @Org,
        @InvoiceNumber,
        @InvoiceDate,
        @Period,
        @PeriodEndDate,
        @Amount,
        @AmountProjectCurrency,
        @AmountBillingCurrency

    END /* WHILE */

  CLOSE curAR
  DEALLOCATE curAR

  /**********************************************************************************************************************************/
  /* When we get to this point, the Cursor was exhausted. We need to flush out data for the last invoice that was processed.        */
  /* --> If the last invoice had a balance, insert one record for each Accounting Period until the end of the Fiscal Calendar.      */
  /**********************************************************************************************************************************/

  IF (@InvoiceBalance <> 0)
    BEGIN /* IF */
      INSERT INTO @ARAging (
        WBS1,
        WBS2,
        WBS3,
        Org, 
        InvoiceNumber,
        Period,
        PeriodEndDate,
        InvoiceBalance,
        InvoiceBalanceProjectCurrency,
        InvoiceBalanceBillingCurrency,
        AgingBucket
        )
        SELECT
          @WBS1,
          @WBS2,
          @WBS3,
          @Org, 
          @InvoiceNumber,
          Period,
          CONVERT(int, AccountPdEnd) as PeriodEndDate,
          @InvoiceBalance,
          @InvoiceBalanceProjectCurrency,
          @InvoiceBalanceBillingCurrency,
          CASE
            WHEN @InvoiceBalance = 0 THEN 0
            WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @InvoiceDate) < 31 THEN 0
            WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @InvoiceDate) < 46 THEN 1
            WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @InvoiceDate) < 61 THEN 2
            WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @InvoiceDate) < 91 THEN 3
            WHEN CONVERT(int, AccountPdEnd) - CONVERT(int, @InvoiceDate) < 121 THEN 4
            ELSE 5
            END As AgingBucket
          FROM CFGDates
          WHERE Period > @Period AND Period <= @MaxPeriod
    END /* IF */

  RETURN

END /* DW$ARAging */
GO
