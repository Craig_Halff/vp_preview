SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_DraftDate] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS datetime
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group. All rights reserved.

	select dbo.fnCCG_EI_DraftDate('2003005.xx',' ',' ')
*/
     declare @d datetime
     select @d = Max(InvoiceDate) from billInvMaster where BillWBS1=@WBS1 and BillWBS2=' ' and Invoice='<Draft>'
     return @d
END
GO
