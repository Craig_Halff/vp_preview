SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create FUNCTION [dbo].[GreatestDate] (@v1 Date, @v2 Date) 
RETURNS Date AS
/*
Copyright (c) 2018 Central Consulting Group. All rights reserved.
04/16/2018 David Springer
           Return greatest of two or three variables
SELECT dbo.greatest(GETDATE(), GETDATE()-2, GETDATE()+3)
*/
BEGIN
	DECLARE @return Date
	SELECT @return = Max (Value) 
	FROM (SELECT @v1 value UNION ALL SELECT @v2) a
	RETURN @return
END


GO
