SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   FUNCTION [dbo].[fnCCG_GetWebServerURLPrefix]()
RETURNS varchar(500)
AS BEGIN
/*
       Copyright (c) 2018 EleVia Software. All rights reserved.
       select dbo.fnCCG_GetWebServerURLPrefix()
*/
       declare @url varchar(500)
	   select top 1 @url = Page from FW_CustomNavtree where (Page like '%EleVia%' or Page like '%CCG%') and Page like '%Launch%.aspx' 
			order by case when Page like '%LaunchPage%' then 0 when Page like '%EleVia%' then 1 else 2 end
	   return REVERSE(Substring(REVERSE(@url), CHARINDEX('/', REVERSE(@url)), 1000)) --keeps last /
END
GO
