SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$LabRate]
  (@strResourceID Nvarchar(20) = NULL,
   @sintCategory smallint = 0,
   @strGRLBCD Nvarchar(14) = NULL,
   @strLaborCode Nvarchar(14) = NULL,
   @sintRtMethod smallint = 0,
   @intRtTabNo int = 0,
   @sintGRMethod smallint = 0,
   @intGRTabNo int = 0,
   @dtStartDate datetime,
   @dtEndDate datetime,
   @sintDecimals smallint = 2,
   @decProvRate decimal(19,4) = 0,
   @decOvrdRate decimal(19,4) = 0)
  RETURNS decimal(19,4)
BEGIN

  /*--------------------------------------------------------------------------------------------------------------------
    This function encapsulates the complex rules that determine the Labor Rate.
    This function will return the Override Rate if it is not zero.
  */--------------------------------------------------------------------------------------------------------------------

  DECLARE @decRate decimal(19,4)
  
  SET @decRate = 0
  
  IF (@decOvrdRate <> 0)
    BEGIN -- There is an override rate, return it as the rate.
      SET @decRate = ROUND(ISNULL(@decOvrdRate, 0), @sintDecimals)
    END -- If-Then
  ELSE
    BEGIN -- Determine rate base on Type of Resource, Rate Method, and Rate Table Number.
      SET @decRate = 
        ROUND(ISNULL((CASE WHEN @strResourceID IS NOT NULL THEN -- Employees
                             CASE @sintRtMethod
                               WHEN 1 THEN @decProvRate -- Employee Provisional Rate
                               WHEN 2 THEN -- Labor Rate Table
                                 dbo.RP$BTRRTEmpls(@intRtTabNo, @strResourceID, @dtStartDate, @dtEndDate)
                               WHEN 3 THEN -- Labor Category Table
                                 dbo.RP$BTRCTCats(@intRtTabNo, @sintCategory, @dtStartDate, @dtEndDate, @strResourceID)
                               WHEN 4 THEN -- Labor Code Table
                                 dbo.RP$BTRLTCodes(@intRtTabNo, @strLaborCode, @dtStartDate, @dtEndDate)
                               END
                           ELSE -- Generic Resources
                             CASE @sintGRMethod
                               WHEN 0 THEN -- Labor Category
                                 dbo.RP$BTRCTCats(@intGRTabNo, @sintCategory, @dtStartDate, @dtEndDate, NULL)
                               WHEN 1 THEN -- Labor Code
                                 dbo.RP$BTRLTCodes(@intGRTabNo, @strGRLBCD, @dtStartDate, @dtEndDate)
                               END
                      END), 0), @sintDecimals)   
    END -- If-Else

  RETURN(@decRate)

END -- fn_RP$LabRate
GO
