SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_AROver60Link] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS varchar(max)
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select dbo.fnCCG_EI_AROver60Link('1999009.xx', ' ', ' ')
*/
	declare @res varchar(max)
	select @res = '@' + Replace(AppURL,'VisionClient','Vision')  + '/EI_NValue.aspx?WBS1=' + @WBS1 + '&WBS2=' + @WBS2 + '&WBS3=' + @WBS3 + 
		'&T=Aged%20AR' +
		'&NV=6' +
		'&HD=Invoice' +
		'&H1=Date' +
		'&H2=0-30' +
		'&H3=31-60' +
		'&H4=61-90' +
		'&H5=91-120' +
		'&H6=121%2B' +
		'&FN=fnCCG_EI_ARDetails' +
		'&DS=' + @@SERVERNAME + '&DB=' + DB_NAME()
		from CFGSystem
	return @res
END
GO
