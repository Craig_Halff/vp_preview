SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PR$ValidateWBS1](
  @strWBS1 nvarchar(30)
)
  RETURNS bit
BEGIN -- Function PR$ValidateWBS1

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF(@strWBS1 IS NULL OR DATALENGTH(LTRIM(RTRIM(@strWBS1))) = 0)
    BEGIN
      RETURN 0
    END /* IF(@strWBS1 IS NULL OR DATALENGTH(LTRIM(RTRIM(@strWBS1))) = 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @bitIsValidWBS1 bit = 0
  DECLARE @bitIsValidFormat bit = 0
  DECLARE @bitNotExistInPR bit = 0

  DECLARE @siWBS1Length smallint
  DECLARE @siWBS1Delimiter1Position smallint
  DECLARE @siWBS1Delimiter2Position smallint

  DECLARE @strWBS1LeadZeros varchar(1)
  DECLARE @strWBS1Delimiter1 nvarchar(1)
  DECLARE @strWBS1Delimiter2 nvarchar(1)
  DECLARE @strWBS1Pattern nvarchar(255) = ''
  DECLARE @strNOTDelimiter nvarchar(10) = '' /* e.g. [^.-]*/

  SELECT 
	  @siWBS1Length = WBS1Length,
	  @strWBS1LeadZeros = WBS1LeadZeros,
	  @strWBS1Delimiter1 = WBS1Delimiter1,
	  @siWBS1Delimiter1Position = WBS1Delimiter1Position,
	  @strWBS1Delimiter2 = WBS1Delimiter2,
	  @siWBS1Delimiter2Position = WBS1Delimiter2Position
    FROM CFGFormat

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- If "Display Leading Zeros" was on, then formulate the format pattern with appropriate position of delimiter(s).

  IF(@strWBS1LeadZeros = 'Y')
    BEGIN

      -- Initialize WBS1 Pattern with all underscores up to formatted length of WBS1.

      SET @strWBS1Pattern = REPLICATE('_', @siWBS1Length)

      -- If Delimiter #1 is defined then set it at location specify by WBS1Delimiter1Position.

      IF(@siWBS1Delimiter1Position > 0)
        BEGIN
          SET @strWBS1Pattern = STUFF(@strWBS1Pattern, @siWBS1Delimiter1Position, 1, @strWBS1Delimiter1)
          SET @strNOTDelimiter = '[^' + @strWBS1Delimiter1 + ']'
        END /* END IF(@siWBS1Delimiter1Position > 0) */

      -- If Delimiter #2 is defined then set it at location specify by WBS1Delimiter2Position.

      IF(@siWBS1Delimiter2Position > 0)
        BEGIN
          SET @strWBS1Pattern = STUFF(@strWBS1Pattern, @siWBS1Delimiter2Position, 1, @strWBS1Delimiter2)
          SET @strNOTDelimiter = REPLACE(@strNOTDelimiter, ']', @strWBS1Delimiter2 + ']')
        END /* END IF(@siWBS1Delimiter1Position > 0) */

      -- If there are Delimiter #1 and/or Delimiter #2 then change the LIKE Pattern from wildcard underscore to "NOT Delimiter(s)"

      IF(@siWBS1Delimiter1Position > 0 OR @siWBS1Delimiter2Position > 0)
        BEGIN
          SET @strWBS1Pattern = REPLACE(@strWBS1Pattern, '_', @strNOTDelimiter)
        END /* END IF(@siWBS1Delimiter1Position > 0 OR @siWBS1Delimiter2Position > 0) */

    END /* END IF(@strWBS1LeadZeros = 'Y') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @bitIsValidFormat = 
    CASE 
      WHEN (@strWBS1LeadZeros = 'Y' AND @strWBS1 LIKE @strWBS1Pattern) THEN 1
      WHEN (@strWBS1LeadZeros = 'N' AND LEN(@strWBS1) <= @siWBS1Length) THEN 1 
      ELSE 0 
    END
  SET @bitNotExistInPR = CASE WHEN NOT EXISTS(SELECT WBS1 FROM PR WHERE WBS1 = @strWBS1 AND WBS2 = ' ' AND WBS3 = ' ') THEN 1 ELSE 0 END
  SET @bitIsValidWBS1 = CASE WHEN (@bitIsValidFormat = 1) AND (@bitNotExistInPR = 1) THEN 1 ELSE 0 END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN @bitIsValidWBS1

END -- PR$ValidateWBS1
GO
