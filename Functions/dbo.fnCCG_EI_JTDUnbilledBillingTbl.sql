SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnCCG_EI_JTDUnbilledBillingTbl] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS @T TABLE
(
	ColValue	money,			-- Spent @ billing
	ColDetail	varchar(100)	-- Set background color to light red if spent exceeds budget and budget > 0
)
AS BEGIN
/*
	Copyright (c) 2013 Central Consulting Group.  All rights reserved.

	select * from dbo.[fnCCG_EI_JTDSpentTbl]('029531.B00',' ',' ')
	select dbo.fnCCG_EI_Budget('2003005.00',' ',' ')
	select * from dbo.fnCCG_EI_JTDSpent('2003005.00',' ',' ')
	select * from dbo.fnCCG_EI_JTDSpent('2003005.00','1PD',' ')
*/
	declare @res money, @res2 money
	select @res = dbo.[fnCCG_EI_JTDUnbilledBilling](@WBS1, @WBS2, @WBS3)
	select @res2 = (SELECT TOP 1 ColValue FROM dbo.fnCCG_EI_DraftInvoiceAmt(@WBS1, @WBS2, @WBS3))
	IF ISNULL(@res2, 0) < ISNULL(@res, 0)
		insert into @T values (@res, 'BackgroundColor=#F2F5A9;Color=#CC0000')	
	ELSE
		insert into @T values (@res, 'BackgroundColor=#F2F5A9')	

	return
END
GO
