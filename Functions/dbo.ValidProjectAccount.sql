SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ValidProjectAccount]
 (@Account varchar(13),   
  @Company varchar(14),
  @AcctType int,
  @AcctsReceivable varchar(13),
  @Proj varchar(13),
  @ProjType varchar(1),
  @OrgLevels int,
  @BalanceSheet varchar(1))
  RETURNS Nvarchar(1)  AS
BEGIN
	if @AcctType = 0 And DATALENGTH(@Account) = 0
		GOTO ExitError;
	if @OrgLevels<=0 
		SET @BalanceSheet = 'N'
	if @Proj = '' 
		begin -- No Project begin
			if (@AcctType >= 4 And	@AcctType < 10) 
			or (@BalanceSheet = 'Y') or (@Account  = @AcctsReceivable)
			or (dbo.AccountIsAR(@Account, @Company) = 'Y')
				begin
					GOTO ExitError;
				end
		end -- end no project
	else 
		begin -- have project
			if (@ProjType = '')
				begin
					GoTo ExitError
				end
			else
				begin
					if @ProjType = 'R' And @AcctType >=4 And @AcctType <9  GOTO ExitValid;
					if @ProjType <> 'R' And @AcctType >= 9 And @AcctType < 10 GOTO ExitValid;
					
					if @AcctType < 4 --Balance Sheet, test AR
					begin
						if ((@Account = @AcctsReceivable)) or dbo.AccountIsAR(@Account, @Company) = 'Y'
						begin
							if @ProjType <> 'R'
								begin
									GOTO ExitError;
								end
						end
						else -- Not AR
							if @BalanceSheet <> 'Y'
								GOTO ExitError;
							else -- Balancesheet OK
								GOTO ExitValid;
					end -- accttype < revenue
					if (@ProjType = 'R' And @AcctType >= 9 And @AcctType < 10)
					or (@ProjType <> 'R' And @AcctType < 9)
						begin	
							GOTO ExitError;
						end															
		end
		end -- end of have project

ExitValid:
	RETURN 'Y'
ExitError:
	RETURN 'N'
END -- ValidProjectAccount
GO
