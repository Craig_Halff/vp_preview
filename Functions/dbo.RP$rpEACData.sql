SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$rpEACData]
  (@strPlanID varchar(32),
   @strETCDate Nvarchar(10)) -- Date must be in format: 'yyyy-mm-dd'

  RETURNS @tabEACPlanData TABLE
  (       EACLabHrs decimal(19,4),
          EACLabCost decimal(19,4),
          EACLabBill decimal(19,4),
          EACExpCost decimal(19,4),
          EACExpBill decimal(19,4),
          EACConCost decimal(19,4),
          EACConBill decimal(19,4),
          EACUntCost decimal(19,4),
          EACUntBill decimal(19,4),
          EACTotalCost decimal(19,4),
          EACTotalBill decimal(19,4))

BEGIN --outer


  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
  
  DECLARE @intHrDecimals int
  DECLARE @intQtyDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intRtCostDecimals int
  DECLARE @intRtBillDecimals int
  DECLARE @intLabRevDecimals int
  DECLARE @intECURevDecimals int
  
  DECLARE @intBaselineLabCount int
  DECLARE @intBaselineExpCount int
  DECLARE @intBaselineConCount int
  DECLARE @intBaselineUntCount int
  
  DECLARE @intPlannedLabCount int
  DECLARE @intPlannedExpCount int
  DECLARE @intPlannedConCount int
  DECLARE @intPlannedUntCount int
  
  DECLARE @intCompCount int
  DECLARE @intConsCount int
  DECLARE @intReimCount int
  DECLARE @intEVCount int

  DECLARE @intTDCount int

  DECLARE @intTaskJTDLabCount int
  DECLARE @intTaskJTDExpCount int
  DECLARE @intTaskJTDConCount int
  DECLARE @intTaskJTDUntCount int

  DECLARE @intJTDLabCount int
  DECLARE @intJTDExpCount int
  DECLARE @intJTDConCount int
  DECLARE @intJTDUntCount int
  
  DECLARE @intSort2 int
  DECLARE @intSort3 int
  DECLARE @intSort4 int
  
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strUserName Nvarchar(32)

  DECLARE @strOutlineNumber Nvarchar(255)
  DECLARE @strRowID Nvarchar(255)
  
   

  DECLARE @strTaskID varchar(32)
  DECLARE @strAssignmentID varchar(32)
  DECLARE @strExpenseID varchar(32)
  DECLARE @strConsultantID varchar(32)
  DECLARE @strUnitID varchar(32)
  
  DECLARE @strExpTab  varchar(1)
  DECLARE @strConTab  varchar(1)
  DECLARE @strUntTab varchar(1)
  
  DECLARE @strFeesByPeriod varchar(1)
  DECLARE @strEffectiveDateEnabled varchar(1)
  
  DECLARE @siCostRtMethod smallint
  DECLARE @siBillingRtMethod smallint
  DECLARE @siGRMethod smallint
  DECLARE @siCategory smallint
  
  DECLARE @intCostRtTableNo int
  DECLARE @intBillingRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int

  DECLARE @strResourceID Nvarchar(20)
  DECLARE @strLaborCode Nvarchar(14)
  DECLARE @strGRLBCD Nvarchar(14)
  
  DECLARE @strUnposted varchar(1)
  DECLARE @strCommitmentFlg varchar(1)
  
	DECLARE @strHasJTDLab  varchar(1)
	DECLARE @strHasJTDECU  varchar(1)
  
  -- Declare Temp tables.
  
  DECLARE @tabCalendarInterval
    TABLE(PlanID varchar(32) COLLATE database_default,
          StartDate datetime,
          EndDate datetime
          PRIMARY KEY(PlanID, StartDate, EndDate))
          
  DECLARE @tabLabETC
    TABLE(AssignmentID varchar(32) COLLATE database_default,
          ETCLabHrs decimal(19,4),
          ETCLabCost decimal(19,4),
          ETCLabBill decimal(19,4)
          PRIMARY KEY(AssignmentID))
          
  DECLARE @tabExpETC
    TABLE(ExpenseID varchar(32) COLLATE database_default,
          DirectAcctFlg Nvarchar(1) COLLATE database_default,
          ETCExpCost decimal(19,4),
          ETCExpBill decimal(19,4)
          PRIMARY KEY(ExpenseID, DirectAcctFlg))
  
  DECLARE @tabConETC
    TABLE(ConsultantID varchar(32) COLLATE database_default,
          DirectAcctFlg varchar(1) COLLATE database_default,
          ETCConCost decimal(19,4),
          ETCConBill decimal(19,4)
          PRIMARY KEY(ConsultantID, DirectAcctFlg))

  DECLARE @tabUntETC
    TABLE(UnitID varchar(32) COLLATE database_default,
          DirectAcctFlg varchar(1) COLLATE database_default,
          ETCUntQty decimal(19,4),
          ETCUntCost decimal(19,4),
          ETCUntBill decimal(19,4)
          PRIMARY KEY(UnitID, DirectAcctFlg))

  DECLARE @tabTaskETC
    TABLE(TaskID varchar(32) COLLATE database_default,
          OutlineLevel int,
          ETCLabHrs decimal(19,4),
          ETCLabCost decimal(19,4),
          ETCLabBill decimal(19,4),
          ETCExpCost decimal(19,4),
          ETCExpBill decimal(19,4),
          ETCConCost decimal(19,4),
          ETCConBill decimal(19,4),
          ETCUntQty decimal(19,4),
          ETCUntCost decimal(19,4),
          ETCUntBill decimal(19,4)
          PRIMARY KEY(TaskID, OutlineLevel))

  DECLARE @tabPlanETC
    TABLE(PlanID varchar(32) COLLATE database_default,
          ETCLabHrs decimal(19,4),
          ETCLabCost decimal(19,4),
          ETCLabBill decimal(19,4),
          ETCExpCost decimal(19,4),
          ETCExpBill decimal(19,4),
          ETCConCost decimal(19,4),
          ETCConBill decimal(19,4),
          ETCUntQty decimal(19,4),
          ETCUntCost decimal(19,4),
          ETCUntBill decimal(19,4),
          ETCDirExpCost decimal(19,4),
          ETCDirConCost decimal(19,4),
          ETCDirUntCost decimal(19,4),
          ETCDirExpBill decimal(19,4),
          ETCDirConBill decimal(19,4),
          ETCDirUntBill decimal(19,4)
          PRIMARY KEY(PlanID))
          
   DECLARE @tabTopTask 
    TABLE (TaskID varchar(32) COLLATE database_default,
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(7) COLLATE database_default,
           WBS3 Nvarchar(7) COLLATE database_default,
           LaborCode Nvarchar(14) COLLATE database_default
           PRIMARY KEY(TaskID))

	DECLARE @tabJTDCalendar 
	  TABLE (StartDate datetime,
           EndDate datetime,
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(StartDate)) 

	DECLARE @tabLD 
	  TABLE (RowID  int IDENTITY(1,1),
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(7) COLLATE database_default,
           WBS3 Nvarchar(7) COLLATE database_default,
           LaborCode Nvarchar(14) COLLATE database_default,
           Employee Nvarchar(20) COLLATE database_default,
           Category smallint,
           StartDate datetime,
           EndDate datetime,
           PeriodHrs decimal(19,4), 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4),
           PostedFlg smallint
           PRIMARY KEY(RowID, WBS1, WBS2, WBS3, LaborCode, Employee, Category, StartDate))

	DECLARE @tabLedger 
	  TABLE (RowID int IDENTITY(1,1),
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(7) COLLATE database_default,
           WBS3 Nvarchar(7) COLLATE database_default,
           Account Nvarchar(13) COLLATE database_default,
           Vendor Nvarchar(30) COLLATE database_default,
           Unit Nvarchar(30) COLLATE database_default,
           UnitTable Nvarchar(30) COLLATE database_default,
           TransType Nvarchar(2) COLLATE database_default,
           StartDate datetime,
           EndDate datetime,
           PeriodQty decimal(19,4), 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4),
           PostedFlg smallint
           PRIMARY KEY(RowID, WBS1, WBS2, WBS3, StartDate))

	DECLARE @tabPlanJTD 
	  TABLE (PlanID varchar(32) COLLATE database_default, 
           JTDLabHrs decimal(19,4), 
           JTDLabCost decimal(19,4), 
           JTDLabBill decimal(19,4), 
           JTDExpCost decimal(19,4), 
           JTDExpBill decimal(19,4), 
           JTDDirExpCost decimal(19,4), 
           JTDDirExpBill decimal(19,4),
           JTDConCost decimal(19,4), 
           JTDConBill decimal(19,4), 
           JTDDirConCost decimal(19,4), 
           JTDDirConBill decimal(19,4), 
           JTDUntQty decimal(19,4), 
           JTDUntCost decimal(19,4), 
           JTDUntBill decimal(19,4), 
           JTDDirUntCost decimal(19,4), 
           JTDDirUntBill decimal(19,4),
           JTDRevenue decimal(19,4) 
           PRIMARY KEY(PlanID)) 

	DECLARE @tabJTDLab
	  TABLE (RowID int IDENTITY(1,1),
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         AssignmentID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodHrs decimal(19,4), 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PostedFlg smallint 
           PRIMARY KEY(RowID, PlanID, TaskID, StartDate, EndDate)) 

	DECLARE @tabJTDExp 
	  TABLE (RowID int IDENTITY(1,1),
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         ExpenseID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PostedFlg smallint 
           PRIMARY KEY(RowID, PlanID, TaskID, StartDate, EndDate)) 

	DECLARE @tabJTDCon
	  TABLE (RowID int IDENTITY(1,1),
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         ConsultantID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PostedFlg smallint 
           PRIMARY KEY(RowID, PlanID, TaskID, StartDate, EndDate)) 

	DECLARE @tabJTDUnt
	  TABLE (RowID int IDENTITY(1,1),
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         UnitID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodQty decimal(19,4), 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PostedFlg smallint 
           PRIMARY KEY(RowID, PlanID, TaskID, StartDate, EndDate))

	DECLARE @tabTaskJTD 
	  TABLE (TaskID varchar(32) COLLATE database_default, 
           JTDLabHrs decimal(19,4), 
           JTDLabCost decimal(19,4), 
           JTDLabBill decimal(19,4), 
           JTDExpCost decimal(19,4), 
           JTDExpBill decimal(19,4), 
           JTDConCost decimal(19,4), 
           JTDConBill decimal(19,4), 
           JTDUntQty decimal(19,4), 
           JTDUntCost decimal(19,4), 
           JTDUntBill decimal(19,4), 
           UnmatchedFlg varchar(1) COLLATE database_default DEFAULT 'N' 
           PRIMARY KEY(TaskID)) 

	DECLARE @tabAssignmentJTD 
	  TABLE (AssignmentID varchar(32) COLLATE database_default, 
           JTDLabHrs decimal(19,4), 
           JTDLabCost decimal(19,4), 
           JTDLabBill decimal(19,4) 
           PRIMARY KEY(AssignmentID)) 

	DECLARE @tabExpenseJTD 
	  TABLE (ExpenseID varchar(32) COLLATE database_default, 
           JTDExpCost decimal(19,4), 
           JTDExpBill decimal(19,4) 
           PRIMARY KEY(ExpenseID)) 

	DECLARE @tabConsultantJTD 
	  TABLE (ConsultantID varchar(32) COLLATE database_default, 
           JTDConCost decimal(19,4), 
           JTDConBill decimal(19,4) 
           PRIMARY KEY(ConsultantID)) 

	DECLARE @tabUnitJTD 
	  TABLE (UnitID varchar(32) COLLATE database_default, 
           JTDUntQty decimal(19,4), 
           JTDUntCost decimal(19,4), 
           JTDUntBill decimal(19,4) 
           PRIMARY KEY(UnitID)) 

  -- Save Active Company string for use later.
  
  SET @strCompany = dbo.GetActiveCompany()
  SET @strUserName = dbo.GetVisionAuditUserName()
          
  -- Set JTD/ETC Dates.
  
  SET @dtETCDate = CONVERT(datetime, REPLACE(@strETCDate, '-', ''))
  SET @dtJTDDate = DATEADD(d, -1, @dtETCDate) 

  -- Save Calendar Intervals into a temp table and add an overflow Calendar Interval.
  
  INSERT @tabCalendarInterval(PlanID, StartDate, EndDate)
  SELECT PlanID, StartDate, EndDate
    FROM
      (SELECT PlanID AS PlanID, StartDate AS StartDate, EndDate AS EndDate
         FROM RPCalendarInterval WHERE PlanID = @strPlanID
       UNION ALL
       SELECT PlanID AS PlanID, DATEADD(d, 1, MAX(EndDate)) AS StartDate,
         DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate
         FROM RPCalendarInterval WHERE PlanID = @strPlanID
         GROUP BY PlanID) AS CI

  -- Get decimal settings.
  
  SELECT @intHrDecimals = HrDecimals,
         @intQtyDecimals = QtyDecimals,
         @intAmtCostDecimals = AmtCostDecimals,
         @intAmtBillDecimals = AmtBillDecimals,
         @intRtCostDecimals = RtCostDecimals,
         @intRtBillDecimals = RtBillDecimals,
         @intLabRevDecimals = LabRevDecimals, 
         @intECURevDecimals = ECURevDecimals
    FROM dbo.RP$tabDecimals(@strPlanID)
    
  -- Get flags to determine which features are being used.
  
  SELECT
     @strExpTab = ExpTab,
     @strConTab = ConTab,
     @strUntTab = UntTab,
     @strFeesByPeriod = FeesByPeriodFlg,
     @strEffectiveDateEnabled = EffectiveDateEnabled
     FROM CFGResourcePlanning
     WHERE CFGResourcePlanning.Company = @strCompany
     
  -- Get Plan parameters to be used later.
  
  SELECT
    @siCostRtMethod = CostRtMethod, 
    @siBillingRtMethod = BillingRtMethod,
    @intCostRtTableNo = CostRtTableNo,
    @intBillingRtTableNo = BillingRtTableNo,
    @siGRMethod = GRMethod,
    @intGenResTableNo = GenResTableNo,
    @intGRBillTableNo = GRBillTableNo,
    @strUnposted = UnpostedFlg,
    @strCommitmentFlg = CommitmentFlg
    FROM RPPlan WHERE PlanID = @strPlanID
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
    INSERT @tabTopTask(TaskID, WBS1, WBS2, WBS3, LaborCode)
    SELECT DISTINCT TaskID, WBS1, WBS2, WBS3, LaborCode
        FROM RPTask AS T
        INNER JOIN (SELECT MIN(OutlineLevel) AS OutlineLevel
            FROM RPTask AS XT INNER JOIN RPWBSLevelFormat AS F ON XT.PlanID = F.PlanID
            AND XT.WBSType = F.WBSType AND F.WBSMatch = 'Y'
            WHERE XT.PlanID = @strPlanID) AS ZT ON T.OutlineLevel = ZT.OutlineLevel
        WHERE T.PlanID = @strPlanID AND T.WBS1 IS NOT NULL AND T.WBS1 != '<none>'
    
	IF (@@ROWCOUNT > 0)
	    BEGIN
	    
	    -- Compute data for JTD Calendar table with overflow periods before and after the regular calendar.

	    INSERT @tabJTDCalendar(StartDate, EndDate, PeriodScale)
		    SELECT StartDate, EndDate, PeriodScale FROM
			    (SELECT StartDate AS StartDate, EndDate AS EndDate, PeriodScale AS PeriodScale
			        FROM RPCalendarInterval WHERE PlanID = @strPlanID
			        UNION ALL
			        SELECT CAST('19000101' AS datetime) AS StartDate,
			        DATEADD(d, -1, MIN(StartDate)) AS EndDate,
			        'o' AS PeriodScale
			        FROM RPCalendarInterval WHERE PlanID = @strPlanID
			        GROUP BY PlanID
			        UNION ALL
			        SELECT DATEADD(d, 1, MAX(EndDate)) AS StartDate,
			        DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate,
			        'o' AS PeriodScale
			        FROM RPCalendarInterval WHERE PlanID = @strPlanID
			    ) AS CI
        
        -- Initialize control variables to indicate whether we have JTD data of certain type.

	    SET @strHasJTDLab = 'N'
	    SET @strHasJTDECU = 'N'

        --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	    -- Save Labor JTD for this Plan into temp table.

	    INSERT @tabLD
			(   WBS1  ,
				WBS2  ,
				WBS3  ,
				LaborCode  ,
				Employee  ,
				Category ,
				StartDate  ,
				EndDate  ,
				PeriodHrs  , 
				PeriodCost  , 
				PeriodBill  ,
				PostedFlg  )
		    SELECT
			    LD.WBS1,
			    LD.WBS2,
			    LD.WBS3,
			    ISNULL(LD.LaborCode, '%') AS LaborCode,
			    ISNULL(LD.Employee, '') AS Employee,
			    LD.Category,
			    CI.StartDate, 
			    CI.EndDate, 
			    SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs, 
			    SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost, 
			    SUM(BillExt) AS PeriodBill, 
			    1 AS PostedFlg
			    FROM LD WITH (INDEX(LDWBS1TransDateIDX))
				    INNER JOIN @tabTopTask AS T ON (LD.WBS1 = T.WBS1
                AND LD.ProjectCost = 'Y' AND LD.TransDate <= @dtJTDDate
                AND LD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
                AND LD.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%')))
				    INNER JOIN @tabJTDCalendar AS CI ON LD.TransDate BETWEEN CI.StartDate AND CI.EndDate 
			    GROUP BY LD.WBS1, LD.WBS2, LD.WBS3, LD.LaborCode, LD.Employee, LD.Category, CI.StartDate, CI.EndDate
        		
	    IF (@@ROWCOUNT > 0) SET @strHasJTDLab = 'Y'

	    -- Save Unposted Labor JTD into temp table.

        IF (@strUnposted = 'Y')
        BEGIN
        
	        INSERT @tabLD
			(   WBS1  ,
				WBS2  ,
				WBS3  ,
				LaborCode  ,
				Employee  ,
				Category ,
				StartDate  ,
				EndDate  ,
				PeriodHrs  , 
				PeriodCost  , 
				PeriodBill  ,
				PostedFlg  )	        
		        SELECT
			        WBS1,
			        WBS2,
			        WBS3,
			        ISNULL(LaborCode, '%') AS LaborCode,
			        ISNULL(Employee, '') AS Employee,
			        Category,
			        StartDate, 
			        EndDate, 
			        SUM(PeriodHrs) AS PeriodHrs, 
			        SUM(PeriodCost) AS PeriodCost, 
			        SUM(PeriodBill) AS PeriodBill, 
			        -1 AS PostedFlg FROM
				        (SELECT
                    TD.WBS1,
                    TD.WBS2,
                    TD.WBS3,
                    TD.LaborCode,
                    TD.Employee,
                    TD.BillCategory AS Category,
                    CI.StartDate, 
                    CI.EndDate, 
                    SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs, 
                    SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost, 
                    SUM(BillExt) AS PeriodBill
                    FROM tkDetail AS TD
                        INNER JOIN @tabTopTask AS T ON (TD.WBS1 = T.WBS1
                        AND TD.TransDate <= @dtJTDDate
                        AND TD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
                        AND TD.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                        AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%')))
                        INNER JOIN @tabJTDCalendar AS CI ON TD.TransDate BETWEEN CI.StartDate AND CI.EndDate
                        INNER JOIN EM ON TD.Employee = EM.Employee  
                        INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
                    GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.LaborCode, TD.Employee, TD.BillCategory, CI.StartDate, CI.EndDate
                    UNION ALL
                    SELECT
                    TD.WBS1,
                    TD.WBS2,
                    TD.WBS3,
                    TD.LaborCode,
                    TD.Employee,
                    TD.BillCategory AS Category,
                    CI.StartDate, 
                    CI.EndDate, 
                    SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs, 
                    SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost, 
                    SUM(BillExt) AS PeriodBill
                    FROM tsDetail AS TD
                        INNER JOIN @tabTopTask AS T ON (TD.WBS1 = T.WBS1
                        AND TD.TransDate <= @dtJTDDate
                        AND TD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
                        AND TD.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                        AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%')))
                        INNER JOIN @tabJTDCalendar AS CI ON TD.TransDate BETWEEN CI.StartDate AND CI.EndDate
                        INNER JOIN EM ON TD.Employee = EM.Employee  
                        INNER JOIN tsControl AS TC ON (TC.Posted = 'N' AND TD.Batch = TC.Batch)
                    GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.LaborCode, TD.Employee, TD.BillCategory, CI.StartDate, CI.EndDate
					        )  AS X
                GROUP BY WBS1, WBS2, WBS3, LaborCode, Employee, Category, StartDate, EndDate
                
            IF (@@ROWCOUNT > 0) SET @strHasJTDLab = 'Y'
            
        END -- If-Then
        
    -->>>>>

        IF (@strHasJTDLab = 'Y')
        BEGIN
        
            INSERT @tabJTDLab
            (   PlanID,
                TaskID,  
                AssignmentID,  
                StartDate,  
                EndDate,  
                PeriodHrs,  
                PeriodCost,  
                PeriodBill,  
                PostedFlg)
            SELECT
                @strPlanID AS PlanID,
                TaskID,  
                AssignmentID,  
                StartDate,  
                EndDate,  
                SUM(ISNULL(PeriodHrs, 0)) AS PeriodHrs,  
                SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,  
                SUM(ISNULL(X.PeriodBill, 0)) AS PeriodBill,  
                SIGN(MIN(PostedFlg) + MAX(PostedFlg)) AS PostedFlg
                FROM
                (SELECT A.AssignmentID,  
                    A.TaskID,  
                    LD.StartDate,  
                    LD.EndDate,  
                    SUM(PeriodHrs) AS PeriodHrs,  
                    SUM(PeriodCost) AS PeriodCost,  
                    SUM(PeriodBill) AS PeriodBill,  
                    PostedFlg
                    FROM @tabLD AS LD
                    INNER JOIN RPAssignment AS A ON (LD.WBS1 = A.WBS1
                        AND LD.WBS2 LIKE (ISNULL(A.WBS2, '%'))  
                        AND LD.WBS3 LIKE (ISNULL(A.WBS3, '%'))  
                        AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(A.LaborCode, '%'))  
                        AND LD.Employee = A.ResourceID)  
                    INNER JOIN (SELECT MIN(AssignmentID) AS AssignmentID FROM RPAssignment  
                        WHERE PlanID = @strPlanID AND ResourceID IS NOT NULL  
                        GROUP BY TaskID, ResourceID) AS A1 ON A.AssignmentID = A1.AssignmentID  
                    WHERE A.PlanID = @strPlanID  
                    GROUP BY A.AssignmentID, A.TaskID, LD.StartDate, LD.EndDate, PostedFlg
                UNION ALL -- Category as Generic Resources.
                SELECT AC.AssignmentID,  
                    AC.TaskID,  
                    LD.StartDate,  
                    LD.EndDate,  
                    SUM(PeriodHrs) AS PeriodHrs,  
                    SUM(PeriodCost) AS PeriodCost,  
                    SUM(PeriodBill) AS PeriodBill,  
                    PostedFlg
                    FROM @tabLD AS LD 
                    INNER JOIN RPAssignment AS AC ON (LD.WBS1 = AC.WBS1 
                        AND AC.ResourceID IS NULL AND AC.GRLBCD IS NULL  
                        AND LD.WBS2 LIKE (ISNULL(AC.WBS2, '%'))  
                        AND LD.WBS3 LIKE (ISNULL(AC.WBS3, '%'))  
                        AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(AC.LaborCode, '%'))  
                        AND LD.Category = AC.Category)  
                    INNER JOIN (SELECT MIN(AssignmentID) AS AssignmentID FROM RPAssignment  
                        WHERE PlanID = @strPlanID AND ResourceID IS NULL AND GRLBCD IS NULL  
                        GROUP BY TaskID, Category) AS A1 ON AC.AssignmentID = A1.AssignmentID  
                    WHERE AC.PlanID = @strPlanID
                    AND (LD.Employee NOT IN (SELECT A2.ResourceID FROM RPAssignment AS A2  
                        WHERE A2.TaskID = AC.TaskID AND A2.ResourceID IS NOT NULL AND A2.PlanID = @strPlanID))  
                    GROUP BY AC.AssignmentID, AC.TaskID, LD.StartDate, LD.EndDate, PostedFlg
                UNION ALL -- Labor Code as Generic Resources.
                SELECT AL.AssignmentID,  
                    AL.TaskID,  
                    LD.StartDate,  
                    LD.EndDate,  
                    SUM(PeriodHrs) AS PeriodHrs,  
                    SUM(PeriodCost) AS PeriodCost,  
                    SUM(PeriodBill) AS PeriodBill,  
                    PostedFlg
                    FROM @tabLD AS LD 
                    INNER JOIN RPAssignment AS AL ON (LD.WBS1 = AL.WBS1 
                        AND AL.ResourceID IS NULL AND AL.Category = 0  
                        AND LD.WBS2 LIKE (ISNULL(AL.WBS2, '%'))  
                        AND LD.WBS3 LIKE (ISNULL(AL.WBS3, '%'))  
                        AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(AL.LaborCode, '%'))  
                        AND ISNULL(LD.LaborCode, '%') LIKE AL.GRLBCD)  
                    INNER JOIN (SELECT MIN(AssignmentID) AS AssignmentID FROM RPAssignment  
                        WHERE PlanID = @strPlanID AND ResourceID IS NULL AND Category = 0  
                        GROUP BY TaskID, GRLBCD) AS A1 ON AL.AssignmentID = A1.AssignmentID  
                    WHERE AL.PlanID = @strPlanID
                    AND (LD.Employee NOT IN (SELECT A2.ResourceID FROM RPAssignment AS A2  
                        WHERE A2.TaskID = AL.TaskID AND A2.ResourceID IS NOT NULL AND A2.PlanID = @strPlanID))  
                    GROUP BY AL.AssignmentID, AL.TaskID, LD.StartDate, LD.EndDate, PostedFlg
                UNION ALL
                SELECT NULL AS AssignmentID,  
                    T.TaskID,  
                    LD.StartDate,  
                    LD.EndDate,  
                    SUM(PeriodHrs) AS PeriodHrs,  
                    SUM(PeriodCost) AS PeriodCost,  
                    SUM(PeriodBill) AS PeriodBill,  
                    PostedFlg
                    FROM @tabLD AS LD
                    INNER JOIN RPTask AS T ON (LD.WBS1 = T.WBS1
                        AND LD.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                        AND LD.WBS3 LIKE (ISNULL(T.WBS3, '%'))  
                        AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%')))
                    INNER JOIN RPWBSLevelFormat AS F ON T.PlanID = F.PlanID AND T.WBSType = F.WBSType AND F.WBSMatch = 'Y'
                    WHERE T.PlanID = @strPlanID  
                    GROUP BY T.TaskID, LD.StartDate, LD.EndDate, PostedFlg) AS X
                GROUP BY TaskID, AssignmentID, StartDate, EndDate
                
            -- Sum up.
            
            DECLARE csrTask CURSOR FOR 
            SELECT DISTINCT T.TaskID, T.OutlineNumber FROM RPTask AS T 
                LEFT JOIN @tabJTDLab AS TPD ON T.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL
                WHERE T.PlanID = @strPlanID AND TPD.TaskID IS NULL 
                ORDER BY T.OutlineNumber DESC 

            OPEN csrTask 

            FETCH NEXT FROM csrTask INTO @strTaskID, @strOutlineNumber 

            WHILE (@@FETCH_STATUS = 0) 
            BEGIN 

                INSERT @tabJTDLab
                (   PlanID,
                    TaskID,  
                    AssignmentID,  
                    StartDate,  
                    EndDate,  
                    PeriodHrs,  
                    PeriodCost,  
                    PeriodBill,  
                    PostedFlg)
                SELECT
                    @strPlanID AS PlanID,
                    @strTaskID AS TaskID,
                    NULL AS AssignmentID,
                    StartDate AS StartDate,
                    EndDate AS EndDate,
                    SUM(ISNULL(PeriodHrs, 0)) AS PeriodHrs, 
                    SUM(ISNULL(PeriodCost, 0)) AS PeriodCost, 
                    SUM(ISNULL(PeriodBill, 0)) AS PeriodBill, 
                    SIGN(MIN(ISNULL(PostedFlg, 0)) + MAX(ISNULL(PostedFlg, 0))) AS PostedFlg
                    FROM 
                    (SELECT 
                        @strTaskID AS TaskID, 
                        TPD.StartDate AS StartDate,
                        TPD.EndDate AS EndDate, 
                        SUM(ISNULL(TPD.PeriodHrs, 0)) AS PeriodHrs, 
                        SUM(ISNULL(TPD.PeriodCost, 0)) AS PeriodCost, 
                        SUM(ISNULL(TPD.PeriodBill, 0)) AS PeriodBill, 
                        TPD.PostedFlg AS PostedFlg 
                        FROM RPTask AS CT 
                            INNER JOIN @tabJTDLab AS TPD ON TPD.TaskID = CT.TaskID AND TPD.AssignmentID IS NULL
                        WHERE CT.PlanID = @strPlanID AND CT.ParentOutlineNumber = @strOutlineNumber 
                        GROUP BY TPD.StartDate, TPD.EndDate, TPD.PostedFlg
                        UNION ALL
                        SELECT 
                        @strTaskID AS TaskID, 
                        TPD.StartDate AS StartDate,
                        TPD.EndDate AS EndDate, 
                        SUM(ISNULL(TPD.PeriodHrs, 0)) AS PeriodHrs, 
                        SUM(ISNULL(TPD.PeriodCost, 0)) AS PeriodCost, 
                        SUM(ISNULL(TPD.PeriodBill, 0)) AS PeriodBill, 
                        TPD.PostedFlg AS PostedFlg 
                        FROM @tabJTDLab AS TPD 
                        WHERE TPD.TaskID = @strTaskID AND TPD.AssignmentID IS NOT NULL 
                        GROUP BY TPD.StartDate, TPD.EndDate, TPD.PostedFlg) AS XTPD 
                    GROUP BY StartDate, EndDate

                FETCH NEXT FROM csrTask INTO @strTaskID, @strOutlineNumber 

            END -- While
            
            CLOSE csrTask 
            DEALLOCATE csrTask
            
            -- Sum across for Assignment rows.
            
            INSERT @tabAssignmentJTD 
            (AssignmentID,
                JTDLabHrs,
                JTDLabCost,
                JTDLabBill)
            SELECT 
                AssignmentID,
                SUM(ISNULL(PeriodHrs, 0)) AS JTDLabHrs,
                SUM(ISNULL(PeriodCost, 0)) AS JTDLabCost,
                SUM(ISNULL(PeriodBill, 0)) AS JTDLabBill
                FROM @tabJTDLab WHERE AssignmentID IS NOT NULL
                GROUP BY AssignmentID

        END -- If-Then (@strHasJTDLab = 'Y')
        
    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        -- Save Expense + Consultant + Unit JTD for this Plan into temp table.
        
  	    INSERT @tabLedger
  	           (WBS1  ,
				WBS2  ,
				WBS3  ,
				Account  ,
				Vendor  ,
				Unit  ,
				UnitTable  ,
				TransType  ,
				StartDate  ,
				EndDate  ,
				PeriodQty  , 
				PeriodCost  , 
				PeriodBill  ,
				PostedFlg  )
		    SELECT
			    WBS1,
			    WBS2,
			    WBS3,
			    Account,
			    Vendor,
			    Unit,
			    UnitTable,
			    TransType,
			    StartDate, 
			    EndDate, 
			    SUM(ISNULL(PeriodQty, 0)) AS PeriodQty, 
			    SUM(ISNULL(PeriodCost, 0)) AS PeriodCost, 
			    SUM(ISNULL(PeriodBill, 0)) AS PeriodBill, 
			    1 AS PostedFlg
			    FROM
			        (SELECT
			        Ledger.WBS1 AS WBS1,
			        Ledger.WBS2 AS WBS2,
			        Ledger.WBS3 AS WBS3,
			        Ledger.Account AS Account,
			        Ledger.Vendor AS Vendor,
			        NULL AS Unit,
			        NULL AS UNitTable,
			        '*' AS TransType,
			        CI.StartDate AS StartDate,
			        CI.EndDate AS EndDate,
			        0 PeriodQty,
                SUM(AmountProjectCurrency) AS PeriodCost,  
                SUM(BillExt) AS PeriodBill
                FROM LedgerAR AS Ledger
                INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                    AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                    AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                    AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                INNER JOIN @tabJTDCalendar AS CI ON Ledger.TransDate BETWEEN CI.StartDate AND CI.EndDate 
			        GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, Ledger.Account, Ledger.Vendor, CI.StartDate, CI.EndDate
			        UNION ALL
			        SELECT
			        Ledger.WBS1 AS WBS1,
			        Ledger.WBS2 AS WBS2,
			        Ledger.WBS3 AS WBS3,
			        Ledger.Account AS Account,
			        Ledger.Vendor AS Vendor,
			        NULL AS Unit,
			        NULL AS UNitTable,
			        '*' AS TransType,
			        CI.StartDate AS StartDate,
			        CI.EndDate AS EndDate,
			        0 PeriodQty,
                SUM(AmountProjectCurrency) AS PeriodCost,  
                SUM(BillExt) AS PeriodBill
                FROM LedgerAP AS Ledger
                INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                    AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                    AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                    AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                INNER JOIN @tabJTDCalendar AS CI ON Ledger.TransDate BETWEEN CI.StartDate AND CI.EndDate 
			        GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, Ledger.Account, Ledger.Vendor, CI.StartDate, CI.EndDate
			        UNION ALL
			        SELECT
			        Ledger.WBS1 AS WBS1,
			        Ledger.WBS2 AS WBS2,
			        Ledger.WBS3 AS WBS3,
			        Ledger.Account AS Account,
			        Ledger.Vendor AS Vendor,
			        NULL AS Unit,
			        NULL AS UNitTable,
			        '*' AS TransType,
			        CI.StartDate AS StartDate,
			        CI.EndDate AS EndDate,
			        0 PeriodQty,
                SUM(AmountProjectCurrency) AS PeriodCost,  
                SUM(BillExt) AS PeriodBill
                FROM LedgerEX AS Ledger
                INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                    AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                    AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                    AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                INNER JOIN @tabJTDCalendar AS CI ON Ledger.TransDate BETWEEN CI.StartDate AND CI.EndDate 
			        GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, Ledger.Account, Ledger.Vendor, CI.StartDate, CI.EndDate
			        UNION ALL
			        SELECT
			        Ledger.WBS1 AS WBS1,
			        Ledger.WBS2 AS WBS2,
			        Ledger.WBS3 AS WBS3,
			        Ledger.Account AS Account,
			        Ledger.Vendor AS Vendor,
			        Ledger.Unit AS Unit,
			        Ledger.UnitTable AS UnitTable,
			        CASE WHEN Ledger.TransType = 'UN' THEN Ledger.TransType ELSE '*' END AS TransType,
			        CI.StartDate AS StartDate,
			        CI.EndDate AS EndDate,
			        SUM(UnitQuantity) AS PeriodQty,
                SUM(AmountProjectCurrency) AS PeriodCost,  
                SUM(BillExt) AS PeriodBill
                FROM LedgerMISC AS Ledger
                INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                    AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                    AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                    AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                INNER JOIN @tabJTDCalendar AS CI ON Ledger.TransDate BETWEEN CI.StartDate AND CI.EndDate 
			        GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, Ledger.Account, Ledger.Vendor, 
			            Ledger.Unit, Ledger.UnitTable, Ledger.TransType, CI.StartDate, CI.EndDate
			        ) AS X
			    GROUP BY WBS1, WBS2, WBS3, Account, Vendor, Unit, UnitTable, TransType, StartDate, EndDate

	    IF (@@ROWCOUNT > 0) SET @strHasJTDECU = 'Y'

	    -- Save Unposted Expense + Consultant + Unit JTD into temp table.

        IF (@strCommitmentFlg = 'Y')
        BEGIN
        
  	        INSERT @tabLedger
  	           (WBS1  ,
				WBS2  ,
				WBS3  ,
				Account  ,
				Vendor  ,
				Unit  ,
				UnitTable  ,
				TransType  ,
				StartDate  ,
				EndDate  ,
				PeriodQty  , 
				PeriodCost  , 
				PeriodBill  ,
				PostedFlg  )
		        SELECT
			        WBS1,
			        WBS2,
			        WBS3,
			        Account,
			        Vendor,
			        NULL AS Unit,
  			        NULL AS UNitTable,
			        '*' AS TransType,
			        StartDate, 
			        EndDate, 
			        0 AS PeriodQty, 
			        SUM(ISNULL(PeriodCost, 0)) AS PeriodCost, 
			        SUM(ISNULL(PeriodBill, 0)) AS PeriodBill, 
			        -1 AS PostedFlg
			        FROM
			            (SELECT -- POC with no Change Orders.
                    POC.WBS1 AS WBS1,
                    POC.WBS2 AS WBS2,
                    POC.WBS3 AS WBS3,
                    POC.Account AS Account,
                    POM.Vendor AS Vendor,
		                CI.StartDate AS StartDate,
			            CI.EndDate AS EndDate,
                    SUM(AmountProjectCurrency) AS PeriodCost,  
                    SUM(BillExt) AS PeriodBill
                    FROM POCommitment AS POC 
                    INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
                    INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey
                    INNER JOIN @tabTopTask AS T ON (POC.WBS1 = T.WBS1
                        AND POC.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                        AND POC.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                    INNER JOIN @tabJTDCalendar AS CI ON POM.OrderDate BETWEEN CI.StartDate AND CI.EndDate
                    WHERE POM.OrderDate <= @dtJTDDate
                    AND AmountProjectCurrency != 0 AND BillExt != 0
			            GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor, CI.StartDate, CI.EndDate
			            UNION ALL
			            SELECT -- POC with Change Orders.
                    POC.WBS1 AS WBS1,
                    POC.WBS2 AS WBS2,
                    POC.WBS3 AS WBS3,
                    POC.Account AS Account,
                    POM.Vendor AS Vendor,
		                CI.StartDate AS StartDate,
			            CI.EndDate AS EndDate,
                    SUM(AmountProjectCurrency) AS PeriodCost,  
                    SUM(BillExt) AS PeriodBill
                    FROM POCommitment AS POC 
                    INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
                    INNER JOIN POCOMaster AS POCOM ON POD.COPKey = POCOM.PKey
                    INNER JOIN POMaster AS POM ON POCOM.MasterPKey = POM.MasterPKey
                    INNER JOIN @tabTopTask AS T ON (POC.WBS1 = T.WBS1
                        AND POC.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                        AND POC.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                    INNER JOIN @tabJTDCalendar AS CI ON POCOM.OrderDate BETWEEN CI.StartDate AND CI.EndDate
                    WHERE POCOM.OrderDate <= @dtJTDDate
                    AND AmountProjectCurrency != 0 AND BillExt != 0
			            GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor, CI.StartDate, CI.EndDate
                ) AS X
			        GROUP BY WBS1, WBS2, WBS3, Account, Vendor, StartDate, EndDate
                
            IF (@@ROWCOUNT > 0) SET @strHasJTDECU = 'Y'
            
        END -- If-Then (@strUnposted = 'Y')

    -->>>>>

        IF (@strHasJTDECU = 'Y')
        BEGIN
        
            --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            -- Expense.                                                                                               +
            --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            
            IF (@strExpTab = 'Y')
            BEGIN
            
                INSERT @tabJTDExp
                (   PlanID,
                    TaskID,  
                    ExpenseID,  
                    StartDate,  
                    EndDate,  
                    PeriodCost,  
                    PeriodBill,  
                    PostedFlg)
                SELECT
                    @strPlanID AS PlanID,
                    TaskID,  
                    ExpenseID,  
                    StartDate,  
                    EndDate,  
                    SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,  
                    SUM(ISNULL(X.PeriodBill, 0)) AS PeriodBill,  
                    SIGN(MIN(PostedFlg) + MAX(PostedFlg)) AS PostedFlg
                    FROM
                    (SELECT E.ExpenseID,  
                        E.TaskID,  
                        Ledger.StartDate,  
                        Ledger.EndDate,  
                        SUM(PeriodCost) AS PeriodCost,  
                        SUM(PeriodBill) AS PeriodBill,  
                        PostedFlg
                        FROM @tabLedger AS Ledger
                            INNER JOIN RPExpense AS E ON (Ledger.WBS1 = E.WBS1
                            AND Ledger.WBS2 LIKE (ISNULL(E.WBS2, '%'))  
                            AND Ledger.WBS3 LIKE (ISNULL(E.WBS3, '%'))
                            AND Ledger.Account = E.Account
                            AND Ledger.TransType != (CASE WHEN @strUntTab = 'Y' THEN 'UN' ELSE '' END)
                            AND (ISNULL(Ledger.Vendor, '%')) LIKE (ISNULL(E.Vendor, '%')))  
                            INNER JOIN (SELECT MIN(ExpenseID) AS ExpenseID FROM RPExpense  
                            WHERE PlanID = @strPlanID  
                            GROUP BY TaskID, Account, Vendor) AS E1 ON E.ExpenseID = E1.ExpenseID
                        WHERE E.PlanID = @strPlanID  
                        GROUP BY E.ExpenseID, E.TaskID, Ledger.StartDate, Ledger.EndDate, PostedFlg
                        UNION ALL
                        SELECT NULL AS ExpenseID,  
                        T.TaskID,  
                        Ledger.StartDate,  
                        Ledger.EndDate,  
                        SUM(PeriodCost) AS PeriodCost,  
                        SUM(PeriodBill) AS PeriodBill,  
                        PostedFlg
                        FROM @tabLedger AS Ledger
                            INNER JOIN RPTask AS T ON (Ledger.WBS1 = T.WBS1
                              AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                              AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))  
                              AND Ledger.TransType != (CASE WHEN @strUntTab = 'Y' THEN 'UN' ELSE '' END))
                            INNER JOIN RPWBSLevelFormat AS F ON T.PlanID = F.PlanID AND T.WBSType = F.WBSType AND F.WBSMatch = 'Y'
                            INNER JOIN CA ON Ledger.Account = CA.Account
                        WHERE T.PlanID = @strPlanID AND CA.Type IN (5, 7)
                        GROUP BY T.TaskID, Ledger.StartDate, Ledger.EndDate, PostedFlg) AS X
                    GROUP BY TaskID, ExpenseID, StartDate, EndDate
                    
                -- Sum up.
                
                DECLARE csrTask CURSOR FOR 
                SELECT DISTINCT T.TaskID, T.OutlineNumber FROM RPTask AS T 
                    LEFT JOIN @tabJTDExp AS TPD ON T.TaskID = TPD.TaskID AND TPD.ExpenseID IS NULL
                    WHERE T.PlanID = @strPlanID AND TPD.TaskID IS NULL 
                    ORDER BY T.OutlineNumber DESC 

                OPEN csrTask 

                FETCH NEXT FROM csrTask INTO @strTaskID, @strOutlineNumber 

                WHILE (@@FETCH_STATUS = 0) 
                BEGIN 

                    INSERT @tabJTDExp
                    (   PlanID,
                        TaskID,  
                        ExpenseID,  
                        StartDate,  
                        EndDate,  
                        PeriodCost,  
                        PeriodBill,  
                        PostedFlg)
                    SELECT
                        @strPlanID AS PlanID,
                        @strTaskID AS TaskID,
                        NULL AS ExpenseID,
                        StartDate AS StartDate,
                        EndDate AS EndDate,
                        SUM(ISNULL(PeriodCost, 0)) AS PeriodCost, 
                        SUM(ISNULL(PeriodBill, 0)) AS PeriodBill, 
                        SIGN(MIN(ISNULL(PostedFlg, 0)) + MAX(ISNULL(PostedFlg, 0))) AS PostedFlg
                        FROM 
                        (SELECT 
                            @strTaskID AS TaskID, 
                            TPD.StartDate AS StartDate,
                            TPD.EndDate AS EndDate, 
                            SUM(ISNULL(TPD.PeriodCost, 0)) AS PeriodCost, 
                            SUM(ISNULL(TPD.PeriodBill, 0)) AS PeriodBill, 
                            TPD.PostedFlg AS PostedFlg 
                            FROM RPTask AS CT 
                                INNER JOIN @tabJTDExp AS TPD ON TPD.TaskID = CT.TaskID AND TPD.ExpenseID IS NULL
                            WHERE CT.PlanID = @strPlanID AND CT.ParentOutlineNumber = @strOutlineNumber 
                            GROUP BY TPD.StartDate, TPD.EndDate, TPD.PostedFlg
                            UNION ALL
                            SELECT 
                            @strTaskID AS TaskID, 
                            TPD.StartDate AS StartDate,
                            TPD.EndDate AS EndDate, 
                            SUM(ISNULL(TPD.PeriodCost, 0)) AS PeriodCost, 
                            SUM(ISNULL(TPD.PeriodBill, 0)) AS PeriodBill, 
                            TPD.PostedFlg AS PostedFlg 
                            FROM @tabJTDExp AS TPD 
                            WHERE TPD.TaskID = @strTaskID AND TPD.ExpenseID IS NOT NULL 
                            GROUP BY TPD.StartDate, TPD.EndDate, TPD.PostedFlg) AS XTPD 
                        GROUP BY StartDate, EndDate

                    FETCH NEXT FROM csrTask INTO @strTaskID, @strOutlineNumber 

                END -- While
                
                CLOSE csrTask 
                DEALLOCATE csrTask
                
                -- Sum across for Expense rows.
                
                INSERT @tabExpenseJTD 
                (ExpenseID,
                    JTDExpCost,
                    JTDExpBill)
                SELECT 
                    ExpenseID,
                    SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
                    SUM(ISNULL(PeriodBill, 0)) AS PeriodBill
                    FROM @tabJTDExp WHERE ExpenseID IS NOT NULL
                    GROUP BY ExpenseID
                            
            END -- If-Then (@strExpTab = 'Y')
            
            --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            -- Consultant.                                                                                            +
            --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            
            IF (@strConTab = 'Y')
            BEGIN
            
                INSERT @tabJTDCon
                (   PlanID,
                    TaskID,  
                    ConsultantID,  
                    StartDate,  
                    EndDate,  
                    PeriodCost,  
                    PeriodBill,  
                    PostedFlg)
                SELECT
                    @strPlanID AS PlanID,
                    TaskID,  
                    ConsultantID,  
                    StartDate,  
                    EndDate,  
                    SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,  
                    SUM(ISNULL(X.PeriodBill, 0)) AS PeriodBill,  
                    SIGN(MIN(PostedFlg) + MAX(PostedFlg)) AS PostedFlg
                    FROM
                    (SELECT C.ConsultantID,  
                        C.TaskID,  
                        Ledger.StartDate,  
                        Ledger.EndDate,  
                        SUM(PeriodCost) AS PeriodCost,  
                        SUM(PeriodBill) AS PeriodBill,  
                        PostedFlg
                        FROM @tabLedger AS Ledger
                            INNER JOIN RPConsultant AS C ON (Ledger.WBS1 = C.WBS1
                            AND Ledger.WBS2 LIKE (ISNULL(C.WBS2, '%'))  
                            AND Ledger.WBS3 LIKE (ISNULL(C.WBS3, '%'))
                            AND Ledger.Account = C.Account
                            AND (ISNULL(Ledger.Vendor, '%')) LIKE (ISNULL(C.Vendor, '%')))  
                            INNER JOIN (SELECT MIN(ConsultantID) AS ConsultantID FROM RPConsultant  
                            WHERE PlanID = @strPlanID  
                            GROUP BY TaskID, Account, Vendor) AS C1 ON C.ConsultantID = C1.ConsultantID
                        WHERE C.PlanID = @strPlanID  
                        GROUP BY C.ConsultantID, C.TaskID, Ledger.StartDate, Ledger.EndDate, PostedFlg
                        UNION ALL
                        SELECT NULL AS ConsultantID,  
                        T.TaskID,  
                        Ledger.StartDate,  
                        Ledger.EndDate,  
                        SUM(PeriodCost) AS PeriodCost,  
                        SUM(PeriodBill) AS PeriodBill,  
                        PostedFlg
                        FROM @tabLedger AS Ledger
                            INNER JOIN RPTask AS T ON (Ledger.WBS1 = T.WBS1
                              AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                              AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                            INNER JOIN RPWBSLevelFormat AS F ON T.PlanID = F.PlanID AND T.WBSType = F.WBSType AND F.WBSMatch = 'Y'
                            INNER JOIN CA ON Ledger.Account = CA.Account
                        WHERE T.PlanID = @strPlanID AND CA.Type IN (6, 8)
                        GROUP BY T.TaskID, Ledger.StartDate, Ledger.EndDate, PostedFlg) AS X
                    GROUP BY TaskID, ConsultantID, StartDate, EndDate
                    
                -- Sum up.
                
                DECLARE csrTask CURSOR FOR 
                SELECT DISTINCT T.TaskID, T.OutlineNumber FROM RPTask AS T 
                    LEFT JOIN @tabJTDCon AS TPD ON T.TaskID = TPD.TaskID AND TPD.ConsultantID IS NULL
                    WHERE T.PlanID = @strPlanID AND TPD.TaskID IS NULL 
                    ORDER BY T.OutlineNumber DESC 

                OPEN csrTask 

                FETCH NEXT FROM csrTask INTO @strTaskID, @strOutlineNumber 

                WHILE (@@FETCH_STATUS = 0) 
                BEGIN 

                    INSERT @tabJTDCon
                    (   PlanID,
                        TaskID,  
                        ConsultantID,  
                        StartDate,  
                        EndDate,  
                        PeriodCost,  
                        PeriodBill,  
                        PostedFlg)
                    SELECT
                        @strPlanID AS PlanID,
                        @strTaskID AS TaskID,
                        NULL AS ConsultantID,
                        StartDate AS StartDate,
                        EndDate AS EndDate,
                        SUM(ISNULL(PeriodCost, 0)) AS PeriodCost, 
                        SUM(ISNULL(PeriodBill, 0)) AS PeriodBill, 
                        SIGN(MIN(ISNULL(PostedFlg, 0)) + MAX(ISNULL(PostedFlg, 0))) AS PostedFlg
                        FROM 
                        (SELECT 
                            @strTaskID AS TaskID, 
                            TPD.StartDate AS StartDate,
                            TPD.EndDate AS EndDate, 
                            SUM(ISNULL(TPD.PeriodCost, 0)) AS PeriodCost, 
                            SUM(ISNULL(TPD.PeriodBill, 0)) AS PeriodBill, 
                            TPD.PostedFlg AS PostedFlg 
                            FROM RPTask AS CT 
                                INNER JOIN @tabJTDCon AS TPD ON TPD.TaskID = CT.TaskID AND TPD.ConsultantID IS NULL
                            WHERE CT.PlanID = @strPlanID AND CT.ParentOutlineNumber = @strOutlineNumber 
                            GROUP BY TPD.StartDate, TPD.EndDate, TPD.PostedFlg
                            UNION ALL
                            SELECT 
                            @strTaskID AS TaskID, 
                            TPD.StartDate AS StartDate,
                            TPD.EndDate AS EndDate, 
                            SUM(ISNULL(TPD.PeriodCost, 0)) AS PeriodCost, 
                            SUM(ISNULL(TPD.PeriodBill, 0)) AS PeriodBill, 
                            TPD.PostedFlg AS PostedFlg 
                            FROM @tabJTDCon AS TPD 
                            WHERE TPD.TaskID = @strTaskID AND TPD.ConsultantID IS NOT NULL 
                            GROUP BY TPD.StartDate, TPD.EndDate, TPD.PostedFlg) AS XTPD 
                        GROUP BY StartDate, EndDate

                    FETCH NEXT FROM csrTask INTO @strTaskID, @strOutlineNumber 

                END -- While
                
                CLOSE csrTask 
                DEALLOCATE csrTask
                
                -- Sum across for Consultant rows.
                
                INSERT @tabConsultantJTD
                (ConsultantID,
                    JTDConCost,
                    JTDConBill)
                SELECT 
                    ConsultantID,
                    SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
                    SUM(ISNULL(PeriodBill, 0)) AS PeriodBill
                    FROM @tabJTDCon WHERE ConsultantID IS NOT NULL
                    GROUP BY ConsultantID
                
            END -- If-Then (@strConTab = 'Y')

            --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            -- Unit.                                                                                                  +
            --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            
            IF (@strUntTab = 'Y')
            BEGIN

                INSERT @tabJTDUnt
                (   PlanID,
                    TaskID,  
                    UnitID,  
                    StartDate,  
                    EndDate,
                    PeriodQty,  
                    PeriodCost,  
                    PeriodBill,  
                    PostedFlg)
                SELECT
                    @strPlanID AS PlanID,
                    TaskID,  
                    UnitID,  
                    StartDate,  
                    EndDate,  
                    SUM(ISNULL(PeriodQty, 0)) AS PeriodQty,  
                    SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,  
                    SUM(ISNULL(X.PeriodBill, 0)) AS PeriodBill,  
                    SIGN(MIN(PostedFlg) + MAX(PostedFlg)) AS PostedFlg
                    FROM
                    (SELECT U.UnitID,  
                        U.TaskID,  
                        Ledger.StartDate,  
                        Ledger.EndDate,  
                        SUM(PeriodQty) AS PeriodQty,  
                        SUM(PeriodCost) AS PeriodCost,  
                        SUM(PeriodBill) AS PeriodBill,  
                        PostedFlg
                        FROM @tabLedger AS Ledger
                            INNER JOIN RPUnit AS U ON (Ledger.WBS1 = U.WBS1
                            AND Ledger.WBS2 LIKE (ISNULL(U.WBS2, '%'))  
                            AND Ledger.WBS3 LIKE (ISNULL(U.WBS3, '%'))
                            AND Ledger.TransType = 'UN' 
                            AND Ledger.Unit = U.Unit 
                            AND Ledger.UnitTable = U.UnitTable 
                            AND Ledger.Account = U.Account)  
                            INNER JOIN (SELECT MIN(UnitID) AS UnitID FROM RPUnit  
                            WHERE PlanID = @strPlanID  
                            GROUP BY TaskID, Unit, Account) AS U1 ON U.UnitID = U1.UnitID
                        WHERE U.PlanID = @strPlanID  
                        GROUP BY U.UnitID, U.TaskID, Ledger.StartDate, Ledger.EndDate, PostedFlg
                        UNION ALL
                        SELECT NULL AS UnitID,  
                        T.TaskID,  
                        Ledger.StartDate,  
                        Ledger.EndDate,  
                        SUM(PeriodQty) AS PeriodQty,  
                        SUM(PeriodCost) AS PeriodCost,  
                        SUM(PeriodBill) AS PeriodBill,  
                        PostedFlg
                        FROM @tabLedger AS Ledger
                            INNER JOIN RPTask AS T ON (Ledger.WBS1 = T.WBS1
                            AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                            AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))  
                            AND Ledger.TransType = 'UN')
                            INNER JOIN RPWBSLevelFormat AS F ON T.PlanID = F.PlanID AND T.WBSType = F.WBSType AND F.WBSMatch = 'Y'
                        WHERE T.PlanID = @strPlanID
                        GROUP BY T.TaskID, Ledger.StartDate, Ledger.EndDate, PostedFlg) AS X
                    GROUP BY TaskID, UnitID, StartDate, EndDate
                    
                -- Sum up.
                
                DECLARE csrTask CURSOR FOR 
                SELECT DISTINCT T.TaskID, T.OutlineNumber FROM RPTask AS T 
                    LEFT JOIN @tabJTDUnt AS TPD ON T.TaskID = TPD.TaskID AND TPD.UnitID IS NULL
                    WHERE T.PlanID = @strPlanID AND TPD.TaskID IS NULL 
                    ORDER BY T.OutlineNumber DESC 

                OPEN csrTask 

                FETCH NEXT FROM csrTask INTO @strTaskID, @strOutlineNumber 

                WHILE (@@FETCH_STATUS = 0) 
                BEGIN 

                    INSERT @tabJTDUnt
                    (   PlanID,
                        TaskID,  
                        UnitID,  
                        StartDate,  
                        EndDate, 
                        PeriodQty, 
                        PeriodCost,  
                        PeriodBill,  
                        PostedFlg)
                    SELECT
                        @strPlanID AS PlanID,
                        @strTaskID AS TaskID,
                        NULL AS UnitID,
                        StartDate AS StartDate,
                        EndDate AS EndDate,
                        SUM(ISNULL(PeriodQty, 0)) AS PeriodQty,
                        SUM(ISNULL(PeriodCost, 0)) AS PeriodCost, 
                        SUM(ISNULL(PeriodBill, 0)) AS PeriodBill, 
                        SIGN(MIN(ISNULL(PostedFlg, 0)) + MAX(ISNULL(PostedFlg, 0))) AS PostedFlg
                        FROM 
                        (SELECT 
                            @strTaskID AS TaskID, 
                            TPD.StartDate AS StartDate,
                            TPD.EndDate AS EndDate, 
                            SUM(ISNULL(TPD.PeriodQty, 0)) AS PeriodQty,
                            SUM(ISNULL(TPD.PeriodCost, 0)) AS PeriodCost, 
                            SUM(ISNULL(TPD.PeriodBill, 0)) AS PeriodBill, 
                            TPD.PostedFlg AS PostedFlg 
                            FROM RPTask AS CT 
                                INNER JOIN @tabJTDUnt AS TPD ON TPD.TaskID = CT.TaskID AND TPD.UnitID IS NULL
                            WHERE CT.PlanID = @strPlanID AND CT.ParentOutlineNumber = @strOutlineNumber 
                            GROUP BY TPD.StartDate, TPD.EndDate, TPD.PostedFlg
                            UNION ALL
                            SELECT 
                            @strTaskID AS TaskID, 
                            TPD.StartDate AS StartDate,
                            TPD.EndDate AS EndDate, 
                            SUM(ISNULL(TPD.PeriodQty, 0)) AS PeriodQty,
                            SUM(ISNULL(TPD.PeriodCost, 0)) AS PeriodCost, 
                            SUM(ISNULL(TPD.PeriodBill, 0)) AS PeriodBill, 
                            TPD.PostedFlg AS PostedFlg 
                            FROM @tabJTDUnt AS TPD 
                            WHERE TPD.TaskID = @strTaskID AND TPD.UnitID IS NOT NULL 
                            GROUP BY TPD.StartDate, TPD.EndDate, TPD.PostedFlg) AS XTPD 
                        GROUP BY StartDate, EndDate

                    FETCH NEXT FROM csrTask INTO @strTaskID, @strOutlineNumber 

                END -- While
                
                CLOSE csrTask 
                DEALLOCATE csrTask
                
                -- Sum across for Unit rows.
                
                INSERT @tabUnitJTD 
                (UnitID,
                    JTDUntQty,
                    JTDUntCost,
                    JTDUntBill)
                SELECT 
                    UnitID,
                    SUM(ISNULL(PeriodQty, 0)) AS PeriodQty,
                    SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
                    SUM(ISNULL(PeriodBill, 0)) AS PeriodBill
                    FROM @tabJTDUnt WHERE UnitID IS NOT NULL
                    GROUP BY UnitID
                        
            END -- If-Then (@strUntTab = 'Y')
            
        END -- If-Then (@strHasJTDECU = 'Y')

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        -- Sum across for Task rows.
        
        INSERT @tabTaskJTD
        (TaskID,
            JTDLabHrs,
            JTDLabCost,
            JTDLabBill,
            JTDExpCost,
            JTDExpBill,
            JTDConCost,
            JTDConBill,
            JTDUntQty,
            JTDUntCost,
            JTDUntBill)
        SELECT XT.TaskID, 
                SUM(ISNULL(U.JTDLabHrs, 0)) AS JTDLabHrs, 
                SUM(ISNULL(U.JTDLabCost, 0)) AS JTDLabCost, 
                SUM(ISNULL(U.JTDLabBill, 0)) AS JTDLabBill, 
                SUM(ISNULL(U.JTDExpCost, 0)) AS JTDExpCost, 
                SUM(ISNULL(U.JTDExpBill, 0)) AS JTDExpBill, 
                SUM(ISNULL(U.JTDConCost, 0)) AS JTDConCost, 
                SUM(ISNULL(U.JTDConBill, 0)) AS JTDConBill, 
                SUM(ISNULL(U.JTDUntQty, 0)) AS JTDUntQty, 
                SUM(ISNULL(U.JTDUntCost, 0)) AS JTDUntCost, 
                SUM(ISNULL(U.JTDUntBill, 0)) AS JTDUntBill 
            FROM RPTask AS XT LEFT JOIN
                (SELECT TaskID AS TaskID, 
                        SUM(PeriodHrs) AS JTDLabHrs, 
                        SUM(PeriodCost) AS JTDLabCost, 
                        SUM(PeriodBill) AS JTDLabBill, 
                        0 AS JTDExpCost, 
                        0 AS JTDExpBill, 
                        0 AS JTDConCost, 
                        0 AS JTDConBill, 
                        0 AS JTDUntQty, 
                        0 AS JTDUntCost, 
                        0 AS JTDUntBill 
                FROM @tabJTDLab 
                WHERE AssignmentID IS NULL 
                GROUP BY TaskID 
                UNION ALL 
                SELECT TaskID AS TaskID, 
                        0 AS JTDLabHrs, 
                        0 AS JTDLabCost, 
                        0 AS JTDLabBill, 
                        SUM(PeriodCost) AS JTDExpCost, 
                        SUM(PeriodBill) AS JTDExpBill, 
                        0 AS JTDConCost, 
                        0 AS JTDConBill, 
                        0 AS JTDUntQty, 
                        0 AS JTDUntCost, 
                        0 AS JTDUntBill 
                FROM @tabJTDExp 
                WHERE ExpenseID IS NULL
                GROUP BY TaskID 
                UNION ALL 
                SELECT TaskID AS TaskID, 
                        0 AS JTDLabHrs, 
                        0 AS JTDLabCost, 
                        0 AS JTDLabBill, 
                        0 AS JTDExpCost, 
                        0 AS JTDExpBill, 
                        SUM(PeriodCost) AS JTDConCost, 
                        SUM(PeriodBill) AS JTDConBill, 
                        0 AS JTDUntQty, 
                        0 AS JTDUntCost, 
                        0 AS JTDUntBill 
                FROM @tabJTDCon 
                WHERE ConsultantID IS NULL
                GROUP BY TaskID 
                UNION ALL 
                SELECT TaskID AS TaskID, 
                        0 AS JTDLabHrs, 
                        0 AS JTDLabCost, 
                        0 AS JTDLabBill, 
                        0 AS JTDExpCost, 
                        0 AS JTDExpBill, 
                        0 AS JTDConCost, 
                        0 AS JTDConBill, 
                        SUM(PeriodQty) AS JTDUntQty, 
                        SUM(PeriodCost) AS JTDUntCost, 
                        SUM(PeriodBill) AS JTDUntBill 
                FROM @tabJTDUnt
                WHERE UnitID IS NULL
                GROUP BY TaskID) AS U 
                ON XT.TaskID = U.TaskID WHERE XT.PlanID = @strPlanID GROUP BY XT.TaskID
        
    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        --> Unmatched JTD.
        
        IF (@strHasJTDLab = 'Y')
        BEGIN
            
            DECLARE @tabUMResTask TABLE
            (RowID int IDENTITY(1,1),
                TaskID varchar(32) COLLATE database_default, 
                WBSType varchar(4) COLLATE database_default, 
                WBS1 Nvarchar(30) COLLATE database_default,
                WBS2 Nvarchar(7) COLLATE database_default,
                WBS3 Nvarchar(7) COLLATE database_default,
                LaborCode Nvarchar(14) COLLATE database_default, 
                ResourceID Nvarchar(20) COLLATE database_default, 
                Category smallint, 
                GRLBCD Nvarchar(14) COLLATE database_default
                PRIMARY KEY(RowID, TaskID, WBS1, WBS2, WBS3, LaborCode))
                
            DECLARE @tabUMResLD TABLE
            (RowID int IDENTITY(1,1),
                TaskID varchar(32) COLLATE database_default, 
                Category smallint, 
                LaborCode Nvarchar(14) COLLATE database_default, 
                Employee Nvarchar(20) COLLATE database_default
                PRIMARY KEY(RowID))
                
            DECLARE @tabUMWBSTask TABLE
            (RowID int IDENTITY(1,1),
                TaskID varchar(32) COLLATE database_default, 
                PWBSType Nvarchar(4) COLLATE database_default, 
                CWBSType Nvarchar(4) COLLATE database_default, 
                WBS1 Nvarchar(30) COLLATE database_default,
                WBS2 Nvarchar(7) COLLATE database_default,
                WBS3 Nvarchar(7) COLLATE database_default,
                LaborCode Nvarchar(14) COLLATE database_default
                PRIMARY KEY(RowID, TaskID, WBS1, WBS2, WBS3, LaborCode))

            DECLARE @tabUMWBSLD TABLE
            (RowID int IDENTITY(1,1),
                TaskID varchar(32) COLLATE database_default, 
                PWBSType Nvarchar(4) COLLATE database_default, 
                CWBSType Nvarchar(4) COLLATE database_default, 
                WBS1 Nvarchar(30) COLLATE database_default,
                WBS2 Nvarchar(7) COLLATE database_default,
                WBS3 Nvarchar(7) COLLATE database_default,
                LaborCode Nvarchar(14) COLLATE database_default
                PRIMARY KEY(RowID))

            -- Save Tasks (with Valid WBS Mapping) that have Resources underneath.
            -- Note that, the Resources do not have to be immediately below the Task.
            -- There could be intermediate Tasks that do not have any WBS Mapping.

            INSERT @tabUMResTask( TaskID, WBSType, WBS1, WBS2, WBS3, LaborCode, ResourceID, Category, GRLBCD) 
            SELECT  
                T.TaskID, T.WBSType, T.WBS1, ISNULL(T.WBS2, '%') AS WBS2, 
                ISNULL(T.WBS3, '%') AS WBS3, ISNULL(T.LaborCode, '%') AS LaborCode, 
                A.ResourceID, A.Category, A.GRLBCD 
                FROM RPTask AS T INNER JOIN RPAssignment AS A 
                ON T.PlanID = A.PlanID AND T.WBS1 = A.WBS1 
                    AND ISNULL(T.WBS2, '') = ISNULL(A.WBS2, '') 
                    AND ISNULL(T.WBS3, '') = ISNULL(A.WBS3, '') 
                    AND ISNULL(T.LaborCode, '') = ISNULL(A.LaborCode, '') 
                    WHERE T.PlanID = @strPlanID AND T.WBSType IS NOT NULL 

            -- Save "Posted" LD records that match with the Task records in the previous step.

            INSERT @tabUMResLD( TaskID, Category, LaborCode, Employee)
            SELECT 
            TaskID, Category, LaborCode, Employee
            FROM
                (SELECT DISTINCT T.TaskID, LD.Category, LD.LaborCode, LD.Employee 
                    FROM @tabLD AS LD INNER JOIN @tabUMResTask AS T 
                    ON LD.WBS2 LIKE T.WBS2 AND LD.WBS3 LIKE T.WBS3 AND LD.LaborCode LIKE T.LaborCode 
                    WHERE(LD.WBS1 = T.WBS1 AND LD.PostedFlg = 1)) AS X

            -- Remove LD records that match with the Generic Resource Category in the Task records.

            DELETE @tabUMResLD 
            FROM @tabUMResLD AS LD INNER JOIN @tabUMResTask AS T 
            ON LD.TaskID = T.TaskID AND LD.Category = T.Category AND T.ResourceID IS NULL 

            -- Remove LD records that match with the Generic Resource Labor Codes in the Task records.

            DELETE @tabUMResLD 
            FROM @tabUMResLD AS LD INNER JOIN @tabUMResTask AS T 
            ON LD.TaskID = T.TaskID AND LD.LaborCode LIKE T.GRLBCD 

            -- Remove LD records that have the same Employee as the Task records.

            DELETE @tabUMResLD 
            FROM @tabUMResLD AS LD INNER JOIN @tabUMResTask AS T 
            ON LD.TaskID = T.TaskID AND LD.Employee = T.ResourceID 

            -- Update UnmatchedFlg in @tabTaskJTD.

            UPDATE @tabTaskJTD SET UnmatchedFlg = UMFlag 
            FROM @tabTaskJTD AS T INNER JOIN 
                (SELECT LD.TaskID, CASE WHEN COUNT(LD.Employee) > 0 THEN 'Y' ELSE 'N' END AS UMFlag 
                    FROM @tabUMResLD AS LD GROUP BY LD.TaskID) AS XLD 
                ON T.TaskID = XLD.TaskID 
            WHERE UnmatchedFlg <> UMFlag 

            --> Unmatched WBS.

            -- Save Tasks (with Valid WBS Mapping) that have [mapped] child Tasks underneath.
            -- Note that, the child Task do not have to be immediately below the Task.
            -- There could be intermediate Tasks that do not have any WBS Mapping.

            INSERT @tabUMWBSTask( TaskID, PWBSType, CWBSType, WBS1, WBS2, WBS3, LaborCode)
            SELECT 
                TaskID, PWBSType, CWBSType, WBS1, WBS2, WBS3, 
                ISNULL(LaborCode, '') AS LaborCode
                FROM 
                (SELECT DISTINCT PT.TaskID, PT.WBSType AS PWBSType, CT.WBSType AS CWBSType, CT.WBS1, CT.WBS2, '' AS WBS3, '' AS LaborCode 
                    FROM RPTask AS PT INNER JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.WBS1 = CT.WBS1 
                    WHERE PT.PlanID = @strPlanID AND PT.WBSType = 'WBS1' AND CT.WBSType = 'WBS2' 
                    UNION 
                    SELECT DISTINCT PT.TaskID, PT.WBSType AS PWBSType, CT.WBSType AS CWBSType, CT.WBS1, CT.WBS2, CT.WBS3, '' AS LaborCode 
                    FROM RPTask AS PT INNER JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.WBS1 = CT.WBS1 AND PT.WBS2 = CT.WBS2 
                    WHERE PT.PlanID = @strPlanID AND PT.WBSType = 'WBS2' AND CT.WBSType = 'WBS3' 
                    UNION 
                    SELECT DISTINCT PT.TaskID, PT.WBSType AS PWBSType, CT.WBSType AS CWBSType, CT.WBS1, '' AS WBS2, '' AS WBS3, CT.LaborCode 
                    FROM RPTask AS PT INNER JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.WBS1 = CT.WBS1 
                    WHERE PT.PlanID = @strPlanID AND PT.WBSType = 'WBS1' AND CT.WBSType = 'LBCD' AND CT.WBS2 IS NULL AND CT.WBS3 IS NULL 
                    UNION 
                    SELECT DISTINCT PT.TaskID, PT.WBSType AS PWBSType, CT.WBSType AS CWBSType, CT.WBS1, CT.WBS2, '' AS WBS3, CT.LaborCode 
                    FROM RPTask AS PT INNER JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.WBS1 = CT.WBS1 AND PT.WBS2 = CT.WBS2 
                    WHERE PT.PlanID = @strPlanID AND PT.WBSType = 'WBS2' AND CT.WBSType = 'LBCD' AND CT.WBS3 IS NULL 
                    UNION 
                    SELECT DISTINCT PT.TaskID, PT.WBSType AS PWBSType, CT.WBSType AS CWBSType, CT.WBS1, CT.WBS2, CT.WBS3, CT.LaborCode 
                    FROM RPTask AS PT INNER JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.WBS1 = CT.WBS1 AND PT.WBS2 = CT.WBS2 AND PT.WBS3 = CT.WBS3 
                    WHERE PT.PlanID = @strPlanID AND PT.WBSType = 'WBS3' AND CT.WBSType = 'LBCD') AS XWT 

            -- Save "Posted" LD records that match with the Task records in the previous step.

            INSERT @tabUMWBSLD( TaskID, PWBSType, CWBSType, WBS1, WBS2, WBS3, LaborCode)
            SELECT   
                TaskID, PWBSType, CWBSType, WBS1, WBS2, WBS3, 
                ISNULL(LaborCode, '') AS LaborCode
                FROM 
                (SELECT DISTINCT T.TaskID, T.PWBSType, T.CWBSType, LD.WBS1, LD.WBS2, '' AS WBS3, '' AS LaborCode 
                    FROM @tabLD AS LD INNER JOIN @tabUMWBSTask AS T ON LD.WBS1 = T.WBS1 AND LD.PostedFlg = 1
                    WHERE T.PWBSType = 'WBS1' AND T.CWBSType = 'WBS2'
                    UNION 
                    SELECT DISTINCT T.TaskID, T.PWBSType, T.CWBSType, LD.WBS1, LD.WBS2, LD.WBS3, '' AS LaborCode 
                    FROM @tabLD AS LD 
                        INNER JOIN @tabUMWBSTask AS T ON LD.WBS1 = T.WBS1 AND LD.WBS2 = T.WBS2 AND LD.PostedFlg = 1
                    WHERE T.PWBSType = 'WBS2' AND T.CWBSType = 'WBS3'
                    UNION 
                    SELECT DISTINCT T.TaskID, T.PWBSType, T.CWBSType, LD.WBS1, '' AS WBS2, '' AS WBS3, LD.LaborCode 
                    FROM @tabLD AS LD 
                        INNER JOIN @tabUMWBSTask AS T ON LD.WBS1 = T.WBS1 AND LD.PostedFlg = 1
                    WHERE T.PWBSType = 'WBS1' AND T.CWBSType = 'LBCD' 
                    UNION 
                    SELECT DISTINCT T.TaskID, T.PWBSType, T.CWBSType, LD.WBS1, LD.WBS2, '' AS WBS3, LD.LaborCode 
                    FROM @tabLD AS LD 
                        INNER JOIN @tabUMWBSTask AS T ON LD.WBS1 = T.WBS1 AND LD.WBS2 = T.WBS2 AND LD.PostedFlg = 1 
                    WHERE T.PWBSType = 'WBS2' AND T.CWBSType = 'LBCD' 
                    UNION 
                    SELECT DISTINCT T.TaskID, T.PWBSType, T.CWBSType, LD.WBS1, LD.WBS2, LD.WBS3, LD.LaborCode 
                    FROM @tabLD AS LD 
                        INNER JOIN @tabUMWBSTask AS T 
                        ON LD.WBS1 = T.WBS1 AND LD.WBS2 = T.WBS2  AND LD.WBS3 = T.WBS3 AND LD.PostedFlg = 1 
                    WHERE T.PWBSType = 'WBS3' AND T.CWBSType = 'LBCD') AS XWLD 

            -- Remove LD records that have the same WBS mapping as the Task records.

            DELETE @tabUMWBSLD FROM @tabUMWBSLD AS U INNER JOIN @tabUMWBSTask AS T 
            ON U.PWBSType = T.PWBSType AND U.CWBSType = T.CWBSType 
                AND U.WBS1 = T.WBS1 
                AND U.WBS2 = T.WBS2 
                AND U.WBS3 = T.WBS3 
                AND U.LaborCode = T.LaborCode 

            -- Update UnmatchedFlg in @tabTaskJTD.

            UPDATE @tabTaskJTD SET UnmatchedFlg = UMFlag 
            FROM @tabTaskJTD AS T INNER JOIN 
                (SELECT LD.TaskID, CASE WHEN COUNT(*) > 0 THEN 'Y' ELSE 'N' END AS UMFlag 
                FROM @tabUMWBSLD AS LD GROUP BY LD.TaskID) AS XLD 
                ON T.TaskID = XLD.TaskID 
            WHERE UnmatchedFlg <> UMFlag 

        END -- If-Then (@strHasJTDLab = 'Y')
        
    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        -- Sum up for Plan.
        
        INSERT @tabPlanJTD
        (PlanID,
            JTDLabHrs,
            JTDLabCost,
            JTDLabBill,
            JTDExpCost,
            JTDExpBill,
            JTDConCost,
            JTDConBill,
            JTDUntQty,
            JTDUntCost,
            JTDUntBill,
            JTDRevenue)
        SELECT @strPlanID, 
                SUM(ISNULL(JTDLabHrs, 0)) AS JTDLabHrs, 
                SUM(ISNULL(JTDLabCost, 0)) AS JTDLabCost, 
                SUM(ISNULL(JTDLabBill, 0)) AS JTDLabBill, 
                SUM(ISNULL(JTDExpCost, 0)) AS JTDExpCost, 
                SUM(ISNULL(JTDExpBill, 0)) AS JTDExpBill, 
                SUM(ISNULL(JTDConCost, 0)) AS JTDConCost, 
                SUM(ISNULL(JTDConBill, 0)) AS JTDConBill, 
                SUM(ISNULL(JTDUntQty, 0)) AS JTDUntQty, 
                SUM(ISNULL(JTDUntCost, 0)) AS JTDUntCost, 
                SUM(ISNULL(JTDUntBill, 0)) AS JTDUntBill,
                SUM(ISNULL(JTDRevenue, 0)) AS JTDRevenue 
            FROM 
            (SELECT SUM(ISNULL(JTDLabHrs, 0)) AS JTDLabHrs, 
                    SUM(ISNULL(JTDLabCost, 0)) AS JTDLabCost, 
                    SUM(ISNULL(JTDLabBill, 0)) AS JTDLabBill, 
                    SUM(ISNULL(JTDExpCost, 0)) AS JTDExpCost, 
                    SUM(ISNULL(JTDExpBill, 0)) AS JTDExpBill, 
                    SUM(ISNULL(JTDConCost, 0)) AS JTDConCost, 
                    SUM(ISNULL(JTDConBill, 0)) AS JTDConBill, 
                    SUM(ISNULL(JTDUntQty, 0)) AS JTDUntQty, 
                    SUM(ISNULL(JTDUntCost, 0)) AS JTDUntCost, 
                    SUM(ISNULL(JTDUntBill, 0)) AS JTDUntBill 
                FROM @tabTaskJTD AS T INNER JOIN @tabTopTask AS TT
                    ON T.TaskID = TT.TaskID) AS X,
                (SELECT SUM(ISNULL(R.JTDRevenue, 0)) AS JTDRevenue FROM
                    (SELECT SUM(-AmountProjectCurrency) AS JTDRevenue
                       FROM LedgerAR AS Ledger 
                         INNER JOIN @tabTopTask AS T ON Ledger.WBS1 = T.WBS1 
                           AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%')) 
                           AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                         INNER JOIN CA ON Ledger.Account = CA.Account
                       WHERE (CA.Type = 4
                         AND NOT (Ledger.TransType='IN' AND (Ledger.SubType IS NOT NULL AND Ledger.SubType IN ('R','I')))) 
                         AND Ledger.TransDate <= @dtJTDDate
                     UNION ALL
                     SELECT SUM(-AmountProjectCurrency) AS JTDRevenue
                       FROM LedgerAP AS Ledger 
                         INNER JOIN @tabTopTask AS T ON Ledger.WBS1 = T.WBS1 
                           AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%')) 
                           AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                         INNER JOIN CA ON Ledger.Account = CA.Account
                        WHERE (CA.Type = 4
                        AND NOT (Ledger.TransType='IN' AND (Ledger.SubType IS NOT NULL AND Ledger.SubType IN ('R','I')))) 
                        AND Ledger.TransDate <= @dtJTDDate
                     UNION ALL
                     SELECT SUM(-AmountProjectCurrency) AS JTDRevenue
                       FROM LedgerEX AS Ledger 
                         INNER JOIN @tabTopTask AS T ON Ledger.WBS1 = T.WBS1 
                           AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%')) 
                           AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                         INNER JOIN CA ON Ledger.Account = CA.Account
                       WHERE (CA.Type = 4
                         AND NOT (Ledger.TransType='IN' AND (Ledger.SubType IS NOT NULL AND Ledger.SubType IN ('R','I')))) 
                         AND Ledger.TransDate <= @dtJTDDate
                     UNION ALL
                     SELECT SUM(-AmountProjectCurrency) AS JTDRevenue
                       FROM LedgerMISC AS Ledger 
                         INNER JOIN @tabTopTask AS T ON Ledger.WBS1 = T.WBS1 
                           AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%')) 
                           AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                         INNER JOIN CA ON Ledger.Account = CA.Account
                       WHERE (CA.Type = 4
                         AND NOT (Ledger.TransType='IN' AND (Ledger.SubType IS NOT NULL AND Ledger.SubType IN ('R','I')))) 
                         AND Ledger.TransDate <= @dtJTDDate) AS R
            ) AS Y  

	    END -- If-Then (@@ROWCOUNT > 0)
        
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- Check to see if there is any time-phased data.
  -- We don't want to do any work if the plan is empty.
  
    BEGIN
      SELECT @intBaselineLabCount = COUNT(*) FROM RPBaselineLabor WHERE PlanID = @strPlanID
      IF (@strExpTab = 'Y') SELECT @intBaselineExpCount = COUNT(*) FROM RPBaselineExpenses WHERE PlanID = @strPlanID
      IF (@strConTab = 'Y') SELECT @intBaselineConCount = COUNT(*) FROM RPBaselineConsultant WHERE PlanID = @strPlanID
      IF (@strUntTab = 'Y') SELECT @intBaselineUntCount = COUNT(*) FROM RPBaselineUnit WHERE PlanID = @strPlanID
    END
    
  SET @intPlannedExpCount = 0 
  SET @intPlannedConCount = 0
  SET @intPlannedUntCount = 0

  SELECT @intPlannedLabCount = COUNT(*) FROM RPPlannedLabor WHERE PlanID = @strPlanID
  IF (@strExpTab = 'Y') SELECT @intPlannedExpCount = COUNT(*) FROM RPPlannedExpenses WHERE PlanID = @strPlanID
  IF (@strConTab = 'Y') SELECT @intPlannedConCount = COUNT(*) FROM RPPlannedConsultant WHERE PlanID = @strPlanID
  IF (@strUntTab = 'Y') SELECT @intPlannedUntCount = COUNT(*) FROM RPPlannedUnit WHERE PlanID = @strPlanID
  
      SELECT @intTaskJTDLabCount = COUNT(*) FROM @tabJTDLab WHERE AssignmentID IS NULL
      SELECT @intJTDLabCount = COUNT(*) FROM @tabJTDLab WHERE AssignmentID IS NOT NULL
      IF (@strExpTab = 'Y')
        BEGIN
          SELECT @intTaskJTDExpCount = COUNT(*) FROM @tabJTDExp WHERE ExpenseID IS NULL
          SELECT @intJTDExpCount = COUNT(*) FROM @tabJTDExp WHERE ExpenseID IS NOT NULL
        END
      IF (@strConTab = 'Y')
        BEGIN
          SELECT @intTaskJTDConCount = COUNT(*) FROM @tabJTDCon WHERE ConsultantID IS NULL
          SELECT @intJTDConCount = COUNT(*) FROM @tabJTDCon WHERE ConsultantID IS NOT NULL
        END
      IF (@strUntTab = 'Y')
        BEGIN
          SELECT @intTaskJTDUntCount = COUNT(*) FROM @tabJTDUnt WHERE UnitID IS NULL
          SELECT @intJTDUntCount = COUNT(*) FROM @tabJTDUnt WHERE UnitID IS NOT NULL
        END
    
    
  IF (@strFeesByPeriod = 'Y')
    BEGIN
      SELECT @intCompCount = COUNT(*) FROM RPCompensationFee WHERE PlanID = @strPlanID
      SELECT @intConsCount = COUNT(*) FROM RPConsultantFee WHERE PlanID = @strPlanID
      SELECT @intReimCount = COUNT(*) FROM RPReimbAllowance WHERE PlanID = @strPlanID
    END

  SELECT @intEVCount = COUNT(*) FROM RPEVT WHERE PlanID = @strPlanID
  SELECT @intTDCount = COUNT(*) FROM RPDependency WHERE PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Calculate ETC when needed
  
    
      -- Calculate Labor ETC.
      
      IF (@intPlannedLabCount > 0)
        INSERT @tabLabETC
          (AssignmentID,
           ETCLabHrs,
           ETCLabCost,
           ETCLabBill)
          SELECT AssignmentID AS AssignmentID, 
            ISNULL(SUM(CASE WHEN TPD.StartDate >= @dtETCDate THEN PeriodHrs 
                            ELSE PeriodHrs * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END), 0) AS ETCLabHrs, 
            ISNULL(SUM(CASE WHEN TPD.StartDate >= @dtETCDate THEN PeriodCost
                            ELSE PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END), 0) AS ETCLabCost, 
            ISNULL(SUM(CASE WHEN TPD.StartDate >= @dtETCDate THEN PeriodBill
                            ELSE PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END), 0) AS ETCLabBill 
            FROM RPPlannedLabor AS TPD 
            WHERE TPD.PlanID = @strPlanID AND TPD.AssignmentID IS NOT NULL 
            AND TPD.EndDate >= @dtETCDate 
            GROUP BY TPD.AssignmentID 
    
      -- Calculate Expense ETC.

      IF (@strExpTab = 'Y' AND @intPlannedExpCount > 0)
        INSERT @tabExpETC
          (ExpenseID,
           DirectAcctFlg,
           ETCExpCost,
           ETCExpBill)
          SELECT TPD.ExpenseID AS ExpenseID, 
            E.DirectAcctFlg AS DirectAcctFlg, 
            ISNULL(SUM(CASE WHEN TPD.StartDate >= @dtETCDate THEN PeriodCost
                            ELSE PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END), 0) AS ETCExpCost, 
            ISNULL(SUM(CASE WHEN TPD.StartDate >= @dtETCDate THEN PeriodBill
                            ELSE PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END), 0) AS ETCExpBill 
            FROM RPPlannedExpenses AS TPD 
              INNER JOIN RPExpense AS E ON TPD.PlanID = E.PlanID AND TPD.TaskID = E.TaskID AND TPD.ExpenseID = E.ExpenseID 
            WHERE TPD.PlanID = @strPlanID AND TPD.ExpenseID IS NOT NULL AND TPD.EndDate >= @dtETCDate
            GROUP BY TPD.ExpenseID, E.DirectAcctFlg
             
      -- Calculate Consultant ETC.
      
      IF (@strConTab = 'Y' AND @intPlannedConCount > 0)
        INSERT @tabConETC
          (ConsultantID,
           DirectAcctFlg,
           ETCConCost,
           ETCConBill)
          SELECT TPD.ConsultantID AS ConsultantID, 
            C.DirectAcctFlg AS DirectAcctFlg, 
            ISNULL(SUM(CASE WHEN TPD.StartDate >= @dtETCDate THEN PeriodCost
                            ELSE PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END), 0) AS ETCConCost, 
            ISNULL(SUM(CASE WHEN TPD.StartDate >= @dtETCDate THEN PeriodBill
                            ELSE PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END), 0) AS ETCConBill 
            FROM RPPlannedConsultant AS TPD 
              INNER JOIN RPConsultant AS C ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID AND TPD.ConsultantID = C.ConsultantID 
            WHERE TPD.PlanID = @strPlanID AND TPD.ConsultantID IS NOT NULL AND TPD.EndDate >= @dtETCDate 
            GROUP BY TPD.ConsultantID, C.DirectAcctFlg
            
      -- Calculate Unit ETC.

      IF (@strUntTab = 'Y' AND @intPlannedUntCount > 0)
        INSERT @tabUntETC
          (UnitID,
           DirectAcctFlg,
           ETCUntQty,
           ETCUntCost,
           ETCUntBill)
          SELECT TPD.UnitID AS UnitID, U.DirectAcctFlg AS DirectAcctFlg, 
            ISNULL(SUM(CASE WHEN TPD.StartDate >= @dtETCDate THEN PeriodQty 
                            ELSE PeriodQty * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END), 0) AS ETCUntQty, 
            ISNULL(SUM(CASE WHEN TPD.StartDate >= @dtETCDate THEN PeriodCost
                            ELSE PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END), 0) AS ETCUntCost, 
            ISNULL(SUM(CASE WHEN TPD.StartDate >= @dtETCDate THEN PeriodBill
                            ELSE PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) END), 0) AS ETCUntBill 
            FROM RPPlannedUnit AS TPD 
              INNER JOIN RPUnit AS U ON TPD.PlanID = U.PlanID AND TPD.TaskID = U.TaskID AND TPD.UnitID = U.UnitID 
            WHERE TPD.PlanID = @strPlanID AND TPD.UnitID IS NOT NULL AND TPD.EndDate >= @dtETCDate 
            GROUP BY TPD.UnitID, U.DirectAcctFlg 

      -- Calculate Task ETC.

      IF ((@intPlannedLabCount + @intPlannedExpCount + @intPlannedConCount + @intPlannedUntCount) > 0)
        INSERT @tabTaskETC
          (TaskID,
           OutlineLevel,
           ETCLabHrs,
           ETCLabCost,
           ETCLabBill,
           ETCExpCost,
           ETCExpBill,
           ETCConCost,
           ETCConBill,
           ETCUntQty,
           ETCUntCost,
           ETCUntBill)
          SELECT TaskID AS TaskID, OutlineLevel AS OutlineLevel, 
            SUM(CASE WHEN RType = 'L' THEN CASE WHEN X.StartDate >= @dtETCDate THEN PeriodHrs 
	                   ELSE PeriodHrs * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END ELSE 0 END) AS ETCLabHrs, 
            SUM(CASE WHEN RType = 'L' THEN CASE WHEN X.StartDate >= @dtETCDate THEN PeriodCost
	                   ELSE PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END ELSE 0 END) AS ETCLabCost, 
            SUM(CASE WHEN RType = 'L' THEN CASE WHEN X.StartDate >= @dtETCDate THEN PeriodBill
	                   ELSE PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END ELSE 0 END) AS ETCLabBill, 
            SUM(CASE WHEN RType = 'E' THEN CASE WHEN X.StartDate >= @dtETCDate THEN PeriodCost
	                   ELSE PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END ELSE 0 END) AS ETCExpCost, 
            SUM(CASE WHEN RType = 'E' THEN CASE WHEN X.StartDate >= @dtETCDate THEN PeriodBill
	                   ELSE PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END ELSE 0 END) AS ETCExpBill, 
            SUM(CASE WHEN RType = 'C' THEN CASE WHEN X.StartDate >= @dtETCDate THEN PeriodCost
	                   ELSE PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END ELSE 0 END) AS ETCConCost, 
            SUM(CASE WHEN RType = 'C' THEN CASE WHEN X.StartDate >= @dtETCDate THEN PeriodBill
	                   ELSE PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END ELSE 0 END) AS ETCConBill, 
            SUM(CASE WHEN RType = 'U' THEN CASE WHEN X.StartDate >= @dtETCDate THEN PeriodQty 
	                   ELSE PeriodQty * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END ELSE 0 END) AS ETCUntQty, 
            SUM(CASE WHEN RType = 'U' THEN CASE WHEN X.StartDate >= @dtETCDate THEN PeriodCost
	                   ELSE PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END ELSE 0 END) AS ETCUntCost, 
            SUM(CASE WHEN RType = 'U' THEN CASE WHEN X.StartDate >= @dtETCDate THEN PeriodBill
	                   ELSE PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, X.EndDate, X.StartDate, X.EndDate, @strCompany) END ELSE 0 END) AS ETCUntBill 
            FROM
              (SELECT T.TaskID AS TaskID, T.OutlineLevel AS OutlineLevel, 'L' AS RType, 
                 ISNULL(PeriodHrs, 0) AS PeriodHrs, 0 AS PeriodQty, ISNULL(PeriodCost, 0) AS PeriodCost, ISNULL(PeriodBill, 0) AS PeriodBill, 
                 TPD.StartDate AS StartDate, TPD.EndDate as EndDate 
                 FROM RPTask AS T INNER JOIN RPPlannedLabor AS TPD ON (T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL AND TPD.EndDate >= @dtETCDate) 
                 WHERE T.PlanID = @strPlanID 
               UNION ALL
               SELECT T.TaskID AS TaskID, T.OutlineLevel AS OutlineLevel, 'E' AS RType, 
                 0 PeriodHrs, 0 AS PeriodQty, ISNULL(PeriodCost, 0) AS PeriodCost, ISNULL(PeriodBill, 0) AS PeriodBill, 
                 TPD.StartDate AS StartDate, TPD.EndDate as EndDate 
                 FROM RPTask AS T INNER JOIN RPPlannedExpenses AS TPD ON (T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID AND TPD.ExpenseID IS NULL AND TPD.EndDate >= @dtETCDate) 
                 WHERE T.PlanID = @strPlanID 
               UNION ALL
               SELECT T.TaskID AS TaskID, T.OutlineLevel AS OutlineLevel, 'C' AS RType, 
                 0 PeriodHrs, 0 AS PeriodQty, ISNULL(PeriodCost, 0) AS PeriodCost, ISNULL(PeriodBill, 0) AS PeriodBill, 
                 TPD.StartDate AS StartDate, TPD.EndDate as EndDate 
                 FROM RPTask AS T INNER JOIN RPPlannedConsultant AS TPD ON (T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID AND TPD.ConsultantID IS NULL AND TPD.EndDate >= @dtETCDate) 
                 WHERE T.PlanID = @strPlanID 
               UNION ALL
               SELECT T.TaskID AS TaskID, T.OutlineLevel AS OutlineLevel, 'U' AS RType, 
                 0 PeriodHrs, ISNULL(PeriodQty, 0) AS PeriodQty, ISNULL(PeriodCost, 0) AS PeriodCost, ISNULL(PeriodBill, 0) AS PeriodBill, 
                 TPD.StartDate AS StartDate, TPD.EndDate as EndDate 
                 FROM RPTask AS T INNER JOIN RPPlannedUnit AS TPD ON (T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID AND TPD.UnitID IS NULL AND TPD.EndDate >= @dtETCDate) 
                 WHERE T.PlanID = @strPlanID) AS X 
            GROUP BY X.TaskID, X.OutlineLevel 

      -- Calculate Plan ETC.
      
      IF ((@intPlannedLabCount + @intPlannedExpCount + @intPlannedConCount + @intPlannedUntCount) > 0)
        INSERT @tabPlanETC
          (PlanID,
           ETCLabHrs,
           ETCLabCost,
           ETCLabBill,
           ETCExpCost,
           ETCExpBill,
           ETCConCost,
           ETCConBill,
           ETCUntQty,
           ETCUntCost,
           ETCUntBill,
           ETCDirExpCost,
           ETCDirConCost,
           ETCDirUntCost,
           ETCDirExpBill,
           ETCDirConBill,
           ETCDirUntBill)
          SELECT @strPlanID AS PlanID, 
            SUM(ETCLabHrs) AS ETCLabHrs, SUM(ETCLabCost) AS ETCLabCost, SUM(ETCLabBill) AS ETCLabBill, 
            SUM(ETCExpCost) AS ETCExpCost, SUM(ETCExpBill) AS ETCExpBill, 
            SUM(ETCConCost) AS ETCConCost, SUM(ETCConBill) AS ETCConBill, 
            SUM(ETCUntQty) AS ETCUntQty, SUM(ETCUntCost) AS ETCUntCost, SUM(ETCUntBill) AS ETCUntBill, 
            SUM(ETCDirExpCost) AS ETCDirExpCost, SUM(ETCDirExpBill) AS ETCDirExpBill, 
            SUM(ETCDirConCost) AS ETCDirConCost, SUM(ETCDirConBill) AS ETCDirConBill, 
            SUM(ETCDirUntCost) AS ETCDirUntCost, SUM(ETCDirUntBill) AS ETCDirUntBill 
            FROM 
            (SELECT -- Top most level Task rows.
               SUM(ETCLabHrs) AS ETCLabHrs, SUM(ETCLabCost) AS ETCLabCost, SUM(ETCLabBill) AS ETCLabBill, 
               SUM(ETCExpCost) AS ETCExpCost, SUM(ETCExpBill) AS ETCExpBill, 
               SUM(ETCConCost) AS ETCConCost, SUM(ETCConBill) AS ETCConBill, 
               SUM(ETCUntQty) AS ETCUntQty, SUM(ETCUntCost) AS ETCUntCost, SUM(ETCUntBill) AS ETCUntBill, 
               0 AS ETCDirExpCost, 0 AS ETCDirExpBill, 
               0 AS ETCDirConCost, 0 AS ETCDirConBill, 
               0 AS ETCDirUntCost, 0 AS ETCDirUntBill 
               FROM @tabTaskETC WHERE OutlineLevel = 0 
             UNION ALL 
             SELECT -- Calculating ETC Direct for Expense.
               0 AS ETCLabHrs, 0 AS ETCLabCost, 0 AS ETCLabBill, 
               0 AS ETCExpCost, 0 AS ETCExpBill, 
               0 AS ETCConCost, 0 AS ETCConBill, 
               0 AS ETCUntQty, 0 AS ETCUntCost, 0 AS ETCUntBill, 
               SUM(ETCExpCost) AS ETCDirExpCost, SUM(ETCExpBill) AS ETCDirExpBill, 
               0 AS ETCDirConCost, 0 AS ETCDirConBill, 
               0 AS ETCDirUntCost, 0 AS ETCDirUntBill 
               FROM @tabExpETC WHERE DirectAcctFlg = 'Y' 
             UNION ALL 
             SELECT  -- Calculating ETC Direct for Consultant.
               0 AS ETCLabHrs, 0 AS ETCLabCost, 0 AS ETCLabBill, 
               0 AS ETCExpCost, 0 AS ETCExpBill, 
               0 AS ETCConCost, 0 AS ETCConBill, 
               0 AS ETCUntQty, 0 AS ETCUntCost, 0 AS ETCUntBill, 
               0 AS ETCDirExpCost, 0 AS ETCDirExpBill, 
               SUM(ETCConCost) AS ETCDirConCost, SUM(ETCConBill) AS ETCDirConBill, 
               0 AS ETCDirUntCost, 0 AS ETCDirUntBill 
               FROM @tabConETC WHERE DirectAcctFlg = 'Y' 
             UNION ALL 
             SELECT  -- Calculating ETC Direct for Unit.
               0 AS ETCLabHrs, 0 AS ETCLabCost, 0 AS ETCLabBill, 
               0 AS ETCExpCost, 0 AS ETCExpBill, 
               0 AS ETCConCost, 0 AS ETCConBill, 
               0 AS ETCUntQty, 0 AS ETCUntCost, 0 AS ETCUntBill, 
               0 AS ETCDirExpCost, 0 AS ETCDirExpBill, 
               0 AS ETCDirConCost, 0 AS ETCDirConBill, 
               SUM(ETCUntCost) AS ETCDirUntCost, SUM(ETCUntBill) AS ETCDirUntBill 
               FROM @tabUntETC WHERE DirectAcctFlg = 'Y' 
             UNION ALL 
             SELECT -- Calculating ETC Direct for Task rows that do not have child Expense/Task rows underneath.
               0 AS ETCLabHrs, 0 AS ETCLabCost, 0 AS ETCLabBill, 
               0 AS ETCExpCost, 0 AS ETCExpBill, 
               0 AS ETCConCost, 0 AS ETCConBill, 
               0 AS ETCUntQty, 0 AS ETCUntCost, 0 AS ETCUntBill, 
               SUM(ETCExpCost) AS ETCDirExpCost, SUM(ETCExpBill) AS ETCDirExpBill, 
               0 AS ETCDirConCost, 0 AS ETCDirConBill, 
               0 AS ETCDirUntCost, 0 AS ETCDirUntBill 
               FROM @tabTaskETC AS T1 INNER JOIN 
                 (SELECT DISTINCT T.TaskID 
	                  FROM RPTask AS T LEFT JOIN RPExpense AS E ON T.PlanID = E.PlanID AND T.TaskID = E.TaskID 
	                  WHERE T.PlanID = @strPlanID AND ChildrenCount = 0 AND E.ExpenseID IS NULL) AS T2 
	               ON T1.TaskID = T2.TaskID 
             UNION ALL 
             SELECT  -- Calculating ETC Direct for Task rows that do not have child Consultant/Task rows underneath.
               0 AS ETCLabHrs, 0 AS ETCLabCost, 0 AS ETCLabBill, 
               0 AS ETCExpCost, 0 AS ETCExpBill, 
               0 AS ETCConCost, 0 AS ETCConBill, 
               0 AS ETCUntQty, 0 AS ETCUntCost, 0 AS ETCUntBill, 
               0 AS ETCDirExpCost, 0 AS ETCDirExpBill, 
               SUM(ETCConCost) AS ETCDirConCost, SUM(ETCConBill) AS ETCDirConBill, 
               0 AS ETCDirUntCost, 0 AS ETCDirUntBill 
               FROM @tabTaskETC AS T1 INNER JOIN 
	               (SELECT DISTINCT T.TaskID 
	                  FROM RPTask AS T LEFT JOIN RPConsultant AS C ON T.PlanID = C.PlanID AND T.TaskID = C.TaskID 
	                  WHERE T.PlanID = @strPlanID AND ChildrenCount = 0 AND C.ConsultantID IS NULL) AS T2 
	               ON T1.TaskID = T2.TaskID) AS X 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- compbine Plan JTD and ETC data into the return table @tabEACPlanData
INSERT @tabEACPlanData
	SELECT 
		   JTDLabHrs  + ETCLabHrs, 
		   JTDLabCost + ETCLabCost,
           JTDLabBill + ETCLabBill,
           JTDExpCost + ETCExpCost,
           JTDExpBill + ETCExpBill,
           JTDConCost + ETCConCost,
           JTDConBill + ETCConBill,
           JTDUntCost + ETCUntCost,
           JTDUntBill + ETCUntBill,	
		   JTDLabCost + ETCLabCost + JTDExpCost + ETCExpCost + JTDConCost + ETCConCost + JTDUntCost + ETCUntCost,
		   JTDLabBill + ETCLabBill + JTDExpBill + ETCExpBill + JTDConBill + ETCConBill + JTDUntBill + ETCUntBill
    FROM   @tabPlanETC AS E,  @tabPlanJTD AS J
    WHERE E.PlanID = J.PlanID
RETURN
END --outer

GO
