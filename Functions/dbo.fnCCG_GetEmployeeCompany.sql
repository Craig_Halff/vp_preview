SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnCCG_GetEmployeeCompany] (@Employee varchar(32))
RETURNS varchar(32)
AS BEGIN

	declare @multi varchar(1), @OrgLevels int, @Org1Start int, @Org1Length int, @Company varchar(32)

	select @multi=MulticompanyEnabled from CFGSystem
	if IsNull(@multi,'N')='N' return ' '

	-- Multicompany, so get company from EM org:
	select @OrgLevels=OrgLevels, @Org1Start=Org1Start, @Org1Length=Org1Length from CFGFormat
	if @OrgLevels < 1 return ' '
	
	select @Company=SubString(Org,@Org1Start,@Org1Length) from EM where Employee=@Employee
	return IsNull(@Company,' ')
END
GO
