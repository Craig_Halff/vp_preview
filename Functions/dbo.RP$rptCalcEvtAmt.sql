SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE FUNCTION [dbo].[RP$rptCalcEvtAmt]
(@planID varchar(32), 
@TaskID varchar(32),
@BaseAmt decimal(19,4),
@EndDate datetime,
@DecimalPos smallint,
@PeriodCount smallint) 
RETURNS decimal(19,4) AS  

BEGIN 

DECLARE @evtAmt decimal(19,4)
DECLARE @maxEvtDate datetime

SELECT @maxEvtDate = max(startDate) from rpEVT where planid=@planID and taskID=@TaskID and startDate<@EndDate
SELECT @evtAmt = ROUND(@BaseAmt * PeriodPct/100 * @PeriodCount,@DecimalPos) from rpEVT where planid=@planID and taskID=@TaskID and startDate=@maxEvtDate
SELECT @evtAmt = ISNULL(@evtAmt,0)

RETURN(@evtAmt)

END

GO
