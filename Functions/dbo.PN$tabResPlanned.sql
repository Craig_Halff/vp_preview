SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$tabResPlanned]
  (@strWBS1 Nvarchar(30),
   @strWBS2 Nvarchar(7),
   @strWBS3 Nvarchar(7),
   @strScale varchar(1) = 'm'
  )
  RETURNS @tabResPlanned TABLE
    (TimePhaseID varchar(32) COLLATE database_default,
     PlanID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     AssignmentID varchar(32) COLLATE database_default,
     Employee Nvarchar(20) COLLATE database_default,
     GenericResourceID Nvarchar(20) COLLATE database_default,
     StartDate datetime,
     EndDate datetime,
     PlannedLabHrs decimal(19,4),
     CostRate decimal(19,4),
     BillingRate decimal(19,4),
     PlannedLabCost decimal(19,4),
     PlannedLabBill decimal(19,4)
    )
BEGIN -- Function PN$tabResPlanned
 
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strMultiCompanyEnabled varchar(1)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

  DECLARE @dtJTDDate datetime
  DECLARE @dtStartDate datetime
  DECLARE @dtEndDate datetime
  DECLARE @dtIntervalEnd datetime
  
  DECLARE @intWkEndDay AS int
  
  DECLARE @intHrDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int

  DECLARE @strVorN char(1)

  DECLARE @tabAssignment TABLE
    (AssignmentID varchar(32) COLLATE database_default,
     PlanID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     PlanCreateDate datetime,
     Employee Nvarchar(20) COLLATE database_default,
     GenericResourceID Nvarchar(20) COLLATE database_default,
     LabBillMultiplier decimal(19,4)
     PRIMARY KEY(PlanID, TaskID, AssignmentID))
       
  DECLARE @tabTPD TABLE
    (TimePhaseID varchar(32) COLLATE database_default,
     PlanID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     AssignmentID varchar(32) COLLATE database_default,
     StartDate datetime,
     EndDate datetime,
     PeriodHrs decimal(19,4),
     CostRate decimal(19,4),
     BillingRate decimal(19,4),
     PeriodCost decimal(19,4),
     PeriodBill decimal(19,4)
     PRIMARY KEY(TimePhaseID, PlanID, TaskID, AssignmentID))

  DECLARE @tabCalendarInterval TABLE
    (StartDate datetime,
     EndDate datetime
     PRIMARY KEY(StartDate, EndDate))
          
  DECLARE @tabPLabTPD TABLE 
    (RowID int identity(1, 1),
     TimePhaseID varchar(32) COLLATE database_default,
     PlanID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     AssignmentID varchar(32) COLLATE database_default,
     Employee Nvarchar(20) COLLATE database_default,
     GenericResourceID Nvarchar(20) COLLATE database_default,
     CIStartDate datetime, 
     StartDate datetime, 
     EndDate datetime, 
     PeriodHrs decimal(19,4),
     CostRate decimal(19,4),
     BillingRate decimal(19,4),
     PeriodCost decimal(19,4),
     PeriodBill decimal(19,4)
     PRIMARY KEY(RowID, TimePhaseID, CIStartDate, EndDate)) 
           
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled flag.
  
  SELECT
    @strMultiCompanyEnabled = MultiCompanyEnabled
    FROM FW_CFGSystem

  -- Get Org Format.

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat     

  -- Save Company string for use later.
  
  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '

  -- Set Start of Week = Monday, End of Week = Sunday.
  
  SELECT @intWkEndDay = 1

  -- Get decimal settings.
  
  SET @intHrDecimals = 2
  SET @intAmtCostDecimals = 4
  SET @intAmtBillDecimals = 4

  -- Determine which set of tables need to be used.

  SET @strVorN = dbo.PN$PlanIs_VorN(@strWBS1)
       
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Extract Assigments from Plans that are marked as "Included in Utilization".
  -- There could be multiple Plans with RPPlan.UlilizationIncludeFlag = 'Y'

  IF (@strVorN = 'V')
    BEGIN

      INSERT @tabAssignment
        (AssignmentID,
         PlanID,
         TaskID,
         PlanCreateDate,
         Employee,
         GenericResourceID,
         LabBillMultiplier
        )
        SELECT
          A.AssignmentID,
          A.PlanID,
          A.TaskID,
          P.CreateDate AS PlanCreateDate,
          A.ResourceID AS Employee,
          A.GenericResourceID AS GenericResourceID,
          P.LabBillMultiplier
          FROM RPAssignment AS A
            INNER JOIN RPPlan AS P ON A.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND A.WBS1 = @strWBS1 AND 
              (ISNULL(A.WBS2, ' ') = @strWBS2) AND 
              (ISNULL(A.WBS3, ' ') = @strWBS3)

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabTPD
        (TimePhaseID,
         PlanID,
         TaskID,
         AssignmentID,
         StartDate,
         EndDate,
         PeriodHrs,
         CostRate,
         BillingRate,
         PeriodCost,
         PeriodBill
        )
        SELECT
          TPD.TimePhaseID,
          TPD.PlanID,
          TPD.TaskID,
          TPD.AssignmentID,
          TPD.StartDate,
          TPD.EndDate,
          TPD.PeriodHrs,
          TPD.CostRate,
          TPD.BillingRate,
          TPD.PeriodCost,
          TPD.PeriodBill
          FROM @tabAssignment AS A
            INNER JOIN RPPlannedLabor AS TPD ON A.PlanID = TPD.PlanID AND A.AssignmentID = TPD.AssignmentID 

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Find the MIN and MAX dates of all TPD.
  
      SELECT @dtStartDate = MIN(MINDate), @dtEndDate = MAX(MAXDate)
        FROM
          (SELECT MIN(TPD.StartDate) AS MINDate, MAX(TPD.EndDate) AS MAXDate 
             FROM @tabTPD AS TPD 
               INNER JOIN @tabAssignment AS A ON TPD.AssignmentID = A.AssignmentID AND TPD.PlanID = A.PlanID
          ) AS X

      -- Save Calendar Intervals into a temp table.
      
      WHILE (@dtStartDate <= @dtEndDate)
        BEGIN
        
          -- Compute End Date of interval.

          IF (@strScale = 'd') 
            SET @dtIntervalEnd = @dtStartDate
          ELSE
            SET @dtIntervalEnd = dbo.DLTK$IntervalEnd(@dtStartDate, @strScale, @intWkEndDay)
        
          IF (@dtIntervalEnd > @dtEndDate) 
            SET @dtIntervalEnd = @dtEndDate
            
          -- Insert new Calendar Interval record.
          
          INSERT @tabCalendarInterval(StartDate, EndDate)
            VALUES (@dtStartDate, @dtIntervalEnd)
          
          -- Set Start Date for next interval.
          
          SET @dtStartDate = DATEADD(d, 1, @dtIntervalEnd)
        
        END -- End While
       
      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
              
      -- Save Planned Labor time-phased data rows.

      INSERT @tabPLabTPD
        (TimePhaseID,
         PlanID,
         TaskID,
         AssignmentID,
         CIStartDate,
         Employee,
         GenericResourceID,
         StartDate, 
         EndDate, 
         PeriodHrs,
         CostRate,
         BillingRate,
         PeriodCost,
         PeriodBill
        )
        SELECT
          TimePhaseID,
          PlanID,
          TaskID,
          AssignmentID,
          CIStartDate AS CIStartDate,
          Employee,
          GenericResourceID,
          StartDate AS StartDate, 
          EndDate AS EndDate,
          ROUND(ISNULL(PeriodHrs * ProrateRatio, 0), @intHrDecimals) AS PeriodHrs,
          0.0000 AS CostRate,
          0.0000 AS BillingRate,
          ROUND(ISNULL(PeriodCost * ProrateRatio, 0), @intAmtCostDecimals) AS PeriodCost,
          ROUND(ISNULL(PeriodBill * ProrateRatio, 0), @intAmtBillDecimals) AS PeriodBill
        FROM 
          (SELECT
             TPD.TimePhaseID AS TimePhaseID,
             TPD.PlanID AS PlanID,
             TPD.TaskID AS TaskID,
             TPD.AssignmentID AS AssignmentID,
             CI.StartDate AS CIStartDate,
             A.Employee,
             A.GenericResourceID,
             CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
             CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
             PeriodHrs AS PeriodHrs,
             PeriodCost AS PeriodCost,
             PeriodBill AS PeriodBill,
             CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                  THEN dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                  THEN TPD.StartDate 
                                                  ELSE CI.StartDate END, 
                                             CASE WHEN TPD.EndDate < CI.EndDate 
                                                  THEN TPD.EndDate 
                                                  ELSE CI.EndDate END, 
                                             TPD.StartDate, TPD.EndDate,
                                             @strCompany)
                  ELSE 1 END AS ProrateRatio
             FROM @tabTPD AS TPD
               INNER JOIN @tabAssignment AS A ON TPD.AssignmentID = A.AssignmentID
               INNER JOIN @tabCalendarInterval AS CI ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
          ) AS X
        WHERE (PeriodHrs IS NOT NULL AND ROUND((PeriodHrs * ProrateRatio), @intHrDecimals) != 0)

      -- Adjust Planned Labor time-phased data to compensate for rounding errors.
     
      UPDATE @tabPLabTPD 
        SET 
          PeriodHrs = (TPD.PeriodHrs + D.DeltaHrs),
          PeriodCost = (TPD.PeriodCost + D.DeltaCost),
          PeriodBill = (TPD.PeriodBill + D.DeltaBill)
        FROM @tabPLabTPD AS TPD INNER JOIN 
          (SELECT 
             YTPD.TimePhaseID AS TimePhaseID, 
             ROUND((YTPD.PeriodHrs - SUM(ISNULL(XTPD.PeriodHrs, 0))), @intHrDecimals) AS DeltaHrs,      
             ROUND((YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))), @intAmtCostDecimals) AS DeltaCost,       
             ROUND((YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))), @intAmtBillDecimals) AS DeltaBill       
             FROM @tabPLabTPD AS XTPD INNER JOIN @tabTPD AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
             GROUP BY YTPD.TimePhaseID, YTPD.PeriodHrs, YTPD.PeriodCost, YTPD.PeriodBill) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
        WHERE RowID IN
          (SELECT RowID FROM @tabPLabTPD AS ATPD INNER JOIN
             (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabPLabTPD GROUP BY TimePhaseID) AS BTPD
             ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
        AND (D.DeltaHrs != 0 OR D.DeltaCost != 0 OR D.DeltaBill != 0)

    END /* End If (@strVorN = 'V') */
  ELSE IF (@strVorN = 'N')
    BEGIN

      INSERT @tabAssignment
        (AssignmentID,
         PlanID,
         TaskID,
         PlanCreateDate,
         Employee,
         GenericResourceID,
         LabBillMultiplier
        )
        SELECT
          A.AssignmentID,
          A.PlanID,
          A.TaskID,
          P.CreateDate AS PlanCreateDate,
          A.ResourceID AS Employee,
          A.GenericResourceID AS GenericResourceID,
          P.LabBillMultiplier
          FROM PNAssignment AS A
            INNER JOIN PNPlan AS P ON A.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND A.WBS1 = @strWBS1 AND 
              (ISNULL(A.WBS2, ' ') = @strWBS2) AND 
              (ISNULL(A.WBS3, ' ') = @strWBS3)

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
              
      -- Save Planned Labor time-phased data rows.

      INSERT @tabPLabTPD
        (TimePhaseID,
         PlanID,
         TaskID,
         AssignmentID,
         CIStartDate,
         Employee,
         GenericResourceID,
         StartDate, 
         EndDate, 
         PeriodHrs,
         CostRate,
         BillingRate,
         PeriodCost,
         PeriodBill
        )
        SELECT
          TPD.TimePhaseID AS TimePhaseID,
          TPD.PlanID AS PlanID,
          TPD.TaskID AS TaskID,
          A.AssignmentID AS AssignmentID,
          TPD.StartDate AS CIStartDate,
          A.Employee AS Employee,
          A.GenericResourceID AS GenericResourceID,
          TPD.StartDate AS StartDate, 
          TPD.EndDate AS EndDate,
          TPD.PeriodHrs AS PeriodHrs,
          TPD.CostRate AS BillingRate,
          TPD.BillingRate * A.LabBillMultiplier AS BillingRate,
          TPD.PeriodCost AS PeriodCost,
          TPD.PeriodBill AS PeriodBill
        FROM @tabAssignment AS A
          INNER JOIN PNPlannedLabor AS TPD ON A.PlanID = TPD.PlanID AND A.AssignmentID = TPD.AssignmentID 

    END /* End If (@strVorN = 'N') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT INTO @tabResPlanned
    (TimePhaseID,
     PlanID,
     TaskID,
     AssignmentID,
     Employee,
     GenericResourceID,
     StartDate,
     EndDate,
     PlannedLabHrs,
     CostRate,
     BillingRate,
     PlannedLabCost,
     PlannedLabBill
    )
    SELECT
      MIN(TimePhaseID) AS TimePhaseID,
      MIN(PlanID) AS PlanID,
      MIN(TaskID) AS TaskID,
      MIN(AssignmentID) AS AssignmentID,
      Employee,
      GenericResourceID,
      MIN(Z.StartDate) AS StartDate,
      MAX(Z.EndDate) AS EndDate,
      ISNULL(SUM(Z.PeriodHrs), 0) AS PlannedLabHrs,
      ISNULL(SUM(Z.CostRate), 0) AS CostRate,
      ISNULL(SUM(Z.BillingRate), 0) AS BillingRate,
      ISNULL(SUM(Z.PeriodCost), 0) AS PlannedLabCost,
      ISNULL(SUM(Z.PeriodBill), 0) AS PlannedLabBill
      FROM
        (SELECT
           TPD.TimePhaseID,
           TPD.PlanID,
           TPD.TaskID,
           TPD.Assignmentid,
           TPD.CIStartDate AS CIStartDate,
           TPD.Employee,
           TPD.GenericResourceID,
           TPD.StartDate,
           TPD.EndDate,
           TPD.PeriodHrs AS PeriodHrs,
           TPD.CostRate AS CostRate,
           TPD.BillingRate AS BillingRate,
           TPD.PeriodCost AS PeriodCost,
           TPD.PeriodBill AS PeriodBill
           FROM @tabPLabTPD AS TPD
        ) AS Z
        GROUP BY Z.Employee, Z.GenericResourceID, Z.CIStartDate

  RETURN

END -- PN$tabResPlanned
GO
