SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DW$tabPlannedExpCon]
  (@strCompany nvarchar(14) = ' ',
   @strScale varchar(1) = 'm')
  RETURNS @tabPlannedExpCon TABLE
    (PlanID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     WBS1 nvarchar(30) COLLATE database_default,
     WBS2 nvarchar(30) COLLATE database_default, 
     WBS3 nvarchar(30) COLLATE database_default,
     Account nvarchar(13) COLLATE database_default,
     Vendor nvarchar(20) COLLATE database_default,
     TransactionDate int,
     CostAmount decimal(19,4),
     BillAmount decimal(19,4),
     RevenueAmount decimal(19,4)
    )
BEGIN -- Function DW$tabPlannedExpCon
 
  DECLARE @dtStartDate datetime
  DECLARE @dtEndDate datetime
  DECLARE @dtIntervalEnd datetime
  
  DECLARE @intWkEndDay AS int
  
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intRevDecimals int
    
  DECLARE @intPlannedExpCount int
  DECLARE @intPlannedConCount int
  
  DECLARE @strExpTab varchar(1)
  DECLARE @strConTab varchar(1)
  
  -- Declare Temp tables.

  DECLARE @tabPlan
    TABLE (PlanID varchar(32) COLLATE database_default
           PRIMARY KEY(PlanID)) 
  
  DECLARE @tabCalendarInterval
    TABLE(StartDate datetime,
          EndDate datetime
          PRIMARY KEY(StartDate, EndDate))
          
  DECLARE @tabPExpTPD
    TABLE (RowID int identity(1, 1),
	       TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	       PlanID varchar(32) COLLATE database_default,
	       TaskID varchar(32) COLLATE database_default,
	       ExpenseID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PeriodRev decimal(19,4)
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 

  DECLARE @tabPConTPD
    TABLE (RowID int identity(1, 1),
	       TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	       PlanID varchar(32) COLLATE database_default,
	       TaskID varchar(32) COLLATE database_default,
	       ConsultantID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PeriodRev decimal(19,4)
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 
            
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	
  -- Filter out only Plans with same Company
  
  INSERT @tabPlan(PlanID) SELECT PlanID FROM RPPlan WHERE Company = @strCompany

  -- Get flags to determine which features are being used.
  
  SELECT
     @strExpTab = ExpTab,
     @strConTab = ConTab,
     @intWkEndDay = (CASE WHEN StartingDayOfWeek = 1 THEN 7 ELSE StartingDayOfWeek - 1 END) 
     FROM CFGResourcePlanning
     WHERE CFGResourcePlanning.Company = @strCompany
      
  -- Get decimal settings.
  -- At this point, there is no way to know what was the basis used in calculation of Baseline Revenue numbers.
  -- Therefore, we decided to use the current basis to determine number of decimal digits to be used
  -- in the realignment of Baseline Revenue numbers.
  
  SELECT @intAmtCostDecimals = 2,
         @intAmtBillDecimals = 2,
         @intRevDecimals = 2
    
  -- Check to see if there is any time-phased data
  
    IF (@strExpTab = 'Y')
      BEGIN
        SELECT @intPlannedExpCount = COUNT(*) FROM RPPlannedExpenses AS TPD INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID 
      END

    IF (@strConTab = 'Y')
      BEGIN
        SELECT @intPlannedConCount = COUNT(*) FROM RPPlannedConsultant AS TPD INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID
      END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find the MIN and MAX dates of all TPD.
  
  SELECT @dtStartDate = MIN(MINDate), @dtEndDate = MAX(MAXDate)
    FROM
      (SELECT MIN(StartDate) AS MINDate, MAX(EndDate) AS MAXDate FROM RPPlannedExpenses AS TPD INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID
       UNION
       SELECT MIN(StartDate) AS MINDate, MAX(EndDate) AS MAXDate FROM RPPlannedConsultant AS TPD INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID
      ) AS X
  
  -- Save Calendar Intervals into a temp table and add an overflow Calendar Interval.
      
  WHILE (@dtStartDate <= @dtEndDate)
    BEGIN
        
      -- Compute End Date of interval.

      IF (@strScale = 'd') 
        SET @dtIntervalEnd = @dtStartDate
      ELSE
        SET @dtIntervalEnd = dbo.DLTK$IntervalEnd(@dtStartDate, @strScale, @intWkEndDay)
        
      IF (@dtIntervalEnd > @dtEndDate) 
        SET @dtIntervalEnd = @dtEndDate
            
      -- Insert new Calendar Interval record.
          
      INSERT @tabCalendarInterval(StartDate, EndDate)
        VALUES (@dtStartDate, @dtIntervalEnd)
          
      -- Set Start Date for next interval.
          
      SET @dtStartDate = DATEADD(d, 1, @dtIntervalEnd)
        
    END -- End While
       
       
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
              
  IF (@intPlannedExpCount > 0)
    BEGIN
    
      -- Save Planned Expense time-phased data rows.

      INSERT @tabPExpTPD
        (TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         ExpenseID,
         StartDate, 
         EndDate, 
         PeriodCost, 
         PeriodBill,
         PeriodRev)
         SELECT
           TimePhaseID AS TimePhaseID,
           CIStartDate AS CIStartDate,
           PlanID AS PlanID, 
           TaskID AS TaskID,
           ExpenseID AS ExpenseID,
           StartDate AS StartDate, 
           EndDate AS EndDate, 
           ROUND(ISNULL(PeriodCost * ProrateRatio, 0), @intAmtCostDecimals) AS PeriodCost, 
           ROUND(ISNULL(PeriodBill * ProrateRatio, 0), @intAmtBillDecimals) AS PeriodBill, 
           ROUND(ISNULL(PeriodRev * ProrateRatio, 0), @intRevDecimals) AS PeriodRev
         FROM (SELECT -- For Expense Rows.
                 CI.StartDate AS CIStartDate, 
                 TPD.TimePhaseID AS TimePhaseID,
                 E.PlanID AS PlanID, 
                 E.TaskID AS TaskID,
                 E.ExpenseID AS ExpenseID, 
                 CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                 CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                 PeriodCost AS PeriodCost,
                 PeriodBill AS PeriodBill,
                 PeriodRev AS PeriodRev,
                 CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                      THEN dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                      THEN TPD.StartDate 
                                                      ELSE CI.StartDate END, 
                                                 CASE WHEN TPD.EndDate < CI.EndDate 
                                                      THEN TPD.EndDate 
                                                      ELSE CI.EndDate END, 
                                                 TPD.StartDate, TPD.EndDate,
                                                 @strCompany)
                      ELSE 1 END AS ProrateRatio
                 FROM RPPlannedExpenses AS TPD
                   INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID				  
                   INNER JOIN RPExpense AS E ON TPD.PlanID = E.PlanID AND TPD.TaskID = E.TaskID AND TPD.ExpenseID = E.ExpenseID				   
                   INNER JOIN @tabCalendarInterval AS CI ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
              ) AS X
         WHERE ((PeriodCost IS NOT NULL AND ROUND((PeriodCost * ProrateRatio), @intAmtCostDecimals) != 0) OR 
                (PeriodBill IS NOT NULL AND ROUND((PeriodBill * ProrateRatio), @intAmtBillDecimals) != 0))
     
      -- Adjust Planned Expense time-phased data to compensate for rounding errors.
     
      UPDATE @tabPExpTPD 
        SET 
          PeriodCost = (TPD.PeriodCost + D.DeltaCost),
          PeriodBill = (TPD.PeriodBill + D.DeltaBill),
          PeriodRev = (TPD.PeriodRev + D.DeltaRev)
        FROM @tabPExpTPD AS TPD INNER JOIN 
          (SELECT 
             YTPD.TimePhaseID AS TimePhaseID, 
             ROUND((YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))), @intAmtCostDecimals) AS DeltaCost,
             ROUND((YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))), @intAmtBillDecimals) AS DeltaBill,
             ROUND((YTPD.PeriodRev - SUM(ISNULL(XTPD.PeriodRev, 0))), @intRevDecimals) AS DeltaRev            
             FROM @tabPExpTPD AS XTPD INNER JOIN RPPlannedExpenses AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
             GROUP BY YTPD.TimePhaseID, YTPD.PeriodCost, YTPD.PeriodBill, YTPD.PeriodRev) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
        WHERE (D.DeltaCost != 0 OR D.DeltaBill != 0 OR D.DeltaRev != 0)
          AND RowID IN
            (SELECT RowID FROM @tabPExpTPD AS ATPD INNER JOIN
               (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabPExpTPD GROUP BY TimePhaseID) AS BTPD
                  ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
               
    END -- IF (@intPlannedExpCount > 0)
    
  IF (@intPlannedConCount > 0)
    BEGIN
    
      -- Save Planned Consultant time-phased data rows.

      INSERT @tabPConTPD
        (TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         ConsultantID,
         StartDate, 
         EndDate, 
         PeriodCost, 
         PeriodBill,
         PeriodRev)
         SELECT
           TimePhaseID AS TimePhaseID,
           CIStartDate AS CIStartDate,
           PlanID AS PlanID, 
           TaskID AS TaskID,
           ConsultantID AS ConsultantID,
           StartDate AS StartDate, 
           EndDate AS EndDate, 
           ROUND(ISNULL(PeriodCost * ProrateRatio, 0), @intAmtCostDecimals) AS PeriodCost, 
           ROUND(ISNULL(PeriodBill * ProrateRatio, 0), @intAmtBillDecimals) AS PeriodBill, 
           ROUND(ISNULL(PeriodRev * ProrateRatio, 0), @intRevDecimals) AS PeriodRev
         FROM (SELECT -- For Consultant Rows.
                 CI.StartDate AS CIStartDate, 
                 TPD.TimePhaseID AS TimePhaseID,
                 C.PlanID AS PlanID, 
                 C.TaskID AS TaskID,
                 C.ConsultantID AS ConsultantID, 
                 CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                 CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                 PeriodCost AS PeriodCost,
                 PeriodBill AS PeriodBill,
                 PeriodRev AS PeriodRev,
                 CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                      THEN dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                      THEN TPD.StartDate 
                                                      ELSE CI.StartDate END, 
                                                 CASE WHEN TPD.EndDate < CI.EndDate 
                                                      THEN TPD.EndDate 
                                                      ELSE CI.EndDate END, 
                                                 TPD.StartDate, TPD.EndDate,
                                                 @strCompany)
                      ELSE 1 END AS ProrateRatio
                 FROM RPPlannedConsultant AS TPD
                   INNER JOIN @tabPlan AS P ON TPD.PlanID = P.PlanID
                   INNER JOIN RPConsultant AS C ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID AND TPD.ConsultantID = C.ConsultantID
                   INNER JOIN @tabCalendarInterval AS CI ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
              ) AS X
         WHERE ((PeriodCost IS NOT NULL AND ROUND((PeriodCost * ProrateRatio), @intAmtCostDecimals) != 0) OR 
                (PeriodBill IS NOT NULL AND ROUND((PeriodBill * ProrateRatio), @intAmtBillDecimals) != 0))
     
      -- Adjust Planned Consultant time-phased data to compensate for rounding errors.
     
      UPDATE @tabPConTPD 
        SET 
          PeriodCost = (TPD.PeriodCost + D.DeltaCost),
          PeriodBill = (TPD.PeriodBill + D.DeltaBill),
          PeriodRev = (TPD.PeriodRev + D.DeltaRev)
        FROM @tabPConTPD AS TPD INNER JOIN 
          (SELECT 
             YTPD.TimePhaseID AS TimePhaseID, 
             ROUND((YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))), @intAmtCostDecimals) AS DeltaCost,
             ROUND((YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))), @intAmtBillDecimals) AS DeltaBill,
             ROUND((YTPD.PeriodRev - SUM(ISNULL(XTPD.PeriodRev, 0))), @intRevDecimals) AS DeltaRev            
             FROM @tabPConTPD AS XTPD INNER JOIN RPPlannedConsultant AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
             GROUP BY YTPD.TimePhaseID, YTPD.PeriodCost, YTPD.PeriodBill, YTPD.PeriodRev) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
        WHERE (D.DeltaCost != 0 OR D.DeltaBill != 0 OR D.DeltaRev != 0)
          AND RowID IN
            (SELECT RowID FROM @tabPConTPD AS ATPD INNER JOIN
               (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabPConTPD GROUP BY TimePhaseID) AS BTPD
                  ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
               
    END -- IF (@intPlannedConCount > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT INTO @tabPlannedExpCon
    (PlanID,
     TaskID,
     WBS1,
     WBS2, 
     WBS3,
     Account,
     Vendor,
     TransactionDate,
     CostAmount,
     BillAmount,
     RevenueAmount
    )
    SELECT
      Z.PlanID,
      ISNULL(Z.RowID, Z.TaskID) AS TaskID,
      ISNULL(T.WBS1, ' ') AS WBS1,
      ISNULL(T.WBS2, ' ') AS WBS2,
      ISNULL(T.WBS3, ' ') AS WBS3,
      Z.Account As Account,
      Z.Vendor AS Vendor,
      ISNULL(CONVERT(INT, MIN(Z.StartDate)), -1) AS TransactionDate,
      ISNULL(SUM(Z.CostAmount), 0) AS CostAmount,
      ISNULL(SUM(Z.BillAmount), 0) AS BillAmount,
      ISNULL(SUM(Z.RevenueAmount), 0) AS RevenueAmount
      FROM
        (SELECT
           TPD.CIStartDate AS CIStartDate,
           TPD.PlanID AS PlanID,
           TPD.TaskID AS TaskID,
           TPD.ExpenseID AS RowID,
           E.Account As Account,
           E.Vendor AS Vendor,
           TPD.StartDate,
           TPD.EndDate,
           TPD.PeriodCost AS CostAmount,
           TPD.PeriodBill AS BillAmount,
           TPD.PeriodRev AS RevenueAmount		   
           FROM @tabPExpTPD AS TPD
             LEFT JOIN RPExpense AS E ON TPD.PlanID = E.PlanID AND TPD.TaskID = E.TaskID AND TPD.ExpenseID = E.ExpenseID			 
         UNION ALL
         SELECT
           TPD.CIStartDate AS CIStartDate,
           TPD.PlanID AS PlanID,
           TPD.TaskID AS TaskID,
           TPD.ConsultantID AS RowID,
           C.Account As Account,
           C.Vendor AS Vendor,
           TPD.StartDate,
           TPD.EndDate,
           TPD.PeriodCost AS CostAmount,
           TPD.PeriodBill AS BillAmount,
           TPD.PeriodRev AS RevenueAmount
           FROM @tabPConTPD AS TPD
             LEFT JOIN RPConsultant AS C ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID AND TPD.ConsultantID = C.ConsultantID			 
        ) AS Z
        INNER JOIN RPTask AS T ON Z.PlanID = T.PlanID AND Z.TaskID = T.TaskID
      GROUP BY Z.PlanID, Z.TaskID, Z.RowID, Z.CIStartDate, T.WBS1, T.WBS2, T.WBS3, Z.Account, Z.Vendor

  RETURN

END -- DW$tabPlannedExpCon

GO
