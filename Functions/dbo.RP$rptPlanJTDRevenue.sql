SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$rptPlanJTDRevenue]
  (@strPlanID varchar(32),
   @dtETCDate datetime)
RETURNS @planJTDRevenue TABLE
   (PlanID varchar(32) COLLATE database_default,
    JTDRevenueCurrencyCode Nvarchar(3) COLLATE database_default,
    ActualRevenue decimal(19,4))
BEGIN 
  
  INSERT @planJTDRevenue
    SELECT 
	  PlanID,
	  JTDRevenueCurrencyCode = MAX(JTDRevenueCurrencyCode),
      ActualRevenue = SUM(ActualRevenue)
      FROM
        (SELECT X.PlanID AS PlanID, 
                   Max(PR.ProjectCurrencyCode) as JTDRevenueCurrencyCode,
                   SUM(-AmountProjectCurrency) AS ActualRevenue
        FROM LedgerAR AS Ledger 
        INNER JOIN PR on PR.WBS1=Ledger.WBS1 and PR.WBS2='' and PR.WBS3=''
        INNER JOIN RPTask AS X ON (Ledger.WBS1 = X.WBS1 AND X.PlanID = @strPlanID)  
        INNER JOIN (SELECT MIN(OutlineLevel) AS OutlineLevel 
                   FROM RPTask AS XT INNER JOIN RPWBSLevelFormat AS F ON XT.PlanID = F.PlanID 
                   AND XT.WBSType = F.WBSType AND F.WBSMatch = 'Y' 
                   WHERE XT.PlanID = @strPlanID) AS ZT ON X.OutlineLevel = ZT.OutlineLevel
        INNER JOIN CA ON Ledger.Account = CA.Account
        WHERE Ledger.WBS2 LIKE (ISNULL(X.WBS2, '%')) AND Ledger.WBS3 LIKE (ISNULL(X.WBS3, '%')) 
	      AND (CA.Type = 4 
             AND NOT (Ledger.TransType='IN' AND (Ledger.SubType IS NOT NULL AND Ledger.SubType IN ('R','I')))) 
             AND Ledger.TransDate <= @dtETCDate 
        GROUP BY X.PlanID, PR.ProjectCurrencyCode
        UNION ALL SELECT X.PlanID AS PlanID, 
                   Max(PR.ProjectCurrencyCode) as JTDRevenueCurrencyCode, 
                   SUM(-AmountProjectCurrency) AS ActualRevenue
        FROM LedgerAP AS Ledger 
        INNER JOIN PR on PR.WBS1=Ledger.WBS1 and PR.WBS2='' and PR.WBS3=''
        INNER JOIN RPTask AS X ON (Ledger.WBS1 = X.WBS1 AND X.PlanID = @strPlanID) 
        INNER JOIN (SELECT MIN(OutlineLevel) AS OutlineLevel 
                   FROM RPTask AS XT INNER JOIN RPWBSLevelFormat AS F ON XT.PlanID = F.PlanID 
                   AND XT.WBSType = F.WBSType AND F.WBSMatch = 'Y' 
                   WHERE XT.PlanID = @strPlanID) AS ZT ON X.OutlineLevel = ZT.OutlineLevel 
        INNER JOIN CA ON Ledger.Account = CA.Account
        WHERE Ledger.WBS2 LIKE (ISNULL(X.WBS2, '%')) AND Ledger.WBS3 LIKE (ISNULL(X.WBS3, '%')) 
	      AND (CA.Type = 4 
             AND NOT (Ledger.TransType='IN' AND (Ledger.SubType IS NOT NULL AND Ledger.SubType IN ('R','I')))) 
             AND Ledger.TransDate <= @dtETCDate 
        GROUP BY X.PlanID, PR.ProjectCurrencyCode
        UNION ALL SELECT X.PlanID AS PlanID, 
                   Max(PR.ProjectCurrencyCode) as JTDRevenueCurrencyCode,
                   SUM(-AmountProjectCurrency) AS ActualRevenue
        FROM LedgerEX AS Ledger 
        INNER JOIN PR on PR.WBS1=Ledger.WBS1 and PR.WBS2='' and PR.WBS3=''
        INNER JOIN RPTask AS X ON (Ledger.WBS1 = X.WBS1 AND X.PlanID = @strPlanID) 
        INNER JOIN (SELECT MIN(OutlineLevel) AS OutlineLevel 
                   FROM RPTask AS XT INNER JOIN RPWBSLevelFormat AS F ON XT.PlanID = F.PlanID 
                   AND XT.WBSType = F.WBSType AND F.WBSMatch = 'Y' 
                   WHERE XT.PlanID = @strPlanID) AS ZT ON X.OutlineLevel = ZT.OutlineLevel 
        INNER JOIN CA ON Ledger.Account = CA.Account
        WHERE Ledger.WBS2 LIKE (ISNULL(X.WBS2, '%')) AND Ledger.WBS3 LIKE (ISNULL(X.WBS3, '%')) 
	      AND (CA.Type = 4 
             AND NOT (Ledger.TransType='IN' AND (Ledger.SubType IS NOT NULL AND Ledger.SubType IN ('R','I')))) 
             AND Ledger.TransDate <= @dtETCDate 
        GROUP BY X.PlanID, PR.ProjectCurrencyCode
        UNION ALL SELECT X.PlanID AS PlanID, 
                   Max(PR.ProjectCurrencyCode) as JTDRevenueCurrencyCode,
                   SUM(-AmountProjectCurrency) AS ActualRevenue
        FROM LedgerMisc AS Ledger 
        INNER JOIN PR on PR.WBS1=Ledger.WBS1 and PR.WBS2='' and PR.WBS3=''
        INNER JOIN RPTask AS X ON (Ledger.WBS1 = X.WBS1 AND X.PlanID = @strPlanID) 
        INNER JOIN (SELECT MIN(OutlineLevel) AS OutlineLevel 
                   FROM RPTask AS XT INNER JOIN RPWBSLevelFormat AS F ON XT.PlanID = F.PlanID 
                   AND XT.WBSType = F.WBSType AND F.WBSMatch = 'Y' 
                   WHERE XT.PlanID = @strPlanID) AS ZT ON X.OutlineLevel = ZT.OutlineLevel 
        INNER JOIN CA ON Ledger.Account = CA.Account
        WHERE Ledger.WBS2 LIKE (ISNULL(X.WBS2, '%')) AND Ledger.WBS3 LIKE (ISNULL(X.WBS3, '%')) 
	      AND (CA.Type = 4 
             AND NOT (Ledger.TransType='IN' AND (Ledger.SubType IS NOT NULL AND Ledger.SubType IN ('R','I')))) 
             AND Ledger.TransDate <= @dtETCDate 
        GROUP BY X.PlanID, PR.ProjectCurrencyCode) X
      GROUP by PlanID, JTDRevenueCurrencyCode

  RETURN 
END

GO
