SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$tabAddJTDResource](
  @strPlanID Nvarchar(255),
  @dtJTDDate  varchar(8),
  @unPosted Nvarchar(1)  
)
  RETURNS @tabAddJTDResource TABLE (
        ResourceName Nvarchar(255) COLLATE database_default,
		WBS1 Nvarchar(255) COLLATE database_default,
		WBS2 Nvarchar(7) COLLATE database_default,
		WBS3 Nvarchar(7) COLLATE database_default,
		LaborCode Nvarchar(14) COLLATE database_default,
		WBS1Name Nvarchar(255) COLLATE database_default,
		WBS2Name Nvarchar(255) COLLATE database_default,
		WBS3Name Nvarchar(255) COLLATE database_default,
		LCName  Nvarchar(Max) COLLATE database_default,
		Resource Nvarchar(20) COLLATE database_default,
		PeriodHrs decimal(19,4),
		HasAssignment varchar(1) COLLATE database_default
)

BEGIN
	DECLARE @tabLD TABLE (
        WBS1 Nvarchar(255) COLLATE database_default,
		WBS2 Nvarchar(7) COLLATE database_default,
		WBS3 Nvarchar(7) COLLATE database_default,
		LaborCode Nvarchar(14) COLLATE database_default,
		PeriodHrs decimal(19,4),
		TransDate datetime,
		Employee Nvarchar(20) COLLATE database_default
    UNIQUE(WBS1, WBS2, WBS3,LaborCode,Employee)
  )

  DECLARE @tabtkDetail TABLE (
        WBS1 Nvarchar(255) COLLATE database_default,
		WBS2 Nvarchar(7) COLLATE database_default,
		WBS3 Nvarchar(7) COLLATE database_default,
		LaborCode Nvarchar(14) COLLATE database_default,
		PeriodHrs decimal(19,4),
		TransDate datetime,
		EndDate datetime,
		Employee Nvarchar(20) COLLATE database_default
    UNIQUE(WBS1, WBS2, WBS3,LaborCode,Employee)
  )

  DECLARE @tabtsDetail TABLE (
        WBS1 Nvarchar(255) COLLATE database_default,
		WBS2 Nvarchar(7) COLLATE database_default,
		WBS3 Nvarchar(7) COLLATE database_default,
		LaborCode Nvarchar(14) COLLATE database_default,
		PeriodHrs decimal(19,4),
		TransDate datetime,
		Batch Nvarchar(30) COLLATE database_default,
		Employee Nvarchar(20) COLLATE database_default
    UNIQUE(WBS1, WBS2, WBS3,LaborCode,Employee)
  )


  INSERT @tabLD(
    WBS1,
    WBS2,
    WBS3,
	LaborCode,
    Employee,   
	TransDate,
	PeriodHrs
  )
	select WBS1, WBS2,WBS3,LaborCode,Employee,max(TransDate) as TransDate,SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs from LD
	Where WBS1 in (Select WBS1 from RPTask where WBSType = 'WBS1' and PlanID = @strPlanID)
	and ProjectCost = 'Y'
    and TransDate <= getDate()
	GROUP BY WBS1
			,WBS2
			,WBS3
			,LaborCode
			,Employee

 INSERT @tabtkDetail(
    WBS1,
    WBS2,
    WBS3,
	LaborCode,
    Employee,   
	TransDate,
	EndDate,
	PeriodHrs
  )
	select WBS1, WBS2,WBS3,LaborCode,Employee,max(TransDate) as TransDate,max(EndDate) as EndDate,SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs from tkDetail
	Where WBS1 in (Select WBS1 from RPTask where WBSType = 'WBS1' and PlanID = @strPlanID)
	and TransDate <= getDate()

	GROUP BY WBS1
			,WBS2
			,WBS3
			,LaborCode
			,Employee

INSERT @tabtsDetail(
     WBS1,
    WBS2,
    WBS3,
	LaborCode,
    Employee,   
	TransDate,
	Batch,
	PeriodHrs
  )
	select WBS1, WBS2,WBS3,LaborCode,Employee,max(TransDate) as TransDate,Max(Batch) as Batch,SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs from tsDetail
	Where WBS1 in (Select WBS1 from RPTask where WBSType = 'WBS1' and PlanID = @strPlanID)
    and TransDate <= getDate()
	GROUP BY WBS1
			,WBS2
			,WBS3
			,LaborCode
			,Employee

if (@unPosted = 'Y')
BEGIN
INSERT @tabAddJTDResource(
	    ResourceName  ,
		WBS1 ,
		WBS2  ,
		WBS3  ,
		WBS1Name  ,
		WBS2Name  ,
		WBS3Name  ,
		LaborCode  ,
		LCName  ,
		Resource,
		PeriodHrs  ,
		HasAssignment  
)
SELECT  IsNull(EM.LastName, '') + IsNull(', ' + EM.FirstName, '') AS ResourceName
		,LD.WBS1 AS WBS1
		,LD.WBS2 AS WBS2
		,LD.WBS3 AS WBS3
		, (PR1.NAME) AS WBS1Name
		, (CASE 
				WHEN LD.WBS2 = N''
					THEN ''
				ELSE PR2.NAME
				END) AS WBS2Name
		, (CASE 
				WHEN LD.WBS3 = N''
					THEN ''
				ELSE PR3.NAME
				END) AS WBS3Name
		,ISNULL(LD.LaborCode, '%') AS LaborCode
		, (LC1.Label + (
				CASE 
					WHEN LC2.Label IS NULL
						THEN ''
					ELSE '/' + LC2.Label
					END
				) + (
				CASE 
					WHEN LC3.Label IS NULL
						THEN ''
					ELSE '/' + LC3.Label
					END
				) + (
				CASE 
					WHEN LC4.Label IS NULL
						THEN ''
					ELSE '/' + LC4.Label
					END
				) + (
				CASE 
					WHEN LC5.Label IS NULL
						THEN ''
					ELSE '/' + LC5.Label
					END
				)) AS LCName
		,ISNULL(LD.Employee, '') AS Resource
		, PeriodHrs
		,'N' AS HasAssignment
	FROM @tabLD as LD
	INNER JOIN (
		SELECT DISTINCT WBS1
			,WBS2
			,WBS3
			,LaborCode
		FROM RPTask AS T
		INNER JOIN (
			SELECT MIN(OutlineLevel) AS OutlineLevel
			FROM RPTask AS XT
			INNER JOIN RPWBSLevelFormat AS F ON XT.PlanID = F.PlanID
				AND XT.WBSType = F.WBSType
				AND F.WBSMatch = 'Y'
			WHERE XT.PlanID = @strPlanID
			) AS ZT ON T.OutlineLevel = ZT.OutlineLevel
		WHERE T.PlanID = @strPlanID
			AND T.WBS1 IS NOT NULL
			AND T.WBS1 != '<none>'
		) AS T ON (
			LD.WBS1 = T.WBS1
			AND LD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
			AND LD.WBS3 LIKE (ISNULL(T.WBS3, '%'))
			AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%'))
			)
	INNER JOIN (
		SELECT StartDate
			,EndDate
		FROM (
			SELECT StartDate AS StartDate
				,EndDate AS EndDate
			FROM RPCalendarInterval
			WHERE PlanID = @strPlanID
			
			UNION ALL
			
			SELECT CAST('19300101' AS DATETIME) AS StartDate
				,DATEADD(d, - 1, MIN(StartDate)) AS EndDate
			FROM RPCalendarInterval
			WHERE PlanID = @strPlanID
			GROUP BY PlanID
			
			UNION ALL
			
			SELECT DATEADD(d, 1, MAX(EndDate)) AS StartDate
				,DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate
			FROM RPCalendarInterval
			WHERE PlanID = @strPlanID
			) AS CI
		) AS CI ON LD.TransDate BETWEEN CI.StartDate
			AND CI.EndDate
	INNER JOIN EM ON EM.Employee = LD.Employee
		AND EM.STATUS <> 'T'
	INNER JOIN CFGFormat F ON 1 = 1
	LEFT JOIN CFGLCCodes AS LC1 ON (
			LC1.LCLevel = 1
			AND LC1.Code = SUBSTRING(LD.LaborCode, F.LC1Start, F.LC1Length)
			)
	LEFT JOIN CFGLCCodes AS LC2 ON (
			LC2.LCLevel = 2
			AND LC2.Code = SUBSTRING(LD.LaborCode, F.LC2Start, F.LC2Length)
			)
	LEFT JOIN CFGLCCodes AS LC3 ON (
			LC3.LCLevel = 3
			AND LC3.Code = SUBSTRING(LD.LaborCode, F.LC3Start, F.LC3Length)
			)
	LEFT JOIN CFGLCCodes AS LC4 ON (
			LC4.LCLevel = 4
			AND LC4.Code = SUBSTRING(LD.LaborCode, F.LC4Start, F.LC4Length)
			)
	LEFT JOIN CFGLCCodes AS LC5 ON (
			LC5.LCLevel = 5
			AND LC5.Code = SUBSTRING(LD.LaborCode, F.LC5Start, F.LC5Length)
			)
	LEFT JOIN PR AS PR1 ON PR1.WBS1 = LD.WBS1 
    AND PR1.WBS2 = ' ' AND LD.WBS2 = ' '
    AND PR1.WBS3 = ' ' AND LD.WBS3 = ' '


   LEFT JOIN PR AS PR2 ON PR2.WBS1 = LD.WBS1
   AND PR2.WBS2 = LD.WBS2
   AND PR2.WBS3 = ' ' AND LD.WBS3 = ' '

	LEFT JOIN PR AS PR3 ON PR3.WBS1 = LD.WBS1
    AND PR3.WBS2 = LD.WBS2
    AND PR3.WBS3 = LD.WBS3

	 
	

	UNION ALL
	
	SELECT IsNull(EM.LastName, '') + IsNull(', ' + EM.FirstName, '') AS ResourceName
		,TD.WBS1 AS WBS1
		,TD.WBS2 AS WBS2
		,TD.WBS3 AS WBS3
		, (PR1.NAME) AS WBS1Name
		, (CASE 
				WHEN TD.WBS2 = N''
					THEN ''
				ELSE PR2.NAME
				END) AS WBS2Name
		, (CASE 
				WHEN TD.WBS3 = N''
					THEN ''
				ELSE PR3.NAME
				END) AS WBS3Name
		,ISNULL(TD.LaborCode, '%') AS LaborCode
		, (LC1.Label + (
				CASE 
					WHEN LC2.Label IS NULL
						THEN ''
					ELSE '/' + LC2.Label
					END
				) + (
				CASE 
					WHEN LC3.Label IS NULL
						THEN ''
					ELSE '/' + LC3.Label
					END
				) + (
				CASE 
					WHEN LC4.Label IS NULL
						THEN ''
					ELSE '/' + LC4.Label
					END
				) + (
				CASE 
					WHEN LC5.Label IS NULL
						THEN ''
					ELSE '/' + LC5.Label
					END
				)) AS LCName
		,ISNULL(TD.Employee, '') AS Resource
		,  PeriodHrs
		,'N' AS HasAssignment
	FROM @tabtkDetail TD
	INNER JOIN (
		SELECT DISTINCT WBS1
			,WBS2
			,WBS3
			,LaborCode
		FROM RPTask AS T
		INNER JOIN (
			SELECT MIN(OutlineLevel) AS OutlineLevel
			FROM RPTask AS XT
			INNER JOIN RPWBSLevelFormat AS F ON XT.PlanID = F.PlanID
				AND XT.WBSType = F.WBSType
				AND F.WBSMatch = 'Y'
			WHERE XT.PlanID = @strPlanID
			) AS ZT ON T.OutlineLevel = ZT.OutlineLevel
		WHERE T.PlanID = @strPlanID
			AND T.WBS1 IS NOT NULL
			AND T.WBS1 != '<none>'
		) AS T ON (
			tD.WBS1 = T.WBS1
			AND TD.TransDate <= @dtJTDDate  
			AND TD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
			AND TD.WBS3 LIKE (ISNULL(T.WBS3, '%'))
			AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%'))
			)
	INNER JOIN (
		SELECT StartDate
			,EndDate
		FROM (
			SELECT StartDate AS StartDate
				,EndDate AS EndDate
			FROM RPCalendarInterval
			WHERE PlanID = @strPlanID
			
			UNION ALL
			
			SELECT CAST('19300101' AS DATETIME) AS StartDate
				,DATEADD(d, - 1, MIN(StartDate)) AS EndDate
			FROM RPCalendarInterval
			WHERE PlanID = @strPlanID
			GROUP BY PlanID
			
			UNION ALL
			
			SELECT DATEADD(d, 1, MAX(EndDate)) AS StartDate
				,DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate
			FROM RPCalendarInterval
			WHERE PlanID = @strPlanID
			) AS CI
		) AS CI ON TD.TransDate BETWEEN CI.StartDate
			AND CI.EndDate
	INNER JOIN EM ON TD.Employee = EM.Employee
		AND EM.STATUS <> 'T'
	INNER JOIN tkMaster AS TM ON (
			TD.Employee = TM.Employee
			AND TM.Submitted <> 'P'
			AND TD.EndDate = TM.EndDate
			)
	INNER JOIN CFGFormat F ON 1 = 1
	LEFT JOIN CFGLCCodes AS LC1 ON (
			LC1.LCLevel = 1
			AND LC1.Code = SUBSTRING(TD.LaborCode, F.LC1Start, F.LC1Length)
			)
	LEFT JOIN CFGLCCodes AS LC2 ON (
			LC2.LCLevel = 2
			AND LC2.Code = SUBSTRING(TD.LaborCode, F.LC2Start, F.LC2Length)
			)
	LEFT JOIN CFGLCCodes AS LC3 ON (
			LC3.LCLevel = 3
			AND LC3.Code = SUBSTRING(TD.LaborCode, F.LC3Start, F.LC3Length)
			)
	LEFT JOIN CFGLCCodes AS LC4 ON (
			LC4.LCLevel = 4
			AND LC4.Code = SUBSTRING(TD.LaborCode, F.LC4Start, F.LC4Length)
			)
	LEFT JOIN CFGLCCodes AS LC5 ON (
			LC5.LCLevel = 5
			AND LC5.Code = SUBSTRING(TD.LaborCode, F.LC5Start, F.LC5Length)
			)
	LEFT JOIN PR AS PR1 ON PR1.WBS1 = TD.WBS1 
    AND PR1.WBS2 = ' ' AND TD.WBS2 = ' '
    AND PR1.WBS3 = ' ' AND TD.WBS3 = ' '


   LEFT JOIN PR AS PR2 ON PR2.WBS1 = TD.WBS1
   AND PR2.WBS2 = TD.WBS2
   AND PR2.WBS3 = ' ' AND TD.WBS3 = ' '

	LEFT JOIN PR AS PR3 ON PR3.WBS1 = TD.WBS1
    AND PR3.WBS2 = TD.WBS2
    AND PR3.WBS3 = TD.WBS3
	 	
	UNION ALL
	
	SELECT  IsNull(EM.LastName, '') + IsNull(', ' + EM.FirstName, '') AS ResourceName
		,TD.WBS1
		,TD.WBS2
		,TD.WBS3
		, (PR1.NAME) AS WBS1Name
		, (CASE 
				WHEN TD.WBS2 = N''
					THEN ''
				ELSE PR2.NAME
				END) AS WBS2Name
		, (CASE 
				WHEN TD.WBS3 = N''
					THEN ''
				ELSE PR3.NAME
				END) AS WBS3Name
		,TD.LaborCode
		, (LC1.Label + (
				CASE 
					WHEN LC2.Label IS NULL
						THEN ''
					ELSE '/' + LC2.Label
					END
				) + (
				CASE 
					WHEN LC3.Label IS NULL
						THEN ''
					ELSE '/' + LC3.Label
					END
				) + (
				CASE 
					WHEN LC4.Label IS NULL
						THEN ''
					ELSE '/' + LC4.Label
					END
				) + (
				CASE 
					WHEN LC5.Label IS NULL
						THEN ''
					ELSE '/' + LC5.Label
					END
				)) AS LCName
		,TD.Employee AS Resource
		,  PeriodHrs
		,'N' AS HasAssignment
	FROM @tabtsDetail AS TD
	INNER JOIN (
		SELECT DISTINCT WBS1
			,WBS2
			,WBS3
			,LaborCode
		FROM RPTask AS T
		INNER JOIN (
			SELECT MIN(OutlineLevel) AS OutlineLevel
			FROM RPTask AS XT
			INNER JOIN RPWBSLevelFormat AS F ON XT.PlanID = F.PlanID
				AND XT.WBSType = F.WBSType
				AND F.WBSMatch = 'Y'
			WHERE XT.PlanID = @strPlanID
			) AS ZT ON T.OutlineLevel = ZT.OutlineLevel
		WHERE T.PlanID = @strPlanID
			AND T.WBS1 IS NOT NULL
			AND T.WBS1 != '<none>'
		) AS T ON (
			TD.WBS1 = T.WBS1
			AND TD.TransDate <= @dtJTDDate 
			AND TD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
			AND TD.WBS3 LIKE (ISNULL(T.WBS3, '%'))
			AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%'))
			)
	INNER JOIN (
		SELECT StartDate
			,EndDate
		FROM (
			SELECT StartDate AS StartDate
				,EndDate AS EndDate
			FROM RPCalendarInterval
			WHERE PlanID = @strPlanID
			
			UNION ALL
			
			SELECT CAST('19300101' AS DATETIME) AS StartDate
				,DATEADD(d, - 1, MIN(StartDate)) AS EndDate
			FROM RPCalendarInterval
			WHERE PlanID = @strPlanID
			GROUP BY PlanID
			
			UNION ALL
			
			SELECT DATEADD(d, 1, MAX(EndDate)) AS StartDate
				,DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate
			FROM RPCalendarInterval
			WHERE PlanID = @strPlanID
			) AS CI
		) AS CI ON TD.TransDate BETWEEN CI.StartDate
			AND CI.EndDate
	INNER JOIN EM ON TD.Employee = EM.Employee
		AND EM.STATUS <> 'T'
	INNER JOIN tsControl AS TC ON (
			TC.Posted = 'N'
			AND TD.Batch = TC.Batch
			)
	INNER JOIN CFGFormat F ON 1 = 1
	LEFT JOIN CFGLCCodes AS LC1 ON (
			LC1.LCLevel = 1
			AND LC1.Code = SUBSTRING(TD.LaborCode, F.LC1Start, F.LC1Length)
			)
	LEFT JOIN CFGLCCodes AS LC2 ON (
			LC2.LCLevel = 2
			AND LC2.Code = SUBSTRING(TD.LaborCode, F.LC2Start, F.LC2Length)
			)
	LEFT JOIN CFGLCCodes AS LC3 ON (
			LC3.LCLevel = 3
			AND LC3.Code = SUBSTRING(TD.LaborCode, F.LC3Start, F.LC3Length)
			)
	LEFT JOIN CFGLCCodes AS LC4 ON (
			LC4.LCLevel = 4
			AND LC4.Code = SUBSTRING(TD.LaborCode, F.LC4Start, F.LC4Length)
			)
	LEFT JOIN CFGLCCodes AS LC5 ON (
			LC5.LCLevel = 5
			AND LC5.Code = SUBSTRING(TD.LaborCode, F.LC5Start, F.LC5Length)
			)
	LEFT JOIN PR AS PR1 ON PR1.WBS1 = TD.WBS1 
    AND PR1.WBS2 = ' ' AND TD.WBS2 = ' '
    AND PR1.WBS3 = ' ' AND TD.WBS3 = ' '


   LEFT JOIN PR AS PR2 ON PR2.WBS1 = TD.WBS1
   AND PR2.WBS2 = TD.WBS2
   AND PR2.WBS3 = ' ' AND TD.WBS3 = ' '

	LEFT JOIN PR AS PR3 ON PR3.WBS1 = TD.WBS1
    AND PR3.WBS2 = TD.WBS2
    AND PR3.WBS3 = TD.WBS3
	END /* if (@unPosted = 'Y')*/

	ELSE IF (@unPosted = 'n')
    BEGIN
	INSERT @tabAddJTDResource(
	    ResourceName  ,
		WBS1 ,
		WBS2  ,
		WBS3  ,
		WBS1Name  ,
		WBS2Name  ,
		WBS3Name  ,
		LaborCode  ,
		LCName  ,
		Resource,
		PeriodHrs  ,
		HasAssignment  
)
SELECT  IsNull(EM.LastName, '') + IsNull(', ' + EM.FirstName, '') AS ResourceName
		,LD.WBS1 AS WBS1
		,LD.WBS2 AS WBS2
		,LD.WBS3 AS WBS3
		, (PR1.NAME) AS WBS1Name
		, (CASE 
				WHEN LD.WBS2 = N''
					THEN ''
				ELSE PR2.NAME
				END) AS WBS2Name
		, (CASE 
				WHEN LD.WBS3 = N''
					THEN ''
				ELSE PR3.NAME
				END) AS WBS3Name
		,ISNULL(LD.LaborCode, '%') AS LaborCode
		, (LC1.Label + (
				CASE 
					WHEN LC2.Label IS NULL
						THEN ''
					ELSE '/' + LC2.Label
					END
				) + (
				CASE 
					WHEN LC3.Label IS NULL
						THEN ''
					ELSE '/' + LC3.Label
					END
				) + (
				CASE 
					WHEN LC4.Label IS NULL
						THEN ''
					ELSE '/' + LC4.Label
					END
				) + (
				CASE 
					WHEN LC5.Label IS NULL
						THEN ''
					ELSE '/' + LC5.Label
					END
				)) AS LCName
		,ISNULL(LD.Employee, '') AS Resource
		, PeriodHrs
		,'N' AS HasAssignment
	FROM @tabLD as LD
	INNER JOIN (
		SELECT DISTINCT WBS1
			,WBS2
			,WBS3
			,LaborCode
		FROM RPTask AS T
		INNER JOIN (
			SELECT MIN(OutlineLevel) AS OutlineLevel
			FROM RPTask AS XT
			INNER JOIN RPWBSLevelFormat AS F ON XT.PlanID = F.PlanID
				AND XT.WBSType = F.WBSType
				AND F.WBSMatch = 'Y'
			WHERE XT.PlanID = @strPlanID
			) AS ZT ON T.OutlineLevel = ZT.OutlineLevel
		WHERE T.PlanID = @strPlanID
			AND T.WBS1 IS NOT NULL
			AND T.WBS1 != '<none>'
		) AS T ON (
			LD.WBS1 = T.WBS1
			AND LD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
			AND LD.WBS3 LIKE (ISNULL(T.WBS3, '%'))
			AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%'))
			)
	INNER JOIN (
		SELECT StartDate
			,EndDate
		FROM (
			SELECT StartDate AS StartDate
				,EndDate AS EndDate
			FROM RPCalendarInterval
			WHERE PlanID = @strPlanID
			
			UNION ALL
			
			SELECT CAST('19300101' AS DATETIME) AS StartDate
				,DATEADD(d, - 1, MIN(StartDate)) AS EndDate
			FROM RPCalendarInterval
			WHERE PlanID = @strPlanID
			GROUP BY PlanID
			
			UNION ALL
			
			SELECT DATEADD(d, 1, MAX(EndDate)) AS StartDate
				,DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate
			FROM RPCalendarInterval
			WHERE PlanID = @strPlanID
			) AS CI
		) AS CI ON LD.TransDate BETWEEN CI.StartDate
			AND CI.EndDate
	INNER JOIN EM ON EM.Employee = LD.Employee
		AND EM.STATUS <> 'T'
	INNER JOIN CFGFormat F ON 1 = 1
	LEFT JOIN CFGLCCodes AS LC1 ON (
			LC1.LCLevel = 1
			AND LC1.Code = SUBSTRING(LD.LaborCode, F.LC1Start, F.LC1Length)
			)
	LEFT JOIN CFGLCCodes AS LC2 ON (
			LC2.LCLevel = 2
			AND LC2.Code = SUBSTRING(LD.LaborCode, F.LC2Start, F.LC2Length)
			)
	LEFT JOIN CFGLCCodes AS LC3 ON (
			LC3.LCLevel = 3
			AND LC3.Code = SUBSTRING(LD.LaborCode, F.LC3Start, F.LC3Length)
			)
	LEFT JOIN CFGLCCodes AS LC4 ON (
			LC4.LCLevel = 4
			AND LC4.Code = SUBSTRING(LD.LaborCode, F.LC4Start, F.LC4Length)
			)
	LEFT JOIN CFGLCCodes AS LC5 ON (
			LC5.LCLevel = 5
			AND LC5.Code = SUBSTRING(LD.LaborCode, F.LC5Start, F.LC5Length)
			)
	LEFT JOIN PR AS PR1 ON PR1.WBS1 = LD.WBS1 
    AND PR1.WBS2 = ' ' AND LD.WBS2 = ' '
    AND PR1.WBS3 = ' ' AND LD.WBS3 = ' '


   LEFT JOIN PR AS PR2 ON PR2.WBS1 = LD.WBS1
   AND PR2.WBS2 = LD.WBS2
   AND PR2.WBS3 = ' ' AND LD.WBS3 = ' '

	LEFT JOIN PR AS PR3 ON PR3.WBS1 = LD.WBS1
    AND PR3.WBS2 = LD.WBS2
    AND PR3.WBS3 = LD.WBS3
	END
  RETURN
  END
GO
