SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[fnCCG_EI_PriorFeeBilled] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), @BTLevel int, @FeeMeth int)
RETURNS @T TABLE 

(
       PriorFee           money,
       Detail		      varchar(100)
)
AS BEGIN
/*
	Copyright (c) 2017 EleVia Software. All rights reserved.
*/
	declare @res1 money, @res2 money
	if @FeeMeth = 1
	begin
		select @res1=IsNull(Sum(-AmountBillingCurrency),0) 
		from LedgerAR 
		where TransType='IN' and (SubType<>'R' or SubType is NULL) and AutoEntry= 'N' and InvoiceSection= 'F'
		  and wbs1= @WBS1 and (@WBS2=' ' or (@BTLevel>=2 and @WBS2=WBS2)) and (@WBS3=' ' or (@BTLevel=3 and @WBS3=WBS3))

		select @res2=IsNull(Sum(-AmountBillingCurrency),0) 
		from LedgerMisc 
		where TransType='IH' and (SubType<>'R' or SubType is NULL) and AutoEntry= 'N' and InvoiceSection= 'F' 
		  and wbs1= @WBS1 and (@WBS2=' ' or (@BTLevel>=2 and @WBS2=WBS2)) and (@WBS3=' ' or (@BTLevel=3 and @WBS3=WBS3))
	end else if @FeeMeth > 1
	begin
		select @res1=IsNull(Sum(-AmountBillingCurrency),0) 
		from LedgerAR 
		where TransType='IN' and (SubType<>'R' or SubType is NULL) and AutoEntry= 'N' and InvoiceSection= 'F'
		  and wbs1= @WBS1 and (@WBS2=' ' or @WBS2=WBS2) and (@WBS3=' ' or @WBS3=WBS3)

		select @res2=IsNull(Sum(-AmountBillingCurrency),0) 
		from LedgerMisc 
		where TransType='IH' and (SubType<>'R' or SubType is NULL) and AutoEntry= 'N' and InvoiceSection= 'F' 
		  and wbs1= @WBS1 and (@WBS2=' ' or @WBS2=WBS2) and (@WBS3=' ' or @WBS3=WBS3)
	end
	set @res1 = IsNull(@res1,0) + IsNull(@res2,0)
	insert into @T values (@res1, '')
	return
END
GO
