SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$ConRate]
  (@strPlanID varchar(32),
   @strAccount Nvarchar(13)
  )
  RETURNS decimal(19,4)
BEGIN

  /*--------------------------------------------------------------------------------------------------------------------
    This function returns a Consultant Billing Multiplier for a given Account in a given Plan.
  */--------------------------------------------------------------------------------------------------------------------

  DECLARE @strCalcConBillAmtFlg varchar(1)

  DECLARE @sintConBillRtMethod smallint
  DECLARE @intConBillRtTableNo int
  DECLARE @decConBillMultiplier decimal(19,4)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get settings from PNPlan.

  SELECT 
    @strCalcConBillAmtFlg = P.CalcConBillAmtFlg,
    @sintConBillRtMethod = P.ConBillRtMethod,
    @intConBillRtTableNo = P.ConBillRtTableNo,
    @decConBillMultiplier = P.ConBillMultiplier
    FROM PNPlan AS P 
    WHERE P.PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN(
    CASE 
      WHEN @strCalcConBillAmtFlg = 'Y' 
      THEN dbo.PN$ExpConMult(@strAccount, @sintConBillRtMethod, @intConBillRtTableNo, @decConBillMultiplier)
      ELSE 1.0000
    END
  )

END -- fn_PN$ConRate
GO
