SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[PN$tabARInvSummary] (
  @strWBS1 nvarchar(30),
  @strJTDDate VARCHAR(8), -- Date must be in format: 'yyyymmdd' regardless of UI Culture.
  @strActivePeriod Varchar(6)
)

  RETURNS @tabARInvSummary TABLE (
    WBS1 nvarchar(30) COLLATE database_default,
    LRTransDate datetime,
    LRInvoiceNumber nvarchar(12) COLLATE database_default,
    LRAmountProjectCurrency decimal(19,4),
    LRAmountBillingCurrency decimal(19,4),
    URAmountProjectCurrency decimal(19,4),
    URAmountBillingCurrency decimal(19,4),
    BilledAmtBillingCurrency decimal(19,4),
    ReceivedAmtBillingCurrency decimal(19,4),
    DueAmtBillingCurrency decimal(19,4),
    RetainageAmtBillingCurrency decimal(19,4),
    RetainerAmtBillingCurrency decimal(19,4),
    CreditMemoAmtBillingCurrency decimal(19,4),
    AvgAge decimal(19,4)
  )

BEGIN

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
 
  DECLARE @strCompany nvarchar(14)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strLRInvoiceNumber nvarchar(12)

  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint

  DECLARE @iInvoiceCount integer
  DECLARE @iDaysOldSum integer

  DECLARE @decAvgAge decimal(19,4)
  DECLARE @decLRAmountProjectCurrency decimal(19,4)
  DECLARE @decLRAmountBillingCurrency decimal(19,4)
  DECLARE @decTotalRetainerProjectCurrency decimal(19,4)
  DECLARE @decTotalRetainertBillingCurrency decimal(19,4)
  DECLARE @decAppliedRetainerProjectCurrency decimal(19,4)
  DECLARE @decAppliedRetainerBillingCurrency decimal(19,4)
  DECLARE @decURAmountProjectCurrency decimal(19,4)
  DECLARE @decURAmountBillingCurrency decimal(19,4)

  DECLARE @decBilledAmtBillingCurrency decimal(19,4)
  DECLARE @decReceivedAmtBillingCurrency decimal(19,4)
  DECLARE @decDueAmtBillingCurrency decimal(19,4)
  DECLARE @decRetainageAmtBillingCurrency decimal(19,4)
  DECLARE @decRetainerAmtBillingCurrency decimal(19,4)
  DECLARE @decCreditMemoAmtBillingCurrency decimal(19,4)

  DECLARE @dtLRTransDate datetime
  
  DECLARE @tabDaysOld TABLE (
    WBS1 nvarchar(30),
    InvoiceNumber nvarchar(12) COLLATE database_default,
    DaysOld integer
    UNIQUE(WBS1, InvoiceNumber)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
          
  -- Get MultiCompanyEnabled flag.
  
  SELECT
    @strMultiCompanyEnabled = MultiCompanyEnabled
    FROM FW_CFGSystem

  -- Get Org Format

  SELECT
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length 
    FROM CFGFormat     

  -- Save Company string for use later.
  
  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          
  -- Set JTD/ETC Dates.
  
  SET @dtJTDDate = CONVERT(datetime, @strJTDDate)
  SET @dtETCDate = DATEADD(d, 1, @dtJTDDate) 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @iInvoiceCount = 0
  SET @iDaysOldSum = 0
  SET @decAvgAge = 0.0000

  -- @tabDaysOld contains Invoices with DaysOld > 0.
  -- This table will be used in the calculation of Average Age.

  INSERT INTO @tabDaysOld (
    WBS1,
    InvoiceNumber,
    DaysOld
  )
  SELECT WBS1,InvoiceNumber, DaysOld FROM ( 
    SELECT
      WBS1,
      InvoiceNumber,
      SUM(InvoiceBalance) AS InvoiceBalance,
      MAX(DaysOld) AS DaysOld
      FROM (
        SELECT DISTINCT
          L.PKey,
          L.WBS1 AS WBS1,
          L.Invoice AS InvoiceNumber,
		  Sum(
			    CASE WHEN L.TransType = 'IN' THEN -AmountBillingCurrency
				WHEN L.TransType = 'CR' AND L.Invoice IS NOT NULL
				THEN
				CASE WHEN L.SubType = 'T' THEN -AmountBillingCurrency ELSE AmountBillingCurrency END
				ELSE 0.0000
				END
			  ) as InvoiceBalance,
		CASE 
			WHEN MIN(
			ISNULL(
				AR.InvoiceDate, AR.RetainageDate
			)
			) IS NULL THEN 0 
			WHEN MAX(AR.PaidPeriod) <= @strActivePeriod  
			AND MAX(L.TransDate) IS NOT NULL THEN DATEDIFF(
			DAY, 
			MIN(AR.InvoiceDate), 
			MAX(L.TransDate)
			) ELSE DATEDIFF(
			DAY, 
			CASE WHEN SUM(
				CASE WHEN L.TransType = 'IN' 
				AND L.SubType = 'R' 
				AND L.TaxCode IS NULL 
				AND L.amountBillingCurrency < 0.0 THEN - L.amountBillingCurrency ELSE 0 END
			) <> 0 
			AND SUM(
				CASE WHEN L.TransType = 'IN' 
				AND L.SubType = 'R' 
				AND L.TaxCode IS NULL 
				AND L.amountBillingCurrency < 0.0 THEN - L.amountBillingCurrency ELSE 0 END
			) >= SUM(
				CASE WHEN L.TaxCode IS NULL THEN CASE WHEN L.TransType = 'IN' THEN -1 * L.amountBillingCurrency WHEN L.Invoice IS NOT NULL 
				AND L.TransType = 'CR' THEN CASE WHEN L.SubType = 'T' THEN - L.amountBillingCurrency ELSE L.amountBillingCurrency END ELSE 0 END ELSE 0.0 END
			) THEN ISNULL(
				MIN(
				CASE WHEN AR.PaidPeriod > @strActivePeriod  THEN AR.RetainageDate ELSE NULL END
				), 
				MIN(AR.InvoiceDate)
			) ELSE MIN(AR.InvoiceDate) END, 
			ISNULL(@strJTDDate , GETDATE())
			) END AS DaysOld
          FROM LedgerAR AS L
            LEFT JOIN AR ON L.WBS1 = AR.WBS1 AND L.WBS2 = AR.WBS2 AND L.WBS3 = AR.WBS3 AND L.Invoice = AR.Invoice
          WHERE L.WBS1 = @strWBS1 AND 
            L.AutoEntry = 'N' AND L.Period <= @strActivePeriod  AND
            (
             (L.TransType= 'IN' AND ISNULL(L.SubType, ' ') <> 'X') OR 
             (L.TransType= 'CR' AND (L.SubType= 'R' OR (L.SubType= 'T' AND ISNULL(L.Invoice, ' ') <> ' ')))
            )
			GROUP BY 
          L.PKey, L.WBS1, L.Invoice 
      ) AS DO      
      GROUP BY WBS1, InvoiceNumber ) AS FinalResult
      WHERE Invoicebalance <> 0 AND  DaysOld > 0
  -- Count number of Invoices and Sum up DaysOld for all Invoices (with non-zero DaysOld).

  SELECT
    @iInvoiceCount = COUNT(InvoiceNumber),
    @iDaysOldSum = SUM(DaysOld)
  FROM @tabDaysOld
  GROUP BY WBS1

  -- Calculate Average Age.

  SELECT @decAvgAge =
    CASE 
      WHEN @iInvoiceCount = 0 
      THEN 0.0000
      ELSE ROUND((@iDaysOldSum / @iInvoiceCount), 0)
    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @strLRInvoiceNumber = ''
  SET @decLRAmountProjectCurrency = 0.0000
  SET @decLRAmountBillingCurrency = 0.0000

  -- Find the Invoice with the last receipt.

    SELECT
      @strLRInvoiceNumber = InvoiceNumber,
      @dtLRTransDate = TransDate,
      @decLRAmountProjectCurrency = AmountProjectCurrency,
      @decLRAmountBillingCurrency = AmountBillingCurrency
      FROM (
        SELECT TOP 1
          InvoiceNumber,
          TransDate,
          AmountProjectCurrency,
          AmountBillingCurrency
          FROM (
            SELECT
              L.Invoice AS InvoiceNumber,
              L.TransDate AS TransDate,
              SUM(-AmountProjectCurrency) AS AmountProjectCurrency,
              SUM(-AmountBillingCurrency) AS AmountBillingCurrency,
              ROW_NUMBER() OVER (ORDER BY L.TransDate DESC, L.Invoice DESC) AS Seq
              FROM LedgerAR AS L
                LEFT JOIN AR ON L.WBS1 = AR.WBS1 AND L.WBS2 = AR.WBS2 AND L.WBS3 = AR.WBS3 AND L.Invoice = AR.Invoice
              WHERE L.WBS1 = @strWBS1 AND 
                L.AutoEntry = 'N' AND L.Period <= @strActivePeriod AND
                L.TransType = 'CR' AND L.SubType = 'R'
              GROUP BY L.WBS1, L.Invoice, L.TransDate
          ) AS XRM 
          WHERE NOT(AmountProjectCurrency = 0 AND AmountBillingCurrency = 0)
          ORDER BY Seq
      ) AS RM

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @decTotalRetainerProjectCurrency = 0.0000
  SET @decTotalRetainertBillingCurrency = 0.0000
  SET @decAppliedRetainerProjectCurrency = 0.0000
  SET @decAppliedRetainerBillingCurrency = 0.0000

  SELECT
    @decTotalRetainerProjectCurrency = ISNULL(SUM(-AmountProjectCurrency), 0),
    @decTotalRetainertBillingCurrency = ISNULL(SUM(-AmountBillingCurrency), 0)
    FROM LedgerAR 
    WHERE TransType = 'CR' AND SubType = 'T' AND Invoice IS NULL AND WBS1 = @strWBS1 AND
      AutoEntry = 'N' AND Period <= @strActivePeriod

  SELECT
    @decAppliedRetainerProjectCurrency = ISNULL(SUM(AmountProjectCurrency), 0),
    @decAppliedRetainerBillingCurrency = ISNULL(SUM(AmountBillingCurrency), 0)
    FROM LedgerAR 
    WHERE TransType = 'CR' AND SubType = 'T' AND Invoice IS NOT NULL AND WBS1 = @strWBS1 AND
      AutoEntry = 'N' AND Period <= @strActivePeriod

  SELECT
    @decURAmountProjectCurrency = @decTotalRetainerProjectCurrency - @decAppliedRetainerProjectCurrency,
    @decURAmountBillingCurrency = @decTotalRetainertBillingCurrency - @decAppliedRetainerBillingCurrency

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @decBilledAmtBillingCurrency = 0.0000
  SET @decReceivedAmtBillingCurrency = 0.0000
  SET @decDueAmtBillingCurrency = 0.0000
  SET @decRetainageAmtBillingCurrency = 0.0000
  SET @decRetainerAmtBillingCurrency = 0.0000
  SET @decCreditMemoAmtBillingCurrency = 0.0000

  SELECT
    @decBilledAmtBillingCurrency = SUM(BilledAmtBillingCurrency),
    @decReceivedAmtBillingCurrency = SUM(ReceivedAmtBillingCurrency),
    @decDueAmtBillingCurrency = SUM(DueAmtBillingCurrency),
    @decRetainageAmtBillingCurrency = SUM(RetainageAmtBillingCurrency),
    @decRetainerAmtBillingCurrency = SUM(RetainerAmtBillingCurrency),
    @decCreditMemoAmtBillingCurrency = SUM(CreditMemoAmtBillingCurrency)
  FROM (

    SELECT DISTINCT
      L.PKey,

      (
        CASE 
          WHEN L.TransType = 'IN' AND L.SubType IS NULL AND CreditMemoRefNo IS NULL THEN -AmountBillingCurrency
          WHEN L.TransType = 'IN' AND L.SubType = 'I' THEN -AmountBillingCurrency
          ELSE 0.0000
        END
      ) AS BilledAmtBillingCurrency,

      (
        CASE
          WHEN (L.TransType = 'CR' AND L.SubType <> 'T') 
          THEN -AmountBillingCurrency 
          ELSE 0.0000
        END
      ) AS ReceivedAmtBillingCurrency,

      (
        CASE
          WHEN L.TransType = 'IN' THEN -AmountBillingCurrency
          WHEN L.TransType = 'CR' AND L.Invoice IS NOT NULL
          THEN
            CASE WHEN L.SubType = 'T' THEN -AmountBillingCurrency ELSE AmountBillingCurrency END
          ELSE 0.0000
        END
      ) AS DueAmtBillingCurrency,

      (
        CASE
          WHEN L.TransType= 'IN' AND L.SubType = 'R'
          THEN AmountBillingCurrency
          ELSE 0.0000
        END
      ) AS RetainageAmtBillingCurrency,

      (
        CASE
          WHEN L.TransType= 'CR' AND SubType = 'T'
          THEN AmountBillingCurrency
          ELSE 0.0000
        END
      ) AS RetainerAmtBillingCurrency,

      (
        CASE 
          WHEN L.TransType = 'IN' AND (ISNULL(L.SubType, ' ') != 'R' AND ISNULL(L.SubType, ' ') != 'I')  AND CreditMemoRefNo IS NOT NULL
          THEN AmountBillingCurrency
          ELSE 0.0000
        END
      ) AS CreditMemoAmtBillingCurrency

      FROM LedgerAR AS L
        LEFT JOIN AR ON L.WBS1 = AR.WBS1 AND L.WBS2 = AR.WBS2 AND L.WBS3 = AR.WBS3 AND L.Invoice = AR.Invoice
        LEFT JOIN CA ON L.Account = CA.Account

      WHERE L.WBS1 = @strWBS1 AND L.Invoice IS NOT NULL AND L.Period <= @strActivePeriod AND
        L.AutoEntry = 'N' AND 
        ((TransType = 'IN' AND L.SubType IS NULL) OR
          (TransType = 'IN' AND L.SubType IN ('I', 'R')) OR
          (TransType = 'CR' AND L.SubType IN ('R', 'T'))
        )

  ) AS X

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT INTO @tabARInvSummary (
    WBS1,
    LRTransDate,
    LRInvoiceNumber,
    LRAmountProjectCurrency,
    LRAmountBillingCurrency,
    URAmountProjectCurrency,
    URAmountBillingCurrency,
    BilledAmtBillingCurrency,
    ReceivedAmtBillingCurrency,
    DueAmtBillingCurrency,
    RetainageAmtBillingCurrency,
    RetainerAmtBillingCurrency,
    CreditMemoAmtBillingCurrency,
    AvgAge
  )
    SELECT
      @strWBS1 AS WBS1,
      @dtLRTransDate AS TransDate,
      ISNULL(@strLRInvoiceNumber, '') AS InvoiceNumber,
      ISNULL(@decLRAmountProjectCurrency, 0.0000) AS AmountBillingCurrency,
      ISNULL(@decLRAmountBillingCurrency, 0.0000) AS AmountProjectCurrency,
      ISNULL(@decURAmountProjectCurrency, 0.0000) AS AmountBillingCurrency,
      ISNULL(@decURAmountBillingCurrency, 0.0000) AS AmountProjectCurrency,
      ISNULL(@decBilledAmtBillingCurrency, 0.0000) AS BilledAmtBillingCurrency,
      ISNULL(@decReceivedAmtBillingCurrency, 0.0000) AS ReceivedAmtBillingCurrency,
      ISNULL(@decDueAmtBillingCurrency, 0.0000) AS DueAmtBillingCurrency,
      ISNULL(@decRetainageAmtBillingCurrency, 0.0000) AS RetainageAmtBillingCurrency,
      ISNULL(@decRetainerAmtBillingCurrency, 0.0000) AS RetainerAmtBillingCurrency,
      ISNULL(@decCreditMemoAmtBillingCurrency, 0.0000) AS CreditMemoAmtBillingCurrency,
      ISNULL(@decAvgAge, 0.0000) AS AvgAge

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
