SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[Least](@v1 SQL_VARIANT, @v2 SQL_VARIANT) 
RETURNS SQL_VARIANT AS
/*
Copyright (c) 2018 Central Consulting Group. All rights reserved.
04/16/2018 David Springer
           Return least of two or three variables
SELECT dbo.least(GETDATE(), GETDATE()-2, GETDATE()+3)
*/
BEGIN
	DECLARE @return SQL_VARIANT

	SELECT @return = Min (Value) 
	FROM (SELECT @v1 value UNION ALL SELECT @v2) a
	RETURN @return
END


GO
