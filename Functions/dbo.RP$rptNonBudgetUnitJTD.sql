SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$rptNonBudgetUnitJTD]
  (@strPlanID varchar(32),
   @dtETCDate datetime,
   @strOutlineLevel int,
   @strPlanAll varchar(1) = 'Y', 
   @strTaskID varchar(32)=NULL,
   @ReportAtBillingInBillingCurr varchar(1) = 'N')
  RETURNS @tabJTDUnit TABLE
   (PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    Account Nvarchar(13) COLLATE database_default,
    Unit Nvarchar(30) COLLATE database_default,
    UnitTable Nvarchar(30) COLLATE database_default,
    TransDate datetime,
    JTDCostCurrencyCode Nvarchar(3) COLLATE database_default,
    JTDBillCurrencyCode Nvarchar(3) COLLATE database_default,    
    PeriodQty decimal(19,4),
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4))
BEGIN

  INSERT @tabJTDUnit
    SELECT 
      PlanID,
      TaskID,  
      Account,  
      Unit,  
      UnitTable,  
      TransDate,  
	  Max(JTDCostCurrencyCode) as JTDCostCurrencyCode,
      Max(JTDBillCurrencyCode) as JTDBillCurrencyCode, 
      SUM(PeriodQty) AS PeriodQty,
      SUM(PeriodCost) AS PeriodCost,  
      SUM(PeriodBill) AS PeriodBill
      FROM
        (SELECT X.PlanID as PlanID, 
                    X.TaskID as TaskID,  
                    Ledger.Account AS Account, 
                    Ledger.Unit AS Unit, 
                    Ledger.UnitTable AS UnitTable, 
                    Ledger.TransDate AS TransDate, 
            	    Max(PR.ProjectCurrencyCode) as JTDCostCurrencyCode,
                    Max(case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end) as JTDBillCurrencyCode,  
                    SUM(Ledger.UnitQuantity) AS PeriodQty, 
                    SUM(Ledger.AmountProjectCurrency) AS PeriodCost, 
                    SUM(Ledger.BillExt) AS PeriodBill 
          FROM LedgerMISC AS Ledger 
          INNER JOIN PR on PR.WBS1=Ledger.WBS1 and PR.WBS2='' and PR.WBS3=''
          INNER JOIN rpTask AS X ON X.PlanID = @strPlanID and X.OutlineLevel=@strOutlineLevel and Ledger.WBS1 = X.WBS1 
          WHERE Ledger.TransType = 'UN' and Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtETCDate
                   AND Ledger.WBS2 LIKE (ISNULL(X.WBS2, '%')) AND Ledger.WBS3 LIKE (ISNULL(X.WBS3, '%')) 
                   AND (@strPlanAll='Y' OR (@strPlanAll='N' AND NOT EXISTS (select 'X' from rpUnit U where U.PlanID = @strPlanID and Ledger.WBS1 = U.WBS1
                   AND Ledger.Unit = U.Unit AND Ledger.UnitTable = U.UnitTable AND Ledger.Account = U.Account
                   AND Ledger.WBS2 LIKE (ISNULL(U.WBS2, '%')) AND Ledger.WBS3 LIKE (ISNULL(U.WBS3, '%')))))                
          GROUP BY X.PlanID, X.TaskID, Ledger.TransDate, Ledger.Account,Ledger.Unit,Ledger.UnitTable, PR.ProjectCurrencyCode,case when @ReportAtBillingInBillingCurr='Y' then PR.BillingCurrencyCode else PR.ProjectCurrencyCode end
    ) AS Z
    GROUP BY Account,Unit,UnitTable,TaskID,PlanID, TransDate, JTDCostCurrencyCode, JTDBillCurrencyCode

  RETURN

END 

GO
