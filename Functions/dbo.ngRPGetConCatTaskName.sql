SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ngRPGetConCatTaskName]
 (@strPlanID varchar(32), 
  @strTaskID varchar(32)) 
  RETURNS Nvarchar(max)  AS
BEGIN

  DECLARE @strConcatName Nvarchar(max)
  DECLARE @strParentOutlineNumber varchar(255)

  -- Get Name of current Task
 -- notice that the leaf node could be labor code, so we retrieve labor code name from labor code setting instead of task name.
 SELECT 
    @strConcatName = CASE WHEN RPTask.LaborCode IS NOT NULL THEN L.LaborCodeName ELSE RPTask.NAME END ,
    @strParentOutlineNumber = ParentOutlineNumber
    FROM RPTask 
	CROSS APPLY dbo.stRP$tabLaborCode(RPTask.PlanID,RPTask.LaborCode) AS L
	WHERE PlanID = @strPlanID AND TaskID = @strTaskID

  -- Loop upward to find parent until getting to the top

  WHILE NOT (@strParentOutlineNumber IS NULL)
    BEGIN
    
      SELECT 
        @strConcatName = Name + ' / ' + @strConcatName,
        @strParentOutlineNumber = ParentOutlineNumber
        FROM RPTask WHERE PlanID = @strPlanID AND OutlineNumber = @strParentOutlineNumber 

    END -- WHILE

  RETURN @strConcatName 

END -- ngRPGetConCatTaskName 
GO
