SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RM$GetLaborCostBillRates]
  (@PlanID varchar(32), @Employee Nvarchar(20), @LaborCode Nvarchar(14), @StartDate DateTime, @EndDate DateTime)
  RETURNS @tabRates TABLE
   (CostRate decimal(19,4),
    BillingRate decimal(19,4))
BEGIN

  /*--------------------------------------------------------------------------------------------------------------------
    This function encapsulates the rules for getting labor billing/cost rates for an employee
    This function will return the following columns:
      CostRate: Cost rate
      BillRate: Bill rate
  */--------------------------------------------------------------------------------------------------------------------

-- Decimal stuff
DECLARE @RtCostDecimals int
DECLARE @RtBillDecimals int
SELECT	@RtCostDecimals = RtCostDecimals,
		@RtBillDecimals = RtBillDecimals
   FROM dbo.RP$tabDecimals(@PlanID)

-- Employee stuff
DECLARE @ProvCostRate AS decimal(19,4)
DECLARE @ProvBillRate AS decimal(19,4)
SELECT @ProvCostRate = ISNULL(ProvCostRate, 0),
       @ProvBillRate = ISNULL(ProvBillRate, 0) 
FROM EM WHERE Employee = @Employee

  INSERT @tabRates
    SELECT 
       dbo.RP$LabRate
          (@Employee,
           0, -- Category 
           NULL, -- RPAssignment.GRLBCD
           @LaborCode,
           P.CostRtMethod,
           P.CostRtTableNo,
           P.GRMethod,
           P.GenResTableNo,
           @StartDate,
           @EndDate,
           @RtCostDecimals,
           @ProvCostRate, 
           0) AS CostRate , -- CostRate override
       dbo.RP$LabRate
          (@Employee,
           0, -- Category 
           NULL, -- RPAssignment.GRLBCD
           @LaborCode,
           P.BillingRtMethod,
           P.BillingRtTableNo,
           P.GRMethod,
           P.GRBillTableNo,
           @StartDate,
           @EndDate,
           @RtBillDecimals,
           @ProvBillRate, 
           0) AS BillingRate  -- CostRate override
   FROM RPPlan AS P
      WHERE P.PlanID = @PlanID 
      
  RETURN

END -- fn_RM$GetLaborCostBillRates
GO
