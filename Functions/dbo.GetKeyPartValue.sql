SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetKeyPartValue]( @pKey Nvarchar(500), @keyPos int)
  RETURNS Nvarchar(100) AS  
BEGIN 
  declare @i int, @startIndex int, @endIndex int

  set @i = 0
  set @startIndex = 1
  set @endIndex = 1

  while (@i < @keyPos)
  BEGIN
    set @endIndex = charindex('|',@pKey,@startIndex)

    if @endIndex = 0 
    begin
      set @endIndex = len(@pKey) +1
	  set @i = @keyPos
	end
	else
		set @i = @i+1

    if (@i < @keyPos)
      set @startIndex = @endIndex +1
  END
  return substring(@pKey, @startIndex,@endIndex -@startIndex )
END
GO
