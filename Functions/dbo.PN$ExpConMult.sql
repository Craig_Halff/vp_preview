SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$ExpConMult]
  (@strAccount Nvarchar(13),
   @sintRtMethod smallint = 0,
   @intRtTabNo int = 0,
   @decMult decimal(19,4) = 1.0
  )
  RETURNS decimal(19,4)
BEGIN

  /*--------------------------------------------------------------------------------------------------------------------
    This function encapsulates the complex rules that determine the Billing Multiplier for Expense & Consultant.
  */--------------------------------------------------------------------------------------------------------------------

  DECLARE @decRetMult decimal(19,4)

  SELECT @decRetMult =
  CASE 
    WHEN Type IN (5, 6) --> Type 5 = "Reimbursable", Type 6 = "Reimbursable Consultant"
    THEN
      CASE @sintRtMethod
        WHEN 0 THEN 1.0
        WHEN 1 THEN @decMult
        WHEN 2 THEN ISNULL((SELECT Multiplier FROM BTEAAccts AS R WHERE R.Account = @strAccount AND R.TableNo = @intRtTabNo), @decMult)
        WHEN 3 THEN ISNULL((SELECT TOP 1 Multiplier FROM BTECAccts AS A INNER JOIN BTECCats AS C ON A.Category = C.Category AND A.TableNo = C.TableNo WHERE A.Account = @strAccount AND A.TableNo = @intRtTabNo ORDER BY A.Category), @decMult) 
      END
    ELSE 1.0000 -- All other Types return 1.0000
  END
  FROM CA WHERE Account = @strAccount
                  
  RETURN(@decRetMult)

END -- fn_PN$ExpConMult
GO
