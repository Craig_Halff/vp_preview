SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DLTK$tabRound](
  @xmlUnRndValue xml,
  @intDecimals int
)
  RETURNS @tabRounded TABLE(
    RowNo bigint,
    RndValue decimal(19,4)
    UNIQUE(RowNo, RndValue)
  )

BEGIN

  SET QUOTED_IDENTIFIER ON

  DECLARE @decDeltalAmt decimal(19,4) = 0
  DECLARE @decSumRndValueBefore decimal(19,4) = 0
  DECLARE @decSumRndValueAfter decimal(19,4) = 0

  DECLARE @tabRunningError TABLE(
    SeqID bigint,
    RowNo bigint,
    UnRndValue decimal(19,4),
    RndValue decimal(19,4),
    RT_RndError decimal(19,4)
    UNIQUE(SeqID, RowNo, UnRndValue, RndValue, RT_RndError)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabRunningError(
    SeqID,
    RowNo,
    UnRndValue,
    RndValue,
    RT_RndError
  ) 
    SELECT 
      SeqID AS SeqID,
      RowNo AS RowNo,
      UnRndValue AS UnRndValue,
      ROUND(UnRndValue, @intDecimals) AS RndValue,
      ROUND((SUM((UnRndValue - ROUND(UnRndValue, @intDecimals)))
        OVER(ORDER BY SeqID ROWS UNBOUNDED PRECEDING)), @intDecimals) AS RT_RndError
      FROM (
        SELECT
          ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS SeqID,
          T.C.value('@RowNo', 'int') AS RowNo,
          T.C.value('@UnRndValue', 'decimal(19,4)') AS UnRndValue
          FROM @xmlUnRndValue.nodes('root/row') T(C)
      ) AS X

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

;

  WITH Rounded AS (
    SELECT 
      SeqID AS SeqID,
      RowNo AS RowNo,
      CONVERT(decimal(19,4), 0.0000) AS RT_RndError,
      CONVERT(decimal(19,4), RndValue) AS RndValue
      FROM @tabRunningError AS RE
      WHERE SeqID = 1
    UNION ALL
    SELECT 
      RE.SeqID AS SeqID,
      RE.RowNo AS RowNo,
      CONVERT(decimal(19,4), RE.RT_RndError) AS RT_RndError,
      CONVERT(decimal(19,4), RE.RndValue + (RE.RT_RndError - REP.RT_RndError)) AS RndValue
    FROM @tabRunningError AS RE
      INNER JOIN @tabRunningError AS REP ON REP.SeqID + 1 = RE.SeqID
    WHERE RE.SeqID > 1
  )
    INSERT @tabRounded(
      RowNo,
      RndValue
    )
		  SELECT 
        RowNo AS RowNo,
        RndValue AS RndValue
        FROM Rounded
        OPTION (MAXRECURSION 0)
        
;

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Need to adjust amount for the last row to compensate for rounding errors.

  SELECT @decSumRndValueBefore = ROUND(SUM(UnRndValue), @intDecimals) FROM @tabRunningError
  SELECT @decSumRndValueAfter = SUM(RndValue) FROM @tabRounded

  SELECT @decDeltalAmt = @decSumRndValueBefore - @decSumRndValueAfter

  UPDATE @tabRounded SET RndValue = (RndValue + @decDeltalAmt)
    FROM @tabRounded AS Z 
    WHERE @decDeltalAmt <> 0 
      AND RowNo IN (SELECT MAX(RowNo) FROM @tabRounded)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN

END -- fn_DLTK$tabRound
GO
