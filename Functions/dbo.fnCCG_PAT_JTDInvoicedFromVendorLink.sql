SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_PAT_JTDInvoicedFromVendorLink] (@PayableSeq int, @WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS varchar(500)
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select dbo.fnCCG_PAT_JTDInvoicedFromVendorLink('1999009.xx', ' ', ' ')
*/
	declare @res varchar(500)
	select @res = '@' + dbo.fnCCG_GetWebServerURLPrefix() + 'PAT_NValue.aspx?PayableSeq=' + Cast(@PayableSeq as varchar) + '&WBS1=' + IsNull(@WBS1,'') + '&WBS2=' + IsNull(@WBS2,'') + '&WBS3=' + IsNull(@WBS3,'') + 
		'&T=JTD%20Invoiced%20from%20Vendor' +
		'&NV=4' +
		'&HD=Vendor' +
		'&H1A=center' +
		'&H1=Invoice%20Date' +
		'&H2A=left' +
		'&H2=Invoice' +
		'&H3=Voucher' +
		'&H4=Amount' +
		'&FN=fnCCG_PAT_JTDInvoicedFromVendor_Details' +
		'&DS=' + @@SERVERNAME + '&DB=' + DB_NAME()
		from CFGSystem
	return @res
END
GO
