SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_PAT_NextRoute] (@PayableSeq int, @WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS varchar(100)
AS BEGIN
/*
	Copyright (c) 2013 Central Consulting Group.  All rights reserved.

	select * from CCG_PAT_Payable
	
	select dbo.fnCCG_PAT_NextRoute (5259,NULL,NULL,NULL) 
	select dbo.fnCCG_PAT_NextRoute (5259, '1993041.00', ' ', ' ') 
*/
    declare @res varchar(100)

	select @res = EM.LastName + ', ' + EM.FirstName
	--select summary.PayableSeq,nextRoute.Seq as nextRouteSeq,RemainingRoutes,
	--nextRoute.Stage,nextRoute.Description,nextRoute.Route,nextRoute.DateLastNotificationSent 
	from 
        (select PayableSeq,MIN(SortOrder) as nextRouteOrder,COUNT(*) as RemainingRoutes from CCG_PAT_Pending where PayableSeq = @PayableSeq group by PayableSeq) summary 
        INNER JOIN CCG_PAT_Pending nextRoute on summary.PayableSeq = nextRoute.PayableSeq and summary.nextRouteOrder = nextRoute.SortOrder
		INNER JOIN EM on EM.Employee = nextRoute.Employee
	return isnull(@res,'')
END
GO
