SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnSplitOrg] 
( 
    @string NVARCHAR(MAX), 
    @delimiter CHAR(1) 
) 
RETURNS @return TABLE(Code1 varchar(30),
Code2 varchar(30),
Code3 varchar(30),
Code4 varchar(30),
Code5 varchar(30),
Code6 varchar(30),
Code7 varchar(30),
Code8 varchar(30),
Code9 varchar(30),
Code10 varchar(30),
Code11 varchar(30),
Code12 varchar(30),
Code13 varchar(30),
Code14 varchar(30),
Code15 varchar(30))
BEGIN 
	DECLARE @output TABLE(orgCode NVARCHAR(MAX), orgLevel int )
    DECLARE @start INT, @end INT, @rowNum Int, @endNoDelim int
	SET @rowNum = 0
    SELECT @start = 1, @end = CHARINDEX(@delimiter, @string) 
    WHILE @start < LEN(@string) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@string) + 1
		else
			Set @end = @end - 1
       	set @rowNum = @rowNum + 1
        INSERT INTO @output (orgCode, orgLevel)  
        VALUES(SUBSTRING(@string, 1, @end),@rowNum) 
        SET @start = @end + 2 
        SET @end = CHARINDEX(@delimiter, @string, @start)
    END 
	insert into @return
	SELECT [1] as Code1,
	 [2] as Code2,
	 [3] as Code3,
	 [4] as Code4,
	 [5] as Code5,
	 [6] as Code6,
	 [7] as Code7,
	 [8] as Code8,
	 [9] as Code9,
	 [10] as Code10,
	 [11] as Code11,
	 [12] as Code12,
	 [13] as Code13,
	 [14] as Code14,
	 [15] as Code15
	FROM
	(SELECT orgCode, Orglevel 
		FROM @output) AS SourceTable
	PIVOT
	(
	min(OrgCode)
	FOR orgLevel IN ([1], [2], [3], [4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15])
	) AS PivotTable;
	return
END
GO
