SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabRevenueNegativeValue]
  (@strPlanID varchar(32),
   @strRevenueSetting varchar(1))

   RETURNS @tabResults TABLE
    (HasNegativeCostFee bit, 
     HasNegativeBillingFee bit,
     HasNegativePlannedRevenue bit
    )
    
BEGIN

	DECLARE @bHasNegativeCostFee bit = 0
	DECLARE @bHasNegativeBillingFee bit = 0
	DECLARE @bHasNegativePlannedRevenue bit = 0

	DECLARE @strOldPlanID varchar(32) 

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	SET @strOldPlanID = @strPlanID
	
	 IF @strRevenueSetting = 'G'
	 BEGIN
		-- check if having negative cost fees
		IF (SELECT COUNT(*) FROM (
			SELECT TimePhaseID FROM RPCompensationFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
				WHERE PeriodCost < 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
			UNION ALL
			SELECT TimePhaseID FROM RPReimbAllowance F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
				WHERE PeriodCost < 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
			UNION ALL
			SELECT TimePhaseID FROM RPConsultantFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
				WHERE PeriodCost < 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
		) AS G ) > 0
        BEGIN
			SET @bHasNegativeCostFee = 1
		END

		IF (SELECT COUNT(*) FROM (
			SELECT TimePhaseID FROM RPCompensationFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
				WHERE PeriodBill < 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
			UNION ALL
			SELECT TimePhaseID FROM RPReimbAllowance F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
				WHERE PeriodBill < 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
			UNION ALL
			SELECT TimePhaseID FROM RPConsultantFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
				WHERE PeriodBill < 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
		) AS G ) > 0
		BEGIN
			SET @bHasNegativeBillingFee = 1
		END
			
		-- check if having negative planned revenue
		IF (SELECT COUNT(*) FROM (
			SELECT F.TimePhaseID
			FROM RPPlannedLabor F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
			WHERE PeriodRev < 0  AND AssignmentID IS NOT NULL AND F.PlanID = @strOldPlanID 
					
			UNION ALL
			SELECT F.TimePhaseID
			FROM RPPlannedExpenses F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
			INNER JOIN RPExpense E ON E.PlanID = F.PlanID AND E.TaskID = F.TaskID AND E.ExpenseID = F.ExpenseID
			INNER JOIN CA ON CA.Account = E.Account
			WHERE PeriodRev < 0 	AND F.ExpenseID IS NOT NULL AND CA.Type = '5' AND F.PlanID = @strOldPlanID		

			UNION ALL
			SELECT F.TimePhaseID
			FROM RPPlannedConsultant F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
					INNER JOIN RPConsultant C ON C.PlanID = F.PlanID AND C.TaskID = F.TaskID AND C.ConsultantID = F.ConsultantID
					INNER JOIN CA ON CA.Account = C.Account
					WHERE PeriodRev < 0 	AND F.ConsultantID IS NOT NULL AND CA.Type = '6' AND F.PlanID = @strOldPlanID								   	

			UNION ALL
			SELECT F.TimePhaseID
					FROM RPPlannedUnit F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
					INNER JOIN RPUnit U ON U.PlanID = F.PlanID AND U.TaskID = F.TaskID AND U.UnitID = F.UnitID
						INNER JOIN CA ON CA.Account = U.Account
						WHERE PeriodRev < 0 	AND F.UnitID IS NOT NULL AND (CA.Type = '5' OR CA.Type = '6') AND F.PlanID = @strOldPlanID		
			) AS G ) > 0 
		BEGIN
			SET @bHasNegativePlannedRevenue = 1
		END			
	 END --IF @strRevenueSetting = 'G'

	 IF @strRevenueSetting = 'I'
	 BEGIN
		-- check if having negative cost fees
		IF (SELECT COUNT(*) FROM (
			SELECT TimePhaseID FROM RPCompensationFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
				WHERE PeriodCost < 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0

			UNION ALL
			SELECT TimePhaseID FROM RPConsultantFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
				WHERE PeriodCost < 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
		) AS G ) > 0
        BEGIN
			SET @bHasNegativeCostFee = 1
		END

		IF (SELECT COUNT(*) FROM (
			SELECT TimePhaseID FROM RPCompensationFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
				WHERE PeriodBill < 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0

			UNION ALL
			SELECT TimePhaseID FROM RPConsultantFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
				WHERE PeriodBill < 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
		) AS G ) > 0
		BEGIN
			SET @bHasNegativeBillingFee = 1
		END
			
		-- check if having negative planned revenue
		IF (SELECT COUNT(*)  
			FROM RPPlannedLabor F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
			WHERE PeriodRev < 0  AND AssignmentID IS NOT NULL AND F.PlanID = @strOldPlanID 					
			)   > 0 
		BEGIN
			SET @bHasNegativePlannedRevenue = 1
		END			
	 END --IF @strRevenueSetting = 'I'

	 IF @strRevenueSetting = 'E'
	 BEGIN
		-- check if having negative cost fees
		IF (SELECT COUNT(*) FROM (
			SELECT TimePhaseID FROM RPCompensationFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
				WHERE PeriodCost < 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
		) AS G ) > 0
        BEGIN
			SET @bHasNegativeCostFee = 1
		END

		IF (SELECT COUNT(*) FROM (
			SELECT TimePhaseID FROM RPCompensationFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
				WHERE PeriodBill < 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
		) AS G ) > 0
		BEGIN
			SET @bHasNegativeBillingFee = 1
		END
			
		-- check if having negative planned revenue
		IF (SELECT COUNT(*)  
			FROM RPPlannedLabor F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
			WHERE PeriodRev < 0  AND AssignmentID IS NOT NULL AND F.PlanID = @strOldPlanID 					
			)   > 0 
		BEGIN
			SET @bHasNegativePlannedRevenue = 1
		END			
	 END --IF @strRevenueSetting = 'E'

	 IF @strRevenueSetting = 'L'
	 BEGIN
		-- check if having negative cost fees
		IF (SELECT COUNT(*) FROM (
			SELECT TimePhaseID FROM RPCompensationFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
				WHERE PeriodDirLabCost < 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
		) AS G ) > 0
        BEGIN
			SET @bHasNegativeCostFee = 1
		END

		IF (SELECT COUNT(*) FROM (
			SELECT TimePhaseID FROM RPCompensationFee F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
				WHERE PeriodDirLabBill < 0 AND F.PlanID = @strOldPlanID AND T.ChildrenCount = 0
		) AS G ) > 0
		BEGIN
			SET @bHasNegativeBillingFee = 1
		END
			
		-- check if having negative planned revenue
		IF (SELECT COUNT(*)  
			FROM RPPlannedLabor F INNER JOIN RPTask T ON T.PlanID = F.PlanID AND T.TaskID = F.TaskID
			WHERE PeriodRev < 0  AND AssignmentID IS NOT NULL AND F.PlanID = @strOldPlanID 					
			)   > 0 
		BEGIN
			SET @bHasNegativePlannedRevenue = 1
		END			
	 END --IF @strRevenueSetting = 'L'


	INSERT @tabResults(
		HasNegativeCostFee,
		HasNegativeBillingFee,
		HasNegativePlannedRevenue
	)
	SELECT
		@bHasNegativeCostFee,
		@bHasNegativeBillingFee,
		@bHasNegativePlannedRevenue
RETURN
END 

GO
