SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$DiffPlanned]
  (@strPlanID varchar(32)
  )
  RETURNS bit
BEGIN -- Function PN$DiffPlanned

  DECLARE @bitDiff bit

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

   SELECT @bitDiff = CASE WHEN SUM(DX) > 0 THEN 1 ELSE 0 END FROM
     (SELECT CASE WHEN EXISTS
        (SELECT MIN(VorN) AS VorN FROM
           (SELECT 
              0 AS VorN,
              PlanName, PlanNumber, OpportunityID, ClientID, ProjMgr, Principal, Supervisor, Org, WBS1, StartDate, EndDate, ExpWBSLevel, ConWBSLevel,
              BudgetType, CostRtMethod, BillingRtMethod, CostRtTableNo, BillingRtTableNo, GRMethod, GenResTableNo, GRBillTableNo,
              CalcExpBillAmtFlg, CalcConBillAmtFlg, ExpBillRtMethod, ExpBillRtTableNo, ConBillRtMethod, ConBillRtTableNo, ExpBillMultiplier, ConBillMultiplier,
              StartingDayOfWeek, Probability, LabMultType, Multiplier, LabBillMultiplier, PctCompleteFormula, RevenueMethod, ReimbMethod, 
              Company, CostCurrencyCode, BillingCurrencyCode, EVFormula, PctComplByPeriodFlg, TargetMultCost, TargetMultBill,
              Status, UtilizationIncludeFlg, AvailableFlg, UnPostedFlg, CommitmentFlg, ContingencyPct, ContingencyAmt, OverheadPct, ProjectedMultiplier, ProjectedRatio,
              PlannedLaborHrs, PlannedLabCost, PlannedLabBill, PlannedExpCost, PlannedExpBill, PlannedConCost, PlannedConBill,
              PlannedDirExpCost, PlannedDirExpBill, PlannedDirConCost, PlannedDirConBill, LabRevenue, ConRevenue, ExpRevenue,
              PctComplete, PctCompleteBill, PctCompleteLabCost, PctCompleteLabBill, PctCompleteExpCost, PctCompleteExpBill, PctCompleteConCost, PctCompleteConBill,
              WeightedLabCost, WeightedLabBill, WeightedExpCost, WeightedExpBill, WeightedConCost, WeightedConBill,
              CompensationFee, ConsultantFee, ReimbAllowance, CompensationFeeBill, ConsultantFeeBill, ReimbAllowanceBill
              FROM PNPlan AS N WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS VorN,
              PlanName, PlanNumber, OpportunityID, ClientID, ProjMgr, Principal, Supervisor, Org, WBS1, StartDate, EndDate, ExpWBSLevel, ConWBSLevel,
              BudgetType, CostRtMethod, BillingRtMethod, CostRtTableNo, BillingRtTableNo, GRMethod, GenResTableNo, GRBillTableNo,
              CalcExpBillAmtFlg, CalcConBillAmtFlg, ExpBillRtMethod, ExpBillRtTableNo, ConBillRtMethod, ConBillRtTableNo, ExpBillMultiplier, ConBillMultiplier,
              StartingDayOfWeek, Probability, LabMultType, Multiplier, LabBillMultiplier, PctCompleteFormula, RevenueMethod, ReimbMethod, 
              Company, CostCurrencyCode, BillingCurrencyCode, EVFormula, PctComplByPeriodFlg, TargetMultCost, TargetMultBill,
              Status, UtilizationIncludeFlg, AvailableFlg, UnPostedFlg, CommitmentFlg, ContingencyPct, ContingencyAmt, OverheadPct, ProjectedMultiplier, ProjectedRatio,
              PlannedLaborHrs, PlannedLabCost, PlannedLabBill, PlannedExpCost, PlannedExpBill, PlannedConCost, PlannedConBill,
              PlannedDirExpCost, PlannedDirExpBill, PlannedDirConCost, PlannedDirConBill, LabRevenue, ConRevenue, ExpRevenue,
              PctComplete, PctCompleteBill, PctCompleteLabCost, PctCompleteLabBill, PctCompleteExpCost, PctCompleteExpBill, PctCompleteConCost, PctCompleteConBill,
              WeightedLabCost, WeightedLabBill, WeightedExpCost, WeightedExpBill, WeightedConCost, WeightedConBill,
              CompensationFee, ConsultantFee, ReimbAllowance, CompensationFeeBill, ConsultantFeeBill, ReimbAllowanceBill
              FROM RPPlan AS V WHERE PlanID = @strPlanID
           ) ZP
           GROUP BY
             PlanName, PlanNumber, OpportunityID, ClientID, ProjMgr, Principal, Supervisor, Org, WBS1, StartDate, EndDate, ExpWBSLevel, ConWBSLevel,
             BudgetType, CostRtMethod, BillingRtMethod, CostRtTableNo, BillingRtTableNo, GRMethod, GenResTableNo, GRBillTableNo,
             CalcExpBillAmtFlg, CalcConBillAmtFlg, ExpBillRtMethod, ExpBillRtTableNo, ConBillRtMethod, ConBillRtTableNo, ExpBillMultiplier, ConBillMultiplier,
             StartingDayOfWeek, Probability, LabMultType, Multiplier, LabBillMultiplier, PctCompleteFormula, RevenueMethod, ReimbMethod, 
             Company, CostCurrencyCode, BillingCurrencyCode, EVFormula, PctComplByPeriodFlg, TargetMultCost, TargetMultBill,
             Status, UtilizationIncludeFlg, AvailableFlg, UnPostedFlg, CommitmentFlg, ContingencyPct, ContingencyAmt, OverheadPct, ProjectedMultiplier, ProjectedRatio,
             PlannedLaborHrs, PlannedLabCost, PlannedLabBill, PlannedExpCost, PlannedExpBill, PlannedConCost, PlannedConBill,
             PlannedDirExpCost, PlannedDirExpBill, PlannedDirConCost, PlannedDirConBill, LabRevenue, ConRevenue, ExpRevenue,
             PctComplete, PctCompleteBill, PctCompleteLabCost, PctCompleteLabBill, PctCompleteExpCost, PctCompleteExpBill, PctCompleteConCost, PctCompleteConBill,
             WeightedLabCost, WeightedLabBill, WeightedExpCost, WeightedExpBill, WeightedConCost, WeightedConBill,
             CompensationFee, ConsultantFee, ReimbAllowance, CompensationFeeBill, ConsultantFeeBill, ReimbAllowanceBill
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Task */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(VorN) AS VorN FROM
           (SELECT 
              0 AS VorN,
              Name, WBS1, WBS2, WBS3, WBSType, ParentOutlineNumber, OutlineNumber, ChildrenCount, OutlineLevel, StartDate, EndDate, CostRate, BillingRate, ExpBillRate, ConBillRate,
              PlannedLaborHrs, PlannedLabCost, PlannedLabBill, PlannedExpCost, PlannedExpBill, PlannedConCost, PlannedConBill,
              PlannedDirExpCost, PlannedDirExpBill, PlannedDirConCost, PlannedDirConBill, LabRevenue, ConRevenue, ExpRevenue,
              PctComplete, PctCompleteLabCost, PctCompleteLabBill, PctCompleteExpCost, PctCompleteExpBill, PctCompleteConCost, PctCompleteConBill,
              WeightedLabCost, WeightedLabBill, WeightedExpCost, WeightedExpBill, WeightedConCost, WeightedConBill,
              LabParentState, ExpParentState, ConParentState, UntParentState, LabVState, ExpVState, ConVState, UntVState,
              CompensationFee, ConsultantFee, ReimbAllowance, CompensationFeeBill, ConsultantFeeBill, ReimbAllowanceBill
              FROM PNTask AS N WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS VorN,
              Name, WBS1, WBS2, WBS3, WBSType, ParentOutlineNumber, OutlineNumber, ChildrenCount, OutlineLevel, StartDate, EndDate, CostRate, BillingRate, ExpBillRate, ConBillRate,
              PlannedLaborHrs, PlannedLabCost, PlannedLabBill, PlannedExpCost, PlannedExpBill, PlannedConCost, PlannedConBill,
              PlannedDirExpCost, PlannedDirExpBill, PlannedDirConCost, PlannedDirConBill, LabRevenue, ConRevenue, ExpRevenue,
              PctComplete, PctCompleteLabCost, PctCompleteLabBill, PctCompleteExpCost, PctCompleteExpBill, PctCompleteConCost, PctCompleteConBill,
              WeightedLabCost, WeightedLabBill, WeightedExpCost, WeightedExpBill, WeightedConCost, WeightedConBill,
              LabParentState, ExpParentState, ConParentState, UntParentState, LabVState, ExpVState, ConVState, UntVState,
              CompensationFee, ConsultantFee, ReimbAllowance, CompensationFeeBill, ConsultantFeeBill, ReimbAllowanceBill
              FROM RPTask AS V WHERE PlanID = @strPlanID
           ) ZT
           GROUP BY
             Name, WBS1, WBS2, WBS3, WBSType, ParentOutlineNumber, OutlineNumber, ChildrenCount, OutlineLevel, StartDate, EndDate, CostRate, BillingRate, ExpBillRate, ConBillRate,
             PlannedLaborHrs, PlannedLabCost, PlannedLabBill, PlannedExpCost, PlannedExpBill, PlannedConCost, PlannedConBill,
             PlannedDirExpCost, PlannedDirExpBill, PlannedDirConCost, PlannedDirConBill, LabRevenue, ConRevenue, ExpRevenue,
             PctComplete, PctCompleteLabCost, PctCompleteLabBill, PctCompleteExpCost, PctCompleteExpBill, PctCompleteConCost, PctCompleteConBill,
             WeightedLabCost, WeightedLabBill, WeightedExpCost, WeightedExpBill, WeightedConCost, WeightedConBill,
             LabParentState, ExpParentState, ConParentState, UntParentState, LabVState, ExpVState, ConVState, UntVState,
             CompensationFee, ConsultantFee, ReimbAllowance, CompensationFeeBill, ConsultantFeeBill, ReimbAllowanceBill
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Assignment */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(VorN) AS VorN FROM
           (SELECT 
              0 AS VorN,
              TaskID, WBS1, WBS2, WBS3, ResourceID, GenericResourceID, Category, GRLBCD, ActivityID, StartDate, EndDate,
              CostRate, BillingRate, PlannedLaborHrs, PlannedLabCost, PlannedLabBill, LabRevenue, 
              PctCompleteLabCost, PctCompleteLabBill, WeightedLabCost, WeightedLabBill, SortSeq,
              LabParentState, ExpParentState, ConParentState, UntParentState, LabVState, ExpVState, ConVState, UntVState
              FROM PNAssignment AS N WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS VorN,
              TaskID, WBS1, WBS2, WBS3, ResourceID, GenericResourceID, Category, GRLBCD, ActivityID, StartDate, EndDate,
              CostRate, BillingRate, PlannedLaborHrs, PlannedLabCost, PlannedLabBill, LabRevenue, 
              PctCompleteLabCost, PctCompleteLabBill, WeightedLabCost, WeightedLabBill, SortSeq,
              LabParentState, ExpParentState, ConParentState, UntParentState, LabVState, ExpVState, ConVState, UntVState
              FROM RPAssignment AS V WHERE PlanID = @strPlanID
           ) ZA
           GROUP BY
             TaskID, WBS1, WBS2, WBS3, ResourceID, GenericResourceID, Category, GRLBCD, ActivityID, StartDate, EndDate,
             CostRate, BillingRate, PlannedLaborHrs, PlannedLabCost, PlannedLabBill, LabRevenue, 
             PctCompleteLabCost, PctCompleteLabBill, WeightedLabCost, WeightedLabBill, SortSeq,
             LabParentState, ExpParentState, ConParentState, UntParentState, LabVState, ExpVState, ConVState, UntVState
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Expense */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(VorN) AS VorN FROM
           (SELECT 
              0 AS VorN,
              TaskID, WBS1, WBS2, WBS3, Account, Vendor, StartDate, EndDate, DirectAcctFlg, ExpBillRate,
              PlannedExpCost, PlannedExpBill, PlannedDirExpCost, PlannedDirExpBill, ExpRevenue, 
              PctCompleteExpCost, PctCompleteExpBill, WeightedExpCost, WeightedExpBill, SortSeq,
              LabParentState, ExpParentState, ConParentState, UntParentState, LabVState, ExpVState, ConVState, UntVState
              FROM PNExpense AS N WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS VorN,
              TaskID, WBS1, WBS2, WBS3, Account, Vendor, StartDate, EndDate, DirectAcctFlg, ExpBillRate,
              PlannedExpCost, PlannedExpBill, PlannedDirExpCost, PlannedDirExpBill, ExpRevenue, 
              PctCompleteExpCost, PctCompleteExpBill, WeightedExpCost, WeightedExpBill, SortSeq,
              LabParentState, ExpParentState, ConParentState, UntParentState, LabVState, ExpVState, ConVState, UntVState
              FROM RPExpense AS V WHERE PlanID = @strPlanID
           ) ZC
           GROUP BY
             TaskID, WBS1, WBS2, WBS3, Account, Vendor, StartDate, EndDate, DirectAcctFlg, ExpBillRate,
             PlannedExpCost, PlannedExpBill, PlannedDirExpCost, PlannedDirExpBill, ExpRevenue, 
             PctCompleteExpCost, PctCompleteExpBill, WeightedExpCost, WeightedExpBill, SortSeq,
             LabParentState, ExpParentState, ConParentState, UntParentState, LabVState, ExpVState, ConVState, UntVState
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Consultant */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(VorN) AS VorN FROM
           (SELECT 
              0 AS VorN,
              TaskID, WBS1, WBS2, WBS3, Account, Vendor, StartDate, EndDate, DirectAcctFlg, ConBillRate,
              PlannedConCost, PlannedConBill, PlannedDirConCost, PlannedDirConBill, ConRevenue, 
              PctCompleteConCost, PctCompleteConBill, WeightedConCost, WeightedConBill, SortSeq,
              LabParentState, ExpParentState, ConParentState, UntParentState, LabVState, ExpVState, ConVState, UntVState
              FROM PNConsultant AS N WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS VorN,
              TaskID, WBS1, WBS2, WBS3, Account, Vendor, StartDate, EndDate, DirectAcctFlg, ConBillRate,
              PlannedConCost, PlannedConBill, PlannedDirConCost, PlannedDirConBill, ConRevenue, 
              PctCompleteConCost, PctCompleteConBill, WeightedConCost, WeightedConBill, SortSeq,
              LabParentState, ExpParentState, ConParentState, UntParentState, LabVState, ExpVState, ConVState, UntVState
              FROM RPConsultant AS V WHERE PlanID = @strPlanID
           ) ZC
           GROUP BY
             TaskID, WBS1, WBS2, WBS3, Account, Vendor, StartDate, EndDate, DirectAcctFlg, ConBillRate,
             PlannedConCost, PlannedConBill, PlannedDirConCost, PlannedDirConBill, ConRevenue, 
             PctCompleteConCost, PctCompleteConBill, WeightedConCost, WeightedConBill, SortSeq,
             LabParentState, ExpParentState, ConParentState, UntParentState, LabVState, ExpVState, ConVState, UntVState
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Labor TPD */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(VorN) AS VorN FROM
           (SELECT 
              0 AS VorN,
              TaskID, AssignmentID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodHrs, PeriodCost, PeriodBill, PeriodRev, CostRate, BillingRate, HardBooked
              FROM PNPlannedLabor AS N WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS VorN,
              TaskID, AssignmentID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodHrs, PeriodCost, PeriodBill, PeriodRev, CostRate, BillingRate, HardBooked
              FROM RPPlannedLabor AS V WHERE PlanID = @strPlanID
           ) ZTPD
           GROUP BY
             TaskID, AssignmentID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodHrs, PeriodCost, PeriodBill, PeriodRev, CostRate, BillingRate, HardBooked
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Expense TPD */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(VorN) AS VorN FROM
           (SELECT 
              0 AS VorN,
              TaskID, ExpenseID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
              FROM PNPlannedExpenses AS N WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS VorN,
              TaskID, ExpenseID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
              FROM RPPlannedExpenses AS V WHERE PlanID = @strPlanID
           ) ZTPD
           GROUP BY
             TaskID, ExpenseID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Consultant TPD */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(VorN) AS VorN FROM
           (SELECT 
              0 AS VorN,
              TaskID, ConsultantID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
              FROM PNPlannedConsultant AS N WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS VorN,
              TaskID, ConsultantID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
              FROM RPPlannedConsultant AS V WHERE PlanID = @strPlanID
           ) ZTPD
           GROUP BY
             TaskID, ConsultantID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* AccordionFormat */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(VorN) AS VorN FROM
           (SELECT 
              0 AS VorN,
              StartDate, EndDate, MajorScale, MinorScale
              FROM PNAccordionFormat AS N WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS VorN,
              StartDate, EndDate, MajorScale, MinorScale
              FROM RPAccordionFormat AS V WHERE PlanID = @strPlanID
           ) ZAF
           GROUP BY
             StartDate, EndDate, MajorScale, MinorScale
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* CalendarInterval */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(VorN) AS VorN FROM
           (SELECT 
              0 AS VorN,
              AccordionFormatID, StartDate, EndDate, PeriodScale, PeriodCount, NumWorkingDays
              FROM PNCalendarInterval AS N WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS VorN,
              AccordionFormatID, StartDate, EndDate, PeriodScale, PeriodCount, NumWorkingDays
              FROM RPCalendarInterval AS V WHERE PlanID = @strPlanID
           ) ZAF
           GROUP BY
             AccordionFormatID, StartDate, EndDate, PeriodScale, PeriodCount, NumWorkingDays
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* WBSLevelFormat */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(VorN) AS VorN FROM
           (SELECT 
              0 AS VorN,
              FmtLevel, WBSType, WBSMatch
              FROM PNWBSLevelFormat AS N WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS VorN,
              FmtLevel, WBSType, WBSMatch
              FROM RPWBSLevelFormat AS V WHERE PlanID = @strPlanID
           ) ZAF
           GROUP BY
             FmtLevel, WBSType, WBSMatch
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
     ) AS Z

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN @bitDiff

END -- PN$DiffPlanned
GO
