SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RP$tabJTDExpCon]
  (@strPlanID varchar(32),
   @dtETCDate datetime,
   @strCommitmentFlg varchar(1) = 'N', 
   @strMatchWBS1Wildcard varchar(1) = 'N',
   @strExcludeUNFlg varchar(1) = 'N',
   @strType varchar(1) = 'E')
  RETURNS @tabJTDExpCon TABLE
   (RowID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    TransDate datetime,
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4),
    PostedFlg smallint)
BEGIN

  /*--------------------------------------------------------------------------------------------------------------------
    This function encapsulates the complex SQLs that are used to retrieve JTD for Expense or Consultant rows.
    This function will return the following columns:
      RowID which is either ExpenseID or ConsultantID
      TaskID
      TransDate
      SUM(AmountProjectCurrency) AS PeriodCost
      SUM(BillExt) AS PeriodBill
      PostedFlg: 1 for Posted, -1 for UnPosted
  */--------------------------------------------------------------------------------------------------------------------

  DECLARE @tabExpCon TABLE
    (RowID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(7) COLLATE database_default,
     WBS3 Nvarchar(7) COLLATE database_default,
     Account Nvarchar(13) COLLATE database_default,
     Vendor Nvarchar(30) COLLATE database_default)
     
  IF (@strType = 'E')
    INSERT @tabExpCon
      SELECT ExpenseID AS RowID, TaskID, WBS1, WBS2, WBS3, Account, Vendor
        FROM RPExpense WHERE PlanID = @strPlanID
  ELSE
    INSERT @tabExpCon
      SELECT ConsultantID AS RowID, TaskID, WBS1, WBS2, WBS3, Account, Vendor
        FROM RPConsultant WHERE PlanID = @strPlanID
  
  -- Insert JTD into returned table
  
  INSERT @tabJTDExpCon
    SELECT 
      RowID,  
      TaskID,  
      TransDate,  
      SUM(PeriodCost) AS PeriodCost,  
      SUM(PeriodBill) AS PeriodBill,  
      1 AS PostedFlg
      FROM
        (SELECT 
           EC.RowID AS RowID,  
           EC.TaskID AS TaskID,  
           TransDate AS TransDate,  
           SUM(AmountProjectCurrency) AS PeriodCost,  
           SUM(BillExt) AS PeriodBill  
           FROM LedgerAR AS Ledger
             INNER JOIN @tabExpCon AS EC
               ON (Ledger.WBS1 = EC.WBS1
                   AND Ledger.Account = EC.Account AND Ledger.ProjectCost = 'Y')  
             INNER JOIN (SELECT MIN(RowID) AS RowID FROM @tabExpCon  
               GROUP BY TaskID, Account, Vendor) AS X1 ON EC.RowID = X1.RowID  
           WHERE TransDate < @dtETCDate
             AND Ledger.TransType != (CASE WHEN @strExcludeUNFlg = 'Y' THEN 'UN' ELSE '*' END)
             AND Ledger.WBS2 LIKE (ISNULL(EC.WBS2, '%'))  
             AND Ledger.WBS3 LIKE (ISNULL(EC.WBS3, '%'))  
             AND (ISNULL(Ledger.Vendor, '%')) LIKE (ISNULL(EC.Vendor, '%'))  
           GROUP BY EC.TaskID, TransDate, EC.RowID
         UNION ALL
         SELECT 
           EC.RowID AS RowID,  
           EC.TaskID AS TaskID,  
           TransDate AS TransDate,  
           SUM(AmountProjectCurrency) AS PeriodCost,  
           SUM(BillExt) AS PeriodBill  
           FROM LedgerAP AS Ledger
             INNER JOIN @tabExpCon AS EC
               ON (Ledger.WBS1 = EC.WBS1
                   AND Ledger.Account = EC.Account AND Ledger.ProjectCost = 'Y')  
             INNER JOIN (SELECT MIN(RowID) AS RowID FROM @tabExpCon  
               GROUP BY TaskID, Account, Vendor) AS X1 ON EC.RowID = X1.RowID  
           WHERE TransDate < @dtETCDate
             AND Ledger.TransType != (CASE WHEN @strExcludeUNFlg = 'Y' THEN 'UN' ELSE '*' END)
             AND Ledger.WBS2 LIKE (ISNULL(EC.WBS2, '%'))  
             AND Ledger.WBS3 LIKE (ISNULL(EC.WBS3, '%'))  
             AND (ISNULL(Ledger.Vendor, '%')) LIKE (ISNULL(EC.Vendor, '%'))  
           GROUP BY EC.TaskID, TransDate, EC.RowID
         UNION ALL
         SELECT 
           EC.RowID AS RowID,  
           EC.TaskID AS TaskID,  
           TransDate AS TransDate,  
           SUM(AmountProjectCurrency) AS PeriodCost,  
           SUM(BillExt) AS PeriodBill  
           FROM LedgerEX AS Ledger
             INNER JOIN @tabExpCon AS EC
               ON (Ledger.WBS1 = EC.WBS1
                   AND Ledger.Account = EC.Account AND Ledger.ProjectCost = 'Y')  
             INNER JOIN (SELECT MIN(RowID) AS RowID FROM @tabExpCon  
               GROUP BY TaskID, Account, Vendor) AS X1 ON EC.RowID = X1.RowID  
           WHERE TransDate < @dtETCDate
             AND Ledger.TransType != (CASE WHEN @strExcludeUNFlg = 'Y' THEN 'UN' ELSE '*' END)
             AND Ledger.WBS2 LIKE (ISNULL(EC.WBS2, '%'))  
             AND Ledger.WBS3 LIKE (ISNULL(EC.WBS3, '%'))  
             AND (ISNULL(Ledger.Vendor, '%')) LIKE (ISNULL(EC.Vendor, '%'))  
           GROUP BY EC.TaskID, TransDate, EC.RowID
         UNION ALL
         SELECT 
           EC.RowID AS RowID,  
           EC.TaskID AS TaskID,  
           TransDate AS TransDate,  
           SUM(AmountProjectCurrency) AS PeriodCost,  
           SUM(BillExt) AS PeriodBill  
           FROM LedgerMISC AS Ledger
             INNER JOIN @tabExpCon AS EC
               ON (Ledger.WBS1 = EC.WBS1
                   AND Ledger.Account = EC.Account AND Ledger.ProjectCost = 'Y')  
             INNER JOIN (SELECT MIN(RowID) AS RowID FROM @tabExpCon  
               GROUP BY TaskID, Account, Vendor) AS X1 ON EC.RowID = X1.RowID  
           WHERE TransDate < @dtETCDate
             AND Ledger.TransType != (CASE WHEN @strExcludeUNFlg = 'Y' THEN 'UN' ELSE '*' END)
             AND Ledger.WBS2 LIKE (ISNULL(EC.WBS2, '%'))  
             AND Ledger.WBS3 LIKE (ISNULL(EC.WBS3, '%'))  
             AND (ISNULL(Ledger.Vendor, '%')) LIKE (ISNULL(EC.Vendor, '%'))  
           GROUP BY EC.TaskID, TransDate, EC.RowID) AS Z
    GROUP BY RowID, TaskID, TransDate
    
  --> PO Commitments.
  
  IF (@strCommitmentFlg = 'Y')
    BEGIN

      -- POC with no Change Orders.

      INSERT @tabJTDExpCon
        SELECT 
          EC.RowID AS RowID,  
          EC.TaskID AS TaskID,  
          POM.OrderDate AS TransDate,  
          SUM(AmountProjectCurrency) AS PeriodCost,  
          SUM(BillExt) AS PeriodBill,  
          -1 AS PostedFlg
          FROM POCommitment AS POC 
            INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
            INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey
            INNER JOIN @tabExpCon AS EC
              ON (POC.WBS1 = EC.WBS1
                  AND POC.Account = EC.Account)  
            INNER JOIN (SELECT MIN(RowID) AS RowID FROM @tabExpCon  
              GROUP BY TaskID, Account, Vendor) AS X1 ON EC.RowID = X1.RowID  
          WHERE POM.OrderDate < @dtETCDate
            AND AmountProjectCurrency != 0 AND BillExt != 0
            AND POC.WBS2 LIKE (ISNULL(EC.WBS2, '%'))  
            AND POC.WBS3 LIKE (ISNULL(EC.WBS3, '%'))  
            AND POM.Vendor LIKE (ISNULL(EC.Vendor, '%'))  
          GROUP BY EC.TaskID, POM.OrderDate, EC.RowID
            
      -- POC with Change Orders.

      INSERT @tabJTDExpCon
        SELECT 
          EC.RowID AS RowID,  
          EC.TaskID AS TaskID,  
          POCOM.OrderDate AS TransDate,  
          SUM(AmountProjectCurrency) AS PeriodCost,  
          SUM(BillExt) AS PeriodBill,  
          -1 AS PostedFlg
          FROM POCommitment AS POC 
            INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
            INNER JOIN POCOMaster AS POCOM ON POD.COPKey = POCOM.PKey
            INNER JOIN POMaster AS POM ON POCOM.MasterPKey = POM.MasterPKey
            INNER JOIN @tabExpCon AS EC
              ON (POC.WBS1 = EC.WBS1
                  AND POC.Account = EC.Account)  
            INNER JOIN (SELECT MIN(RowID) AS RowID FROM @tabExpCon  
              GROUP BY TaskID, Account, Vendor) AS X1 ON EC.RowID = X1.RowID  
          WHERE POCOM.OrderDate < @dtETCDate
            AND AmountProjectCurrency != 0 AND BillExt != 0
            AND POC.WBS2 LIKE (ISNULL(EC.WBS2, '%'))  
            AND POC.WBS3 LIKE (ISNULL(EC.WBS3, '%'))  
            AND POM.Vendor LIKE (ISNULL(EC.Vendor, '%'))  
          GROUP BY EC.TaskID, POCOM.OrderDate, EC.RowID

    END -- If-Then
       
  RETURN

END -- fn_RP$tabJTDExpCon
GO
