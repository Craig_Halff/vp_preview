SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[DW$tabFactLabor]()
  RETURNS @tabFactLabor TABLE
    (Period int,
     PostSeq int,
     PKey varchar(32) COLLATE database_default,
     WBS1 nvarchar(30) COLLATE database_default,
     WBS2 nvarchar(30) COLLATE database_default, 
     WBS3 nvarchar(30) COLLATE database_default,
     Employee nvarchar(20) COLLATE database_default,
     TransType nvarchar(2) COLLATE database_default,
     Category smallint,
     LaborCode nvarchar(14) COLLATE database_default,
     BillStatus varchar(1) COLLATE database_default,
     SK_TransactionDateID int,
     RegularHours decimal(19, 4),
     OvertimeHours decimal(19, 4),
     RegularAmount decimal(19, 4),
     OvertimeAmount decimal(19, 4),
     LaborBilling decimal(19, 4),
     SpecialOvertimeHours decimal(19, 4),
     SpecialOvertimeAmount decimal(19, 4),
     RegularAmountProjectCurrency decimal(19, 4),
     OvertimeAmountProjectCurrency decimal(19, 4),
     SpecialOvertimeAmountProjectCurrency decimal(19, 4),
     RegularAmountBillingCurrency decimal(19, 4),
     OvertimeAmountBillingCurrency decimal(19, 4),
     SpecialOvertimeAmountBillingCurrency decimal(19, 4),
     RegularAmountEmployeeCurrency decimal(19, 4),
     OvertimeAmountEmployeeCurrency decimal(19, 4),
     SpecialOvertimeAmountEmployeeCurrency decimal(19, 4),
     DirectHours decimal(19, 4),
     [DirectOvertimeHours] decimal(19, 4),
     [DirectSpecialOvertimeHours] decimal(19, 4),
     DirectCost decimal(19, 4),
     [DirectOvertimeAmount] decimal(19, 4),
     [DirectSpecialOvertimeAmount] decimal(19, 4),
     IndirectHours decimal(19, 4),
     [IndirectOvertimeHours] decimal(19, 4),
     [IndirectSpecialOvertimeHours] decimal(19, 4),
     IndirectCost decimal(19, 4),
     [IndirectOvertimeAmount] decimal(19, 4),
     [IndirectSpecialOvertimeAmount] decimal(19, 4),
     BenefitHours decimal(19, 4),
     BenefitCost decimal(19, 4),
     NonBillableHours decimal(19, 4),
     [NonBillableOvertimeHours] decimal(19, 4),
     [NonBillableSpecialOvertimeHours] decimal(19, 4),
     NonBillableCost decimal(19, 4),
     [NonBillableOvertimeAmount] decimal(19, 4),
     [NonBillableSpecialOvertimeAmount] decimal(19, 4),

     RealizationAmountEmployeeCurrency decimal(19, 4),
     RealizationAmountProjectCurrency decimal(19, 4),
     RealizationAmountBillingCurrency decimal(19, 4),

--JYC Time Analysis - Multicurrency
     DirectCostProjectCurrency decimal(19, 4),
     DirectCostBillingCurrency decimal(19, 4),
     [DirectOvertimeAmountProjectCurrency] decimal(19, 4),
     [DirectSpecialOvertimeAmountProjectCurrency] decimal(19, 4),
     [DirectOvertimeAmountBillingCurrency] decimal(19, 4),
     [DirectSpecialOvertimeAmountBillingCurrency] decimal(19, 4),
     IndirectCostProjectCurrency decimal(19, 4),
     IndirectCostBillingCurrency decimal(19, 4),
     [IndirectOvertimeAmountProjectCurrency] decimal(19, 4),
     [IndirectOvertimeAmountBillingCurrency] decimal(19, 4),
     [IndirectSpecialOvertimeAmountProjectCurrency] decimal(19, 4),
     [IndirectSpecialOvertimeAmountBillingCurrency] decimal(19, 4),
     BenefitCostProjectCurrency decimal(19, 4),
     BenefitCostBillingCurrency decimal(19, 4),
     NonBillableCostProjectCurrency decimal(19, 4),
     NonBillableCostBillingCurrency decimal(19, 4),
     [NonBillableOvertimeAmountProjectCurrency] decimal(19, 4),
     [NonBillableOvertimeAmountBillingCurrency] decimal(19, 4),
     [NonBillableSpecialOvertimeAmountProjectCurrency] decimal(19, 4),
     [NonBillableSpecialOvertimeAmountBillingCurrency] decimal(19, 4),
-- support Multicurrency/Presentation currency     
     [EMCurrencyCode] nvarchar(3) COLLATE database_default
    )
BEGIN -- Function DW$tabFactLabor

  DECLARE @MulticompanyEnabled AS varchar(1)
  DECLARE @org1Start AS smallInt
  DECLARE @org1Length AS smallInt
  
  SELECT @MulticompanyEnabled = MulticompanyEnabled FROM FW_CFGSystem
  SELECT @org1Start = org1Start, @org1Length = org1Length FROM CFGFormat

  DECLARE @tabTAD AS TABLE
    (BenefitWBS1 Nvarchar(30) COLLATE database_default, 
     Company Nvarchar(14) COLLATE database_default
     PRIMARY KEY (BenefitWBS1, Company))
     
  INSERT INTO @tabTAD(BenefitWBS1, Company)
    SELECT DISTINCT PR.WBS1, TA.Company
    FROM PR 
	  INNER JOIN CFGTimeAnalysis TA ON PR.WBS1 BETWEEN TA.StartWBS1 AND TA.EndWBS1
      LEFT JOIN CFGTimeAnalysisHeadingsData ON TA.ReportColumn = CFGTimeAnalysisHeadingsData.ReportColumn
    WHERE TA.ReportColumn > 0 AND CFGTimeAnalysisHeadingsData.Benefit = 'Y'
	ORDER BY TA.Company, PR.WBS1

  INSERT INTO @tabFactLabor 
    (Period,
     PostSeq,
     PKey,
     WBS1,
     WBS2,
     WBS3,
     Employee,
     TransType,
     Category,
     LaborCode,
     BillStatus,
     SK_TransactionDateID,
     RegularHours,
     OvertimeHours,
     RegularAmount,
     OvertimeAmount,
     LaborBilling,
     SpecialOvertimeHours,
     SpecialOvertimeAmount,
     RegularAmountProjectCurrency,
     OvertimeAmountProjectCurrency,
     SpecialOvertimeAmountProjectCurrency,
     RegularAmountBillingCurrency,
     OvertimeAmountBillingCurrency,
     SpecialOvertimeAmountBillingCurrency,
     RegularAmountEmployeeCurrency,
     OvertimeAmountEmployeeCurrency,
     SpecialOvertimeAmountEmployeeCurrency,
     DirectHours,
     [DirectOvertimeHours],
     [DirectSpecialOvertimeHours],
     DirectCost,
     [DirectOvertimeAmount],
     [DirectSpecialOvertimeAmount],
     IndirectHours,
     [IndirectOvertimeHours],
     [IndirectSpecialOvertimeHours],
     IndirectCost,
     [IndirectOvertimeAmount],
     [IndirectSpecialOvertimeAmount],
     BenefitHours,
     BenefitCost,
     NonBillableHours,
     [NonBillableOvertimeHours],
     [NonBillableSpecialOvertimeHours],
     NonBillableCost,
     [NonBillableOvertimeAmount],
     [NonBillableSpecialOvertimeAmount],

     RealizationAmountEmployeeCurrency,
     RealizationAmountProjectCurrency,
     RealizationAmountBillingCurrency,

--JYC Time Analysis - Multicurrency
     DirectCostProjectCurrency,
     DirectCostBillingCurrency,
     [DirectOvertimeAmountProjectCurrency],
     [DirectSpecialOvertimeAmountProjectCurrency],
     [DirectOvertimeAmountBillingCurrency],
     [DirectSpecialOvertimeAmountBillingCurrency],
     IndirectCostProjectCurrency,
     IndirectCostBillingCurrency,
     [IndirectOvertimeAmountProjectCurrency],
     [IndirectOvertimeAmountBillingCurrency],
     [IndirectSpecialOvertimeAmountProjectCurrency],
     [IndirectSpecialOvertimeAmountBillingCurrency],
     BenefitCostProjectCurrency,
     BenefitCostBillingCurrency,
     NonBillableCostProjectCurrency,
     NonBillableCostBillingCurrency,
     [NonBillableOvertimeAmountProjectCurrency],
     [NonBillableOvertimeAmountBillingCurrency],
     [NonBillableSpecialOvertimeAmountProjectCurrency],
     [NonBillableSpecialOvertimeAmountBillingCurrency], 
     [EMCurrencyCode]
    )
    SELECT 
      LD.Period AS Period,
      LD.PostSeq AS PostSeq,
      LD.Pkey AS Pkey,
      CONVERT(nvarchar(30), WBS1) AS WBS1, 
      CONVERT(nvarchar(30), CASE WHEN WBS2='' THEN ' ' ELSE WBS2 END) AS WBS2,
      CONVERT(nvarchar(30), CASE WHEN WBS3='' THEN ' ' ELSE WBS3 END) AS WBS3,
      CONVERT(nvarchar(20), ISNULL(LD.Employee, '<empty>')) AS Employee,
      CONVERT(nvarchar(2), LD.TransType) AS TransType, 
      Category AS Category,
      CONVERT(nvarchar(14), LaborCode) AS LaborCode,
      BillStatus AS BillStatus,
      CONVERT(int, CASE WHEN TransDate IS NULL THEN -1 ELSE TransDate END) AS SK_TransactionDateID,
      RegHrs AS RegularHours, 
      OvtHrs AS OvertimeHours, 
      RegAmt AS RegularAmount, 
      OvtAmt AS OvertimeAmount, 
      BillExt AS LaborBilling, 
      SpecialOvtHrs AS SpecialOvertimeHours, 
      SpecialOvtAmt AS SpecialOvertimeAmount, 
      RegAmtProjectCurrency AS RegularAmountProjectCurrency, 
      OvtAmtProjectCurrency AS OvertimeAmountProjectCurrency, 
      SpecialOvtAmtProjectCurrency AS SpecialOvertimeAmountProjectCurrency, 
      RegAmtBillingCurrency AS RegularAmountBillingCurrency, 
      OvtAmtBillingCurrency AS OvertimeAmountBillingCurrency, 
      SpecialOvtAmtBillingCurrency AS SpecialOvertimeAmountBillingCurrency, 
      RegAmtEmployeeCurrency AS RegularAmountEmployeeCurrency, 
      OvtAmtEmployeeCurrency AS OvertimeAmountEmployeeCurrency, 
      SpecialOvtAmtEmployeeCurrency AS SpecialOvertimeAmountEmployeeCurrency,

      CASE WHEN LD.ChargeType = 'R' THEN (LD.RegHrs + LD.OvtHrs + LD.SpecialOvtHrs) ELSE 0 END AS DirectHours,
      CASE WHEN LD.ChargeType = 'R' THEN (LD.OvtHrs) ELSE 0 END AS [DirectOvertimeHours],
      CASE WHEN LD.ChargeType = 'R' THEN (LD.SpecialOvtHrs) ELSE 0 END AS [DirectSpecialOvertimeHours],

      CASE WHEN LD.ChargeType = 'R' THEN (LD.RegAmtEmployeeCurrency + LD.OvtAmtEmployeeCurrency + LD.SpecialOvtAmtEmployeeCurrency) ELSE 0 END AS DirectCost,
      CASE WHEN LD.ChargeType = 'R' THEN (LD.OvtAmtEmployeeCurrency) ELSE 0 END AS [DirectOvertimeAmount],
      CASE WHEN LD.ChargeType = 'R' THEN (LD.SpecialOvtAmtEmployeeCurrency) ELSE 0 END AS [DirectSpecialOvertimeAmount],

      CASE WHEN LD.ChargeType <> 'R' THEN (LD.RegHrs + LD.OvtHrs + LD.SpecialOvtHrs) ELSE 0 END AS IndirectHours,
      CASE WHEN LD.ChargeType <> 'R' THEN (LD.OvtHrs) ELSE 0 END AS [IndirectOvertimeHours],
      CASE WHEN LD.ChargeType <> 'R' THEN (LD.SpecialOvtHrs) ELSE 0 END AS [IndirectSpecialOvertimeHours],

      CASE WHEN LD.ChargeType <> 'R' THEN (LD.RegAmtEmployeeCurrency + LD.OvtAmtEmployeeCurrency + LD.SpecialOvtAmtEmployeeCurrency) ELSE 0 END AS IndirectCost,
      CASE WHEN LD.ChargeType <> 'R' THEN (LD.OvtAmtEmployeeCurrency) ELSE 0 END AS [IndirectOvertimeAmount],
      CASE WHEN LD.ChargeType <> 'R' THEN (LD.SpecialOvtAmtEmployeeCurrency) ELSE 0 END AS [IndirectSpecialOvertimeAmount],

      CASE WHEN LD.ChargeType = 'R' or TA.BenefitWBS1 IS NULL THEN 0 ELSE (LD.RegHrs + LD.OvtHrs + LD.SpecialOvtHrs) END AS BenefitHours,
      CASE WHEN LD.ChargeType = 'R' or TA.BenefitWBS1 IS NULL THEN 0 ELSE (LD.RegAmtEmployeeCurrency + LD.OvtAmtEmployeeCurrency + LD.SpecialOvtAmtEmployeeCurrency) END AS BenefitCost,

      CASE WHEN LD.ChargeType = 'R' and LD.LaborCode like CFGBillMain.NonBillLaborCode THEN (LD.RegHrs + LD.OvtHrs + LD.SpecialOvtHrs) ELSE 0 END AS NonBillableHours,
      CASE WHEN LD.ChargeType = 'R' and LD.LaborCode like CFGBillMain.NonBillLaborCode THEN (LD.OvtHrs) ELSE 0 END AS [NonBillableOvertimeHours],
      CASE WHEN LD.ChargeType = 'R' and LD.LaborCode like CFGBillMain.NonBillLaborCode THEN (LD.SpecialOvtHrs) ELSE 0 END AS [NonBillableSpecialOvertimeHours],

      CASE WHEN LD.ChargeType = 'R' and LD.LaborCode like CFGBillMain.NonBillLaborCode THEN (LD.RegAmtEmployeeCurrency + LD.OvtAmtEmployeeCurrency + LD.SpecialOvtAmtEmployeeCurrency) ELSE 0 END AS NonBillableCost,
      CASE WHEN LD.ChargeType = 'R' and LD.LaborCode like CFGBillMain.NonBillLaborCode THEN (LD.OvtAmtEmployeeCurrency) ELSE 0 END AS [NonBillableOvertimeAmount],
      CASE WHEN LD.ChargeType = 'R' and LD.LaborCode like CFGBillMain.NonBillLaborCode THEN (LD.SpecialOvtAmtEmployeeCurrency) ELSE 0 END AS [NonBillableSpecialOvertimeAmount],

     LD.RealizationAmountEmployeeCurrency,
     LD.RealizationAmountProjectCurrency,
     LD.RealizationAmountBillingCurrency,

--JYC Time Analysis - Multicurrency
      CASE WHEN LD.ChargeType = 'R' THEN (LD.RegAmtProjectCurrency + LD.OvtAmtProjectCurrency + LD.SpecialOvtAmtProjectCurrency) ELSE 0 END AS DirectCostProjectCurrency,
      CASE WHEN LD.ChargeType = 'R' THEN (LD.RegAmtBillingCurrency + LD.OvtAmtBillingCurrency + LD.SpecialOvtAmtBillingCurrency) ELSE 0 END AS DirectCostBillingCurrency,
      CASE WHEN LD.ChargeType = 'R' THEN (LD.OvtAmtProjectCurrency) ELSE 0 END AS [DirectOvertimeAmountProjectCurrency],
      CASE WHEN LD.ChargeType = 'R' THEN (LD.SpecialOvtAmtProjectCurrency) ELSE 0 END AS [DirectSpecialOvertimeAmountProjectCurrency],
      CASE WHEN LD.ChargeType = 'R' THEN (LD.OvtAmtBillingCurrency) ELSE 0 END AS [DirectOvertimeAmountBillingCurrency],
      CASE WHEN LD.ChargeType = 'R' THEN (LD.SpecialOvtAmtBillingCurrency) ELSE 0 END AS [DirectSpecialOvertimeAmountBillingCurrency],
      CASE WHEN LD.ChargeType <> 'R' THEN (LD.RegAmtProjectCurrency + LD.OvtAmtProjectCurrency + LD.SpecialOvtAmtProjectCurrency) ELSE 0 END AS IndirectCostProjectCurrency,
      CASE WHEN LD.ChargeType <> 'R' THEN (LD.RegAmtBillingCurrency + LD.OvtAmtBillingCurrency + LD.SpecialOvtAmtBillingCurrency) ELSE 0 END AS IndirectCostBillingCurrency,
      CASE WHEN LD.ChargeType <> 'R' THEN (LD.OvtAmtProjectCurrency) ELSE 0 END AS [IndirectOvertimeAmountProjectCurrency],
      CASE WHEN LD.ChargeType <> 'R' THEN (LD.OvtAmtBillingCurrency) ELSE 0 END AS [IndirectOvertimeAmountBillingCurrency],
      CASE WHEN LD.ChargeType <> 'R' THEN (LD.SpecialOvtAmtProjectCurrency) ELSE 0 END AS [IndirectSpecialOvertimeAmountProjectCurrency],
      CASE WHEN LD.ChargeType <> 'R' THEN (LD.SpecialOvtAmtBillingCurrency) ELSE 0 END AS [IndirectSpecialOvertimeAmountBillingCurrency],
      CASE WHEN LD.ChargeType <> 'R' and TA.BenefitWBS1 IS NULL THEN 0 ELSE (LD.RegAmtProjectCurrency + LD.OvtAmtProjectCurrency + LD.SpecialOvtAmtProjectCurrency) END AS BenefitCostProjectCurrency,
      CASE WHEN LD.ChargeType <> 'R' and TA.BenefitWBS1 IS NULL THEN 0 ELSE (LD.RegAmtBillingCurrency + LD.OvtAmtBillingCurrency + LD.SpecialOvtAmtBillingCurrency) END AS BenefitCostBillingCurrency,
      CASE WHEN LD.ChargeType = 'R' and LD.LaborCode like CFGBillMain.NonBillLaborCode THEN (LD.RegAmtProjectCurrency + LD.OvtAmtProjectCurrency + LD.SpecialOvtAmtProjectCurrency) ELSE 0 END AS NonBillableCostProjectCurrency,
      CASE WHEN LD.ChargeType = 'R' and LD.LaborCode like CFGBillMain.NonBillLaborCode THEN (LD.RegAmtBillingCurrency + LD.OvtAmtBillingCurrency + LD.SpecialOvtAmtBillingCurrency) ELSE 0 END AS NonBillableCostBillingCurrency,
      CASE WHEN LD.ChargeType = 'R' and LD.LaborCode like CFGBillMain.NonBillLaborCode THEN (LD.OvtAmtProjectCurrency) ELSE 0 END AS [NonBillableOvertimeAmountProjectCurrency],
      CASE WHEN LD.ChargeType = 'R' and LD.LaborCode like CFGBillMain.NonBillLaborCode THEN (LD.OvtAmtBillingCurrency) ELSE 0 END AS [NonBillableOvertimeAmountBillingCurrency],
      CASE WHEN LD.ChargeType = 'R' and LD.LaborCode like CFGBillMain.NonBillLaborCode THEN (LD.SpecialOvtAmtProjectCurrency) ELSE 0 END AS [NonBillableSpecialOvertimeAmountProjectCurrency],
      CASE WHEN LD.ChargeType = 'R' and LD.LaborCode like CFGBillMain.NonBillLaborCode THEN (LD.SpecialOvtAmtBillingCurrency) ELSE 0 END AS [NonBillableSpecialOvertimeAmountBillingCurrency],
      CONVERT(nvarchar(3), EMCurrencyCode) AS EMCurrencyCode
      FROM LD
        LEFT JOIN CFGBillMain ON CFGBillMain.Company = CASE WHEN @MulticompanyEnabled = 'Y' THEN SUBSTRING(LD.EMOrg, @org1Start, @org1Length) ELSE ' ' END
        LEFT JOIN @tabTAD AS TA ON TA.Company = CASE WHEN @MulticompanyEnabled = 'Y' THEN SUBSTRING(LD.EMOrg, @org1Start, @org1Length) ELSE ' ' END 
          AND LD.WBS1 = TA.BenefitWBS1
        LEFT JOIN CFGPostControl ON CFGPostControl.company = CASE WHEN @MulticompanyEnabled = 'Y' THEN SUBSTRING(LD.EMOrg, @org1Start, @org1Length) ELSE ' ' END 
          AND LD.Period = CFGPostControl.Period AND LD.PostSeq = CFGPostControl.PostSeq AND XChargeStatus = 'X'
     Where ProjectCost = 'Y'
----------------------------------------------------------------------------------------------------
	UNION ALL	SELECT 
      BILD.Period AS Period,
      BILD.PostSeq AS PostSeq,
      BILD.Pkey AS Pkey,
      CONVERT(nvarchar(30), WBS1) AS WBS1, 
      CONVERT(nvarchar(30), CASE WHEN WBS2='' THEN ' ' ELSE WBS2 END) AS WBS2,
      CONVERT(nvarchar(30), CASE WHEN WBS3='' THEN ' ' ELSE WBS3 END) AS WBS3,
      CONVERT(nvarchar(20), ISNULL(BILD.Employee, '<empty>')) AS Employee,
      CONVERT(nvarchar(2), BILD.TransType) AS TransType, 
      Category AS Category,
      CONVERT(nvarchar(14), LaborCode) AS LaborCode,
      BillStatus AS BillStatus,
      CONVERT(int, CASE WHEN TransDate IS NULL THEN -1 ELSE TransDate END) AS SK_TransactionDateID,
      0 AS RegularHours, 
      0 AS OvertimeHours, 
      0 AS RegularAmount, 
      0 AS OvertimeAmount, 
      0 AS LaborBilling, 
      0 AS SpecialOvertimeHours, 
      0 AS SpecialOvertimeAmount, 
      0 AS RegularAmountProjectCurrency, 
      0 AS OvertimeAmountProjectCurrency, 
      0 AS SpecialOvertimeAmountProjectCurrency, 
      0 AS RegularAmountBillingCurrency, 
      0 AS OvertimeAmountBillingCurrency, 
      0 AS SpecialOvertimeAmountBillingCurrency, 
      0 AS RegularAmountEmployeeCurrency, 
      0 AS OvertimeAmountEmployeeCurrency, 
      0 AS SpecialOvertimeAmountEmployeeCurrency,

      0 AS DirectHours,
      0 AS [DirectOvertimeHours],
      0 AS [DirectSpecialOvertimeHours],

      0 AS DirectCost,
      0 AS [DirectOvertimeAmount],
      0 AS [DirectSpecialOvertimeAmount],

      0 AS IndirectHours,
      0 AS [IndirectOvertimeHours],
      0 AS [IndirectSpecialOvertimeHours],

      0 AS IndirectCost,
      0 AS [IndirectOvertimeAmount],
      0 AS [IndirectSpecialOvertimeAmount],

      0 AS BenefitHours,
      0 AS BenefitCost,

      0 AS NonBillableHours,
      0 AS [NonBillableOvertimeHours],
      0 AS [NonBillableSpecialOvertimeHours],

      0 AS NonBillableCost,
      0 AS [NonBillableOvertimeAmount],
      0 AS [NonBillableSpecialOvertimeAmount],

     BILD.RealizationAmountEmployeeCurrency,
     BILD.RealizationAmountProjectCurrency,
     BILD.RealizationAmountBillingCurrency,

      0 AS DirectCostProjectCurrency,
      0 AS DirectCostBillingCurrency,
      0 AS [DirectOvertimeAmountProjectCurrency],
      0 AS [DirectSpecialOvertimeAmountProjectCurrency],
      0 AS [DirectOvertimeAmountBillingCurrency],
      0 AS [DirectSpecialOvertimeAmountBillingCurrency],
      0 AS IndirectCostProjectCurrency,
      0 AS IndirectCostBillingCurrency,
      0 AS [IndirectOvertimeAmountProjectCurrency],
      0 AS [IndirectOvertimeAmountBillingCurrency],
      0 AS [IndirectSpecialOvertimeAmountProjectCurrency],
      0 AS [IndirectSpecialOvertimeAmountBillingCurrency],
      0 AS BenefitCostProjectCurrency,
      0 AS BenefitCostBillingCurrency,
      0 AS NonBillableCostProjectCurrency,
      0 AS NonBillableCostBillingCurrency,
      0 AS [NonBillableOvertimeAmountProjectCurrency],
      0 AS [NonBillableOvertimeAmountBillingCurrency],
      0 AS [NonBillableSpecialOvertimeAmountProjectCurrency],
      0 AS [NonBillableSpecialOvertimeAmountBillingCurrency],
      CONVERT(nvarchar(3), EMCurrencyCode) AS EMCurrencyCode
		
      FROM BILD
      where (BILD.TransType = 'TS')  AND BILD.ProjectCost = 'Y' AND BILD.RealizationAmountBillingCurrency<>0 
		AND NOT EXISTS (SELECT 'x' FROM CFGPostControl WHERE CFGPostControl.company = CASE WHEN @MulticompanyEnabled = 'Y' THEN SUBSTRING(BILD.EMOrg, @org1Start, @org1Length) ELSE ' ' END 
		and BILD.Period = CFGPostControl.Period AND BILD.PostSeq = CFGPostControl.PostSeq AND XChargeStatus = 'X')  
		
----------------------------------------------------------------------------------------------------
	   UNION ALL	SELECT 
      LedgerMisc.Period AS Period,
      LedgerMisc.PostSeq AS PostSeq,
      LedgerMisc.Pkey AS Pkey,
      CONVERT(nvarchar(30), WBS1) AS WBS1, 
      CONVERT(nvarchar(30), CASE WHEN WBS2='' THEN ' ' ELSE WBS2 END) AS WBS2,
      CONVERT(nvarchar(30), CASE WHEN WBS3='' THEN ' ' ELSE WBS3 END) AS WBS3,
      CONVERT(nvarchar(20), ISNULL(LedgerMisc.Employee, '<empty>')) AS Employee,
      CONVERT(nvarchar(2), LedgerMisc.TransType) AS TransType, 
      0 AS Category,
       '<empty>' AS LaborCode,
      BillStatus AS BillStatus,
      CONVERT(int, CASE WHEN TransDate IS NULL THEN -1 ELSE TransDate END) AS SK_TransactionDateID,
      0 AS RegularHours, 
      0 AS OvertimeHours, 
      0 AS RegularAmount, 
      0 AS OvertimeAmount, 
      0 AS LaborBilling, 
      0 AS SpecialOvertimeHours, 
      0 AS SpecialOvertimeAmount, 
      0 AS RegularAmountProjectCurrency, 
      0 AS OvertimeAmountProjectCurrency, 
      0 AS SpecialOvertimeAmountProjectCurrency, 
      0 AS RegularAmountBillingCurrency, 
      0 AS OvertimeAmountBillingCurrency, 
      0 AS SpecialOvertimeAmountBillingCurrency, 
      0 AS RegularAmountEmployeeCurrency, 
      0 AS OvertimeAmountEmployeeCurrency, 
      0 AS SpecialOvertimeAmountEmployeeCurrency,

      0 AS DirectHours,
      0 AS [DirectOvertimeHours],
      0 AS [DirectSpecialOvertimeHours],

      0 AS DirectCost,
      0 AS [DirectOvertimeAmount],
      0 AS [DirectSpecialOvertimeAmount],

      0 AS IndirectHours,
      0 AS [IndirectOvertimeHours],
      0 AS [IndirectSpecialOvertimeHours],

      0 AS IndirectCost,
      0 AS [IndirectOvertimeAmount],
      0 AS [IndirectSpecialOvertimeAmount],

      0 AS BenefitHours,
      0 AS BenefitCost,

      0 AS NonBillableHours,
      0 AS [NonBillableOvertimeHours],
      0 AS [NonBillableSpecialOvertimeHours],

      0 AS NonBillableCost,
      0 AS [NonBillableOvertimeAmount],
      0 AS [NonBillableSpecialOvertimeAmount],

     LedgerMisc.RealizationAmountEmployeeCurrency,
     LedgerMisc.RealizationAmountProjectCurrency,
     LedgerMisc.RealizationAmountBillingCurrency,

      0 AS DirectCostProjectCurrency,
      0 AS DirectCostBillingCurrency,
      0 AS [DirectOvertimeAmountProjectCurrency],
      0 AS [DirectSpecialOvertimeAmountProjectCurrency],
      0 AS [DirectOvertimeAmountBillingCurrency],
      0 AS [DirectSpecialOvertimeAmountBillingCurrency],
      0 AS IndirectCostProjectCurrency,
      0 AS IndirectCostBillingCurrency,
      0 AS [IndirectOvertimeAmountProjectCurrency],
      0 AS [IndirectOvertimeAmountBillingCurrency],
      0 AS [IndirectSpecialOvertimeAmountProjectCurrency],
      0 AS [IndirectSpecialOvertimeAmountBillingCurrency],
      0 AS BenefitCostProjectCurrency,
      0 AS BenefitCostBillingCurrency,
      0 AS NonBillableCostProjectCurrency,
      0 AS NonBillableCostBillingCurrency,
      0 AS [NonBillableOvertimeAmountProjectCurrency],
      0 AS [NonBillableOvertimeAmountBillingCurrency],
      0 AS [NonBillableSpecialOvertimeAmountProjectCurrency],
      0 AS [NonBillableSpecialOvertimeAmountBillingCurrency],
      CONVERT(nvarchar(3), ISNULL(CFGMainData.FunctionalCurrencyCode,Main_EM.FunctionalCurrencyCode)) AS EMCurrencyCode
	 FROM LedgerMisc LEFT JOIN UN ON UN.UnitTable= LedgerMisc.UnitTable and UN.Unit = LedgerMisc.Unit 
		LEFT JOIN EM ON LedgerMisc.Employee = EM.Employee
		LEFT JOIN Organization ON EM.Org = Organization.Org 
		LEFT JOIN CFGPostControl CP on CP.Period=LedgerMisc.Period and CP.PostSeq=LedgerMisc.PostSeq
		Left JOIN CFGMainData on CFGMainData.Company=CP.Company
		Left JOIN CFGMainData Main_EM on Main_EM.Company=CASE WHEN @MulticompanyEnabled = 'Y' THEN SUBSTRING(EM.Org, @org1Start, @org1Length) ELSE ' ' END
	 where un.EmployeeSpecificRevenue='Y'
		AND (LedgerMisc.TransType = 'UN')  AND LedgerMisc.ProjectCost = 'Y' AND LedgerMisc.RealizationAmountBillingCurrency<>0 
		AND NOT EXISTS (SELECT 'x' FROM CFGPostControl WHERE CFGPostControl.company = CASE WHEN @MulticompanyEnabled = 'Y' THEN SUBSTRING(LedgerMisc.Org, @org1Start, @org1Length) ELSE ' ' END 
		and LedgerMisc.Period = CFGPostControl.Period AND LedgerMisc.PostSeq = CFGPostControl.PostSeq AND XChargeStatus = 'X')	  
  RETURN

END -- DW$tabFactLabor

GO
