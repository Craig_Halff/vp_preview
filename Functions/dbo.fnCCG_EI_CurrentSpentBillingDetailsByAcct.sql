SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_CurrentSpentBillingDetailsByAcct] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), @Key varchar(32))
RETURNS @T TABLE 
(
	TopOrder	int,
	Descr		varchar(255),
--	Trans Date					Vendor						Billing						Comments
	Value1		datetime,		Value2		varchar(255),	Value3		money,			Value4		varchar(255),
	HTMLValue1	varchar(50),	HTMLValue2	varchar(255),	HTMLValue3	varchar(50),	HTMLValue4	varchar(255),
	Align1		varchar(10),	Align2		varchar(10),	Align3		varchar(10),	Align4		varchar(10)
)
AS BEGIN
/*
	Copyright (c) 2016 Central Consulting Group.  All rights reserved.
	
	select * from LD where wbs1='2003005.xx'
	select * from dbo.fnCCG_EI_CurrentSpentBillingDetailsByAcct('2003005.00',' ',' ','00011')
	select * from dbo.fnCCG_EI_CurrentSpentBillingDetailsByAcct('2003005.xx','1PD',' ')
*/
	declare @ThruPeriod int, @ThruDate datetime

	select @ThruPeriod=ThruPeriod, @ThruDate=ThruDate
	from CCG_EI_ConfigInvoiceGroups cig
	inner join ProjectCustomTabFields pctf on pctf.custInvoiceGroup=cig.InvoiceGroup
	where pctf.WBS1=@WBS1 and pctf.WBS2=' '

	if @ThruPeriod is null	set @ThruPeriod=999999
	if @ThruDate is null	set @ThruDate='12/31/2050'

	insert into @T (TopOrder, Descr, Value1, Value2, Value3,Value4, HTMLValue1, HTMLValue2, HTMLValue3,HTMLValue4, Align1, Align2, Align3,Align4)
	select 4 as TopOrder, CA.Name, TransDate, VE.Name, BillExt, Descr, '', '', '','', 'right', 'left', 'right','left'
	from (
		select Account, TransDate, L.Vendor , BillExt, IsNull(Desc1,'') + ' / ' + IsNull(Desc2,'') as Descr
		from LedgerMisc L --left join CA  on CA.Account=L.Account
		where Account=@Key and WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus not in ('M','R') and BillExt<>0  
		  and Period = @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
		UNION ALL
		select Account, TransDate, L.Vendor , BillExt, IsNull(Desc1,'') + ' / ' + IsNull(Desc2,'') as Descr
		from LedgerAP L --left join CA  on CA.Account=L.Account
		where Account=@Key and WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus not in ('M','R') and BillExt<>0  
		  and Period = @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
		UNION ALL
		select Account, TransDate, L.Vendor , BillExt, IsNull(Desc1,'') + ' / ' + IsNull(Desc2,'') as Descr
		from LedgerEX L --left join CA  on CA.Account=L.Account
		where Account=@Key and WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus not in ('M','R') and BillExt<>0  
		  and Period = @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
		UNION ALL
		select Account, TransDate, L.Vendor , BillExt, IsNull(Desc1,'') + ' / ' + IsNull(Desc2,'') as Descr
		from LedgerAR L --left join CA  on CA.Account=L.Account
		where Account=@Key and WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus not in ('M','R') and BillExt<>0  
		  and Period = @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
		--UNION ALL
		--select Account, TransDate, L.Vendor , BillExt, IsNull(Desc1,'') + ' / ' + IsNull(Desc2,'') as Descr
		--from BIED L --left join CA  on CA.Account=L.Account
		--where Account=@Key and WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillStatus not in ('M','R') and BillExt<>0  
		--  and Period = @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
	) as Expenses left join CA on CA.Account=Expenses.Account left join VE on VE.Vendor = Expenses.Vendor
	
		-- Add grand total line:
	insert into @T (TopOrder, Descr, Value3, HTMLValue3, Align3)
	select 90 as TopOrder, '<b>Total</b>', (select sum(Value3) from @T where TopOrder in (4)), '', 'right'

	update @T set 
		HTMLValue1 = Case When Value1 is null Then '&nbsp;' Else Convert(varchar(10), Value1, 101) End,
		HTMLValue2 = IsNull(Value2, '&nbsp;'),
		HTMLValue3 = Case When Value3 is null Then '&nbsp;' Else '$' + Convert(varchar,Convert(money, Value3),1) End,
		HTMLValue4 = IsNull(Value4, '&nbsp;')
		
	update @T set HTMLValue3='<b>'+HTMLValue3+'</b>' where TopOrder in (90)
	return
END
GO
