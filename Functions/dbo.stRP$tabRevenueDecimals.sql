SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabRevenueDecimals]
  (@strPlanID varchar(32))

  RETURNS @tabDecimals TABLE(
    HrDecimals smallint,
    AmtCostDecimals smallint,
    AmtBillDecimals smallint,
    LabRateDecimals smallint
  )

BEGIN
  DECLARE @strCostCurrencyCode nvarchar(3)
  DECLARE @strBillCurrencyCode nvarchar(3)

  DECLARE @siHrDecimals smallint
  DECLARE @siAmtDecimals smallint
  DECLARE @siAmtCostDecimals smallint
  DECLARE @siAmtBillDecimals smallint
  DECLARE @siLabRateDecimals smallint

 
  SELECT 
    @strCostCurrencyCode = PR.ProjectCurrencyCode,
    @strBillCurrencyCode = PR.BillingCurrencyCode
    FROM PNPlan AS P
	INNER JOIN PR ON PR.WBS1 = P.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' 
    WHERE P.PlanID = @strPlanID

   -- Get Decimal Settings from CFGRMSettings by Company.

  SELECT
    @siHrDecimals = HrDecimals,
    @siAmtDecimals = AmtDecimals
    FROM CFGRMSettings AS CRM

  -- Compute @siAmtCostDecimals and @siAmtBillDecimals

  SELECT 
    @siAmtCostDecimals = ISNULL(MIN(CASE WHEN @siAmtDecimals <> 0 THEN CC.DecimalPlaces ELSE 0 END), 0), 
    @siAmtBillDecimals = ISNULL(MIN(CASE WHEN @siAmtDecimals <> 0 THEN BC.DecimalPlaces ELSE 0 END), 0)
    FROM FW_CFGCurrency AS CC, FW_CFGCurrency AS BC 
    WHERE CC.Code = @strCostCurrencyCode AND BC.Code = @strBillCurrencyCode 

  INSERT @tabDecimals (
    HrDecimals,
    AmtCostDecimals,
    AmtBillDecimals,
    LabRateDecimals
  )
    SELECT 
      @siHrDecimals AS HrDecimals,
      @siAmtCostDecimals AS AmtCostDecimals, 
      @siAmtBillDecimals AS AmtBillDecimals, 
      ISNULL(@siLabRateDecimals, 0) AS LabRateDecimals
      
  RETURN

END -- fn_stRP$tabPlanDecimals
GO
