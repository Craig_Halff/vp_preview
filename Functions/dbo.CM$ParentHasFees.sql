SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[CM$ParentHasFees]
  (@strWBS1 Nvarchar(30),
   @strWBS2 Nvarchar(7) = ' ',
   @strWBS3 Nvarchar(7) = ' ')
  RETURNS char(1)
BEGIN

  /*--------------------------------------------------------------------------------------------------------------------
    This function is used to determine whether the input Parent Project row has any Contract Management Fees.
  */--------------------------------------------------------------------------------------------------------------------

  DECLARE @strHasFees AS char(1)
 
  SELECT
    @strHasFees = CASE WHEN (AutoSumComp = 'Y') AND ISNULL(SUM(ABS(TotalFees)), 0) != 0
                       THEN 'Y'
                       ELSE 'N'
                       END
    FROM 
      (SELECT 
         ISNULL(SUM(ABS(CD.Fee) + ABS(CD.ConsultFee) + ABS(CD.ReimbAllow) + 
                    ABS(CD.FeeBillingCurrency) + ABS(CD.ConsultFeeBillingCurrency) + ABS(CD.ReimbAllowBillingCurrency) +
                    ABS(CD.FeeFunctionalCurrency) + ABS(CD.ConsultFeeFunctionalCurrency) + ABS(CD.ReimbAllowFunctionalCurrency)), 0) AS TotalFees
         FROM ContractDetails AS CD
           INNER JOIN Contracts AS C ON CD.WBS1 = C.WBS1 AND CD.ContractNumber = C.ContractNumber AND C.FeeIncludeInd = 'Y'
           INNER JOIN PR ON CD.WBS1 = PR.WBS1 AND CD.WBS2 = PR.WBS2 AND CD.WBS3 = PR.WBS3 AND PR.SubLevel = 'N'
         WHERE CD.WBS1 = @strWBS1 AND CD.WBS2 = @strWBS2 AND CD.WBS3 = @strWBS3) AS X
      INNER JOIN FW_CFGSystem ON (1 = 1)
      GROUP BY AutoSumComp, SyncProjToContractFees

  RETURN(@strHasFees)

END -- fn_CM$ParentHasFees
GO
