SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$tabWIPAging]
  (@strWBS1 Nvarchar(30),
   @strJTDDate VARCHAR(8)) -- Date must be in format: 'yyyymmdd' regardless of UI Culture.

  RETURNS @tabWIPAging TABLE
    (BilledAmt decimal(19,4),
     TotalUnbilledAmt decimal(19,4),
     WIPOver30 decimal(19,4),
     WIPOver45 decimal(19,4),
     WIPOver60 decimal(19,4),
     WIPOver90 decimal(19,4),
     WIPOver120 decimal(19,4)
    )

BEGIN

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime

  DECLARE @decBilledAmt decimal(19,4)
 
	DECLARE @tabLD
	  TABLE (RowID int IDENTITY(1,1),
           TransDate datetime,
           BillExt decimal(19,4)
           PRIMARY KEY(RowID, TransDate))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
          
  -- Set JTD/ETC Dates.
  
  SET @dtJTDDate = CONVERT(datetime, @strJTDDate)
  SET @dtETCDate = DATEADD(d, 1, @dtJTDDate) 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Calculate Billed Amount.

  SELECT DISTINCT
    @decBilledAmt = SUM(-AmountBillingCurrency)
    FROM LedgerAR AS L
    WHERE L.WBS1 = @strWBS1 AND
      (TransType = 'IN' AND L.AutoEntry = 'N' AND 
       (ISNULL(L.SubType, ' ') <> 'I' AND ISNULL(L.SubType, ' ') <> 'R'))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find Unbilled Amounts.

  INSERT INTO @tabLD
    (TransDate,
     BillExt
    )
    SELECT
      TransDate AS TransDate,
      SUM(BillExt) AS BillExt
      FROM
        (SELECT
           LD.TransDate AS TransDate,
           SUM(LD.BillExt) AS BillExt
           FROM LD 
           WHERE LD.ProjectCost = 'Y' AND LD.TransDate <= @dtJTDDate AND LD.WBS1 = @strWBS1 AND LD.BillStatus IN ('B', 'H')
           GROUP BY LD.TransDate
         UNION ALL SELECT
           TD.TransDate AS TransDate,
           SUM(TD.BillExt) AS BillExt
           FROM tkDetail AS TD
             INNER JOIN EM ON TD.Employee = EM.Employee  
             INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
           WHERE TD.TransDate <= @dtJTDDate AND TD.WBS1 = @strWBS1
           GROUP BY TD.TransDate
         UNION ALL SELECT
           TD.TransDate AS TransDate,
           SUM(TD.BillExt) AS BillExt
           FROM tsDetail AS TD
             INNER JOIN EM ON TD.Employee = EM.Employee  
             INNER JOIN tsControl AS TC ON (TC.Posted = 'N' AND TD.Batch = TC.Batch)
           WHERE TD.TransDate <= @dtJTDDate AND TD.WBS1 = @strWBS1
           GROUP BY TD.TransDate
        ) AS X
      GROUP BY TransDate
 
 -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT INTO @tabWIPAging 
    (BilledAmt,
     TotalUnbilledAmt,
     WIPOver30,
     WIPOver45,
     WIPOver60,
     WIPOver90,
     WIPOver120
    )
    SELECT
      @decBilledAmt,
      SUM(BillExt) AS TotalUnbilledAmt,
      SUM(WIPOver30) AS WIPOver30,
      SUM(WIPOver45) AS WIPOver45,
      SUM(WIPOver60) AS WIPOver60,
      SUM(WIPOver90) AS WIPOver90,
      SUM(WIPOver120) AS WIPOver120
      FROM
        (SELECT
           SUM(BillExt) AS BillExt,
           CASE WHEN AgingBucket = 0 THEN SUM(BillExt) ELSE 0.0000 END AS WIPOver30,
           CASE WHEN AgingBucket = 1 THEN SUM(BillExt) ELSE 0.0000 END AS WIPOver45,
           CASE WHEN AgingBucket = 2 THEN SUM(BillExt) ELSE 0.0000 END AS WIPOver60,
           CASE WHEN AgingBucket = 3 THEN SUM(BillExt) ELSE 0.0000 END AS WIPOver90,
           CASE WHEN AgingBucket = 4 THEN SUM(BillExt) ELSE 0.0000 END AS WIPOver120
           FROM
             (SELECT
                SUM(BillExt) AS BillExt,
                CASE
                  WHEN SUM(BillExt) = 0 THEN -1
                  WHEN CONVERT(int, @dtJTDDate) - CONVERT(int, TransDate) BETWEEN 31 AND 45 THEN 0 /* AR Over 30 */
                  WHEN CONVERT(int, @dtJTDDate) - CONVERT(int, TransDate) BETWEEN 46 AND 60 THEN 1 /* AR Over 45 */
                  WHEN CONVERT(int, @dtJTDDate) - CONVERT(int, TransDate) BETWEEN 61 AND 90 THEN 2 /* AR Over 60 */
                  WHEN CONVERT(int, @dtJTDDate) - CONVERT(int, TransDate) BETWEEN 91 AND 120 THEN 3 /* AR Over 90 */
                  WHEN CONVERT(int, @dtJTDDate) - CONVERT(int, TransDate) > 120 THEN 4 /* AR Over 120 */
                  END As AgingBucket
                FROM @tabLD
                GROUP BY TransDate
             ) AS B
           GROUP BY AgingBucket
        ) AS X
            
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
