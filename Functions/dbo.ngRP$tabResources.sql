SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[ngRP$tabResources](
  @strRowID nvarchar(255),
  @strScopeStartDate varchar(8) /* Date must be in format: 'yyyymmdd' regardless of UI Culture */,
  @strScopeEndDate varchar(8) /* Date must be in format: 'yyyymmdd' regardless of UI Culture */,
  @strMode varchar(1) /* S = Self, C = Children */,
  @bitUseJTDData bit = 1
)
  RETURNS @tabResources TABLE (
    RowID nvarchar(255) COLLATE database_default,
    ParentRowID nvarchar(255) COLLATE database_default,
    RowLevel int,
    HasChildren bit,
    SelectFlag varchar(1) COLLATE database_default,
    Name nvarchar(255) COLLATE database_default,
    BillableFlg varchar(1) COLLATE database_default,
    MatchPct decimal(19,4),
    StartDate datetime,
    EndDate datetime,
    AvailableHrs decimal(19,4),
    PlannedHrs decimal(19,4),
    BaselineHrs decimal(19,4),
    SelectedPlannedHrs decimal(19,4),
    SelectedScheduledHrs decimal(19,4),
    SelectedBillableHrs decimal(19,4),
    ScheduledPct decimal(19,4),
    TargetUtilization decimal(19,4),
    UtilizationPct decimal(19,4),
    SelectedETCHrs decimal(19,4),
    ETCHrs decimal(19,4),
    JTDHrs decimal(19,4),
    EACHrs decimal(19,4),
    HasPhoto bit,
	PhotoModDate datetime,
    BeforeETCHrs decimal(19,4),
    AfterETCHrs decimal(19,4),
    UtilRatioFlg varchar(1) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ResourceID nvarchar(20) COLLATE database_default,
    GenericResourceID nvarchar(20) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    Company nvarchar(14) COLLATE database_default,
    VersionID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    TopWBS1 nvarchar(30) COLLATE database_default,
    TopName nvarchar(255) COLLATE database_default,
    TopKonaSpace int,
    PMFullName nvarchar(255) COLLATE database_default,
    ClientName nvarchar(100) COLLATE database_default,
    RT_Status varchar(1) COLLATE database_default,
    R_Status varchar(1) COLLATE database_default,
    HardBooked varchar(1) COLLATE database_default,
    HoursPerDay decimal(19,4),
    SumBeforeWD decimal(19,4),
    SumAfterWD decimal(19,4),
    ContractStartDate datetime,
    ContractEndDate datetime,
    UtilizationScheduleFlg varchar(1),
    MinASGDate datetime,
    MaxASGDate datetime,
    MinTPDDate datetime,
    MaxTPDDate datetime,
    Org nvarchar(255),
    OrgName nvarchar(255)
  )

BEGIN

/*

  When this UDF is executed, it will return different kinds of data depending on the input parameters @strRowID and @intOutlineLevel.

  1. To return data for a Resource row: @strMode = "S", @strRowID contains only ID of a Resource and no TaskID.

  2. To return data for the top most WBS rows: @strMode = "C", @strRowID contains only ID of a Resource and no TaskID.
  3. To return data for WBS Level 1 rows: @strMode = "C", @strRowID contains ID of a Resource and TaskID of a WBS Level 0 row.
  4. To return data for WBS Level 2 rows: @strMode = "C", @strRowID contains ID of a Resource and TaskID of a WBS Level 1 row.

  5. To return data for the top most WBS rows: @strMode = "S", @strRowID contains ID of a Resource and TaskID of a WBS Level 0 row.
  6. To return data for WBS Level 1 row: @strMode = "S", @strRowID contains ID of a Resource and TaskID of a WBS Level 1 row.
  7. To return data for WBS Level 2 row: @strMode = "S", @strRowID contains ID of a Resource and TaskID of a WBS Level 2 row.

  For WBS Level {1 .. n}, @strRowID will dictate which WBS or chidren rows being return using the TaskID embedded in @strRowID.
  @intOutlineLevel will be calculated from that TaskID. 

*/


  DECLARE @strCompany nvarchar(14)
  DECLARE @strEMHomeCompany nvarchar(14)
  DECLARE @strResourceID nvarchar(20)
  DECLARE @strResourceName nvarchar(255)
  DECLARE @strGenericResourceID nvarchar(20)
  DECLARE @strPlanID varchar(32) = NULL
  DECLARE @strTaskID varchar(32)
  DECLARE @strResourceType varchar(1)
  DECLARE @strIDPrefix nvarchar(255)
  DECLARE @bitHasPhoto bit
  DECLARE @dtPhotoModDate datetime

  DECLARE @strUtilRatioFlg varchar(1)
  DECLARE @strUseBookingForEmpHours varchar(1)
  DECLARE @strUseBookingForGenHours varchar(1)
  DECLARE @strIncludeSoftBookedHours varchar(1)
  DECLARE @strTopName nvarchar(255)
  DECLARE @strParentRowID nvarchar(255) = ''

  DECLARE @strR_Status varchar(1)
  DECLARE @strASG_HardBooked varchar(1)

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
  DECLARE @dtScopeStartDate datetime
  DECLARE @dtScopeEndDate datetime
  DECLARE @dtScopeBeforeDate datetime
  DECLARE @dtScopeAfterDate datetime
 
  DECLARE @intOutlineLevel int
  DECLARE @intTaskCount int
  DECLARE @intLDTaskCount int 

  DECLARE @siHrDecimals int /* CFGRMSettings.HrDecimals */
  DECLARE @siOverUtilOption smallint
  DECLARE @siEmpUtilPercent smallint
  DECLARE @siEmpUtilPlusPercent smallint
  DECLARE @siEmpUtilMorePercent smallint
  DECLARE @siUnderUtilPercent smallint
  DECLARE @siOverSchPercent smallint
  DECLARE @siUnderSchPercent smallint

  DECLARE @tiPercentOfUtilization tinyint = 1
  DECLARE @tiRatioPlusPercentage tinyint = 2
  DECLARE @tiGreaterThanPercentage tinyint = 3

  DECLARE @decAvailableHrs decimal(19,4)
  DECLARE @decHoursPerDay decimal(19,4)
  DECLARE @decUtilizationRatio decimal(19,4)

  DECLARE @decScheduledPct decimal(19,4)
  DECLARE @decUtilizationPct decimal(19,4)
  DECLARE @decURUnderLimit decimal(19,4)
  DECLARE @decUROverLimit decimal(19,4)
  
  -- Declare Temp tables.

  DECLARE @tabAssignment TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    BeforeWD decimal(19,4),
    AfterWD decimal(19,4),
    HardBookedFlg int,
    OutlineNumber varchar(255) COLLATE database_default
    UNIQUE(PlanID, TaskID, AssignmentID)
  )

  DECLARE @tabLDEmployee TABLE ( 
    WBS1 nvarchar(255) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    Employee nvarchar(20) COLLATE database_default
    UNIQUE(WBS1, WBS2, WBS3, LaborCode, Employee) 
  )

  DECLARE @tabTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    Name nvarchar(255) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    Company nvarchar(14) COLLATE database_default,
    VersionID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    T_Status varchar(1) COLLATE database_default,
    ChargeType varchar(1) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int,
    ChildrenCount int,
    TopWBS1 nvarchar(30) COLLATE database_default,
    TopName nvarchar(255) COLLATE database_default,
    TopKonaSpace int,
    PMFullName nvarchar(255) COLLATE database_default,
    ClientName nvarchar(100) COLLATE database_default,
    ContractStartDate datetime,
    ContractEndDate datetime,
    UtilizationScheduleFlg varchar(1),
    ASG_HardBooked varchar(1) COLLATE database_default,
    ASG_BeforeWD decimal(19,4),
    ASG_AfterWD decimal(19,4),
    ASG_StartDate datetime,
    ASG_EndDate datetime,
    Org nvarchar(255),
    OrgName nvarchar(255)
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabLabTPD TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChargeType varchar(1) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    UtilizationScheduleFlg varchar(1),
    PlannedHrs decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, OutlineNumber, StartDate, EndDate)
  )

  DECLARE @tabLabBaselineTPD TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PlannedHrs decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, OutlineNumber, StartDate, EndDate)
  )

  DECLARE @tabScheduledTPD TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChargeType varchar(1) COLLATE database_default,
    UtilizationScheduleFlg  varchar(1),
    PlannedHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabSelectedTPD TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChargeType varchar(1) COLLATE database_default,
    UtilizationScheduleFlg  varchar(1),
    PlannedHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabBillableTPD TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    UtilizationScheduleFlg  varchar(1),
    PlannedHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabETCTPD TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PeriodHrs decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, OutlineNumber, StartDate, EndDate)
  )

  DECLARE @tabTotalETC TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ETCHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabBeforeETC TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ETCHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabAfterETC TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ETCHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabSelectedETC TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ETCHrs decimal(19,4)
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabJTDLD TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    PeriodHrs decimal(19,4),
    JTDLabCost decimal(19,4),	
    JTDLabBill decimal(19,4)
    UNIQUE (RowID, WBS1, WBS2, WBS3, LaborCode)
  )

  DECLARE @tabLD TABLE (
    RowID  int IDENTITY(1,1),
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    PeriodHrs decimal(19,4),
    JTDLabCost decimal(19,4),	
    JTDLabBill decimal(19,4)
    PRIMARY KEY(RowID, PlanID, OutlineNumber)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Set Company to non-Multi-Company.
  
  SELECT @strCompany = dbo.GetActiveCompany()

  -- Get RM Settings.
  
  SELECT    
    @siOverUtilOption = OverUtilOption,
    @siEmpUtilPercent = EmpUtilPercent,
    @siEmpUtilPlusPercent = EmpUtilPlusPercent,
    @siEmpUtilMorePercent = EmpUtilMorePercent,
    @siUnderUtilPercent = UnderUtilPercent,
    @siOverSchPercent = OverSchPercent,
    @siUnderSchPercent = UnderSchPercent,
    @strUseBookingForEmpHours = UseBookingForEmpHours,
    @strUseBookingForGenHours = UseBookingForGenHours,
    @strIncludeSoftBookedHours = IncludeSoftBookedHours,
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE()))),
    @siHrDecimals = HrDecimals
    FROM CFGRMSettings

  -- Set Dates
  
  SELECT @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)
  SELECT @dtScopeStartDate = CONVERT(datetime, @strScopeStartDate)
  SELECT @dtScopeEndDate = CONVERT(datetime, @strScopeEndDate)
  SELECT @dtScopeBeforeDate = DATEADD(DAY, -1, @dtScopeStartDate)
  SELECT @dtScopeAfterDate = DATEADD(DAY, 1, @dtScopeEndDate)

  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. E~<RPAssignment.ResourceID>|<RPTask.TaskID>
  --   2. G~<RPAssignment.GenericResourceID>|<RPTask.TaskID>

  SET @strResourceType = SUBSTRING(@strRowID, 1, 1)
  SET @strIDPrefix = LEFT(@strRowID, CHARINDEX('|', @strRowID) - 1)

  SET @decAvailableHrs = 0
  SET @decHoursPerDay = 0

  IF (@strResourceType = 'E')
    BEGIN

      SET @strResourceID = REPLACE(@strIDPrefix, 'E~', '')
      SET @strGenericResourceID = NULL

      SELECT 
        @strResourceName = CONVERT(nvarchar(255), case when EM.PreferredName is Null then isnull(EM.firstName + ' ', N'') + EM.lastName else EM.PreferredName + ' ' + EM.lastname end + isnull(', ' + EMSuffix.Suffix, N'')),
        @bitHasPhoto = CASE WHEN EP.Photo IS NULL THEN 0 ELSE 1 END,
		@dtPhotoModDate = CASE WHEN EP.Photo IS NOT NULL THEN EP.ModDate ELSE NULL END,
        @decUtilizationRatio = EM.UtilizationRatio,
        @decHoursPerDay = EM.HoursPerDay,
        @strR_Status = EM.Status,
    @strEMHomeCompany = EM.HomeCompany
    FROM EM 
    LEFT JOIN EMPhoto AS EP ON EM.Employee = EP.Employee
    LEFT JOIN CFGSuffix AS EMSuffix ON EMSuffix.Code = EM.Suffix
    WHERE EM.Employee = @strResourceID

      SELECT @decAvailableHrs = ROUND(@decHoursPerDay * dbo.DLTK$NumWorkingDays(@dtScopeStartDate, @dtScopeEndDate, @strEMHomeCompany), @siHrDecimals)

      SET @decURUnderLimit = ROUND(((@decUtilizationRatio / 100.0000) * CONVERT(DECIMAL, @siUnderUtilPercent)), 2)

      SET @decUROverLimit = 
        CASE @siOverUtilOption
          WHEN @tiPercentOfUtilization
            THEN ROUND(((@decUtilizationRatio / 100.0000) * CONVERT(DECIMAL, @siEmpUtilPercent)), 2)
          WHEN @tiRatioPlusPercentage
            THEN ROUND((@decUtilizationRatio + CONVERT(DECIMAL, @siEmpUtilPlusPercent)), 2)
          WHEN @tiGreaterThanPercentage
            THEN ROUND(CONVERT(DECIMAL, @siEmpUtilMorePercent), 2)
        END

    END
  ELSE
    BEGIN
      SET @strGenericResourceID = REPLACE(@strIDPrefix, 'G~', '')
      SET @strResourceID = NULL
      SET @bitHasPhoto = 0
	  SET @dtPhotoModDate = NULL
      SET @decUtilizationRatio = 0
      SELECT 
        @strResourceName = GR.Name,
        @strR_Status = GR.Status
        FROM GR 
        WHERE GR.Code = @strGenericResourceID
    END

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

  IF (DATALENGTH(@strTaskID) = 0)
    BEGIN
      SELECT 
        @intOutlineLevel = 
          CASE 
            WHEN @strMode = 'S' THEN -1
            WHEN @strMode = 'C' THEN 0
          END,
        @strPlanID = NULL
    END
  ELSE
    BEGIN
      SELECT 
        @intOutlineLevel = 
          CASE 
            WHEN @strMode = 'S' THEN OutlineLevel
            WHEN @strMode = 'C' THEN OutlineLevel + 1
          END,
        @strPlanID = T.PlanID
        FROM RPTask AS T
        WHERE T.TaskID = @strTaskID
    END

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Determine ParentRowID based on @strMode.

  SELECT
    @strParentRowID = 
      CASE 
        WHEN @strMode = 'C' THEN @strRowID
        WHEN (@strMode = 'S' AND @intOutlineLevel >= 0) THEN @strIDPrefix + '|' + ISNULL(PT.TaskID, '')
      END 
    FROM RPTask AS CT
      LEFT JOIN RPTask AS PT ON PT.PlanID = CT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber
    WHERE CT.TaskID = @strTaskID

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strTaskID = '')
  BEGIN
    INSERT @tabAssignment(
    PlanID,
    TaskID,
    AssignmentID,
    StartDate,
    EndDate,
    BeforeWD,
    AfterWD,
    HardBookedFlg,
    OutlineNumber
    )
    SELECT DISTINCT
      A.PlanID,
      A.TaskID,
      A.AssignmentID,
      A.StartDate,
      A.EndDate,
      CASE
      WHEN ((A.StartDate >= @dtScopeStartDate) OR (@dtScopeBeforeDate <= @dtETCDate))
      THEN 0
      ELSE
        dbo.DLTK$NumWorkingDays(
        CASE
          WHEN (A.StartDate BETWEEN @dtETCDate AND @dtScopeBeforeDate)
          THEN A.StartDate
          ELSE @dtETCDate
        END, 
        CASE
          WHEN (A.EndDate BETWEEN @dtETCDate AND @dtScopeBeforeDate)
          THEN A.EndDate
          ELSE @dtScopeBeforeDate
        END, 
        @strCompany
        )
      END AS BeforeWD,
      CASE
      WHEN (A.EndDate <= @dtScopeEndDate)
      THEN 0
      ELSE
        dbo.DLTK$NumWorkingDays(
        CASE
          WHEN (A.StartDate >= @dtScopeAfterDate)
          THEN A.StartDate
          ELSE @dtScopeAfterDate
        END, 
        A.EndDate,
        @strCompany
        )
      END AS AfterWD,
      CASE HardBooked
      WHEN 'Y' THEN 1
      WHEN 'N' THEN -1
      END AS HardBookedFlg,
      AT.OutlineNumber AS OutlineNumber
      FROM RPAssignment AS A
      INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
      WHERE
      A.PlanID LIKE ISNULL(@strPlanID, '%') AND AT.WBS1 <> '<none>' AND 
      ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') AND 
      ISNULL(A.GenericResourceID, '|') = ISNULL( @strGenericResourceID, '|') AND
      A.EndDate >= @dtETCDate
  END
  ELSE
  BEGIN
    INSERT @tabAssignment(
    PlanID,
    TaskID,
    AssignmentID,
    StartDate,
    EndDate,
    BeforeWD,
    AfterWD,
    HardBookedFlg,
    OutlineNumber
    )
    SELECT DISTINCT
      A.PlanID,
      A.TaskID,
      A.AssignmentID,
      A.StartDate,
      A.EndDate,
      CASE
      WHEN ((A.StartDate >= @dtScopeStartDate) OR (@dtScopeBeforeDate <= @dtETCDate))
      THEN 0
      ELSE
        dbo.DLTK$NumWorkingDays(
        CASE
          WHEN (A.StartDate BETWEEN @dtETCDate AND @dtScopeBeforeDate)
          THEN A.StartDate
          ELSE @dtETCDate
        END, 
        CASE
          WHEN (A.EndDate BETWEEN @dtETCDate AND @dtScopeBeforeDate)
          THEN A.EndDate
          ELSE @dtScopeBeforeDate
        END, 
        @strCompany
        )
      END AS BeforeWD,
      CASE
      WHEN (A.EndDate <= @dtScopeEndDate)
      THEN 0
      ELSE
        dbo.DLTK$NumWorkingDays(
        CASE
          WHEN (A.StartDate >= @dtScopeAfterDate)
          THEN A.StartDate
          ELSE @dtScopeAfterDate
        END, 
        A.EndDate,
        @strCompany
        )
      END AS AfterWD,
      CASE HardBooked
      WHEN 'Y' THEN 1
      WHEN 'N' THEN -1
      END AS HardBookedFlg,
      AT.OutlineNumber AS OutlineNumber
      FROM RPAssignment AS A
      INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
      WHERE
      A.PlanID LIKE ISNULL(@strPlanID, '%') AND
      ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') AND 
      ISNULL(A.GenericResourceID, '|') = ISNULL( @strGenericResourceID, '|')
  END

  SELECT @intTaskCount = COUNT(DISTINCT TaskID) FROM @tabAssignment

  if @intTaskCount = 0 AND @bitUseJTDData = 1 
  BEGIN
  -- Put all JTD records for the employee into the temparary table
  INSERT @tabLDEmployee (
    WBS1,
    WBS2,
    WBS3,
    LaborCode,
    Employee
  )
    SELECT DISTINCT
      WBS1,
      WBS2,
      WBS3,
      LaborCode,
      Employee
      FROM LD    
      WHERE  Employee = @strResourceID AND LD.TransDate < = @dtJTDDate
    UNION
    SELECT DISTINCT
      TD.WBS1,
      TD.WBS2,
      TD.WBS3,
      TD.LaborCode,
      TD.Employee
      FROM tkDetail AS TD
        INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate AND TD.EmployeeCompany = TM.EmployeeCompany )
      WHERE TD.Employee = @strResourceID AND TD.TransDate < = @dtJTDDate
    UNION
    SELECT DISTINCT
      TD.WBS1,
      TD.WBS2,
      TD.WBS3,
      TD.LaborCode,
      TD.Employee
      FROM tsDetail AS TD
        INNER JOIN tsControl AS TC ON TD.Batch = TC.Batch AND TC.Posted = 'N'
        WHERE TD.Employee = @strResourceID AND TD.TransDate < = @dtJTDDate

  -- check if any JTD for any plan
    Select @intTaskCount = Count (*) From @tabLDEmployee LD
    INNER JOIN RPTask AS T ON LD.WBS1  = T.WBS1   AND LD.WBS2 = ISNULL(T.WBS2, ' ') AND LD.WBS3 = ISNULL(T.WBS3, ' ')
  LEFT JOIN RPAssignment AS A ON LD.WBS1 = A.WBS1 AND LD.WBS2 = ISNULL(A.WBS2, ' ') AND LD.WBS3 = ISNULL(A.WBS3, ' ')  AND LD.Employee = A.ResourceID   
                                 AND A.PlanID = T.PlanID    AND A.TaskID = T.TaskID
    WHERE  T.EndDate >= @dtETCDate   AND A.AssignmentID IS NULL 
END
 
  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  IF (@intOutlineLevel = 0)
    BEGIN
      INSERT @tabTask(
        PlanID,
        TaskID,
        Name,
        WBS1,
        WBS2,
        WBS3,
        LaborCode,
        Company,
        VersionID,
        StartDate,
        EndDate,
        T_Status,
        ChargeType,
        OutlineNumber,
        OutlineLevel,
        ChildrenCount,
        TopWBS1,
        TopName,
        TopKonaSpace,
        PMFullName,
        ClientName,
        ContractStartDate,
        ContractEndDate,
    UtilizationscheduleFlg,
        ASG_HardBooked,
        ASG_BeforeWD,
        ASG_AfterWD,
        ASG_StartDate,
        ASG_EndDate,
        Org,
        OrgName
      )
        SELECT DISTINCT
          A.PlanID AS PlanID,
          PT.TaskID AS TaskID,
          Max(CASE WHEN PT.LaborCode IS NOT NULL THEN L.LaborCodeName ELSE PT.NAME END) AS NAME,
          PT.WBS1 AS WBS1,
          PT.WBS2 AS WBS2,
          PT.WBS3 AS WBS3,
          PT.LaborCode AS LaborCode,
          RP.Company AS Company,
          ISNULL(RP.VersionID, '') AS VersionID,
          PT.StartDate AS StartDate,
          PT.EndDate AS EndDate,
          PT.Status AS T_Status,
          PT.ChargeType AS ChargeType,
          PT.OutlineNumber AS OutlineNumber,
          PT.OutlineLevel AS OutlineLevel,
          PT.ChildrenCount AS ChildrenCount,
          PT.WBS1 AS TopWBS1,
          PT.Name AS TopName,
          ISNULL(PAD.KonaSpace, 0) AS TopKonaSpace,
          CONVERT(nvarchar(255), ISNULL(ISNULL(PM.PreferredName, PM.FirstName), '') + ISNULL((' ' + PM.LastName), '') + ISNULL(', ' + PMSuffix.Suffix, '')) AS PMFullName,
          ISNULL(CL.Name, '') AS ClientName,
          PR.StartDate AS ContractStartDate,
          COALESCE(PR.EndDate, PR.ActCompletionDate, PR.EstCompletionDate) AS ContractEndDate,
      PR.UtilizationscheduleFlg AS UtilizationscheduleFlg,
          CASE SIGN(MIN(A.HardBookedFlg) + MAX(A.HardBookedFlg))
            WHEN 0 THEN 'B'
            WHEN 1 THEN 'Y'
            WHEN -1 THEN 'N'
          END AS ASG_HardBooked,
          SUM(A.BeforeWD) AS ASG_BeforeWD,
          SUM(A.AfterWD) AS ASG_AfterWD,
          MIN(A.StartDate) AS ASG_StartDate,
          MAX(A.EndDate) AS ASG_EndDate,
          ISNULL(ORG.Org, '') AS Org,
          ISNULL(ORG.Name, '') AS OrgName
          FROM @tabAssignment AS A
            INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID
            INNER JOIN RPPlan AS RP	ON RP.PlanID = PT.PlanID
            LEFT JOIN EM AS PM ON PT.ProjMgr = PM.Employee
            LEFT JOIN CFGSuffix PMSuffix ON PMSuffix.Code = PM.Suffix
            LEFT JOIN CL ON PT.ClientID = CL.ClientID
            LEFT JOIN PR AS PR ON PT.WBS1 = PR.WBS1 AND ISNULL(PT.WBS2, ' ') = PR.WBS2 AND ISNULL(PT.WBS3, ' ') = PR.WBS3
            LEFT JOIN PRAdditionalData AS PAD ON PT.WBS1 = PAD.WBS1 AND ISNULL(PT.WBS2, ' ') = PAD.WBS2 AND ISNULL(PT.WBS3, ' ') = PAD.WBS3
            LEFT JOIN Organization ORG ON ORG.Org = PT.Org
			CROSS APPLY dbo.stRP$tabLaborCode(PT.PlanID,PT.LaborCode) AS L
          WHERE A.OutlineNumber LIKE PT.OutlineNumber + '%' AND PT.OutlineLevel = 0
          GROUP BY A.PlanID, PT.TaskID, PT.Name, PT.WBS1,PT.WBS2,PT.WBS3, PT.LaborCode, PT.StartDate, PT.EndDate, PT.Status, PT.ChargeType, PT.OutlineNumber, PT.OutlineLevel, PT.ChildrenCount, 
            PAD.KonaSpace, PM.PreferredName, PM.FirstName, PM.LastName, PMSuffix.Suffix, CL.Name, RP.Company, 
            PR.StartDate, PR.EndDate, PR.UtilizationscheduleFlg,PR.ActCompletionDate, PR.EstCompletionDate, RP.VersionID, ORG.Org, ORG.Name

     
    END /* IF (@intOutlineLevel = 0) */
  ELSE
    BEGIN

      IF (@strMode = 'C')
        BEGIN

          INSERT @tabTask(
            PlanID,
            TaskID,
            Name,
            WBS1,
            WBS2,
            WBS3,
            LaborCode,
            Company,
            VersionID,
            StartDate,
            EndDate,
            T_Status,
            ChargeType,
            OutlineNumber,
            OutlineLevel,
            ChildrenCount,
            TopWBS1,
            TopName,
            TopKonaSpace,
            PMFullName,
            ClientName,
            ContractStartDate,
            ContractEndDate,
      UtilizationscheduleFlg,
            ASG_HardBooked,
            ASG_BeforeWD,
            ASG_AfterWD,
            ASG_StartDate,
            ASG_EndDate,
            Org,
            OrgName
          )
            SELECT DISTINCT
              A.PlanID AS PlanID,
              CT.TaskID AS TaskID,
			  Max(CASE WHEN CT.LaborCode IS NOT NULL THEN L.LaborCodeName ELSE CT.NAME END) AS NAME,
              CT.WBS1 AS WBS1,
              CT.WBS2 AS WBS2,
              CT.WBS3 AS WBS3,
              CT.LaborCode AS LaborCode,
              RP.Company AS Company,
              ISNULL(RP.VersionID, '') AS VersionID,
              CT.StartDate AS StartDate,
              CT.EndDate AS EndDate,
              CT.Status AS T_Status,
              CT.ChargeType AS ChargeType,
              CT.OutlineNumber AS OutlineNumber,
              CT.OutlineLevel AS OutlineLevel,
              CT.ChildrenCount AS ChildrenCount,
              TT.WBS1 AS TopWBS1,
              TT.Name AS TopName,
              ISNULL(PAD.KonaSpace, 0) AS TopKonaSpace,
              CONVERT(nvarchar(255), ISNULL(ISNULL(PM.PreferredName, PM.FirstName), '') + ISNULL((' ' + PM.LastName), '') + ISNULL(', ' + PMSuffix.Suffix, '')) AS PMFullName,
              ISNULL(CL.Name, '') AS ClientName,
              CTPR.StartDate AS ContractStartDate,
              COALESCE(CTPR.EndDate, CTPR.ActCompletionDate, CTPR.EstCompletionDate) AS ContractEndDate,
        CTPR.UtilizationscheduleFlg AS UtilizationscheduleFlg,
              CASE SIGN(MIN(A.HardBookedFlg) + MAX(A.HardBookedFlg))
                WHEN 0 THEN 'B'
                WHEN 1 THEN 'Y'
                WHEN -1 THEN 'N'
              END AS ASG_HardBooked,
              SUM(A.BeforeWD) AS ASG_BeforeWD,
              SUM(A.AfterWD) AS ASG_AfterWD,
              MIN(A.StartDate) AS ASG_StartDate,
              MAX(A.EndDate) AS ASG_EndDate,
              ISNULL(ORG.Org, '') As Org,
              ISNULL(ORG.Name, '') As OrgName
              FROM @tabAssignment AS A
                INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID 
                INNER JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber
                INNER JOIN RPTask AS TT ON CT.PlanID = TT.PlanID AND TT.OutlineLevel = 0
                INNER JOIN RPPlan AS RP	ON RP.PlanID = PT.PlanID
                LEFT JOIN EM AS PM ON CT.ProjMgr = PM.Employee
                LEFT JOIN CFGSuffix PMSuffix ON PMSuffix.Code = PM.Suffix
                LEFT JOIN CL ON TT.ClientID = CL.ClientID
                LEFT JOIN PR AS CTPR ON CT.WBS1 = CTPR.WBS1 AND ISNULL(CT.WBS2, ' ') = CTPR.WBS2 AND ISNULL(CT.WBS3, ' ') = CTPR.WBS3
                LEFT JOIN PRAdditionalData AS PAD ON TT.WBS1 = PAD.WBS1 AND ISNULL(TT.WBS2, ' ') = PAD.WBS2 AND ISNULL(TT.WBS3, ' ') = PAD.WBS3
                LEFT JOIN Organization ORG ON ORG.Org = PT.Org
				CROSS APPLY dbo.stRP$tabLaborCode(CT.PlanID,CT.LaborCode) AS L
              WHERE PT.TaskID = @strTaskID AND CT.OutlineLevel = @intOutlineLevel 
                AND A.OutlineNumber LIKE PT.OutlineNumber + '%' AND A.OutlineNumber LIKE CT.OutlineNumber + '%' 
              GROUP BY A.PlanID, CT.TaskID, CT.Name, CT.WBS1, CT.WBS2, CT.WBS3, CT.LaborCode, CT.StartDate, CT.EndDate, CT.Status, CT.ChargeType, CT.OutlineNumber, CT.OutlineLevel, CT.ChildrenCount, 
                TT.WBS1, TT.Name, PAD.KonaSpace, PM.PreferredName, PM.FirstName, PM.LastName, PMSuffix.Suffix, CL.Name, RP.Company,
                CTPR.StartDate, CTPR.EndDate, CTPR.ActCompletionDate, CTPR.EstCompletionDate, CTPR.UtilizationscheduleFlg,RP.VersionID, ORG.Org, ORG.Name
        END /* END IF (@strMode = 'C') */

      ELSE IF (@strMode = 'S')
        BEGIN

          INSERT @tabTask(
            PlanID,
            TaskID,
            Name,
            WBS1,
            WBS2,
            WBS3,
            LaborCode,
            Company,
            VersionID,
            StartDate,
            EndDate,
            T_Status,
            ChargeType,
            OutlineNumber,
            OutlineLevel,
            ChildrenCount,
            TopWBS1,
            TopName,
            TopKonaSpace,
            PMFullName,
            ClientName,
            ContractStartDate,
            ContractEndDate,
      UtilizationscheduleFlg,
            ASG_HardBooked,
            ASG_BeforeWD,
            ASG_AfterWD,
            ASG_StartDate,
            ASG_EndDate,
            Org,
            OrgName
          )
            SELECT DISTINCT
              A.PlanID AS PlanID,
              PT.TaskID AS TaskID,
              Max(CASE WHEN PT.LaborCode IS NOT NULL THEN L.LaborCodeName ELSE PT.NAME END) AS NAME,
              PT.WBS1 AS WBS1,
              PT.WBS2 AS WBS2,
              PT.WBS3 AS WBS3,
              PT.LaborCode AS LaborCode,
              RP.Company AS Company,
              ISNULL(RP.VersionID, '') AS VersionID,
              PT.StartDate AS StartDate,
              PT.EndDate AS EndDate,
              PT.Status AS T_Status,
              PT.ChargeType AS ChargeType,
              PT.OutlineNumber AS OutlineNumber,
              PT.OutlineLevel AS OutlineLevel,
              PT.ChildrenCount AS ChildrenCount,
              TT.WBS1 AS TopWBS1,
              TT.Name AS TopName,
              ISNULL(PAD.KonaSpace, 0) AS TopKonaSpace,
              CONVERT(nvarchar(255), ISNULL(ISNULL(PM.PreferredName, PM.FirstName), '') + ISNULL((' ' + PM.LastName), '') + ISNULL(', ' + PMSuffix.Suffix, '')) AS PMFullName,
              ISNULL(CL.Name, '') AS ClientName,
              PTPR.StartDate AS ContractStartDate,
              COALESCE(PTPR.EndDate, PTPR.ActCompletionDate, PTPR.EstCompletionDate) AS ContractEndDate,
        PTPR.UtilizationscheduleFlg AS UtilizationscheduleFlg,
              CASE SIGN(MIN(A.HardBookedFlg) + MAX(A.HardBookedFlg))
                WHEN 0 THEN 'B'
                WHEN 1 THEN 'Y'
                WHEN -1 THEN 'N'
              END AS ASG_HardBooked,
              SUM(A.BeforeWD) AS ASG_BeforeWD,
              SUM(A.AfterWD) AS ASG_AfterWD,
              MIN(A.StartDate) AS ASG_StartDate,
              MAX(A.EndDate) AS ASG_EndDate,
              ISNULL(ORG.Org, '') AS Org,
              ISNULL(ORG.Name, '') AS OrgName
              FROM @tabAssignment AS A
                INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID 
                INNER JOIN RPTask AS TT ON PT.PlanID = TT.PlanID AND TT.OutlineLevel = 0
                INNER JOIN RPPlan AS RP	ON RP.PlanID = PT.PlanID
                LEFT JOIN EM AS PM ON PT.ProjMgr = PM.Employee
                LEFT JOIN CFGSuffix PMSuffix ON PMSuffix.Code = PM.Suffix
                LEFT JOIN CL ON TT.ClientID = CL.ClientID
                LEFT JOIN PR AS PTPR ON PT.WBS1 = PTPR.WBS1 AND ISNULL(PT.WBS2, ' ') = PTPR.WBS2 AND ISNULL(PT.WBS3, ' ') = PTPR.WBS3
                LEFT JOIN PRAdditionalData AS PAD ON TT.WBS1 = PAD.WBS1 AND ISNULL(TT.WBS2, ' ') = PAD.WBS2 AND ISNULL(TT.WBS3, ' ') = PAD.WBS3
                LEFT JOIN Organization ORG ON ORG.Org = PT.Org
				CROSS APPLY dbo.stRP$tabLaborCode(PT.PlanID,PT.LaborCode) AS L
              WHERE PT.TaskID = @strTaskID AND PT.OutlineLevel = @intOutlineLevel 
                AND A.OutlineNumber LIKE PT.OutlineNumber + '%' 
              GROUP BY A.PlanID, PT.TaskID, PT.Name, PT.WBS1, PT.WBS2, PT.WBS3, PT.LaborCode, PT.StartDate, PT.EndDate, PT.Status, PT.ChargeType, PT.OutlineNumber, PT.OutlineLevel, PT.ChildrenCount, 
                TT.WBS1, TT.Name, PAD.KonaSpace, PM.PreferredName, PM.FirstName, PM.LastName, PMSuffix.Suffix, CL.Name, RP.Company,
                PTPR.StartDate, PTPR.EndDate, PTPR.ActCompletionDate, PTPR.EstCompletionDate,PTPR.UtilizationscheduleFlg , RP.VersionID, ORG.Org, ORG.Name
        END  /* END ELSE IF (@strMode = 'S') */

    END /* ELSE (@intOutlineLevel > 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collecting Labor Time-Phased Data to be used in subsequent calculations.
  -- The TPD records come from only the Assignments included in @tabAssignment.

  INSERT @tabLabTPD(
    TimePhaseID,
    PlanID,
    TaskID,
    AssignmentID,
    OutlineNumber,
    ChargeType,
    StartDate,
    EndDate,
  UtilizationScheduleFlg ,
    PlannedHrs
  )
    SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID,
      TPD.TaskID AS TaskID,
      TPD.AssignmentID AS AssignmentID,
      T.OutlineNumber AS OutlineNumber,
      T.ChargeType AS ChargeType,
      TPD.StartDate AS StartDate,
      TPD.EndDate AS EndDate,
    IsNull (PR.UtilizationScheduleFlg,'N') As UtilizationScheduleFlg ,
      TPD.PeriodHrs AS PlannedHrs
      FROM @tabAssignment AS A
        INNER JOIN RPTask AS T ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
        INNER JOIN RPPlannedLabor AS TPD ON
          A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID AND
          TPD.PeriodHrs <> 0 AND TPD.AssignmentID IS NOT NULL 
    Left  Join PR On PR.WBS1 = T.WBS1 AND PR.WBS2 = ' '
   
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collecting Baseline Labor Time-Phased Data to be used in subsequent calculations.
  -- The TPD records come from only the Assignments included in @tabAssignment.

  INSERT @tabLabBaselineTPD(
    TimePhaseID,
    PlanID,
    TaskID,
    AssignmentID,
    OutlineNumber,
    StartDate,
    EndDate,
    PlannedHrs
  )
    SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID,
      TPD.TaskID AS TaskID,
      TPD.AssignmentID AS AssignmentID,
      T.OutlineNumber AS OutlineNumber,
      TPD.StartDate AS StartDate,
      TPD.EndDate AS EndDate,
      TPD.PeriodHrs AS PlannedHrs
      FROM @tabAssignment AS A
        INNER JOIN RPTask AS T ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
        INNER JOIN RPBaselineLabor AS TPD ON
          A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID AND
          TPD.PeriodHrs <> 0 AND TPD.AssignmentID IS NOT NULL 
   
--+++++

  -- Calculate Selected TPD (i.e. Planned Hrs in forecast range) 
  -- GROUP BY PlanID, OutlineNumber, ChargeType

  INSERT @tabSelectedTPD(
    PlanID,
    OutlineNumber,
    ChargeType,
  UtilizationScheduleFlg,
    PlannedHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
      TPD.ChargeType,
    TPD.UtilizationScheduleFlg,
      ROUND(ISNULL(SUM(
        CASE
          WHEN TPD.StartDate >= @dtScopeStartDate
          THEN 
            CASE
              WHEN TPD.EndDate <= @dtScopeEndDate
              THEN TPD.PlannedHrs
              ELSE TPD.PlannedHrs * dbo.DLTK$ProrateRatio(TPD.StartDate, @dtScopeEndDate, TPD.StartDate, TPD.EndDate, RPPlan.Company)
            END
          ELSE
            CASE
              WHEN TPD.EndDate <= @dtScopeEndDate
              THEN TPD.PlannedHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, RPPlan.Company)
              ELSE TPD.PlannedHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, @dtScopeEndDate, TPD.StartDate, TPD.EndDate, RPPlan.Company)
            END
        END
      ), 0), @siHrDecimals) AS PlannedHrs
      FROM @tabLabTPD AS TPD INNER JOIN RPPlan ON RPPlan.PlanID = TPD.PlanID
      WHERE TPD.StartDate <= @dtScopeEndDate AND TPD.EndDate >= @dtScopeStartDate 
      GROUP BY TPD.PlanID, OutlineNumber, ChargeType,UtilizationScheduleFlg

--+++++

  -- Calculate Scheduled TPD (i.e. Planned Hrs in forecast range and also witn utilization flag = S or Y) 
  -- GROUP BY PlanID, OutlineNumber, ChargeType

  INSERT @tabScheduledTPD(
    PlanID,
    OutlineNumber,
    ChargeType,
  UtilizationScheduleFlg,
    PlannedHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
      TPD.ChargeType,
    TPD.UtilizationScheduleFlg AS  UtilizationScheduleFlg,
      ROUND(ISNULL(SUM(
        CASE
          WHEN TPD.StartDate >= @dtScopeStartDate
          THEN 
            CASE
              WHEN TPD.EndDate <= @dtScopeEndDate
              THEN TPD.PlannedHrs
              ELSE TPD.PlannedHrs * dbo.DLTK$ProrateRatio(TPD.StartDate, @dtScopeEndDate, TPD.StartDate, TPD.EndDate, RPPlan.Company)
            END
          ELSE
            CASE
              WHEN TPD.EndDate <= @dtScopeEndDate
              THEN TPD.PlannedHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, RPPlan.Company)
              ELSE TPD.PlannedHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, @dtScopeEndDate, TPD.StartDate, TPD.EndDate, RPPlan.Company)
            END
        END
      ), 0), @siHrDecimals) AS PlannedHrs
      FROM @tabLabTPD AS TPD INNER JOIN RPPlan ON RPPlan.PlanID = TPD.PlanID
      WHERE TPD.StartDate <= @dtScopeEndDate AND TPD.EndDate >= @dtScopeStartDate AND (TPD.UtilizationScheduleFlg = 'S' OR TPD.UtilizationScheduleFlg = 'Y')
      GROUP BY TPD.PlanID, OutlineNumber, ChargeType,UtilizationScheduleFlg

--+++++

  -- Calculate Billable TPD (i.e. Billable Hrs in forecast range) 
  -- GROUP BY PlanID, OutlineNumber

  INSERT @tabBillableTPD(
    PlanID,
    OutlineNumber,
    UtilizationScheduleFlg,
    PlannedHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
    TPD.UtilizationScheduleFlg AS UtilizationScheduleFlg,
      ISNULL(SUM(TPD.PlannedHrs), 0) AS PlannedHrs
      FROM @tabSelectedTPD AS TPD
      WHERE TPD.ChargeType = 'R' AND TPD.UtilizationScheduleFlg = 'Y' 
      GROUP BY PlanID, OutlineNumber,UtilizationScheduleFlg

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collecting ETC Time-Phased Data to be used in subsequent calculations.

  INSERT @tabETCTPD(
    TimePhaseID,
    PlanID,
    TaskID,
    AssignmentID,
    OutlineNumber,
    StartDate,
    EndDate,
    PeriodHrs
  )
    SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID,
      TPD.TaskID AS TaskID,
      TPD.AssignmentID AS AssignmentID,
      TPD.OutlineNumber AS OutlineNumber,
      CASE WHEN TPD.StartDate >= @dtETCDate THEN TPD.StartDate ELSE @dtETCDate END AS StartDate,
      TPD.EndDate AS EndDate,
      ROUND(ISNULL(
        CASE 
          WHEN TPD.StartDate >= @dtETCDate 
          THEN TPD.PlannedHrs
          ELSE TPD.PlannedHrs * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) 
        END 
      , 0), @siHrDecimals) AS PeriodHrs
      FROM @tabLabTPD AS TPD
      WHERE TPD.PlannedHrs <> 0 AND TPD.EndDate >= @dtETCDate 

--+++++

  -- Calculate Total ETC
  -- GROUP BY PlanID, OutlineNumber

  INSERT @tabTotalETC(
    PlanID,
    OutlineNumber,
    ETCHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
      ISNULL(SUM(TPD.PeriodHrs), 0) AS ETCHrs
      FROM @tabETCTPD AS TPD
      GROUP BY PlanID, OutlineNumber

--+++++

  -- Calculate Before ETC
  -- GROUP BY PlanID, OutlineNumber

  INSERT @tabBeforeETC(
    PlanID,
    OutlineNumber,
    ETCHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
      ROUND(ISNULL(SUM(
        CASE
          WHEN TPD.EndDate <= @dtScopeBeforeDate
          THEN TPD.PeriodHrs
          ELSE TPD.PeriodHrs * dbo.DLTK$ProrateRatio(TPD.StartDate, @dtScopeBeforeDate, TPD.StartDate, TPD.EndDate, @strCompany)
        END
      ), 0), @siHrDecimals) AS ETCHrs
      FROM @tabETCTPD AS TPD
      WHERE TPD.StartDate <= @dtScopeBeforeDate
      GROUP BY PlanID, OutlineNumber

--+++++

  -- Calculate After ETC
  -- GROUP BY PlanID, OutlineNumber

  INSERT @tabAfterETC(
    PlanID,
    OutlineNumber,
    ETCHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
      ROUND(ISNULL(SUM(
        CASE
          WHEN TPD.StartDate >= @dtScopeAfterDate
          THEN TPD.PeriodHrs
          ELSE TPD.PeriodHrs * dbo.DLTK$ProrateRatio(@dtScopeAfterDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)
        END
      ), 0), @siHrDecimals) AS ETCHrs
      FROM @tabETCTPD AS TPD
      WHERE TPD.EndDate >= @dtScopeAfterDate
      GROUP BY PlanID, OutlineNumber

--+++++

  -- Calculate Selected ETC
  -- GROUP BY PlanID, OutlineNumber

  INSERT @tabSelectedETC(
    PlanID,
    OutlineNumber,
    ETCHrs
  )
    SELECT
      TPD.PlanID,
      TPD.OutlineNumber,
      ROUND(ISNULL(SUM(
        CASE
          WHEN TPD.StartDate >= @dtScopeStartDate
          THEN 
            CASE
              WHEN TPD.EndDate <= @dtScopeEndDate
              THEN TPD.PeriodHrs
              ELSE TPD.PeriodHrs * dbo.DLTK$ProrateRatio(TPD.StartDate, @dtScopeEndDate, TPD.StartDate, TPD.EndDate, @strCompany)
            END
          ELSE
            CASE
              WHEN TPD.EndDate <= @dtScopeEndDate
              THEN TPD.PeriodHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)
              ELSE TPD.PeriodHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, @dtScopeEndDate, TPD.StartDate, TPD.EndDate, @strCompany)
            END
        END
      ), 0), @siHrDecimals) AS ETCHrs
      FROM @tabETCTPD AS TPD
      WHERE TPD.StartDate <= @dtScopeEndDate AND TPD.EndDate >= @dtScopeStartDate
      GROUP BY PlanID, OutlineNumber

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Calculate @decScheduledPct, @decUtilizationPct, @strUtilRatioFlg for the Resource row.

  IF (@intOutlineLevel < 0 AND @strResourceType = 'E')
    BEGIN

      SELECT @decScheduledPct =
        CASE
          WHEN @decAvailableHrs > 0
          THEN ROUND(((XSP.SelectedPlannedHrs / @decAvailableHrs) * 100), @siHrDecimals) 
          ELSE 0
        END
        FROM (
          SELECT 
            ROUND(ISNULL(SUM(PlannedHrs), 0.0000), @siHrDecimals) AS SelectedPlannedHrs 
            FROM @tabScheduledTPD XSP 
        ) AS XSP

      SELECT @decUtilizationPct =
        CASE
          WHEN @decAvailableHrs > 0 
          THEN ROUND(((XSB.SelectedBillableHrs / @decAvailableHrs) * 100), @siHrDecimals) 
          ELSE 0
        END
        FROM (
          SELECT 
            ROUND(ISNULL(SUM(PlannedHrs), 0.0000), @siHrDecimals) AS SelectedBillableHrs 
            FROM @tabBillableTPD XSB 
        ) AS XSB

      SELECT @strUtilRatioFlg = 
        CASE
          WHEN @decUtilizationPct <= @decURUnderLimit THEN 'U'
          WHEN @decUtilizationPct > @decUROverLimit THEN 'O'
          ELSE 'P'
        END

    END
  ELSE
    BEGIN
      SELECT @decScheduledPct = 0
      SELECT @decUtilizationPct = 0
      SELECT @strUtilRatioFlg = ''
    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save Labor JTD & Unposted Labor JTD for this Resource into temp table.
  -- The JTD data are posted at the leaf node in the WBS Tree. 
  -- Therefore, we need to calculate JTD for the summary levels.
    if @bitUseJTDData = 1 AND (@strMode = 'C' Or (@strMode = 'S' And @intOutlineLevel >= 0))
  BEGIN
    INSERT @tabJTDLD(
      WBS1,
      WBS2,
      WBS3,
      LaborCode,	  
      PeriodHrs,
      JTDLabCost,
      JTDLabBill
    )      
      SELECT
        WBS1,
        WBS2,
        WBS3,
        LaborCode,
        SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,
        SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS JTDLabCost,
        SUM(LD.BillExt) AS JTDLabBill
      FROM LD
      WHERE LD.TransDate <= @dtJTDDate AND LD.Employee = @strResourceID
      GROUP BY WBS1, WBS2, WBS3, LaborCode    

      UNION ALL
      SELECT
        WBS1,
        WBS2,
        WBS3,
        LaborCode,
        SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,
        SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS JTDLabCost,
        SUM(TD.BillExt) AS JTDLabBill
      FROM tkDetail AS TD 
      INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate AND TD.EmployeeCompany = TM.EmployeeCompany)
      WHERE  TD.Employee = @strResourceID AND TD.TransDate <= @dtJTDDate AND TD.Employee = @strResourceID
      GROUP BY WBS1, WBS2, WBS3, LaborCode    
  
      UNION ALL
       SELECT
        WBS1,
        WBS2,
        WBS3, 
        LaborCode,
        SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs,
        SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS JTDLabCost,
        SUM(TD.BillExt) AS JTDLabBill
      FROM tsDetail AS TD
      INNER JOIN tsControl AS TC ON (TD.Batch = TC.Batch AND TC.Posted = 'N')  
      WHERE  TD.Employee = @strResourceID AND TD.TransDate <= @dtJTDDate AND TD.Employee = @strResourceID
      GROUP BY WBS1, WBS2, WBS3, LaborCode    


    INSERT @tabLD(
      PlanID,
      OutlineNumber,
      PeriodHrs,
      JTDLabCost,
      JTDLabBill
    )      
		SELECT
        T.PlanID AS PlanID,
        T.OutlineNumber AS OutlineNumber,
        SUM(LD.PeriodHrs) AS PeriodHrs,
        SUM(LD.JTDLabCost) AS JTDLabCost,
        SUM(LD.JTDLabBill) AS JTDLabBill
      FROM @tabJTDLD LD
        INNER JOIN RPTask AS T ON LD.WBS1 = T.WBS1
            AND LD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
            AND LD.WBS3 LIKE (ISNULL(T.WBS3 + '%', '%'))
            AND ISNULL(LD.LaborCode, '%') LIKE ISNULL(T.LaborCode, '%') 
			AND T.ChildrenCount = 0
      GROUP BY T.PlanID, T.OutlineNumber
  END    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Combine various calculations and return the end result.
  -- CASE WHEN
  --   @intOutlineLevel = -1: The return data is for the Resource row. 
  --   @intOutlineLevel >= 0: The return data are for the WBS rows.

  IF (@intOutlineLevel = -1)
    BEGIN

      SELECT @strASG_HardBooked =
        CASE SIGN(MIN(A.HardBookedFlg) + MAX(A.HardBookedFlg))
          WHEN 0 THEN 'B'
          WHEN 1 THEN 'Y'
          WHEN -1 THEN 'N'
        END
        FROM @tabAssignment AS A

      INSERT @tabResources(
        RowID,
        ParentRowID,
        RowLevel,
        HasChildren,
        SelectFlag,
        Name,
        BillableFlg,
        MatchPct,
        StartDate,
        EndDate,
        AvailableHrs,
        PlannedHrs,
        BaselineHrs,
        SelectedPlannedHrs,
    SelectedScheduledHrs,
        SelectedBillableHrs,
        ScheduledPct,
        TargetUtilization,
        UtilizationPct,
        SelectedETCHrs,
        ETCHrs,
        JTDHrs,
        EACHrs,
        HasPhoto,
		PhotoModDate,
        BeforeETCHrs,
        AfterETCHrs,
        UtilRatioFlg,
        PlanID,
        TaskID, 
        AssignmentID,
        ResourceID,
        GenericResourceID,
        WBS1,
        WBS2,
        WBS3,
        LaborCode,
        Company,
        VersionID,
        OutlineNumber,
        TopWBS1,
        TopName,
        TopKonaSpace,
        PMFullName,
        ClientName,
        RT_Status,
        R_Status,
        HardBooked,
        HoursPerDay,
        SumBeforeWD,
        SumAfterWD,
        ContractStartDate,
        ContractEndDate,
    UtilizationscheduleFlg,
        MinASGDate,
        MaxASGDate,
        MinTPDDate,
        MaxTPDDate,
        Org,
        OrgName
      )
        SELECT
          @strRowID AS RowID,
          '' AS ParentRowID,
          0 AS RowLevel,
          CASE WHEN @intTaskCount > 0 THEN CONVERT(bit, 1) ELSE CONVERT(bit, 0) END AS HasChildren,
          'N' AS SelectFlag,
          @strResourceName AS Name,
          '' AS BillableFlg,
          0 AS MatchPct,
          NULL AS StartDate,
          NULL AS EndDate,
          @decAvailableHrs AS AvailableHrs,
          XE.PlannedHrs AS PlannedHrs,
          XE.BaselineHrs AS BaselineHrs,
          XE.SelectedPlannedHrs AS SelectedPlannedHrs,
      XE.ScheduledPlannedHrs AS ScheduledPlannedHrs,
          XE.SelectedBillableHrs AS SelectedBillableHrs,
          @decScheduledPct AS ScheduledPct,
          @decUtilizationRatio AS TargetUtilization,
          @decUtilizationPct AS UtilizationPct,
          XE.SelectedETCHrs AS SelectedETCHrs,
          XE.ETCHrs AS ETCHrs,
          XE.JTDHrs AS JTDHrs,
          XE.JTDHrs + XE.ETCHrs AS EACHrs,
          @bitHasPhoto AS HasPhoto,
		  @dtPhotoModDate as PhotoModDate,
          XE.BeforeETCHrs AS BeforeETCHrs,
          XE.AfterETCHrs AS AfterETCHrs,
          @strUtilRatioFlg AS UtilRatioFlg,
          NULL AS PlanID,
          NULL AS TaskID,
          NULL AS AssignmentID,
          @strResourceID AS ResourceID,
          @strGenericResourceID AS GenericResourceID,
          NULL AS WBS1,
          NULL AS WBS2,
          NULL AS WBS3,
          NULL AS LaborCode,
          NULL AS Company,
          NULL AS VersionID,
          NULL AS OutlineNumber,
          NULL AS TopWBS1,
          NULL AS TopName,
          0 AS TopKonaSpace,
          NULL AS PMFullName,
          NULL AS ClientName,
          ISNULL(@strR_Status, '') AS RT_Status,
          ISNULL(@strR_Status, '') AS R_Status,
          @strASG_HardBooked AS HardBooked,
          @decHoursPerDay AS HoursPerDay,
          0 AS SumBeforeWD,
          0 AS SumAfterWD,
          NULL AS ContractStartDate,
          NULL AS ContractEndDate,
      NULL AS UtilizationscheduleFlg,
          NULL AS MinASGDate,
          NULL AS MaxASGDate,
          NULL AS MinTPDDate,
          NULL AS MaxTPDDate,
          NULL AS Org,
          NULL AS OrgName
          FROM (
            SELECT
              ROUND(ISNULL(SUM(TETCHrs), 0.0000), @siHrDecimals) AS ETCHrs,
              ROUND(ISNULL(SUM(BETCHrs), 0.0000), @siHrDecimals) AS BeforeETCHrs,
              ROUND(ISNULL(SUM(AETCHrs), 0.0000), @siHrDecimals) AS AfterETCHrs,
              ROUND(ISNULL(SUM(SETCHrs), 0.0000), @siHrDecimals) AS SelectedETCHrs,
              ROUND(ISNULL(SUM(PTPDHrs), 0.0000), @siHrDecimals) AS PlannedHrs,
              ROUND(ISNULL(SUM(STPDHrs), 0.0000), @siHrDecimals) AS SelectedPlannedHrs,
              ROUND(ISNULL(SUM(SCHTPDHrs), 0.0000), @siHrDecimals) AS ScheduledPlannedHrs,
              ROUND(ISNULL(SUM(BTPDHrs), 0.0000), @siHrDecimals) AS SelectedBillableHrs,
              ROUND(ISNULL(SUM(JTDHrs), 0.0000), @siHrDecimals) AS JTDHrs,
              ROUND(ISNULL(SUM(BaselineHrs), 0.0000), @siHrDecimals) AS BaselineHrs
             FROM (
                SELECT 
                  SUM(ETCHrs) AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                  0 AS PTPDHrs, 0 AS STPDHrs, 0 AS SCHTPDHrs, 0 AS BTPDHrs,
                  0 AS JTDHrs, 0 AS BaselineHrs
                  FROM @tabTotalETC
                UNION ALL
                SELECT 
                  0 AS TETCHrs, SUM(ETCHrs) AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                  0 AS PTPDHrs, 0 AS STPDHrs, 0 AS SCHTPDHrs, 0 AS BTPDHrs,
                  0 AS JTDHrs, 0 AS BaselineHrs
                  FROM @tabBeforeETC
                UNION ALL
                SELECT 
                  0 AS TETCHrs, 0 AS BETCHrs, SUM(ETCHrs) AS AETCHrs, 0 AS SETCHrs,
                  0 AS PTPDHrs, 0 AS STPDHrs, 0 AS SCHTPDHrs, 0 AS BTPDHrs,
                  0 AS JTDHrs, 0 AS BaselineHrs
                  FROM @tabAfterETC
                UNION ALL
                SELECT 
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, SUM(ETCHrs) AS SETCHrs,
                  0 AS PTPDHrs, 0 AS STPDHrs, 0 AS SCHTPDHrs, 0 AS BTPDHrs,
                  0 AS JTDHrs, 0 AS BaselineHrs
                  FROM @tabSelectedETC
                UNION ALL
                SELECT 
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                  SUM(PlannedHrs) AS PTPDHrs, 0 AS STPDHrs, 0 AS SCHTPDHrs, 0 AS BTPDHrs,
                  0 AS JTDHrs, 0 AS BaselineHrs
                  FROM @tabLabTPD
                UNION ALL
                SELECT 
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                  0 AS PTPDHrs, 0 AS STPDHrs, 0 AS SCHTPDHrs, 0 AS BTPDHrs,
                  0 AS JTDHrs, SUM(PlannedHrs) AS BaselineHrs
                  FROM @tabLabBaselineTPD
                UNION ALL
                SELECT 
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                  0 AS PTPDHrs, SUM(PlannedHrs) AS STPDHrs, 0 AS SCHTPDHrs, 0 AS BTPDHrs,
                  0 AS JTDHrs, 0 AS BaselineHrs
                  FROM @tabSelectedTPD
                UNION ALL
                SELECT 
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                  0 AS PTPDHrs, 0 AS STPDHrs, SUM(PlannedHrs) AS SCHTPDHrs, 0 AS BTPDHrs,
                  0 AS JTDHrs, 0 AS BaselineHrs
                  FROM @tabScheduledTPD
                UNION ALL
                SELECT 
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                  0 AS PTPDHrs, 0 AS STPDHrs, 0 AS SCHTPDHrs, SUM(PlannedHrs) AS BTPDHrs,
                  0 AS JTDHrs, 0 AS BaselineHrs
                  FROM @tabBillableTPD
                UNION ALL
                SELECT 
                  0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                  0 AS PTPDHrs, 0 AS STPDHrs, 0 AS SCHTPDHrs, 0 AS BTPDHrs, SUM(PeriodHrs) AS JTDHrs, 0 AS BaselineHrs
                  FROM @tabLD
              ) AS YE
          ) AS XE

    END /* IF (@intOutlineLevel = -1) */
  ELSE IF (@intOutlineLevel >= 0)
    BEGIN

      INSERT @tabResources(
        RowID,
        ParentRowID,
        RowLevel,
        HasChildren,
        SelectFlag,
        Name,
        BillableFlg,
        MatchPct,
        StartDate,
        EndDate,
        AvailableHrs,
        PlannedHrs,
        BaselineHrs,
        SelectedPlannedHrs,
    SelectedScheduledHrs,
        SelectedBillableHrs,
        ScheduledPct,
        TargetUtilization,
        UtilizationPct,
        SelectedETCHrs,
        ETCHrs,
        JTDHrs,
        EACHrs,
        HasPhoto,
		PhotoModDate,
        BeforeETCHrs,
        AfterETCHrs,
        UtilRatioFlg,
        PlanID,
        TaskID,
        AssignmentID,
        ResourceID,
        GenericResourceID,
        WBS1,
        WBS2,
        WBS3,
        LaborCode,
        Company,
        VersionID,
        OutlineNumber,
        TopWBS1,
        TopName,
        TopKonaSpace,
        PMFullName,
        ClientName,
        RT_Status,
        R_Status,
        HardBooked,
        HoursPerDay,
        SumBeforeWD,
        SumAfterWD,
        ContractStartDate,
        ContractEndDate,
    UtilizationscheduleFlg,
        MinASGDate,
        MaxASGDate,
        MinTPDDate,
        MaxTPDDate,
        Org,
        OrgName
      )
        SELECT
          @strIDPrefix + '|' + CT.TaskID AS RowID,
          @strParentRowID AS ParentRowID,
          (CT.OutlineLevel + 1) AS RowLevel,
          CASE WHEN CT.ChildrenCount > 0 THEN CONVERT(bit, 1) ELSE CONVERT(bit, 0) END AS HasChildren,
          '' AS SelectFlag,
          CT.Name AS Name,
          CASE WHEN CT.ChargeType = 'R' THEN 'Y' ELSE 'N' END AS BillableFlg,
          0 AS MatchPct,
          CT.StartDate AS StartDate,
          CT.EndDate AS EndDate,
          0 AS AvailableHrs,
          XE.PlannedHrs AS PlannedHrs,
          XE.BaselineHrs AS BaselineHrs,
          XE.SelectedPlannedHrs AS SelectedPlannedHrs,
      XE.ScheduledPlannedHrs AS ScheduledPlannedHrs,
          XE.SelectedBillableHrs AS SelectedBillableHrs,
          0 AS ScheduledPct,
          0 AS TargetUtilization,
          0 AS UtilizationPct,
          XE.SelectedETCHrs AS SelectedETCHrs,
          XE.ETCHrs AS ETCHrs,
          XE.JTDHrs AS JTDHrs,
          XE.JTDHrs + XE.ETCHrs AS EACHrs,
          0 AS HasPhoto,
		  NULL as PhotoModDate,
          XE.BeforeETCHrs AS BeforeETCHrs,
          XE.AfterETCHrs AS AfterETCHrs,
          '' AS UtilRatioFlg,
          CT.PlanID AS PlanID,
          CT.TaskID AS TaskID,
          NULL AS AssignmentID,
          @strResourceID AS ResourceID,
          @strGenericResourceID AS GenericResourceID,
          CT.WBS1 AS WBS1,
          CT.WBS2 AS WBS2,
          CT.WBS3 AS WBS3,
          CT.LaborCode AS LaborCode,
          CT.Company AS Company,
          CT.VersionID AS VersionID,
          CT.OutlineNumber AS OutlineNumber,
          CT.TopWBS1 AS TopWBS1,
          CT.TopName AS TopName,
          CT.TopKonaSpace AS TopKonaSpace,
          CT.PMFullName AS PMFullName,
          CT.ClientName AS ClientName,
          ISNULL(CT.T_Status, '') AS RT_Status,
          ISNULL(@strR_Status, '') AS R_Status,
          CT.ASG_HardBooked AS HardBooked,
          0 AS HoursPerDay,
          CT.ASG_BeforeWD AS SumBeforeWD,
          CT.ASG_AfterWD AS SumAfterWD,
          CT.ContractStartDate AS ContractStartDate,
          CT.ContractEndDate AS ContractEndDate,
      CT.UtilizationScheduleFlg AS UtilizationscheduleFlg,
          CT.ASG_StartDate AS MinASGDate,
          CT.ASG_EndDate AS MaxASGDate,
          XE.MinTPDDate AS MinTPDDate,
          XE.MaxTPDDate AS MaxTPDDate,
          CT.Org,
          CT.OrgName
          FROM @tabTask AS CT
            LEFT JOIN (
              SELECT
                XT.PlanID AS PlanID,
                XT.OutlineNumber AS OutlineNumber,
                MIN(YD.MinTPDDate) AS MinTPDDate,
                MAX(YD.MaxTPDDate) AS MaxTPDDate,
                ROUND(ISNULL(SUM(TETCHrs), 0.0000), @siHrDecimals) AS ETCHrs,
                ROUND(ISNULL(SUM(BETCHrs), 0.0000), @siHrDecimals) AS BeforeETCHrs,
                ROUND(ISNULL(SUM(AETCHrs), 0.0000), @siHrDecimals) AS AfterETCHrs,
                ROUND(ISNULL(SUM(SETCHrs), 0.0000), @siHrDecimals) AS SelectedETCHrs,
                ROUND(ISNULL(SUM(PTPDHrs), 0.0000), @siHrDecimals) AS PlannedHrs,
                ROUND(ISNULL(SUM(STPDHrs), 0.0000), @siHrDecimals) AS SelectedPlannedHrs,
                ROUND(ISNULL(SUM(SCHTPDHrs), 0.0000), @siHrDecimals) AS ScheduledPlannedHrs,
                ROUND(ISNULL(SUM(BTPDHrs), 0.0000), @siHrDecimals) AS SelectedBillableHrs,
                ROUND(ISNULL(SUM(JTDHrs), 0.0000), @siHrDecimals) AS JTDHrs,
                ROUND(ISNULL(SUM(BaselineHrs), 0.0000), @siHrDecimals) AS BaselineHrs
                FROM @tabTask AS XT

                  LEFT JOIN (
                    SELECT
                      PlanID AS PlanID,
                      OutlineNumber AS OutlineNumber,
                      ISNULL(SUM(TETCHrs), 0.0000) AS TETCHrs,
                      ISNULL(SUM(BETCHrs), 0.0000) AS BETCHrs,
                      ISNULL(SUM(AETCHrs), 0.0000) AS AETCHrs,
                      ISNULL(SUM(SETCHrs), 0.0000) AS SETCHrs,
                      ISNULL(SUM(PTPDHrs), 0.0000) AS PTPDHrs,
                      ISNULL(SUM(STPDHrs), 0.0000) AS STPDHrs,
                      ISNULL(SUM(SCHTPDHrs), 0.0000) AS SCHTPDHrs,
                      ISNULL(SUM(BTPDHrs), 0.0000) AS BTPDHrs,
                      ISNULL(SUM(JTDHrs), 0.0000) AS JTDHrs,
                      ISNULL(SUM(BaselineHrs), 0.0000) AS BaselineHrs
                      FROM (
                        SELECT
                          PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                          ETCHrs AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                          0 AS PTPDHrs, 0 AS STPDHrs, 0 AS SCHTPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                          FROM @tabTotalETC
                        UNION ALL
                        SELECT 
                          PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                          0 AS TETCHrs, ETCHrs AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                          0 AS PTPDHrs, 0 AS STPDHrs, 0 AS SCHTPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                          FROM @tabBeforeETC
                        UNION ALL
                        SELECT 
                          PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                          0 AS TETCHrs, 0 AS BETCHrs, ETCHrs AS AETCHrs, 0 AS SETCHrs, 
                          0 AS PTPDHrs, 0 AS STPDHrs, 0 AS SCHTPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                          FROM @tabAfterETC
                        UNION ALL
                        SELECT 
                          PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                          0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, ETCHrs AS SETCHrs, 
                          0 AS PTPDHrs, 0 AS STPDHrs, 0 AS SCHTPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                          FROM @tabSelectedETC
                        UNION ALL
                        SELECT 
                          PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                          0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                          PlannedHrs AS PTPDHrs, 0 AS STPDHrs, 0 AS SCHTPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                          FROM @tabLabTPD
                        UNION ALL
                        SELECT 
                          PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                          0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs,
                          0 AS PTPDHrs, 0 AS STPDHrs, 0 AS SCHTPDHrs, 0 AS BTPDHrs,
                          0 AS JTDHrs, PlannedHrs AS BaselineHrs
                          FROM @tabLabBaselineTPD
                        UNION ALL
                        SELECT 
                          PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                          0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                          0 AS PTPDHrs, PlannedHrs AS STPDHrs, 0 AS SCHTPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                          FROM @tabSelectedTPD
                        UNION ALL
                        SELECT 
                          PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                          0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                          0 AS PTPDHrs, 0 AS STPDHrs, PlannedHrs AS SCHTPDHrs, 0 AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                          FROM @tabScheduledTPD
                        UNION ALL
                        SELECT 
                          PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                          0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                          0 AS PTPDHrs, 0 AS STPDHrs, 0 AS SCHTPDHrs, PlannedHrs AS BTPDHrs, 0 AS JTDHrs, 0 AS BaselineHrs
                          FROM @tabBillableTPD
                        UNION ALL
                        SELECT 
                          PlanID AS PlanID, OutlineNumber AS OutlineNumber,
                          0 AS TETCHrs, 0 AS BETCHrs, 0 AS AETCHrs, 0 AS SETCHrs, 
                          0 AS PTPDHrs, 0 AS STPDHrs, 0 AS SCHTPDHrs, 0 AS BTPDHrs, PeriodHrs as JTDHrs, 0 AS BaselineHrs
                          FROM @tabLD
                      ) AS ZE
                    GROUP BY ZE.PlanID, ZE.OutlineNumber
                  ) AS YE
                    ON XT.PlanID = YE.PlanID AND YE.OutlineNumber LIKE XT.OutlineNumber + '%'

                  LEFT JOIN (
                      SELECT
                        T.PlanID AS PlanID,
                        T.OutlineNumber AS OutlineNumber,
                        MIN(MinTPDDate) AS MinTPDDate,
                        MAX(MaxTPDDate) AS MaxTPDDate
                      FROM @tabTask AS T
                        LEFT JOIN (
                        SELECT
                          PlanID AS PlanID,
                          OutlineNumber AS OutlineNumber,
                          MIN(StartDate) AS MinTPDDate,
                          MAX(EndDate) AS MaxTPDDate
                        FROM @tabLabTPD AS TPD
                        GROUP BY TPD.PlanID, TPD.OutlineNumber
                      ) AS TPD
                      ON T.PlanID = TPD.PlanID AND TPD.OutlineNumber LIKE T.OutlineNumber + '%'
                      GROUP BY T.PlanID, T.OutlineNumber
                  ) AS YD
                    ON XT.PlanID = YD.PlanID AND YD.OutlineNumber LIKE XT.OutlineNumber + '%'

                GROUP BY XT.PlanID, XT.OutlineNumber

            ) AS XE
              ON CT.PlanID = XE.PlanID AND CT.OutlineNumber = XE.OutlineNumber

    END /* ELSE (@intOutlineLevel >= 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
