SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_Compensation] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS money 
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select dbo.fnCCG_EI_Compensation('2003005.xx',' ',' ')
	select dbo.fnCCG_EI_Compensation('2003005.xx','1PD',' ')
	select dbo.fnCCG_EI_Compensation('2003005.xx','1PD','COD')
*/
	declare @res money
	select @res = IsNull(Fee + ConsultFee + ReimbAllow,0) from PR where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3
	if @res=0 set @res=null
	return @res
END
GO
