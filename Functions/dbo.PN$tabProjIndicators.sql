SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$tabProjIndicators]
  (@strWBS1 Nvarchar(30),
   @strJTDDate VARCHAR(8)) -- Date must be in format: 'yyyymmdd' regardless of UI Culture.

  RETURNS @tabIndicators TABLE (
    FeeLabCost decimal(19,4),
    FeeLabBill decimal(19,4),
    FeeExpCost decimal(19,4),
    FeeExpBill decimal(19,4),
    FeeConCost decimal(19,4),
    FeeConBill decimal(19,4),
    EACLabCost decimal(19,4),
    EACLabBill decimal(19,4),
    EACExpCost decimal(19,4),
    EACExpBill decimal(19,4),
    EACConCost decimal(19,4),
    EACConBill decimal(19,4),
	  ProfitLabCost decimal(19,4),
	  EACLabMult decimal(19,4),
	  LabTargetMult decimal(19,4),
	  BudgetType varchar(1) COLLATE database_default,
	  CostCurrencyCode varchar(3) COLLATE database_default,
	  BillingCurrencyCode varchar(3) COLLATE database_default
  )

BEGIN

  DECLARE @decFeeLabCost decimal(19,4)
  DECLARE @decFeeLabBill decimal(19,4)
  DECLARE @decFeeExpCost decimal(19,4)
  DECLARE @decFeeExpBill decimal(19,4)
  DECLARE @decFeeConCost decimal(19,4)
  DECLARE @decFeeConBill decimal(19,4)
 
  DECLARE @decEACLabCost decimal(19,4)
  DECLARE @decEACLabBill decimal(19,4)
  DECLARE @decEACExpCost decimal(19,4)
  DECLARE @decEACExpBill decimal(19,4)
  DECLARE @decEACConCost decimal(19,4)
  DECLARE @decEACConBill decimal(19,4)

  DECLARE @decTargetMultCost decimal(19,4)
  DECLARE @decEACMultCost decimal(19,4)
  DECLARE @decEACProfit decimal(19,4)

  DECLARE @strBudgetType varchar(1)
	DECLARE @strCostCurrencyCode varchar(3)
	DECLARE @strBillingCurrencyCode varchar(3)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @decFeeLabCost = 0.0000
  SET @decFeeLabBill = 0.0000
  SET @decFeeExpCost = 0.0000
  SET @decFeeExpBill = 0.0000
  SET @decFeeConCost = 0.0000
  SET @decFeeConBill = 0.0000
 
  SET @decEACLabCost = 0.0000
  SET @decEACLabBill = 0.0000
  SET @decEACExpCost = 0.0000
  SET @decEACExpBill = 0.0000
  SET @decEACConCost = 0.0000
  SET @decEACConBill = 0.0000

  SET @decTargetMultCost = 0.0000
  SET @decEACMultCost = 0.0000
  SET @decEACProfit = 0.0000

  SET @strBudgetType = ''
	SET @strCostCurrencyCode = ''
	SET @strBillingCurrencyCode = ''

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT
    @decFeeLabCost = ISNULL(FeeLabCost, 0.0000),
    @decFeeLabBill = ISNULL(FeeLabBill, 0.0000),
    @decEACLabCost = ISNULL(EACLabCost, 0.0000),
    @decEACLabBill = ISNULL(EACLabBill, 0.0000),
    @decTargetMultCost = ISNULL(TargetMultCost, 0.0000),
    @decEACMultCost = ISNULL(EACMultCost, 0.0000),
    @decEACProfit = ISNULL(EACProfit, 0.0000),
    @strBudgetType = ISNULL(BudgetType, ''),
    @strCostCurrencyCode = ISNULL(CostCurrencyCode, ''),
    @strBillingCurrencyCode = ISNULL(BillingCurrencyCode, '')
    FROM dbo.PN$tabTask(@strWBS1, @strJTDDate)
    WHERE WBS2 = ' ' AND WBS3 = ' '

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT
    @decFeeExpCost = ISNULL(FeeExpCost, 0.0000),
    @decFeeExpBill = ISNULL(FeeExpBill, 0.0000),
    @decEACExpCost = ISNULL(ETCExpCost, 0.0000) + ISNULL(JTDExpCost, 0.0000),
    @decEACExpBill = ISNULL(ETCExpBill, 0.0000) + ISNULL(JTDExpBill, 0.0000)
    FROM dbo.PN$tabExpTask(@strWBS1, @strJTDDate)
    WHERE WBS2 = ' ' AND WBS3 = ' '

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT
    @decFeeConCost = ISNULL(FeeConCost, 0.0000),
    @decFeeConBill = ISNULL(FeeConBill, 0.0000),
    @decEACConCost = ISNULL(ETCConCost, 0.0000) + ISNULL(JTDConCost, 0.0000),
    @decEACConBill = ISNULL(ETCConBill, 0.0000) + ISNULL(JTDConBill, 0.0000)
    FROM dbo.PN$tabConTask(@strWBS1, @strJTDDate)
    WHERE WBS2 = ' ' AND WBS3 = ' '

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabIndicators (
    FeeLabCost,
    FeeLabBill,
    FeeExpCost,
    FeeExpBill,
    FeeConCost,
    FeeConBill,
    EACLabCost,
    EACLabBill,
    EACExpCost,
    EACExpBill,
    EACConCost,
    EACConBill,
    ProfitLabCost,
    EACLabMult,
    LabTargetMult,
    BudgetType,
    CostCurrencyCode,
    BillingCurrencyCode
  )
    SELECT
      ISNULL(@decFeeLabCost, 0.0000) AS FeeLabCost,
      ISNULL(@decFeeLabBill, 0.0000) AS FeeLabBill,
      ISNULL(@decFeeExpCost, 0.0000) AS FeeExpCost,
      ISNULL(@decFeeExpBill, 0.0000) AS FeeExpBill,
      ISNULL(@decFeeConCost, 0.0000) AS FeeConCost,
      ISNULL(@decFeeConBill, 0.0000) AS FeeConBill,
      ISNULL(@decEACLabCost, 0.0000) AS EACLabCost,
      ISNULL(@decEACLabBill, 0.0000) AS EACLabBill,
      ISNULL(@decEACExpCost, 0.0000) AS EACExpCost,
      ISNULL(@decEACExpBill, 0.0000) AS EACExpBill,
      ISNULL(@decEACConCost, 0.0000) AS EACConCost,
      ISNULL(@decEACConBill, 0.0000) AS EACConBill,
      ISNULL(@decEACProfit, 0.0000) AS ProfitLabCost,
      ISNULL(@decEACMultCost, 0.0000) AS EACLabMult,
      ISNULL(@decTargetMultCost, 0.0000) AS LabTargetMult,
      ISNULL(@strBudgetType, 0.0000) AS BudgetType,
      ISNULL(@strCostCurrencyCode, 0.0000) AS CostCurrencyCode,
      ISNULL(@strBillingCurrencyCode, 0.0000) AS BillingCurrencyCode

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
