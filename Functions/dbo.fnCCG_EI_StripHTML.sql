SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_StripHTML]( @text varchar(max) ) returns varchar(max) as
BEGIN
    DECLARE @textXML xml
    DECLARE @result varchar(max)
    set @textXML = REPLACE(REPLACE(@text, '&', ''), '<BR>', '<BR/>');
    with doc(contents) as
    (
        select chunks.chunk.query('.') from @textXML.nodes('/') as chunks(chunk)
    )
    SELECT @result = contents.value('.', 'varchar(max)') from doc
    RETURN @result
END
GO
