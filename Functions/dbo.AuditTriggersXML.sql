SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[AuditTriggersXML]() RETURNS XML AS
BEGIN

DECLARE @XML AS XML

SET @XML = CONVERT(XML, 
'<root>
<insert>
	<columnTemplate>
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, ''INSERT'', {1}, ''{0}'', NULL, CONVERT(NVARCHAR(2000),[{0}],121), @source, @app
		FROM INSERTED
	</columnTemplate>
	<codeColumnTemplate>
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, ''INSERT'', {1}, ''{0}'', NULL, CONVERT(NVARCHAR(2000),INSERTED.[{0}],121), NULL, {3}, @source, @app
		FROM INSERTED LEFT JOIN {2} AS newDesc ON INSERTED.{0} = newDesc.{4}
	</codeColumnTemplate>
	<skipColumnTemplate>
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, ''INSERT'', {2}, ''{0}'', NULL, ''[{1}]'', @source, @app
		FROM INSERTED
	</skipColumnTemplate>
</insert>
<delete>
	<columnTemplate>
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, ''DELETE'', {1}, ''{0}'', CONVERT(NVARCHAR(2000),[{0}],121), NULL, @source, @app
		FROM DELETED
	</columnTemplate>
	<codeColumnTemplate>
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, ''DELETE'', {1}, ''{0}'', CONVERT(NVARCHAR(2000),DELETED.[{0}],121), NULL, {3}, NULL, @source, @app
		FROM DELETED LEFT JOIN {2} AS oldDesc ON DELETED.{0} = oldDesc.{4}
	</codeColumnTemplate>
	<skipColumnTemplate>
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, ''DELETE'', {2}, ''{0}'', ''[{1}]'', NULL, @source, @app
		FROM DELETED
	</skipColumnTemplate>
	<deleteAfterStart>
		DECLARE @noAuditDetails varchar(1)
		SET @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		IF EXISTS(SELECT AuditKeyValuesDelete FROM FW_CFGSystem WHERE AuditKeyValuesDelete = ''Y'' and @noAuditDetails = ''Y'')
		BEGIN
		DECLARE @placeholder varchar(1)
		END
		ELSE
		BEGIN
	</deleteAfterStart>
</delete>
<update>
	<beforeCodeColumnTemplate>
		IF UPDATE([{0}])
		BEGIN
		INSERT
		INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, ''UPDATE'', {1}, ''{0}'',
		CONVERT(NVARCHAR(2000),DELETED.[{0}],121),
		CONVERT(NVARCHAR(2000),INSERTED.[{0}],121),
		{2}, {3}, @source, @app
		FROM INSERTED JOIN DELETED ON
	</beforeCodeColumnTemplate>
	<beforeColumnTemplate>
		IF UPDATE([{0}])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, ''UPDATE'', {1}, ''{0}'',
		CONVERT(NVARCHAR(2000),DELETED.[{0}],121),
		CONVERT(NVARCHAR(2000),INSERTED.[{0}],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	</beforeColumnTemplate>
	<afterCodeColumnTemplate>
		(
			(
				INSERTED.[{0}] IS NULL AND
				DELETED.[{0}] IS NOT NULL
			) OR
			(
				INSERTED.[{0}] IS NOT NULL AND
				DELETED.[{0}] IS NULL
			) OR
			(
				INSERTED.[{0}] !=
				DELETED.[{0}]
			)
		) LEFT JOIN {1} AS oldDesc ON DELETED.{0} = oldDesc.{2} LEFT JOIN {1} AS newDesc ON INSERTED.{0} = newDesc.{2}
		END
	</afterCodeColumnTemplate>
	<afterColumnTemplate>
		(
			(
				INSERTED.[{0}] IS NULL AND
				DELETED.[{0}] IS NOT NULL
			) OR
			(
				INSERTED.[{0}] IS NOT NULL AND
				DELETED.[{0}] IS NULL
			) OR
			(
				INSERTED.[{0}] !=
				DELETED.[{0}]
			)
		) 
		END		
	</afterColumnTemplate>
	<skipColumnTemplate>
		IF UPDATE([{0}])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, ''UPDATE'', {2}, ''{0}'',
		''[{1}]'',
		''[{1}]'', @source, @app
		FROM INSERTED
		END
	</skipColumnTemplate>
</update>
<shared>
	<beforeStartScript>
		CREATE TABLE #AuditTemp (TriggerName varchar(500), TriggerDisabled varchar(1))

		INSERT INTO #AuditTemp 
		SELECT name, OBJECTPROPERTY(id, ''ExecIsTriggerDisabled'')
		FROM sys.objects AS triggers 
		WHERE name LIKE ''VisionAudit[_]%'' AND type = ''TR'' 
	</beforeStartScript>
	<afterEndScript>
		DROP TABLE #AuditTemp
	</afterEndScript>
	<afterAddTrigger>
		IF (SELECT TriggerDisabled FROM #AuditTemp WHERE TriggerName = ''VisionAudit_{1}_{0}'') = 1 
		BEGIN
		ALTER TABLE {0} DISABLE TRIGGER VisionAudit_{1}_{0}
		END
	</afterAddTrigger>
	<deleteTrigger>
		If EXISTS (SELECT [name] FROM sys.objects WHERE object_id = OBJECT_ID(''dbo.VisionAudit_{1}_{0}'') AND type = ''TR'')
		BEGIN
		DROP TRIGGER dbo.VisionAudit_{1}_{0}
		END
	</deleteTrigger>
	<ICModules>
		IF @app NOT IN (''Clients'',''Vendors'',''Projects'',''Opportunities'',''MktCampaigns'',''Leads'',''Contacts'',''Equipments'',''Employees'') AND @app NOT LIKE ''UDIC_%''
		RETURN
	</ICModules>	
	<start>
		CREATE TRIGGER dbo.VisionAudit_{1}_{0}
		ON [{0}]
		FOR {1}
		NOT FOR REPLICATION
		AS BEGIN
		SET NOCOUNT ON
		DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
		SET @VisionAuditUser = dbo.FW_GetUsername()
		SET @source = dbo.GetVisionAuditSource()
		SET @app = (SELECT TOP 1 lastapp FROM FW_Useractivity WHERE userid = @VisionAuditUser ORDER BY lastaccess DESC)

		IF @VisionAuditUser = ''''
		RETURN

		SET @now = dbo.GetVisionAuditTime()
		SET @table = ''{0}''
	</start>
	<end>
		SET NOCOUNT OFF 
		END
	</end>
</shared>
</root>')
	
RETURN @XML
END
GO
