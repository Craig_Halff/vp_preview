SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   FUNCTION [dbo].[fnCCG_PAT_GetE] (@s Nvarchar(max))
RETURNS Nvarchar(max)
WITH ENCRYPTION
BEGIN
	RETURN Convert(Nvarchar(max),EncryptByPassPhrase(N'CCG_PAT_123', @s),2)
END
GO
