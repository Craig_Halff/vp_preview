SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DLTK$IntervalStart]
  (@dtCurrDate AS datetime, 
   @strScale AS char)
  RETURNS datetime
BEGIN

  -- When period scale and periods per year is not 13 then use months, else use 28 day periods
  If (@strScale = 'p')
	BEGIN
	  IF ((SELECT PeriodsPerYear FROM FW_CFGSystem) <> 13) 
		SET @strScale = 'm'
	END

  RETURN(
  
    CASE

      -- Month.
      WHEN @strScale = 'm' 
        THEN CONVERT(datetime, CONVERT(varchar, YEAR(@dtCurrDate)) + '-' + CONVERT(varchar, MONTH(@dtCurrDate)) + '-01')

      -- Year.
      WHEN @strScale = 'y' 
        THEN CONVERT(datetime, CONVERT(varchar, (YEAR(@dtCurrDate) + 1)) + '-01-01')

      -- Fiscal period with periods per year = 13 then 28 day periods, which would start 27 days before the end date provided
      WHEN @strScale = 'p'
        THEN DATEADD(day, -27, @dtCurrDate)

    END -- End Case
  
  ) -- End Return

END -- fn_DLTK$IntervalStart
GO
