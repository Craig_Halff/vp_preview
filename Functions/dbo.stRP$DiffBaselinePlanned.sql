SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$DiffBaselinePlanned](
  @strPlanID varchar(32)
)
  RETURNS bit
BEGIN -- Function stRP$DiffBaselinePlanned

  DECLARE @bitDiff bit

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Comparing differences between Planned vs. Baseline.
  -- PNPlannedLabor vs. PNBaselineLabor
  -- PNPlannedExpenses vs. PNBaselineExpenses
  -- PNPlannedConsultant vs. PNBaselineConsultant

  SELECT @bitDiff = CASE WHEN SUM(DX) > 0 THEN 1 ELSE 0 END FROM ( /* Z */

    /* Labor TPD */
    SELECT CASE WHEN EXISTS ( /* DX */
      SELECT MIN(PorB) AS PorB FROM ( /* LTPD */
        SELECT 
          0 AS PorB,
          TaskID, AssignmentID, StartDate, EndDate, PeriodHrs
          FROM PNPlannedLabor AS N WHERE PlanID = @strPlanID AND AssignmentID IS NOT NULL
        UNION ALL
        SELECT
          1 AS PorB,
          TaskID, AssignmentID, StartDate, EndDate, PeriodHrs
          FROM PNBaselineLabor AS V WHERE PlanID = @strPlanID AND AssignmentID IS NOT NULL
        ) AS LTPD
        GROUP BY
          TaskID, AssignmentID, StartDate, EndDate, PeriodHrs
        HAVING COUNT(*) = 1
      ) THEN 1 ELSE 0 END AS DX

    UNION ALL /* Expense TPD */
    SELECT CASE WHEN EXISTS ( /* DX */
      SELECT MIN(PorB) AS PorB FROM ( /* ETPD */
        SELECT 
          0 AS PorB,
          TaskID, ExpenseID, StartDate, EndDate, PeriodCost, PeriodBill
          FROM PNPlannedExpenses AS N WHERE PlanID = @strPlanID AND ExpenseID IS NOT NULL
        UNION ALL
        SELECT
          1 AS PorB,
          TaskID, ExpenseID, StartDate, EndDate, PeriodCost, PeriodBill
          FROM PNBaselineExpenses AS V WHERE PlanID = @strPlanID AND ExpenseID IS NOT NULL
        ) AS ETPD
        GROUP BY
          TaskID, ExpenseID, StartDate, EndDate, PeriodCost, PeriodBill
        HAVING COUNT(*) = 1
      ) THEN 1 ELSE 0 END AS DX

    UNION ALL /* Consultant TPD */
    SELECT CASE WHEN EXISTS ( /* DX */
      SELECT MIN(PorB) AS PorB FROM ( /* CTPD */
        SELECT 
            0 AS PorB,
            TaskID, ConsultantID, StartDate, EndDate, PeriodCost, PeriodBill
            FROM PNPlannedConsultant AS N WHERE PlanID = @strPlanID AND ConsultantID IS NOT NULL
          UNION ALL
          SELECT
            1 AS PorB,
            TaskID, ConsultantID, StartDate, EndDate, PeriodCost, PeriodBill
            FROM PNBaselineConsultant AS V WHERE PlanID = @strPlanID AND ConsultantID IS NOT NULL
          ) AS CTPD
          GROUP BY
            TaskID, ConsultantID, StartDate, EndDate, PeriodCost, PeriodBill
          HAVING COUNT(*) = 1
      ) THEN 1 ELSE 0 END AS DX

  ) AS Z

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN @bitDiff

END -- stRP$DiffBaselinePlanned
GO
