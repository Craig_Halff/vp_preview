SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   FUNCTION [dbo].[fnCCG_SplitString] (@text Nvarchar(max), @delimiter varchar(20) = ';')
RETURNS @Strings TABLE (
	  position    int IDENTITY PRIMARY KEY,
	  value       Nvarchar(max)
)
AS BEGIN
	  declare @index int
	  set @index = -1

	  while (LEN(@text) > 0)
	  begin
			/* Find the first delimiter */
			set @index = CHARINDEX(@delimiter , @text)

			/* No delimiter left?
			 Insert the remaining @text and break the loop */
			if (@index = 0) AND (LEN(@text) > 0)
			begin
				  insert INTO @Strings values (@text)
				  break
			end

			/* Found a delimiter
			 Insert left of the delimiter and truncate the @text */
			if (@index > 1)
			begin
				  insert INTO @Strings values (LEFT(@text, @index - 1))
				  set @text = RIGHT(@text, (LEN(@text) - @index + 1 - len(@delimiter)))
			end
			/* Delimiter is 1st position = no @text to insert */
			else
				  set @text = RIGHT(@text, (LEN(@text) - @index))
	  end
	  return
END
GO
