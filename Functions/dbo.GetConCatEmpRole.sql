SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetConCatEmpRole]
 (@sysBillerLabel Nvarchar(100), 
  @sysPMLabel Nvarchar(100),
  @sysPRLabel Nvarchar(100),
  @sysSPLabel Nvarchar(100),
  @roleString Nvarchar(max)) 
  RETURNS Nvarchar(max)  AS

BEGIN

Declare @roleDescString Nvarchar(max) = ''
Declare @oneRoleString Nvarchar(max) = ''
Declare @oneRoleDesc Nvarchar(max) = ''

SET @roleString = RTRIM(LTRIM(@roleString))
WHILE LEN(@roleString) > 0
	BEGIN
		IF PATINDEX('%|%',@roleString) > 0
			BEGIN
				SET @oneRoleString = SUBSTRING(@roleString, 0, PATINDEX('%|%',@roleString))
				SET @roleString = SUBSTRING(@roleString, LEN(@oneRoleString + '|') + 1, LEN(@roleString))
			END
		ELSE
			BEGIN
				SET @oneRoleString = @roleString
				SET @roleString = ''
			END
    
		IF @oneRoleString = 'sysBill' OR @oneRoleString = 'sysPM' OR @oneRoleString = 'sysPR' OR @oneRoleString = 'sysSP'
			BEGIN
    			IF @oneRoleString = 'sysBill'
					SET @oneRoleDesc = @sysBillerLabel
				ELSE
					IF @oneRoleString = 'sysPM'
						SET @oneRoleDesc = @sysPMLabel
					ELSE			
						IF @oneRoleString = 'sysPR'
							SET @oneRoleDesc = @sysPRLabel
						ELSE
							IF @oneRoleString = 'sysSP'
								SET @oneRoleDesc = @sysSPLabel									
			END
		ELSE
			BEGIN
				SELECT @oneRoleDesc = Label FROM FW_CustomColumnsData ccd,FW_CustomColumnCaptions ccc 
                            where ccd.InfoCenterArea='Projects' and ccd.gridID='X' and DataType='employee'
                            and ccd.InfoCenterArea = ccc.InfoCenterArea and ccd.Name = ccc.Name
                            and ccd.Name = @oneRoleString
			END
    
		SET @roleDescString = CASE WHEN len(@roleDescString)>0 then @roleDescString + ',' + @oneRoleDesc else @oneRoleDesc end
END -- WHILE

RETURN @roleDescString
 		
END -- GetConCatEmpRole
GO
