SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_DraftAmt] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS @T TABLE
(
	ColValue	money,
	ColDetail	varchar(100)	-- Set background color 
) 
AS BEGIN
/*
	Copyright (c) 2016 Central Consulting Group.  All rights reserved.

	select * from dbo.[fnCCG_EI_DraftAmt]('028960.000', ' ', ' ')
*/
	declare @res money

	select @res = IsNull(Sum(FinalAmount), 0)
	from billInvSums
	where BillWBS1=@WBS1 and (isnull(@WBS2,' ')=' ' or @WBS2=BillWBS2) and (isnull(@WBS3,' ')=' ' or @WBS3=BillWBS3)
	  and Invoice = '<Draft>'  and ArrayType = 'I'
	--group by BillWBS1--, BillWBS2, BillWBS3

	if @res = 0
		set @res = null
	insert into @T values (@res, '')
	return
END
GO
