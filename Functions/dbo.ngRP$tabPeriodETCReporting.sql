SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ngRP$tabPeriodETCReporting](
  @strRowID Nvarchar(255),
  @dtStart varchar(8) /* Date must be in format: 'yyyymmdd' regardless of UI Culture */,
  @numPeriods int,
	@strPeriodScale varchar(1),
  @strMode varchar(1) /* S = Self, C = Children */,
  @strResourceSummary varchar(1) /* Y/N */
)
  RETURNS @tabPeriodETC TABLE (
    ResourceID Nvarchar(20) COLLATE database_default,
		TaskID varchar(32),
		StartDate datetime,
		PeriodHrs decimal(19,4),
		ScheduledPct decimal(19,4),
		UtilizationPct decimal(19,4),
		BillableHrs decimal(19,4),
		JTDHrs decimal(19,4),
		NumWorkingDays int,
		ETC varchar(1)
)

BEGIN

/*

Execute dbo.setContextInfo @StrCompany = N'02',@StrAuditingEnabled  = 'Y',@strUserName = N'LVU',@StrCultureName = 'en-US',@StrAuditSource = 'DTK';

SELECT * FROM dbo.ngRP$tabPeriodETC('E~00126|','20151001',12,'M',0)
*/

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	DECLARE @strIncludeJTD varchar(1) = 'Y'
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strResourceID Nvarchar(20)
  DECLARE @strResourceName Nvarchar(255)
  DECLARE @strGenericResourceID Nvarchar(20)
  DECLARE @strTaskID varchar(32)
  DECLARE @strResourceType varchar(1)
  DECLARE @strIDPrefix Nvarchar(255)

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
  DECLARE @dtScopeStartDate datetime
  DECLARE @dtScopeEndDate datetime
  DECLARE @dtScopeBeforeDate datetime
  DECLARE @dtScopeAfterDate datetime
	DECLARE @dtMinEndDate datetime
 
  DECLARE @intHrDecimals int /* CFGRMSettings.HrDecimals */
  DECLARE @intTaskCount int 
	DECLARE @intOutlineLevel int
	DECLARE @intWorkingHoursPerDay int
 
  -- Declare Temp tables.

	DECLARE @tabCalendar TABLE (
		SeqID int,
		StartDate	datetime,
		EndDate	datetime,
		PeriodScale	varchar(1) COLLATE database_default,
		NumWorkingDays int,
		ETC varchar(1)
	)

  DECLARE @tabAssignment TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
		ResourceID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    OutlineNumber varchar(255) COLLATE database_default,
		ChargeType varchar(1) COLLATE database_default
    UNIQUE(PlanID, TaskID, AssignmentID)
  )

  DECLARE @tabTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    Name Nvarchar(255) COLLATE database_default,
    WBS1 Nvarchar(30) COLLATE database_default,
    MA_StartDate datetime,
    MA_EndDate datetime,
    ChargeType varchar(1) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int,
    ChildrenCount int
    UNIQUE(PlanID, OutlineNumber)
  )

  DECLARE @tabSelectedETC TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    StartDate datetime,	
    EndDate datetime,
    PeriodHrs decimal(19,4),
    BillableHrs decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, OutlineNumber, StartDate, EndDate)
  )

	DECLARE @tabPLabTPD TABLE (
		RowID int identity,
		SeqID int,
		TimePhaseID varchar(32) COLLATE database_default,
		CIStartDate datetime, 
		CIEndDate datetime, 
		PlanID varchar(32) COLLATE database_default,
		TaskID varchar(32) COLLATE database_default,
		AssignmentID varchar(32) COLLATE database_default,
		OutlineNumber varchar(255) COLLATE database_default,
		StartDate datetime, 
		EndDate datetime, 
		PeriodHrs decimal(19,4), 
		BillableHrs decimal(19,4), 
		CINumWorkingDays int,
		ETC varchar(1)
    UNIQUE (RowID,PlanID, TaskID, AssignmentID, OutlineNumber, StartDate, EndDate, ETC)
  )

DECLARE @tabLD TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
		PeriodHrs decimal(19,4),
		TransDate datetime
		UNIQUE (PlanID,OutlineNumber,TransDate)
)
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Set Company to non-Multi-Company.
  
  SELECT @strCompany = dbo.GetActiveCompany()

  -- Get RM Settings.
  
  SELECT 
		@dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE()))),
		@intHrDecimals = HrDecimals
    FROM CFGRMSettings
  -- Set Dates
  
  SELECT @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)
  
  -- Calculate calendar
	INSERT @tabCalendar(
	  SeqID,
	  StartDate,
	  EndDate,
	  PeriodScale,
	  NumWorkingDays,
		ETC
	)	
		SELECT 
		  SeqID,
		  StartDate,
		  EndDate,
		  Scale,
		  NumWorkingDays,
			'N' 
		  FROM dbo.ngRP$tabCalendarInterval(@dtStart, @strPeriodScale, @numPeriods)

  SELECT
    @dtScopeStartDate = MIN(StartDate),
    @dtScopeEndDate = MAX(EndDate),
		@dtMinEndDate = MIN(EndDate)
    FROM @tabCalendar

  -- Insert one special calendar row with StartDate = ETCDate 
	IF (@dtETCDate > @dtScopeStartDate)
    BEGIN

			INSERT @tabCalendar(
				SeqID,
				StartDate,
				EndDate,
				PeriodScale,
				NumWorkingDays,
				ETC
			)	
				SELECT
					SeqID,
					@dtETCDate,
					EndDate,
					@strPeriodScale,
					dbo.DLTK$NumWorkingDays(@dtETCDate, EndDate, @strCompany),
					'Y'
				FROM @tabCalendar where @dtETCDate >= StartDate AND @dtETCDate <= EndDate
		END
	  
  SELECT @dtScopeBeforeDate = DATEADD(DAY, -1, @dtScopeStartDate)
  SELECT @dtScopeAfterDate = DATEADD(DAY, 1, @dtScopeEndDate)

  -- Parse @strRowID
  -- RowID is a string with the following format
  --   1. E~<RPAssignment.ResourceID>|<RPTask.TaskID>
  --   2. G~<RPAssignment.GenericResourceID>|<RPTask.TaskID>

  SET @strResourceType = SUBSTRING(@strRowID, 1, 1)
  SET @strIDPrefix = LEFT(@strRowID, CHARINDEX('|', @strRowID) - 1)

  IF (@strResourceType = 'E')
    BEGIN

      SET @strResourceID = REPLACE(@strIDPrefix, 'E~', '')
      SET @strGenericResourceID = NULL

      SELECT 
        @strResourceName = CONVERT(Nvarchar(255), ISNULL(EM.FirstName, '') + ISNULL(' ' + EM.LastName, ''))
        FROM EM 
          LEFT JOIN EMPhoto AS EP ON EM.Employee = EP.Employee
        WHERE EM.Employee = @strResourceID

    END
  ELSE
    BEGIN
      SET @strGenericResourceID = REPLACE(@strIDPrefix, 'G~', '')
      SET @strResourceID = NULL
      SELECT @strResourceName = Name FROM GR WHERE GR.Code = @strGenericResourceID
    END

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

  IF (DATALENGTH(@strTaskID) = 0)
    BEGIN
      SELECT 
        @intOutlineLevel = 
          CASE 
            WHEN @strMode = 'S' THEN -1
            WHEN @strMode = 'C' THEN 0
          END 
    END
  ELSE
    BEGIN
      SELECT 
        @intOutlineLevel = 
          CASE 
            WHEN @strMode = 'S' THEN OutlineLevel
            WHEN @strMode = 'C' THEN OutlineLevel + 1
          END 
        FROM RPTask 
        WHERE TaskID = @strTaskID
    END

	SELECT @intWorkingHoursPerDay = 8

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabAssignment(
    PlanID,
    TaskID,
    AssignmentID,
		ResourceID,
    StartDate,
    EndDate,
    OutlineNumber,
		ChargeType
  )
    SELECT DISTINCT
      A.PlanID,
      A.TaskID,
      A.AssignmentID,
			A.ResourceID,
      A.StartDate,
      A.EndDate,
      AT.OutlineNumber,
			AT.ChargeType
      FROM RPAssignment AS A
        INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
      WHERE
        ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') AND 
        ISNULL(A.GenericResourceID, '|') = ISNULL( @strGenericResourceID, '|') AND
				A.EndDate > @dtScopeStartDate AND
        dbo.DLTK$NumWorkingDays(@dtScopeStartDate, A.EndDate, @strCompany) > 0

    SELECT @intTaskCount = COUNT(DISTINCT TaskID) FROM @tabAssignment

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collecting Labor Time-Phased Data to be used in subsequent calculations.
  -- The TPD records come from only the Assignments included in @tabAssignment.

  -- Calculate Selected ETC

  INSERT @tabSelectedETC(
    TimePhaseID,
    PlanID,
    TaskID,
    AssignmentID,
    OutlineNumber,
    StartDate,
    EndDate,
    PeriodHrs,
		BillableHrs
  )
    SELECT  /* Keep only TPD in the Selected ETC range */
      STPD.TimePhaseID AS TimePhaseID,
      STPD.PlanID AS PlanID,
      STPD.TaskID AS TaskID,
      STPD.AssignmentID AS AssignmentID,
      STPD.OutlineNumber AS OutlineNumber,
      CASE WHEN STPD.StartDate >= @dtScopeStartDate THEN STPD.StartDate ELSE @dtScopeStartDate END AS StartDate,
      CASE WHEN STPD.EndDate <= @dtScopeEndDate THEN STPD.EndDate ELSE @dtScopeEndDate END AS EndDate,
      CASE
        WHEN STPD.StartDate >= @dtScopeStartDate
        THEN 
          CASE
            WHEN STPD.EndDate <= @dtScopeEndDate
            THEN STPD.PeriodHrs
            ELSE STPD.PeriodHrs * dbo.DLTK$ProrateRatio(STPD.StartDate, @dtScopeEndDate, STPD.StartDate, STPD.EndDate, @strCompany)
          END
        ELSE
          CASE
            WHEN STPD.EndDate <= @dtScopeEndDate
            THEN STPD.PeriodHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, STPD.EndDate, STPD.StartDate, STPD.EndDate, @strCompany)
            ELSE STPD.PeriodHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, @dtScopeEndDate, STPD.StartDate, STPD.EndDate, @strCompany)
          END
      END AS PeriodHrs,
			CASE WHEN STPD.BillableHrs = 0 THEN 0 ELSE STPD.PeriodHrs END AS BillableHrs
      FROM (
        SELECT /* Select only TPD in the ETC range */
          ETPD.TimePhaseID AS TimePhaseID,
          ETPD.PlanID AS PlanID,
          ETPD.TaskID AS TaskID,
          ETPD.AssignmentID AS AssignmentID,
          ETPD.OutlineNumber AS OutlineNumber,
          CASE WHEN ETPD.StartDate >= @dtScopeStartDate THEN ETPD.StartDate ELSE @dtScopeStartDate END AS StartDate,
          ETPD.EndDate AS EndDate,
          ISNULL(
            CASE 
              WHEN ETPD.StartDate >= @dtScopeStartDate 
              THEN ETPD.PlannedHrs
	            ELSE ETPD.PlannedHrs * dbo.DLTK$ProrateRatio(@dtScopeStartDate, ETPD.EndDate, ETPD.StartDate, ETPD.EndDate, @strCompany) 
            END 
          , 0) AS PeriodHrs,
					BillableHrs
          FROM (
            SELECT /* TPD for Assignment rows in @tabAssignment */
              TPD.TimePhaseID AS TimePhaseID,
              TPD.PlanID AS PlanID,
              TPD.TaskID AS TaskID,
              TPD.AssignmentID AS AssignmentID,
              T.OutlineNumber AS OutlineNumber,
              TPD.StartDate AS StartDate,
              TPD.EndDate AS EndDate,
              TPD.PeriodHrs AS PlannedHrs,
							CASE WHEN T.ChargeType = 'R' THEN TPD.PeriodHrs ELSE 0 END AS BillableHrs
              FROM @tabAssignment AS A
                INNER JOIN RPTask AS T ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
                INNER JOIN RPPlannedLabor AS TPD ON
                  A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID AND
                  TPD.PeriodHrs <> 0 AND TPD.AssignmentID IS NOT NULL 
          ) AS ETPD
          WHERE ETPD.PlannedHrs <> 0 AND ETPD.EndDate >= @dtScopeStartDate 
      ) AS STPD
      WHERE STPD.StartDate <= @dtScopeEndDate AND STPD.EndDate >= @dtScopeStartDate

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Split up TPD in @tabSelectedETC to match with Calendar Intervals.

  INSERT @tabPLabTPD (
	SeqID,
    TimePhaseID,
    CIStartDate,
		CIEndDate,
    PlanID, 
    TaskID,
    AssignmentID,
    OutlineNumber,
    StartDate, 
    EndDate, 
    PeriodHrs,
		BillableHrs,
		CINumWorkingDays,
		ETC
   )
     SELECT
	  SeqID AS SeqID,
      TimePhaseID AS TimePhaseID,
      CIStartDate AS CIStartDate,
      CIEndDate AS CIEndDate,
      PlanID AS PlanID, 
      TaskID AS TaskID,
      AssignmentID AS AssignmentID,
      OutlineNumber AS OutlineNumber,
      StartDate AS StartDate, 
      EndDate AS EndDate, 
      ROUND(ISNULL(PeriodHrs, 0), @intHrDecimals) AS PeriodHrs,
      ROUND(ISNULL(CASE WHEN BillableHrs = 0 THEN 0 ELSE PeriodHrs END, 0), @intHrDecimals) AS BillableHrs,
			CINumWorkingDays,
			ETC
    FROM (
      SELECT -- For Assignment Rows.
		CI.SeqID AS SeqID,
        CI.StartDate AS CIStartDate, 
        CI.EndDate AS CIEndDate, 
        TPD.TimePhaseID AS TimePhaseID,
        TPD.PlanID AS PlanID, 
        TPD.TaskID AS TaskID,
        TPD.AssignmentID AS AssignmentID,
        TPD.OutlineNumber AS OutlineNumber,
        CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
        CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
        CASE 
          WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
          THEN TPD.PeriodHrs * 
            dbo.DLTK$ProrateRatio(
              CASE 
                WHEN TPD.StartDate > CI.StartDate 
                THEN TPD.StartDate 
                ELSE CI.StartDate 
              END, 
              CASE 
                WHEN TPD.EndDate < CI.EndDate 
                THEN TPD.EndDate 
                ELSE CI.EndDate 
              END, 
              TPD.StartDate, TPD.EndDate,
              @strCompany)             
          ELSE PeriodHrs 
        END AS PeriodHrs,
				BillableHrs,
				CI.NumWorkingDays as CINumWorkingDays,
				CI.ETC
        FROM @tabCalendar AS CI 
          INNER JOIN @tabSelectedETC AS TPD 
            ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
    ) AS X

-- Once everything is done run the periodhrs through the rounding function and update with the rounded values.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

UPDATE @tabPLabTPD SET 
	PeriodHrs = #R1.RndValue,
	BillableHrs = #R1.BillingRndValue
FROM @tabPLabTPD P 
	INNER JOIN (SELECT 
								TimePhaseID, 
								H.RowNo, 
								H.RndValue,
								B.RndValue AS BillingRndValue
							FROM (SELECT 
											TimePhaseID, 
											CONVERT(xml,'<root>' + (SELECT SUM(ISNULL(PeriodHrs,0)) AS UnRndValue, SeqID AS RowNo FROM @tabPLabTPD P WHERE P.TimePhaseID = ETC.TimePhaseID AND ETC = 'N' AND PeriodHrs <> 0 GROUP BY SeqID FOR XML RAW) + '</root>') AS HrsXML,
											CONVERT(xml,'<root>' + (SELECT SUM(ISNULL(BillableHrs,0)) AS UnRndValue, SeqID AS RowNo FROM @tabPLabTPD P WHERE P.TimePhaseID = ETC.TimePhaseID AND ETC = 'N' AND PeriodHrs <> 0 GROUP BY SeqID FOR XML RAW) + '</root>') AS BillableHrsXML
										FROM @tabSelectedETC ETC
								) AS A 
								CROSS APPLY dbo.DLTK$tabRound(HrsXML,@intHrDecimals) AS H  
								CROSS APPLY dbo.DLTK$tabRound(BillableHrsXML,@intHrDecimals) AS B
								WHERE H.RowNo = B.RowNo AND NOT (A.HrsXML IS NULL AND A.BillableHrsXML IS NULL)
							) AS #R1
			ON P.TimePhaseID = #R1.TimePhaseID AND P.SeqID = #R1.RowNo

-- One final update to set the ETC Hrs column.  Use the existing ratio of NumWorkingDays that are already there.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
UPDATE @tabPLabTPD SET
	PeriodHrs = ROUND(ETC.PeriodHrs, @intHrDecimals),
	BillableHrs = ROUND(ETC.BillableHrs, @intHrDecimals)

FROM (SELECT SeqID, CINumWorkingDays, TimePhaseID, CIStartDate, Periodhrs, BillableHrs FROM @tabPLabTPD P WHERE ETC = 'Y') AS ETC 
		INNER JOIN @tabPLabTPD P ON P.SeqID = ETC.SeqID AND P.TimePhaseID = ETC.TimePhaseID AND P.CIStartDate = ETC.CIStartDate

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	-- Save Labor JTD & Unposted Labor JTD for this Resource into temp table.
  -- The JTD data are posted at the leaf node in the WBS Tree. 
  -- Therefore, we need to calculate JTD for the summary levels.

	IF (@strIncludeJTD = 'Y')
	BEGIN
		INSERT @tabLD
			(PlanID,
			 OutlineNumber,
			 TransDate,
			 PeriodHrs)      
			SELECT
				T.PlanID AS PlanID,
				T.OutlineNumber AS OutlineNumber,
				TransDate,
				SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs
			FROM @tabAssignment AS A
				INNER JOIN RPTask AS T
					ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
				INNER JOIN LD
					ON  LD.WBS1 = T.WBS1
					AND LD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
					AND LD.WBS3 LIKE (ISNULL(T.WBS3 + '%', '%'))
			WHERE LD.TransDate <= @dtJTDDate AND LD.Employee = @strResourceID
			GROUP BY T.PlanID, T.OutlineNumber, LD.TransDate
	END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

IF @strMode = 'P'
	BEGIN
		INSERT @tabPeriodETC(
			ResourceID,
			TaskID,
			StartDate,
			PeriodHrs,
			ScheduledPct,
			UtilizationPct,
			BillableHrs,
			JTDHrs,
			NumWorkingDays,
			ETC
		)
		SELECT 
			ResourceID,
			TaskID,
			X.StartDate,
			X.PeriodHrs,
			CASE WHEN C.NumWorkingDays > 0 THEN PeriodHrs/(C.NumWorkingDays*@intWorkingHoursPerDay)*100 ELSE 0 END AS SchedulePct,
			CASE WHEN C.NumWorkingDays > 0 THEN BillableHrs/(C.NumWorkingDays*@intWorkingHoursPerDay)*100 ELSE 0 END AS UtilizationPct,
			X.BillableHrs,
			X.JTDHrs,
			X.NumWorkingDays,
			C.ETC
		FROM 
			(SELECT 
						ResourceID,
						AA.TaskID,
						AA.StartDate,
						SUM(ISNULL(AA.PeriodHrs,0)) AS PeriodHrs,
						SUM(ISNULL(BillableHrs,0)) AS BillableHrs,
						SUM(ISNULL(AA.NumWorkingDays,0)) AS NumWorkingDays,
						SUM(ISNULL(AA.JTDHrs,0)) AS JTDHrs,
						AA.ETC
					FROM (SELECT 
						ISNULL(@strResourceID,@strGenericResourceID) AS ResourceID,
						PT.TaskID,
						A.StartDate,
						SUM(ISNULL(ETC.PeriodHrs,0)) AS PeriodHrs,
						SUM(ISNULL(BillableHrs,0)) AS BillableHrs,
						MAX(ISNULL(A.NumWorkingDays,0)) AS NumWorkingDays,
						SUM(ISNULL(LD.PeriodHrs,0)) AS JTDHrs,
						A.ETC
					FROM (
					SELECT
					A.PlanID,
					C.StartDate,
					C.EndDate,
					SUM(CASE
						WHEN (A.StartDate <= C.StartDate AND A.EndDate >= C.EndDate)
						THEN NumWorkingDays
						ELSE dbo.DLTK$NumWorkingDays(
							CASE 
								WHEN A.StartDate > C.StartDate 
								THEN A.StartDate 
								ELSE C.StartDate 
							END, 
							CASE 
								WHEN A.EndDate < C.EndDate 
								THEN A.EndDate 
								ELSE C.EndDate 
							END,
						@strCompany)
					END)
					AS NumWorkingDays,
					C.ETC
					FROM @tabAssignment AS A
						INNER JOIN @tabCalendar AS C
						ON 1 = 1
					GROUP BY PlanID, C.StartDate, C.EndDate, C.ETC
				) AS A
				INNER JOIN RPTask AS PT ON PT.PlanID = A.PlanID AND PT.ParentOutlineNumber IS NULL
				LEFT JOIN @tabPLabTPD AS ETC
					ON A.PlanID = ETC.PlanID AND ETC.CIStartDate = A.StartDate AND ETC.ETC = A.ETC
				LEFT JOIN @tabLD AS LD
					ON A.StartDate <= LD.TransDate AND A.EndDate >= LD.TransDate
						AND LD.PlanID = PT.PlanID 
			GROUP BY PT.TaskID,A.StartDate,A.ETC) AS AA
			GROUP BY ResourceID,AA.TaskID,AA.StartDate,AA.ETC
			) AS X INNER JOIN @tabCalendar as C on X.StartDate = C.StartDate AND X.ETC = C.ETC
			ORDER BY TaskID,StartDate
	END
ELSE
	IF @strResourceSummary = 'Y'
		BEGIN
			INSERT @tabPeriodETC(
				ResourceID,
				TaskID,
				StartDate,
				PeriodHrs,
				ScheduledPct,
				UtilizationPct,
				BillableHrs,
				JTDHrs,
				NumWorkingDays,
				ETC
			)
			 SELECT 
				ResourceID,
				NULL AS TaskID,
				X.StartDate,
				X.PeriodHrs,
				CASE WHEN C.NumWorkingDays > 0 THEN PeriodHrs/(C.NumWorkingDays*@intWorkingHoursPerDay)*100 ELSE 0 END AS SchedulePct,
				CASE WHEN C.NumWorkingDays	 > 0 THEN BillableHrs/(C.NumWorkingDays*@intWorkingHoursPerDay)*100 ELSE 0 END AS UtilizationPct,
				X.BillableHrs,
				X.JTDHrs,
				X.NumWorkingDays,
				C.ETC 
			FROM 
				(SELECT 
					ResourceID,
					A.StartDate,
					SUM(ISNULL(ETC.PeriodHrs,0)) as Periodhrs,
					SUM(ISNULL(BillableHrs,0)) as BillableHrs,
					SUM(ISNULL(A.NumWorkingDays,0)) AS NumWorkingDays,
					SUM(ISNULL(LD.PeriodHrs,0)) AS JTDHrs,
					A.ETC
				FROM  
					(
						SELECT
						A.AssignmentID,
						A.ResourceID,
						C.StartDate,
						C.EndDate,
						SUM(ISNULL(CASE
							WHEN (A.StartDate <= C.StartDate AND A.EndDate >= C.EndDate)
							THEN NumWorkingDays
							ELSE dbo.DLTK$NumWorkingDays(
								CASE 
									WHEN A.StartDate > C.StartDate 
									THEN A.StartDate 
									ELSE C.StartDate 
								END, 
								CASE 
									WHEN A.EndDate < C.EndDate 
									THEN A.EndDate 
									ELSE C.EndDate 
								END,
							@strCompany)
						END,0))
						AS NumWorkingDays,
						C.ETC
						FROM @tabAssignment AS A
							INNER JOIN @tabCalendar AS C
							ON A.StartDate <= C.EndDate AND A.EndDate >= C.StartDate
						GROUP BY A.AssignmentID,A.ResourceID,C.StartDate,C.EndDate,C.ETC
					) AS A
					LEFT JOIN (
						SELECT
							AssignmentID,
							CIStartDate,
							ETC,
							SUM(ISNULL(PeriodHrs,0)) AS PeriodHrs,
							SUM(ISNULL(BillableHrs,0)) AS BillableHrs
						FROM @tabPLabTPD AS ETC
						GROUP BY AssignmentID, CIStartDate, ETC
					) AS ETC
					ON A.AssignmentID = ETC.AssignmentID AND ETC.CIStartDate = A.StartDate AND ETC.ETC = A.ETC
				
					LEFT JOIN @tabLD AS LD
					ON A.StartDate <= LD.TransDate AND A.EndDate >= LD.TransDate
				GROUP BY ResourceID,A.StartDate,A.ETC
				) AS X INNER JOIN @tabCalendar as C on X.StartDate = C.StartDate AND X.ETC = C.ETC
				ORDER BY C.StartDate
		END
	ELSE
		BEGIN
				INSERT @tabPeriodETC(
					ResourceID,
					TaskID,
					StartDate,
					PeriodHrs,
					ScheduledPct,
					UtilizationPct,
					BillableHrs,
					JTDHrs,
					NumWorkingDays,
					ETC
				)
				SELECT 
					ResourceID,
					TaskID,
					X.StartDate,
					X.PeriodHrs,
					CASE WHEN C.NumWorkingDays > 0 THEN PeriodHrs/(C.NumWorkingDays*@intWorkingHoursPerDay)*100 ELSE 0 END AS SchedulePct,
					CASE WHEN C.NumWorkingDays	 > 0 THEN BillableHrs/(C.NumWorkingDays*@intWorkingHoursPerDay)*100 ELSE 0 END AS UtilizationPct,
					X.BillableHrs,
					X.JTDHrs,
					X.NumWorkingDays,
					C.ETC
				FROM 
					(SELECT 
								ResourceID,
								AA.TaskID,
								AA.StartDate,
								SUM(ISNULL(AA.PeriodHrs,0)) AS PeriodHrs,
								SUM(ISNULL(BillableHrs,0)) AS BillableHrs,
								SUM(ISNULL(AA.NumWorkingDays,0)) AS NumWorkingDays,
								SUM(ISNULL(AA.JTDHrs,0)) AS JTDHrs,
								AA.ETC
							FROM (SELECT 
								ResourceID,
								CT.TaskID,
								A.StartDate,
								SUM(ISNULL(ETC.PeriodHrs,0)) AS PeriodHrs,
								SUM(ISNULL(BillableHrs,0)) AS BillableHrs,
								MAX(ISNULL(A.NumWorkingDays,0)) AS NumWorkingDays,
								SUM(ISNULL(LD.PeriodHrs,0)) AS JTDHrs,
								A.ETC
							FROM (
							SELECT
							A.AssignmentID,
							A.PlanID,
							A.TaskID,
							A.ResourceID,
							C.StartDate,
							C.EndDate,
							A.OutlineNumber,
							CASE
								WHEN (A.StartDate <= C.StartDate AND A.EndDate >= C.EndDate)
								THEN NumWorkingDays
								ELSE dbo.DLTK$NumWorkingDays(
									CASE 
										WHEN A.StartDate > C.StartDate 
										THEN A.StartDate 
										ELSE C.StartDate 
									END, 
									CASE 
										WHEN A.EndDate < C.EndDate 
										THEN A.EndDate 
										ELSE C.EndDate 
									END,
								@strCompany)
							END
							AS NumWorkingDays,
							C.ETC
							FROM @tabAssignment AS A
								INNER JOIN @tabCalendar AS C
								ON 1 = 1
						) AS A
						LEFT JOIN @tabPLabTPD AS ETC
							ON A.AssignmentID = ETC.AssignmentID AND ETC.CIStartDate = A.StartDate AND ETC.ETC = A.ETC
							LEFT JOIN RPTask AS PT ON PT.PlanID = A.PlanID
								INNER JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber
				
							LEFT JOIN @tabLD AS LD
							ON A.StartDate <= LD.TransDate AND A.EndDate >= LD.TransDate
								AND LD.PlanID = CT.PlanID 
								AND LD.OutlineNumber = CT.OutlineNumber
							WHERE A.OutlineNumber LIKE PT.OutlineNumber + '%' AND A.OutlineNumber LIKE CT.OutlineNumber + '%' 
					GROUP BY ResourceID,CT.TaskID,A.AssignmentID,A.StartDate,A.ETC) AS AA
					GROUP BY ResourceID,AA.TaskID,AA.StartDate,AA.ETC
					) AS X INNER JOIN @tabCalendar as C on X.StartDate = C.StartDate AND X.ETC = C.ETC
					ORDER BY TaskID,StartDate
			END

RETURN
END
GO
