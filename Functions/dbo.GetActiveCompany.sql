SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetActiveCompany]() RETURNS nvarchar(14) AS  
BEGIN 
-- Leave NVARCHAR alone since converting binary and need to know position for parsing and nvarchar is double byte
 RETURN CONVERT(nvarchar(14),(SELECT ISNULL(SESSION_CONTEXT(N'Company'),' ')))
END
GO
