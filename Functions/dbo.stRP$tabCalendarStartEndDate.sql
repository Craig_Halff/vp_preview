SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabCalendarStartEndDate](
  @strScopeStartDate varchar(8) /* Date must be in format: 'yyyymmdd' regardless of UI Culture */,
  @strScopeEndDate varchar(8) /* Date must be in format: 'yyyymmdd' regardless of UI Culture */,
  @strScale varchar(1) /* {d, w, m, p} */,
  @strInputCompany  Nvarchar(14)
)
  RETURNS @tabCalendarInterval TABLE(
    SeqID int IDENTITY(1,1),
    StartDate datetime,
    EndDate datetime,
    Scale varchar(1) COLLATE database_default, 
    NumWorkingDays integer,
    Heading varchar(255) COLLATE database_default
  )

BEGIN

  -- Plan range dates
  DECLARE @dtScopeStartDate datetime
  DECLARE @dtScopeEndDate datetime

  -- Calendar bucket dates
  DECLARE @dtCIStartDate datetime
  DECLARE @dtCIEndDate datetime

  -- Complete fiscal period dates
  DECLARE @dtTotalFiscalStartDate datetime
  DECLARE @dtTotalFiscalEndDate datetime

  -- Fiscal period during plan range dates
  DECLARE @dtFiscalStartDate datetime
  DECLARE @dtFiscalEndDate datetime
 
  DECLARE @strCompany Nvarchar(14)

  DECLARE @intWkEndDay AS integer

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Set Company to non-Multi-Company.
  SELECT @strCompany = @strInputCompany -- Should use input plan company instead of login dbo.GetActiveCompany()

  -- Get StartingDayOfWeek from Configuration Settings.
  SELECT @intWkEndDay = (CASE WHEN StartingDayOfWeek = 1 THEN 7 ELSE StartingDayOfWeek - 1 END)
  FROM CFGResourcePlanning Where Company = @strCompany

  -- Set Plan Dates
  SELECT @dtScopeStartDate = CONVERT(datetime, @strScopeStartDate)
  SELECT @dtScopeEndDate = CONVERT(datetime, @strScopeEndDate)

  -- Set complete fiscal period range dates
  SELECT @dtTotalFiscalStartDate = CONVERT(datetime, MIN(AccountPdStart)),
    @dtTotalFiscalEndDate = CONVERT(datetime, MAX(AccountPdEnd))
  FROM CFGDates

  -- When no fiscal periods are set up in the DB, use monthly scale
  IF (@dtTotalFiscalEndDate IS NULL AND @dtTotalFiscalStartDate IS NULL)
	  SET @strScale = 'm'

  -- Set first calendar bucket start equal to plan start
  SET @dtCIStartDate = @dtScopeStartDate
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  --If scale is fiscal period then first copy over any existing periods into the calendar
  IF (@strScale = 'p')
  BEGIN
    INSERT @tabCalendarInterval(
    StartDate, 
    EndDate, 
    Scale, 
    NumWorkingDays,
    Heading
    )
    SELECT
      AccountPdStart,
      AccountPdEnd,
      'p',
      dbo.DLTK$NumWorkingDays(AccountPdStart, AccountPdEnd, @strCompany) AS NumWorkingDays,
      CONVERT(varchar,Period % 100) + '/' + CONVERT(varchar,Period/100) AS Heading
    FROM CFGDates
    WHERE AccountPdStart <= @dtScopeEndDate AND AccountPdEnd >= @dtScopeStartDate

    --Retrieve last end date and first start date to determine if there are more calendar intervals to insert
    SELECT @dtFiscalEndDate = MAX(EndDate),
        @dtFiscalStartDate = MIN(StartDate)
    FROM @tabCalendarInterval

    -- Set first calendar bucket start date equal to plan start date when plan start date falls in the middle of an existing fiscal period
    IF (@dtFiscalStartDate < @dtScopeStartDate)
    BEGIN
      UPDATE @tabCalendarInterval
      SET StartDate = @dtScopeStartDate
      WHERE StartDate = @dtFiscalStartDate
    END

    -- Set last calendar bucket end date equal to plan end date when plan end date falls in the middle of an existing fiscal period
    IF (@dtFiscalEndDate > @dtScopeEndDate)
    BEGIN
      UPDATE @tabCalendarInterval
      SET EndDate = @dtScopeEndDate
      WHERE EndDate = @dtFiscalEndDate
    END

  END --IF (@strScale = 'p')

  -- Add additional buckets not covered by fiscal periods
  -- If not using periods, the plan range extends past the fiscal periods provided, or the plan range is completely past the fiscal periods then add on missing periods
  IF (@strScale <> 'p' OR (@dtScopeEndDate > @dtFiscalEndDate OR @dtScopeStartDate > @dtTotalFiscalEndDate))
  BEGIN

	-- reset calendar bucket start to be 1 day past the fiscal end
	IF (@dtFiscalEndDate IS NOT NULL AND @dtFiscalEndDate < @dtScopeEndDate)
		SET @dtCIStartDate = DATEADD(day, 1, @dtFiscalEndDate)
	ELSE IF (@dtScopeStartDate > @dtTotalFiscalEndDate)
		SET @dtCIStartDate = DATEADD(day, 1, @dtTotalFiscalEndDate)

    WHILE @dtCIStartDate <= @dtScopeEndDate
      BEGIN
	  	
		-- set bucket end date based on bucket start date
		IF (@strScale = 'd') 
			SET @dtCIEndDate = @dtCIStartDate
		ELSE
			SET @dtCIEndDate = dbo.DLTK$IntervalEnd(@dtCIStartDate, @strScale, @intWkEndDay)
        
		-- when the bucket end date is past the plan end date, reset to the plan end date
        IF (@dtCIEndDate > @dtScopeEndDate)
			SET @dtCIEndDate = @dtScopeEndDate

    -- when the plan is completely past fiscal periods, the first bucket start might be before the plan start
		IF (@dtCIStartDate < @dtScopeStartDate AND @dtCIEndDate >= @dtScopeStartDate AND @dtCIEndDate <= @dtScopeEndDate)
			SET @dtCIStartDate = @dtScopeStartDate
		
		-- make sure bucket is in plan range before inserting, could be outside if the plan is completely past fiscal periods
		IF (@dtCIStartDate >= @dtScopeStartDate AND @dtCIEndDate <= @dtScopeEndDate)
		BEGIN
			INSERT @tabCalendarInterval(
			  StartDate, 
			  EndDate, 
			  Scale, 
			  NumWorkingDays,
			  Heading
			)
			  SELECT
				@dtCIStartDate AS StartDate,
				@dtCIEndDate AS EndDate,
				@strScale AS Scale,
				dbo.DLTK$NumWorkingDays(@dtCIStartDate, @dtCIEndDate, @strCompany) AS NumWorkingDays,
				CASE 
				  WHEN YEAR(@dtCIStartDate) = YEAR(@dtCIEndDate) 
					THEN DATENAME(yyyy, @dtCIStartDate) + '<br />' + CONVERT(VARCHAR(5), @dtCIStartDate, 101)  + ' - ' +  CONVERT(VARCHAR(5), @dtCIEndDate, 101) 
				  WHEN YEAR(@dtCIStartDate) != YEAR(@dtCIEndDate) 
					THEN DATENAME(yyyy, @dtCIStartDate) + ' - ' + DATENAME(yyyy, @dtCIEndDate) + '<br />' + CONVERT(VARCHAR(5), @dtCIStartDate, 101)  + ' - ' +  CONVERT(VARCHAR(5), @dtCIEndDate, 101) 
            END AS Heading
		END
            
        -- Set @dtStartDate for next interval     
        SET @dtCIStartDate = DATEADD(d, 1, @dtCIEndDate)

      END --WHILE @dtCIStartDate <= @dtScopeEndDate
    END --IF (@strScale <> 'p' OR (@dtScopeEndDate > @dtFiscalEndDate OR @dtScopeStartDate > @dtTotalFiscalEndDate))


  -- now loop backwards to pick up calendar buckets where the plan range is before any fiscal periods
  IF (@strScale = 'p' AND (@dtScopeStartDate < @dtFiscalStartDate OR @dtScopeEndDate < @dtTotalFiscalStartDate))
  BEGIN
		IF (@dtFiscalStartDate IS NOT NULL)
			SET @dtCIEndDate = DATEADD(d, -1, @dtFiscalStartDate)
		ELSE 
			SET @dtCIEndDate = DATEADD(d, -1, @dtTotalFiscalStartDate)

		-- keep looping until calendar end date is past plan start date
		WHILE  @dtCIEndDate >= @dtScopeStartDate
			BEGIN
				-- set bucket start date based on bucket end date
				SET @dtCIStartDate = dbo.DLTK$IntervalStart(@dtCIEndDate, @strScale)
        
				-- when the bucket start date is before the plan start date, reset to the plan start date
				IF (@dtCIStartDate < @dtScopeStartDate)
					SET @dtCIStartDate = @dtScopeStartDate
          
        -- when the plan is completely before fiscal periods, the first bucket end might be after the plan end
				IF (@dtCIEndDate > @dtScopeEndDate AND @dtCIStartDate >= @dtScopeStartDate AND @dtCIStartDate <= @dtScopeEndDate)
					SET @dtCIEndDate = @dtScopeEndDate

				-- make sure bucket is in plan range before inserting, could be outside if the plan is completely before fiscal periods
				IF (@dtCIStartDate >= @dtScopeStartDate AND @dtCIEndDate <= @dtScopeEndDate)
				BEGIN
					INSERT @tabCalendarInterval(
						StartDate, 
						EndDate, 
						Scale, 
						NumWorkingDays,
						Heading
					)
						SELECT
						@dtCIStartDate AS StartDate,
						@dtCIEndDate AS EndDate,
						@strScale AS Scale,
						dbo.DLTK$NumWorkingDays(@dtCIStartDate, @dtCIEndDate, @strCompany) AS NumWorkingDays,
						CASE 
							WHEN YEAR(@dtCIStartDate) = YEAR(@dtCIEndDate) 
							THEN DATENAME(yyyy, @dtCIStartDate) + '<br />' + CONVERT(VARCHAR(5), @dtCIStartDate, 101)  + ' - ' +  CONVERT(VARCHAR(5), @dtCIEndDate, 101) 
							WHEN YEAR(@dtCIStartDate) != YEAR(@dtCIEndDate) 
							THEN DATENAME(yyyy, @dtCIStartDate) + ' - ' + DATENAME(yyyy, @dtCIEndDate) + '<br />' + CONVERT(VARCHAR(5), @dtCIStartDate, 101)  + ' - ' +  CONVERT(VARCHAR(5), @dtCIEndDate, 101) 
						END AS Heading
				END
            
				-- Set @dtCIEndDate for next interval     
				SET @dtCIEndDate = DATEADD(d, -1, @dtCIStartDate)

		END -- WHILE  @dtCIEndDate >= @dtScopeStartDate
	END -- IF (@strScale = 'p' AND (@dtScopeStartDate < @dtFiscalStartDate OR @dtScopeEndDate < @dtTotalFiscalStartDate))

RETURN
END

GO
