SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_PAT_PayableFilter] (@Employee varchar(20), @PayableSeq INT)
	RETURNS char(1)
	AS
	BEGIN
		declare @AcctRoles varchar(1024)
		declare @IsAcct char(1)
		declare @DelegationEnabled char(1), @ApprovalRequired char(1)
		select @AcctRoles = ';' + AcctRoles +';', @DelegationEnabled = Delegation, @ApprovalRequired = c.DelegationApproval FROM CCG_PAT_Config c

		select @IsAcct = case when max(charindex(';'+Role+';',@AcctRoles)) > 0 then 'Y' else '' end from SEUSER where employee = @Employee

		if @IsAcct = 'Y'
			RETURN '1'
		ELSE
		BEGIN
			IF @DelegationEnabled <> 'Y'
				IF EXISTS (
					select PayableSeq from CCG_PAT_Pending
						where Employee = @Employee AND PayableSeq = @PayableSeq
					UNION select PayableSeq from CCG_PAT_ProjectAmount pa
							INNER JOIN PR on PR.WBS1 = pa.WBS1 and PR.WBS2 = ' '
						where (PR.ProjMgr = @Employee or PR.Principal = @Employee)
							AND PayableSeq = @PayableSeq
					UNION select Seq from CCG_PAT_Payable where CreateUser = @Employee AND Seq = @PayableSeq
					UNION select PayableSeq from CCG_PAT_History
						where (ActionTakenBy = @Employee or ActionRecipient = @Employee or DelegateFor = @Employee)
							AND PayableSeq = @PayableSeq
				) RETURN '1'
				ELSE RETURN '0'
			ELSE
				IF EXISTS (
					select PayableSeq from CCG_PAT_Pending where Employee = @Employee
					UNION select PayableSeq from CCG_PAT_ProjectAmount pa
							INNER JOIN PR on PR.WBS1 = pa.WBS1 and PR.WBS2 = ' '
						where (PR.ProjMgr = @Employee or PR.Principal = @Employee)
							AND PayableSeq = @PayableSeq
					UNION select Seq from CCG_PAT_Payable where CreateUser = @Employee AND Seq = @PayableSeq
					UNION select PayableSeq from CCG_PAT_History
						where (ActionTakenBy = @Employee or ActionRecipient = @Employee or DelegateFor = @Employee)
							AND PayableSeq = @PayableSeq
					UNION select PayableSeq from CCG_PAT_Pending p
							inner join CCG_PAT_Delegation d on d.Delegate = @Employee and d.Employee = p.Employee
								AND (ISNULL(d.ApprovedBy,'') <> '' OR @ApprovalRequired <> 'Y') AND GETUTCDATE() BETWEEN d.FromDate AND d.ToDate
						where PayableSeq = @PayableSeq
				) RETURN '1'
				ELSE RETURN '0'
		END
		RETURN '0'
	END 
GO
