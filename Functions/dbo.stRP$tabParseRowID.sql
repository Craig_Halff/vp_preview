SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[stRP$tabParseRowID](
  @strRowID nvarchar(255)
)
  RETURNS @tabRowID TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ExpenseID varchar(32) COLLATE database_default,
    ConsultantID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    ResourceID nvarchar(20) COLLATE database_default,
    GenericResourceID nvarchar(20) COLLATE database_default,
    Account nvarchar(13) COLLATE database_default,
    Vendor nvarchar(20) COLLATE database_default,
    AECFlg varchar(1) COLLATE database_default
  )

BEGIN

  DECLARE @_RightArrowSign nchar = NCHAR(10132) -- N'➔'

  DECLARE @strIDLeft nvarchar(255)
  DECLARE @strAssignmentType varchar(1) = ''
  DECLARE @strAECFlg varchar(1) = ''

  DECLARE @strPlanID varchar(32) = NULL
  DECLARE @strTaskID varchar(32) = NULL
  DECLARE @strWBS1 nvarchar(30) = NULL
  DECLARE @strWBS2 nvarchar(30) = NULL
  DECLARE @strWBS3 nvarchar(30) = NULL
  DECLARE @strWBS1WBS2WBS3 nvarchar(92) = NULL
  DECLARE @strAssignmentID varchar(32) = NULL
  DECLARE @strExpenseID varchar(32) = NULL
  DECLARE @strConsultantID varchar(32) = NULL
  DECLARE @strResourceID nvarchar(20) = NULL
  DECLARE @strGenericResourceID nvarchar(20) = NULL
  DECLARE @strAccount nvarchar(13) = NULL
  DECLARE @strVendor nvarchar(20) = NULL

  DECLARE @bitIsAssignment bit = 0
  DECLARE @bitIsExpCon bit = 0

  DECLARE @siAccountType smallint

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. For an Assignment row, @strRowID = 'E~<PNAssignment.ResourceID>|<PNTask.TaskID>'
  --   2. For an Assignment row, @strRowID = 'G~<PNAssignment.GenericResourceID>|<PNTask.TaskID>'
  --   3. For an Expense row, @strRowID = '<PNExpense.Account>➔<PNExpense.Vendor>|<PNTask.TaskID>'
  --   4. For an Expense row, @strRowID = '<PNExpense.Account>➔|<PNTask.TaskID>'
  --   5. For a Consultant row, @strRowID = '<PNConsultant.Account>➔<PNConsultant.Vendor>|<PNTask.TaskID>'
  --   6. For a Consultant row, @strRowID = '<PNConsultant.Account>➔|<PNTask.TaskID>'
  --   7. For a WBS row, @strRowID = '|<PNTask.TaskID>'

  -- If @strRowID does not contain '|', then stop executing this UDF.

  IF (CHARINDEX('|', @strRowID) = 0) RETURN

  -- Seperate out TaskID.

  SET @strIDLeft = LEFT(@strRowID, CHARINDEX('|', @strRowID) - 1)
  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Setting various Plan parameter.

  SELECT 
    @strPlanID = PT.PlanID,
    @strWBS1 = PT.WBS1,
    @strWBS2 = ISNULL(PT.WBS2, ' '),
    @strWBS3 = ISNULL(PT.WBS3, ' ')
    FROM PNTask AS PT 
      LEFT JOIN PNTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
      LEFT JOIN PNTask AS TT ON PT.PlanID = TT.PlanID AND TT.OutlineLevel = 0
      INNER JOIN PNPlan AS P ON PT.PlanID = P.PlanID
    WHERE PT.TaskID = @strTaskID
    GROUP BY PT.PlanID, PT.TaskID, PT.Status, PT.WBS1, PT.WBS2, PT.WBS3, P.Company
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determining whether RowID was from an Assignment row.

  SET @bitIsAssignment =
    CASE 
      WHEN CHARINDEX('~', @strIDLeft) > 0
      THEN 1
      ELSE 0
    END

  IF (@bitIsAssignment = 1)
    BEGIN

      SET @strAECFlg = 'A'

      -- Determining Type of an Assignment row: {'E', 'G'}.

      SET @strAssignmentType = SUBSTRING(@strIDLeft, 1, 1)

      SET @strResourceID = 
        CASE
          WHEN @strAssignmentType = 'E'
          THEN REPLACE(@strIDLeft, 'E~', '')
          ELSE NULL
        END

      SET @strGenericResourceID =
        CASE
          WHEN @strAssignmentType = 'G'
          THEN REPLACE(@strIDLeft, 'G~', '')
          ELSE NULL
        END

      SELECT 
        @strAssignmentID = A.AssignmentID
        FROM PNAssignment AS A
        WHERE A.PlanID = @strPlanID AND A.TaskID = @strTaskID
          AND (ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') AND ISNULL(A.GenericResourceID, '|') = ISNULL( @strGenericResourceID, '|'))

    END /* END IF (@bitIsAssignment = 1) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determining whether RowID was from an Expense or Consultant row.

  SET @bitIsExpCon =
    CASE 
      WHEN CHARINDEX(@_RightArrowSign, @strIDLeft) > 0
      THEN 1
      ELSE 0
    END

  IF (@bitIsExpCon = 1)
    BEGIN

      SET @strAccount = LEFT(@strIDLeft, (CHARINDEX(@_RightArrowSign, @strIDLeft) - 1))
      SET @strVendor = 
        CASE
          WHEN CHARINDEX(@_RightArrowSign, REVERSE(@strIDLeft)) - 1 > 0
          THEN RIGHT(@strIDLeft, CHARINDEX(@_RightArrowSign, REVERSE(@strIDLeft)) - 1)
          ELSE NULL
        END

      SELECT 
        @siAccountType = CA.Type
        FROM CA
        WHERE CA.Account = @strAccount

      SET @strAECFlg =
        CASE 
          WHEN @siAccountType IN (5, 7) THEN 'E'
          WHEN @siAccountType IN (6, 8) THEN 'C'
          ELSE ''
        END

      IF (@strAECFlg = 'E')
        BEGIN

          SELECT 
            @strExpenseID = E.ExpenseID
            FROM PNExpense AS E
            WHERE E.PlanID = @strPlanID AND E.TaskID = @strTaskID 
              AND (E.Account = @strAccount AND ISNULL(E.Vendor, '|') = ISNULL( @strVendor, '|'))

        END /* END IF (@strAECFlg = 'E') */

      ELSE IF (@strAECFlg = 'C')
        BEGIN

          SELECT 
            @strConsultantID = C.ConsultantID
            FROM PNConsultant AS C
            WHERE C.PlanID = @strPlanID AND C.TaskID = @strTaskID 
              AND (C.Account = @strAccount AND ISNULL(C.Vendor, '|') = ISNULL( @strVendor, '|'))

        END /* END ELSE IF (@strAECFlg = 'C') */

    END /* END IF (@bitIsExpCon = 1) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabRowID(
    PlanID,
    TaskID,
    AssignmentID,
    ExpenseID,
    ConsultantID,
    WBS1,
    WBS2,
    WBS3,
    ResourceID,
    GenericResourceID,
    Account,
    Vendor,
    AECFlg
  )
    SELECT
      @strPlanID AS PlanID,
      @strTaskID AS TaskID,
      @strAssignmentID AS AssignmentID,
      @strExpenseID AS ExpenseID,
      @strConsultantID AS ConsultantID,
      @strWBS1 AS WBS1,
      @strWBS2 AS WBS2,
      @strWBS3 AS WBS3,
      @strResourceID AS ResourceID,
      @strGenericResourceID AS GenericResourceID,
      @strAccount AS Account,
      @strVendor AS Vendor,
      @strAECFlg AS AECFlg

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURN
END 

GO
