SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCCG_EI_JTDBilled] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
RETURNS money
AS BEGIN
/*
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select dbo.fnCCG_EI_JTDBilled('2003005.xx',' ',' ')
	select dbo.fnCCG_EI_JTDBilled('2003005.xx','1PD',' ')
*/
    declare @res money
	declare @Period			int
	declare @BillThruDate	datetime
	declare @draft money

	SELECT @Period=ThruPeriod, @BillThruDate=ThruDate 
	FROM CCG_EI_ConfigInvoiceGroups cig
	INNER JOIN ProjectCustomTabFields pctf on pctf.custInvoiceGroup=cig.InvoiceGroup
	WHERE pctf.WBS1=@WBS1 and pctf.WBS2=@WBS2 and pctf.WBS3=@WBS3
	
	select @draft  = colvalue from dbo.fnCCG_EI_DraftInvoiceAmt(@wbs1,@wbs2,@wbs3)
	if isnull(@Period ,0) = 0	set @Period=999999
	If @BillThruDate is null	set @BillThruDate='12/31/2020'

      SELECT @res = IsNull(Sum(-LedgerAR.Amount),0)
      FROM LedgerAR inner join AR on AR.WBS1=LedgerAR.WBS1 and AR.WBS2=LedgerAR.WBS2 and AR.WBS3=LedgerAR.WBS3 and AR.Invoice=LedgerAR.Invoice
		LEFT JOIN CA on CA.Account=LedgerAR.Account
		where LedgerAR.WBS1=@WBS1 and (@WBS2=' ' or @WBS2=LedgerAR.WBS2) and (@WBS3=' ' or @WBS3=LedgerAR.WBS3)
		  and (ledgerAR.TransType = 'IN' AND IsNull(SubType, ' ') not in ('I','R') AND AutoEntry = 'N')
		  and (CA.Type = 4 OR LedgerAR.Account IS NULL)
     Set @res = isnull(@res,0) + isnull(@draft,0)
     if @res = 0 set @res = null
     return @res
END
GO
