SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[spCCG_ReplaceASCII](@InputString VARCHAR(8000))
RETURNS VARCHAR(55)
AS
/*
Copyright (c) 2018 Central Consulting Group. All rights reserved.
05/11/2018 David Springer
           Strip special characters from text field.
*/
BEGIN
	Set @InputString = Replace (@InputString, ' ', '')
	Set @InputString = Replace (@InputString, '~', '')
	Set @InputString = Replace (@InputString, '`', '')
	Set @InputString = Replace (@InputString, '!', '')
	Set @InputString = Replace (@InputString, '@', '')
	Set @InputString = Replace (@InputString, '#', '')
	Set @InputString = Replace (@InputString, '$', '')
	Set @InputString = Replace (@InputString, '%', '')
	Set @InputString = Replace (@InputString, '^', '')
	Set @InputString = Replace (@InputString, '&', '')
	Set @InputString = Replace (@InputString, '*', '')
	Set @InputString = Replace (@InputString, '(', '')
	Set @InputString = Replace (@InputString, ')', '')
	Set @InputString = Replace (@InputString, '-', '')
	Set @InputString = Replace (@InputString, '|', '')
	Set @InputString = Replace (@InputString, '?', '')
	Set @InputString = Replace (@InputString, '/', '')

	RETURN @InputString;
END;
GO
