SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[PN$DiffBaselinePlanned]
  (@strPlanID varchar(32)
  )
  RETURNS bit
BEGIN -- Function PN$DiffBaselinePlanned

  DECLARE @bitDiff bit

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

   SELECT @bitDiff = CASE WHEN SUM(DX) > 0 THEN 1 ELSE 0 END FROM
     (SELECT CASE WHEN EXISTS
        (SELECT MIN(PorB) AS PorB FROM
           (SELECT 
              0 AS PorB,
              PlannedLaborHrs AS LabHrs, PlannedLabCost AS LabCost, PlannedLabBill AS LabBill, 
              PlannedExpCost AS ExpCost, PlannedExpBill AS ExpBill, PlannedConCost AS ConCost, PlannedConBill AS ConBill,
              PlannedDirExpCost AS DirExpCost, PlannedDirExpBill AS DirExpBill, PlannedDirConCost AS DirConCost, PlannedDirConBill AS DirConBill,
              StartDate AS StartDate, EndDate AS EndDate
              FROM PNPlan AS P WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS PorB,
              BaselineLaborHrs AS LabHrs, BaselineLabCost AS LabCost, BaselineLabBill AS LabBill, 
              BaselineExpCost AS ExpCost, BaselineExpBill AS ExpBill, BaselineConCost AS ConCost, BaselineConBill AS ConBill,
              BaselineDirExpCost AS DirExpCost, BaselineDirExpBill AS DirExpBill, BaselineDirConCost AS DirConCost, BaselineDirConBill AS DirConBill,
              BaselineStart AS StartDate, BaselineFinish AS EndDate
              FROM PNPlan AS B WHERE PlanID = @strPlanID
           ) ZP
           GROUP BY
             LabHrs, LabCost, LabBill, ExpCost, ExpBill, ConCost, ConBill,
             DirExpCost, DirExpBill, DirConCost, DirConBill, StartDate, EndDate
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Task */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(PorB) AS PorB FROM
           (SELECT 
              0 AS PorB,
              PlannedLaborHrs AS LabHrs, PlannedLabCost AS LabCost, PlannedLabBill AS LabBill, 
              PlannedExpCost AS ExpCost, PlannedExpBill AS ExpBill, PlannedConCost AS ConCost, PlannedConBill AS ConBill,
              PlannedDirExpCost AS DirExpCost, PlannedDirExpBill AS DirExpBill, PlannedDirConCost AS DirConCost, PlannedDirConBill AS DirConBill,
              StartDate AS StartDate, EndDate AS EndDate
              FROM PNTask AS P WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS PorB,
              BaselineLaborHrs AS LabHrs, BaselineLabCost AS LabCost, BaselineLabBill AS LabBill, 
              BaselineExpCost AS ExpCost, BaselineExpBill AS ExpBill, BaselineConCost AS ConCost, BaselineConBill AS ConBill,
              BaselineDirExpCost AS DirExpCost, BaselineDirExpBill AS DirExpBill, BaselineDirConCost AS DirConCost, BaselineDirConBill AS DirConBill,
              BaselineStart AS StartDate, BaselineFinish AS EndDate
              FROM PNTask AS B WHERE PlanID = @strPlanID
           ) ZT
           GROUP BY
             LabHrs, LabCost, LabBill, ExpCost, ExpBill, ConCost, ConBill,
             DirExpCost, DirExpBill, DirConCost, DirConBill, StartDate, EndDate
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Assignment */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(PorB) AS PorB FROM
           (SELECT 
              0 AS PorB,
              PlannedLaborHrs AS LabHrs, PlannedLabCost AS LabCost, PlannedLabBill AS LabBill, StartDate AS StartDate, EndDate AS EndDate
              FROM PNAssignment AS P WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS PorB,
              BaselineLaborHrs AS LabHrs, BaselineLabCost AS LabCost, BaselineLabBill AS LabBill, BaselineStart AS StartDate, BaselineFinish AS EndDate
              FROM PNAssignment AS B WHERE PlanID = @strPlanID
           ) ZA
           GROUP BY
             LabHrs, LabCost, LabBill, StartDate, EndDate
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Expense */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(PorB) AS PorB FROM
           (SELECT 
              0 AS PorB,
              PlannedExpCost AS ExpCost, PlannedExpBill AS ExpBill, PlannedDirExpCost AS DirExpCost, PlannedDirExpBill AS DirExpBill, StartDate AS StartDate, EndDate AS EndDate
              FROM PNExpense AS P WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS PorB,
              BaselineExpCost AS ExpCost, BaselineExpBill AS ExpBill, BaselineDirExpCost AS DirExpCost, BaselineDirExpBill AS DirExpBill, BaselineStart AS StartDate, BaselineFinish AS EndDate
              FROM PNExpense AS B WHERE PlanID = @strPlanID
           ) ZC
           GROUP BY
             ExpCost, ExpBill, DirExpCost, DirExpBill, StartDate, EndDate
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Consultant */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(PorB) AS PorB FROM
           (SELECT 
              0 AS PorB,
              PlannedConCost AS ConCost, PlannedConBill AS ConBill, PlannedDirConCost AS DirConCost, PlannedDirConBill AS DirConBill, StartDate AS StartDate, EndDate AS EndDate
              FROM PNConsultant AS P WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS PorB,
              BaselineConCost AS ConCost, BaselineConBill AS ConBill, BaselineDirConCost AS DirConCost, BaselineDirConBill AS DirConBill, BaselineStart AS StartDate, BaselineFinish AS EndDate
              FROM PNConsultant AS B WHERE PlanID = @strPlanID
           ) ZC
           GROUP BY
             ConCost, ConBill, DirConCost, DirConBill, StartDate, EndDate
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Labor TPD */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(PorB) AS PorB FROM
           (SELECT 
              0 AS PorB,
              TaskID, PlanID, AssignmentID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodHrs, PeriodCost, PeriodBill, PeriodRev, CostRate, BillingRate
              FROM PNPlannedLabor AS P WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS PorB,
              TaskID, PlanID, AssignmentID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodHrs, PeriodCost, PeriodBill, PeriodRev, CostRate, BillingRate
              FROM PNBaselineLabor AS B WHERE PlanID = @strPlanID
           ) ZTPD
           GROUP BY
             TaskID, PlanID, AssignmentID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodHrs, PeriodCost, PeriodBill, PeriodRev, CostRate, BillingRate
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Expense TPD */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(PorB) AS PorB FROM
           (SELECT 
              0 AS PorB,
              TaskID, PlanID, ExpenseID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
              FROM PNPlannedExpenses AS P WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS PorB,
              TaskID, PlanID, ExpenseID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
              FROM PNBaselineExpenses AS B WHERE PlanID = @strPlanID
           ) ZTPD
           GROUP BY
             TaskID, PlanID, ExpenseID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
      UNION ALL /* Consultant TPD */
      SELECT CASE WHEN EXISTS
        (SELECT MIN(PorB) AS PorB FROM
           (SELECT 
              0 AS PorB,
              TaskID, PlanID, ConsultantID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
              FROM PNPlannedConsultant AS P WHERE PlanID = @strPlanID
            UNION ALL
            SELECT
              1 AS PorB,
              TaskID, PlanID, ConsultantID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
              FROM PNBaselineConsultant AS B WHERE PlanID = @strPlanID
           ) ZTPD
           GROUP BY
             TaskID, PlanID, ConsultantID, StartDate, EndDate, PeriodScale, PeriodCount, PeriodCost, PeriodBill, PeriodRev
           HAVING COUNT(*) = 1
        ) THEN 1 ELSE 0 END AS DX
     ) AS Z

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  RETURN @bitDiff

END -- PN$DiffBaselinePlanned
GO
