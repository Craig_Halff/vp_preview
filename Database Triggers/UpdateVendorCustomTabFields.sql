SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [UpdateVendorCustomTabFields]
ON DATABASE
FOR DDL_TABLE_EVENTS
AS
DECLARE @EventData      xml
SET @EventData=EVENTDATA()

IF @EventData.value('(/EVENT_INSTANCE/ObjectType)[1]', 'varchar(50)')='TABLE'
    AND @EventData.value('(/EVENT_INSTANCE/ObjectName)[1]', 'varchar(50)') ='ClientCustomTabFields'
BEGIN
	EXECUTE sp_refreshview 'VendorCustomTabFields';  
   
END
GO
