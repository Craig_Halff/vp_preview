CREATE TABLE [dbo].[FW_CFGGridSettings]
(
[ApplicationClassName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Username] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ResultSetID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColWidth] [smallint] NOT NULL CONSTRAINT [DF__FW_CFGGri__ColWi__76249345] DEFAULT ((0)),
[ColList] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGGridSettings] ADD CONSTRAINT [FW_CFGGridSettingsPK] PRIMARY KEY NONCLUSTERED ([ApplicationClassName], [Username], [ResultSetID], [ColName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
