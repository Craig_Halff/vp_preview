CREATE TABLE [dbo].[HAI_CrewEntryAppLog]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[UserId] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Timestamp] [datetime] NOT NULL,
[Action] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Details] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HAI_CrewEntryAppLog] ADD CONSTRAINT [PK__HAI_Unit__3213E83FE9F1BEEA] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
