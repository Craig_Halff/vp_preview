CREATE TABLE [dbo].[OppLabor_Backup]
(
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LaborID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EstimateHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OppLabor___Estim__41F0C228] DEFAULT ((0)),
[EstimateCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OppLabor___Estim__42E4E661] DEFAULT ((0)),
[EstimateBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OppLabor___Estim__43D90A9A] DEFAULT ((0)),
[CostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OppLabor___CostR__44CD2ED3] DEFAULT ((0)),
[BillingRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OppLabor___Billi__45C1530C] DEFAULT ((0)),
[Category] [smallint] NOT NULL CONSTRAINT [DF__OppLabor___Categ__46B57745] DEFAULT ((0)),
[SeqNo] [smallint] NOT NULL CONSTRAINT [DF__OppLabor___SeqNo__47A99B7E] DEFAULT ((0)),
[GenericResourceID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OppLabor_Backup] ADD CONSTRAINT [OppLaborPK] PRIMARY KEY NONCLUSTERED ([LaborID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
