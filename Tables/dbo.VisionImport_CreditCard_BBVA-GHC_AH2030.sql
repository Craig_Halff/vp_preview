CREATE TABLE [dbo].[VisionImport_CreditCard_BBVA-GHC_AH2030]
(
[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrimaryCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SecondaryCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserDefined1] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date] [datetime] NULL,
[Amount] [decimal] (19, 4) NULL,
[MerchantDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CardholderName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SecondaryAccountNumber] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountCurrency] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentAmount] [decimal] (19, 4) NULL,
[UserDefined2] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserDefined3] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserDefined4] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserDefined5] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VisionImport_CreditCard_BBVA-GHC_AH2030] ADD CONSTRAINT [VisionImport_CreditCard_BBVA-GHC_AH2030PK] PRIMARY KEY NONCLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
