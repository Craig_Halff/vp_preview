CREATE TABLE [dbo].[Consolidations]
(
[ReportingGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__Consolida__Perio__4FF3FD65] DEFAULT ((0)),
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Amoun__50E8219E] DEFAULT ((0)),
[CBAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__CBAmo__51DC45D7] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Consolidations] ADD CONSTRAINT [ConsolidationsPK] PRIMARY KEY NONCLUSTERED ([ReportingGroup], [Company], [Period], [Account]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
