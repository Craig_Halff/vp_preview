CREATE TABLE [dbo].[CFGClientHierarchyDescriptions]
(
[PKEY] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGClientHierarchyDescriptions] ADD CONSTRAINT [CFGClientHierarchyDescriptionsPK] PRIMARY KEY CLUSTERED ([PKEY], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
