CREATE TABLE [dbo].[erControl]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PostPeriod] [int] NOT NULL CONSTRAINT [DF__erControl__PostP__04DCC6F0] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__erControl__PostS__05D0EB29] DEFAULT ((0)),
[Recurring] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__erControl__Recur__06C50F62] DEFAULT ('N'),
[Selected] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__erControl__Selec__07B9339B] DEFAULT ('N'),
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__erControl__Poste__08AD57D4] DEFAULT ('N'),
[Creator] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__erControl__Perio__09A17C0D] DEFAULT ((0)),
[EndDate] [datetime] NULL,
[Total] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__erControl__Total__0A95A046] DEFAULT ((0)),
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SubmittedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedDate] [datetime] NULL,
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[erControl] ADD CONSTRAINT [erControlPK] PRIMARY KEY NONCLUSTERED ([Batch]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
