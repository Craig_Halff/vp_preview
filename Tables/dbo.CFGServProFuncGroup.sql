CREATE TABLE [dbo].[CFGServProFuncGroup]
(
[ServProCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FuncGroupCode] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Disabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGServPr__Disab__6A1DE3BC] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGServProFuncGroup] ADD CONSTRAINT [CFGServProFuncGroupPK] PRIMARY KEY CLUSTERED ([ServProCode], [FuncGroupCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
