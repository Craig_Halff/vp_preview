CREATE TABLE [dbo].[BTDefaultsTaxCodes]
(
[DefaultType] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__BTDefaultsT__Seq__46C9BA87] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTDefaultsTaxCodes] ADD CONSTRAINT [BTDefaultsTaxCodesPK] PRIMARY KEY CLUSTERED ([DefaultType], [Company], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
