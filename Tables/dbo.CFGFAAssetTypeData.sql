CREATE TABLE [dbo].[CFGFAAssetTypeData]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PropertyType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DepMethod] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UsefulLifeYr] [int] NOT NULL CONSTRAINT [DF__CFGFAAsse__Usefu__7F2E0F3B] DEFAULT ((0)),
[AssetAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccuDepAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DepExpAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InUse] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGFAAsse__InUse__00223374] DEFAULT ('Y')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGFAAssetTypeData] ADD CONSTRAINT [CFGFAAssetTypeDataPK] PRIMARY KEY CLUSTERED ([Code], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
