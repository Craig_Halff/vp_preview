CREATE TABLE [dbo].[SF255Employees]
(
[SF255ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__SF255Employ__Seq__26B3875E] DEFAULT ((0)),
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifiedInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF255Empl__Verif__27A7AB97] DEFAULT ('N'),
[EmployeeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectAssignment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompanyName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearsWithFirm] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearsOtherFirms] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Experience] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FormatPreference] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GraphicPath] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DegreeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegistrationInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF255Employees] ADD CONSTRAINT [SF255EmployeesPK] PRIMARY KEY NONCLUSTERED ([SF255ID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
