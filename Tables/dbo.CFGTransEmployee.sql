CREATE TABLE [dbo].[CFGTransEmployee]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Approval] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGTransEmployee] ADD CONSTRAINT [CFGTransEmployeePK] PRIMARY KEY CLUSTERED ([Company], [Approval], [Employee]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
