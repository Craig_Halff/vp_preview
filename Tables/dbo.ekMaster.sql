CREATE TABLE [dbo].[ekMaster]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportDate] [datetime] NOT NULL,
[ReportName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdvanceAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ekMaster___Advan__58DE4921] DEFAULT ((0)),
[Selected] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ekMaster___Selec__59D26D5A] DEFAULT ('N'),
[SubmittedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedDate] [datetime] NULL,
[ApprovedDate] [datetime] NULL,
[DefaultCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentExchangeOverrideMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ekMaster___Payme__5AC69193] DEFAULT ('N'),
[PaymentExchangeOverrideDate] [datetime] NULL,
[PaymentExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__ekMaster___Payme__5BBAB5CC] DEFAULT ((0)),
[CurrencyExchangeOverrideMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ekMaster___Curre__5CAEDA05] DEFAULT ('N'),
[CurrencyExchangeOverrideDate] [datetime] NULL,
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__ekMaster___Curre__5DA2FE3E] DEFAULT ((0)),
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ekMaster__Master__010E9811] DEFAULT (left(replace(newid(),'-',''),(32))),
[DefaultTaxLocation] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ekMaster] ADD CONSTRAINT [ekMasterPK] PRIMARY KEY NONCLUSTERED ([Employee], [ReportDate], [ReportName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [EKMasterPKeyIDX] ON [dbo].[ekMaster] ([MasterPKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
