CREATE TABLE [dbo].[MktCampaignClientAssoc]
(
[PKey] [int] NOT NULL IDENTITY(1, 1),
[CampaignID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NewClient] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__MktCampai__NewCl__7B7FDBED] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MktCampaignClientAssoc] ADD CONSTRAINT [MktCampaignClientAssocPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [MktCampaignClientAssocIDX] ON [dbo].[MktCampaignClientAssoc] ([CampaignID], [ContactID], [ClientID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
