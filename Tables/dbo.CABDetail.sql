CREATE TABLE [dbo].[CABDetail]
(
[BudgetName] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefBudget] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CABDetail__RefBu__328DB7B0] DEFAULT ((0)),
[Annual] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CABDetail__Annua__3381DBE9] DEFAULT ((0)),
[Amount1] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CABDetail__Amoun__34760022] DEFAULT ((0)),
[Amount2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CABDetail__Amoun__356A245B] DEFAULT ((0)),
[Amount3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CABDetail__Amoun__365E4894] DEFAULT ((0)),
[Amount4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CABDetail__Amoun__37526CCD] DEFAULT ((0)),
[Amount5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CABDetail__Amoun__38469106] DEFAULT ((0)),
[Amount6] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CABDetail__Amoun__393AB53F] DEFAULT ((0)),
[Amount7] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CABDetail__Amoun__3A2ED978] DEFAULT ((0)),
[Amount8] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CABDetail__Amoun__3B22FDB1] DEFAULT ((0)),
[Amount9] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CABDetail__Amoun__3C1721EA] DEFAULT ((0)),
[Amount10] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CABDetail__Amoun__3D0B4623] DEFAULT ((0)),
[Amount11] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CABDetail__Amoun__3DFF6A5C] DEFAULT ((0)),
[Amount12] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CABDetail__Amoun__3EF38E95] DEFAULT ((0)),
[Amount13] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CABDetail__Amoun__3FE7B2CE] DEFAULT ((0)),
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CABDetail] ADD CONSTRAINT [CABDetailPK] PRIMARY KEY NONCLUSTERED ([BudgetName], [Account]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
