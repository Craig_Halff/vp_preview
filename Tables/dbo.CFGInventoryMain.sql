CREATE TABLE [dbo].[CFGInventoryMain]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EnableInventory] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInvent__Enabl__2BCBAB88] DEFAULT ('N'),
[ModifyLocation] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInvent__Modif__2CBFCFC1] DEFAULT ('N'),
[ModifyInventoryQty] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInvent__Modif__2DB3F3FA] DEFAULT ('N'),
[RequireQtyAdjReason] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInvent__Requi__2EA81833] DEFAULT ('N'),
[NeedDays] [smallint] NOT NULL CONSTRAINT [DF__CFGInvent__NeedD__2F9C3C6C] DEFAULT ((0)),
[NextIR] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGInvent__NextI__309060A5] DEFAULT ((1)),
[NextIT] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGInvent__NextI__318484DE] DEFAULT ((1)),
[InventoryWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InventoryWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InventoryWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultLocation] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequireCostAdjReason] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInvent__Requi__3278A917] DEFAULT ('N'),
[DefaultInLocation] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInvent__Defau__336CCD50] DEFAULT ('N'),
[ApprovalWorkflowIR] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGInventoryMain] ADD CONSTRAINT [CFGInventoryMainPK] PRIMARY KEY CLUSTERED ([Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
