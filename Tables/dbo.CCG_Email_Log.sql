CREATE TABLE [dbo].[CCG_Email_Log]
(
[LogID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_CCG_Email_Log_LogID] DEFAULT (newid()),
[Action] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionDateTime] [datetime] NOT NULL CONSTRAINT [DF_CCG_Email_Log_ActionDateTime] DEFAULT (getutcdate()),
[Detail] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_Email_Log] ADD CONSTRAINT [PK_CCG_Email_Log] PRIMARY KEY CLUSTERED ([LogID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_CCG_Email_Log] ON [dbo].[CCG_Email_Log] ([ActionDateTime]) ON [PRIMARY]
GO
