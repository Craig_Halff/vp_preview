CREATE TABLE [dbo].[MktCampaigns_CustAssociatedProjects]
(
[CampaignID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustAssociatedProjectCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustAssociatedProject] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustAQCategory] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustAssociatedProjectTotalFee] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__MktCampai__CustA__2C018109] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_MktCampaigns_CustAssociatedProjects]
      ON [dbo].[MktCampaigns_CustAssociatedProjects]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'MktCampaigns_CustAssociatedProjects'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CampaignID',CONVERT(NVARCHAR(2000),[CampaignID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Seq',CONVERT(NVARCHAR(2000),[Seq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustAssociatedProjectCode',CONVERT(NVARCHAR(2000),[CustAssociatedProjectCode],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustAssociatedProject',CONVERT(NVARCHAR(2000),DELETED.[CustAssociatedProject],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join PR as oldDesc   on DELETED.CustAssociatedProject = oldDesc.WBS1

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustAQCategory',CONVERT(NVARCHAR(2000),[CustAQCategory],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustAssociatedProjectTotalFee',CONVERT(NVARCHAR(2000),[CustAssociatedProjectTotalFee],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_MktCampaigns_CustAssociatedProjects] ON [dbo].[MktCampaigns_CustAssociatedProjects]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_MktCampaigns_CustAssociatedProjects]
      ON [dbo].[MktCampaigns_CustAssociatedProjects]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'MktCampaigns_CustAssociatedProjects'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CampaignID',NULL,CONVERT(NVARCHAR(2000),[CampaignID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',NULL,CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustAssociatedProjectCode',NULL,CONVERT(NVARCHAR(2000),[CustAssociatedProjectCode],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustAssociatedProject',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustAssociatedProject],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  PR as newDesc  on INSERTED.CustAssociatedProject = newDesc.WBS1

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustAQCategory',NULL,CONVERT(NVARCHAR(2000),[CustAQCategory],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustAssociatedProjectTotalFee',NULL,CONVERT(NVARCHAR(2000),[CustAssociatedProjectTotalFee],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_MktCampaigns_CustAssociatedProjects] ON [dbo].[MktCampaigns_CustAssociatedProjects]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_MktCampaigns_CustAssociatedProjects]
      ON [dbo].[MktCampaigns_CustAssociatedProjects]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'MktCampaigns_CustAssociatedProjects'
    
      If UPDATE([CampaignID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CampaignID',
      CONVERT(NVARCHAR(2000),DELETED.[CampaignID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CampaignID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CampaignID] Is Null And
				DELETED.[CampaignID] Is Not Null
			) Or
			(
				INSERTED.[CampaignID] Is Not Null And
				DELETED.[CampaignID] Is Null
			) Or
			(
				INSERTED.[CampaignID] !=
				DELETED.[CampaignID]
			)
		) 
		END		
		
      If UPDATE([Seq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',
      CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] Is Null And
				DELETED.[Seq] Is Not Null
			) Or
			(
				INSERTED.[Seq] Is Not Null And
				DELETED.[Seq] Is Null
			) Or
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
		
      If UPDATE([CustAssociatedProjectCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustAssociatedProjectCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustAssociatedProjectCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustAssociatedProjectCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustAssociatedProjectCode] Is Null And
				DELETED.[CustAssociatedProjectCode] Is Not Null
			) Or
			(
				INSERTED.[CustAssociatedProjectCode] Is Not Null And
				DELETED.[CustAssociatedProjectCode] Is Null
			) Or
			(
				INSERTED.[CustAssociatedProjectCode] !=
				DELETED.[CustAssociatedProjectCode]
			)
		) 
		END		
		
     If UPDATE([CustAssociatedProject])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustAssociatedProject',
     CONVERT(NVARCHAR(2000),DELETED.[CustAssociatedProject],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustAssociatedProject],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustAssociatedProject] Is Null And
				DELETED.[CustAssociatedProject] Is Not Null
			) Or
			(
				INSERTED.[CustAssociatedProject] Is Not Null And
				DELETED.[CustAssociatedProject] Is Null
			) Or
			(
				INSERTED.[CustAssociatedProject] !=
				DELETED.[CustAssociatedProject]
			)
		) left join PR as oldDesc  on DELETED.CustAssociatedProject = oldDesc.WBS1  left join  PR as newDesc  on INSERTED.CustAssociatedProject = newDesc.WBS1
		END		
		
      If UPDATE([CustAQCategory])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustAQCategory',
      CONVERT(NVARCHAR(2000),DELETED.[CustAQCategory],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustAQCategory],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustAQCategory] Is Null And
				DELETED.[CustAQCategory] Is Not Null
			) Or
			(
				INSERTED.[CustAQCategory] Is Not Null And
				DELETED.[CustAQCategory] Is Null
			) Or
			(
				INSERTED.[CustAQCategory] !=
				DELETED.[CustAQCategory]
			)
		) 
		END		
		
      If UPDATE([CustAssociatedProjectTotalFee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustAssociatedProjectTotalFee',
      CONVERT(NVARCHAR(2000),DELETED.[CustAssociatedProjectTotalFee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustAssociatedProjectTotalFee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustAssociatedProjectTotalFee] Is Null And
				DELETED.[CustAssociatedProjectTotalFee] Is Not Null
			) Or
			(
				INSERTED.[CustAssociatedProjectTotalFee] Is Not Null And
				DELETED.[CustAssociatedProjectTotalFee] Is Null
			) Or
			(
				INSERTED.[CustAssociatedProjectTotalFee] !=
				DELETED.[CustAssociatedProjectTotalFee]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_MktCampaigns_CustAssociatedProjects] ON [dbo].[MktCampaigns_CustAssociatedProjects]
GO
ALTER TABLE [dbo].[MktCampaigns_CustAssociatedProjects] ADD CONSTRAINT [MktCampaigns_CustAssociatedProjectsPK] PRIMARY KEY CLUSTERED ([CampaignID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
