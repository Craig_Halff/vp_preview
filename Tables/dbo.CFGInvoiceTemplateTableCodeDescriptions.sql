CREATE TABLE [dbo].[CFGInvoiceTemplateTableCodeDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGInvoiceT__Seq__3B0DEF18] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGInvoiceTemplateTableCodeDescriptions] ADD CONSTRAINT [CFGInvoiceTemplateTableCodeDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
