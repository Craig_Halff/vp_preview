CREATE TABLE [dbo].[GLSummary]
(
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__GLSummary__Perio__1DC8704B] DEFAULT ((0)),
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__GLSummary__Amoun__1EBC9484] DEFAULT ((0)),
[CBAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__GLSummary__CBAmo__1FB0B8BD] DEFAULT ((0)),
[CreditAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__GLSummary__Credi__1BD8683D] DEFAULT ((0)),
[DebitAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__GLSummary__Debit__1CCC8C76] DEFAULT ((0)),
[CBCreditAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__GLSummary__CBCre__1DC0B0AF] DEFAULT ((0)),
[CBDebitAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__GLSummary__CBDeb__1EB4D4E8] DEFAULT ((0)),
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GLSummary] ADD CONSTRAINT [GLSummaryPK] PRIMARY KEY NONCLUSTERED ([Account], [Period], [Org], [TransType]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
