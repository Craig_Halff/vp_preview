CREATE TABLE [dbo].[jeMaster]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransDate] [datetime] NULL,
[Type] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__jeMaster___Poste__039E89F4] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__jeMaster_Ne__Seq__0492AE2D] DEFAULT ((0)),
[UpdateBilled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__jeMaster___Updat__0586D266] DEFAULT ('N'),
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyExchangeOverrideMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__jeMaster___Curre__067AF69F] DEFAULT ('N'),
[CurrencyExchangeOverrideDate] [datetime] NULL,
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__jeMaster___Curre__076F1AD8] DEFAULT ((0)),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__jeMaster___Statu__08633F11] DEFAULT ('N'),
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RejectReason] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Diary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaryNo] [int] NOT NULL CONSTRAINT [DF__jeMaster___Diary__0957634A] DEFAULT ((0)),
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[AllowAssetEntries] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__jeMaster__AllowA__74F2D9EF] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[jeMaster] ADD CONSTRAINT [jeMasterPK] PRIMARY KEY CLUSTERED ([Batch], [RefNo]) ON [PRIMARY]
GO
