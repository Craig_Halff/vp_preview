CREATE TABLE [dbo].[CostCTCats]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__CostCTCat__Table__143CDA05] DEFAULT ((0)),
[RateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Category] [smallint] NOT NULL,
[EffectiveDate] [datetime] NULL,
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CostCTCats__Rate__1530FE3E] DEFAULT ((0)),
[OvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CostCTCat__OvtPc__16252277] DEFAULT ((0)),
[Ovt2Pct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CostCTCat__Ovt2P__171946B0] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CostCTCats] ADD CONSTRAINT [CostCTCatsPK] PRIMARY KEY NONCLUSTERED ([TableNo], [RateID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CostCTCatsCategoryIDX] ON [dbo].[CostCTCats] ([TableNo], [Category]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
