CREATE TABLE [dbo].[SavedOptionsDetail]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__SavedOption__Seq__234D0ACE] DEFAULT ((0)),
[ParentKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OptionName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Operator] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportOption] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SavedOpti__Repor__24412F07] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[Condition] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TableName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SearchLevel] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SavedOptionsDetail] ADD CONSTRAINT [SavedOptionsDetailPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [SavedOptionsDetailParentKeyIDX] ON [dbo].[SavedOptionsDetail] ([ParentKey]) ON [PRIMARY]
GO
