CREATE TABLE [dbo].[CFGContactSourceDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContactSource] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGContactS__Seq__75D9AF2B] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGContactSourceDescriptions] ADD CONSTRAINT [CFGContactSourceDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
