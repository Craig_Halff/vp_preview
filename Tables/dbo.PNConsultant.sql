CREATE TABLE [dbo].[PNConsultant]
(
[ConsultantID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcctName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BaselineConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNConsult__Basel__68A07AF5] DEFAULT ((0)),
[BaselineConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNConsult__Basel__69949F2E] DEFAULT ((0)),
[PlannedConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNConsult__Plann__6A88C367] DEFAULT ((0)),
[PlannedConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNConsult__Plann__6B7CE7A0] DEFAULT ((0)),
[PlannedDirConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNConsult__Plann__6C710BD9] DEFAULT ((0)),
[PlannedDirConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNConsult__Plann__6D653012] DEFAULT ((0)),
[PctCompleteConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNConsult__PctCo__6E59544B] DEFAULT ((0)),
[PctCompleteConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNConsult__PctCo__6F4D7884] DEFAULT ((0)),
[WeightedConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNConsult__Weigh__70419CBD] DEFAULT ((0)),
[WeightedConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNConsult__Weigh__7135C0F6] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[DirectAcctFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNConsult__Direc__7229E52F] DEFAULT ('N'),
[ConBillRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNConsult__ConBi__731E0968] DEFAULT ((0)),
[ConRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNConsult__ConRe__74122DA1] DEFAULT ((0)),
[SortSeq] [smallint] NOT NULL CONSTRAINT [DF__PNConsult__SortS__750651DA] DEFAULT ((0)),
[LabParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNConsult__LabPa__75FA7613] DEFAULT ('N'),
[ExpParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNConsult__ExpPa__76EE9A4C] DEFAULT ('N'),
[ConParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNConsult__ConPa__77E2BE85] DEFAULT ('N'),
[UntParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNConsult__UntPa__78D6E2BE] DEFAULT ('N'),
[BaselineStart] [datetime] NULL,
[BaselineFinish] [datetime] NULL,
[ConVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNConsult__ConVS__79CB06F7] DEFAULT ('Y'),
[ExpVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNConsult__ExpVS__7ABF2B30] DEFAULT ('N'),
[LabVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNConsult__LabVS__7BB34F69] DEFAULT ('N'),
[UntVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNConsult__UntVS__7CA773A2] DEFAULT ('N'),
[BaselineDirConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNConsult__Basel__7D9B97DB] DEFAULT ((0)),
[BaselineDirConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNConsult__Basel__7E8FBC14] DEFAULT ((0)),
[JTDConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNConsult__JTDCo__7F83E04D] DEFAULT ((0)),
[JTDConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNConsult__JTDCo__00780486] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NOT NULL CONSTRAINT [DF__PNConsult__Creat__016C28BF] DEFAULT (getutcdate()),
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NOT NULL CONSTRAINT [DF__PNConsult__ModDa__02604CF8] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PNConsultant] ADD CONSTRAINT [PNConsultantPK] PRIMARY KEY NONCLUSTERED ([ConsultantID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [PNConsultantPlanIDIDX] ON [dbo].[PNConsultant] ([PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNConsultantTaskIDIDX] ON [dbo].[PNConsultant] ([TaskID]) ON [PRIMARY]
GO
