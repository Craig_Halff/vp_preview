CREATE TABLE [dbo].[CFGConsolidationGroups]
(
[ReportingGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GainsAndLossesAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssetTranslationMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LiabilityTranslationMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NetWorthTranslationMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevenueTranslationMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReimbTranslationMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DirectTranslationMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndirectTranslationMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OtherChargesTranslationMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGConsolidationGroups] ADD CONSTRAINT [CFGConsolidationGroupsPK] PRIMARY KEY CLUSTERED ([ReportingGroup]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
