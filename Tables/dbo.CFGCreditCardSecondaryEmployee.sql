CREATE TABLE [dbo].[CFGCreditCardSecondaryEmployee]
(
[PrimaryCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCreditCardSecondaryEmployee] ADD CONSTRAINT [CFGCreditCardSecondaryEmployeePK] PRIMARY KEY NONCLUSTERED ([PrimaryCode], [Company], [Code], [Employee]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
