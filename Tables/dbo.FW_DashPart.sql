CREATE TABLE [dbo].[FW_DashPart]
(
[Dashpart_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PartKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Dashboard_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PartType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PartName] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XML] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hidden] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_DashPa__Hidde__2F280677] DEFAULT ('N'),
[Global] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_DashPa__Globa__301C2AB0] DEFAULT ('N'),
[ByRole] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_DashPa__ByRol__31104EE9] DEFAULT ('N'),
[Readonly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_DashPa__Reado__32047322] DEFAULT ('N'),
[IsPublished] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_DashPa__IsPub__32F8975B] DEFAULT ('N'),
[Owner] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_DashPart] ADD CONSTRAINT [FW_DashPartPK] PRIMARY KEY CLUSTERED ([Dashpart_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FW_DashPartIDX] ON [dbo].[FW_DashPart] ([UserName], [PartKey], [Dashboard_UID]) ON [PRIMARY]
GO
