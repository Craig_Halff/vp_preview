CREATE TABLE [dbo].[EMAccrual]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HoursPerYear] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMAccrual__Hours__68208CB1] DEFAULT ((0)),
[ChangeDate] [datetime] NULL,
[CurrentEarned] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMAccrual__Curre__6914B0EA] DEFAULT ((0)),
[Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMAccrual__Overr__6A08D523] DEFAULT ('N'),
[CurrentTaken] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMAccrual__Curre__6AFCF95C] DEFAULT ((0)),
[Adjust] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMAccrual__Adjus__6BF11D95] DEFAULT ((0)),
[HoursToAccrue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMAccrual__Hours__6CE541CE] DEFAULT ((0)),
[HasCarryoverLimit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMAccrual__HasCa__6DD96607] DEFAULT ('S'),
[CarryoverLimit] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMAccrual__Carry__6ECD8A40] DEFAULT ((0)),
[HasMaximum] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMAccrual__HasMa__6FC1AE79] DEFAULT ('S'),
[Maximum] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMAccrual__Maxim__70B5D2B2] DEFAULT ((0)),
[UseSchedule] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMAccrual__UseSc__71A9F6EB] DEFAULT ('N'),
[ScheduleID] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HoursPerHourWorked] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMAccrual__Hours__729E1B24] DEFAULT ((0)),
[MaxHoursPerProcess] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMAccrual__MaxHo__73923F5D] DEFAULT ((0)),
[HoursWorked] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMAccrual__Hours__74866396] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_EMAccrual]
      ON [dbo].[EMAccrual]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMAccrual'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'Employee',CONVERT(NVARCHAR(2000),DELETED.[Employee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Employee = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'EmployeeCompany',CONVERT(NVARCHAR(2000),[EmployeeCompany],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'Code',CONVERT(NVARCHAR(2000),[Code],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'HoursPerYear',CONVERT(NVARCHAR(2000),[HoursPerYear],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'ChangeDate',CONVERT(NVARCHAR(2000),[ChangeDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'CurrentEarned',CONVERT(NVARCHAR(2000),[CurrentEarned],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'Override',CONVERT(NVARCHAR(2000),[Override],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'CurrentTaken',CONVERT(NVARCHAR(2000),[CurrentTaken],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'Adjust',CONVERT(NVARCHAR(2000),[Adjust],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'HoursToAccrue',CONVERT(NVARCHAR(2000),[HoursToAccrue],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'HasCarryoverLimit',CONVERT(NVARCHAR(2000),[HasCarryoverLimit],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'CarryoverLimit',CONVERT(NVARCHAR(2000),[CarryoverLimit],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'HasMaximum',CONVERT(NVARCHAR(2000),[HasMaximum],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'Maximum',CONVERT(NVARCHAR(2000),[Maximum],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'UseSchedule',CONVERT(NVARCHAR(2000),[UseSchedule],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'ScheduleID',CONVERT(NVARCHAR(2000),[ScheduleID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'HoursPerHourWorked',CONVERT(NVARCHAR(2000),[HoursPerHourWorked],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'MaxHoursPerProcess',CONVERT(NVARCHAR(2000),[MaxHoursPerProcess],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'HoursWorked',CONVERT(NVARCHAR(2000),[HoursWorked],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_EMAccrual] ON [dbo].[EMAccrual]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_EMAccrual]
      ON [dbo].[EMAccrual]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMAccrual'
    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Employee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Employee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Employee = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'EmployeeCompany',NULL,CONVERT(NVARCHAR(2000),[EmployeeCompany],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Code',NULL,CONVERT(NVARCHAR(2000),[Code],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'HoursPerYear',NULL,CONVERT(NVARCHAR(2000),[HoursPerYear],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'ChangeDate',NULL,CONVERT(NVARCHAR(2000),[ChangeDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'CurrentEarned',NULL,CONVERT(NVARCHAR(2000),[CurrentEarned],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Override',NULL,CONVERT(NVARCHAR(2000),[Override],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'CurrentTaken',NULL,CONVERT(NVARCHAR(2000),[CurrentTaken],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Adjust',NULL,CONVERT(NVARCHAR(2000),[Adjust],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'HoursToAccrue',NULL,CONVERT(NVARCHAR(2000),[HoursToAccrue],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'HasCarryoverLimit',NULL,CONVERT(NVARCHAR(2000),[HasCarryoverLimit],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'CarryoverLimit',NULL,CONVERT(NVARCHAR(2000),[CarryoverLimit],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'HasMaximum',NULL,CONVERT(NVARCHAR(2000),[HasMaximum],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Maximum',NULL,CONVERT(NVARCHAR(2000),[Maximum],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'UseSchedule',NULL,CONVERT(NVARCHAR(2000),[UseSchedule],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'ScheduleID',NULL,CONVERT(NVARCHAR(2000),[ScheduleID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'HoursPerHourWorked',NULL,CONVERT(NVARCHAR(2000),[HoursPerHourWorked],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'MaxHoursPerProcess',NULL,CONVERT(NVARCHAR(2000),[MaxHoursPerProcess],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'HoursWorked',NULL,CONVERT(NVARCHAR(2000),[HoursWorked],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_EMAccrual] ON [dbo].[EMAccrual]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_EMAccrual]
      ON [dbo].[EMAccrual]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMAccrual'
    
     If UPDATE([Employee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Employee',
     CONVERT(NVARCHAR(2000),DELETED.[Employee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Employee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[Employee] Is Null And
				DELETED.[Employee] Is Not Null
			) Or
			(
				INSERTED.[Employee] Is Not Null And
				DELETED.[Employee] Is Null
			) Or
			(
				INSERTED.[Employee] !=
				DELETED.[Employee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Employee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Employee = newDesc.Employee
		END		
		
      If UPDATE([EmployeeCompany])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'EmployeeCompany',
      CONVERT(NVARCHAR(2000),DELETED.[EmployeeCompany],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EmployeeCompany],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[EmployeeCompany] Is Null And
				DELETED.[EmployeeCompany] Is Not Null
			) Or
			(
				INSERTED.[EmployeeCompany] Is Not Null And
				DELETED.[EmployeeCompany] Is Null
			) Or
			(
				INSERTED.[EmployeeCompany] !=
				DELETED.[EmployeeCompany]
			)
		) 
		END		
		
      If UPDATE([Code])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Code',
      CONVERT(NVARCHAR(2000),DELETED.[Code],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Code],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[Code] Is Null And
				DELETED.[Code] Is Not Null
			) Or
			(
				INSERTED.[Code] Is Not Null And
				DELETED.[Code] Is Null
			) Or
			(
				INSERTED.[Code] !=
				DELETED.[Code]
			)
		) 
		END		
		
      If UPDATE([HoursPerYear])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'HoursPerYear',
      CONVERT(NVARCHAR(2000),DELETED.[HoursPerYear],121),
      CONVERT(NVARCHAR(2000),INSERTED.[HoursPerYear],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[HoursPerYear] Is Null And
				DELETED.[HoursPerYear] Is Not Null
			) Or
			(
				INSERTED.[HoursPerYear] Is Not Null And
				DELETED.[HoursPerYear] Is Null
			) Or
			(
				INSERTED.[HoursPerYear] !=
				DELETED.[HoursPerYear]
			)
		) 
		END		
		
      If UPDATE([ChangeDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'ChangeDate',
      CONVERT(NVARCHAR(2000),DELETED.[ChangeDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ChangeDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[ChangeDate] Is Null And
				DELETED.[ChangeDate] Is Not Null
			) Or
			(
				INSERTED.[ChangeDate] Is Not Null And
				DELETED.[ChangeDate] Is Null
			) Or
			(
				INSERTED.[ChangeDate] !=
				DELETED.[ChangeDate]
			)
		) 
		END		
		
      If UPDATE([CurrentEarned])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'CurrentEarned',
      CONVERT(NVARCHAR(2000),DELETED.[CurrentEarned],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CurrentEarned],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[CurrentEarned] Is Null And
				DELETED.[CurrentEarned] Is Not Null
			) Or
			(
				INSERTED.[CurrentEarned] Is Not Null And
				DELETED.[CurrentEarned] Is Null
			) Or
			(
				INSERTED.[CurrentEarned] !=
				DELETED.[CurrentEarned]
			)
		) 
		END		
		
      If UPDATE([Override])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Override',
      CONVERT(NVARCHAR(2000),DELETED.[Override],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Override],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[Override] Is Null And
				DELETED.[Override] Is Not Null
			) Or
			(
				INSERTED.[Override] Is Not Null And
				DELETED.[Override] Is Null
			) Or
			(
				INSERTED.[Override] !=
				DELETED.[Override]
			)
		) 
		END		
		
      If UPDATE([CurrentTaken])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'CurrentTaken',
      CONVERT(NVARCHAR(2000),DELETED.[CurrentTaken],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CurrentTaken],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[CurrentTaken] Is Null And
				DELETED.[CurrentTaken] Is Not Null
			) Or
			(
				INSERTED.[CurrentTaken] Is Not Null And
				DELETED.[CurrentTaken] Is Null
			) Or
			(
				INSERTED.[CurrentTaken] !=
				DELETED.[CurrentTaken]
			)
		) 
		END		
		
      If UPDATE([Adjust])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Adjust',
      CONVERT(NVARCHAR(2000),DELETED.[Adjust],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Adjust],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[Adjust] Is Null And
				DELETED.[Adjust] Is Not Null
			) Or
			(
				INSERTED.[Adjust] Is Not Null And
				DELETED.[Adjust] Is Null
			) Or
			(
				INSERTED.[Adjust] !=
				DELETED.[Adjust]
			)
		) 
		END		
		
      If UPDATE([HoursToAccrue])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'HoursToAccrue',
      CONVERT(NVARCHAR(2000),DELETED.[HoursToAccrue],121),
      CONVERT(NVARCHAR(2000),INSERTED.[HoursToAccrue],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[HoursToAccrue] Is Null And
				DELETED.[HoursToAccrue] Is Not Null
			) Or
			(
				INSERTED.[HoursToAccrue] Is Not Null And
				DELETED.[HoursToAccrue] Is Null
			) Or
			(
				INSERTED.[HoursToAccrue] !=
				DELETED.[HoursToAccrue]
			)
		) 
		END		
		
      If UPDATE([HasCarryoverLimit])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'HasCarryoverLimit',
      CONVERT(NVARCHAR(2000),DELETED.[HasCarryoverLimit],121),
      CONVERT(NVARCHAR(2000),INSERTED.[HasCarryoverLimit],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[HasCarryoverLimit] Is Null And
				DELETED.[HasCarryoverLimit] Is Not Null
			) Or
			(
				INSERTED.[HasCarryoverLimit] Is Not Null And
				DELETED.[HasCarryoverLimit] Is Null
			) Or
			(
				INSERTED.[HasCarryoverLimit] !=
				DELETED.[HasCarryoverLimit]
			)
		) 
		END		
		
      If UPDATE([CarryoverLimit])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'CarryoverLimit',
      CONVERT(NVARCHAR(2000),DELETED.[CarryoverLimit],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CarryoverLimit],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[CarryoverLimit] Is Null And
				DELETED.[CarryoverLimit] Is Not Null
			) Or
			(
				INSERTED.[CarryoverLimit] Is Not Null And
				DELETED.[CarryoverLimit] Is Null
			) Or
			(
				INSERTED.[CarryoverLimit] !=
				DELETED.[CarryoverLimit]
			)
		) 
		END		
		
      If UPDATE([HasMaximum])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'HasMaximum',
      CONVERT(NVARCHAR(2000),DELETED.[HasMaximum],121),
      CONVERT(NVARCHAR(2000),INSERTED.[HasMaximum],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[HasMaximum] Is Null And
				DELETED.[HasMaximum] Is Not Null
			) Or
			(
				INSERTED.[HasMaximum] Is Not Null And
				DELETED.[HasMaximum] Is Null
			) Or
			(
				INSERTED.[HasMaximum] !=
				DELETED.[HasMaximum]
			)
		) 
		END		
		
      If UPDATE([Maximum])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Maximum',
      CONVERT(NVARCHAR(2000),DELETED.[Maximum],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Maximum],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[Maximum] Is Null And
				DELETED.[Maximum] Is Not Null
			) Or
			(
				INSERTED.[Maximum] Is Not Null And
				DELETED.[Maximum] Is Null
			) Or
			(
				INSERTED.[Maximum] !=
				DELETED.[Maximum]
			)
		) 
		END		
		
      If UPDATE([UseSchedule])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'UseSchedule',
      CONVERT(NVARCHAR(2000),DELETED.[UseSchedule],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UseSchedule],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[UseSchedule] Is Null And
				DELETED.[UseSchedule] Is Not Null
			) Or
			(
				INSERTED.[UseSchedule] Is Not Null And
				DELETED.[UseSchedule] Is Null
			) Or
			(
				INSERTED.[UseSchedule] !=
				DELETED.[UseSchedule]
			)
		) 
		END		
		
      If UPDATE([ScheduleID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'ScheduleID',
      CONVERT(NVARCHAR(2000),DELETED.[ScheduleID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ScheduleID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[ScheduleID] Is Null And
				DELETED.[ScheduleID] Is Not Null
			) Or
			(
				INSERTED.[ScheduleID] Is Not Null And
				DELETED.[ScheduleID] Is Null
			) Or
			(
				INSERTED.[ScheduleID] !=
				DELETED.[ScheduleID]
			)
		) 
		END		
		
      If UPDATE([HoursPerHourWorked])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'HoursPerHourWorked',
      CONVERT(NVARCHAR(2000),DELETED.[HoursPerHourWorked],121),
      CONVERT(NVARCHAR(2000),INSERTED.[HoursPerHourWorked],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[HoursPerHourWorked] Is Null And
				DELETED.[HoursPerHourWorked] Is Not Null
			) Or
			(
				INSERTED.[HoursPerHourWorked] Is Not Null And
				DELETED.[HoursPerHourWorked] Is Null
			) Or
			(
				INSERTED.[HoursPerHourWorked] !=
				DELETED.[HoursPerHourWorked]
			)
		) 
		END		
		
      If UPDATE([MaxHoursPerProcess])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'MaxHoursPerProcess',
      CONVERT(NVARCHAR(2000),DELETED.[MaxHoursPerProcess],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MaxHoursPerProcess],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[MaxHoursPerProcess] Is Null And
				DELETED.[MaxHoursPerProcess] Is Not Null
			) Or
			(
				INSERTED.[MaxHoursPerProcess] Is Not Null And
				DELETED.[MaxHoursPerProcess] Is Null
			) Or
			(
				INSERTED.[MaxHoursPerProcess] !=
				DELETED.[MaxHoursPerProcess]
			)
		) 
		END		
		
      If UPDATE([HoursWorked])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'HoursWorked',
      CONVERT(NVARCHAR(2000),DELETED.[HoursWorked],121),
      CONVERT(NVARCHAR(2000),INSERTED.[HoursWorked],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[HoursWorked] Is Null And
				DELETED.[HoursWorked] Is Not Null
			) Or
			(
				INSERTED.[HoursWorked] Is Not Null And
				DELETED.[HoursWorked] Is Null
			) Or
			(
				INSERTED.[HoursWorked] !=
				DELETED.[HoursWorked]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_EMAccrual] ON [dbo].[EMAccrual]
GO
ALTER TABLE [dbo].[EMAccrual] ADD CONSTRAINT [EMAccrualPK] PRIMARY KEY NONCLUSTERED ([Employee], [EmployeeCompany], [Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
