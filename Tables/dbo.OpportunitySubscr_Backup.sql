CREATE TABLE [dbo].[OpportunitySubscr_Backup]
(
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OpportunitySubscr_Backup] ADD CONSTRAINT [OpportunitySubscrPK] PRIMARY KEY NONCLUSTERED ([PartKey], [OpportunityID], [UserName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
