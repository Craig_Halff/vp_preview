CREATE TABLE [dbo].[CFGTaxCountryRegion]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ISOCountryCode] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Region] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGTaxCountryRegion] ADD CONSTRAINT [CFGTaxCountryRegionPK] PRIMARY KEY CLUSTERED ([Company], [ISOCountryCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
