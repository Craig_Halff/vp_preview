CREATE TABLE [dbo].[CFGEMDegreeDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGEMDegree__Seq__5814421A] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGEMDegreeDescriptions] ADD CONSTRAINT [CFGEMDegreeDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
