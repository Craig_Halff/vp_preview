CREATE TABLE [dbo].[SETab]
(
[Role] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_SETab]
      ON [dbo].[SETab]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SETab'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[TabID],121),'Role',CONVERT(NVARCHAR(2000),[Role],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[TabID],121),'InfoCenterArea',CONVERT(NVARCHAR(2000),[InfoCenterArea],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[TabID],121),'TabID',CONVERT(NVARCHAR(2000),[TabID],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_SETab]
      ON [dbo].[SETab]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SETab'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[TabID],121),'Role',NULL,CONVERT(NVARCHAR(2000),[Role],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[TabID],121),'InfoCenterArea',NULL,CONVERT(NVARCHAR(2000),[InfoCenterArea],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[TabID],121),'TabID',NULL,CONVERT(NVARCHAR(2000),[TabID],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_SETab]
      ON [dbo].[SETab]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SETab'
    
      If UPDATE([Role])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[TabID],121),'Role',
      CONVERT(NVARCHAR(2000),DELETED.[Role],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Role],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[InfoCenterArea] = DELETED.[InfoCenterArea] AND INSERTED.[TabID] = DELETED.[TabID] AND 
		(
			(
				INSERTED.[Role] Is Null And
				DELETED.[Role] Is Not Null
			) Or
			(
				INSERTED.[Role] Is Not Null And
				DELETED.[Role] Is Null
			) Or
			(
				INSERTED.[Role] !=
				DELETED.[Role]
			)
		) 
		END		
		
      If UPDATE([InfoCenterArea])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[TabID],121),'InfoCenterArea',
      CONVERT(NVARCHAR(2000),DELETED.[InfoCenterArea],121),
      CONVERT(NVARCHAR(2000),INSERTED.[InfoCenterArea],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[InfoCenterArea] = DELETED.[InfoCenterArea] AND INSERTED.[TabID] = DELETED.[TabID] AND 
		(
			(
				INSERTED.[InfoCenterArea] Is Null And
				DELETED.[InfoCenterArea] Is Not Null
			) Or
			(
				INSERTED.[InfoCenterArea] Is Not Null And
				DELETED.[InfoCenterArea] Is Null
			) Or
			(
				INSERTED.[InfoCenterArea] !=
				DELETED.[InfoCenterArea]
			)
		) 
		END		
		
      If UPDATE([TabID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[TabID],121),'TabID',
      CONVERT(NVARCHAR(2000),DELETED.[TabID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TabID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[InfoCenterArea] = DELETED.[InfoCenterArea] AND INSERTED.[TabID] = DELETED.[TabID] AND 
		(
			(
				INSERTED.[TabID] Is Null And
				DELETED.[TabID] Is Not Null
			) Or
			(
				INSERTED.[TabID] Is Not Null And
				DELETED.[TabID] Is Null
			) Or
			(
				INSERTED.[TabID] !=
				DELETED.[TabID]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[SETab] ADD CONSTRAINT [SETabPK] PRIMARY KEY NONCLUSTERED ([Role], [InfoCenterArea], [TabID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
