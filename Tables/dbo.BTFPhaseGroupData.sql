CREATE TABLE [dbo].[BTFPhaseGroupData]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__BTFPhaseGro__Seq__6DE387A8] DEFAULT ((0)),
[PhaseGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrintHeader] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BTFPhaseG__Print__6ED7ABE1] DEFAULT ('N'),
[PrintSubTotal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BTFPhaseG__Print__6FCBD01A] DEFAULT ('N'),
[HideDetail] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BTFPhaseG__HideD__70BFF453] DEFAULT ('N'),
[AddSpaceAfter] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BTFPhaseG__AddSp__71B4188C] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTFPhaseGroupData] ADD CONSTRAINT [BTFPhaseGroupDataPK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [Seq], [PhaseGroup]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
