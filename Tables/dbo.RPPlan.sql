CREATE TABLE [dbo].[RPPlan]
(
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlanNumber] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjMgr] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Principal] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Supervisor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[BaselineLaborHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__6F4356E3] DEFAULT ((0)),
[BaselineLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__70377B1C] DEFAULT ((0)),
[BaselineLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__712B9F55] DEFAULT ((0)),
[BaselineExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__721FC38E] DEFAULT ((0)),
[BaselineExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__7313E7C7] DEFAULT ((0)),
[BaselineConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__74080C00] DEFAULT ((0)),
[BaselineConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__74FC3039] DEFAULT ((0)),
[BaselineRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__75F05472] DEFAULT ((0)),
[BaselineStart] [datetime] NULL,
[BaselineFinish] [datetime] NULL,
[PlannedLaborHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Plann__76E478AB] DEFAULT ((0)),
[PlannedLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Plann__77D89CE4] DEFAULT ((0)),
[PlannedLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Plann__78CCC11D] DEFAULT ((0)),
[PlannedExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Plann__79C0E556] DEFAULT ((0)),
[PlannedExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Plann__7AB5098F] DEFAULT ((0)),
[PlannedConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Plann__7BA92DC8] DEFAULT ((0)),
[PlannedConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Plann__7C9D5201] DEFAULT ((0)),
[LabRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__LabRe__7D91763A] DEFAULT ((0)),
[PlannedDirExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Plann__7E859A73] DEFAULT ((0)),
[PlannedDirExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Plann__7F79BEAC] DEFAULT ((0)),
[PlannedDirConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Plann__006DE2E5] DEFAULT ((0)),
[PlannedDirConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Plann__0162071E] DEFAULT ((0)),
[CostRtMethod] [smallint] NOT NULL CONSTRAINT [DF__RPPlan_Ne__CostR__02562B57] DEFAULT ((0)),
[BillingRtMethod] [smallint] NOT NULL CONSTRAINT [DF__RPPlan_Ne__Billi__034A4F90] DEFAULT ((0)),
[CostRtTableNo] [int] NOT NULL CONSTRAINT [DF__RPPlan_Ne__CostR__043E73C9] DEFAULT ((0)),
[BillingRtTableNo] [int] NOT NULL CONSTRAINT [DF__RPPlan_Ne__Billi__05329802] DEFAULT ((0)),
[CompensationFee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Compe__0626BC3B] DEFAULT ((0)),
[ConsultantFee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Consu__071AE074] DEFAULT ((0)),
[ContingencyPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Conti__080F04AD] DEFAULT ((0)),
[ContingencyAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Conti__090328E6] DEFAULT ((0)),
[OverheadPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Overh__09F74D1F] DEFAULT ((0)),
[AvailableFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan_Ne__Avail__0AEB7158] DEFAULT ('Y'),
[GenResTableNo] [int] NOT NULL CONSTRAINT [DF__RPPlan_Ne__GenRe__0BDF9591] DEFAULT ((0)),
[UnPostedFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan_Ne__UnPos__0CD3B9CA] DEFAULT ('N'),
[BudgetType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan_Ne__Budge__0DC7DE03] DEFAULT ('C'),
[PctCompleteFormula] [smallint] NOT NULL CONSTRAINT [DF__RPPlan_Ne__PctCo__0EBC023C] DEFAULT ((0)),
[PctComplete] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__PctCo__0FB02675] DEFAULT ((0)),
[PctCompleteBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__PctCo__10A44AAE] DEFAULT ((0)),
[PctCompleteLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__PctCo__11986EE7] DEFAULT ((0)),
[PctCompleteLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__PctCo__128C9320] DEFAULT ((0)),
[PctCompleteExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__PctCo__1380B759] DEFAULT ((0)),
[PctCompleteExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__PctCo__1474DB92] DEFAULT ((0)),
[PctCompleteConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__PctCo__1568FFCB] DEFAULT ((0)),
[PctCompleteConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__PctCo__165D2404] DEFAULT ((0)),
[WeightedLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Weigh__1751483D] DEFAULT ((0)),
[WeightedLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Weigh__18456C76] DEFAULT ((0)),
[WeightedExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Weigh__193990AF] DEFAULT ((0)),
[WeightedExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Weigh__1A2DB4E8] DEFAULT ((0)),
[WeightedConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Weigh__1B21D921] DEFAULT ((0)),
[WeightedConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Weigh__1C15FD5A] DEFAULT ((0)),
[UtilizationIncludeFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan_Ne__Utili__1D0A2193] DEFAULT ('Y'),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReimbAllowance] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Reimb__1DFE45CC] DEFAULT ((0)),
[Probability] [smallint] NOT NULL CONSTRAINT [DF__RPPlan_Ne__Proba__1EF26A05] DEFAULT ((0)),
[LabMultType] [smallint] NOT NULL CONSTRAINT [DF__RPPlan_Ne__LabMu__1FE68E3E] DEFAULT ((0)),
[Multiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Multi__20DAB277] DEFAULT ((0)),
[ProjectedMultiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Proje__21CED6B0] DEFAULT ((0)),
[ProjectedRatio] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Proje__22C2FAE9] DEFAULT ((0)),
[CalcExpBillAmtFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan_Ne__CalcE__23B71F22] DEFAULT ('N'),
[CalcConBillAmtFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan_Ne__CalcC__24AB435B] DEFAULT ('N'),
[ExpBillRtMethod] [smallint] NOT NULL CONSTRAINT [DF__RPPlan_Ne__ExpBi__259F6794] DEFAULT ((0)),
[ExpBillRtTableNo] [int] NOT NULL CONSTRAINT [DF__RPPlan_Ne__ExpBi__26938BCD] DEFAULT ((0)),
[ConBillRtMethod] [smallint] NOT NULL CONSTRAINT [DF__RPPlan_Ne__ConBi__2787B006] DEFAULT ((0)),
[ConBillRtTableNo] [int] NOT NULL CONSTRAINT [DF__RPPlan_Ne__ConBi__287BD43F] DEFAULT ((0)),
[GRBillTableNo] [int] NOT NULL CONSTRAINT [DF__RPPlan_Ne__GRBil__296FF878] DEFAULT ((0)),
[ExpBillMultiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__ExpBi__2A641CB1] DEFAULT ((0)),
[ConBillMultiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__ConBi__2B5840EA] DEFAULT ((0)),
[ReimbMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan_Ne__Reimb__2C4C6523] DEFAULT ('C'),
[ExpRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__ExpRe__2D40895C] DEFAULT ((0)),
[ConRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__ConRe__2E34AD95] DEFAULT ((0)),
[TargetMultCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Targe__2F28D1CE] DEFAULT ((0)),
[TargetMultBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Targe__301CF607] DEFAULT ((0)),
[BaselineUntQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__31111A40] DEFAULT ((0)),
[BaselineUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__32053E79] DEFAULT ((0)),
[BaselineUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__32F962B2] DEFAULT ((0)),
[PlannedUntQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Plann__33ED86EB] DEFAULT ((0)),
[PlannedUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Plann__34E1AB24] DEFAULT ((0)),
[PlannedUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Plann__35D5CF5D] DEFAULT ((0)),
[PctCompleteUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__PctCo__36C9F396] DEFAULT ((0)),
[PctCompleteUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__PctCo__37BE17CF] DEFAULT ((0)),
[PlannedDirUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Plann__38B23C08] DEFAULT ((0)),
[PlannedDirUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Plann__39A66041] DEFAULT ((0)),
[WeightedUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Weigh__3A9A847A] DEFAULT ((0)),
[WeightedUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Weigh__3B8EA8B3] DEFAULT ((0)),
[UntRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__UntRe__3C82CCEC] DEFAULT ((0)),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan_Ne__Statu__3D76F125] DEFAULT ('A'),
[PctComplByPeriodFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan_Ne__PctCo__3E6B155E] DEFAULT ('N'),
[RevenueMethod] [smallint] NOT NULL CONSTRAINT [DF__RPPlan_Ne__Reven__3F5F3997] DEFAULT ((1)),
[AnalysisBasis] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan_Ne__Analy__40535DD0] DEFAULT ('P'),
[GRMethod] [smallint] NOT NULL CONSTRAINT [DF__RPPlan_Ne__GRMet__41478209] DEFAULT ((0)),
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartingDayOfWeek] [smallint] NOT NULL CONSTRAINT [DF__RPPlan_Ne__Start__423BA642] DEFAULT ((2)),
[CostCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BillingCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CompensationFeeBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Compe__432FCA7B] DEFAULT ((0)),
[ConsultantFeeBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Consu__4423EEB4] DEFAULT ((0)),
[ReimbAllowanceBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Reimb__451812ED] DEFAULT ((0)),
[EVFormula] [smallint] NOT NULL CONSTRAINT [DF__RPPlan_Ne__EVFor__460C3726] DEFAULT ((0)),
[LabBillMultiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__LabBi__47005B5F] DEFAULT ((1)),
[UntBillMultiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__UntBi__47F47F98] DEFAULT ((1)),
[BaselineDirExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__48E8A3D1] DEFAULT ((0)),
[BaselineDirExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__49DCC80A] DEFAULT ((0)),
[BaselineDirConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__4AD0EC43] DEFAULT ((0)),
[BaselineDirConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__4BC5107C] DEFAULT ((0)),
[BaselineDirUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__4CB934B5] DEFAULT ((0)),
[BaselineDirUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Basel__4DAD58EE] DEFAULT ((0)),
[CommitmentFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan_Ne__Commi__4EA17D27] DEFAULT ('N'),
[TopdownFee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Topdo__4F95A160] DEFAULT ((0)),
[JTDLabHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__JTDLa__5089C599] DEFAULT ((0)),
[JTDLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__JTDLa__517DE9D2] DEFAULT ((0)),
[JTDLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__JTDLa__52720E0B] DEFAULT ((0)),
[JTDExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__JTDEx__53663244] DEFAULT ((0)),
[JTDExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__JTDEx__545A567D] DEFAULT ((0)),
[JTDConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__JTDCo__554E7AB6] DEFAULT ((0)),
[JTDConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__JTDCo__56429EEF] DEFAULT ((0)),
[JTDUntQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__JTDUn__5736C328] DEFAULT ((0)),
[JTDUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__JTDUn__582AE761] DEFAULT ((0)),
[JTDUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__JTDUn__591F0B9A] DEFAULT ((0)),
[JTDRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__JTDRe__5A132FD3] DEFAULT ((0)),
[NeedsRecalc] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan_Ne__Needs__5B07540C] DEFAULT ('N'),
[CheckedOutUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckedOutDate] [datetime] NULL,
[CheckedOutID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConWBSLevel] [smallint] NOT NULL CONSTRAINT [DF__RPPlan_Ne__ConWB__5BFB7845] DEFAULT ((3)),
[ExpWBSLevel] [smallint] NOT NULL CONSTRAINT [DF__RPPlan_Ne__ExpWB__5CEF9C7E] DEFAULT ((3)),
[CompensationFeeDirLab] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Compe__5DE3C0B7] DEFAULT ((0)),
[CompensationFeeDirExp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Compe__5ED7E4F0] DEFAULT ((0)),
[ReimbAllowanceExp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Reimb__5FCC0929] DEFAULT ((0)),
[ReimbAllowanceCon] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Reimb__60C02D62] DEFAULT ((0)),
[CompensationFeeDirLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Compe__61B4519B] DEFAULT ((0)),
[CompensationFeeDirExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Compe__62A875D4] DEFAULT ((0)),
[ReimbAllowanceExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Reimb__639C9A0D] DEFAULT ((0)),
[ReimbAllowanceConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlan_Ne__Reimb__6490BE46] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CostGRRtMethod] [smallint] NOT NULL CONSTRAINT [DF__RPPlan__CostGRRt__5A73EDDD] DEFAULT ((0)),
[BillGRRtMethod] [smallint] NOT NULL CONSTRAINT [DF__RPPlan__BillGRRt__5B681216] DEFAULT ((0)),
[VersionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LCLevel1Enabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan__LCLevel1__1A796459] DEFAULT ('Y'),
[LCLevel2Enabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan__LCLevel2__1B6D8892] DEFAULT ('Y'),
[LCLevel3Enabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan__LCLevel3__1C61ACCB] DEFAULT ('Y'),
[LCLevel4Enabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan__LCLevel4__1D55D104] DEFAULT ('Y'),
[LCLevel5Enabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan__LCLevel5__1E49F53D] DEFAULT ('Y'),
[StoredCurrency] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlan__StoredCu__2402CE93] DEFAULT ('P'),
[UntWBSLevel] [smallint] NOT NULL CONSTRAINT [DF__RPPlan__UntWBSLe__7A777472] DEFAULT ((1))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteRPPlanTrigger] ON [dbo].[RPPlan] FOR DELETE
AS
  delete from RPPlanSubscr where PlanID in (select deleted.PlanID from deleted )
GO
ALTER TABLE [dbo].[RPPlan] ADD CONSTRAINT [RPPlanPK] PRIMARY KEY NONCLUSTERED ([PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPPlanOpportunityIDIDX] ON [dbo].[RPPlan] ([OpportunityID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPPlanUtilizationIncludeFlgIDX] ON [dbo].[RPPlan] ([UtilizationIncludeFlg]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPPlanWBS1IDX] ON [dbo].[RPPlan] ([WBS1]) INCLUDE ([PlanName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
