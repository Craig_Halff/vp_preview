CREATE TABLE [dbo].[cvDetail]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CheckNo] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__cvDetail_Ne__Seq__71DF0115] DEFAULT ((0)),
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__cvDetail___Amoun__72D3254E] DEFAULT ((0)),
[SuppressBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__cvDetail___Suppr__73C74987] DEFAULT ('N'),
[NetAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__cvDetail___NetAm__74BB6DC0] DEFAULT ((0)),
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__cvDetail___Curre__75AF91F9] DEFAULT ((0)),
[PaymentAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__cvDetail___Payme__76A3B632] DEFAULT ((0)),
[ExpenseCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginatingVendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cvDetail] ADD CONSTRAINT [cvDetailPK] PRIMARY KEY NONCLUSTERED ([Batch], [CheckNo], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
