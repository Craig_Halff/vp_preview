CREATE TABLE [dbo].[CFGCurrencyDownloadCode]
(
[FromCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ToCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCurrencyDownloadCode] ADD CONSTRAINT [CFGCurrencyDownloadCodePK] PRIMARY KEY CLUSTERED ([FromCode], [ToCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
