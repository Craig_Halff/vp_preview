CREATE TABLE [dbo].[BTFPhaseGroupDescriptions]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__BTFPhaseGro__Seq__739C60FE] DEFAULT ((0)),
[PhaseGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTFPhaseGroupDescriptions] ADD CONSTRAINT [BTFPhaseGroupDescriptionsPK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [Seq], [PhaseGroup], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
