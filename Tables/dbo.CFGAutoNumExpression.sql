CREATE TABLE [dbo].[CFGAutoNumExpression]
(
[RuleID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NavID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChargeType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAutoNu__Charg__28CF434C] DEFAULT (' '),
[Item] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartPosition] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGAutoNu__Start__29C36785] DEFAULT ((0)),
[LengthUsed] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGAutoNu__Lengt__2AB78BBE] DEFAULT ((0)),
[DefaultValue] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultValueDesc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResetSeq] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__CFGAutoNu__Reset__2BABAFF7] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAutoNumExpression] ADD CONSTRAINT [CFGAutoNumExpressionPK] PRIMARY KEY CLUSTERED ([RuleID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
