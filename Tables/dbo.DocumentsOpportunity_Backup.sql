CREATE TABLE [dbo].[DocumentsOpportunity_Backup]
(
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ListID] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FolderURL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DocID] [int] NOT NULL CONSTRAINT [DF__Documents__DocID__2EE80F55] DEFAULT ((0)),
[DocURL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DocumentsOpportunity_Backup] ADD CONSTRAINT [DocumentsOpportunityPK] PRIMARY KEY NONCLUSTERED ([OpportunityID], [ListID], [FolderURL], [DocID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
