CREATE TABLE [dbo].[ItemPhoto]
(
[Item] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Photo] [varbinary] (max) NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ItemPhoto] ADD CONSTRAINT [ItemPhotoPK] PRIMARY KEY CLUSTERED ([Item]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
