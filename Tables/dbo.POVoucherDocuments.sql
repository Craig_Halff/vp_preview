CREATE TABLE [dbo].[POVoucherDocuments]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL,
[AssociateNew] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POVoucher__Assoc__40313633] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__POVoucherDo__Seq__41255A6C] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POVoucherDocuments] ADD CONSTRAINT [POVoucherDocumentsPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
