CREATE TABLE [dbo].[GLParentHeading]
(
[GLGroup] [smallint] NOT NULL CONSTRAINT [DF__GLParentH__GLGro__77E09279] DEFAULT ((0)),
[TableNo] [smallint] NOT NULL CONSTRAINT [DF__GLParentH__Table__78D4B6B2] DEFAULT ((0)),
[ExcludeTotal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__GLParentH__Exclu__79C8DAEB] DEFAULT ('N'),
[ShowDetail] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__GLParentH__ShowD__7ABCFF24] DEFAULT ('Y'),
[SubTotal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__GLParentH__SubTo__7BB1235D] DEFAULT ('R'),
[SortOrder] [smallint] NOT NULL CONSTRAINT [DF__GLParentH__SortO__7CA54796] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GLParentHeading] ADD CONSTRAINT [GLParentHeadingPK] PRIMARY KEY CLUSTERED ([GLGroup], [TableNo]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
