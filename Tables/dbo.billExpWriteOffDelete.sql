CREATE TABLE [dbo].[billExpWriteOffDelete]
(
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OriginalTable] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OriginalPeriod] [int] NOT NULL CONSTRAINT [DF__billExpWr__Origi__79353EC3] DEFAULT ((0)),
[OriginalPostSeq] [int] NOT NULL CONSTRAINT [DF__billExpWr__Origi__7A2962FC] DEFAULT ((0)),
[OriginalPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PostWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PostWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PostWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[billExpWriteOffDelete] ADD CONSTRAINT [billExpWriteOffDeletePK] PRIMARY KEY NONCLUSTERED ([Invoice], [MainWBS1], [OriginalTable], [OriginalPeriod], [OriginalPostSeq], [OriginalPKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
