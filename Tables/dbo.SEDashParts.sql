CREATE TABLE [dbo].[SEDashParts]
(
[Role] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Partid] [int] NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SEDashParts] ADD CONSTRAINT [SEDashPartsPK] PRIMARY KEY NONCLUSTERED ([Role], [Partid]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
