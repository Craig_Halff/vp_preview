CREATE TABLE [dbo].[FW_CFGCurrencyData]
(
[Code] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DecimalPlaces] [smallint] NOT NULL CONSTRAINT [DF__FW_CFGCur__Decim__72540261] DEFAULT ((0)),
[CurrencySymbol] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGCurrencyData] ADD CONSTRAINT [FW_CFGCurrencyDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
