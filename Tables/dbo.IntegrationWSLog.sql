CREATE TABLE [dbo].[IntegrationWSLog]
(
[ProcessID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProcessStatus] [smallint] NOT NULL CONSTRAINT [DF__Integrati__Proce__30A63A95] DEFAULT ((0)),
[TimeStarted] [datetime] NULL,
[TimeEnded] [datetime] NULL,
[Submitter] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IntegrationWSLog] ADD CONSTRAINT [IntegrationWSLogPK] PRIMARY KEY CLUSTERED ([ProcessID], [Type]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
