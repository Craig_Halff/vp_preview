CREATE TABLE [dbo].[cvDetailCustomFields]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CheckNo] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cvDetailCustomFields] ADD CONSTRAINT [cvDetailCustomFieldsPK] PRIMARY KEY NONCLUSTERED ([Batch], [CheckNo], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
