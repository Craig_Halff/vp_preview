CREATE TABLE [dbo].[CustomDashpart]
(
[DashpartID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JsonDefinition] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CustomDas__JsonD__35AC7D82] DEFAULT ('{}'),
[JsonOptions] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CustomDas__JsonO__36A0A1BB] DEFAULT ('{}'),
[IsSystemDashpart] [smallint] NOT NULL CONSTRAINT [DF__CustomDas__IsSys__3794C5F4] DEFAULT ((0)),
[LicenseRestrictions] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomDashpart] ADD CONSTRAINT [CustomDashpartPK] PRIMARY KEY CLUSTERED ([DashpartID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
