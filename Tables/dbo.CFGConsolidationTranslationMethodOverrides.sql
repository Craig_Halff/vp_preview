CREATE TABLE [dbo].[CFGConsolidationTranslationMethodOverrides]
(
[ReportingGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TranslationMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGConsolidationTranslationMethodOverrides] ADD CONSTRAINT [CFGConsolidationTranslationMethodOverridesPK] PRIMARY KEY CLUSTERED ([ReportingGroup], [Account]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
