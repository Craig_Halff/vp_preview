CREATE TABLE [dbo].[EMPayrollWithholding]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Method] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__AmtPc__1B361C63] DEFAULT ((0)),
[Suppress] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Suppr__1C2A409C] DEFAULT ('A'),
[FilingStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Exemptions] [smallint] NOT NULL CONSTRAINT [DF__EMPayroll__Exemp__1D1E64D5] DEFAULT ((0)),
[AdditionalAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Addit__1E12890E] DEFAULT ((0)),
[OtherExemptions] [smallint] NOT NULL CONSTRAINT [DF__EMPayroll__Other__1F06AD47] DEFAULT ((0)),
[Limit] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Limit__1FFAD180] DEFAULT ((0)),
[CurrentAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Curre__20EEF5B9] DEFAULT ((0)),
[Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Overr__21E319F2] DEFAULT ('N'),
[Adjust] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Adjus__22D73E2B] DEFAULT ((0)),
[PrintOnCheck] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Print__23CB6264] DEFAULT ('P'),
[TaxablePayBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Taxab__24BF869D] DEFAULT ((0)),
[AdjustedGrossPayBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Adjus__25B3AAD6] DEFAULT ((0)),
[Amt401K] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Amt40__26A7CF0F] DEFAULT ((0)),
[Amt125] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Amt12__279BF348] DEFAULT ((0)),
[Exclude401k] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Exclu__28901781] DEFAULT ('N'),
[ExcludeCafeteria] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Exclu__29843BBA] DEFAULT ('N'),
[ExcludeOtherPay1] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Exclu__2A785FF3] DEFAULT ('N'),
[ExcludeOtherPay2] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Exclu__2B6C842C] DEFAULT ('N'),
[ExcludeOtherPay3] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Exclu__2C60A865] DEFAULT ('N'),
[ExcludeOtherPay4] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Exclu__2D54CC9E] DEFAULT ('N'),
[ExcludeOtherPay5] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Exclu__2E48F0D7] DEFAULT ('N'),
[OtherPay1] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__2F3D1510] DEFAULT ((0)),
[OtherPay2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__30313949] DEFAULT ((0)),
[OtherPay3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__31255D82] DEFAULT ((0)),
[OtherPay4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__321981BB] DEFAULT ((0)),
[OtherPay5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__330DA5F4] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[FormW4Version] [smallint] NOT NULL CONSTRAINT [DF__EMPayroll__FormW__35EA129F] DEFAULT ((0)),
[FormW4Step2] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__FormW__36DE36D8] DEFAULT ('N'),
[FormW4Dependents] [smallint] NOT NULL CONSTRAINT [DF__EMPayroll__FormW__37D25B11] DEFAULT ((0)),
[FormW4DependentsOther] [smallint] NOT NULL CONSTRAINT [DF__EMPayroll__FormW__38C67F4A] DEFAULT ((0)),
[FormW4OtherTaxCredit] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__FormW__39BAA383] DEFAULT ((0)),
[FormW4OtherIncome] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__FormW__3AAEC7BC] DEFAULT ((0)),
[FormW4Deductions] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__FormW__3BA2EBF5] DEFAULT ((0)),
[FormW4DependentsAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__FormW__3C97102E] DEFAULT ((0)),
[FormW4DependentsOtherAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__FormW__3D8B3467] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_EMPayrollWithholding]
      ON [dbo].[EMPayrollWithholding]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMPayrollWithholding'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'Employee',CONVERT(NVARCHAR(2000),DELETED.[Employee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Employee = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'EmployeeCompany',CONVERT(NVARCHAR(2000),[EmployeeCompany],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'Code',CONVERT(NVARCHAR(2000),[Code],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'Method',CONVERT(NVARCHAR(2000),[Method],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'AmtPct',CONVERT(NVARCHAR(2000),[AmtPct],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'Suppress',CONVERT(NVARCHAR(2000),[Suppress],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'FilingStatus',CONVERT(NVARCHAR(2000),[FilingStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'Exemptions',CONVERT(NVARCHAR(2000),[Exemptions],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'AdditionalAmt',CONVERT(NVARCHAR(2000),[AdditionalAmt],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'OtherExemptions',CONVERT(NVARCHAR(2000),[OtherExemptions],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'Limit',CONVERT(NVARCHAR(2000),[Limit],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'CurrentAmt',CONVERT(NVARCHAR(2000),[CurrentAmt],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'Override',CONVERT(NVARCHAR(2000),[Override],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'Adjust',CONVERT(NVARCHAR(2000),[Adjust],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'PrintOnCheck',CONVERT(NVARCHAR(2000),[PrintOnCheck],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'TaxablePayBasis',CONVERT(NVARCHAR(2000),[TaxablePayBasis],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'AdjustedGrossPayBasis',CONVERT(NVARCHAR(2000),[AdjustedGrossPayBasis],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'Amt401K',CONVERT(NVARCHAR(2000),[Amt401K],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'Amt125',CONVERT(NVARCHAR(2000),[Amt125],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'Exclude401k',CONVERT(NVARCHAR(2000),[Exclude401k],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'ExcludeCafeteria',CONVERT(NVARCHAR(2000),[ExcludeCafeteria],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'ExcludeOtherPay1',CONVERT(NVARCHAR(2000),[ExcludeOtherPay1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'ExcludeOtherPay2',CONVERT(NVARCHAR(2000),[ExcludeOtherPay2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'ExcludeOtherPay3',CONVERT(NVARCHAR(2000),[ExcludeOtherPay3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'ExcludeOtherPay4',CONVERT(NVARCHAR(2000),[ExcludeOtherPay4],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'ExcludeOtherPay5',CONVERT(NVARCHAR(2000),[ExcludeOtherPay5],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'OtherPay1',CONVERT(NVARCHAR(2000),[OtherPay1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'OtherPay2',CONVERT(NVARCHAR(2000),[OtherPay2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'OtherPay3',CONVERT(NVARCHAR(2000),[OtherPay3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'OtherPay4',CONVERT(NVARCHAR(2000),[OtherPay4],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'OtherPay5',CONVERT(NVARCHAR(2000),[OtherPay5],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'FormW4Version',CONVERT(NVARCHAR(2000),[FormW4Version],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'FormW4Step2',CONVERT(NVARCHAR(2000),[FormW4Step2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'FormW4Dependents',CONVERT(NVARCHAR(2000),[FormW4Dependents],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'FormW4DependentsOther',CONVERT(NVARCHAR(2000),[FormW4DependentsOther],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'FormW4OtherTaxCredit',CONVERT(NVARCHAR(2000),[FormW4OtherTaxCredit],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'FormW4OtherIncome',CONVERT(NVARCHAR(2000),[FormW4OtherIncome],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'FormW4Deductions',CONVERT(NVARCHAR(2000),[FormW4Deductions],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'FormW4DependentsAmount',CONVERT(NVARCHAR(2000),[FormW4DependentsAmount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121),'FormW4DependentsOtherAmount',CONVERT(NVARCHAR(2000),[FormW4DependentsOtherAmount],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_EMPayrollWithholding] ON [dbo].[EMPayrollWithholding]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_EMPayrollWithholding]
      ON [dbo].[EMPayrollWithholding]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMPayrollWithholding'
    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Employee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Employee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Employee = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'EmployeeCompany',NULL,CONVERT(NVARCHAR(2000),[EmployeeCompany],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Code',NULL,CONVERT(NVARCHAR(2000),[Code],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Method',NULL,CONVERT(NVARCHAR(2000),[Method],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'AmtPct',NULL,CONVERT(NVARCHAR(2000),[AmtPct],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Suppress',NULL,CONVERT(NVARCHAR(2000),[Suppress],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FilingStatus',NULL,CONVERT(NVARCHAR(2000),[FilingStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Exemptions',NULL,CONVERT(NVARCHAR(2000),[Exemptions],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'AdditionalAmt',NULL,CONVERT(NVARCHAR(2000),[AdditionalAmt],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'OtherExemptions',NULL,CONVERT(NVARCHAR(2000),[OtherExemptions],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Limit',NULL,CONVERT(NVARCHAR(2000),[Limit],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'CurrentAmt',NULL,CONVERT(NVARCHAR(2000),[CurrentAmt],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Override',NULL,CONVERT(NVARCHAR(2000),[Override],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Adjust',NULL,CONVERT(NVARCHAR(2000),[Adjust],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'PrintOnCheck',NULL,CONVERT(NVARCHAR(2000),[PrintOnCheck],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'TaxablePayBasis',NULL,CONVERT(NVARCHAR(2000),[TaxablePayBasis],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'AdjustedGrossPayBasis',NULL,CONVERT(NVARCHAR(2000),[AdjustedGrossPayBasis],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Amt401K',NULL,CONVERT(NVARCHAR(2000),[Amt401K],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Amt125',NULL,CONVERT(NVARCHAR(2000),[Amt125],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Exclude401k',NULL,CONVERT(NVARCHAR(2000),[Exclude401k],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'ExcludeCafeteria',NULL,CONVERT(NVARCHAR(2000),[ExcludeCafeteria],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'ExcludeOtherPay1',NULL,CONVERT(NVARCHAR(2000),[ExcludeOtherPay1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'ExcludeOtherPay2',NULL,CONVERT(NVARCHAR(2000),[ExcludeOtherPay2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'ExcludeOtherPay3',NULL,CONVERT(NVARCHAR(2000),[ExcludeOtherPay3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'ExcludeOtherPay4',NULL,CONVERT(NVARCHAR(2000),[ExcludeOtherPay4],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'ExcludeOtherPay5',NULL,CONVERT(NVARCHAR(2000),[ExcludeOtherPay5],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'OtherPay1',NULL,CONVERT(NVARCHAR(2000),[OtherPay1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'OtherPay2',NULL,CONVERT(NVARCHAR(2000),[OtherPay2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'OtherPay3',NULL,CONVERT(NVARCHAR(2000),[OtherPay3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'OtherPay4',NULL,CONVERT(NVARCHAR(2000),[OtherPay4],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'OtherPay5',NULL,CONVERT(NVARCHAR(2000),[OtherPay5],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4Version',NULL,CONVERT(NVARCHAR(2000),[FormW4Version],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4Step2',NULL,CONVERT(NVARCHAR(2000),[FormW4Step2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4Dependents',NULL,CONVERT(NVARCHAR(2000),[FormW4Dependents],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4DependentsOther',NULL,CONVERT(NVARCHAR(2000),[FormW4DependentsOther],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4OtherTaxCredit',NULL,CONVERT(NVARCHAR(2000),[FormW4OtherTaxCredit],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4OtherIncome',NULL,CONVERT(NVARCHAR(2000),[FormW4OtherIncome],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4Deductions',NULL,CONVERT(NVARCHAR(2000),[FormW4Deductions],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4DependentsAmount',NULL,CONVERT(NVARCHAR(2000),[FormW4DependentsAmount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4DependentsOtherAmount',NULL,CONVERT(NVARCHAR(2000),[FormW4DependentsOtherAmount],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_EMPayrollWithholding] ON [dbo].[EMPayrollWithholding]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_EMPayrollWithholding]
      ON [dbo].[EMPayrollWithholding]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMPayrollWithholding'
    
     If UPDATE([Employee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Employee',
     CONVERT(NVARCHAR(2000),DELETED.[Employee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Employee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[Employee] Is Null And
				DELETED.[Employee] Is Not Null
			) Or
			(
				INSERTED.[Employee] Is Not Null And
				DELETED.[Employee] Is Null
			) Or
			(
				INSERTED.[Employee] !=
				DELETED.[Employee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Employee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Employee = newDesc.Employee
		END		
		
      If UPDATE([EmployeeCompany])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'EmployeeCompany',
      CONVERT(NVARCHAR(2000),DELETED.[EmployeeCompany],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EmployeeCompany],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[EmployeeCompany] Is Null And
				DELETED.[EmployeeCompany] Is Not Null
			) Or
			(
				INSERTED.[EmployeeCompany] Is Not Null And
				DELETED.[EmployeeCompany] Is Null
			) Or
			(
				INSERTED.[EmployeeCompany] !=
				DELETED.[EmployeeCompany]
			)
		) 
		END		
		
      If UPDATE([Code])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Code',
      CONVERT(NVARCHAR(2000),DELETED.[Code],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Code],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[Code] Is Null And
				DELETED.[Code] Is Not Null
			) Or
			(
				INSERTED.[Code] Is Not Null And
				DELETED.[Code] Is Null
			) Or
			(
				INSERTED.[Code] !=
				DELETED.[Code]
			)
		) 
		END		
		
      If UPDATE([Method])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Method',
      CONVERT(NVARCHAR(2000),DELETED.[Method],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Method],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[Method] Is Null And
				DELETED.[Method] Is Not Null
			) Or
			(
				INSERTED.[Method] Is Not Null And
				DELETED.[Method] Is Null
			) Or
			(
				INSERTED.[Method] !=
				DELETED.[Method]
			)
		) 
		END		
		
      If UPDATE([AmtPct])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'AmtPct',
      CONVERT(NVARCHAR(2000),DELETED.[AmtPct],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AmtPct],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[AmtPct] Is Null And
				DELETED.[AmtPct] Is Not Null
			) Or
			(
				INSERTED.[AmtPct] Is Not Null And
				DELETED.[AmtPct] Is Null
			) Or
			(
				INSERTED.[AmtPct] !=
				DELETED.[AmtPct]
			)
		) 
		END		
		
      If UPDATE([Suppress])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Suppress',
      CONVERT(NVARCHAR(2000),DELETED.[Suppress],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Suppress],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[Suppress] Is Null And
				DELETED.[Suppress] Is Not Null
			) Or
			(
				INSERTED.[Suppress] Is Not Null And
				DELETED.[Suppress] Is Null
			) Or
			(
				INSERTED.[Suppress] !=
				DELETED.[Suppress]
			)
		) 
		END		
		
      If UPDATE([FilingStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FilingStatus',
      CONVERT(NVARCHAR(2000),DELETED.[FilingStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FilingStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[FilingStatus] Is Null And
				DELETED.[FilingStatus] Is Not Null
			) Or
			(
				INSERTED.[FilingStatus] Is Not Null And
				DELETED.[FilingStatus] Is Null
			) Or
			(
				INSERTED.[FilingStatus] !=
				DELETED.[FilingStatus]
			)
		) 
		END		
		
      If UPDATE([Exemptions])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Exemptions',
      CONVERT(NVARCHAR(2000),DELETED.[Exemptions],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Exemptions],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[Exemptions] Is Null And
				DELETED.[Exemptions] Is Not Null
			) Or
			(
				INSERTED.[Exemptions] Is Not Null And
				DELETED.[Exemptions] Is Null
			) Or
			(
				INSERTED.[Exemptions] !=
				DELETED.[Exemptions]
			)
		) 
		END		
		
      If UPDATE([AdditionalAmt])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'AdditionalAmt',
      CONVERT(NVARCHAR(2000),DELETED.[AdditionalAmt],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AdditionalAmt],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[AdditionalAmt] Is Null And
				DELETED.[AdditionalAmt] Is Not Null
			) Or
			(
				INSERTED.[AdditionalAmt] Is Not Null And
				DELETED.[AdditionalAmt] Is Null
			) Or
			(
				INSERTED.[AdditionalAmt] !=
				DELETED.[AdditionalAmt]
			)
		) 
		END		
		
      If UPDATE([OtherExemptions])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'OtherExemptions',
      CONVERT(NVARCHAR(2000),DELETED.[OtherExemptions],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OtherExemptions],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[OtherExemptions] Is Null And
				DELETED.[OtherExemptions] Is Not Null
			) Or
			(
				INSERTED.[OtherExemptions] Is Not Null And
				DELETED.[OtherExemptions] Is Null
			) Or
			(
				INSERTED.[OtherExemptions] !=
				DELETED.[OtherExemptions]
			)
		) 
		END		
		
      If UPDATE([Limit])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Limit',
      CONVERT(NVARCHAR(2000),DELETED.[Limit],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Limit],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[Limit] Is Null And
				DELETED.[Limit] Is Not Null
			) Or
			(
				INSERTED.[Limit] Is Not Null And
				DELETED.[Limit] Is Null
			) Or
			(
				INSERTED.[Limit] !=
				DELETED.[Limit]
			)
		) 
		END		
		
      If UPDATE([CurrentAmt])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'CurrentAmt',
      CONVERT(NVARCHAR(2000),DELETED.[CurrentAmt],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CurrentAmt],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[CurrentAmt] Is Null And
				DELETED.[CurrentAmt] Is Not Null
			) Or
			(
				INSERTED.[CurrentAmt] Is Not Null And
				DELETED.[CurrentAmt] Is Null
			) Or
			(
				INSERTED.[CurrentAmt] !=
				DELETED.[CurrentAmt]
			)
		) 
		END		
		
      If UPDATE([Override])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Override',
      CONVERT(NVARCHAR(2000),DELETED.[Override],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Override],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[Override] Is Null And
				DELETED.[Override] Is Not Null
			) Or
			(
				INSERTED.[Override] Is Not Null And
				DELETED.[Override] Is Null
			) Or
			(
				INSERTED.[Override] !=
				DELETED.[Override]
			)
		) 
		END		
		
      If UPDATE([Adjust])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Adjust',
      CONVERT(NVARCHAR(2000),DELETED.[Adjust],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Adjust],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[Adjust] Is Null And
				DELETED.[Adjust] Is Not Null
			) Or
			(
				INSERTED.[Adjust] Is Not Null And
				DELETED.[Adjust] Is Null
			) Or
			(
				INSERTED.[Adjust] !=
				DELETED.[Adjust]
			)
		) 
		END		
		
      If UPDATE([PrintOnCheck])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'PrintOnCheck',
      CONVERT(NVARCHAR(2000),DELETED.[PrintOnCheck],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PrintOnCheck],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[PrintOnCheck] Is Null And
				DELETED.[PrintOnCheck] Is Not Null
			) Or
			(
				INSERTED.[PrintOnCheck] Is Not Null And
				DELETED.[PrintOnCheck] Is Null
			) Or
			(
				INSERTED.[PrintOnCheck] !=
				DELETED.[PrintOnCheck]
			)
		) 
		END		
		
      If UPDATE([TaxablePayBasis])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'TaxablePayBasis',
      CONVERT(NVARCHAR(2000),DELETED.[TaxablePayBasis],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TaxablePayBasis],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[TaxablePayBasis] Is Null And
				DELETED.[TaxablePayBasis] Is Not Null
			) Or
			(
				INSERTED.[TaxablePayBasis] Is Not Null And
				DELETED.[TaxablePayBasis] Is Null
			) Or
			(
				INSERTED.[TaxablePayBasis] !=
				DELETED.[TaxablePayBasis]
			)
		) 
		END		
		
      If UPDATE([AdjustedGrossPayBasis])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'AdjustedGrossPayBasis',
      CONVERT(NVARCHAR(2000),DELETED.[AdjustedGrossPayBasis],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AdjustedGrossPayBasis],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[AdjustedGrossPayBasis] Is Null And
				DELETED.[AdjustedGrossPayBasis] Is Not Null
			) Or
			(
				INSERTED.[AdjustedGrossPayBasis] Is Not Null And
				DELETED.[AdjustedGrossPayBasis] Is Null
			) Or
			(
				INSERTED.[AdjustedGrossPayBasis] !=
				DELETED.[AdjustedGrossPayBasis]
			)
		) 
		END		
		
      If UPDATE([Amt401K])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Amt401K',
      CONVERT(NVARCHAR(2000),DELETED.[Amt401K],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Amt401K],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[Amt401K] Is Null And
				DELETED.[Amt401K] Is Not Null
			) Or
			(
				INSERTED.[Amt401K] Is Not Null And
				DELETED.[Amt401K] Is Null
			) Or
			(
				INSERTED.[Amt401K] !=
				DELETED.[Amt401K]
			)
		) 
		END		
		
      If UPDATE([Amt125])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Amt125',
      CONVERT(NVARCHAR(2000),DELETED.[Amt125],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Amt125],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[Amt125] Is Null And
				DELETED.[Amt125] Is Not Null
			) Or
			(
				INSERTED.[Amt125] Is Not Null And
				DELETED.[Amt125] Is Null
			) Or
			(
				INSERTED.[Amt125] !=
				DELETED.[Amt125]
			)
		) 
		END		
		
      If UPDATE([Exclude401k])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'Exclude401k',
      CONVERT(NVARCHAR(2000),DELETED.[Exclude401k],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Exclude401k],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[Exclude401k] Is Null And
				DELETED.[Exclude401k] Is Not Null
			) Or
			(
				INSERTED.[Exclude401k] Is Not Null And
				DELETED.[Exclude401k] Is Null
			) Or
			(
				INSERTED.[Exclude401k] !=
				DELETED.[Exclude401k]
			)
		) 
		END		
		
      If UPDATE([ExcludeCafeteria])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'ExcludeCafeteria',
      CONVERT(NVARCHAR(2000),DELETED.[ExcludeCafeteria],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ExcludeCafeteria],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[ExcludeCafeteria] Is Null And
				DELETED.[ExcludeCafeteria] Is Not Null
			) Or
			(
				INSERTED.[ExcludeCafeteria] Is Not Null And
				DELETED.[ExcludeCafeteria] Is Null
			) Or
			(
				INSERTED.[ExcludeCafeteria] !=
				DELETED.[ExcludeCafeteria]
			)
		) 
		END		
		
      If UPDATE([ExcludeOtherPay1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'ExcludeOtherPay1',
      CONVERT(NVARCHAR(2000),DELETED.[ExcludeOtherPay1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ExcludeOtherPay1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[ExcludeOtherPay1] Is Null And
				DELETED.[ExcludeOtherPay1] Is Not Null
			) Or
			(
				INSERTED.[ExcludeOtherPay1] Is Not Null And
				DELETED.[ExcludeOtherPay1] Is Null
			) Or
			(
				INSERTED.[ExcludeOtherPay1] !=
				DELETED.[ExcludeOtherPay1]
			)
		) 
		END		
		
      If UPDATE([ExcludeOtherPay2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'ExcludeOtherPay2',
      CONVERT(NVARCHAR(2000),DELETED.[ExcludeOtherPay2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ExcludeOtherPay2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[ExcludeOtherPay2] Is Null And
				DELETED.[ExcludeOtherPay2] Is Not Null
			) Or
			(
				INSERTED.[ExcludeOtherPay2] Is Not Null And
				DELETED.[ExcludeOtherPay2] Is Null
			) Or
			(
				INSERTED.[ExcludeOtherPay2] !=
				DELETED.[ExcludeOtherPay2]
			)
		) 
		END		
		
      If UPDATE([ExcludeOtherPay3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'ExcludeOtherPay3',
      CONVERT(NVARCHAR(2000),DELETED.[ExcludeOtherPay3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ExcludeOtherPay3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[ExcludeOtherPay3] Is Null And
				DELETED.[ExcludeOtherPay3] Is Not Null
			) Or
			(
				INSERTED.[ExcludeOtherPay3] Is Not Null And
				DELETED.[ExcludeOtherPay3] Is Null
			) Or
			(
				INSERTED.[ExcludeOtherPay3] !=
				DELETED.[ExcludeOtherPay3]
			)
		) 
		END		
		
      If UPDATE([ExcludeOtherPay4])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'ExcludeOtherPay4',
      CONVERT(NVARCHAR(2000),DELETED.[ExcludeOtherPay4],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ExcludeOtherPay4],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[ExcludeOtherPay4] Is Null And
				DELETED.[ExcludeOtherPay4] Is Not Null
			) Or
			(
				INSERTED.[ExcludeOtherPay4] Is Not Null And
				DELETED.[ExcludeOtherPay4] Is Null
			) Or
			(
				INSERTED.[ExcludeOtherPay4] !=
				DELETED.[ExcludeOtherPay4]
			)
		) 
		END		
		
      If UPDATE([ExcludeOtherPay5])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'ExcludeOtherPay5',
      CONVERT(NVARCHAR(2000),DELETED.[ExcludeOtherPay5],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ExcludeOtherPay5],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[ExcludeOtherPay5] Is Null And
				DELETED.[ExcludeOtherPay5] Is Not Null
			) Or
			(
				INSERTED.[ExcludeOtherPay5] Is Not Null And
				DELETED.[ExcludeOtherPay5] Is Null
			) Or
			(
				INSERTED.[ExcludeOtherPay5] !=
				DELETED.[ExcludeOtherPay5]
			)
		) 
		END		
		
      If UPDATE([OtherPay1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'OtherPay1',
      CONVERT(NVARCHAR(2000),DELETED.[OtherPay1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OtherPay1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[OtherPay1] Is Null And
				DELETED.[OtherPay1] Is Not Null
			) Or
			(
				INSERTED.[OtherPay1] Is Not Null And
				DELETED.[OtherPay1] Is Null
			) Or
			(
				INSERTED.[OtherPay1] !=
				DELETED.[OtherPay1]
			)
		) 
		END		
		
      If UPDATE([OtherPay2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'OtherPay2',
      CONVERT(NVARCHAR(2000),DELETED.[OtherPay2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OtherPay2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[OtherPay2] Is Null And
				DELETED.[OtherPay2] Is Not Null
			) Or
			(
				INSERTED.[OtherPay2] Is Not Null And
				DELETED.[OtherPay2] Is Null
			) Or
			(
				INSERTED.[OtherPay2] !=
				DELETED.[OtherPay2]
			)
		) 
		END		
		
      If UPDATE([OtherPay3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'OtherPay3',
      CONVERT(NVARCHAR(2000),DELETED.[OtherPay3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OtherPay3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[OtherPay3] Is Null And
				DELETED.[OtherPay3] Is Not Null
			) Or
			(
				INSERTED.[OtherPay3] Is Not Null And
				DELETED.[OtherPay3] Is Null
			) Or
			(
				INSERTED.[OtherPay3] !=
				DELETED.[OtherPay3]
			)
		) 
		END		
		
      If UPDATE([OtherPay4])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'OtherPay4',
      CONVERT(NVARCHAR(2000),DELETED.[OtherPay4],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OtherPay4],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[OtherPay4] Is Null And
				DELETED.[OtherPay4] Is Not Null
			) Or
			(
				INSERTED.[OtherPay4] Is Not Null And
				DELETED.[OtherPay4] Is Null
			) Or
			(
				INSERTED.[OtherPay4] !=
				DELETED.[OtherPay4]
			)
		) 
		END		
		
      If UPDATE([OtherPay5])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'OtherPay5',
      CONVERT(NVARCHAR(2000),DELETED.[OtherPay5],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OtherPay5],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[OtherPay5] Is Null And
				DELETED.[OtherPay5] Is Not Null
			) Or
			(
				INSERTED.[OtherPay5] Is Not Null And
				DELETED.[OtherPay5] Is Null
			) Or
			(
				INSERTED.[OtherPay5] !=
				DELETED.[OtherPay5]
			)
		) 
		END		
		
      If UPDATE([FormW4Version])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4Version',
      CONVERT(NVARCHAR(2000),DELETED.[FormW4Version],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FormW4Version],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[FormW4Version] Is Null And
				DELETED.[FormW4Version] Is Not Null
			) Or
			(
				INSERTED.[FormW4Version] Is Not Null And
				DELETED.[FormW4Version] Is Null
			) Or
			(
				INSERTED.[FormW4Version] !=
				DELETED.[FormW4Version]
			)
		) 
		END		
		
      If UPDATE([FormW4Step2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4Step2',
      CONVERT(NVARCHAR(2000),DELETED.[FormW4Step2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FormW4Step2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[FormW4Step2] Is Null And
				DELETED.[FormW4Step2] Is Not Null
			) Or
			(
				INSERTED.[FormW4Step2] Is Not Null And
				DELETED.[FormW4Step2] Is Null
			) Or
			(
				INSERTED.[FormW4Step2] !=
				DELETED.[FormW4Step2]
			)
		) 
		END		
		
      If UPDATE([FormW4Dependents])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4Dependents',
      CONVERT(NVARCHAR(2000),DELETED.[FormW4Dependents],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FormW4Dependents],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[FormW4Dependents] Is Null And
				DELETED.[FormW4Dependents] Is Not Null
			) Or
			(
				INSERTED.[FormW4Dependents] Is Not Null And
				DELETED.[FormW4Dependents] Is Null
			) Or
			(
				INSERTED.[FormW4Dependents] !=
				DELETED.[FormW4Dependents]
			)
		) 
		END		
		
      If UPDATE([FormW4DependentsOther])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4DependentsOther',
      CONVERT(NVARCHAR(2000),DELETED.[FormW4DependentsOther],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FormW4DependentsOther],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[FormW4DependentsOther] Is Null And
				DELETED.[FormW4DependentsOther] Is Not Null
			) Or
			(
				INSERTED.[FormW4DependentsOther] Is Not Null And
				DELETED.[FormW4DependentsOther] Is Null
			) Or
			(
				INSERTED.[FormW4DependentsOther] !=
				DELETED.[FormW4DependentsOther]
			)
		) 
		END		
		
      If UPDATE([FormW4OtherTaxCredit])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4OtherTaxCredit',
      CONVERT(NVARCHAR(2000),DELETED.[FormW4OtherTaxCredit],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FormW4OtherTaxCredit],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[FormW4OtherTaxCredit] Is Null And
				DELETED.[FormW4OtherTaxCredit] Is Not Null
			) Or
			(
				INSERTED.[FormW4OtherTaxCredit] Is Not Null And
				DELETED.[FormW4OtherTaxCredit] Is Null
			) Or
			(
				INSERTED.[FormW4OtherTaxCredit] !=
				DELETED.[FormW4OtherTaxCredit]
			)
		) 
		END		
		
      If UPDATE([FormW4OtherIncome])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4OtherIncome',
      CONVERT(NVARCHAR(2000),DELETED.[FormW4OtherIncome],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FormW4OtherIncome],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[FormW4OtherIncome] Is Null And
				DELETED.[FormW4OtherIncome] Is Not Null
			) Or
			(
				INSERTED.[FormW4OtherIncome] Is Not Null And
				DELETED.[FormW4OtherIncome] Is Null
			) Or
			(
				INSERTED.[FormW4OtherIncome] !=
				DELETED.[FormW4OtherIncome]
			)
		) 
		END		
		
      If UPDATE([FormW4Deductions])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4Deductions',
      CONVERT(NVARCHAR(2000),DELETED.[FormW4Deductions],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FormW4Deductions],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[FormW4Deductions] Is Null And
				DELETED.[FormW4Deductions] Is Not Null
			) Or
			(
				INSERTED.[FormW4Deductions] Is Not Null And
				DELETED.[FormW4Deductions] Is Null
			) Or
			(
				INSERTED.[FormW4Deductions] !=
				DELETED.[FormW4Deductions]
			)
		) 
		END		
		
      If UPDATE([FormW4DependentsAmount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4DependentsAmount',
      CONVERT(NVARCHAR(2000),DELETED.[FormW4DependentsAmount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FormW4DependentsAmount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[FormW4DependentsAmount] Is Null And
				DELETED.[FormW4DependentsAmount] Is Not Null
			) Or
			(
				INSERTED.[FormW4DependentsAmount] Is Not Null And
				DELETED.[FormW4DependentsAmount] Is Null
			) Or
			(
				INSERTED.[FormW4DependentsAmount] !=
				DELETED.[FormW4DependentsAmount]
			)
		) 
		END		
		
      If UPDATE([FormW4DependentsOtherAmount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121),'FormW4DependentsOtherAmount',
      CONVERT(NVARCHAR(2000),DELETED.[FormW4DependentsOtherAmount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FormW4DependentsOtherAmount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND 
		(
			(
				INSERTED.[FormW4DependentsOtherAmount] Is Null And
				DELETED.[FormW4DependentsOtherAmount] Is Not Null
			) Or
			(
				INSERTED.[FormW4DependentsOtherAmount] Is Not Null And
				DELETED.[FormW4DependentsOtherAmount] Is Null
			) Or
			(
				INSERTED.[FormW4DependentsOtherAmount] !=
				DELETED.[FormW4DependentsOtherAmount]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_EMPayrollWithholding] ON [dbo].[EMPayrollWithholding]
GO
ALTER TABLE [dbo].[EMPayrollWithholding] ADD CONSTRAINT [EMPayrollWithholdingPK] PRIMARY KEY NONCLUSTERED ([Employee], [EmployeeCompany], [Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
