CREATE TABLE [dbo].[VisionImportAPVoucherLine_APD]
(
[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Voucher] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VoucherLine] [int] NULL,
[Description] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentAmount] [decimal] (19, 4) NULL,
[org] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
