CREATE TABLE [dbo].[cvMaster]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CheckNo] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransDate] [datetime] NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL,
[Invoice] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__cvMaster___Poste__7F38FC33] DEFAULT ('N'),
[LiabCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayDate] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__cvMaster_Ne__Seq__002D206C] DEFAULT ((0)),
[Voucher] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BarCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyExchangeOverrideMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__cvMaster___Curre__012144A5] DEFAULT ('N'),
[CurrencyExchangeOverrideDate] [datetime] NULL,
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__cvMaster___Curre__021568DE] DEFAULT ((0)),
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__cvMaster___Statu__03098D17] DEFAULT ('N'),
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RejectReason] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Diary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaryNo] [int] NOT NULL CONSTRAINT [DF__cvMaster___Diary__03FDB150] DEFAULT ((0)),
[TransactionID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardPrimaryCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cvMaster] ADD CONSTRAINT [cvMasterPK] PRIMARY KEY NONCLUSTERED ([Batch], [CheckNo]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
