CREATE TABLE [dbo].[EMDegree]
(
[RecordID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__EMDegree_Ne__Seq__42B9FDD8] DEFAULT ((0)),
[Degree] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Specialty] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Institution] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearEarned] [smallint] NOT NULL CONSTRAINT [DF__EMDegree___YearE__43AE2211] DEFAULT ((0)),
[IncludeInProposal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMDegree___Inclu__44A2464A] DEFAULT ('Y'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_EMDegree]
      ON [dbo].[EMDegree]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMDegree'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'RecordID',CONVERT(NVARCHAR(2000),[RecordID],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'Employee',CONVERT(NVARCHAR(2000),DELETED.[Employee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Employee = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'Seq',CONVERT(NVARCHAR(2000),[Seq],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'Degree',CONVERT(NVARCHAR(2000),DELETED.[Degree],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGEMDegree as oldDesc  on DELETED.Degree = oldDesc.Code

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'Specialty',CONVERT(NVARCHAR(2000),[Specialty],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'Institution',CONVERT(NVARCHAR(2000),[Institution],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'YearEarned',CONVERT(NVARCHAR(2000),[YearEarned],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'IncludeInProposal',CONVERT(NVARCHAR(2000),[IncludeInProposal],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_EMDegree] ON [dbo].[EMDegree]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_EMDegree]
      ON [dbo].[EMDegree]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMDegree'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'RecordID',NULL,CONVERT(NVARCHAR(2000),[RecordID],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Employee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Employee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Employee = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Seq',NULL,CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Degree',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Degree],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGEMDegree as newDesc  on INSERTED.Degree = newDesc.Code

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Specialty',NULL,CONVERT(NVARCHAR(2000),[Specialty],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Institution',NULL,CONVERT(NVARCHAR(2000),[Institution],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'YearEarned',NULL,CONVERT(NVARCHAR(2000),[YearEarned],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'IncludeInProposal',NULL,CONVERT(NVARCHAR(2000),[IncludeInProposal],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_EMDegree] ON [dbo].[EMDegree]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_EMDegree]
      ON [dbo].[EMDegree]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMDegree'
    
      If UPDATE([RecordID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'RecordID',
      CONVERT(NVARCHAR(2000),DELETED.[RecordID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecordID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[RecordID] Is Null And
				DELETED.[RecordID] Is Not Null
			) Or
			(
				INSERTED.[RecordID] Is Not Null And
				DELETED.[RecordID] Is Null
			) Or
			(
				INSERTED.[RecordID] !=
				DELETED.[RecordID]
			)
		) 
		END		
		
     If UPDATE([Employee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Employee',
     CONVERT(NVARCHAR(2000),DELETED.[Employee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Employee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[Employee] Is Null And
				DELETED.[Employee] Is Not Null
			) Or
			(
				INSERTED.[Employee] Is Not Null And
				DELETED.[Employee] Is Null
			) Or
			(
				INSERTED.[Employee] !=
				DELETED.[Employee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Employee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Employee = newDesc.Employee
		END		
		
      If UPDATE([Seq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Seq',
      CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[Seq] Is Null And
				DELETED.[Seq] Is Not Null
			) Or
			(
				INSERTED.[Seq] Is Not Null And
				DELETED.[Seq] Is Null
			) Or
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
		
     If UPDATE([Degree])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Degree',
     CONVERT(NVARCHAR(2000),DELETED.[Degree],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Degree],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[Degree] Is Null And
				DELETED.[Degree] Is Not Null
			) Or
			(
				INSERTED.[Degree] Is Not Null And
				DELETED.[Degree] Is Null
			) Or
			(
				INSERTED.[Degree] !=
				DELETED.[Degree]
			)
		) left join CFGEMDegree as oldDesc  on DELETED.Degree = oldDesc.Code  left join  CFGEMDegree as newDesc  on INSERTED.Degree = newDesc.Code
		END		
		
      If UPDATE([Specialty])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Specialty',
      CONVERT(NVARCHAR(2000),DELETED.[Specialty],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Specialty],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[Specialty] Is Null And
				DELETED.[Specialty] Is Not Null
			) Or
			(
				INSERTED.[Specialty] Is Not Null And
				DELETED.[Specialty] Is Null
			) Or
			(
				INSERTED.[Specialty] !=
				DELETED.[Specialty]
			)
		) 
		END		
		
      If UPDATE([Institution])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Institution',
      CONVERT(NVARCHAR(2000),DELETED.[Institution],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Institution],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[Institution] Is Null And
				DELETED.[Institution] Is Not Null
			) Or
			(
				INSERTED.[Institution] Is Not Null And
				DELETED.[Institution] Is Null
			) Or
			(
				INSERTED.[Institution] !=
				DELETED.[Institution]
			)
		) 
		END		
		
      If UPDATE([YearEarned])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'YearEarned',
      CONVERT(NVARCHAR(2000),DELETED.[YearEarned],121),
      CONVERT(NVARCHAR(2000),INSERTED.[YearEarned],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[YearEarned] Is Null And
				DELETED.[YearEarned] Is Not Null
			) Or
			(
				INSERTED.[YearEarned] Is Not Null And
				DELETED.[YearEarned] Is Null
			) Or
			(
				INSERTED.[YearEarned] !=
				DELETED.[YearEarned]
			)
		) 
		END		
		
      If UPDATE([IncludeInProposal])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'IncludeInProposal',
      CONVERT(NVARCHAR(2000),DELETED.[IncludeInProposal],121),
      CONVERT(NVARCHAR(2000),INSERTED.[IncludeInProposal],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[IncludeInProposal] Is Null And
				DELETED.[IncludeInProposal] Is Not Null
			) Or
			(
				INSERTED.[IncludeInProposal] Is Not Null And
				DELETED.[IncludeInProposal] Is Null
			) Or
			(
				INSERTED.[IncludeInProposal] !=
				DELETED.[IncludeInProposal]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_EMDegree] ON [dbo].[EMDegree]
GO
ALTER TABLE [dbo].[EMDegree] ADD CONSTRAINT [EMDegreePK] PRIMARY KEY NONCLUSTERED ([RecordID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
