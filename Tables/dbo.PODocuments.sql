CREATE TABLE [dbo].[PODocuments]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL,
[AssociateNew] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PODocumen__Assoc__6597C50C] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__PODocuments__Seq__668BE945] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PODocuments] ADD CONSTRAINT [PODocumentsPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
