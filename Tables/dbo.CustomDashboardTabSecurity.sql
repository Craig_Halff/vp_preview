CREATE TABLE [dbo].[CustomDashboardTabSecurity]
(
[TabSecurityID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RoleID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomDashboardTabSecurity] ADD CONSTRAINT [CustomDashboardTabSecurityPK] PRIMARY KEY CLUSTERED ([TabSecurityID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
