CREATE TABLE [dbo].[_InvoiceXref]
(
[Invoice] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewInvoice] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvAmt] [decimal] (38, 4) NULL,
[Payment] [decimal] (38, 4) NULL,
[PaidPeriod] [int] NOT NULL,
[Var] [decimal] (38, 4) NULL
) ON [PRIMARY]
GO
