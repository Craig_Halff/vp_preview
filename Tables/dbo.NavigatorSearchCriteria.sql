CREATE TABLE [dbo].[NavigatorSearchCriteria]
(
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Criteria] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[NavigatorSearchCriteria] ADD CONSTRAINT [NavigatorSearchCriteriaPK] PRIMARY KEY NONCLUSTERED ([Username], [Name]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
