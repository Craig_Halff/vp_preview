CREATE TABLE [dbo].[CFGIntegrationWS]
(
[ConfigID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EnableWebService] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__Enabl__1117B54C] DEFAULT ('N'),
[WebServiceURL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RowsPerCall] [int] NOT NULL CONSTRAINT [DF__CFGIntegr__RowsP__120BD985] DEFAULT ((24999)),
[RequestTimeout] [int] NOT NULL CONSTRAINT [DF__CFGIntegr__Reque__12FFFDBE] DEFAULT ((10000)),
[ReceiveNewHires] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__Recei__13F421F7] DEFAULT ('N'),
[SendChangesSinceLastSend] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__SendC__14E84630] DEFAULT ('N'),
[SendEmployees] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__SendE__15DC6A69] DEFAULT ('N'),
[SendOrganizations] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__SendO__16D08EA2] DEFAULT ('N'),
[AuditUser] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TerminationDate] [datetime] NULL,
[LastSendEmployees] [datetime] NULL,
[LastSendOrganizations] [datetime] NULL,
[UserName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Options] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastReceiveProjects] [datetime] NULL,
[LastReceiveTimesheets] [datetime] NULL,
[LastSyncFields] [datetime] NULL,
[EnablePIMContacts] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__Enabl__18B8D714] DEFAULT ('N'),
[EnablePIMEmployees] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__Enabl__19ACFB4D] DEFAULT ('N'),
[EnablePIMMktCampaigns] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__Enabl__1AA11F86] DEFAULT ('N'),
[EnablePIMProjects] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__Enabl__1C8967F8] DEFAULT ('N'),
[OnlySendOrganizationsWithEmployees] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__OnlyS__1E71B06A] DEFAULT ('N'),
[LimitByEmployeeType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__Limit__1F65D4A3] DEFAULT ('N'),
[SyncProjectsRegular] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__SyncP__2059F8DC] DEFAULT ('Y'),
[SyncProjectsOverhead] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__SyncP__214E1D15] DEFAULT ('Y'),
[SyncProjectsPromotional] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__SyncP__2242414E] DEFAULT ('Y'),
[SyncEmployeesPreferredName] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__SyncE__23366587] DEFAULT ('N'),
[SyncContactsPreferredName] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__SyncC__242A89C0] DEFAULT ('N'),
[PIMAllowFullAccess] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__PIMAl__251EADF9] DEFAULT ('Y'),
[RoleNotification] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnablePIMFirms] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGIntegr__Enabl__313CCE20] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGIntegrationWS] ADD CONSTRAINT [CFGIntegrationWSPK] PRIMARY KEY CLUSTERED ([ConfigID], [Type]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
