CREATE TABLE [dbo].[AnalysisCubesKPIOverride]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OverridePKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__AnalysisCub__Seq__4713D34A] DEFAULT ((0)),
[DimensionRec] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Year] [int] NOT NULL CONSTRAINT [DF__AnalysisCu__Year__4807F783] DEFAULT ((0)),
[AnnualGoal] [decimal] (19, 4) NOT NULL,
[GoalLowValue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__AnalysisC__GoalL__48FC1BBC] DEFAULT ((0)),
[GoalHighValue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__AnalysisC__GoalH__49F03FF5] DEFAULT ((0)),
[Period1Goal] [decimal] (19, 4) NOT NULL,
[Period2Goal] [decimal] (19, 4) NOT NULL,
[Period3Goal] [decimal] (19, 4) NOT NULL,
[Period4Goal] [decimal] (19, 4) NOT NULL,
[Period5Goal] [decimal] (19, 4) NOT NULL,
[Period6Goal] [decimal] (19, 4) NOT NULL,
[Period7Goal] [decimal] (19, 4) NOT NULL,
[Period8Goal] [decimal] (19, 4) NOT NULL,
[Period9Goal] [decimal] (19, 4) NOT NULL,
[Period10Goal] [decimal] (19, 4) NOT NULL,
[Period11Goal] [decimal] (19, 4) NOT NULL,
[Period12Goal] [decimal] (19, 4) NOT NULL,
[Period13Goal] [decimal] (19, 4) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AnalysisCubesKPIOverride] ADD CONSTRAINT [AnalysisCubesKPIOverridePK] PRIMARY KEY NONCLUSTERED ([PKey], [OverridePKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
