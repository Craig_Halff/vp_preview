CREATE TABLE [dbo].[FW_WorkbookFilters]
(
[FilterID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WorkbookID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FilterType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FilterValue] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_WorkbookFilters] ADD CONSTRAINT [FW_WorkbookFiltersPK] PRIMARY KEY CLUSTERED ([FilterID], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
