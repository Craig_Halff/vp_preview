CREATE TABLE [dbo].[CFGClientHierarchyData]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Level] [smallint] NOT NULL CONSTRAINT [DF__CFGClient__Level__10615C29] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGClientHierarchyData] ADD CONSTRAINT [CFGClientHierarchyDataPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
