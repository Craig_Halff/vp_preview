CREATE TABLE [dbo].[OppConsultant_Backup]
(
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConsultantID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OppConCost] [decimal] (19, 4) NOT NULL,
[OppConBill] [decimal] (19, 4) NOT NULL,
[DirectAcctFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__OppConsul__Direc__358AEB43] DEFAULT ('N'),
[SeqNo] [smallint] NOT NULL CONSTRAINT [DF__OppConsul__SeqNo__367F0F7C] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OppConsultant_Backup] ADD CONSTRAINT [OppConsultantPK] PRIMARY KEY NONCLUSTERED ([ConsultantID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
