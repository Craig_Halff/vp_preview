CREATE TABLE [dbo].[Contacts]
(
[ContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLAddress] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VEAddress] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Salutation] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suffix] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Addressee] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address3] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address4] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIP] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fax] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Pager] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CellPhone] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HomePhone] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMail] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Memo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MailingAddress] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Contacts___Maili__63FAF612] DEFAULT ('N'),
[Billing] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__Contacts___Billi__64EF1A4B] DEFAULT ('N'),
[PrimaryInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__Contacts___Prima__65E33E84] DEFAULT ('N'),
[ContactStatus] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreferredName] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FaxFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PagerFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CellPhoneFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HomePhoneFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AjeraSync] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__Contacts___Ajera__66D762BD] DEFAULT ('N'),
[TLInternalKey] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLSyncModDate] [datetime] NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[Owner] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusReason] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Rating] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmDescription] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmAddress1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmAddress2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmAddress3] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmAddress4] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmCity] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmState] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmZip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmCountry] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmBusinessPhone] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmBusinessPhoneFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmBusinessFax] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmBusinessFaxFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmPager] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Market] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Website] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QualifiedStatus] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusDate] [datetime] NULL,
[ProfessionalSuffix] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFID] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFLastModifiedDate] [datetime] NULL,
[QBOID] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QBOIsMainContact] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Contacts__QBOIsM__2E606175] DEFAULT ('N'),
[QBOLastUpdated] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_Contacts]
      ON [dbo].[Contacts]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Contacts'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 )
begin

        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'ClientID',CONVERT(NVARCHAR(2000),DELETED.[ClientID],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join CL as oldDesc  on DELETED.ClientID = oldDesc.ClientID

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'ContactStatus',CONVERT(NVARCHAR(2000),DELETED.[ContactStatus],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join ContactStatus as oldDesc  on DELETED.ContactStatus = oldDesc.Code

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Email',CONVERT(NVARCHAR(2000),[Email],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FirstName',CONVERT(NVARCHAR(2000),[FirstName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'LastName',CONVERT(NVARCHAR(2000),[LastName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Title',CONVERT(NVARCHAR(2000),[Title],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Type',CONVERT(NVARCHAR(2000),DELETED.[Type],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGContactType as oldDesc  on DELETED.Type = oldDesc.Code

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Vendor',CONVERT(NVARCHAR(2000),DELETED.[Vendor],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join VE as oldDesc  on DELETED.Vendor = oldDesc.Vendor

      
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'ContactID',CONVERT(NVARCHAR(2000),[ContactID],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'ClientID',CONVERT(NVARCHAR(2000),DELETED.[ClientID],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join CL as oldDesc  on DELETED.ClientID = oldDesc.ClientID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'CLAddress',CONVERT(NVARCHAR(2000),[CLAddress],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Vendor',CONVERT(NVARCHAR(2000),DELETED.[Vendor],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join VE as oldDesc  on DELETED.Vendor = oldDesc.Vendor

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'VEAddress',CONVERT(NVARCHAR(2000),[VEAddress],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Type',CONVERT(NVARCHAR(2000),DELETED.[Type],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGContactType as oldDesc  on DELETED.Type = oldDesc.Code

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'LastName',CONVERT(NVARCHAR(2000),[LastName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FirstName',CONVERT(NVARCHAR(2000),[FirstName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'MiddleName',CONVERT(NVARCHAR(2000),[MiddleName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Salutation',CONVERT(NVARCHAR(2000),[Salutation],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Suffix',CONVERT(NVARCHAR(2000),[Suffix],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Title',CONVERT(NVARCHAR(2000),[Title],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Addressee',CONVERT(NVARCHAR(2000),[Addressee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Address1',CONVERT(NVARCHAR(2000),[Address1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Address2',CONVERT(NVARCHAR(2000),[Address2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Address3',CONVERT(NVARCHAR(2000),[Address3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Address4',CONVERT(NVARCHAR(2000),[Address4],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'City',CONVERT(NVARCHAR(2000),[City],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'State',CONVERT(NVARCHAR(2000),DELETED.[State],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGStates as oldDesc  on DELETED.State = oldDesc.Code

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'ZIP',CONVERT(NVARCHAR(2000),[ZIP],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Country',CONVERT(NVARCHAR(2000),[Country],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Phone',CONVERT(NVARCHAR(2000),[Phone],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Fax',CONVERT(NVARCHAR(2000),[Fax],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Pager',CONVERT(NVARCHAR(2000),[Pager],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'CellPhone',CONVERT(NVARCHAR(2000),[CellPhone],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'HomePhone',CONVERT(NVARCHAR(2000),[HomePhone],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'EMail',CONVERT(NVARCHAR(2000),[EMail],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Memo','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'MailingAddress',CONVERT(NVARCHAR(2000),[MailingAddress],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Billing',CONVERT(NVARCHAR(2000),[Billing],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'PrimaryInd',CONVERT(NVARCHAR(2000),[PrimaryInd],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'ContactStatus',CONVERT(NVARCHAR(2000),DELETED.[ContactStatus],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join ContactStatus as oldDesc  on DELETED.ContactStatus = oldDesc.Code

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'PreferredName',CONVERT(NVARCHAR(2000),[PreferredName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'CustomCurrencyCode',CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Source',CONVERT(NVARCHAR(2000),[Source],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'PhoneFormat',CONVERT(NVARCHAR(2000),[PhoneFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FaxFormat',CONVERT(NVARCHAR(2000),[FaxFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'PagerFormat',CONVERT(NVARCHAR(2000),[PagerFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'CellPhoneFormat',CONVERT(NVARCHAR(2000),[CellPhoneFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'HomePhoneFormat',CONVERT(NVARCHAR(2000),[HomePhoneFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'AjeraSync',CONVERT(NVARCHAR(2000),[AjeraSync],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'TLInternalKey',CONVERT(NVARCHAR(2000),[TLInternalKey],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'TLSyncModDate',CONVERT(NVARCHAR(2000),[TLSyncModDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Owner',CONVERT(NVARCHAR(2000),[Owner],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'StatusReason',CONVERT(NVARCHAR(2000),[StatusReason],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Rating',CONVERT(NVARCHAR(2000),[Rating],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FirmDescription',CONVERT(NVARCHAR(2000),[FirmDescription],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FirmAddress1',CONVERT(NVARCHAR(2000),[FirmAddress1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FirmAddress2',CONVERT(NVARCHAR(2000),[FirmAddress2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FirmAddress3',CONVERT(NVARCHAR(2000),[FirmAddress3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FirmAddress4',CONVERT(NVARCHAR(2000),[FirmAddress4],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FirmCity',CONVERT(NVARCHAR(2000),[FirmCity],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FirmState',CONVERT(NVARCHAR(2000),[FirmState],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FirmZip',CONVERT(NVARCHAR(2000),[FirmZip],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FirmCountry',CONVERT(NVARCHAR(2000),[FirmCountry],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FirmBusinessPhone',CONVERT(NVARCHAR(2000),[FirmBusinessPhone],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FirmBusinessFax',CONVERT(NVARCHAR(2000),[FirmBusinessFax],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FirmPager',CONVERT(NVARCHAR(2000),[FirmPager],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'ProjectDescription',CONVERT(NVARCHAR(2000),[ProjectDescription],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Market',CONVERT(NVARCHAR(2000),[Market],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Website',CONVERT(NVARCHAR(2000),[Website],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'QualifiedStatus',CONVERT(NVARCHAR(2000),[QualifiedStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'StatusDate',CONVERT(NVARCHAR(2000),[StatusDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'ProfessionalSuffix',CONVERT(NVARCHAR(2000),[ProfessionalSuffix],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'SFID',CONVERT(NVARCHAR(2000),[SFID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'SFLastModifiedDate',CONVERT(NVARCHAR(2000),[SFLastModifiedDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FirmBusinessPhoneFormat',CONVERT(NVARCHAR(2000),[FirmBusinessPhoneFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FirmBusinessFaxFormat',CONVERT(NVARCHAR(2000),[FirmBusinessFaxFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'QBOID',CONVERT(NVARCHAR(2000),[QBOID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'QBOIsMainContact',CONVERT(NVARCHAR(2000),[QBOIsMainContact],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'QBOLastUpdated',CONVERT(NVARCHAR(2000),[QBOLastUpdated],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_Contacts] ON [dbo].[Contacts]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_Contacts]
      ON [dbo].[Contacts]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Contacts'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'ContactID',NULL,CONVERT(NVARCHAR(2000),[ContactID],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'ClientID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[ClientID],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  CL as newDesc  on INSERTED.ClientID = newDesc.ClientID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'CLAddress',NULL,CONVERT(NVARCHAR(2000),[CLAddress],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Vendor',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Vendor],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  VE as newDesc  on INSERTED.Vendor = newDesc.Vendor

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'VEAddress',NULL,CONVERT(NVARCHAR(2000),[VEAddress],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Type',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Type],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGContactType as newDesc  on INSERTED.Type = newDesc.Code

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'LastName',NULL,CONVERT(NVARCHAR(2000),[LastName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirstName',NULL,CONVERT(NVARCHAR(2000),[FirstName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'MiddleName',NULL,CONVERT(NVARCHAR(2000),[MiddleName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Salutation',NULL,CONVERT(NVARCHAR(2000),[Salutation],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Suffix',NULL,CONVERT(NVARCHAR(2000),[Suffix],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Title',NULL,CONVERT(NVARCHAR(2000),[Title],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Addressee',NULL,CONVERT(NVARCHAR(2000),[Addressee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Address1',NULL,CONVERT(NVARCHAR(2000),[Address1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Address2',NULL,CONVERT(NVARCHAR(2000),[Address2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Address3',NULL,CONVERT(NVARCHAR(2000),[Address3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Address4',NULL,CONVERT(NVARCHAR(2000),[Address4],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'City',NULL,CONVERT(NVARCHAR(2000),[City],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'State',NULL,CONVERT(NVARCHAR(2000),INSERTED.[State],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGStates as newDesc  on INSERTED.State = newDesc.Code

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'ZIP',NULL,CONVERT(NVARCHAR(2000),[ZIP],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Country',NULL,CONVERT(NVARCHAR(2000),[Country],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Phone',NULL,CONVERT(NVARCHAR(2000),[Phone],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Fax',NULL,CONVERT(NVARCHAR(2000),[Fax],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Pager',NULL,CONVERT(NVARCHAR(2000),[Pager],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'CellPhone',NULL,CONVERT(NVARCHAR(2000),[CellPhone],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'HomePhone',NULL,CONVERT(NVARCHAR(2000),[HomePhone],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'EMail',NULL,CONVERT(NVARCHAR(2000),[EMail],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Memo',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'MailingAddress',NULL,CONVERT(NVARCHAR(2000),[MailingAddress],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Billing',NULL,CONVERT(NVARCHAR(2000),[Billing],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'PrimaryInd',NULL,CONVERT(NVARCHAR(2000),[PrimaryInd],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'ContactStatus',NULL,CONVERT(NVARCHAR(2000),INSERTED.[ContactStatus],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  ContactStatus as newDesc  on INSERTED.ContactStatus = newDesc.Code

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'PreferredName',NULL,CONVERT(NVARCHAR(2000),[PreferredName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'CustomCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Source',NULL,CONVERT(NVARCHAR(2000),[Source],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'PhoneFormat',NULL,CONVERT(NVARCHAR(2000),[PhoneFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FaxFormat',NULL,CONVERT(NVARCHAR(2000),[FaxFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'PagerFormat',NULL,CONVERT(NVARCHAR(2000),[PagerFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'CellPhoneFormat',NULL,CONVERT(NVARCHAR(2000),[CellPhoneFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'HomePhoneFormat',NULL,CONVERT(NVARCHAR(2000),[HomePhoneFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'AjeraSync',NULL,CONVERT(NVARCHAR(2000),[AjeraSync],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'TLInternalKey',NULL,CONVERT(NVARCHAR(2000),[TLInternalKey],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'TLSyncModDate',NULL,CONVERT(NVARCHAR(2000),[TLSyncModDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Owner',NULL,CONVERT(NVARCHAR(2000),[Owner],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'StatusReason',NULL,CONVERT(NVARCHAR(2000),[StatusReason],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Rating',NULL,CONVERT(NVARCHAR(2000),[Rating],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmDescription',NULL,CONVERT(NVARCHAR(2000),[FirmDescription],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmAddress1',NULL,CONVERT(NVARCHAR(2000),[FirmAddress1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmAddress2',NULL,CONVERT(NVARCHAR(2000),[FirmAddress2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmAddress3',NULL,CONVERT(NVARCHAR(2000),[FirmAddress3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmAddress4',NULL,CONVERT(NVARCHAR(2000),[FirmAddress4],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmCity',NULL,CONVERT(NVARCHAR(2000),[FirmCity],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmState',NULL,CONVERT(NVARCHAR(2000),[FirmState],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmZip',NULL,CONVERT(NVARCHAR(2000),[FirmZip],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmCountry',NULL,CONVERT(NVARCHAR(2000),[FirmCountry],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmBusinessPhone',NULL,CONVERT(NVARCHAR(2000),[FirmBusinessPhone],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmBusinessFax',NULL,CONVERT(NVARCHAR(2000),[FirmBusinessFax],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmPager',NULL,CONVERT(NVARCHAR(2000),[FirmPager],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'ProjectDescription',NULL,CONVERT(NVARCHAR(2000),[ProjectDescription],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Market',NULL,CONVERT(NVARCHAR(2000),[Market],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Website',NULL,CONVERT(NVARCHAR(2000),[Website],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'QualifiedStatus',NULL,CONVERT(NVARCHAR(2000),[QualifiedStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'StatusDate',NULL,CONVERT(NVARCHAR(2000),[StatusDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'ProfessionalSuffix',NULL,CONVERT(NVARCHAR(2000),[ProfessionalSuffix],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'SFID',NULL,CONVERT(NVARCHAR(2000),[SFID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'SFLastModifiedDate',NULL,CONVERT(NVARCHAR(2000),[SFLastModifiedDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmBusinessPhoneFormat',NULL,CONVERT(NVARCHAR(2000),[FirmBusinessPhoneFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmBusinessFaxFormat',NULL,CONVERT(NVARCHAR(2000),[FirmBusinessFaxFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'QBOID',NULL,CONVERT(NVARCHAR(2000),[QBOID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'QBOIsMainContact',NULL,CONVERT(NVARCHAR(2000),[QBOIsMainContact],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'QBOLastUpdated',NULL,CONVERT(NVARCHAR(2000),[QBOLastUpdated],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_Contacts] ON [dbo].[Contacts]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_Contacts]
      ON [dbo].[Contacts]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Contacts'
    
      If UPDATE([ContactID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'ContactID',
      CONVERT(NVARCHAR(2000),DELETED.[ContactID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ContactID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[ContactID] Is Null And
				DELETED.[ContactID] Is Not Null
			) Or
			(
				INSERTED.[ContactID] Is Not Null And
				DELETED.[ContactID] Is Null
			) Or
			(
				INSERTED.[ContactID] !=
				DELETED.[ContactID]
			)
		) 
		END		
		
     If UPDATE([ClientID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'ClientID',
     CONVERT(NVARCHAR(2000),DELETED.[ClientID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[ClientID],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[ClientID] Is Null And
				DELETED.[ClientID] Is Not Null
			) Or
			(
				INSERTED.[ClientID] Is Not Null And
				DELETED.[ClientID] Is Null
			) Or
			(
				INSERTED.[ClientID] !=
				DELETED.[ClientID]
			)
		) left join CL as oldDesc  on DELETED.ClientID = oldDesc.ClientID  left join  CL as newDesc  on INSERTED.ClientID = newDesc.ClientID
		END		
		
      If UPDATE([CLAddress])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'CLAddress',
      CONVERT(NVARCHAR(2000),DELETED.[CLAddress],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CLAddress],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[CLAddress] Is Null And
				DELETED.[CLAddress] Is Not Null
			) Or
			(
				INSERTED.[CLAddress] Is Not Null And
				DELETED.[CLAddress] Is Null
			) Or
			(
				INSERTED.[CLAddress] !=
				DELETED.[CLAddress]
			)
		) 
		END		
		
     If UPDATE([Vendor])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Vendor',
     CONVERT(NVARCHAR(2000),DELETED.[Vendor],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Vendor],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Vendor] Is Null And
				DELETED.[Vendor] Is Not Null
			) Or
			(
				INSERTED.[Vendor] Is Not Null And
				DELETED.[Vendor] Is Null
			) Or
			(
				INSERTED.[Vendor] !=
				DELETED.[Vendor]
			)
		) left join VE as oldDesc  on DELETED.Vendor = oldDesc.Vendor  left join  VE as newDesc  on INSERTED.Vendor = newDesc.Vendor
		END		
		
      If UPDATE([VEAddress])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'VEAddress',
      CONVERT(NVARCHAR(2000),DELETED.[VEAddress],121),
      CONVERT(NVARCHAR(2000),INSERTED.[VEAddress],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[VEAddress] Is Null And
				DELETED.[VEAddress] Is Not Null
			) Or
			(
				INSERTED.[VEAddress] Is Not Null And
				DELETED.[VEAddress] Is Null
			) Or
			(
				INSERTED.[VEAddress] !=
				DELETED.[VEAddress]
			)
		) 
		END		
		
     If UPDATE([Type])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Type',
     CONVERT(NVARCHAR(2000),DELETED.[Type],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Type],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Type] Is Null And
				DELETED.[Type] Is Not Null
			) Or
			(
				INSERTED.[Type] Is Not Null And
				DELETED.[Type] Is Null
			) Or
			(
				INSERTED.[Type] !=
				DELETED.[Type]
			)
		) left join CFGContactType as oldDesc  on DELETED.Type = oldDesc.Code  left join  CFGContactType as newDesc  on INSERTED.Type = newDesc.Code
		END		
		
      If UPDATE([LastName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'LastName',
      CONVERT(NVARCHAR(2000),DELETED.[LastName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LastName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[LastName] Is Null And
				DELETED.[LastName] Is Not Null
			) Or
			(
				INSERTED.[LastName] Is Not Null And
				DELETED.[LastName] Is Null
			) Or
			(
				INSERTED.[LastName] !=
				DELETED.[LastName]
			)
		) 
		END		
		
      If UPDATE([FirstName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirstName',
      CONVERT(NVARCHAR(2000),DELETED.[FirstName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirstName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FirstName] Is Null And
				DELETED.[FirstName] Is Not Null
			) Or
			(
				INSERTED.[FirstName] Is Not Null And
				DELETED.[FirstName] Is Null
			) Or
			(
				INSERTED.[FirstName] !=
				DELETED.[FirstName]
			)
		) 
		END		
		
      If UPDATE([MiddleName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'MiddleName',
      CONVERT(NVARCHAR(2000),DELETED.[MiddleName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MiddleName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[MiddleName] Is Null And
				DELETED.[MiddleName] Is Not Null
			) Or
			(
				INSERTED.[MiddleName] Is Not Null And
				DELETED.[MiddleName] Is Null
			) Or
			(
				INSERTED.[MiddleName] !=
				DELETED.[MiddleName]
			)
		) 
		END		
		
      If UPDATE([Salutation])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Salutation',
      CONVERT(NVARCHAR(2000),DELETED.[Salutation],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Salutation],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Salutation] Is Null And
				DELETED.[Salutation] Is Not Null
			) Or
			(
				INSERTED.[Salutation] Is Not Null And
				DELETED.[Salutation] Is Null
			) Or
			(
				INSERTED.[Salutation] !=
				DELETED.[Salutation]
			)
		) 
		END		
		
      If UPDATE([Suffix])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Suffix',
      CONVERT(NVARCHAR(2000),DELETED.[Suffix],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Suffix],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Suffix] Is Null And
				DELETED.[Suffix] Is Not Null
			) Or
			(
				INSERTED.[Suffix] Is Not Null And
				DELETED.[Suffix] Is Null
			) Or
			(
				INSERTED.[Suffix] !=
				DELETED.[Suffix]
			)
		) 
		END		
		
      If UPDATE([Title])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Title',
      CONVERT(NVARCHAR(2000),DELETED.[Title],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Title],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Title] Is Null And
				DELETED.[Title] Is Not Null
			) Or
			(
				INSERTED.[Title] Is Not Null And
				DELETED.[Title] Is Null
			) Or
			(
				INSERTED.[Title] !=
				DELETED.[Title]
			)
		) 
		END		
		
      If UPDATE([Addressee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Addressee',
      CONVERT(NVARCHAR(2000),DELETED.[Addressee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Addressee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Addressee] Is Null And
				DELETED.[Addressee] Is Not Null
			) Or
			(
				INSERTED.[Addressee] Is Not Null And
				DELETED.[Addressee] Is Null
			) Or
			(
				INSERTED.[Addressee] !=
				DELETED.[Addressee]
			)
		) 
		END		
		
      If UPDATE([Address1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Address1',
      CONVERT(NVARCHAR(2000),DELETED.[Address1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Address1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Address1] Is Null And
				DELETED.[Address1] Is Not Null
			) Or
			(
				INSERTED.[Address1] Is Not Null And
				DELETED.[Address1] Is Null
			) Or
			(
				INSERTED.[Address1] !=
				DELETED.[Address1]
			)
		) 
		END		
		
      If UPDATE([Address2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Address2',
      CONVERT(NVARCHAR(2000),DELETED.[Address2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Address2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Address2] Is Null And
				DELETED.[Address2] Is Not Null
			) Or
			(
				INSERTED.[Address2] Is Not Null And
				DELETED.[Address2] Is Null
			) Or
			(
				INSERTED.[Address2] !=
				DELETED.[Address2]
			)
		) 
		END		
		
      If UPDATE([Address3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Address3',
      CONVERT(NVARCHAR(2000),DELETED.[Address3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Address3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Address3] Is Null And
				DELETED.[Address3] Is Not Null
			) Or
			(
				INSERTED.[Address3] Is Not Null And
				DELETED.[Address3] Is Null
			) Or
			(
				INSERTED.[Address3] !=
				DELETED.[Address3]
			)
		) 
		END		
		
      If UPDATE([Address4])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Address4',
      CONVERT(NVARCHAR(2000),DELETED.[Address4],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Address4],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Address4] Is Null And
				DELETED.[Address4] Is Not Null
			) Or
			(
				INSERTED.[Address4] Is Not Null And
				DELETED.[Address4] Is Null
			) Or
			(
				INSERTED.[Address4] !=
				DELETED.[Address4]
			)
		) 
		END		
		
      If UPDATE([City])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'City',
      CONVERT(NVARCHAR(2000),DELETED.[City],121),
      CONVERT(NVARCHAR(2000),INSERTED.[City],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[City] Is Null And
				DELETED.[City] Is Not Null
			) Or
			(
				INSERTED.[City] Is Not Null And
				DELETED.[City] Is Null
			) Or
			(
				INSERTED.[City] !=
				DELETED.[City]
			)
		) 
		END		
		
     If UPDATE([State])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'State',
     CONVERT(NVARCHAR(2000),DELETED.[State],121),
     CONVERT(NVARCHAR(2000),INSERTED.[State],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[State] Is Null And
				DELETED.[State] Is Not Null
			) Or
			(
				INSERTED.[State] Is Not Null And
				DELETED.[State] Is Null
			) Or
			(
				INSERTED.[State] !=
				DELETED.[State]
			)
		) left join CFGStates as oldDesc  on DELETED.State = oldDesc.Code  left join  CFGStates as newDesc  on INSERTED.State = newDesc.Code
		END		
		
      If UPDATE([ZIP])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'ZIP',
      CONVERT(NVARCHAR(2000),DELETED.[ZIP],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ZIP],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[ZIP] Is Null And
				DELETED.[ZIP] Is Not Null
			) Or
			(
				INSERTED.[ZIP] Is Not Null And
				DELETED.[ZIP] Is Null
			) Or
			(
				INSERTED.[ZIP] !=
				DELETED.[ZIP]
			)
		) 
		END		
		
      If UPDATE([Country])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Country',
      CONVERT(NVARCHAR(2000),DELETED.[Country],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Country],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Country] Is Null And
				DELETED.[Country] Is Not Null
			) Or
			(
				INSERTED.[Country] Is Not Null And
				DELETED.[Country] Is Null
			) Or
			(
				INSERTED.[Country] !=
				DELETED.[Country]
			)
		) 
		END		
		
      If UPDATE([Phone])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Phone',
      CONVERT(NVARCHAR(2000),DELETED.[Phone],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Phone],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Phone] Is Null And
				DELETED.[Phone] Is Not Null
			) Or
			(
				INSERTED.[Phone] Is Not Null And
				DELETED.[Phone] Is Null
			) Or
			(
				INSERTED.[Phone] !=
				DELETED.[Phone]
			)
		) 
		END		
		
      If UPDATE([Fax])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Fax',
      CONVERT(NVARCHAR(2000),DELETED.[Fax],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fax],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Fax] Is Null And
				DELETED.[Fax] Is Not Null
			) Or
			(
				INSERTED.[Fax] Is Not Null And
				DELETED.[Fax] Is Null
			) Or
			(
				INSERTED.[Fax] !=
				DELETED.[Fax]
			)
		) 
		END		
		
      If UPDATE([Pager])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Pager',
      CONVERT(NVARCHAR(2000),DELETED.[Pager],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Pager],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Pager] Is Null And
				DELETED.[Pager] Is Not Null
			) Or
			(
				INSERTED.[Pager] Is Not Null And
				DELETED.[Pager] Is Null
			) Or
			(
				INSERTED.[Pager] !=
				DELETED.[Pager]
			)
		) 
		END		
		
      If UPDATE([CellPhone])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'CellPhone',
      CONVERT(NVARCHAR(2000),DELETED.[CellPhone],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CellPhone],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[CellPhone] Is Null And
				DELETED.[CellPhone] Is Not Null
			) Or
			(
				INSERTED.[CellPhone] Is Not Null And
				DELETED.[CellPhone] Is Null
			) Or
			(
				INSERTED.[CellPhone] !=
				DELETED.[CellPhone]
			)
		) 
		END		
		
      If UPDATE([HomePhone])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'HomePhone',
      CONVERT(NVARCHAR(2000),DELETED.[HomePhone],121),
      CONVERT(NVARCHAR(2000),INSERTED.[HomePhone],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[HomePhone] Is Null And
				DELETED.[HomePhone] Is Not Null
			) Or
			(
				INSERTED.[HomePhone] Is Not Null And
				DELETED.[HomePhone] Is Null
			) Or
			(
				INSERTED.[HomePhone] !=
				DELETED.[HomePhone]
			)
		) 
		END		
		
      If UPDATE([EMail])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'EMail',
      CONVERT(NVARCHAR(2000),DELETED.[EMail],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EMail],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[EMail] Is Null And
				DELETED.[EMail] Is Not Null
			) Or
			(
				INSERTED.[EMail] Is Not Null And
				DELETED.[EMail] Is Null
			) Or
			(
				INSERTED.[EMail] !=
				DELETED.[EMail]
			)
		) 
		END		
		
      If UPDATE([Memo])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Memo',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([MailingAddress])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'MailingAddress',
      CONVERT(NVARCHAR(2000),DELETED.[MailingAddress],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MailingAddress],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[MailingAddress] Is Null And
				DELETED.[MailingAddress] Is Not Null
			) Or
			(
				INSERTED.[MailingAddress] Is Not Null And
				DELETED.[MailingAddress] Is Null
			) Or
			(
				INSERTED.[MailingAddress] !=
				DELETED.[MailingAddress]
			)
		) 
		END		
		
      If UPDATE([Billing])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Billing',
      CONVERT(NVARCHAR(2000),DELETED.[Billing],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Billing],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Billing] Is Null And
				DELETED.[Billing] Is Not Null
			) Or
			(
				INSERTED.[Billing] Is Not Null And
				DELETED.[Billing] Is Null
			) Or
			(
				INSERTED.[Billing] !=
				DELETED.[Billing]
			)
		) 
		END		
		
      If UPDATE([PrimaryInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'PrimaryInd',
      CONVERT(NVARCHAR(2000),DELETED.[PrimaryInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PrimaryInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[PrimaryInd] Is Null And
				DELETED.[PrimaryInd] Is Not Null
			) Or
			(
				INSERTED.[PrimaryInd] Is Not Null And
				DELETED.[PrimaryInd] Is Null
			) Or
			(
				INSERTED.[PrimaryInd] !=
				DELETED.[PrimaryInd]
			)
		) 
		END		
		
     If UPDATE([ContactStatus])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'ContactStatus',
     CONVERT(NVARCHAR(2000),DELETED.[ContactStatus],121),
     CONVERT(NVARCHAR(2000),INSERTED.[ContactStatus],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[ContactStatus] Is Null And
				DELETED.[ContactStatus] Is Not Null
			) Or
			(
				INSERTED.[ContactStatus] Is Not Null And
				DELETED.[ContactStatus] Is Null
			) Or
			(
				INSERTED.[ContactStatus] !=
				DELETED.[ContactStatus]
			)
		) left join ContactStatus as oldDesc  on DELETED.ContactStatus = oldDesc.Code  left join  ContactStatus as newDesc  on INSERTED.ContactStatus = newDesc.Code
		END		
		
      If UPDATE([PreferredName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'PreferredName',
      CONVERT(NVARCHAR(2000),DELETED.[PreferredName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PreferredName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[PreferredName] Is Null And
				DELETED.[PreferredName] Is Not Null
			) Or
			(
				INSERTED.[PreferredName] Is Not Null And
				DELETED.[PreferredName] Is Null
			) Or
			(
				INSERTED.[PreferredName] !=
				DELETED.[PreferredName]
			)
		) 
		END		
		
      If UPDATE([CustomCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'CustomCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustomCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustomCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[CustomCurrencyCode] Is Null And
				DELETED.[CustomCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] Is Not Null And
				DELETED.[CustomCurrencyCode] Is Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] !=
				DELETED.[CustomCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([Source])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Source',
      CONVERT(NVARCHAR(2000),DELETED.[Source],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Source],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Source] Is Null And
				DELETED.[Source] Is Not Null
			) Or
			(
				INSERTED.[Source] Is Not Null And
				DELETED.[Source] Is Null
			) Or
			(
				INSERTED.[Source] !=
				DELETED.[Source]
			)
		) 
		END		
		
      If UPDATE([PhoneFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'PhoneFormat',
      CONVERT(NVARCHAR(2000),DELETED.[PhoneFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PhoneFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[PhoneFormat] Is Null And
				DELETED.[PhoneFormat] Is Not Null
			) Or
			(
				INSERTED.[PhoneFormat] Is Not Null And
				DELETED.[PhoneFormat] Is Null
			) Or
			(
				INSERTED.[PhoneFormat] !=
				DELETED.[PhoneFormat]
			)
		) 
		END		
		
      If UPDATE([FaxFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FaxFormat',
      CONVERT(NVARCHAR(2000),DELETED.[FaxFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FaxFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FaxFormat] Is Null And
				DELETED.[FaxFormat] Is Not Null
			) Or
			(
				INSERTED.[FaxFormat] Is Not Null And
				DELETED.[FaxFormat] Is Null
			) Or
			(
				INSERTED.[FaxFormat] !=
				DELETED.[FaxFormat]
			)
		) 
		END		
		
      If UPDATE([PagerFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'PagerFormat',
      CONVERT(NVARCHAR(2000),DELETED.[PagerFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PagerFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[PagerFormat] Is Null And
				DELETED.[PagerFormat] Is Not Null
			) Or
			(
				INSERTED.[PagerFormat] Is Not Null And
				DELETED.[PagerFormat] Is Null
			) Or
			(
				INSERTED.[PagerFormat] !=
				DELETED.[PagerFormat]
			)
		) 
		END		
		
      If UPDATE([CellPhoneFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'CellPhoneFormat',
      CONVERT(NVARCHAR(2000),DELETED.[CellPhoneFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CellPhoneFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[CellPhoneFormat] Is Null And
				DELETED.[CellPhoneFormat] Is Not Null
			) Or
			(
				INSERTED.[CellPhoneFormat] Is Not Null And
				DELETED.[CellPhoneFormat] Is Null
			) Or
			(
				INSERTED.[CellPhoneFormat] !=
				DELETED.[CellPhoneFormat]
			)
		) 
		END		
		
      If UPDATE([HomePhoneFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'HomePhoneFormat',
      CONVERT(NVARCHAR(2000),DELETED.[HomePhoneFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[HomePhoneFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[HomePhoneFormat] Is Null And
				DELETED.[HomePhoneFormat] Is Not Null
			) Or
			(
				INSERTED.[HomePhoneFormat] Is Not Null And
				DELETED.[HomePhoneFormat] Is Null
			) Or
			(
				INSERTED.[HomePhoneFormat] !=
				DELETED.[HomePhoneFormat]
			)
		) 
		END		
		
      If UPDATE([AjeraSync])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'AjeraSync',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraSync],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraSync],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[AjeraSync] Is Null And
				DELETED.[AjeraSync] Is Not Null
			) Or
			(
				INSERTED.[AjeraSync] Is Not Null And
				DELETED.[AjeraSync] Is Null
			) Or
			(
				INSERTED.[AjeraSync] !=
				DELETED.[AjeraSync]
			)
		) 
		END		
		
      If UPDATE([TLInternalKey])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'TLInternalKey',
      CONVERT(NVARCHAR(2000),DELETED.[TLInternalKey],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TLInternalKey],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[TLInternalKey] Is Null And
				DELETED.[TLInternalKey] Is Not Null
			) Or
			(
				INSERTED.[TLInternalKey] Is Not Null And
				DELETED.[TLInternalKey] Is Null
			) Or
			(
				INSERTED.[TLInternalKey] !=
				DELETED.[TLInternalKey]
			)
		) 
		END		
		
      If UPDATE([TLSyncModDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'TLSyncModDate',
      CONVERT(NVARCHAR(2000),DELETED.[TLSyncModDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TLSyncModDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[TLSyncModDate] Is Null And
				DELETED.[TLSyncModDate] Is Not Null
			) Or
			(
				INSERTED.[TLSyncModDate] Is Not Null And
				DELETED.[TLSyncModDate] Is Null
			) Or
			(
				INSERTED.[TLSyncModDate] !=
				DELETED.[TLSyncModDate]
			)
		) 
		END		
		
      If UPDATE([Owner])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Owner',
      CONVERT(NVARCHAR(2000),DELETED.[Owner],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Owner],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Owner] Is Null And
				DELETED.[Owner] Is Not Null
			) Or
			(
				INSERTED.[Owner] Is Not Null And
				DELETED.[Owner] Is Null
			) Or
			(
				INSERTED.[Owner] !=
				DELETED.[Owner]
			)
		) 
		END		
		
      If UPDATE([StatusReason])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'StatusReason',
      CONVERT(NVARCHAR(2000),DELETED.[StatusReason],121),
      CONVERT(NVARCHAR(2000),INSERTED.[StatusReason],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[StatusReason] Is Null And
				DELETED.[StatusReason] Is Not Null
			) Or
			(
				INSERTED.[StatusReason] Is Not Null And
				DELETED.[StatusReason] Is Null
			) Or
			(
				INSERTED.[StatusReason] !=
				DELETED.[StatusReason]
			)
		) 
		END		
		
      If UPDATE([Rating])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Rating',
      CONVERT(NVARCHAR(2000),DELETED.[Rating],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Rating],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Rating] Is Null And
				DELETED.[Rating] Is Not Null
			) Or
			(
				INSERTED.[Rating] Is Not Null And
				DELETED.[Rating] Is Null
			) Or
			(
				INSERTED.[Rating] !=
				DELETED.[Rating]
			)
		) 
		END		
		
      If UPDATE([FirmDescription])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmDescription',
      CONVERT(NVARCHAR(2000),DELETED.[FirmDescription],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirmDescription],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FirmDescription] Is Null And
				DELETED.[FirmDescription] Is Not Null
			) Or
			(
				INSERTED.[FirmDescription] Is Not Null And
				DELETED.[FirmDescription] Is Null
			) Or
			(
				INSERTED.[FirmDescription] !=
				DELETED.[FirmDescription]
			)
		) 
		END		
		
      If UPDATE([FirmAddress1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmAddress1',
      CONVERT(NVARCHAR(2000),DELETED.[FirmAddress1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirmAddress1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FirmAddress1] Is Null And
				DELETED.[FirmAddress1] Is Not Null
			) Or
			(
				INSERTED.[FirmAddress1] Is Not Null And
				DELETED.[FirmAddress1] Is Null
			) Or
			(
				INSERTED.[FirmAddress1] !=
				DELETED.[FirmAddress1]
			)
		) 
		END		
		
      If UPDATE([FirmAddress2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmAddress2',
      CONVERT(NVARCHAR(2000),DELETED.[FirmAddress2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirmAddress2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FirmAddress2] Is Null And
				DELETED.[FirmAddress2] Is Not Null
			) Or
			(
				INSERTED.[FirmAddress2] Is Not Null And
				DELETED.[FirmAddress2] Is Null
			) Or
			(
				INSERTED.[FirmAddress2] !=
				DELETED.[FirmAddress2]
			)
		) 
		END		
		
      If UPDATE([FirmAddress3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmAddress3',
      CONVERT(NVARCHAR(2000),DELETED.[FirmAddress3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirmAddress3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FirmAddress3] Is Null And
				DELETED.[FirmAddress3] Is Not Null
			) Or
			(
				INSERTED.[FirmAddress3] Is Not Null And
				DELETED.[FirmAddress3] Is Null
			) Or
			(
				INSERTED.[FirmAddress3] !=
				DELETED.[FirmAddress3]
			)
		) 
		END		
		
      If UPDATE([FirmAddress4])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmAddress4',
      CONVERT(NVARCHAR(2000),DELETED.[FirmAddress4],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirmAddress4],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FirmAddress4] Is Null And
				DELETED.[FirmAddress4] Is Not Null
			) Or
			(
				INSERTED.[FirmAddress4] Is Not Null And
				DELETED.[FirmAddress4] Is Null
			) Or
			(
				INSERTED.[FirmAddress4] !=
				DELETED.[FirmAddress4]
			)
		) 
		END		
		
      If UPDATE([FirmCity])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmCity',
      CONVERT(NVARCHAR(2000),DELETED.[FirmCity],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirmCity],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FirmCity] Is Null And
				DELETED.[FirmCity] Is Not Null
			) Or
			(
				INSERTED.[FirmCity] Is Not Null And
				DELETED.[FirmCity] Is Null
			) Or
			(
				INSERTED.[FirmCity] !=
				DELETED.[FirmCity]
			)
		) 
		END		
		
      If UPDATE([FirmState])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmState',
      CONVERT(NVARCHAR(2000),DELETED.[FirmState],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirmState],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FirmState] Is Null And
				DELETED.[FirmState] Is Not Null
			) Or
			(
				INSERTED.[FirmState] Is Not Null And
				DELETED.[FirmState] Is Null
			) Or
			(
				INSERTED.[FirmState] !=
				DELETED.[FirmState]
			)
		) 
		END		
		
      If UPDATE([FirmZip])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmZip',
      CONVERT(NVARCHAR(2000),DELETED.[FirmZip],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirmZip],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FirmZip] Is Null And
				DELETED.[FirmZip] Is Not Null
			) Or
			(
				INSERTED.[FirmZip] Is Not Null And
				DELETED.[FirmZip] Is Null
			) Or
			(
				INSERTED.[FirmZip] !=
				DELETED.[FirmZip]
			)
		) 
		END		
		
      If UPDATE([FirmCountry])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmCountry',
      CONVERT(NVARCHAR(2000),DELETED.[FirmCountry],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirmCountry],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FirmCountry] Is Null And
				DELETED.[FirmCountry] Is Not Null
			) Or
			(
				INSERTED.[FirmCountry] Is Not Null And
				DELETED.[FirmCountry] Is Null
			) Or
			(
				INSERTED.[FirmCountry] !=
				DELETED.[FirmCountry]
			)
		) 
		END		
		
      If UPDATE([FirmBusinessPhone])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmBusinessPhone',
      CONVERT(NVARCHAR(2000),DELETED.[FirmBusinessPhone],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirmBusinessPhone],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FirmBusinessPhone] Is Null And
				DELETED.[FirmBusinessPhone] Is Not Null
			) Or
			(
				INSERTED.[FirmBusinessPhone] Is Not Null And
				DELETED.[FirmBusinessPhone] Is Null
			) Or
			(
				INSERTED.[FirmBusinessPhone] !=
				DELETED.[FirmBusinessPhone]
			)
		) 
		END		
		
      If UPDATE([FirmBusinessFax])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmBusinessFax',
      CONVERT(NVARCHAR(2000),DELETED.[FirmBusinessFax],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirmBusinessFax],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FirmBusinessFax] Is Null And
				DELETED.[FirmBusinessFax] Is Not Null
			) Or
			(
				INSERTED.[FirmBusinessFax] Is Not Null And
				DELETED.[FirmBusinessFax] Is Null
			) Or
			(
				INSERTED.[FirmBusinessFax] !=
				DELETED.[FirmBusinessFax]
			)
		) 
		END		
		
      If UPDATE([FirmPager])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmPager',
      CONVERT(NVARCHAR(2000),DELETED.[FirmPager],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirmPager],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FirmPager] Is Null And
				DELETED.[FirmPager] Is Not Null
			) Or
			(
				INSERTED.[FirmPager] Is Not Null And
				DELETED.[FirmPager] Is Null
			) Or
			(
				INSERTED.[FirmPager] !=
				DELETED.[FirmPager]
			)
		) 
		END		
		
      If UPDATE([ProjectDescription])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'ProjectDescription',
      CONVERT(NVARCHAR(2000),DELETED.[ProjectDescription],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProjectDescription],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[ProjectDescription] Is Null And
				DELETED.[ProjectDescription] Is Not Null
			) Or
			(
				INSERTED.[ProjectDescription] Is Not Null And
				DELETED.[ProjectDescription] Is Null
			) Or
			(
				INSERTED.[ProjectDescription] !=
				DELETED.[ProjectDescription]
			)
		) 
		END		
		
      If UPDATE([Market])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Market',
      CONVERT(NVARCHAR(2000),DELETED.[Market],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Market],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Market] Is Null And
				DELETED.[Market] Is Not Null
			) Or
			(
				INSERTED.[Market] Is Not Null And
				DELETED.[Market] Is Null
			) Or
			(
				INSERTED.[Market] !=
				DELETED.[Market]
			)
		) 
		END		
		
      If UPDATE([Website])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Website',
      CONVERT(NVARCHAR(2000),DELETED.[Website],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Website],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Website] Is Null And
				DELETED.[Website] Is Not Null
			) Or
			(
				INSERTED.[Website] Is Not Null And
				DELETED.[Website] Is Null
			) Or
			(
				INSERTED.[Website] !=
				DELETED.[Website]
			)
		) 
		END		
		
      If UPDATE([QualifiedStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'QualifiedStatus',
      CONVERT(NVARCHAR(2000),DELETED.[QualifiedStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[QualifiedStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[QualifiedStatus] Is Null And
				DELETED.[QualifiedStatus] Is Not Null
			) Or
			(
				INSERTED.[QualifiedStatus] Is Not Null And
				DELETED.[QualifiedStatus] Is Null
			) Or
			(
				INSERTED.[QualifiedStatus] !=
				DELETED.[QualifiedStatus]
			)
		) 
		END		
		
      If UPDATE([StatusDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'StatusDate',
      CONVERT(NVARCHAR(2000),DELETED.[StatusDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[StatusDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[StatusDate] Is Null And
				DELETED.[StatusDate] Is Not Null
			) Or
			(
				INSERTED.[StatusDate] Is Not Null And
				DELETED.[StatusDate] Is Null
			) Or
			(
				INSERTED.[StatusDate] !=
				DELETED.[StatusDate]
			)
		) 
		END		
		
      If UPDATE([ProfessionalSuffix])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'ProfessionalSuffix',
      CONVERT(NVARCHAR(2000),DELETED.[ProfessionalSuffix],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProfessionalSuffix],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[ProfessionalSuffix] Is Null And
				DELETED.[ProfessionalSuffix] Is Not Null
			) Or
			(
				INSERTED.[ProfessionalSuffix] Is Not Null And
				DELETED.[ProfessionalSuffix] Is Null
			) Or
			(
				INSERTED.[ProfessionalSuffix] !=
				DELETED.[ProfessionalSuffix]
			)
		) 
		END		
		
      If UPDATE([SFID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'SFID',
      CONVERT(NVARCHAR(2000),DELETED.[SFID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SFID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[SFID] Is Null And
				DELETED.[SFID] Is Not Null
			) Or
			(
				INSERTED.[SFID] Is Not Null And
				DELETED.[SFID] Is Null
			) Or
			(
				INSERTED.[SFID] !=
				DELETED.[SFID]
			)
		) 
		END		
		
      If UPDATE([SFLastModifiedDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'SFLastModifiedDate',
      CONVERT(NVARCHAR(2000),DELETED.[SFLastModifiedDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SFLastModifiedDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[SFLastModifiedDate] Is Null And
				DELETED.[SFLastModifiedDate] Is Not Null
			) Or
			(
				INSERTED.[SFLastModifiedDate] Is Not Null And
				DELETED.[SFLastModifiedDate] Is Null
			) Or
			(
				INSERTED.[SFLastModifiedDate] !=
				DELETED.[SFLastModifiedDate]
			)
		) 
		END		
		
      If UPDATE([FirmBusinessPhoneFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmBusinessPhoneFormat',
      CONVERT(NVARCHAR(2000),DELETED.[FirmBusinessPhoneFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirmBusinessPhoneFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FirmBusinessPhoneFormat] Is Null And
				DELETED.[FirmBusinessPhoneFormat] Is Not Null
			) Or
			(
				INSERTED.[FirmBusinessPhoneFormat] Is Not Null And
				DELETED.[FirmBusinessPhoneFormat] Is Null
			) Or
			(
				INSERTED.[FirmBusinessPhoneFormat] !=
				DELETED.[FirmBusinessPhoneFormat]
			)
		) 
		END		
		
      If UPDATE([FirmBusinessFaxFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FirmBusinessFaxFormat',
      CONVERT(NVARCHAR(2000),DELETED.[FirmBusinessFaxFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirmBusinessFaxFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FirmBusinessFaxFormat] Is Null And
				DELETED.[FirmBusinessFaxFormat] Is Not Null
			) Or
			(
				INSERTED.[FirmBusinessFaxFormat] Is Not Null And
				DELETED.[FirmBusinessFaxFormat] Is Null
			) Or
			(
				INSERTED.[FirmBusinessFaxFormat] !=
				DELETED.[FirmBusinessFaxFormat]
			)
		) 
		END		
		
      If UPDATE([QBOID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'QBOID',
      CONVERT(NVARCHAR(2000),DELETED.[QBOID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[QBOID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[QBOID] Is Null And
				DELETED.[QBOID] Is Not Null
			) Or
			(
				INSERTED.[QBOID] Is Not Null And
				DELETED.[QBOID] Is Null
			) Or
			(
				INSERTED.[QBOID] !=
				DELETED.[QBOID]
			)
		) 
		END		
		
      If UPDATE([QBOIsMainContact])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'QBOIsMainContact',
      CONVERT(NVARCHAR(2000),DELETED.[QBOIsMainContact],121),
      CONVERT(NVARCHAR(2000),INSERTED.[QBOIsMainContact],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[QBOIsMainContact] Is Null And
				DELETED.[QBOIsMainContact] Is Not Null
			) Or
			(
				INSERTED.[QBOIsMainContact] Is Not Null And
				DELETED.[QBOIsMainContact] Is Null
			) Or
			(
				INSERTED.[QBOIsMainContact] !=
				DELETED.[QBOIsMainContact]
			)
		) 
		END		
		
      If UPDATE([QBOLastUpdated])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'QBOLastUpdated',
      CONVERT(NVARCHAR(2000),DELETED.[QBOLastUpdated],121),
      CONVERT(NVARCHAR(2000),INSERTED.[QBOLastUpdated],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[QBOLastUpdated] Is Null And
				DELETED.[QBOLastUpdated] Is Not Null
			) Or
			(
				INSERTED.[QBOLastUpdated] Is Not Null And
				DELETED.[QBOLastUpdated] Is Null
			) Or
			(
				INSERTED.[QBOLastUpdated] !=
				DELETED.[QBOLastUpdated]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_Contacts] ON [dbo].[Contacts]
GO
ALTER TABLE [dbo].[Contacts] ADD CONSTRAINT [ContactsPK] PRIMARY KEY NONCLUSTERED ([ContactID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ContactsClientIDIDX] ON [dbo].[Contacts] ([ClientID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [ContactsContactIDIDX] ON [dbo].[Contacts] ([ContactID]) INCLUDE ([LastName], [FirstName], [Suffix], [Phone], [CellPhone], [EMail], [PreferredName], [PhoneFormat], [CellPhoneFormat], [ClientID], [State], [ZIP], [Fax], [Pager], [HomePhone], [MailingAddress], [HomePhoneFormat], [CLAddress], [Salutation], [Title], [Address1], [Address2], [Address3], [Address4], [City]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ContactsCreateDateIDX] ON [dbo].[Contacts] ([CreateDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ContactsModDateIDX] ON [dbo].[Contacts] ([ModDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ContactsQBOIDIDX] ON [dbo].[Contacts] ([QBOID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ContactsSFIDIDX] ON [dbo].[Contacts] ([SFID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
