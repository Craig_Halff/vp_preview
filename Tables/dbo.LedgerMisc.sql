CREATE TABLE [dbo].[LedgerMisc]
(
[Period] [int] NOT NULL CONSTRAINT [DF__LedgerMis__Perio__37A83DDF] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__LedgerMis__PostS__389C6218] DEFAULT ((0)),
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransDate] [datetime] NULL,
[Desc1] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Desc2] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__Amoun__39908651] DEFAULT ((0)),
[CBAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__CBAmo__3A84AA8A] DEFAULT ((0)),
[BillExt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__BillE__3B78CEC3] DEFAULT ((0)),
[ProjectCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerMis__Proje__3C6CF2FC] DEFAULT ('N'),
[AutoEntry] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerMis__AutoE__3D611735] DEFAULT ('N'),
[SuppressBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerMis__Suppr__3E553B6E] DEFAULT ('N'),
[BillStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SkipGL] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerMis__SkipG__3F495FA7] DEFAULT ('N'),
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceSection] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Line] [smallint] NOT NULL CONSTRAINT [DF__LedgerMisc__Line__403D83E0] DEFAULT ((0)),
[PartialPayment] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__Parti__4131A819] DEFAULT ((0)),
[Discount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__Disco__4225CC52] DEFAULT ((0)),
[Voucher] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledInvoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledPeriod] [int] NOT NULL CONSTRAINT [DF__LedgerMis__Bille__4319F08B] DEFAULT ((0)),
[Unit] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitTable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitQuantity] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__UnitQ__440E14C4] DEFAULT ((0)),
[UnitCostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__UnitC__450238FD] DEFAULT ((0)),
[UnitBillingRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__UnitB__45F65D36] DEFAULT ((0)),
[UnitBillExt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__UnitB__46EA816F] DEFAULT ((0)),
[XferWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__TaxBa__47DEA5A8] DEFAULT ((0)),
[TaxCBBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__TaxCB__48D2C9E1] DEFAULT ((0)),
[BillTaxCodeOverride] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WrittenOffPeriod] [int] NOT NULL CONSTRAINT [DF__LedgerMis__Writt__49C6EE1A] DEFAULT ((0)),
[TransactionAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__Trans__4ABB1253] DEFAULT ((0)),
[TransactionCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__Amoun__4BAF368C] DEFAULT ((0)),
[ProjectExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__Amoun__4CA35AC5] DEFAULT ((0)),
[BillingExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoEntryAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__AutoE__4D977EFE] DEFAULT ((0)),
[AutoEntryExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoEntryOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoEntryAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountSourceCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__Amoun__4E8BA337] DEFAULT ((0)),
[SourceExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PONumber] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitCostRateBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__UnitC__4F7FC770] DEFAULT ((0)),
[LinkCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillTax2CodeOverride] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GainsAndLossesType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__Amoun__5073EBA9] DEFAULT ((0)),
[CBAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__CBAmo__51680FE2] DEFAULT ((0)),
[TaxBasisTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__TaxBa__525C341B] DEFAULT ((0)),
[TaxCBBasisTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__TaxCB__53505854] DEFAULT ((0)),
[TaxBasisFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__TaxBa__54447C8D] DEFAULT ((0)),
[TaxCBBasisFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__TaxCB__5538A0C6] DEFAULT ((0)),
[DiscountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__Disco__562CC4FF] DEFAULT ((0)),
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RealizationAmountEmployeeCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__Reali__5720E938] DEFAULT ((0)),
[RealizationAmountProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__Reali__58150D71] DEFAULT ((0)),
[RealizationAmountBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__Reali__590931AA] DEFAULT ((0)),
[NonBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__LedgerMis__NonBi__59FD55E3] DEFAULT ('N'),
[CreditMemoRefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalAmountSourceCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__Origi__5AF17A1C] DEFAULT ((0)),
[OriginalPaymentCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InProcessAccount] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerMis__InPro__5BE59E55] DEFAULT ('N'),
[InProcessAccountCleared] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerMis__InPro__5CD9C28E] DEFAULT ('N'),
[EKOriginalLine] [smallint] NOT NULL CONSTRAINT [DF__LedgerMis__EKOri__5DCDE6C7] DEFAULT ((0)),
[InvoiceStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginatingVendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Diary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaryNo] [int] NOT NULL CONSTRAINT [DF__LedgerMis__Diary__5EC20B00] DEFAULT ((0)),
[CreditCardPrimaryCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransferredPeriod] [int] NOT NULL CONSTRAINT [DF__LedgerMis__Trans__5FB62F39] DEFAULT ((0)),
[TransferredBillStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreInvoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EquipmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BookCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastPSAExportDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LedgerMisc] ADD CONSTRAINT [LedgerMiscPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerMiscAccountIDX] ON [dbo].[LedgerMisc] ([Account]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerMiscBilledWBS1WBS2WBS3IDX] ON [dbo].[LedgerMisc] ([BilledWBS1], [BilledWBS2], [BilledWBS3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerMiscBillStatusIDX] ON [dbo].[LedgerMisc] ([BillStatus]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerMiscEmployeeVoucherIDX] ON [dbo].[LedgerMisc] ([Employee], [Voucher]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerMiscEquipIDBookCodeIDX] ON [dbo].[LedgerMisc] ([EquipmentID], [BookCode]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerMiscGainsAndLossesTypePeriodIDX] ON [dbo].[LedgerMisc] ([GainsAndLossesType], [Period]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerMiscInvoiceIDX] ON [dbo].[LedgerMisc] ([Invoice]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerMiscProjectCostPeriodIDX] ON [dbo].[LedgerMisc] ([ProjectCost], [Period]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerMiscTransTypeSubTypeIDX] ON [dbo].[LedgerMisc] ([TransType], [SubType]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerMiscVendorVoucherIDX] ON [dbo].[LedgerMisc] ([Vendor], [Voucher]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerMiscWBS1AccountIDX] ON [dbo].[LedgerMisc] ([WBS1], [Account]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerMiscWBS1WBS2WBS3IDX] ON [dbo].[LedgerMisc] ([WBS1], [WBS2], [WBS3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerMiscCoveringIDX] ON [dbo].[LedgerMisc] ([WBS1], [WBS2], [WBS3], [Account], [Vendor], [Employee], [Unit], [UnitTable], [TransDate], [ProjectCost], [TransType]) INCLUDE ([BillExt], [UnitQuantity], [AmountProjectCurrency]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
