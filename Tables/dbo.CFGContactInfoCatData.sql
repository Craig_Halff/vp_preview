CREATE TABLE [dbo].[CFGContactInfoCatData]
(
[Code] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGContactInfoCatData] ADD CONSTRAINT [CFGcontactInfoCatDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
