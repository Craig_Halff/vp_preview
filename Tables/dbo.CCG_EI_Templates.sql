CREATE TABLE [dbo].[CCG_EI_Templates]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[DateChanged] [datetime] NULL,
[ChangedBy] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_Templates] ADD CONSTRAINT [PK_CCG_EI_Templates] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Description of packaging templates available in the system', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Templates', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'The user who last changed the template', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Templates', 'COLUMN', N'ChangedBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date/time when this template was last changed', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Templates', 'COLUMN', N'DateChanged'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Templates', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Template name', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Templates', 'COLUMN', N'Name'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Templates', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The path to the template file', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Templates', 'COLUMN', N'Source'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Future use, currently all templates are of type ''File''', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Templates', 'COLUMN', N'SourceType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Status of the Template - [A]ctive / [I]nactive', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Templates', 'COLUMN', N'Status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The template type to be generated (XSLT / email / substitution / Word / Excel / Webservice)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Templates', 'COLUMN', N'Type'
GO
