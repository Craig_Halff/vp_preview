CREATE TABLE [dbo].[WorkflowEMActivity]
(
[ActivityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Owner] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PopupDismissed] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowEMActivity] ADD CONSTRAINT [WorkflowEMActivityPK] PRIMARY KEY NONCLUSTERED ([ActivityID], [Employee]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
