CREATE TABLE [dbo].[CFGAPApprovalStatusData]
(
[Code] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAPApprovalStatusData] ADD CONSTRAINT [CFGAPApprovalStatusDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
