CREATE TABLE [dbo].[EMDirectDeposit]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__EMDirectDep__Seq__4872D72E] DEFAULT ((0)),
[BankID] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Method] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMDirectD__AmtPc__4966FB67] DEFAULT ((0)),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMDirectD__Curre__4A5B1FA0] DEFAULT ((0)),
[Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMDirectD__Overr__4B4F43D9] DEFAULT ('N'),
[exMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[exAmtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMDirectD__exAmt__4C436812] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_EMDirectDeposit]
      ON [dbo].[EMDirectDeposit]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMDirectDeposit'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Employee',CONVERT(NVARCHAR(2000),DELETED.[Employee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Employee = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'EmployeeCompany',CONVERT(NVARCHAR(2000),[EmployeeCompany],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Seq',CONVERT(NVARCHAR(2000),[Seq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'BankID',CONVERT(NVARCHAR(2000),[BankID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Account',CONVERT(NVARCHAR(2000),[Account],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'AccountType',CONVERT(NVARCHAR(2000),[AccountType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Method',CONVERT(NVARCHAR(2000),[Method],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'AmtPct',CONVERT(NVARCHAR(2000),[AmtPct],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Status',CONVERT(NVARCHAR(2000),[Status],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CurrentAmt',CONVERT(NVARCHAR(2000),[CurrentAmt],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Override',CONVERT(NVARCHAR(2000),[Override],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'exMethod',CONVERT(NVARCHAR(2000),[exMethod],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'exAmtPct',CONVERT(NVARCHAR(2000),[exAmtPct],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_EMDirectDeposit] ON [dbo].[EMDirectDeposit]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_EMDirectDeposit]
      ON [dbo].[EMDirectDeposit]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMDirectDeposit'
    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Employee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Employee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Employee = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'EmployeeCompany',NULL,CONVERT(NVARCHAR(2000),[EmployeeCompany],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',NULL,CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'BankID',NULL,CONVERT(NVARCHAR(2000),[BankID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Account',NULL,CONVERT(NVARCHAR(2000),[Account],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'AccountType',NULL,CONVERT(NVARCHAR(2000),[AccountType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Method',NULL,CONVERT(NVARCHAR(2000),[Method],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'AmtPct',NULL,CONVERT(NVARCHAR(2000),[AmtPct],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Status',NULL,CONVERT(NVARCHAR(2000),[Status],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CurrentAmt',NULL,CONVERT(NVARCHAR(2000),[CurrentAmt],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Override',NULL,CONVERT(NVARCHAR(2000),[Override],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'exMethod',NULL,CONVERT(NVARCHAR(2000),[exMethod],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'exAmtPct',NULL,CONVERT(NVARCHAR(2000),[exAmtPct],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_EMDirectDeposit] ON [dbo].[EMDirectDeposit]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_EMDirectDeposit]
      ON [dbo].[EMDirectDeposit]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMDirectDeposit'
    
     If UPDATE([Employee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Employee',
     CONVERT(NVARCHAR(2000),DELETED.[Employee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Employee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Employee] Is Null And
				DELETED.[Employee] Is Not Null
			) Or
			(
				INSERTED.[Employee] Is Not Null And
				DELETED.[Employee] Is Null
			) Or
			(
				INSERTED.[Employee] !=
				DELETED.[Employee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Employee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Employee = newDesc.Employee
		END		
		
      If UPDATE([EmployeeCompany])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'EmployeeCompany',
      CONVERT(NVARCHAR(2000),DELETED.[EmployeeCompany],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EmployeeCompany],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[EmployeeCompany] Is Null And
				DELETED.[EmployeeCompany] Is Not Null
			) Or
			(
				INSERTED.[EmployeeCompany] Is Not Null And
				DELETED.[EmployeeCompany] Is Null
			) Or
			(
				INSERTED.[EmployeeCompany] !=
				DELETED.[EmployeeCompany]
			)
		) 
		END		
		
      If UPDATE([Seq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',
      CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] Is Null And
				DELETED.[Seq] Is Not Null
			) Or
			(
				INSERTED.[Seq] Is Not Null And
				DELETED.[Seq] Is Null
			) Or
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
		
      If UPDATE([BankID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'BankID',
      CONVERT(NVARCHAR(2000),DELETED.[BankID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BankID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[BankID] Is Null And
				DELETED.[BankID] Is Not Null
			) Or
			(
				INSERTED.[BankID] Is Not Null And
				DELETED.[BankID] Is Null
			) Or
			(
				INSERTED.[BankID] !=
				DELETED.[BankID]
			)
		) 
		END		
		
      If UPDATE([Account])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Account',
      CONVERT(NVARCHAR(2000),DELETED.[Account],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Account],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Account] Is Null And
				DELETED.[Account] Is Not Null
			) Or
			(
				INSERTED.[Account] Is Not Null And
				DELETED.[Account] Is Null
			) Or
			(
				INSERTED.[Account] !=
				DELETED.[Account]
			)
		) 
		END		
		
      If UPDATE([AccountType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'AccountType',
      CONVERT(NVARCHAR(2000),DELETED.[AccountType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccountType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[AccountType] Is Null And
				DELETED.[AccountType] Is Not Null
			) Or
			(
				INSERTED.[AccountType] Is Not Null And
				DELETED.[AccountType] Is Null
			) Or
			(
				INSERTED.[AccountType] !=
				DELETED.[AccountType]
			)
		) 
		END		
		
      If UPDATE([Method])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Method',
      CONVERT(NVARCHAR(2000),DELETED.[Method],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Method],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Method] Is Null And
				DELETED.[Method] Is Not Null
			) Or
			(
				INSERTED.[Method] Is Not Null And
				DELETED.[Method] Is Null
			) Or
			(
				INSERTED.[Method] !=
				DELETED.[Method]
			)
		) 
		END		
		
      If UPDATE([AmtPct])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'AmtPct',
      CONVERT(NVARCHAR(2000),DELETED.[AmtPct],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AmtPct],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[AmtPct] Is Null And
				DELETED.[AmtPct] Is Not Null
			) Or
			(
				INSERTED.[AmtPct] Is Not Null And
				DELETED.[AmtPct] Is Null
			) Or
			(
				INSERTED.[AmtPct] !=
				DELETED.[AmtPct]
			)
		) 
		END		
		
      If UPDATE([Status])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Status',
      CONVERT(NVARCHAR(2000),DELETED.[Status],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Status],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Status] Is Null And
				DELETED.[Status] Is Not Null
			) Or
			(
				INSERTED.[Status] Is Not Null And
				DELETED.[Status] Is Null
			) Or
			(
				INSERTED.[Status] !=
				DELETED.[Status]
			)
		) 
		END		
		
      If UPDATE([CurrentAmt])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CurrentAmt',
      CONVERT(NVARCHAR(2000),DELETED.[CurrentAmt],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CurrentAmt],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CurrentAmt] Is Null And
				DELETED.[CurrentAmt] Is Not Null
			) Or
			(
				INSERTED.[CurrentAmt] Is Not Null And
				DELETED.[CurrentAmt] Is Null
			) Or
			(
				INSERTED.[CurrentAmt] !=
				DELETED.[CurrentAmt]
			)
		) 
		END		
		
      If UPDATE([Override])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Override',
      CONVERT(NVARCHAR(2000),DELETED.[Override],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Override],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Override] Is Null And
				DELETED.[Override] Is Not Null
			) Or
			(
				INSERTED.[Override] Is Not Null And
				DELETED.[Override] Is Null
			) Or
			(
				INSERTED.[Override] !=
				DELETED.[Override]
			)
		) 
		END		
		
      If UPDATE([exMethod])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'exMethod',
      CONVERT(NVARCHAR(2000),DELETED.[exMethod],121),
      CONVERT(NVARCHAR(2000),INSERTED.[exMethod],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[exMethod] Is Null And
				DELETED.[exMethod] Is Not Null
			) Or
			(
				INSERTED.[exMethod] Is Not Null And
				DELETED.[exMethod] Is Null
			) Or
			(
				INSERTED.[exMethod] !=
				DELETED.[exMethod]
			)
		) 
		END		
		
      If UPDATE([exAmtPct])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'exAmtPct',
      CONVERT(NVARCHAR(2000),DELETED.[exAmtPct],121),
      CONVERT(NVARCHAR(2000),INSERTED.[exAmtPct],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[exAmtPct] Is Null And
				DELETED.[exAmtPct] Is Not Null
			) Or
			(
				INSERTED.[exAmtPct] Is Not Null And
				DELETED.[exAmtPct] Is Null
			) Or
			(
				INSERTED.[exAmtPct] !=
				DELETED.[exAmtPct]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_EMDirectDeposit] ON [dbo].[EMDirectDeposit]
GO
ALTER TABLE [dbo].[EMDirectDeposit] ADD CONSTRAINT [EMDirectDepositPK] PRIMARY KEY NONCLUSTERED ([Employee], [EmployeeCompany], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
