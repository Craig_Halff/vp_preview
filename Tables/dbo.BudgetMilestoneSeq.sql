CREATE TABLE [dbo].[BudgetMilestoneSeq]
(
[AlertID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__BudgetMiles__Seq__1E86BF03] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BudgetMilestoneSeq] ADD CONSTRAINT [BudgetMilestoneSeqPK] PRIMARY KEY NONCLUSTERED ([AlertID], [PlanID], [Account], [Vendor], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
