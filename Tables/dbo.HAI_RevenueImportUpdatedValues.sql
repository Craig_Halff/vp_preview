CREATE TABLE [dbo].[HAI_RevenueImportUpdatedValues]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustFeeType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEACInputLevel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEffortatCompletion] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEfforttoComplete] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustWriteoffAdjmt] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFeeTypeRollup] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPercentComplete] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Modified] [datetime2] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HAI_RevenueImportUpdatedValues] ADD CONSTRAINT [PK_HAI_RevenueImportUpdatedValues] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HAI_RevenueImportUpdatedValues] ADD CONSTRAINT [DF_HAI_RevenueImportUpdatedValues_WBS1WBS2WBS3] UNIQUE NONCLUSTERED ([WBS1], [WBS2], [WBS3]) ON [PRIMARY]
GO
