CREATE TABLE [dbo].[upMaster]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__upMaster___Poste__27FCB152] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__upMaster_Ne__Seq__28F0D58B] DEFAULT ((0)),
[CurrencyExchangeOverrideMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__upMaster___Curre__29E4F9C4] DEFAULT ('N'),
[CurrencyExchangeOverrideDate] [datetime] NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__upMaster___Statu__2AD91DFD] DEFAULT ('N'),
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RejectReason] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[upMaster] ADD CONSTRAINT [upMasterPK] PRIMARY KEY NONCLUSTERED ([Batch], [RefNo]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
