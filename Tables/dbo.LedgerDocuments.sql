CREATE TABLE [dbo].[LedgerDocuments]
(
[Period] [int] NOT NULL CONSTRAINT [DF__LedgerDoc__Perio__7B9353D8] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__LedgerDoc__PostS__7C877811] DEFAULT ((0)),
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL,
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TableName] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrintWithInvoice] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerDoc__Print__7D7B9C4A] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LedgerDocuments] ADD CONSTRAINT [LedgerDocumentsPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [PKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
