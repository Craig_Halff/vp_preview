CREATE TABLE [dbo].[PRConsultant]
(
[ConsultantID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OppConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRConsult__OppCo__58C0AF95] DEFAULT ((0)),
[OppConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRConsult__OppCo__59B4D3CE] DEFAULT ((0)),
[DirectAcctFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRConsult__Direc__5AA8F807] DEFAULT ('N'),
[SeqNo] [smallint] NOT NULL CONSTRAINT [DF__PRConsult__SeqNo__5B9D1C40] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PRConsultant] ADD CONSTRAINT [PRConsultantPK] PRIMARY KEY NONCLUSTERED ([ConsultantID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
