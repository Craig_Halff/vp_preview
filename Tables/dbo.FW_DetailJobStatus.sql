CREATE TABLE [dbo].[FW_DetailJobStatus]
(
[QueueID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProcessID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DetailID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusID] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusDate] [datetime] NULL,
[StatusMessage] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_DetailJobStatus] ADD CONSTRAINT [FW_DetailJobStatusPK] PRIMARY KEY NONCLUSTERED ([QueueID], [ProcessID], [DetailID], [StatusID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
