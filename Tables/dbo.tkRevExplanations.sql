CREATE TABLE [dbo].[tkRevExplanations]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__tkRevExplan__Seq__41327370] DEFAULT ((0)),
[Reason] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tkRevExplanations] ADD CONSTRAINT [tkRevExplanationsPK] PRIMARY KEY NONCLUSTERED ([Company], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
