CREATE TABLE [dbo].[TLSyncFields]
(
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TLSyncFields] ADD CONSTRAINT [TLSyncFieldsPK] PRIMARY KEY NONCLUSTERED ([InfoCenterArea], [Name]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
