CREATE TABLE [dbo].[DashboardLookupOptions]
(
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[XML] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DashboardLookupOptions] ADD CONSTRAINT [DashboardLookupOptionsPK] PRIMARY KEY NONCLUSTERED ([UserName], [PartKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
