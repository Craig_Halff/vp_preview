CREATE TABLE [dbo].[VisionImportCL_ClientTypeImport]
(
[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Client] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFirmType] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Accounting] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Addressee] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AjeraSync] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AlaskaNative] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AnnualRevenue] [decimal] (19, 4) NULL,
[Billing] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLAddressID] [uniqueidentifier] NULL,
[ClientInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Competitor] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentStatus] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustConsolidatedClient] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustConsolidatedClientName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLegacyId] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomCurrencyCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustSource] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisabledVetOwnedSmallBusiness] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisadvBusiness] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EightA] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMail] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employees] [int] NULL,
[ExportInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FAX] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FaxFormat] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GovernmentAgency] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HBCU] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hubzone] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Incumbent] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IQID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkedVendor] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Memo] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MinorityBusiness] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentLevel1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentLevel2] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentLevel3] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentLevel4] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneFormat] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrimaryInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PriorWork] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Recommend] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SmallBusiness] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Specialty] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpecialtyType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxCountryCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxRegistrationNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLInternalKey] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLSyncModDate] [datetime] NULL,
[VendorInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VetOwnedSmallBusiness] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WebSite] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WomanOwned] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIP] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
