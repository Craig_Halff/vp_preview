CREATE TABLE [dbo].[InvoiceApprover]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Approve] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__InvoiceAp__Appro__402A0BF5] DEFAULT ('N'),
[Reject] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__InvoiceAp__Rejec__411E302E] DEFAULT ('N'),
[EmployeeName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[InvoiceApprover] ADD CONSTRAINT [InvoiceApproverPK] PRIMARY KEY CLUSTERED ([WBS1], [Employee]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [InvoiceApproverEmployeeIDX] ON [dbo].[InvoiceApprover] ([Employee]) INCLUDE ([WBS1]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
