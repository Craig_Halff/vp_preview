CREATE TABLE [dbo].[APApprovalWBSTax]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__APApprova__TaxAm__59328385] DEFAULT ((0)),
[CompoundOnTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxAmountProjectFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__APApprova__TaxAm__5A26A7BE] DEFAULT ((0)),
[Seq] [int] NOT NULL CONSTRAINT [DF__APApprovalW__Seq__5B1ACBF7] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[APApprovalWBSTax] ADD CONSTRAINT [APApprovalWBSTaxPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [PKey], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
