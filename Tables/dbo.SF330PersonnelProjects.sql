CREATE TABLE [dbo].[SF330PersonnelProjects]
(
[SF330ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmpID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProjID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PersProjSeq] [smallint] NOT NULL CONSTRAINT [DF__SF330Pers__PersP__60E028F3] DEFAULT ((0)),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearComplProfSvcs] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearComplConst] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DescriptionAndRole] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WithCurrentFirm] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF330PersonnelProjects] ADD CONSTRAINT [SF330PersonnelProjectsPK] PRIMARY KEY NONCLUSTERED ([SF330ID], [EmpID], [ProjID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
