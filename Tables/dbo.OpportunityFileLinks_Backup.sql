CREATE TABLE [dbo].[OpportunityFileLinks_Backup]
(
[LinkID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilePath] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Graphic] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Opportuni__Graph__66ED3CAD] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OpportunityFileLinks_Backup] ADD CONSTRAINT [OpportunityFileLinksPK] PRIMARY KEY NONCLUSTERED ([LinkID], [OpportunityID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
