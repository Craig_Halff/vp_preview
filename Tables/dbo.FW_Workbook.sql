CREATE TABLE [dbo].[FW_Workbook]
(
[WorkbookID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ViewID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WorkbookName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ViewName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContentURL] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_Workbook] ADD CONSTRAINT [FW_WorkbookPK] PRIMARY KEY CLUSTERED ([WorkbookID], [ViewID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
