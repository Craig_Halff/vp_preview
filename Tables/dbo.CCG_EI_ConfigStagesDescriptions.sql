CREATE TABLE [dbo].[CCG_EI_ConfigStagesDescriptions]
(
[Stage] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StageLabel] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StageDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_ConfigStagesDescriptions] ADD CONSTRAINT [PK_CCG_EI_ConfigStagesDescriptions] PRIMARY KEY CLUSTERED ([Stage], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Contains descriptions for Stages as used for multi-language companies', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStagesDescriptions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stage name (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStagesDescriptions', 'COLUMN', N'Stage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*<CCG_EI_ConfigStagesDescriptions / StageDescription Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStagesDescriptions', 'COLUMN', N'StageDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The display label to use for this Stage and for this culture code', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStagesDescriptions', 'COLUMN', N'StageLabel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The UI culture code of this label', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStagesDescriptions', 'COLUMN', N'UICultureName'
GO
