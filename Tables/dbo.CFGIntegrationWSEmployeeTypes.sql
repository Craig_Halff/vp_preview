CREATE TABLE [dbo].[CFGIntegrationWSEmployeeTypes]
(
[ConfigID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGIntegrationWSEmployeeTypes] ADD CONSTRAINT [CFGIntegrationWSEmployeeTypesPK] PRIMARY KEY NONCLUSTERED ([ConfigID], [Type], [EmployeeType]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
