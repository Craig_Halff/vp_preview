CREATE TABLE [dbo].[CCG_EI_Renotify]
(
[ID] [uniqueidentifier] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InvoiceStage] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateChanged] [datetime] NOT NULL,
[ChangedBy] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateLastNotificationSent] [datetime] NULL,
[EmailSubject] [nvarchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailSubjectBatch] [nvarchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCSender] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageDelay] [int] NULL,
[NotifyId] [int] NULL,
[EmployeePICField] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [IDX_CCG_EI_Renotify] ON [dbo].[CCG_EI_Renotify] ([ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Historical records of reoccurring notifications, used by renotification procedure only', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Renotify', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Was the sender CC''ed? (Y / N)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Renotify', 'COLUMN', N'CCSender'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The employee id of the one who last modified the underlying item that triggered the notification', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Renotify', 'COLUMN', N'ChangedBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date/time when the payable item was last modified', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Renotify', 'COLUMN', N'DateChanged'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date when the previous notification was sent', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Renotify', 'COLUMN', N'DateLastNotificationSent'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The notification email body', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Renotify', 'COLUMN', N'EmailMessage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The notification email subject line', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Renotify', 'COLUMN', N'EmailSubject'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The notification email subject line when sent as a batched email', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Renotify', 'COLUMN', N'EmailSubjectBatch'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The employee id of the one receiving the notification', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Renotify', 'COLUMN', N'Employee'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_Renotify / EmployeePICField Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Renotify', 'COLUMN', N'EmployeePICField'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Batch renotify id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Renotify', 'COLUMN', N'ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date/time when this notification record was added', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Renotify', 'COLUMN', N'InsertDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Current stage of the project at the time of the notification', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Renotify', 'COLUMN', N'InvoiceStage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Current delay setting at the time of the message', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Renotify', 'COLUMN', N'MessageDelay'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_Renotify / NotifyId Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Renotify', 'COLUMN', N'NotifyId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The WBS1 number (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Renotify', 'COLUMN', N'WBS1'
GO
