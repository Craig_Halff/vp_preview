CREATE TABLE [dbo].[CustomDashboardTab]
(
[TabID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DashboardID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayOrder] [smallint] NOT NULL CONSTRAINT [DF__CustomDas__Displ__32D010D7] DEFAULT ((0)),
[LicenseRestrictions] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DashboardFilterType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomDashboardTab] ADD CONSTRAINT [CustomDashboardTabPK] PRIMARY KEY CLUSTERED ([TabID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
