CREATE TABLE [dbo].[DashPartsDescriptions]
(
[PartID] [int] NOT NULL CONSTRAINT [DF__DashParts__PartI__19ECF26F] DEFAULT ((0)),
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DashPartsDescriptions] ADD CONSTRAINT [DashPartsDescriptionsPK] PRIMARY KEY CLUSTERED ([PartID], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
