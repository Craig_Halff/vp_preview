CREATE TABLE [dbo].[CCG_EI_ConfigRoles]
(
[Role] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VisionField] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColumnLabel] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColumnOrder] [int] NOT NULL,
[RoleDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MarkupColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HighlightColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HidePdfTools] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowMessaging] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowInputColControls] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowResetStageMenu] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageDelay] [int] NULL,
[EditAllAnnot] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocRights] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_ConfigRoles] ADD CONSTRAINT [PK_CCG_EI_ConfigRoles_Role] PRIMARY KEY CLUSTERED ([Role]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'EI Role configurations and settings', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRoles', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Allow input column controls for this role', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRoles', 'COLUMN', N'AllowInputColControls'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Allow messaging for this role', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRoles', 'COLUMN', N'AllowMessaging'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Enables or disables access to the Reset Stage menu for this role (Y / N)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRoles', 'COLUMN', N'AllowResetStageMenu'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The label to use in the column for this role', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRoles', 'COLUMN', N'ColumnLabel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The placement order of the role column', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRoles', 'COLUMN', N'ColumnOrder'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Role options for the Additional document interface', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRoles', 'COLUMN', N'DocRights'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Allow this role to edit other users'' annotations', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRoles', 'COLUMN', N'EditAllAnnot'
GO
EXEC sp_addextendedproperty N'MS_Description', N'A semicolon separated list of PDF tools to hide for this role', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRoles', 'COLUMN', N'HidePdfTools'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Highlight color for this role when editing PDFs and throughout the application where applicable', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRoles', 'COLUMN', N'HighlightColor'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Markup role for this role when editing PDFs', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRoles', 'COLUMN', N'MarkupColor'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Max time to wait for additional notificaiton/messages to batch into a single email for this role', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRoles', 'COLUMN', N'MessageDelay'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*EI Role Key', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRoles', 'COLUMN', N'Role'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Description for role, used in reporting / informational only', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRoles', 'COLUMN', N'RoleDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Status of the Role type - [A]ctive / [I]nactive', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRoles', 'COLUMN', N'Status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The name of the Vision UDF used to determine who has this role for any given project', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRoles', 'COLUMN', N'VisionField'
GO
