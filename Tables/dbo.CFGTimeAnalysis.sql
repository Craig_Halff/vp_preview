CREATE TABLE [dbo].[CFGTimeAnalysis]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EndWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportColumn] [smallint] NOT NULL CONSTRAINT [DF__CFGTimeAn__Repor__01014914] DEFAULT ((0)),
[AccrualCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGTimeAnalysis] ADD CONSTRAINT [CFGTimeAnalysisPK] PRIMARY KEY NONCLUSTERED ([Company], [StartWBS1], [EndWBS1]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CFGTimeAnalysisAllIDX] ON [dbo].[CFGTimeAnalysis] ([StartWBS1], [EndWBS1], [ReportColumn], [Company]) ON [PRIMARY]
GO
