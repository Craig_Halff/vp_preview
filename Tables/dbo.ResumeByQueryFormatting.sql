CREATE TABLE [dbo].[ResumeByQueryFormatting]
(
[Name] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Format] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ResumeByQueryFormatting] ADD CONSTRAINT [ResumeByQueryFormattingPK] PRIMARY KEY NONCLUSTERED ([Name]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
