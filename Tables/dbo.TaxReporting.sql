CREATE TABLE [dbo].[TaxReporting]
(
[SessionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridNo] [int] NOT NULL CONSTRAINT [DF__TaxReport__GridN__0AD662BF] DEFAULT ((0)),
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientSeq] [int] NOT NULL CONSTRAINT [DF__TaxReport__Clien__0BCA86F8] DEFAULT ((0)),
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__TaxReport__Perio__0CBEAB31] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__TaxReport__PostS__0DB2CF6A] DEFAULT ((0)),
[TaxPointDate] [datetime] NULL,
[Name] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressCode] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxRegistrationNo] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IssuedBy] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BaseAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NetAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__TaxReport__NetAm__0EA6F3A3] DEFAULT ((0)),
[TaxAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__TaxReport__TaxAm__0F9B17DC] DEFAULT ((0)),
[GrossAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__TaxReport__Gross__108F3C15] DEFAULT ((0)),
[InputTax] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__TaxReport__Input__1183604E] DEFAULT ('N'),
[NonRecoverTaxAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__TaxReport__NonRe__12778487] DEFAULT ((0)),
[LastRun] [datetime] NULL,
[ClientNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Diary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaryNo] [int] NOT NULL CONSTRAINT [DF__TaxReport__Diary__136BA8C0] DEFAULT ((0)),
[RecoverTaxAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__TaxReport__Recov__145FCCF9] DEFAULT ((0)),
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TaxReporting] ADD CONSTRAINT [TaxReportingPK] PRIMARY KEY NONCLUSTERED ([SessionID], [PKey], [GridNo], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
