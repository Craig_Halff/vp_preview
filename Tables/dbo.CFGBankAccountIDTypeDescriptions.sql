CREATE TABLE [dbo].[CFGBankAccountIDTypeDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGBankAcco__Seq__3DCA6032] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGBankAccountIDTypeDescriptions] ADD CONSTRAINT [CFGBankAccountIDTypeDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
