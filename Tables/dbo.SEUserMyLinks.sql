CREATE TABLE [dbo].[SEUserMyLinks]
(
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ID] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URLLinkTitle] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SEUserMyLinks] ADD CONSTRAINT [SEUserMyLinksPK] PRIMARY KEY NONCLUSTERED ([PartKey], [ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
