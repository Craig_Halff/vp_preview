CREATE TABLE [dbo].[CFGNavCRMTabsData]
(
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGNavCRMTa__Seq__0B29D1CC] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGNavCRMTabsData] ADD CONSTRAINT [CFGNavCRMTabsDataPK] PRIMARY KEY NONCLUSTERED ([InfoCenterArea], [TabID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
