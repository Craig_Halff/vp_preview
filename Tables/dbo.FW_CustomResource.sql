CREATE TABLE [dbo].[FW_CustomResource]
(
[ResourceName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ResourceFile] [varbinary] (max) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CustomResource] ADD CONSTRAINT [FW_CustomResourcePK] PRIMARY KEY NONCLUSTERED ([ResourceName], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
