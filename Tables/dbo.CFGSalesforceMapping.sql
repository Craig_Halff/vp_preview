CREATE TABLE [dbo].[CFGSalesforceMapping]
(
[SFTable] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SFColumn] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ToColumn] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SFColumnDesc] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGSalesfor__Seq__2A3ACAD6] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGSalesforceMapping] ADD CONSTRAINT [CFGSalesforceMappingPK] PRIMARY KEY CLUSTERED ([SFTable], [SFColumn], [ToColumn]) ON [PRIMARY]
GO
