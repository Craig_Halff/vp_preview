CREATE TABLE [dbo].[FW_CFGUserSettingsData]
(
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ElementID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ElementType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Locked] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Required] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hidden] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DesignerCreated] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGUse__Desig__0DC712AC] DEFAULT ('N'),
[ButtonType] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GenericPropValue] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecordLimit] [int] NOT NULL CONSTRAINT [DF__FW_CFGUse__Recor__0EBB36E5] DEFAULT ((0)),
[ColumnName] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TableName] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_FW_CFGUserSettingsData]
      ON [dbo].[FW_CFGUserSettingsData]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'FW_CFGUserSettingsData'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[TabID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ElementID],121),'Locked',CONVERT(NVARCHAR(2000),[Locked],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[TabID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ElementID],121),'Required',CONVERT(NVARCHAR(2000),[Required],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[TabID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ElementID],121),'Hidden',CONVERT(NVARCHAR(2000),[Hidden],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_FW_CFGUserSettingsData]
      ON [dbo].[FW_CFGUserSettingsData]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'FW_CFGUserSettingsData'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[TabID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ElementID],121),'Locked',NULL,CONVERT(NVARCHAR(2000),[Locked],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[TabID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ElementID],121),'Required',NULL,CONVERT(NVARCHAR(2000),[Required],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[TabID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ElementID],121),'Hidden',NULL,CONVERT(NVARCHAR(2000),[Hidden],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_FW_CFGUserSettingsData]
      ON [dbo].[FW_CFGUserSettingsData]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'FW_CFGUserSettingsData'
    
      If UPDATE([Locked])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[TabID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ElementID],121),'Locked',
      CONVERT(NVARCHAR(2000),DELETED.[Locked],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Locked],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[InfoCenterArea] = DELETED.[InfoCenterArea] AND INSERTED.[GridID] = DELETED.[GridID] AND INSERTED.[ElementID] = DELETED.[ElementID] AND 
		(
			(
				INSERTED.[Locked] Is Null And
				DELETED.[Locked] Is Not Null
			) Or
			(
				INSERTED.[Locked] Is Not Null And
				DELETED.[Locked] Is Null
			) Or
			(
				INSERTED.[Locked] !=
				DELETED.[Locked]
			)
		) 
		END		
		
      If UPDATE([Required])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[TabID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ElementID],121),'Required',
      CONVERT(NVARCHAR(2000),DELETED.[Required],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Required],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[InfoCenterArea] = DELETED.[InfoCenterArea] AND INSERTED.[GridID] = DELETED.[GridID] AND INSERTED.[ElementID] = DELETED.[ElementID] AND 
		(
			(
				INSERTED.[Required] Is Null And
				DELETED.[Required] Is Not Null
			) Or
			(
				INSERTED.[Required] Is Not Null And
				DELETED.[Required] Is Null
			) Or
			(
				INSERTED.[Required] !=
				DELETED.[Required]
			)
		) 
		END		
		
      If UPDATE([Hidden])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[TabID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ElementID],121),'Hidden',
      CONVERT(NVARCHAR(2000),DELETED.[Hidden],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Hidden],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[InfoCenterArea] = DELETED.[InfoCenterArea] AND INSERTED.[GridID] = DELETED.[GridID] AND INSERTED.[ElementID] = DELETED.[ElementID] AND 
		(
			(
				INSERTED.[Hidden] Is Null And
				DELETED.[Hidden] Is Not Null
			) Or
			(
				INSERTED.[Hidden] Is Not Null And
				DELETED.[Hidden] Is Null
			) Or
			(
				INSERTED.[Hidden] !=
				DELETED.[Hidden]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[FW_CFGUserSettingsData] ADD CONSTRAINT [FW_CFGUserSettingsDataPK] PRIMARY KEY NONCLUSTERED ([InfoCenterArea], [TabID], [GridID], [ElementID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
