CREATE TABLE [dbo].[ICBillTaxDetail]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__ICBillTax__Perio__0D5CFE58] DEFAULT ((0)),
[RunSeq] [int] NOT NULL CONSTRAINT [DF__ICBillTax__RunSe__0E512291] DEFAULT ((0)),
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecdSeq] [int] NOT NULL CONSTRAINT [DF__ICBillTax__RecdS__0F4546CA] DEFAULT ((0)),
[BillWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Computation] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ICBillTax__Amoun__10396B03] DEFAULT ((0)),
[Basis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ICBillTax__Basis__112D8F3C] DEFAULT ((0)),
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ICBillTaxD__Rate__1221B375] DEFAULT ((0)),
[ICBillInvoiceCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ICBillTaxDetail] ADD CONSTRAINT [ICBillTaxDetailPK] PRIMARY KEY NONCLUSTERED ([Company], [Period], [RunSeq], [Invoice], [MainWBS1], [RecdSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
