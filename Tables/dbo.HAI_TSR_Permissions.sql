CREATE TABLE [dbo].[HAI_TSR_Permissions]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[Employee] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Team] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Practice] [nvarchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Office] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HAI_TSR_Permissions] ADD CONSTRAINT [PK_HAI_TSR_Permissions] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
