CREATE TABLE [dbo].[exChecksB]
(
[Period] [int] NOT NULL CONSTRAINT [DF__exChecksB__Perio__2FC724F5] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__exChecksB__PostS__30BB492E] DEFAULT ((0)),
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__exChecksB_N__Seq__31AF6D67] DEFAULT ((0)),
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckNo] [bigint] NOT NULL CONSTRAINT [DF__exChecksB__Check__32A391A0] DEFAULT ((0)),
[CheckDate] [datetime] NULL,
[ReportName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportDate] [datetime] NULL,
[AppliedAdvance] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__exChecksB__Appli__3397B5D9] DEFAULT ((0)),
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__exChecksB__Amoun__348BDA12] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[exChecksB] ADD CONSTRAINT [exChecksBPK] PRIMARY KEY NONCLUSTERED ([Period], [PostSeq], [Employee], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
