CREATE TABLE [dbo].[BulkUpdate]
(
[BulkUpdateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdatedColumn] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateMethod] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValueDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdatingColumn] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SelectionXML] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WhereClause] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastRunDate] [datetime] NULL,
[SelectRecords] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InfoCenterLabel] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBSLevel] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[SearchPkey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BulkUpdate] ADD CONSTRAINT [BulkUpdatePK] PRIMARY KEY CLUSTERED ([BulkUpdateID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
