CREATE TABLE [dbo].[CFGBankImport]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Delimiter] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Qualifier] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DecimalSymbol] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateOrder] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateDelimiter] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadingZeros] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGBankIm__Leadi__456B81FA] DEFAULT ('N'),
[FourDigit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGBankIm__FourD__465FA633] DEFAULT ('N'),
[AutoClearTrans] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGBankIm__AutoC__4753CA6C] DEFAULT ('N'),
[ReceiptPaymentDef] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentDays] [int] NOT NULL CONSTRAINT [DF__CFGBankIm__Payme__4847EEA5] DEFAULT ((0)),
[PaymentAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGBankIm__Payme__493C12DE] DEFAULT ((0)),
[ReceiptDays] [int] NOT NULL CONSTRAINT [DF__CFGBankIm__Recei__4A303717] DEFAULT ((0)),
[ReceiptAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGBankIm__Recei__4B245B50] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGBankImport] ADD CONSTRAINT [CFGBankImportPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
