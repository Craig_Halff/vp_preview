CREATE TABLE [dbo].[PYChecksC]
(
[Period] [int] NOT NULL CONSTRAINT [DF__PYChecksC__Perio__141FD5D7] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__PYChecksC__PostS__1513FA10] DEFAULT ((0)),
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Method] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksC__AmtPc__16081E49] DEFAULT ((0)),
[Suppress] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksC__Suppr__16FC4282] DEFAULT ('A'),
[Limit] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksC__Limit__17F066BB] DEFAULT ((0)),
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksC__Amoun__18E48AF4] DEFAULT ((0)),
[Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksC__Overr__19D8AF2D] DEFAULT ('N'),
[QTDAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksC__QTDAm__1ACCD366] DEFAULT ((0)),
[YTDAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksC__YTDAm__1BC0F79F] DEFAULT ((0)),
[AdjustedGrossPayBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksC__Adjus__1CB51BD8] DEFAULT ((0)),
[TaxablePayBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksC__Taxab__1DA94011] DEFAULT ((0)),
[Amt401K] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksC__Amt40__1E9D644A] DEFAULT ((0)),
[Amt125] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksC__Amt12__1F918883] DEFAULT ((0)),
[Exclude401k] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksC__Exclu__2085ACBC] DEFAULT ('N'),
[ExcludeCafeteria] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksC__Exclu__2179D0F5] DEFAULT ('N'),
[ExcludeOtherPay1] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksC__Exclu__226DF52E] DEFAULT ('N'),
[ExcludeOtherPay2] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksC__Exclu__23621967] DEFAULT ('N'),
[ExcludeOtherPay3] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksC__Exclu__24563DA0] DEFAULT ('N'),
[ExcludeOtherPay4] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksC__Exclu__254A61D9] DEFAULT ('N'),
[ExcludeOtherPay5] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksC__Exclu__263E8612] DEFAULT ('N'),
[OtherPay1] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksC__Other__2732AA4B] DEFAULT ((0)),
[OtherPay2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksC__Other__2826CE84] DEFAULT ((0)),
[OtherPay3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksC__Other__291AF2BD] DEFAULT ((0)),
[OtherPay4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksC__Other__2A0F16F6] DEFAULT ((0)),
[OtherPay5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksC__Other__2B033B2F] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PYChecksC] ADD CONSTRAINT [PYChecksCPK] PRIMARY KEY NONCLUSTERED ([Period], [PostSeq], [Employee], [Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
