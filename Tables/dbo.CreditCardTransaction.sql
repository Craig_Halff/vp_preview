CREATE TABLE [dbo].[CreditCardTransaction]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrimaryCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatementDate] [datetime] NULL,
[SecondaryCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[CloseBalance] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CreditCar__Close__77CCE495] DEFAULT ((0)),
[StatementNumber] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Memo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cleared] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CreditCar__Clear__78C108CE] DEFAULT ('N'),
[Closed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CreditCar__Close__79B52D07] DEFAULT ('N'),
[ImportPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SecondaryAccount] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CardholderName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransDate] [datetime] NULL,
[MerchantDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CreditCar__Amoun__7AA95140] DEFAULT ((0)),
[TransactionID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CreditCar__Payme__7B9D7579] DEFAULT ((0)),
[UserDefined1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserDefined2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserDefined3] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserDefined4] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserDefined5] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CreditCar__Amoun__7C9199B2] DEFAULT ((0)),
[PersonalCharge] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CreditCar__Perso__7D85BDEB] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CreditCardTransaction] ADD CONSTRAINT [CreditCardTransactionPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
