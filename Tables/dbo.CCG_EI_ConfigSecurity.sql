CREATE TABLE [dbo].[CCG_EI_ConfigSecurity]
(
[SecurityId] [int] NOT NULL IDENTITY(1, 1),
[Role] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PctComplFullRights] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PctComplChangeFeeMethod] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PctComplChangeFeeBasis] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PctComplChangeFee] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PctComplChangeConstrAmt] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PctComplChangeConstrFeePct] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PctComplChangeUnitQty] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PctComplChangeUnitPrice] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PctComplChangeUnitLabel] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_ConfigSecurity] ADD CONSTRAINT [PK_CCG_EI_ConfigSecurity] PRIMARY KEY CLUSTERED ([SecurityId]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_CCG_EI_ConfigSecurity_Role] ON [dbo].[CCG_EI_ConfigSecurity] ([Role]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Vision Role security for the Percent Complete Grid', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigSecurity', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Can this role modify the Constraint Amount values? (Y / N)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigSecurity', 'COLUMN', N'PctComplChangeConstrAmt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Can this role modify the Constraint Fee Pct values? (Y / N)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigSecurity', 'COLUMN', N'PctComplChangeConstrFeePct'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Can this role modify the Fee values? (Y / N)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigSecurity', 'COLUMN', N'PctComplChangeFee'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Can this role modify the Fee Basis values? (Y / N)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigSecurity', 'COLUMN', N'PctComplChangeFeeBasis'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Can this role modify the Fee Method values? (Y / N)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigSecurity', 'COLUMN', N'PctComplChangeFeeMethod'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Can this role modify the Unit Label values? (Y / N)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigSecurity', 'COLUMN', N'PctComplChangeUnitLabel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Can this role modify the Unit Price values? (Y / N)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigSecurity', 'COLUMN', N'PctComplChangeUnitPrice'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Can this role modify the Unit Qty values? (Y / N)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigSecurity', 'COLUMN', N'PctComplChangeUnitQty'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Does this Vision Role have full rights? (Y / N)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigSecurity', 'COLUMN', N'PctComplFullRights'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Vision role name key (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigSecurity', 'COLUMN', N'Role'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigSecurity', 'COLUMN', N'SecurityId'
GO
