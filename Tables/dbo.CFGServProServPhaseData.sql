CREATE TABLE [dbo].[CFGServProServPhaseData]
(
[ServProCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StdPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGServPr__StdPc__6C062C2E] DEFAULT ((0)),
[Disabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGServPr__Disab__6CFA5067] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGServProServPhaseData] ADD CONSTRAINT [CFGServProServPhaseDataPK] PRIMARY KEY CLUSTERED ([ServProCode], [Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
