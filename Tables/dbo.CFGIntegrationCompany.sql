CREATE TABLE [dbo].[CFGIntegrationCompany]
(
[Type] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGIntegrationCompany] ADD CONSTRAINT [CFGIntegrationCompanyPK] PRIMARY KEY NONCLUSTERED ([Type], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
