CREATE TABLE [dbo].[FW_CustomNavTreeData]
(
[ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__FW_CustomNa__Seq__23B653CB] DEFAULT ((0)),
[NavLevel] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Page] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Args] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentNode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreviousSibling] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomNode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_Custom__Custo__24AA7804] DEFAULT ('N'),
[FileName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Folder] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Users] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OptionOpen] [smallint] NOT NULL CONSTRAINT [DF__FW_Custom__Optio__259E9C3D] DEFAULT ((0)),
[ReportFavoriteName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RebuildReport] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_Custom__Rebui__2692C076] DEFAULT ('N'),
[HelpPage] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Icon] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CustomNavTreeData] ADD CONSTRAINT [FW_CustomNavTreeDataPK] PRIMARY KEY CLUSTERED ([ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
