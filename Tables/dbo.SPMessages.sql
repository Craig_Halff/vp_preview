CREATE TABLE [dbo].[SPMessages]
(
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MsgID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StoredProcName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageText] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SPMessages] ADD CONSTRAINT [SPMessagesPK] PRIMARY KEY NONCLUSTERED ([UICultureName], [MsgID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
