CREATE TABLE [dbo].[FW_CFGPhoneFormatData]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ISOCountryCode] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PhoneFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGPhoneFormatData] ADD CONSTRAINT [FW_CFGPhoneFormatDataPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
