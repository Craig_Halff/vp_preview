CREATE TABLE [dbo].[PNPlannedConsultant]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConsultantID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodCount] [int] NOT NULL CONSTRAINT [DF__PNPlanned__Perio__190EA826] DEFAULT ((1)),
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlanned__Perio__1A02CC5F] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlanned__Perio__1AF6F098] DEFAULT ((0)),
[PeriodRev] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlanned__Perio__1BEB14D1] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PNPlannedConsultant] ADD CONSTRAINT [PNPlannedConsultantPK] PRIMARY KEY NONCLUSTERED ([TimePhaseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNPlannedConsultantPTCIDX] ON [dbo].[PNPlannedConsultant] ([PlanID], [TaskID], [ConsultantID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNPlannedConsultantAllIDX] ON [dbo].[PNPlannedConsultant] ([PlanID], [TimePhaseID], [TaskID], [ConsultantID], [StartDate], [EndDate], [PeriodCost], [PeriodBill], [PeriodRev]) ON [PRIMARY]
GO
