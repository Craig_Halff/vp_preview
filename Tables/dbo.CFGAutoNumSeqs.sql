CREATE TABLE [dbo].[CFGAutoNumSeqs]
(
[NavID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChargeType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAutoNu__Charg__2E881CA2] DEFAULT (' '),
[ResetPart] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastAutoSeq] [int] NOT NULL CONSTRAINT [DF__CFGAutoNu__LastA__2F7C40DB] DEFAULT ((-1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAutoNumSeqs] ADD CONSTRAINT [CFGAutoNumSeqsPK] PRIMARY KEY NONCLUSTERED ([NavID], [ChargeType], [ResetPart]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
