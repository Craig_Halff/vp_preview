CREATE TABLE [dbo].[MktCampaignLeads]
(
[CampaignID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LeadID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_MktCampaignLeads]
      ON [dbo].[MktCampaignLeads]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'MktCampaignLeads'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[LeadID],121),'CampaignID',CONVERT(NVARCHAR(2000),DELETED.[CampaignID],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join MktCampaign as oldDesc  on DELETED.CampaignID = oldDesc.CampaignID

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[LeadID],121),'LeadID',CONVERT(NVARCHAR(2000),DELETED.[LeadID],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join Contacts as oldDesc  on DELETED.LeadID = oldDesc.ContactID

      
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_MktCampaignLeads] ON [dbo].[MktCampaignLeads]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_MktCampaignLeads]
      ON [dbo].[MktCampaignLeads]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'MktCampaignLeads'
    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[LeadID],121),'CampaignID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CampaignID],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  MktCampaign as newDesc  on INSERTED.CampaignID = newDesc.CampaignID

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[LeadID],121),'LeadID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[LeadID],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  Contacts as newDesc  on INSERTED.LeadID = newDesc.ContactID

     
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_MktCampaignLeads] ON [dbo].[MktCampaignLeads]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_MktCampaignLeads]
      ON [dbo].[MktCampaignLeads]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'MktCampaignLeads'
    
     If UPDATE([CampaignID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[LeadID],121),'CampaignID',
     CONVERT(NVARCHAR(2000),DELETED.[CampaignID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CampaignID],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND INSERTED.[LeadID] = DELETED.[LeadID] AND 
		(
			(
				INSERTED.[CampaignID] Is Null And
				DELETED.[CampaignID] Is Not Null
			) Or
			(
				INSERTED.[CampaignID] Is Not Null And
				DELETED.[CampaignID] Is Null
			) Or
			(
				INSERTED.[CampaignID] !=
				DELETED.[CampaignID]
			)
		) left join MktCampaign as oldDesc  on DELETED.CampaignID = oldDesc.CampaignID  left join  MktCampaign as newDesc  on INSERTED.CampaignID = newDesc.CampaignID
		END		
		
     If UPDATE([LeadID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[LeadID],121),'LeadID',
     CONVERT(NVARCHAR(2000),DELETED.[LeadID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[LeadID],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND INSERTED.[LeadID] = DELETED.[LeadID] AND 
		(
			(
				INSERTED.[LeadID] Is Null And
				DELETED.[LeadID] Is Not Null
			) Or
			(
				INSERTED.[LeadID] Is Not Null And
				DELETED.[LeadID] Is Null
			) Or
			(
				INSERTED.[LeadID] !=
				DELETED.[LeadID]
			)
		) left join Contacts as oldDesc  on DELETED.LeadID = oldDesc.ContactID  left join  Contacts as newDesc  on INSERTED.LeadID = newDesc.ContactID
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_MktCampaignLeads] ON [dbo].[MktCampaignLeads]
GO
ALTER TABLE [dbo].[MktCampaignLeads] ADD CONSTRAINT [MktCampaignLeadsPK] PRIMARY KEY NONCLUSTERED ([CampaignID], [LeadID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
