CREATE TABLE [dbo].[RPBaselineRevenue]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AssignmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodCount] [int] NOT NULL CONSTRAINT [DF__RPBaselin__Perio__33637706] DEFAULT ((1)),
[PeriodAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPBaselin__Perio__34579B3F] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPBaselineRevenue] ADD CONSTRAINT [RPBaselineRevenuePK] PRIMARY KEY NONCLUSTERED ([TimePhaseID], [TaskID], [PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
