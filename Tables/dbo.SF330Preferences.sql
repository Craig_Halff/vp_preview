CREATE TABLE [dbo].[SF330Preferences]
(
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NameFormat] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PointOfContactRole] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmpProjDescription] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmpProjRoleFormat] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefProjOwner] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefProjContact] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefProjDescription] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultFont] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultFontSize] [smallint] NOT NULL CONSTRAINT [DF__SF330Pref__Defau__64B0B9D7] DEFAULT ((0)),
[DefaultTemplateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DegreeSeparator] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegSeparator] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpaceAfterSep] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpaceBeforeSep] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InclSignatureImg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InclInstitution] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InclYearEarned] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InclYearRegEarned] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InclRegNumber] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OtherProfQual] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InclAllProjectAssociations] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF330Preferences] ADD CONSTRAINT [SF330PreferencesPK] PRIMARY KEY CLUSTERED ([UserName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
