CREATE TABLE [dbo].[FW_Attachments]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Key1] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Key2] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Key3] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CategoryCode] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileID] [uniqueidentifier] NOT NULL,
[Application] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[FileDescription] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_FW_Attachments]
      ON [dbo].[FW_Attachments]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'FW_Attachments'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FileID],121),'PKey',CONVERT(NVARCHAR(2000),[PKey],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FileID],121),'Key1',CONVERT(NVARCHAR(2000),[Key1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FileID],121),'Key2',CONVERT(NVARCHAR(2000),[Key2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FileID],121),'Key3',CONVERT(NVARCHAR(2000),[Key3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FileID],121),'CategoryCode',CONVERT(NVARCHAR(2000),[CategoryCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FileID],121),'FileID',CONVERT(NVARCHAR(2000),[FileID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FileID],121),'Application',CONVERT(NVARCHAR(2000),[Application],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FileID],121),'FileDescription',CONVERT(NVARCHAR(2000),[FileDescription],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_FW_Attachments]
      ON [dbo].[FW_Attachments]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'FW_Attachments'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'PKey',NULL,CONVERT(NVARCHAR(2000),[PKey],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'Key1',NULL,CONVERT(NVARCHAR(2000),[Key1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'Key2',NULL,CONVERT(NVARCHAR(2000),[Key2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'Key3',NULL,CONVERT(NVARCHAR(2000),[Key3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'CategoryCode',NULL,CONVERT(NVARCHAR(2000),[CategoryCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'FileID',NULL,CONVERT(NVARCHAR(2000),[FileID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'Application',NULL,CONVERT(NVARCHAR(2000),[Application],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'FileDescription',NULL,CONVERT(NVARCHAR(2000),[FileDescription],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_FW_Attachments]
      ON [dbo].[FW_Attachments]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'FW_Attachments'
    
      If UPDATE([PKey])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'PKey',
      CONVERT(NVARCHAR(2000),DELETED.[PKey],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PKey],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND INSERTED.[FileID] = DELETED.[FileID] AND 
		(
			(
				INSERTED.[PKey] Is Null And
				DELETED.[PKey] Is Not Null
			) Or
			(
				INSERTED.[PKey] Is Not Null And
				DELETED.[PKey] Is Null
			) Or
			(
				INSERTED.[PKey] !=
				DELETED.[PKey]
			)
		) 
		END		
		
      If UPDATE([Key1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'Key1',
      CONVERT(NVARCHAR(2000),DELETED.[Key1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Key1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND INSERTED.[FileID] = DELETED.[FileID] AND 
		(
			(
				INSERTED.[Key1] Is Null And
				DELETED.[Key1] Is Not Null
			) Or
			(
				INSERTED.[Key1] Is Not Null And
				DELETED.[Key1] Is Null
			) Or
			(
				INSERTED.[Key1] !=
				DELETED.[Key1]
			)
		) 
		END		
		
      If UPDATE([Key2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'Key2',
      CONVERT(NVARCHAR(2000),DELETED.[Key2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Key2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND INSERTED.[FileID] = DELETED.[FileID] AND 
		(
			(
				INSERTED.[Key2] Is Null And
				DELETED.[Key2] Is Not Null
			) Or
			(
				INSERTED.[Key2] Is Not Null And
				DELETED.[Key2] Is Null
			) Or
			(
				INSERTED.[Key2] !=
				DELETED.[Key2]
			)
		) 
		END		
		
      If UPDATE([Key3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'Key3',
      CONVERT(NVARCHAR(2000),DELETED.[Key3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Key3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND INSERTED.[FileID] = DELETED.[FileID] AND 
		(
			(
				INSERTED.[Key3] Is Null And
				DELETED.[Key3] Is Not Null
			) Or
			(
				INSERTED.[Key3] Is Not Null And
				DELETED.[Key3] Is Null
			) Or
			(
				INSERTED.[Key3] !=
				DELETED.[Key3]
			)
		) 
		END		
		
      If UPDATE([CategoryCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'CategoryCode',
      CONVERT(NVARCHAR(2000),DELETED.[CategoryCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CategoryCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND INSERTED.[FileID] = DELETED.[FileID] AND 
		(
			(
				INSERTED.[CategoryCode] Is Null And
				DELETED.[CategoryCode] Is Not Null
			) Or
			(
				INSERTED.[CategoryCode] Is Not Null And
				DELETED.[CategoryCode] Is Null
			) Or
			(
				INSERTED.[CategoryCode] !=
				DELETED.[CategoryCode]
			)
		) 
		END		
		
      If UPDATE([FileID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'FileID',
      CONVERT(NVARCHAR(2000),DELETED.[FileID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FileID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND INSERTED.[FileID] = DELETED.[FileID] AND 
		(
			(
				INSERTED.[FileID] Is Null And
				DELETED.[FileID] Is Not Null
			) Or
			(
				INSERTED.[FileID] Is Not Null And
				DELETED.[FileID] Is Null
			) Or
			(
				INSERTED.[FileID] !=
				DELETED.[FileID]
			)
		) 
		END		
		
      If UPDATE([Application])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'Application',
      CONVERT(NVARCHAR(2000),DELETED.[Application],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Application],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND INSERTED.[FileID] = DELETED.[FileID] AND 
		(
			(
				INSERTED.[Application] Is Null And
				DELETED.[Application] Is Not Null
			) Or
			(
				INSERTED.[Application] Is Not Null And
				DELETED.[Application] Is Null
			) Or
			(
				INSERTED.[Application] !=
				DELETED.[Application]
			)
		) 
		END		
		
      If UPDATE([FileDescription])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'FileDescription',
      CONVERT(NVARCHAR(2000),DELETED.[FileDescription],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FileDescription],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND INSERTED.[FileID] = DELETED.[FileID] AND 
		(
			(
				INSERTED.[FileDescription] Is Null And
				DELETED.[FileDescription] Is Not Null
			) Or
			(
				INSERTED.[FileDescription] Is Not Null And
				DELETED.[FileDescription] Is Null
			) Or
			(
				INSERTED.[FileDescription] !=
				DELETED.[FileDescription]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[FW_Attachments] ADD CONSTRAINT [FW_AttachmentsPK] PRIMARY KEY NONCLUSTERED ([PKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
