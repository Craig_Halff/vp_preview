CREATE TABLE [dbo].[CCG_EI_ConfigMessages]
(
[Message] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stored Predified Message Text for the Messaging interface', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigMessages', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Message Text', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigMessages', 'COLUMN', N'Message'
GO
