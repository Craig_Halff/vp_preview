CREATE TABLE [dbo].[RPConsultant]
(
[ConsultantID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcctName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BaselineConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__Basel__4D234909] DEFAULT ((0)),
[BaselineConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__Basel__4E176D42] DEFAULT ((0)),
[PlannedConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__Plann__4F0B917B] DEFAULT ((0)),
[PlannedConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__Plann__4FFFB5B4] DEFAULT ((0)),
[PlannedDirConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__Plann__50F3D9ED] DEFAULT ((0)),
[PlannedDirConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__Plann__51E7FE26] DEFAULT ((0)),
[PctCompleteConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__PctCo__52DC225F] DEFAULT ((0)),
[PctCompleteConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__PctCo__53D04698] DEFAULT ((0)),
[WeightedConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__Weigh__54C46AD1] DEFAULT ((0)),
[WeightedConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__Weigh__55B88F0A] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[DirectAcctFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPConsult__Direc__56ACB343] DEFAULT ('N'),
[ConBillRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__ConBi__57A0D77C] DEFAULT ((0)),
[ConRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__ConRe__5894FBB5] DEFAULT ((0)),
[SortSeq] [smallint] NOT NULL CONSTRAINT [DF__RPConsult__SortS__59891FEE] DEFAULT ((0)),
[LabParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPConsult__LabPa__5A7D4427] DEFAULT ('N'),
[ExpParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPConsult__ExpPa__5B716860] DEFAULT ('N'),
[ConParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPConsult__ConPa__5C658C99] DEFAULT ('N'),
[UntParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPConsult__UntPa__5D59B0D2] DEFAULT ('N'),
[BaselineStart] [datetime] NULL,
[BaselineFinish] [datetime] NULL,
[ConVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPConsult__ConVS__5E4DD50B] DEFAULT ('Y'),
[ExpVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPConsult__ExpVS__5F41F944] DEFAULT ('N'),
[LabVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPConsult__LabVS__60361D7D] DEFAULT ('N'),
[UntVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPConsult__UntVS__612A41B6] DEFAULT ('N'),
[BaselineDirConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__Basel__621E65EF] DEFAULT ((0)),
[BaselineDirConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__Basel__63128A28] DEFAULT ((0)),
[JTDConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__JTDCo__6406AE61] DEFAULT ((0)),
[JTDConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__JTDCo__64FAD29A] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NOT NULL CONSTRAINT [DF__RPConsult__Creat__65EEF6D3] DEFAULT (getutcdate()),
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NOT NULL CONSTRAINT [DF__RPConsult__ModDa__66E31B0C] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPConsultant] ADD CONSTRAINT [RPConsultantPK] PRIMARY KEY NONCLUSTERED ([ConsultantID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPConsultantWBS1WBS2WBS3AccountVendorIDX] ON [dbo].[RPConsultant] ([ConsultantID], [PlanID], [TaskID], [WBS1], [WBS2], [WBS3], [Account], [Vendor]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [RPConsultantPlanIDIDX] ON [dbo].[RPConsultant] ([PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPConsultantTaskIDIDX] ON [dbo].[RPConsultant] ([TaskID]) ON [PRIMARY]
GO
