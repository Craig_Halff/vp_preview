CREATE TABLE [dbo].[PRCompetitionAssoc]
(
[RecordID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Strengths] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Weakness] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Incumbent] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRCompeti__Incum__07B0A8A8] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PRCompetitionAssoc] ADD CONSTRAINT [PRCompetitionAssocPK] PRIMARY KEY CLUSTERED ([RecordID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
