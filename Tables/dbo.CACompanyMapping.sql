CREATE TABLE [dbo].[CACompanyMapping]
(
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_CACompanyMapping]
      ON [dbo].[CACompanyMapping]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CACompanyMapping'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'Account',CONVERT(NVARCHAR(2000),[Account],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'Company',CONVERT(NVARCHAR(2000),[Company],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_CACompanyMapping] ON [dbo].[CACompanyMapping]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_CACompanyMapping]
      ON [dbo].[CACompanyMapping]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CACompanyMapping'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Account',NULL,CONVERT(NVARCHAR(2000),[Account],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Company',NULL,CONVERT(NVARCHAR(2000),[Company],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_CACompanyMapping] ON [dbo].[CACompanyMapping]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_CACompanyMapping]
      ON [dbo].[CACompanyMapping]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CACompanyMapping'
    
      If UPDATE([Account])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Account',
      CONVERT(NVARCHAR(2000),DELETED.[Account],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Account],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[Account] Is Null And
				DELETED.[Account] Is Not Null
			) Or
			(
				INSERTED.[Account] Is Not Null And
				DELETED.[Account] Is Null
			) Or
			(
				INSERTED.[Account] !=
				DELETED.[Account]
			)
		) 
		END		
		
      If UPDATE([Company])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Company',
      CONVERT(NVARCHAR(2000),DELETED.[Company],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Company],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[Company] Is Null And
				DELETED.[Company] Is Not Null
			) Or
			(
				INSERTED.[Company] Is Not Null And
				DELETED.[Company] Is Null
			) Or
			(
				INSERTED.[Company] !=
				DELETED.[Company]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_CACompanyMapping] ON [dbo].[CACompanyMapping]
GO
ALTER TABLE [dbo].[CACompanyMapping] ADD CONSTRAINT [CACompanyMappingPK] PRIMARY KEY CLUSTERED ([Account], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
