CREATE TABLE [dbo].[X_INTFC_ELEM_INST]
(
[INTFC_ID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[INST_ID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OBJ_ID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ELEM_ID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EXCL_ON_INS_FL] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EXCL_ON_UPD_FL] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ALLOW_EXCL_INS_FL] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ALLOW_EXCL_UPD_FL] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DFLT_VALUE] [varchar] (254) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ELEM_LENGTH] [smallint] NOT NULL,
[ELEM_decimalS] [smallint] NOT NULL,
[ALLOW_DFLT_FL] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TIME_STAMP] [datetime] NOT NULL,
[MODIFIED_BY] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ROWVERSION] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[X_INTFC_ELEM_INST] ADD CONSTRAINT [X_INTFC_ELEM_INSTPK] PRIMARY KEY NONCLUSTERED ([INTFC_ID], [INST_ID], [OBJ_ID], [ELEM_ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
