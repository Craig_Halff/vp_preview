CREATE TABLE [dbo].[cdControlDefaultTaxCodes]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__cdControlDe__Seq__4E35D225] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cdControlDefaultTaxCodes] ADD CONSTRAINT [cdControlDefaultTaxCodesPK] PRIMARY KEY NONCLUSTERED ([Batch], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
