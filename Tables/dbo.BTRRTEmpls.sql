CREATE TABLE [dbo].[BTRRTEmpls]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__BTRRTEmpl__Table__1220E81E] DEFAULT ((0)),
[RateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BTRRTEmpls__Rate__13150C57] DEFAULT ((0)),
[EffectiveDate] [datetime] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTRRTEmpls] ADD CONSTRAINT [BTRRTEmplsPK] PRIMARY KEY NONCLUSTERED ([TableNo], [RateID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [BTRRTEmplsEmployeeIDX] ON [dbo].[BTRRTEmpls] ([TableNo], [Employee]) ON [PRIMARY]
GO
