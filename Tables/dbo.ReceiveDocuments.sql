CREATE TABLE [dbo].[ReceiveDocuments]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL,
[AssociateNew] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ReceiveDo__Assoc__3DFED4FE] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__ReceiveDocu__Seq__3EF2F937] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReceiveDocuments] ADD CONSTRAINT [ReceiveDocumentsPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [DetailPKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
