CREATE TABLE [dbo].[VisionImportCLAddress_CLAddress]
(
[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Accounting] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Addressee] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Billing] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLAddressID] [uniqueidentifier] NULL,
[Country] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMail] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FAX] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FaxFormat] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneFormat] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrimaryInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxCountryCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxRegistrationNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIP] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
