CREATE TABLE [dbo].[HAI_CrewEntryAppPermissions]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[Employee] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Org] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Role] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HAI_CrewEntryAppPermissions] ADD CONSTRAINT [PK_HAI_CrewEntryAppPermissions] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
