CREATE TABLE [dbo].[OpportunityCompetitionAssoc_Backup]
(
[RecordID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Strengths] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Weakness] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Incumbent] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Opportuni__Incum__02CA614C] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OpportunityCompetitionAssoc_Backup] ADD CONSTRAINT [OpportunityCompetitionAssocPK] PRIMARY KEY NONCLUSTERED ([RecordID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [OpportunityCompetitionAssocOppIDClIDIDX] ON [dbo].[OpportunityCompetitionAssoc_Backup] ([OpportunityID], [ClientID]) ON [PRIMARY]
GO
