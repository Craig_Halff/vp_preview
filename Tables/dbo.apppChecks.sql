CREATE TABLE [dbo].[apppChecks]
(
[Period] [int] NOT NULL CONSTRAINT [DF__apppCheck__Perio__060529FC] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__apppCheck__PostS__06F94E35] DEFAULT ((0)),
[Seq] [int] NOT NULL CONSTRAINT [DF__apppChecks___Seq__07ED726E] DEFAULT ((0)),
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Voucher] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL,
[LiabCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiscCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Line] [smallint] NOT NULL CONSTRAINT [DF__apppChecks__Line__08E196A7] DEFAULT ((0)),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apppCheck__Amoun__09D5BAE0] DEFAULT ((0)),
[Discount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apppCheck__Disco__0AC9DF19] DEFAULT ((0)),
[PrevPay] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apppCheck__PrevP__0BBE0352] DEFAULT ((0)),
[Payment] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apppCheck__Payme__0CB2278B] DEFAULT ((0)),
[PayTerms] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Printed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__apppCheck__Print__0DA64BC4] DEFAULT ('N'),
[Assigned] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__apppCheck__Assig__0E9A6FFD] DEFAULT ('N'),
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sequence] [int] NOT NULL CONSTRAINT [DF__apppCheck__Seque__0F8E9436] DEFAULT ((0)),
[CheckNo] [bigint] NOT NULL CONSTRAINT [DF__apppCheck__Check__1082B86F] DEFAULT ((0)),
[CheckDate] [datetime] NULL,
[CBDebitAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apppCheck__CBDeb__1176DCA8] DEFAULT ((0)),
[Memo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EFT] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailRemittance] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExportText] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__apppCheck__Expor__126B00E1] DEFAULT ('N'),
[CheckNoRef] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SEPAIndicator] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Diary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[apppChecks] ADD CONSTRAINT [apppChecksPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [APPPChecksPostSeqVendorBankCodeIDX] ON [dbo].[apppChecks] ([PostSeq], [Vendor], [BankCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
