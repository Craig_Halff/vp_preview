CREATE TABLE [dbo].[CCG_PAT_ConfigRouteDetail]
(
[Route] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RouteType] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RouteValue] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Param1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Param2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NOT NULL,
[Parallel] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Configuration details for route entries', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRouteDetail', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated - see CCG_PAT_ConfigRouteDetailDescriptions', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRouteDetail', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Is this route step a part of a parallel approval process? Y/N.  Parallel items must have the same SortOrder value and this field set to Y.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRouteDetail', 'COLUMN', N'Parallel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Optional parameter for the route type', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRouteDetail', 'COLUMN', N'Param1'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Optional parameter for the route type', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRouteDetail', 'COLUMN', N'Param2'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - Route', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRouteDetail', 'COLUMN', N'Route'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Type of route entry - PR / EM / VE / SP', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRouteDetail', 'COLUMN', N'RouteType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Value for the route type', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRouteDetail', 'COLUMN', N'RouteValue'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Relative order of the route entries in the approval process', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRouteDetail', 'COLUMN', N'SortOrder'
GO
