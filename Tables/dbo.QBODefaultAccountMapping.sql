CREATE TABLE [dbo].[QBODefaultAccountMapping]
(
[PKey] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[QBOName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QBOClassification] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QBOAccountType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QBOAccountSubType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DPSAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DPSName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DPSType] [smallint] NOT NULL CONSTRAINT [DF__QBODefaul__DPSTy__329B06AD] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QBODefaultAccountMapping] ADD CONSTRAINT [QBODefaultAccountMappingPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
