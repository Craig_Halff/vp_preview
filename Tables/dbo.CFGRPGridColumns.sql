CREATE TABLE [dbo].[CFGRPGridColumns]
(
[PKey] [int] NOT NULL IDENTITY(1, 1),
[LECUType] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridType] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColName] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Visible] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColSeq] [smallint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGRPGridColumns] ADD CONSTRAINT [CFGRPGridColumnsPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
