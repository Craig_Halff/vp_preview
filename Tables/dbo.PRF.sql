CREATE TABLE [dbo].[PRF]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__PRF_New__Period__34D6BE2C] DEFAULT ((0)),
[RegOH] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRF_New__RegOH__35CAE265] DEFAULT ((0)),
[GandAOH] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRF_New__GandAOH__36BF069E] DEFAULT ((0)),
[RegOHProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRF_New__RegOHPr__37B32AD7] DEFAULT ((0)),
[GandAOHProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRF_New__GandAOH__38A74F10] DEFAULT ((0)),
[ProjectExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegOHBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRF_New__RegOHBi__399B7349] DEFAULT ((0)),
[GandAOHBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRF_New__GandAOH__3A8F9782] DEFAULT ((0)),
[BillingExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PRF] ADD CONSTRAINT [PRFPK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [Period]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PRFPeriodIDX] ON [dbo].[PRF] ([Period]) ON [PRIMARY]
GO
