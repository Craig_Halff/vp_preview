CREATE TABLE [dbo].[WorkflowLog]
(
[LogID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SaveID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApplicationName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TableName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EventType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkflowKey] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EventID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EventDescription] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionDescription] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkflowUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkflowTime] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowLog] ADD CONSTRAINT [WorkflowLogPK] PRIMARY KEY NONCLUSTERED ([LogID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [WorkflowLogWorkflowTimeIDX] ON [dbo].[WorkflowLog] ([WorkflowTime]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
