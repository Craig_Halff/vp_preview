CREATE TABLE [dbo].[AlertsDescriptions]
(
[AlertID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlertName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AlertsDescriptions] ADD CONSTRAINT [AlertsDescriptionsPK] PRIMARY KEY NONCLUSTERED ([AlertID], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
