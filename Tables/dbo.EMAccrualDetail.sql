CREATE TABLE [dbo].[EMAccrualDetail]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__EMAccrual__Perio__7856F47A] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__EMAccrual__PostS__794B18B3] DEFAULT ((0)),
[Earned] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMAccrual__Earne__7A3F3CEC] DEFAULT ((0)),
[Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMAccrual__Overr__7B336125] DEFAULT ('N'),
[Taken] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMAccrual__Taken__7C27855E] DEFAULT ((0)),
[EarnedSinceLastPayroll] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMAccrual__Earne__7D1BA997] DEFAULT ((0)),
[TakenSinceLastPayroll] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMAccrual__Taken__7E0FCDD0] DEFAULT ((0)),
[HoursWorked] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMAccrual__Hours__7F03F209] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_EMAccrualDetail]
      ON [dbo].[EMAccrualDetail]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMAccrualDetail'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PostSeq],121),'Employee',CONVERT(NVARCHAR(2000),DELETED.[Employee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Employee = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PostSeq],121),'EmployeeCompany',CONVERT(NVARCHAR(2000),[EmployeeCompany],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PostSeq],121),'Code',CONVERT(NVARCHAR(2000),[Code],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PostSeq],121),'Period',CONVERT(NVARCHAR(2000),[Period],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PostSeq],121),'PostSeq',CONVERT(NVARCHAR(2000),[PostSeq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PostSeq],121),'Earned',CONVERT(NVARCHAR(2000),[Earned],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PostSeq],121),'Override',CONVERT(NVARCHAR(2000),[Override],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PostSeq],121),'Taken',CONVERT(NVARCHAR(2000),[Taken],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PostSeq],121),'EarnedSinceLastPayroll',CONVERT(NVARCHAR(2000),[EarnedSinceLastPayroll],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PostSeq],121),'TakenSinceLastPayroll',CONVERT(NVARCHAR(2000),[TakenSinceLastPayroll],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Code],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PostSeq],121),'HoursWorked',CONVERT(NVARCHAR(2000),[HoursWorked],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_EMAccrualDetail] ON [dbo].[EMAccrualDetail]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_EMAccrualDetail]
      ON [dbo].[EMAccrualDetail]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMAccrualDetail'
    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'Employee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Employee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Employee = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'EmployeeCompany',NULL,CONVERT(NVARCHAR(2000),[EmployeeCompany],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'Code',NULL,CONVERT(NVARCHAR(2000),[Code],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'Period',NULL,CONVERT(NVARCHAR(2000),[Period],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'PostSeq',NULL,CONVERT(NVARCHAR(2000),[PostSeq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'Earned',NULL,CONVERT(NVARCHAR(2000),[Earned],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'Override',NULL,CONVERT(NVARCHAR(2000),[Override],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'Taken',NULL,CONVERT(NVARCHAR(2000),[Taken],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'EarnedSinceLastPayroll',NULL,CONVERT(NVARCHAR(2000),[EarnedSinceLastPayroll],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'TakenSinceLastPayroll',NULL,CONVERT(NVARCHAR(2000),[TakenSinceLastPayroll],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'HoursWorked',NULL,CONVERT(NVARCHAR(2000),[HoursWorked],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_EMAccrualDetail] ON [dbo].[EMAccrualDetail]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_EMAccrualDetail]
      ON [dbo].[EMAccrualDetail]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMAccrualDetail'
    
     If UPDATE([Employee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'Employee',
     CONVERT(NVARCHAR(2000),DELETED.[Employee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Employee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND INSERTED.[Period] = DELETED.[Period] AND INSERTED.[PostSeq] = DELETED.[PostSeq] AND 
		(
			(
				INSERTED.[Employee] Is Null And
				DELETED.[Employee] Is Not Null
			) Or
			(
				INSERTED.[Employee] Is Not Null And
				DELETED.[Employee] Is Null
			) Or
			(
				INSERTED.[Employee] !=
				DELETED.[Employee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Employee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Employee = newDesc.Employee
		END		
		
      If UPDATE([EmployeeCompany])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'EmployeeCompany',
      CONVERT(NVARCHAR(2000),DELETED.[EmployeeCompany],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EmployeeCompany],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND INSERTED.[Period] = DELETED.[Period] AND INSERTED.[PostSeq] = DELETED.[PostSeq] AND 
		(
			(
				INSERTED.[EmployeeCompany] Is Null And
				DELETED.[EmployeeCompany] Is Not Null
			) Or
			(
				INSERTED.[EmployeeCompany] Is Not Null And
				DELETED.[EmployeeCompany] Is Null
			) Or
			(
				INSERTED.[EmployeeCompany] !=
				DELETED.[EmployeeCompany]
			)
		) 
		END		
		
      If UPDATE([Code])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'Code',
      CONVERT(NVARCHAR(2000),DELETED.[Code],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Code],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND INSERTED.[Period] = DELETED.[Period] AND INSERTED.[PostSeq] = DELETED.[PostSeq] AND 
		(
			(
				INSERTED.[Code] Is Null And
				DELETED.[Code] Is Not Null
			) Or
			(
				INSERTED.[Code] Is Not Null And
				DELETED.[Code] Is Null
			) Or
			(
				INSERTED.[Code] !=
				DELETED.[Code]
			)
		) 
		END		
		
      If UPDATE([Period])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'Period',
      CONVERT(NVARCHAR(2000),DELETED.[Period],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Period],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND INSERTED.[Period] = DELETED.[Period] AND INSERTED.[PostSeq] = DELETED.[PostSeq] AND 
		(
			(
				INSERTED.[Period] Is Null And
				DELETED.[Period] Is Not Null
			) Or
			(
				INSERTED.[Period] Is Not Null And
				DELETED.[Period] Is Null
			) Or
			(
				INSERTED.[Period] !=
				DELETED.[Period]
			)
		) 
		END		
		
      If UPDATE([PostSeq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'PostSeq',
      CONVERT(NVARCHAR(2000),DELETED.[PostSeq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PostSeq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND INSERTED.[Period] = DELETED.[Period] AND INSERTED.[PostSeq] = DELETED.[PostSeq] AND 
		(
			(
				INSERTED.[PostSeq] Is Null And
				DELETED.[PostSeq] Is Not Null
			) Or
			(
				INSERTED.[PostSeq] Is Not Null And
				DELETED.[PostSeq] Is Null
			) Or
			(
				INSERTED.[PostSeq] !=
				DELETED.[PostSeq]
			)
		) 
		END		
		
      If UPDATE([Earned])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'Earned',
      CONVERT(NVARCHAR(2000),DELETED.[Earned],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Earned],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND INSERTED.[Period] = DELETED.[Period] AND INSERTED.[PostSeq] = DELETED.[PostSeq] AND 
		(
			(
				INSERTED.[Earned] Is Null And
				DELETED.[Earned] Is Not Null
			) Or
			(
				INSERTED.[Earned] Is Not Null And
				DELETED.[Earned] Is Null
			) Or
			(
				INSERTED.[Earned] !=
				DELETED.[Earned]
			)
		) 
		END		
		
      If UPDATE([Override])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'Override',
      CONVERT(NVARCHAR(2000),DELETED.[Override],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Override],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND INSERTED.[Period] = DELETED.[Period] AND INSERTED.[PostSeq] = DELETED.[PostSeq] AND 
		(
			(
				INSERTED.[Override] Is Null And
				DELETED.[Override] Is Not Null
			) Or
			(
				INSERTED.[Override] Is Not Null And
				DELETED.[Override] Is Null
			) Or
			(
				INSERTED.[Override] !=
				DELETED.[Override]
			)
		) 
		END		
		
      If UPDATE([Taken])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'Taken',
      CONVERT(NVARCHAR(2000),DELETED.[Taken],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Taken],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND INSERTED.[Period] = DELETED.[Period] AND INSERTED.[PostSeq] = DELETED.[PostSeq] AND 
		(
			(
				INSERTED.[Taken] Is Null And
				DELETED.[Taken] Is Not Null
			) Or
			(
				INSERTED.[Taken] Is Not Null And
				DELETED.[Taken] Is Null
			) Or
			(
				INSERTED.[Taken] !=
				DELETED.[Taken]
			)
		) 
		END		
		
      If UPDATE([EarnedSinceLastPayroll])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'EarnedSinceLastPayroll',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedSinceLastPayroll],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedSinceLastPayroll],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND INSERTED.[Period] = DELETED.[Period] AND INSERTED.[PostSeq] = DELETED.[PostSeq] AND 
		(
			(
				INSERTED.[EarnedSinceLastPayroll] Is Null And
				DELETED.[EarnedSinceLastPayroll] Is Not Null
			) Or
			(
				INSERTED.[EarnedSinceLastPayroll] Is Not Null And
				DELETED.[EarnedSinceLastPayroll] Is Null
			) Or
			(
				INSERTED.[EarnedSinceLastPayroll] !=
				DELETED.[EarnedSinceLastPayroll]
			)
		) 
		END		
		
      If UPDATE([TakenSinceLastPayroll])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'TakenSinceLastPayroll',
      CONVERT(NVARCHAR(2000),DELETED.[TakenSinceLastPayroll],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TakenSinceLastPayroll],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND INSERTED.[Period] = DELETED.[Period] AND INSERTED.[PostSeq] = DELETED.[PostSeq] AND 
		(
			(
				INSERTED.[TakenSinceLastPayroll] Is Null And
				DELETED.[TakenSinceLastPayroll] Is Not Null
			) Or
			(
				INSERTED.[TakenSinceLastPayroll] Is Not Null And
				DELETED.[TakenSinceLastPayroll] Is Null
			) Or
			(
				INSERTED.[TakenSinceLastPayroll] !=
				DELETED.[TakenSinceLastPayroll]
			)
		) 
		END		
		
      If UPDATE([HoursWorked])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Code],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PostSeq],121),'HoursWorked',
      CONVERT(NVARCHAR(2000),DELETED.[HoursWorked],121),
      CONVERT(NVARCHAR(2000),INSERTED.[HoursWorked],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND INSERTED.[Code] = DELETED.[Code] AND INSERTED.[Period] = DELETED.[Period] AND INSERTED.[PostSeq] = DELETED.[PostSeq] AND 
		(
			(
				INSERTED.[HoursWorked] Is Null And
				DELETED.[HoursWorked] Is Not Null
			) Or
			(
				INSERTED.[HoursWorked] Is Not Null And
				DELETED.[HoursWorked] Is Null
			) Or
			(
				INSERTED.[HoursWorked] !=
				DELETED.[HoursWorked]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_EMAccrualDetail] ON [dbo].[EMAccrualDetail]
GO
ALTER TABLE [dbo].[EMAccrualDetail] ADD CONSTRAINT [EMAccrualDetailPK] PRIMARY KEY NONCLUSTERED ([Employee], [EmployeeCompany], [Code], [Period], [PostSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [EMAccrualDetailPeriodIDX] ON [dbo].[EMAccrualDetail] ([Period]) ON [PRIMARY]
GO
