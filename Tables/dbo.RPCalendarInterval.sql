CREATE TABLE [dbo].[RPCalendarInterval]
(
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NOT NULL,
[AccordionFormatID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EndDate] [datetime] NOT NULL,
[PeriodScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPCalenda__Perio__3FC94DEB] DEFAULT ('w'),
[PeriodCount] [int] NOT NULL CONSTRAINT [DF__RPCalenda__Perio__40BD7224] DEFAULT ((1)),
[NumWorkingDays] [int] NULL,
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPCalendarInterval] ADD CONSTRAINT [RPCalendarIntervalPK] PRIMARY KEY NONCLUSTERED ([PlanID], [AccordionFormatID], [StartDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPCalendarIntervalAccordionFormatIDIDX] ON [dbo].[RPCalendarInterval] ([AccordionFormatID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPCalendarIntervalPlanIDStartDateEndDateIDX] ON [dbo].[RPCalendarInterval] ([PlanID], [StartDate], [EndDate]) ON [PRIMARY]
GO
