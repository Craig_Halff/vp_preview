CREATE TABLE [dbo].[FW_CFGPhoneFormatDesc]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__FW_CFGPhone__Seq__019645F1] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGPhoneFormatDesc] ADD CONSTRAINT [FW_CFGPhoneFormatDescPK] PRIMARY KEY CLUSTERED ([PKey], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
