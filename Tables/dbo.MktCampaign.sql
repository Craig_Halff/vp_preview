CREATE TABLE [dbo].[MktCampaign]
(
[CampaignID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Number] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Audience] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Objective] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Budget] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__MktCampai__Budge__0AA08D3E] DEFAULT ((0)),
[ActualCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__MktCampai__Actua__0B94B177] DEFAULT ((0)),
[Revenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__MktCampai__Reven__0C88D5B0] DEFAULT ((0)),
[CampaignMgr] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MktgMgr] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Manager3] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LaunchDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[FirstAction] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentAction] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NextAction] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PotentialResponses] [smallint] NOT NULL CONSTRAINT [DF__MktCampai__Poten__0D7CF9E9] DEFAULT ((0)),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecordStatus] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ExchangeRateDate] [datetime] NULL,
[ActualRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__MktCampai__Actua__6C929E18] DEFAULT ((0)),
[PotentialRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__MktCampai__Poten__6D86C251] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_MktCampaign]
      ON [dbo].[MktCampaign]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'MktCampaign'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 )
begin

      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'Name',CONVERT(NVARCHAR(2000),[Name],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'Number',CONVERT(NVARCHAR(2000),[Number],121),NULL, @source, @app
      FROM DELETED
    
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'CampaignID',CONVERT(NVARCHAR(2000),[CampaignID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'Name',CONVERT(NVARCHAR(2000),[Name],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'Number',CONVERT(NVARCHAR(2000),[Number],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'Description','[text]',NULL, @source,@app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'Org',CONVERT(NVARCHAR(2000),DELETED.[Org],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join Organization as oldDesc  on DELETED.Org = oldDesc.Org

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'Type',CONVERT(NVARCHAR(2000),DELETED.[Type],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGCampaignType as oldDesc  on DELETED.Type = oldDesc.Code

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'Audience',CONVERT(NVARCHAR(2000),DELETED.[Audience],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGCampaignAudience as oldDesc  on DELETED.Audience = oldDesc.Code

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'Objective',CONVERT(NVARCHAR(2000),DELETED.[Objective],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGCampaignObjective as oldDesc  on DELETED.Objective = oldDesc.Code

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'Budget',CONVERT(NVARCHAR(2000),[Budget],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'ActualCost',CONVERT(NVARCHAR(2000),[ActualCost],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'Revenue',CONVERT(NVARCHAR(2000),[Revenue],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'CampaignMgr',CONVERT(NVARCHAR(2000),DELETED.[CampaignMgr],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CampaignMgr = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'MktgMgr',CONVERT(NVARCHAR(2000),DELETED.[MktgMgr],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.MktgMgr = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'Manager3',CONVERT(NVARCHAR(2000),DELETED.[Manager3],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Manager3 = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'LaunchDate',CONVERT(NVARCHAR(2000),[LaunchDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'EndDate',CONVERT(NVARCHAR(2000),[EndDate],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'FirstAction',CONVERT(NVARCHAR(2000),DELETED.[FirstAction],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGCampaignAction as oldDesc  on DELETED.FirstAction = oldDesc.Code

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'CurrentAction',CONVERT(NVARCHAR(2000),DELETED.[CurrentAction],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGCampaignAction as oldDesc  on DELETED.CurrentAction = oldDesc.Code

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'NextAction',CONVERT(NVARCHAR(2000),DELETED.[NextAction],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGCampaignAction as oldDesc  on DELETED.NextAction = oldDesc.Code

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'Status',CONVERT(NVARCHAR(2000),DELETED.[Status],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGCampaignStatus as oldDesc  on DELETED.Status = oldDesc.Code

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'PotentialResponses',CONVERT(NVARCHAR(2000),[PotentialResponses],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'WBS1',CONVERT(NVARCHAR(2000),[WBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'WBS2',CONVERT(NVARCHAR(2000),[WBS2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'WBS3',CONVERT(NVARCHAR(2000),[WBS3],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'RecordStatus',CONVERT(NVARCHAR(2000),DELETED.[RecordStatus],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGCampaignRecStatus as oldDesc  on DELETED.RecordStatus = oldDesc.Code

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'CustomCurrencyCode',CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'ExchangeRateDate',CONVERT(NVARCHAR(2000),[ExchangeRateDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'ActualRevenue',CONVERT(NVARCHAR(2000),[ActualRevenue],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'PotentialRevenue',CONVERT(NVARCHAR(2000),[PotentialRevenue],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_MktCampaign] ON [dbo].[MktCampaign]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_MktCampaign]
      ON [dbo].[MktCampaign]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'MktCampaign'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'CampaignID',NULL,CONVERT(NVARCHAR(2000),[CampaignID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Name',NULL,CONVERT(NVARCHAR(2000),[Name],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Number',NULL,CONVERT(NVARCHAR(2000),[Number],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Description',NULL,'[text]', @source, @app
      FROM INSERTED


    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Org',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Org],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  Organization as newDesc  on INSERTED.Org = newDesc.Org

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Type',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Type],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGCampaignType as newDesc  on INSERTED.Type = newDesc.Code

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Audience',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Audience],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGCampaignAudience as newDesc  on INSERTED.Audience = newDesc.Code

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Objective',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Objective],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGCampaignObjective as newDesc  on INSERTED.Objective = newDesc.Code

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Budget',NULL,CONVERT(NVARCHAR(2000),[Budget],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'ActualCost',NULL,CONVERT(NVARCHAR(2000),[ActualCost],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Revenue',NULL,CONVERT(NVARCHAR(2000),[Revenue],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'CampaignMgr',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CampaignMgr],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CampaignMgr = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'MktgMgr',NULL,CONVERT(NVARCHAR(2000),INSERTED.[MktgMgr],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.MktgMgr = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Manager3',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Manager3],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Manager3 = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'LaunchDate',NULL,CONVERT(NVARCHAR(2000),[LaunchDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'EndDate',NULL,CONVERT(NVARCHAR(2000),[EndDate],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'FirstAction',NULL,CONVERT(NVARCHAR(2000),INSERTED.[FirstAction],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGCampaignAction as newDesc  on INSERTED.FirstAction = newDesc.Code

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'CurrentAction',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CurrentAction],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGCampaignAction as newDesc  on INSERTED.CurrentAction = newDesc.Code

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'NextAction',NULL,CONVERT(NVARCHAR(2000),INSERTED.[NextAction],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGCampaignAction as newDesc  on INSERTED.NextAction = newDesc.Code

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Status',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Status],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGCampaignStatus as newDesc  on INSERTED.Status = newDesc.Code

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'PotentialResponses',NULL,CONVERT(NVARCHAR(2000),[PotentialResponses],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'WBS1',NULL,CONVERT(NVARCHAR(2000),[WBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'WBS2',NULL,CONVERT(NVARCHAR(2000),[WBS2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'WBS3',NULL,CONVERT(NVARCHAR(2000),[WBS3],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'RecordStatus',NULL,CONVERT(NVARCHAR(2000),INSERTED.[RecordStatus],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGCampaignRecStatus as newDesc  on INSERTED.RecordStatus = newDesc.Code

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'CustomCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'ExchangeRateDate',NULL,CONVERT(NVARCHAR(2000),[ExchangeRateDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'ActualRevenue',NULL,CONVERT(NVARCHAR(2000),[ActualRevenue],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'PotentialRevenue',NULL,CONVERT(NVARCHAR(2000),[PotentialRevenue],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_MktCampaign] ON [dbo].[MktCampaign]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_MktCampaign]
      ON [dbo].[MktCampaign]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'MktCampaign'
    
      If UPDATE([CampaignID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'CampaignID',
      CONVERT(NVARCHAR(2000),DELETED.[CampaignID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CampaignID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[CampaignID] Is Null And
				DELETED.[CampaignID] Is Not Null
			) Or
			(
				INSERTED.[CampaignID] Is Not Null And
				DELETED.[CampaignID] Is Null
			) Or
			(
				INSERTED.[CampaignID] !=
				DELETED.[CampaignID]
			)
		) 
		END		
		
      If UPDATE([Name])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Name',
      CONVERT(NVARCHAR(2000),DELETED.[Name],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Name],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[Name] Is Null And
				DELETED.[Name] Is Not Null
			) Or
			(
				INSERTED.[Name] Is Not Null And
				DELETED.[Name] Is Null
			) Or
			(
				INSERTED.[Name] !=
				DELETED.[Name]
			)
		) 
		END		
		
      If UPDATE([Number])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Number',
      CONVERT(NVARCHAR(2000),DELETED.[Number],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Number],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[Number] Is Null And
				DELETED.[Number] Is Not Null
			) Or
			(
				INSERTED.[Number] Is Not Null And
				DELETED.[Number] Is Null
			) Or
			(
				INSERTED.[Number] !=
				DELETED.[Number]
			)
		) 
		END		
		
      If UPDATE([Description])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Description',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
     If UPDATE([Org])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Org',
     CONVERT(NVARCHAR(2000),DELETED.[Org],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Org],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[Org] Is Null And
				DELETED.[Org] Is Not Null
			) Or
			(
				INSERTED.[Org] Is Not Null And
				DELETED.[Org] Is Null
			) Or
			(
				INSERTED.[Org] !=
				DELETED.[Org]
			)
		) left join Organization as oldDesc  on DELETED.Org = oldDesc.Org  left join  Organization as newDesc  on INSERTED.Org = newDesc.Org
		END		
		
     If UPDATE([Type])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Type',
     CONVERT(NVARCHAR(2000),DELETED.[Type],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Type],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[Type] Is Null And
				DELETED.[Type] Is Not Null
			) Or
			(
				INSERTED.[Type] Is Not Null And
				DELETED.[Type] Is Null
			) Or
			(
				INSERTED.[Type] !=
				DELETED.[Type]
			)
		) left join CFGCampaignType as oldDesc  on DELETED.Type = oldDesc.Code  left join  CFGCampaignType as newDesc  on INSERTED.Type = newDesc.Code
		END		
		
     If UPDATE([Audience])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Audience',
     CONVERT(NVARCHAR(2000),DELETED.[Audience],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Audience],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[Audience] Is Null And
				DELETED.[Audience] Is Not Null
			) Or
			(
				INSERTED.[Audience] Is Not Null And
				DELETED.[Audience] Is Null
			) Or
			(
				INSERTED.[Audience] !=
				DELETED.[Audience]
			)
		) left join CFGCampaignAudience as oldDesc  on DELETED.Audience = oldDesc.Code  left join  CFGCampaignAudience as newDesc  on INSERTED.Audience = newDesc.Code
		END		
		
     If UPDATE([Objective])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Objective',
     CONVERT(NVARCHAR(2000),DELETED.[Objective],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Objective],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[Objective] Is Null And
				DELETED.[Objective] Is Not Null
			) Or
			(
				INSERTED.[Objective] Is Not Null And
				DELETED.[Objective] Is Null
			) Or
			(
				INSERTED.[Objective] !=
				DELETED.[Objective]
			)
		) left join CFGCampaignObjective as oldDesc  on DELETED.Objective = oldDesc.Code  left join  CFGCampaignObjective as newDesc  on INSERTED.Objective = newDesc.Code
		END		
		
      If UPDATE([Budget])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Budget',
      CONVERT(NVARCHAR(2000),DELETED.[Budget],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Budget],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[Budget] Is Null And
				DELETED.[Budget] Is Not Null
			) Or
			(
				INSERTED.[Budget] Is Not Null And
				DELETED.[Budget] Is Null
			) Or
			(
				INSERTED.[Budget] !=
				DELETED.[Budget]
			)
		) 
		END		
		
      If UPDATE([ActualCost])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'ActualCost',
      CONVERT(NVARCHAR(2000),DELETED.[ActualCost],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ActualCost],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[ActualCost] Is Null And
				DELETED.[ActualCost] Is Not Null
			) Or
			(
				INSERTED.[ActualCost] Is Not Null And
				DELETED.[ActualCost] Is Null
			) Or
			(
				INSERTED.[ActualCost] !=
				DELETED.[ActualCost]
			)
		) 
		END		
		
      If UPDATE([Revenue])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Revenue',
      CONVERT(NVARCHAR(2000),DELETED.[Revenue],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Revenue],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[Revenue] Is Null And
				DELETED.[Revenue] Is Not Null
			) Or
			(
				INSERTED.[Revenue] Is Not Null And
				DELETED.[Revenue] Is Null
			) Or
			(
				INSERTED.[Revenue] !=
				DELETED.[Revenue]
			)
		) 
		END		
		
     If UPDATE([CampaignMgr])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'CampaignMgr',
     CONVERT(NVARCHAR(2000),DELETED.[CampaignMgr],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CampaignMgr],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[CampaignMgr] Is Null And
				DELETED.[CampaignMgr] Is Not Null
			) Or
			(
				INSERTED.[CampaignMgr] Is Not Null And
				DELETED.[CampaignMgr] Is Null
			) Or
			(
				INSERTED.[CampaignMgr] !=
				DELETED.[CampaignMgr]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CampaignMgr = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CampaignMgr = newDesc.Employee
		END		
		
     If UPDATE([MktgMgr])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'MktgMgr',
     CONVERT(NVARCHAR(2000),DELETED.[MktgMgr],121),
     CONVERT(NVARCHAR(2000),INSERTED.[MktgMgr],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[MktgMgr] Is Null And
				DELETED.[MktgMgr] Is Not Null
			) Or
			(
				INSERTED.[MktgMgr] Is Not Null And
				DELETED.[MktgMgr] Is Null
			) Or
			(
				INSERTED.[MktgMgr] !=
				DELETED.[MktgMgr]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.MktgMgr = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.MktgMgr = newDesc.Employee
		END		
		
     If UPDATE([Manager3])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Manager3',
     CONVERT(NVARCHAR(2000),DELETED.[Manager3],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Manager3],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[Manager3] Is Null And
				DELETED.[Manager3] Is Not Null
			) Or
			(
				INSERTED.[Manager3] Is Not Null And
				DELETED.[Manager3] Is Null
			) Or
			(
				INSERTED.[Manager3] !=
				DELETED.[Manager3]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Manager3 = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Manager3 = newDesc.Employee
		END		
		
      If UPDATE([LaunchDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'LaunchDate',
      CONVERT(NVARCHAR(2000),DELETED.[LaunchDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LaunchDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[LaunchDate] Is Null And
				DELETED.[LaunchDate] Is Not Null
			) Or
			(
				INSERTED.[LaunchDate] Is Not Null And
				DELETED.[LaunchDate] Is Null
			) Or
			(
				INSERTED.[LaunchDate] !=
				DELETED.[LaunchDate]
			)
		) 
		END		
		
      If UPDATE([EndDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'EndDate',
      CONVERT(NVARCHAR(2000),DELETED.[EndDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EndDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[EndDate] Is Null And
				DELETED.[EndDate] Is Not Null
			) Or
			(
				INSERTED.[EndDate] Is Not Null And
				DELETED.[EndDate] Is Null
			) Or
			(
				INSERTED.[EndDate] !=
				DELETED.[EndDate]
			)
		) 
		END		
		
     If UPDATE([FirstAction])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'FirstAction',
     CONVERT(NVARCHAR(2000),DELETED.[FirstAction],121),
     CONVERT(NVARCHAR(2000),INSERTED.[FirstAction],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[FirstAction] Is Null And
				DELETED.[FirstAction] Is Not Null
			) Or
			(
				INSERTED.[FirstAction] Is Not Null And
				DELETED.[FirstAction] Is Null
			) Or
			(
				INSERTED.[FirstAction] !=
				DELETED.[FirstAction]
			)
		) left join CFGCampaignAction as oldDesc  on DELETED.FirstAction = oldDesc.Code  left join  CFGCampaignAction as newDesc  on INSERTED.FirstAction = newDesc.Code
		END		
		
     If UPDATE([CurrentAction])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'CurrentAction',
     CONVERT(NVARCHAR(2000),DELETED.[CurrentAction],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CurrentAction],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[CurrentAction] Is Null And
				DELETED.[CurrentAction] Is Not Null
			) Or
			(
				INSERTED.[CurrentAction] Is Not Null And
				DELETED.[CurrentAction] Is Null
			) Or
			(
				INSERTED.[CurrentAction] !=
				DELETED.[CurrentAction]
			)
		) left join CFGCampaignAction as oldDesc  on DELETED.CurrentAction = oldDesc.Code  left join  CFGCampaignAction as newDesc  on INSERTED.CurrentAction = newDesc.Code
		END		
		
     If UPDATE([NextAction])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'NextAction',
     CONVERT(NVARCHAR(2000),DELETED.[NextAction],121),
     CONVERT(NVARCHAR(2000),INSERTED.[NextAction],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[NextAction] Is Null And
				DELETED.[NextAction] Is Not Null
			) Or
			(
				INSERTED.[NextAction] Is Not Null And
				DELETED.[NextAction] Is Null
			) Or
			(
				INSERTED.[NextAction] !=
				DELETED.[NextAction]
			)
		) left join CFGCampaignAction as oldDesc  on DELETED.NextAction = oldDesc.Code  left join  CFGCampaignAction as newDesc  on INSERTED.NextAction = newDesc.Code
		END		
		
     If UPDATE([Status])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'Status',
     CONVERT(NVARCHAR(2000),DELETED.[Status],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Status],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[Status] Is Null And
				DELETED.[Status] Is Not Null
			) Or
			(
				INSERTED.[Status] Is Not Null And
				DELETED.[Status] Is Null
			) Or
			(
				INSERTED.[Status] !=
				DELETED.[Status]
			)
		) left join CFGCampaignStatus as oldDesc  on DELETED.Status = oldDesc.Code  left join  CFGCampaignStatus as newDesc  on INSERTED.Status = newDesc.Code
		END		
		
      If UPDATE([PotentialResponses])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'PotentialResponses',
      CONVERT(NVARCHAR(2000),DELETED.[PotentialResponses],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PotentialResponses],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[PotentialResponses] Is Null And
				DELETED.[PotentialResponses] Is Not Null
			) Or
			(
				INSERTED.[PotentialResponses] Is Not Null And
				DELETED.[PotentialResponses] Is Null
			) Or
			(
				INSERTED.[PotentialResponses] !=
				DELETED.[PotentialResponses]
			)
		) 
		END		
		
      If UPDATE([WBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'WBS1',
      CONVERT(NVARCHAR(2000),DELETED.[WBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[WBS1] Is Null And
				DELETED.[WBS1] Is Not Null
			) Or
			(
				INSERTED.[WBS1] Is Not Null And
				DELETED.[WBS1] Is Null
			) Or
			(
				INSERTED.[WBS1] !=
				DELETED.[WBS1]
			)
		) 
		END		
		
      If UPDATE([WBS2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'WBS2',
      CONVERT(NVARCHAR(2000),DELETED.[WBS2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[WBS2] Is Null And
				DELETED.[WBS2] Is Not Null
			) Or
			(
				INSERTED.[WBS2] Is Not Null And
				DELETED.[WBS2] Is Null
			) Or
			(
				INSERTED.[WBS2] !=
				DELETED.[WBS2]
			)
		) 
		END		
		
      If UPDATE([WBS3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'WBS3',
      CONVERT(NVARCHAR(2000),DELETED.[WBS3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[WBS3] Is Null And
				DELETED.[WBS3] Is Not Null
			) Or
			(
				INSERTED.[WBS3] Is Not Null And
				DELETED.[WBS3] Is Null
			) Or
			(
				INSERTED.[WBS3] !=
				DELETED.[WBS3]
			)
		) 
		END		
		
     If UPDATE([RecordStatus])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'RecordStatus',
     CONVERT(NVARCHAR(2000),DELETED.[RecordStatus],121),
     CONVERT(NVARCHAR(2000),INSERTED.[RecordStatus],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[RecordStatus] Is Null And
				DELETED.[RecordStatus] Is Not Null
			) Or
			(
				INSERTED.[RecordStatus] Is Not Null And
				DELETED.[RecordStatus] Is Null
			) Or
			(
				INSERTED.[RecordStatus] !=
				DELETED.[RecordStatus]
			)
		) left join CFGCampaignRecStatus as oldDesc  on DELETED.RecordStatus = oldDesc.Code  left join  CFGCampaignRecStatus as newDesc  on INSERTED.RecordStatus = newDesc.Code
		END		
		
      If UPDATE([CustomCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'CustomCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustomCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustomCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[CustomCurrencyCode] Is Null And
				DELETED.[CustomCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] Is Not Null And
				DELETED.[CustomCurrencyCode] Is Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] !=
				DELETED.[CustomCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([ExchangeRateDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'ExchangeRateDate',
      CONVERT(NVARCHAR(2000),DELETED.[ExchangeRateDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ExchangeRateDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[ExchangeRateDate] Is Null And
				DELETED.[ExchangeRateDate] Is Not Null
			) Or
			(
				INSERTED.[ExchangeRateDate] Is Not Null And
				DELETED.[ExchangeRateDate] Is Null
			) Or
			(
				INSERTED.[ExchangeRateDate] !=
				DELETED.[ExchangeRateDate]
			)
		) 
		END		
		
      If UPDATE([ActualRevenue])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'ActualRevenue',
      CONVERT(NVARCHAR(2000),DELETED.[ActualRevenue],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ActualRevenue],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[ActualRevenue] Is Null And
				DELETED.[ActualRevenue] Is Not Null
			) Or
			(
				INSERTED.[ActualRevenue] Is Not Null And
				DELETED.[ActualRevenue] Is Null
			) Or
			(
				INSERTED.[ActualRevenue] !=
				DELETED.[ActualRevenue]
			)
		) 
		END		
		
      If UPDATE([PotentialRevenue])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'PotentialRevenue',
      CONVERT(NVARCHAR(2000),DELETED.[PotentialRevenue],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PotentialRevenue],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[PotentialRevenue] Is Null And
				DELETED.[PotentialRevenue] Is Not Null
			) Or
			(
				INSERTED.[PotentialRevenue] Is Not Null And
				DELETED.[PotentialRevenue] Is Null
			) Or
			(
				INSERTED.[PotentialRevenue] !=
				DELETED.[PotentialRevenue]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_MktCampaign] ON [dbo].[MktCampaign]
GO
ALTER TABLE [dbo].[MktCampaign] ADD CONSTRAINT [MktCampaignPK] PRIMARY KEY NONCLUSTERED ([CampaignID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
