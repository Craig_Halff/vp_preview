CREATE TABLE [dbo].[CFGCubeTranslationData]
(
[Usage] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCubeTranslationData] ADD CONSTRAINT [CFGCubeTranslationDataPK] PRIMARY KEY NONCLUSTERED ([Usage], [Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
