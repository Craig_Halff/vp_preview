CREATE TABLE [dbo].[BTROTCats]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__BTROTCats__Table__3449B6E4] DEFAULT ((0)),
[RateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BTROTCats__Rate__353DDB1D] DEFAULT ((0)),
[Category] [smallint] NOT NULL CONSTRAINT [DF__BTROTCats__Categ__3631FF56] DEFAULT ((0)),
[EffectiveDate] [datetime] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTROTCats] ADD CONSTRAINT [BTROTCatsPK] PRIMARY KEY NONCLUSTERED ([TableNo], [RateID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [BTROTCatsCategoryIDX] ON [dbo].[BTROTCats] ([TableNo], [Category]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
