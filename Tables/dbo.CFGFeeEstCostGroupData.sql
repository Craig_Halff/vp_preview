CREATE TABLE [dbo].[CFGFeeEstCostGroupData]
(
[Code] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Disabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGFeeEst__Disab__170598CC] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGFeeEstCostGroupData] ADD CONSTRAINT [CFGFeeEstCostGroupDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
