CREATE TABLE [dbo].[CCG_EI_ConfigGrid]
(
[GridInstanceName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridVisibleInEI] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridProjectsVisibleInEI] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_ConfigGrid] ADD CONSTRAINT [PK_CCG_EI_ConfigGrid] PRIMARY KEY CLUSTERED ([GridInstanceName]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Defines the EleVia Grid instances available', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigGrid', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'The name of the Grid Instance', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigGrid', 'COLUMN', N'GridInstanceName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_ConfigGrid / GridProjectsVisibleInEI Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigGrid', 'COLUMN', N'GridProjectsVisibleInEI'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_ConfigGrid / GridVisibleInEI Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigGrid', 'COLUMN', N'GridVisibleInEI'
GO
