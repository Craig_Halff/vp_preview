CREATE TABLE [dbo].[CFGOppEstService_Backup]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EstServiceFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOppEst__EstSe__1E3CA640] DEFAULT ('N'),
[ExpenseFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOppEst__Expen__1F30CA79] DEFAULT ('Y'),
[ConsultantFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOppEst__Consu__2024EEB2] DEFAULT ('Y'),
[UnitFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOppEst__UnitF__211912EB] DEFAULT ('Y'),
[EstimateType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOppEst__Estim__220D3724] DEFAULT ('C'),
[LabCostTable] [int] NOT NULL CONSTRAINT [DF__CFGOppEst__LabCo__23015B5D] DEFAULT ((0)),
[LabBillTable] [int] NOT NULL CONSTRAINT [DF__CFGOppEst__LabBi__23F57F96] DEFAULT ((0)),
[ExpBillMultiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGOppEst__ExpBi__24E9A3CF] DEFAULT ((0)),
[ConBillMultiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGOppEst__ConBi__25DDC808] DEFAULT ((0)),
[HrDecimals] [smallint] NOT NULL CONSTRAINT [DF__CFGOppEst__HrDec__26D1EC41] DEFAULT ((0)),
[QtyDecimals] [smallint] NOT NULL CONSTRAINT [DF__CFGOppEst__QtyDe__27C6107A] DEFAULT ((0)),
[AmtDecimals] [smallint] NOT NULL CONSTRAINT [DF__CFGOppEst__AmtDe__28BA34B3] DEFAULT ((0)),
[BVSURL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGOppEstService_Backup] ADD CONSTRAINT [CFGOppEstServicePK] PRIMARY KEY CLUSTERED ([Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
