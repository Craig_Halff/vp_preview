CREATE TABLE [dbo].[CFGICBillingOverrides]
(
[OvrType] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TargetCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OriginatingCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OvrMethod] [smallint] NOT NULL CONSTRAINT [DF__CFGICBill__OvrMe__02C995F5] DEFAULT ((0)),
[OvrTableNo] [int] NOT NULL CONSTRAINT [DF__CFGICBill__OvrTa__03BDBA2E] DEFAULT ((0)),
[OvrMult] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGICBill__OvrMu__04B1DE67] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGICBillingOverrides] ADD CONSTRAINT [CFGICBillingOverridesPK] PRIMARY KEY CLUSTERED ([OvrType], [TargetCompany], [OriginatingCompany]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
