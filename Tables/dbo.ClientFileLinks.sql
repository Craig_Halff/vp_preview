CREATE TABLE [dbo].[ClientFileLinks]
(
[LinkID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilePath] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Graphic] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ClientFil__Graph__21390E7C] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_ClientFileLinks]
      ON [dbo].[ClientFileLinks]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ClientFileLinks'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'LinkID',CONVERT(NVARCHAR(2000),[LinkID],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'ClientID',CONVERT(NVARCHAR(2000),DELETED.[ClientID],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join CL as oldDesc  on DELETED.ClientID = oldDesc.ClientID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Description',CONVERT(NVARCHAR(2000),[Description],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'FilePath',CONVERT(NVARCHAR(2000),[FilePath],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Graphic',CONVERT(NVARCHAR(2000),[Graphic],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_ClientFileLinks]
      ON [dbo].[ClientFileLinks]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ClientFileLinks'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'LinkID',NULL,CONVERT(NVARCHAR(2000),[LinkID],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ClientID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[ClientID],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  CL as newDesc  on INSERTED.ClientID = newDesc.ClientID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Description',NULL,CONVERT(NVARCHAR(2000),[Description],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'FilePath',NULL,CONVERT(NVARCHAR(2000),[FilePath],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Graphic',NULL,CONVERT(NVARCHAR(2000),[Graphic],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_ClientFileLinks]
      ON [dbo].[ClientFileLinks]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ClientFileLinks'
    
      If UPDATE([LinkID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'LinkID',
      CONVERT(NVARCHAR(2000),DELETED.[LinkID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LinkID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[LinkID] = DELETED.[LinkID] AND INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[LinkID] Is Null And
				DELETED.[LinkID] Is Not Null
			) Or
			(
				INSERTED.[LinkID] Is Not Null And
				DELETED.[LinkID] Is Null
			) Or
			(
				INSERTED.[LinkID] !=
				DELETED.[LinkID]
			)
		) 
		END		
		
     If UPDATE([ClientID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ClientID',
     CONVERT(NVARCHAR(2000),DELETED.[ClientID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[ClientID],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[LinkID] = DELETED.[LinkID] AND INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[ClientID] Is Null And
				DELETED.[ClientID] Is Not Null
			) Or
			(
				INSERTED.[ClientID] Is Not Null And
				DELETED.[ClientID] Is Null
			) Or
			(
				INSERTED.[ClientID] !=
				DELETED.[ClientID]
			)
		) left join CL as oldDesc  on DELETED.ClientID = oldDesc.ClientID  left join  CL as newDesc  on INSERTED.ClientID = newDesc.ClientID
		END		
		
      If UPDATE([Description])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Description',
      CONVERT(NVARCHAR(2000),DELETED.[Description],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Description],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[LinkID] = DELETED.[LinkID] AND INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[Description] Is Null And
				DELETED.[Description] Is Not Null
			) Or
			(
				INSERTED.[Description] Is Not Null And
				DELETED.[Description] Is Null
			) Or
			(
				INSERTED.[Description] !=
				DELETED.[Description]
			)
		) 
		END		
		
      If UPDATE([FilePath])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'FilePath',
      CONVERT(NVARCHAR(2000),DELETED.[FilePath],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FilePath],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[LinkID] = DELETED.[LinkID] AND INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[FilePath] Is Null And
				DELETED.[FilePath] Is Not Null
			) Or
			(
				INSERTED.[FilePath] Is Not Null And
				DELETED.[FilePath] Is Null
			) Or
			(
				INSERTED.[FilePath] !=
				DELETED.[FilePath]
			)
		) 
		END		
		
      If UPDATE([Graphic])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Graphic',
      CONVERT(NVARCHAR(2000),DELETED.[Graphic],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Graphic],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[LinkID] = DELETED.[LinkID] AND INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[Graphic] Is Null And
				DELETED.[Graphic] Is Not Null
			) Or
			(
				INSERTED.[Graphic] Is Not Null And
				DELETED.[Graphic] Is Null
			) Or
			(
				INSERTED.[Graphic] !=
				DELETED.[Graphic]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[ClientFileLinks] ADD CONSTRAINT [ClientFileLinksPK] PRIMARY KEY NONCLUSTERED ([LinkID], [ClientID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
