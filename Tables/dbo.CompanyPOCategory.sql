CREATE TABLE [dbo].[CompanyPOCategory]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Category] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReimbAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DirectAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndirectAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BalanceSheetAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Taxable] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CompanyPO__Taxab__344BE2F0] DEFAULT ('Y'),
[RequireQTY] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CompanyPO__Requi__35400729] DEFAULT ('N'),
[RequireUOM] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CompanyPO__Requi__36342B62] DEFAULT ('N'),
[InventoryAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CompanyPOCategory] ADD CONSTRAINT [CompanyPOCategoryPK] PRIMARY KEY NONCLUSTERED ([Company], [Category]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
