CREATE TABLE [dbo].[PYChecksE]
(
[Period] [int] NOT NULL CONSTRAINT [DF__PYChecksE__Perio__30BC1485] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__PYChecksE__PostS__31B038BE] DEFAULT ((0)),
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL,
[BankID] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Method] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksE__AmtPc__32A45CF7] DEFAULT ((0)),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksE__Amoun__33988130] DEFAULT ((0)),
[Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksE__Overr__348CA569] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PYChecksE] ADD CONSTRAINT [PYChecksEPK] PRIMARY KEY NONCLUSTERED ([Period], [PostSeq], [Employee], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
