CREATE TABLE [dbo].[SF330Projects]
(
[SF330ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProjID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProjSeq] [smallint] NOT NULL CONSTRAINT [DF__SF330Proj__ProjS__6F2E484A] DEFAULT ((0)),
[Wbs1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Wbs2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Wbs3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjKeyName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjOwner] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPhone] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearComplProfSvcs] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearComplConst] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF330Projects] ADD CONSTRAINT [SF330ProjectsPK] PRIMARY KEY NONCLUSTERED ([SF330ID], [ProjID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
