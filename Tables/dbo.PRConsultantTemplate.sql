CREATE TABLE [dbo].[PRConsultantTemplate]
(
[ConsultantID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OppConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRConsult__OppCo__5E7988EB] DEFAULT ((0)),
[OppConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRConsult__OppCo__5F6DAD24] DEFAULT ((0)),
[DirectAcctFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRConsult__Direc__6061D15D] DEFAULT ('N'),
[SeqNo] [smallint] NOT NULL CONSTRAINT [DF__PRConsult__SeqNo__6155F596] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PRConsultantTemplate] ADD CONSTRAINT [PRConsultantTemplatePK] PRIMARY KEY NONCLUSTERED ([ConsultantID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
