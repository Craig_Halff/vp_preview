CREATE TABLE [dbo].[_BeshRateUnits]
(
[WBS1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateSchedule] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitTable] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
