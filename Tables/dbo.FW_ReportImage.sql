CREATE TABLE [dbo].[FW_ReportImage]
(
[ImageID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Image] [varbinary] (max) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_ReportImage] ADD CONSTRAINT [FW_ReportImagePK] PRIMARY KEY CLUSTERED ([ImageID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
