CREATE TABLE [dbo].[MktCampaignCustomTabFields]
(
[CampaignID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustCampaignClient] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_MktCampaignCustomTabFields]
      ON [dbo].[MktCampaignCustomTabFields]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'MktCampaignCustomTabFields'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'CampaignID',CONVERT(NVARCHAR(2000),DELETED.[CampaignID],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join MktCampaign as oldDesc   on DELETED.CampaignID = oldDesc.CampaignID

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[CampaignID],121),'CustCampaignClient',CONVERT(NVARCHAR(2000),DELETED.[CustCampaignClient],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join CL as oldDesc   on DELETED.CustCampaignClient = oldDesc.ClientID

      
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_MktCampaignCustomTabFields] ON [dbo].[MktCampaignCustomTabFields]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_MktCampaignCustomTabFields]
      ON [dbo].[MktCampaignCustomTabFields]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'MktCampaignCustomTabFields'
    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'CampaignID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CampaignID],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  MktCampaign as newDesc  on INSERTED.CampaignID = newDesc.CampaignID

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'CustCampaignClient',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustCampaignClient],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  CL as newDesc  on INSERTED.CustCampaignClient = newDesc.ClientID

     
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_MktCampaignCustomTabFields] ON [dbo].[MktCampaignCustomTabFields]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_MktCampaignCustomTabFields]
      ON [dbo].[MktCampaignCustomTabFields]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'MktCampaignCustomTabFields'
    
     If UPDATE([CampaignID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'CampaignID',
     CONVERT(NVARCHAR(2000),DELETED.[CampaignID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CampaignID],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[CampaignID] Is Null And
				DELETED.[CampaignID] Is Not Null
			) Or
			(
				INSERTED.[CampaignID] Is Not Null And
				DELETED.[CampaignID] Is Null
			) Or
			(
				INSERTED.[CampaignID] !=
				DELETED.[CampaignID]
			)
		) left join MktCampaign as oldDesc  on DELETED.CampaignID = oldDesc.CampaignID  left join  MktCampaign as newDesc  on INSERTED.CampaignID = newDesc.CampaignID
		END		
		
     If UPDATE([CustCampaignClient])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[CampaignID],121),'CustCampaignClient',
     CONVERT(NVARCHAR(2000),DELETED.[CustCampaignClient],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustCampaignClient],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[CampaignID] = DELETED.[CampaignID] AND 
		(
			(
				INSERTED.[CustCampaignClient] Is Null And
				DELETED.[CustCampaignClient] Is Not Null
			) Or
			(
				INSERTED.[CustCampaignClient] Is Not Null And
				DELETED.[CustCampaignClient] Is Null
			) Or
			(
				INSERTED.[CustCampaignClient] !=
				DELETED.[CustCampaignClient]
			)
		) left join CL as oldDesc  on DELETED.CustCampaignClient = oldDesc.ClientID  left join  CL as newDesc  on INSERTED.CustCampaignClient = newDesc.ClientID
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_MktCampaignCustomTabFields] ON [dbo].[MktCampaignCustomTabFields]
GO
ALTER TABLE [dbo].[MktCampaignCustomTabFields] ADD CONSTRAINT [MktCampaignCustomTabFieldsPK] PRIMARY KEY NONCLUSTERED ([CampaignID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
