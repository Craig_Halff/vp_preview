CREATE TABLE [dbo].[CFGSync]
(
[PKey] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AddNewClient] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGSync_N__AddNe__7960274C] DEFAULT ('N'),
[AddNewCLAddress] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGSync_N__AddNe__7A544B85] DEFAULT ('N'),
[EIPassword] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EIAuthType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefCLType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefCLRelationship] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Param1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Param2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Param3] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GroupwareSvr] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGSync] ADD CONSTRAINT [CFGSyncPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
