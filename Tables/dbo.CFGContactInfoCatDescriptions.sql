CREATE TABLE [dbo].[CFGContactInfoCatDescriptions]
(
[Code] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Category] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGContactI__Seq__6D44692A] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGContactInfoCatDescriptions] ADD CONSTRAINT [CFGcontactInfoCatDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
