CREATE TABLE [dbo].[RPUnit]
(
[UnitID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Unit] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[UntCostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__UntCo__7F0FAA58] DEFAULT ((0)),
[UntBillRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__UntBi__0003CE91] DEFAULT ((0)),
[BaselineUntQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__Basel__00F7F2CA] DEFAULT ((0)),
[BaselineUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__Basel__01EC1703] DEFAULT ((0)),
[BaselineUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__Basel__02E03B3C] DEFAULT ((0)),
[PlannedUntQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__Plann__03D45F75] DEFAULT ((0)),
[PlannedUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__Plann__04C883AE] DEFAULT ((0)),
[PlannedUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__Plann__05BCA7E7] DEFAULT ((0)),
[PctCompleteUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__PctCo__06B0CC20] DEFAULT ((0)),
[PctCompleteUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__PctCo__07A4F059] DEFAULT ((0)),
[PlannedDirUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__Plann__08991492] DEFAULT ((0)),
[PlannedDirUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__Plann__098D38CB] DEFAULT ((0)),
[WeightedUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__Weigh__0A815D04] DEFAULT ((0)),
[WeightedUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__Weigh__0B75813D] DEFAULT ((0)),
[DirectAcctFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPUnit_Ne__Direc__0C69A576] DEFAULT ('N'),
[UntRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__UntRe__0D5DC9AF] DEFAULT ((0)),
[SortSeq] [smallint] NOT NULL CONSTRAINT [DF__RPUnit_Ne__SortS__0E51EDE8] DEFAULT ((0)),
[BaselineStart] [datetime] NULL,
[BaselineFinish] [datetime] NULL,
[LabParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPUnit_Ne__LabPa__0F461221] DEFAULT ('N'),
[ExpParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPUnit_Ne__ExpPa__103A365A] DEFAULT ('N'),
[ConParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPUnit_Ne__ConPa__112E5A93] DEFAULT ('N'),
[UntParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPUnit_Ne__UntPa__12227ECC] DEFAULT ('N'),
[ConVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPUnit_Ne__ConVS__1316A305] DEFAULT ('N'),
[ExpVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPUnit_Ne__ExpVS__140AC73E] DEFAULT ('N'),
[LabVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPUnit_Ne__LabVS__14FEEB77] DEFAULT ('N'),
[UntVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPUnit_Ne__UntVS__15F30FB0] DEFAULT ('Y'),
[BaselineDirUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__Basel__16E733E9] DEFAULT ((0)),
[BaselineDirUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__Basel__17DB5822] DEFAULT ((0)),
[UnitTable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[JTDUntQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__JTDUn__18CF7C5B] DEFAULT ((0)),
[JTDUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__JTDUn__19C3A094] DEFAULT ((0)),
[JTDUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPUnit_Ne__JTDUn__1AB7C4CD] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPUnit] ADD CONSTRAINT [RPUnitPK] PRIMARY KEY NONCLUSTERED ([UnitID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [RPUnitPlanIDIDX] ON [dbo].[RPUnit] ([PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPUnitTaskIDIDX] ON [dbo].[RPUnit] ([TaskID]) ON [PRIMARY]
GO
