CREATE TABLE [dbo].[EQICBooks]
(
[RecordID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EquipmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BookCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DepMethod] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UsefulLifeYr] [int] NOT NULL CONSTRAINT [DF__EQICBooks__Usefu__5EB71E08] DEFAULT ((0)),
[AcquisitionDate] [datetime] NULL,
[InServiceDate] [datetime] NULL,
[BusinessUsePercent] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EQICBooks__Busin__5FAB4241] DEFAULT ((0)),
[AdditionalCostInformation] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdditionalCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EQICBooks__Addit__609F667A] DEFAULT ((0)),
[SalvageValue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EQICBooks__Salva__61938AB3] DEFAULT ((0)),
[FirstYearDepreciation] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EQICBooks__First__6287AEEC] DEFAULT ((0)),
[Section179Deductions] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EQICBooks__Secti__637BD325] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EQICBooks] ADD CONSTRAINT [EQICBooksPK] PRIMARY KEY CLUSTERED ([RecordID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
