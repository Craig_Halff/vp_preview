CREATE TABLE [dbo].[CCG_EI_ConfigStageFlows]
(
[StageFlow] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Stage] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StageOrder] [int] NOT NULL,
[StageFlowDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_ConfigStageFlows] ADD CONSTRAINT [PK_CCG_EI_ConfigStageFlows] PRIMARY KEY CLUSTERED ([StageFlow], [Stage]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Defines which Stages belong in each Stage Flow', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStageFlows', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stage (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStageFlows', 'COLUMN', N'Stage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of the Stage Flow', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStageFlows', 'COLUMN', N'StageFlow'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Description for stage flow, used in reporting / informational only', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStageFlows', 'COLUMN', N'StageFlowDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The order in which this Stage appears in the list of Stages', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStageFlows', 'COLUMN', N'StageOrder'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Status of the StageFlow - [A]ctive / [I]nactive', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStageFlows', 'COLUMN', N'Status'
GO
