CREATE TABLE [dbo].[DataSourceExportAudit]
(
[ExportAuditID] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RowKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExportID] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DataSourceID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExportDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DataSourceExportAudit] ADD CONSTRAINT [DataSourceExportAuditPK] PRIMARY KEY CLUSTERED ([ExportAuditID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [DataSourceExportAuditExportIDIDX] ON [dbo].[DataSourceExportAudit] ([ExportID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [DataSourceExportAuditRowKeyIDX] ON [dbo].[DataSourceExportAudit] ([RowKey], [DataSourceID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
