CREATE TABLE [dbo].[CFGSetupStatus]
(
[ConfigurationID] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Touched] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGSetupS__Touch__5FD7C178] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGSetupStatus] ADD CONSTRAINT [CFGSetupStatusPK] PRIMARY KEY CLUSTERED ([ConfigurationID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
