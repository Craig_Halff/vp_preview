CREATE TABLE [dbo].[POVoucherDetail]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PODetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReceivePKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VoucheredAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POVoucher__Vouch__17B06F99] DEFAULT ((0)),
[VoucheredQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POVoucher__Vouch__18A493D2] DEFAULT ((0)),
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POVoucher__TaxAm__1998B80B] DEFAULT ((0)),
[ShipAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POVoucher__ShipA__1A8CDC44] DEFAULT ((0)),
[MiscAmount1] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POVoucher__MiscA__1B81007D] DEFAULT ((0)),
[MiscAmount2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POVoucher__MiscA__1C7524B6] DEFAULT ((0)),
[MiscAmount3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POVoucher__MiscA__1D6948EF] DEFAULT ((0)),
[MiscAmount4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POVoucher__MiscA__1E5D6D28] DEFAULT ((0)),
[MiscAmount5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POVoucher__MiscA__1F519161] DEFAULT ((0)),
[PaymentExchangeRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__POVoucher__Payme__2045B59A] DEFAULT ((0)),
[PaymentExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tax2Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tax2Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POVoucher__Tax2A__2139D9D3] DEFAULT ((0)),
[CompoundTax] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POVoucher__Compo__222DFE0C] DEFAULT ('N'),
[SelectedPeriod] [int] NOT NULL CONSTRAINT [DF__POVoucher__Selec__23222245] DEFAULT ((0)),
[SelectedPostSeq] [int] NOT NULL CONSTRAINT [DF__POVoucher__Selec__2416467E] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POVoucherDetail] ADD CONSTRAINT [POVoucherDetailPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [POVoucherDetailPODetailPKeyIDX] ON [dbo].[POVoucherDetail] ([PODetailPKey]) ON [PRIMARY]
GO
