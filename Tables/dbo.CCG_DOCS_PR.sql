CREATE TABLE [dbo].[CCG_DOCS_PR]
(
[FILEID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Invoice] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilePath] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDescription] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[Category] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_CCG_DOCS_PR_Category] DEFAULT ('ADD_DOCS'),
[StorageSeq] [int] NULL,
[StoragePathOrID] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevisionSeq] [int] NOT NULL CONSTRAINT [DF__CCG_DOCS___Revis__19627EF6] DEFAULT ((0)),
[FileState] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileSize] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_DOCS_PR] ADD CONSTRAINT [PK_CCG_DOCS_PR] PRIMARY KEY CLUSTERED ([FILEID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_CCG_DOCS_PR_WBS1] ON [dbo].[CCG_DOCS_PR] ([WBS1]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
