CREATE TABLE [dbo].[SEReport]
(
[Role] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Custom] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Folder] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Report] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccessAllColumns] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEReport___Acces__325A4434] DEFAULT ('N'),
[AccessAllGroups] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEReport___Acces__334E686D] DEFAULT ('N'),
[ViewOnly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEReport___ViewO__34428CA6] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_SEReport]
      ON [dbo].[SEReport]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SEReport'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Report],121),'Role',CONVERT(NVARCHAR(2000),[Role],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Report],121),'Custom',CONVERT(NVARCHAR(2000),[Custom],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Report],121),'Folder',CONVERT(NVARCHAR(2000),[Folder],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Report],121),'Report',CONVERT(NVARCHAR(2000),[Report],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Report],121),'AccessAllColumns',CONVERT(NVARCHAR(2000),[AccessAllColumns],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Report],121),'AccessAllGroups',CONVERT(NVARCHAR(2000),[AccessAllGroups],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Report],121),'ViewOnly',CONVERT(NVARCHAR(2000),[ViewOnly],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_SEReport]
      ON [dbo].[SEReport]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SEReport'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Report],121),'Role',NULL,CONVERT(NVARCHAR(2000),[Role],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Report],121),'Custom',NULL,CONVERT(NVARCHAR(2000),[Custom],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Report],121),'Folder',NULL,CONVERT(NVARCHAR(2000),[Folder],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Report],121),'Report',NULL,CONVERT(NVARCHAR(2000),[Report],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Report],121),'AccessAllColumns',NULL,CONVERT(NVARCHAR(2000),[AccessAllColumns],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Report],121),'AccessAllGroups',NULL,CONVERT(NVARCHAR(2000),[AccessAllGroups],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Report],121),'ViewOnly',NULL,CONVERT(NVARCHAR(2000),[ViewOnly],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_SEReport]
      ON [dbo].[SEReport]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SEReport'
    
      If UPDATE([Role])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Report],121),'Role',
      CONVERT(NVARCHAR(2000),DELETED.[Role],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Role],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[Custom] = DELETED.[Custom] AND INSERTED.[Folder] = DELETED.[Folder] AND INSERTED.[Report] = DELETED.[Report] AND 
		(
			(
				INSERTED.[Role] Is Null And
				DELETED.[Role] Is Not Null
			) Or
			(
				INSERTED.[Role] Is Not Null And
				DELETED.[Role] Is Null
			) Or
			(
				INSERTED.[Role] !=
				DELETED.[Role]
			)
		) 
		END		
		
      If UPDATE([Custom])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Report],121),'Custom',
      CONVERT(NVARCHAR(2000),DELETED.[Custom],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Custom],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[Custom] = DELETED.[Custom] AND INSERTED.[Folder] = DELETED.[Folder] AND INSERTED.[Report] = DELETED.[Report] AND 
		(
			(
				INSERTED.[Custom] Is Null And
				DELETED.[Custom] Is Not Null
			) Or
			(
				INSERTED.[Custom] Is Not Null And
				DELETED.[Custom] Is Null
			) Or
			(
				INSERTED.[Custom] !=
				DELETED.[Custom]
			)
		) 
		END		
		
      If UPDATE([Folder])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Report],121),'Folder',
      CONVERT(NVARCHAR(2000),DELETED.[Folder],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Folder],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[Custom] = DELETED.[Custom] AND INSERTED.[Folder] = DELETED.[Folder] AND INSERTED.[Report] = DELETED.[Report] AND 
		(
			(
				INSERTED.[Folder] Is Null And
				DELETED.[Folder] Is Not Null
			) Or
			(
				INSERTED.[Folder] Is Not Null And
				DELETED.[Folder] Is Null
			) Or
			(
				INSERTED.[Folder] !=
				DELETED.[Folder]
			)
		) 
		END		
		
      If UPDATE([Report])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Report],121),'Report',
      CONVERT(NVARCHAR(2000),DELETED.[Report],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Report],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[Custom] = DELETED.[Custom] AND INSERTED.[Folder] = DELETED.[Folder] AND INSERTED.[Report] = DELETED.[Report] AND 
		(
			(
				INSERTED.[Report] Is Null And
				DELETED.[Report] Is Not Null
			) Or
			(
				INSERTED.[Report] Is Not Null And
				DELETED.[Report] Is Null
			) Or
			(
				INSERTED.[Report] !=
				DELETED.[Report]
			)
		) 
		END		
		
      If UPDATE([AccessAllColumns])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Report],121),'AccessAllColumns',
      CONVERT(NVARCHAR(2000),DELETED.[AccessAllColumns],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccessAllColumns],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[Custom] = DELETED.[Custom] AND INSERTED.[Folder] = DELETED.[Folder] AND INSERTED.[Report] = DELETED.[Report] AND 
		(
			(
				INSERTED.[AccessAllColumns] Is Null And
				DELETED.[AccessAllColumns] Is Not Null
			) Or
			(
				INSERTED.[AccessAllColumns] Is Not Null And
				DELETED.[AccessAllColumns] Is Null
			) Or
			(
				INSERTED.[AccessAllColumns] !=
				DELETED.[AccessAllColumns]
			)
		) 
		END		
		
      If UPDATE([AccessAllGroups])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Report],121),'AccessAllGroups',
      CONVERT(NVARCHAR(2000),DELETED.[AccessAllGroups],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccessAllGroups],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[Custom] = DELETED.[Custom] AND INSERTED.[Folder] = DELETED.[Folder] AND INSERTED.[Report] = DELETED.[Report] AND 
		(
			(
				INSERTED.[AccessAllGroups] Is Null And
				DELETED.[AccessAllGroups] Is Not Null
			) Or
			(
				INSERTED.[AccessAllGroups] Is Not Null And
				DELETED.[AccessAllGroups] Is Null
			) Or
			(
				INSERTED.[AccessAllGroups] !=
				DELETED.[AccessAllGroups]
			)
		) 
		END		
		
      If UPDATE([ViewOnly])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Custom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Folder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Report],121),'ViewOnly',
      CONVERT(NVARCHAR(2000),DELETED.[ViewOnly],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ViewOnly],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[Custom] = DELETED.[Custom] AND INSERTED.[Folder] = DELETED.[Folder] AND INSERTED.[Report] = DELETED.[Report] AND 
		(
			(
				INSERTED.[ViewOnly] Is Null And
				DELETED.[ViewOnly] Is Not Null
			) Or
			(
				INSERTED.[ViewOnly] Is Not Null And
				DELETED.[ViewOnly] Is Null
			) Or
			(
				INSERTED.[ViewOnly] !=
				DELETED.[ViewOnly]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[SEReport] ADD CONSTRAINT [SEReportPK] PRIMARY KEY NONCLUSTERED ([Role], [Custom], [Folder], [Report]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
