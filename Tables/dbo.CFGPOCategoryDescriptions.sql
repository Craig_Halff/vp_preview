CREATE TABLE [dbo].[CFGPOCategoryDescriptions]
(
[Category] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPOCategoryDescriptions] ADD CONSTRAINT [CFGPOCategoryDescriptionsPK] PRIMARY KEY CLUSTERED ([Category], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
