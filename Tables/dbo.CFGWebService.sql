CREATE TABLE [dbo].[CFGWebService]
(
[WebServiceID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NodeID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WebServiceURL] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WebServiceMethod] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WebServiceDLL] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sequence] [int] NOT NULL CONSTRAINT [DF__CFGWebSer__Seque__5F6B4B1F] DEFAULT ((0)),
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGWebService] ADD CONSTRAINT [CFGWebServicePK] PRIMARY KEY CLUSTERED ([WebServiceID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
