CREATE TABLE [dbo].[jeDetailCustomFields]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[jeDetailCustomFields] ADD CONSTRAINT [jeDetailCustomFieldsPK] PRIMARY KEY NONCLUSTERED ([Batch], [RefNo], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
