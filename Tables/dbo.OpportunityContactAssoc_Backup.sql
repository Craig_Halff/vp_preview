CREATE TABLE [dbo].[OpportunityContactAssoc_Backup]
(
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Role] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoleDescription] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrimaryInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Opportuni__Prima__04B2A9BE] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OpportunityContactAssoc_Backup] ADD CONSTRAINT [OpportunityContactAssocPK] PRIMARY KEY NONCLUSTERED ([OpportunityID], [ContactID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
