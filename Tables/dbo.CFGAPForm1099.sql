CREATE TABLE [dbo].[CFGAPForm1099]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FederalEIN] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayerAddress1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayerAddress2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayerCity] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayerState] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayerZIPCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Minimum1099Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGAPForm__Minim__7CF0C10E] DEFAULT ((600)),
[TransmitterControlCode] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransmitterMediaNumber] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPhone] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPhoneExt] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactEMail] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReplacementAlphaCharacter] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayerNameControl] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayerOfficeCode] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastFilingIndicator] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReplacementFileIndicator] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TestFileIndicator] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReplacementFileName] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayerPhone] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[T5018SubmissionReferenceID] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[T5018ContactName] [nvarchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[T5018ContactPhoneNumber] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[T5018ContactPhoneExt] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[T5018ContactEmail] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[T5018TransmitterNumber] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[T5018TransmitterTypeCode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[T5018ReportTypeCode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Options] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAPForm1099] ADD CONSTRAINT [CFGAPForm1099PK] PRIMARY KEY CLUSTERED ([Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
