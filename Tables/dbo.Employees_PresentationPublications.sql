CREATE TABLE [dbo].[Employees_PresentationPublications]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustPresPubDate] [datetime] NULL,
[CustPresPubTitle] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPresPubPublisher] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPresPubEvent] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPresPubCoAuthor] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_Employees_PresentationPublications]
      ON [dbo].[Employees_PresentationPublications]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Employees_PresentationPublications'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Employee',CONVERT(NVARCHAR(2000),[Employee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Seq',CONVERT(NVARCHAR(2000),[Seq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustPresPubDate',CONVERT(NVARCHAR(2000),[CustPresPubDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustPresPubTitle',CONVERT(NVARCHAR(2000),[CustPresPubTitle],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustPresPubPublisher',CONVERT(NVARCHAR(2000),[CustPresPubPublisher],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustPresPubEvent',CONVERT(NVARCHAR(2000),[CustPresPubEvent],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustPresPubCoAuthor',CONVERT(NVARCHAR(2000),[CustPresPubCoAuthor],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_Employees_PresentationPublications] ON [dbo].[Employees_PresentationPublications]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_Employees_PresentationPublications]
      ON [dbo].[Employees_PresentationPublications]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Employees_PresentationPublications'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Employee',NULL,CONVERT(NVARCHAR(2000),[Employee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',NULL,CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustPresPubDate',NULL,CONVERT(NVARCHAR(2000),[CustPresPubDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustPresPubTitle',NULL,CONVERT(NVARCHAR(2000),[CustPresPubTitle],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustPresPubPublisher',NULL,CONVERT(NVARCHAR(2000),[CustPresPubPublisher],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustPresPubEvent',NULL,CONVERT(NVARCHAR(2000),[CustPresPubEvent],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustPresPubCoAuthor',NULL,CONVERT(NVARCHAR(2000),[CustPresPubCoAuthor],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_Employees_PresentationPublications] ON [dbo].[Employees_PresentationPublications]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_Employees_PresentationPublications]
      ON [dbo].[Employees_PresentationPublications]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Employees_PresentationPublications'
    
      If UPDATE([Employee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Employee',
      CONVERT(NVARCHAR(2000),DELETED.[Employee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Employee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Employee] Is Null And
				DELETED.[Employee] Is Not Null
			) Or
			(
				INSERTED.[Employee] Is Not Null And
				DELETED.[Employee] Is Null
			) Or
			(
				INSERTED.[Employee] !=
				DELETED.[Employee]
			)
		) 
		END		
		
      If UPDATE([Seq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',
      CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] Is Null And
				DELETED.[Seq] Is Not Null
			) Or
			(
				INSERTED.[Seq] Is Not Null And
				DELETED.[Seq] Is Null
			) Or
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
		
      If UPDATE([CustPresPubDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustPresPubDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustPresPubDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPresPubDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustPresPubDate] Is Null And
				DELETED.[CustPresPubDate] Is Not Null
			) Or
			(
				INSERTED.[CustPresPubDate] Is Not Null And
				DELETED.[CustPresPubDate] Is Null
			) Or
			(
				INSERTED.[CustPresPubDate] !=
				DELETED.[CustPresPubDate]
			)
		) 
		END		
		
      If UPDATE([CustPresPubTitle])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustPresPubTitle',
      CONVERT(NVARCHAR(2000),DELETED.[CustPresPubTitle],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPresPubTitle],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustPresPubTitle] Is Null And
				DELETED.[CustPresPubTitle] Is Not Null
			) Or
			(
				INSERTED.[CustPresPubTitle] Is Not Null And
				DELETED.[CustPresPubTitle] Is Null
			) Or
			(
				INSERTED.[CustPresPubTitle] !=
				DELETED.[CustPresPubTitle]
			)
		) 
		END		
		
      If UPDATE([CustPresPubPublisher])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustPresPubPublisher',
      CONVERT(NVARCHAR(2000),DELETED.[CustPresPubPublisher],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPresPubPublisher],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustPresPubPublisher] Is Null And
				DELETED.[CustPresPubPublisher] Is Not Null
			) Or
			(
				INSERTED.[CustPresPubPublisher] Is Not Null And
				DELETED.[CustPresPubPublisher] Is Null
			) Or
			(
				INSERTED.[CustPresPubPublisher] !=
				DELETED.[CustPresPubPublisher]
			)
		) 
		END		
		
      If UPDATE([CustPresPubEvent])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustPresPubEvent',
      CONVERT(NVARCHAR(2000),DELETED.[CustPresPubEvent],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPresPubEvent],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustPresPubEvent] Is Null And
				DELETED.[CustPresPubEvent] Is Not Null
			) Or
			(
				INSERTED.[CustPresPubEvent] Is Not Null And
				DELETED.[CustPresPubEvent] Is Null
			) Or
			(
				INSERTED.[CustPresPubEvent] !=
				DELETED.[CustPresPubEvent]
			)
		) 
		END		
		
      If UPDATE([CustPresPubCoAuthor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustPresPubCoAuthor',
      CONVERT(NVARCHAR(2000),DELETED.[CustPresPubCoAuthor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPresPubCoAuthor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustPresPubCoAuthor] Is Null And
				DELETED.[CustPresPubCoAuthor] Is Not Null
			) Or
			(
				INSERTED.[CustPresPubCoAuthor] Is Not Null And
				DELETED.[CustPresPubCoAuthor] Is Null
			) Or
			(
				INSERTED.[CustPresPubCoAuthor] !=
				DELETED.[CustPresPubCoAuthor]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_Employees_PresentationPublications] ON [dbo].[Employees_PresentationPublications]
GO
ALTER TABLE [dbo].[Employees_PresentationPublications] ADD CONSTRAINT [Employees_PresentationPublicationsPK] PRIMARY KEY CLUSTERED ([Employee], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
