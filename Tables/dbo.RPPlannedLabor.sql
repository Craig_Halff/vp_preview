CREATE TABLE [dbo].[RPPlannedLabor]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AssignmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodCount] [int] NOT NULL CONSTRAINT [DF__RPPlanned__Perio__75BB4A48] DEFAULT ((1)),
[PeriodHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Perio__76AF6E81] DEFAULT ((0)),
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Perio__77A392BA] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Perio__7897B6F3] DEFAULT ((0)),
[CostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__CostR__798BDB2C] DEFAULT ((0)),
[BillingRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Billi__7A7FFF65] DEFAULT ((0)),
[PeriodRev] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Perio__7B74239E] DEFAULT ((0)),
[HardBooked] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPPlanned__HardB__7C6847D7] DEFAULT ('N'),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPPlannedLabor] ADD CONSTRAINT [RPPlannedLaborPK] PRIMARY KEY NONCLUSTERED ([TimePhaseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPPlannedLaborPTAIDX] ON [dbo].[RPPlannedLabor] ([PlanID], [TaskID], [AssignmentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPPlannedLaborAll2IDX] ON [dbo].[RPPlannedLabor] ([PlanID], [TaskID], [StartDate], [EndDate], [AssignmentID], [PeriodHrs], [PeriodCost], [PeriodBill], [PeriodRev]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPPlannedLaborAllIDX] ON [dbo].[RPPlannedLabor] ([PlanID], [TimePhaseID], [TaskID], [AssignmentID], [StartDate], [EndDate], [PeriodHrs], [PeriodCost], [PeriodBill], [PeriodRev], [CostRate], [BillingRate]) ON [PRIMARY]
GO
