CREATE TABLE [dbo].[CFGClientStatusData]
(
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGClientStatusData__Status__DefA] DEFAULT ('A')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGClientStatusData] ADD CONSTRAINT [CFGClientStatusDataPK] PRIMARY KEY CLUSTERED ([Status]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
