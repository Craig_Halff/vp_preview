CREATE TABLE [dbo].[billAddDetail]
(
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecdSeq] [int] NOT NULL CONSTRAINT [DF__billAddDe__RecdS__49862BA1] DEFAULT ((0)),
[BillWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sequence] [int] NOT NULL CONSTRAINT [DF__billAddDe__Seque__4A7A4FDA] DEFAULT ((0)),
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Computation] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billAddDe__Amoun__4B6E7413] DEFAULT ((0)),
[Basis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billAddDe__Basis__4C62984C] DEFAULT ((0)),
[Markup] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billAddDe__Marku__4D56BC85] DEFAULT ((0)),
[Section] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Retainer] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__billAddDe__Retai__4E4AE0BE] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[billAddDetail] ADD CONSTRAINT [billAddDetailPK] PRIMARY KEY NONCLUSTERED ([Invoice], [MainWBS1], [RecdSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
