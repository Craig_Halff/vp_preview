CREATE TABLE [dbo].[tempINV]
(
[arsh] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[co_code] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[org_code] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[acct_code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[prj_code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[doc_nbr] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InvDate] [datetime] NOT NULL,
[PaidPeriod] [int] NOT NULL,
[Hdr_Db] [numeric] (28, 9) NOT NULL,
[Hdr_Cr] [numeric] (28, 9) NOT NULL,
[ar_subsid_skey] [int] NOT NULL,
[arsd] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[seq_code] [smallint] NULL,
[sys_doc_type_code] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[accounting_period] [int] NULL,
[TransDate] [datetime] NULL,
[Detail_Db] [numeric] (28, 9) NULL,
[Detail_Cr] [numeric] (28, 9) NULL,
[ihs] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[inv_hist_sum_skey] [int] NULL,
[inv_amt_pc] [numeric] (28, 9) NULL,
[retainage_pc] [numeric] (28, 9) NULL,
[ihad] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[accumulate_code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phase_code] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phase_Amt] [numeric] (28, 9) NULL,
[tally] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DetailTotal] [numeric] (38, 9) NULL,
[jtd_billed] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[billed] [numeric] (38, 9) NULL,
[TieToJTD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
