CREATE TABLE [dbo].[HAI_RevenueImportException]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HAI_RevenueImportException] ADD CONSTRAINT [PK_HAI_RevenueImportException] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
