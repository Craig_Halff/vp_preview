CREATE TABLE [dbo].[FW_ReportCustomColumns]
(
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportFolder] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportType] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DataType] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Heading1] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Heading2] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Format] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Width] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__FW_Report__Width__75BA7EF1] DEFAULT ((0)),
[CondExp] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MainExp] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ElseExp] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalculationType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_ReportCustomColumns] ADD CONSTRAINT [FW_ReportCustomColumnsPK] PRIMARY KEY NONCLUSTERED ([ReportFolder], [ReportType], [Username], [Name]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
