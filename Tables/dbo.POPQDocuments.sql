CREATE TABLE [dbo].[POPQDocuments]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL,
[AssociateNew] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POPQDocum__Assoc__7D6F4E9D] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__POPQDocumen__Seq__7E6372D6] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POPQDocuments] ADD CONSTRAINT [POPQDocumentsPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
