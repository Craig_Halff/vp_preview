CREATE TABLE [dbo].[tkRevisionDetail]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EndDate] [datetime] NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Revision] [smallint] NOT NULL CONSTRAINT [DF__tkRevisio__Revis__431ABBE2] DEFAULT ((0)),
[RowNumber] [smallint] NOT NULL CONSTRAINT [DF__tkRevisio__RowNu__440EE01B] DEFAULT ((0)),
[Sequence] [int] NOT NULL CONSTRAINT [DF__tkRevisio__Seque__45030454] DEFAULT ((0)),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LaborCode] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillCategory] [smallint] NOT NULL CONSTRAINT [DF__tkRevisio__BillC__45F7288D] DEFAULT ((0)),
[TransDate] [datetime] NULL,
[ColumnName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OldValue] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewValue] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevisionDesc] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Locale] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTKUD01] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTKUD02] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__tkRevisio__CustT__1807D759] DEFAULT ((0)),
[CustTKUD03] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tkRevisionDetail] ADD CONSTRAINT [tkRevisionDetailPK] PRIMARY KEY NONCLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [tkRevisionDetailIDX] ON [dbo].[tkRevisionDetail] ([EndDate], [Employee], [Revision]) ON [PRIMARY]
GO
