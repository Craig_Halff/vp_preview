CREATE TABLE [dbo].[RecurrActivity]
(
[ActivityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Daily] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RecurrAct__Daily__44ABD28D] DEFAULT ('N'),
[Weekly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RecurrAct__Weekl__459FF6C6] DEFAULT ('N'),
[Monthly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RecurrAct__Month__46941AFF] DEFAULT ('N'),
[Yearly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RecurrAct__Yearl__47883F38] DEFAULT ('N'),
[DailyFreq] [smallint] NOT NULL CONSTRAINT [DF__RecurrAct__Daily__487C6371] DEFAULT ((0)),
[DailyWeekDay] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RecurrAct__Daily__497087AA] DEFAULT ('N'),
[WeeklyFreq] [smallint] NOT NULL CONSTRAINT [DF__RecurrAct__Weekl__4A64ABE3] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RecurrActivity] ADD CONSTRAINT [RecurrActivityPK] PRIMARY KEY NONCLUSTERED ([ActivityID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
