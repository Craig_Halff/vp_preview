CREATE TABLE [dbo].[WorkflowActionCreateItem]
(
[ActionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ItemType_UID] [nvarchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AssignTo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssignToDisplay] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeAssignToDisplay] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeAssignto] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowActionCreateItem] ADD CONSTRAINT [WorkflowActionCreateItemPK] PRIMARY KEY NONCLUSTERED ([ActionID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
