CREATE TABLE [dbo].[exDetailTax]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__exDetailT__TaxAm__55ECCDDD] DEFAULT ((0)),
[CompoundOnTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__exDetailTax__Seq__56E0F216] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[exDetailTax] ADD CONSTRAINT [exDetailTaxPK] PRIMARY KEY NONCLUSTERED ([Batch], [MasterPKey], [PKey], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
