CREATE TABLE [dbo].[tsMaster]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__tsMaster___Poste__6C1CD175] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__tsMaster_Ne__Seq__6D10F5AE] DEFAULT ((0)),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__tsMaster___Statu__6E0519E7] DEFAULT ('N'),
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RejectReason] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tsMaster] ADD CONSTRAINT [tsMasterPK] PRIMARY KEY NONCLUSTERED ([Batch], [Employee]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
