CREATE TABLE [dbo].[RPGridCell]
(
[CellID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RPGridID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CellOrder] [int] NOT NULL CONSTRAINT [DF__RPGridCel__CellO__210FBCA1] DEFAULT ((0)),
[CellType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCel__CellT__2203E0DA] DEFAULT ('N'),
[Row] [int] NOT NULL CONSTRAINT [DF__RPGridCell___Row__22F80513] DEFAULT ((0)),
[Rowspan] [int] NOT NULL CONSTRAINT [DF__RPGridCel__Rowsp__23EC294C] DEFAULT ((0)),
[Contents] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Width] [int] NOT NULL CONSTRAINT [DF__RPGridCel__Width__24E04D85] DEFAULT ((0)),
[Colspan] [int] NOT NULL CONSTRAINT [DF__RPGridCel__Colsp__25D471BE] DEFAULT ((0)),
[ScrollRegion] [int] NOT NULL CONSTRAINT [DF__RPGridCel__Scrol__26C895F7] DEFAULT ((0)),
[Data] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Visible] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCel__Visib__27BCBA30] DEFAULT ('N'),
[Formula] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Editable] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCel__Edita__28B0DE69] DEFAULT ('N'),
[DisplayOrder] [int] NOT NULL CONSTRAINT [DF__RPGridCel__Displ__29A502A2] DEFAULT ((-1)),
[DataType] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCel__DataT__2A9926DB] DEFAULT ('string'),
[Decimals] [smallint] NOT NULL CONSTRAINT [DF__RPGridCel__Decim__2B8D4B14] DEFAULT ((0)),
[EditStyle] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCel__EditS__2C816F4D] DEFAULT ('text'),
[Required] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCel__Requi__2D759386] DEFAULT ('N'),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPGridCell] ADD CONSTRAINT [RPGridCellPK] PRIMARY KEY NONCLUSTERED ([CellID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
