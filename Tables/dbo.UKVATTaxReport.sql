CREATE TABLE [dbo].[UKVATTaxReport]
(
[SessionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TimeFrame] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[CreateDate] [datetime] NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UKVATTaxReport] ADD CONSTRAINT [UKVATTaxReportPK] PRIMARY KEY NONCLUSTERED ([SessionID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
