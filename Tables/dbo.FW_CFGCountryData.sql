CREATE TABLE [dbo].[FW_CFGCountryData]
(
[ISOCountryCode] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGCou__Displ__6D8F4D44] DEFAULT ('C'),
[AddressFormat] [smallint] NOT NULL CONSTRAINT [DF__FW_CFGCou__Addre__6E83717D] DEFAULT ((1)),
[TaxAuthority] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGCountryData] ADD CONSTRAINT [FW_CFGCountryDataPK] PRIMARY KEY CLUSTERED ([ISOCountryCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
