CREATE TABLE [dbo].[MktCampaignOppAssoc_Backup]
(
[CampaignID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MktCampaignOppAssoc_Backup] ADD CONSTRAINT [MktCampaignOppAssocPK] PRIMARY KEY NONCLUSTERED ([CampaignID], [OpportunityID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
