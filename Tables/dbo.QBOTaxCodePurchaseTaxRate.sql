CREATE TABLE [dbo].[QBOTaxCodePurchaseTaxRate]
(
[QBOID] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QBOTaxCodePurchaseTaxRate] ADD CONSTRAINT [QBOTaxCodePurchaseTaxRatePK] PRIMARY KEY CLUSTERED ([QBOID], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
