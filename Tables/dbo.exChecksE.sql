CREATE TABLE [dbo].[exChecksE]
(
[Period] [int] NOT NULL CONSTRAINT [DF__exChecksE__Perio__36742284] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__exChecksE__PostS__376846BD] DEFAULT ((0)),
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__exChecksE_N__Seq__385C6AF6] DEFAULT ((0)),
[BankID] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Method] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__exChecksE__AmtPc__39508F2F] DEFAULT ((0)),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__exChecksE__Amoun__3A44B368] DEFAULT ((0)),
[SourceBankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckRefNo] [bigint] NOT NULL CONSTRAINT [DF__exChecksE__Check__3B38D7A1] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[exChecksE] ADD CONSTRAINT [exChecksEPK] PRIMARY KEY NONCLUSTERED ([Period], [PostSeq], [Employee], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
