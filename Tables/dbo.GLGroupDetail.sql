CREATE TABLE [dbo].[GLGroupDetail]
(
[TableNo] [smallint] NOT NULL CONSTRAINT [DF__GLGroupDe__Table__180F96F5] DEFAULT ((0)),
[GLGroup] [smallint] NOT NULL CONSTRAINT [DF__GLGroupDe__GLGro__1903BB2E] DEFAULT ((0)),
[StartAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EndAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GLGroupDetail] ADD CONSTRAINT [GLGroupDetailPK] PRIMARY KEY NONCLUSTERED ([TableNo], [GLGroup], [StartAccount], [EndAccount]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
