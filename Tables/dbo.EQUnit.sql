CREATE TABLE [dbo].[EQUnit]
(
[RecordID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EquipmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Unit] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UnitTable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EQUnit] ADD CONSTRAINT [EQUnitPK] PRIMARY KEY CLUSTERED ([RecordID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
