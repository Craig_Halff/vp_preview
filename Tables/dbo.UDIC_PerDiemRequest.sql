CREATE TABLE [dbo].[UDIC_PerDiemRequest]
(
[UDIC_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPayee] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPDStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFilePath] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEmployee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustRequestDate] [datetime] NULL CONSTRAINT [DF__UDIC_PerD__CustR__3C38B429] DEFAULT (getdate()),
[CustDueDate] [datetime] NULL CONSTRAINT [DF__UDIC_PerD__CustD__3D2CD862] DEFAULT (getdate()),
[CustNotes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustApprover] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCity] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPerDiemHotel] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_PerD__CustP__3E20FC9B] DEFAULT ((0)),
[CustPerDiemMeals] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_PerD__CustP__3F1520D4] DEFAULT ((0)),
[CustPerDiem] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_PerD__CustP__4009450D] DEFAULT ((0)),
[CustRequestedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_UDIC_PerDiemRequest]
      ON [dbo].[UDIC_PerDiemRequest]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_PerDiemRequest'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'UDIC_UID',CONVERT(NVARCHAR(2000),[UDIC_UID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustomCurrencyCode',CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustNumber',CONVERT(NVARCHAR(2000),[CustNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustPayee',CONVERT(NVARCHAR(2000),[CustPayee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustPDStatus',CONVERT(NVARCHAR(2000),[CustPDStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustFilePath',CONVERT(NVARCHAR(2000),[CustFilePath],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustEmployee',CONVERT(NVARCHAR(2000),DELETED.[CustEmployee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustEmployee = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustRequestDate',CONVERT(NVARCHAR(2000),[CustRequestDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustDueDate',CONVERT(NVARCHAR(2000),[CustDueDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustNotes',CONVERT(NVARCHAR(2000),[CustNotes],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustApprover',CONVERT(NVARCHAR(2000),DELETED.[CustApprover],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustApprover = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCity',CONVERT(NVARCHAR(2000),[CustCity],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustPerDiemHotel',CONVERT(NVARCHAR(2000),[CustPerDiemHotel],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustPerDiemMeals',CONVERT(NVARCHAR(2000),[CustPerDiemMeals],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustPerDiem',CONVERT(NVARCHAR(2000),[CustPerDiem],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustRequestedBy',CONVERT(NVARCHAR(2000),DELETED.[CustRequestedBy],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustRequestedBy = oldDesc.Employee

      
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_UDIC_PerDiemRequest]
      ON [dbo].[UDIC_PerDiemRequest]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_PerDiemRequest'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'UDIC_UID',NULL,CONVERT(NVARCHAR(2000),[UDIC_UID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustomCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustNumber',NULL,CONVERT(NVARCHAR(2000),[CustNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPayee',NULL,CONVERT(NVARCHAR(2000),[CustPayee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPDStatus',NULL,CONVERT(NVARCHAR(2000),[CustPDStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustFilePath',NULL,CONVERT(NVARCHAR(2000),[CustFilePath],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustEmployee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustEmployee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEmployee = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustRequestDate',NULL,CONVERT(NVARCHAR(2000),[CustRequestDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustDueDate',NULL,CONVERT(NVARCHAR(2000),[CustDueDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustNotes',NULL,CONVERT(NVARCHAR(2000),[CustNotes],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustApprover',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustApprover],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustApprover = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCity',NULL,CONVERT(NVARCHAR(2000),[CustCity],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPerDiemHotel',NULL,CONVERT(NVARCHAR(2000),[CustPerDiemHotel],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPerDiemMeals',NULL,CONVERT(NVARCHAR(2000),[CustPerDiemMeals],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPerDiem',NULL,CONVERT(NVARCHAR(2000),[CustPerDiem],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustRequestedBy',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustRequestedBy],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustRequestedBy = newDesc.Employee

     
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_UDIC_PerDiemRequest]
      ON [dbo].[UDIC_PerDiemRequest]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_PerDiemRequest'
    
      If UPDATE([UDIC_UID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'UDIC_UID',
      CONVERT(NVARCHAR(2000),DELETED.[UDIC_UID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UDIC_UID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[UDIC_UID] Is Null And
				DELETED.[UDIC_UID] Is Not Null
			) Or
			(
				INSERTED.[UDIC_UID] Is Not Null And
				DELETED.[UDIC_UID] Is Null
			) Or
			(
				INSERTED.[UDIC_UID] !=
				DELETED.[UDIC_UID]
			)
		) 
		END		
		
      If UPDATE([CustomCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustomCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustomCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustomCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustomCurrencyCode] Is Null And
				DELETED.[CustomCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] Is Not Null And
				DELETED.[CustomCurrencyCode] Is Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] !=
				DELETED.[CustomCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([CustNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustNumber',
      CONVERT(NVARCHAR(2000),DELETED.[CustNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustNumber] Is Null And
				DELETED.[CustNumber] Is Not Null
			) Or
			(
				INSERTED.[CustNumber] Is Not Null And
				DELETED.[CustNumber] Is Null
			) Or
			(
				INSERTED.[CustNumber] !=
				DELETED.[CustNumber]
			)
		) 
		END		
		
      If UPDATE([CustPayee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPayee',
      CONVERT(NVARCHAR(2000),DELETED.[CustPayee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPayee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustPayee] Is Null And
				DELETED.[CustPayee] Is Not Null
			) Or
			(
				INSERTED.[CustPayee] Is Not Null And
				DELETED.[CustPayee] Is Null
			) Or
			(
				INSERTED.[CustPayee] !=
				DELETED.[CustPayee]
			)
		) 
		END		
		
      If UPDATE([CustPDStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPDStatus',
      CONVERT(NVARCHAR(2000),DELETED.[CustPDStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPDStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustPDStatus] Is Null And
				DELETED.[CustPDStatus] Is Not Null
			) Or
			(
				INSERTED.[CustPDStatus] Is Not Null And
				DELETED.[CustPDStatus] Is Null
			) Or
			(
				INSERTED.[CustPDStatus] !=
				DELETED.[CustPDStatus]
			)
		) 
		END		
		
      If UPDATE([CustFilePath])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustFilePath',
      CONVERT(NVARCHAR(2000),DELETED.[CustFilePath],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFilePath],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustFilePath] Is Null And
				DELETED.[CustFilePath] Is Not Null
			) Or
			(
				INSERTED.[CustFilePath] Is Not Null And
				DELETED.[CustFilePath] Is Null
			) Or
			(
				INSERTED.[CustFilePath] !=
				DELETED.[CustFilePath]
			)
		) 
		END		
		
     If UPDATE([CustEmployee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustEmployee',
     CONVERT(NVARCHAR(2000),DELETED.[CustEmployee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustEmployee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustEmployee] Is Null And
				DELETED.[CustEmployee] Is Not Null
			) Or
			(
				INSERTED.[CustEmployee] Is Not Null And
				DELETED.[CustEmployee] Is Null
			) Or
			(
				INSERTED.[CustEmployee] !=
				DELETED.[CustEmployee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEmployee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEmployee = newDesc.Employee
		END		
		
      If UPDATE([CustRequestDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustRequestDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustRequestDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRequestDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustRequestDate] Is Null And
				DELETED.[CustRequestDate] Is Not Null
			) Or
			(
				INSERTED.[CustRequestDate] Is Not Null And
				DELETED.[CustRequestDate] Is Null
			) Or
			(
				INSERTED.[CustRequestDate] !=
				DELETED.[CustRequestDate]
			)
		) 
		END		
		
      If UPDATE([CustDueDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustDueDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustDueDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustDueDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustDueDate] Is Null And
				DELETED.[CustDueDate] Is Not Null
			) Or
			(
				INSERTED.[CustDueDate] Is Not Null And
				DELETED.[CustDueDate] Is Null
			) Or
			(
				INSERTED.[CustDueDate] !=
				DELETED.[CustDueDate]
			)
		) 
		END		
		
      If UPDATE([CustNotes])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustNotes',
      CONVERT(NVARCHAR(2000),DELETED.[CustNotes],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustNotes],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustNotes] Is Null And
				DELETED.[CustNotes] Is Not Null
			) Or
			(
				INSERTED.[CustNotes] Is Not Null And
				DELETED.[CustNotes] Is Null
			) Or
			(
				INSERTED.[CustNotes] !=
				DELETED.[CustNotes]
			)
		) 
		END		
		
     If UPDATE([CustApprover])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustApprover',
     CONVERT(NVARCHAR(2000),DELETED.[CustApprover],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustApprover],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustApprover] Is Null And
				DELETED.[CustApprover] Is Not Null
			) Or
			(
				INSERTED.[CustApprover] Is Not Null And
				DELETED.[CustApprover] Is Null
			) Or
			(
				INSERTED.[CustApprover] !=
				DELETED.[CustApprover]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustApprover = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustApprover = newDesc.Employee
		END		
		
      If UPDATE([CustCity])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCity',
      CONVERT(NVARCHAR(2000),DELETED.[CustCity],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCity],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCity] Is Null And
				DELETED.[CustCity] Is Not Null
			) Or
			(
				INSERTED.[CustCity] Is Not Null And
				DELETED.[CustCity] Is Null
			) Or
			(
				INSERTED.[CustCity] !=
				DELETED.[CustCity]
			)
		) 
		END		
		
      If UPDATE([CustPerDiemHotel])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPerDiemHotel',
      CONVERT(NVARCHAR(2000),DELETED.[CustPerDiemHotel],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPerDiemHotel],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustPerDiemHotel] Is Null And
				DELETED.[CustPerDiemHotel] Is Not Null
			) Or
			(
				INSERTED.[CustPerDiemHotel] Is Not Null And
				DELETED.[CustPerDiemHotel] Is Null
			) Or
			(
				INSERTED.[CustPerDiemHotel] !=
				DELETED.[CustPerDiemHotel]
			)
		) 
		END		
		
      If UPDATE([CustPerDiemMeals])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPerDiemMeals',
      CONVERT(NVARCHAR(2000),DELETED.[CustPerDiemMeals],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPerDiemMeals],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustPerDiemMeals] Is Null And
				DELETED.[CustPerDiemMeals] Is Not Null
			) Or
			(
				INSERTED.[CustPerDiemMeals] Is Not Null And
				DELETED.[CustPerDiemMeals] Is Null
			) Or
			(
				INSERTED.[CustPerDiemMeals] !=
				DELETED.[CustPerDiemMeals]
			)
		) 
		END		
		
      If UPDATE([CustPerDiem])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPerDiem',
      CONVERT(NVARCHAR(2000),DELETED.[CustPerDiem],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPerDiem],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustPerDiem] Is Null And
				DELETED.[CustPerDiem] Is Not Null
			) Or
			(
				INSERTED.[CustPerDiem] Is Not Null And
				DELETED.[CustPerDiem] Is Null
			) Or
			(
				INSERTED.[CustPerDiem] !=
				DELETED.[CustPerDiem]
			)
		) 
		END		
		
     If UPDATE([CustRequestedBy])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustRequestedBy',
     CONVERT(NVARCHAR(2000),DELETED.[CustRequestedBy],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustRequestedBy],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustRequestedBy] Is Null And
				DELETED.[CustRequestedBy] Is Not Null
			) Or
			(
				INSERTED.[CustRequestedBy] Is Not Null And
				DELETED.[CustRequestedBy] Is Null
			) Or
			(
				INSERTED.[CustRequestedBy] !=
				DELETED.[CustRequestedBy]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustRequestedBy = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustRequestedBy = newDesc.Employee
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[UDIC_PerDiemRequest] ADD CONSTRAINT [UDIC_PerDiemRequestPK] PRIMARY KEY CLUSTERED ([UDIC_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
