CREATE TABLE [dbo].[CustomProposalPages]
(
[PageID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomPropID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__CustomPropo__Seq__506073BE] DEFAULT ((0)),
[Properties] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomProposalPages] ADD CONSTRAINT [CustomProposalPagesPK] PRIMARY KEY CLUSTERED ([PageID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
