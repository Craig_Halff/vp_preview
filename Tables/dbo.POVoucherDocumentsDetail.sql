CREATE TABLE [dbo].[POVoucherDocumentsDetail]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POVoucherDocumentsDetail] ADD CONSTRAINT [POVoucherDocumentsDetailPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [DetailPKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
