CREATE TABLE [dbo].[FW_PerfManagement]
(
[PMServer] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Port] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Username] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Password] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AuthType] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ImagePath] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrgLevelsInUse] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_PerfMan__PKey__0C804ABD] DEFAULT ('Dltk')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_PerfManagement] ADD CONSTRAINT [FW_PerfManagementPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
