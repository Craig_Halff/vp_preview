CREATE TABLE [dbo].[EMDirectDepositDetail]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__EMDirectDep__Seq__5013F8F6] DEFAULT ((0)),
[Period] [int] NOT NULL CONSTRAINT [DF__EMDirectD__Perio__51081D2F] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__EMDirectD__PostS__51FC4168] DEFAULT ((0)),
[BankID] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Method] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMDirectD__AmtPc__52F065A1] DEFAULT ((0)),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMDirectD__Amoun__53E489DA] DEFAULT ((0)),
[Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMDirectD__Overr__54D8AE13] DEFAULT ('N'),
[SourceBankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckRefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EMDirectDepositDetail] ADD CONSTRAINT [EMDirectDepositDetailPK] PRIMARY KEY NONCLUSTERED ([Employee], [Seq], [Period], [PostSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [EMDirectDepositDetailVoidCheckIDX] ON [dbo].[EMDirectDepositDetail] ([SourceBankCode], [CheckRefNo]) ON [PRIMARY]
GO
