CREATE TABLE [dbo].[RPDependency]
(
[DependencyID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PredecessorID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DependencyType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LagValue] [int] NOT NULL CONSTRAINT [DF__RPDepende__LagVa__6D90189B] DEFAULT ((0)),
[LagScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPDepende__LagSc__6E843CD4] DEFAULT ('d'),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPDependency] ADD CONSTRAINT [RPDependencyPK] PRIMARY KEY NONCLUSTERED ([DependencyID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPDependencyPlanTaskPredIDX] ON [dbo].[RPDependency] ([PlanID], [TaskID], [PredecessorID]) ON [PRIMARY]
GO
