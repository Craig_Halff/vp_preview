CREATE TABLE [dbo].[IQItem]
(
[Pkey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IQID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IQItem] ADD CONSTRAINT [IQItemPK] PRIMARY KEY CLUSTERED ([Pkey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
