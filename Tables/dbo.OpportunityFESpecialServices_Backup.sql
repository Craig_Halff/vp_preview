CREATE TABLE [dbo].[OpportunityFESpecialServices_Backup]
(
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PhaseCode] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportunity__Fee__6504F43B] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OpportunityFESpecialServices_Backup] ADD CONSTRAINT [OpportunityFESpecialServicesPK] PRIMARY KEY NONCLUSTERED ([OpportunityID], [PhaseCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
