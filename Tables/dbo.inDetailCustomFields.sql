CREATE TABLE [dbo].[inDetailCustomFields]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[inDetailCustomFields] ADD CONSTRAINT [inDetailCustomFieldsPK] PRIMARY KEY NONCLUSTERED ([Batch], [Invoice], [WBS1], [WBS2], [WBS3], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
