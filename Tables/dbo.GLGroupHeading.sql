CREATE TABLE [dbo].[GLGroupHeading]
(
[GLGroup] [smallint] NOT NULL CONSTRAINT [DF__GLGroupHe__GLGro__6A86975B] DEFAULT ((0)),
[TableNo] [smallint] NOT NULL CONSTRAINT [DF__GLGroupHe__Table__6B7ABB94] DEFAULT ((0)),
[ExcludeTotal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__GLGroupHe__Exclu__6C6EDFCD] DEFAULT ('N'),
[ShowDetail] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__GLGroupHe__ShowD__6D630406] DEFAULT ('R'),
[SubTotal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__GLGroupHe__SubTo__6E57283F] DEFAULT ('R'),
[SortOrder] [smallint] NOT NULL CONSTRAINT [DF__GLGroupHe__SortO__6F4B4C78] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GLGroupHeading] ADD CONSTRAINT [GLGroupHeadingPK] PRIMARY KEY CLUSTERED ([GLGroup], [TableNo]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
