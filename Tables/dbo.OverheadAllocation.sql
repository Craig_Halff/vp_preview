CREATE TABLE [dbo].[OverheadAllocation]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__OverheadAll__Seq__1E3D7197] DEFAULT ((0)),
[Period] [int] NOT NULL CONSTRAINT [DF__OverheadA__Perio__1F3195D0] DEFAULT ((0)),
[Pass] [smallint] NOT NULL CONSTRAINT [DF__OverheadAl__Pass__2025BA09] DEFAULT ((0)),
[OrgToDistribute] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DistributionTarget] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Base] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OverheadAl__Base__2119DE42] DEFAULT ((0)),
[Expenses] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OverheadA__Expen__220E027B] DEFAULT ((0)),
[Revenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OverheadA__Reven__230226B4] DEFAULT ((0)),
[Received] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OverheadA__Recei__23F64AED] DEFAULT ((0)),
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OverheadAl__Rate__24EA6F26] DEFAULT ((0)),
[Overhead] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OverheadA__Overh__25DE935F] DEFAULT ((0)),
[Variance] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OverheadA__Varia__26D2B798] DEFAULT ((0)),
[Method] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Basis] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OverheadAllocation] ADD CONSTRAINT [OverheadAllocationPK] PRIMARY KEY CLUSTERED ([Company], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
