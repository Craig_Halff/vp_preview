CREATE TABLE [dbo].[CFGSuffixDescriptions]
(
[Code] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Suffix] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGSuffixDe__Seq__7777DEDA] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGSuffixDescriptions] ADD CONSTRAINT [CFGSuffixDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
