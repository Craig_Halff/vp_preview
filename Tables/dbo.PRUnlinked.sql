CREATE TABLE [dbo].[PRUnlinked]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LongName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Stage] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubLevel] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AvailableForCRM] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReadyForApproval] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReadyForProcessing] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProjectCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
