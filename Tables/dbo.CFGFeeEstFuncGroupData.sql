CREATE TABLE [dbo].[CFGFeeEstFuncGroupData]
(
[Code] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Disabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGFeeEst__Disab__19E20577] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGFeeEstFuncGroupData] ADD CONSTRAINT [CFGFeeEstFuncGroupDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
