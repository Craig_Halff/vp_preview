CREATE TABLE [dbo].[PNCalendarInterval]
(
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NOT NULL,
[AccordionFormatID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EndDate] [datetime] NOT NULL,
[PeriodScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNCalenda__Perio__63DBC5D8] DEFAULT ('w'),
[PeriodCount] [int] NOT NULL CONSTRAINT [DF__PNCalenda__Perio__64CFEA11] DEFAULT ((1)),
[NumWorkingDays] [int] NULL,
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PNCalendarInterval] ADD CONSTRAINT [PNCalendarIntervalPK] PRIMARY KEY NONCLUSTERED ([PlanID], [AccordionFormatID], [StartDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNCalendarIntervalAccordionFormatIDIDX] ON [dbo].[PNCalendarInterval] ([AccordionFormatID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNCalendarIntervalPlanIDStartDateEndDateIDX] ON [dbo].[PNCalendarInterval] ([PlanID], [StartDate], [EndDate]) ON [PRIMARY]
GO
