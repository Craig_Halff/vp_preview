CREATE TABLE [dbo].[POFinalPrint]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Screen] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POFinalPrint] ADD CONSTRAINT [POFinalPrintPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [UserName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
