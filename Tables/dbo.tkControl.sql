CREATE TABLE [dbo].[tkControl]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EndDate] [datetime] NOT NULL,
[StartDate] [datetime] NULL,
[Closed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__tkControl__Close__258A58FB] DEFAULT ('N'),
[PeriodNumber] [smallint] NOT NULL CONSTRAINT [DF__tkControl__Perio__267E7D34] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tkControl] ADD CONSTRAINT [tkControlPK] PRIMARY KEY NONCLUSTERED ([Company], [EndDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
