CREATE TABLE [dbo].[Employees_Certifications]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustCertificationName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCertificationAgency] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCertificationEffective] [datetime] NULL,
[CustCertificationExpiration] [datetime] NULL,
[CustCertificationNo] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCertificationState] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_Employees_Certifications]
      ON [dbo].[Employees_Certifications]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Employees_Certifications'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Employee',CONVERT(NVARCHAR(2000),[Employee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Seq',CONVERT(NVARCHAR(2000),[Seq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCertificationName',CONVERT(NVARCHAR(2000),[CustCertificationName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCertificationAgency',CONVERT(NVARCHAR(2000),[CustCertificationAgency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCertificationEffective',CONVERT(NVARCHAR(2000),[CustCertificationEffective],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCertificationExpiration',CONVERT(NVARCHAR(2000),[CustCertificationExpiration],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCertificationNo',CONVERT(NVARCHAR(2000),[CustCertificationNo],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCertificationState',CONVERT(NVARCHAR(2000),[CustCertificationState],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_Employees_Certifications] ON [dbo].[Employees_Certifications]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_Employees_Certifications]
      ON [dbo].[Employees_Certifications]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Employees_Certifications'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Employee',NULL,CONVERT(NVARCHAR(2000),[Employee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',NULL,CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCertificationName',NULL,CONVERT(NVARCHAR(2000),[CustCertificationName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCertificationAgency',NULL,CONVERT(NVARCHAR(2000),[CustCertificationAgency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCertificationEffective',NULL,CONVERT(NVARCHAR(2000),[CustCertificationEffective],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCertificationExpiration',NULL,CONVERT(NVARCHAR(2000),[CustCertificationExpiration],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCertificationNo',NULL,CONVERT(NVARCHAR(2000),[CustCertificationNo],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCertificationState',NULL,CONVERT(NVARCHAR(2000),[CustCertificationState],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_Employees_Certifications] ON [dbo].[Employees_Certifications]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_Employees_Certifications]
      ON [dbo].[Employees_Certifications]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Employees_Certifications'
    
      If UPDATE([Employee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Employee',
      CONVERT(NVARCHAR(2000),DELETED.[Employee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Employee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Employee] Is Null And
				DELETED.[Employee] Is Not Null
			) Or
			(
				INSERTED.[Employee] Is Not Null And
				DELETED.[Employee] Is Null
			) Or
			(
				INSERTED.[Employee] !=
				DELETED.[Employee]
			)
		) 
		END		
		
      If UPDATE([Seq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',
      CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] Is Null And
				DELETED.[Seq] Is Not Null
			) Or
			(
				INSERTED.[Seq] Is Not Null And
				DELETED.[Seq] Is Null
			) Or
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
		
      If UPDATE([CustCertificationName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCertificationName',
      CONVERT(NVARCHAR(2000),DELETED.[CustCertificationName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCertificationName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCertificationName] Is Null And
				DELETED.[CustCertificationName] Is Not Null
			) Or
			(
				INSERTED.[CustCertificationName] Is Not Null And
				DELETED.[CustCertificationName] Is Null
			) Or
			(
				INSERTED.[CustCertificationName] !=
				DELETED.[CustCertificationName]
			)
		) 
		END		
		
      If UPDATE([CustCertificationAgency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCertificationAgency',
      CONVERT(NVARCHAR(2000),DELETED.[CustCertificationAgency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCertificationAgency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCertificationAgency] Is Null And
				DELETED.[CustCertificationAgency] Is Not Null
			) Or
			(
				INSERTED.[CustCertificationAgency] Is Not Null And
				DELETED.[CustCertificationAgency] Is Null
			) Or
			(
				INSERTED.[CustCertificationAgency] !=
				DELETED.[CustCertificationAgency]
			)
		) 
		END		
		
      If UPDATE([CustCertificationEffective])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCertificationEffective',
      CONVERT(NVARCHAR(2000),DELETED.[CustCertificationEffective],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCertificationEffective],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCertificationEffective] Is Null And
				DELETED.[CustCertificationEffective] Is Not Null
			) Or
			(
				INSERTED.[CustCertificationEffective] Is Not Null And
				DELETED.[CustCertificationEffective] Is Null
			) Or
			(
				INSERTED.[CustCertificationEffective] !=
				DELETED.[CustCertificationEffective]
			)
		) 
		END		
		
      If UPDATE([CustCertificationExpiration])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCertificationExpiration',
      CONVERT(NVARCHAR(2000),DELETED.[CustCertificationExpiration],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCertificationExpiration],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCertificationExpiration] Is Null And
				DELETED.[CustCertificationExpiration] Is Not Null
			) Or
			(
				INSERTED.[CustCertificationExpiration] Is Not Null And
				DELETED.[CustCertificationExpiration] Is Null
			) Or
			(
				INSERTED.[CustCertificationExpiration] !=
				DELETED.[CustCertificationExpiration]
			)
		) 
		END		
		
      If UPDATE([CustCertificationNo])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCertificationNo',
      CONVERT(NVARCHAR(2000),DELETED.[CustCertificationNo],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCertificationNo],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCertificationNo] Is Null And
				DELETED.[CustCertificationNo] Is Not Null
			) Or
			(
				INSERTED.[CustCertificationNo] Is Not Null And
				DELETED.[CustCertificationNo] Is Null
			) Or
			(
				INSERTED.[CustCertificationNo] !=
				DELETED.[CustCertificationNo]
			)
		) 
		END		
		
      If UPDATE([CustCertificationState])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCertificationState',
      CONVERT(NVARCHAR(2000),DELETED.[CustCertificationState],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCertificationState],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCertificationState] Is Null And
				DELETED.[CustCertificationState] Is Not Null
			) Or
			(
				INSERTED.[CustCertificationState] Is Not Null And
				DELETED.[CustCertificationState] Is Null
			) Or
			(
				INSERTED.[CustCertificationState] !=
				DELETED.[CustCertificationState]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_Employees_Certifications] ON [dbo].[Employees_Certifications]
GO
ALTER TABLE [dbo].[Employees_Certifications] ADD CONSTRAINT [Employees_CertificationsPK] PRIMARY KEY CLUSTERED ([Employee], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
