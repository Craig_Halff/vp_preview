CREATE TABLE [dbo].[ConsolidationBudgets]
(
[BudgetName] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportingGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoDistribute] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Consolida__AutoD__381C73D4] DEFAULT ('N'),
[CompoundPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Compo__3910980D] DEFAULT ((0)),
[CompoundAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Compo__3A04BC46] DEFAULT ((0)),
[AdjustPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Adjus__3AF8E07F] DEFAULT ((0)),
[AdjustAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Adjus__3BED04B8] DEFAULT ((0)),
[StartPeriod] [smallint] NOT NULL CONSTRAINT [DF__Consolida__Start__3CE128F1] DEFAULT ((0)),
[EndPeriod] [smallint] NOT NULL CONSTRAINT [DF__Consolida__EndPe__3DD54D2A] DEFAULT ((0)),
[BudgetYear] [smallint] NOT NULL CONSTRAINT [DF__Consolida__Budge__3EC97163] DEFAULT ((0)),
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConsolidationBudgets] ADD CONSTRAINT [ConsolidationBudgetsPK] PRIMARY KEY NONCLUSTERED ([BudgetName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
