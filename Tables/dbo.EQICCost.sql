CREATE TABLE [dbo].[EQICCost]
(
[RecordID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EquipmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CostType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EQICCost___CostT__674C6409] DEFAULT ('N'),
[LinkedPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncludeInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EQICCost___Inclu__68408842] DEFAULT ('N'),
[AutoEntry] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EQICCost___AutoE__6934AC7B] DEFAULT ('N'),
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EQICCost___Amoun__6A28D0B4] DEFAULT ((0)),
[Period] [int] NOT NULL CONSTRAINT [DF__EQICCost___Perio__6B1CF4ED] DEFAULT ((0)),
[Description] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VoucherPONumber] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LifeOfAsset] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EQICCost___LifeO__6C111926] DEFAULT ('Y'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EQICCost] ADD CONSTRAINT [EQICCostPK] PRIMARY KEY CLUSTERED ([RecordID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [EQICCostEquipIDPeriodIDX] ON [dbo].[EQICCost] ([EquipmentID], [Period]) ON [PRIMARY]
GO
