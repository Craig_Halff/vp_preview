CREATE TABLE [dbo].[CFGBillApprovalActionAlerts]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Language] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmailAlertSubject] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailAlertContent] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DashboardAlertSubject] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DashboardAlertContent] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGBillApprovalActionAlerts] ADD CONSTRAINT [CFGBillApprovalActionAlertsPK] PRIMARY KEY CLUSTERED ([Code], [Action], [Language]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
