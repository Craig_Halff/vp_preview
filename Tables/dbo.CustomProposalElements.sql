CREATE TABLE [dbo].[CustomProposalElements]
(
[ElementID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomPropID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PageID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ElementType] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ParentElementID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TemplateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecordID] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ColPos] [int] NOT NULL CONSTRAINT [DF__CustomPro__ColPo__47CB2DBD] DEFAULT ((0)),
[RowPos] [int] NOT NULL CONSTRAINT [DF__CustomPro__RowPo__48BF51F6] DEFAULT ((0)),
[ColWidth] [int] NOT NULL CONSTRAINT [DF__CustomPro__ColWi__49B3762F] DEFAULT ((0)),
[RowHeight] [int] NOT NULL CONSTRAINT [DF__CustomPro__RowHe__4AA79A68] DEFAULT ((0)),
[Text] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InfocenterArea] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ElementOptions] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[FileID] [uniqueidentifier] NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__CustomPropo__Seq__79B78F0C] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomProposalElements] ADD CONSTRAINT [CustomProposalElementsPK] PRIMARY KEY CLUSTERED ([ElementID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
