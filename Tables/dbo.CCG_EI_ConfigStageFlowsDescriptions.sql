CREATE TABLE [dbo].[CCG_EI_ConfigStageFlowsDescriptions]
(
[StageFlow] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Stage] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StageFlowDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_ConfigStageFlowsDescriptions] ADD CONSTRAINT [PK_CCG_EI_ConfigStageFlowsDescriptions] PRIMARY KEY CLUSTERED ([StageFlow], [Stage], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Contains descriptions for Stage Flows as used for multi-language companies', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStageFlowsDescriptions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Stage (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStageFlowsDescriptions', 'COLUMN', N'Stage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Name of the Stage Flow (not multilanguage)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStageFlowsDescriptions', 'COLUMN', N'StageFlow'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The display label to use for this Stage Flow and this culture code', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStageFlowsDescriptions', 'COLUMN', N'StageFlowDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The UI culture code of this label', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStageFlowsDescriptions', 'COLUMN', N'UICultureName'
GO
