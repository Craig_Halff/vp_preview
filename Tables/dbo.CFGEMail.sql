CREATE TABLE [dbo].[CFGEMail]
(
[PKey] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Host] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Port] [smallint] NOT NULL CONSTRAINT [DF__CFGEMail_N__Port__4E8AD7E0] DEFAULT ((0)),
[DefaultSender] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultRecipient] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthUserName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthPassword] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UseSenderForReply] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEMail___UseSe__4F7EFC19] DEFAULT ('Y'),
[ReplyTo] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LicenseNotification] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__CFGEMail___Licen__50732052] DEFAULT ('AccountManager@deltek.com'),
[TLS] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEMail_Ne__TLS__5167448B] DEFAULT ('N'),
[PrefixEmailSender] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEMail___Prefi__525B68C4] DEFAULT ('Y'),
[EmailChunkSize] [int] NOT NULL CONSTRAINT [DF__CFGEMail___Email__534F8CFD] DEFAULT ((0)),
[MaxEmailSize] [int] NOT NULL CONSTRAINT [DF__CFGEMail___MaxEm__5443B136] DEFAULT ((0)),
[DisposeMailSender] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEMail___Dispo__5537D56F] DEFAULT ('N'),
[EmailLog] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEMail__EmailL__6FF9E5FF] DEFAULT ('Y'),
[EmailLogRetainDays] [int] NOT NULL CONSTRAINT [DF__CFGEMail__EmailL__70EE0A38] DEFAULT ((30))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGEMail] ADD CONSTRAINT [CFGEMailPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
