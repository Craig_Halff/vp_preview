CREATE TABLE [dbo].[DashLayoutPart]
(
[DashLayoutID] [int] NOT NULL CONSTRAINT [DF__DashLayou__DashL__0F6F63FC] DEFAULT ((0)),
[PartID] [int] NOT NULL CONSTRAINT [DF__DashLayou__PartI__10638835] DEFAULT ((0)),
[Col] [smallint] NOT NULL CONSTRAINT [DF__DashLayoutP__Col__1157AC6E] DEFAULT ((0)),
[Pos] [smallint] NOT NULL CONSTRAINT [DF__DashLayoutP__Pos__124BD0A7] DEFAULT ((0)),
[State] [smallint] NOT NULL CONSTRAINT [DF__DashLayou__State__133FF4E0] DEFAULT ((0)),
[Prefs] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DashLayoutPart] ADD CONSTRAINT [DashLayoutPartPK] PRIMARY KEY CLUSTERED ([DashLayoutID], [PartID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
