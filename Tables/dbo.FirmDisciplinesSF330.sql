CREATE TABLE [dbo].[FirmDisciplinesSF330]
(
[FirmID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Discipline] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__FirmDiscipl__Seq__6DC4576E] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FirmDisciplinesSF330] ADD CONSTRAINT [FirmDisciplinesSF330PK] PRIMARY KEY NONCLUSTERED ([FirmID], [Discipline]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
