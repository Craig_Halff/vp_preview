CREATE TABLE [dbo].[CFGProbabilityDescriptions]
(
[Probability] [smallint] NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGProbabil__Seq__15725615] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGProbabilityDescriptions] ADD CONSTRAINT [CFGProbabilityDescriptionsPK] PRIMARY KEY CLUSTERED ([Probability], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
