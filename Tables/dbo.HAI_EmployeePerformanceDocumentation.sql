CREATE TABLE [dbo].[HAI_EmployeePerformanceDocumentation]
(
[EmployeeNumber] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReviewerEmployeeNumber] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReviewerEmployeeName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OverallAttitude] [int] NULL,
[Teamwork] [int] NULL,
[DemonstrateOwnership] [int] NULL,
[TechnicalSkills] [int] NULL,
[QualityOfWork] [int] NULL,
[MentorTrainOthers] [int] NULL,
[ClientRelationships] [int] NULL,
[MeetDeadlines] [int] NULL,
[Comments] [nvarchar] (140) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HAI_EmployeePerformanceDocumentation] ADD CONSTRAINT [HAI_EmployeePerformanceDocumentation_EmployeeNumber] PRIMARY KEY CLUSTERED ([EmployeeNumber]) ON [PRIMARY]
GO
