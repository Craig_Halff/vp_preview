CREATE TABLE [dbo].[CustomRepOptsData]
(
[ReportName] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColName] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColDataType] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ColSeq] [smallint] NOT NULL CONSTRAINT [DF__CustomRep__ColSe__5AFB9BBD] DEFAULT ((0)),
[ColDisplayWidth] [smallint] NOT NULL CONSTRAINT [DF__CustomRep__ColDi__5BEFBFF6] DEFAULT ((0)),
[LimitToList] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CustomRep__Limit__5CE3E42F] DEFAULT ('N'),
[ColTotal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CustomRep__ColTo__5DD80868] DEFAULT ('N'),
[CurrencyMask] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CustomRep__Curre__5ECC2CA1] DEFAULT ('N'),
[decimalPlaces] [smallint] NOT NULL CONSTRAINT [DF__CustomRep__decim__5FC050DA] DEFAULT ((0)),
[MinValue] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaxValue] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MinDate] [datetime] NULL,
[MaxDate] [datetime] NULL,
[AcctRequired] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CustomRep__AcctR__60B47513] DEFAULT ('N'),
[MktRequired] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CustomRep__MktRe__61A8994C] DEFAULT ('N'),
[HTMLHeight] [smallint] NOT NULL CONSTRAINT [DF__CustomRep__HTMLH__629CBD85] DEFAULT ((0)),
[LayoutColumn] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CustomRep__Layou__6390E1BE] DEFAULT ('L'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomRepOptsData] ADD CONSTRAINT [CustomRepOptsDataPK] PRIMARY KEY NONCLUSTERED ([ReportName], [TabID], [ColName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
