CREATE TABLE [dbo].[_HAI_TempGroups]
(
[OriginalCode] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalDescription] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdatedCode] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdatedDescription] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
