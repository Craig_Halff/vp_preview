CREATE TABLE [dbo].[CFGOppRevenueTemplateMaster_Backup]
(
[TemplateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TemplateName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGOppRevenueTemplateMaster_Backup] ADD CONSTRAINT [CFGOppRevenueTemplateMasterPK] PRIMARY KEY CLUSTERED ([TemplateID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
