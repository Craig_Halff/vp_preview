CREATE TABLE [dbo].[CCG_Email_History]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[MessageSeq] [int] NOT NULL,
[MessageGroupSeq] [int] NULL,
[SourceApplication] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Sender] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ToList] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CCList] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BCCList] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Subject] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Body] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaxDelay] [int] NULL,
[Priority] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionDateTime] [datetime] NOT NULL CONSTRAINT [DF_CCG_Email_History_ActionDateTime] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_Email_History] ADD CONSTRAINT [PK_CCG_Email_History] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
