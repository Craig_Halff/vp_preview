CREATE TABLE [dbo].[EMSkills]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Skill] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SkillLevel] [smallint] NOT NULL CONSTRAINT [DF__EMSkills___Skill__52514723] DEFAULT ((0)),
[PrimaryInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMSkills___Prima__53456B5C] DEFAULT ('N'),
[SkillUsage] [smallint] NOT NULL CONSTRAINT [DF__EMSkills___Skill__54398F95] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_EMSkills]
      ON [dbo].[EMSkills]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMSkills'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Skill],121),'Employee',CONVERT(NVARCHAR(2000),DELETED.[Employee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Employee = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Skill],121),'Skill',CONVERT(NVARCHAR(2000),DELETED.[Skill],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGEMSkill as oldDesc  on DELETED.Skill = oldDesc.Code

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Skill],121),'SkillLevel',CONVERT(NVARCHAR(2000),DELETED.[SkillLevel],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGEMSkillLevel as oldDesc  on DELETED.SkillLevel = oldDesc.Code

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Skill],121),'PrimaryInd',CONVERT(NVARCHAR(2000),[PrimaryInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Skill],121),'SkillUsage',CONVERT(NVARCHAR(2000),[SkillUsage],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_EMSkills] ON [dbo].[EMSkills]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_EMSkills]
      ON [dbo].[EMSkills]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMSkills'
    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Skill],121),'Employee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Employee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Employee = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Skill],121),'Skill',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Skill],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGEMSkill as newDesc  on INSERTED.Skill = newDesc.Code

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Skill],121),'SkillLevel',NULL,CONVERT(NVARCHAR(2000),INSERTED.[SkillLevel],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGEMSkillLevel as newDesc  on INSERTED.SkillLevel = newDesc.Code

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Skill],121),'PrimaryInd',NULL,CONVERT(NVARCHAR(2000),[PrimaryInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Skill],121),'SkillUsage',NULL,CONVERT(NVARCHAR(2000),[SkillUsage],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_EMSkills] ON [dbo].[EMSkills]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_EMSkills]
      ON [dbo].[EMSkills]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMSkills'
    
     If UPDATE([Employee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Skill],121),'Employee',
     CONVERT(NVARCHAR(2000),DELETED.[Employee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Employee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Skill] = DELETED.[Skill] AND 
		(
			(
				INSERTED.[Employee] Is Null And
				DELETED.[Employee] Is Not Null
			) Or
			(
				INSERTED.[Employee] Is Not Null And
				DELETED.[Employee] Is Null
			) Or
			(
				INSERTED.[Employee] !=
				DELETED.[Employee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Employee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Employee = newDesc.Employee
		END		
		
     If UPDATE([Skill])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Skill],121),'Skill',
     CONVERT(NVARCHAR(2000),DELETED.[Skill],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Skill],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Skill] = DELETED.[Skill] AND 
		(
			(
				INSERTED.[Skill] Is Null And
				DELETED.[Skill] Is Not Null
			) Or
			(
				INSERTED.[Skill] Is Not Null And
				DELETED.[Skill] Is Null
			) Or
			(
				INSERTED.[Skill] !=
				DELETED.[Skill]
			)
		) left join CFGEMSkill as oldDesc  on DELETED.Skill = oldDesc.Code  left join  CFGEMSkill as newDesc  on INSERTED.Skill = newDesc.Code
		END		
		
     If UPDATE([SkillLevel])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Skill],121),'SkillLevel',
     CONVERT(NVARCHAR(2000),DELETED.[SkillLevel],121),
     CONVERT(NVARCHAR(2000),INSERTED.[SkillLevel],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Skill] = DELETED.[Skill] AND 
		(
			(
				INSERTED.[SkillLevel] Is Null And
				DELETED.[SkillLevel] Is Not Null
			) Or
			(
				INSERTED.[SkillLevel] Is Not Null And
				DELETED.[SkillLevel] Is Null
			) Or
			(
				INSERTED.[SkillLevel] !=
				DELETED.[SkillLevel]
			)
		) left join CFGEMSkillLevel as oldDesc  on DELETED.SkillLevel = oldDesc.Code  left join  CFGEMSkillLevel as newDesc  on INSERTED.SkillLevel = newDesc.Code
		END		
		
      If UPDATE([PrimaryInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Skill],121),'PrimaryInd',
      CONVERT(NVARCHAR(2000),DELETED.[PrimaryInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PrimaryInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Skill] = DELETED.[Skill] AND 
		(
			(
				INSERTED.[PrimaryInd] Is Null And
				DELETED.[PrimaryInd] Is Not Null
			) Or
			(
				INSERTED.[PrimaryInd] Is Not Null And
				DELETED.[PrimaryInd] Is Null
			) Or
			(
				INSERTED.[PrimaryInd] !=
				DELETED.[PrimaryInd]
			)
		) 
		END		
		
      If UPDATE([SkillUsage])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Skill],121),'SkillUsage',
      CONVERT(NVARCHAR(2000),DELETED.[SkillUsage],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SkillUsage],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Skill] = DELETED.[Skill] AND 
		(
			(
				INSERTED.[SkillUsage] Is Null And
				DELETED.[SkillUsage] Is Not Null
			) Or
			(
				INSERTED.[SkillUsage] Is Not Null And
				DELETED.[SkillUsage] Is Null
			) Or
			(
				INSERTED.[SkillUsage] !=
				DELETED.[SkillUsage]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_EMSkills] ON [dbo].[EMSkills]
GO
ALTER TABLE [dbo].[EMSkills] ADD CONSTRAINT [EMSkillsPK] PRIMARY KEY NONCLUSTERED ([Employee], [Skill]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
