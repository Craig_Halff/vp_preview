CREATE TABLE [dbo].[SE]
(
[Role] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SystemAdmin] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__SystemAd__2FB2E1B3] DEFAULT ('N'),
[AccountingType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__Accounti__30A705EC] DEFAULT ('N'),
[MarketingType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__Marketin__319B2A25] DEFAULT ('N'),
[AccessAllNavNodes] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AccessAl__328F4E5E] DEFAULT ('N'),
[AccessAllReports] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AccessAl__33837297] DEFAULT ('N'),
[AccessAllTemplates] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AccessAl__347796D0] DEFAULT ('N'),
[AccessAllDashParts] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AccessAl__356BBB09] DEFAULT ('N'),
[AccessAllTabs] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AccessAl__365FDF42] DEFAULT ('N'),
[SeeCostRates] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__SeeCostR__3754037B] DEFAULT ('N'),
[ProjectBudgetAccess] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayrollCheckRec] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__PayrollC__384827B4] DEFAULT ('N'),
[VoucherReviewModify] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__VoucherR__393C4BED] DEFAULT ('N'),
[ProcessClosedPeriods] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__ProcessC__3A307026] DEFAULT ('N'),
[BillingRow] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__BillingR__3B24945F] DEFAULT ('N'),
[BillingGroupRow] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__BillingG__3C18B898] DEFAULT ('N'),
[InteractiveBillingAccess] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteInvoices] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__DeleteIn__3D0CDCD1] DEFAULT ('N'),
[ProcessPriorPeriods] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__ProcessP__3E01010A] DEFAULT ('N'),
[TKRow] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__TKRow__3EF52543] DEFAULT ('N'),
[EKRow] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__EKRow__3FE9497C] DEFAULT ('N'),
[DataEntryRow] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__DataEntr__40DD6DB5] DEFAULT ('N'),
[ARCommentReview] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__ARCommen__41D191EE] DEFAULT ('N'),
[ARCommentUpdate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__ARCommen__42C5B627] DEFAULT ('N'),
[OrgRowLevelWhere] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcessQueuePriority] [smallint] NOT NULL CONSTRAINT [DF__SE_New__ProcessQ__43B9DA60] DEFAULT ((0)),
[AccessAllPrinters] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AccessAl__44ADFE99] DEFAULT ('N'),
[AccessAllLookupFields] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AccessAl__45A222D2] DEFAULT ('N'),
[RPChangeRate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPChange__4696470B] DEFAULT ('Y'),
[RPChangeDefWBSMapping] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPChange__478A6B44] DEFAULT ('Y'),
[RPApproveAssignment] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPApprov__487E8F7D] DEFAULT ('Y'),
[RPSubstitutePeople] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPSubsti__4972B3B6] DEFAULT ('Y'),
[RPChangeBudgetType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPChange__4A66D7EF] DEFAULT ('Y'),
[RPRateMethodTable] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPRateMe__4B5AFC28] DEFAULT ('Y'),
[RPChangePercComplete] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPChange__4C4F2061] DEFAULT ('Y'),
[RPChangeBudget] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPChange__4D43449A] DEFAULT ('Y'),
[RPChangeCompensation] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPChange__4E3768D3] DEFAULT ('Y'),
[RPChangeConsultantFees] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPChange__4F2B8D0C] DEFAULT ('Y'),
[RPReimbursableAllowance] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPReimbu__501FB145] DEFAULT ('Y'),
[SeeBurdenRates] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__SeeBurde__5113D57E] DEFAULT ('N'),
[AllowPriorW2Quarter] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AllowPri__5207F9B7] DEFAULT ('N'),
[ReportAdmin] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__ReportAd__52FC1DF0] DEFAULT ('N'),
[AccessAllTransTypes] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AccessAl__53F04229] DEFAULT ('N'),
[RPChangeRevFormula] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPChange__54E46662] DEFAULT ('Y'),
[AllowAddWebParts] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AllowAdd__55D88A9B] DEFAULT ('N'),
[AllowCalcFields] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AllowCal__56CCAED4] DEFAULT ('N'),
[AllowAddDashParts] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AllowAdd__57C0D30D] DEFAULT ('N'),
[AccessAll20DashParts] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AccessAl__58B4F746] DEFAULT ('Y'),
[AccessAllCompanies] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AccessAl__59A91B7F] DEFAULT ('N'),
[RPChangeOverheadPct] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPChange__5A9D3FB8] DEFAULT ('Y'),
[POPrintPO] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__POPrintP__5B9163F1] DEFAULT ('N'),
[PODeleteOpenPO] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__PODelete__5C85882A] DEFAULT ('N'),
[POReleasePO] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__POReleas__5D79AC63] DEFAULT ('N'),
[POUnreceive] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__POUnrece__5E6DD09C] DEFAULT ('N'),
[POClosePO] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__POCloseP__5F61F4D5] DEFAULT ('N'),
[PODeleteOpenPR] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__PODelete__6056190E] DEFAULT ('N'),
[FinalBatchBillingAccess] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__FinalBat__614A3D47] DEFAULT ('N'),
[AllowSQLWhereInLookup] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AllowSQL__623E6180] DEFAULT ('N'),
[RPUpdatePlanLabor] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPUpdate__633285B9] DEFAULT ('N'),
[AccessDocMgt] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AccessDo__651ACE2B] DEFAULT ('N'),
[RPEMRow] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPEMRow__660EF264] DEFAULT ('N'),
[SeeCostRatesIgnoreProfitCalc] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__SeeCostR__6703169D] DEFAULT ('N'),
[SeeBurdenRatesIgnoreProfitCalc] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__SeeBurde__67F73AD6] DEFAULT ('N'),
[AllowIntercompanyReceipts] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AllowInt__68EB5F0F] DEFAULT ('N'),
[InteractiveBillingModify] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__Interact__69DF8348] DEFAULT ('N'),
[InteractiveBillingDelete] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__Interact__6AD3A781] DEFAULT ('N'),
[InteractiveBillingWriteOff] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__Interact__6BC7CBBA] DEFAULT ('N'),
[InteractiveBillingFinalAccess] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__Interact__6CBBEFF3] DEFAULT ('N'),
[InteractiveBillingHold] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__Interact__6DB0142C] DEFAULT ('N'),
[InteractiveBillingInsert] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__Interact__6EA43865] DEFAULT ('N'),
[InteractiveBillingTransfer] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__Interact__6F985C9E] DEFAULT ('N'),
[InteractiveBillingEditedInvoices] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__Interact__708C80D7] DEFAULT ('Y'),
[EmailTemplateAdmin] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__EmailTem__7180A510] DEFAULT ('N'),
[RPHardBooking] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPHardBo__7274C949] DEFAULT ('N'),
[ContactDownload] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientDownload] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActivityDownload] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OpportunityDownload] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPChangeAssignments] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPChange__7368ED82] DEFAULT ('N'),
[AllowAcrossICLookup] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AllowAcr__745D11BB] DEFAULT ('N'),
[AllowReprocessIntercompanyBilling] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AllowRep__755135F4] DEFAULT ('N'),
[EmployeeSelfServiceInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__Employee__76455A2D] DEFAULT ('N'),
[AllowChangeDisableLogin] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AllowCha__77397E66] DEFAULT ('N'),
[AllowChangePassword] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AllowCha__782DA29F] DEFAULT ('N'),
[AllowChangeSupportDocument] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AllowCha__7921C6D8] DEFAULT ('N'),
[Navigator] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__Navigato__7A15EB11] DEFAULT ('N'),
[NavEmpWorkspace] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavEmpWo__7B0A0F4A] DEFAULT ('N'),
[NavPMWorkspace] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavPMWor__7BFE3383] DEFAULT ('N'),
[NavTimesheet] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavTimes__7CF257BC] DEFAULT ('N'),
[NavDashboard] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavDashb__7DE67BF5] DEFAULT ('N'),
[NavPlanning] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavPlann__7EDAA02E] DEFAULT ('N'),
[NavLabor] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavLabor__7FCEC467] DEFAULT ('N'),
[NavConsultant] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavConsu__00C2E8A0] DEFAULT ('N'),
[NavExpense] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavExpen__01B70CD9] DEFAULT ('N'),
[NavContract] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavContr__02AB3112] DEFAULT ('N'),
[NavUseProjectAccess] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavUsePr__039F554B] DEFAULT ('N'),
[NavUsePlanningAccess] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavUsePl__04937984] DEFAULT ('N'),
[NavUseProjectView] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavUsePr__05879DBD] DEFAULT ('N'),
[NavUseProjectUpdate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavUsePr__067BC1F6] DEFAULT ('N'),
[NavUsePlanningView] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavUsePl__076FE62F] DEFAULT ('N'),
[NavUsePlanningUpdate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavUsePl__08640A68] DEFAULT ('N'),
[NavExpRpt] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavExpRp__09582EA1] DEFAULT ('N'),
[NavInvoice] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavInvoi__0A4C52DA] DEFAULT ('N'),
[NavCRMWorkspace] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__NavCRMWo__0B407713] DEFAULT ('N'),
[RPChangeCompensationExp] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPChange__0C349B4C] DEFAULT ('N'),
[RPReimbursableAllowanceCon] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__RPReimbu__0D28BF85] DEFAULT ('N'),
[AllowLookupLimitOverride] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AllowLoo__0E1CE3BE] DEFAULT ('N'),
[AllowEditPriorDepreciation] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AllowEdi__0F1107F7] DEFAULT ('N'),
[AllowAssetEntries] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE_New__AllowAss__10052C30] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[SystemSetup] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE__SystemSetup__69B6316D] DEFAULT ('N'),
[AllowedPRCreateTypes] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowPRPromoCreate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE__AllowPRPromo__69812743] DEFAULT ('Y'),
[OrgRowLevelID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DashboardAdmin] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE__DashboardAdm__77CF469A] DEFAULT ('N'),
[DataPackAdmin] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE__DataPackAdmi__7AABB345] DEFAULT ('N'),
[AccessAllDataPacks] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE__AccessAllDat__7B9FD77E] DEFAULT ('N'),
[DataSourceAdmin] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE__DataSourceAd__7C93FBB7] DEFAULT ('N'),
[AccessAllDataSources] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE__AccessAllDat__7D881FF0] DEFAULT ('N'),
[InteractiveBillingAllowEditsAccept] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE__InteractiveB__0D746EF6] DEFAULT ('N'),
[AllowAccessToVIDashpart] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE__AllowAccessT__0E68932F] DEFAULT ('N'),
[AllowCubeFullRefresh] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE__AllowCubeFul__6F05C1C6] DEFAULT ('N'),
[DeltekInternal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SE__DeltekIntern__1AE44404] DEFAULT ('N'),
[DeltekInternalInfo] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_SE]
      ON [dbo].[SE]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SE'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'Role',CONVERT(NVARCHAR(2000),[Role],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'SystemAdmin',CONVERT(NVARCHAR(2000),[SystemAdmin],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AccountingType',CONVERT(NVARCHAR(2000),[AccountingType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'MarketingType',CONVERT(NVARCHAR(2000),[MarketingType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AccessAllNavNodes',CONVERT(NVARCHAR(2000),[AccessAllNavNodes],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AccessAllReports',CONVERT(NVARCHAR(2000),[AccessAllReports],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AccessAllTemplates',CONVERT(NVARCHAR(2000),[AccessAllTemplates],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AccessAllDashParts',CONVERT(NVARCHAR(2000),[AccessAllDashParts],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AccessAllTabs',CONVERT(NVARCHAR(2000),[AccessAllTabs],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'SeeCostRates',CONVERT(NVARCHAR(2000),[SeeCostRates],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'ProjectBudgetAccess',CONVERT(NVARCHAR(2000),[ProjectBudgetAccess],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'PayrollCheckRec',CONVERT(NVARCHAR(2000),[PayrollCheckRec],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'VoucherReviewModify',CONVERT(NVARCHAR(2000),[VoucherReviewModify],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'ProcessClosedPeriods',CONVERT(NVARCHAR(2000),[ProcessClosedPeriods],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'BillingRow',CONVERT(NVARCHAR(2000),[BillingRow],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'BillingGroupRow',CONVERT(NVARCHAR(2000),[BillingGroupRow],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'InteractiveBillingAccess',CONVERT(NVARCHAR(2000),[InteractiveBillingAccess],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'DeleteInvoices',CONVERT(NVARCHAR(2000),[DeleteInvoices],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'ProcessPriorPeriods',CONVERT(NVARCHAR(2000),[ProcessPriorPeriods],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'TKRow',CONVERT(NVARCHAR(2000),[TKRow],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'EKRow',CONVERT(NVARCHAR(2000),[EKRow],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'DataEntryRow',CONVERT(NVARCHAR(2000),[DataEntryRow],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'ARCommentReview',CONVERT(NVARCHAR(2000),[ARCommentReview],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'ARCommentUpdate',CONVERT(NVARCHAR(2000),[ARCommentUpdate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'OrgRowLevelWhere','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'ProcessQueuePriority',CONVERT(NVARCHAR(2000),[ProcessQueuePriority],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AccessAllPrinters',CONVERT(NVARCHAR(2000),[AccessAllPrinters],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AccessAllLookupFields',CONVERT(NVARCHAR(2000),[AccessAllLookupFields],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPChangeRate',CONVERT(NVARCHAR(2000),[RPChangeRate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPChangeDefWBSMapping',CONVERT(NVARCHAR(2000),[RPChangeDefWBSMapping],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPApproveAssignment',CONVERT(NVARCHAR(2000),[RPApproveAssignment],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPSubstitutePeople',CONVERT(NVARCHAR(2000),[RPSubstitutePeople],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPChangeBudgetType',CONVERT(NVARCHAR(2000),[RPChangeBudgetType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPRateMethodTable',CONVERT(NVARCHAR(2000),[RPRateMethodTable],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPChangePercComplete',CONVERT(NVARCHAR(2000),[RPChangePercComplete],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPChangeBudget',CONVERT(NVARCHAR(2000),[RPChangeBudget],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPChangeCompensation',CONVERT(NVARCHAR(2000),[RPChangeCompensation],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPChangeConsultantFees',CONVERT(NVARCHAR(2000),[RPChangeConsultantFees],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPReimbursableAllowance',CONVERT(NVARCHAR(2000),[RPReimbursableAllowance],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'SeeBurdenRates',CONVERT(NVARCHAR(2000),[SeeBurdenRates],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowPriorW2Quarter',CONVERT(NVARCHAR(2000),[AllowPriorW2Quarter],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'ReportAdmin',CONVERT(NVARCHAR(2000),[ReportAdmin],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AccessAllTransTypes',CONVERT(NVARCHAR(2000),[AccessAllTransTypes],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPChangeRevFormula',CONVERT(NVARCHAR(2000),[RPChangeRevFormula],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowAddWebParts',CONVERT(NVARCHAR(2000),[AllowAddWebParts],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowCalcFields',CONVERT(NVARCHAR(2000),[AllowCalcFields],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowAddDashParts',CONVERT(NVARCHAR(2000),[AllowAddDashParts],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AccessAll20DashParts',CONVERT(NVARCHAR(2000),[AccessAll20DashParts],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AccessAllCompanies',CONVERT(NVARCHAR(2000),[AccessAllCompanies],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPChangeOverheadPct',CONVERT(NVARCHAR(2000),[RPChangeOverheadPct],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'POPrintPO',CONVERT(NVARCHAR(2000),[POPrintPO],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'PODeleteOpenPO',CONVERT(NVARCHAR(2000),[PODeleteOpenPO],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'POReleasePO',CONVERT(NVARCHAR(2000),[POReleasePO],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'POUnreceive',CONVERT(NVARCHAR(2000),[POUnreceive],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'POClosePO',CONVERT(NVARCHAR(2000),[POClosePO],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'PODeleteOpenPR',CONVERT(NVARCHAR(2000),[PODeleteOpenPR],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'FinalBatchBillingAccess',CONVERT(NVARCHAR(2000),[FinalBatchBillingAccess],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowSQLWhereInLookup',CONVERT(NVARCHAR(2000),[AllowSQLWhereInLookup],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPUpdatePlanLabor',CONVERT(NVARCHAR(2000),[RPUpdatePlanLabor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AccessDocMgt',CONVERT(NVARCHAR(2000),[AccessDocMgt],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPEMRow',CONVERT(NVARCHAR(2000),[RPEMRow],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'SeeCostRatesIgnoreProfitCalc',CONVERT(NVARCHAR(2000),[SeeCostRatesIgnoreProfitCalc],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'SeeBurdenRatesIgnoreProfitCalc',CONVERT(NVARCHAR(2000),[SeeBurdenRatesIgnoreProfitCalc],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowIntercompanyReceipts',CONVERT(NVARCHAR(2000),[AllowIntercompanyReceipts],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'InteractiveBillingModify',CONVERT(NVARCHAR(2000),[InteractiveBillingModify],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'InteractiveBillingDelete',CONVERT(NVARCHAR(2000),[InteractiveBillingDelete],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'InteractiveBillingWriteOff',CONVERT(NVARCHAR(2000),[InteractiveBillingWriteOff],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'InteractiveBillingFinalAccess',CONVERT(NVARCHAR(2000),[InteractiveBillingFinalAccess],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'InteractiveBillingHold',CONVERT(NVARCHAR(2000),[InteractiveBillingHold],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'InteractiveBillingInsert',CONVERT(NVARCHAR(2000),[InteractiveBillingInsert],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'InteractiveBillingTransfer',CONVERT(NVARCHAR(2000),[InteractiveBillingTransfer],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'InteractiveBillingEditedInvoices',CONVERT(NVARCHAR(2000),[InteractiveBillingEditedInvoices],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'EmailTemplateAdmin',CONVERT(NVARCHAR(2000),[EmailTemplateAdmin],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPHardBooking',CONVERT(NVARCHAR(2000),[RPHardBooking],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'ContactDownload','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'ClientDownload','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'ActivityDownload','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'OpportunityDownload','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPChangeAssignments',CONVERT(NVARCHAR(2000),[RPChangeAssignments],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowAcrossICLookup',CONVERT(NVARCHAR(2000),[AllowAcrossICLookup],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowReprocessIntercompanyBilling',CONVERT(NVARCHAR(2000),[AllowReprocessIntercompanyBilling],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'EmployeeSelfServiceInd',CONVERT(NVARCHAR(2000),[EmployeeSelfServiceInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowChangeDisableLogin',CONVERT(NVARCHAR(2000),[AllowChangeDisableLogin],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowChangePassword',CONVERT(NVARCHAR(2000),[AllowChangePassword],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowChangeSupportDocument',CONVERT(NVARCHAR(2000),[AllowChangeSupportDocument],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'Navigator',CONVERT(NVARCHAR(2000),[Navigator],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavEmpWorkspace',CONVERT(NVARCHAR(2000),[NavEmpWorkspace],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavPMWorkspace',CONVERT(NVARCHAR(2000),[NavPMWorkspace],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavTimesheet',CONVERT(NVARCHAR(2000),[NavTimesheet],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavDashboard',CONVERT(NVARCHAR(2000),[NavDashboard],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavPlanning',CONVERT(NVARCHAR(2000),[NavPlanning],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavLabor',CONVERT(NVARCHAR(2000),[NavLabor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavConsultant',CONVERT(NVARCHAR(2000),[NavConsultant],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavExpense',CONVERT(NVARCHAR(2000),[NavExpense],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavContract',CONVERT(NVARCHAR(2000),[NavContract],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavUseProjectAccess',CONVERT(NVARCHAR(2000),[NavUseProjectAccess],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavUsePlanningAccess',CONVERT(NVARCHAR(2000),[NavUsePlanningAccess],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavUseProjectView',CONVERT(NVARCHAR(2000),[NavUseProjectView],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavUseProjectUpdate',CONVERT(NVARCHAR(2000),[NavUseProjectUpdate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavUsePlanningView',CONVERT(NVARCHAR(2000),[NavUsePlanningView],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavUsePlanningUpdate',CONVERT(NVARCHAR(2000),[NavUsePlanningUpdate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavExpRpt',CONVERT(NVARCHAR(2000),[NavExpRpt],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavInvoice',CONVERT(NVARCHAR(2000),[NavInvoice],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'NavCRMWorkspace',CONVERT(NVARCHAR(2000),[NavCRMWorkspace],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPChangeCompensationExp',CONVERT(NVARCHAR(2000),[RPChangeCompensationExp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'RPReimbursableAllowanceCon',CONVERT(NVARCHAR(2000),[RPReimbursableAllowanceCon],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowLookupLimitOverride',CONVERT(NVARCHAR(2000),[AllowLookupLimitOverride],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowEditPriorDepreciation',CONVERT(NVARCHAR(2000),[AllowEditPriorDepreciation],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowAssetEntries',CONVERT(NVARCHAR(2000),[AllowAssetEntries],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'SystemSetup',CONVERT(NVARCHAR(2000),[SystemSetup],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowedPRCreateTypes',CONVERT(NVARCHAR(2000),[AllowedPRCreateTypes],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowPRPromoCreate',CONVERT(NVARCHAR(2000),[AllowPRPromoCreate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'OrgRowLevelID',CONVERT(NVARCHAR(2000),[OrgRowLevelID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'DashboardAdmin',CONVERT(NVARCHAR(2000),[DashboardAdmin],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'DataPackAdmin',CONVERT(NVARCHAR(2000),[DataPackAdmin],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AccessAllDataPacks',CONVERT(NVARCHAR(2000),[AccessAllDataPacks],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'DataSourceAdmin',CONVERT(NVARCHAR(2000),[DataSourceAdmin],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AccessAllDataSources',CONVERT(NVARCHAR(2000),[AccessAllDataSources],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'InteractiveBillingAllowEditsAccept',CONVERT(NVARCHAR(2000),[InteractiveBillingAllowEditsAccept],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowAccessToVIDashpart',CONVERT(NVARCHAR(2000),[AllowAccessToVIDashpart],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'AllowCubeFullRefresh',CONVERT(NVARCHAR(2000),[AllowCubeFullRefresh],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'DeltekInternal',CONVERT(NVARCHAR(2000),[DeltekInternal],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121),'DeltekInternalInfo',CONVERT(NVARCHAR(2000),[DeltekInternalInfo],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_SE]
      ON [dbo].[SE]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SE'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'Role',NULL,CONVERT(NVARCHAR(2000),[Role],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'SystemAdmin',NULL,CONVERT(NVARCHAR(2000),[SystemAdmin],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccountingType',NULL,CONVERT(NVARCHAR(2000),[AccountingType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'MarketingType',NULL,CONVERT(NVARCHAR(2000),[MarketingType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllNavNodes',NULL,CONVERT(NVARCHAR(2000),[AccessAllNavNodes],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllReports',NULL,CONVERT(NVARCHAR(2000),[AccessAllReports],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllTemplates',NULL,CONVERT(NVARCHAR(2000),[AccessAllTemplates],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllDashParts',NULL,CONVERT(NVARCHAR(2000),[AccessAllDashParts],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllTabs',NULL,CONVERT(NVARCHAR(2000),[AccessAllTabs],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'SeeCostRates',NULL,CONVERT(NVARCHAR(2000),[SeeCostRates],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ProjectBudgetAccess',NULL,CONVERT(NVARCHAR(2000),[ProjectBudgetAccess],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'PayrollCheckRec',NULL,CONVERT(NVARCHAR(2000),[PayrollCheckRec],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'VoucherReviewModify',NULL,CONVERT(NVARCHAR(2000),[VoucherReviewModify],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ProcessClosedPeriods',NULL,CONVERT(NVARCHAR(2000),[ProcessClosedPeriods],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'BillingRow',NULL,CONVERT(NVARCHAR(2000),[BillingRow],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'BillingGroupRow',NULL,CONVERT(NVARCHAR(2000),[BillingGroupRow],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingAccess',NULL,CONVERT(NVARCHAR(2000),[InteractiveBillingAccess],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'DeleteInvoices',NULL,CONVERT(NVARCHAR(2000),[DeleteInvoices],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ProcessPriorPeriods',NULL,CONVERT(NVARCHAR(2000),[ProcessPriorPeriods],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'TKRow',NULL,CONVERT(NVARCHAR(2000),[TKRow],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'EKRow',NULL,CONVERT(NVARCHAR(2000),[EKRow],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'DataEntryRow',NULL,CONVERT(NVARCHAR(2000),[DataEntryRow],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ARCommentReview',NULL,CONVERT(NVARCHAR(2000),[ARCommentReview],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ARCommentUpdate',NULL,CONVERT(NVARCHAR(2000),[ARCommentUpdate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'OrgRowLevelWhere',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ProcessQueuePriority',NULL,CONVERT(NVARCHAR(2000),[ProcessQueuePriority],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllPrinters',NULL,CONVERT(NVARCHAR(2000),[AccessAllPrinters],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllLookupFields',NULL,CONVERT(NVARCHAR(2000),[AccessAllLookupFields],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeRate',NULL,CONVERT(NVARCHAR(2000),[RPChangeRate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeDefWBSMapping',NULL,CONVERT(NVARCHAR(2000),[RPChangeDefWBSMapping],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPApproveAssignment',NULL,CONVERT(NVARCHAR(2000),[RPApproveAssignment],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPSubstitutePeople',NULL,CONVERT(NVARCHAR(2000),[RPSubstitutePeople],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeBudgetType',NULL,CONVERT(NVARCHAR(2000),[RPChangeBudgetType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPRateMethodTable',NULL,CONVERT(NVARCHAR(2000),[RPRateMethodTable],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangePercComplete',NULL,CONVERT(NVARCHAR(2000),[RPChangePercComplete],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeBudget',NULL,CONVERT(NVARCHAR(2000),[RPChangeBudget],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeCompensation',NULL,CONVERT(NVARCHAR(2000),[RPChangeCompensation],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeConsultantFees',NULL,CONVERT(NVARCHAR(2000),[RPChangeConsultantFees],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPReimbursableAllowance',NULL,CONVERT(NVARCHAR(2000),[RPReimbursableAllowance],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'SeeBurdenRates',NULL,CONVERT(NVARCHAR(2000),[SeeBurdenRates],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowPriorW2Quarter',NULL,CONVERT(NVARCHAR(2000),[AllowPriorW2Quarter],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ReportAdmin',NULL,CONVERT(NVARCHAR(2000),[ReportAdmin],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllTransTypes',NULL,CONVERT(NVARCHAR(2000),[AccessAllTransTypes],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeRevFormula',NULL,CONVERT(NVARCHAR(2000),[RPChangeRevFormula],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowAddWebParts',NULL,CONVERT(NVARCHAR(2000),[AllowAddWebParts],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowCalcFields',NULL,CONVERT(NVARCHAR(2000),[AllowCalcFields],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowAddDashParts',NULL,CONVERT(NVARCHAR(2000),[AllowAddDashParts],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAll20DashParts',NULL,CONVERT(NVARCHAR(2000),[AccessAll20DashParts],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllCompanies',NULL,CONVERT(NVARCHAR(2000),[AccessAllCompanies],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeOverheadPct',NULL,CONVERT(NVARCHAR(2000),[RPChangeOverheadPct],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'POPrintPO',NULL,CONVERT(NVARCHAR(2000),[POPrintPO],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'PODeleteOpenPO',NULL,CONVERT(NVARCHAR(2000),[PODeleteOpenPO],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'POReleasePO',NULL,CONVERT(NVARCHAR(2000),[POReleasePO],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'POUnreceive',NULL,CONVERT(NVARCHAR(2000),[POUnreceive],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'POClosePO',NULL,CONVERT(NVARCHAR(2000),[POClosePO],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'PODeleteOpenPR',NULL,CONVERT(NVARCHAR(2000),[PODeleteOpenPR],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'FinalBatchBillingAccess',NULL,CONVERT(NVARCHAR(2000),[FinalBatchBillingAccess],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowSQLWhereInLookup',NULL,CONVERT(NVARCHAR(2000),[AllowSQLWhereInLookup],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPUpdatePlanLabor',NULL,CONVERT(NVARCHAR(2000),[RPUpdatePlanLabor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessDocMgt',NULL,CONVERT(NVARCHAR(2000),[AccessDocMgt],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPEMRow',NULL,CONVERT(NVARCHAR(2000),[RPEMRow],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'SeeCostRatesIgnoreProfitCalc',NULL,CONVERT(NVARCHAR(2000),[SeeCostRatesIgnoreProfitCalc],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'SeeBurdenRatesIgnoreProfitCalc',NULL,CONVERT(NVARCHAR(2000),[SeeBurdenRatesIgnoreProfitCalc],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowIntercompanyReceipts',NULL,CONVERT(NVARCHAR(2000),[AllowIntercompanyReceipts],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingModify',NULL,CONVERT(NVARCHAR(2000),[InteractiveBillingModify],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingDelete',NULL,CONVERT(NVARCHAR(2000),[InteractiveBillingDelete],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingWriteOff',NULL,CONVERT(NVARCHAR(2000),[InteractiveBillingWriteOff],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingFinalAccess',NULL,CONVERT(NVARCHAR(2000),[InteractiveBillingFinalAccess],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingHold',NULL,CONVERT(NVARCHAR(2000),[InteractiveBillingHold],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingInsert',NULL,CONVERT(NVARCHAR(2000),[InteractiveBillingInsert],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingTransfer',NULL,CONVERT(NVARCHAR(2000),[InteractiveBillingTransfer],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingEditedInvoices',NULL,CONVERT(NVARCHAR(2000),[InteractiveBillingEditedInvoices],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'EmailTemplateAdmin',NULL,CONVERT(NVARCHAR(2000),[EmailTemplateAdmin],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPHardBooking',NULL,CONVERT(NVARCHAR(2000),[RPHardBooking],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ContactDownload',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ClientDownload',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ActivityDownload',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'OpportunityDownload',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeAssignments',NULL,CONVERT(NVARCHAR(2000),[RPChangeAssignments],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowAcrossICLookup',NULL,CONVERT(NVARCHAR(2000),[AllowAcrossICLookup],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowReprocessIntercompanyBilling',NULL,CONVERT(NVARCHAR(2000),[AllowReprocessIntercompanyBilling],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'EmployeeSelfServiceInd',NULL,CONVERT(NVARCHAR(2000),[EmployeeSelfServiceInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowChangeDisableLogin',NULL,CONVERT(NVARCHAR(2000),[AllowChangeDisableLogin],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowChangePassword',NULL,CONVERT(NVARCHAR(2000),[AllowChangePassword],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowChangeSupportDocument',NULL,CONVERT(NVARCHAR(2000),[AllowChangeSupportDocument],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'Navigator',NULL,CONVERT(NVARCHAR(2000),[Navigator],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavEmpWorkspace',NULL,CONVERT(NVARCHAR(2000),[NavEmpWorkspace],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavPMWorkspace',NULL,CONVERT(NVARCHAR(2000),[NavPMWorkspace],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavTimesheet',NULL,CONVERT(NVARCHAR(2000),[NavTimesheet],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavDashboard',NULL,CONVERT(NVARCHAR(2000),[NavDashboard],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavPlanning',NULL,CONVERT(NVARCHAR(2000),[NavPlanning],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavLabor',NULL,CONVERT(NVARCHAR(2000),[NavLabor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavConsultant',NULL,CONVERT(NVARCHAR(2000),[NavConsultant],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavExpense',NULL,CONVERT(NVARCHAR(2000),[NavExpense],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavContract',NULL,CONVERT(NVARCHAR(2000),[NavContract],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavUseProjectAccess',NULL,CONVERT(NVARCHAR(2000),[NavUseProjectAccess],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavUsePlanningAccess',NULL,CONVERT(NVARCHAR(2000),[NavUsePlanningAccess],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavUseProjectView',NULL,CONVERT(NVARCHAR(2000),[NavUseProjectView],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavUseProjectUpdate',NULL,CONVERT(NVARCHAR(2000),[NavUseProjectUpdate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavUsePlanningView',NULL,CONVERT(NVARCHAR(2000),[NavUsePlanningView],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavUsePlanningUpdate',NULL,CONVERT(NVARCHAR(2000),[NavUsePlanningUpdate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavExpRpt',NULL,CONVERT(NVARCHAR(2000),[NavExpRpt],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavInvoice',NULL,CONVERT(NVARCHAR(2000),[NavInvoice],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavCRMWorkspace',NULL,CONVERT(NVARCHAR(2000),[NavCRMWorkspace],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeCompensationExp',NULL,CONVERT(NVARCHAR(2000),[RPChangeCompensationExp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPReimbursableAllowanceCon',NULL,CONVERT(NVARCHAR(2000),[RPReimbursableAllowanceCon],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowLookupLimitOverride',NULL,CONVERT(NVARCHAR(2000),[AllowLookupLimitOverride],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowEditPriorDepreciation',NULL,CONVERT(NVARCHAR(2000),[AllowEditPriorDepreciation],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowAssetEntries',NULL,CONVERT(NVARCHAR(2000),[AllowAssetEntries],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'SystemSetup',NULL,CONVERT(NVARCHAR(2000),[SystemSetup],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowedPRCreateTypes',NULL,CONVERT(NVARCHAR(2000),[AllowedPRCreateTypes],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowPRPromoCreate',NULL,CONVERT(NVARCHAR(2000),[AllowPRPromoCreate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'OrgRowLevelID',NULL,CONVERT(NVARCHAR(2000),[OrgRowLevelID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'DashboardAdmin',NULL,CONVERT(NVARCHAR(2000),[DashboardAdmin],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'DataPackAdmin',NULL,CONVERT(NVARCHAR(2000),[DataPackAdmin],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllDataPacks',NULL,CONVERT(NVARCHAR(2000),[AccessAllDataPacks],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'DataSourceAdmin',NULL,CONVERT(NVARCHAR(2000),[DataSourceAdmin],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllDataSources',NULL,CONVERT(NVARCHAR(2000),[AccessAllDataSources],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingAllowEditsAccept',NULL,CONVERT(NVARCHAR(2000),[InteractiveBillingAllowEditsAccept],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowAccessToVIDashpart',NULL,CONVERT(NVARCHAR(2000),[AllowAccessToVIDashpart],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowCubeFullRefresh',NULL,CONVERT(NVARCHAR(2000),[AllowCubeFullRefresh],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'DeltekInternal',NULL,CONVERT(NVARCHAR(2000),[DeltekInternal],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'DeltekInternalInfo',NULL,CONVERT(NVARCHAR(2000),[DeltekInternalInfo],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_SE]
      ON [dbo].[SE]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SE'
    
      If UPDATE([Role])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'Role',
      CONVERT(NVARCHAR(2000),DELETED.[Role],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Role],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[Role] Is Null And
				DELETED.[Role] Is Not Null
			) Or
			(
				INSERTED.[Role] Is Not Null And
				DELETED.[Role] Is Null
			) Or
			(
				INSERTED.[Role] !=
				DELETED.[Role]
			)
		) 
		END		
		
      If UPDATE([SystemAdmin])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'SystemAdmin',
      CONVERT(NVARCHAR(2000),DELETED.[SystemAdmin],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SystemAdmin],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[SystemAdmin] Is Null And
				DELETED.[SystemAdmin] Is Not Null
			) Or
			(
				INSERTED.[SystemAdmin] Is Not Null And
				DELETED.[SystemAdmin] Is Null
			) Or
			(
				INSERTED.[SystemAdmin] !=
				DELETED.[SystemAdmin]
			)
		) 
		END		
		
      If UPDATE([AccountingType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccountingType',
      CONVERT(NVARCHAR(2000),DELETED.[AccountingType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccountingType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AccountingType] Is Null And
				DELETED.[AccountingType] Is Not Null
			) Or
			(
				INSERTED.[AccountingType] Is Not Null And
				DELETED.[AccountingType] Is Null
			) Or
			(
				INSERTED.[AccountingType] !=
				DELETED.[AccountingType]
			)
		) 
		END		
		
      If UPDATE([MarketingType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'MarketingType',
      CONVERT(NVARCHAR(2000),DELETED.[MarketingType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MarketingType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[MarketingType] Is Null And
				DELETED.[MarketingType] Is Not Null
			) Or
			(
				INSERTED.[MarketingType] Is Not Null And
				DELETED.[MarketingType] Is Null
			) Or
			(
				INSERTED.[MarketingType] !=
				DELETED.[MarketingType]
			)
		) 
		END		
		
      If UPDATE([AccessAllNavNodes])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllNavNodes',
      CONVERT(NVARCHAR(2000),DELETED.[AccessAllNavNodes],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccessAllNavNodes],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AccessAllNavNodes] Is Null And
				DELETED.[AccessAllNavNodes] Is Not Null
			) Or
			(
				INSERTED.[AccessAllNavNodes] Is Not Null And
				DELETED.[AccessAllNavNodes] Is Null
			) Or
			(
				INSERTED.[AccessAllNavNodes] !=
				DELETED.[AccessAllNavNodes]
			)
		) 
		END		
		
      If UPDATE([AccessAllReports])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllReports',
      CONVERT(NVARCHAR(2000),DELETED.[AccessAllReports],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccessAllReports],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AccessAllReports] Is Null And
				DELETED.[AccessAllReports] Is Not Null
			) Or
			(
				INSERTED.[AccessAllReports] Is Not Null And
				DELETED.[AccessAllReports] Is Null
			) Or
			(
				INSERTED.[AccessAllReports] !=
				DELETED.[AccessAllReports]
			)
		) 
		END		
		
      If UPDATE([AccessAllTemplates])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllTemplates',
      CONVERT(NVARCHAR(2000),DELETED.[AccessAllTemplates],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccessAllTemplates],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AccessAllTemplates] Is Null And
				DELETED.[AccessAllTemplates] Is Not Null
			) Or
			(
				INSERTED.[AccessAllTemplates] Is Not Null And
				DELETED.[AccessAllTemplates] Is Null
			) Or
			(
				INSERTED.[AccessAllTemplates] !=
				DELETED.[AccessAllTemplates]
			)
		) 
		END		
		
      If UPDATE([AccessAllDashParts])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllDashParts',
      CONVERT(NVARCHAR(2000),DELETED.[AccessAllDashParts],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccessAllDashParts],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AccessAllDashParts] Is Null And
				DELETED.[AccessAllDashParts] Is Not Null
			) Or
			(
				INSERTED.[AccessAllDashParts] Is Not Null And
				DELETED.[AccessAllDashParts] Is Null
			) Or
			(
				INSERTED.[AccessAllDashParts] !=
				DELETED.[AccessAllDashParts]
			)
		) 
		END		
		
      If UPDATE([AccessAllTabs])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllTabs',
      CONVERT(NVARCHAR(2000),DELETED.[AccessAllTabs],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccessAllTabs],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AccessAllTabs] Is Null And
				DELETED.[AccessAllTabs] Is Not Null
			) Or
			(
				INSERTED.[AccessAllTabs] Is Not Null And
				DELETED.[AccessAllTabs] Is Null
			) Or
			(
				INSERTED.[AccessAllTabs] !=
				DELETED.[AccessAllTabs]
			)
		) 
		END		
		
      If UPDATE([SeeCostRates])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'SeeCostRates',
      CONVERT(NVARCHAR(2000),DELETED.[SeeCostRates],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SeeCostRates],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[SeeCostRates] Is Null And
				DELETED.[SeeCostRates] Is Not Null
			) Or
			(
				INSERTED.[SeeCostRates] Is Not Null And
				DELETED.[SeeCostRates] Is Null
			) Or
			(
				INSERTED.[SeeCostRates] !=
				DELETED.[SeeCostRates]
			)
		) 
		END		
		
      If UPDATE([ProjectBudgetAccess])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ProjectBudgetAccess',
      CONVERT(NVARCHAR(2000),DELETED.[ProjectBudgetAccess],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProjectBudgetAccess],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[ProjectBudgetAccess] Is Null And
				DELETED.[ProjectBudgetAccess] Is Not Null
			) Or
			(
				INSERTED.[ProjectBudgetAccess] Is Not Null And
				DELETED.[ProjectBudgetAccess] Is Null
			) Or
			(
				INSERTED.[ProjectBudgetAccess] !=
				DELETED.[ProjectBudgetAccess]
			)
		) 
		END		
		
      If UPDATE([PayrollCheckRec])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'PayrollCheckRec',
      CONVERT(NVARCHAR(2000),DELETED.[PayrollCheckRec],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PayrollCheckRec],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[PayrollCheckRec] Is Null And
				DELETED.[PayrollCheckRec] Is Not Null
			) Or
			(
				INSERTED.[PayrollCheckRec] Is Not Null And
				DELETED.[PayrollCheckRec] Is Null
			) Or
			(
				INSERTED.[PayrollCheckRec] !=
				DELETED.[PayrollCheckRec]
			)
		) 
		END		
		
      If UPDATE([VoucherReviewModify])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'VoucherReviewModify',
      CONVERT(NVARCHAR(2000),DELETED.[VoucherReviewModify],121),
      CONVERT(NVARCHAR(2000),INSERTED.[VoucherReviewModify],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[VoucherReviewModify] Is Null And
				DELETED.[VoucherReviewModify] Is Not Null
			) Or
			(
				INSERTED.[VoucherReviewModify] Is Not Null And
				DELETED.[VoucherReviewModify] Is Null
			) Or
			(
				INSERTED.[VoucherReviewModify] !=
				DELETED.[VoucherReviewModify]
			)
		) 
		END		
		
      If UPDATE([ProcessClosedPeriods])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ProcessClosedPeriods',
      CONVERT(NVARCHAR(2000),DELETED.[ProcessClosedPeriods],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProcessClosedPeriods],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[ProcessClosedPeriods] Is Null And
				DELETED.[ProcessClosedPeriods] Is Not Null
			) Or
			(
				INSERTED.[ProcessClosedPeriods] Is Not Null And
				DELETED.[ProcessClosedPeriods] Is Null
			) Or
			(
				INSERTED.[ProcessClosedPeriods] !=
				DELETED.[ProcessClosedPeriods]
			)
		) 
		END		
		
      If UPDATE([BillingRow])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'BillingRow',
      CONVERT(NVARCHAR(2000),DELETED.[BillingRow],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BillingRow],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[BillingRow] Is Null And
				DELETED.[BillingRow] Is Not Null
			) Or
			(
				INSERTED.[BillingRow] Is Not Null And
				DELETED.[BillingRow] Is Null
			) Or
			(
				INSERTED.[BillingRow] !=
				DELETED.[BillingRow]
			)
		) 
		END		
		
      If UPDATE([BillingGroupRow])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'BillingGroupRow',
      CONVERT(NVARCHAR(2000),DELETED.[BillingGroupRow],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BillingGroupRow],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[BillingGroupRow] Is Null And
				DELETED.[BillingGroupRow] Is Not Null
			) Or
			(
				INSERTED.[BillingGroupRow] Is Not Null And
				DELETED.[BillingGroupRow] Is Null
			) Or
			(
				INSERTED.[BillingGroupRow] !=
				DELETED.[BillingGroupRow]
			)
		) 
		END		
		
      If UPDATE([InteractiveBillingAccess])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingAccess',
      CONVERT(NVARCHAR(2000),DELETED.[InteractiveBillingAccess],121),
      CONVERT(NVARCHAR(2000),INSERTED.[InteractiveBillingAccess],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[InteractiveBillingAccess] Is Null And
				DELETED.[InteractiveBillingAccess] Is Not Null
			) Or
			(
				INSERTED.[InteractiveBillingAccess] Is Not Null And
				DELETED.[InteractiveBillingAccess] Is Null
			) Or
			(
				INSERTED.[InteractiveBillingAccess] !=
				DELETED.[InteractiveBillingAccess]
			)
		) 
		END		
		
      If UPDATE([DeleteInvoices])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'DeleteInvoices',
      CONVERT(NVARCHAR(2000),DELETED.[DeleteInvoices],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DeleteInvoices],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[DeleteInvoices] Is Null And
				DELETED.[DeleteInvoices] Is Not Null
			) Or
			(
				INSERTED.[DeleteInvoices] Is Not Null And
				DELETED.[DeleteInvoices] Is Null
			) Or
			(
				INSERTED.[DeleteInvoices] !=
				DELETED.[DeleteInvoices]
			)
		) 
		END		
		
      If UPDATE([ProcessPriorPeriods])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ProcessPriorPeriods',
      CONVERT(NVARCHAR(2000),DELETED.[ProcessPriorPeriods],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProcessPriorPeriods],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[ProcessPriorPeriods] Is Null And
				DELETED.[ProcessPriorPeriods] Is Not Null
			) Or
			(
				INSERTED.[ProcessPriorPeriods] Is Not Null And
				DELETED.[ProcessPriorPeriods] Is Null
			) Or
			(
				INSERTED.[ProcessPriorPeriods] !=
				DELETED.[ProcessPriorPeriods]
			)
		) 
		END		
		
      If UPDATE([TKRow])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'TKRow',
      CONVERT(NVARCHAR(2000),DELETED.[TKRow],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TKRow],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[TKRow] Is Null And
				DELETED.[TKRow] Is Not Null
			) Or
			(
				INSERTED.[TKRow] Is Not Null And
				DELETED.[TKRow] Is Null
			) Or
			(
				INSERTED.[TKRow] !=
				DELETED.[TKRow]
			)
		) 
		END		
		
      If UPDATE([EKRow])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'EKRow',
      CONVERT(NVARCHAR(2000),DELETED.[EKRow],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EKRow],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[EKRow] Is Null And
				DELETED.[EKRow] Is Not Null
			) Or
			(
				INSERTED.[EKRow] Is Not Null And
				DELETED.[EKRow] Is Null
			) Or
			(
				INSERTED.[EKRow] !=
				DELETED.[EKRow]
			)
		) 
		END		
		
      If UPDATE([DataEntryRow])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'DataEntryRow',
      CONVERT(NVARCHAR(2000),DELETED.[DataEntryRow],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DataEntryRow],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[DataEntryRow] Is Null And
				DELETED.[DataEntryRow] Is Not Null
			) Or
			(
				INSERTED.[DataEntryRow] Is Not Null And
				DELETED.[DataEntryRow] Is Null
			) Or
			(
				INSERTED.[DataEntryRow] !=
				DELETED.[DataEntryRow]
			)
		) 
		END		
		
      If UPDATE([ARCommentReview])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ARCommentReview',
      CONVERT(NVARCHAR(2000),DELETED.[ARCommentReview],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ARCommentReview],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[ARCommentReview] Is Null And
				DELETED.[ARCommentReview] Is Not Null
			) Or
			(
				INSERTED.[ARCommentReview] Is Not Null And
				DELETED.[ARCommentReview] Is Null
			) Or
			(
				INSERTED.[ARCommentReview] !=
				DELETED.[ARCommentReview]
			)
		) 
		END		
		
      If UPDATE([ARCommentUpdate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ARCommentUpdate',
      CONVERT(NVARCHAR(2000),DELETED.[ARCommentUpdate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ARCommentUpdate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[ARCommentUpdate] Is Null And
				DELETED.[ARCommentUpdate] Is Not Null
			) Or
			(
				INSERTED.[ARCommentUpdate] Is Not Null And
				DELETED.[ARCommentUpdate] Is Null
			) Or
			(
				INSERTED.[ARCommentUpdate] !=
				DELETED.[ARCommentUpdate]
			)
		) 
		END		
		
      If UPDATE([OrgRowLevelWhere])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'OrgRowLevelWhere',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([ProcessQueuePriority])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ProcessQueuePriority',
      CONVERT(NVARCHAR(2000),DELETED.[ProcessQueuePriority],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProcessQueuePriority],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[ProcessQueuePriority] Is Null And
				DELETED.[ProcessQueuePriority] Is Not Null
			) Or
			(
				INSERTED.[ProcessQueuePriority] Is Not Null And
				DELETED.[ProcessQueuePriority] Is Null
			) Or
			(
				INSERTED.[ProcessQueuePriority] !=
				DELETED.[ProcessQueuePriority]
			)
		) 
		END		
		
      If UPDATE([AccessAllPrinters])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllPrinters',
      CONVERT(NVARCHAR(2000),DELETED.[AccessAllPrinters],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccessAllPrinters],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AccessAllPrinters] Is Null And
				DELETED.[AccessAllPrinters] Is Not Null
			) Or
			(
				INSERTED.[AccessAllPrinters] Is Not Null And
				DELETED.[AccessAllPrinters] Is Null
			) Or
			(
				INSERTED.[AccessAllPrinters] !=
				DELETED.[AccessAllPrinters]
			)
		) 
		END		
		
      If UPDATE([AccessAllLookupFields])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllLookupFields',
      CONVERT(NVARCHAR(2000),DELETED.[AccessAllLookupFields],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccessAllLookupFields],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AccessAllLookupFields] Is Null And
				DELETED.[AccessAllLookupFields] Is Not Null
			) Or
			(
				INSERTED.[AccessAllLookupFields] Is Not Null And
				DELETED.[AccessAllLookupFields] Is Null
			) Or
			(
				INSERTED.[AccessAllLookupFields] !=
				DELETED.[AccessAllLookupFields]
			)
		) 
		END		
		
      If UPDATE([RPChangeRate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeRate',
      CONVERT(NVARCHAR(2000),DELETED.[RPChangeRate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPChangeRate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPChangeRate] Is Null And
				DELETED.[RPChangeRate] Is Not Null
			) Or
			(
				INSERTED.[RPChangeRate] Is Not Null And
				DELETED.[RPChangeRate] Is Null
			) Or
			(
				INSERTED.[RPChangeRate] !=
				DELETED.[RPChangeRate]
			)
		) 
		END		
		
      If UPDATE([RPChangeDefWBSMapping])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeDefWBSMapping',
      CONVERT(NVARCHAR(2000),DELETED.[RPChangeDefWBSMapping],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPChangeDefWBSMapping],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPChangeDefWBSMapping] Is Null And
				DELETED.[RPChangeDefWBSMapping] Is Not Null
			) Or
			(
				INSERTED.[RPChangeDefWBSMapping] Is Not Null And
				DELETED.[RPChangeDefWBSMapping] Is Null
			) Or
			(
				INSERTED.[RPChangeDefWBSMapping] !=
				DELETED.[RPChangeDefWBSMapping]
			)
		) 
		END		
		
      If UPDATE([RPApproveAssignment])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPApproveAssignment',
      CONVERT(NVARCHAR(2000),DELETED.[RPApproveAssignment],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPApproveAssignment],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPApproveAssignment] Is Null And
				DELETED.[RPApproveAssignment] Is Not Null
			) Or
			(
				INSERTED.[RPApproveAssignment] Is Not Null And
				DELETED.[RPApproveAssignment] Is Null
			) Or
			(
				INSERTED.[RPApproveAssignment] !=
				DELETED.[RPApproveAssignment]
			)
		) 
		END		
		
      If UPDATE([RPSubstitutePeople])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPSubstitutePeople',
      CONVERT(NVARCHAR(2000),DELETED.[RPSubstitutePeople],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPSubstitutePeople],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPSubstitutePeople] Is Null And
				DELETED.[RPSubstitutePeople] Is Not Null
			) Or
			(
				INSERTED.[RPSubstitutePeople] Is Not Null And
				DELETED.[RPSubstitutePeople] Is Null
			) Or
			(
				INSERTED.[RPSubstitutePeople] !=
				DELETED.[RPSubstitutePeople]
			)
		) 
		END		
		
      If UPDATE([RPChangeBudgetType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeBudgetType',
      CONVERT(NVARCHAR(2000),DELETED.[RPChangeBudgetType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPChangeBudgetType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPChangeBudgetType] Is Null And
				DELETED.[RPChangeBudgetType] Is Not Null
			) Or
			(
				INSERTED.[RPChangeBudgetType] Is Not Null And
				DELETED.[RPChangeBudgetType] Is Null
			) Or
			(
				INSERTED.[RPChangeBudgetType] !=
				DELETED.[RPChangeBudgetType]
			)
		) 
		END		
		
      If UPDATE([RPRateMethodTable])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPRateMethodTable',
      CONVERT(NVARCHAR(2000),DELETED.[RPRateMethodTable],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPRateMethodTable],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPRateMethodTable] Is Null And
				DELETED.[RPRateMethodTable] Is Not Null
			) Or
			(
				INSERTED.[RPRateMethodTable] Is Not Null And
				DELETED.[RPRateMethodTable] Is Null
			) Or
			(
				INSERTED.[RPRateMethodTable] !=
				DELETED.[RPRateMethodTable]
			)
		) 
		END		
		
      If UPDATE([RPChangePercComplete])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangePercComplete',
      CONVERT(NVARCHAR(2000),DELETED.[RPChangePercComplete],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPChangePercComplete],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPChangePercComplete] Is Null And
				DELETED.[RPChangePercComplete] Is Not Null
			) Or
			(
				INSERTED.[RPChangePercComplete] Is Not Null And
				DELETED.[RPChangePercComplete] Is Null
			) Or
			(
				INSERTED.[RPChangePercComplete] !=
				DELETED.[RPChangePercComplete]
			)
		) 
		END		
		
      If UPDATE([RPChangeBudget])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeBudget',
      CONVERT(NVARCHAR(2000),DELETED.[RPChangeBudget],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPChangeBudget],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPChangeBudget] Is Null And
				DELETED.[RPChangeBudget] Is Not Null
			) Or
			(
				INSERTED.[RPChangeBudget] Is Not Null And
				DELETED.[RPChangeBudget] Is Null
			) Or
			(
				INSERTED.[RPChangeBudget] !=
				DELETED.[RPChangeBudget]
			)
		) 
		END		
		
      If UPDATE([RPChangeCompensation])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeCompensation',
      CONVERT(NVARCHAR(2000),DELETED.[RPChangeCompensation],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPChangeCompensation],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPChangeCompensation] Is Null And
				DELETED.[RPChangeCompensation] Is Not Null
			) Or
			(
				INSERTED.[RPChangeCompensation] Is Not Null And
				DELETED.[RPChangeCompensation] Is Null
			) Or
			(
				INSERTED.[RPChangeCompensation] !=
				DELETED.[RPChangeCompensation]
			)
		) 
		END		
		
      If UPDATE([RPChangeConsultantFees])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeConsultantFees',
      CONVERT(NVARCHAR(2000),DELETED.[RPChangeConsultantFees],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPChangeConsultantFees],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPChangeConsultantFees] Is Null And
				DELETED.[RPChangeConsultantFees] Is Not Null
			) Or
			(
				INSERTED.[RPChangeConsultantFees] Is Not Null And
				DELETED.[RPChangeConsultantFees] Is Null
			) Or
			(
				INSERTED.[RPChangeConsultantFees] !=
				DELETED.[RPChangeConsultantFees]
			)
		) 
		END		
		
      If UPDATE([RPReimbursableAllowance])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPReimbursableAllowance',
      CONVERT(NVARCHAR(2000),DELETED.[RPReimbursableAllowance],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPReimbursableAllowance],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPReimbursableAllowance] Is Null And
				DELETED.[RPReimbursableAllowance] Is Not Null
			) Or
			(
				INSERTED.[RPReimbursableAllowance] Is Not Null And
				DELETED.[RPReimbursableAllowance] Is Null
			) Or
			(
				INSERTED.[RPReimbursableAllowance] !=
				DELETED.[RPReimbursableAllowance]
			)
		) 
		END		
		
      If UPDATE([SeeBurdenRates])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'SeeBurdenRates',
      CONVERT(NVARCHAR(2000),DELETED.[SeeBurdenRates],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SeeBurdenRates],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[SeeBurdenRates] Is Null And
				DELETED.[SeeBurdenRates] Is Not Null
			) Or
			(
				INSERTED.[SeeBurdenRates] Is Not Null And
				DELETED.[SeeBurdenRates] Is Null
			) Or
			(
				INSERTED.[SeeBurdenRates] !=
				DELETED.[SeeBurdenRates]
			)
		) 
		END		
		
      If UPDATE([AllowPriorW2Quarter])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowPriorW2Quarter',
      CONVERT(NVARCHAR(2000),DELETED.[AllowPriorW2Quarter],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowPriorW2Quarter],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowPriorW2Quarter] Is Null And
				DELETED.[AllowPriorW2Quarter] Is Not Null
			) Or
			(
				INSERTED.[AllowPriorW2Quarter] Is Not Null And
				DELETED.[AllowPriorW2Quarter] Is Null
			) Or
			(
				INSERTED.[AllowPriorW2Quarter] !=
				DELETED.[AllowPriorW2Quarter]
			)
		) 
		END		
		
      If UPDATE([ReportAdmin])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ReportAdmin',
      CONVERT(NVARCHAR(2000),DELETED.[ReportAdmin],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReportAdmin],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[ReportAdmin] Is Null And
				DELETED.[ReportAdmin] Is Not Null
			) Or
			(
				INSERTED.[ReportAdmin] Is Not Null And
				DELETED.[ReportAdmin] Is Null
			) Or
			(
				INSERTED.[ReportAdmin] !=
				DELETED.[ReportAdmin]
			)
		) 
		END		
		
      If UPDATE([AccessAllTransTypes])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllTransTypes',
      CONVERT(NVARCHAR(2000),DELETED.[AccessAllTransTypes],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccessAllTransTypes],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AccessAllTransTypes] Is Null And
				DELETED.[AccessAllTransTypes] Is Not Null
			) Or
			(
				INSERTED.[AccessAllTransTypes] Is Not Null And
				DELETED.[AccessAllTransTypes] Is Null
			) Or
			(
				INSERTED.[AccessAllTransTypes] !=
				DELETED.[AccessAllTransTypes]
			)
		) 
		END		
		
      If UPDATE([RPChangeRevFormula])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeRevFormula',
      CONVERT(NVARCHAR(2000),DELETED.[RPChangeRevFormula],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPChangeRevFormula],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPChangeRevFormula] Is Null And
				DELETED.[RPChangeRevFormula] Is Not Null
			) Or
			(
				INSERTED.[RPChangeRevFormula] Is Not Null And
				DELETED.[RPChangeRevFormula] Is Null
			) Or
			(
				INSERTED.[RPChangeRevFormula] !=
				DELETED.[RPChangeRevFormula]
			)
		) 
		END		
		
      If UPDATE([AllowAddWebParts])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowAddWebParts',
      CONVERT(NVARCHAR(2000),DELETED.[AllowAddWebParts],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowAddWebParts],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowAddWebParts] Is Null And
				DELETED.[AllowAddWebParts] Is Not Null
			) Or
			(
				INSERTED.[AllowAddWebParts] Is Not Null And
				DELETED.[AllowAddWebParts] Is Null
			) Or
			(
				INSERTED.[AllowAddWebParts] !=
				DELETED.[AllowAddWebParts]
			)
		) 
		END		
		
      If UPDATE([AllowCalcFields])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowCalcFields',
      CONVERT(NVARCHAR(2000),DELETED.[AllowCalcFields],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowCalcFields],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowCalcFields] Is Null And
				DELETED.[AllowCalcFields] Is Not Null
			) Or
			(
				INSERTED.[AllowCalcFields] Is Not Null And
				DELETED.[AllowCalcFields] Is Null
			) Or
			(
				INSERTED.[AllowCalcFields] !=
				DELETED.[AllowCalcFields]
			)
		) 
		END		
		
      If UPDATE([AllowAddDashParts])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowAddDashParts',
      CONVERT(NVARCHAR(2000),DELETED.[AllowAddDashParts],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowAddDashParts],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowAddDashParts] Is Null And
				DELETED.[AllowAddDashParts] Is Not Null
			) Or
			(
				INSERTED.[AllowAddDashParts] Is Not Null And
				DELETED.[AllowAddDashParts] Is Null
			) Or
			(
				INSERTED.[AllowAddDashParts] !=
				DELETED.[AllowAddDashParts]
			)
		) 
		END		
		
      If UPDATE([AccessAll20DashParts])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAll20DashParts',
      CONVERT(NVARCHAR(2000),DELETED.[AccessAll20DashParts],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccessAll20DashParts],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AccessAll20DashParts] Is Null And
				DELETED.[AccessAll20DashParts] Is Not Null
			) Or
			(
				INSERTED.[AccessAll20DashParts] Is Not Null And
				DELETED.[AccessAll20DashParts] Is Null
			) Or
			(
				INSERTED.[AccessAll20DashParts] !=
				DELETED.[AccessAll20DashParts]
			)
		) 
		END		
		
      If UPDATE([AccessAllCompanies])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllCompanies',
      CONVERT(NVARCHAR(2000),DELETED.[AccessAllCompanies],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccessAllCompanies],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AccessAllCompanies] Is Null And
				DELETED.[AccessAllCompanies] Is Not Null
			) Or
			(
				INSERTED.[AccessAllCompanies] Is Not Null And
				DELETED.[AccessAllCompanies] Is Null
			) Or
			(
				INSERTED.[AccessAllCompanies] !=
				DELETED.[AccessAllCompanies]
			)
		) 
		END		
		
      If UPDATE([RPChangeOverheadPct])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeOverheadPct',
      CONVERT(NVARCHAR(2000),DELETED.[RPChangeOverheadPct],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPChangeOverheadPct],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPChangeOverheadPct] Is Null And
				DELETED.[RPChangeOverheadPct] Is Not Null
			) Or
			(
				INSERTED.[RPChangeOverheadPct] Is Not Null And
				DELETED.[RPChangeOverheadPct] Is Null
			) Or
			(
				INSERTED.[RPChangeOverheadPct] !=
				DELETED.[RPChangeOverheadPct]
			)
		) 
		END		
		
      If UPDATE([POPrintPO])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'POPrintPO',
      CONVERT(NVARCHAR(2000),DELETED.[POPrintPO],121),
      CONVERT(NVARCHAR(2000),INSERTED.[POPrintPO],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[POPrintPO] Is Null And
				DELETED.[POPrintPO] Is Not Null
			) Or
			(
				INSERTED.[POPrintPO] Is Not Null And
				DELETED.[POPrintPO] Is Null
			) Or
			(
				INSERTED.[POPrintPO] !=
				DELETED.[POPrintPO]
			)
		) 
		END		
		
      If UPDATE([PODeleteOpenPO])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'PODeleteOpenPO',
      CONVERT(NVARCHAR(2000),DELETED.[PODeleteOpenPO],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PODeleteOpenPO],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[PODeleteOpenPO] Is Null And
				DELETED.[PODeleteOpenPO] Is Not Null
			) Or
			(
				INSERTED.[PODeleteOpenPO] Is Not Null And
				DELETED.[PODeleteOpenPO] Is Null
			) Or
			(
				INSERTED.[PODeleteOpenPO] !=
				DELETED.[PODeleteOpenPO]
			)
		) 
		END		
		
      If UPDATE([POReleasePO])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'POReleasePO',
      CONVERT(NVARCHAR(2000),DELETED.[POReleasePO],121),
      CONVERT(NVARCHAR(2000),INSERTED.[POReleasePO],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[POReleasePO] Is Null And
				DELETED.[POReleasePO] Is Not Null
			) Or
			(
				INSERTED.[POReleasePO] Is Not Null And
				DELETED.[POReleasePO] Is Null
			) Or
			(
				INSERTED.[POReleasePO] !=
				DELETED.[POReleasePO]
			)
		) 
		END		
		
      If UPDATE([POUnreceive])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'POUnreceive',
      CONVERT(NVARCHAR(2000),DELETED.[POUnreceive],121),
      CONVERT(NVARCHAR(2000),INSERTED.[POUnreceive],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[POUnreceive] Is Null And
				DELETED.[POUnreceive] Is Not Null
			) Or
			(
				INSERTED.[POUnreceive] Is Not Null And
				DELETED.[POUnreceive] Is Null
			) Or
			(
				INSERTED.[POUnreceive] !=
				DELETED.[POUnreceive]
			)
		) 
		END		
		
      If UPDATE([POClosePO])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'POClosePO',
      CONVERT(NVARCHAR(2000),DELETED.[POClosePO],121),
      CONVERT(NVARCHAR(2000),INSERTED.[POClosePO],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[POClosePO] Is Null And
				DELETED.[POClosePO] Is Not Null
			) Or
			(
				INSERTED.[POClosePO] Is Not Null And
				DELETED.[POClosePO] Is Null
			) Or
			(
				INSERTED.[POClosePO] !=
				DELETED.[POClosePO]
			)
		) 
		END		
		
      If UPDATE([PODeleteOpenPR])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'PODeleteOpenPR',
      CONVERT(NVARCHAR(2000),DELETED.[PODeleteOpenPR],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PODeleteOpenPR],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[PODeleteOpenPR] Is Null And
				DELETED.[PODeleteOpenPR] Is Not Null
			) Or
			(
				INSERTED.[PODeleteOpenPR] Is Not Null And
				DELETED.[PODeleteOpenPR] Is Null
			) Or
			(
				INSERTED.[PODeleteOpenPR] !=
				DELETED.[PODeleteOpenPR]
			)
		) 
		END		
		
      If UPDATE([FinalBatchBillingAccess])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'FinalBatchBillingAccess',
      CONVERT(NVARCHAR(2000),DELETED.[FinalBatchBillingAccess],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FinalBatchBillingAccess],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[FinalBatchBillingAccess] Is Null And
				DELETED.[FinalBatchBillingAccess] Is Not Null
			) Or
			(
				INSERTED.[FinalBatchBillingAccess] Is Not Null And
				DELETED.[FinalBatchBillingAccess] Is Null
			) Or
			(
				INSERTED.[FinalBatchBillingAccess] !=
				DELETED.[FinalBatchBillingAccess]
			)
		) 
		END		
		
      If UPDATE([AllowSQLWhereInLookup])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowSQLWhereInLookup',
      CONVERT(NVARCHAR(2000),DELETED.[AllowSQLWhereInLookup],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowSQLWhereInLookup],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowSQLWhereInLookup] Is Null And
				DELETED.[AllowSQLWhereInLookup] Is Not Null
			) Or
			(
				INSERTED.[AllowSQLWhereInLookup] Is Not Null And
				DELETED.[AllowSQLWhereInLookup] Is Null
			) Or
			(
				INSERTED.[AllowSQLWhereInLookup] !=
				DELETED.[AllowSQLWhereInLookup]
			)
		) 
		END		
		
      If UPDATE([RPUpdatePlanLabor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPUpdatePlanLabor',
      CONVERT(NVARCHAR(2000),DELETED.[RPUpdatePlanLabor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPUpdatePlanLabor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPUpdatePlanLabor] Is Null And
				DELETED.[RPUpdatePlanLabor] Is Not Null
			) Or
			(
				INSERTED.[RPUpdatePlanLabor] Is Not Null And
				DELETED.[RPUpdatePlanLabor] Is Null
			) Or
			(
				INSERTED.[RPUpdatePlanLabor] !=
				DELETED.[RPUpdatePlanLabor]
			)
		) 
		END		
		
      If UPDATE([AccessDocMgt])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessDocMgt',
      CONVERT(NVARCHAR(2000),DELETED.[AccessDocMgt],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccessDocMgt],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AccessDocMgt] Is Null And
				DELETED.[AccessDocMgt] Is Not Null
			) Or
			(
				INSERTED.[AccessDocMgt] Is Not Null And
				DELETED.[AccessDocMgt] Is Null
			) Or
			(
				INSERTED.[AccessDocMgt] !=
				DELETED.[AccessDocMgt]
			)
		) 
		END		
		
      If UPDATE([RPEMRow])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPEMRow',
      CONVERT(NVARCHAR(2000),DELETED.[RPEMRow],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPEMRow],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPEMRow] Is Null And
				DELETED.[RPEMRow] Is Not Null
			) Or
			(
				INSERTED.[RPEMRow] Is Not Null And
				DELETED.[RPEMRow] Is Null
			) Or
			(
				INSERTED.[RPEMRow] !=
				DELETED.[RPEMRow]
			)
		) 
		END		
		
      If UPDATE([SeeCostRatesIgnoreProfitCalc])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'SeeCostRatesIgnoreProfitCalc',
      CONVERT(NVARCHAR(2000),DELETED.[SeeCostRatesIgnoreProfitCalc],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SeeCostRatesIgnoreProfitCalc],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[SeeCostRatesIgnoreProfitCalc] Is Null And
				DELETED.[SeeCostRatesIgnoreProfitCalc] Is Not Null
			) Or
			(
				INSERTED.[SeeCostRatesIgnoreProfitCalc] Is Not Null And
				DELETED.[SeeCostRatesIgnoreProfitCalc] Is Null
			) Or
			(
				INSERTED.[SeeCostRatesIgnoreProfitCalc] !=
				DELETED.[SeeCostRatesIgnoreProfitCalc]
			)
		) 
		END		
		
      If UPDATE([SeeBurdenRatesIgnoreProfitCalc])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'SeeBurdenRatesIgnoreProfitCalc',
      CONVERT(NVARCHAR(2000),DELETED.[SeeBurdenRatesIgnoreProfitCalc],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SeeBurdenRatesIgnoreProfitCalc],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[SeeBurdenRatesIgnoreProfitCalc] Is Null And
				DELETED.[SeeBurdenRatesIgnoreProfitCalc] Is Not Null
			) Or
			(
				INSERTED.[SeeBurdenRatesIgnoreProfitCalc] Is Not Null And
				DELETED.[SeeBurdenRatesIgnoreProfitCalc] Is Null
			) Or
			(
				INSERTED.[SeeBurdenRatesIgnoreProfitCalc] !=
				DELETED.[SeeBurdenRatesIgnoreProfitCalc]
			)
		) 
		END		
		
      If UPDATE([AllowIntercompanyReceipts])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowIntercompanyReceipts',
      CONVERT(NVARCHAR(2000),DELETED.[AllowIntercompanyReceipts],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowIntercompanyReceipts],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowIntercompanyReceipts] Is Null And
				DELETED.[AllowIntercompanyReceipts] Is Not Null
			) Or
			(
				INSERTED.[AllowIntercompanyReceipts] Is Not Null And
				DELETED.[AllowIntercompanyReceipts] Is Null
			) Or
			(
				INSERTED.[AllowIntercompanyReceipts] !=
				DELETED.[AllowIntercompanyReceipts]
			)
		) 
		END		
		
      If UPDATE([InteractiveBillingModify])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingModify',
      CONVERT(NVARCHAR(2000),DELETED.[InteractiveBillingModify],121),
      CONVERT(NVARCHAR(2000),INSERTED.[InteractiveBillingModify],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[InteractiveBillingModify] Is Null And
				DELETED.[InteractiveBillingModify] Is Not Null
			) Or
			(
				INSERTED.[InteractiveBillingModify] Is Not Null And
				DELETED.[InteractiveBillingModify] Is Null
			) Or
			(
				INSERTED.[InteractiveBillingModify] !=
				DELETED.[InteractiveBillingModify]
			)
		) 
		END		
		
      If UPDATE([InteractiveBillingDelete])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingDelete',
      CONVERT(NVARCHAR(2000),DELETED.[InteractiveBillingDelete],121),
      CONVERT(NVARCHAR(2000),INSERTED.[InteractiveBillingDelete],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[InteractiveBillingDelete] Is Null And
				DELETED.[InteractiveBillingDelete] Is Not Null
			) Or
			(
				INSERTED.[InteractiveBillingDelete] Is Not Null And
				DELETED.[InteractiveBillingDelete] Is Null
			) Or
			(
				INSERTED.[InteractiveBillingDelete] !=
				DELETED.[InteractiveBillingDelete]
			)
		) 
		END		
		
      If UPDATE([InteractiveBillingWriteOff])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingWriteOff',
      CONVERT(NVARCHAR(2000),DELETED.[InteractiveBillingWriteOff],121),
      CONVERT(NVARCHAR(2000),INSERTED.[InteractiveBillingWriteOff],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[InteractiveBillingWriteOff] Is Null And
				DELETED.[InteractiveBillingWriteOff] Is Not Null
			) Or
			(
				INSERTED.[InteractiveBillingWriteOff] Is Not Null And
				DELETED.[InteractiveBillingWriteOff] Is Null
			) Or
			(
				INSERTED.[InteractiveBillingWriteOff] !=
				DELETED.[InteractiveBillingWriteOff]
			)
		) 
		END		
		
      If UPDATE([InteractiveBillingFinalAccess])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingFinalAccess',
      CONVERT(NVARCHAR(2000),DELETED.[InteractiveBillingFinalAccess],121),
      CONVERT(NVARCHAR(2000),INSERTED.[InteractiveBillingFinalAccess],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[InteractiveBillingFinalAccess] Is Null And
				DELETED.[InteractiveBillingFinalAccess] Is Not Null
			) Or
			(
				INSERTED.[InteractiveBillingFinalAccess] Is Not Null And
				DELETED.[InteractiveBillingFinalAccess] Is Null
			) Or
			(
				INSERTED.[InteractiveBillingFinalAccess] !=
				DELETED.[InteractiveBillingFinalAccess]
			)
		) 
		END		
		
      If UPDATE([InteractiveBillingHold])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingHold',
      CONVERT(NVARCHAR(2000),DELETED.[InteractiveBillingHold],121),
      CONVERT(NVARCHAR(2000),INSERTED.[InteractiveBillingHold],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[InteractiveBillingHold] Is Null And
				DELETED.[InteractiveBillingHold] Is Not Null
			) Or
			(
				INSERTED.[InteractiveBillingHold] Is Not Null And
				DELETED.[InteractiveBillingHold] Is Null
			) Or
			(
				INSERTED.[InteractiveBillingHold] !=
				DELETED.[InteractiveBillingHold]
			)
		) 
		END		
		
      If UPDATE([InteractiveBillingInsert])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingInsert',
      CONVERT(NVARCHAR(2000),DELETED.[InteractiveBillingInsert],121),
      CONVERT(NVARCHAR(2000),INSERTED.[InteractiveBillingInsert],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[InteractiveBillingInsert] Is Null And
				DELETED.[InteractiveBillingInsert] Is Not Null
			) Or
			(
				INSERTED.[InteractiveBillingInsert] Is Not Null And
				DELETED.[InteractiveBillingInsert] Is Null
			) Or
			(
				INSERTED.[InteractiveBillingInsert] !=
				DELETED.[InteractiveBillingInsert]
			)
		) 
		END		
		
      If UPDATE([InteractiveBillingTransfer])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingTransfer',
      CONVERT(NVARCHAR(2000),DELETED.[InteractiveBillingTransfer],121),
      CONVERT(NVARCHAR(2000),INSERTED.[InteractiveBillingTransfer],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[InteractiveBillingTransfer] Is Null And
				DELETED.[InteractiveBillingTransfer] Is Not Null
			) Or
			(
				INSERTED.[InteractiveBillingTransfer] Is Not Null And
				DELETED.[InteractiveBillingTransfer] Is Null
			) Or
			(
				INSERTED.[InteractiveBillingTransfer] !=
				DELETED.[InteractiveBillingTransfer]
			)
		) 
		END		
		
      If UPDATE([InteractiveBillingEditedInvoices])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingEditedInvoices',
      CONVERT(NVARCHAR(2000),DELETED.[InteractiveBillingEditedInvoices],121),
      CONVERT(NVARCHAR(2000),INSERTED.[InteractiveBillingEditedInvoices],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[InteractiveBillingEditedInvoices] Is Null And
				DELETED.[InteractiveBillingEditedInvoices] Is Not Null
			) Or
			(
				INSERTED.[InteractiveBillingEditedInvoices] Is Not Null And
				DELETED.[InteractiveBillingEditedInvoices] Is Null
			) Or
			(
				INSERTED.[InteractiveBillingEditedInvoices] !=
				DELETED.[InteractiveBillingEditedInvoices]
			)
		) 
		END		
		
      If UPDATE([EmailTemplateAdmin])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'EmailTemplateAdmin',
      CONVERT(NVARCHAR(2000),DELETED.[EmailTemplateAdmin],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EmailTemplateAdmin],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[EmailTemplateAdmin] Is Null And
				DELETED.[EmailTemplateAdmin] Is Not Null
			) Or
			(
				INSERTED.[EmailTemplateAdmin] Is Not Null And
				DELETED.[EmailTemplateAdmin] Is Null
			) Or
			(
				INSERTED.[EmailTemplateAdmin] !=
				DELETED.[EmailTemplateAdmin]
			)
		) 
		END		
		
      If UPDATE([RPHardBooking])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPHardBooking',
      CONVERT(NVARCHAR(2000),DELETED.[RPHardBooking],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPHardBooking],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPHardBooking] Is Null And
				DELETED.[RPHardBooking] Is Not Null
			) Or
			(
				INSERTED.[RPHardBooking] Is Not Null And
				DELETED.[RPHardBooking] Is Null
			) Or
			(
				INSERTED.[RPHardBooking] !=
				DELETED.[RPHardBooking]
			)
		) 
		END		
		
      If UPDATE([ContactDownload])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ContactDownload',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([ClientDownload])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ClientDownload',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([ActivityDownload])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'ActivityDownload',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([OpportunityDownload])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'OpportunityDownload',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([RPChangeAssignments])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeAssignments',
      CONVERT(NVARCHAR(2000),DELETED.[RPChangeAssignments],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPChangeAssignments],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPChangeAssignments] Is Null And
				DELETED.[RPChangeAssignments] Is Not Null
			) Or
			(
				INSERTED.[RPChangeAssignments] Is Not Null And
				DELETED.[RPChangeAssignments] Is Null
			) Or
			(
				INSERTED.[RPChangeAssignments] !=
				DELETED.[RPChangeAssignments]
			)
		) 
		END		
		
      If UPDATE([AllowAcrossICLookup])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowAcrossICLookup',
      CONVERT(NVARCHAR(2000),DELETED.[AllowAcrossICLookup],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowAcrossICLookup],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowAcrossICLookup] Is Null And
				DELETED.[AllowAcrossICLookup] Is Not Null
			) Or
			(
				INSERTED.[AllowAcrossICLookup] Is Not Null And
				DELETED.[AllowAcrossICLookup] Is Null
			) Or
			(
				INSERTED.[AllowAcrossICLookup] !=
				DELETED.[AllowAcrossICLookup]
			)
		) 
		END		
		
      If UPDATE([AllowReprocessIntercompanyBilling])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowReprocessIntercompanyBilling',
      CONVERT(NVARCHAR(2000),DELETED.[AllowReprocessIntercompanyBilling],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowReprocessIntercompanyBilling],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowReprocessIntercompanyBilling] Is Null And
				DELETED.[AllowReprocessIntercompanyBilling] Is Not Null
			) Or
			(
				INSERTED.[AllowReprocessIntercompanyBilling] Is Not Null And
				DELETED.[AllowReprocessIntercompanyBilling] Is Null
			) Or
			(
				INSERTED.[AllowReprocessIntercompanyBilling] !=
				DELETED.[AllowReprocessIntercompanyBilling]
			)
		) 
		END		
		
      If UPDATE([EmployeeSelfServiceInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'EmployeeSelfServiceInd',
      CONVERT(NVARCHAR(2000),DELETED.[EmployeeSelfServiceInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EmployeeSelfServiceInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[EmployeeSelfServiceInd] Is Null And
				DELETED.[EmployeeSelfServiceInd] Is Not Null
			) Or
			(
				INSERTED.[EmployeeSelfServiceInd] Is Not Null And
				DELETED.[EmployeeSelfServiceInd] Is Null
			) Or
			(
				INSERTED.[EmployeeSelfServiceInd] !=
				DELETED.[EmployeeSelfServiceInd]
			)
		) 
		END		
		
      If UPDATE([AllowChangeDisableLogin])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowChangeDisableLogin',
      CONVERT(NVARCHAR(2000),DELETED.[AllowChangeDisableLogin],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowChangeDisableLogin],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowChangeDisableLogin] Is Null And
				DELETED.[AllowChangeDisableLogin] Is Not Null
			) Or
			(
				INSERTED.[AllowChangeDisableLogin] Is Not Null And
				DELETED.[AllowChangeDisableLogin] Is Null
			) Or
			(
				INSERTED.[AllowChangeDisableLogin] !=
				DELETED.[AllowChangeDisableLogin]
			)
		) 
		END		
		
      If UPDATE([AllowChangePassword])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowChangePassword',
      CONVERT(NVARCHAR(2000),DELETED.[AllowChangePassword],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowChangePassword],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowChangePassword] Is Null And
				DELETED.[AllowChangePassword] Is Not Null
			) Or
			(
				INSERTED.[AllowChangePassword] Is Not Null And
				DELETED.[AllowChangePassword] Is Null
			) Or
			(
				INSERTED.[AllowChangePassword] !=
				DELETED.[AllowChangePassword]
			)
		) 
		END		
		
      If UPDATE([AllowChangeSupportDocument])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowChangeSupportDocument',
      CONVERT(NVARCHAR(2000),DELETED.[AllowChangeSupportDocument],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowChangeSupportDocument],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowChangeSupportDocument] Is Null And
				DELETED.[AllowChangeSupportDocument] Is Not Null
			) Or
			(
				INSERTED.[AllowChangeSupportDocument] Is Not Null And
				DELETED.[AllowChangeSupportDocument] Is Null
			) Or
			(
				INSERTED.[AllowChangeSupportDocument] !=
				DELETED.[AllowChangeSupportDocument]
			)
		) 
		END		
		
      If UPDATE([Navigator])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'Navigator',
      CONVERT(NVARCHAR(2000),DELETED.[Navigator],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Navigator],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[Navigator] Is Null And
				DELETED.[Navigator] Is Not Null
			) Or
			(
				INSERTED.[Navigator] Is Not Null And
				DELETED.[Navigator] Is Null
			) Or
			(
				INSERTED.[Navigator] !=
				DELETED.[Navigator]
			)
		) 
		END		
		
      If UPDATE([NavEmpWorkspace])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavEmpWorkspace',
      CONVERT(NVARCHAR(2000),DELETED.[NavEmpWorkspace],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavEmpWorkspace],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavEmpWorkspace] Is Null And
				DELETED.[NavEmpWorkspace] Is Not Null
			) Or
			(
				INSERTED.[NavEmpWorkspace] Is Not Null And
				DELETED.[NavEmpWorkspace] Is Null
			) Or
			(
				INSERTED.[NavEmpWorkspace] !=
				DELETED.[NavEmpWorkspace]
			)
		) 
		END		
		
      If UPDATE([NavPMWorkspace])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavPMWorkspace',
      CONVERT(NVARCHAR(2000),DELETED.[NavPMWorkspace],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavPMWorkspace],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavPMWorkspace] Is Null And
				DELETED.[NavPMWorkspace] Is Not Null
			) Or
			(
				INSERTED.[NavPMWorkspace] Is Not Null And
				DELETED.[NavPMWorkspace] Is Null
			) Or
			(
				INSERTED.[NavPMWorkspace] !=
				DELETED.[NavPMWorkspace]
			)
		) 
		END		
		
      If UPDATE([NavTimesheet])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavTimesheet',
      CONVERT(NVARCHAR(2000),DELETED.[NavTimesheet],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavTimesheet],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavTimesheet] Is Null And
				DELETED.[NavTimesheet] Is Not Null
			) Or
			(
				INSERTED.[NavTimesheet] Is Not Null And
				DELETED.[NavTimesheet] Is Null
			) Or
			(
				INSERTED.[NavTimesheet] !=
				DELETED.[NavTimesheet]
			)
		) 
		END		
		
      If UPDATE([NavDashboard])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavDashboard',
      CONVERT(NVARCHAR(2000),DELETED.[NavDashboard],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavDashboard],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavDashboard] Is Null And
				DELETED.[NavDashboard] Is Not Null
			) Or
			(
				INSERTED.[NavDashboard] Is Not Null And
				DELETED.[NavDashboard] Is Null
			) Or
			(
				INSERTED.[NavDashboard] !=
				DELETED.[NavDashboard]
			)
		) 
		END		
		
      If UPDATE([NavPlanning])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavPlanning',
      CONVERT(NVARCHAR(2000),DELETED.[NavPlanning],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavPlanning],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavPlanning] Is Null And
				DELETED.[NavPlanning] Is Not Null
			) Or
			(
				INSERTED.[NavPlanning] Is Not Null And
				DELETED.[NavPlanning] Is Null
			) Or
			(
				INSERTED.[NavPlanning] !=
				DELETED.[NavPlanning]
			)
		) 
		END		
		
      If UPDATE([NavLabor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavLabor',
      CONVERT(NVARCHAR(2000),DELETED.[NavLabor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavLabor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavLabor] Is Null And
				DELETED.[NavLabor] Is Not Null
			) Or
			(
				INSERTED.[NavLabor] Is Not Null And
				DELETED.[NavLabor] Is Null
			) Or
			(
				INSERTED.[NavLabor] !=
				DELETED.[NavLabor]
			)
		) 
		END		
		
      If UPDATE([NavConsultant])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavConsultant',
      CONVERT(NVARCHAR(2000),DELETED.[NavConsultant],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavConsultant],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavConsultant] Is Null And
				DELETED.[NavConsultant] Is Not Null
			) Or
			(
				INSERTED.[NavConsultant] Is Not Null And
				DELETED.[NavConsultant] Is Null
			) Or
			(
				INSERTED.[NavConsultant] !=
				DELETED.[NavConsultant]
			)
		) 
		END		
		
      If UPDATE([NavExpense])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavExpense',
      CONVERT(NVARCHAR(2000),DELETED.[NavExpense],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavExpense],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavExpense] Is Null And
				DELETED.[NavExpense] Is Not Null
			) Or
			(
				INSERTED.[NavExpense] Is Not Null And
				DELETED.[NavExpense] Is Null
			) Or
			(
				INSERTED.[NavExpense] !=
				DELETED.[NavExpense]
			)
		) 
		END		
		
      If UPDATE([NavContract])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavContract',
      CONVERT(NVARCHAR(2000),DELETED.[NavContract],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavContract],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavContract] Is Null And
				DELETED.[NavContract] Is Not Null
			) Or
			(
				INSERTED.[NavContract] Is Not Null And
				DELETED.[NavContract] Is Null
			) Or
			(
				INSERTED.[NavContract] !=
				DELETED.[NavContract]
			)
		) 
		END		
		
      If UPDATE([NavUseProjectAccess])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavUseProjectAccess',
      CONVERT(NVARCHAR(2000),DELETED.[NavUseProjectAccess],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavUseProjectAccess],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavUseProjectAccess] Is Null And
				DELETED.[NavUseProjectAccess] Is Not Null
			) Or
			(
				INSERTED.[NavUseProjectAccess] Is Not Null And
				DELETED.[NavUseProjectAccess] Is Null
			) Or
			(
				INSERTED.[NavUseProjectAccess] !=
				DELETED.[NavUseProjectAccess]
			)
		) 
		END		
		
      If UPDATE([NavUsePlanningAccess])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavUsePlanningAccess',
      CONVERT(NVARCHAR(2000),DELETED.[NavUsePlanningAccess],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavUsePlanningAccess],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavUsePlanningAccess] Is Null And
				DELETED.[NavUsePlanningAccess] Is Not Null
			) Or
			(
				INSERTED.[NavUsePlanningAccess] Is Not Null And
				DELETED.[NavUsePlanningAccess] Is Null
			) Or
			(
				INSERTED.[NavUsePlanningAccess] !=
				DELETED.[NavUsePlanningAccess]
			)
		) 
		END		
		
      If UPDATE([NavUseProjectView])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavUseProjectView',
      CONVERT(NVARCHAR(2000),DELETED.[NavUseProjectView],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavUseProjectView],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavUseProjectView] Is Null And
				DELETED.[NavUseProjectView] Is Not Null
			) Or
			(
				INSERTED.[NavUseProjectView] Is Not Null And
				DELETED.[NavUseProjectView] Is Null
			) Or
			(
				INSERTED.[NavUseProjectView] !=
				DELETED.[NavUseProjectView]
			)
		) 
		END		
		
      If UPDATE([NavUseProjectUpdate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavUseProjectUpdate',
      CONVERT(NVARCHAR(2000),DELETED.[NavUseProjectUpdate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavUseProjectUpdate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavUseProjectUpdate] Is Null And
				DELETED.[NavUseProjectUpdate] Is Not Null
			) Or
			(
				INSERTED.[NavUseProjectUpdate] Is Not Null And
				DELETED.[NavUseProjectUpdate] Is Null
			) Or
			(
				INSERTED.[NavUseProjectUpdate] !=
				DELETED.[NavUseProjectUpdate]
			)
		) 
		END		
		
      If UPDATE([NavUsePlanningView])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavUsePlanningView',
      CONVERT(NVARCHAR(2000),DELETED.[NavUsePlanningView],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavUsePlanningView],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavUsePlanningView] Is Null And
				DELETED.[NavUsePlanningView] Is Not Null
			) Or
			(
				INSERTED.[NavUsePlanningView] Is Not Null And
				DELETED.[NavUsePlanningView] Is Null
			) Or
			(
				INSERTED.[NavUsePlanningView] !=
				DELETED.[NavUsePlanningView]
			)
		) 
		END		
		
      If UPDATE([NavUsePlanningUpdate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavUsePlanningUpdate',
      CONVERT(NVARCHAR(2000),DELETED.[NavUsePlanningUpdate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavUsePlanningUpdate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavUsePlanningUpdate] Is Null And
				DELETED.[NavUsePlanningUpdate] Is Not Null
			) Or
			(
				INSERTED.[NavUsePlanningUpdate] Is Not Null And
				DELETED.[NavUsePlanningUpdate] Is Null
			) Or
			(
				INSERTED.[NavUsePlanningUpdate] !=
				DELETED.[NavUsePlanningUpdate]
			)
		) 
		END		
		
      If UPDATE([NavExpRpt])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavExpRpt',
      CONVERT(NVARCHAR(2000),DELETED.[NavExpRpt],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavExpRpt],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavExpRpt] Is Null And
				DELETED.[NavExpRpt] Is Not Null
			) Or
			(
				INSERTED.[NavExpRpt] Is Not Null And
				DELETED.[NavExpRpt] Is Null
			) Or
			(
				INSERTED.[NavExpRpt] !=
				DELETED.[NavExpRpt]
			)
		) 
		END		
		
      If UPDATE([NavInvoice])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavInvoice',
      CONVERT(NVARCHAR(2000),DELETED.[NavInvoice],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavInvoice],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavInvoice] Is Null And
				DELETED.[NavInvoice] Is Not Null
			) Or
			(
				INSERTED.[NavInvoice] Is Not Null And
				DELETED.[NavInvoice] Is Null
			) Or
			(
				INSERTED.[NavInvoice] !=
				DELETED.[NavInvoice]
			)
		) 
		END		
		
      If UPDATE([NavCRMWorkspace])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'NavCRMWorkspace',
      CONVERT(NVARCHAR(2000),DELETED.[NavCRMWorkspace],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NavCRMWorkspace],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[NavCRMWorkspace] Is Null And
				DELETED.[NavCRMWorkspace] Is Not Null
			) Or
			(
				INSERTED.[NavCRMWorkspace] Is Not Null And
				DELETED.[NavCRMWorkspace] Is Null
			) Or
			(
				INSERTED.[NavCRMWorkspace] !=
				DELETED.[NavCRMWorkspace]
			)
		) 
		END		
		
      If UPDATE([RPChangeCompensationExp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPChangeCompensationExp',
      CONVERT(NVARCHAR(2000),DELETED.[RPChangeCompensationExp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPChangeCompensationExp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPChangeCompensationExp] Is Null And
				DELETED.[RPChangeCompensationExp] Is Not Null
			) Or
			(
				INSERTED.[RPChangeCompensationExp] Is Not Null And
				DELETED.[RPChangeCompensationExp] Is Null
			) Or
			(
				INSERTED.[RPChangeCompensationExp] !=
				DELETED.[RPChangeCompensationExp]
			)
		) 
		END		
		
      If UPDATE([RPReimbursableAllowanceCon])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'RPReimbursableAllowanceCon',
      CONVERT(NVARCHAR(2000),DELETED.[RPReimbursableAllowanceCon],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RPReimbursableAllowanceCon],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[RPReimbursableAllowanceCon] Is Null And
				DELETED.[RPReimbursableAllowanceCon] Is Not Null
			) Or
			(
				INSERTED.[RPReimbursableAllowanceCon] Is Not Null And
				DELETED.[RPReimbursableAllowanceCon] Is Null
			) Or
			(
				INSERTED.[RPReimbursableAllowanceCon] !=
				DELETED.[RPReimbursableAllowanceCon]
			)
		) 
		END		
		
      If UPDATE([AllowLookupLimitOverride])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowLookupLimitOverride',
      CONVERT(NVARCHAR(2000),DELETED.[AllowLookupLimitOverride],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowLookupLimitOverride],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowLookupLimitOverride] Is Null And
				DELETED.[AllowLookupLimitOverride] Is Not Null
			) Or
			(
				INSERTED.[AllowLookupLimitOverride] Is Not Null And
				DELETED.[AllowLookupLimitOverride] Is Null
			) Or
			(
				INSERTED.[AllowLookupLimitOverride] !=
				DELETED.[AllowLookupLimitOverride]
			)
		) 
		END		
		
      If UPDATE([AllowEditPriorDepreciation])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowEditPriorDepreciation',
      CONVERT(NVARCHAR(2000),DELETED.[AllowEditPriorDepreciation],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowEditPriorDepreciation],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowEditPriorDepreciation] Is Null And
				DELETED.[AllowEditPriorDepreciation] Is Not Null
			) Or
			(
				INSERTED.[AllowEditPriorDepreciation] Is Not Null And
				DELETED.[AllowEditPriorDepreciation] Is Null
			) Or
			(
				INSERTED.[AllowEditPriorDepreciation] !=
				DELETED.[AllowEditPriorDepreciation]
			)
		) 
		END		
		
      If UPDATE([AllowAssetEntries])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowAssetEntries',
      CONVERT(NVARCHAR(2000),DELETED.[AllowAssetEntries],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowAssetEntries],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowAssetEntries] Is Null And
				DELETED.[AllowAssetEntries] Is Not Null
			) Or
			(
				INSERTED.[AllowAssetEntries] Is Not Null And
				DELETED.[AllowAssetEntries] Is Null
			) Or
			(
				INSERTED.[AllowAssetEntries] !=
				DELETED.[AllowAssetEntries]
			)
		) 
		END		
		
      If UPDATE([SystemSetup])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'SystemSetup',
      CONVERT(NVARCHAR(2000),DELETED.[SystemSetup],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SystemSetup],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[SystemSetup] Is Null And
				DELETED.[SystemSetup] Is Not Null
			) Or
			(
				INSERTED.[SystemSetup] Is Not Null And
				DELETED.[SystemSetup] Is Null
			) Or
			(
				INSERTED.[SystemSetup] !=
				DELETED.[SystemSetup]
			)
		) 
		END		
		
      If UPDATE([AllowedPRCreateTypes])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowedPRCreateTypes',
      CONVERT(NVARCHAR(2000),DELETED.[AllowedPRCreateTypes],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowedPRCreateTypes],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowedPRCreateTypes] Is Null And
				DELETED.[AllowedPRCreateTypes] Is Not Null
			) Or
			(
				INSERTED.[AllowedPRCreateTypes] Is Not Null And
				DELETED.[AllowedPRCreateTypes] Is Null
			) Or
			(
				INSERTED.[AllowedPRCreateTypes] !=
				DELETED.[AllowedPRCreateTypes]
			)
		) 
		END		
		
      If UPDATE([AllowPRPromoCreate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowPRPromoCreate',
      CONVERT(NVARCHAR(2000),DELETED.[AllowPRPromoCreate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowPRPromoCreate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowPRPromoCreate] Is Null And
				DELETED.[AllowPRPromoCreate] Is Not Null
			) Or
			(
				INSERTED.[AllowPRPromoCreate] Is Not Null And
				DELETED.[AllowPRPromoCreate] Is Null
			) Or
			(
				INSERTED.[AllowPRPromoCreate] !=
				DELETED.[AllowPRPromoCreate]
			)
		) 
		END		
		
      If UPDATE([OrgRowLevelID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'OrgRowLevelID',
      CONVERT(NVARCHAR(2000),DELETED.[OrgRowLevelID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OrgRowLevelID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[OrgRowLevelID] Is Null And
				DELETED.[OrgRowLevelID] Is Not Null
			) Or
			(
				INSERTED.[OrgRowLevelID] Is Not Null And
				DELETED.[OrgRowLevelID] Is Null
			) Or
			(
				INSERTED.[OrgRowLevelID] !=
				DELETED.[OrgRowLevelID]
			)
		) 
		END		
		
      If UPDATE([DashboardAdmin])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'DashboardAdmin',
      CONVERT(NVARCHAR(2000),DELETED.[DashboardAdmin],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DashboardAdmin],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[DashboardAdmin] Is Null And
				DELETED.[DashboardAdmin] Is Not Null
			) Or
			(
				INSERTED.[DashboardAdmin] Is Not Null And
				DELETED.[DashboardAdmin] Is Null
			) Or
			(
				INSERTED.[DashboardAdmin] !=
				DELETED.[DashboardAdmin]
			)
		) 
		END		
		
      If UPDATE([DataPackAdmin])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'DataPackAdmin',
      CONVERT(NVARCHAR(2000),DELETED.[DataPackAdmin],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DataPackAdmin],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[DataPackAdmin] Is Null And
				DELETED.[DataPackAdmin] Is Not Null
			) Or
			(
				INSERTED.[DataPackAdmin] Is Not Null And
				DELETED.[DataPackAdmin] Is Null
			) Or
			(
				INSERTED.[DataPackAdmin] !=
				DELETED.[DataPackAdmin]
			)
		) 
		END		
		
      If UPDATE([AccessAllDataPacks])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllDataPacks',
      CONVERT(NVARCHAR(2000),DELETED.[AccessAllDataPacks],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccessAllDataPacks],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AccessAllDataPacks] Is Null And
				DELETED.[AccessAllDataPacks] Is Not Null
			) Or
			(
				INSERTED.[AccessAllDataPacks] Is Not Null And
				DELETED.[AccessAllDataPacks] Is Null
			) Or
			(
				INSERTED.[AccessAllDataPacks] !=
				DELETED.[AccessAllDataPacks]
			)
		) 
		END		
		
      If UPDATE([DataSourceAdmin])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'DataSourceAdmin',
      CONVERT(NVARCHAR(2000),DELETED.[DataSourceAdmin],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DataSourceAdmin],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[DataSourceAdmin] Is Null And
				DELETED.[DataSourceAdmin] Is Not Null
			) Or
			(
				INSERTED.[DataSourceAdmin] Is Not Null And
				DELETED.[DataSourceAdmin] Is Null
			) Or
			(
				INSERTED.[DataSourceAdmin] !=
				DELETED.[DataSourceAdmin]
			)
		) 
		END		
		
      If UPDATE([AccessAllDataSources])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AccessAllDataSources',
      CONVERT(NVARCHAR(2000),DELETED.[AccessAllDataSources],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccessAllDataSources],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AccessAllDataSources] Is Null And
				DELETED.[AccessAllDataSources] Is Not Null
			) Or
			(
				INSERTED.[AccessAllDataSources] Is Not Null And
				DELETED.[AccessAllDataSources] Is Null
			) Or
			(
				INSERTED.[AccessAllDataSources] !=
				DELETED.[AccessAllDataSources]
			)
		) 
		END		
		
      If UPDATE([InteractiveBillingAllowEditsAccept])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'InteractiveBillingAllowEditsAccept',
      CONVERT(NVARCHAR(2000),DELETED.[InteractiveBillingAllowEditsAccept],121),
      CONVERT(NVARCHAR(2000),INSERTED.[InteractiveBillingAllowEditsAccept],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[InteractiveBillingAllowEditsAccept] Is Null And
				DELETED.[InteractiveBillingAllowEditsAccept] Is Not Null
			) Or
			(
				INSERTED.[InteractiveBillingAllowEditsAccept] Is Not Null And
				DELETED.[InteractiveBillingAllowEditsAccept] Is Null
			) Or
			(
				INSERTED.[InteractiveBillingAllowEditsAccept] !=
				DELETED.[InteractiveBillingAllowEditsAccept]
			)
		) 
		END		
		
      If UPDATE([AllowAccessToVIDashpart])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowAccessToVIDashpart',
      CONVERT(NVARCHAR(2000),DELETED.[AllowAccessToVIDashpart],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowAccessToVIDashpart],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowAccessToVIDashpart] Is Null And
				DELETED.[AllowAccessToVIDashpart] Is Not Null
			) Or
			(
				INSERTED.[AllowAccessToVIDashpart] Is Not Null And
				DELETED.[AllowAccessToVIDashpart] Is Null
			) Or
			(
				INSERTED.[AllowAccessToVIDashpart] !=
				DELETED.[AllowAccessToVIDashpart]
			)
		) 
		END		
		
      If UPDATE([AllowCubeFullRefresh])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'AllowCubeFullRefresh',
      CONVERT(NVARCHAR(2000),DELETED.[AllowCubeFullRefresh],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowCubeFullRefresh],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[AllowCubeFullRefresh] Is Null And
				DELETED.[AllowCubeFullRefresh] Is Not Null
			) Or
			(
				INSERTED.[AllowCubeFullRefresh] Is Not Null And
				DELETED.[AllowCubeFullRefresh] Is Null
			) Or
			(
				INSERTED.[AllowCubeFullRefresh] !=
				DELETED.[AllowCubeFullRefresh]
			)
		) 
		END		
		
      If UPDATE([DeltekInternal])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'DeltekInternal',
      CONVERT(NVARCHAR(2000),DELETED.[DeltekInternal],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DeltekInternal],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[DeltekInternal] Is Null And
				DELETED.[DeltekInternal] Is Not Null
			) Or
			(
				INSERTED.[DeltekInternal] Is Not Null And
				DELETED.[DeltekInternal] Is Null
			) Or
			(
				INSERTED.[DeltekInternal] !=
				DELETED.[DeltekInternal]
			)
		) 
		END		
		
      If UPDATE([DeltekInternalInfo])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121),'DeltekInternalInfo',
      CONVERT(NVARCHAR(2000),DELETED.[DeltekInternalInfo],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DeltekInternalInfo],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND 
		(
			(
				INSERTED.[DeltekInternalInfo] Is Null And
				DELETED.[DeltekInternalInfo] Is Not Null
			) Or
			(
				INSERTED.[DeltekInternalInfo] Is Not Null And
				DELETED.[DeltekInternalInfo] Is Null
			) Or
			(
				INSERTED.[DeltekInternalInfo] !=
				DELETED.[DeltekInternalInfo]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[SE] ADD CONSTRAINT [SEPK] PRIMARY KEY NONCLUSTERED ([Role]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [SERoleIDX] ON [dbo].[SE] ([Role]) INCLUDE ([AccountingType], [MarketingType]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
