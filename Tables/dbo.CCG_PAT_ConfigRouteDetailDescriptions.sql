CREATE TABLE [dbo].[CCG_PAT_ConfigRouteDetailDescriptions]
(
[Route] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RouteType] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RouteValue] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Contains descriptions for route entries as used for multi-language companies', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRouteDetailDescriptions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Optional language specific description for the route entry', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRouteDetailDescriptions', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - Route', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRouteDetailDescriptions', 'COLUMN', N'Route'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Type of route entry - PR / EM / VE / SP', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRouteDetailDescriptions', 'COLUMN', N'RouteType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Value for the route type', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRouteDetailDescriptions', 'COLUMN', N'RouteValue'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The UI culture (language/region) code of this description', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRouteDetailDescriptions', 'COLUMN', N'UICultureName'
GO
