CREATE TABLE [dbo].[PYChecksWWage]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__PYChecksW__Perio__57D5E1A6] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__PYChecksW__PostS__58CA05DF] DEFAULT ((0)),
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodeWage] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__Amoun__59BE2A18] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PYChecksWWage] ADD CONSTRAINT [PYChecksWWagePK] PRIMARY KEY NONCLUSTERED ([Employee], [Period], [PostSeq], [Code], [CodeWage]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
