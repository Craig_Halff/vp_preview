CREATE TABLE [dbo].[VisionImportProjectInvoice_INV]
(
[PKEY] [varchar] (108) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransDate] [datetime] NOT NULL,
[Amount] [numeric] (30, 9) NULL,
[Invoice] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceSection] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RetainageAmount] [int] NOT NULL,
[Period] [int] NOT NULL
) ON [PRIMARY]
GO
