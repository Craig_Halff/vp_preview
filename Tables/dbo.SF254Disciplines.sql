CREATE TABLE [dbo].[SF254Disciplines]
(
[SF254ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Discipline] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__SF254Discip__Seq__008DDE76] DEFAULT ((0)),
[CompanyCount] [smallint] NOT NULL CONSTRAINT [DF__SF254Disc__Compa__018202AF] DEFAULT ((0)),
[IncludeCommentInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF254Disc__Inclu__027626E8] DEFAULT ('N'),
[DisciplineCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF254Disciplines] ADD CONSTRAINT [SF254DisciplinesPK] PRIMARY KEY NONCLUSTERED ([SF254ID], [Discipline]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
