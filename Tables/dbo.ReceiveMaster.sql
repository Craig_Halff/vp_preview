CREATE TABLE [dbo].[ReceiveMaster]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReceiveDate] [datetime] NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PackingSlip] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillofLading] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Memo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReceiveMaster] ADD CONSTRAINT [ReceiveMasterPK] PRIMARY KEY NONCLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
