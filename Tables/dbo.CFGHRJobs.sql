CREATE TABLE [dbo].[CFGHRJobs]
(
[JobID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGHRJobs__JobID__79628951] DEFAULT (left(replace(newid(),'-',''),(32))),
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TitleCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobLevelCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FLSAStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGHRJobs__FLSAS__7A56AD8A] DEFAULT ('E'),
[WorksCompCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EEOCategoryCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SOCCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGHRJobs__Statu__7B4AD1C3] DEFAULT ('A')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGHRJobs] ADD CONSTRAINT [CFGHRJobsPK] PRIMARY KEY CLUSTERED ([JobID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
