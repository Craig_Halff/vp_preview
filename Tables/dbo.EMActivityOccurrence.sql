CREATE TABLE [dbo].[EMActivityOccurrence]
(
[ActivityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NOT NULL,
[ReminderDate] [datetime] NULL,
[PopupDismissed] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EMActivityOccurrence] ADD CONSTRAINT [EMActivityOccurrencePK] PRIMARY KEY NONCLUSTERED ([ActivityID], [Employee], [StartDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
