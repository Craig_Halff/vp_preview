CREATE TABLE [dbo].[InvDetail]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__InvDetail_N__Seq__3476CB79] DEFAULT ((0)),
[Item] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Location] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationFrom] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Qty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__InvDetail_N__Qty__356AEFB2] DEFAULT ((0)),
[UnitCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__InvDetail__UnitC__365F13EB] DEFAULT ((0)),
[AdjUnitCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__InvDetail__AdjUn__37533824] DEFAULT ((0)),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Skipgl] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__InvDetail__Skipg__38475C5D] DEFAULT ('N'),
[Reason] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IRDetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[InvDetail] ADD CONSTRAINT [InvDetailPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [DetailPKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
