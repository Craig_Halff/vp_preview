CREATE TABLE [dbo].[Quantities]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Item] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[QtyonReq] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Quantitie__Qtyon__5BA6728A] DEFAULT ((0)),
[QtyonOrder] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Quantitie__Qtyon__5C9A96C3] DEFAULT ((0)),
[QtyReserved] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Quantitie__QtyRe__5D8EBAFC] DEFAULT ((0)),
[QtyRequested] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Quantitie__QtyRe__5E82DF35] DEFAULT ((0)),
[AvgCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Quantitie__AvgCo__5F77036E] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Quantities] ADD CONSTRAINT [QuantitiesPK] PRIMARY KEY NONCLUSTERED ([Company], [Item]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
