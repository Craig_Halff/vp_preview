CREATE TABLE [dbo].[CFGBankImportDetailData]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FieldName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Include] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGBankIm__Inclu__4D0CA3C2] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__CFGBankImpo__Seq__4E00C7FB] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGBankImportDetailData] ADD CONSTRAINT [CFGBankImportDetailDataPK] PRIMARY KEY NONCLUSTERED ([Code], [FieldName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
