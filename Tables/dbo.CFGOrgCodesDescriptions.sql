CREATE TABLE [dbo].[CFGOrgCodesDescriptions]
(
[Code] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrgLevel] [smallint] NOT NULL CONSTRAINT [DF__CFGOrgCod__OrgLe__427A06B6] DEFAULT ((0)),
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGOrgCodesDescriptions] ADD CONSTRAINT [CFGOrgCodesDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [OrgLevel], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
