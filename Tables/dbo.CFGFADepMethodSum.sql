CREATE TABLE [dbo].[CFGFADepMethodSum]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Year] [int] NOT NULL CONSTRAINT [DF__CFGFADepMe__Year__0B93E620] DEFAULT ((0)),
[Periods] [int] NOT NULL CONSTRAINT [DF__CFGFADepM__Perio__0C880A59] DEFAULT ((0)),
[Percentage] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGFADepM__Perce__0D7C2E92] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGFADepMethodSum] ADD CONSTRAINT [CFGFADepMethodSumPK] PRIMARY KEY CLUSTERED ([Code], [Company], [Year]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
