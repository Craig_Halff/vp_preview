CREATE TABLE [dbo].[PurchaseTemplate]
(
[Name] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Options] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PurchaseTemplate] ADD CONSTRAINT [PurchaseTemplatePK] PRIMARY KEY NONCLUSTERED ([Name]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
