CREATE TABLE [dbo].[billFeeAllocation]
(
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecdSeq] [int] NOT NULL CONSTRAINT [DF__billFeeAl__RecdS__7C11AB6E] DEFAULT ((0)),
[BillWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Section] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[billFeeAllocation] ADD CONSTRAINT [billFeeAllocationPK] PRIMARY KEY NONCLUSTERED ([MainWBS1], [RecdSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
