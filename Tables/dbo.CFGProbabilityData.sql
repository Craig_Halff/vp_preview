CREATE TABLE [dbo].[CFGProbabilityData]
(
[Probability] [smallint] NOT NULL CONSTRAINT [DF__CFGProbab__Proba__4C9641C1] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGProbabilityData] ADD CONSTRAINT [CFGProbabilityDataPK] PRIMARY KEY CLUSTERED ([Probability]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
