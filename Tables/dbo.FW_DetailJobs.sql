CREATE TABLE [dbo].[FW_DetailJobs]
(
[QueueID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProcessID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DetailID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__FW_DetailJo__Seq__37BD4C78] DEFAULT ((0)),
[ProgID] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeStarted] [datetime] NULL,
[TimeEnded] [datetime] NULL,
[ProcessStatus] [smallint] NOT NULL CONSTRAINT [DF__FW_Detail__Proce__38B170B1] DEFAULT ((0)),
[AlertPending] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TerminationMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[xmlParams] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServerName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateTimeFormat] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumberFormat] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContextState] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_DetailJobs] ADD CONSTRAINT [FW_DetailJobsPK] PRIMARY KEY NONCLUSTERED ([QueueID], [ProcessID], [DetailID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
