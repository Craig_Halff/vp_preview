CREATE TABLE [dbo].[GLParentGroup]
(
[Code] [smallint] NOT NULL CONSTRAINT [DF__GLParentGr__Code__1AEC03A0] DEFAULT ((0)),
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GroupType] [smallint] NOT NULL CONSTRAINT [DF__GLParentG__Group__1BE027D9] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GLParentGroup] ADD CONSTRAINT [GLParentGroupPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
