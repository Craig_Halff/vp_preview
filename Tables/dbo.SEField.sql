CREATE TABLE [dbo].[SEField]
(
[Role] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ComponentID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Tablename] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ColumnName] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReadOnly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEField__ReadOnl__4E432122] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_SEField]
      ON [dbo].[SEField]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SEField'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ComponentID],121),'Role',CONVERT(NVARCHAR(2000),[Role],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ComponentID],121),'InfoCenterArea',CONVERT(NVARCHAR(2000),[InfoCenterArea],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ComponentID],121),'GridID',CONVERT(NVARCHAR(2000),[GridID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ComponentID],121),'ComponentID',CONVERT(NVARCHAR(2000),[ComponentID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ComponentID],121),'Tablename',CONVERT(NVARCHAR(2000),[Tablename],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ComponentID],121),'ColumnName',CONVERT(NVARCHAR(2000),[ColumnName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ComponentID],121),'ReadOnly',CONVERT(NVARCHAR(2000),[ReadOnly],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_SEField]
      ON [dbo].[SEField]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SEField'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'Role',NULL,CONVERT(NVARCHAR(2000),[Role],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'InfoCenterArea',NULL,CONVERT(NVARCHAR(2000),[InfoCenterArea],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'GridID',NULL,CONVERT(NVARCHAR(2000),[GridID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'ComponentID',NULL,CONVERT(NVARCHAR(2000),[ComponentID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'Tablename',NULL,CONVERT(NVARCHAR(2000),[Tablename],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'ColumnName',NULL,CONVERT(NVARCHAR(2000),[ColumnName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'ReadOnly',NULL,CONVERT(NVARCHAR(2000),[ReadOnly],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_SEField]
      ON [dbo].[SEField]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SEField'
    
      If UPDATE([Role])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'Role',
      CONVERT(NVARCHAR(2000),DELETED.[Role],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Role],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[InfoCenterArea] = DELETED.[InfoCenterArea] AND INSERTED.[GridID] = DELETED.[GridID] AND INSERTED.[ComponentID] = DELETED.[ComponentID] AND 
		(
			(
				INSERTED.[Role] Is Null And
				DELETED.[Role] Is Not Null
			) Or
			(
				INSERTED.[Role] Is Not Null And
				DELETED.[Role] Is Null
			) Or
			(
				INSERTED.[Role] !=
				DELETED.[Role]
			)
		) 
		END		
		
      If UPDATE([InfoCenterArea])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'InfoCenterArea',
      CONVERT(NVARCHAR(2000),DELETED.[InfoCenterArea],121),
      CONVERT(NVARCHAR(2000),INSERTED.[InfoCenterArea],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[InfoCenterArea] = DELETED.[InfoCenterArea] AND INSERTED.[GridID] = DELETED.[GridID] AND INSERTED.[ComponentID] = DELETED.[ComponentID] AND 
		(
			(
				INSERTED.[InfoCenterArea] Is Null And
				DELETED.[InfoCenterArea] Is Not Null
			) Or
			(
				INSERTED.[InfoCenterArea] Is Not Null And
				DELETED.[InfoCenterArea] Is Null
			) Or
			(
				INSERTED.[InfoCenterArea] !=
				DELETED.[InfoCenterArea]
			)
		) 
		END		
		
      If UPDATE([GridID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'GridID',
      CONVERT(NVARCHAR(2000),DELETED.[GridID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[GridID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[InfoCenterArea] = DELETED.[InfoCenterArea] AND INSERTED.[GridID] = DELETED.[GridID] AND INSERTED.[ComponentID] = DELETED.[ComponentID] AND 
		(
			(
				INSERTED.[GridID] Is Null And
				DELETED.[GridID] Is Not Null
			) Or
			(
				INSERTED.[GridID] Is Not Null And
				DELETED.[GridID] Is Null
			) Or
			(
				INSERTED.[GridID] !=
				DELETED.[GridID]
			)
		) 
		END		
		
      If UPDATE([ComponentID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'ComponentID',
      CONVERT(NVARCHAR(2000),DELETED.[ComponentID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ComponentID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[InfoCenterArea] = DELETED.[InfoCenterArea] AND INSERTED.[GridID] = DELETED.[GridID] AND INSERTED.[ComponentID] = DELETED.[ComponentID] AND 
		(
			(
				INSERTED.[ComponentID] Is Null And
				DELETED.[ComponentID] Is Not Null
			) Or
			(
				INSERTED.[ComponentID] Is Not Null And
				DELETED.[ComponentID] Is Null
			) Or
			(
				INSERTED.[ComponentID] !=
				DELETED.[ComponentID]
			)
		) 
		END		
		
      If UPDATE([Tablename])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'Tablename',
      CONVERT(NVARCHAR(2000),DELETED.[Tablename],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Tablename],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[InfoCenterArea] = DELETED.[InfoCenterArea] AND INSERTED.[GridID] = DELETED.[GridID] AND INSERTED.[ComponentID] = DELETED.[ComponentID] AND 
		(
			(
				INSERTED.[Tablename] Is Null And
				DELETED.[Tablename] Is Not Null
			) Or
			(
				INSERTED.[Tablename] Is Not Null And
				DELETED.[Tablename] Is Null
			) Or
			(
				INSERTED.[Tablename] !=
				DELETED.[Tablename]
			)
		) 
		END		
		
      If UPDATE([ColumnName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'ColumnName',
      CONVERT(NVARCHAR(2000),DELETED.[ColumnName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ColumnName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[InfoCenterArea] = DELETED.[InfoCenterArea] AND INSERTED.[GridID] = DELETED.[GridID] AND INSERTED.[ComponentID] = DELETED.[ComponentID] AND 
		(
			(
				INSERTED.[ColumnName] Is Null And
				DELETED.[ColumnName] Is Not Null
			) Or
			(
				INSERTED.[ColumnName] Is Not Null And
				DELETED.[ColumnName] Is Null
			) Or
			(
				INSERTED.[ColumnName] !=
				DELETED.[ColumnName]
			)
		) 
		END		
		
      If UPDATE([ReadOnly])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[InfoCenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'ReadOnly',
      CONVERT(NVARCHAR(2000),DELETED.[ReadOnly],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReadOnly],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[InfoCenterArea] = DELETED.[InfoCenterArea] AND INSERTED.[GridID] = DELETED.[GridID] AND INSERTED.[ComponentID] = DELETED.[ComponentID] AND 
		(
			(
				INSERTED.[ReadOnly] Is Null And
				DELETED.[ReadOnly] Is Not Null
			) Or
			(
				INSERTED.[ReadOnly] Is Not Null And
				DELETED.[ReadOnly] Is Null
			) Or
			(
				INSERTED.[ReadOnly] !=
				DELETED.[ReadOnly]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[SEField] ADD CONSTRAINT [SEFieldPK] PRIMARY KEY NONCLUSTERED ([Role], [InfoCenterArea], [GridID], [ComponentID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [SEFieldInfoIDX] ON [dbo].[SEField] ([Role], [InfoCenterArea], [Tablename]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
