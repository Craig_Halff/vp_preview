CREATE TABLE [dbo].[CFGPYContribution]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUI] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYContri__SUI__37C76E19] DEFAULT ('N'),
[CreditAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DebitAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DebitWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DebitWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DebitWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Method] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Withholding] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYCont__AmtPc__38BB9252] DEFAULT ((0)),
[RateCap] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYCont__RateC__39AFB68B] DEFAULT ((0)),
[WageBase] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYCont__WageB__3AA3DAC4] DEFAULT ((0)),
[Limit] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYCont__Limit__3B97FEFD] DEFAULT ((0)),
[DefaultEM] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYCont__Defau__3C8C2336] DEFAULT ('N'),
[Suppress] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYCont__Suppr__3D80476F] DEFAULT ('N'),
[Exclude401k] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYCont__Exclu__3E746BA8] DEFAULT ('N'),
[ExcludeCafeteria] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYCont__Exclu__3F688FE1] DEFAULT ('N'),
[W2LimitReset] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYCont__W2Lim__405CB41A] DEFAULT ('N'),
[BonusAutoZero] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYCont__Bonus__4150D853] DEFAULT ('N'),
[ExcludeOtherPay1] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYCont__Exclu__4244FC8C] DEFAULT ('N'),
[ExcludeOtherPay2] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYCont__Exclu__433920C5] DEFAULT ('N'),
[ExcludeOtherPay3] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYCont__Exclu__442D44FE] DEFAULT ('N'),
[ExcludeOtherPay4] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYCont__Exclu__45216937] DEFAULT ('N'),
[ExcludeOtherPay5] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYCont__Exclu__46158D70] DEFAULT ('N'),
[RateTable] [int] NOT NULL CONSTRAINT [DF__CFGPYCont__RateT__4709B1A9] DEFAULT ((0)),
[PrintOnW2] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYCont__Print__47FDD5E2] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPYContribution] ADD CONSTRAINT [CFGPYContributionPK] PRIMARY KEY CLUSTERED ([Company], [Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
