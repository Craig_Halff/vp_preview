CREATE TABLE [dbo].[laDetail]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__laDetail_Ne__Seq__2CA09F87] DEFAULT ((0)),
[TransDate] [datetime] NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LaborCode] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laDetail___RegHr__2D94C3C0] DEFAULT ((0)),
[OvtHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laDetail___OvtHr__2E88E7F9] DEFAULT ((0)),
[RegAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laDetail___RegAm__2F7D0C32] DEFAULT ((0)),
[OvtAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laDetail___OvtAm__3071306B] DEFAULT ((0)),
[BillExt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laDetail___BillE__316554A4] DEFAULT ((0)),
[SuppressBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__laDetail___Suppr__325978DD] DEFAULT ('N'),
[TransComment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpecialOvtHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laDetail___Speci__334D9D16] DEFAULT ((0)),
[SpecialOvtAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laDetail___Speci__3441C14F] DEFAULT ((0)),
[BillCategory] [smallint] NOT NULL CONSTRAINT [DF__laDetail___BillC__3535E588] DEFAULT ((0)),
[RegAmtProjectFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laDetail___RegAm__362A09C1] DEFAULT ((0)),
[OvtAmtProjectFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laDetail___OvtAm__371E2DFA] DEFAULT ((0)),
[SpecialOvtAmtProjectFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laDetail___Speci__38125233] DEFAULT ((0)),
[LinkCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RealizationAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laDetail___Reali__3906766C] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[laDetail] ADD CONSTRAINT [laDetailPK] PRIMARY KEY NONCLUSTERED ([Batch], [Employee], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
