CREATE TABLE [dbo].[FW_CFGLabelData]
(
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LabelName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LabelValue] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlaceHolder] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Gender] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGLab__Gende__7DC5B50D] DEFAULT ('N'),
[LeadingVowelTreatment] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGLab__Leadi__7EB9D946] DEFAULT ('N'),
[UDIC_ID] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[UpdateCFGLabelDataTrigger] ON [dbo].[FW_CFGLabelData] FOR UPDATE
AS
	UPDATE FW_CFGSystem set LastLabelUpdateDate = GetUTCDate()
GO
ALTER TABLE [dbo].[FW_CFGLabelData] ADD CONSTRAINT [FW_CFGLabelDataPK] PRIMARY KEY NONCLUSTERED ([UICultureName], [LabelName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
