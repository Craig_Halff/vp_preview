CREATE TABLE [dbo].[CMOCustomPackages]
(
[Role] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PackageID] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HashValue] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CMOCustomPackages] ADD CONSTRAINT [CMOCustomPackagesPK] PRIMARY KEY CLUSTERED ([Role]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
