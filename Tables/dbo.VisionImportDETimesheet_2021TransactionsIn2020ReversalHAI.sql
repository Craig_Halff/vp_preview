CREATE TABLE [dbo].[VisionImportDETimesheet_2021TransactionsIn2020ReversalHAI]
(
[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransDate] [datetime] NULL,
[WBS1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LaborCode] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LaborCategory] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegHrs] [decimal] (19, 4) NULL,
[OvtHrs] [decimal] (19, 4) NULL,
[TransComment] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpecialOvtHrsTotal] [decimal] (19, 4) NULL
) ON [PRIMARY]
GO
