CREATE TABLE [dbo].[~tkUnitDetail]
(
[EndDate] [datetime] NOT NULL,
[Employee] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeCompany] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL,
[TransDate] [datetime] NOT NULL,
[WBS1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitTable] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Unit] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Qty] [decimal] (19, 4) NOT NULL,
[TransComment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
