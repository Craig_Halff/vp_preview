CREATE TABLE [dbo].[CCG_EI_ConfigOptions]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[DateChanged] [datetime] NULL,
[ChangedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Detail] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_ConfigOptions] ADD CONSTRAINT [PK_CCG_EI_ConfigOptions] PRIMARY KEY CLUSTERED ([Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Configuration settings for various areas of the application, such as Grid States.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigOptions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The modification user or the user for which this configuration applies', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigOptions', 'COLUMN', N'ChangedBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Last modified date', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigOptions', 'COLUMN', N'DateChanged'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The description of the configuration option', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigOptions', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Detail values for the option, usually in XML or name value pair syntax', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigOptions', 'COLUMN', N'Detail'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigOptions', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Type of option (GridOptions, PctCmp_GridOptions, etc)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigOptions', 'COLUMN', N'Type'
GO
