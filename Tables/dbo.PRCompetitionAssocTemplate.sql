CREATE TABLE [dbo].[PRCompetitionAssocTemplate]
(
[RecordID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Strengths] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Weakness] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Incumbent] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRCompeti__Incum__0A8D1553] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PRCompetitionAssocTemplate] ADD CONSTRAINT [PRCompetitionAssocTemplatePK] PRIMARY KEY CLUSTERED ([RecordID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
