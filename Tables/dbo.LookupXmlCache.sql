CREATE TABLE [dbo].[LookupXmlCache]
(
[LookupType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LookupXML] [image] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LookupXmlCache] ADD CONSTRAINT [LookupXmlCachePK] PRIMARY KEY NONCLUSTERED ([LookupType]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
