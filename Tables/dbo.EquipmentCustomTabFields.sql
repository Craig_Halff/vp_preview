CREATE TABLE [dbo].[EquipmentCustomTabFields]
(
[EquipmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustFleetID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_EquipmentCustomTabFields]
      ON [dbo].[EquipmentCustomTabFields]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EquipmentCustomTabFields'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[EquipmentID],121),'EquipmentID',CONVERT(NVARCHAR(2000),[EquipmentID],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[EquipmentID],121),'CustFleetID',CONVERT(NVARCHAR(2000),DELETED.[CustFleetID],121),NULL, oldDesc.CustNumber, NULL, @source,@app
        FROM DELETED left join UDIC_FleetManagement as oldDesc   on DELETED.CustFleetID = oldDesc.UDIC_UID

      
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_EquipmentCustomTabFields]
      ON [dbo].[EquipmentCustomTabFields]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EquipmentCustomTabFields'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[EquipmentID],121),'EquipmentID',NULL,CONVERT(NVARCHAR(2000),[EquipmentID],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[EquipmentID],121),'CustFleetID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustFleetID],121), NULL, newDesc.CustNumber, @source, @app
       FROM INSERTED left join  UDIC_FleetManagement as newDesc  on INSERTED.CustFleetID = newDesc.UDIC_UID

     
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_EquipmentCustomTabFields]
      ON [dbo].[EquipmentCustomTabFields]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EquipmentCustomTabFields'
    
      If UPDATE([EquipmentID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[EquipmentID],121),'EquipmentID',
      CONVERT(NVARCHAR(2000),DELETED.[EquipmentID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EquipmentID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[EquipmentID] = DELETED.[EquipmentID] AND 
		(
			(
				INSERTED.[EquipmentID] Is Null And
				DELETED.[EquipmentID] Is Not Null
			) Or
			(
				INSERTED.[EquipmentID] Is Not Null And
				DELETED.[EquipmentID] Is Null
			) Or
			(
				INSERTED.[EquipmentID] !=
				DELETED.[EquipmentID]
			)
		) 
		END		
		
     If UPDATE([CustFleetID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[EquipmentID],121),'CustFleetID',
     CONVERT(NVARCHAR(2000),DELETED.[CustFleetID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustFleetID],121),
     oldDesc.CustNumber, newDesc.CustNumber, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[EquipmentID] = DELETED.[EquipmentID] AND 
		(
			(
				INSERTED.[CustFleetID] Is Null And
				DELETED.[CustFleetID] Is Not Null
			) Or
			(
				INSERTED.[CustFleetID] Is Not Null And
				DELETED.[CustFleetID] Is Null
			) Or
			(
				INSERTED.[CustFleetID] !=
				DELETED.[CustFleetID]
			)
		) left join UDIC_FleetManagement as oldDesc  on DELETED.CustFleetID = oldDesc.UDIC_UID  left join  UDIC_FleetManagement as newDesc  on INSERTED.CustFleetID = newDesc.UDIC_UID
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[EquipmentCustomTabFields] ADD CONSTRAINT [EquipmentCustomTabFieldsPK] PRIMARY KEY CLUSTERED ([EquipmentID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
