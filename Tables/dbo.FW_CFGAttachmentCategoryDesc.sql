CREATE TABLE [dbo].[FW_CFGAttachmentCategoryDesc]
(
[Code] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Application] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__FW_CFGAttac__Seq__6BA704D2] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGAttachmentCategoryDesc] ADD CONSTRAINT [FW_CFGAttachmentCategoryDescPK] PRIMARY KEY NONCLUSTERED ([Code], [Application], [UICultureName]) ON [PRIMARY]
GO
