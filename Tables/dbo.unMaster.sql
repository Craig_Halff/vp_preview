CREATE TABLE [dbo].[unMaster]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Unit] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__unMaster___Poste__120D7033] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__unMaster_Ne__Seq__1301946C] DEFAULT ((0)),
[CurrencyExchangeOverrideMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__unMaster___Curre__13F5B8A5] DEFAULT ('N'),
[CurrencyExchangeOverrideDate] [datetime] NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__unMaster___Statu__14E9DCDE] DEFAULT ('N'),
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RejectReason] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[unMaster] ADD CONSTRAINT [unMasterPK] PRIMARY KEY NONCLUSTERED ([Batch], [Unit]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
