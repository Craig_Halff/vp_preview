CREATE TABLE [dbo].[PlanningCustomTabFields]
(
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlanningCustomTabFields] ADD CONSTRAINT [PlanningCustomTabFieldsPK] PRIMARY KEY NONCLUSTERED ([PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
