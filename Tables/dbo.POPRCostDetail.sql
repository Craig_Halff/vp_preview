CREATE TABLE [dbo].[POPRCostDetail]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PRDetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Line] [int] NOT NULL CONSTRAINT [DF__POPRCostDe__Line__7CFC795D] DEFAULT ((0)),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Billable] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POPRCostD__Billa__7DF09D96] DEFAULT ('N'),
[Pct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POPRCostDet__Pct__7EE4C1CF] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POPRCostDetail] ADD CONSTRAINT [POPRCostDetailPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [PRDetailPKey], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
