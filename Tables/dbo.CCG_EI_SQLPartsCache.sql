CREATE TABLE [dbo].[CCG_EI_SQLPartsCache]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[CreateDate] [datetime] NULL,
[Role] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SqlPart] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_SQLPartsCache] ADD CONSTRAINT [PK_CCG_EI_SQLPartsCache] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CCG_EI_SQLPartsCache_Role_Type_IDX] ON [dbo].[CCG_EI_SQLPartsCache] ([Role], [Type]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stores SQL query parts to be used to create the main grid query', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_SQLPartsCache', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date this query part was cached', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_SQLPartsCache', 'COLUMN', N'CreateDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The EI Role associated with this query part (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_SQLPartsCache', 'COLUMN', N'Role'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_SQLPartsCache', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The cached partial SQL query', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_SQLPartsCache', 'COLUMN', N'SqlPart'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The query part type key', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_SQLPartsCache', 'COLUMN', N'Type'
GO
