CREATE TABLE [dbo].[Projects_SpentTemplate]
(
[WBS1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustSpentDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustSpentTotalBacklog] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Projects___CustS__5D39B541] DEFAULT ((0)),
[CustSpentCurrentBacklog] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Projects___CustS__5E2DD97A] DEFAULT ((0)),
[CustSpentWeek1] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Projects___CustS__5F21FDB3] DEFAULT ((0)),
[CustSpentWeek2] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Projects___CustS__601621EC] DEFAULT ((0)),
[CustTest] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_Projects_SpentTemplate]
      ON [dbo].[Projects_SpentTemplate]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Projects_SpentTemplate'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'WBS1',CONVERT(NVARCHAR(2000),[WBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'WBS2',CONVERT(NVARCHAR(2000),[WBS2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'WBS3',CONVERT(NVARCHAR(2000),[WBS3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Seq',CONVERT(NVARCHAR(2000),[Seq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustSpentDescription',CONVERT(NVARCHAR(2000),[CustSpentDescription],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustSpentTotalBacklog',CONVERT(NVARCHAR(2000),[CustSpentTotalBacklog],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustSpentCurrentBacklog',CONVERT(NVARCHAR(2000),[CustSpentCurrentBacklog],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustSpentWeek1',CONVERT(NVARCHAR(2000),[CustSpentWeek1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustSpentWeek2',CONVERT(NVARCHAR(2000),[CustSpentWeek2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustTest','[text]',NULL, @source,@app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_Projects_SpentTemplate] ON [dbo].[Projects_SpentTemplate]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_Projects_SpentTemplate]
      ON [dbo].[Projects_SpentTemplate]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Projects_SpentTemplate'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS1',NULL,CONVERT(NVARCHAR(2000),[WBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS2',NULL,CONVERT(NVARCHAR(2000),[WBS2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS3',NULL,CONVERT(NVARCHAR(2000),[WBS3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',NULL,CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustSpentDescription',NULL,CONVERT(NVARCHAR(2000),[CustSpentDescription],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustSpentTotalBacklog',NULL,CONVERT(NVARCHAR(2000),[CustSpentTotalBacklog],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustSpentCurrentBacklog',NULL,CONVERT(NVARCHAR(2000),[CustSpentCurrentBacklog],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustSpentWeek1',NULL,CONVERT(NVARCHAR(2000),[CustSpentWeek1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustSpentWeek2',NULL,CONVERT(NVARCHAR(2000),[CustSpentWeek2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustTest',NULL,'[text]', @source, @app
      FROM INSERTED


    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_Projects_SpentTemplate] ON [dbo].[Projects_SpentTemplate]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_Projects_SpentTemplate]
      ON [dbo].[Projects_SpentTemplate]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Projects_SpentTemplate'
    
      If UPDATE([WBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS1',
      CONVERT(NVARCHAR(2000),DELETED.[WBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS1] Is Null And
				DELETED.[WBS1] Is Not Null
			) Or
			(
				INSERTED.[WBS1] Is Not Null And
				DELETED.[WBS1] Is Null
			) Or
			(
				INSERTED.[WBS1] !=
				DELETED.[WBS1]
			)
		) 
		END		
		
      If UPDATE([WBS2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS2',
      CONVERT(NVARCHAR(2000),DELETED.[WBS2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS2] Is Null And
				DELETED.[WBS2] Is Not Null
			) Or
			(
				INSERTED.[WBS2] Is Not Null And
				DELETED.[WBS2] Is Null
			) Or
			(
				INSERTED.[WBS2] !=
				DELETED.[WBS2]
			)
		) 
		END		
		
      If UPDATE([WBS3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS3',
      CONVERT(NVARCHAR(2000),DELETED.[WBS3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS3] Is Null And
				DELETED.[WBS3] Is Not Null
			) Or
			(
				INSERTED.[WBS3] Is Not Null And
				DELETED.[WBS3] Is Null
			) Or
			(
				INSERTED.[WBS3] !=
				DELETED.[WBS3]
			)
		) 
		END		
		
      If UPDATE([Seq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',
      CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] Is Null And
				DELETED.[Seq] Is Not Null
			) Or
			(
				INSERTED.[Seq] Is Not Null And
				DELETED.[Seq] Is Null
			) Or
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
		
      If UPDATE([CustSpentDescription])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustSpentDescription',
      CONVERT(NVARCHAR(2000),DELETED.[CustSpentDescription],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSpentDescription],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustSpentDescription] Is Null And
				DELETED.[CustSpentDescription] Is Not Null
			) Or
			(
				INSERTED.[CustSpentDescription] Is Not Null And
				DELETED.[CustSpentDescription] Is Null
			) Or
			(
				INSERTED.[CustSpentDescription] !=
				DELETED.[CustSpentDescription]
			)
		) 
		END		
		
      If UPDATE([CustSpentTotalBacklog])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustSpentTotalBacklog',
      CONVERT(NVARCHAR(2000),DELETED.[CustSpentTotalBacklog],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSpentTotalBacklog],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustSpentTotalBacklog] Is Null And
				DELETED.[CustSpentTotalBacklog] Is Not Null
			) Or
			(
				INSERTED.[CustSpentTotalBacklog] Is Not Null And
				DELETED.[CustSpentTotalBacklog] Is Null
			) Or
			(
				INSERTED.[CustSpentTotalBacklog] !=
				DELETED.[CustSpentTotalBacklog]
			)
		) 
		END		
		
      If UPDATE([CustSpentCurrentBacklog])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustSpentCurrentBacklog',
      CONVERT(NVARCHAR(2000),DELETED.[CustSpentCurrentBacklog],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSpentCurrentBacklog],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustSpentCurrentBacklog] Is Null And
				DELETED.[CustSpentCurrentBacklog] Is Not Null
			) Or
			(
				INSERTED.[CustSpentCurrentBacklog] Is Not Null And
				DELETED.[CustSpentCurrentBacklog] Is Null
			) Or
			(
				INSERTED.[CustSpentCurrentBacklog] !=
				DELETED.[CustSpentCurrentBacklog]
			)
		) 
		END		
		
      If UPDATE([CustSpentWeek1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustSpentWeek1',
      CONVERT(NVARCHAR(2000),DELETED.[CustSpentWeek1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSpentWeek1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustSpentWeek1] Is Null And
				DELETED.[CustSpentWeek1] Is Not Null
			) Or
			(
				INSERTED.[CustSpentWeek1] Is Not Null And
				DELETED.[CustSpentWeek1] Is Null
			) Or
			(
				INSERTED.[CustSpentWeek1] !=
				DELETED.[CustSpentWeek1]
			)
		) 
		END		
		
      If UPDATE([CustSpentWeek2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustSpentWeek2',
      CONVERT(NVARCHAR(2000),DELETED.[CustSpentWeek2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSpentWeek2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustSpentWeek2] Is Null And
				DELETED.[CustSpentWeek2] Is Not Null
			) Or
			(
				INSERTED.[CustSpentWeek2] Is Not Null And
				DELETED.[CustSpentWeek2] Is Null
			) Or
			(
				INSERTED.[CustSpentWeek2] !=
				DELETED.[CustSpentWeek2]
			)
		) 
		END		
		
      If UPDATE([CustTest])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustTest',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_Projects_SpentTemplate] ON [dbo].[Projects_SpentTemplate]
GO
ALTER TABLE [dbo].[Projects_SpentTemplate] ADD CONSTRAINT [Projects_SpentTemplatePK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
