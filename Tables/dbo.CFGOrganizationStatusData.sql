CREATE TABLE [dbo].[CFGOrganizationStatusData]
(
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGOrganizationStatusData] ADD CONSTRAINT [CFGOrganizationStatusDataPK] PRIMARY KEY CLUSTERED ([Status]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
