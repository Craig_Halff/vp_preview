CREATE TABLE [dbo].[EMPayrollWithholdingDetailWage]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__EMPayroll__Perio__5FE04C6B] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__EMPayroll__PostS__60D470A4] DEFAULT ((0)),
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodeWage] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Amoun__61C894DD] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EMPayrollWithholdingDetailWage] ADD CONSTRAINT [EMPayrollWithholdingDetailWagePK] PRIMARY KEY NONCLUSTERED ([Employee], [Period], [PostSeq], [Code], [CodeWage]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
