CREATE TABLE [dbo].[PNExpense]
(
[ExpenseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcctName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BaselineExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNExpense__Basel__0448956A] DEFAULT ((0)),
[BaselineExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNExpense__Basel__053CB9A3] DEFAULT ((0)),
[PlannedExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNExpense__Plann__0630DDDC] DEFAULT ((0)),
[PlannedExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNExpense__Plann__07250215] DEFAULT ((0)),
[PlannedDirExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNExpense__Plann__0819264E] DEFAULT ((0)),
[PlannedDirExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNExpense__Plann__090D4A87] DEFAULT ((0)),
[PctCompleteExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNExpense__PctCo__0A016EC0] DEFAULT ((0)),
[PctCompleteExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNExpense__PctCo__0AF592F9] DEFAULT ((0)),
[WeightedExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNExpense__Weigh__0BE9B732] DEFAULT ((0)),
[WeightedExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNExpense__Weigh__0CDDDB6B] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[DirectAcctFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNExpense__Direc__0DD1FFA4] DEFAULT ('N'),
[ExpBillRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNExpense__ExpBi__0EC623DD] DEFAULT ((0)),
[ExpRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNExpense__ExpRe__0FBA4816] DEFAULT ((0)),
[SortSeq] [smallint] NOT NULL CONSTRAINT [DF__PNExpense__SortS__10AE6C4F] DEFAULT ((0)),
[LabParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNExpense__LabPa__11A29088] DEFAULT ('N'),
[ExpParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNExpense__ExpPa__1296B4C1] DEFAULT ('N'),
[ConParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNExpense__ConPa__138AD8FA] DEFAULT ('N'),
[UntParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNExpense__UntPa__147EFD33] DEFAULT ('N'),
[BaselineStart] [datetime] NULL,
[BaselineFinish] [datetime] NULL,
[ConVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNExpense__ConVS__1573216C] DEFAULT ('N'),
[ExpVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNExpense__ExpVS__166745A5] DEFAULT ('Y'),
[LabVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNExpense__LabVS__175B69DE] DEFAULT ('N'),
[UntVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNExpense__UntVS__184F8E17] DEFAULT ('N'),
[BaselineDirExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNExpense__Basel__1943B250] DEFAULT ((0)),
[BaselineDirExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNExpense__Basel__1A37D689] DEFAULT ((0)),
[JTDExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNExpense__JTDEx__1B2BFAC2] DEFAULT ((0)),
[JTDExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNExpense__JTDEx__1C201EFB] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PNExpense] ADD CONSTRAINT [PNExpensePK] PRIMARY KEY NONCLUSTERED ([ExpenseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [PNExpensePlanIDIDX] ON [dbo].[PNExpense] ([PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNExpenseTaskIDIDX] ON [dbo].[PNExpense] ([TaskID]) ON [PRIMARY]
GO
