CREATE TABLE [dbo].[FW_InfoCenterTabsData]
(
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__FW_InfoCent__Seq__4423235D] DEFAULT ((0)),
[Installed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HiddenFor] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Platform] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_InfoCe__Platf__67CDE8FB] DEFAULT ('WinUI'),
[PageID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_InfoCenterTabsData] ADD CONSTRAINT [FW_InfoCenterTabsDataPK] PRIMARY KEY NONCLUSTERED ([InfoCenterArea], [TabID], [Platform]) ON [PRIMARY]
GO
