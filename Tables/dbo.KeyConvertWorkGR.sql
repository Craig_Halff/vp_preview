CREATE TABLE [dbo].[KeyConvertWorkGR]
(
[PKey] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KeyConvert__PKey__14C915F6] DEFAULT (newid()),
[OldCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NewCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NewCategory] [smallint] NOT NULL CONSTRAINT [DF__KeyConver__NewCa__15BD3A2F] DEFAULT ((0)),
[NewLaborCode] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[KeyConvertWorkGR] ADD CONSTRAINT [KeyConvertWorkGRPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
