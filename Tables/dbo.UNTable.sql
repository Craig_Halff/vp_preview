CREATE TABLE [dbo].[UNTable]
(
[UnitTable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProjectCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AvailableForPlanning] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__UNTable__Availab__78A36F42] DEFAULT ('N'),
[FilterOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterPrincipal] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterProjMgr] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterSupervisor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_UNTable]
      ON [dbo].[UNTable]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UNTable'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121),'UnitTable',CONVERT(NVARCHAR(2000),[UnitTable],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121),'Company',CONVERT(NVARCHAR(2000),[Company],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121),'ProjectCurrencyCode',CONVERT(NVARCHAR(2000),[ProjectCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121),'BillingCurrencyCode',CONVERT(NVARCHAR(2000),[BillingCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121),'AvailableForPlanning',CONVERT(NVARCHAR(2000),[AvailableForPlanning],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121),'FilterOrg',CONVERT(NVARCHAR(2000),[FilterOrg],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121),'FilterPrincipal',CONVERT(NVARCHAR(2000),[FilterPrincipal],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121),'FilterProjMgr',CONVERT(NVARCHAR(2000),[FilterProjMgr],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121),'FilterSupervisor',CONVERT(NVARCHAR(2000),[FilterSupervisor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121),'FilterCode',CONVERT(NVARCHAR(2000),[FilterCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121),'Status',CONVERT(NVARCHAR(2000),[Status],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_UNTable]
      ON [dbo].[UNTable]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UNTable'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'UnitTable',NULL,CONVERT(NVARCHAR(2000),[UnitTable],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'Company',NULL,CONVERT(NVARCHAR(2000),[Company],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'ProjectCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[ProjectCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'BillingCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[BillingCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'AvailableForPlanning',NULL,CONVERT(NVARCHAR(2000),[AvailableForPlanning],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'FilterOrg',NULL,CONVERT(NVARCHAR(2000),[FilterOrg],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'FilterPrincipal',NULL,CONVERT(NVARCHAR(2000),[FilterPrincipal],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'FilterProjMgr',NULL,CONVERT(NVARCHAR(2000),[FilterProjMgr],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'FilterSupervisor',NULL,CONVERT(NVARCHAR(2000),[FilterSupervisor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'FilterCode',NULL,CONVERT(NVARCHAR(2000),[FilterCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'Status',NULL,CONVERT(NVARCHAR(2000),[Status],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_UNTable]
      ON [dbo].[UNTable]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UNTable'
    
      If UPDATE([UnitTable])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'UnitTable',
      CONVERT(NVARCHAR(2000),DELETED.[UnitTable],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UnitTable],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND 
		(
			(
				INSERTED.[UnitTable] Is Null And
				DELETED.[UnitTable] Is Not Null
			) Or
			(
				INSERTED.[UnitTable] Is Not Null And
				DELETED.[UnitTable] Is Null
			) Or
			(
				INSERTED.[UnitTable] !=
				DELETED.[UnitTable]
			)
		) 
		END		
		
      If UPDATE([Company])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'Company',
      CONVERT(NVARCHAR(2000),DELETED.[Company],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Company],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND 
		(
			(
				INSERTED.[Company] Is Null And
				DELETED.[Company] Is Not Null
			) Or
			(
				INSERTED.[Company] Is Not Null And
				DELETED.[Company] Is Null
			) Or
			(
				INSERTED.[Company] !=
				DELETED.[Company]
			)
		) 
		END		
		
      If UPDATE([ProjectCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'ProjectCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[ProjectCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProjectCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND 
		(
			(
				INSERTED.[ProjectCurrencyCode] Is Null And
				DELETED.[ProjectCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[ProjectCurrencyCode] Is Not Null And
				DELETED.[ProjectCurrencyCode] Is Null
			) Or
			(
				INSERTED.[ProjectCurrencyCode] !=
				DELETED.[ProjectCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([BillingCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'BillingCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[BillingCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BillingCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND 
		(
			(
				INSERTED.[BillingCurrencyCode] Is Null And
				DELETED.[BillingCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[BillingCurrencyCode] Is Not Null And
				DELETED.[BillingCurrencyCode] Is Null
			) Or
			(
				INSERTED.[BillingCurrencyCode] !=
				DELETED.[BillingCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([AvailableForPlanning])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'AvailableForPlanning',
      CONVERT(NVARCHAR(2000),DELETED.[AvailableForPlanning],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AvailableForPlanning],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND 
		(
			(
				INSERTED.[AvailableForPlanning] Is Null And
				DELETED.[AvailableForPlanning] Is Not Null
			) Or
			(
				INSERTED.[AvailableForPlanning] Is Not Null And
				DELETED.[AvailableForPlanning] Is Null
			) Or
			(
				INSERTED.[AvailableForPlanning] !=
				DELETED.[AvailableForPlanning]
			)
		) 
		END		
		
      If UPDATE([FilterOrg])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'FilterOrg',
      CONVERT(NVARCHAR(2000),DELETED.[FilterOrg],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FilterOrg],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND 
		(
			(
				INSERTED.[FilterOrg] Is Null And
				DELETED.[FilterOrg] Is Not Null
			) Or
			(
				INSERTED.[FilterOrg] Is Not Null And
				DELETED.[FilterOrg] Is Null
			) Or
			(
				INSERTED.[FilterOrg] !=
				DELETED.[FilterOrg]
			)
		) 
		END		
		
      If UPDATE([FilterPrincipal])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'FilterPrincipal',
      CONVERT(NVARCHAR(2000),DELETED.[FilterPrincipal],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FilterPrincipal],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND 
		(
			(
				INSERTED.[FilterPrincipal] Is Null And
				DELETED.[FilterPrincipal] Is Not Null
			) Or
			(
				INSERTED.[FilterPrincipal] Is Not Null And
				DELETED.[FilterPrincipal] Is Null
			) Or
			(
				INSERTED.[FilterPrincipal] !=
				DELETED.[FilterPrincipal]
			)
		) 
		END		
		
      If UPDATE([FilterProjMgr])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'FilterProjMgr',
      CONVERT(NVARCHAR(2000),DELETED.[FilterProjMgr],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FilterProjMgr],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND 
		(
			(
				INSERTED.[FilterProjMgr] Is Null And
				DELETED.[FilterProjMgr] Is Not Null
			) Or
			(
				INSERTED.[FilterProjMgr] Is Not Null And
				DELETED.[FilterProjMgr] Is Null
			) Or
			(
				INSERTED.[FilterProjMgr] !=
				DELETED.[FilterProjMgr]
			)
		) 
		END		
		
      If UPDATE([FilterSupervisor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'FilterSupervisor',
      CONVERT(NVARCHAR(2000),DELETED.[FilterSupervisor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FilterSupervisor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND 
		(
			(
				INSERTED.[FilterSupervisor] Is Null And
				DELETED.[FilterSupervisor] Is Not Null
			) Or
			(
				INSERTED.[FilterSupervisor] Is Not Null And
				DELETED.[FilterSupervisor] Is Null
			) Or
			(
				INSERTED.[FilterSupervisor] !=
				DELETED.[FilterSupervisor]
			)
		) 
		END		
		
      If UPDATE([FilterCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'FilterCode',
      CONVERT(NVARCHAR(2000),DELETED.[FilterCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FilterCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND 
		(
			(
				INSERTED.[FilterCode] Is Null And
				DELETED.[FilterCode] Is Not Null
			) Or
			(
				INSERTED.[FilterCode] Is Not Null And
				DELETED.[FilterCode] Is Null
			) Or
			(
				INSERTED.[FilterCode] !=
				DELETED.[FilterCode]
			)
		) 
		END		
		
      If UPDATE([Status])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121),'Status',
      CONVERT(NVARCHAR(2000),DELETED.[Status],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Status],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND 
		(
			(
				INSERTED.[Status] Is Null And
				DELETED.[Status] Is Not Null
			) Or
			(
				INSERTED.[Status] Is Not Null And
				DELETED.[Status] Is Null
			) Or
			(
				INSERTED.[Status] !=
				DELETED.[Status]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[UNTable] ADD CONSTRAINT [UNTablePK] PRIMARY KEY CLUSTERED ([UnitTable], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [UNTableIDX] ON [dbo].[UNTable] ([UnitTable], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
