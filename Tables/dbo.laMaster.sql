CREATE TABLE [dbo].[laMaster]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__laMaster___Poste__3AEEBEDE] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__laMaster_Ne__Seq__3BE2E317] DEFAULT ((0)),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__laMaster___Statu__3CD70750] DEFAULT ('N'),
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RejectReason] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[laMaster] ADD CONSTRAINT [laMasterPK] PRIMARY KEY NONCLUSTERED ([Batch], [Employee]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
