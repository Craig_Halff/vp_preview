CREATE TABLE [dbo].[PRFECostGroups]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FuncGroupCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CostGroupCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChargeableCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFECostG__Charg__3C77DFF4] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_PRFECostGroups]
      ON [dbo].[PRFECostGroups]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'PRFECostGroups'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FuncGroupCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[CostGroupCode],121),'WBS1',CONVERT(NVARCHAR(2000),[WBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FuncGroupCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[CostGroupCode],121),'FuncGroupCode',CONVERT(NVARCHAR(2000),[FuncGroupCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FuncGroupCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[CostGroupCode],121),'CostGroupCode',CONVERT(NVARCHAR(2000),[CostGroupCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FuncGroupCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[CostGroupCode],121),'ChargeableCost',CONVERT(NVARCHAR(2000),[ChargeableCost],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_PRFECostGroups] ON [dbo].[PRFECostGroups]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_PRFECostGroups]
      ON [dbo].[PRFECostGroups]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'PRFECostGroups'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FuncGroupCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[CostGroupCode],121),'WBS1',NULL,CONVERT(NVARCHAR(2000),[WBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FuncGroupCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[CostGroupCode],121),'FuncGroupCode',NULL,CONVERT(NVARCHAR(2000),[FuncGroupCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FuncGroupCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[CostGroupCode],121),'CostGroupCode',NULL,CONVERT(NVARCHAR(2000),[CostGroupCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FuncGroupCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[CostGroupCode],121),'ChargeableCost',NULL,CONVERT(NVARCHAR(2000),[ChargeableCost],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_PRFECostGroups] ON [dbo].[PRFECostGroups]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_PRFECostGroups]
      ON [dbo].[PRFECostGroups]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'PRFECostGroups'
    
      If UPDATE([WBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FuncGroupCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[CostGroupCode],121),'WBS1',
      CONVERT(NVARCHAR(2000),DELETED.[WBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[FuncGroupCode] = DELETED.[FuncGroupCode] AND INSERTED.[CostGroupCode] = DELETED.[CostGroupCode] AND 
		(
			(
				INSERTED.[WBS1] Is Null And
				DELETED.[WBS1] Is Not Null
			) Or
			(
				INSERTED.[WBS1] Is Not Null And
				DELETED.[WBS1] Is Null
			) Or
			(
				INSERTED.[WBS1] !=
				DELETED.[WBS1]
			)
		) 
		END		
		
      If UPDATE([FuncGroupCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FuncGroupCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[CostGroupCode],121),'FuncGroupCode',
      CONVERT(NVARCHAR(2000),DELETED.[FuncGroupCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FuncGroupCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[FuncGroupCode] = DELETED.[FuncGroupCode] AND INSERTED.[CostGroupCode] = DELETED.[CostGroupCode] AND 
		(
			(
				INSERTED.[FuncGroupCode] Is Null And
				DELETED.[FuncGroupCode] Is Not Null
			) Or
			(
				INSERTED.[FuncGroupCode] Is Not Null And
				DELETED.[FuncGroupCode] Is Null
			) Or
			(
				INSERTED.[FuncGroupCode] !=
				DELETED.[FuncGroupCode]
			)
		) 
		END		
		
      If UPDATE([CostGroupCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FuncGroupCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[CostGroupCode],121),'CostGroupCode',
      CONVERT(NVARCHAR(2000),DELETED.[CostGroupCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CostGroupCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[FuncGroupCode] = DELETED.[FuncGroupCode] AND INSERTED.[CostGroupCode] = DELETED.[CostGroupCode] AND 
		(
			(
				INSERTED.[CostGroupCode] Is Null And
				DELETED.[CostGroupCode] Is Not Null
			) Or
			(
				INSERTED.[CostGroupCode] Is Not Null And
				DELETED.[CostGroupCode] Is Null
			) Or
			(
				INSERTED.[CostGroupCode] !=
				DELETED.[CostGroupCode]
			)
		) 
		END		
		
      If UPDATE([ChargeableCost])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FuncGroupCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[CostGroupCode],121),'ChargeableCost',
      CONVERT(NVARCHAR(2000),DELETED.[ChargeableCost],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ChargeableCost],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[FuncGroupCode] = DELETED.[FuncGroupCode] AND INSERTED.[CostGroupCode] = DELETED.[CostGroupCode] AND 
		(
			(
				INSERTED.[ChargeableCost] Is Null And
				DELETED.[ChargeableCost] Is Not Null
			) Or
			(
				INSERTED.[ChargeableCost] Is Not Null And
				DELETED.[ChargeableCost] Is Null
			) Or
			(
				INSERTED.[ChargeableCost] !=
				DELETED.[ChargeableCost]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_PRFECostGroups] ON [dbo].[PRFECostGroups]
GO
ALTER TABLE [dbo].[PRFECostGroups] ADD CONSTRAINT [PRFECostGroupsPK] PRIMARY KEY NONCLUSTERED ([WBS1], [FuncGroupCode], [CostGroupCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
