CREATE TABLE [dbo].[CCG_PAT_ConfigStamps]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[Label] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StampType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayOrder] [int] NULL,
[Content] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PositionType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PageSet] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Width] [int] NULL,
[ValidRoles] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_PAT_C__Statu__05BAADA5] DEFAULT ('A'),
[ModDate] [datetime] NOT NULL,
[ModUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_ConfigStamps] ADD CONSTRAINT [PK_CCG_PAT_ConfigStamps] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Configured stamp data', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStamps', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated - see CCG_PAT_ConfigStampsDescriptions', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStamps', 'COLUMN', N'Content'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The order the stamps are listed in the dropdown menu', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStamps', 'COLUMN', N'DisplayOrder'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated - see CCG_PAT_ConfigStampsDescriptions', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStamps', 'COLUMN', N'Label'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Last modified date/time', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStamps', 'COLUMN', N'ModDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Last modified employee id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStamps', 'COLUMN', N'ModUser'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Pages where the stamp would be placed (All / First / Current)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStamps', 'COLUMN', N'PageSet'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Where you want the stamp to be placed on the document (Top Right / Top Left / Top Center / Center)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStamps', 'COLUMN', N'PositionType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStamps', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Type of the stamp (Text / Image)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStamps', 'COLUMN', N'StampType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Status of the stamp - (A)ctive / (I)nactive', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStamps', 'COLUMN', N'Status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The roles that have access to the stamp. By default all roles have access.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStamps', 'COLUMN', N'ValidRoles'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The size of the stamp (% of page)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStamps', 'COLUMN', N'Width'
GO
