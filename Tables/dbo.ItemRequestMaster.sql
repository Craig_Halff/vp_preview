CREATE TABLE [dbo].[ItemRequestMaster]
(
[RequestNo] [int] NOT NULL CONSTRAINT [DF__ItemReque__Reque__52072E60] DEFAULT ((0)),
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransDate] [datetime] NULL,
[RequestedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Closed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ItemReque__Close__52FB5299] DEFAULT ('N'),
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthorizedDate] [datetime] NULL,
[SubmittedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedDate] [datetime] NULL,
[TotalAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ItemReque__Total__53EF76D2] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ItemRequestMaster] ADD CONSTRAINT [ItemRequestMasterPK] PRIMARY KEY NONCLUSTERED ([MasterPKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
