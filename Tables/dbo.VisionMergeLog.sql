CREATE TABLE [dbo].[VisionMergeLog]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TableName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrigMasterRecs] [int] NULL,
[OrigSecondaryRecs] [int] NULL,
[FinalMasterRecs] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VisionMergeLog] ADD CONSTRAINT [VisionMergeLogPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
