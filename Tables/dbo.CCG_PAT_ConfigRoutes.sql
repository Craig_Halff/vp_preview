CREATE TABLE [dbo].[CCG_PAT_ConfigRoutes]
(
[Route] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RouteLabel] [varchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RouteDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_ConfigRoutes] ADD CONSTRAINT [PK_CCG_PAT_ConfigRoutes] PRIMARY KEY CLUSTERED ([Route]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Basic descriptions of routes', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoutes', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'The name/key of the route', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoutes', 'COLUMN', N'Route'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The route description. <Future use>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoutes', 'COLUMN', N'RouteDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The display label of the route', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoutes', 'COLUMN', N'RouteLabel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Status of the route - (A)ctive / (I)nactive', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoutes', 'COLUMN', N'Status'
GO
