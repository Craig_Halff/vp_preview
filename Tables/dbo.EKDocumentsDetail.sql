CREATE TABLE [dbo].[EKDocumentsDetail]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportDate] [datetime] NOT NULL,
[ReportName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL,
[DetailSeq] [int] NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EKDocumen__Maste__06C77167] DEFAULT (left(replace(newid(),'-',''),(32))),
[DetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EKDocumen__Detai__07BB95A0] DEFAULT (left(replace(newid(),'-',''),(32)))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EKDocumentsDetail] ADD CONSTRAINT [EKDocumentsDetailPK] PRIMARY KEY NONCLUSTERED ([Employee], [ReportDate], [ReportName], [FileID], [DetailSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [EKDocumentsDetailPKeyIDX] ON [dbo].[EKDocumentsDetail] ([MasterPKey], [DetailPKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
