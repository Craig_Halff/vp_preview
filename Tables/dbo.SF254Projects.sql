CREATE TABLE [dbo].[SF254Projects]
(
[SF254ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__SF254Projec__Seq__073ADC05] DEFAULT ((0)),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifiedInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF254Proj__Verif__082F003E] DEFAULT ('N'),
[ProjectProfileCode] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmResponsibilityCode] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OwnerInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompletionDate] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectCost] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentFedInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF254Proj__Curre__09232477] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF254Projects] ADD CONSTRAINT [SF254ProjectsPK] PRIMARY KEY NONCLUSTERED ([SF254ID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
