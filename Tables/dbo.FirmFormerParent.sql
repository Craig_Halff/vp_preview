CREATE TABLE [dbo].[FirmFormerParent]
(
[FirmID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateEstablished] [datetime] NULL,
[DateNameChanged] [datetime] NULL,
[DunsNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FirmFormerParent] ADD CONSTRAINT [FirmFormerParentPK] PRIMARY KEY NONCLUSTERED ([FirmID], [Name]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
