CREATE TABLE [dbo].[CFGCampaignObjectiveDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGCampaign__Seq__434E2F5E] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCampaignObjectiveDescriptions] ADD CONSTRAINT [CFGCampaignObjectiveDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
