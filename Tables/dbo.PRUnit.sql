CREATE TABLE [dbo].[PRUnit]
(
[UnitID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Unit] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UnitTable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlannedQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRUnit__PlannedQ__00CEA0EF] DEFAULT ((0)),
[PlannedCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRUnit__PlannedC__01C2C528] DEFAULT ((0)),
[PlannedBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRUnit__PlannedB__02B6E961] DEFAULT ((0)),
[CostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRUnit__CostRate__03AB0D9A] DEFAULT ((0)),
[BillingRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRUnit__BillingR__049F31D3] DEFAULT ((0)),
[DirectAcctFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRUnit__DirectAc__0593560C] DEFAULT ('N'),
[SeqNo] [smallint] NOT NULL CONSTRAINT [DF__PRUnit__SeqNo__06877A45] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PRUnit] ADD CONSTRAINT [PRUnitPK] PRIMARY KEY NONCLUSTERED ([UnitID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
