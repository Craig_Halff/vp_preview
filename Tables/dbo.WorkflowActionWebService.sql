CREATE TABLE [dbo].[WorkflowActionWebService]
(
[ActionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LocationURL] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WebMethod] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowActionWebService] ADD CONSTRAINT [WorkflowActionWebServicePK] PRIMARY KEY NONCLUSTERED ([ActionID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
