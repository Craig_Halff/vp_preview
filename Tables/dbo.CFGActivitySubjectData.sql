CREATE TABLE [dbo].[CFGActivitySubjectData]
(
[Subject] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGActivity__Seq__6F96C5F0] DEFAULT ((0)),
[isMilestone] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGActivi__isMil__708AEA29] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGActivitySubjectData] ADD CONSTRAINT [CFGActivitySubjectDataPK] PRIMARY KEY CLUSTERED ([Subject], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
