CREATE TABLE [dbo].[ApprovalConditions]
(
[ConditionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Workflow_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColumnName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Operator] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExpectedValue] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExpectedValueDescription] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConditionOrder] [int] NOT NULL CONSTRAINT [DF__ApprovalC__Condi__2D1EF71D] DEFAULT ((0)),
[ConditionOperator] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SQLExpression] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkflowAction_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ApprovalConditions] ADD CONSTRAINT [ApprovalConditionsPK] PRIMARY KEY CLUSTERED ([ConditionID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
