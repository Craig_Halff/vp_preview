CREATE TABLE [dbo].[UDIC_ProjectType_CustUnitsOfMeasure]
(
[UDIC_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustUnitOfMeasure] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_UDIC_ProjectType_CustUnitsOfMeasure]
      ON [dbo].[UDIC_ProjectType_CustUnitsOfMeasure]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'UDIC_ProjectType_CustUnitsOfMeasure'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'UDIC_UID',CONVERT(NVARCHAR(2000),[UDIC_UID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Seq',CONVERT(NVARCHAR(2000),[Seq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustUnitOfMeasure',CONVERT(NVARCHAR(2000),[CustUnitOfMeasure],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_UDIC_ProjectType_CustUnitsOfMeasure]
      ON [dbo].[UDIC_ProjectType_CustUnitsOfMeasure]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'UDIC_ProjectType_CustUnitsOfMeasure'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'UDIC_UID',NULL,CONVERT(NVARCHAR(2000),[UDIC_UID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',NULL,CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustUnitOfMeasure',NULL,CONVERT(NVARCHAR(2000),[CustUnitOfMeasure],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_UDIC_ProjectType_CustUnitsOfMeasure]
      ON [dbo].[UDIC_ProjectType_CustUnitsOfMeasure]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'UDIC_ProjectType_CustUnitsOfMeasure'
    
      If UPDATE([UDIC_UID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'UDIC_UID',
      CONVERT(NVARCHAR(2000),DELETED.[UDIC_UID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UDIC_UID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[UDIC_UID] Is Null And
				DELETED.[UDIC_UID] Is Not Null
			) Or
			(
				INSERTED.[UDIC_UID] Is Not Null And
				DELETED.[UDIC_UID] Is Null
			) Or
			(
				INSERTED.[UDIC_UID] !=
				DELETED.[UDIC_UID]
			)
		) 
		END		
		
      If UPDATE([Seq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',
      CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] Is Null And
				DELETED.[Seq] Is Not Null
			) Or
			(
				INSERTED.[Seq] Is Not Null And
				DELETED.[Seq] Is Null
			) Or
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
		
      If UPDATE([CustUnitOfMeasure])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustUnitOfMeasure',
      CONVERT(NVARCHAR(2000),DELETED.[CustUnitOfMeasure],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustUnitOfMeasure],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustUnitOfMeasure] Is Null And
				DELETED.[CustUnitOfMeasure] Is Not Null
			) Or
			(
				INSERTED.[CustUnitOfMeasure] Is Not Null And
				DELETED.[CustUnitOfMeasure] Is Null
			) Or
			(
				INSERTED.[CustUnitOfMeasure] !=
				DELETED.[CustUnitOfMeasure]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[UDIC_ProjectType_CustUnitsOfMeasure] ADD CONSTRAINT [UDIC_ProjectType_UnitsOfMeasurePK] PRIMARY KEY CLUSTERED ([UDIC_UID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
