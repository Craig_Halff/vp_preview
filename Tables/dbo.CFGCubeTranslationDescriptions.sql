CREATE TABLE [dbo].[CFGCubeTranslationDescriptions]
(
[Usage] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCubeTranslationDescriptions] ADD CONSTRAINT [CFGCubeTranslationDescriptionsPK] PRIMARY KEY NONCLUSTERED ([Usage], [Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
