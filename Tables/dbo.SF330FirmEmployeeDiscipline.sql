CREATE TABLE [dbo].[SF330FirmEmployeeDiscipline]
(
[SF330ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisciplineCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisciplineDesc] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FirmCount] [smallint] NOT NULL CONSTRAINT [DF__SF330Firm__FirmC__41677D9A] DEFAULT ((0)),
[BranchCount] [smallint] NOT NULL CONSTRAINT [DF__SF330Firm__Branc__425BA1D3] DEFAULT ((0)),
[DisciplineSeq] [smallint] NOT NULL CONSTRAINT [DF__SF330Firm__Disci__434FC60C] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF330FirmEmployeeDiscipline] ADD CONSTRAINT [SF330FirmEmployeeDisciplinePK] PRIMARY KEY NONCLUSTERED ([SF330ID], [ID], [DisciplineDesc]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
