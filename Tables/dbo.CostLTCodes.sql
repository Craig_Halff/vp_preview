CREATE TABLE [dbo].[CostLTCodes]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__CostLTCod__Table__4729AD3A] DEFAULT ((0)),
[RateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LaborCodeMask] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EffectiveDate] [datetime] NULL,
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CostLTCode__Rate__481DD173] DEFAULT ((0)),
[OvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CostLTCod__OvtPc__4911F5AC] DEFAULT ((0)),
[Ovt2Pct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CostLTCod__Ovt2P__4A0619E5] DEFAULT ((0)),
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CostLTCodes] ADD CONSTRAINT [CostLTCodesPK] PRIMARY KEY NONCLUSTERED ([TableNo], [RateID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CostLTCodesLaborCodeIDX] ON [dbo].[CostLTCodes] ([TableNo], [LaborCodeMask]) ON [PRIMARY]
GO
