CREATE TABLE [dbo].[DashboardUser]
(
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PartType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PartName] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XML] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hidden] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Dashboard__Hidde__0AAAAEDF] DEFAULT ('N'),
[Global] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Dashboard__Globa__0B9ED318] DEFAULT ('N'),
[ByRole] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Dashboard__ByRol__0C92F751] DEFAULT ('N'),
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DashboardUser] ADD CONSTRAINT [DashboardUserPK] PRIMARY KEY NONCLUSTERED ([UserName], [PartKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
