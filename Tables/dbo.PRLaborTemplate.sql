CREATE TABLE [dbo].[PRLaborTemplate]
(
[LaborID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EstimateHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRLaborTe__Estim__6CC7A842] DEFAULT ((0)),
[EstimateCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRLaborTe__Estim__6DBBCC7B] DEFAULT ((0)),
[EstimateBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRLaborTe__Estim__6EAFF0B4] DEFAULT ((0)),
[CostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRLaborTe__CostR__6FA414ED] DEFAULT ((0)),
[BillingRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRLaborTe__Billi__70983926] DEFAULT ((0)),
[Category] [smallint] NOT NULL CONSTRAINT [DF__PRLaborTe__Categ__718C5D5F] DEFAULT ((0)),
[SeqNo] [smallint] NOT NULL CONSTRAINT [DF__PRLaborTe__SeqNo__72808198] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[GenericResourceID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PRLaborTemplate] ADD CONSTRAINT [PRLaborTemplatePK] PRIMARY KEY NONCLUSTERED ([LaborID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
