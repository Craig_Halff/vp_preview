CREATE TABLE [dbo].[CCG_EI_ConfigRolesDescriptions]
(
[Role] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColumnLabel] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RoleDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_ConfigRolesDescriptions] ADD CONSTRAINT [CCG_EI_ConfigRolesDescriptionsPK] PRIMARY KEY CLUSTERED ([Role], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Contains descriptions for Roles as used for multi-language companies', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRolesDescriptions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The label to use in the column for this role and for this culture code', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRolesDescriptions', 'COLUMN', N'ColumnLabel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*EI Role Key (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRolesDescriptions', 'COLUMN', N'Role'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*<CCG_EI_ConfigRolesDescriptions / RoleDescription Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRolesDescriptions', 'COLUMN', N'RoleDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The UI culture code of this label', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRolesDescriptions', 'COLUMN', N'UICultureName'
GO
