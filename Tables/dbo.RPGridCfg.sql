CREATE TABLE [dbo].[RPGridCfg]
(
[RPGridID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CellHeight] [int] NOT NULL CONSTRAINT [DF__RPGridCfg__CellH__3146246A] DEFAULT ((0)),
[Region1Width] [int] NOT NULL CONSTRAINT [DF__RPGridCfg__Regio__323A48A3] DEFAULT ((0)),
[CalendarCellWidth] [int] NOT NULL CONSTRAINT [DF__RPGridCfg__Calen__332E6CDC] DEFAULT ((0)),
[BaselineHours] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__Basel__34229115] DEFAULT ('N'),
[BaselineCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__Basel__3516B54E] DEFAULT ('N'),
[BaselineBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__Basel__360AD987] DEFAULT ('N'),
[PlannedHours] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__Plann__36FEFDC0] DEFAULT ('N'),
[PlannedCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__Plann__37F321F9] DEFAULT ('N'),
[PlannedBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__Plann__38E74632] DEFAULT ('N'),
[ActualHours] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__Actua__39DB6A6B] DEFAULT ('N'),
[ActualCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__Actua__3ACF8EA4] DEFAULT ('N'),
[ActualBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__Actua__3BC3B2DD] DEFAULT ('N'),
[NonWBSRows] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__NonWB__3CB7D716] DEFAULT ('Y'),
[Gantt] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__Gantt__3DABFB4F] DEFAULT ('N'),
[Revenue] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__Reven__3EA01F88] DEFAULT ('N'),
[BaselineQty] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__Basel__3F9443C1] DEFAULT ('N'),
[PlannedQty] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__Plann__408867FA] DEFAULT ('N'),
[CostRate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__CostR__417C8C33] DEFAULT ('N'),
[BillRate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__BillR__4270B06C] DEFAULT ('N'),
[ActualQty] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__Actua__4364D4A5] DEFAULT ('N'),
[FreezeColName] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EVPct] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__EVPct__4458F8DE] DEFAULT ('N'),
[CompCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__CompC__454D1D17] DEFAULT ('N'),
[CompBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__CompB__46414150] DEFAULT ('N'),
[ConsCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__ConsC__47356589] DEFAULT ('N'),
[ConsBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__ConsB__482989C2] DEFAULT ('N'),
[ReimCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__ReimC__491DADFB] DEFAULT ('N'),
[ReimBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__ReimB__4A11D234] DEFAULT ('N'),
[CompDirLabCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__CompD__4B05F66D] DEFAULT ('N'),
[CompDirExpCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__CompD__4BFA1AA6] DEFAULT ('N'),
[CompDirLabBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__CompD__4CEE3EDF] DEFAULT ('N'),
[CompDirExpBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__CompD__4DE26318] DEFAULT ('N'),
[ReimExpCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__ReimE__4ED68751] DEFAULT ('N'),
[ReimConCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__ReimC__4FCAAB8A] DEFAULT ('N'),
[ReimExpBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__ReimE__50BECFC3] DEFAULT ('N'),
[ReimConBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGridCfg__ReimC__51B2F3FC] DEFAULT ('N'),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPGridCfg] ADD CONSTRAINT [RPGridCfgPK] PRIMARY KEY NONCLUSTERED ([RPGridID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
