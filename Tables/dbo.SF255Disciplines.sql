CREATE TABLE [dbo].[SF255Disciplines]
(
[SF255ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Discipline] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__SF255Discip__Seq__1E1E415D] DEFAULT ((0)),
[ConsultantCount] [smallint] NOT NULL CONSTRAINT [DF__SF255Disc__Consu__1F126596] DEFAULT ((0)),
[CompanyCount] [smallint] NOT NULL CONSTRAINT [DF__SF255Disc__Compa__200689CF] DEFAULT ((0)),
[IncludeCommentInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF255Disc__Inclu__20FAAE08] DEFAULT ('N'),
[DisciplineCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF255Disciplines] ADD CONSTRAINT [SF255DisciplinesPK] PRIMARY KEY NONCLUSTERED ([SF255ID], [Discipline]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
