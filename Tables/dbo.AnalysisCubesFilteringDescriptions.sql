CREATE TABLE [dbo].[AnalysisCubesFilteringDescriptions]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GroupNameDescription] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FolderNameDescription] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Label] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AnalysisCubesFilteringDescriptions] ADD CONSTRAINT [AnalysisCubesFilteringDescriptionsPK] PRIMARY KEY NONCLUSTERED ([PKey], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
