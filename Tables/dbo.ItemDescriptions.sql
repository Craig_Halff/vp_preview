CREATE TABLE [dbo].[ItemDescriptions]
(
[Item] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ItemDescriptions] ADD CONSTRAINT [ItemDescriptionsPK] PRIMARY KEY NONCLUSTERED ([Item], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
