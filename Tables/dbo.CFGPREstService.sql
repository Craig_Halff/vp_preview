CREATE TABLE [dbo].[CFGPREstService]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EstServiceFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPREstS__EstSe__335A20BC] DEFAULT ('N'),
[ExpenseFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPREstS__Expen__344E44F5] DEFAULT ('Y'),
[ConsultantFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPREstS__Consu__3542692E] DEFAULT ('Y'),
[UnitFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPREstS__UnitF__36368D67] DEFAULT ('Y'),
[EstimateType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPREstS__Estim__372AB1A0] DEFAULT ('C'),
[LabCostTable] [int] NOT NULL CONSTRAINT [DF__CFGPREstS__LabCo__381ED5D9] DEFAULT ((0)),
[LabBillTable] [int] NOT NULL CONSTRAINT [DF__CFGPREstS__LabBi__3912FA12] DEFAULT ((0)),
[ExpBillMultiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPREstS__ExpBi__3A071E4B] DEFAULT ((0)),
[ConBillMultiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPREstS__ConBi__3AFB4284] DEFAULT ((0)),
[HrDecimals] [smallint] NOT NULL CONSTRAINT [DF__CFGPREstS__HrDec__3BEF66BD] DEFAULT ((0)),
[QtyDecimals] [smallint] NOT NULL CONSTRAINT [DF__CFGPREstS__QtyDe__3CE38AF6] DEFAULT ((0)),
[AmtDecimals] [smallint] NOT NULL CONSTRAINT [DF__CFGPREstS__AmtDe__3DD7AF2F] DEFAULT ((0)),
[BVSURL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPREstService] ADD CONSTRAINT [CFGPREstServicePK] PRIMARY KEY CLUSTERED ([Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
