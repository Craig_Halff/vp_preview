CREATE TABLE [dbo].[EKDocuments]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportDate] [datetime] NOT NULL,
[ReportName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL,
[AssociateNew] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EKDocumen__Maste__05D34D2E] DEFAULT (left(replace(newid(),'-',''),(32)))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EKDocuments] ADD CONSTRAINT [EKDocumentsPK] PRIMARY KEY NONCLUSTERED ([Employee], [ReportDate], [ReportName], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [EKDocumentsPKeyIDX] ON [dbo].[EKDocuments] ([MasterPKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
