CREATE TABLE [dbo].[KeyConvertWork]
(
[PKey] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KeyConvert__PKey__12E0CD84] DEFAULT (newid()),
[Entity] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldKey] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldKey2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldKey3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewKey] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewKey2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewKey3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBSName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[KeyConvertWork] ADD CONSTRAINT [KeyConvertWorkPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
