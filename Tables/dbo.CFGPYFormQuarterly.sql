CREATE TABLE [dbo].[CFGPYFormQuarterly]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProcessingState] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[W2Year] [smallint] NOT NULL CONSTRAINT [DF__CFGPYForm__W2Yea__4CC28AFF] DEFAULT ((0)),
[W2Quarter] [smallint] NOT NULL CONSTRAINT [DF__CFGPYForm__W2Qua__4DB6AF38] DEFAULT ((0)),
[FederalEIN] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateUIAccountNumber] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIP] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Contact] [nvarchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactTitle] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactNumber] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactExtension] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactFAX] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactEMail] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WageBase] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__WageB__4EAAD371] DEFAULT ((0)),
[SocialSecurityWageBase] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__Socia__4F9EF7AA] DEFAULT ((0)),
[Exclude401K] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYForm__Exclu__50931BE3] DEFAULT ('N'),
[ExcludeCafeteria] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYForm__Exclu__5187401C] DEFAULT ('N'),
[IncludeOnlyStateEmployees] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYForm__Inclu__527B6455] DEFAULT ('N'),
[TestFile] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYForm__TestF__536F888E] DEFAULT ('Y'),
[TAN] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[C3] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuffixCode] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllocationLists] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceAgentID] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RemittanceAmount] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreviousQuarterAdjustments] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__Previ__5463ACC7] DEFAULT ((0)),
[Credit_OverPayment] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__Credi__5557D100] DEFAULT ((0)),
[BranchCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SeasonalIndicator] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssessmentRate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UITaxRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__UITax__564BF539] DEFAULT ((0)),
[DocumentControlNumber] [smallint] NOT NULL CONSTRAINT [DF__CFGPYForm__Docum__57401972] DEFAULT ((0)),
[InterestOnLatePayment] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__Inter__58343DAB] DEFAULT ((0)),
[Penalty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__Penal__592861E4] DEFAULT ((0)),
[WagePlanCodeA] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WagePlanCodeB] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TypeOfReturn] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EstablishmentNumber_PRU] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoWorkers_NoWages] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxTypeCode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxingEntityCode] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateControlNumber] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlantCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OtherEIN] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OfficerCode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CoverageGroup] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayrollRecordUnit] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MagneticAuthorizationNumber] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxJurisdictionCode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MultipleCountyIndustry] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MultipleWorksiteLocation] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MultipleWorksiteIndicator] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ElectronicFundsTransferIndicator] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TerminatingBusinessIndicator] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndustryCode] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WithholdingAccountNumber] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateLocalNumber] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LimitationOfLiability] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitDivisionLocationPlantCode] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgentIDNumber] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ComputerManufacturer] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileName] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount1] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__Amoun__5A1C861D] DEFAULT ((0)),
[Amount2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__Amoun__5B10AA56] DEFAULT ((0)),
[Amount3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__Amoun__5C04CE8F] DEFAULT ((0)),
[Amount4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__Amoun__5CF8F2C8] DEFAULT ((0)),
[Amount5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__Amoun__5DED1701] DEFAULT ((0)),
[Amount6] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__Amoun__5EE13B3A] DEFAULT ((0)),
[Amount7] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__Amoun__5FD55F73] DEFAULT ((0)),
[Amount8] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__Amoun__60C983AC] DEFAULT ((0)),
[Amount9] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__Amoun__61BDA7E5] DEFAULT ((0)),
[Amount10] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYForm__Amoun__62B1CC1E] DEFAULT ((0)),
[AdditionalOptions] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAICSCode] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OutsideCountyEmployees] [smallint] NOT NULL CONSTRAINT [DF__CFGPYForm__Outsi__63A5F057] DEFAULT ((0)),
[ContactNumberFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactFaxFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPYFormQuarterly] ADD CONSTRAINT [CFGPYFormQuarterlyPK] PRIMARY KEY CLUSTERED ([Company], [ProcessingState]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
