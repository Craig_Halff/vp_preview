CREATE TABLE [dbo].[SF255Projects]
(
[SF255ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FederalInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF255Proj__Feder__303CF198] DEFAULT ('N'),
[Seq] [smallint] NOT NULL CONSTRAINT [DF__SF255Projec__Seq__313115D1] DEFAULT ((0)),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifiedInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF255Proj__Verif__32253A0A] DEFAULT ('N'),
[ProjectInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Responsibility] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OwnerInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompletionDate] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PercentageComplete] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmResponsibilityCost] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TotalProjectCost] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GraphicPath] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF255Projects] ADD CONSTRAINT [SF255ProjectsPK] PRIMARY KEY NONCLUSTERED ([SF255ID], [FederalInd], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
