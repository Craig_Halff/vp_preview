CREATE TABLE [dbo].[APExpenseCodeOTCodes]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__APExpense__Table__799F5317] DEFAULT ((0)),
[ExpenseCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BillByDefault] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__APExpense__BillB__7A937750] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[APExpenseCodeOTCodes] ADD CONSTRAINT [APExpenseCodeOTCodesPK] PRIMARY KEY CLUSTERED ([TableNo], [ExpenseCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
