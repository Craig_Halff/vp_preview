CREATE TABLE [dbo].[exChecksTax]
(
[Period] [int] NOT NULL CONSTRAINT [DF__exChecksT__Perio__3D212013] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__exChecksT__PostS__3E15444C] DEFAULT ((0)),
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CheckSeq] [int] NOT NULL CONSTRAINT [DF__exChecksT__Check__3F096885] DEFAULT ((0)),
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__exChecksT__TaxAm__3FFD8CBE] DEFAULT ((0)),
[CompoundOnTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__exChecksTax__Seq__40F1B0F7] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[exChecksTax] ADD CONSTRAINT [exChecksTaxPK] PRIMARY KEY NONCLUSTERED ([Period], [PostSeq], [Employee], [CheckSeq], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
