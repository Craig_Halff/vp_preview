CREATE TABLE [dbo].[Contacts_FirmHistory]
(
[ContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustFHDate] [datetime] NULL CONSTRAINT [DF__Contacts___CustF__6F6CA8BE] DEFAULT (getdate()),
[CustFHCompany] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFHTitle] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_Contacts_FirmHistory]
      ON [dbo].[Contacts_FirmHistory]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Contacts_FirmHistory'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'ContactID',CONVERT(NVARCHAR(2000),[ContactID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Seq',CONVERT(NVARCHAR(2000),[Seq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustFHDate',CONVERT(NVARCHAR(2000),[CustFHDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustFHCompany',CONVERT(NVARCHAR(2000),[CustFHCompany],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustFHTitle',CONVERT(NVARCHAR(2000),[CustFHTitle],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_Contacts_FirmHistory] ON [dbo].[Contacts_FirmHistory]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_Contacts_FirmHistory]
      ON [dbo].[Contacts_FirmHistory]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Contacts_FirmHistory'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'ContactID',NULL,CONVERT(NVARCHAR(2000),[ContactID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',NULL,CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustFHDate',NULL,CONVERT(NVARCHAR(2000),[CustFHDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustFHCompany',NULL,CONVERT(NVARCHAR(2000),[CustFHCompany],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustFHTitle',NULL,CONVERT(NVARCHAR(2000),[CustFHTitle],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_Contacts_FirmHistory] ON [dbo].[Contacts_FirmHistory]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_Contacts_FirmHistory]
      ON [dbo].[Contacts_FirmHistory]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Contacts_FirmHistory'
    
      If UPDATE([ContactID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'ContactID',
      CONVERT(NVARCHAR(2000),DELETED.[ContactID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ContactID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[ContactID] Is Null And
				DELETED.[ContactID] Is Not Null
			) Or
			(
				INSERTED.[ContactID] Is Not Null And
				DELETED.[ContactID] Is Null
			) Or
			(
				INSERTED.[ContactID] !=
				DELETED.[ContactID]
			)
		) 
		END		
		
      If UPDATE([Seq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',
      CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] Is Null And
				DELETED.[Seq] Is Not Null
			) Or
			(
				INSERTED.[Seq] Is Not Null And
				DELETED.[Seq] Is Null
			) Or
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
		
      If UPDATE([CustFHDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustFHDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustFHDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFHDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustFHDate] Is Null And
				DELETED.[CustFHDate] Is Not Null
			) Or
			(
				INSERTED.[CustFHDate] Is Not Null And
				DELETED.[CustFHDate] Is Null
			) Or
			(
				INSERTED.[CustFHDate] !=
				DELETED.[CustFHDate]
			)
		) 
		END		
		
      If UPDATE([CustFHCompany])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustFHCompany',
      CONVERT(NVARCHAR(2000),DELETED.[CustFHCompany],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFHCompany],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustFHCompany] Is Null And
				DELETED.[CustFHCompany] Is Not Null
			) Or
			(
				INSERTED.[CustFHCompany] Is Not Null And
				DELETED.[CustFHCompany] Is Null
			) Or
			(
				INSERTED.[CustFHCompany] !=
				DELETED.[CustFHCompany]
			)
		) 
		END		
		
      If UPDATE([CustFHTitle])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustFHTitle',
      CONVERT(NVARCHAR(2000),DELETED.[CustFHTitle],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFHTitle],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustFHTitle] Is Null And
				DELETED.[CustFHTitle] Is Not Null
			) Or
			(
				INSERTED.[CustFHTitle] Is Not Null And
				DELETED.[CustFHTitle] Is Null
			) Or
			(
				INSERTED.[CustFHTitle] !=
				DELETED.[CustFHTitle]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_Contacts_FirmHistory] ON [dbo].[Contacts_FirmHistory]
GO
ALTER TABLE [dbo].[Contacts_FirmHistory] ADD CONSTRAINT [Contacts_FirmHistoryPK] PRIMARY KEY CLUSTERED ([ContactID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
