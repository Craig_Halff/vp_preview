CREATE TABLE [dbo].[BulkUpdateHistory]
(
[BulkUpdateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RunDate] [datetime] NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BulkUpdateHistory] ADD CONSTRAINT [BulkUpdateHistoryPK] PRIMARY KEY NONCLUSTERED ([BulkUpdateID], [UserName], [RunDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
