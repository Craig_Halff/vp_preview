CREATE TABLE [dbo].[CFGEKMain]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AllowResubmit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__Allow__3C6C27A5] DEFAULT ('N'),
[AmountPerMile] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGEKMain__Amoun__3D604BDE] DEFAULT ((0)),
[AllowCompanyPaid] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__Allow__3E547017] DEFAULT ('N'),
[AllowAdvances] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__Allow__3F489450] DEFAULT ('N'),
[DistanceType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CategoriesRequired] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__Categ__403CB889] DEFAULT ('N'),
[CompanyPaidWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompanyPaidWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompanyPaidWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompanyPaidAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisableLogin] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__Disab__4130DCC2] DEFAULT ('N'),
[ApprovalRequired] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__Appro__422500FB] DEFAULT ('N'),
[ElectronicSignature] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__Elect__43192534] DEFAULT ('N'),
[AllowSubmitterToApprove] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__Allow__440D496D] DEFAULT ('N'),
[ShowWBS1] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__ShowW__45016DA6] DEFAULT ('1'),
[ShowWBS2] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__ShowW__45F591DF] DEFAULT ('1'),
[ShowWBS3] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__ShowW__46E9B618] DEFAULT ('1'),
[ShowAccount] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__ShowA__47DDDA51] DEFAULT ('1'),
[TreatInactiveAsDormant] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__Treat__48D1FE8A] DEFAULT ('N'),
[SigningMemo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisallowEditAmountPerMile] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__Disal__49C622C3] DEFAULT ('N'),
[ShowSeqNumber] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__ShowS__4ABA46FC] DEFAULT ('N'),
[UseApprovalWorkflow] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__UseAp__4BAE6B35] DEFAULT ('N'),
[Workflow_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LogFixApprovalErrorsEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKMain__LogFi__56243A0C] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGEKMain] ADD CONSTRAINT [CFGEKMainPK] PRIMARY KEY CLUSTERED ([Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
