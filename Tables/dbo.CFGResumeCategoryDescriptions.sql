CREATE TABLE [dbo].[CFGResumeCategoryDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGResumeCa__Seq__4F69ED80] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGResumeCategoryDescriptions] ADD CONSTRAINT [CFGResumeCategoryDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
