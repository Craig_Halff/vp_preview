CREATE TABLE [dbo].[Contacts_ClientSurveyHistory]
(
[ContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustProjectNumber] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustSurveyDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_Contacts_ClientSurveyHistory]
      ON [dbo].[Contacts_ClientSurveyHistory]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Contacts_ClientSurveyHistory'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'ContactID',CONVERT(NVARCHAR(2000),[ContactID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Seq',CONVERT(NVARCHAR(2000),[Seq],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustProjectNumber',CONVERT(NVARCHAR(2000),DELETED.[CustProjectNumber],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join PR as oldDesc   on DELETED.CustProjectNumber = oldDesc.WBS1

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustSurveyDate',CONVERT(NVARCHAR(2000),[CustSurveyDate],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_Contacts_ClientSurveyHistory] ON [dbo].[Contacts_ClientSurveyHistory]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_Contacts_ClientSurveyHistory]
      ON [dbo].[Contacts_ClientSurveyHistory]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Contacts_ClientSurveyHistory'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'ContactID',NULL,CONVERT(NVARCHAR(2000),[ContactID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',NULL,CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustProjectNumber',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustProjectNumber],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  PR as newDesc  on INSERTED.CustProjectNumber = newDesc.WBS1

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustSurveyDate',NULL,CONVERT(NVARCHAR(2000),[CustSurveyDate],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_Contacts_ClientSurveyHistory] ON [dbo].[Contacts_ClientSurveyHistory]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_Contacts_ClientSurveyHistory]
      ON [dbo].[Contacts_ClientSurveyHistory]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Contacts_ClientSurveyHistory'
    
      If UPDATE([ContactID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'ContactID',
      CONVERT(NVARCHAR(2000),DELETED.[ContactID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ContactID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[ContactID] Is Null And
				DELETED.[ContactID] Is Not Null
			) Or
			(
				INSERTED.[ContactID] Is Not Null And
				DELETED.[ContactID] Is Null
			) Or
			(
				INSERTED.[ContactID] !=
				DELETED.[ContactID]
			)
		) 
		END		
		
      If UPDATE([Seq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',
      CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] Is Null And
				DELETED.[Seq] Is Not Null
			) Or
			(
				INSERTED.[Seq] Is Not Null And
				DELETED.[Seq] Is Null
			) Or
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
		
     If UPDATE([CustProjectNumber])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustProjectNumber',
     CONVERT(NVARCHAR(2000),DELETED.[CustProjectNumber],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustProjectNumber],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[ContactID] = DELETED.[ContactID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustProjectNumber] Is Null And
				DELETED.[CustProjectNumber] Is Not Null
			) Or
			(
				INSERTED.[CustProjectNumber] Is Not Null And
				DELETED.[CustProjectNumber] Is Null
			) Or
			(
				INSERTED.[CustProjectNumber] !=
				DELETED.[CustProjectNumber]
			)
		) left join PR as oldDesc  on DELETED.CustProjectNumber = oldDesc.WBS1  left join  PR as newDesc  on INSERTED.CustProjectNumber = newDesc.WBS1
		END		
		
      If UPDATE([CustSurveyDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustSurveyDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustSurveyDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSurveyDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContactID] = DELETED.[ContactID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustSurveyDate] Is Null And
				DELETED.[CustSurveyDate] Is Not Null
			) Or
			(
				INSERTED.[CustSurveyDate] Is Not Null And
				DELETED.[CustSurveyDate] Is Null
			) Or
			(
				INSERTED.[CustSurveyDate] !=
				DELETED.[CustSurveyDate]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_Contacts_ClientSurveyHistory] ON [dbo].[Contacts_ClientSurveyHistory]
GO
ALTER TABLE [dbo].[Contacts_ClientSurveyHistory] ADD CONSTRAINT [Contacts_ClientSurveyHistoryPK] PRIMARY KEY CLUSTERED ([ContactID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
