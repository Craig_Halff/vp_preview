CREATE TABLE [dbo].[POPRDocuments]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL,
[AssociateNew] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POPRDocum__Assoc__1DDC1E2F] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__POPRDocumen__Seq__1ED04268] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POPRDocuments] ADD CONSTRAINT [POPRDocumentsPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
