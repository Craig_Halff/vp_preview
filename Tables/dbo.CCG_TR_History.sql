CREATE TABLE [dbo].[CCG_TR_History]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[OriginalTable] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OriginalPeriod] [int] NOT NULL,
[OriginalPostSeq] [int] NOT NULL,
[OriginalPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NewTable] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewPeriod] [int] NULL,
[NewPostSeq] [int] NULL,
[NewPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionTaken] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionDetail] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionDate] [datetime] NOT NULL,
[ActionTakenBy] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldValue] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewValue] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldValueDecimal] [decimal] (19, 5) NULL,
[NewValueDecimal] [decimal] (19, 5) NULL,
[ModificationComment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovalComment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLastNotificationSent] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_TR_History] ADD CONSTRAINT [PK_CCG_TR_History] PRIMARY KEY CLUSTERED ([Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_CCG_TR_History_OriginalTableFields] ON [dbo].[CCG_TR_History] ([OriginalTable], [OriginalPeriod], [OriginalPostSeq], [OriginalPKey]) ON [PRIMARY]
GO
