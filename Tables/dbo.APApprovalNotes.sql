CREATE TABLE [dbo].[APApprovalNotes]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Comments] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[APApprovalNotes] ADD CONSTRAINT [APApprovalNotesPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
