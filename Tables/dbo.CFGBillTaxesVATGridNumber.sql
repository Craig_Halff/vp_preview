CREATE TABLE [dbo].[CFGBillTaxesVATGridNumber]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NetAmountGridNo] [int] NOT NULL CONSTRAINT [DF__CFGBillTa__NetAm__37DC7CB2] DEFAULT ((0)),
[TaxAmountGridNo] [int] NOT NULL CONSTRAINT [DF__CFGBillTa__TaxAm__38D0A0EB] DEFAULT ((0)),
[NonRecoverTaxAmtGridNo] [int] NOT NULL CONSTRAINT [DF__CFGBillTa__NonRe__39C4C524] DEFAULT ((0)),
[RecoverTaxAmtGridNo] [int] NOT NULL CONSTRAINT [DF__CFGBillTa__Recov__3AB8E95D] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGBillTaxesVATGridNumber] ADD CONSTRAINT [CFGBillTaxesVATGridNumberPK] PRIMARY KEY CLUSTERED ([Code], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
