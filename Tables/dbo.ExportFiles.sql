CREATE TABLE [dbo].[ExportFiles]
(
[PKey] [uniqueidentifier] NOT NULL CONSTRAINT [DF__ExportFile__PKey__02EBF38B] DEFAULT (newid()),
[FileID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__ExportFil__FileI__03E017C4] DEFAULT (newid()),
[ExportType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ExportFiles] ADD CONSTRAINT [ExportFilesPK] PRIMARY KEY NONCLUSTERED ([PKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
