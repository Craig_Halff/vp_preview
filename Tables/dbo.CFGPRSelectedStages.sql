CREATE TABLE [dbo].[CFGPRSelectedStages]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPRSelectedStages] ADD CONSTRAINT [CFGPRSelectedStagesPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
