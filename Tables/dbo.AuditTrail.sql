CREATE TABLE [dbo].[AuditTrail]
(
[ModUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[TableName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrimaryKey] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColumnName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OldValueDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldValue] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewValueDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewValue] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Application] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuditID] [bigint] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditTrail] ADD CONSTRAINT [AuditTrailPK] PRIMARY KEY CLUSTERED ([AuditID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [AuditTrailModDateIDX] ON [dbo].[AuditTrail] ([ModDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [AuditTrailTableNameIDX] ON [dbo].[AuditTrail] ([TableName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
