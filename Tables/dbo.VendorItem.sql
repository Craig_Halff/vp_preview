CREATE TABLE [dbo].[VendorItem]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Item] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UOM] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastPrice] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__VendorIte__LastP__6ABE98E8] DEFAULT ((0)),
[LastPriceDate] [datetime] NULL,
[TotalQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__VendorIte__Total__6BB2BD21] DEFAULT ((0)),
[TotalAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__VendorIte__Total__6CA6E15A] DEFAULT ((0)),
[VendorItem] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastPriceCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VendorItem] ADD CONSTRAINT [VendorItemPK] PRIMARY KEY NONCLUSTERED ([Company], [Vendor], [Item]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
