CREATE TABLE [dbo].[laControl]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PostPeriod] [int] NOT NULL CONSTRAINT [DF__laControl__PostP__1A81EF4C] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__laControl__PostS__1B761385] DEFAULT ((0)),
[Recurring] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__laControl__Recur__1C6A37BE] DEFAULT ('N'),
[Selected] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__laControl__Selec__1D5E5BF7] DEFAULT ('N'),
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__laControl__Poste__1E528030] DEFAULT ('N'),
[Creator] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__laControl__Perio__1F46A469] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[RegHrsTotal] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laControl__RegHr__203AC8A2] DEFAULT ((0)),
[OvtHrsTotal] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laControl__OvtHr__212EECDB] DEFAULT ((0)),
[RegAmtTotal] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laControl__RegAm__22231114] DEFAULT ((0)),
[OvtAmtTotal] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laControl__OvtAm__2317354D] DEFAULT ((0)),
[BillExtTotal] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laControl__BillE__240B5986] DEFAULT ((0)),
[SpecialOvtHrsTotal] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laControl__Speci__24FF7DBF] DEFAULT ((0)),
[SpecialOvtAmtTotal] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laControl__Speci__25F3A1F8] DEFAULT ((0)),
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RegAmtProjectFunctionalCurrencyTotal] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laControl__RegAm__26E7C631] DEFAULT ((0)),
[OvtAmtProjectFunctionalCurrencyTotal] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laControl__OvtAm__27DBEA6A] DEFAULT ((0)),
[SpecialOvtAmtProjectFunctionalCurrencyTotal] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laControl__Speci__28D00EA3] DEFAULT ((0)),
[RealizationTotal] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__laControl__Reali__29C432DC] DEFAULT ((0)),
[SubmittedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedDate] [datetime] NULL,
[Diary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaryNo] [int] NOT NULL CONSTRAINT [DF__laControl__Diary__2AB85715] DEFAULT ((0)),
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[laControl] ADD CONSTRAINT [laControlPK] PRIMARY KEY NONCLUSTERED ([Batch]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
