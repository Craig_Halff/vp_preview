CREATE TABLE [dbo].[UNDocuments]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL,
[AssociateNew] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__UNDocumen__Assoc__0B6072A4] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__UNDocuments__Seq__0C5496DD] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UNDocuments] ADD CONSTRAINT [unDocumentsPK] PRIMARY KEY NONCLUSTERED ([Batch], [MasterPKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
