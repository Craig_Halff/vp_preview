CREATE TABLE [dbo].[AnalysisCubesCurrencyExchange]
(
[RateID] [int] NOT NULL,
[FromCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EffectiveDate] [datetime] NULL,
[Rate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__AnalysisCu__Rate__3AADFC65] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AnalysisCubesCurrencyExchange] ADD CONSTRAINT [AnalysisCubesCurrencyExchangePK] PRIMARY KEY CLUSTERED ([RateID], [FromCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
