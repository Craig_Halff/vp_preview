CREATE TABLE [dbo].[tkRevisionMaster]
(
[EndDate] [datetime] NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Revision] [smallint] NOT NULL CONSTRAINT [DF__tkRevisio__Revis__47DF70FF] DEFAULT ((0)),
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[OldStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevisionExplanation] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tkRevisionMaster] ADD CONSTRAINT [tkRevisionMasterPK] PRIMARY KEY NONCLUSTERED ([EndDate], [Employee], [EmployeeCompany], [Revision]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
