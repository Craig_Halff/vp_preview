CREATE TABLE [dbo].[CFGAutoNumWebServiceArgs]
(
[RuleID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ArgName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ArgOrder] [int] NOT NULL CONSTRAINT [DF__CFGAutoNu__ArgOr__36293E6A] DEFAULT ((0)),
[NavID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAutoNu__NavID__371D62A3] DEFAULT (' '),
[SQLExpression] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SQLIfExpression] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SQLElseExpression] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAutoNumWebServiceArgs] ADD CONSTRAINT [CFGAutoNumWebServiceArgsPK] PRIMARY KEY NONCLUSTERED ([RuleID], [ArgName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
