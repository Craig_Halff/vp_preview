CREATE TABLE [dbo].[SF330PersonnelMatrix]
(
[SF330ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmpID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmpSeq] [smallint] NOT NULL CONSTRAINT [DF__SF330Pers__EmpSe__5756BEB9] DEFAULT ((0)),
[EmployeeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Role] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF330PersonnelMatrix] ADD CONSTRAINT [SF330PersonnelMatrixPK] PRIMARY KEY NONCLUSTERED ([SF330ID], [EmpID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
