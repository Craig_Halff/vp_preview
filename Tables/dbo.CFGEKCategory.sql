CREATE TABLE [dbo].[CFGEKCategory]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Category] [int] NOT NULL CONSTRAINT [DF__CFGEKCate__Categ__30FA74F9] DEFAULT ((0)),
[EKGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DirectAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReimbAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndirectAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DetailType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillByDefault] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKCate__BillB__31EE9932] DEFAULT ('N'),
[BillableWarning] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowAccountChange] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKCate__Allow__32E2BD6B] DEFAULT ('N'),
[CompanyPaidWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompanyPaidWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompanyPaidWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompanyPaidAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowTaxCodeChange] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKCate__Allow__33D6E1A4] DEFAULT ('N'),
[AllowTaxAmtChange] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKCate__Allow__34CB05DD] DEFAULT ('N'),
[AmountPerMile] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGEKCate__Amoun__35BF2A16] DEFAULT ((0)),
[DistanceType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisallowEditAmountPerMile] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalculateDistanceUnits] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKCate__Calcu__36B34E4F] DEFAULT ('Y'),
[CategoryID] [int] NOT NULL IDENTITY(1, 1),
[PromotionalAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKCate__Statu__630933DE] DEFAULT ('A'),
[ExpenseType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKCate__Expen__7122490B] DEFAULT ('O'),
[AvailableToEXReports] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKCate__Avail__72166D44] DEFAULT ('Y'),
[AvailableToAPVouchers] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKCate__Avail__730A917D] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGEKCategory] ADD CONSTRAINT [CFGEKCategoryPK] PRIMARY KEY CLUSTERED ([Company], [Category], [EKGroup]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
