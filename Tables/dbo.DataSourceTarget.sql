CREATE TABLE [dbo].[DataSourceTarget]
(
[TargetID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TargetName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TargetType] [int] NOT NULL,
[FTPUrl] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FTPType] [int] NULL,
[FTPPort] [int] NULL,
[FTPUsername] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FTPPassword] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FTPLocation] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMailRecipients] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMailCC] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMailBCC] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMailSubject] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMailBody] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DataSourceTarget] ADD CONSTRAINT [DataSourceTargetPK] PRIMARY KEY CLUSTERED ([TargetID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
