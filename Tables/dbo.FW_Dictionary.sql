CREATE TABLE [dbo].[FW_Dictionary]
(
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Dictionary] [varbinary] (max) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_Dictionary] ADD CONSTRAINT [FW_DictionaryPK] PRIMARY KEY NONCLUSTERED ([Username], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
