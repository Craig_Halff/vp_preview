CREATE TABLE [dbo].[VisionImportAPVoucher_APH]
(
[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Voucher] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL,
[Invoice] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VoucherDate] [datetime] NULL,
[LiabilityCode] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankCode] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentTerms] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentDate] [datetime] NULL
) ON [PRIMARY]
GO
