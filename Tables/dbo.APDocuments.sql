CREATE TABLE [dbo].[APDocuments]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL,
[AssociateNew] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__APDocumen__Assoc__74DA9DFA] DEFAULT ('N'),
[Seq] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[APDocuments] ADD CONSTRAINT [apDocumentsPK] PRIMARY KEY NONCLUSTERED ([Batch], [MasterPKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
