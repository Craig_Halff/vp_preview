CREATE TABLE [dbo].[CCG_EI_CustomColumns]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Comments] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments_ModEmp] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status_ModEmp] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CA_Notes] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CA_Notes_ModEmp] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_CustomColumns] ADD CONSTRAINT [PK_CCG_EI_CustomColumns] PRIMARY KEY CLUSTERED ([WBS1], [WBS2], [WBS3]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'A table intended to be customized with custom columns whenever requested.  Additional fields in this table will be specific to the implementation and vary from database to database', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_CustomColumns', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The WBS1 number (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_CustomColumns', 'COLUMN', N'WBS1'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The WBS2 number (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_CustomColumns', 'COLUMN', N'WBS2'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The WBS3 number (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_CustomColumns', 'COLUMN', N'WBS3'
GO
