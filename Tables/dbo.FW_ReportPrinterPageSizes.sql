CREATE TABLE [dbo].[FW_ReportPrinterPageSizes]
(
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrinterName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RawKind] [int] NOT NULL CONSTRAINT [DF__FW_Report__RawKi__7E4FC4F2] DEFAULT ((0)),
[PageSizeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_ReportPrinterPageSizes] ADD CONSTRAINT [FW_ReportPrinterPageSizesPK] PRIMARY KEY NONCLUSTERED ([UICultureName], [PrinterName], [RawKind]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
