CREATE TABLE [dbo].[CCG_PAT_Config]
(
[Version] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Series] [int] NOT NULL,
[ActivationPassword] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LogLevel] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LogFile] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VoucherNumberLength] [int] NULL,
[ProjectNameLength] [int] NULL,
[AcctRoles] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SupportMessages] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConfigEmployees] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NotificationInterval] [tinyint] NOT NULL,
[SupportPeriodBillThru] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupportDateBillThru] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterSQL] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubgridMode] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShowResetVisible] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FreezeColumns] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeDisplay] [tinyint] NULL,
[VendorDisplay] [tinyint] NULL,
[AccountDisplay] [tinyint] NULL,
[BankCodeDefault] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLAccountDefault] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultContractFilename] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultInvoiceFilename] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultTransactionFilename] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultSourcePath] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractEnabled] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GridHidePrName] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShowVisionAmounts] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultGLAccount] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLAccountStatus] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLAccountType] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefPrLookupStatus] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefPrLookupChargeType] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ControlAmount] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailLink] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_PAT_C__Email__6A129330] DEFAULT (''),
[EnforceWindowsUsernameInLink] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_PAT_C__Enfor__6B06B769] DEFAULT ('N'),
[EncTimeout] [int] NOT NULL CONSTRAINT [DF__CCG_PAT_C__EncTi__6BFADBA2] DEFAULT ((7)),
[ExportDocs] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VoucherDate] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GroupsEnabled] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POMode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdvancedOptions] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Delegation] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_PAT_C__Deleg__6CEEFFDB] DEFAULT ('N'),
[ParallelApproval] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_PAT_C__Paral__6DE32414] DEFAULT ('N'),
[EmployeeSelfDelegation] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_PAT_C__Emplo__6ED7484D] DEFAULT ('N'),
[DelegationApproval] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_PAT_C__Deleg__6FCB6C86] DEFAULT ('N'),
[DelegationBySupervisor] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_PAT_C__Deleg__70BF90BF] DEFAULT ('N'),
[DelegationEmployeesList] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DelegationDualAccess] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_PAT_C__Deleg__71B3B4F8] DEFAULT ('N'),
[DelegationRoles] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'General configuration settings for the entire PAT application', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'How the account is displayed in Payables Approval and Tracking (account name only, account number only, account name and number).', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'AccountDisplay'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*A semicolon separated list of Vision security roles that define the users that function as ?accounting users?', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'AcctRoles'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PAT application activation password - provided by EleVia', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'ActivationPassword'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*One or more name value pairs of advanced configuration options', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'AdvancedOptions'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The default bank code used by this company. Setting this also implies a default company. The batch load setting will override this. You can leave blank if you don?t want a default.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'BankCodeDefault'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*A semicolon separated list of employee numbers designating who will have access to the Configuration menu', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'ConfigEmployees'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Used to enable PAT?s Consultant Contract capability', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'ContractEnabled'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Enables the control amount feature to force the detail line amounts of an invoice to match an expected total.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'ControlAmount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The default contract filename PAT will use. Placeholders allowed.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'DefaultContractFilename'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Options for if an how the GL account field should be defaulted during data entry', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'DefaultGLAccount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The default invoice / disbursement filename PAT will use. Placeholders allowed.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'DefaultInvoiceFilename'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Default filesystem path when linking files in single entry', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'DefaultSourcePath'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The default transaction filename PAT will use. Placeholders allowed.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'DefaultTransactionFilename'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Choose whether to be able to charge to Regular Projects or All Projects', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'DefPrLookupChargeType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Choose whether to allow charges to be to Projects that are Active only or Active and Inactive Projects', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'DefPrLookupStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Is delegation enabled? Y/N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'Delegation'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Do delegations need to be approved? Y/N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'DelegationApproval'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Can supervisors approve delegation requests for their employees? Y/N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'DelegationBySupervisor'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Is dual access allowed for delegations? Y/N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'DelegationDualAccess'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Custom CSV list of employees who can do approvals for delegations', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'DelegationEmployeesList'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Email setup - HTML code for optional link in email notifications', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'EmailLink'
GO
EXEC sp_addextendedproperty N'MS_Description', N'How employee information is displayed in Payables Approval and Tracking (name only, employee number only, name and employee number)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'EmployeeDisplay'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Can employees request/make delegations for themselves? Y/N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'EmployeeSelfDelegation'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of days an encoded email link is valid', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'EncTimeout'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Option for PAT to enforce the correct windows username when starting up', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'EnforceWindowsUsernameInLink'
GO
EXEC sp_addextendedproperty N'MS_Description', N'If you want to use the Transaction Document Management in Vision, then you have a choice of uploading the invoices from PAT with Markups on the PDF or without the markup. (requires Legacy API set up in ISS - contact EleVia for this set up ) If you are not using TDM in Vision then choose No', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'ExportDocs'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'FilterSQL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'FreezeColumns'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'GLAccountDefault'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Choose whether you want PAT to allow charges to active or all General Ledger Accounts', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'GLAccountStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'If not checked, the only General Ledger accounts accessible to you in PAT will be income statement accounts. If checked you have all General Ledger Accounts available to you.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'GLAccountType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Choose whether you want to hide the project name or what level of the tree structure you might not want to show in PAT', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'GridHidePrName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Activates the grouping feature for the main grid', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'GroupsEnabled'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The full path and name of the log file', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'LogFile'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The log level to use while logging (OFF / ERROR / WARNING / INFO / DEBUG)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'LogLevel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The number of days between re-notifications being sent to approvers/reviewers to remind them they still have invoices to review or approve', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'NotificationInterval'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Is parallel approval enabled? Y/N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'ParallelApproval'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'POMode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Future use - The number of characters to be displayed from the project?s long name', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'ProjectNameLength'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PAT series number', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'Series'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated - moved to roles', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'ShowResetVisible'
GO
EXEC sp_addextendedproperty N'MS_Description', N'If you modify the amount of the voucher in Vision this will update the voucher in PAT if checked', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'ShowVisionAmounts'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'SubgridMode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'SupportDateBillThru'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'SupportMessages'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'SupportPeriodBillThru'
GO
EXEC sp_addextendedproperty N'MS_Description', N'How the vendor is displayed in Payables Approval and Tracking (vendor name only, vendor number only, vendor name and vendor number).', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'VendorDisplay'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PAT version number for the database', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'Version'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Options for how voucher date is used', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'VoucherDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The length of the voucher number to be displayed in the application', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Config', 'COLUMN', N'VoucherNumberLength'
GO
