CREATE TABLE [dbo].[FW_CFGUserLabelsOtherData]
(
[LabelType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TypeID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LabelID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGUserLabelsOtherData] ADD CONSTRAINT [FW_CFGUserLabelsOtherDataPK] PRIMARY KEY NONCLUSTERED ([LabelType], [TypeID], [LabelID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
