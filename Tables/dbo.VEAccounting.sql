CREATE TABLE [dbo].[VEAccounting]
(
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PayTerms] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReimbAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OHAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiscPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__VEAccount__DiscP__41BC8355] DEFAULT ((0)),
[DiscPeriod] [smallint] NOT NULL CONSTRAINT [DF__VEAccount__DiscP__42B0A78E] DEFAULT ((0)),
[DiscCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThisYear1099] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__VEAccount__ThisY__43A4CBC7] DEFAULT ((0)),
[LastYear1099] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__VEAccount__LastY__4498F000] DEFAULT ((0)),
[Req1099] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__VEAccount__Req10__458D1439] DEFAULT ('N'),
[CheckPerVoucher] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__VEAccount__Check__46813872] DEFAULT ('N'),
[AccountNumber] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Memo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MemoPrintOnCheck] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__VEAccount__MemoP__47755CAB] DEFAULT ('N'),
[EFTBankID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EFTAccount] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EFTAccountType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EFTStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EFTAddenda] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__VEAccount__EFTAd__486980E4] DEFAULT ('N'),
[EFTRemittance] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__VEAccount__EFTRe__495DA51D] DEFAULT ('Y'),
[POTemplate] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatchMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__VEAccount__Match__4A51C956] DEFAULT ('3'),
[DefaultTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultExpenseCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EFTEmail] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankAddr1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankAddr2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankAddr3] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankAddr4] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankCity] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankState] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankZip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankCountry] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankIDType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankID] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankAccountIDType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankAccountID] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EFTClieOp] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__VEAccount__EFTCl__4B45ED8F] DEFAULT ('N'),
[EFTTransactionType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SEPAIBAN] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SEPABIC] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PurchasingContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ElectronicPaymentMethodID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_VEAccounting]
      ON [dbo].[VEAccounting]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'VEAccounting'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'Vendor',CONVERT(NVARCHAR(2000),DELETED.[Vendor],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join VE as oldDesc  on DELETED.Vendor = oldDesc.Vendor

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'Company',CONVERT(NVARCHAR(2000),[Company],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'PayTerms',CONVERT(NVARCHAR(2000),[PayTerms],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'RegAccount',CONVERT(NVARCHAR(2000),[RegAccount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'ReimbAccount',CONVERT(NVARCHAR(2000),[ReimbAccount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'OHAccount',CONVERT(NVARCHAR(2000),[OHAccount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'DiscPct',CONVERT(NVARCHAR(2000),[DiscPct],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'DiscPeriod',CONVERT(NVARCHAR(2000),[DiscPeriod],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'DiscCode',CONVERT(NVARCHAR(2000),[DiscCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'ThisYear1099',CONVERT(NVARCHAR(2000),[ThisYear1099],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'LastYear1099',CONVERT(NVARCHAR(2000),[LastYear1099],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'Req1099',CONVERT(NVARCHAR(2000),[Req1099],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'CheckPerVoucher',CONVERT(NVARCHAR(2000),[CheckPerVoucher],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'AccountNumber',CONVERT(NVARCHAR(2000),[AccountNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'Memo','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'MemoPrintOnCheck',CONVERT(NVARCHAR(2000),[MemoPrintOnCheck],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'EFTBankID',CONVERT(NVARCHAR(2000),[EFTBankID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'EFTAccount',CONVERT(NVARCHAR(2000),[EFTAccount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'EFTAccountType',CONVERT(NVARCHAR(2000),[EFTAccountType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'EFTStatus',CONVERT(NVARCHAR(2000),[EFTStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'EFTAddenda',CONVERT(NVARCHAR(2000),[EFTAddenda],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'EFTRemittance',CONVERT(NVARCHAR(2000),[EFTRemittance],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'POTemplate',CONVERT(NVARCHAR(2000),[POTemplate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'MatchMethod',CONVERT(NVARCHAR(2000),[MatchMethod],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'DefaultTaxCode',CONVERT(NVARCHAR(2000),[DefaultTaxCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'DefaultExpenseCode',CONVERT(NVARCHAR(2000),[DefaultExpenseCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'EFTEmail','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'BankName',CONVERT(NVARCHAR(2000),[BankName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'BankAddr1',CONVERT(NVARCHAR(2000),[BankAddr1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'BankAddr2',CONVERT(NVARCHAR(2000),[BankAddr2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'BankAddr3',CONVERT(NVARCHAR(2000),[BankAddr3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'BankAddr4',CONVERT(NVARCHAR(2000),[BankAddr4],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'BankCity',CONVERT(NVARCHAR(2000),[BankCity],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'BankState',CONVERT(NVARCHAR(2000),[BankState],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'BankZip',CONVERT(NVARCHAR(2000),[BankZip],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'BankCountry',CONVERT(NVARCHAR(2000),[BankCountry],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'BankIDType',CONVERT(NVARCHAR(2000),[BankIDType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'BankID',CONVERT(NVARCHAR(2000),[BankID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'BankAccountIDType',CONVERT(NVARCHAR(2000),[BankAccountIDType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'BankAccountID',CONVERT(NVARCHAR(2000),[BankAccountID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'EFTClieOp',CONVERT(NVARCHAR(2000),[EFTClieOp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'EFTTransactionType',CONVERT(NVARCHAR(2000),[EFTTransactionType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'SEPAIBAN',CONVERT(NVARCHAR(2000),[SEPAIBAN],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'SEPABIC',CONVERT(NVARCHAR(2000),[SEPABIC],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'PurchasingContactID',CONVERT(NVARCHAR(2000),[PurchasingContactID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'ElectronicPaymentMethodID',CONVERT(NVARCHAR(2000),[ElectronicPaymentMethodID],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_VEAccounting]
      ON [dbo].[VEAccounting]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'VEAccounting'
    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Vendor',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Vendor],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  VE as newDesc  on INSERTED.Vendor = newDesc.Vendor

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Company',NULL,CONVERT(NVARCHAR(2000),[Company],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'PayTerms',NULL,CONVERT(NVARCHAR(2000),[PayTerms],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'RegAccount',NULL,CONVERT(NVARCHAR(2000),[RegAccount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'ReimbAccount',NULL,CONVERT(NVARCHAR(2000),[ReimbAccount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'OHAccount',NULL,CONVERT(NVARCHAR(2000),[OHAccount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'DiscPct',NULL,CONVERT(NVARCHAR(2000),[DiscPct],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'DiscPeriod',NULL,CONVERT(NVARCHAR(2000),[DiscPeriod],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'DiscCode',NULL,CONVERT(NVARCHAR(2000),[DiscCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'ThisYear1099',NULL,CONVERT(NVARCHAR(2000),[ThisYear1099],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'LastYear1099',NULL,CONVERT(NVARCHAR(2000),[LastYear1099],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Req1099',NULL,CONVERT(NVARCHAR(2000),[Req1099],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'CheckPerVoucher',NULL,CONVERT(NVARCHAR(2000),[CheckPerVoucher],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'AccountNumber',NULL,CONVERT(NVARCHAR(2000),[AccountNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Memo',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'MemoPrintOnCheck',NULL,CONVERT(NVARCHAR(2000),[MemoPrintOnCheck],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTBankID',NULL,CONVERT(NVARCHAR(2000),[EFTBankID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTAccount',NULL,CONVERT(NVARCHAR(2000),[EFTAccount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTAccountType',NULL,CONVERT(NVARCHAR(2000),[EFTAccountType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTStatus',NULL,CONVERT(NVARCHAR(2000),[EFTStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTAddenda',NULL,CONVERT(NVARCHAR(2000),[EFTAddenda],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTRemittance',NULL,CONVERT(NVARCHAR(2000),[EFTRemittance],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'POTemplate',NULL,CONVERT(NVARCHAR(2000),[POTemplate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'MatchMethod',NULL,CONVERT(NVARCHAR(2000),[MatchMethod],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'DefaultTaxCode',NULL,CONVERT(NVARCHAR(2000),[DefaultTaxCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'DefaultExpenseCode',NULL,CONVERT(NVARCHAR(2000),[DefaultExpenseCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTEmail',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankName',NULL,CONVERT(NVARCHAR(2000),[BankName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankAddr1',NULL,CONVERT(NVARCHAR(2000),[BankAddr1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankAddr2',NULL,CONVERT(NVARCHAR(2000),[BankAddr2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankAddr3',NULL,CONVERT(NVARCHAR(2000),[BankAddr3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankAddr4',NULL,CONVERT(NVARCHAR(2000),[BankAddr4],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankCity',NULL,CONVERT(NVARCHAR(2000),[BankCity],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankState',NULL,CONVERT(NVARCHAR(2000),[BankState],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankZip',NULL,CONVERT(NVARCHAR(2000),[BankZip],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankCountry',NULL,CONVERT(NVARCHAR(2000),[BankCountry],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankIDType',NULL,CONVERT(NVARCHAR(2000),[BankIDType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankID',NULL,CONVERT(NVARCHAR(2000),[BankID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankAccountIDType',NULL,CONVERT(NVARCHAR(2000),[BankAccountIDType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankAccountID',NULL,CONVERT(NVARCHAR(2000),[BankAccountID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTClieOp',NULL,CONVERT(NVARCHAR(2000),[EFTClieOp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTTransactionType',NULL,CONVERT(NVARCHAR(2000),[EFTTransactionType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'SEPAIBAN',NULL,CONVERT(NVARCHAR(2000),[SEPAIBAN],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'SEPABIC',NULL,CONVERT(NVARCHAR(2000),[SEPABIC],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'PurchasingContactID',NULL,CONVERT(NVARCHAR(2000),[PurchasingContactID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'ElectronicPaymentMethodID',NULL,CONVERT(NVARCHAR(2000),[ElectronicPaymentMethodID],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_VEAccounting]
      ON [dbo].[VEAccounting]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'VEAccounting'
    
     If UPDATE([Vendor])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Vendor',
     CONVERT(NVARCHAR(2000),DELETED.[Vendor],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Vendor],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[Vendor] Is Null And
				DELETED.[Vendor] Is Not Null
			) Or
			(
				INSERTED.[Vendor] Is Not Null And
				DELETED.[Vendor] Is Null
			) Or
			(
				INSERTED.[Vendor] !=
				DELETED.[Vendor]
			)
		) left join VE as oldDesc  on DELETED.Vendor = oldDesc.Vendor  left join  VE as newDesc  on INSERTED.Vendor = newDesc.Vendor
		END		
		
      If UPDATE([Company])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Company',
      CONVERT(NVARCHAR(2000),DELETED.[Company],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Company],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[Company] Is Null And
				DELETED.[Company] Is Not Null
			) Or
			(
				INSERTED.[Company] Is Not Null And
				DELETED.[Company] Is Null
			) Or
			(
				INSERTED.[Company] !=
				DELETED.[Company]
			)
		) 
		END		
		
      If UPDATE([PayTerms])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'PayTerms',
      CONVERT(NVARCHAR(2000),DELETED.[PayTerms],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PayTerms],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[PayTerms] Is Null And
				DELETED.[PayTerms] Is Not Null
			) Or
			(
				INSERTED.[PayTerms] Is Not Null And
				DELETED.[PayTerms] Is Null
			) Or
			(
				INSERTED.[PayTerms] !=
				DELETED.[PayTerms]
			)
		) 
		END		
		
      If UPDATE([RegAccount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'RegAccount',
      CONVERT(NVARCHAR(2000),DELETED.[RegAccount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RegAccount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[RegAccount] Is Null And
				DELETED.[RegAccount] Is Not Null
			) Or
			(
				INSERTED.[RegAccount] Is Not Null And
				DELETED.[RegAccount] Is Null
			) Or
			(
				INSERTED.[RegAccount] !=
				DELETED.[RegAccount]
			)
		) 
		END		
		
      If UPDATE([ReimbAccount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'ReimbAccount',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAccount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAccount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[ReimbAccount] Is Null And
				DELETED.[ReimbAccount] Is Not Null
			) Or
			(
				INSERTED.[ReimbAccount] Is Not Null And
				DELETED.[ReimbAccount] Is Null
			) Or
			(
				INSERTED.[ReimbAccount] !=
				DELETED.[ReimbAccount]
			)
		) 
		END		
		
      If UPDATE([OHAccount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'OHAccount',
      CONVERT(NVARCHAR(2000),DELETED.[OHAccount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OHAccount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[OHAccount] Is Null And
				DELETED.[OHAccount] Is Not Null
			) Or
			(
				INSERTED.[OHAccount] Is Not Null And
				DELETED.[OHAccount] Is Null
			) Or
			(
				INSERTED.[OHAccount] !=
				DELETED.[OHAccount]
			)
		) 
		END		
		
      If UPDATE([DiscPct])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'DiscPct',
      CONVERT(NVARCHAR(2000),DELETED.[DiscPct],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DiscPct],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[DiscPct] Is Null And
				DELETED.[DiscPct] Is Not Null
			) Or
			(
				INSERTED.[DiscPct] Is Not Null And
				DELETED.[DiscPct] Is Null
			) Or
			(
				INSERTED.[DiscPct] !=
				DELETED.[DiscPct]
			)
		) 
		END		
		
      If UPDATE([DiscPeriod])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'DiscPeriod',
      CONVERT(NVARCHAR(2000),DELETED.[DiscPeriod],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DiscPeriod],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[DiscPeriod] Is Null And
				DELETED.[DiscPeriod] Is Not Null
			) Or
			(
				INSERTED.[DiscPeriod] Is Not Null And
				DELETED.[DiscPeriod] Is Null
			) Or
			(
				INSERTED.[DiscPeriod] !=
				DELETED.[DiscPeriod]
			)
		) 
		END		
		
      If UPDATE([DiscCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'DiscCode',
      CONVERT(NVARCHAR(2000),DELETED.[DiscCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DiscCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[DiscCode] Is Null And
				DELETED.[DiscCode] Is Not Null
			) Or
			(
				INSERTED.[DiscCode] Is Not Null And
				DELETED.[DiscCode] Is Null
			) Or
			(
				INSERTED.[DiscCode] !=
				DELETED.[DiscCode]
			)
		) 
		END		
		
      If UPDATE([ThisYear1099])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'ThisYear1099',
      CONVERT(NVARCHAR(2000),DELETED.[ThisYear1099],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ThisYear1099],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[ThisYear1099] Is Null And
				DELETED.[ThisYear1099] Is Not Null
			) Or
			(
				INSERTED.[ThisYear1099] Is Not Null And
				DELETED.[ThisYear1099] Is Null
			) Or
			(
				INSERTED.[ThisYear1099] !=
				DELETED.[ThisYear1099]
			)
		) 
		END		
		
      If UPDATE([LastYear1099])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'LastYear1099',
      CONVERT(NVARCHAR(2000),DELETED.[LastYear1099],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LastYear1099],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[LastYear1099] Is Null And
				DELETED.[LastYear1099] Is Not Null
			) Or
			(
				INSERTED.[LastYear1099] Is Not Null And
				DELETED.[LastYear1099] Is Null
			) Or
			(
				INSERTED.[LastYear1099] !=
				DELETED.[LastYear1099]
			)
		) 
		END		
		
      If UPDATE([Req1099])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Req1099',
      CONVERT(NVARCHAR(2000),DELETED.[Req1099],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Req1099],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[Req1099] Is Null And
				DELETED.[Req1099] Is Not Null
			) Or
			(
				INSERTED.[Req1099] Is Not Null And
				DELETED.[Req1099] Is Null
			) Or
			(
				INSERTED.[Req1099] !=
				DELETED.[Req1099]
			)
		) 
		END		
		
      If UPDATE([CheckPerVoucher])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'CheckPerVoucher',
      CONVERT(NVARCHAR(2000),DELETED.[CheckPerVoucher],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CheckPerVoucher],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[CheckPerVoucher] Is Null And
				DELETED.[CheckPerVoucher] Is Not Null
			) Or
			(
				INSERTED.[CheckPerVoucher] Is Not Null And
				DELETED.[CheckPerVoucher] Is Null
			) Or
			(
				INSERTED.[CheckPerVoucher] !=
				DELETED.[CheckPerVoucher]
			)
		) 
		END		
		
      If UPDATE([AccountNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'AccountNumber',
      CONVERT(NVARCHAR(2000),DELETED.[AccountNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccountNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[AccountNumber] Is Null And
				DELETED.[AccountNumber] Is Not Null
			) Or
			(
				INSERTED.[AccountNumber] Is Not Null And
				DELETED.[AccountNumber] Is Null
			) Or
			(
				INSERTED.[AccountNumber] !=
				DELETED.[AccountNumber]
			)
		) 
		END		
		
      If UPDATE([Memo])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Memo',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([MemoPrintOnCheck])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'MemoPrintOnCheck',
      CONVERT(NVARCHAR(2000),DELETED.[MemoPrintOnCheck],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MemoPrintOnCheck],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[MemoPrintOnCheck] Is Null And
				DELETED.[MemoPrintOnCheck] Is Not Null
			) Or
			(
				INSERTED.[MemoPrintOnCheck] Is Not Null And
				DELETED.[MemoPrintOnCheck] Is Null
			) Or
			(
				INSERTED.[MemoPrintOnCheck] !=
				DELETED.[MemoPrintOnCheck]
			)
		) 
		END		
		
      If UPDATE([EFTBankID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTBankID',
      CONVERT(NVARCHAR(2000),DELETED.[EFTBankID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EFTBankID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[EFTBankID] Is Null And
				DELETED.[EFTBankID] Is Not Null
			) Or
			(
				INSERTED.[EFTBankID] Is Not Null And
				DELETED.[EFTBankID] Is Null
			) Or
			(
				INSERTED.[EFTBankID] !=
				DELETED.[EFTBankID]
			)
		) 
		END		
		
      If UPDATE([EFTAccount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTAccount',
      CONVERT(NVARCHAR(2000),DELETED.[EFTAccount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EFTAccount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[EFTAccount] Is Null And
				DELETED.[EFTAccount] Is Not Null
			) Or
			(
				INSERTED.[EFTAccount] Is Not Null And
				DELETED.[EFTAccount] Is Null
			) Or
			(
				INSERTED.[EFTAccount] !=
				DELETED.[EFTAccount]
			)
		) 
		END		
		
      If UPDATE([EFTAccountType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTAccountType',
      CONVERT(NVARCHAR(2000),DELETED.[EFTAccountType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EFTAccountType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[EFTAccountType] Is Null And
				DELETED.[EFTAccountType] Is Not Null
			) Or
			(
				INSERTED.[EFTAccountType] Is Not Null And
				DELETED.[EFTAccountType] Is Null
			) Or
			(
				INSERTED.[EFTAccountType] !=
				DELETED.[EFTAccountType]
			)
		) 
		END		
		
      If UPDATE([EFTStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTStatus',
      CONVERT(NVARCHAR(2000),DELETED.[EFTStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EFTStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[EFTStatus] Is Null And
				DELETED.[EFTStatus] Is Not Null
			) Or
			(
				INSERTED.[EFTStatus] Is Not Null And
				DELETED.[EFTStatus] Is Null
			) Or
			(
				INSERTED.[EFTStatus] !=
				DELETED.[EFTStatus]
			)
		) 
		END		
		
      If UPDATE([EFTAddenda])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTAddenda',
      CONVERT(NVARCHAR(2000),DELETED.[EFTAddenda],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EFTAddenda],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[EFTAddenda] Is Null And
				DELETED.[EFTAddenda] Is Not Null
			) Or
			(
				INSERTED.[EFTAddenda] Is Not Null And
				DELETED.[EFTAddenda] Is Null
			) Or
			(
				INSERTED.[EFTAddenda] !=
				DELETED.[EFTAddenda]
			)
		) 
		END		
		
      If UPDATE([EFTRemittance])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTRemittance',
      CONVERT(NVARCHAR(2000),DELETED.[EFTRemittance],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EFTRemittance],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[EFTRemittance] Is Null And
				DELETED.[EFTRemittance] Is Not Null
			) Or
			(
				INSERTED.[EFTRemittance] Is Not Null And
				DELETED.[EFTRemittance] Is Null
			) Or
			(
				INSERTED.[EFTRemittance] !=
				DELETED.[EFTRemittance]
			)
		) 
		END		
		
      If UPDATE([POTemplate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'POTemplate',
      CONVERT(NVARCHAR(2000),DELETED.[POTemplate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[POTemplate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[POTemplate] Is Null And
				DELETED.[POTemplate] Is Not Null
			) Or
			(
				INSERTED.[POTemplate] Is Not Null And
				DELETED.[POTemplate] Is Null
			) Or
			(
				INSERTED.[POTemplate] !=
				DELETED.[POTemplate]
			)
		) 
		END		
		
      If UPDATE([MatchMethod])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'MatchMethod',
      CONVERT(NVARCHAR(2000),DELETED.[MatchMethod],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MatchMethod],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[MatchMethod] Is Null And
				DELETED.[MatchMethod] Is Not Null
			) Or
			(
				INSERTED.[MatchMethod] Is Not Null And
				DELETED.[MatchMethod] Is Null
			) Or
			(
				INSERTED.[MatchMethod] !=
				DELETED.[MatchMethod]
			)
		) 
		END		
		
      If UPDATE([DefaultTaxCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'DefaultTaxCode',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultTaxCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultTaxCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[DefaultTaxCode] Is Null And
				DELETED.[DefaultTaxCode] Is Not Null
			) Or
			(
				INSERTED.[DefaultTaxCode] Is Not Null And
				DELETED.[DefaultTaxCode] Is Null
			) Or
			(
				INSERTED.[DefaultTaxCode] !=
				DELETED.[DefaultTaxCode]
			)
		) 
		END		
		
      If UPDATE([DefaultExpenseCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'DefaultExpenseCode',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultExpenseCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultExpenseCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[DefaultExpenseCode] Is Null And
				DELETED.[DefaultExpenseCode] Is Not Null
			) Or
			(
				INSERTED.[DefaultExpenseCode] Is Not Null And
				DELETED.[DefaultExpenseCode] Is Null
			) Or
			(
				INSERTED.[DefaultExpenseCode] !=
				DELETED.[DefaultExpenseCode]
			)
		) 
		END		
		
      If UPDATE([EFTEmail])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTEmail',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([BankName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankName',
      CONVERT(NVARCHAR(2000),DELETED.[BankName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BankName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[BankName] Is Null And
				DELETED.[BankName] Is Not Null
			) Or
			(
				INSERTED.[BankName] Is Not Null And
				DELETED.[BankName] Is Null
			) Or
			(
				INSERTED.[BankName] !=
				DELETED.[BankName]
			)
		) 
		END		
		
      If UPDATE([BankAddr1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankAddr1',
      CONVERT(NVARCHAR(2000),DELETED.[BankAddr1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BankAddr1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[BankAddr1] Is Null And
				DELETED.[BankAddr1] Is Not Null
			) Or
			(
				INSERTED.[BankAddr1] Is Not Null And
				DELETED.[BankAddr1] Is Null
			) Or
			(
				INSERTED.[BankAddr1] !=
				DELETED.[BankAddr1]
			)
		) 
		END		
		
      If UPDATE([BankAddr2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankAddr2',
      CONVERT(NVARCHAR(2000),DELETED.[BankAddr2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BankAddr2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[BankAddr2] Is Null And
				DELETED.[BankAddr2] Is Not Null
			) Or
			(
				INSERTED.[BankAddr2] Is Not Null And
				DELETED.[BankAddr2] Is Null
			) Or
			(
				INSERTED.[BankAddr2] !=
				DELETED.[BankAddr2]
			)
		) 
		END		
		
      If UPDATE([BankAddr3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankAddr3',
      CONVERT(NVARCHAR(2000),DELETED.[BankAddr3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BankAddr3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[BankAddr3] Is Null And
				DELETED.[BankAddr3] Is Not Null
			) Or
			(
				INSERTED.[BankAddr3] Is Not Null And
				DELETED.[BankAddr3] Is Null
			) Or
			(
				INSERTED.[BankAddr3] !=
				DELETED.[BankAddr3]
			)
		) 
		END		
		
      If UPDATE([BankAddr4])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankAddr4',
      CONVERT(NVARCHAR(2000),DELETED.[BankAddr4],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BankAddr4],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[BankAddr4] Is Null And
				DELETED.[BankAddr4] Is Not Null
			) Or
			(
				INSERTED.[BankAddr4] Is Not Null And
				DELETED.[BankAddr4] Is Null
			) Or
			(
				INSERTED.[BankAddr4] !=
				DELETED.[BankAddr4]
			)
		) 
		END		
		
      If UPDATE([BankCity])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankCity',
      CONVERT(NVARCHAR(2000),DELETED.[BankCity],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BankCity],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[BankCity] Is Null And
				DELETED.[BankCity] Is Not Null
			) Or
			(
				INSERTED.[BankCity] Is Not Null And
				DELETED.[BankCity] Is Null
			) Or
			(
				INSERTED.[BankCity] !=
				DELETED.[BankCity]
			)
		) 
		END		
		
      If UPDATE([BankState])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankState',
      CONVERT(NVARCHAR(2000),DELETED.[BankState],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BankState],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[BankState] Is Null And
				DELETED.[BankState] Is Not Null
			) Or
			(
				INSERTED.[BankState] Is Not Null And
				DELETED.[BankState] Is Null
			) Or
			(
				INSERTED.[BankState] !=
				DELETED.[BankState]
			)
		) 
		END		
		
      If UPDATE([BankZip])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankZip',
      CONVERT(NVARCHAR(2000),DELETED.[BankZip],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BankZip],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[BankZip] Is Null And
				DELETED.[BankZip] Is Not Null
			) Or
			(
				INSERTED.[BankZip] Is Not Null And
				DELETED.[BankZip] Is Null
			) Or
			(
				INSERTED.[BankZip] !=
				DELETED.[BankZip]
			)
		) 
		END		
		
      If UPDATE([BankCountry])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankCountry',
      CONVERT(NVARCHAR(2000),DELETED.[BankCountry],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BankCountry],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[BankCountry] Is Null And
				DELETED.[BankCountry] Is Not Null
			) Or
			(
				INSERTED.[BankCountry] Is Not Null And
				DELETED.[BankCountry] Is Null
			) Or
			(
				INSERTED.[BankCountry] !=
				DELETED.[BankCountry]
			)
		) 
		END		
		
      If UPDATE([BankIDType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankIDType',
      CONVERT(NVARCHAR(2000),DELETED.[BankIDType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BankIDType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[BankIDType] Is Null And
				DELETED.[BankIDType] Is Not Null
			) Or
			(
				INSERTED.[BankIDType] Is Not Null And
				DELETED.[BankIDType] Is Null
			) Or
			(
				INSERTED.[BankIDType] !=
				DELETED.[BankIDType]
			)
		) 
		END		
		
      If UPDATE([BankID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankID',
      CONVERT(NVARCHAR(2000),DELETED.[BankID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BankID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[BankID] Is Null And
				DELETED.[BankID] Is Not Null
			) Or
			(
				INSERTED.[BankID] Is Not Null And
				DELETED.[BankID] Is Null
			) Or
			(
				INSERTED.[BankID] !=
				DELETED.[BankID]
			)
		) 
		END		
		
      If UPDATE([BankAccountIDType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankAccountIDType',
      CONVERT(NVARCHAR(2000),DELETED.[BankAccountIDType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BankAccountIDType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[BankAccountIDType] Is Null And
				DELETED.[BankAccountIDType] Is Not Null
			) Or
			(
				INSERTED.[BankAccountIDType] Is Not Null And
				DELETED.[BankAccountIDType] Is Null
			) Or
			(
				INSERTED.[BankAccountIDType] !=
				DELETED.[BankAccountIDType]
			)
		) 
		END		
		
      If UPDATE([BankAccountID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'BankAccountID',
      CONVERT(NVARCHAR(2000),DELETED.[BankAccountID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BankAccountID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[BankAccountID] Is Null And
				DELETED.[BankAccountID] Is Not Null
			) Or
			(
				INSERTED.[BankAccountID] Is Not Null And
				DELETED.[BankAccountID] Is Null
			) Or
			(
				INSERTED.[BankAccountID] !=
				DELETED.[BankAccountID]
			)
		) 
		END		
		
      If UPDATE([EFTClieOp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTClieOp',
      CONVERT(NVARCHAR(2000),DELETED.[EFTClieOp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EFTClieOp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[EFTClieOp] Is Null And
				DELETED.[EFTClieOp] Is Not Null
			) Or
			(
				INSERTED.[EFTClieOp] Is Not Null And
				DELETED.[EFTClieOp] Is Null
			) Or
			(
				INSERTED.[EFTClieOp] !=
				DELETED.[EFTClieOp]
			)
		) 
		END		
		
      If UPDATE([EFTTransactionType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'EFTTransactionType',
      CONVERT(NVARCHAR(2000),DELETED.[EFTTransactionType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EFTTransactionType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[EFTTransactionType] Is Null And
				DELETED.[EFTTransactionType] Is Not Null
			) Or
			(
				INSERTED.[EFTTransactionType] Is Not Null And
				DELETED.[EFTTransactionType] Is Null
			) Or
			(
				INSERTED.[EFTTransactionType] !=
				DELETED.[EFTTransactionType]
			)
		) 
		END		
		
      If UPDATE([SEPAIBAN])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'SEPAIBAN',
      CONVERT(NVARCHAR(2000),DELETED.[SEPAIBAN],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SEPAIBAN],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[SEPAIBAN] Is Null And
				DELETED.[SEPAIBAN] Is Not Null
			) Or
			(
				INSERTED.[SEPAIBAN] Is Not Null And
				DELETED.[SEPAIBAN] Is Null
			) Or
			(
				INSERTED.[SEPAIBAN] !=
				DELETED.[SEPAIBAN]
			)
		) 
		END		
		
      If UPDATE([SEPABIC])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'SEPABIC',
      CONVERT(NVARCHAR(2000),DELETED.[SEPABIC],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SEPABIC],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[SEPABIC] Is Null And
				DELETED.[SEPABIC] Is Not Null
			) Or
			(
				INSERTED.[SEPABIC] Is Not Null And
				DELETED.[SEPABIC] Is Null
			) Or
			(
				INSERTED.[SEPABIC] !=
				DELETED.[SEPABIC]
			)
		) 
		END		
		
      If UPDATE([PurchasingContactID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'PurchasingContactID',
      CONVERT(NVARCHAR(2000),DELETED.[PurchasingContactID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PurchasingContactID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[PurchasingContactID] Is Null And
				DELETED.[PurchasingContactID] Is Not Null
			) Or
			(
				INSERTED.[PurchasingContactID] Is Not Null And
				DELETED.[PurchasingContactID] Is Null
			) Or
			(
				INSERTED.[PurchasingContactID] !=
				DELETED.[PurchasingContactID]
			)
		) 
		END		
		
      If UPDATE([ElectronicPaymentMethodID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Vendor],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'ElectronicPaymentMethodID',
      CONVERT(NVARCHAR(2000),DELETED.[ElectronicPaymentMethodID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ElectronicPaymentMethodID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Vendor] = DELETED.[Vendor] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[ElectronicPaymentMethodID] Is Null And
				DELETED.[ElectronicPaymentMethodID] Is Not Null
			) Or
			(
				INSERTED.[ElectronicPaymentMethodID] Is Not Null And
				DELETED.[ElectronicPaymentMethodID] Is Null
			) Or
			(
				INSERTED.[ElectronicPaymentMethodID] !=
				DELETED.[ElectronicPaymentMethodID]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[VEAccounting] ADD CONSTRAINT [VEAccountingPK] PRIMARY KEY NONCLUSTERED ([Vendor], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
