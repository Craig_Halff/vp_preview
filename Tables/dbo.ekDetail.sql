CREATE TABLE [dbo].[ekDetail]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportDate] [datetime] NOT NULL,
[ReportName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__ekDetail_Ne__Seq__45CB74AD] DEFAULT ((0)),
[SortOrder] [int] NOT NULL CONSTRAINT [DF__ekDetail___SortO__46BF98E6] DEFAULT ((0)),
[TransDate] [datetime] NULL,
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Category] [int] NOT NULL CONSTRAINT [DF__ekDetail___Categ__47B3BD1F] DEFAULT ((0)),
[Billable] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ekDetail___Billa__48A7E158] DEFAULT ('N'),
[CompanyPaid] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ekDetail___Compa__499C0591] DEFAULT ('N'),
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ekDetail___Amoun__4A9029CA] DEFAULT ((0)),
[Miles] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ekDetail___Miles__4B844E03] DEFAULT ((0)),
[AmountPerMile] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ekDetail___Amoun__4C78723C] DEFAULT ((0)),
[Reason] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Other] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EditDetail] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ekDetail___EditD__4D6C9675] DEFAULT ('Y'),
[EKGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LineItemApprovedBy] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NetAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ekDetail___NetAm__4E60BAAE] DEFAULT ((0)),
[PaymentExchangeRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__ekDetail___Payme__4F54DEE7] DEFAULT ((0)),
[PaymentAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ekDetail___Payme__50490320] DEFAULT ((0)),
[PaymentExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__ekDetail___Curre__513D2759] DEFAULT ((0)),
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LineItemApprovalStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LineItemApprover] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginatingVendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardPrimaryCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxLocation] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ekDetail__Master__0202BC4A] DEFAULT (left(replace(newid(),'-',''),(32))),
[DetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ekDetail__Detail__02F6E083] DEFAULT (left(replace(newid(),'-',''),(32))),
[MerchantDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ekDetail] ADD CONSTRAINT [ekDetailPK] PRIMARY KEY NONCLUSTERED ([Employee], [ReportDate], [ReportName], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [EKDetailCCardPKeySortOrderDescrIDX] ON [dbo].[ekDetail] ([CreditCardPKey], [SortOrder], [Description]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [EKDetailPKeyIDX] ON [dbo].[ekDetail] ([MasterPKey], [DetailPKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
