CREATE TABLE [dbo].[CFGAccrualScheduleDetail]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ScheduleID] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL,
[StartMonth] [int] NOT NULL CONSTRAINT [DF__CFGAccrua__Start__6519377D] DEFAULT ((0)),
[EndMonth] [int] NOT NULL CONSTRAINT [DF__CFGAccrua__EndMo__660D5BB6] DEFAULT ((0)),
[HoursPerYear] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGAccrua__Hours__67017FEF] DEFAULT ((0)),
[HoursPerHourWorked] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGAccrua__Hours__67F5A428] DEFAULT ((0)),
[MaxHoursPerProcess] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGAccrua__MaxHo__68E9C861] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAccrualScheduleDetail] ADD CONSTRAINT [CFGAccrualScheduleDetailPK] PRIMARY KEY CLUSTERED ([Company], [ScheduleID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
