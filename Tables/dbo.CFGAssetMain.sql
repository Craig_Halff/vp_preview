CREATE TABLE [dbo].[CFGAssetMain]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CapLimit] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGAssetM__CapLi__1F45D912] DEFAULT ((0)),
[AssetPeriod] [int] NOT NULL CONSTRAINT [DF__CFGAssetM__Asset__2039FD4B] DEFAULT ((0)),
[CreateAssetFrom] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAssetM__Creat__212E2184] DEFAULT ('AP'),
[AssetSourcePO] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAssetM__Asset__222245BD] DEFAULT ('N'),
[AssetSourceAP] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAssetM__Asset__231669F6] DEFAULT ('N'),
[AssetSourceJE] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAssetM__Asset__240A8E2F] DEFAULT ('N'),
[FAAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FAWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FAWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FAWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAssetMain] ADD CONSTRAINT [CFGAssetMainPK] PRIMARY KEY CLUSTERED ([Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
