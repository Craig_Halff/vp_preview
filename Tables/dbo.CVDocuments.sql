CREATE TABLE [dbo].[CVDocuments]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL,
[AssociateNew] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CVDocumen__Assoc__7B686B4F] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__CVDocuments__Seq__7C5C8F88] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CVDocuments] ADD CONSTRAINT [cvDocumentsPK] PRIMARY KEY NONCLUSTERED ([Batch], [MasterPKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
