CREATE TABLE [dbo].[CustomColumnMappings]
(
[MappingID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OICTable] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OICColumn] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PICTable] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PICColumn] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlanTable] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlanColumn] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomColumnMappings] ADD CONSTRAINT [CustomColumnMappingsPK] PRIMARY KEY CLUSTERED ([MappingID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
