CREATE TABLE [dbo].[CFGFABookDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGFABookDescriptions] ADD CONSTRAINT [CFGFABookDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [Company], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
