CREATE TABLE [dbo].[CFGCreditCardImport]
(
[PrimaryCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Delimiter] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Qualifier] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DecimalSymbol] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateOrder] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateDelimiter] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadingZeros] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGCredit__Leadi__0BC8F04A] DEFAULT ('N'),
[FourDigit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGCredit__FourD__0CBD1483] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCreditCardImport] ADD CONSTRAINT [CFGCreditCardImportPK] PRIMARY KEY CLUSTERED ([PrimaryCode], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
