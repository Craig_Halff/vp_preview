CREATE TABLE [dbo].[EmailTemplates]
(
[TemplateID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NavID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EmailTemp__Usern__05B0EF98] DEFAULT (' '),
[Name] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UseAsDefault] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EmailTemp__UseAs__06A513D1] DEFAULT ('N'),
[Record] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecordWhere] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToListEmails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToListUsers] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToListRoles] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToListSpecial] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToListSpecialDisplay] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCListEmails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCListUsers] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCListRoles] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCListSpecial] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCListSpecialDisplay] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BCCListEmails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BCCListUsers] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BCCListRoles] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BCCListSpecial] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BCCListSpecialDisplay] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailSubject] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailContent] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[byRole] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EmailTemp__byRol__0799380A] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[OtherOptions] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EmailTemplates] ADD CONSTRAINT [EmailTemplatesPK] PRIMARY KEY NONCLUSTERED ([TemplateID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [EmailTemplatesNavIDUsernameIDX] ON [dbo].[EmailTemplates] ([NavID], [Username]) ON [PRIMARY]
GO
