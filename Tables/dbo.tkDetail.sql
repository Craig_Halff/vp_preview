CREATE TABLE [dbo].[tkDetail]
(
[EndDate] [datetime] NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__tkDetail_Ne__Seq__2866C5A6] DEFAULT ((0)),
[TransDate] [datetime] NOT NULL,
[Category] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LaborCode] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___RegHr__295AE9DF] DEFAULT ((0)),
[OvtHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___OvtHr__2A4F0E18] DEFAULT ((0)),
[SpecialOvtHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___Speci__2B433251] DEFAULT ((0)),
[TransComment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillCategory] [smallint] NOT NULL CONSTRAINT [DF__tkDetail___BillC__2C37568A] DEFAULT ((0)),
[LineItemApprovedBy] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Locale] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___RegAm__2D2B7AC3] DEFAULT ((0)),
[OvtAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___OvtAm__2E1F9EFC] DEFAULT ((0)),
[SpecialOvtAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___Speci__2F13C335] DEFAULT ((0)),
[ExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegAmtProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___RegAm__3007E76E] DEFAULT ((0)),
[OvtAmtProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___OvtAm__30FC0BA7] DEFAULT ((0)),
[SpecialOvtAmtProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___Speci__31F02FE0] DEFAULT ((0)),
[ProjectExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillExt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___BillE__32E45419] DEFAULT ((0)),
[OvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___OvtPc__33D87852] DEFAULT ((0)),
[SpecialOvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___Speci__34CC9C8B] DEFAULT ((0)),
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail_N__Rate__35C0C0C4] DEFAULT ((0)),
[OvtRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___OvtRa__36B4E4FD] DEFAULT ((0)),
[SpecialOvtRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___Speci__37A90936] DEFAULT ((0)),
[RateProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___RateP__389D2D6F] DEFAULT ((0)),
[OvtRateProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___OvtRa__399151A8] DEFAULT ((0)),
[SpecialOvtRateProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkDetail___Speci__3A8575E1] DEFAULT ((0)),
[StartDateTime] [datetime] NULL,
[EndDateTime] [datetime] NULL,
[LineItemApprovalStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LineItemApprover] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalStartDateTime] [datetime] NULL,
[CalEndDateTime] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tkDetail] ADD CONSTRAINT [tkDetailPK] PRIMARY KEY NONCLUSTERED ([EndDate], [Employee], [EmployeeCompany], [Seq], [TransDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [tkDetailEndDateEmployeeIDX] ON [dbo].[tkDetail] ([EndDate], [Employee]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [tkDetailWBS1IDX] ON [dbo].[tkDetail] ([WBS1]) ON [PRIMARY]
GO
