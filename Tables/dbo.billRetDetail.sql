CREATE TABLE [dbo].[billRetDetail]
(
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecdSeq] [int] NOT NULL CONSTRAINT [DF__billRetDe__RecdS__7623C7EE] DEFAULT ((0)),
[BillWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Prior] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billRetDe__Prior__7717EC27] DEFAULT ((0)),
[Limit] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billRetDe__Limit__780C1060] DEFAULT ((0)),
[LimitHit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__billRetDe__Limit__79003499] DEFAULT ('N'),
[Pct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billRetDeta__Pct__79F458D2] DEFAULT ((0)),
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billRetDe__Amoun__7AE87D0B] DEFAULT ((0)),
[Retainage] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billRetDe__Retai__7BDCA144] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[billRetDetail] ADD CONSTRAINT [billRetDetailPK] PRIMARY KEY NONCLUSTERED ([Invoice], [MainWBS1], [RecdSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
