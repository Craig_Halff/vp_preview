CREATE TABLE [dbo].[GR]
(
[Code] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Category] [smallint] NOT NULL CONSTRAINT [DF__GR_New__Category__238149A1] DEFAULT ((0)),
[LaborCode] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Supervisor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Skill] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GR] ADD CONSTRAINT [GRPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [GRAllIDX] ON [dbo].[GR] ([Code]) INCLUDE ([Name], [Org], [Category], [LaborCode], [Supervisor], [Skill], [Status]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
