CREATE TABLE [dbo].[UDIC_FleetManagement]
(
[UDIC_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLicensePlate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLicensePlateState] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustVIN] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustMake] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustModel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustYear] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustColor] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustResponsibleParty] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustSupervisor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustOrganization] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustUnit] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustActive] [datetime] NULL CONSTRAINT [DF__UDIC_Flee__CustA__03F45B06] DEFAULT (NULL),
[CustInactive] [datetime] NULL,
[CustDisposed] [datetime] NULL,
[CustStartDate] [datetime] NULL CONSTRAINT [DF__UDIC_Flee__CustS__04E87F3F] DEFAULT (NULL),
[CustComputedThruDate] [datetime] NULL CONSTRAINT [DF__UDIC_Flee__CustC__05DCA378] DEFAULT (NULL),
[CustAsset] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustComputed] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_Flee__CustC__07C4EBEA] DEFAULT ((0)),
[CustTollTagNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustWexCardNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEFleetNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustOwnership] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustRegistrationExpirationDate] [datetime] NULL CONSTRAINT [DF__UDIC_Flee__CustR__08B91023] DEFAULT (NULL),
[CustGroup] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustGeomatics] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustStart] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_Flee__CustS__081C53AA] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_UDIC_FleetManagement]
      ON [dbo].[UDIC_FleetManagement]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_FleetManagement'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'UDIC_UID',CONVERT(NVARCHAR(2000),[UDIC_UID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustomCurrencyCode',CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustNumber',CONVERT(NVARCHAR(2000),[CustNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustLicensePlate',CONVERT(NVARCHAR(2000),[CustLicensePlate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustLicensePlateState',CONVERT(NVARCHAR(2000),[CustLicensePlateState],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustVIN',CONVERT(NVARCHAR(2000),[CustVIN],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustMake',CONVERT(NVARCHAR(2000),[CustMake],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustModel',CONVERT(NVARCHAR(2000),[CustModel],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustYear',CONVERT(NVARCHAR(2000),[CustYear],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustColor',CONVERT(NVARCHAR(2000),[CustColor],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustResponsibleParty',CONVERT(NVARCHAR(2000),DELETED.[CustResponsibleParty],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustResponsibleParty = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustSupervisor',CONVERT(NVARCHAR(2000),DELETED.[CustSupervisor],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustSupervisor = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustOrganization',CONVERT(NVARCHAR(2000),DELETED.[CustOrganization],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join Organization as oldDesc  on DELETED.CustOrganization = oldDesc.Org

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustUnit',CONVERT(NVARCHAR(2000),[CustUnit],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustStatus',CONVERT(NVARCHAR(2000),[CustStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustActive',CONVERT(NVARCHAR(2000),[CustActive],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustInactive',CONVERT(NVARCHAR(2000),[CustInactive],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustDisposed',CONVERT(NVARCHAR(2000),[CustDisposed],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustStartDate',CONVERT(NVARCHAR(2000),[CustStartDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustComputedThruDate',CONVERT(NVARCHAR(2000),[CustComputedThruDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustAsset',CONVERT(NVARCHAR(2000),[CustAsset],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustComputed',CONVERT(NVARCHAR(2000),[CustComputed],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustTollTagNumber',CONVERT(NVARCHAR(2000),[CustTollTagNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustWexCardNumber',CONVERT(NVARCHAR(2000),[CustWexCardNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustEFleetNumber',CONVERT(NVARCHAR(2000),[CustEFleetNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustOwnership',CONVERT(NVARCHAR(2000),[CustOwnership],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustRegistrationExpirationDate',CONVERT(NVARCHAR(2000),[CustRegistrationExpirationDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustGroup',CONVERT(NVARCHAR(2000),[CustGroup],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustGeomatics',CONVERT(NVARCHAR(2000),[CustGeomatics],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustStart',CONVERT(NVARCHAR(2000),[CustStart],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_UDIC_FleetManagement]
      ON [dbo].[UDIC_FleetManagement]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_FleetManagement'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'UDIC_UID',NULL,CONVERT(NVARCHAR(2000),[UDIC_UID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustomCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustNumber',NULL,CONVERT(NVARCHAR(2000),[CustNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustLicensePlate',NULL,CONVERT(NVARCHAR(2000),[CustLicensePlate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustLicensePlateState',NULL,CONVERT(NVARCHAR(2000),[CustLicensePlateState],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustVIN',NULL,CONVERT(NVARCHAR(2000),[CustVIN],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustMake',NULL,CONVERT(NVARCHAR(2000),[CustMake],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustModel',NULL,CONVERT(NVARCHAR(2000),[CustModel],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustYear',NULL,CONVERT(NVARCHAR(2000),[CustYear],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustColor',NULL,CONVERT(NVARCHAR(2000),[CustColor],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustResponsibleParty',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustResponsibleParty],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustResponsibleParty = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustSupervisor',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustSupervisor],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustSupervisor = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustOrganization',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustOrganization],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  Organization as newDesc  on INSERTED.CustOrganization = newDesc.Org

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustUnit',NULL,CONVERT(NVARCHAR(2000),[CustUnit],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustStatus',NULL,CONVERT(NVARCHAR(2000),[CustStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustActive',NULL,CONVERT(NVARCHAR(2000),[CustActive],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustInactive',NULL,CONVERT(NVARCHAR(2000),[CustInactive],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustDisposed',NULL,CONVERT(NVARCHAR(2000),[CustDisposed],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustStartDate',NULL,CONVERT(NVARCHAR(2000),[CustStartDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustComputedThruDate',NULL,CONVERT(NVARCHAR(2000),[CustComputedThruDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustAsset',NULL,CONVERT(NVARCHAR(2000),[CustAsset],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustComputed',NULL,CONVERT(NVARCHAR(2000),[CustComputed],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustTollTagNumber',NULL,CONVERT(NVARCHAR(2000),[CustTollTagNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustWexCardNumber',NULL,CONVERT(NVARCHAR(2000),[CustWexCardNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustEFleetNumber',NULL,CONVERT(NVARCHAR(2000),[CustEFleetNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustOwnership',NULL,CONVERT(NVARCHAR(2000),[CustOwnership],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustRegistrationExpirationDate',NULL,CONVERT(NVARCHAR(2000),[CustRegistrationExpirationDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustGroup',NULL,CONVERT(NVARCHAR(2000),[CustGroup],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustGeomatics',NULL,CONVERT(NVARCHAR(2000),[CustGeomatics],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustStart',NULL,CONVERT(NVARCHAR(2000),[CustStart],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_UDIC_FleetManagement]
      ON [dbo].[UDIC_FleetManagement]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_FleetManagement'
    
      If UPDATE([UDIC_UID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'UDIC_UID',
      CONVERT(NVARCHAR(2000),DELETED.[UDIC_UID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UDIC_UID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[UDIC_UID] Is Null And
				DELETED.[UDIC_UID] Is Not Null
			) Or
			(
				INSERTED.[UDIC_UID] Is Not Null And
				DELETED.[UDIC_UID] Is Null
			) Or
			(
				INSERTED.[UDIC_UID] !=
				DELETED.[UDIC_UID]
			)
		) 
		END		
		
      If UPDATE([CustomCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustomCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustomCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustomCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustomCurrencyCode] Is Null And
				DELETED.[CustomCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] Is Not Null And
				DELETED.[CustomCurrencyCode] Is Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] !=
				DELETED.[CustomCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([CustNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustNumber',
      CONVERT(NVARCHAR(2000),DELETED.[CustNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustNumber] Is Null And
				DELETED.[CustNumber] Is Not Null
			) Or
			(
				INSERTED.[CustNumber] Is Not Null And
				DELETED.[CustNumber] Is Null
			) Or
			(
				INSERTED.[CustNumber] !=
				DELETED.[CustNumber]
			)
		) 
		END		
		
      If UPDATE([CustLicensePlate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustLicensePlate',
      CONVERT(NVARCHAR(2000),DELETED.[CustLicensePlate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLicensePlate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustLicensePlate] Is Null And
				DELETED.[CustLicensePlate] Is Not Null
			) Or
			(
				INSERTED.[CustLicensePlate] Is Not Null And
				DELETED.[CustLicensePlate] Is Null
			) Or
			(
				INSERTED.[CustLicensePlate] !=
				DELETED.[CustLicensePlate]
			)
		) 
		END		
		
      If UPDATE([CustLicensePlateState])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustLicensePlateState',
      CONVERT(NVARCHAR(2000),DELETED.[CustLicensePlateState],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLicensePlateState],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustLicensePlateState] Is Null And
				DELETED.[CustLicensePlateState] Is Not Null
			) Or
			(
				INSERTED.[CustLicensePlateState] Is Not Null And
				DELETED.[CustLicensePlateState] Is Null
			) Or
			(
				INSERTED.[CustLicensePlateState] !=
				DELETED.[CustLicensePlateState]
			)
		) 
		END		
		
      If UPDATE([CustVIN])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustVIN',
      CONVERT(NVARCHAR(2000),DELETED.[CustVIN],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustVIN],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustVIN] Is Null And
				DELETED.[CustVIN] Is Not Null
			) Or
			(
				INSERTED.[CustVIN] Is Not Null And
				DELETED.[CustVIN] Is Null
			) Or
			(
				INSERTED.[CustVIN] !=
				DELETED.[CustVIN]
			)
		) 
		END		
		
      If UPDATE([CustMake])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustMake',
      CONVERT(NVARCHAR(2000),DELETED.[CustMake],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustMake],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustMake] Is Null And
				DELETED.[CustMake] Is Not Null
			) Or
			(
				INSERTED.[CustMake] Is Not Null And
				DELETED.[CustMake] Is Null
			) Or
			(
				INSERTED.[CustMake] !=
				DELETED.[CustMake]
			)
		) 
		END		
		
      If UPDATE([CustModel])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustModel',
      CONVERT(NVARCHAR(2000),DELETED.[CustModel],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustModel],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustModel] Is Null And
				DELETED.[CustModel] Is Not Null
			) Or
			(
				INSERTED.[CustModel] Is Not Null And
				DELETED.[CustModel] Is Null
			) Or
			(
				INSERTED.[CustModel] !=
				DELETED.[CustModel]
			)
		) 
		END		
		
      If UPDATE([CustYear])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustYear',
      CONVERT(NVARCHAR(2000),DELETED.[CustYear],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustYear],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustYear] Is Null And
				DELETED.[CustYear] Is Not Null
			) Or
			(
				INSERTED.[CustYear] Is Not Null And
				DELETED.[CustYear] Is Null
			) Or
			(
				INSERTED.[CustYear] !=
				DELETED.[CustYear]
			)
		) 
		END		
		
      If UPDATE([CustColor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustColor',
      CONVERT(NVARCHAR(2000),DELETED.[CustColor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustColor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustColor] Is Null And
				DELETED.[CustColor] Is Not Null
			) Or
			(
				INSERTED.[CustColor] Is Not Null And
				DELETED.[CustColor] Is Null
			) Or
			(
				INSERTED.[CustColor] !=
				DELETED.[CustColor]
			)
		) 
		END		
		
     If UPDATE([CustResponsibleParty])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustResponsibleParty',
     CONVERT(NVARCHAR(2000),DELETED.[CustResponsibleParty],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustResponsibleParty],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustResponsibleParty] Is Null And
				DELETED.[CustResponsibleParty] Is Not Null
			) Or
			(
				INSERTED.[CustResponsibleParty] Is Not Null And
				DELETED.[CustResponsibleParty] Is Null
			) Or
			(
				INSERTED.[CustResponsibleParty] !=
				DELETED.[CustResponsibleParty]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustResponsibleParty = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustResponsibleParty = newDesc.Employee
		END		
		
     If UPDATE([CustSupervisor])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustSupervisor',
     CONVERT(NVARCHAR(2000),DELETED.[CustSupervisor],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustSupervisor],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustSupervisor] Is Null And
				DELETED.[CustSupervisor] Is Not Null
			) Or
			(
				INSERTED.[CustSupervisor] Is Not Null And
				DELETED.[CustSupervisor] Is Null
			) Or
			(
				INSERTED.[CustSupervisor] !=
				DELETED.[CustSupervisor]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustSupervisor = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustSupervisor = newDesc.Employee
		END		
		
     If UPDATE([CustOrganization])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustOrganization',
     CONVERT(NVARCHAR(2000),DELETED.[CustOrganization],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustOrganization],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustOrganization] Is Null And
				DELETED.[CustOrganization] Is Not Null
			) Or
			(
				INSERTED.[CustOrganization] Is Not Null And
				DELETED.[CustOrganization] Is Null
			) Or
			(
				INSERTED.[CustOrganization] !=
				DELETED.[CustOrganization]
			)
		) left join Organization as oldDesc  on DELETED.CustOrganization = oldDesc.Org  left join  Organization as newDesc  on INSERTED.CustOrganization = newDesc.Org
		END		
		
      If UPDATE([CustUnit])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustUnit',
      CONVERT(NVARCHAR(2000),DELETED.[CustUnit],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustUnit],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustUnit] Is Null And
				DELETED.[CustUnit] Is Not Null
			) Or
			(
				INSERTED.[CustUnit] Is Not Null And
				DELETED.[CustUnit] Is Null
			) Or
			(
				INSERTED.[CustUnit] !=
				DELETED.[CustUnit]
			)
		) 
		END		
		
      If UPDATE([CustStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustStatus',
      CONVERT(NVARCHAR(2000),DELETED.[CustStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustStatus] Is Null And
				DELETED.[CustStatus] Is Not Null
			) Or
			(
				INSERTED.[CustStatus] Is Not Null And
				DELETED.[CustStatus] Is Null
			) Or
			(
				INSERTED.[CustStatus] !=
				DELETED.[CustStatus]
			)
		) 
		END		
		
      If UPDATE([CustActive])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustActive',
      CONVERT(NVARCHAR(2000),DELETED.[CustActive],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustActive],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustActive] Is Null And
				DELETED.[CustActive] Is Not Null
			) Or
			(
				INSERTED.[CustActive] Is Not Null And
				DELETED.[CustActive] Is Null
			) Or
			(
				INSERTED.[CustActive] !=
				DELETED.[CustActive]
			)
		) 
		END		
		
      If UPDATE([CustInactive])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustInactive',
      CONVERT(NVARCHAR(2000),DELETED.[CustInactive],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustInactive],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustInactive] Is Null And
				DELETED.[CustInactive] Is Not Null
			) Or
			(
				INSERTED.[CustInactive] Is Not Null And
				DELETED.[CustInactive] Is Null
			) Or
			(
				INSERTED.[CustInactive] !=
				DELETED.[CustInactive]
			)
		) 
		END		
		
      If UPDATE([CustDisposed])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustDisposed',
      CONVERT(NVARCHAR(2000),DELETED.[CustDisposed],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustDisposed],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustDisposed] Is Null And
				DELETED.[CustDisposed] Is Not Null
			) Or
			(
				INSERTED.[CustDisposed] Is Not Null And
				DELETED.[CustDisposed] Is Null
			) Or
			(
				INSERTED.[CustDisposed] !=
				DELETED.[CustDisposed]
			)
		) 
		END		
		
      If UPDATE([CustStartDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustStartDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustStartDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustStartDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustStartDate] Is Null And
				DELETED.[CustStartDate] Is Not Null
			) Or
			(
				INSERTED.[CustStartDate] Is Not Null And
				DELETED.[CustStartDate] Is Null
			) Or
			(
				INSERTED.[CustStartDate] !=
				DELETED.[CustStartDate]
			)
		) 
		END		
		
      If UPDATE([CustComputedThruDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustComputedThruDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustComputedThruDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustComputedThruDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustComputedThruDate] Is Null And
				DELETED.[CustComputedThruDate] Is Not Null
			) Or
			(
				INSERTED.[CustComputedThruDate] Is Not Null And
				DELETED.[CustComputedThruDate] Is Null
			) Or
			(
				INSERTED.[CustComputedThruDate] !=
				DELETED.[CustComputedThruDate]
			)
		) 
		END		
		
      If UPDATE([CustAsset])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustAsset',
      CONVERT(NVARCHAR(2000),DELETED.[CustAsset],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustAsset],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustAsset] Is Null And
				DELETED.[CustAsset] Is Not Null
			) Or
			(
				INSERTED.[CustAsset] Is Not Null And
				DELETED.[CustAsset] Is Null
			) Or
			(
				INSERTED.[CustAsset] !=
				DELETED.[CustAsset]
			)
		) 
		END		
		
      If UPDATE([CustComputed])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustComputed',
      CONVERT(NVARCHAR(2000),DELETED.[CustComputed],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustComputed],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustComputed] Is Null And
				DELETED.[CustComputed] Is Not Null
			) Or
			(
				INSERTED.[CustComputed] Is Not Null And
				DELETED.[CustComputed] Is Null
			) Or
			(
				INSERTED.[CustComputed] !=
				DELETED.[CustComputed]
			)
		) 
		END		
		
      If UPDATE([CustTollTagNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustTollTagNumber',
      CONVERT(NVARCHAR(2000),DELETED.[CustTollTagNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTollTagNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustTollTagNumber] Is Null And
				DELETED.[CustTollTagNumber] Is Not Null
			) Or
			(
				INSERTED.[CustTollTagNumber] Is Not Null And
				DELETED.[CustTollTagNumber] Is Null
			) Or
			(
				INSERTED.[CustTollTagNumber] !=
				DELETED.[CustTollTagNumber]
			)
		) 
		END		
		
      If UPDATE([CustWexCardNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustWexCardNumber',
      CONVERT(NVARCHAR(2000),DELETED.[CustWexCardNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustWexCardNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustWexCardNumber] Is Null And
				DELETED.[CustWexCardNumber] Is Not Null
			) Or
			(
				INSERTED.[CustWexCardNumber] Is Not Null And
				DELETED.[CustWexCardNumber] Is Null
			) Or
			(
				INSERTED.[CustWexCardNumber] !=
				DELETED.[CustWexCardNumber]
			)
		) 
		END		
		
      If UPDATE([CustEFleetNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustEFleetNumber',
      CONVERT(NVARCHAR(2000),DELETED.[CustEFleetNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEFleetNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustEFleetNumber] Is Null And
				DELETED.[CustEFleetNumber] Is Not Null
			) Or
			(
				INSERTED.[CustEFleetNumber] Is Not Null And
				DELETED.[CustEFleetNumber] Is Null
			) Or
			(
				INSERTED.[CustEFleetNumber] !=
				DELETED.[CustEFleetNumber]
			)
		) 
		END		
		
      If UPDATE([CustOwnership])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustOwnership',
      CONVERT(NVARCHAR(2000),DELETED.[CustOwnership],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustOwnership],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustOwnership] Is Null And
				DELETED.[CustOwnership] Is Not Null
			) Or
			(
				INSERTED.[CustOwnership] Is Not Null And
				DELETED.[CustOwnership] Is Null
			) Or
			(
				INSERTED.[CustOwnership] !=
				DELETED.[CustOwnership]
			)
		) 
		END		
		
      If UPDATE([CustRegistrationExpirationDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustRegistrationExpirationDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustRegistrationExpirationDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRegistrationExpirationDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustRegistrationExpirationDate] Is Null And
				DELETED.[CustRegistrationExpirationDate] Is Not Null
			) Or
			(
				INSERTED.[CustRegistrationExpirationDate] Is Not Null And
				DELETED.[CustRegistrationExpirationDate] Is Null
			) Or
			(
				INSERTED.[CustRegistrationExpirationDate] !=
				DELETED.[CustRegistrationExpirationDate]
			)
		) 
		END		
		
      If UPDATE([CustGroup])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustGroup',
      CONVERT(NVARCHAR(2000),DELETED.[CustGroup],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustGroup],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustGroup] Is Null And
				DELETED.[CustGroup] Is Not Null
			) Or
			(
				INSERTED.[CustGroup] Is Not Null And
				DELETED.[CustGroup] Is Null
			) Or
			(
				INSERTED.[CustGroup] !=
				DELETED.[CustGroup]
			)
		) 
		END		
		
      If UPDATE([CustGeomatics])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustGeomatics',
      CONVERT(NVARCHAR(2000),DELETED.[CustGeomatics],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustGeomatics],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustGeomatics] Is Null And
				DELETED.[CustGeomatics] Is Not Null
			) Or
			(
				INSERTED.[CustGeomatics] Is Not Null And
				DELETED.[CustGeomatics] Is Null
			) Or
			(
				INSERTED.[CustGeomatics] !=
				DELETED.[CustGeomatics]
			)
		) 
		END		
		
      If UPDATE([CustStart])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustStart',
      CONVERT(NVARCHAR(2000),DELETED.[CustStart],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustStart],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustStart] Is Null And
				DELETED.[CustStart] Is Not Null
			) Or
			(
				INSERTED.[CustStart] Is Not Null And
				DELETED.[CustStart] Is Null
			) Or
			(
				INSERTED.[CustStart] !=
				DELETED.[CustStart]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[UDIC_FleetManagement] ADD CONSTRAINT [UDIC_FleetManagementPK] PRIMARY KEY CLUSTERED ([UDIC_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
