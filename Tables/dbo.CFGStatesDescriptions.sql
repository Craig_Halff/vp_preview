CREATE TABLE [dbo].[CFGStatesDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ISOCountryCode] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGStatesDe__Seq__749B722F] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGStatesDescriptions] ADD CONSTRAINT [CFGStatesDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [ISOCountryCode], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
