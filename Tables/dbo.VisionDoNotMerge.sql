CREATE TABLE [dbo].[VisionDoNotMerge]
(
[TableName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VisionDoNotMerge] ADD CONSTRAINT [VisionDoNotMergePK] PRIMARY KEY CLUSTERED ([TableName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
