CREATE TABLE [dbo].[FW_CFGUserLabelsOtherDesc]
(
[LabelType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TypeID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LabelID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NewLabel] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Heading1] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Heading2] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HeadingAlt1] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HeadingAlt2] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGUserLabelsOtherDesc] ADD CONSTRAINT [FW_CFGUserLabelsOtherDescPK] PRIMARY KEY NONCLUSTERED ([LabelType], [TypeID], [LabelID], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
