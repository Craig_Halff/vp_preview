CREATE TABLE [dbo].[RPReimbAllowance]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPReimbAl__Perio__09C242F5] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPReimbAl__Perio__0AB6672E] DEFAULT ((0)),
[PeriodExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPReimbAl__Perio__0BAA8B67] DEFAULT ((0)),
[PeriodConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPReimbAl__Perio__0C9EAFA0] DEFAULT ((0)),
[PeriodExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPReimbAl__Perio__0D92D3D9] DEFAULT ((0)),
[PeriodConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPReimbAl__Perio__0E86F812] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPReimbAllowance] ADD CONSTRAINT [RPReimbAllowancePK] PRIMARY KEY NONCLUSTERED ([TimePhaseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPReimbAllowancePTIDX] ON [dbo].[RPReimbAllowance] ([PlanID], [TaskID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPReimbAllowanceAllIDX] ON [dbo].[RPReimbAllowance] ([PlanID], [TimePhaseID], [TaskID], [StartDate], [EndDate], [PeriodCost], [PeriodBill]) ON [PRIMARY]
GO
