CREATE TABLE [dbo].[VEEFTDetail]
(
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__VEEFTDetail__Seq__54CF57C9] DEFAULT ((0)),
[Period] [int] NOT NULL CONSTRAINT [DF__VEEFTDeta__Perio__55C37C02] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__VEEFTDeta__PostS__56B7A03B] DEFAULT ((0)),
[BankID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Addenda] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__VEEFTDeta__Adden__57ABC474] DEFAULT ('N'),
[Remittance] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__VEEFTDeta__Remit__589FE8AD] DEFAULT ('N'),
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__VEEFTDeta__Amoun__59940CE6] DEFAULT ((0)),
[Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__VEEFTDeta__Overr__5A88311F] DEFAULT ('N'),
[SourceBankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckRefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VEEFTDetail] ADD CONSTRAINT [VEEFTDetailPK] PRIMARY KEY NONCLUSTERED ([Vendor], [Seq], [Period], [PostSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [VEEFTDetailVoidCheckIDX] ON [dbo].[VEEFTDetail] ([SourceBankCode], [CheckRefNo]) ON [PRIMARY]
GO
