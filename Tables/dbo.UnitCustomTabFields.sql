CREATE TABLE [dbo].[UnitCustomTabFields]
(
[UnitTable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Unit] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UnitCustomTabFields] ADD CONSTRAINT [UnitCustomTabFieldsPK] PRIMARY KEY NONCLUSTERED ([UnitTable], [Unit]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
