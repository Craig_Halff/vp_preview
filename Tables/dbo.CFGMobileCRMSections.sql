CREATE TABLE [dbo].[CFGMobileCRMSections]
(
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL,
[SectionName] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGMobileCRMSections] ADD CONSTRAINT [CFGMobileCRMSectionsPK] PRIMARY KEY NONCLUSTERED ([InfoCenterArea], [Seq], [SectionName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
