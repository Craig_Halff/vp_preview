CREATE TABLE [dbo].[CFGEKCategoryTaxCodes]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Category] [int] NOT NULL CONSTRAINT [DF__CFGEKCate__Categ__389B96C1] DEFAULT ((0)),
[EKGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGEKCatego__Seq__398FBAFA] DEFAULT ((0)),
[TaxLocation] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEKCateg__PKey__0A98024B] DEFAULT (left(replace(newid(),'-',''),(32)))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGEKCategoryTaxCodes] ADD CONSTRAINT [CFGEKCategoryTaxCodesPK] PRIMARY KEY CLUSTERED ([Company], [Category], [EKGroup], [TaxCode], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
