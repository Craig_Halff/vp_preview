CREATE TABLE [dbo].[IntegrationWSLogStatus]
(
[ProcessID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusDate] [datetime] NULL,
[StatusMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__Integration__Seq__328E8307] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IntegrationWSLogStatus] ADD CONSTRAINT [IntegrationWSLogStatusPK] PRIMARY KEY CLUSTERED ([ProcessID], [StatusID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
