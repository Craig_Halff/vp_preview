CREATE TABLE [dbo].[FW_CFGCurrencyDesc]
(
[Code] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MajorUnitLabelPlural] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MajorUnitLabelSingular] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MinorUnitLabelPlural] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MinorUnitLabelSingular] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGCurrencyDesc] ADD CONSTRAINT [FW_CFGCurrencyDescPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
