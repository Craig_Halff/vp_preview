CREATE TABLE [dbo].[ETMapping]
(
[ETColumnName] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ETTableName] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL,
[VisionColumnName] [nvarchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ETDataType] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Required] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ETMapping__Requi__16FB772B] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ETMapping] ADD CONSTRAINT [ETMappingPK] PRIMARY KEY NONCLUSTERED ([ETColumnName], [ETTableName], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
