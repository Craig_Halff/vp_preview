CREATE TABLE [dbo].[SEUserMyFavs]
(
[ID] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDivID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SEUserMyFavs] ADD CONSTRAINT [SEUserMyFavsPK] PRIMARY KEY NONCLUSTERED ([ID], [PartKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
