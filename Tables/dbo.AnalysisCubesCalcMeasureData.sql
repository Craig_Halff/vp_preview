CREATE TABLE [dbo].[AnalysisCubesCalcMeasureData]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MeasureGroup] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MeasureFolder] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DataType] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasureExpression] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoOptimize] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__AnalysisC__AutoO__37D18FBA] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AnalysisCubesCalcMeasureData] ADD CONSTRAINT [AnalysisCubesCalcMeasureDataPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
