CREATE TABLE [dbo].[apppChecksW]
(
[Period] [int] NOT NULL CONSTRAINT [DF__apppCheck__Perio__23958CE3] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__apppCheck__PostS__2489B11C] DEFAULT ((0)),
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reference] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankAddr1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankAddr2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankAddr3] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankAddr4] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankCity] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankState] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankZip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankCountry] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankIDType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankID] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankAccountIDType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankAccountID] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WireFee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apppCheck__WireF__257DD555] DEFAULT ((0)),
[FeeDebitAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeeDebitWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeeDebitWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeeDebitWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentMemo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apppCheck__Payme__2671F98E] DEFAULT ((0)),
[ExchangeRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__apppCheck__Excha__27661DC7] DEFAULT ((0)),
[BankAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apppCheck__BankA__285A4200] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[apppChecksW] ADD CONSTRAINT [apppChecksWPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [Vendor]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
