CREATE TABLE [dbo].[CFGModuleConfiguration]
(
[ModuleID] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Password] [varchar] (41) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Enabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGModule__Enabl__39B87777] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGModuleConfiguration] ADD CONSTRAINT [CFGModuleConfigurationPK] PRIMARY KEY CLUSTERED ([ModuleID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
