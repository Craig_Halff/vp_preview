CREATE TABLE [dbo].[CCG_PAT_ProjectAmount]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[PKey] [varchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PayableSeq] [int] NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__CCG_PAT_P__Amoun__0D5BCF6D] DEFAULT ((0)),
[GLAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExpenseCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuppressBill] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NetAmount] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__CCG_PAT_P__NetAm__0E4FF3A6] DEFAULT ((0)),
[TaxAmount] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__CCG_PAT_P__TaxAm__0F4417DF] DEFAULT ((0)),
[TaxCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tax2Amount] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__CCG_PAT_P__Tax2A__10383C18] DEFAULT ((0)),
[Tax2Code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginatingVendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_ProjectAmount] ADD CONSTRAINT [PK_CCG_PAT_ProjectAmount] PRIMARY KEY CLUSTERED ([PKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CCG_PAT_ProjectAmount_PayableSeq_IDX] ON [dbo].[CCG_PAT_ProjectAmount] ([PayableSeq]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'List of project amounts in each payable item.  All these fields are exported into the Vision AP Voucher / Disbursement.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Currency amount', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'Amount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Description for this line item entry', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*FK - Vision expense code', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'ExpenseCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*FK - GL Account in Vision used for this amount', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'GLAccount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Net currency amount', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'NetAmount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Populates Vision''s OriginatingVendor field', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'OriginatingVendor'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - Seq number of the payable item to which this project amount belongs', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'PayableSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'PKey'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Depricated auto generated key', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Populates Vision''s SuppressBill field', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'SuppressBill'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Depricated - See Table CCG_PAT_ProjectAmountTax', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'Tax2Amount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Depricated - See Table CCG_PAT_ProjectAmountTax', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'Tax2Code'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Depricated - See Table CCG_PAT_ProjectAmountTax', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'TaxAmount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Depricated - See Table CCG_PAT_ProjectAmountTax', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'TaxCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Project number for this amount', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'WBS1'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*"Phase" (WBS2) number for this amount, if any', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'WBS2'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*"Task" (WBS3) number for this amount, if any', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmount', 'COLUMN', N'WBS3'
GO
