CREATE TABLE [dbo].[CCG_PAT_History]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[ActionTaken] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionDate] [datetime] NOT NULL,
[ActionTakenBy] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DelegateFor] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParallelSort] [int] NULL,
[ActionRecipient] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayableSeq] [int] NULL,
[Stage] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PriorStage] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PriorStageDateSet] [datetime] NULL,
[ApprovedDetail] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_History] ADD CONSTRAINT [PK_CCG_PAT_History] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CCG_PAT_History_PayableSeq_IDX] ON [dbo].[CCG_PAT_History] ([PayableSeq]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Historical tracking of significant PAT activity', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_History', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date when this action occurred', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_History', 'COLUMN', N'ActionDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Employee id of the action recipient/target (if any)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_History', 'COLUMN', N'ActionRecipient'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Short description of type of action that is captured', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_History', 'COLUMN', N'ActionTaken'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Employee id of the action initiator', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_History', 'COLUMN', N'ActionTakenBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Detail from project amount rows at the time of the approval.  Can be used to determine if some changes where made since the approval. (relates only to approval stages)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_History', 'COLUMN', N'ApprovedDetail'
GO
EXEC sp_addextendedproperty N'MS_Description', N'If action user is a delegate for another, this will be the employee id of the delegator', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_History', 'COLUMN', N'DelegateFor'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Additional detail - varies by action type', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_History', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Sort order for parallel routes. NULL for non-parallel routes.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_History', 'COLUMN', N'ParallelSort'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - The associated payable sequence number of this action', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_History', 'COLUMN', N'PayableSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Previous stage (only relates to ''Stage Change'' action)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_History', 'COLUMN', N'PriorStage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date/time when stage was previously changed (only relates to ''Stage Change'' action)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_History', 'COLUMN', N'PriorStageDateSet'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_History', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Current stage of the payable item (only relates to certain actions)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_History', 'COLUMN', N'Stage'
GO
