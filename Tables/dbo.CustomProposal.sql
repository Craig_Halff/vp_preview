CREATE TABLE [dbo].[CustomProposal]
(
[CustomPropID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Number] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProposalManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAdvertised] [datetime] NULL,
[DueDate] [datetime] NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreferredDateFormat] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastSectionNumber] [smallint] NOT NULL CONSTRAINT [DF__CustomPro__LastS__08F77097] DEFAULT ((0)),
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[Status] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittalDate] [datetime] NULL,
[AwardDate] [datetime] NULL,
[Fee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CustomPropo__Fee__7EB14E53] DEFAULT ((0)),
[Type] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExtensionDescription] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExtensionCategory] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExtensionClass] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentProperties] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PDFFileID] [uniqueidentifier] NULL,
[ProposalType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkedSF330] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomProposal] ADD CONSTRAINT [CustomProposalPK] PRIMARY KEY CLUSTERED ([CustomPropID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
