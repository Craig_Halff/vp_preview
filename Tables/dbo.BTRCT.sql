CREATE TABLE [dbo].[BTRCT]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__BTRCT_New__Table__79553A54] DEFAULT ((0)),
[TableName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AvailableForPlanning] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BTRCT_New__Avail__7A495E8D] DEFAULT ('N'),
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterPrincipal] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterProjMgr] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterSupervisor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTRCT] ADD CONSTRAINT [BTRCTPK] PRIMARY KEY CLUSTERED ([TableNo]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
