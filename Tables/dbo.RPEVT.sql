CREATE TABLE [dbo].[RPEVT]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPEVT_New__Perio__7254CDB8] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPEVT] ADD CONSTRAINT [RPEVTPK] PRIMARY KEY NONCLUSTERED ([TimePhaseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPEVTPTSEIDX] ON [dbo].[RPEVT] ([TaskID], [PlanID], [StartDate], [EndDate], [PeriodPct]) ON [PRIMARY]
GO
