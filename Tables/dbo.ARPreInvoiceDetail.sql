CREATE TABLE [dbo].[ARPreInvoiceDetail]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PreInvoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL,
[InvoiceSection] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ARPreInvo__Amoun__5068335A] DEFAULT ((0)),
[PaidAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ARPreInvo__PaidA__515C5793] DEFAULT ((0)),
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2ToPost] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3ToPost] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ARPreInvo__TaxBa__52507BCC] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ARPreInvoiceDetail] ADD CONSTRAINT [ARPreInvoiceDetailPK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [PreInvoice], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
