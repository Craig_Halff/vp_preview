CREATE TABLE [dbo].[tkMaster]
(
[EndDate] [datetime] NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Submitted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__tkMaster___Submi__3C6DBE53] DEFAULT ('N'),
[Selected] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__tkMaster___Selec__3D61E28C] DEFAULT ('N'),
[SubmittedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TKGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SubmittedDate] [datetime] NULL,
[ApprovedDate] [datetime] NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tkMaster] ADD CONSTRAINT [tkMasterPK] PRIMARY KEY NONCLUSTERED ([EndDate], [Employee], [EmployeeCompany]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [tkMasterSubmittedIDX] ON [dbo].[tkMaster] ([Submitted]) INCLUDE ([Employee], [EmployeeCompany], [EndDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
