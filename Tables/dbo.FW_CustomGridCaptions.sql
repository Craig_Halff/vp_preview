CREATE TABLE [dbo].[FW_CustomGridCaptions]
(
[InfocenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Caption] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CustomGridCaptions] ADD CONSTRAINT [FW_CustomGridCaptionsPK] PRIMARY KEY NONCLUSTERED ([InfocenterArea], [GridID], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
