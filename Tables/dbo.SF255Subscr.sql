CREATE TABLE [dbo].[SF255Subscr]
(
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SF255ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF255Subscr] ADD CONSTRAINT [SF255SubscrPK] PRIMARY KEY NONCLUSTERED ([PartKey], [SF255ID], [UserName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
