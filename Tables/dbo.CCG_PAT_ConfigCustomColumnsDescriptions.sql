CREATE TABLE [dbo].[CCG_PAT_ConfigCustomColumnsDescriptions]
(
[Seq] [int] NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GroupName] [varchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_ConfigCustomColumnsDescriptions] ADD CONSTRAINT [CCG_PAT_ConfigCustomColumnsDescriptionsPK] PRIMARY KEY NONCLUSTERED ([Seq], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Contains descriptions for custom columns as used for multi-language companies', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumnsDescriptions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The description of the custom column', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumnsDescriptions', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The header label to use to display the column', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumnsDescriptions', 'COLUMN', N'Label'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumnsDescriptions', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The UI culture code of this label', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumnsDescriptions', 'COLUMN', N'UICultureName'
GO
