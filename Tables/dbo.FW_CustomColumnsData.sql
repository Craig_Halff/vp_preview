CREATE TABLE [dbo].[FW_CustomColumnsData]
(
[ColumnID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataType] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisplayWidth] [smallint] NOT NULL CONSTRAINT [DF__FW_Custom__Displ__128BC7C9] DEFAULT ((0)),
[Lines] [smallint] NOT NULL CONSTRAINT [DF__FW_Custom__Lines__137FEC02] DEFAULT ((0)),
[Sort] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CustomC__Sort__1474103B] DEFAULT ('N'),
[LimitToList] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_Custom__Limit__15683474] DEFAULT ('N'),
[Total] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_Custom__Total__165C58AD] DEFAULT ('N'),
[Decimals] [smallint] NOT NULL CONSTRAINT [DF__FW_Custom__Decim__17507CE6] DEFAULT ((0)),
[MinValue] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaxValue] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Required] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReqWBSLevel] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [smallint] NULL,
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FieldType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_Custom__Field__1844A11F] DEFAULT ('C'),
[PropertyBag] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AvailableForCubes] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_Custom__Avail__1938C558] DEFAULT ('N'),
[SearchBy] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_Custom__Searc__1A2CE991] DEFAULT ('N'),
[SearchResults] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_Custom__Searc__1B210DCA] DEFAULT ('N'),
[DefaultValue] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CustomColumnsData] ADD CONSTRAINT [FW_CustomColumnsDataPK] PRIMARY KEY NONCLUSTERED ([InfoCenterArea], [GridID], [Name]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
