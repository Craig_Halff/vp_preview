CREATE TABLE [dbo].[CFGOrgSetup]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PayrollDefOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LabDist] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOrgSet__LabDi__44624F28] DEFAULT ('N'),
[EmplExp] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOrgSet__EmplE__45567361] DEFAULT ('N'),
[Prints] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOrgSet__Print__464A979A] DEFAULT ('N'),
[Cons] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOrgSetu__Cons__473EBBD3] DEFAULT ('N'),
[Misc] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOrgSetu__Misc__4832E00C] DEFAULT ('N'),
[Units] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOrgSet__Units__49270445] DEFAULT ('N'),
[Payroll] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOrgSet__Payro__4A1B287E] DEFAULT ('N'),
[FICA] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOrgSetu__FICA__4B0F4CB7] DEFAULT ('N'),
[OrgIntercompany] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContributionCredit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOrgSet__Contr__4C0370F0] DEFAULT ('N'),
[ContributionDebit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOrgSet__Contr__4CF79529] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGOrgSetup] ADD CONSTRAINT [CFGOrgSetupPK] PRIMARY KEY CLUSTERED ([Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
