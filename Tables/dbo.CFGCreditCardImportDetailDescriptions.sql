CREATE TABLE [dbo].[CFGCreditCardImportDetailDescriptions]
(
[PrimaryCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FieldName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FieldLabel] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCreditCardImportDetailDescriptions] ADD CONSTRAINT [CFGCreditCardImportDetailDescriptionsPK] PRIMARY KEY NONCLUSTERED ([PrimaryCode], [Company], [FieldName], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
