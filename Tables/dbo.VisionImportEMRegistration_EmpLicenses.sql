CREATE TABLE [dbo].[VisionImportEMRegistration_EmpLicenses]
(
[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecordID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [int] NULL,
[DateEarned] [datetime] NULL,
[DateExpires] [datetime] NULL,
[Registration] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegistrationNo] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateRegistered] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryRegistered] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncludeInProposal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastRenewed] [datetime] NULL,
[RegistrationType] [varchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
