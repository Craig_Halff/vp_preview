CREATE TABLE [dbo].[CFGLocale]
(
[Locale] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxTypeCode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGLocale] ADD CONSTRAINT [CFGLocalePK] PRIMARY KEY CLUSTERED ([Locale]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
