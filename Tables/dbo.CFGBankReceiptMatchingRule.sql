CREATE TABLE [dbo].[CFGBankReceiptMatchingRule]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[App] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MatchingRule] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGBankReceiptMatchingRule] ADD CONSTRAINT [CFGBankReceiptMatchingRulePK] PRIMARY KEY CLUSTERED ([Code], [App], [TransType]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
