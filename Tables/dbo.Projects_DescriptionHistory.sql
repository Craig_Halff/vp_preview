CREATE TABLE [dbo].[Projects_DescriptionHistory]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustDescriptionHistoryDate] [datetime] NULL CONSTRAINT [DF__Projects___CustD__5C7A9B32] DEFAULT (NULL),
[CustDescriptionHistoryCategory] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDescriptionHistoryOld] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDescriptionHistoryNew] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDescriptionHistoryModUser] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDescriptionHistoryExplanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_Projects_DescriptionHistory]
      ON [dbo].[Projects_DescriptionHistory]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Projects_DescriptionHistory'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'WBS1',CONVERT(NVARCHAR(2000),[WBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'WBS2',CONVERT(NVARCHAR(2000),[WBS2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'WBS3',CONVERT(NVARCHAR(2000),[WBS3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Seq',CONVERT(NVARCHAR(2000),[Seq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustDescriptionHistoryDate',CONVERT(NVARCHAR(2000),[CustDescriptionHistoryDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustDescriptionHistoryCategory',CONVERT(NVARCHAR(2000),[CustDescriptionHistoryCategory],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustDescriptionHistoryOld','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustDescriptionHistoryNew','[text]',NULL, @source,@app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustDescriptionHistoryModUser',CONVERT(NVARCHAR(2000),DELETED.[CustDescriptionHistoryModUser],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustDescriptionHistoryModUser = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustDescriptionHistoryExplanation',CONVERT(NVARCHAR(2000),[CustDescriptionHistoryExplanation],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_Projects_DescriptionHistory] ON [dbo].[Projects_DescriptionHistory]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_Projects_DescriptionHistory]
      ON [dbo].[Projects_DescriptionHistory]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Projects_DescriptionHistory'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS1',NULL,CONVERT(NVARCHAR(2000),[WBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS2',NULL,CONVERT(NVARCHAR(2000),[WBS2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS3',NULL,CONVERT(NVARCHAR(2000),[WBS3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',NULL,CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustDescriptionHistoryDate',NULL,CONVERT(NVARCHAR(2000),[CustDescriptionHistoryDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustDescriptionHistoryCategory',NULL,CONVERT(NVARCHAR(2000),[CustDescriptionHistoryCategory],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustDescriptionHistoryOld',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustDescriptionHistoryNew',NULL,'[text]', @source, @app
      FROM INSERTED


    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustDescriptionHistoryModUser',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustDescriptionHistoryModUser],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustDescriptionHistoryModUser = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustDescriptionHistoryExplanation',NULL,CONVERT(NVARCHAR(2000),[CustDescriptionHistoryExplanation],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_Projects_DescriptionHistory] ON [dbo].[Projects_DescriptionHistory]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_Projects_DescriptionHistory]
      ON [dbo].[Projects_DescriptionHistory]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Projects_DescriptionHistory'
    
      If UPDATE([WBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS1',
      CONVERT(NVARCHAR(2000),DELETED.[WBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS1] Is Null And
				DELETED.[WBS1] Is Not Null
			) Or
			(
				INSERTED.[WBS1] Is Not Null And
				DELETED.[WBS1] Is Null
			) Or
			(
				INSERTED.[WBS1] !=
				DELETED.[WBS1]
			)
		) 
		END		
		
      If UPDATE([WBS2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS2',
      CONVERT(NVARCHAR(2000),DELETED.[WBS2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS2] Is Null And
				DELETED.[WBS2] Is Not Null
			) Or
			(
				INSERTED.[WBS2] Is Not Null And
				DELETED.[WBS2] Is Null
			) Or
			(
				INSERTED.[WBS2] !=
				DELETED.[WBS2]
			)
		) 
		END		
		
      If UPDATE([WBS3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS3',
      CONVERT(NVARCHAR(2000),DELETED.[WBS3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS3] Is Null And
				DELETED.[WBS3] Is Not Null
			) Or
			(
				INSERTED.[WBS3] Is Not Null And
				DELETED.[WBS3] Is Null
			) Or
			(
				INSERTED.[WBS3] !=
				DELETED.[WBS3]
			)
		) 
		END		
		
      If UPDATE([Seq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',
      CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] Is Null And
				DELETED.[Seq] Is Not Null
			) Or
			(
				INSERTED.[Seq] Is Not Null And
				DELETED.[Seq] Is Null
			) Or
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
		
      If UPDATE([CustDescriptionHistoryDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustDescriptionHistoryDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustDescriptionHistoryDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustDescriptionHistoryDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustDescriptionHistoryDate] Is Null And
				DELETED.[CustDescriptionHistoryDate] Is Not Null
			) Or
			(
				INSERTED.[CustDescriptionHistoryDate] Is Not Null And
				DELETED.[CustDescriptionHistoryDate] Is Null
			) Or
			(
				INSERTED.[CustDescriptionHistoryDate] !=
				DELETED.[CustDescriptionHistoryDate]
			)
		) 
		END		
		
      If UPDATE([CustDescriptionHistoryCategory])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustDescriptionHistoryCategory',
      CONVERT(NVARCHAR(2000),DELETED.[CustDescriptionHistoryCategory],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustDescriptionHistoryCategory],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustDescriptionHistoryCategory] Is Null And
				DELETED.[CustDescriptionHistoryCategory] Is Not Null
			) Or
			(
				INSERTED.[CustDescriptionHistoryCategory] Is Not Null And
				DELETED.[CustDescriptionHistoryCategory] Is Null
			) Or
			(
				INSERTED.[CustDescriptionHistoryCategory] !=
				DELETED.[CustDescriptionHistoryCategory]
			)
		) 
		END		
		
      If UPDATE([CustDescriptionHistoryOld])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustDescriptionHistoryOld',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustDescriptionHistoryNew])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustDescriptionHistoryNew',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
     If UPDATE([CustDescriptionHistoryModUser])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustDescriptionHistoryModUser',
     CONVERT(NVARCHAR(2000),DELETED.[CustDescriptionHistoryModUser],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustDescriptionHistoryModUser],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustDescriptionHistoryModUser] Is Null And
				DELETED.[CustDescriptionHistoryModUser] Is Not Null
			) Or
			(
				INSERTED.[CustDescriptionHistoryModUser] Is Not Null And
				DELETED.[CustDescriptionHistoryModUser] Is Null
			) Or
			(
				INSERTED.[CustDescriptionHistoryModUser] !=
				DELETED.[CustDescriptionHistoryModUser]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustDescriptionHistoryModUser = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustDescriptionHistoryModUser = newDesc.Employee
		END		
		
      If UPDATE([CustDescriptionHistoryExplanation])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustDescriptionHistoryExplanation',
      CONVERT(NVARCHAR(2000),DELETED.[CustDescriptionHistoryExplanation],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustDescriptionHistoryExplanation],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustDescriptionHistoryExplanation] Is Null And
				DELETED.[CustDescriptionHistoryExplanation] Is Not Null
			) Or
			(
				INSERTED.[CustDescriptionHistoryExplanation] Is Not Null And
				DELETED.[CustDescriptionHistoryExplanation] Is Null
			) Or
			(
				INSERTED.[CustDescriptionHistoryExplanation] !=
				DELETED.[CustDescriptionHistoryExplanation]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_Projects_DescriptionHistory] ON [dbo].[Projects_DescriptionHistory]
GO
ALTER TABLE [dbo].[Projects_DescriptionHistory] ADD CONSTRAINT [Projects_DescriptionHistoryPK] PRIMARY KEY CLUSTERED ([WBS1], [WBS2], [WBS3], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
