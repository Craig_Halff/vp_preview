CREATE TABLE [dbo].[WorkflowColumns]
(
[WorkflowColumns_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ApplicationName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApplicationType] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TableName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ColumnName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowColumns] ADD CONSTRAINT [WorkflowColumnsPK] PRIMARY KEY NONCLUSTERED ([WorkflowColumns_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
