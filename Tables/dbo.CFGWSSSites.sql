CREATE TABLE [dbo].[CFGWSSSites]
(
[SiteID] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SiteURL] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WebID] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WebURL] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WebTitle] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TopLevel] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGWSSSit__TopLe__727E1F93] DEFAULT ('N'),
[TemplateID] [int] NULL,
[TemplateName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TemplateTitle] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGWSSSites] ADD CONSTRAINT [CFGWSSSitesPK] PRIMARY KEY NONCLUSTERED ([SiteID], [WebID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
