CREATE TABLE [dbo].[CCG_PAT_Delegation]
(
[Id] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Delegate] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FromDate] [datetime] NOT NULL,
[ToDate] [datetime] NOT NULL,
[MaxAmount] [int] NULL,
[Dual] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ApprovedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovedOn] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_Delegation] ADD CONSTRAINT [PK_CCG_PAT_Delegation] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_CCG_PAT_Delegation] ON [dbo].[CCG_PAT_Delegation] ([FromDate], [ToDate]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'List of pending and active delegations in the system', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Delegation', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Employee id of the one who approved the delegation request (if any)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Delegation', 'COLUMN', N'ApprovedBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date when the delegation was approved', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Delegation', 'COLUMN', N'ApprovedOn'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The employee id of the delegate', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Delegation', 'COLUMN', N'Delegate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Will the delegator have access to items simultaneously with the delegate? Y/N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Delegation', 'COLUMN', N'Dual'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The employee id of the delegator (one for which items will be delegated to another)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Delegation', 'COLUMN', N'Employee'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The start date of the delegation period', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Delegation', 'COLUMN', N'FromDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Delegation', 'COLUMN', N'Id'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The maximum currency amount for which this delegation is valid (NULL if no limit)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Delegation', 'COLUMN', N'MaxAmount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The end date of the delegation period', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Delegation', 'COLUMN', N'ToDate'
GO
