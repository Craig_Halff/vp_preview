CREATE TABLE [dbo].[EmployeeCustomTabFields]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustLegacyId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBSTName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBSTLaborGroup] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBSTExpGroup] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBSTAdminGroup] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBSTLocation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custBSTAltOrg2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustAnnualSalary] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__EmployeeC__CustA__6A5DDADE] DEFAULT ((0)),
[CustBiweeklySalary] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__EmployeeC__CustB__6B51FF17] DEFAULT ((0)),
[CustOperationsManager] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustO__6C462350] DEFAULT ('N'),
[CustEKApprover] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEKAlternateApprover] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustWageType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEKOperationsManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEKDirector] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEKManagementLeader] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustUnpaidLeaveStart] [datetime] NULL,
[CustUnpaidLeaveEnd] [datetime] NULL,
[CustTeamPracticeArea] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustRoleManagementTeam] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__6D3A4789] DEFAULT ('N'),
[CustAdjHireDate] [datetime] NULL,
[CustTrueHireDate] [datetime] NULL,
[CustOldTarget] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__EmployeeC__CustO__6E2E6BC2] DEFAULT ((0)),
[CustTeamLeader] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustT__6F228FFB] DEFAULT ('N'),
[CustHireTermDate] [datetime] NULL CONSTRAINT [DF__EmployeeC__CustH__7016B434] DEFAULT (NULL),
[CustHireMentor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHireComputerSetup] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHirePhoneRequired] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHireOfficeSpaceLoc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHireComments] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHireResponseComplete] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustH__710AD86D] DEFAULT ('N'),
[CustHireTermStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustH__71FEFCA6] DEFAULT ('New Hire'),
[CustHireITSetupEmployee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHireITDomain] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHireITCompShip] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHireITCompPlaced] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHireITPhone] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHireITEmail] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHireITWelcome] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHireITSetupComplete] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustH__72F320DF] DEFAULT ('N'),
[CustTermReplacementPercent] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTermReplacementDate] [datetime] NULL,
[CustTermEmailMonitorReq] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTermEmailMonitorEmployee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTermPhoneMonitorReq] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTermPhoneMonitorEmployee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTermComments] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustSupervisorTermResponseComplete] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustS__73E74518] DEFAULT ('N'),
[CustTermITTeardownEmployee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTermITEmailPhone] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTermITCompRecover] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTermITTeardownComplete] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustT__74DB6951] DEFAULT ('N'),
[CustCredentials] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPATBillerReview] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustP__75CF8D8A] DEFAULT ('N'),
[CustEmployeeInfoVerified] [datetime] NULL,
[CustRoleBoardofDirectors] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__76C3B1C3] DEFAULT ('N'),
[CustRoleExecutiveVicePresident] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__77B7D5FC] DEFAULT ('N'),
[CustRoleVicePresident] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__78ABFA35] DEFAULT ('N'),
[CustRolePracticeLeader] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__79A01E6E] DEFAULT ('N'),
[CustRoleDirector] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__7A9442A7] DEFAULT ('N'),
[CustRoleAuthorizedAgent] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__7B8866E0] DEFAULT ('N'),
[CustRoleProjectManager] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__7C7C8B19] DEFAULT ('N'),
[CustRoleStockholder] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__7D70AF52] DEFAULT ('N'),
[CustDriverLicense] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDriverLicenseState] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDiSCType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustRoleSeniorVicePresident] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__7E64D38B] DEFAULT ('N'),
[CustRolePresident] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__7F58F7C4] DEFAULT ('N'),
[CustRoleChiefOperatingOfficer] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__004D1BFD] DEFAULT ('N'),
[CustRoleChiefStrategyOfficer] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__01414036] DEFAULT ('N'),
[CustRoleViceChairmanoftheBoard] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__0235646F] DEFAULT ('N'),
[CustRoleChairmanoftheBoard] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__032988A8] DEFAULT ('N'),
[CustRoleCorporateSecretary] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__041DACE1] DEFAULT ('N'),
[CustRoleDeputyCorporateSecretary] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__0511D11A] DEFAULT ('N'),
[CustRoleChairmanEmeritus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__0605F553] DEFAULT ('N'),
[CustRoleSecofRealEstateServices] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__06FA198C] DEFAULT ('N'),
[CustRoleGeneralCounsel] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__07EE3DC5] DEFAULT ('N'),
[CustExperienceCurrentThisFirm] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__EmployeeC__CustE__08E261FE] DEFAULT ((0)),
[CustExperienceTotalAllFirm] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__EmployeeC__CustE__09D68637] DEFAULT ((0)),
[CustExperienceTotalThisFirm] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__EmployeeC__CustE__0ACAAA70] DEFAULT ((0)),
[CustEKPracticeLeader] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustRoleChiefFinancialOfficer] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__0BBECEA9] DEFAULT ('N'),
[CustEmpGender] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEmpEthnicity] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEmp] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLegacySource] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCorporateTitle] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEmploymentType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustManagement] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEmployeeInfoVerifiedUser] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCertificationAdd] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustRolePracticeLeaderRegional] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__0CB2F2E2] DEFAULT ('N'),
[CustRoleESOPParticipant] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__0DA7171B] DEFAULT ('N'),
[CustRoleChiefMarketingOfficer] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__0E9B3B54] DEFAULT ('N'),
[CustRoleOpsTeamMember] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EmployeeC__CustR__0F8F5F8D] DEFAULT ('N'),
[CustConflictofInterest] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EmployeeC__CustC__30DE721B] DEFAULT ('N')
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_EmployeeCustomTabFields]
      ON [dbo].[EmployeeCustomTabFields]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EmployeeCustomTabFields'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'Employee',CONVERT(NVARCHAR(2000),DELETED.[Employee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Employee = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustLegacyId',CONVERT(NVARCHAR(2000),[CustLegacyId],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustBSTName',CONVERT(NVARCHAR(2000),[CustBSTName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustBSTLaborGroup',CONVERT(NVARCHAR(2000),[CustBSTLaborGroup],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustBSTExpGroup',CONVERT(NVARCHAR(2000),[CustBSTExpGroup],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustBSTAdminGroup',CONVERT(NVARCHAR(2000),[CustBSTAdminGroup],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustBSTLocation',CONVERT(NVARCHAR(2000),[CustBSTLocation],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'custBSTAltOrg2',CONVERT(NVARCHAR(2000),[custBSTAltOrg2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustAnnualSalary',CONVERT(NVARCHAR(2000),[CustAnnualSalary],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustBiweeklySalary',CONVERT(NVARCHAR(2000),[CustBiweeklySalary],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustOperationsManager',CONVERT(NVARCHAR(2000),[CustOperationsManager],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustEKApprover',CONVERT(NVARCHAR(2000),DELETED.[CustEKApprover],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEKApprover = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustEKAlternateApprover',CONVERT(NVARCHAR(2000),DELETED.[CustEKAlternateApprover],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEKAlternateApprover = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustWageType',CONVERT(NVARCHAR(2000),[CustWageType],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustEKOperationsManager',CONVERT(NVARCHAR(2000),DELETED.[CustEKOperationsManager],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEKOperationsManager = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustEKDirector',CONVERT(NVARCHAR(2000),DELETED.[CustEKDirector],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEKDirector = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustEKManagementLeader',CONVERT(NVARCHAR(2000),DELETED.[CustEKManagementLeader],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEKManagementLeader = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustUnpaidLeaveStart',CONVERT(NVARCHAR(2000),[CustUnpaidLeaveStart],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustUnpaidLeaveEnd',CONVERT(NVARCHAR(2000),[CustUnpaidLeaveEnd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustTeamPracticeArea',CONVERT(NVARCHAR(2000),[CustTeamPracticeArea],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleManagementTeam',CONVERT(NVARCHAR(2000),[CustRoleManagementTeam],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustAdjHireDate',CONVERT(NVARCHAR(2000),[CustAdjHireDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustTrueHireDate',CONVERT(NVARCHAR(2000),[CustTrueHireDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustOldTarget',CONVERT(NVARCHAR(2000),[CustOldTarget],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustTeamLeader',CONVERT(NVARCHAR(2000),[CustTeamLeader],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustHireTermDate',CONVERT(NVARCHAR(2000),[CustHireTermDate],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustHireMentor',CONVERT(NVARCHAR(2000),DELETED.[CustHireMentor],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHireMentor = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustHireComputerSetup',CONVERT(NVARCHAR(2000),DELETED.[CustHireComputerSetup],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHireComputerSetup = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustHirePhoneRequired',CONVERT(NVARCHAR(2000),[CustHirePhoneRequired],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustHireOfficeSpaceLoc',CONVERT(NVARCHAR(2000),[CustHireOfficeSpaceLoc],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustHireComments',CONVERT(NVARCHAR(2000),[CustHireComments],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustHireResponseComplete',CONVERT(NVARCHAR(2000),[CustHireResponseComplete],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustHireTermStatus',CONVERT(NVARCHAR(2000),[CustHireTermStatus],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustHireITSetupEmployee',CONVERT(NVARCHAR(2000),DELETED.[CustHireITSetupEmployee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHireITSetupEmployee = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustHireITDomain',CONVERT(NVARCHAR(2000),[CustHireITDomain],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustHireITCompShip',CONVERT(NVARCHAR(2000),[CustHireITCompShip],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustHireITCompPlaced',CONVERT(NVARCHAR(2000),[CustHireITCompPlaced],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustHireITPhone',CONVERT(NVARCHAR(2000),[CustHireITPhone],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustHireITEmail',CONVERT(NVARCHAR(2000),[CustHireITEmail],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustHireITWelcome',CONVERT(NVARCHAR(2000),[CustHireITWelcome],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustHireITSetupComplete',CONVERT(NVARCHAR(2000),[CustHireITSetupComplete],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustTermReplacementPercent',CONVERT(NVARCHAR(2000),[CustTermReplacementPercent],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustTermReplacementDate',CONVERT(NVARCHAR(2000),[CustTermReplacementDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustTermEmailMonitorReq',CONVERT(NVARCHAR(2000),[CustTermEmailMonitorReq],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustTermEmailMonitorEmployee',CONVERT(NVARCHAR(2000),DELETED.[CustTermEmailMonitorEmployee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustTermEmailMonitorEmployee = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustTermPhoneMonitorReq',CONVERT(NVARCHAR(2000),[CustTermPhoneMonitorReq],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustTermPhoneMonitorEmployee',CONVERT(NVARCHAR(2000),DELETED.[CustTermPhoneMonitorEmployee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustTermPhoneMonitorEmployee = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustTermComments',CONVERT(NVARCHAR(2000),[CustTermComments],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustSupervisorTermResponseComplete',CONVERT(NVARCHAR(2000),[CustSupervisorTermResponseComplete],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustTermITTeardownEmployee',CONVERT(NVARCHAR(2000),DELETED.[CustTermITTeardownEmployee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustTermITTeardownEmployee = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustTermITEmailPhone',CONVERT(NVARCHAR(2000),[CustTermITEmailPhone],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustTermITCompRecover',CONVERT(NVARCHAR(2000),[CustTermITCompRecover],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustTermITTeardownComplete',CONVERT(NVARCHAR(2000),[CustTermITTeardownComplete],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustCredentials',CONVERT(NVARCHAR(2000),[CustCredentials],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustPATBillerReview',CONVERT(NVARCHAR(2000),[CustPATBillerReview],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustEmployeeInfoVerified',CONVERT(NVARCHAR(2000),[CustEmployeeInfoVerified],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleBoardofDirectors',CONVERT(NVARCHAR(2000),[CustRoleBoardofDirectors],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleExecutiveVicePresident',CONVERT(NVARCHAR(2000),[CustRoleExecutiveVicePresident],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleVicePresident',CONVERT(NVARCHAR(2000),[CustRoleVicePresident],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRolePracticeLeader',CONVERT(NVARCHAR(2000),[CustRolePracticeLeader],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleDirector',CONVERT(NVARCHAR(2000),[CustRoleDirector],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleAuthorizedAgent',CONVERT(NVARCHAR(2000),[CustRoleAuthorizedAgent],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleProjectManager',CONVERT(NVARCHAR(2000),[CustRoleProjectManager],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleStockholder',CONVERT(NVARCHAR(2000),[CustRoleStockholder],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustDriverLicense',CONVERT(NVARCHAR(2000),[CustDriverLicense],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustDriverLicenseState',CONVERT(NVARCHAR(2000),[CustDriverLicenseState],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustDiSCType',CONVERT(NVARCHAR(2000),[CustDiSCType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleSeniorVicePresident',CONVERT(NVARCHAR(2000),[CustRoleSeniorVicePresident],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRolePresident',CONVERT(NVARCHAR(2000),[CustRolePresident],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleChiefOperatingOfficer',CONVERT(NVARCHAR(2000),[CustRoleChiefOperatingOfficer],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleChiefStrategyOfficer',CONVERT(NVARCHAR(2000),[CustRoleChiefStrategyOfficer],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleViceChairmanoftheBoard',CONVERT(NVARCHAR(2000),[CustRoleViceChairmanoftheBoard],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleChairmanoftheBoard',CONVERT(NVARCHAR(2000),[CustRoleChairmanoftheBoard],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleCorporateSecretary',CONVERT(NVARCHAR(2000),[CustRoleCorporateSecretary],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleDeputyCorporateSecretary',CONVERT(NVARCHAR(2000),[CustRoleDeputyCorporateSecretary],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleChairmanEmeritus',CONVERT(NVARCHAR(2000),[CustRoleChairmanEmeritus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleSecofRealEstateServices',CONVERT(NVARCHAR(2000),[CustRoleSecofRealEstateServices],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleGeneralCounsel',CONVERT(NVARCHAR(2000),[CustRoleGeneralCounsel],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustExperienceCurrentThisFirm',CONVERT(NVARCHAR(2000),[CustExperienceCurrentThisFirm],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustExperienceTotalAllFirm',CONVERT(NVARCHAR(2000),[CustExperienceTotalAllFirm],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustExperienceTotalThisFirm',CONVERT(NVARCHAR(2000),[CustExperienceTotalThisFirm],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustEKPracticeLeader',CONVERT(NVARCHAR(2000),DELETED.[CustEKPracticeLeader],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEKPracticeLeader = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleChiefFinancialOfficer',CONVERT(NVARCHAR(2000),[CustRoleChiefFinancialOfficer],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustEmpGender',CONVERT(NVARCHAR(2000),[CustEmpGender],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustEmpEthnicity',CONVERT(NVARCHAR(2000),[CustEmpEthnicity],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustEmp',CONVERT(NVARCHAR(2000),DELETED.[CustEmp],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEmp = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustLegacySource',CONVERT(NVARCHAR(2000),[CustLegacySource],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustCorporateTitle',CONVERT(NVARCHAR(2000),[CustCorporateTitle],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustEmploymentType',CONVERT(NVARCHAR(2000),[CustEmploymentType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustManagement',CONVERT(NVARCHAR(2000),[CustManagement],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustEmployeeInfoVerifiedUser',CONVERT(NVARCHAR(2000),DELETED.[CustEmployeeInfoVerifiedUser],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEmployeeInfoVerifiedUser = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustCertificationAdd',CONVERT(NVARCHAR(2000),[CustCertificationAdd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRolePracticeLeaderRegional',CONVERT(NVARCHAR(2000),[CustRolePracticeLeaderRegional],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleESOPParticipant',CONVERT(NVARCHAR(2000),[CustRoleESOPParticipant],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleChiefMarketingOfficer',CONVERT(NVARCHAR(2000),[CustRoleChiefMarketingOfficer],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustRoleOpsTeamMember',CONVERT(NVARCHAR(2000),[CustRoleOpsTeamMember],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CustConflictofInterest',CONVERT(NVARCHAR(2000),[CustConflictofInterest],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_EmployeeCustomTabFields]
      ON [dbo].[EmployeeCustomTabFields]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EmployeeCustomTabFields'
    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Employee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Employee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Employee = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustLegacyId',NULL,CONVERT(NVARCHAR(2000),[CustLegacyId],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustBSTName',NULL,CONVERT(NVARCHAR(2000),[CustBSTName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustBSTLaborGroup',NULL,CONVERT(NVARCHAR(2000),[CustBSTLaborGroup],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustBSTExpGroup',NULL,CONVERT(NVARCHAR(2000),[CustBSTExpGroup],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustBSTAdminGroup',NULL,CONVERT(NVARCHAR(2000),[CustBSTAdminGroup],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustBSTLocation',NULL,CONVERT(NVARCHAR(2000),[CustBSTLocation],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'custBSTAltOrg2',NULL,CONVERT(NVARCHAR(2000),[custBSTAltOrg2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustAnnualSalary',NULL,CONVERT(NVARCHAR(2000),[CustAnnualSalary],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustBiweeklySalary',NULL,CONVERT(NVARCHAR(2000),[CustBiweeklySalary],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustOperationsManager',NULL,CONVERT(NVARCHAR(2000),[CustOperationsManager],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEKApprover',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustEKApprover],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEKApprover = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEKAlternateApprover',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustEKAlternateApprover],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEKAlternateApprover = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustWageType',NULL,CONVERT(NVARCHAR(2000),[CustWageType],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEKOperationsManager',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustEKOperationsManager],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEKOperationsManager = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEKDirector',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustEKDirector],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEKDirector = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEKManagementLeader',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustEKManagementLeader],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEKManagementLeader = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustUnpaidLeaveStart',NULL,CONVERT(NVARCHAR(2000),[CustUnpaidLeaveStart],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustUnpaidLeaveEnd',NULL,CONVERT(NVARCHAR(2000),[CustUnpaidLeaveEnd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTeamPracticeArea',NULL,CONVERT(NVARCHAR(2000),[CustTeamPracticeArea],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleManagementTeam',NULL,CONVERT(NVARCHAR(2000),[CustRoleManagementTeam],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustAdjHireDate',NULL,CONVERT(NVARCHAR(2000),[CustAdjHireDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTrueHireDate',NULL,CONVERT(NVARCHAR(2000),[CustTrueHireDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustOldTarget',NULL,CONVERT(NVARCHAR(2000),[CustOldTarget],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTeamLeader',NULL,CONVERT(NVARCHAR(2000),[CustTeamLeader],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireTermDate',NULL,CONVERT(NVARCHAR(2000),[CustHireTermDate],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireMentor',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHireMentor],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHireMentor = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireComputerSetup',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHireComputerSetup],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHireComputerSetup = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHirePhoneRequired',NULL,CONVERT(NVARCHAR(2000),[CustHirePhoneRequired],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireOfficeSpaceLoc',NULL,CONVERT(NVARCHAR(2000),[CustHireOfficeSpaceLoc],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireComments',NULL,CONVERT(NVARCHAR(2000),[CustHireComments],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireResponseComplete',NULL,CONVERT(NVARCHAR(2000),[CustHireResponseComplete],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireTermStatus',NULL,CONVERT(NVARCHAR(2000),[CustHireTermStatus],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireITSetupEmployee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHireITSetupEmployee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHireITSetupEmployee = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireITDomain',NULL,CONVERT(NVARCHAR(2000),[CustHireITDomain],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireITCompShip',NULL,CONVERT(NVARCHAR(2000),[CustHireITCompShip],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireITCompPlaced',NULL,CONVERT(NVARCHAR(2000),[CustHireITCompPlaced],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireITPhone',NULL,CONVERT(NVARCHAR(2000),[CustHireITPhone],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireITEmail',NULL,CONVERT(NVARCHAR(2000),[CustHireITEmail],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireITWelcome',NULL,CONVERT(NVARCHAR(2000),[CustHireITWelcome],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireITSetupComplete',NULL,CONVERT(NVARCHAR(2000),[CustHireITSetupComplete],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermReplacementPercent',NULL,CONVERT(NVARCHAR(2000),[CustTermReplacementPercent],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermReplacementDate',NULL,CONVERT(NVARCHAR(2000),[CustTermReplacementDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermEmailMonitorReq',NULL,CONVERT(NVARCHAR(2000),[CustTermEmailMonitorReq],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermEmailMonitorEmployee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustTermEmailMonitorEmployee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustTermEmailMonitorEmployee = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermPhoneMonitorReq',NULL,CONVERT(NVARCHAR(2000),[CustTermPhoneMonitorReq],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermPhoneMonitorEmployee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustTermPhoneMonitorEmployee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustTermPhoneMonitorEmployee = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermComments',NULL,CONVERT(NVARCHAR(2000),[CustTermComments],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustSupervisorTermResponseComplete',NULL,CONVERT(NVARCHAR(2000),[CustSupervisorTermResponseComplete],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermITTeardownEmployee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustTermITTeardownEmployee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustTermITTeardownEmployee = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermITEmailPhone',NULL,CONVERT(NVARCHAR(2000),[CustTermITEmailPhone],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermITCompRecover',NULL,CONVERT(NVARCHAR(2000),[CustTermITCompRecover],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermITTeardownComplete',NULL,CONVERT(NVARCHAR(2000),[CustTermITTeardownComplete],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustCredentials',NULL,CONVERT(NVARCHAR(2000),[CustCredentials],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustPATBillerReview',NULL,CONVERT(NVARCHAR(2000),[CustPATBillerReview],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEmployeeInfoVerified',NULL,CONVERT(NVARCHAR(2000),[CustEmployeeInfoVerified],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleBoardofDirectors',NULL,CONVERT(NVARCHAR(2000),[CustRoleBoardofDirectors],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleExecutiveVicePresident',NULL,CONVERT(NVARCHAR(2000),[CustRoleExecutiveVicePresident],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleVicePresident',NULL,CONVERT(NVARCHAR(2000),[CustRoleVicePresident],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRolePracticeLeader',NULL,CONVERT(NVARCHAR(2000),[CustRolePracticeLeader],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleDirector',NULL,CONVERT(NVARCHAR(2000),[CustRoleDirector],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleAuthorizedAgent',NULL,CONVERT(NVARCHAR(2000),[CustRoleAuthorizedAgent],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleProjectManager',NULL,CONVERT(NVARCHAR(2000),[CustRoleProjectManager],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleStockholder',NULL,CONVERT(NVARCHAR(2000),[CustRoleStockholder],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustDriverLicense',NULL,CONVERT(NVARCHAR(2000),[CustDriverLicense],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustDriverLicenseState',NULL,CONVERT(NVARCHAR(2000),[CustDriverLicenseState],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustDiSCType',NULL,CONVERT(NVARCHAR(2000),[CustDiSCType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleSeniorVicePresident',NULL,CONVERT(NVARCHAR(2000),[CustRoleSeniorVicePresident],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRolePresident',NULL,CONVERT(NVARCHAR(2000),[CustRolePresident],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleChiefOperatingOfficer',NULL,CONVERT(NVARCHAR(2000),[CustRoleChiefOperatingOfficer],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleChiefStrategyOfficer',NULL,CONVERT(NVARCHAR(2000),[CustRoleChiefStrategyOfficer],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleViceChairmanoftheBoard',NULL,CONVERT(NVARCHAR(2000),[CustRoleViceChairmanoftheBoard],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleChairmanoftheBoard',NULL,CONVERT(NVARCHAR(2000),[CustRoleChairmanoftheBoard],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleCorporateSecretary',NULL,CONVERT(NVARCHAR(2000),[CustRoleCorporateSecretary],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleDeputyCorporateSecretary',NULL,CONVERT(NVARCHAR(2000),[CustRoleDeputyCorporateSecretary],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleChairmanEmeritus',NULL,CONVERT(NVARCHAR(2000),[CustRoleChairmanEmeritus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleSecofRealEstateServices',NULL,CONVERT(NVARCHAR(2000),[CustRoleSecofRealEstateServices],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleGeneralCounsel',NULL,CONVERT(NVARCHAR(2000),[CustRoleGeneralCounsel],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustExperienceCurrentThisFirm',NULL,CONVERT(NVARCHAR(2000),[CustExperienceCurrentThisFirm],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustExperienceTotalAllFirm',NULL,CONVERT(NVARCHAR(2000),[CustExperienceTotalAllFirm],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustExperienceTotalThisFirm',NULL,CONVERT(NVARCHAR(2000),[CustExperienceTotalThisFirm],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEKPracticeLeader',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustEKPracticeLeader],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEKPracticeLeader = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleChiefFinancialOfficer',NULL,CONVERT(NVARCHAR(2000),[CustRoleChiefFinancialOfficer],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEmpGender',NULL,CONVERT(NVARCHAR(2000),[CustEmpGender],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEmpEthnicity',NULL,CONVERT(NVARCHAR(2000),[CustEmpEthnicity],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEmp',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustEmp],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEmp = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustLegacySource',NULL,CONVERT(NVARCHAR(2000),[CustLegacySource],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustCorporateTitle',NULL,CONVERT(NVARCHAR(2000),[CustCorporateTitle],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEmploymentType',NULL,CONVERT(NVARCHAR(2000),[CustEmploymentType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustManagement',NULL,CONVERT(NVARCHAR(2000),[CustManagement],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEmployeeInfoVerifiedUser',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustEmployeeInfoVerifiedUser],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEmployeeInfoVerifiedUser = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustCertificationAdd',NULL,CONVERT(NVARCHAR(2000),[CustCertificationAdd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRolePracticeLeaderRegional',NULL,CONVERT(NVARCHAR(2000),[CustRolePracticeLeaderRegional],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleESOPParticipant',NULL,CONVERT(NVARCHAR(2000),[CustRoleESOPParticipant],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleChiefMarketingOfficer',NULL,CONVERT(NVARCHAR(2000),[CustRoleChiefMarketingOfficer],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleOpsTeamMember',NULL,CONVERT(NVARCHAR(2000),[CustRoleOpsTeamMember],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustConflictofInterest',NULL,CONVERT(NVARCHAR(2000),[CustConflictofInterest],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_EmployeeCustomTabFields]
      ON [dbo].[EmployeeCustomTabFields]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EmployeeCustomTabFields'
    
     If UPDATE([Employee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Employee',
     CONVERT(NVARCHAR(2000),DELETED.[Employee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Employee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[Employee] Is Null And
				DELETED.[Employee] Is Not Null
			) Or
			(
				INSERTED.[Employee] Is Not Null And
				DELETED.[Employee] Is Null
			) Or
			(
				INSERTED.[Employee] !=
				DELETED.[Employee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Employee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Employee = newDesc.Employee
		END		
		
      If UPDATE([CustLegacyId])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustLegacyId',
      CONVERT(NVARCHAR(2000),DELETED.[CustLegacyId],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLegacyId],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustLegacyId] Is Null And
				DELETED.[CustLegacyId] Is Not Null
			) Or
			(
				INSERTED.[CustLegacyId] Is Not Null And
				DELETED.[CustLegacyId] Is Null
			) Or
			(
				INSERTED.[CustLegacyId] !=
				DELETED.[CustLegacyId]
			)
		) 
		END		
		
      If UPDATE([CustBSTName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustBSTName',
      CONVERT(NVARCHAR(2000),DELETED.[CustBSTName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBSTName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustBSTName] Is Null And
				DELETED.[CustBSTName] Is Not Null
			) Or
			(
				INSERTED.[CustBSTName] Is Not Null And
				DELETED.[CustBSTName] Is Null
			) Or
			(
				INSERTED.[CustBSTName] !=
				DELETED.[CustBSTName]
			)
		) 
		END		
		
      If UPDATE([CustBSTLaborGroup])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustBSTLaborGroup',
      CONVERT(NVARCHAR(2000),DELETED.[CustBSTLaborGroup],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBSTLaborGroup],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustBSTLaborGroup] Is Null And
				DELETED.[CustBSTLaborGroup] Is Not Null
			) Or
			(
				INSERTED.[CustBSTLaborGroup] Is Not Null And
				DELETED.[CustBSTLaborGroup] Is Null
			) Or
			(
				INSERTED.[CustBSTLaborGroup] !=
				DELETED.[CustBSTLaborGroup]
			)
		) 
		END		
		
      If UPDATE([CustBSTExpGroup])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustBSTExpGroup',
      CONVERT(NVARCHAR(2000),DELETED.[CustBSTExpGroup],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBSTExpGroup],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustBSTExpGroup] Is Null And
				DELETED.[CustBSTExpGroup] Is Not Null
			) Or
			(
				INSERTED.[CustBSTExpGroup] Is Not Null And
				DELETED.[CustBSTExpGroup] Is Null
			) Or
			(
				INSERTED.[CustBSTExpGroup] !=
				DELETED.[CustBSTExpGroup]
			)
		) 
		END		
		
      If UPDATE([CustBSTAdminGroup])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustBSTAdminGroup',
      CONVERT(NVARCHAR(2000),DELETED.[CustBSTAdminGroup],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBSTAdminGroup],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustBSTAdminGroup] Is Null And
				DELETED.[CustBSTAdminGroup] Is Not Null
			) Or
			(
				INSERTED.[CustBSTAdminGroup] Is Not Null And
				DELETED.[CustBSTAdminGroup] Is Null
			) Or
			(
				INSERTED.[CustBSTAdminGroup] !=
				DELETED.[CustBSTAdminGroup]
			)
		) 
		END		
		
      If UPDATE([CustBSTLocation])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustBSTLocation',
      CONVERT(NVARCHAR(2000),DELETED.[CustBSTLocation],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBSTLocation],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustBSTLocation] Is Null And
				DELETED.[CustBSTLocation] Is Not Null
			) Or
			(
				INSERTED.[CustBSTLocation] Is Not Null And
				DELETED.[CustBSTLocation] Is Null
			) Or
			(
				INSERTED.[CustBSTLocation] !=
				DELETED.[CustBSTLocation]
			)
		) 
		END		
		
      If UPDATE([custBSTAltOrg2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'custBSTAltOrg2',
      CONVERT(NVARCHAR(2000),DELETED.[custBSTAltOrg2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custBSTAltOrg2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[custBSTAltOrg2] Is Null And
				DELETED.[custBSTAltOrg2] Is Not Null
			) Or
			(
				INSERTED.[custBSTAltOrg2] Is Not Null And
				DELETED.[custBSTAltOrg2] Is Null
			) Or
			(
				INSERTED.[custBSTAltOrg2] !=
				DELETED.[custBSTAltOrg2]
			)
		) 
		END		
		
      If UPDATE([CustAnnualSalary])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustAnnualSalary',
      CONVERT(NVARCHAR(2000),DELETED.[CustAnnualSalary],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustAnnualSalary],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustAnnualSalary] Is Null And
				DELETED.[CustAnnualSalary] Is Not Null
			) Or
			(
				INSERTED.[CustAnnualSalary] Is Not Null And
				DELETED.[CustAnnualSalary] Is Null
			) Or
			(
				INSERTED.[CustAnnualSalary] !=
				DELETED.[CustAnnualSalary]
			)
		) 
		END		
		
      If UPDATE([CustBiweeklySalary])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustBiweeklySalary',
      CONVERT(NVARCHAR(2000),DELETED.[CustBiweeklySalary],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBiweeklySalary],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustBiweeklySalary] Is Null And
				DELETED.[CustBiweeklySalary] Is Not Null
			) Or
			(
				INSERTED.[CustBiweeklySalary] Is Not Null And
				DELETED.[CustBiweeklySalary] Is Null
			) Or
			(
				INSERTED.[CustBiweeklySalary] !=
				DELETED.[CustBiweeklySalary]
			)
		) 
		END		
		
      If UPDATE([CustOperationsManager])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustOperationsManager',
      CONVERT(NVARCHAR(2000),DELETED.[CustOperationsManager],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustOperationsManager],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustOperationsManager] Is Null And
				DELETED.[CustOperationsManager] Is Not Null
			) Or
			(
				INSERTED.[CustOperationsManager] Is Not Null And
				DELETED.[CustOperationsManager] Is Null
			) Or
			(
				INSERTED.[CustOperationsManager] !=
				DELETED.[CustOperationsManager]
			)
		) 
		END		
		
     If UPDATE([CustEKApprover])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEKApprover',
     CONVERT(NVARCHAR(2000),DELETED.[CustEKApprover],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustEKApprover],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustEKApprover] Is Null And
				DELETED.[CustEKApprover] Is Not Null
			) Or
			(
				INSERTED.[CustEKApprover] Is Not Null And
				DELETED.[CustEKApprover] Is Null
			) Or
			(
				INSERTED.[CustEKApprover] !=
				DELETED.[CustEKApprover]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEKApprover = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEKApprover = newDesc.Employee
		END		
		
     If UPDATE([CustEKAlternateApprover])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEKAlternateApprover',
     CONVERT(NVARCHAR(2000),DELETED.[CustEKAlternateApprover],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustEKAlternateApprover],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustEKAlternateApprover] Is Null And
				DELETED.[CustEKAlternateApprover] Is Not Null
			) Or
			(
				INSERTED.[CustEKAlternateApprover] Is Not Null And
				DELETED.[CustEKAlternateApprover] Is Null
			) Or
			(
				INSERTED.[CustEKAlternateApprover] !=
				DELETED.[CustEKAlternateApprover]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEKAlternateApprover = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEKAlternateApprover = newDesc.Employee
		END		
		
      If UPDATE([CustWageType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustWageType',
      CONVERT(NVARCHAR(2000),DELETED.[CustWageType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustWageType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustWageType] Is Null And
				DELETED.[CustWageType] Is Not Null
			) Or
			(
				INSERTED.[CustWageType] Is Not Null And
				DELETED.[CustWageType] Is Null
			) Or
			(
				INSERTED.[CustWageType] !=
				DELETED.[CustWageType]
			)
		) 
		END		
		
     If UPDATE([CustEKOperationsManager])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEKOperationsManager',
     CONVERT(NVARCHAR(2000),DELETED.[CustEKOperationsManager],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustEKOperationsManager],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustEKOperationsManager] Is Null And
				DELETED.[CustEKOperationsManager] Is Not Null
			) Or
			(
				INSERTED.[CustEKOperationsManager] Is Not Null And
				DELETED.[CustEKOperationsManager] Is Null
			) Or
			(
				INSERTED.[CustEKOperationsManager] !=
				DELETED.[CustEKOperationsManager]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEKOperationsManager = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEKOperationsManager = newDesc.Employee
		END		
		
     If UPDATE([CustEKDirector])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEKDirector',
     CONVERT(NVARCHAR(2000),DELETED.[CustEKDirector],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustEKDirector],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustEKDirector] Is Null And
				DELETED.[CustEKDirector] Is Not Null
			) Or
			(
				INSERTED.[CustEKDirector] Is Not Null And
				DELETED.[CustEKDirector] Is Null
			) Or
			(
				INSERTED.[CustEKDirector] !=
				DELETED.[CustEKDirector]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEKDirector = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEKDirector = newDesc.Employee
		END		
		
     If UPDATE([CustEKManagementLeader])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEKManagementLeader',
     CONVERT(NVARCHAR(2000),DELETED.[CustEKManagementLeader],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustEKManagementLeader],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustEKManagementLeader] Is Null And
				DELETED.[CustEKManagementLeader] Is Not Null
			) Or
			(
				INSERTED.[CustEKManagementLeader] Is Not Null And
				DELETED.[CustEKManagementLeader] Is Null
			) Or
			(
				INSERTED.[CustEKManagementLeader] !=
				DELETED.[CustEKManagementLeader]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEKManagementLeader = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEKManagementLeader = newDesc.Employee
		END		
		
      If UPDATE([CustUnpaidLeaveStart])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustUnpaidLeaveStart',
      CONVERT(NVARCHAR(2000),DELETED.[CustUnpaidLeaveStart],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustUnpaidLeaveStart],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustUnpaidLeaveStart] Is Null And
				DELETED.[CustUnpaidLeaveStart] Is Not Null
			) Or
			(
				INSERTED.[CustUnpaidLeaveStart] Is Not Null And
				DELETED.[CustUnpaidLeaveStart] Is Null
			) Or
			(
				INSERTED.[CustUnpaidLeaveStart] !=
				DELETED.[CustUnpaidLeaveStart]
			)
		) 
		END		
		
      If UPDATE([CustUnpaidLeaveEnd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustUnpaidLeaveEnd',
      CONVERT(NVARCHAR(2000),DELETED.[CustUnpaidLeaveEnd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustUnpaidLeaveEnd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustUnpaidLeaveEnd] Is Null And
				DELETED.[CustUnpaidLeaveEnd] Is Not Null
			) Or
			(
				INSERTED.[CustUnpaidLeaveEnd] Is Not Null And
				DELETED.[CustUnpaidLeaveEnd] Is Null
			) Or
			(
				INSERTED.[CustUnpaidLeaveEnd] !=
				DELETED.[CustUnpaidLeaveEnd]
			)
		) 
		END		
		
      If UPDATE([CustTeamPracticeArea])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTeamPracticeArea',
      CONVERT(NVARCHAR(2000),DELETED.[CustTeamPracticeArea],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTeamPracticeArea],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustTeamPracticeArea] Is Null And
				DELETED.[CustTeamPracticeArea] Is Not Null
			) Or
			(
				INSERTED.[CustTeamPracticeArea] Is Not Null And
				DELETED.[CustTeamPracticeArea] Is Null
			) Or
			(
				INSERTED.[CustTeamPracticeArea] !=
				DELETED.[CustTeamPracticeArea]
			)
		) 
		END		
		
      If UPDATE([CustRoleManagementTeam])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleManagementTeam',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleManagementTeam],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleManagementTeam],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleManagementTeam] Is Null And
				DELETED.[CustRoleManagementTeam] Is Not Null
			) Or
			(
				INSERTED.[CustRoleManagementTeam] Is Not Null And
				DELETED.[CustRoleManagementTeam] Is Null
			) Or
			(
				INSERTED.[CustRoleManagementTeam] !=
				DELETED.[CustRoleManagementTeam]
			)
		) 
		END		
		
      If UPDATE([CustAdjHireDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustAdjHireDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustAdjHireDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustAdjHireDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustAdjHireDate] Is Null And
				DELETED.[CustAdjHireDate] Is Not Null
			) Or
			(
				INSERTED.[CustAdjHireDate] Is Not Null And
				DELETED.[CustAdjHireDate] Is Null
			) Or
			(
				INSERTED.[CustAdjHireDate] !=
				DELETED.[CustAdjHireDate]
			)
		) 
		END		
		
      If UPDATE([CustTrueHireDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTrueHireDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustTrueHireDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTrueHireDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustTrueHireDate] Is Null And
				DELETED.[CustTrueHireDate] Is Not Null
			) Or
			(
				INSERTED.[CustTrueHireDate] Is Not Null And
				DELETED.[CustTrueHireDate] Is Null
			) Or
			(
				INSERTED.[CustTrueHireDate] !=
				DELETED.[CustTrueHireDate]
			)
		) 
		END		
		
      If UPDATE([CustOldTarget])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustOldTarget',
      CONVERT(NVARCHAR(2000),DELETED.[CustOldTarget],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustOldTarget],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustOldTarget] Is Null And
				DELETED.[CustOldTarget] Is Not Null
			) Or
			(
				INSERTED.[CustOldTarget] Is Not Null And
				DELETED.[CustOldTarget] Is Null
			) Or
			(
				INSERTED.[CustOldTarget] !=
				DELETED.[CustOldTarget]
			)
		) 
		END		
		
      If UPDATE([CustTeamLeader])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTeamLeader',
      CONVERT(NVARCHAR(2000),DELETED.[CustTeamLeader],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTeamLeader],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustTeamLeader] Is Null And
				DELETED.[CustTeamLeader] Is Not Null
			) Or
			(
				INSERTED.[CustTeamLeader] Is Not Null And
				DELETED.[CustTeamLeader] Is Null
			) Or
			(
				INSERTED.[CustTeamLeader] !=
				DELETED.[CustTeamLeader]
			)
		) 
		END		
		
      If UPDATE([CustHireTermDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireTermDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustHireTermDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHireTermDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustHireTermDate] Is Null And
				DELETED.[CustHireTermDate] Is Not Null
			) Or
			(
				INSERTED.[CustHireTermDate] Is Not Null And
				DELETED.[CustHireTermDate] Is Null
			) Or
			(
				INSERTED.[CustHireTermDate] !=
				DELETED.[CustHireTermDate]
			)
		) 
		END		
		
     If UPDATE([CustHireMentor])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireMentor',
     CONVERT(NVARCHAR(2000),DELETED.[CustHireMentor],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHireMentor],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustHireMentor] Is Null And
				DELETED.[CustHireMentor] Is Not Null
			) Or
			(
				INSERTED.[CustHireMentor] Is Not Null And
				DELETED.[CustHireMentor] Is Null
			) Or
			(
				INSERTED.[CustHireMentor] !=
				DELETED.[CustHireMentor]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHireMentor = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHireMentor = newDesc.Employee
		END		
		
     If UPDATE([CustHireComputerSetup])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireComputerSetup',
     CONVERT(NVARCHAR(2000),DELETED.[CustHireComputerSetup],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHireComputerSetup],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustHireComputerSetup] Is Null And
				DELETED.[CustHireComputerSetup] Is Not Null
			) Or
			(
				INSERTED.[CustHireComputerSetup] Is Not Null And
				DELETED.[CustHireComputerSetup] Is Null
			) Or
			(
				INSERTED.[CustHireComputerSetup] !=
				DELETED.[CustHireComputerSetup]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHireComputerSetup = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHireComputerSetup = newDesc.Employee
		END		
		
      If UPDATE([CustHirePhoneRequired])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHirePhoneRequired',
      CONVERT(NVARCHAR(2000),DELETED.[CustHirePhoneRequired],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHirePhoneRequired],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustHirePhoneRequired] Is Null And
				DELETED.[CustHirePhoneRequired] Is Not Null
			) Or
			(
				INSERTED.[CustHirePhoneRequired] Is Not Null And
				DELETED.[CustHirePhoneRequired] Is Null
			) Or
			(
				INSERTED.[CustHirePhoneRequired] !=
				DELETED.[CustHirePhoneRequired]
			)
		) 
		END		
		
      If UPDATE([CustHireOfficeSpaceLoc])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireOfficeSpaceLoc',
      CONVERT(NVARCHAR(2000),DELETED.[CustHireOfficeSpaceLoc],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHireOfficeSpaceLoc],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustHireOfficeSpaceLoc] Is Null And
				DELETED.[CustHireOfficeSpaceLoc] Is Not Null
			) Or
			(
				INSERTED.[CustHireOfficeSpaceLoc] Is Not Null And
				DELETED.[CustHireOfficeSpaceLoc] Is Null
			) Or
			(
				INSERTED.[CustHireOfficeSpaceLoc] !=
				DELETED.[CustHireOfficeSpaceLoc]
			)
		) 
		END		
		
      If UPDATE([CustHireComments])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireComments',
      CONVERT(NVARCHAR(2000),DELETED.[CustHireComments],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHireComments],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustHireComments] Is Null And
				DELETED.[CustHireComments] Is Not Null
			) Or
			(
				INSERTED.[CustHireComments] Is Not Null And
				DELETED.[CustHireComments] Is Null
			) Or
			(
				INSERTED.[CustHireComments] !=
				DELETED.[CustHireComments]
			)
		) 
		END		
		
      If UPDATE([CustHireResponseComplete])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireResponseComplete',
      CONVERT(NVARCHAR(2000),DELETED.[CustHireResponseComplete],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHireResponseComplete],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustHireResponseComplete] Is Null And
				DELETED.[CustHireResponseComplete] Is Not Null
			) Or
			(
				INSERTED.[CustHireResponseComplete] Is Not Null And
				DELETED.[CustHireResponseComplete] Is Null
			) Or
			(
				INSERTED.[CustHireResponseComplete] !=
				DELETED.[CustHireResponseComplete]
			)
		) 
		END		
		
      If UPDATE([CustHireTermStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireTermStatus',
      CONVERT(NVARCHAR(2000),DELETED.[CustHireTermStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHireTermStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustHireTermStatus] Is Null And
				DELETED.[CustHireTermStatus] Is Not Null
			) Or
			(
				INSERTED.[CustHireTermStatus] Is Not Null And
				DELETED.[CustHireTermStatus] Is Null
			) Or
			(
				INSERTED.[CustHireTermStatus] !=
				DELETED.[CustHireTermStatus]
			)
		) 
		END		
		
     If UPDATE([CustHireITSetupEmployee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireITSetupEmployee',
     CONVERT(NVARCHAR(2000),DELETED.[CustHireITSetupEmployee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHireITSetupEmployee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustHireITSetupEmployee] Is Null And
				DELETED.[CustHireITSetupEmployee] Is Not Null
			) Or
			(
				INSERTED.[CustHireITSetupEmployee] Is Not Null And
				DELETED.[CustHireITSetupEmployee] Is Null
			) Or
			(
				INSERTED.[CustHireITSetupEmployee] !=
				DELETED.[CustHireITSetupEmployee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHireITSetupEmployee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHireITSetupEmployee = newDesc.Employee
		END		
		
      If UPDATE([CustHireITDomain])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireITDomain',
      CONVERT(NVARCHAR(2000),DELETED.[CustHireITDomain],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHireITDomain],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustHireITDomain] Is Null And
				DELETED.[CustHireITDomain] Is Not Null
			) Or
			(
				INSERTED.[CustHireITDomain] Is Not Null And
				DELETED.[CustHireITDomain] Is Null
			) Or
			(
				INSERTED.[CustHireITDomain] !=
				DELETED.[CustHireITDomain]
			)
		) 
		END		
		
      If UPDATE([CustHireITCompShip])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireITCompShip',
      CONVERT(NVARCHAR(2000),DELETED.[CustHireITCompShip],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHireITCompShip],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustHireITCompShip] Is Null And
				DELETED.[CustHireITCompShip] Is Not Null
			) Or
			(
				INSERTED.[CustHireITCompShip] Is Not Null And
				DELETED.[CustHireITCompShip] Is Null
			) Or
			(
				INSERTED.[CustHireITCompShip] !=
				DELETED.[CustHireITCompShip]
			)
		) 
		END		
		
      If UPDATE([CustHireITCompPlaced])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireITCompPlaced',
      CONVERT(NVARCHAR(2000),DELETED.[CustHireITCompPlaced],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHireITCompPlaced],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustHireITCompPlaced] Is Null And
				DELETED.[CustHireITCompPlaced] Is Not Null
			) Or
			(
				INSERTED.[CustHireITCompPlaced] Is Not Null And
				DELETED.[CustHireITCompPlaced] Is Null
			) Or
			(
				INSERTED.[CustHireITCompPlaced] !=
				DELETED.[CustHireITCompPlaced]
			)
		) 
		END		
		
      If UPDATE([CustHireITPhone])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireITPhone',
      CONVERT(NVARCHAR(2000),DELETED.[CustHireITPhone],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHireITPhone],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustHireITPhone] Is Null And
				DELETED.[CustHireITPhone] Is Not Null
			) Or
			(
				INSERTED.[CustHireITPhone] Is Not Null And
				DELETED.[CustHireITPhone] Is Null
			) Or
			(
				INSERTED.[CustHireITPhone] !=
				DELETED.[CustHireITPhone]
			)
		) 
		END		
		
      If UPDATE([CustHireITEmail])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireITEmail',
      CONVERT(NVARCHAR(2000),DELETED.[CustHireITEmail],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHireITEmail],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustHireITEmail] Is Null And
				DELETED.[CustHireITEmail] Is Not Null
			) Or
			(
				INSERTED.[CustHireITEmail] Is Not Null And
				DELETED.[CustHireITEmail] Is Null
			) Or
			(
				INSERTED.[CustHireITEmail] !=
				DELETED.[CustHireITEmail]
			)
		) 
		END		
		
      If UPDATE([CustHireITWelcome])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireITWelcome',
      CONVERT(NVARCHAR(2000),DELETED.[CustHireITWelcome],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHireITWelcome],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustHireITWelcome] Is Null And
				DELETED.[CustHireITWelcome] Is Not Null
			) Or
			(
				INSERTED.[CustHireITWelcome] Is Not Null And
				DELETED.[CustHireITWelcome] Is Null
			) Or
			(
				INSERTED.[CustHireITWelcome] !=
				DELETED.[CustHireITWelcome]
			)
		) 
		END		
		
      If UPDATE([CustHireITSetupComplete])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustHireITSetupComplete',
      CONVERT(NVARCHAR(2000),DELETED.[CustHireITSetupComplete],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHireITSetupComplete],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustHireITSetupComplete] Is Null And
				DELETED.[CustHireITSetupComplete] Is Not Null
			) Or
			(
				INSERTED.[CustHireITSetupComplete] Is Not Null And
				DELETED.[CustHireITSetupComplete] Is Null
			) Or
			(
				INSERTED.[CustHireITSetupComplete] !=
				DELETED.[CustHireITSetupComplete]
			)
		) 
		END		
		
      If UPDATE([CustTermReplacementPercent])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermReplacementPercent',
      CONVERT(NVARCHAR(2000),DELETED.[CustTermReplacementPercent],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTermReplacementPercent],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustTermReplacementPercent] Is Null And
				DELETED.[CustTermReplacementPercent] Is Not Null
			) Or
			(
				INSERTED.[CustTermReplacementPercent] Is Not Null And
				DELETED.[CustTermReplacementPercent] Is Null
			) Or
			(
				INSERTED.[CustTermReplacementPercent] !=
				DELETED.[CustTermReplacementPercent]
			)
		) 
		END		
		
      If UPDATE([CustTermReplacementDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermReplacementDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustTermReplacementDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTermReplacementDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustTermReplacementDate] Is Null And
				DELETED.[CustTermReplacementDate] Is Not Null
			) Or
			(
				INSERTED.[CustTermReplacementDate] Is Not Null And
				DELETED.[CustTermReplacementDate] Is Null
			) Or
			(
				INSERTED.[CustTermReplacementDate] !=
				DELETED.[CustTermReplacementDate]
			)
		) 
		END		
		
      If UPDATE([CustTermEmailMonitorReq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermEmailMonitorReq',
      CONVERT(NVARCHAR(2000),DELETED.[CustTermEmailMonitorReq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTermEmailMonitorReq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustTermEmailMonitorReq] Is Null And
				DELETED.[CustTermEmailMonitorReq] Is Not Null
			) Or
			(
				INSERTED.[CustTermEmailMonitorReq] Is Not Null And
				DELETED.[CustTermEmailMonitorReq] Is Null
			) Or
			(
				INSERTED.[CustTermEmailMonitorReq] !=
				DELETED.[CustTermEmailMonitorReq]
			)
		) 
		END		
		
     If UPDATE([CustTermEmailMonitorEmployee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermEmailMonitorEmployee',
     CONVERT(NVARCHAR(2000),DELETED.[CustTermEmailMonitorEmployee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustTermEmailMonitorEmployee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustTermEmailMonitorEmployee] Is Null And
				DELETED.[CustTermEmailMonitorEmployee] Is Not Null
			) Or
			(
				INSERTED.[CustTermEmailMonitorEmployee] Is Not Null And
				DELETED.[CustTermEmailMonitorEmployee] Is Null
			) Or
			(
				INSERTED.[CustTermEmailMonitorEmployee] !=
				DELETED.[CustTermEmailMonitorEmployee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustTermEmailMonitorEmployee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustTermEmailMonitorEmployee = newDesc.Employee
		END		
		
      If UPDATE([CustTermPhoneMonitorReq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermPhoneMonitorReq',
      CONVERT(NVARCHAR(2000),DELETED.[CustTermPhoneMonitorReq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTermPhoneMonitorReq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustTermPhoneMonitorReq] Is Null And
				DELETED.[CustTermPhoneMonitorReq] Is Not Null
			) Or
			(
				INSERTED.[CustTermPhoneMonitorReq] Is Not Null And
				DELETED.[CustTermPhoneMonitorReq] Is Null
			) Or
			(
				INSERTED.[CustTermPhoneMonitorReq] !=
				DELETED.[CustTermPhoneMonitorReq]
			)
		) 
		END		
		
     If UPDATE([CustTermPhoneMonitorEmployee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermPhoneMonitorEmployee',
     CONVERT(NVARCHAR(2000),DELETED.[CustTermPhoneMonitorEmployee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustTermPhoneMonitorEmployee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustTermPhoneMonitorEmployee] Is Null And
				DELETED.[CustTermPhoneMonitorEmployee] Is Not Null
			) Or
			(
				INSERTED.[CustTermPhoneMonitorEmployee] Is Not Null And
				DELETED.[CustTermPhoneMonitorEmployee] Is Null
			) Or
			(
				INSERTED.[CustTermPhoneMonitorEmployee] !=
				DELETED.[CustTermPhoneMonitorEmployee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustTermPhoneMonitorEmployee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustTermPhoneMonitorEmployee = newDesc.Employee
		END		
		
      If UPDATE([CustTermComments])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermComments',
      CONVERT(NVARCHAR(2000),DELETED.[CustTermComments],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTermComments],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustTermComments] Is Null And
				DELETED.[CustTermComments] Is Not Null
			) Or
			(
				INSERTED.[CustTermComments] Is Not Null And
				DELETED.[CustTermComments] Is Null
			) Or
			(
				INSERTED.[CustTermComments] !=
				DELETED.[CustTermComments]
			)
		) 
		END		
		
      If UPDATE([CustSupervisorTermResponseComplete])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustSupervisorTermResponseComplete',
      CONVERT(NVARCHAR(2000),DELETED.[CustSupervisorTermResponseComplete],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSupervisorTermResponseComplete],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustSupervisorTermResponseComplete] Is Null And
				DELETED.[CustSupervisorTermResponseComplete] Is Not Null
			) Or
			(
				INSERTED.[CustSupervisorTermResponseComplete] Is Not Null And
				DELETED.[CustSupervisorTermResponseComplete] Is Null
			) Or
			(
				INSERTED.[CustSupervisorTermResponseComplete] !=
				DELETED.[CustSupervisorTermResponseComplete]
			)
		) 
		END		
		
     If UPDATE([CustTermITTeardownEmployee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermITTeardownEmployee',
     CONVERT(NVARCHAR(2000),DELETED.[CustTermITTeardownEmployee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustTermITTeardownEmployee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustTermITTeardownEmployee] Is Null And
				DELETED.[CustTermITTeardownEmployee] Is Not Null
			) Or
			(
				INSERTED.[CustTermITTeardownEmployee] Is Not Null And
				DELETED.[CustTermITTeardownEmployee] Is Null
			) Or
			(
				INSERTED.[CustTermITTeardownEmployee] !=
				DELETED.[CustTermITTeardownEmployee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustTermITTeardownEmployee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustTermITTeardownEmployee = newDesc.Employee
		END		
		
      If UPDATE([CustTermITEmailPhone])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermITEmailPhone',
      CONVERT(NVARCHAR(2000),DELETED.[CustTermITEmailPhone],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTermITEmailPhone],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustTermITEmailPhone] Is Null And
				DELETED.[CustTermITEmailPhone] Is Not Null
			) Or
			(
				INSERTED.[CustTermITEmailPhone] Is Not Null And
				DELETED.[CustTermITEmailPhone] Is Null
			) Or
			(
				INSERTED.[CustTermITEmailPhone] !=
				DELETED.[CustTermITEmailPhone]
			)
		) 
		END		
		
      If UPDATE([CustTermITCompRecover])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermITCompRecover',
      CONVERT(NVARCHAR(2000),DELETED.[CustTermITCompRecover],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTermITCompRecover],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustTermITCompRecover] Is Null And
				DELETED.[CustTermITCompRecover] Is Not Null
			) Or
			(
				INSERTED.[CustTermITCompRecover] Is Not Null And
				DELETED.[CustTermITCompRecover] Is Null
			) Or
			(
				INSERTED.[CustTermITCompRecover] !=
				DELETED.[CustTermITCompRecover]
			)
		) 
		END		
		
      If UPDATE([CustTermITTeardownComplete])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustTermITTeardownComplete',
      CONVERT(NVARCHAR(2000),DELETED.[CustTermITTeardownComplete],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTermITTeardownComplete],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustTermITTeardownComplete] Is Null And
				DELETED.[CustTermITTeardownComplete] Is Not Null
			) Or
			(
				INSERTED.[CustTermITTeardownComplete] Is Not Null And
				DELETED.[CustTermITTeardownComplete] Is Null
			) Or
			(
				INSERTED.[CustTermITTeardownComplete] !=
				DELETED.[CustTermITTeardownComplete]
			)
		) 
		END		
		
      If UPDATE([CustCredentials])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustCredentials',
      CONVERT(NVARCHAR(2000),DELETED.[CustCredentials],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCredentials],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustCredentials] Is Null And
				DELETED.[CustCredentials] Is Not Null
			) Or
			(
				INSERTED.[CustCredentials] Is Not Null And
				DELETED.[CustCredentials] Is Null
			) Or
			(
				INSERTED.[CustCredentials] !=
				DELETED.[CustCredentials]
			)
		) 
		END		
		
      If UPDATE([CustPATBillerReview])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustPATBillerReview',
      CONVERT(NVARCHAR(2000),DELETED.[CustPATBillerReview],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPATBillerReview],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustPATBillerReview] Is Null And
				DELETED.[CustPATBillerReview] Is Not Null
			) Or
			(
				INSERTED.[CustPATBillerReview] Is Not Null And
				DELETED.[CustPATBillerReview] Is Null
			) Or
			(
				INSERTED.[CustPATBillerReview] !=
				DELETED.[CustPATBillerReview]
			)
		) 
		END		
		
      If UPDATE([CustEmployeeInfoVerified])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEmployeeInfoVerified',
      CONVERT(NVARCHAR(2000),DELETED.[CustEmployeeInfoVerified],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEmployeeInfoVerified],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustEmployeeInfoVerified] Is Null And
				DELETED.[CustEmployeeInfoVerified] Is Not Null
			) Or
			(
				INSERTED.[CustEmployeeInfoVerified] Is Not Null And
				DELETED.[CustEmployeeInfoVerified] Is Null
			) Or
			(
				INSERTED.[CustEmployeeInfoVerified] !=
				DELETED.[CustEmployeeInfoVerified]
			)
		) 
		END		
		
      If UPDATE([CustRoleBoardofDirectors])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleBoardofDirectors',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleBoardofDirectors],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleBoardofDirectors],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleBoardofDirectors] Is Null And
				DELETED.[CustRoleBoardofDirectors] Is Not Null
			) Or
			(
				INSERTED.[CustRoleBoardofDirectors] Is Not Null And
				DELETED.[CustRoleBoardofDirectors] Is Null
			) Or
			(
				INSERTED.[CustRoleBoardofDirectors] !=
				DELETED.[CustRoleBoardofDirectors]
			)
		) 
		END		
		
      If UPDATE([CustRoleExecutiveVicePresident])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleExecutiveVicePresident',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleExecutiveVicePresident],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleExecutiveVicePresident],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleExecutiveVicePresident] Is Null And
				DELETED.[CustRoleExecutiveVicePresident] Is Not Null
			) Or
			(
				INSERTED.[CustRoleExecutiveVicePresident] Is Not Null And
				DELETED.[CustRoleExecutiveVicePresident] Is Null
			) Or
			(
				INSERTED.[CustRoleExecutiveVicePresident] !=
				DELETED.[CustRoleExecutiveVicePresident]
			)
		) 
		END		
		
      If UPDATE([CustRoleVicePresident])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleVicePresident',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleVicePresident],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleVicePresident],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleVicePresident] Is Null And
				DELETED.[CustRoleVicePresident] Is Not Null
			) Or
			(
				INSERTED.[CustRoleVicePresident] Is Not Null And
				DELETED.[CustRoleVicePresident] Is Null
			) Or
			(
				INSERTED.[CustRoleVicePresident] !=
				DELETED.[CustRoleVicePresident]
			)
		) 
		END		
		
      If UPDATE([CustRolePracticeLeader])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRolePracticeLeader',
      CONVERT(NVARCHAR(2000),DELETED.[CustRolePracticeLeader],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRolePracticeLeader],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRolePracticeLeader] Is Null And
				DELETED.[CustRolePracticeLeader] Is Not Null
			) Or
			(
				INSERTED.[CustRolePracticeLeader] Is Not Null And
				DELETED.[CustRolePracticeLeader] Is Null
			) Or
			(
				INSERTED.[CustRolePracticeLeader] !=
				DELETED.[CustRolePracticeLeader]
			)
		) 
		END		
		
      If UPDATE([CustRoleDirector])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleDirector',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleDirector],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleDirector],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleDirector] Is Null And
				DELETED.[CustRoleDirector] Is Not Null
			) Or
			(
				INSERTED.[CustRoleDirector] Is Not Null And
				DELETED.[CustRoleDirector] Is Null
			) Or
			(
				INSERTED.[CustRoleDirector] !=
				DELETED.[CustRoleDirector]
			)
		) 
		END		
		
      If UPDATE([CustRoleAuthorizedAgent])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleAuthorizedAgent',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleAuthorizedAgent],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleAuthorizedAgent],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleAuthorizedAgent] Is Null And
				DELETED.[CustRoleAuthorizedAgent] Is Not Null
			) Or
			(
				INSERTED.[CustRoleAuthorizedAgent] Is Not Null And
				DELETED.[CustRoleAuthorizedAgent] Is Null
			) Or
			(
				INSERTED.[CustRoleAuthorizedAgent] !=
				DELETED.[CustRoleAuthorizedAgent]
			)
		) 
		END		
		
      If UPDATE([CustRoleProjectManager])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleProjectManager',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleProjectManager],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleProjectManager],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleProjectManager] Is Null And
				DELETED.[CustRoleProjectManager] Is Not Null
			) Or
			(
				INSERTED.[CustRoleProjectManager] Is Not Null And
				DELETED.[CustRoleProjectManager] Is Null
			) Or
			(
				INSERTED.[CustRoleProjectManager] !=
				DELETED.[CustRoleProjectManager]
			)
		) 
		END		
		
      If UPDATE([CustRoleStockholder])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleStockholder',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleStockholder],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleStockholder],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleStockholder] Is Null And
				DELETED.[CustRoleStockholder] Is Not Null
			) Or
			(
				INSERTED.[CustRoleStockholder] Is Not Null And
				DELETED.[CustRoleStockholder] Is Null
			) Or
			(
				INSERTED.[CustRoleStockholder] !=
				DELETED.[CustRoleStockholder]
			)
		) 
		END		
		
      If UPDATE([CustDriverLicense])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustDriverLicense',
      CONVERT(NVARCHAR(2000),DELETED.[CustDriverLicense],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustDriverLicense],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustDriverLicense] Is Null And
				DELETED.[CustDriverLicense] Is Not Null
			) Or
			(
				INSERTED.[CustDriverLicense] Is Not Null And
				DELETED.[CustDriverLicense] Is Null
			) Or
			(
				INSERTED.[CustDriverLicense] !=
				DELETED.[CustDriverLicense]
			)
		) 
		END		
		
      If UPDATE([CustDriverLicenseState])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustDriverLicenseState',
      CONVERT(NVARCHAR(2000),DELETED.[CustDriverLicenseState],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustDriverLicenseState],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustDriverLicenseState] Is Null And
				DELETED.[CustDriverLicenseState] Is Not Null
			) Or
			(
				INSERTED.[CustDriverLicenseState] Is Not Null And
				DELETED.[CustDriverLicenseState] Is Null
			) Or
			(
				INSERTED.[CustDriverLicenseState] !=
				DELETED.[CustDriverLicenseState]
			)
		) 
		END		
		
      If UPDATE([CustDiSCType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustDiSCType',
      CONVERT(NVARCHAR(2000),DELETED.[CustDiSCType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustDiSCType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustDiSCType] Is Null And
				DELETED.[CustDiSCType] Is Not Null
			) Or
			(
				INSERTED.[CustDiSCType] Is Not Null And
				DELETED.[CustDiSCType] Is Null
			) Or
			(
				INSERTED.[CustDiSCType] !=
				DELETED.[CustDiSCType]
			)
		) 
		END		
		
      If UPDATE([CustRoleSeniorVicePresident])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleSeniorVicePresident',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleSeniorVicePresident],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleSeniorVicePresident],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleSeniorVicePresident] Is Null And
				DELETED.[CustRoleSeniorVicePresident] Is Not Null
			) Or
			(
				INSERTED.[CustRoleSeniorVicePresident] Is Not Null And
				DELETED.[CustRoleSeniorVicePresident] Is Null
			) Or
			(
				INSERTED.[CustRoleSeniorVicePresident] !=
				DELETED.[CustRoleSeniorVicePresident]
			)
		) 
		END		
		
      If UPDATE([CustRolePresident])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRolePresident',
      CONVERT(NVARCHAR(2000),DELETED.[CustRolePresident],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRolePresident],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRolePresident] Is Null And
				DELETED.[CustRolePresident] Is Not Null
			) Or
			(
				INSERTED.[CustRolePresident] Is Not Null And
				DELETED.[CustRolePresident] Is Null
			) Or
			(
				INSERTED.[CustRolePresident] !=
				DELETED.[CustRolePresident]
			)
		) 
		END		
		
      If UPDATE([CustRoleChiefOperatingOfficer])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleChiefOperatingOfficer',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleChiefOperatingOfficer],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleChiefOperatingOfficer],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleChiefOperatingOfficer] Is Null And
				DELETED.[CustRoleChiefOperatingOfficer] Is Not Null
			) Or
			(
				INSERTED.[CustRoleChiefOperatingOfficer] Is Not Null And
				DELETED.[CustRoleChiefOperatingOfficer] Is Null
			) Or
			(
				INSERTED.[CustRoleChiefOperatingOfficer] !=
				DELETED.[CustRoleChiefOperatingOfficer]
			)
		) 
		END		
		
      If UPDATE([CustRoleChiefStrategyOfficer])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleChiefStrategyOfficer',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleChiefStrategyOfficer],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleChiefStrategyOfficer],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleChiefStrategyOfficer] Is Null And
				DELETED.[CustRoleChiefStrategyOfficer] Is Not Null
			) Or
			(
				INSERTED.[CustRoleChiefStrategyOfficer] Is Not Null And
				DELETED.[CustRoleChiefStrategyOfficer] Is Null
			) Or
			(
				INSERTED.[CustRoleChiefStrategyOfficer] !=
				DELETED.[CustRoleChiefStrategyOfficer]
			)
		) 
		END		
		
      If UPDATE([CustRoleViceChairmanoftheBoard])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleViceChairmanoftheBoard',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleViceChairmanoftheBoard],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleViceChairmanoftheBoard],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleViceChairmanoftheBoard] Is Null And
				DELETED.[CustRoleViceChairmanoftheBoard] Is Not Null
			) Or
			(
				INSERTED.[CustRoleViceChairmanoftheBoard] Is Not Null And
				DELETED.[CustRoleViceChairmanoftheBoard] Is Null
			) Or
			(
				INSERTED.[CustRoleViceChairmanoftheBoard] !=
				DELETED.[CustRoleViceChairmanoftheBoard]
			)
		) 
		END		
		
      If UPDATE([CustRoleChairmanoftheBoard])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleChairmanoftheBoard',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleChairmanoftheBoard],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleChairmanoftheBoard],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleChairmanoftheBoard] Is Null And
				DELETED.[CustRoleChairmanoftheBoard] Is Not Null
			) Or
			(
				INSERTED.[CustRoleChairmanoftheBoard] Is Not Null And
				DELETED.[CustRoleChairmanoftheBoard] Is Null
			) Or
			(
				INSERTED.[CustRoleChairmanoftheBoard] !=
				DELETED.[CustRoleChairmanoftheBoard]
			)
		) 
		END		
		
      If UPDATE([CustRoleCorporateSecretary])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleCorporateSecretary',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleCorporateSecretary],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleCorporateSecretary],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleCorporateSecretary] Is Null And
				DELETED.[CustRoleCorporateSecretary] Is Not Null
			) Or
			(
				INSERTED.[CustRoleCorporateSecretary] Is Not Null And
				DELETED.[CustRoleCorporateSecretary] Is Null
			) Or
			(
				INSERTED.[CustRoleCorporateSecretary] !=
				DELETED.[CustRoleCorporateSecretary]
			)
		) 
		END		
		
      If UPDATE([CustRoleDeputyCorporateSecretary])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleDeputyCorporateSecretary',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleDeputyCorporateSecretary],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleDeputyCorporateSecretary],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleDeputyCorporateSecretary] Is Null And
				DELETED.[CustRoleDeputyCorporateSecretary] Is Not Null
			) Or
			(
				INSERTED.[CustRoleDeputyCorporateSecretary] Is Not Null And
				DELETED.[CustRoleDeputyCorporateSecretary] Is Null
			) Or
			(
				INSERTED.[CustRoleDeputyCorporateSecretary] !=
				DELETED.[CustRoleDeputyCorporateSecretary]
			)
		) 
		END		
		
      If UPDATE([CustRoleChairmanEmeritus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleChairmanEmeritus',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleChairmanEmeritus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleChairmanEmeritus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleChairmanEmeritus] Is Null And
				DELETED.[CustRoleChairmanEmeritus] Is Not Null
			) Or
			(
				INSERTED.[CustRoleChairmanEmeritus] Is Not Null And
				DELETED.[CustRoleChairmanEmeritus] Is Null
			) Or
			(
				INSERTED.[CustRoleChairmanEmeritus] !=
				DELETED.[CustRoleChairmanEmeritus]
			)
		) 
		END		
		
      If UPDATE([CustRoleSecofRealEstateServices])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleSecofRealEstateServices',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleSecofRealEstateServices],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleSecofRealEstateServices],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleSecofRealEstateServices] Is Null And
				DELETED.[CustRoleSecofRealEstateServices] Is Not Null
			) Or
			(
				INSERTED.[CustRoleSecofRealEstateServices] Is Not Null And
				DELETED.[CustRoleSecofRealEstateServices] Is Null
			) Or
			(
				INSERTED.[CustRoleSecofRealEstateServices] !=
				DELETED.[CustRoleSecofRealEstateServices]
			)
		) 
		END		
		
      If UPDATE([CustRoleGeneralCounsel])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleGeneralCounsel',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleGeneralCounsel],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleGeneralCounsel],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleGeneralCounsel] Is Null And
				DELETED.[CustRoleGeneralCounsel] Is Not Null
			) Or
			(
				INSERTED.[CustRoleGeneralCounsel] Is Not Null And
				DELETED.[CustRoleGeneralCounsel] Is Null
			) Or
			(
				INSERTED.[CustRoleGeneralCounsel] !=
				DELETED.[CustRoleGeneralCounsel]
			)
		) 
		END		
		
      If UPDATE([CustExperienceCurrentThisFirm])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustExperienceCurrentThisFirm',
      CONVERT(NVARCHAR(2000),DELETED.[CustExperienceCurrentThisFirm],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustExperienceCurrentThisFirm],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustExperienceCurrentThisFirm] Is Null And
				DELETED.[CustExperienceCurrentThisFirm] Is Not Null
			) Or
			(
				INSERTED.[CustExperienceCurrentThisFirm] Is Not Null And
				DELETED.[CustExperienceCurrentThisFirm] Is Null
			) Or
			(
				INSERTED.[CustExperienceCurrentThisFirm] !=
				DELETED.[CustExperienceCurrentThisFirm]
			)
		) 
		END		
		
      If UPDATE([CustExperienceTotalAllFirm])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustExperienceTotalAllFirm',
      CONVERT(NVARCHAR(2000),DELETED.[CustExperienceTotalAllFirm],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustExperienceTotalAllFirm],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustExperienceTotalAllFirm] Is Null And
				DELETED.[CustExperienceTotalAllFirm] Is Not Null
			) Or
			(
				INSERTED.[CustExperienceTotalAllFirm] Is Not Null And
				DELETED.[CustExperienceTotalAllFirm] Is Null
			) Or
			(
				INSERTED.[CustExperienceTotalAllFirm] !=
				DELETED.[CustExperienceTotalAllFirm]
			)
		) 
		END		
		
      If UPDATE([CustExperienceTotalThisFirm])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustExperienceTotalThisFirm',
      CONVERT(NVARCHAR(2000),DELETED.[CustExperienceTotalThisFirm],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustExperienceTotalThisFirm],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustExperienceTotalThisFirm] Is Null And
				DELETED.[CustExperienceTotalThisFirm] Is Not Null
			) Or
			(
				INSERTED.[CustExperienceTotalThisFirm] Is Not Null And
				DELETED.[CustExperienceTotalThisFirm] Is Null
			) Or
			(
				INSERTED.[CustExperienceTotalThisFirm] !=
				DELETED.[CustExperienceTotalThisFirm]
			)
		) 
		END		
		
     If UPDATE([CustEKPracticeLeader])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEKPracticeLeader',
     CONVERT(NVARCHAR(2000),DELETED.[CustEKPracticeLeader],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustEKPracticeLeader],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustEKPracticeLeader] Is Null And
				DELETED.[CustEKPracticeLeader] Is Not Null
			) Or
			(
				INSERTED.[CustEKPracticeLeader] Is Not Null And
				DELETED.[CustEKPracticeLeader] Is Null
			) Or
			(
				INSERTED.[CustEKPracticeLeader] !=
				DELETED.[CustEKPracticeLeader]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEKPracticeLeader = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEKPracticeLeader = newDesc.Employee
		END		
		
      If UPDATE([CustRoleChiefFinancialOfficer])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleChiefFinancialOfficer',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleChiefFinancialOfficer],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleChiefFinancialOfficer],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleChiefFinancialOfficer] Is Null And
				DELETED.[CustRoleChiefFinancialOfficer] Is Not Null
			) Or
			(
				INSERTED.[CustRoleChiefFinancialOfficer] Is Not Null And
				DELETED.[CustRoleChiefFinancialOfficer] Is Null
			) Or
			(
				INSERTED.[CustRoleChiefFinancialOfficer] !=
				DELETED.[CustRoleChiefFinancialOfficer]
			)
		) 
		END		
		
      If UPDATE([CustEmpGender])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEmpGender',
      CONVERT(NVARCHAR(2000),DELETED.[CustEmpGender],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEmpGender],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustEmpGender] Is Null And
				DELETED.[CustEmpGender] Is Not Null
			) Or
			(
				INSERTED.[CustEmpGender] Is Not Null And
				DELETED.[CustEmpGender] Is Null
			) Or
			(
				INSERTED.[CustEmpGender] !=
				DELETED.[CustEmpGender]
			)
		) 
		END		
		
      If UPDATE([CustEmpEthnicity])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEmpEthnicity',
      CONVERT(NVARCHAR(2000),DELETED.[CustEmpEthnicity],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEmpEthnicity],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustEmpEthnicity] Is Null And
				DELETED.[CustEmpEthnicity] Is Not Null
			) Or
			(
				INSERTED.[CustEmpEthnicity] Is Not Null And
				DELETED.[CustEmpEthnicity] Is Null
			) Or
			(
				INSERTED.[CustEmpEthnicity] !=
				DELETED.[CustEmpEthnicity]
			)
		) 
		END		
		
     If UPDATE([CustEmp])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEmp',
     CONVERT(NVARCHAR(2000),DELETED.[CustEmp],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustEmp],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustEmp] Is Null And
				DELETED.[CustEmp] Is Not Null
			) Or
			(
				INSERTED.[CustEmp] Is Not Null And
				DELETED.[CustEmp] Is Null
			) Or
			(
				INSERTED.[CustEmp] !=
				DELETED.[CustEmp]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEmp = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEmp = newDesc.Employee
		END		
		
      If UPDATE([CustLegacySource])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustLegacySource',
      CONVERT(NVARCHAR(2000),DELETED.[CustLegacySource],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLegacySource],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustLegacySource] Is Null And
				DELETED.[CustLegacySource] Is Not Null
			) Or
			(
				INSERTED.[CustLegacySource] Is Not Null And
				DELETED.[CustLegacySource] Is Null
			) Or
			(
				INSERTED.[CustLegacySource] !=
				DELETED.[CustLegacySource]
			)
		) 
		END		
		
      If UPDATE([CustCorporateTitle])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustCorporateTitle',
      CONVERT(NVARCHAR(2000),DELETED.[CustCorporateTitle],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCorporateTitle],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustCorporateTitle] Is Null And
				DELETED.[CustCorporateTitle] Is Not Null
			) Or
			(
				INSERTED.[CustCorporateTitle] Is Not Null And
				DELETED.[CustCorporateTitle] Is Null
			) Or
			(
				INSERTED.[CustCorporateTitle] !=
				DELETED.[CustCorporateTitle]
			)
		) 
		END		
		
      If UPDATE([CustEmploymentType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEmploymentType',
      CONVERT(NVARCHAR(2000),DELETED.[CustEmploymentType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEmploymentType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustEmploymentType] Is Null And
				DELETED.[CustEmploymentType] Is Not Null
			) Or
			(
				INSERTED.[CustEmploymentType] Is Not Null And
				DELETED.[CustEmploymentType] Is Null
			) Or
			(
				INSERTED.[CustEmploymentType] !=
				DELETED.[CustEmploymentType]
			)
		) 
		END		
		
      If UPDATE([CustManagement])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustManagement',
      CONVERT(NVARCHAR(2000),DELETED.[CustManagement],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustManagement],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustManagement] Is Null And
				DELETED.[CustManagement] Is Not Null
			) Or
			(
				INSERTED.[CustManagement] Is Not Null And
				DELETED.[CustManagement] Is Null
			) Or
			(
				INSERTED.[CustManagement] !=
				DELETED.[CustManagement]
			)
		) 
		END		
		
     If UPDATE([CustEmployeeInfoVerifiedUser])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustEmployeeInfoVerifiedUser',
     CONVERT(NVARCHAR(2000),DELETED.[CustEmployeeInfoVerifiedUser],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustEmployeeInfoVerifiedUser],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustEmployeeInfoVerifiedUser] Is Null And
				DELETED.[CustEmployeeInfoVerifiedUser] Is Not Null
			) Or
			(
				INSERTED.[CustEmployeeInfoVerifiedUser] Is Not Null And
				DELETED.[CustEmployeeInfoVerifiedUser] Is Null
			) Or
			(
				INSERTED.[CustEmployeeInfoVerifiedUser] !=
				DELETED.[CustEmployeeInfoVerifiedUser]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEmployeeInfoVerifiedUser = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEmployeeInfoVerifiedUser = newDesc.Employee
		END		
		
      If UPDATE([CustCertificationAdd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustCertificationAdd',
      CONVERT(NVARCHAR(2000),DELETED.[CustCertificationAdd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCertificationAdd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustCertificationAdd] Is Null And
				DELETED.[CustCertificationAdd] Is Not Null
			) Or
			(
				INSERTED.[CustCertificationAdd] Is Not Null And
				DELETED.[CustCertificationAdd] Is Null
			) Or
			(
				INSERTED.[CustCertificationAdd] !=
				DELETED.[CustCertificationAdd]
			)
		) 
		END		
		
      If UPDATE([CustRolePracticeLeaderRegional])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRolePracticeLeaderRegional',
      CONVERT(NVARCHAR(2000),DELETED.[CustRolePracticeLeaderRegional],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRolePracticeLeaderRegional],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRolePracticeLeaderRegional] Is Null And
				DELETED.[CustRolePracticeLeaderRegional] Is Not Null
			) Or
			(
				INSERTED.[CustRolePracticeLeaderRegional] Is Not Null And
				DELETED.[CustRolePracticeLeaderRegional] Is Null
			) Or
			(
				INSERTED.[CustRolePracticeLeaderRegional] !=
				DELETED.[CustRolePracticeLeaderRegional]
			)
		) 
		END		
		
      If UPDATE([CustRoleESOPParticipant])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleESOPParticipant',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleESOPParticipant],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleESOPParticipant],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleESOPParticipant] Is Null And
				DELETED.[CustRoleESOPParticipant] Is Not Null
			) Or
			(
				INSERTED.[CustRoleESOPParticipant] Is Not Null And
				DELETED.[CustRoleESOPParticipant] Is Null
			) Or
			(
				INSERTED.[CustRoleESOPParticipant] !=
				DELETED.[CustRoleESOPParticipant]
			)
		) 
		END		
		
      If UPDATE([CustRoleChiefMarketingOfficer])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleChiefMarketingOfficer',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleChiefMarketingOfficer],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleChiefMarketingOfficer],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleChiefMarketingOfficer] Is Null And
				DELETED.[CustRoleChiefMarketingOfficer] Is Not Null
			) Or
			(
				INSERTED.[CustRoleChiefMarketingOfficer] Is Not Null And
				DELETED.[CustRoleChiefMarketingOfficer] Is Null
			) Or
			(
				INSERTED.[CustRoleChiefMarketingOfficer] !=
				DELETED.[CustRoleChiefMarketingOfficer]
			)
		) 
		END		
		
      If UPDATE([CustRoleOpsTeamMember])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustRoleOpsTeamMember',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleOpsTeamMember],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleOpsTeamMember],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustRoleOpsTeamMember] Is Null And
				DELETED.[CustRoleOpsTeamMember] Is Not Null
			) Or
			(
				INSERTED.[CustRoleOpsTeamMember] Is Not Null And
				DELETED.[CustRoleOpsTeamMember] Is Null
			) Or
			(
				INSERTED.[CustRoleOpsTeamMember] !=
				DELETED.[CustRoleOpsTeamMember]
			)
		) 
		END		
		
      If UPDATE([CustConflictofInterest])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CustConflictofInterest',
      CONVERT(NVARCHAR(2000),DELETED.[CustConflictofInterest],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustConflictofInterest],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CustConflictofInterest] Is Null And
				DELETED.[CustConflictofInterest] Is Not Null
			) Or
			(
				INSERTED.[CustConflictofInterest] Is Not Null And
				DELETED.[CustConflictofInterest] Is Null
			) Or
			(
				INSERTED.[CustConflictofInterest] !=
				DELETED.[CustConflictofInterest]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[EmployeeCustomTabFields] ADD CONSTRAINT [EmployeeCustomTabFieldsPK] PRIMARY KEY NONCLUSTERED ([Employee]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
