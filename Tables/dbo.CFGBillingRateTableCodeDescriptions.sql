CREATE TABLE [dbo].[CFGBillingRateTableCodeDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGBillingR__Seq__7BC792AB] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGBillingRateTableCodeDescriptions] ADD CONSTRAINT [CFGBillingRateTableCodeDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
