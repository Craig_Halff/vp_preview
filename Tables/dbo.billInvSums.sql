CREATE TABLE [dbo].[billInvSums]
(
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecdSeq] [int] NOT NULL CONSTRAINT [DF__billInvSu__RecdS__4768D905] DEFAULT ((0)),
[ArrayType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PreInvoice] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__billInvSu__PreIn__485CFD3E] DEFAULT ('N'),
[BillWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Section] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubSection] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BaseAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billInvSu__BaseA__49512177] DEFAULT ((0)),
[FinalAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billInvSu__Final__4A4545B0] DEFAULT ((0)),
[TaxCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FinalAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billInvSu__Final__4B3969E9] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[billInvSums] ADD CONSTRAINT [billInvSumsPK] PRIMARY KEY NONCLUSTERED ([Invoice], [MainWBS1], [RecdSeq], [ArrayType], [PreInvoice]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [udic_Collection_IDX4] ON [dbo].[billInvSums] ([ArrayType]) ON [PRIMARY]
GO
