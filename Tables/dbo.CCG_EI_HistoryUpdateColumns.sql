CREATE TABLE [dbo].[CCG_EI_HistoryUpdateColumns]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[ActionTaken] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionDate] [datetime] NOT NULL,
[ActionTakenBy] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConfigCustomColumn] [int] NULL,
[ColumnLabel] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldValue] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewValue] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldValueDecimal] [decimal] (19, 5) NULL,
[NewValueDecimal] [decimal] (19, 5) NULL,
[BTFSeq] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_HistoryUpdateColumns] ADD CONSTRAINT [PK_CCG_EI_HistoryUpdateColumns] PRIMARY KEY CLUSTERED ([Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CCG_EI_HistoryUpdateColumns_WBS1_IDX] ON [dbo].[CCG_EI_HistoryUpdateColumns] ([WBS1]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Records history of input column and PCT complete updates', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_HistoryUpdateColumns', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date of the action', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_HistoryUpdateColumns', 'COLUMN', N'ActionDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The type of action done on this column', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_HistoryUpdateColumns', 'COLUMN', N'ActionTaken'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The Vision employee who did the action', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_HistoryUpdateColumns', 'COLUMN', N'ActionTakenBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'For PCT Complete updates, Visions BTF.seq field (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_HistoryUpdateColumns', 'COLUMN', N'BTFSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The name of the column modified', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_HistoryUpdateColumns', 'COLUMN', N'ColumnLabel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'(Optional) The custom column id (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_HistoryUpdateColumns', 'COLUMN', N'ConfigCustomColumn'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The type of data in the column (A = alphanumeric / C = currency / N = numeric / D = date / I = integer / P = percent / L = link / V = dropdown / T = time)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_HistoryUpdateColumns', 'COLUMN', N'DataType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The new alphanumeric value of the column after the action', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_HistoryUpdateColumns', 'COLUMN', N'NewValue'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The new value of the column after the action (for numeric values only)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_HistoryUpdateColumns', 'COLUMN', N'NewValueDecimal'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Original alphanumeric value of the column before this action', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_HistoryUpdateColumns', 'COLUMN', N'OldValue'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Original value of the column before this action (for numeric values only)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_HistoryUpdateColumns', 'COLUMN', N'OldValueDecimal'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_HistoryUpdateColumns', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The WBS1 number (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_HistoryUpdateColumns', 'COLUMN', N'WBS1'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The WBS2 number (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_HistoryUpdateColumns', 'COLUMN', N'WBS2'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The WBS3 number (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_HistoryUpdateColumns', 'COLUMN', N'WBS3'
GO
