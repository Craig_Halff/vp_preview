CREATE TABLE [dbo].[CFGBankTextFormat]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__CFGBankText__Seq__69A8E270] DEFAULT ((0)),
[FieldLocation] [smallint] NOT NULL CONSTRAINT [DF__CFGBankTe__Field__6A9D06A9] DEFAULT ((1)),
[Expression] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LineType] [smallint] NOT NULL CONSTRAINT [DF__CFGBankTe__LineT__6B912AE2] DEFAULT ((1)),
[FieldNumber] [int] NOT NULL CONSTRAINT [DF__CFGBankTe__Field__6C854F1B] DEFAULT ((0)),
[Length] [int] NOT NULL CONSTRAINT [DF__CFGBankTe__Lengt__6D797354] DEFAULT ((0)),
[Start] [int] NOT NULL CONSTRAINT [DF__CFGBankTe__Start__6E6D978D] DEFAULT ((0)),
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calculation] [smallint] NOT NULL CONSTRAINT [DF__CFGBankTe__Calcu__6F61BBC6] DEFAULT ((0)),
[CalcLineType] [smallint] NOT NULL CONSTRAINT [DF__CFGBankTe__CalcL__7055DFFF] DEFAULT ((0)),
[CalcFieldNumber] [int] NOT NULL CONSTRAINT [DF__CFGBankTe__CalcF__714A0438] DEFAULT ((0)),
[PadCharacter] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Justify] [smallint] NOT NULL CONSTRAINT [DF__CFGBankTe__Justi__723E2871] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGBankTextFormat] ADD CONSTRAINT [CFGBankTextFormatPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
