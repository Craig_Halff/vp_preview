CREATE TABLE [dbo].[VisionSystemAnalysisLog]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LogType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[App] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppSection] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__VisionSyste__Seq__7BE924EA] DEFAULT ((0)),
[Severity] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VisionSystemAnalysisLog] ADD CONSTRAINT [VisionSystemAnalysisLogPK] PRIMARY KEY NONCLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
