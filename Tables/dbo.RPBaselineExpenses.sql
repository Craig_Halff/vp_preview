CREATE TABLE [dbo].[RPBaselineExpenses]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExpenseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodCount] [int] NOT NULL CONSTRAINT [DF__RPBaselin__Perio__232D0F3D] DEFAULT ((1)),
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPBaselin__Perio__24213376] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPBaselin__Perio__251557AF] DEFAULT ((0)),
[PeriodRev] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPBaselin__Perio__26097BE8] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPBaselineExpenses] ADD CONSTRAINT [RPBaselineExpensesPK] PRIMARY KEY NONCLUSTERED ([TimePhaseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPBaselineExpensesAllIDX] ON [dbo].[RPBaselineExpenses] ([PlanID], [TaskID], [ExpenseID], [StartDate], [EndDate], [PeriodCost], [PeriodBill]) ON [PRIMARY]
GO
