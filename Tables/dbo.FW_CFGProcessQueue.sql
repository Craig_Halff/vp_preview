CREATE TABLE [dbo].[FW_CFGProcessQueue]
(
[QueueID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[QueueName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QueueStatus] [smallint] NOT NULL CONSTRAINT [DF__FW_CFGPro__Queue__037E8E63] DEFAULT ((0)),
[Priority] [smallint] NOT NULL CONSTRAINT [DF__FW_CFGPro__Prior__0472B29C] DEFAULT ((0)),
[ProcessServerMachineName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaxRunningThreads] [int] NOT NULL CONSTRAINT [DF__FW_CFGPro__MaxRu__0566D6D5] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGProcessQueue] ADD CONSTRAINT [FW_CFGProcessQueuePK] PRIMARY KEY CLUSTERED ([QueueID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
