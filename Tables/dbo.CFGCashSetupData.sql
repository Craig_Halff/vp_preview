CREATE TABLE [dbo].[CFGCashSetupData]
(
[Code] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportColumn] [smallint] NOT NULL CONSTRAINT [DF__CFGCashSe__Repor__4FB40643] DEFAULT ((0)),
[SystemAccounts] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCashSetupData] ADD CONSTRAINT [CFGCashSetupDATAPK] PRIMARY KEY NONCLUSTERED ([Code], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
