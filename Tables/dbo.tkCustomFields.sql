CREATE TABLE [dbo].[tkCustomFields]
(
[EndDate] [datetime] NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__tkCustomFie__Seq__2323B8F3] DEFAULT ((0)),
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustTKUD01] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTKUD02] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__tkCustomF__CustT__2417DD2C] DEFAULT ((0)),
[CustTKUD03] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tkCustomFields] ADD CONSTRAINT [tkCustomFieldsPK] PRIMARY KEY CLUSTERED ([EndDate], [Employee], [Seq], [EmployeeCompany]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
