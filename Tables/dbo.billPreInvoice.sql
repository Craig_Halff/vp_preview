CREATE TABLE [dbo].[billPreInvoice]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PreInvoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AppliedAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billPreIn__Appli__706AEE98] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[billPreInvoice] ADD CONSTRAINT [billPreInvoicePK] PRIMARY KEY NONCLUSTERED ([WBS1], [Invoice], [PreInvoice]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
