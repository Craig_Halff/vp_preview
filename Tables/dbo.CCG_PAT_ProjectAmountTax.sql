CREATE TABLE [dbo].[CCG_PAT_ProjectAmountTax]
(
[PayableSeq] [int] NOT NULL,
[PKey] [varchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxAmount] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__CCG_PAT_P__TaxAm__1314A8C3] DEFAULT ((0)),
[Seq] [int] NOT NULL CONSTRAINT [DF__CCG_PAT_Pro__Seq__1408CCFC] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_ProjectAmountTax] ADD CONSTRAINT [PK_CCG_PAT_ProjectAmountTax] PRIMARY KEY CLUSTERED ([PayableSeq], [PKey], [TaxCode]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CCG_PAT_ProjectAmountTax_PayableSeq_IDX] ON [dbo].[CCG_PAT_ProjectAmountTax] ([PayableSeq]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stores tax information for each project amount record', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmountTax', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - Seq number of the payable item to which this record belongs', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmountTax', 'COLUMN', N'PayableSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - Pkey of the project amount to which this record belongs', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmountTax', 'COLUMN', N'PKey'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Order of the tax codes', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmountTax', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Amount for this tax code entry', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmountTax', 'COLUMN', N'TaxAmount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*FK - Vision Tax Code', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ProjectAmountTax', 'COLUMN', N'TaxCode'
GO
