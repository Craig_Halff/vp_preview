CREATE TABLE [dbo].[CFGActivityTypeDescriptions]
(
[Code] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActivityType] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGActivity__Seq__736756D4] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGActivityTypeDescriptions] ADD CONSTRAINT [CFGActivityTypeDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
