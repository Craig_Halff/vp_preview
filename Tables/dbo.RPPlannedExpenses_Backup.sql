CREATE TABLE [dbo].[RPPlannedExpenses_Backup]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExpenseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodCount] [int] NOT NULL,
[PeriodCost] [decimal] (19, 4) NOT NULL,
[PeriodBill] [decimal] (19, 4) NOT NULL,
[PeriodRev] [decimal] (19, 4) NOT NULL,
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
