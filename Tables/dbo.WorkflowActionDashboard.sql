CREATE TABLE [dbo].[WorkflowActionDashboard]
(
[ActionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ToListUsers] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToListRoles] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToListSpecial] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToListSpecialDisplay] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SummaryLink] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DashboardSummary] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DashboardContent] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NotificationAlertType] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowActionDashboard] ADD CONSTRAINT [WorkflowActionDashboardPK] PRIMARY KEY NONCLUSTERED ([ActionID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
