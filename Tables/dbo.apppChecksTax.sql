CREATE TABLE [dbo].[apppChecksTax]
(
[Period] [int] NOT NULL CONSTRAINT [DF__apppCheck__Perio__1CE88F54] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__apppCheck__PostS__1DDCB38D] DEFAULT ((0)),
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CheckSeq] [int] NOT NULL CONSTRAINT [DF__apppCheck__Check__1ED0D7C6] DEFAULT ((0)),
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxPayment] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apppCheck__TaxPa__1FC4FBFF] DEFAULT ((0)),
[TaxDiscount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apppCheck__TaxDi__20B92038] DEFAULT ((0)),
[CompoundOnTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__apppChecksT__Seq__21AD4471] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[apppChecksTax] ADD CONSTRAINT [apppChecksTaxPK] PRIMARY KEY NONCLUSTERED ([Period], [PostSeq], [Vendor], [CheckSeq], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
