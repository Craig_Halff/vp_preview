CREATE TABLE [dbo].[CCG_EI_ConfigRightsDescriptions]
(
[StageFlow] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Role] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Stage] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmailSubject] [nvarchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailSubjectBatch] [nvarchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_ConfigRightsDescriptions] ADD CONSTRAINT [PK_CCG_EI_ConfigRightsDescriptions] PRIMARY KEY CLUSTERED ([StageFlow], [Role], [Stage], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Contains descriptions for Config Rights as used for multi-language companies', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRightsDescriptions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The body of the notification email for this particular culture code', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRightsDescriptions', 'COLUMN', N'EmailMessage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The subject line of the notification email for this particular culture code', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRightsDescriptions', 'COLUMN', N'EmailSubject'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The subject line of the notification email when batched for this particular culture code', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRightsDescriptions', 'COLUMN', N'EmailSubjectBatch'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*EI Role Key (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRightsDescriptions', 'COLUMN', N'Role'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stage (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRightsDescriptions', 'COLUMN', N'Stage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The Stage Flow (FK) for this rights record (not multilanguage)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRightsDescriptions', 'COLUMN', N'StageFlow'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The UI culture code of this label', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRightsDescriptions', 'COLUMN', N'UICultureName'
GO
