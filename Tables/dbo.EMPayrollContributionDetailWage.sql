CREATE TABLE [dbo].[EMPayrollContributionDetailWage]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__EMPayroll__Perio__7DA5B97C] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__EMPayroll__PostS__7E99DDB5] DEFAULT ((0)),
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodeWage] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Amoun__7F8E01EE] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EMPayrollContributionDetailWage] ADD CONSTRAINT [EMPayrollContributionDetailWagePK] PRIMARY KEY NONCLUSTERED ([Employee], [Period], [PostSeq], [Code], [CodeWage]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
