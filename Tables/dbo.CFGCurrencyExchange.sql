CREATE TABLE [dbo].[CFGCurrencyExchange]
(
[FromCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ToCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EffectiveDate] [datetime] NULL,
[Rate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__CFGCurrenc__Rate__1CF37C4C] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Source] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_CFGCurrencyExchange]
      ON [dbo].[CFGCurrencyExchange]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CFGCurrencyExchange'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[RateID],121),'FromCode',CONVERT(NVARCHAR(2000),[FromCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[RateID],121),'ToCode',CONVERT(NVARCHAR(2000),[ToCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[RateID],121),'RateID',CONVERT(NVARCHAR(2000),[RateID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[RateID],121),'EffectiveDate',CONVERT(NVARCHAR(2000),[EffectiveDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[RateID],121),'Rate',CONVERT(NVARCHAR(2000),[Rate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[RateID],121),'StartDate',CONVERT(NVARCHAR(2000),[StartDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[RateID],121),'EndDate',CONVERT(NVARCHAR(2000),[EndDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[RateID],121),'Source',CONVERT(NVARCHAR(2000),[Source],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_CFGCurrencyExchange]
      ON [dbo].[CFGCurrencyExchange]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CFGCurrencyExchange'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RateID],121),'FromCode',NULL,CONVERT(NVARCHAR(2000),[FromCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RateID],121),'ToCode',NULL,CONVERT(NVARCHAR(2000),[ToCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RateID],121),'RateID',NULL,CONVERT(NVARCHAR(2000),[RateID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RateID],121),'EffectiveDate',NULL,CONVERT(NVARCHAR(2000),[EffectiveDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RateID],121),'Rate',NULL,CONVERT(NVARCHAR(2000),[Rate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RateID],121),'StartDate',NULL,CONVERT(NVARCHAR(2000),[StartDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RateID],121),'EndDate',NULL,CONVERT(NVARCHAR(2000),[EndDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RateID],121),'Source',NULL,CONVERT(NVARCHAR(2000),[Source],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_CFGCurrencyExchange]
      ON [dbo].[CFGCurrencyExchange]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CFGCurrencyExchange'
    
      If UPDATE([FromCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RateID],121),'FromCode',
      CONVERT(NVARCHAR(2000),DELETED.[FromCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FromCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[FromCode] = DELETED.[FromCode] AND INSERTED.[ToCode] = DELETED.[ToCode] AND INSERTED.[RateID] = DELETED.[RateID] AND 
		(
			(
				INSERTED.[FromCode] Is Null And
				DELETED.[FromCode] Is Not Null
			) Or
			(
				INSERTED.[FromCode] Is Not Null And
				DELETED.[FromCode] Is Null
			) Or
			(
				INSERTED.[FromCode] !=
				DELETED.[FromCode]
			)
		) 
		END		
		
      If UPDATE([ToCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RateID],121),'ToCode',
      CONVERT(NVARCHAR(2000),DELETED.[ToCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ToCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[FromCode] = DELETED.[FromCode] AND INSERTED.[ToCode] = DELETED.[ToCode] AND INSERTED.[RateID] = DELETED.[RateID] AND 
		(
			(
				INSERTED.[ToCode] Is Null And
				DELETED.[ToCode] Is Not Null
			) Or
			(
				INSERTED.[ToCode] Is Not Null And
				DELETED.[ToCode] Is Null
			) Or
			(
				INSERTED.[ToCode] !=
				DELETED.[ToCode]
			)
		) 
		END		
		
      If UPDATE([RateID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RateID],121),'RateID',
      CONVERT(NVARCHAR(2000),DELETED.[RateID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RateID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[FromCode] = DELETED.[FromCode] AND INSERTED.[ToCode] = DELETED.[ToCode] AND INSERTED.[RateID] = DELETED.[RateID] AND 
		(
			(
				INSERTED.[RateID] Is Null And
				DELETED.[RateID] Is Not Null
			) Or
			(
				INSERTED.[RateID] Is Not Null And
				DELETED.[RateID] Is Null
			) Or
			(
				INSERTED.[RateID] !=
				DELETED.[RateID]
			)
		) 
		END		
		
      If UPDATE([EffectiveDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RateID],121),'EffectiveDate',
      CONVERT(NVARCHAR(2000),DELETED.[EffectiveDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EffectiveDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[FromCode] = DELETED.[FromCode] AND INSERTED.[ToCode] = DELETED.[ToCode] AND INSERTED.[RateID] = DELETED.[RateID] AND 
		(
			(
				INSERTED.[EffectiveDate] Is Null And
				DELETED.[EffectiveDate] Is Not Null
			) Or
			(
				INSERTED.[EffectiveDate] Is Not Null And
				DELETED.[EffectiveDate] Is Null
			) Or
			(
				INSERTED.[EffectiveDate] !=
				DELETED.[EffectiveDate]
			)
		) 
		END		
		
      If UPDATE([Rate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RateID],121),'Rate',
      CONVERT(NVARCHAR(2000),DELETED.[Rate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Rate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[FromCode] = DELETED.[FromCode] AND INSERTED.[ToCode] = DELETED.[ToCode] AND INSERTED.[RateID] = DELETED.[RateID] AND 
		(
			(
				INSERTED.[Rate] Is Null And
				DELETED.[Rate] Is Not Null
			) Or
			(
				INSERTED.[Rate] Is Not Null And
				DELETED.[Rate] Is Null
			) Or
			(
				INSERTED.[Rate] !=
				DELETED.[Rate]
			)
		) 
		END		
		
      If UPDATE([StartDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RateID],121),'StartDate',
      CONVERT(NVARCHAR(2000),DELETED.[StartDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[StartDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[FromCode] = DELETED.[FromCode] AND INSERTED.[ToCode] = DELETED.[ToCode] AND INSERTED.[RateID] = DELETED.[RateID] AND 
		(
			(
				INSERTED.[StartDate] Is Null And
				DELETED.[StartDate] Is Not Null
			) Or
			(
				INSERTED.[StartDate] Is Not Null And
				DELETED.[StartDate] Is Null
			) Or
			(
				INSERTED.[StartDate] !=
				DELETED.[StartDate]
			)
		) 
		END		
		
      If UPDATE([EndDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RateID],121),'EndDate',
      CONVERT(NVARCHAR(2000),DELETED.[EndDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EndDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[FromCode] = DELETED.[FromCode] AND INSERTED.[ToCode] = DELETED.[ToCode] AND INSERTED.[RateID] = DELETED.[RateID] AND 
		(
			(
				INSERTED.[EndDate] Is Null And
				DELETED.[EndDate] Is Not Null
			) Or
			(
				INSERTED.[EndDate] Is Not Null And
				DELETED.[EndDate] Is Null
			) Or
			(
				INSERTED.[EndDate] !=
				DELETED.[EndDate]
			)
		) 
		END		
		
      If UPDATE([Source])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RateID],121),'Source',
      CONVERT(NVARCHAR(2000),DELETED.[Source],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Source],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[FromCode] = DELETED.[FromCode] AND INSERTED.[ToCode] = DELETED.[ToCode] AND INSERTED.[RateID] = DELETED.[RateID] AND 
		(
			(
				INSERTED.[Source] Is Null And
				DELETED.[Source] Is Not Null
			) Or
			(
				INSERTED.[Source] Is Not Null And
				DELETED.[Source] Is Null
			) Or
			(
				INSERTED.[Source] !=
				DELETED.[Source]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[CFGCurrencyExchange] ADD CONSTRAINT [CFGCurrencyExchangePK] PRIMARY KEY CLUSTERED ([FromCode], [ToCode], [RateID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CFGCurrencyExchangeEffectiveDateIDX] ON [dbo].[CFGCurrencyExchange] ([FromCode], [ToCode], [EffectiveDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CFGCurrencyExchangeByDateIDX] ON [dbo].[CFGCurrencyExchange] ([FromCode], [ToCode], [StartDate], [EndDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
