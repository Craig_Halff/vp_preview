CREATE TABLE [dbo].[CFGApprovalType]
(
[ItemType_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ApplicationID] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovalType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultDueDate] [smallint] NOT NULL CONSTRAINT [DF__CFGApprov__Defau__0E1B4D10] DEFAULT ((0)),
[ProjectRoutingBasis] [smallint] NOT NULL CONSTRAINT [DF__CFGApprov__Proje__0F0F7149] DEFAULT ((1)),
[ApprovalAdmin] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowApproverReassign] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGApprov__Allow__10039582] DEFAULT ('N'),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGApprov__Statu__10F7B9BB] DEFAULT ('A'),
[AllowReopen] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGApprov__Allow__11EBDDF4] DEFAULT ('N'),
[EmployeeReopen] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UseProcessServer] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGApprov__UsePr__12E0022D] DEFAULT ('Y'),
[ProcessServerMinimum] [smallint] NOT NULL CONSTRAINT [DF__CFGApprov__Proce__13D42666] DEFAULT ((0)),
[AutoApprove] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGApprov__AutoA__14C84A9F] DEFAULT ('N'),
[AllowUnSubmit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGApprov__Allow__15BC6ED8] DEFAULT ('N'),
[ConcurrentApprovals] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGApprov__Concu__16B09311] DEFAULT ('N'),
[AutoApproveDetailLines] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGApprov__AutoA__17A4B74A] DEFAULT ('N'),
[ApprovalLevel] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGApprov__Appro__1898DB83] DEFAULT ('Master'),
[TKAutoApprove] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGApprov__TKAut__45AEA678] DEFAULT ('Y')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGApprovalType] ADD CONSTRAINT [CFGApprovalTypePK] PRIMARY KEY CLUSTERED ([ItemType_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
