CREATE TABLE [dbo].[PRRevenueTemplate]
(
[RevAllocID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevenueDate] [datetime] NULL,
[RevenueAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRRevenue__Reven__54F01EB1] DEFAULT ((0)),
[PercentRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRRevenueTemplate__PercentRevenue] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PRRevenueTemplate] ADD CONSTRAINT [PRRevenueTemplatePK] PRIMARY KEY NONCLUSTERED ([RevAllocID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
