CREATE TABLE [dbo].[VisionImportUDIC_FleetManagement_VehiclesImport20201027]
(
[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustUnit] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustMake] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustModel] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustYear] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustColor] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLicensePlate] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLicensePlateState] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UDIC_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustActive] [datetime] NULL,
[CustAsset] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustComputed] [decimal] (19, 5) NULL,
[CustComputedThruDate] [datetime] NULL,
[CustDisposed] [datetime] NULL,
[CustInactive] [datetime] NULL,
[CustomCurrencyCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustOrganization] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustResponsibleParty] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustStart] [decimal] (19, 5) NULL,
[CustStartDate] [datetime] NULL,
[CustStatus] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustSupervisor] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustVIN] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
