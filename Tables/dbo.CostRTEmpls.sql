CREATE TABLE [dbo].[CostRTEmpls]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__CostRTEmp__Table__4ECACF02] DEFAULT ((0)),
[RateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EffectiveDate] [datetime] NULL,
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CostRTEmpl__Rate__4FBEF33B] DEFAULT ((0)),
[OvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CostRTEmp__OvtPc__50B31774] DEFAULT ((0)),
[Ovt2Pct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CostRTEmp__Ovt2P__51A73BAD] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CostRTEmpls] ADD CONSTRAINT [CostRTEmplsPK] PRIMARY KEY NONCLUSTERED ([TableNo], [RateID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CostRTEmplsEmployeeIDX] ON [dbo].[CostRTEmpls] ([TableNo], [Employee]) ON [PRIMARY]
GO
