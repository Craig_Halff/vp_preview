CREATE TABLE [dbo].[RevenueImport-202003]
(
[WBS1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustRevenueLabor] [decimal] (18, 10) NOT NULL,
[CustRevenueConsultant] [decimal] (18, 10) NOT NULL,
[CustRevenueExpense] [decimal] (18, 10) NOT NULL
) ON [PRIMARY]
GO
