CREATE TABLE [dbo].[BTRCTCats]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__BTRCTCats__Table__7C31A6FF] DEFAULT ((0)),
[RateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Category] [smallint] NOT NULL CONSTRAINT [DF__BTRCTCats__Categ__7D25CB38] DEFAULT ((0)),
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BTRCTCats___Rate__7E19EF71] DEFAULT ((0)),
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortSeq] [smallint] NOT NULL CONSTRAINT [DF__BTRCTCats__SortS__7F0E13AA] DEFAULT ((0)),
[EffectiveDate] [datetime] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTRCTCats] ADD CONSTRAINT [BTRCTCatsPK] PRIMARY KEY NONCLUSTERED ([TableNo], [RateID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [BTRCTCatsCategoryIDX] ON [dbo].[BTRCTCats] ([TableNo], [Category]) ON [PRIMARY]
GO
