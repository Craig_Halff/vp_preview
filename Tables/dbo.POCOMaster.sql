CREATE TABLE [dbo].[POCOMaster]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PONumber] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CONumber] [int] NOT NULL CONSTRAINT [DF__POCOMaste__CONum__1AC1E66E] DEFAULT ((0)),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POCOMaste__Statu__1BB60AA7] DEFAULT ('N'),
[OrderDate] [datetime] NULL,
[Closed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POCOMaste__Close__1CAA2EE0] DEFAULT ('N'),
[Reason] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthorizedDate] [datetime] NULL,
[SubmittedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedDate] [datetime] NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[Buyer] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NotToExceed] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCOMaste__NotTo__1D9E5319] DEFAULT ((0)),
[FinalPrintDate] [datetime] NULL,
[EndDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POCOMaster] ADD CONSTRAINT [POCOMasterPK] PRIMARY KEY NONCLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [POCOMasterRP1IDX] ON [dbo].[POCOMaster] ([PKey], [MasterPKey], [OrderDate]) ON [PRIMARY]
GO
