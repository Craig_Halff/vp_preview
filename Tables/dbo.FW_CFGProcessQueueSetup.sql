CREATE TABLE [dbo].[FW_CFGProcessQueueSetup]
(
[PKey] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PollingFrequency] [int] NOT NULL CONSTRAINT [DF__FW_CFGPro__Polli__074F1F47] DEFAULT ((0)),
[RetainHistory] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DaysToRetainHistory] [smallint] NOT NULL CONSTRAINT [DF__FW_CFGPro__DaysT__08434380] DEFAULT ((0)),
[RetainErrors] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGPro__Retai__093767B9] DEFAULT ('N'),
[DaysToRetainErrors] [int] NOT NULL CONSTRAINT [DF__FW_CFGPro__DaysT__0A2B8BF2] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGProcessQueueSetup] ADD CONSTRAINT [FW_CFGProcessQueueSetupPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
