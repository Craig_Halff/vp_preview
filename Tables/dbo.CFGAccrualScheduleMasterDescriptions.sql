CREATE TABLE [dbo].[CFGAccrualScheduleMasterDescriptions]
(
[ScheduleID] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAccrua__Compa__6CBA5945] DEFAULT ((0)),
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAccrualScheduleMasterDescriptions] ADD CONSTRAINT [CFGAccrualScheduleMasterDescriptionsPK] PRIMARY KEY CLUSTERED ([ScheduleID], [Company], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
