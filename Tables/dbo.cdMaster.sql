CREATE TABLE [dbo].[cdMaster]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CheckNo] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransDate] [datetime] NULL,
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Payee] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__cdMaster___Poste__57BF3C5F] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__cdMaster_Ne__Seq__58B36098] DEFAULT ((0)),
[CurrencyExchangeOverrideMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__cdMaster___Curre__59A784D1] DEFAULT ('N'),
[CurrencyExchangeOverrideDate] [datetime] NULL,
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__cdMaster___Curre__5A9BA90A] DEFAULT ((0)),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__cdMaster___Statu__5B8FCD43] DEFAULT ('N'),
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RejectReason] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Diary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaryNo] [int] NOT NULL CONSTRAINT [DF__cdMaster___Diary__5C83F17C] DEFAULT ((0)),
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cdMaster] ADD CONSTRAINT [cdMasterPK] PRIMARY KEY NONCLUSTERED ([Batch], [CheckNo]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
