CREATE TABLE [dbo].[ICBillInvMaster]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__ICBillInv__Perio__560CC96E] DEFAULT ((0)),
[RunSeq] [int] NOT NULL CONSTRAINT [DF__ICBillInv__RunSe__5700EDA7] DEFAULT ((0)),
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecdSeq] [int] NOT NULL CONSTRAINT [DF__ICBillInv__RecdS__57F511E0] DEFAULT ((0)),
[InvoiceDate] [datetime] NULL,
[MainName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MainType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillGroup] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__BillG__58E93619] DEFAULT ('N'),
[ConsolidatePrint] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__Conso__59DD5A52] DEFAULT ('N'),
[ARByGroup] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__ARByG__5AD17E8B] DEFAULT ('N'),
[BTDByGroup] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__BTDBy__5BC5A2C4] DEFAULT ('N'),
[InterestByGroup] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__Inter__5CB9C6FD] DEFAULT ('N'),
[TaxByGroup] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__TaxBy__5DADEB36] DEFAULT ('N'),
[ByGroupWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Footer] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConsolidateWBS2] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__Conso__5EA20F6F] DEFAULT ('N'),
[ConsolidateWBS3] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__Conso__5F9633A8] DEFAULT ('N'),
[PrintWBS1AR] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__Print__608A57E1] DEFAULT ('N'),
[PrintARWBS2Totals] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__Print__617E7C1A] DEFAULT ('N'),
[PrintWBS1BTD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__Print__6272A053] DEFAULT ('N'),
[PrintBTDWBS2Totals] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__Print__6366C48C] DEFAULT ('N'),
[PrintWBS2AR] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__Print__645AE8C5] DEFAULT ('N'),
[PrintWBS2BTD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__Print__654F0CFE] DEFAULT ('N'),
[BillWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS2Name] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS3Name] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FromDate] [datetime] NULL,
[ToDate] [datetime] NULL,
[Addressee] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address3] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address4] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address5] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address6] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address7] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address8] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RetByGroup] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__RetBy__66433137] DEFAULT ('N'),
[WBS2Rollup] [smallint] NOT NULL CONSTRAINT [DF__ICBillInv__WBS2R__67375570] DEFAULT ((0)),
[WBS3Rollup] [smallint] NOT NULL CONSTRAINT [DF__ICBillInv__WBS3R__682B79A9] DEFAULT ((0)),
[TaxCountryCode] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxRegistrationNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrintBTDReceivedTotal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__Print__691F9DE2] DEFAULT ('N'),
[PrintBTDARBalanceTotal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ICBillInv__Print__6A13C21B] DEFAULT ('N'),
[ICBillLinkWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillLinkWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillLinkWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillInvoiceCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingInvoiceTemplateName] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ICBillInvMaster] ADD CONSTRAINT [ICBillInvMasterPK] PRIMARY KEY NONCLUSTERED ([Company], [Period], [RunSeq], [Invoice], [MainWBS1], [RecdSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
