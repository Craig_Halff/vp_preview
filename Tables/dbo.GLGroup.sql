CREATE TABLE [dbo].[GLGroup]
(
[Code] [smallint] NOT NULL CONSTRAINT [DF__GLGroup_Ne__Code__15332A4A] DEFAULT ((0)),
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GroupType] [smallint] NOT NULL CONSTRAINT [DF__GLGroup_N__Group__16274E83] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GLGroup] ADD CONSTRAINT [GLGroupPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
