CREATE TABLE [dbo].[CFGVeteranDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGVeteranD__Seq__552528DB] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGVeteranDescriptions] ADD CONSTRAINT [CFGVeteranDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
