CREATE TABLE [dbo].[CCG_PAT_ConfigStages]
(
[Stage] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StageLabel] [varchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StageDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[Type] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequireStamp] [int] NULL,
[EmailSubject] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailSubjectBatch] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailMessage] [varchar] (950) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountingOnly] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValidRoles] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_ConfigStages] ADD CONSTRAINT [PK_CCG_PAT_ConfigStages] PRIMARY KEY CLUSTERED ([Stage]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Definitions of stages available for payable items', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStages', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated - see ValidRoles', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStages', 'COLUMN', N'AccountingOnly'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated - see CCG_PAT_ConfigStagesDescriptions', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStages', 'COLUMN', N'EmailMessage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated - see CCG_PAT_ConfigStagesDescriptions', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStages', 'COLUMN', N'EmailSubject'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated - see CCG_PAT_ConfigStagesDescriptions', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStages', 'COLUMN', N'EmailSubjectBatch'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Allows you to create a stamp in the Stamps tab and assign it to a Stage so that the vendor invoices you want to require a stamped impression are validated', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStages', 'COLUMN', N'RequireStamp'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The display order for this stage', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStages', 'COLUMN', N'SortOrder'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The stage name/key', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStages', 'COLUMN', N'Stage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated - see CCG_PAT_ConfigStagesDescriptions', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStages', 'COLUMN', N'StageDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated - see CCG_PAT_ConfigStagesDescriptions', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStages', 'COLUMN', N'StageLabel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Status of the stage - (A)ctive / (I)nactive', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStages', 'COLUMN', N'Status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Assigns a movement to the stage (Ready / NoExport / Exported)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStages', 'COLUMN', N'SubType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The type or function of the stage (Default / Next / Continue / Approve / Abort)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStages', 'COLUMN', N'Type'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Limit this stage to certain roles. If blank, all roles have access.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStages', 'COLUMN', N'ValidRoles'
GO
