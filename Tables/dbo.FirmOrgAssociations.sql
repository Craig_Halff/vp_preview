CREATE TABLE [dbo].[FirmOrgAssociations]
(
[FirmID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FirmOrgAssociations] ADD CONSTRAINT [FirmOrgAssociationsPK] PRIMARY KEY NONCLUSTERED ([FirmID], [Org]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
