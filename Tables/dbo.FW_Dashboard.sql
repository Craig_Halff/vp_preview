CREATE TABLE [dbo].[FW_Dashboard]
(
[Dashboard_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[onMenu] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_Dashbo__onMen__2A63515A] DEFAULT ('N'),
[isPublished] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_Dashbo__isPub__2B577593] DEFAULT ('N'),
[isGlobal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_Dashbo__isGlo__2C4B99CC] DEFAULT ('N'),
[Owner] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReadOnly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_Dashbo__ReadO__2D3FBE05] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_Dashboard] ADD CONSTRAINT [FW_DashboardPK] PRIMARY KEY CLUSTERED ([Dashboard_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FW_DashboardOwnerIDX] ON [dbo].[FW_Dashboard] ([Owner]) ON [PRIMARY]
GO
