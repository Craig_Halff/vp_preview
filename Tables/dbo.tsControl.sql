CREATE TABLE [dbo].[tsControl]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PostPeriod] [int] NOT NULL CONSTRAINT [DF__tsControl__PostP__4D984A55] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__tsControl__PostS__4E8C6E8E] DEFAULT ((0)),
[Recurring] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__tsControl__Recur__4F8092C7] DEFAULT ('N'),
[Selected] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__tsControl__Selec__5074B700] DEFAULT ('N'),
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__tsControl__Poste__5168DB39] DEFAULT ('N'),
[Creator] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__tsControl__Perio__525CFF72] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[RegHrsTotal] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsControl__RegHr__535123AB] DEFAULT ((0)),
[OvtHrsTotal] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsControl__OvtHr__544547E4] DEFAULT ((0)),
[SpecialOvtHrsTotal] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsControl__Speci__55396C1D] DEFAULT ((0)),
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SubmittedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedDate] [datetime] NULL,
[Diary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaryNo] [int] NOT NULL CONSTRAINT [DF__tsControl__Diary__562D9056] DEFAULT ((0)),
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tsControl] ADD CONSTRAINT [tsControlPK] PRIMARY KEY NONCLUSTERED ([Batch]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
