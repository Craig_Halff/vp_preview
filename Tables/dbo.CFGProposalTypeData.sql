CREATE TABLE [dbo].[CFGProposalTypeData]
(
[Code] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGProposalTypeData] ADD CONSTRAINT [CFGProposalTypeDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
