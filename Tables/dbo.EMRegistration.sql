CREATE TABLE [dbo].[EMRegistration]
(
[RecordID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__EMRegistrat__Seq__49BC0122] DEFAULT ((0)),
[Registration] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegistrationNo] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateRegistered] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateEarned] [datetime] NULL,
[DateExpires] [datetime] NULL,
[IncludeInProposal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMRegistr__Inclu__4AB0255B] DEFAULT ('Y'),
[LastRenewed] [datetime] NULL,
[CountryRegistered] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegistrationType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_EMRegistration]
      ON [dbo].[EMRegistration]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMRegistration'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'RecordID',CONVERT(NVARCHAR(2000),[RecordID],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'Employee',CONVERT(NVARCHAR(2000),DELETED.[Employee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Employee = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'Seq',CONVERT(NVARCHAR(2000),[Seq],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'Registration',CONVERT(NVARCHAR(2000),DELETED.[Registration],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGEMRegistration as oldDesc  on DELETED.Registration = oldDesc.Code

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'RegistrationNo',CONVERT(NVARCHAR(2000),[RegistrationNo],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'StateRegistered',CONVERT(NVARCHAR(2000),[StateRegistered],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'DateEarned',CONVERT(NVARCHAR(2000),[DateEarned],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'DateExpires',CONVERT(NVARCHAR(2000),[DateExpires],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'IncludeInProposal',CONVERT(NVARCHAR(2000),[IncludeInProposal],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'LastRenewed',CONVERT(NVARCHAR(2000),[LastRenewed],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'CountryRegistered',CONVERT(NVARCHAR(2000),[CountryRegistered],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'Status',CONVERT(NVARCHAR(2000),[Status],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'RegistrationType',CONVERT(NVARCHAR(2000),[RegistrationType],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_EMRegistration] ON [dbo].[EMRegistration]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_EMRegistration]
      ON [dbo].[EMRegistration]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMRegistration'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'RecordID',NULL,CONVERT(NVARCHAR(2000),[RecordID],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Employee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Employee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Employee = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Seq',NULL,CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Registration',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Registration],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGEMRegistration as newDesc  on INSERTED.Registration = newDesc.Code

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'RegistrationNo',NULL,CONVERT(NVARCHAR(2000),[RegistrationNo],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'StateRegistered',NULL,CONVERT(NVARCHAR(2000),[StateRegistered],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'DateEarned',NULL,CONVERT(NVARCHAR(2000),[DateEarned],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'DateExpires',NULL,CONVERT(NVARCHAR(2000),[DateExpires],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'IncludeInProposal',NULL,CONVERT(NVARCHAR(2000),[IncludeInProposal],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'LastRenewed',NULL,CONVERT(NVARCHAR(2000),[LastRenewed],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'CountryRegistered',NULL,CONVERT(NVARCHAR(2000),[CountryRegistered],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Status',NULL,CONVERT(NVARCHAR(2000),[Status],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'RegistrationType',NULL,CONVERT(NVARCHAR(2000),[RegistrationType],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_EMRegistration] ON [dbo].[EMRegistration]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_EMRegistration]
      ON [dbo].[EMRegistration]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMRegistration'
    
      If UPDATE([RecordID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'RecordID',
      CONVERT(NVARCHAR(2000),DELETED.[RecordID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecordID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[RecordID] Is Null And
				DELETED.[RecordID] Is Not Null
			) Or
			(
				INSERTED.[RecordID] Is Not Null And
				DELETED.[RecordID] Is Null
			) Or
			(
				INSERTED.[RecordID] !=
				DELETED.[RecordID]
			)
		) 
		END		
		
     If UPDATE([Employee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Employee',
     CONVERT(NVARCHAR(2000),DELETED.[Employee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Employee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[Employee] Is Null And
				DELETED.[Employee] Is Not Null
			) Or
			(
				INSERTED.[Employee] Is Not Null And
				DELETED.[Employee] Is Null
			) Or
			(
				INSERTED.[Employee] !=
				DELETED.[Employee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Employee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Employee = newDesc.Employee
		END		
		
      If UPDATE([Seq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Seq',
      CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[Seq] Is Null And
				DELETED.[Seq] Is Not Null
			) Or
			(
				INSERTED.[Seq] Is Not Null And
				DELETED.[Seq] Is Null
			) Or
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
		
     If UPDATE([Registration])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Registration',
     CONVERT(NVARCHAR(2000),DELETED.[Registration],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Registration],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[Registration] Is Null And
				DELETED.[Registration] Is Not Null
			) Or
			(
				INSERTED.[Registration] Is Not Null And
				DELETED.[Registration] Is Null
			) Or
			(
				INSERTED.[Registration] !=
				DELETED.[Registration]
			)
		) left join CFGEMRegistration as oldDesc  on DELETED.Registration = oldDesc.Code  left join  CFGEMRegistration as newDesc  on INSERTED.Registration = newDesc.Code
		END		
		
      If UPDATE([RegistrationNo])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'RegistrationNo',
      CONVERT(NVARCHAR(2000),DELETED.[RegistrationNo],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RegistrationNo],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[RegistrationNo] Is Null And
				DELETED.[RegistrationNo] Is Not Null
			) Or
			(
				INSERTED.[RegistrationNo] Is Not Null And
				DELETED.[RegistrationNo] Is Null
			) Or
			(
				INSERTED.[RegistrationNo] !=
				DELETED.[RegistrationNo]
			)
		) 
		END		
		
      If UPDATE([StateRegistered])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'StateRegistered',
      CONVERT(NVARCHAR(2000),DELETED.[StateRegistered],121),
      CONVERT(NVARCHAR(2000),INSERTED.[StateRegistered],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[StateRegistered] Is Null And
				DELETED.[StateRegistered] Is Not Null
			) Or
			(
				INSERTED.[StateRegistered] Is Not Null And
				DELETED.[StateRegistered] Is Null
			) Or
			(
				INSERTED.[StateRegistered] !=
				DELETED.[StateRegistered]
			)
		) 
		END		
		
      If UPDATE([DateEarned])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'DateEarned',
      CONVERT(NVARCHAR(2000),DELETED.[DateEarned],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DateEarned],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[DateEarned] Is Null And
				DELETED.[DateEarned] Is Not Null
			) Or
			(
				INSERTED.[DateEarned] Is Not Null And
				DELETED.[DateEarned] Is Null
			) Or
			(
				INSERTED.[DateEarned] !=
				DELETED.[DateEarned]
			)
		) 
		END		
		
      If UPDATE([DateExpires])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'DateExpires',
      CONVERT(NVARCHAR(2000),DELETED.[DateExpires],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DateExpires],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[DateExpires] Is Null And
				DELETED.[DateExpires] Is Not Null
			) Or
			(
				INSERTED.[DateExpires] Is Not Null And
				DELETED.[DateExpires] Is Null
			) Or
			(
				INSERTED.[DateExpires] !=
				DELETED.[DateExpires]
			)
		) 
		END		
		
      If UPDATE([IncludeInProposal])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'IncludeInProposal',
      CONVERT(NVARCHAR(2000),DELETED.[IncludeInProposal],121),
      CONVERT(NVARCHAR(2000),INSERTED.[IncludeInProposal],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[IncludeInProposal] Is Null And
				DELETED.[IncludeInProposal] Is Not Null
			) Or
			(
				INSERTED.[IncludeInProposal] Is Not Null And
				DELETED.[IncludeInProposal] Is Null
			) Or
			(
				INSERTED.[IncludeInProposal] !=
				DELETED.[IncludeInProposal]
			)
		) 
		END		
		
      If UPDATE([LastRenewed])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'LastRenewed',
      CONVERT(NVARCHAR(2000),DELETED.[LastRenewed],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LastRenewed],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[LastRenewed] Is Null And
				DELETED.[LastRenewed] Is Not Null
			) Or
			(
				INSERTED.[LastRenewed] Is Not Null And
				DELETED.[LastRenewed] Is Null
			) Or
			(
				INSERTED.[LastRenewed] !=
				DELETED.[LastRenewed]
			)
		) 
		END		
		
      If UPDATE([CountryRegistered])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'CountryRegistered',
      CONVERT(NVARCHAR(2000),DELETED.[CountryRegistered],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CountryRegistered],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[CountryRegistered] Is Null And
				DELETED.[CountryRegistered] Is Not Null
			) Or
			(
				INSERTED.[CountryRegistered] Is Not Null And
				DELETED.[CountryRegistered] Is Null
			) Or
			(
				INSERTED.[CountryRegistered] !=
				DELETED.[CountryRegistered]
			)
		) 
		END		
		
      If UPDATE([Status])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Status',
      CONVERT(NVARCHAR(2000),DELETED.[Status],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Status],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[Status] Is Null And
				DELETED.[Status] Is Not Null
			) Or
			(
				INSERTED.[Status] Is Not Null And
				DELETED.[Status] Is Null
			) Or
			(
				INSERTED.[Status] !=
				DELETED.[Status]
			)
		) 
		END		
		
      If UPDATE([RegistrationType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'RegistrationType',
      CONVERT(NVARCHAR(2000),DELETED.[RegistrationType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RegistrationType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[RegistrationType] Is Null And
				DELETED.[RegistrationType] Is Not Null
			) Or
			(
				INSERTED.[RegistrationType] Is Not Null And
				DELETED.[RegistrationType] Is Null
			) Or
			(
				INSERTED.[RegistrationType] !=
				DELETED.[RegistrationType]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_EMRegistration] ON [dbo].[EMRegistration]
GO
ALTER TABLE [dbo].[EMRegistration] ADD CONSTRAINT [EMRegistrationPK] PRIMARY KEY NONCLUSTERED ([RecordID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
