CREATE TABLE [dbo].[SEWSSLists]
(
[Role] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ListID] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Perms] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEWSSList__Perms__6E6F2E3B] DEFAULT ('R'),
[WebID] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_SEWSSLists]
      ON [dbo].[SEWSSLists]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SEWSSLists'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ListID],121),'Role',CONVERT(NVARCHAR(2000),[Role],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ListID],121),'ListID',CONVERT(NVARCHAR(2000),[ListID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ListID],121),'Perms',CONVERT(NVARCHAR(2000),[Perms],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ListID],121),'WebID',CONVERT(NVARCHAR(2000),[WebID],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_SEWSSLists]
      ON [dbo].[SEWSSLists]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SEWSSLists'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121),'Role',NULL,CONVERT(NVARCHAR(2000),[Role],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121),'ListID',NULL,CONVERT(NVARCHAR(2000),[ListID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121),'Perms',NULL,CONVERT(NVARCHAR(2000),[Perms],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121),'WebID',NULL,CONVERT(NVARCHAR(2000),[WebID],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_SEWSSLists]
      ON [dbo].[SEWSSLists]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SEWSSLists'
    
      If UPDATE([Role])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121),'Role',
      CONVERT(NVARCHAR(2000),DELETED.[Role],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Role],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[ListID] = DELETED.[ListID] AND 
		(
			(
				INSERTED.[Role] Is Null And
				DELETED.[Role] Is Not Null
			) Or
			(
				INSERTED.[Role] Is Not Null And
				DELETED.[Role] Is Null
			) Or
			(
				INSERTED.[Role] !=
				DELETED.[Role]
			)
		) 
		END		
		
      If UPDATE([ListID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121),'ListID',
      CONVERT(NVARCHAR(2000),DELETED.[ListID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ListID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[ListID] = DELETED.[ListID] AND 
		(
			(
				INSERTED.[ListID] Is Null And
				DELETED.[ListID] Is Not Null
			) Or
			(
				INSERTED.[ListID] Is Not Null And
				DELETED.[ListID] Is Null
			) Or
			(
				INSERTED.[ListID] !=
				DELETED.[ListID]
			)
		) 
		END		
		
      If UPDATE([Perms])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121),'Perms',
      CONVERT(NVARCHAR(2000),DELETED.[Perms],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Perms],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[ListID] = DELETED.[ListID] AND 
		(
			(
				INSERTED.[Perms] Is Null And
				DELETED.[Perms] Is Not Null
			) Or
			(
				INSERTED.[Perms] Is Not Null And
				DELETED.[Perms] Is Null
			) Or
			(
				INSERTED.[Perms] !=
				DELETED.[Perms]
			)
		) 
		END		
		
      If UPDATE([WebID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121),'WebID',
      CONVERT(NVARCHAR(2000),DELETED.[WebID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WebID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[ListID] = DELETED.[ListID] AND 
		(
			(
				INSERTED.[WebID] Is Null And
				DELETED.[WebID] Is Not Null
			) Or
			(
				INSERTED.[WebID] Is Not Null And
				DELETED.[WebID] Is Null
			) Or
			(
				INSERTED.[WebID] !=
				DELETED.[WebID]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[SEWSSLists] ADD CONSTRAINT [SEWSSListsPK] PRIMARY KEY NONCLUSTERED ([Role], [ListID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
