CREATE TABLE [dbo].[FW_CFGUserAppSelections]
(
[ApplicationClassName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Username] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastAppView] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastAppWhere] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastKeyValue] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGUserAppSelections] ADD CONSTRAINT [FW_CFGUserAppSelectionsPK] PRIMARY KEY NONCLUSTERED ([ApplicationClassName], [Username]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
