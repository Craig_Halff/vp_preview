CREATE TABLE [dbo].[CFGProjectCodeDescriptions]
(
[ProjectCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGProjectC__Seq__1942E6F9] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGProjectCodeDescriptions] ADD CONSTRAINT [CFGProjectCodeDescriptionsPK] PRIMARY KEY CLUSTERED ([ProjectCode], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
