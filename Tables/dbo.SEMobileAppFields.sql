CREATE TABLE [dbo].[SEMobileAppFields]
(
[Role] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Field] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Infocenter] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Sequence] [int] NOT NULL CONSTRAINT [DF__SEMobileA__Seque__2AB9226C] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SEMobileAppFields] ADD CONSTRAINT [SEMobileAppFieldsPK] PRIMARY KEY NONCLUSTERED ([Role], [Field], [Infocenter]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
