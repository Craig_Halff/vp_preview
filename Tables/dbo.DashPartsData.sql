CREATE TABLE [dbo].[DashPartsData]
(
[PartID] [int] NOT NULL CONSTRAINT [DF__DashParts__PartI__1804A9FD] DEFAULT ((0)),
[Type] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Sql] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultPrefs] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DashPartsData] ADD CONSTRAINT [DashPartsDataPK] PRIMARY KEY CLUSTERED ([PartID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
