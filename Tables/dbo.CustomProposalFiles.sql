CREATE TABLE [dbo].[CustomProposalFiles]
(
[CustomPropID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL,
[OriginalFileID] [uniqueidentifier] NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomProposalFiles] ADD CONSTRAINT [CustomProposalFilesPK] PRIMARY KEY CLUSTERED ([CustomPropID], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
