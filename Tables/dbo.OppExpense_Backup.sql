CREATE TABLE [dbo].[OppExpense_Backup]
(
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExpenseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OppExpCost] [decimal] (19, 4) NOT NULL,
[OppExpBill] [decimal] (19, 4) NOT NULL,
[DirectAcctFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__OppExpens__Direc__3A4FA060] DEFAULT ('N'),
[SeqNo] [smallint] NOT NULL CONSTRAINT [DF__OppExpens__SeqNo__3B43C499] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OppExpense_Backup] ADD CONSTRAINT [OppExpensePK] PRIMARY KEY NONCLUSTERED ([ExpenseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
