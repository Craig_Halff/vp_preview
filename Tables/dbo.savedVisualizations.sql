CREATE TABLE [dbo].[savedVisualizations]
(
[ID] [uniqueidentifier] NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportFolder] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportType] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportName] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateDate] [datetime] NOT NULL,
[ExpDate] [datetime] NOT NULL,
[Options] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Tbl] [image] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[savedVisualizations] ADD CONSTRAINT [savedVisualizationsPK] PRIMARY KEY CLUSTERED ([ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
