CREATE TABLE [dbo].[CFGScreenDesignerData]
(
[InfocenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ComponentID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ComponentType] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LockedFor] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequiredFor] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HiddenFor] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DesignerCreated] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGScreen__Desig__62B42E23] DEFAULT ('N'),
[ButtonType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecordLimit] [int] NOT NULL CONSTRAINT [DF__CFGScreen__Recor__63A8525C] DEFAULT ((0)),
[GenericPropValue] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ColPos] [int] NOT NULL CONSTRAINT [DF__CFGScreen__ColPo__649C7695] DEFAULT ((0)),
[RowPos] [int] NOT NULL CONSTRAINT [DF__CFGScreen__RowPo__65909ACE] DEFAULT ((0)),
[ColWidth] [int] NOT NULL CONSTRAINT [DF__CFGScreen__ColWi__6684BF07] DEFAULT ((0)),
[RowHeight] [int] NOT NULL CONSTRAINT [DF__CFGScreen__RowHe__6778E340] DEFAULT ((0)),
[LabelPosition] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultValue] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[PropertyBag] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeletePRDefaultsTrigger] 
   ON  [dbo].[CFGScreenDesignerData]
After Delete
AS 
BEGIN
	SET NOCOUNT ON;

	If exists (Select InfoCenterArea From Deleted Where InfoCenterArea='Projects' and left(ComponentID,3)='PR.' and GridID='X' and NOT RIGHT(ComponentID, len(ComponentID)-3) 
	in ('FederalInd','Referable','AvailableForCRM','ReadyForApproval','ReadyForProcessing','CostRateTableNo','PayRateTableNo','XChargeMethod','XChargeMult','UnitTable',
	'BudgetedLevel','BudgetSourse','TKCheckRPDate','TKCheckRPPlannedHrs','BillableWarning','ICBillingExpMethod','ICBillingExpTableNo','ICBillingExpMult',
	'ICBillingLabMethod','ICBillingLabTableNo','ICBillingLabMult','Stage','TotalProjectCost','TotalCostComment','FirmCost','FirmCostComment'))
	BEGIN
		DECLARE @VisionAuditUser Nvarchar(50), @now varchar(50)
		set @VisionAuditUser = dbo.FW_GetUsername()
		set @now = convert(varchar(100), getutcdate(), 121)

		DECLARE @SQLString Nvarchar(1000)
		DECLARE @FieldName Nvarchar(125)
		DECLARE @ComponentType Nvarchar(30)
		DECLARE @DefaultValue Nvarchar(1000)

		SELECT top 1 @FieldName = Substring(ComponentID, 4, len(ComponentID)), 
		@ComponentType = ComponentType, 
		@DefaultValue = DefaultValue 
		FROM Deleted

		IF EXISTS (select sys.columns.name from sys.columns inner join sys.objects on sys.columns.object_id = sys.objects.object_id where sys.objects.name = 'PRDefaults' and sys.columns.name = @FieldName)
		Begin
			if not (@DefaultValue is null)
			Begin
-- Update the field in PRDefaults table with null or the SQL default value
				DECLARE @Custom_Default Nvarchar(125) 
				Set @Custom_Default = (SELECT isnull(replace(replace(replace(
                                      (SELECT object_definition(default_object_id) AS definition FROM sys.columns c WHERE  name = sys.columns.name AND    object_id = sys.objects.object_id) 
				, '(', ''), ')', ''), 'DEFAULT ', ''), case when sys.columns.system_type_id in (52, 106, 52) then 0 else null end) as Custom_Default 
				From sys.objects left join sys.columns on sys.objects.object_id = sys.columns.object_id left join sys.types on sys.columns.user_type_id = sys.types.user_type_id 
				WHERE sys.objects.name='PRDefaults' and sys.columns.name = @FieldName 
                and sys.columns.is_nullable = 0 and sys.objects.type = 'U' AND sys.objects.name<>'DTPROPERTIES' AND sys.objects.name NOT LIKE 'TMP%' )

				if (@Custom_Default is null) 
					Set @SQLString = 'Update PRDefaults set ' + @FieldName + ' = null, ModUser = ''' + @VisionAuditUser + ''', ModDate = ''' + @now + ''' where not ' + @FieldName + ' is null'
				else
					Set @SQLString = 'Update PRDefaults set ' + @FieldName + ' = ' + @Custom_Default + ', ModUser = ''' + @VisionAuditUser + ''', ModDate = ''' + @now + ''' where ' + @FieldName + ' is null or ' + @FieldName + ' <> ' + @Custom_Default 
				print @SQLString
				execute(@SQLString)
			End
		End		
	End
	SET NOCOUNT OFF
END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdatePRDefaultsTrigger] 
   ON  [dbo].[CFGScreenDesignerData]
AFTER INSERT, UPDATE 
as
BEGIN
	SET NOCOUNT ON;

	If UPDATE([DefaultValue]) and exists (Select DefaultValue From inserted Where InfoCenterArea='Projects' and left(ComponentID,3)='PR.' and GridID='X' and NOT RIGHT(ComponentID, len(ComponentID)-3) 
	in ('FederalInd','Referable','AvailableForCRM','ReadyForApproval','ReadyForProcessing','CostRateTableNo','PayRateTableNo','XChargeMethod','XChargeMult','UnitTable',
	'BudgetedLevel','BudgetSourse','TKCheckRPDate','TKCheckRPPlannedHrs','BillableWarning','ICBillingExpMethod','ICBillingExpTableNo','ICBillingExpMult',
	'ICBillingLabMethod','ICBillingLabTableNo','ICBillingLabMult','Stage','TotalProjectCost','TotalCostComment','FirmCost','FirmCostComment'))
	BEGIN
		DECLARE @VisionAuditUser Nvarchar(50), @now varchar(50)
		set @VisionAuditUser = dbo.FW_GetUsername()
		set @now = convert(varchar(100), getutcdate(), 121)

		DECLARE @SQLString Nvarchar(1000)
		DECLARE @FieldName Nvarchar(125)
		DECLARE @ComponentType Nvarchar(30)
		DECLARE @DefaultValue Nvarchar(1000)

		SELECT top 1 @FieldName = Substring(ComponentID, 4, len(ComponentID)), 
		@ComponentType = ComponentType, 
		@DefaultValue = DefaultValue 
		FROM inserted

		IF EXISTS (select sys.columns.name from sys.columns inner join sys.objects on sys.columns.object_id = sys.objects.object_id where sys.objects.name = 'PRDefaults' and sys.columns.name = @FieldName)
		Begin
			if (@DefaultValue is null)
			Begin
-- Update the field in PRDefaults table with null or the SQL default value
				DECLARE @Custom_Default Nvarchar(125) 
				Set @Custom_Default = (SELECT isnull(replace(replace(replace(
                                      (SELECT object_definition(default_object_id) AS definition FROM sys.columns c WHERE  name = sys.columns.name AND    object_id = sys.objects.object_id) 
				, '(', ''), ')', ''), 'DEFAULT ', ''), case when sys.columns.system_type_id in (52, 106, 52) then 0 else null end) as Custom_Default 
				From sys.objects left join sys.columns on sys.objects.object_id = sys.columns.object_id left join sys.types on sys.columns.user_type_id = sys.types.user_type_id 
				WHERE sys.objects.name='PRDefaults' and sys.columns.name = @FieldName 
                and sys.columns.is_nullable = 0 and sys.objects.type = 'U' AND sys.objects.name<>'DTPROPERTIES' AND sys.objects.name NOT LIKE 'TMP%' )

				if (@Custom_Default is null) 
					Set @SQLString = 'Update PRDefaults set ' + @FieldName + ' = null, ModUser = ''' + @VisionAuditUser + ''', ModDate = ''' + @now + ''' where not ' + @FieldName + ' is null'
				else
					Set @SQLString = 'Update PRDefaults set ' + @FieldName + ' = ' + @Custom_Default + ', ModUser = ''' + @VisionAuditUser + ''', ModDate = ''' + @now + ''' where ' + @FieldName + ' is null or ' + @FieldName + ' <> ' + @Custom_Default 
				--print @SQLString
				execute(@SQLString)
			End
			else
			Begin
-- Update the field in PRDefaults table with the updated values
				if (left(@ComponentType, 8) = 'currency' or @ComponentType = 'numeric')
					Set @SQLString = 'Update PRDefaults set ' + @FieldName + ' = ' + @DefaultValue + ', ModUser = ''' + @VisionAuditUser + ''', ModDate = ''' + @now + ''' where ' + @FieldName + ' is null or ' + @FieldName + ' <> ' + @DefaultValue
				else
					Set @SQLString = 'Update PRDefaults set ' + @FieldName + ' = ''' + @DefaultValue + ''', ModUser = ''' + @VisionAuditUser + ''', ModDate = ''' + @now + ''' where ' + @FieldName + ' is null or ' + @FieldName + ' <> ''' + @DefaultValue + ''''
				print @SQLString
				execute(@SQLString)
			End
		End		
	End
	SET NOCOUNT OFF
END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_CFGScreenDesignerData]
      ON [dbo].[CFGScreenDesignerData]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CFGScreenDesignerData'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[InfocenterArea],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ComponentID],121),'LockedFor',CONVERT(NVARCHAR(2000),[LockedFor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[InfocenterArea],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ComponentID],121),'RequiredFor',CONVERT(NVARCHAR(2000),[RequiredFor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[InfocenterArea],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ComponentID],121),'HiddenFor',CONVERT(NVARCHAR(2000),[HiddenFor],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_CFGScreenDesignerData]
      ON [dbo].[CFGScreenDesignerData]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CFGScreenDesignerData'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[InfocenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'LockedFor',NULL,CONVERT(NVARCHAR(2000),[LockedFor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[InfocenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'RequiredFor',NULL,CONVERT(NVARCHAR(2000),[RequiredFor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[InfocenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'HiddenFor',NULL,CONVERT(NVARCHAR(2000),[HiddenFor],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_CFGScreenDesignerData]
      ON [dbo].[CFGScreenDesignerData]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CFGScreenDesignerData'
    
      If UPDATE([LockedFor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[InfocenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'LockedFor',
      CONVERT(NVARCHAR(2000),DELETED.[LockedFor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LockedFor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[InfocenterArea] = DELETED.[InfocenterArea] AND INSERTED.[GridID] = DELETED.[GridID] AND INSERTED.[ComponentID] = DELETED.[ComponentID] AND 
		(
			(
				INSERTED.[LockedFor] Is Null And
				DELETED.[LockedFor] Is Not Null
			) Or
			(
				INSERTED.[LockedFor] Is Not Null And
				DELETED.[LockedFor] Is Null
			) Or
			(
				INSERTED.[LockedFor] !=
				DELETED.[LockedFor]
			)
		) 
		END		
		
      If UPDATE([RequiredFor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[InfocenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'RequiredFor',
      CONVERT(NVARCHAR(2000),DELETED.[RequiredFor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RequiredFor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[InfocenterArea] = DELETED.[InfocenterArea] AND INSERTED.[GridID] = DELETED.[GridID] AND INSERTED.[ComponentID] = DELETED.[ComponentID] AND 
		(
			(
				INSERTED.[RequiredFor] Is Null And
				DELETED.[RequiredFor] Is Not Null
			) Or
			(
				INSERTED.[RequiredFor] Is Not Null And
				DELETED.[RequiredFor] Is Null
			) Or
			(
				INSERTED.[RequiredFor] !=
				DELETED.[RequiredFor]
			)
		) 
		END		
		
      If UPDATE([HiddenFor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[InfocenterArea],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[GridID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ComponentID],121),'HiddenFor',
      CONVERT(NVARCHAR(2000),DELETED.[HiddenFor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[HiddenFor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[InfocenterArea] = DELETED.[InfocenterArea] AND INSERTED.[GridID] = DELETED.[GridID] AND INSERTED.[ComponentID] = DELETED.[ComponentID] AND 
		(
			(
				INSERTED.[HiddenFor] Is Null And
				DELETED.[HiddenFor] Is Not Null
			) Or
			(
				INSERTED.[HiddenFor] Is Not Null And
				DELETED.[HiddenFor] Is Null
			) Or
			(
				INSERTED.[HiddenFor] !=
				DELETED.[HiddenFor]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[CFGScreenDesignerData] ADD CONSTRAINT [CFGScreenDesignerDataPK] PRIMARY KEY NONCLUSTERED ([InfocenterArea], [GridID], [ComponentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CFGScreenDesignerDataInfocenterAreaComponentIDIDX] ON [dbo].[CFGScreenDesignerData] ([InfocenterArea], [ComponentID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CFGScreenDesignerDataInfocenterAreaGridIDIDX] ON [dbo].[CFGScreenDesignerData] ([InfocenterArea], [GridID], [ComponentID]) INCLUDE ([DefaultValue], [GenericPropValue], [HiddenFor], [RequiredFor], [TabID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
