CREATE TABLE [dbo].[CFGCompetitionTypeDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGCompetit__Seq__66976B9B] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCompetitionTypeDescriptions] ADD CONSTRAINT [CFGCompetitionTypeDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
