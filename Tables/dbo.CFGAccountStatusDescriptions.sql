CREATE TABLE [dbo].[CFGAccountStatusDescriptions]
(
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAccoun__Statu__6330EF0B] DEFAULT ('N'),
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAccountStatusDescriptions] ADD CONSTRAINT [CFGAccountStatusDescriptionsPK] PRIMARY KEY CLUSTERED ([Status], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
