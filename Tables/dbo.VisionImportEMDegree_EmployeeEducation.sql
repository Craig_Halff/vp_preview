CREATE TABLE [dbo].[VisionImportEMDegree_EmployeeEducation]
(
[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecordID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [int] NULL,
[Degree] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Specialty] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Institution] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearEarned] [int] NULL,
[IncludeInProposal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
