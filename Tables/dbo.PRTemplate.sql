CREATE TABLE [dbo].[PRTemplate]
(
[WBS1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubLevel] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__SubLe__6F235B52] DEFAULT ('N'),
[Principal] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjMgr] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Supervisor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLAddress] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplate___Fee__70177F8B] DEFAULT ((0)),
[ReimbAllow] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Reimb__710BA3C4] DEFAULT ((0)),
[ConsultFee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Consu__71FFC7FD] DEFAULT ((0)),
[BudOHRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__BudOH__72F3EC36] DEFAULT ((0)),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MultAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__MultA__73E8106F] DEFAULT ((0)),
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitTable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PctComp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__PctCo__74DC34A8] DEFAULT ((0)),
[LabPctComp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__LabPc__75D058E1] DEFAULT ((0)),
[ExpPctComp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__ExpPc__76C47D1A] DEFAULT ((0)),
[BillByDefault] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__BillB__77B8A153] DEFAULT ('N'),
[BillableWarning] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__Billa__78ACC58C] DEFAULT ('N'),
[Memo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BudgetedFlag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__Budge__79A0E9C5] DEFAULT ('N'),
[BudgetedLevels] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XCharge] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__XChar__7A950DFE] DEFAULT ('G'),
[XChargeMethod] [smallint] NOT NULL CONSTRAINT [DF__PRTemplat__XChar__7B893237] DEFAULT ((0)),
[XChargeMult] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__XChar__7C7D5670] DEFAULT ((0)),
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Closed] [int] NOT NULL CONSTRAINT [DF__PRTemplat__Close__7D717AA9] DEFAULT ((0)),
[ReadOnly] [int] NOT NULL CONSTRAINT [DF__PRTemplat__ReadO__7E659EE2] DEFAULT ((0)),
[DefaultEffortDriven] [int] NOT NULL CONSTRAINT [DF__PRTemplat__Defau__7F59C31B] DEFAULT ((0)),
[DefaultTaskType] [int] NOT NULL CONSTRAINT [DF__PRTemplat__Defau__004DE754] DEFAULT ((0)),
[VersionID] [int] NOT NULL CONSTRAINT [DF__PRTemplat__Versi__01420B8D] DEFAULT ((0)),
[ContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLBillingAddr] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LongName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address3] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[County] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FederalInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__Feder__02362FC6] DEFAULT ('N'),
[ProjectType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Responsibility] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Referable] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__Refer__032A53FF] DEFAULT ('N'),
[EstCompletionDate] [datetime] NULL,
[ActCompletionDate] [datetime] NULL,
[ContractDate] [datetime] NULL,
[BidDate] [datetime] NULL,
[ComplDateComment] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__FirmC__041E7838] DEFAULT ((0)),
[FirmCostComment] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TotalProjectCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Total__05129C71] DEFAULT ((0)),
[TotalCostComment] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientConfidential] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientAlias] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AvailableForCRM] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__Avail__0606C0AA] DEFAULT ('N'),
[ReadyForApproval] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__Ready__06FAE4E3] DEFAULT ('N'),
[ReadyForProcessing] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__Ready__07EF091C] DEFAULT ('N'),
[BillingClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fax] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMail] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProposalWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CostRateMeth] [smallint] NOT NULL CONSTRAINT [DF__PRTemplat__CostR__08E32D55] DEFAULT ((0)),
[CostRateTableNo] [int] NOT NULL CONSTRAINT [DF__PRTemplat__CostR__09D7518E] DEFAULT ((0)),
[PayRateMeth] [smallint] NOT NULL CONSTRAINT [DF__PRTemplat__PayRa__0ACB75C7] DEFAULT ((0)),
[PayRateTableNo] [int] NOT NULL CONSTRAINT [DF__PRTemplat__PayRa__0BBF9A00] DEFAULT ((0)),
[Locale] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LineItemApproval] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplate__LineItemApproval__DefN] DEFAULT ('N'),
[LineItemApprovalEK] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplate__LineItemApprovalEK__DefN] DEFAULT ('N'),
[BudgetSource] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BudgetLevel] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProfServicesComplDate] [datetime] NULL,
[ConstComplDate] [datetime] NULL,
[ProjectCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectExchangeRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__PRTemplat__Proje__0E9C06AB] DEFAULT ((0)),
[BillingCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingExchangeRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__PRTemplat__Billi__0F902AE4] DEFAULT ((0)),
[RestrictChargeCompanies] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__Restr__10844F1D] DEFAULT ('N'),
[FeeBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__FeeBi__11787356] DEFAULT ((0)),
[ReimbAllowBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Reimb__126C978F] DEFAULT ((0)),
[ConsultFeeBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Consu__1360BBC8] DEFAULT ((0)),
[RevUpsetLimits] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__RevUp__1454E001] DEFAULT ('N'),
[RevUpsetWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevUpsetWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevUpsetIncludeComp] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__RevUp__1549043A] DEFAULT ('N'),
[RevUpsetIncludeCons] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__RevUp__163D2873] DEFAULT ('N'),
[RevUpsetIncludeReimb] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__RevUp__17314CAC] DEFAULT ('N'),
[PORMBRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__PORMB__182570E5] DEFAULT ((0)),
[POCNSRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__POCNS__1919951E] DEFAULT ((0)),
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TKCheckRPDate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__TKChe__1A0DB957] DEFAULT ('N'),
[ICBillingLab] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__ICBil__1B01DD90] DEFAULT ('G'),
[ICBillingLabMethod] [smallint] NOT NULL CONSTRAINT [DF__PRTemplat__ICBil__1BF601C9] DEFAULT ((0)),
[ICBillingLabMult] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__ICBil__1CEA2602] DEFAULT ((0)),
[ICBillingExp] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__ICBil__1DDE4A3B] DEFAULT ('G'),
[ICBillingExpMethod] [smallint] NOT NULL CONSTRAINT [DF__PRTemplat__ICBil__1ED26E74] DEFAULT ((0)),
[ICBillingExpMult] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__ICBil__1FC692AD] DEFAULT ((0)),
[RequireComments] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__Requi__20BAB6E6] DEFAULT ('C'),
[TKCheckRPPlannedHrs] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__TKChe__21AEDB1F] DEFAULT ('N'),
[BillByDefaultConsultants] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__BillB__22A2FF58] DEFAULT ('E'),
[BillByDefaultOtherExp] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__BillB__23972391] DEFAULT ('E'),
[BillByDefaultORTable] [int] NOT NULL CONSTRAINT [DF__PRTemplat__BillB__248B47CA] DEFAULT ((0)),
[PhoneFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FaxFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevType2] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__RevTy__257F6C03] DEFAULT ('N'),
[RevType3] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__RevTy__2673903C] DEFAULT ('N'),
[RevType4] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__RevTy__2767B475] DEFAULT ('N'),
[RevType5] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__RevTy__285BD8AE] DEFAULT ('N'),
[RevUpsetCategoryToAdjust] [smallint] NOT NULL CONSTRAINT [DF__PRTemplat__RevUp__294FFCE7] DEFAULT ((0)),
[FeeFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__FeeFu__2A442120] DEFAULT ((0)),
[ReimbAllowFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Reimb__2B384559] DEFAULT ((0)),
[ConsultFeeFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Consu__2C2C6992] DEFAULT ((0)),
[RevenueMethod] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__Reven__2D208DCB] DEFAULT ('U'),
[ICBillingLabTableNo] [int] NOT NULL CONSTRAINT [DF__PRTemplat__ICBil__2E14B204] DEFAULT ((0)),
[ICBillingExpTableNo] [int] NOT NULL CONSTRAINT [DF__PRTemplat__ICBil__2F08D63D] DEFAULT ((0)),
[Biller] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeeDirLab] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__FeeDi__2FFCFA76] DEFAULT ((0)),
[FeeDirExp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__FeeDi__30F11EAF] DEFAULT ((0)),
[ReimbAllowExp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Reimb__31E542E8] DEFAULT ((0)),
[ReimbAllowCons] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Reimb__32D96721] DEFAULT ((0)),
[FeeDirLabBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__FeeDi__33CD8B5A] DEFAULT ((0)),
[FeeDirExpBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__FeeDi__34C1AF93] DEFAULT ((0)),
[ReimbAllowExpBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Reimb__35B5D3CC] DEFAULT ((0)),
[ReimbAllowConsBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Reimb__36A9F805] DEFAULT ((0)),
[FeeDirLabFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__FeeDi__379E1C3E] DEFAULT ((0)),
[FeeDirExpFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__FeeDi__38924077] DEFAULT ((0)),
[ReimbAllowExpFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Reimb__398664B0] DEFAULT ((0)),
[ReimbAllowConsFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Reimb__3A7A88E9] DEFAULT ((0)),
[RevUpsetIncludeCompDirExp] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__RevUp__3B6EAD22] DEFAULT ('N'),
[RevUpsetIncludeReimbCons] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__RevUp__3C62D15B] DEFAULT ('N'),
[AwardType] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Duration] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractTypeGovCon] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompetitionType] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MasterContract] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Solicitation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAICS] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurRole] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AjeraSync] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__Ajera__3D56F594] DEFAULT ('N'),
[ServProCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FESurchargePct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__FESur__3E4B19CD] DEFAULT ((0)),
[FESurcharge] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__FESur__3F3F3E06] DEFAULT ((0)),
[FEAddlExpensesPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__FEAdd__4033623F] DEFAULT ((0)),
[FEAddlExpenses] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__FEAdd__41278678] DEFAULT ((0)),
[FEOtherPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__FEOth__421BAAB1] DEFAULT ((0)),
[FEOther] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__FEOth__430FCEEA] DEFAULT ((0)),
[ProjectTemplate] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AjeraSpentLabor] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Ajera__4403F323] DEFAULT ((0)),
[AjeraSpentReimbursable] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Ajera__44F8175C] DEFAULT ((0)),
[AjeraSpentConsultant] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Ajera__45EC3B95] DEFAULT ((0)),
[AjeraCostLabor] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Ajera__46E05FCE] DEFAULT ((0)),
[AjeraCostReimbursable] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Ajera__47D48407] DEFAULT ((0)),
[AjeraCostConsultant] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Ajera__48C8A840] DEFAULT ((0)),
[AjeraWIPLabor] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Ajera__49BCCC79] DEFAULT ((0)),
[AjeraWIPReimbursable] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Ajera__4AB0F0B2] DEFAULT ((0)),
[AjeraWIPConsultant] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Ajera__4BA514EB] DEFAULT ((0)),
[AjeraBilledLabor] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Ajera__4C993924] DEFAULT ((0)),
[AjeraBilledReimbursable] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Ajera__4D8D5D5D] DEFAULT ((0)),
[AjeraBilledConsultant] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Ajera__4E818196] DEFAULT ((0)),
[AjeraReceivedLabor] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Ajera__4F75A5CF] DEFAULT ((0)),
[AjeraReceivedReimbursable] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Ajera__5069CA08] DEFAULT ((0)),
[AjeraReceivedConsultant] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Ajera__515DEE41] DEFAULT ((0)),
[TLInternalKey] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLProjectID] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLProjectName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLChargeBandInternalKey] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLChargeBandExternalCode] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLSyncModDate] [datetime] NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[PIMID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SiblingWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Stage] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EstStartDate] [datetime] NULL,
[EstEndDate] [datetime] NULL,
[EstFees] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__EstFe__21FA8A90] DEFAULT ((0)),
[EstConstructionCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__EstCo__22EEAEC9] DEFAULT ((0)),
[Revenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Reven__23E2D302] DEFAULT ((0)),
[Probability] [smallint] NOT NULL CONSTRAINT [DF__PRTemplat__Proba__24D6F73B] DEFAULT ((0)),
[WeightedRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRTemplat__Weigh__25CB1B74] DEFAULT ((0)),
[CloseDate] [datetime] NULL,
[OpenDate] [datetime] NULL,
[Source] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PublicNoticeDate] [datetime] NULL,
[SolicitationNum] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LabBillTable] [int] NOT NULL CONSTRAINT [DF__PRTemplat__LabBi__26BF3FAD] DEFAULT ((0)),
[LabCostTable] [int] NOT NULL CONSTRAINT [DF__PRTemplat__LabCo__27B363E6] DEFAULT ((0)),
[AllocMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Timescale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClosedReason] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClosedNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IQID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IQLastUpdate] [datetime] NULL,
[MarketingCoordinator] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProposalManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BusinessDeveloperLead] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFID] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFLastModifiedDate] [datetime] NULL,
[TotalContractValue] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__PRTemplat__Total__299BAC58] DEFAULT ((0)),
[PeriodOfPerformance] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__PRTemplat__Perio__2A8FD091] DEFAULT ((0)),
[LostTo] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreAwardWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UtilizationScheduleFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRTemplat__Utili__6C5D93EE] DEFAULT ('N')
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[stPRTemplateLinkSiblingsTrigger] ON [dbo].[PRTemplate] AFTER INSERT, UPDATE 
AS
BEGIN

/*

  In order to keep the algorithm straight, let's use the following naming conventions:
    1. Project "Me" will be the project with WBS1 = @strWBS1
    2. Project "Sibling" will be the project with WBS1 = @strSiblingWBS1

*/

  SET NOCOUNT ON

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Declare Temp tables.
  
  DECLARE @tabChanged TABLE (
    WBS1 nvarchar(32) COLLATE database_default,
    SiblingWBS1 nvarchar(30) COLLATE database_default
    UNIQUE(WBS1, SiblingWBS1)
  )
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (UPDATE(SiblingWBS1))
    BEGIN

      -- Collect Projects that had inserted or updated SiblingWBS1.
      -- For those Projects that were changed, get SiblingWBS1 of the Project row.

      INSERT @tabChanged(
        WBS1,
        SiblingWBS1
      )
        SELECT DISTINCT
          I.WBS1,
          PRTemplate.SiblingWBS1
          FROM (
            SELECT DISTINCT IX.WBS1 FROM INSERTED AS IX
          ) AS I
            LEFT JOIN PRTemplate ON I.WBS1 = PRTemplate.WBS1 AND PRTemplate.WBS2 = ' ' AND PRTemplate.WBS3 = ' '

      -- Make link to "Sibling" Project and its descendants.

      UPDATE PR_Sibling SET
        SiblingWBS1 = C.WBS1
        FROM PRTemplate AS PR_Sibling
          INNER JOIN @tabChanged AS C ON PR_Sibling.WBS1 = C.SiblingWBS1
        WHERE 
          NOT EXISTS(SELECT PR_Sibling.SiblingWBS1 INTERSECT SELECT C.WBS1)
          AND EXISTS(SELECT 'X' FROM PRTemplate WHERE WBS1 = C.WBS1)
 
    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Propagate sibling links from "Me" Project down to its descendants.

      UPDATE PR_Me SET
        SiblingWBS1 = C.SiblingWBS1
        FROM PRTemplate AS PR_Me 
          INNER JOIN @tabChanged AS C ON PR_Me.WBS1 = C.WBS1
        WHERE 
          NOT (PR_Me.WBS2 = ' ' AND PR_Me.WBS3 = ' ')
          AND NOT EXISTS(SELECT PR_Me.SiblingWBS1 INTERSECT SELECT C.SiblingWBS1)
          AND EXISTS(SELECT 'X' FROM PRTemplate WHERE WBS1 = C.SiblingWBS1)

    END /* END IF (UPDATE(SiblingWBS1)) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_PRTemplate]
      ON [dbo].[PRTemplate]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'PRTemplate'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'WBS1',CONVERT(NVARCHAR(2000),[WBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'WBS2',CONVERT(NVARCHAR(2000),[WBS2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'WBS3',CONVERT(NVARCHAR(2000),[WBS3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Name',CONVERT(NVARCHAR(2000),[Name],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ChargeType',CONVERT(NVARCHAR(2000),DELETED.[ChargeType],121),NULL, oldDesc.Label, NULL, @source,@app
        FROM DELETED left join CFGChargeType as oldDesc  on DELETED.ChargeType = oldDesc.Type

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'SubLevel',CONVERT(NVARCHAR(2000),[SubLevel],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Principal',CONVERT(NVARCHAR(2000),DELETED.[Principal],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Principal = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ProjMgr',CONVERT(NVARCHAR(2000),DELETED.[ProjMgr],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.ProjMgr = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Supervisor',CONVERT(NVARCHAR(2000),DELETED.[Supervisor],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Supervisor = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ClientID',CONVERT(NVARCHAR(2000),DELETED.[ClientID],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join CL as oldDesc  on DELETED.ClientID = oldDesc.ClientID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CLAddress',CONVERT(NVARCHAR(2000),[CLAddress],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Fee',CONVERT(NVARCHAR(2000),[Fee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ReimbAllow',CONVERT(NVARCHAR(2000),[ReimbAllow],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ConsultFee',CONVERT(NVARCHAR(2000),[ConsultFee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BudOHRate',CONVERT(NVARCHAR(2000),[BudOHRate],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Status',CONVERT(NVARCHAR(2000),DELETED.[Status],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGProjectStatus as oldDesc  on DELETED.Status = oldDesc.Code

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RevType',CONVERT(NVARCHAR(2000),DELETED.[RevType],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGRGMethods as oldDesc  on DELETED.RevType = oldDesc.Method

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'MultAmt',CONVERT(NVARCHAR(2000),[MultAmt],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Org',CONVERT(NVARCHAR(2000),DELETED.[Org],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join Organization as oldDesc  on DELETED.Org = oldDesc.Org

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'UnitTable',CONVERT(NVARCHAR(2000),[UnitTable],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'StartDate',CONVERT(NVARCHAR(2000),[StartDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'EndDate',CONVERT(NVARCHAR(2000),[EndDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'PctComp',CONVERT(NVARCHAR(2000),[PctComp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'LabPctComp',CONVERT(NVARCHAR(2000),[LabPctComp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ExpPctComp',CONVERT(NVARCHAR(2000),[ExpPctComp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BillByDefault',CONVERT(NVARCHAR(2000),[BillByDefault],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BillableWarning',CONVERT(NVARCHAR(2000),[BillableWarning],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Memo','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BudgetedFlag',CONVERT(NVARCHAR(2000),[BudgetedFlag],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BudgetedLevels',CONVERT(NVARCHAR(2000),[BudgetedLevels],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BillWBS1',CONVERT(NVARCHAR(2000),[BillWBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BillWBS2',CONVERT(NVARCHAR(2000),[BillWBS2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BillWBS3',CONVERT(NVARCHAR(2000),[BillWBS3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'XCharge',CONVERT(NVARCHAR(2000),[XCharge],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'XChargeMethod',CONVERT(NVARCHAR(2000),[XChargeMethod],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'XChargeMult',CONVERT(NVARCHAR(2000),[XChargeMult],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Description',CONVERT(NVARCHAR(2000),[Description],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Closed',CONVERT(NVARCHAR(2000),[Closed],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ReadOnly',CONVERT(NVARCHAR(2000),[ReadOnly],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'DefaultEffortDriven',CONVERT(NVARCHAR(2000),[DefaultEffortDriven],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'DefaultTaskType',CONVERT(NVARCHAR(2000),[DefaultTaskType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'VersionID',CONVERT(NVARCHAR(2000),[VersionID],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ContactID',CONVERT(NVARCHAR(2000),DELETED.[ContactID],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join Contacts as oldDesc  on DELETED.ContactID = oldDesc.ContactID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CLBillingAddr',CONVERT(NVARCHAR(2000),[CLBillingAddr],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'LongName',CONVERT(NVARCHAR(2000),[LongName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Address1',CONVERT(NVARCHAR(2000),[Address1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Address2',CONVERT(NVARCHAR(2000),[Address2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Address3',CONVERT(NVARCHAR(2000),[Address3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'City',CONVERT(NVARCHAR(2000),[City],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'State',CONVERT(NVARCHAR(2000),[State],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Zip',CONVERT(NVARCHAR(2000),[Zip],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'County',CONVERT(NVARCHAR(2000),[County],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Country',CONVERT(NVARCHAR(2000),[Country],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FederalInd',CONVERT(NVARCHAR(2000),[FederalInd],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ProjectType',CONVERT(NVARCHAR(2000),DELETED.[ProjectType],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGProjectType as oldDesc  on DELETED.ProjectType = oldDesc.Code

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Responsibility',CONVERT(NVARCHAR(2000),DELETED.[Responsibility],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGPRResponsibility as oldDesc  on DELETED.Responsibility = oldDesc.Code

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Referable',CONVERT(NVARCHAR(2000),[Referable],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'EstCompletionDate',CONVERT(NVARCHAR(2000),[EstCompletionDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ActCompletionDate',CONVERT(NVARCHAR(2000),[ActCompletionDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ContractDate',CONVERT(NVARCHAR(2000),[ContractDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BidDate',CONVERT(NVARCHAR(2000),[BidDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ComplDateComment',CONVERT(NVARCHAR(2000),[ComplDateComment],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FirmCost',CONVERT(NVARCHAR(2000),[FirmCost],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FirmCostComment',CONVERT(NVARCHAR(2000),[FirmCostComment],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'TotalProjectCost',CONVERT(NVARCHAR(2000),[TotalProjectCost],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'TotalCostComment',CONVERT(NVARCHAR(2000),[TotalCostComment],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'OpportunityID',CONVERT(NVARCHAR(2000),[OpportunityID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ClientConfidential',CONVERT(NVARCHAR(2000),[ClientConfidential],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ClientAlias',CONVERT(NVARCHAR(2000),[ClientAlias],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AvailableForCRM',CONVERT(NVARCHAR(2000),[AvailableForCRM],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ReadyForApproval',CONVERT(NVARCHAR(2000),[ReadyForApproval],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ReadyForProcessing',CONVERT(NVARCHAR(2000),[ReadyForProcessing],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BillingClientID',CONVERT(NVARCHAR(2000),DELETED.[BillingClientID],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join CL as oldDesc  on DELETED.BillingClientID = oldDesc.ClientID

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BillingContactID',CONVERT(NVARCHAR(2000),DELETED.[BillingContactID],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join Contacts as oldDesc  on DELETED.BillingContactID = oldDesc.ContactID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Phone',CONVERT(NVARCHAR(2000),[Phone],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Fax',CONVERT(NVARCHAR(2000),[Fax],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'EMail',CONVERT(NVARCHAR(2000),[EMail],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ProposalWBS1',CONVERT(NVARCHAR(2000),[ProposalWBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CostRateMeth',CONVERT(NVARCHAR(2000),[CostRateMeth],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CostRateTableNo',CONVERT(NVARCHAR(2000),[CostRateTableNo],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'PayRateMeth',CONVERT(NVARCHAR(2000),[PayRateMeth],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'PayRateTableNo',CONVERT(NVARCHAR(2000),[PayRateTableNo],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Locale',CONVERT(NVARCHAR(2000),DELETED.[Locale],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGPYWHCodes as oldDesc  on DELETED.Locale = oldDesc.Code

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'LineItemApproval',CONVERT(NVARCHAR(2000),[LineItemApproval],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'LineItemApprovalEK',CONVERT(NVARCHAR(2000),[LineItemApprovalEK],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BudgetSource',CONVERT(NVARCHAR(2000),[BudgetSource],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BudgetLevel',CONVERT(NVARCHAR(2000),[BudgetLevel],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ProfServicesComplDate',CONVERT(NVARCHAR(2000),[ProfServicesComplDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ConstComplDate',CONVERT(NVARCHAR(2000),[ConstComplDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ProjectCurrencyCode',CONVERT(NVARCHAR(2000),[ProjectCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ProjectExchangeRate',CONVERT(NVARCHAR(2000),[ProjectExchangeRate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BillingCurrencyCode',CONVERT(NVARCHAR(2000),[BillingCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BillingExchangeRate',CONVERT(NVARCHAR(2000),[BillingExchangeRate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RestrictChargeCompanies',CONVERT(NVARCHAR(2000),[RestrictChargeCompanies],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FeeBillingCurrency',CONVERT(NVARCHAR(2000),[FeeBillingCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ReimbAllowBillingCurrency',CONVERT(NVARCHAR(2000),[ReimbAllowBillingCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ConsultFeeBillingCurrency',CONVERT(NVARCHAR(2000),[ConsultFeeBillingCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RevUpsetLimits',CONVERT(NVARCHAR(2000),[RevUpsetLimits],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RevUpsetWBS2',CONVERT(NVARCHAR(2000),[RevUpsetWBS2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RevUpsetWBS3',CONVERT(NVARCHAR(2000),[RevUpsetWBS3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RevUpsetIncludeComp',CONVERT(NVARCHAR(2000),[RevUpsetIncludeComp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RevUpsetIncludeCons',CONVERT(NVARCHAR(2000),[RevUpsetIncludeCons],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RevUpsetIncludeReimb',CONVERT(NVARCHAR(2000),[RevUpsetIncludeReimb],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'PORMBRate',CONVERT(NVARCHAR(2000),[PORMBRate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'POCNSRate',CONVERT(NVARCHAR(2000),[POCNSRate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'PlanID',CONVERT(NVARCHAR(2000),[PlanID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'TKCheckRPDate',CONVERT(NVARCHAR(2000),[TKCheckRPDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ICBillingLab',CONVERT(NVARCHAR(2000),[ICBillingLab],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ICBillingLabMethod',CONVERT(NVARCHAR(2000),[ICBillingLabMethod],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ICBillingLabMult',CONVERT(NVARCHAR(2000),[ICBillingLabMult],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ICBillingExp',CONVERT(NVARCHAR(2000),[ICBillingExp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ICBillingExpMethod',CONVERT(NVARCHAR(2000),[ICBillingExpMethod],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ICBillingExpMult',CONVERT(NVARCHAR(2000),[ICBillingExpMult],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RequireComments',CONVERT(NVARCHAR(2000),[RequireComments],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'TKCheckRPPlannedHrs',CONVERT(NVARCHAR(2000),[TKCheckRPPlannedHrs],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BillByDefaultConsultants',CONVERT(NVARCHAR(2000),[BillByDefaultConsultants],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BillByDefaultOtherExp',CONVERT(NVARCHAR(2000),[BillByDefaultOtherExp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BillByDefaultORTable',CONVERT(NVARCHAR(2000),[BillByDefaultORTable],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'PhoneFormat',CONVERT(NVARCHAR(2000),[PhoneFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FaxFormat',CONVERT(NVARCHAR(2000),[FaxFormat],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RevType2',CONVERT(NVARCHAR(2000),DELETED.[RevType2],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGRGMethods as oldDesc  on DELETED.RevType2 = oldDesc.Method

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RevType3',CONVERT(NVARCHAR(2000),DELETED.[RevType3],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGRGMethods as oldDesc  on DELETED.RevType3 = oldDesc.Method

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RevType4',CONVERT(NVARCHAR(2000),DELETED.[RevType4],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGRGMethods as oldDesc  on DELETED.RevType4 = oldDesc.Method

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RevType5',CONVERT(NVARCHAR(2000),DELETED.[RevType5],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGRGMethods as oldDesc  on DELETED.RevType5 = oldDesc.Method

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RevUpsetCategoryToAdjust',CONVERT(NVARCHAR(2000),[RevUpsetCategoryToAdjust],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FeeFunctionalCurrency',CONVERT(NVARCHAR(2000),[FeeFunctionalCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ReimbAllowFunctionalCurrency',CONVERT(NVARCHAR(2000),[ReimbAllowFunctionalCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ConsultFeeFunctionalCurrency',CONVERT(NVARCHAR(2000),[ConsultFeeFunctionalCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RevenueMethod',CONVERT(NVARCHAR(2000),[RevenueMethod],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ICBillingLabTableNo',CONVERT(NVARCHAR(2000),[ICBillingLabTableNo],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ICBillingExpTableNo',CONVERT(NVARCHAR(2000),[ICBillingExpTableNo],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Biller',CONVERT(NVARCHAR(2000),[Biller],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FeeDirLab',CONVERT(NVARCHAR(2000),[FeeDirLab],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FeeDirExp',CONVERT(NVARCHAR(2000),[FeeDirExp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ReimbAllowExp',CONVERT(NVARCHAR(2000),[ReimbAllowExp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ReimbAllowCons',CONVERT(NVARCHAR(2000),[ReimbAllowCons],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FeeDirLabBillingCurrency',CONVERT(NVARCHAR(2000),[FeeDirLabBillingCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FeeDirExpBillingCurrency',CONVERT(NVARCHAR(2000),[FeeDirExpBillingCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ReimbAllowExpBillingCurrency',CONVERT(NVARCHAR(2000),[ReimbAllowExpBillingCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ReimbAllowConsBillingCurrency',CONVERT(NVARCHAR(2000),[ReimbAllowConsBillingCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FeeDirLabFunctionalCurrency',CONVERT(NVARCHAR(2000),[FeeDirLabFunctionalCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FeeDirExpFunctionalCurrency',CONVERT(NVARCHAR(2000),[FeeDirExpFunctionalCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ReimbAllowExpFunctionalCurrency',CONVERT(NVARCHAR(2000),[ReimbAllowExpFunctionalCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ReimbAllowConsFunctionalCurrency',CONVERT(NVARCHAR(2000),[ReimbAllowConsFunctionalCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RevUpsetIncludeCompDirExp',CONVERT(NVARCHAR(2000),[RevUpsetIncludeCompDirExp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'RevUpsetIncludeReimbCons',CONVERT(NVARCHAR(2000),[RevUpsetIncludeReimbCons],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AwardType',CONVERT(NVARCHAR(2000),[AwardType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Duration',CONVERT(NVARCHAR(2000),[Duration],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ContractTypeGovCon',CONVERT(NVARCHAR(2000),[ContractTypeGovCon],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CompetitionType',CONVERT(NVARCHAR(2000),[CompetitionType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'MasterContract',CONVERT(NVARCHAR(2000),[MasterContract],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Solicitation',CONVERT(NVARCHAR(2000),[Solicitation],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'NAICS',CONVERT(NVARCHAR(2000),[NAICS],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'OurRole',CONVERT(NVARCHAR(2000),[OurRole],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AjeraSync',CONVERT(NVARCHAR(2000),[AjeraSync],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ServProCode',CONVERT(NVARCHAR(2000),[ServProCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FESurchargePct',CONVERT(NVARCHAR(2000),[FESurchargePct],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FESurcharge',CONVERT(NVARCHAR(2000),[FESurcharge],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FEAddlExpensesPct',CONVERT(NVARCHAR(2000),[FEAddlExpensesPct],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FEAddlExpenses',CONVERT(NVARCHAR(2000),[FEAddlExpenses],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FEOtherPct',CONVERT(NVARCHAR(2000),[FEOtherPct],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'FEOther',CONVERT(NVARCHAR(2000),[FEOther],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ProjectTemplate',CONVERT(NVARCHAR(2000),[ProjectTemplate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AjeraSpentLabor',CONVERT(NVARCHAR(2000),[AjeraSpentLabor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AjeraSpentReimbursable',CONVERT(NVARCHAR(2000),[AjeraSpentReimbursable],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AjeraSpentConsultant',CONVERT(NVARCHAR(2000),[AjeraSpentConsultant],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AjeraCostLabor',CONVERT(NVARCHAR(2000),[AjeraCostLabor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AjeraCostReimbursable',CONVERT(NVARCHAR(2000),[AjeraCostReimbursable],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AjeraCostConsultant',CONVERT(NVARCHAR(2000),[AjeraCostConsultant],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AjeraWIPLabor',CONVERT(NVARCHAR(2000),[AjeraWIPLabor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AjeraWIPReimbursable',CONVERT(NVARCHAR(2000),[AjeraWIPReimbursable],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AjeraWIPConsultant',CONVERT(NVARCHAR(2000),[AjeraWIPConsultant],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AjeraBilledLabor',CONVERT(NVARCHAR(2000),[AjeraBilledLabor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AjeraBilledReimbursable',CONVERT(NVARCHAR(2000),[AjeraBilledReimbursable],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AjeraBilledConsultant',CONVERT(NVARCHAR(2000),[AjeraBilledConsultant],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AjeraReceivedLabor',CONVERT(NVARCHAR(2000),[AjeraReceivedLabor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AjeraReceivedReimbursable',CONVERT(NVARCHAR(2000),[AjeraReceivedReimbursable],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AjeraReceivedConsultant',CONVERT(NVARCHAR(2000),[AjeraReceivedConsultant],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'TLInternalKey',CONVERT(NVARCHAR(2000),[TLInternalKey],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'TLProjectID',CONVERT(NVARCHAR(2000),[TLProjectID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'TLProjectName',CONVERT(NVARCHAR(2000),[TLProjectName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'TLChargeBandInternalKey',CONVERT(NVARCHAR(2000),[TLChargeBandInternalKey],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'TLChargeBandExternalCode',CONVERT(NVARCHAR(2000),[TLChargeBandExternalCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'TLSyncModDate',CONVERT(NVARCHAR(2000),[TLSyncModDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'SiblingWBS1',CONVERT(NVARCHAR(2000),[SiblingWBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Stage',CONVERT(NVARCHAR(2000),[Stage],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'EstStartDate',CONVERT(NVARCHAR(2000),[EstStartDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'EstEndDate',CONVERT(NVARCHAR(2000),[EstEndDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'EstFees',CONVERT(NVARCHAR(2000),[EstFees],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'EstConstructionCost',CONVERT(NVARCHAR(2000),[EstConstructionCost],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Revenue',CONVERT(NVARCHAR(2000),[Revenue],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Probability',CONVERT(NVARCHAR(2000),[Probability],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'WeightedRevenue',CONVERT(NVARCHAR(2000),[WeightedRevenue],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CloseDate',CONVERT(NVARCHAR(2000),[CloseDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'OpenDate',CONVERT(NVARCHAR(2000),[OpenDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Source',CONVERT(NVARCHAR(2000),[Source],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'PublicNoticeDate',CONVERT(NVARCHAR(2000),[PublicNoticeDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'SolicitationNum',CONVERT(NVARCHAR(2000),[SolicitationNum],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustomCurrencyCode',CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'LabBillTable',CONVERT(NVARCHAR(2000),[LabBillTable],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'LabCostTable',CONVERT(NVARCHAR(2000),[LabCostTable],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'AllocMethod',CONVERT(NVARCHAR(2000),[AllocMethod],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'Timescale',CONVERT(NVARCHAR(2000),[Timescale],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ClosedReason',CONVERT(NVARCHAR(2000),[ClosedReason],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ClosedNotes','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'IQID',CONVERT(NVARCHAR(2000),[IQID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'IQLastUpdate',CONVERT(NVARCHAR(2000),[IQLastUpdate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'MarketingCoordinator',CONVERT(NVARCHAR(2000),[MarketingCoordinator],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'ProposalManager',CONVERT(NVARCHAR(2000),[ProposalManager],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'BusinessDeveloperLead',CONVERT(NVARCHAR(2000),[BusinessDeveloperLead],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'SFID',CONVERT(NVARCHAR(2000),[SFID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'SFLastModifiedDate',CONVERT(NVARCHAR(2000),[SFLastModifiedDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'TotalContractValue',CONVERT(NVARCHAR(2000),[TotalContractValue],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'PeriodOfPerformance',CONVERT(NVARCHAR(2000),[PeriodOfPerformance],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'LostTo',CONVERT(NVARCHAR(2000),[LostTo],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'PreAwardWBS1',CONVERT(NVARCHAR(2000),[PreAwardWBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'UtilizationScheduleFlg',CONVERT(NVARCHAR(2000),[UtilizationScheduleFlg],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'PIMID',CONVERT(NVARCHAR(2000),[PIMID],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_PRTemplate] ON [dbo].[PRTemplate]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_PRTemplate]
      ON [dbo].[PRTemplate]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'PRTemplate'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'WBS1',NULL,CONVERT(NVARCHAR(2000),[WBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'WBS2',NULL,CONVERT(NVARCHAR(2000),[WBS2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'WBS3',NULL,CONVERT(NVARCHAR(2000),[WBS3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Name',NULL,CONVERT(NVARCHAR(2000),[Name],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ChargeType',NULL,CONVERT(NVARCHAR(2000),INSERTED.[ChargeType],121), NULL, newDesc.Label, @source, @app
       FROM INSERTED left join  CFGChargeType as newDesc  on INSERTED.ChargeType = newDesc.Type

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'SubLevel',NULL,CONVERT(NVARCHAR(2000),[SubLevel],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Principal',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Principal],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Principal = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ProjMgr',NULL,CONVERT(NVARCHAR(2000),INSERTED.[ProjMgr],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.ProjMgr = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Supervisor',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Supervisor],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Supervisor = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ClientID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[ClientID],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  CL as newDesc  on INSERTED.ClientID = newDesc.ClientID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CLAddress',NULL,CONVERT(NVARCHAR(2000),[CLAddress],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Fee',NULL,CONVERT(NVARCHAR(2000),[Fee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllow',NULL,CONVERT(NVARCHAR(2000),[ReimbAllow],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ConsultFee',NULL,CONVERT(NVARCHAR(2000),[ConsultFee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BudOHRate',NULL,CONVERT(NVARCHAR(2000),[BudOHRate],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Status',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Status],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGProjectStatus as newDesc  on INSERTED.Status = newDesc.Code

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevType',NULL,CONVERT(NVARCHAR(2000),INSERTED.[RevType],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGRGMethods as newDesc  on INSERTED.RevType = newDesc.Method

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'MultAmt',NULL,CONVERT(NVARCHAR(2000),[MultAmt],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Org',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Org],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  Organization as newDesc  on INSERTED.Org = newDesc.Org

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'UnitTable',NULL,CONVERT(NVARCHAR(2000),[UnitTable],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'StartDate',NULL,CONVERT(NVARCHAR(2000),[StartDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'EndDate',NULL,CONVERT(NVARCHAR(2000),[EndDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PctComp',NULL,CONVERT(NVARCHAR(2000),[PctComp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'LabPctComp',NULL,CONVERT(NVARCHAR(2000),[LabPctComp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ExpPctComp',NULL,CONVERT(NVARCHAR(2000),[ExpPctComp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillByDefault',NULL,CONVERT(NVARCHAR(2000),[BillByDefault],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillableWarning',NULL,CONVERT(NVARCHAR(2000),[BillableWarning],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Memo',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BudgetedFlag',NULL,CONVERT(NVARCHAR(2000),[BudgetedFlag],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BudgetedLevels',NULL,CONVERT(NVARCHAR(2000),[BudgetedLevels],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillWBS1',NULL,CONVERT(NVARCHAR(2000),[BillWBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillWBS2',NULL,CONVERT(NVARCHAR(2000),[BillWBS2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillWBS3',NULL,CONVERT(NVARCHAR(2000),[BillWBS3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'XCharge',NULL,CONVERT(NVARCHAR(2000),[XCharge],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'XChargeMethod',NULL,CONVERT(NVARCHAR(2000),[XChargeMethod],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'XChargeMult',NULL,CONVERT(NVARCHAR(2000),[XChargeMult],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Description',NULL,CONVERT(NVARCHAR(2000),[Description],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Closed',NULL,CONVERT(NVARCHAR(2000),[Closed],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReadOnly',NULL,CONVERT(NVARCHAR(2000),[ReadOnly],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'DefaultEffortDriven',NULL,CONVERT(NVARCHAR(2000),[DefaultEffortDriven],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'DefaultTaskType',NULL,CONVERT(NVARCHAR(2000),[DefaultTaskType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'VersionID',NULL,CONVERT(NVARCHAR(2000),[VersionID],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ContactID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[ContactID],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  Contacts as newDesc  on INSERTED.ContactID = newDesc.ContactID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CLBillingAddr',NULL,CONVERT(NVARCHAR(2000),[CLBillingAddr],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'LongName',NULL,CONVERT(NVARCHAR(2000),[LongName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Address1',NULL,CONVERT(NVARCHAR(2000),[Address1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Address2',NULL,CONVERT(NVARCHAR(2000),[Address2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Address3',NULL,CONVERT(NVARCHAR(2000),[Address3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'City',NULL,CONVERT(NVARCHAR(2000),[City],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'State',NULL,CONVERT(NVARCHAR(2000),[State],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Zip',NULL,CONVERT(NVARCHAR(2000),[Zip],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'County',NULL,CONVERT(NVARCHAR(2000),[County],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Country',NULL,CONVERT(NVARCHAR(2000),[Country],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FederalInd',NULL,CONVERT(NVARCHAR(2000),[FederalInd],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ProjectType',NULL,CONVERT(NVARCHAR(2000),INSERTED.[ProjectType],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGProjectType as newDesc  on INSERTED.ProjectType = newDesc.Code

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Responsibility',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Responsibility],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGPRResponsibility as newDesc  on INSERTED.Responsibility = newDesc.Code

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Referable',NULL,CONVERT(NVARCHAR(2000),[Referable],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'EstCompletionDate',NULL,CONVERT(NVARCHAR(2000),[EstCompletionDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ActCompletionDate',NULL,CONVERT(NVARCHAR(2000),[ActCompletionDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ContractDate',NULL,CONVERT(NVARCHAR(2000),[ContractDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BidDate',NULL,CONVERT(NVARCHAR(2000),[BidDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ComplDateComment',NULL,CONVERT(NVARCHAR(2000),[ComplDateComment],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FirmCost',NULL,CONVERT(NVARCHAR(2000),[FirmCost],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FirmCostComment',NULL,CONVERT(NVARCHAR(2000),[FirmCostComment],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TotalProjectCost',NULL,CONVERT(NVARCHAR(2000),[TotalProjectCost],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TotalCostComment',NULL,CONVERT(NVARCHAR(2000),[TotalCostComment],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'OpportunityID',NULL,CONVERT(NVARCHAR(2000),[OpportunityID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ClientConfidential',NULL,CONVERT(NVARCHAR(2000),[ClientConfidential],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ClientAlias',NULL,CONVERT(NVARCHAR(2000),[ClientAlias],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AvailableForCRM',NULL,CONVERT(NVARCHAR(2000),[AvailableForCRM],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReadyForApproval',NULL,CONVERT(NVARCHAR(2000),[ReadyForApproval],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReadyForProcessing',NULL,CONVERT(NVARCHAR(2000),[ReadyForProcessing],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillingClientID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[BillingClientID],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  CL as newDesc  on INSERTED.BillingClientID = newDesc.ClientID

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillingContactID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[BillingContactID],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  Contacts as newDesc  on INSERTED.BillingContactID = newDesc.ContactID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Phone',NULL,CONVERT(NVARCHAR(2000),[Phone],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Fax',NULL,CONVERT(NVARCHAR(2000),[Fax],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'EMail',NULL,CONVERT(NVARCHAR(2000),[EMail],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ProposalWBS1',NULL,CONVERT(NVARCHAR(2000),[ProposalWBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CostRateMeth',NULL,CONVERT(NVARCHAR(2000),[CostRateMeth],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CostRateTableNo',NULL,CONVERT(NVARCHAR(2000),[CostRateTableNo],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PayRateMeth',NULL,CONVERT(NVARCHAR(2000),[PayRateMeth],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PayRateTableNo',NULL,CONVERT(NVARCHAR(2000),[PayRateTableNo],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Locale',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Locale],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGPYWHCodes as newDesc  on INSERTED.Locale = newDesc.Code

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'LineItemApproval',NULL,CONVERT(NVARCHAR(2000),[LineItemApproval],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'LineItemApprovalEK',NULL,CONVERT(NVARCHAR(2000),[LineItemApprovalEK],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BudgetSource',NULL,CONVERT(NVARCHAR(2000),[BudgetSource],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BudgetLevel',NULL,CONVERT(NVARCHAR(2000),[BudgetLevel],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ProfServicesComplDate',NULL,CONVERT(NVARCHAR(2000),[ProfServicesComplDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ConstComplDate',NULL,CONVERT(NVARCHAR(2000),[ConstComplDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ProjectCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[ProjectCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ProjectExchangeRate',NULL,CONVERT(NVARCHAR(2000),[ProjectExchangeRate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillingCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[BillingCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillingExchangeRate',NULL,CONVERT(NVARCHAR(2000),[BillingExchangeRate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RestrictChargeCompanies',NULL,CONVERT(NVARCHAR(2000),[RestrictChargeCompanies],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FeeBillingCurrency',NULL,CONVERT(NVARCHAR(2000),[FeeBillingCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllowBillingCurrency',NULL,CONVERT(NVARCHAR(2000),[ReimbAllowBillingCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ConsultFeeBillingCurrency',NULL,CONVERT(NVARCHAR(2000),[ConsultFeeBillingCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetLimits',NULL,CONVERT(NVARCHAR(2000),[RevUpsetLimits],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetWBS2',NULL,CONVERT(NVARCHAR(2000),[RevUpsetWBS2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetWBS3',NULL,CONVERT(NVARCHAR(2000),[RevUpsetWBS3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetIncludeComp',NULL,CONVERT(NVARCHAR(2000),[RevUpsetIncludeComp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetIncludeCons',NULL,CONVERT(NVARCHAR(2000),[RevUpsetIncludeCons],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetIncludeReimb',NULL,CONVERT(NVARCHAR(2000),[RevUpsetIncludeReimb],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PORMBRate',NULL,CONVERT(NVARCHAR(2000),[PORMBRate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'POCNSRate',NULL,CONVERT(NVARCHAR(2000),[POCNSRate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PlanID',NULL,CONVERT(NVARCHAR(2000),[PlanID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TKCheckRPDate',NULL,CONVERT(NVARCHAR(2000),[TKCheckRPDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ICBillingLab',NULL,CONVERT(NVARCHAR(2000),[ICBillingLab],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ICBillingLabMethod',NULL,CONVERT(NVARCHAR(2000),[ICBillingLabMethod],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ICBillingLabMult',NULL,CONVERT(NVARCHAR(2000),[ICBillingLabMult],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ICBillingExp',NULL,CONVERT(NVARCHAR(2000),[ICBillingExp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ICBillingExpMethod',NULL,CONVERT(NVARCHAR(2000),[ICBillingExpMethod],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ICBillingExpMult',NULL,CONVERT(NVARCHAR(2000),[ICBillingExpMult],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RequireComments',NULL,CONVERT(NVARCHAR(2000),[RequireComments],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TKCheckRPPlannedHrs',NULL,CONVERT(NVARCHAR(2000),[TKCheckRPPlannedHrs],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillByDefaultConsultants',NULL,CONVERT(NVARCHAR(2000),[BillByDefaultConsultants],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillByDefaultOtherExp',NULL,CONVERT(NVARCHAR(2000),[BillByDefaultOtherExp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillByDefaultORTable',NULL,CONVERT(NVARCHAR(2000),[BillByDefaultORTable],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PhoneFormat',NULL,CONVERT(NVARCHAR(2000),[PhoneFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FaxFormat',NULL,CONVERT(NVARCHAR(2000),[FaxFormat],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevType2',NULL,CONVERT(NVARCHAR(2000),INSERTED.[RevType2],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGRGMethods as newDesc  on INSERTED.RevType2 = newDesc.Method

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevType3',NULL,CONVERT(NVARCHAR(2000),INSERTED.[RevType3],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGRGMethods as newDesc  on INSERTED.RevType3 = newDesc.Method

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevType4',NULL,CONVERT(NVARCHAR(2000),INSERTED.[RevType4],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGRGMethods as newDesc  on INSERTED.RevType4 = newDesc.Method

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevType5',NULL,CONVERT(NVARCHAR(2000),INSERTED.[RevType5],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGRGMethods as newDesc  on INSERTED.RevType5 = newDesc.Method

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetCategoryToAdjust',NULL,CONVERT(NVARCHAR(2000),[RevUpsetCategoryToAdjust],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FeeFunctionalCurrency',NULL,CONVERT(NVARCHAR(2000),[FeeFunctionalCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllowFunctionalCurrency',NULL,CONVERT(NVARCHAR(2000),[ReimbAllowFunctionalCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ConsultFeeFunctionalCurrency',NULL,CONVERT(NVARCHAR(2000),[ConsultFeeFunctionalCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevenueMethod',NULL,CONVERT(NVARCHAR(2000),[RevenueMethod],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ICBillingLabTableNo',NULL,CONVERT(NVARCHAR(2000),[ICBillingLabTableNo],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ICBillingExpTableNo',NULL,CONVERT(NVARCHAR(2000),[ICBillingExpTableNo],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Biller',NULL,CONVERT(NVARCHAR(2000),[Biller],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FeeDirLab',NULL,CONVERT(NVARCHAR(2000),[FeeDirLab],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FeeDirExp',NULL,CONVERT(NVARCHAR(2000),[FeeDirExp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllowExp',NULL,CONVERT(NVARCHAR(2000),[ReimbAllowExp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllowCons',NULL,CONVERT(NVARCHAR(2000),[ReimbAllowCons],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FeeDirLabBillingCurrency',NULL,CONVERT(NVARCHAR(2000),[FeeDirLabBillingCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FeeDirExpBillingCurrency',NULL,CONVERT(NVARCHAR(2000),[FeeDirExpBillingCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllowExpBillingCurrency',NULL,CONVERT(NVARCHAR(2000),[ReimbAllowExpBillingCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllowConsBillingCurrency',NULL,CONVERT(NVARCHAR(2000),[ReimbAllowConsBillingCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FeeDirLabFunctionalCurrency',NULL,CONVERT(NVARCHAR(2000),[FeeDirLabFunctionalCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FeeDirExpFunctionalCurrency',NULL,CONVERT(NVARCHAR(2000),[FeeDirExpFunctionalCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllowExpFunctionalCurrency',NULL,CONVERT(NVARCHAR(2000),[ReimbAllowExpFunctionalCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllowConsFunctionalCurrency',NULL,CONVERT(NVARCHAR(2000),[ReimbAllowConsFunctionalCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetIncludeCompDirExp',NULL,CONVERT(NVARCHAR(2000),[RevUpsetIncludeCompDirExp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetIncludeReimbCons',NULL,CONVERT(NVARCHAR(2000),[RevUpsetIncludeReimbCons],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AwardType',NULL,CONVERT(NVARCHAR(2000),[AwardType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Duration',NULL,CONVERT(NVARCHAR(2000),[Duration],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ContractTypeGovCon',NULL,CONVERT(NVARCHAR(2000),[ContractTypeGovCon],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CompetitionType',NULL,CONVERT(NVARCHAR(2000),[CompetitionType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'MasterContract',NULL,CONVERT(NVARCHAR(2000),[MasterContract],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Solicitation',NULL,CONVERT(NVARCHAR(2000),[Solicitation],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'NAICS',NULL,CONVERT(NVARCHAR(2000),[NAICS],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'OurRole',NULL,CONVERT(NVARCHAR(2000),[OurRole],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraSync',NULL,CONVERT(NVARCHAR(2000),[AjeraSync],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ServProCode',NULL,CONVERT(NVARCHAR(2000),[ServProCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FESurchargePct',NULL,CONVERT(NVARCHAR(2000),[FESurchargePct],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FESurcharge',NULL,CONVERT(NVARCHAR(2000),[FESurcharge],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FEAddlExpensesPct',NULL,CONVERT(NVARCHAR(2000),[FEAddlExpensesPct],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FEAddlExpenses',NULL,CONVERT(NVARCHAR(2000),[FEAddlExpenses],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FEOtherPct',NULL,CONVERT(NVARCHAR(2000),[FEOtherPct],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FEOther',NULL,CONVERT(NVARCHAR(2000),[FEOther],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ProjectTemplate',NULL,CONVERT(NVARCHAR(2000),[ProjectTemplate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraSpentLabor',NULL,CONVERT(NVARCHAR(2000),[AjeraSpentLabor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraSpentReimbursable',NULL,CONVERT(NVARCHAR(2000),[AjeraSpentReimbursable],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraSpentConsultant',NULL,CONVERT(NVARCHAR(2000),[AjeraSpentConsultant],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraCostLabor',NULL,CONVERT(NVARCHAR(2000),[AjeraCostLabor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraCostReimbursable',NULL,CONVERT(NVARCHAR(2000),[AjeraCostReimbursable],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraCostConsultant',NULL,CONVERT(NVARCHAR(2000),[AjeraCostConsultant],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraWIPLabor',NULL,CONVERT(NVARCHAR(2000),[AjeraWIPLabor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraWIPReimbursable',NULL,CONVERT(NVARCHAR(2000),[AjeraWIPReimbursable],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraWIPConsultant',NULL,CONVERT(NVARCHAR(2000),[AjeraWIPConsultant],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraBilledLabor',NULL,CONVERT(NVARCHAR(2000),[AjeraBilledLabor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraBilledReimbursable',NULL,CONVERT(NVARCHAR(2000),[AjeraBilledReimbursable],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraBilledConsultant',NULL,CONVERT(NVARCHAR(2000),[AjeraBilledConsultant],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraReceivedLabor',NULL,CONVERT(NVARCHAR(2000),[AjeraReceivedLabor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraReceivedReimbursable',NULL,CONVERT(NVARCHAR(2000),[AjeraReceivedReimbursable],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraReceivedConsultant',NULL,CONVERT(NVARCHAR(2000),[AjeraReceivedConsultant],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TLInternalKey',NULL,CONVERT(NVARCHAR(2000),[TLInternalKey],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TLProjectID',NULL,CONVERT(NVARCHAR(2000),[TLProjectID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TLProjectName',NULL,CONVERT(NVARCHAR(2000),[TLProjectName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TLChargeBandInternalKey',NULL,CONVERT(NVARCHAR(2000),[TLChargeBandInternalKey],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TLChargeBandExternalCode',NULL,CONVERT(NVARCHAR(2000),[TLChargeBandExternalCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TLSyncModDate',NULL,CONVERT(NVARCHAR(2000),[TLSyncModDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'SiblingWBS1',NULL,CONVERT(NVARCHAR(2000),[SiblingWBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Stage',NULL,CONVERT(NVARCHAR(2000),[Stage],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'EstStartDate',NULL,CONVERT(NVARCHAR(2000),[EstStartDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'EstEndDate',NULL,CONVERT(NVARCHAR(2000),[EstEndDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'EstFees',NULL,CONVERT(NVARCHAR(2000),[EstFees],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'EstConstructionCost',NULL,CONVERT(NVARCHAR(2000),[EstConstructionCost],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Revenue',NULL,CONVERT(NVARCHAR(2000),[Revenue],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Probability',NULL,CONVERT(NVARCHAR(2000),[Probability],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'WeightedRevenue',NULL,CONVERT(NVARCHAR(2000),[WeightedRevenue],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CloseDate',NULL,CONVERT(NVARCHAR(2000),[CloseDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'OpenDate',NULL,CONVERT(NVARCHAR(2000),[OpenDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Source',NULL,CONVERT(NVARCHAR(2000),[Source],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PublicNoticeDate',NULL,CONVERT(NVARCHAR(2000),[PublicNoticeDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'SolicitationNum',NULL,CONVERT(NVARCHAR(2000),[SolicitationNum],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustomCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'LabBillTable',NULL,CONVERT(NVARCHAR(2000),[LabBillTable],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'LabCostTable',NULL,CONVERT(NVARCHAR(2000),[LabCostTable],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AllocMethod',NULL,CONVERT(NVARCHAR(2000),[AllocMethod],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Timescale',NULL,CONVERT(NVARCHAR(2000),[Timescale],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ClosedReason',NULL,CONVERT(NVARCHAR(2000),[ClosedReason],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ClosedNotes',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'IQID',NULL,CONVERT(NVARCHAR(2000),[IQID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'IQLastUpdate',NULL,CONVERT(NVARCHAR(2000),[IQLastUpdate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'MarketingCoordinator',NULL,CONVERT(NVARCHAR(2000),[MarketingCoordinator],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ProposalManager',NULL,CONVERT(NVARCHAR(2000),[ProposalManager],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BusinessDeveloperLead',NULL,CONVERT(NVARCHAR(2000),[BusinessDeveloperLead],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'SFID',NULL,CONVERT(NVARCHAR(2000),[SFID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'SFLastModifiedDate',NULL,CONVERT(NVARCHAR(2000),[SFLastModifiedDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TotalContractValue',NULL,CONVERT(NVARCHAR(2000),[TotalContractValue],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PeriodOfPerformance',NULL,CONVERT(NVARCHAR(2000),[PeriodOfPerformance],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'LostTo',NULL,CONVERT(NVARCHAR(2000),[LostTo],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PreAwardWBS1',NULL,CONVERT(NVARCHAR(2000),[PreAwardWBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'UtilizationScheduleFlg',NULL,CONVERT(NVARCHAR(2000),[UtilizationScheduleFlg],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PIMID',NULL,CONVERT(NVARCHAR(2000),[PIMID],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_PRTemplate] ON [dbo].[PRTemplate]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_PRTemplate]
      ON [dbo].[PRTemplate]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'PRTemplate'
    
      If UPDATE([WBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'WBS1',
      CONVERT(NVARCHAR(2000),DELETED.[WBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[WBS1] Is Null And
				DELETED.[WBS1] Is Not Null
			) Or
			(
				INSERTED.[WBS1] Is Not Null And
				DELETED.[WBS1] Is Null
			) Or
			(
				INSERTED.[WBS1] !=
				DELETED.[WBS1]
			)
		) 
		END		
		
      If UPDATE([WBS2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'WBS2',
      CONVERT(NVARCHAR(2000),DELETED.[WBS2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[WBS2] Is Null And
				DELETED.[WBS2] Is Not Null
			) Or
			(
				INSERTED.[WBS2] Is Not Null And
				DELETED.[WBS2] Is Null
			) Or
			(
				INSERTED.[WBS2] !=
				DELETED.[WBS2]
			)
		) 
		END		
		
      If UPDATE([WBS3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'WBS3',
      CONVERT(NVARCHAR(2000),DELETED.[WBS3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[WBS3] Is Null And
				DELETED.[WBS3] Is Not Null
			) Or
			(
				INSERTED.[WBS3] Is Not Null And
				DELETED.[WBS3] Is Null
			) Or
			(
				INSERTED.[WBS3] !=
				DELETED.[WBS3]
			)
		) 
		END		
		
      If UPDATE([Name])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Name',
      CONVERT(NVARCHAR(2000),DELETED.[Name],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Name],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Name] Is Null And
				DELETED.[Name] Is Not Null
			) Or
			(
				INSERTED.[Name] Is Not Null And
				DELETED.[Name] Is Null
			) Or
			(
				INSERTED.[Name] !=
				DELETED.[Name]
			)
		) 
		END		
		
     If UPDATE([ChargeType])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ChargeType',
     CONVERT(NVARCHAR(2000),DELETED.[ChargeType],121),
     CONVERT(NVARCHAR(2000),INSERTED.[ChargeType],121),
     oldDesc.Label, newDesc.Label, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ChargeType] Is Null And
				DELETED.[ChargeType] Is Not Null
			) Or
			(
				INSERTED.[ChargeType] Is Not Null And
				DELETED.[ChargeType] Is Null
			) Or
			(
				INSERTED.[ChargeType] !=
				DELETED.[ChargeType]
			)
		) left join CFGChargeType as oldDesc  on DELETED.ChargeType = oldDesc.Type  left join  CFGChargeType as newDesc  on INSERTED.ChargeType = newDesc.Type
		END		
		
      If UPDATE([SubLevel])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'SubLevel',
      CONVERT(NVARCHAR(2000),DELETED.[SubLevel],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SubLevel],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[SubLevel] Is Null And
				DELETED.[SubLevel] Is Not Null
			) Or
			(
				INSERTED.[SubLevel] Is Not Null And
				DELETED.[SubLevel] Is Null
			) Or
			(
				INSERTED.[SubLevel] !=
				DELETED.[SubLevel]
			)
		) 
		END		
		
     If UPDATE([Principal])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Principal',
     CONVERT(NVARCHAR(2000),DELETED.[Principal],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Principal],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Principal] Is Null And
				DELETED.[Principal] Is Not Null
			) Or
			(
				INSERTED.[Principal] Is Not Null And
				DELETED.[Principal] Is Null
			) Or
			(
				INSERTED.[Principal] !=
				DELETED.[Principal]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Principal = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Principal = newDesc.Employee
		END		
		
     If UPDATE([ProjMgr])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ProjMgr',
     CONVERT(NVARCHAR(2000),DELETED.[ProjMgr],121),
     CONVERT(NVARCHAR(2000),INSERTED.[ProjMgr],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ProjMgr] Is Null And
				DELETED.[ProjMgr] Is Not Null
			) Or
			(
				INSERTED.[ProjMgr] Is Not Null And
				DELETED.[ProjMgr] Is Null
			) Or
			(
				INSERTED.[ProjMgr] !=
				DELETED.[ProjMgr]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.ProjMgr = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.ProjMgr = newDesc.Employee
		END		
		
     If UPDATE([Supervisor])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Supervisor',
     CONVERT(NVARCHAR(2000),DELETED.[Supervisor],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Supervisor],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Supervisor] Is Null And
				DELETED.[Supervisor] Is Not Null
			) Or
			(
				INSERTED.[Supervisor] Is Not Null And
				DELETED.[Supervisor] Is Null
			) Or
			(
				INSERTED.[Supervisor] !=
				DELETED.[Supervisor]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.Supervisor = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.Supervisor = newDesc.Employee
		END		
		
     If UPDATE([ClientID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ClientID',
     CONVERT(NVARCHAR(2000),DELETED.[ClientID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[ClientID],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ClientID] Is Null And
				DELETED.[ClientID] Is Not Null
			) Or
			(
				INSERTED.[ClientID] Is Not Null And
				DELETED.[ClientID] Is Null
			) Or
			(
				INSERTED.[ClientID] !=
				DELETED.[ClientID]
			)
		) left join CL as oldDesc  on DELETED.ClientID = oldDesc.ClientID  left join  CL as newDesc  on INSERTED.ClientID = newDesc.ClientID
		END		
		
      If UPDATE([CLAddress])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CLAddress',
      CONVERT(NVARCHAR(2000),DELETED.[CLAddress],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CLAddress],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CLAddress] Is Null And
				DELETED.[CLAddress] Is Not Null
			) Or
			(
				INSERTED.[CLAddress] Is Not Null And
				DELETED.[CLAddress] Is Null
			) Or
			(
				INSERTED.[CLAddress] !=
				DELETED.[CLAddress]
			)
		) 
		END		
		
      If UPDATE([Fee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Fee',
      CONVERT(NVARCHAR(2000),DELETED.[Fee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Fee] Is Null And
				DELETED.[Fee] Is Not Null
			) Or
			(
				INSERTED.[Fee] Is Not Null And
				DELETED.[Fee] Is Null
			) Or
			(
				INSERTED.[Fee] !=
				DELETED.[Fee]
			)
		) 
		END		
		
      If UPDATE([ReimbAllow])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllow',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllow],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllow],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ReimbAllow] Is Null And
				DELETED.[ReimbAllow] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllow] Is Not Null And
				DELETED.[ReimbAllow] Is Null
			) Or
			(
				INSERTED.[ReimbAllow] !=
				DELETED.[ReimbAllow]
			)
		) 
		END		
		
      If UPDATE([ConsultFee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ConsultFee',
      CONVERT(NVARCHAR(2000),DELETED.[ConsultFee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ConsultFee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ConsultFee] Is Null And
				DELETED.[ConsultFee] Is Not Null
			) Or
			(
				INSERTED.[ConsultFee] Is Not Null And
				DELETED.[ConsultFee] Is Null
			) Or
			(
				INSERTED.[ConsultFee] !=
				DELETED.[ConsultFee]
			)
		) 
		END		
		
      If UPDATE([BudOHRate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BudOHRate',
      CONVERT(NVARCHAR(2000),DELETED.[BudOHRate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BudOHRate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BudOHRate] Is Null And
				DELETED.[BudOHRate] Is Not Null
			) Or
			(
				INSERTED.[BudOHRate] Is Not Null And
				DELETED.[BudOHRate] Is Null
			) Or
			(
				INSERTED.[BudOHRate] !=
				DELETED.[BudOHRate]
			)
		) 
		END		
		
     If UPDATE([Status])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Status',
     CONVERT(NVARCHAR(2000),DELETED.[Status],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Status],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Status] Is Null And
				DELETED.[Status] Is Not Null
			) Or
			(
				INSERTED.[Status] Is Not Null And
				DELETED.[Status] Is Null
			) Or
			(
				INSERTED.[Status] !=
				DELETED.[Status]
			)
		) left join CFGProjectStatus as oldDesc  on DELETED.Status = oldDesc.Code  left join  CFGProjectStatus as newDesc  on INSERTED.Status = newDesc.Code
		END		
		
     If UPDATE([RevType])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevType',
     CONVERT(NVARCHAR(2000),DELETED.[RevType],121),
     CONVERT(NVARCHAR(2000),INSERTED.[RevType],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RevType] Is Null And
				DELETED.[RevType] Is Not Null
			) Or
			(
				INSERTED.[RevType] Is Not Null And
				DELETED.[RevType] Is Null
			) Or
			(
				INSERTED.[RevType] !=
				DELETED.[RevType]
			)
		) left join CFGRGMethods as oldDesc  on DELETED.RevType = oldDesc.Method  left join  CFGRGMethods as newDesc  on INSERTED.RevType = newDesc.Method
		END		
		
      If UPDATE([MultAmt])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'MultAmt',
      CONVERT(NVARCHAR(2000),DELETED.[MultAmt],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MultAmt],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[MultAmt] Is Null And
				DELETED.[MultAmt] Is Not Null
			) Or
			(
				INSERTED.[MultAmt] Is Not Null And
				DELETED.[MultAmt] Is Null
			) Or
			(
				INSERTED.[MultAmt] !=
				DELETED.[MultAmt]
			)
		) 
		END		
		
     If UPDATE([Org])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Org',
     CONVERT(NVARCHAR(2000),DELETED.[Org],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Org],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Org] Is Null And
				DELETED.[Org] Is Not Null
			) Or
			(
				INSERTED.[Org] Is Not Null And
				DELETED.[Org] Is Null
			) Or
			(
				INSERTED.[Org] !=
				DELETED.[Org]
			)
		) left join Organization as oldDesc  on DELETED.Org = oldDesc.Org  left join  Organization as newDesc  on INSERTED.Org = newDesc.Org
		END		
		
      If UPDATE([UnitTable])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'UnitTable',
      CONVERT(NVARCHAR(2000),DELETED.[UnitTable],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UnitTable],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[UnitTable] Is Null And
				DELETED.[UnitTable] Is Not Null
			) Or
			(
				INSERTED.[UnitTable] Is Not Null And
				DELETED.[UnitTable] Is Null
			) Or
			(
				INSERTED.[UnitTable] !=
				DELETED.[UnitTable]
			)
		) 
		END		
		
      If UPDATE([StartDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'StartDate',
      CONVERT(NVARCHAR(2000),DELETED.[StartDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[StartDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[StartDate] Is Null And
				DELETED.[StartDate] Is Not Null
			) Or
			(
				INSERTED.[StartDate] Is Not Null And
				DELETED.[StartDate] Is Null
			) Or
			(
				INSERTED.[StartDate] !=
				DELETED.[StartDate]
			)
		) 
		END		
		
      If UPDATE([EndDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'EndDate',
      CONVERT(NVARCHAR(2000),DELETED.[EndDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EndDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[EndDate] Is Null And
				DELETED.[EndDate] Is Not Null
			) Or
			(
				INSERTED.[EndDate] Is Not Null And
				DELETED.[EndDate] Is Null
			) Or
			(
				INSERTED.[EndDate] !=
				DELETED.[EndDate]
			)
		) 
		END		
		
      If UPDATE([PctComp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PctComp',
      CONVERT(NVARCHAR(2000),DELETED.[PctComp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[PctComp] Is Null And
				DELETED.[PctComp] Is Not Null
			) Or
			(
				INSERTED.[PctComp] Is Not Null And
				DELETED.[PctComp] Is Null
			) Or
			(
				INSERTED.[PctComp] !=
				DELETED.[PctComp]
			)
		) 
		END		
		
      If UPDATE([LabPctComp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'LabPctComp',
      CONVERT(NVARCHAR(2000),DELETED.[LabPctComp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LabPctComp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[LabPctComp] Is Null And
				DELETED.[LabPctComp] Is Not Null
			) Or
			(
				INSERTED.[LabPctComp] Is Not Null And
				DELETED.[LabPctComp] Is Null
			) Or
			(
				INSERTED.[LabPctComp] !=
				DELETED.[LabPctComp]
			)
		) 
		END		
		
      If UPDATE([ExpPctComp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ExpPctComp',
      CONVERT(NVARCHAR(2000),DELETED.[ExpPctComp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ExpPctComp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ExpPctComp] Is Null And
				DELETED.[ExpPctComp] Is Not Null
			) Or
			(
				INSERTED.[ExpPctComp] Is Not Null And
				DELETED.[ExpPctComp] Is Null
			) Or
			(
				INSERTED.[ExpPctComp] !=
				DELETED.[ExpPctComp]
			)
		) 
		END		
		
      If UPDATE([BillByDefault])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillByDefault',
      CONVERT(NVARCHAR(2000),DELETED.[BillByDefault],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BillByDefault],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BillByDefault] Is Null And
				DELETED.[BillByDefault] Is Not Null
			) Or
			(
				INSERTED.[BillByDefault] Is Not Null And
				DELETED.[BillByDefault] Is Null
			) Or
			(
				INSERTED.[BillByDefault] !=
				DELETED.[BillByDefault]
			)
		) 
		END		
		
      If UPDATE([BillableWarning])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillableWarning',
      CONVERT(NVARCHAR(2000),DELETED.[BillableWarning],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BillableWarning],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BillableWarning] Is Null And
				DELETED.[BillableWarning] Is Not Null
			) Or
			(
				INSERTED.[BillableWarning] Is Not Null And
				DELETED.[BillableWarning] Is Null
			) Or
			(
				INSERTED.[BillableWarning] !=
				DELETED.[BillableWarning]
			)
		) 
		END		
		
      If UPDATE([Memo])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Memo',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([BudgetedFlag])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BudgetedFlag',
      CONVERT(NVARCHAR(2000),DELETED.[BudgetedFlag],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BudgetedFlag],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BudgetedFlag] Is Null And
				DELETED.[BudgetedFlag] Is Not Null
			) Or
			(
				INSERTED.[BudgetedFlag] Is Not Null And
				DELETED.[BudgetedFlag] Is Null
			) Or
			(
				INSERTED.[BudgetedFlag] !=
				DELETED.[BudgetedFlag]
			)
		) 
		END		
		
      If UPDATE([BudgetedLevels])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BudgetedLevels',
      CONVERT(NVARCHAR(2000),DELETED.[BudgetedLevels],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BudgetedLevels],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BudgetedLevels] Is Null And
				DELETED.[BudgetedLevels] Is Not Null
			) Or
			(
				INSERTED.[BudgetedLevels] Is Not Null And
				DELETED.[BudgetedLevels] Is Null
			) Or
			(
				INSERTED.[BudgetedLevels] !=
				DELETED.[BudgetedLevels]
			)
		) 
		END		
		
      If UPDATE([BillWBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillWBS1',
      CONVERT(NVARCHAR(2000),DELETED.[BillWBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BillWBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BillWBS1] Is Null And
				DELETED.[BillWBS1] Is Not Null
			) Or
			(
				INSERTED.[BillWBS1] Is Not Null And
				DELETED.[BillWBS1] Is Null
			) Or
			(
				INSERTED.[BillWBS1] !=
				DELETED.[BillWBS1]
			)
		) 
		END		
		
      If UPDATE([BillWBS2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillWBS2',
      CONVERT(NVARCHAR(2000),DELETED.[BillWBS2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BillWBS2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BillWBS2] Is Null And
				DELETED.[BillWBS2] Is Not Null
			) Or
			(
				INSERTED.[BillWBS2] Is Not Null And
				DELETED.[BillWBS2] Is Null
			) Or
			(
				INSERTED.[BillWBS2] !=
				DELETED.[BillWBS2]
			)
		) 
		END		
		
      If UPDATE([BillWBS3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillWBS3',
      CONVERT(NVARCHAR(2000),DELETED.[BillWBS3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BillWBS3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BillWBS3] Is Null And
				DELETED.[BillWBS3] Is Not Null
			) Or
			(
				INSERTED.[BillWBS3] Is Not Null And
				DELETED.[BillWBS3] Is Null
			) Or
			(
				INSERTED.[BillWBS3] !=
				DELETED.[BillWBS3]
			)
		) 
		END		
		
      If UPDATE([XCharge])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'XCharge',
      CONVERT(NVARCHAR(2000),DELETED.[XCharge],121),
      CONVERT(NVARCHAR(2000),INSERTED.[XCharge],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[XCharge] Is Null And
				DELETED.[XCharge] Is Not Null
			) Or
			(
				INSERTED.[XCharge] Is Not Null And
				DELETED.[XCharge] Is Null
			) Or
			(
				INSERTED.[XCharge] !=
				DELETED.[XCharge]
			)
		) 
		END		
		
      If UPDATE([XChargeMethod])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'XChargeMethod',
      CONVERT(NVARCHAR(2000),DELETED.[XChargeMethod],121),
      CONVERT(NVARCHAR(2000),INSERTED.[XChargeMethod],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[XChargeMethod] Is Null And
				DELETED.[XChargeMethod] Is Not Null
			) Or
			(
				INSERTED.[XChargeMethod] Is Not Null And
				DELETED.[XChargeMethod] Is Null
			) Or
			(
				INSERTED.[XChargeMethod] !=
				DELETED.[XChargeMethod]
			)
		) 
		END		
		
      If UPDATE([XChargeMult])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'XChargeMult',
      CONVERT(NVARCHAR(2000),DELETED.[XChargeMult],121),
      CONVERT(NVARCHAR(2000),INSERTED.[XChargeMult],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[XChargeMult] Is Null And
				DELETED.[XChargeMult] Is Not Null
			) Or
			(
				INSERTED.[XChargeMult] Is Not Null And
				DELETED.[XChargeMult] Is Null
			) Or
			(
				INSERTED.[XChargeMult] !=
				DELETED.[XChargeMult]
			)
		) 
		END		
		
      If UPDATE([Description])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Description',
      CONVERT(NVARCHAR(2000),DELETED.[Description],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Description],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Description] Is Null And
				DELETED.[Description] Is Not Null
			) Or
			(
				INSERTED.[Description] Is Not Null And
				DELETED.[Description] Is Null
			) Or
			(
				INSERTED.[Description] !=
				DELETED.[Description]
			)
		) 
		END		
		
      If UPDATE([Closed])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Closed',
      CONVERT(NVARCHAR(2000),DELETED.[Closed],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Closed],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Closed] Is Null And
				DELETED.[Closed] Is Not Null
			) Or
			(
				INSERTED.[Closed] Is Not Null And
				DELETED.[Closed] Is Null
			) Or
			(
				INSERTED.[Closed] !=
				DELETED.[Closed]
			)
		) 
		END		
		
      If UPDATE([ReadOnly])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReadOnly',
      CONVERT(NVARCHAR(2000),DELETED.[ReadOnly],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReadOnly],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ReadOnly] Is Null And
				DELETED.[ReadOnly] Is Not Null
			) Or
			(
				INSERTED.[ReadOnly] Is Not Null And
				DELETED.[ReadOnly] Is Null
			) Or
			(
				INSERTED.[ReadOnly] !=
				DELETED.[ReadOnly]
			)
		) 
		END		
		
      If UPDATE([DefaultEffortDriven])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'DefaultEffortDriven',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultEffortDriven],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultEffortDriven],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[DefaultEffortDriven] Is Null And
				DELETED.[DefaultEffortDriven] Is Not Null
			) Or
			(
				INSERTED.[DefaultEffortDriven] Is Not Null And
				DELETED.[DefaultEffortDriven] Is Null
			) Or
			(
				INSERTED.[DefaultEffortDriven] !=
				DELETED.[DefaultEffortDriven]
			)
		) 
		END		
		
      If UPDATE([DefaultTaskType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'DefaultTaskType',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultTaskType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultTaskType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[DefaultTaskType] Is Null And
				DELETED.[DefaultTaskType] Is Not Null
			) Or
			(
				INSERTED.[DefaultTaskType] Is Not Null And
				DELETED.[DefaultTaskType] Is Null
			) Or
			(
				INSERTED.[DefaultTaskType] !=
				DELETED.[DefaultTaskType]
			)
		) 
		END		
		
      If UPDATE([VersionID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'VersionID',
      CONVERT(NVARCHAR(2000),DELETED.[VersionID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[VersionID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[VersionID] Is Null And
				DELETED.[VersionID] Is Not Null
			) Or
			(
				INSERTED.[VersionID] Is Not Null And
				DELETED.[VersionID] Is Null
			) Or
			(
				INSERTED.[VersionID] !=
				DELETED.[VersionID]
			)
		) 
		END		
		
     If UPDATE([ContactID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ContactID',
     CONVERT(NVARCHAR(2000),DELETED.[ContactID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[ContactID],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ContactID] Is Null And
				DELETED.[ContactID] Is Not Null
			) Or
			(
				INSERTED.[ContactID] Is Not Null And
				DELETED.[ContactID] Is Null
			) Or
			(
				INSERTED.[ContactID] !=
				DELETED.[ContactID]
			)
		) left join Contacts as oldDesc  on DELETED.ContactID = oldDesc.ContactID  left join  Contacts as newDesc  on INSERTED.ContactID = newDesc.ContactID
		END		
		
      If UPDATE([CLBillingAddr])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CLBillingAddr',
      CONVERT(NVARCHAR(2000),DELETED.[CLBillingAddr],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CLBillingAddr],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CLBillingAddr] Is Null And
				DELETED.[CLBillingAddr] Is Not Null
			) Or
			(
				INSERTED.[CLBillingAddr] Is Not Null And
				DELETED.[CLBillingAddr] Is Null
			) Or
			(
				INSERTED.[CLBillingAddr] !=
				DELETED.[CLBillingAddr]
			)
		) 
		END		
		
      If UPDATE([LongName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'LongName',
      CONVERT(NVARCHAR(2000),DELETED.[LongName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LongName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[LongName] Is Null And
				DELETED.[LongName] Is Not Null
			) Or
			(
				INSERTED.[LongName] Is Not Null And
				DELETED.[LongName] Is Null
			) Or
			(
				INSERTED.[LongName] !=
				DELETED.[LongName]
			)
		) 
		END		
		
      If UPDATE([Address1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Address1',
      CONVERT(NVARCHAR(2000),DELETED.[Address1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Address1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Address1] Is Null And
				DELETED.[Address1] Is Not Null
			) Or
			(
				INSERTED.[Address1] Is Not Null And
				DELETED.[Address1] Is Null
			) Or
			(
				INSERTED.[Address1] !=
				DELETED.[Address1]
			)
		) 
		END		
		
      If UPDATE([Address2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Address2',
      CONVERT(NVARCHAR(2000),DELETED.[Address2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Address2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Address2] Is Null And
				DELETED.[Address2] Is Not Null
			) Or
			(
				INSERTED.[Address2] Is Not Null And
				DELETED.[Address2] Is Null
			) Or
			(
				INSERTED.[Address2] !=
				DELETED.[Address2]
			)
		) 
		END		
		
      If UPDATE([Address3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Address3',
      CONVERT(NVARCHAR(2000),DELETED.[Address3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Address3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Address3] Is Null And
				DELETED.[Address3] Is Not Null
			) Or
			(
				INSERTED.[Address3] Is Not Null And
				DELETED.[Address3] Is Null
			) Or
			(
				INSERTED.[Address3] !=
				DELETED.[Address3]
			)
		) 
		END		
		
      If UPDATE([City])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'City',
      CONVERT(NVARCHAR(2000),DELETED.[City],121),
      CONVERT(NVARCHAR(2000),INSERTED.[City],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[City] Is Null And
				DELETED.[City] Is Not Null
			) Or
			(
				INSERTED.[City] Is Not Null And
				DELETED.[City] Is Null
			) Or
			(
				INSERTED.[City] !=
				DELETED.[City]
			)
		) 
		END		
		
      If UPDATE([State])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'State',
      CONVERT(NVARCHAR(2000),DELETED.[State],121),
      CONVERT(NVARCHAR(2000),INSERTED.[State],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[State] Is Null And
				DELETED.[State] Is Not Null
			) Or
			(
				INSERTED.[State] Is Not Null And
				DELETED.[State] Is Null
			) Or
			(
				INSERTED.[State] !=
				DELETED.[State]
			)
		) 
		END		
		
      If UPDATE([Zip])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Zip',
      CONVERT(NVARCHAR(2000),DELETED.[Zip],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Zip],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Zip] Is Null And
				DELETED.[Zip] Is Not Null
			) Or
			(
				INSERTED.[Zip] Is Not Null And
				DELETED.[Zip] Is Null
			) Or
			(
				INSERTED.[Zip] !=
				DELETED.[Zip]
			)
		) 
		END		
		
      If UPDATE([County])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'County',
      CONVERT(NVARCHAR(2000),DELETED.[County],121),
      CONVERT(NVARCHAR(2000),INSERTED.[County],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[County] Is Null And
				DELETED.[County] Is Not Null
			) Or
			(
				INSERTED.[County] Is Not Null And
				DELETED.[County] Is Null
			) Or
			(
				INSERTED.[County] !=
				DELETED.[County]
			)
		) 
		END		
		
      If UPDATE([Country])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Country',
      CONVERT(NVARCHAR(2000),DELETED.[Country],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Country],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Country] Is Null And
				DELETED.[Country] Is Not Null
			) Or
			(
				INSERTED.[Country] Is Not Null And
				DELETED.[Country] Is Null
			) Or
			(
				INSERTED.[Country] !=
				DELETED.[Country]
			)
		) 
		END		
		
      If UPDATE([FederalInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FederalInd',
      CONVERT(NVARCHAR(2000),DELETED.[FederalInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FederalInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FederalInd] Is Null And
				DELETED.[FederalInd] Is Not Null
			) Or
			(
				INSERTED.[FederalInd] Is Not Null And
				DELETED.[FederalInd] Is Null
			) Or
			(
				INSERTED.[FederalInd] !=
				DELETED.[FederalInd]
			)
		) 
		END		
		
     If UPDATE([ProjectType])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ProjectType',
     CONVERT(NVARCHAR(2000),DELETED.[ProjectType],121),
     CONVERT(NVARCHAR(2000),INSERTED.[ProjectType],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ProjectType] Is Null And
				DELETED.[ProjectType] Is Not Null
			) Or
			(
				INSERTED.[ProjectType] Is Not Null And
				DELETED.[ProjectType] Is Null
			) Or
			(
				INSERTED.[ProjectType] !=
				DELETED.[ProjectType]
			)
		) left join CFGProjectType as oldDesc  on DELETED.ProjectType = oldDesc.Code  left join  CFGProjectType as newDesc  on INSERTED.ProjectType = newDesc.Code
		END		
		
     If UPDATE([Responsibility])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Responsibility',
     CONVERT(NVARCHAR(2000),DELETED.[Responsibility],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Responsibility],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Responsibility] Is Null And
				DELETED.[Responsibility] Is Not Null
			) Or
			(
				INSERTED.[Responsibility] Is Not Null And
				DELETED.[Responsibility] Is Null
			) Or
			(
				INSERTED.[Responsibility] !=
				DELETED.[Responsibility]
			)
		) left join CFGPRResponsibility as oldDesc  on DELETED.Responsibility = oldDesc.Code  left join  CFGPRResponsibility as newDesc  on INSERTED.Responsibility = newDesc.Code
		END		
		
      If UPDATE([Referable])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Referable',
      CONVERT(NVARCHAR(2000),DELETED.[Referable],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Referable],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Referable] Is Null And
				DELETED.[Referable] Is Not Null
			) Or
			(
				INSERTED.[Referable] Is Not Null And
				DELETED.[Referable] Is Null
			) Or
			(
				INSERTED.[Referable] !=
				DELETED.[Referable]
			)
		) 
		END		
		
      If UPDATE([EstCompletionDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'EstCompletionDate',
      CONVERT(NVARCHAR(2000),DELETED.[EstCompletionDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EstCompletionDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[EstCompletionDate] Is Null And
				DELETED.[EstCompletionDate] Is Not Null
			) Or
			(
				INSERTED.[EstCompletionDate] Is Not Null And
				DELETED.[EstCompletionDate] Is Null
			) Or
			(
				INSERTED.[EstCompletionDate] !=
				DELETED.[EstCompletionDate]
			)
		) 
		END		
		
      If UPDATE([ActCompletionDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ActCompletionDate',
      CONVERT(NVARCHAR(2000),DELETED.[ActCompletionDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ActCompletionDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ActCompletionDate] Is Null And
				DELETED.[ActCompletionDate] Is Not Null
			) Or
			(
				INSERTED.[ActCompletionDate] Is Not Null And
				DELETED.[ActCompletionDate] Is Null
			) Or
			(
				INSERTED.[ActCompletionDate] !=
				DELETED.[ActCompletionDate]
			)
		) 
		END		
		
      If UPDATE([ContractDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ContractDate',
      CONVERT(NVARCHAR(2000),DELETED.[ContractDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ContractDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ContractDate] Is Null And
				DELETED.[ContractDate] Is Not Null
			) Or
			(
				INSERTED.[ContractDate] Is Not Null And
				DELETED.[ContractDate] Is Null
			) Or
			(
				INSERTED.[ContractDate] !=
				DELETED.[ContractDate]
			)
		) 
		END		
		
      If UPDATE([BidDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BidDate',
      CONVERT(NVARCHAR(2000),DELETED.[BidDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BidDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BidDate] Is Null And
				DELETED.[BidDate] Is Not Null
			) Or
			(
				INSERTED.[BidDate] Is Not Null And
				DELETED.[BidDate] Is Null
			) Or
			(
				INSERTED.[BidDate] !=
				DELETED.[BidDate]
			)
		) 
		END		
		
      If UPDATE([ComplDateComment])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ComplDateComment',
      CONVERT(NVARCHAR(2000),DELETED.[ComplDateComment],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ComplDateComment],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ComplDateComment] Is Null And
				DELETED.[ComplDateComment] Is Not Null
			) Or
			(
				INSERTED.[ComplDateComment] Is Not Null And
				DELETED.[ComplDateComment] Is Null
			) Or
			(
				INSERTED.[ComplDateComment] !=
				DELETED.[ComplDateComment]
			)
		) 
		END		
		
      If UPDATE([FirmCost])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FirmCost',
      CONVERT(NVARCHAR(2000),DELETED.[FirmCost],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirmCost],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FirmCost] Is Null And
				DELETED.[FirmCost] Is Not Null
			) Or
			(
				INSERTED.[FirmCost] Is Not Null And
				DELETED.[FirmCost] Is Null
			) Or
			(
				INSERTED.[FirmCost] !=
				DELETED.[FirmCost]
			)
		) 
		END		
		
      If UPDATE([FirmCostComment])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FirmCostComment',
      CONVERT(NVARCHAR(2000),DELETED.[FirmCostComment],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirmCostComment],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FirmCostComment] Is Null And
				DELETED.[FirmCostComment] Is Not Null
			) Or
			(
				INSERTED.[FirmCostComment] Is Not Null And
				DELETED.[FirmCostComment] Is Null
			) Or
			(
				INSERTED.[FirmCostComment] !=
				DELETED.[FirmCostComment]
			)
		) 
		END		
		
      If UPDATE([TotalProjectCost])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TotalProjectCost',
      CONVERT(NVARCHAR(2000),DELETED.[TotalProjectCost],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TotalProjectCost],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[TotalProjectCost] Is Null And
				DELETED.[TotalProjectCost] Is Not Null
			) Or
			(
				INSERTED.[TotalProjectCost] Is Not Null And
				DELETED.[TotalProjectCost] Is Null
			) Or
			(
				INSERTED.[TotalProjectCost] !=
				DELETED.[TotalProjectCost]
			)
		) 
		END		
		
      If UPDATE([TotalCostComment])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TotalCostComment',
      CONVERT(NVARCHAR(2000),DELETED.[TotalCostComment],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TotalCostComment],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[TotalCostComment] Is Null And
				DELETED.[TotalCostComment] Is Not Null
			) Or
			(
				INSERTED.[TotalCostComment] Is Not Null And
				DELETED.[TotalCostComment] Is Null
			) Or
			(
				INSERTED.[TotalCostComment] !=
				DELETED.[TotalCostComment]
			)
		) 
		END		
		
      If UPDATE([OpportunityID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'OpportunityID',
      CONVERT(NVARCHAR(2000),DELETED.[OpportunityID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OpportunityID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[OpportunityID] Is Null And
				DELETED.[OpportunityID] Is Not Null
			) Or
			(
				INSERTED.[OpportunityID] Is Not Null And
				DELETED.[OpportunityID] Is Null
			) Or
			(
				INSERTED.[OpportunityID] !=
				DELETED.[OpportunityID]
			)
		) 
		END		
		
      If UPDATE([ClientConfidential])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ClientConfidential',
      CONVERT(NVARCHAR(2000),DELETED.[ClientConfidential],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClientConfidential],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ClientConfidential] Is Null And
				DELETED.[ClientConfidential] Is Not Null
			) Or
			(
				INSERTED.[ClientConfidential] Is Not Null And
				DELETED.[ClientConfidential] Is Null
			) Or
			(
				INSERTED.[ClientConfidential] !=
				DELETED.[ClientConfidential]
			)
		) 
		END		
		
      If UPDATE([ClientAlias])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ClientAlias',
      CONVERT(NVARCHAR(2000),DELETED.[ClientAlias],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClientAlias],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ClientAlias] Is Null And
				DELETED.[ClientAlias] Is Not Null
			) Or
			(
				INSERTED.[ClientAlias] Is Not Null And
				DELETED.[ClientAlias] Is Null
			) Or
			(
				INSERTED.[ClientAlias] !=
				DELETED.[ClientAlias]
			)
		) 
		END		
		
      If UPDATE([AvailableForCRM])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AvailableForCRM',
      CONVERT(NVARCHAR(2000),DELETED.[AvailableForCRM],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AvailableForCRM],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AvailableForCRM] Is Null And
				DELETED.[AvailableForCRM] Is Not Null
			) Or
			(
				INSERTED.[AvailableForCRM] Is Not Null And
				DELETED.[AvailableForCRM] Is Null
			) Or
			(
				INSERTED.[AvailableForCRM] !=
				DELETED.[AvailableForCRM]
			)
		) 
		END		
		
      If UPDATE([ReadyForApproval])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReadyForApproval',
      CONVERT(NVARCHAR(2000),DELETED.[ReadyForApproval],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReadyForApproval],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ReadyForApproval] Is Null And
				DELETED.[ReadyForApproval] Is Not Null
			) Or
			(
				INSERTED.[ReadyForApproval] Is Not Null And
				DELETED.[ReadyForApproval] Is Null
			) Or
			(
				INSERTED.[ReadyForApproval] !=
				DELETED.[ReadyForApproval]
			)
		) 
		END		
		
      If UPDATE([ReadyForProcessing])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReadyForProcessing',
      CONVERT(NVARCHAR(2000),DELETED.[ReadyForProcessing],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReadyForProcessing],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ReadyForProcessing] Is Null And
				DELETED.[ReadyForProcessing] Is Not Null
			) Or
			(
				INSERTED.[ReadyForProcessing] Is Not Null And
				DELETED.[ReadyForProcessing] Is Null
			) Or
			(
				INSERTED.[ReadyForProcessing] !=
				DELETED.[ReadyForProcessing]
			)
		) 
		END		
		
     If UPDATE([BillingClientID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillingClientID',
     CONVERT(NVARCHAR(2000),DELETED.[BillingClientID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[BillingClientID],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BillingClientID] Is Null And
				DELETED.[BillingClientID] Is Not Null
			) Or
			(
				INSERTED.[BillingClientID] Is Not Null And
				DELETED.[BillingClientID] Is Null
			) Or
			(
				INSERTED.[BillingClientID] !=
				DELETED.[BillingClientID]
			)
		) left join CL as oldDesc  on DELETED.BillingClientID = oldDesc.ClientID  left join  CL as newDesc  on INSERTED.BillingClientID = newDesc.ClientID
		END		
		
     If UPDATE([BillingContactID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillingContactID',
     CONVERT(NVARCHAR(2000),DELETED.[BillingContactID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[BillingContactID],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BillingContactID] Is Null And
				DELETED.[BillingContactID] Is Not Null
			) Or
			(
				INSERTED.[BillingContactID] Is Not Null And
				DELETED.[BillingContactID] Is Null
			) Or
			(
				INSERTED.[BillingContactID] !=
				DELETED.[BillingContactID]
			)
		) left join Contacts as oldDesc  on DELETED.BillingContactID = oldDesc.ContactID  left join  Contacts as newDesc  on INSERTED.BillingContactID = newDesc.ContactID
		END		
		
      If UPDATE([Phone])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Phone',
      CONVERT(NVARCHAR(2000),DELETED.[Phone],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Phone],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Phone] Is Null And
				DELETED.[Phone] Is Not Null
			) Or
			(
				INSERTED.[Phone] Is Not Null And
				DELETED.[Phone] Is Null
			) Or
			(
				INSERTED.[Phone] !=
				DELETED.[Phone]
			)
		) 
		END		
		
      If UPDATE([Fax])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Fax',
      CONVERT(NVARCHAR(2000),DELETED.[Fax],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fax],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Fax] Is Null And
				DELETED.[Fax] Is Not Null
			) Or
			(
				INSERTED.[Fax] Is Not Null And
				DELETED.[Fax] Is Null
			) Or
			(
				INSERTED.[Fax] !=
				DELETED.[Fax]
			)
		) 
		END		
		
      If UPDATE([EMail])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'EMail',
      CONVERT(NVARCHAR(2000),DELETED.[EMail],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EMail],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[EMail] Is Null And
				DELETED.[EMail] Is Not Null
			) Or
			(
				INSERTED.[EMail] Is Not Null And
				DELETED.[EMail] Is Null
			) Or
			(
				INSERTED.[EMail] !=
				DELETED.[EMail]
			)
		) 
		END		
		
      If UPDATE([ProposalWBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ProposalWBS1',
      CONVERT(NVARCHAR(2000),DELETED.[ProposalWBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProposalWBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ProposalWBS1] Is Null And
				DELETED.[ProposalWBS1] Is Not Null
			) Or
			(
				INSERTED.[ProposalWBS1] Is Not Null And
				DELETED.[ProposalWBS1] Is Null
			) Or
			(
				INSERTED.[ProposalWBS1] !=
				DELETED.[ProposalWBS1]
			)
		) 
		END		
		
      If UPDATE([CostRateMeth])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CostRateMeth',
      CONVERT(NVARCHAR(2000),DELETED.[CostRateMeth],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CostRateMeth],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CostRateMeth] Is Null And
				DELETED.[CostRateMeth] Is Not Null
			) Or
			(
				INSERTED.[CostRateMeth] Is Not Null And
				DELETED.[CostRateMeth] Is Null
			) Or
			(
				INSERTED.[CostRateMeth] !=
				DELETED.[CostRateMeth]
			)
		) 
		END		
		
      If UPDATE([CostRateTableNo])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CostRateTableNo',
      CONVERT(NVARCHAR(2000),DELETED.[CostRateTableNo],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CostRateTableNo],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CostRateTableNo] Is Null And
				DELETED.[CostRateTableNo] Is Not Null
			) Or
			(
				INSERTED.[CostRateTableNo] Is Not Null And
				DELETED.[CostRateTableNo] Is Null
			) Or
			(
				INSERTED.[CostRateTableNo] !=
				DELETED.[CostRateTableNo]
			)
		) 
		END		
		
      If UPDATE([PayRateMeth])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PayRateMeth',
      CONVERT(NVARCHAR(2000),DELETED.[PayRateMeth],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PayRateMeth],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[PayRateMeth] Is Null And
				DELETED.[PayRateMeth] Is Not Null
			) Or
			(
				INSERTED.[PayRateMeth] Is Not Null And
				DELETED.[PayRateMeth] Is Null
			) Or
			(
				INSERTED.[PayRateMeth] !=
				DELETED.[PayRateMeth]
			)
		) 
		END		
		
      If UPDATE([PayRateTableNo])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PayRateTableNo',
      CONVERT(NVARCHAR(2000),DELETED.[PayRateTableNo],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PayRateTableNo],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[PayRateTableNo] Is Null And
				DELETED.[PayRateTableNo] Is Not Null
			) Or
			(
				INSERTED.[PayRateTableNo] Is Not Null And
				DELETED.[PayRateTableNo] Is Null
			) Or
			(
				INSERTED.[PayRateTableNo] !=
				DELETED.[PayRateTableNo]
			)
		) 
		END		
		
     If UPDATE([Locale])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Locale',
     CONVERT(NVARCHAR(2000),DELETED.[Locale],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Locale],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Locale] Is Null And
				DELETED.[Locale] Is Not Null
			) Or
			(
				INSERTED.[Locale] Is Not Null And
				DELETED.[Locale] Is Null
			) Or
			(
				INSERTED.[Locale] !=
				DELETED.[Locale]
			)
		) left join CFGPYWHCodes as oldDesc  on DELETED.Locale = oldDesc.Code  left join  CFGPYWHCodes as newDesc  on INSERTED.Locale = newDesc.Code
		END		
		
      If UPDATE([LineItemApproval])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'LineItemApproval',
      CONVERT(NVARCHAR(2000),DELETED.[LineItemApproval],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LineItemApproval],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[LineItemApproval] Is Null And
				DELETED.[LineItemApproval] Is Not Null
			) Or
			(
				INSERTED.[LineItemApproval] Is Not Null And
				DELETED.[LineItemApproval] Is Null
			) Or
			(
				INSERTED.[LineItemApproval] !=
				DELETED.[LineItemApproval]
			)
		) 
		END		
		
      If UPDATE([LineItemApprovalEK])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'LineItemApprovalEK',
      CONVERT(NVARCHAR(2000),DELETED.[LineItemApprovalEK],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LineItemApprovalEK],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[LineItemApprovalEK] Is Null And
				DELETED.[LineItemApprovalEK] Is Not Null
			) Or
			(
				INSERTED.[LineItemApprovalEK] Is Not Null And
				DELETED.[LineItemApprovalEK] Is Null
			) Or
			(
				INSERTED.[LineItemApprovalEK] !=
				DELETED.[LineItemApprovalEK]
			)
		) 
		END		
		
      If UPDATE([BudgetSource])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BudgetSource',
      CONVERT(NVARCHAR(2000),DELETED.[BudgetSource],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BudgetSource],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BudgetSource] Is Null And
				DELETED.[BudgetSource] Is Not Null
			) Or
			(
				INSERTED.[BudgetSource] Is Not Null And
				DELETED.[BudgetSource] Is Null
			) Or
			(
				INSERTED.[BudgetSource] !=
				DELETED.[BudgetSource]
			)
		) 
		END		
		
      If UPDATE([BudgetLevel])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BudgetLevel',
      CONVERT(NVARCHAR(2000),DELETED.[BudgetLevel],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BudgetLevel],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BudgetLevel] Is Null And
				DELETED.[BudgetLevel] Is Not Null
			) Or
			(
				INSERTED.[BudgetLevel] Is Not Null And
				DELETED.[BudgetLevel] Is Null
			) Or
			(
				INSERTED.[BudgetLevel] !=
				DELETED.[BudgetLevel]
			)
		) 
		END		
		
      If UPDATE([ProfServicesComplDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ProfServicesComplDate',
      CONVERT(NVARCHAR(2000),DELETED.[ProfServicesComplDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProfServicesComplDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ProfServicesComplDate] Is Null And
				DELETED.[ProfServicesComplDate] Is Not Null
			) Or
			(
				INSERTED.[ProfServicesComplDate] Is Not Null And
				DELETED.[ProfServicesComplDate] Is Null
			) Or
			(
				INSERTED.[ProfServicesComplDate] !=
				DELETED.[ProfServicesComplDate]
			)
		) 
		END		
		
      If UPDATE([ConstComplDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ConstComplDate',
      CONVERT(NVARCHAR(2000),DELETED.[ConstComplDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ConstComplDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ConstComplDate] Is Null And
				DELETED.[ConstComplDate] Is Not Null
			) Or
			(
				INSERTED.[ConstComplDate] Is Not Null And
				DELETED.[ConstComplDate] Is Null
			) Or
			(
				INSERTED.[ConstComplDate] !=
				DELETED.[ConstComplDate]
			)
		) 
		END		
		
      If UPDATE([ProjectCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ProjectCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[ProjectCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProjectCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ProjectCurrencyCode] Is Null And
				DELETED.[ProjectCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[ProjectCurrencyCode] Is Not Null And
				DELETED.[ProjectCurrencyCode] Is Null
			) Or
			(
				INSERTED.[ProjectCurrencyCode] !=
				DELETED.[ProjectCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([ProjectExchangeRate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ProjectExchangeRate',
      CONVERT(NVARCHAR(2000),DELETED.[ProjectExchangeRate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProjectExchangeRate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ProjectExchangeRate] Is Null And
				DELETED.[ProjectExchangeRate] Is Not Null
			) Or
			(
				INSERTED.[ProjectExchangeRate] Is Not Null And
				DELETED.[ProjectExchangeRate] Is Null
			) Or
			(
				INSERTED.[ProjectExchangeRate] !=
				DELETED.[ProjectExchangeRate]
			)
		) 
		END		
		
      If UPDATE([BillingCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillingCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[BillingCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BillingCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BillingCurrencyCode] Is Null And
				DELETED.[BillingCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[BillingCurrencyCode] Is Not Null And
				DELETED.[BillingCurrencyCode] Is Null
			) Or
			(
				INSERTED.[BillingCurrencyCode] !=
				DELETED.[BillingCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([BillingExchangeRate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillingExchangeRate',
      CONVERT(NVARCHAR(2000),DELETED.[BillingExchangeRate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BillingExchangeRate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BillingExchangeRate] Is Null And
				DELETED.[BillingExchangeRate] Is Not Null
			) Or
			(
				INSERTED.[BillingExchangeRate] Is Not Null And
				DELETED.[BillingExchangeRate] Is Null
			) Or
			(
				INSERTED.[BillingExchangeRate] !=
				DELETED.[BillingExchangeRate]
			)
		) 
		END		
		
      If UPDATE([RestrictChargeCompanies])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RestrictChargeCompanies',
      CONVERT(NVARCHAR(2000),DELETED.[RestrictChargeCompanies],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RestrictChargeCompanies],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RestrictChargeCompanies] Is Null And
				DELETED.[RestrictChargeCompanies] Is Not Null
			) Or
			(
				INSERTED.[RestrictChargeCompanies] Is Not Null And
				DELETED.[RestrictChargeCompanies] Is Null
			) Or
			(
				INSERTED.[RestrictChargeCompanies] !=
				DELETED.[RestrictChargeCompanies]
			)
		) 
		END		
		
      If UPDATE([FeeBillingCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FeeBillingCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[FeeBillingCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeBillingCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FeeBillingCurrency] Is Null And
				DELETED.[FeeBillingCurrency] Is Not Null
			) Or
			(
				INSERTED.[FeeBillingCurrency] Is Not Null And
				DELETED.[FeeBillingCurrency] Is Null
			) Or
			(
				INSERTED.[FeeBillingCurrency] !=
				DELETED.[FeeBillingCurrency]
			)
		) 
		END		
		
      If UPDATE([ReimbAllowBillingCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllowBillingCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllowBillingCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllowBillingCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ReimbAllowBillingCurrency] Is Null And
				DELETED.[ReimbAllowBillingCurrency] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllowBillingCurrency] Is Not Null And
				DELETED.[ReimbAllowBillingCurrency] Is Null
			) Or
			(
				INSERTED.[ReimbAllowBillingCurrency] !=
				DELETED.[ReimbAllowBillingCurrency]
			)
		) 
		END		
		
      If UPDATE([ConsultFeeBillingCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ConsultFeeBillingCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[ConsultFeeBillingCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ConsultFeeBillingCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ConsultFeeBillingCurrency] Is Null And
				DELETED.[ConsultFeeBillingCurrency] Is Not Null
			) Or
			(
				INSERTED.[ConsultFeeBillingCurrency] Is Not Null And
				DELETED.[ConsultFeeBillingCurrency] Is Null
			) Or
			(
				INSERTED.[ConsultFeeBillingCurrency] !=
				DELETED.[ConsultFeeBillingCurrency]
			)
		) 
		END		
		
      If UPDATE([RevUpsetLimits])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetLimits',
      CONVERT(NVARCHAR(2000),DELETED.[RevUpsetLimits],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RevUpsetLimits],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RevUpsetLimits] Is Null And
				DELETED.[RevUpsetLimits] Is Not Null
			) Or
			(
				INSERTED.[RevUpsetLimits] Is Not Null And
				DELETED.[RevUpsetLimits] Is Null
			) Or
			(
				INSERTED.[RevUpsetLimits] !=
				DELETED.[RevUpsetLimits]
			)
		) 
		END		
		
      If UPDATE([RevUpsetWBS2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetWBS2',
      CONVERT(NVARCHAR(2000),DELETED.[RevUpsetWBS2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RevUpsetWBS2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RevUpsetWBS2] Is Null And
				DELETED.[RevUpsetWBS2] Is Not Null
			) Or
			(
				INSERTED.[RevUpsetWBS2] Is Not Null And
				DELETED.[RevUpsetWBS2] Is Null
			) Or
			(
				INSERTED.[RevUpsetWBS2] !=
				DELETED.[RevUpsetWBS2]
			)
		) 
		END		
		
      If UPDATE([RevUpsetWBS3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetWBS3',
      CONVERT(NVARCHAR(2000),DELETED.[RevUpsetWBS3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RevUpsetWBS3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RevUpsetWBS3] Is Null And
				DELETED.[RevUpsetWBS3] Is Not Null
			) Or
			(
				INSERTED.[RevUpsetWBS3] Is Not Null And
				DELETED.[RevUpsetWBS3] Is Null
			) Or
			(
				INSERTED.[RevUpsetWBS3] !=
				DELETED.[RevUpsetWBS3]
			)
		) 
		END		
		
      If UPDATE([RevUpsetIncludeComp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetIncludeComp',
      CONVERT(NVARCHAR(2000),DELETED.[RevUpsetIncludeComp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RevUpsetIncludeComp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RevUpsetIncludeComp] Is Null And
				DELETED.[RevUpsetIncludeComp] Is Not Null
			) Or
			(
				INSERTED.[RevUpsetIncludeComp] Is Not Null And
				DELETED.[RevUpsetIncludeComp] Is Null
			) Or
			(
				INSERTED.[RevUpsetIncludeComp] !=
				DELETED.[RevUpsetIncludeComp]
			)
		) 
		END		
		
      If UPDATE([RevUpsetIncludeCons])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetIncludeCons',
      CONVERT(NVARCHAR(2000),DELETED.[RevUpsetIncludeCons],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RevUpsetIncludeCons],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RevUpsetIncludeCons] Is Null And
				DELETED.[RevUpsetIncludeCons] Is Not Null
			) Or
			(
				INSERTED.[RevUpsetIncludeCons] Is Not Null And
				DELETED.[RevUpsetIncludeCons] Is Null
			) Or
			(
				INSERTED.[RevUpsetIncludeCons] !=
				DELETED.[RevUpsetIncludeCons]
			)
		) 
		END		
		
      If UPDATE([RevUpsetIncludeReimb])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetIncludeReimb',
      CONVERT(NVARCHAR(2000),DELETED.[RevUpsetIncludeReimb],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RevUpsetIncludeReimb],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RevUpsetIncludeReimb] Is Null And
				DELETED.[RevUpsetIncludeReimb] Is Not Null
			) Or
			(
				INSERTED.[RevUpsetIncludeReimb] Is Not Null And
				DELETED.[RevUpsetIncludeReimb] Is Null
			) Or
			(
				INSERTED.[RevUpsetIncludeReimb] !=
				DELETED.[RevUpsetIncludeReimb]
			)
		) 
		END		
		
      If UPDATE([PORMBRate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PORMBRate',
      CONVERT(NVARCHAR(2000),DELETED.[PORMBRate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PORMBRate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[PORMBRate] Is Null And
				DELETED.[PORMBRate] Is Not Null
			) Or
			(
				INSERTED.[PORMBRate] Is Not Null And
				DELETED.[PORMBRate] Is Null
			) Or
			(
				INSERTED.[PORMBRate] !=
				DELETED.[PORMBRate]
			)
		) 
		END		
		
      If UPDATE([POCNSRate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'POCNSRate',
      CONVERT(NVARCHAR(2000),DELETED.[POCNSRate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[POCNSRate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[POCNSRate] Is Null And
				DELETED.[POCNSRate] Is Not Null
			) Or
			(
				INSERTED.[POCNSRate] Is Not Null And
				DELETED.[POCNSRate] Is Null
			) Or
			(
				INSERTED.[POCNSRate] !=
				DELETED.[POCNSRate]
			)
		) 
		END		
		
      If UPDATE([PlanID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PlanID',
      CONVERT(NVARCHAR(2000),DELETED.[PlanID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PlanID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[PlanID] Is Null And
				DELETED.[PlanID] Is Not Null
			) Or
			(
				INSERTED.[PlanID] Is Not Null And
				DELETED.[PlanID] Is Null
			) Or
			(
				INSERTED.[PlanID] !=
				DELETED.[PlanID]
			)
		) 
		END		
		
      If UPDATE([TKCheckRPDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TKCheckRPDate',
      CONVERT(NVARCHAR(2000),DELETED.[TKCheckRPDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TKCheckRPDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[TKCheckRPDate] Is Null And
				DELETED.[TKCheckRPDate] Is Not Null
			) Or
			(
				INSERTED.[TKCheckRPDate] Is Not Null And
				DELETED.[TKCheckRPDate] Is Null
			) Or
			(
				INSERTED.[TKCheckRPDate] !=
				DELETED.[TKCheckRPDate]
			)
		) 
		END		
		
      If UPDATE([ICBillingLab])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ICBillingLab',
      CONVERT(NVARCHAR(2000),DELETED.[ICBillingLab],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ICBillingLab],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ICBillingLab] Is Null And
				DELETED.[ICBillingLab] Is Not Null
			) Or
			(
				INSERTED.[ICBillingLab] Is Not Null And
				DELETED.[ICBillingLab] Is Null
			) Or
			(
				INSERTED.[ICBillingLab] !=
				DELETED.[ICBillingLab]
			)
		) 
		END		
		
      If UPDATE([ICBillingLabMethod])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ICBillingLabMethod',
      CONVERT(NVARCHAR(2000),DELETED.[ICBillingLabMethod],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ICBillingLabMethod],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ICBillingLabMethod] Is Null And
				DELETED.[ICBillingLabMethod] Is Not Null
			) Or
			(
				INSERTED.[ICBillingLabMethod] Is Not Null And
				DELETED.[ICBillingLabMethod] Is Null
			) Or
			(
				INSERTED.[ICBillingLabMethod] !=
				DELETED.[ICBillingLabMethod]
			)
		) 
		END		
		
      If UPDATE([ICBillingLabMult])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ICBillingLabMult',
      CONVERT(NVARCHAR(2000),DELETED.[ICBillingLabMult],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ICBillingLabMult],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ICBillingLabMult] Is Null And
				DELETED.[ICBillingLabMult] Is Not Null
			) Or
			(
				INSERTED.[ICBillingLabMult] Is Not Null And
				DELETED.[ICBillingLabMult] Is Null
			) Or
			(
				INSERTED.[ICBillingLabMult] !=
				DELETED.[ICBillingLabMult]
			)
		) 
		END		
		
      If UPDATE([ICBillingExp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ICBillingExp',
      CONVERT(NVARCHAR(2000),DELETED.[ICBillingExp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ICBillingExp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ICBillingExp] Is Null And
				DELETED.[ICBillingExp] Is Not Null
			) Or
			(
				INSERTED.[ICBillingExp] Is Not Null And
				DELETED.[ICBillingExp] Is Null
			) Or
			(
				INSERTED.[ICBillingExp] !=
				DELETED.[ICBillingExp]
			)
		) 
		END		
		
      If UPDATE([ICBillingExpMethod])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ICBillingExpMethod',
      CONVERT(NVARCHAR(2000),DELETED.[ICBillingExpMethod],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ICBillingExpMethod],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ICBillingExpMethod] Is Null And
				DELETED.[ICBillingExpMethod] Is Not Null
			) Or
			(
				INSERTED.[ICBillingExpMethod] Is Not Null And
				DELETED.[ICBillingExpMethod] Is Null
			) Or
			(
				INSERTED.[ICBillingExpMethod] !=
				DELETED.[ICBillingExpMethod]
			)
		) 
		END		
		
      If UPDATE([ICBillingExpMult])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ICBillingExpMult',
      CONVERT(NVARCHAR(2000),DELETED.[ICBillingExpMult],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ICBillingExpMult],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ICBillingExpMult] Is Null And
				DELETED.[ICBillingExpMult] Is Not Null
			) Or
			(
				INSERTED.[ICBillingExpMult] Is Not Null And
				DELETED.[ICBillingExpMult] Is Null
			) Or
			(
				INSERTED.[ICBillingExpMult] !=
				DELETED.[ICBillingExpMult]
			)
		) 
		END		
		
      If UPDATE([RequireComments])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RequireComments',
      CONVERT(NVARCHAR(2000),DELETED.[RequireComments],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RequireComments],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RequireComments] Is Null And
				DELETED.[RequireComments] Is Not Null
			) Or
			(
				INSERTED.[RequireComments] Is Not Null And
				DELETED.[RequireComments] Is Null
			) Or
			(
				INSERTED.[RequireComments] !=
				DELETED.[RequireComments]
			)
		) 
		END		
		
      If UPDATE([TKCheckRPPlannedHrs])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TKCheckRPPlannedHrs',
      CONVERT(NVARCHAR(2000),DELETED.[TKCheckRPPlannedHrs],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TKCheckRPPlannedHrs],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[TKCheckRPPlannedHrs] Is Null And
				DELETED.[TKCheckRPPlannedHrs] Is Not Null
			) Or
			(
				INSERTED.[TKCheckRPPlannedHrs] Is Not Null And
				DELETED.[TKCheckRPPlannedHrs] Is Null
			) Or
			(
				INSERTED.[TKCheckRPPlannedHrs] !=
				DELETED.[TKCheckRPPlannedHrs]
			)
		) 
		END		
		
      If UPDATE([BillByDefaultConsultants])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillByDefaultConsultants',
      CONVERT(NVARCHAR(2000),DELETED.[BillByDefaultConsultants],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BillByDefaultConsultants],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BillByDefaultConsultants] Is Null And
				DELETED.[BillByDefaultConsultants] Is Not Null
			) Or
			(
				INSERTED.[BillByDefaultConsultants] Is Not Null And
				DELETED.[BillByDefaultConsultants] Is Null
			) Or
			(
				INSERTED.[BillByDefaultConsultants] !=
				DELETED.[BillByDefaultConsultants]
			)
		) 
		END		
		
      If UPDATE([BillByDefaultOtherExp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillByDefaultOtherExp',
      CONVERT(NVARCHAR(2000),DELETED.[BillByDefaultOtherExp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BillByDefaultOtherExp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BillByDefaultOtherExp] Is Null And
				DELETED.[BillByDefaultOtherExp] Is Not Null
			) Or
			(
				INSERTED.[BillByDefaultOtherExp] Is Not Null And
				DELETED.[BillByDefaultOtherExp] Is Null
			) Or
			(
				INSERTED.[BillByDefaultOtherExp] !=
				DELETED.[BillByDefaultOtherExp]
			)
		) 
		END		
		
      If UPDATE([BillByDefaultORTable])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BillByDefaultORTable',
      CONVERT(NVARCHAR(2000),DELETED.[BillByDefaultORTable],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BillByDefaultORTable],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BillByDefaultORTable] Is Null And
				DELETED.[BillByDefaultORTable] Is Not Null
			) Or
			(
				INSERTED.[BillByDefaultORTable] Is Not Null And
				DELETED.[BillByDefaultORTable] Is Null
			) Or
			(
				INSERTED.[BillByDefaultORTable] !=
				DELETED.[BillByDefaultORTable]
			)
		) 
		END		
		
      If UPDATE([PhoneFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PhoneFormat',
      CONVERT(NVARCHAR(2000),DELETED.[PhoneFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PhoneFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[PhoneFormat] Is Null And
				DELETED.[PhoneFormat] Is Not Null
			) Or
			(
				INSERTED.[PhoneFormat] Is Not Null And
				DELETED.[PhoneFormat] Is Null
			) Or
			(
				INSERTED.[PhoneFormat] !=
				DELETED.[PhoneFormat]
			)
		) 
		END		
		
      If UPDATE([FaxFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FaxFormat',
      CONVERT(NVARCHAR(2000),DELETED.[FaxFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FaxFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FaxFormat] Is Null And
				DELETED.[FaxFormat] Is Not Null
			) Or
			(
				INSERTED.[FaxFormat] Is Not Null And
				DELETED.[FaxFormat] Is Null
			) Or
			(
				INSERTED.[FaxFormat] !=
				DELETED.[FaxFormat]
			)
		) 
		END		
		
     If UPDATE([RevType2])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevType2',
     CONVERT(NVARCHAR(2000),DELETED.[RevType2],121),
     CONVERT(NVARCHAR(2000),INSERTED.[RevType2],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RevType2] Is Null And
				DELETED.[RevType2] Is Not Null
			) Or
			(
				INSERTED.[RevType2] Is Not Null And
				DELETED.[RevType2] Is Null
			) Or
			(
				INSERTED.[RevType2] !=
				DELETED.[RevType2]
			)
		) left join CFGRGMethods as oldDesc  on DELETED.RevType2 = oldDesc.Method  left join  CFGRGMethods as newDesc  on INSERTED.RevType2 = newDesc.Method
		END		
		
     If UPDATE([RevType3])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevType3',
     CONVERT(NVARCHAR(2000),DELETED.[RevType3],121),
     CONVERT(NVARCHAR(2000),INSERTED.[RevType3],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RevType3] Is Null And
				DELETED.[RevType3] Is Not Null
			) Or
			(
				INSERTED.[RevType3] Is Not Null And
				DELETED.[RevType3] Is Null
			) Or
			(
				INSERTED.[RevType3] !=
				DELETED.[RevType3]
			)
		) left join CFGRGMethods as oldDesc  on DELETED.RevType3 = oldDesc.Method  left join  CFGRGMethods as newDesc  on INSERTED.RevType3 = newDesc.Method
		END		
		
     If UPDATE([RevType4])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevType4',
     CONVERT(NVARCHAR(2000),DELETED.[RevType4],121),
     CONVERT(NVARCHAR(2000),INSERTED.[RevType4],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RevType4] Is Null And
				DELETED.[RevType4] Is Not Null
			) Or
			(
				INSERTED.[RevType4] Is Not Null And
				DELETED.[RevType4] Is Null
			) Or
			(
				INSERTED.[RevType4] !=
				DELETED.[RevType4]
			)
		) left join CFGRGMethods as oldDesc  on DELETED.RevType4 = oldDesc.Method  left join  CFGRGMethods as newDesc  on INSERTED.RevType4 = newDesc.Method
		END		
		
     If UPDATE([RevType5])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevType5',
     CONVERT(NVARCHAR(2000),DELETED.[RevType5],121),
     CONVERT(NVARCHAR(2000),INSERTED.[RevType5],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RevType5] Is Null And
				DELETED.[RevType5] Is Not Null
			) Or
			(
				INSERTED.[RevType5] Is Not Null And
				DELETED.[RevType5] Is Null
			) Or
			(
				INSERTED.[RevType5] !=
				DELETED.[RevType5]
			)
		) left join CFGRGMethods as oldDesc  on DELETED.RevType5 = oldDesc.Method  left join  CFGRGMethods as newDesc  on INSERTED.RevType5 = newDesc.Method
		END		
		
      If UPDATE([RevUpsetCategoryToAdjust])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetCategoryToAdjust',
      CONVERT(NVARCHAR(2000),DELETED.[RevUpsetCategoryToAdjust],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RevUpsetCategoryToAdjust],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RevUpsetCategoryToAdjust] Is Null And
				DELETED.[RevUpsetCategoryToAdjust] Is Not Null
			) Or
			(
				INSERTED.[RevUpsetCategoryToAdjust] Is Not Null And
				DELETED.[RevUpsetCategoryToAdjust] Is Null
			) Or
			(
				INSERTED.[RevUpsetCategoryToAdjust] !=
				DELETED.[RevUpsetCategoryToAdjust]
			)
		) 
		END		
		
      If UPDATE([FeeFunctionalCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FeeFunctionalCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[FeeFunctionalCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeFunctionalCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FeeFunctionalCurrency] Is Null And
				DELETED.[FeeFunctionalCurrency] Is Not Null
			) Or
			(
				INSERTED.[FeeFunctionalCurrency] Is Not Null And
				DELETED.[FeeFunctionalCurrency] Is Null
			) Or
			(
				INSERTED.[FeeFunctionalCurrency] !=
				DELETED.[FeeFunctionalCurrency]
			)
		) 
		END		
		
      If UPDATE([ReimbAllowFunctionalCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllowFunctionalCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllowFunctionalCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllowFunctionalCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ReimbAllowFunctionalCurrency] Is Null And
				DELETED.[ReimbAllowFunctionalCurrency] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllowFunctionalCurrency] Is Not Null And
				DELETED.[ReimbAllowFunctionalCurrency] Is Null
			) Or
			(
				INSERTED.[ReimbAllowFunctionalCurrency] !=
				DELETED.[ReimbAllowFunctionalCurrency]
			)
		) 
		END		
		
      If UPDATE([ConsultFeeFunctionalCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ConsultFeeFunctionalCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[ConsultFeeFunctionalCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ConsultFeeFunctionalCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ConsultFeeFunctionalCurrency] Is Null And
				DELETED.[ConsultFeeFunctionalCurrency] Is Not Null
			) Or
			(
				INSERTED.[ConsultFeeFunctionalCurrency] Is Not Null And
				DELETED.[ConsultFeeFunctionalCurrency] Is Null
			) Or
			(
				INSERTED.[ConsultFeeFunctionalCurrency] !=
				DELETED.[ConsultFeeFunctionalCurrency]
			)
		) 
		END		
		
      If UPDATE([RevenueMethod])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevenueMethod',
      CONVERT(NVARCHAR(2000),DELETED.[RevenueMethod],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RevenueMethod],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RevenueMethod] Is Null And
				DELETED.[RevenueMethod] Is Not Null
			) Or
			(
				INSERTED.[RevenueMethod] Is Not Null And
				DELETED.[RevenueMethod] Is Null
			) Or
			(
				INSERTED.[RevenueMethod] !=
				DELETED.[RevenueMethod]
			)
		) 
		END		
		
      If UPDATE([ICBillingLabTableNo])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ICBillingLabTableNo',
      CONVERT(NVARCHAR(2000),DELETED.[ICBillingLabTableNo],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ICBillingLabTableNo],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ICBillingLabTableNo] Is Null And
				DELETED.[ICBillingLabTableNo] Is Not Null
			) Or
			(
				INSERTED.[ICBillingLabTableNo] Is Not Null And
				DELETED.[ICBillingLabTableNo] Is Null
			) Or
			(
				INSERTED.[ICBillingLabTableNo] !=
				DELETED.[ICBillingLabTableNo]
			)
		) 
		END		
		
      If UPDATE([ICBillingExpTableNo])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ICBillingExpTableNo',
      CONVERT(NVARCHAR(2000),DELETED.[ICBillingExpTableNo],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ICBillingExpTableNo],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ICBillingExpTableNo] Is Null And
				DELETED.[ICBillingExpTableNo] Is Not Null
			) Or
			(
				INSERTED.[ICBillingExpTableNo] Is Not Null And
				DELETED.[ICBillingExpTableNo] Is Null
			) Or
			(
				INSERTED.[ICBillingExpTableNo] !=
				DELETED.[ICBillingExpTableNo]
			)
		) 
		END		
		
      If UPDATE([Biller])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Biller',
      CONVERT(NVARCHAR(2000),DELETED.[Biller],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Biller],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Biller] Is Null And
				DELETED.[Biller] Is Not Null
			) Or
			(
				INSERTED.[Biller] Is Not Null And
				DELETED.[Biller] Is Null
			) Or
			(
				INSERTED.[Biller] !=
				DELETED.[Biller]
			)
		) 
		END		
		
      If UPDATE([FeeDirLab])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FeeDirLab',
      CONVERT(NVARCHAR(2000),DELETED.[FeeDirLab],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeDirLab],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FeeDirLab] Is Null And
				DELETED.[FeeDirLab] Is Not Null
			) Or
			(
				INSERTED.[FeeDirLab] Is Not Null And
				DELETED.[FeeDirLab] Is Null
			) Or
			(
				INSERTED.[FeeDirLab] !=
				DELETED.[FeeDirLab]
			)
		) 
		END		
		
      If UPDATE([FeeDirExp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FeeDirExp',
      CONVERT(NVARCHAR(2000),DELETED.[FeeDirExp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeDirExp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FeeDirExp] Is Null And
				DELETED.[FeeDirExp] Is Not Null
			) Or
			(
				INSERTED.[FeeDirExp] Is Not Null And
				DELETED.[FeeDirExp] Is Null
			) Or
			(
				INSERTED.[FeeDirExp] !=
				DELETED.[FeeDirExp]
			)
		) 
		END		
		
      If UPDATE([ReimbAllowExp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllowExp',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllowExp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllowExp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ReimbAllowExp] Is Null And
				DELETED.[ReimbAllowExp] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllowExp] Is Not Null And
				DELETED.[ReimbAllowExp] Is Null
			) Or
			(
				INSERTED.[ReimbAllowExp] !=
				DELETED.[ReimbAllowExp]
			)
		) 
		END		
		
      If UPDATE([ReimbAllowCons])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllowCons',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllowCons],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllowCons],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ReimbAllowCons] Is Null And
				DELETED.[ReimbAllowCons] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllowCons] Is Not Null And
				DELETED.[ReimbAllowCons] Is Null
			) Or
			(
				INSERTED.[ReimbAllowCons] !=
				DELETED.[ReimbAllowCons]
			)
		) 
		END		
		
      If UPDATE([FeeDirLabBillingCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FeeDirLabBillingCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[FeeDirLabBillingCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeDirLabBillingCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FeeDirLabBillingCurrency] Is Null And
				DELETED.[FeeDirLabBillingCurrency] Is Not Null
			) Or
			(
				INSERTED.[FeeDirLabBillingCurrency] Is Not Null And
				DELETED.[FeeDirLabBillingCurrency] Is Null
			) Or
			(
				INSERTED.[FeeDirLabBillingCurrency] !=
				DELETED.[FeeDirLabBillingCurrency]
			)
		) 
		END		
		
      If UPDATE([FeeDirExpBillingCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FeeDirExpBillingCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[FeeDirExpBillingCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeDirExpBillingCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FeeDirExpBillingCurrency] Is Null And
				DELETED.[FeeDirExpBillingCurrency] Is Not Null
			) Or
			(
				INSERTED.[FeeDirExpBillingCurrency] Is Not Null And
				DELETED.[FeeDirExpBillingCurrency] Is Null
			) Or
			(
				INSERTED.[FeeDirExpBillingCurrency] !=
				DELETED.[FeeDirExpBillingCurrency]
			)
		) 
		END		
		
      If UPDATE([ReimbAllowExpBillingCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllowExpBillingCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllowExpBillingCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllowExpBillingCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ReimbAllowExpBillingCurrency] Is Null And
				DELETED.[ReimbAllowExpBillingCurrency] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllowExpBillingCurrency] Is Not Null And
				DELETED.[ReimbAllowExpBillingCurrency] Is Null
			) Or
			(
				INSERTED.[ReimbAllowExpBillingCurrency] !=
				DELETED.[ReimbAllowExpBillingCurrency]
			)
		) 
		END		
		
      If UPDATE([ReimbAllowConsBillingCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllowConsBillingCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllowConsBillingCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllowConsBillingCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ReimbAllowConsBillingCurrency] Is Null And
				DELETED.[ReimbAllowConsBillingCurrency] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllowConsBillingCurrency] Is Not Null And
				DELETED.[ReimbAllowConsBillingCurrency] Is Null
			) Or
			(
				INSERTED.[ReimbAllowConsBillingCurrency] !=
				DELETED.[ReimbAllowConsBillingCurrency]
			)
		) 
		END		
		
      If UPDATE([FeeDirLabFunctionalCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FeeDirLabFunctionalCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[FeeDirLabFunctionalCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeDirLabFunctionalCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FeeDirLabFunctionalCurrency] Is Null And
				DELETED.[FeeDirLabFunctionalCurrency] Is Not Null
			) Or
			(
				INSERTED.[FeeDirLabFunctionalCurrency] Is Not Null And
				DELETED.[FeeDirLabFunctionalCurrency] Is Null
			) Or
			(
				INSERTED.[FeeDirLabFunctionalCurrency] !=
				DELETED.[FeeDirLabFunctionalCurrency]
			)
		) 
		END		
		
      If UPDATE([FeeDirExpFunctionalCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FeeDirExpFunctionalCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[FeeDirExpFunctionalCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeDirExpFunctionalCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FeeDirExpFunctionalCurrency] Is Null And
				DELETED.[FeeDirExpFunctionalCurrency] Is Not Null
			) Or
			(
				INSERTED.[FeeDirExpFunctionalCurrency] Is Not Null And
				DELETED.[FeeDirExpFunctionalCurrency] Is Null
			) Or
			(
				INSERTED.[FeeDirExpFunctionalCurrency] !=
				DELETED.[FeeDirExpFunctionalCurrency]
			)
		) 
		END		
		
      If UPDATE([ReimbAllowExpFunctionalCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllowExpFunctionalCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllowExpFunctionalCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllowExpFunctionalCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ReimbAllowExpFunctionalCurrency] Is Null And
				DELETED.[ReimbAllowExpFunctionalCurrency] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllowExpFunctionalCurrency] Is Not Null And
				DELETED.[ReimbAllowExpFunctionalCurrency] Is Null
			) Or
			(
				INSERTED.[ReimbAllowExpFunctionalCurrency] !=
				DELETED.[ReimbAllowExpFunctionalCurrency]
			)
		) 
		END		
		
      If UPDATE([ReimbAllowConsFunctionalCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ReimbAllowConsFunctionalCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllowConsFunctionalCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllowConsFunctionalCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ReimbAllowConsFunctionalCurrency] Is Null And
				DELETED.[ReimbAllowConsFunctionalCurrency] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllowConsFunctionalCurrency] Is Not Null And
				DELETED.[ReimbAllowConsFunctionalCurrency] Is Null
			) Or
			(
				INSERTED.[ReimbAllowConsFunctionalCurrency] !=
				DELETED.[ReimbAllowConsFunctionalCurrency]
			)
		) 
		END		
		
      If UPDATE([RevUpsetIncludeCompDirExp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetIncludeCompDirExp',
      CONVERT(NVARCHAR(2000),DELETED.[RevUpsetIncludeCompDirExp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RevUpsetIncludeCompDirExp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RevUpsetIncludeCompDirExp] Is Null And
				DELETED.[RevUpsetIncludeCompDirExp] Is Not Null
			) Or
			(
				INSERTED.[RevUpsetIncludeCompDirExp] Is Not Null And
				DELETED.[RevUpsetIncludeCompDirExp] Is Null
			) Or
			(
				INSERTED.[RevUpsetIncludeCompDirExp] !=
				DELETED.[RevUpsetIncludeCompDirExp]
			)
		) 
		END		
		
      If UPDATE([RevUpsetIncludeReimbCons])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'RevUpsetIncludeReimbCons',
      CONVERT(NVARCHAR(2000),DELETED.[RevUpsetIncludeReimbCons],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RevUpsetIncludeReimbCons],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[RevUpsetIncludeReimbCons] Is Null And
				DELETED.[RevUpsetIncludeReimbCons] Is Not Null
			) Or
			(
				INSERTED.[RevUpsetIncludeReimbCons] Is Not Null And
				DELETED.[RevUpsetIncludeReimbCons] Is Null
			) Or
			(
				INSERTED.[RevUpsetIncludeReimbCons] !=
				DELETED.[RevUpsetIncludeReimbCons]
			)
		) 
		END		
		
      If UPDATE([AwardType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AwardType',
      CONVERT(NVARCHAR(2000),DELETED.[AwardType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AwardType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AwardType] Is Null And
				DELETED.[AwardType] Is Not Null
			) Or
			(
				INSERTED.[AwardType] Is Not Null And
				DELETED.[AwardType] Is Null
			) Or
			(
				INSERTED.[AwardType] !=
				DELETED.[AwardType]
			)
		) 
		END		
		
      If UPDATE([Duration])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Duration',
      CONVERT(NVARCHAR(2000),DELETED.[Duration],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Duration],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Duration] Is Null And
				DELETED.[Duration] Is Not Null
			) Or
			(
				INSERTED.[Duration] Is Not Null And
				DELETED.[Duration] Is Null
			) Or
			(
				INSERTED.[Duration] !=
				DELETED.[Duration]
			)
		) 
		END		
		
      If UPDATE([ContractTypeGovCon])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ContractTypeGovCon',
      CONVERT(NVARCHAR(2000),DELETED.[ContractTypeGovCon],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ContractTypeGovCon],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ContractTypeGovCon] Is Null And
				DELETED.[ContractTypeGovCon] Is Not Null
			) Or
			(
				INSERTED.[ContractTypeGovCon] Is Not Null And
				DELETED.[ContractTypeGovCon] Is Null
			) Or
			(
				INSERTED.[ContractTypeGovCon] !=
				DELETED.[ContractTypeGovCon]
			)
		) 
		END		
		
      If UPDATE([CompetitionType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CompetitionType',
      CONVERT(NVARCHAR(2000),DELETED.[CompetitionType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CompetitionType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CompetitionType] Is Null And
				DELETED.[CompetitionType] Is Not Null
			) Or
			(
				INSERTED.[CompetitionType] Is Not Null And
				DELETED.[CompetitionType] Is Null
			) Or
			(
				INSERTED.[CompetitionType] !=
				DELETED.[CompetitionType]
			)
		) 
		END		
		
      If UPDATE([MasterContract])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'MasterContract',
      CONVERT(NVARCHAR(2000),DELETED.[MasterContract],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MasterContract],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[MasterContract] Is Null And
				DELETED.[MasterContract] Is Not Null
			) Or
			(
				INSERTED.[MasterContract] Is Not Null And
				DELETED.[MasterContract] Is Null
			) Or
			(
				INSERTED.[MasterContract] !=
				DELETED.[MasterContract]
			)
		) 
		END		
		
      If UPDATE([Solicitation])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Solicitation',
      CONVERT(NVARCHAR(2000),DELETED.[Solicitation],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Solicitation],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Solicitation] Is Null And
				DELETED.[Solicitation] Is Not Null
			) Or
			(
				INSERTED.[Solicitation] Is Not Null And
				DELETED.[Solicitation] Is Null
			) Or
			(
				INSERTED.[Solicitation] !=
				DELETED.[Solicitation]
			)
		) 
		END		
		
      If UPDATE([NAICS])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'NAICS',
      CONVERT(NVARCHAR(2000),DELETED.[NAICS],121),
      CONVERT(NVARCHAR(2000),INSERTED.[NAICS],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[NAICS] Is Null And
				DELETED.[NAICS] Is Not Null
			) Or
			(
				INSERTED.[NAICS] Is Not Null And
				DELETED.[NAICS] Is Null
			) Or
			(
				INSERTED.[NAICS] !=
				DELETED.[NAICS]
			)
		) 
		END		
		
      If UPDATE([OurRole])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'OurRole',
      CONVERT(NVARCHAR(2000),DELETED.[OurRole],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OurRole],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[OurRole] Is Null And
				DELETED.[OurRole] Is Not Null
			) Or
			(
				INSERTED.[OurRole] Is Not Null And
				DELETED.[OurRole] Is Null
			) Or
			(
				INSERTED.[OurRole] !=
				DELETED.[OurRole]
			)
		) 
		END		
		
      If UPDATE([AjeraSync])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraSync',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraSync],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraSync],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AjeraSync] Is Null And
				DELETED.[AjeraSync] Is Not Null
			) Or
			(
				INSERTED.[AjeraSync] Is Not Null And
				DELETED.[AjeraSync] Is Null
			) Or
			(
				INSERTED.[AjeraSync] !=
				DELETED.[AjeraSync]
			)
		) 
		END		
		
      If UPDATE([ServProCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ServProCode',
      CONVERT(NVARCHAR(2000),DELETED.[ServProCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ServProCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ServProCode] Is Null And
				DELETED.[ServProCode] Is Not Null
			) Or
			(
				INSERTED.[ServProCode] Is Not Null And
				DELETED.[ServProCode] Is Null
			) Or
			(
				INSERTED.[ServProCode] !=
				DELETED.[ServProCode]
			)
		) 
		END		
		
      If UPDATE([FESurchargePct])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FESurchargePct',
      CONVERT(NVARCHAR(2000),DELETED.[FESurchargePct],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FESurchargePct],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FESurchargePct] Is Null And
				DELETED.[FESurchargePct] Is Not Null
			) Or
			(
				INSERTED.[FESurchargePct] Is Not Null And
				DELETED.[FESurchargePct] Is Null
			) Or
			(
				INSERTED.[FESurchargePct] !=
				DELETED.[FESurchargePct]
			)
		) 
		END		
		
      If UPDATE([FESurcharge])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FESurcharge',
      CONVERT(NVARCHAR(2000),DELETED.[FESurcharge],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FESurcharge],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FESurcharge] Is Null And
				DELETED.[FESurcharge] Is Not Null
			) Or
			(
				INSERTED.[FESurcharge] Is Not Null And
				DELETED.[FESurcharge] Is Null
			) Or
			(
				INSERTED.[FESurcharge] !=
				DELETED.[FESurcharge]
			)
		) 
		END		
		
      If UPDATE([FEAddlExpensesPct])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FEAddlExpensesPct',
      CONVERT(NVARCHAR(2000),DELETED.[FEAddlExpensesPct],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FEAddlExpensesPct],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FEAddlExpensesPct] Is Null And
				DELETED.[FEAddlExpensesPct] Is Not Null
			) Or
			(
				INSERTED.[FEAddlExpensesPct] Is Not Null And
				DELETED.[FEAddlExpensesPct] Is Null
			) Or
			(
				INSERTED.[FEAddlExpensesPct] !=
				DELETED.[FEAddlExpensesPct]
			)
		) 
		END		
		
      If UPDATE([FEAddlExpenses])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FEAddlExpenses',
      CONVERT(NVARCHAR(2000),DELETED.[FEAddlExpenses],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FEAddlExpenses],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FEAddlExpenses] Is Null And
				DELETED.[FEAddlExpenses] Is Not Null
			) Or
			(
				INSERTED.[FEAddlExpenses] Is Not Null And
				DELETED.[FEAddlExpenses] Is Null
			) Or
			(
				INSERTED.[FEAddlExpenses] !=
				DELETED.[FEAddlExpenses]
			)
		) 
		END		
		
      If UPDATE([FEOtherPct])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FEOtherPct',
      CONVERT(NVARCHAR(2000),DELETED.[FEOtherPct],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FEOtherPct],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FEOtherPct] Is Null And
				DELETED.[FEOtherPct] Is Not Null
			) Or
			(
				INSERTED.[FEOtherPct] Is Not Null And
				DELETED.[FEOtherPct] Is Null
			) Or
			(
				INSERTED.[FEOtherPct] !=
				DELETED.[FEOtherPct]
			)
		) 
		END		
		
      If UPDATE([FEOther])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'FEOther',
      CONVERT(NVARCHAR(2000),DELETED.[FEOther],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FEOther],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[FEOther] Is Null And
				DELETED.[FEOther] Is Not Null
			) Or
			(
				INSERTED.[FEOther] Is Not Null And
				DELETED.[FEOther] Is Null
			) Or
			(
				INSERTED.[FEOther] !=
				DELETED.[FEOther]
			)
		) 
		END		
		
      If UPDATE([ProjectTemplate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ProjectTemplate',
      CONVERT(NVARCHAR(2000),DELETED.[ProjectTemplate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProjectTemplate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ProjectTemplate] Is Null And
				DELETED.[ProjectTemplate] Is Not Null
			) Or
			(
				INSERTED.[ProjectTemplate] Is Not Null And
				DELETED.[ProjectTemplate] Is Null
			) Or
			(
				INSERTED.[ProjectTemplate] !=
				DELETED.[ProjectTemplate]
			)
		) 
		END		
		
      If UPDATE([AjeraSpentLabor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraSpentLabor',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraSpentLabor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraSpentLabor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AjeraSpentLabor] Is Null And
				DELETED.[AjeraSpentLabor] Is Not Null
			) Or
			(
				INSERTED.[AjeraSpentLabor] Is Not Null And
				DELETED.[AjeraSpentLabor] Is Null
			) Or
			(
				INSERTED.[AjeraSpentLabor] !=
				DELETED.[AjeraSpentLabor]
			)
		) 
		END		
		
      If UPDATE([AjeraSpentReimbursable])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraSpentReimbursable',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraSpentReimbursable],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraSpentReimbursable],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AjeraSpentReimbursable] Is Null And
				DELETED.[AjeraSpentReimbursable] Is Not Null
			) Or
			(
				INSERTED.[AjeraSpentReimbursable] Is Not Null And
				DELETED.[AjeraSpentReimbursable] Is Null
			) Or
			(
				INSERTED.[AjeraSpentReimbursable] !=
				DELETED.[AjeraSpentReimbursable]
			)
		) 
		END		
		
      If UPDATE([AjeraSpentConsultant])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraSpentConsultant',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraSpentConsultant],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraSpentConsultant],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AjeraSpentConsultant] Is Null And
				DELETED.[AjeraSpentConsultant] Is Not Null
			) Or
			(
				INSERTED.[AjeraSpentConsultant] Is Not Null And
				DELETED.[AjeraSpentConsultant] Is Null
			) Or
			(
				INSERTED.[AjeraSpentConsultant] !=
				DELETED.[AjeraSpentConsultant]
			)
		) 
		END		
		
      If UPDATE([AjeraCostLabor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraCostLabor',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraCostLabor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraCostLabor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AjeraCostLabor] Is Null And
				DELETED.[AjeraCostLabor] Is Not Null
			) Or
			(
				INSERTED.[AjeraCostLabor] Is Not Null And
				DELETED.[AjeraCostLabor] Is Null
			) Or
			(
				INSERTED.[AjeraCostLabor] !=
				DELETED.[AjeraCostLabor]
			)
		) 
		END		
		
      If UPDATE([AjeraCostReimbursable])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraCostReimbursable',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraCostReimbursable],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraCostReimbursable],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AjeraCostReimbursable] Is Null And
				DELETED.[AjeraCostReimbursable] Is Not Null
			) Or
			(
				INSERTED.[AjeraCostReimbursable] Is Not Null And
				DELETED.[AjeraCostReimbursable] Is Null
			) Or
			(
				INSERTED.[AjeraCostReimbursable] !=
				DELETED.[AjeraCostReimbursable]
			)
		) 
		END		
		
      If UPDATE([AjeraCostConsultant])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraCostConsultant',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraCostConsultant],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraCostConsultant],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AjeraCostConsultant] Is Null And
				DELETED.[AjeraCostConsultant] Is Not Null
			) Or
			(
				INSERTED.[AjeraCostConsultant] Is Not Null And
				DELETED.[AjeraCostConsultant] Is Null
			) Or
			(
				INSERTED.[AjeraCostConsultant] !=
				DELETED.[AjeraCostConsultant]
			)
		) 
		END		
		
      If UPDATE([AjeraWIPLabor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraWIPLabor',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraWIPLabor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraWIPLabor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AjeraWIPLabor] Is Null And
				DELETED.[AjeraWIPLabor] Is Not Null
			) Or
			(
				INSERTED.[AjeraWIPLabor] Is Not Null And
				DELETED.[AjeraWIPLabor] Is Null
			) Or
			(
				INSERTED.[AjeraWIPLabor] !=
				DELETED.[AjeraWIPLabor]
			)
		) 
		END		
		
      If UPDATE([AjeraWIPReimbursable])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraWIPReimbursable',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraWIPReimbursable],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraWIPReimbursable],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AjeraWIPReimbursable] Is Null And
				DELETED.[AjeraWIPReimbursable] Is Not Null
			) Or
			(
				INSERTED.[AjeraWIPReimbursable] Is Not Null And
				DELETED.[AjeraWIPReimbursable] Is Null
			) Or
			(
				INSERTED.[AjeraWIPReimbursable] !=
				DELETED.[AjeraWIPReimbursable]
			)
		) 
		END		
		
      If UPDATE([AjeraWIPConsultant])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraWIPConsultant',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraWIPConsultant],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraWIPConsultant],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AjeraWIPConsultant] Is Null And
				DELETED.[AjeraWIPConsultant] Is Not Null
			) Or
			(
				INSERTED.[AjeraWIPConsultant] Is Not Null And
				DELETED.[AjeraWIPConsultant] Is Null
			) Or
			(
				INSERTED.[AjeraWIPConsultant] !=
				DELETED.[AjeraWIPConsultant]
			)
		) 
		END		
		
      If UPDATE([AjeraBilledLabor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraBilledLabor',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraBilledLabor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraBilledLabor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AjeraBilledLabor] Is Null And
				DELETED.[AjeraBilledLabor] Is Not Null
			) Or
			(
				INSERTED.[AjeraBilledLabor] Is Not Null And
				DELETED.[AjeraBilledLabor] Is Null
			) Or
			(
				INSERTED.[AjeraBilledLabor] !=
				DELETED.[AjeraBilledLabor]
			)
		) 
		END		
		
      If UPDATE([AjeraBilledReimbursable])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraBilledReimbursable',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraBilledReimbursable],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraBilledReimbursable],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AjeraBilledReimbursable] Is Null And
				DELETED.[AjeraBilledReimbursable] Is Not Null
			) Or
			(
				INSERTED.[AjeraBilledReimbursable] Is Not Null And
				DELETED.[AjeraBilledReimbursable] Is Null
			) Or
			(
				INSERTED.[AjeraBilledReimbursable] !=
				DELETED.[AjeraBilledReimbursable]
			)
		) 
		END		
		
      If UPDATE([AjeraBilledConsultant])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraBilledConsultant',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraBilledConsultant],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraBilledConsultant],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AjeraBilledConsultant] Is Null And
				DELETED.[AjeraBilledConsultant] Is Not Null
			) Or
			(
				INSERTED.[AjeraBilledConsultant] Is Not Null And
				DELETED.[AjeraBilledConsultant] Is Null
			) Or
			(
				INSERTED.[AjeraBilledConsultant] !=
				DELETED.[AjeraBilledConsultant]
			)
		) 
		END		
		
      If UPDATE([AjeraReceivedLabor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraReceivedLabor',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraReceivedLabor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraReceivedLabor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AjeraReceivedLabor] Is Null And
				DELETED.[AjeraReceivedLabor] Is Not Null
			) Or
			(
				INSERTED.[AjeraReceivedLabor] Is Not Null And
				DELETED.[AjeraReceivedLabor] Is Null
			) Or
			(
				INSERTED.[AjeraReceivedLabor] !=
				DELETED.[AjeraReceivedLabor]
			)
		) 
		END		
		
      If UPDATE([AjeraReceivedReimbursable])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraReceivedReimbursable',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraReceivedReimbursable],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraReceivedReimbursable],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AjeraReceivedReimbursable] Is Null And
				DELETED.[AjeraReceivedReimbursable] Is Not Null
			) Or
			(
				INSERTED.[AjeraReceivedReimbursable] Is Not Null And
				DELETED.[AjeraReceivedReimbursable] Is Null
			) Or
			(
				INSERTED.[AjeraReceivedReimbursable] !=
				DELETED.[AjeraReceivedReimbursable]
			)
		) 
		END		
		
      If UPDATE([AjeraReceivedConsultant])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AjeraReceivedConsultant',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraReceivedConsultant],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraReceivedConsultant],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AjeraReceivedConsultant] Is Null And
				DELETED.[AjeraReceivedConsultant] Is Not Null
			) Or
			(
				INSERTED.[AjeraReceivedConsultant] Is Not Null And
				DELETED.[AjeraReceivedConsultant] Is Null
			) Or
			(
				INSERTED.[AjeraReceivedConsultant] !=
				DELETED.[AjeraReceivedConsultant]
			)
		) 
		END		
		
      If UPDATE([TLInternalKey])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TLInternalKey',
      CONVERT(NVARCHAR(2000),DELETED.[TLInternalKey],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TLInternalKey],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[TLInternalKey] Is Null And
				DELETED.[TLInternalKey] Is Not Null
			) Or
			(
				INSERTED.[TLInternalKey] Is Not Null And
				DELETED.[TLInternalKey] Is Null
			) Or
			(
				INSERTED.[TLInternalKey] !=
				DELETED.[TLInternalKey]
			)
		) 
		END		
		
      If UPDATE([TLProjectID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TLProjectID',
      CONVERT(NVARCHAR(2000),DELETED.[TLProjectID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TLProjectID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[TLProjectID] Is Null And
				DELETED.[TLProjectID] Is Not Null
			) Or
			(
				INSERTED.[TLProjectID] Is Not Null And
				DELETED.[TLProjectID] Is Null
			) Or
			(
				INSERTED.[TLProjectID] !=
				DELETED.[TLProjectID]
			)
		) 
		END		
		
      If UPDATE([TLProjectName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TLProjectName',
      CONVERT(NVARCHAR(2000),DELETED.[TLProjectName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TLProjectName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[TLProjectName] Is Null And
				DELETED.[TLProjectName] Is Not Null
			) Or
			(
				INSERTED.[TLProjectName] Is Not Null And
				DELETED.[TLProjectName] Is Null
			) Or
			(
				INSERTED.[TLProjectName] !=
				DELETED.[TLProjectName]
			)
		) 
		END		
		
      If UPDATE([TLChargeBandInternalKey])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TLChargeBandInternalKey',
      CONVERT(NVARCHAR(2000),DELETED.[TLChargeBandInternalKey],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TLChargeBandInternalKey],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[TLChargeBandInternalKey] Is Null And
				DELETED.[TLChargeBandInternalKey] Is Not Null
			) Or
			(
				INSERTED.[TLChargeBandInternalKey] Is Not Null And
				DELETED.[TLChargeBandInternalKey] Is Null
			) Or
			(
				INSERTED.[TLChargeBandInternalKey] !=
				DELETED.[TLChargeBandInternalKey]
			)
		) 
		END		
		
      If UPDATE([TLChargeBandExternalCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TLChargeBandExternalCode',
      CONVERT(NVARCHAR(2000),DELETED.[TLChargeBandExternalCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TLChargeBandExternalCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[TLChargeBandExternalCode] Is Null And
				DELETED.[TLChargeBandExternalCode] Is Not Null
			) Or
			(
				INSERTED.[TLChargeBandExternalCode] Is Not Null And
				DELETED.[TLChargeBandExternalCode] Is Null
			) Or
			(
				INSERTED.[TLChargeBandExternalCode] !=
				DELETED.[TLChargeBandExternalCode]
			)
		) 
		END		
		
      If UPDATE([TLSyncModDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TLSyncModDate',
      CONVERT(NVARCHAR(2000),DELETED.[TLSyncModDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TLSyncModDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[TLSyncModDate] Is Null And
				DELETED.[TLSyncModDate] Is Not Null
			) Or
			(
				INSERTED.[TLSyncModDate] Is Not Null And
				DELETED.[TLSyncModDate] Is Null
			) Or
			(
				INSERTED.[TLSyncModDate] !=
				DELETED.[TLSyncModDate]
			)
		) 
		END		
		
      If UPDATE([SiblingWBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'SiblingWBS1',
      CONVERT(NVARCHAR(2000),DELETED.[SiblingWBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SiblingWBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[SiblingWBS1] Is Null And
				DELETED.[SiblingWBS1] Is Not Null
			) Or
			(
				INSERTED.[SiblingWBS1] Is Not Null And
				DELETED.[SiblingWBS1] Is Null
			) Or
			(
				INSERTED.[SiblingWBS1] !=
				DELETED.[SiblingWBS1]
			)
		) 
		END		
		
      If UPDATE([Stage])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Stage',
      CONVERT(NVARCHAR(2000),DELETED.[Stage],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Stage],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Stage] Is Null And
				DELETED.[Stage] Is Not Null
			) Or
			(
				INSERTED.[Stage] Is Not Null And
				DELETED.[Stage] Is Null
			) Or
			(
				INSERTED.[Stage] !=
				DELETED.[Stage]
			)
		) 
		END		
		
      If UPDATE([EstStartDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'EstStartDate',
      CONVERT(NVARCHAR(2000),DELETED.[EstStartDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EstStartDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[EstStartDate] Is Null And
				DELETED.[EstStartDate] Is Not Null
			) Or
			(
				INSERTED.[EstStartDate] Is Not Null And
				DELETED.[EstStartDate] Is Null
			) Or
			(
				INSERTED.[EstStartDate] !=
				DELETED.[EstStartDate]
			)
		) 
		END		
		
      If UPDATE([EstEndDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'EstEndDate',
      CONVERT(NVARCHAR(2000),DELETED.[EstEndDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EstEndDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[EstEndDate] Is Null And
				DELETED.[EstEndDate] Is Not Null
			) Or
			(
				INSERTED.[EstEndDate] Is Not Null And
				DELETED.[EstEndDate] Is Null
			) Or
			(
				INSERTED.[EstEndDate] !=
				DELETED.[EstEndDate]
			)
		) 
		END		
		
      If UPDATE([EstFees])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'EstFees',
      CONVERT(NVARCHAR(2000),DELETED.[EstFees],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EstFees],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[EstFees] Is Null And
				DELETED.[EstFees] Is Not Null
			) Or
			(
				INSERTED.[EstFees] Is Not Null And
				DELETED.[EstFees] Is Null
			) Or
			(
				INSERTED.[EstFees] !=
				DELETED.[EstFees]
			)
		) 
		END		
		
      If UPDATE([EstConstructionCost])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'EstConstructionCost',
      CONVERT(NVARCHAR(2000),DELETED.[EstConstructionCost],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EstConstructionCost],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[EstConstructionCost] Is Null And
				DELETED.[EstConstructionCost] Is Not Null
			) Or
			(
				INSERTED.[EstConstructionCost] Is Not Null And
				DELETED.[EstConstructionCost] Is Null
			) Or
			(
				INSERTED.[EstConstructionCost] !=
				DELETED.[EstConstructionCost]
			)
		) 
		END		
		
      If UPDATE([Revenue])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Revenue',
      CONVERT(NVARCHAR(2000),DELETED.[Revenue],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Revenue],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Revenue] Is Null And
				DELETED.[Revenue] Is Not Null
			) Or
			(
				INSERTED.[Revenue] Is Not Null And
				DELETED.[Revenue] Is Null
			) Or
			(
				INSERTED.[Revenue] !=
				DELETED.[Revenue]
			)
		) 
		END		
		
      If UPDATE([Probability])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Probability',
      CONVERT(NVARCHAR(2000),DELETED.[Probability],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Probability],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Probability] Is Null And
				DELETED.[Probability] Is Not Null
			) Or
			(
				INSERTED.[Probability] Is Not Null And
				DELETED.[Probability] Is Null
			) Or
			(
				INSERTED.[Probability] !=
				DELETED.[Probability]
			)
		) 
		END		
		
      If UPDATE([WeightedRevenue])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'WeightedRevenue',
      CONVERT(NVARCHAR(2000),DELETED.[WeightedRevenue],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WeightedRevenue],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[WeightedRevenue] Is Null And
				DELETED.[WeightedRevenue] Is Not Null
			) Or
			(
				INSERTED.[WeightedRevenue] Is Not Null And
				DELETED.[WeightedRevenue] Is Null
			) Or
			(
				INSERTED.[WeightedRevenue] !=
				DELETED.[WeightedRevenue]
			)
		) 
		END		
		
      If UPDATE([CloseDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CloseDate',
      CONVERT(NVARCHAR(2000),DELETED.[CloseDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CloseDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CloseDate] Is Null And
				DELETED.[CloseDate] Is Not Null
			) Or
			(
				INSERTED.[CloseDate] Is Not Null And
				DELETED.[CloseDate] Is Null
			) Or
			(
				INSERTED.[CloseDate] !=
				DELETED.[CloseDate]
			)
		) 
		END		
		
      If UPDATE([OpenDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'OpenDate',
      CONVERT(NVARCHAR(2000),DELETED.[OpenDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OpenDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[OpenDate] Is Null And
				DELETED.[OpenDate] Is Not Null
			) Or
			(
				INSERTED.[OpenDate] Is Not Null And
				DELETED.[OpenDate] Is Null
			) Or
			(
				INSERTED.[OpenDate] !=
				DELETED.[OpenDate]
			)
		) 
		END		
		
      If UPDATE([Source])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Source',
      CONVERT(NVARCHAR(2000),DELETED.[Source],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Source],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Source] Is Null And
				DELETED.[Source] Is Not Null
			) Or
			(
				INSERTED.[Source] Is Not Null And
				DELETED.[Source] Is Null
			) Or
			(
				INSERTED.[Source] !=
				DELETED.[Source]
			)
		) 
		END		
		
      If UPDATE([PublicNoticeDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PublicNoticeDate',
      CONVERT(NVARCHAR(2000),DELETED.[PublicNoticeDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PublicNoticeDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[PublicNoticeDate] Is Null And
				DELETED.[PublicNoticeDate] Is Not Null
			) Or
			(
				INSERTED.[PublicNoticeDate] Is Not Null And
				DELETED.[PublicNoticeDate] Is Null
			) Or
			(
				INSERTED.[PublicNoticeDate] !=
				DELETED.[PublicNoticeDate]
			)
		) 
		END		
		
      If UPDATE([SolicitationNum])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'SolicitationNum',
      CONVERT(NVARCHAR(2000),DELETED.[SolicitationNum],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SolicitationNum],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[SolicitationNum] Is Null And
				DELETED.[SolicitationNum] Is Not Null
			) Or
			(
				INSERTED.[SolicitationNum] Is Not Null And
				DELETED.[SolicitationNum] Is Null
			) Or
			(
				INSERTED.[SolicitationNum] !=
				DELETED.[SolicitationNum]
			)
		) 
		END		
		
      If UPDATE([CustomCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustomCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustomCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustomCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustomCurrencyCode] Is Null And
				DELETED.[CustomCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] Is Not Null And
				DELETED.[CustomCurrencyCode] Is Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] !=
				DELETED.[CustomCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([LabBillTable])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'LabBillTable',
      CONVERT(NVARCHAR(2000),DELETED.[LabBillTable],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LabBillTable],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[LabBillTable] Is Null And
				DELETED.[LabBillTable] Is Not Null
			) Or
			(
				INSERTED.[LabBillTable] Is Not Null And
				DELETED.[LabBillTable] Is Null
			) Or
			(
				INSERTED.[LabBillTable] !=
				DELETED.[LabBillTable]
			)
		) 
		END		
		
      If UPDATE([LabCostTable])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'LabCostTable',
      CONVERT(NVARCHAR(2000),DELETED.[LabCostTable],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LabCostTable],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[LabCostTable] Is Null And
				DELETED.[LabCostTable] Is Not Null
			) Or
			(
				INSERTED.[LabCostTable] Is Not Null And
				DELETED.[LabCostTable] Is Null
			) Or
			(
				INSERTED.[LabCostTable] !=
				DELETED.[LabCostTable]
			)
		) 
		END		
		
      If UPDATE([AllocMethod])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'AllocMethod',
      CONVERT(NVARCHAR(2000),DELETED.[AllocMethod],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllocMethod],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[AllocMethod] Is Null And
				DELETED.[AllocMethod] Is Not Null
			) Or
			(
				INSERTED.[AllocMethod] Is Not Null And
				DELETED.[AllocMethod] Is Null
			) Or
			(
				INSERTED.[AllocMethod] !=
				DELETED.[AllocMethod]
			)
		) 
		END		
		
      If UPDATE([Timescale])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'Timescale',
      CONVERT(NVARCHAR(2000),DELETED.[Timescale],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Timescale],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[Timescale] Is Null And
				DELETED.[Timescale] Is Not Null
			) Or
			(
				INSERTED.[Timescale] Is Not Null And
				DELETED.[Timescale] Is Null
			) Or
			(
				INSERTED.[Timescale] !=
				DELETED.[Timescale]
			)
		) 
		END		
		
      If UPDATE([ClosedReason])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ClosedReason',
      CONVERT(NVARCHAR(2000),DELETED.[ClosedReason],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClosedReason],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ClosedReason] Is Null And
				DELETED.[ClosedReason] Is Not Null
			) Or
			(
				INSERTED.[ClosedReason] Is Not Null And
				DELETED.[ClosedReason] Is Null
			) Or
			(
				INSERTED.[ClosedReason] !=
				DELETED.[ClosedReason]
			)
		) 
		END		
		
      If UPDATE([ClosedNotes])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ClosedNotes',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([IQID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'IQID',
      CONVERT(NVARCHAR(2000),DELETED.[IQID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[IQID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[IQID] Is Null And
				DELETED.[IQID] Is Not Null
			) Or
			(
				INSERTED.[IQID] Is Not Null And
				DELETED.[IQID] Is Null
			) Or
			(
				INSERTED.[IQID] !=
				DELETED.[IQID]
			)
		) 
		END		
		
      If UPDATE([IQLastUpdate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'IQLastUpdate',
      CONVERT(NVARCHAR(2000),DELETED.[IQLastUpdate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[IQLastUpdate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[IQLastUpdate] Is Null And
				DELETED.[IQLastUpdate] Is Not Null
			) Or
			(
				INSERTED.[IQLastUpdate] Is Not Null And
				DELETED.[IQLastUpdate] Is Null
			) Or
			(
				INSERTED.[IQLastUpdate] !=
				DELETED.[IQLastUpdate]
			)
		) 
		END		
		
      If UPDATE([MarketingCoordinator])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'MarketingCoordinator',
      CONVERT(NVARCHAR(2000),DELETED.[MarketingCoordinator],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MarketingCoordinator],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[MarketingCoordinator] Is Null And
				DELETED.[MarketingCoordinator] Is Not Null
			) Or
			(
				INSERTED.[MarketingCoordinator] Is Not Null And
				DELETED.[MarketingCoordinator] Is Null
			) Or
			(
				INSERTED.[MarketingCoordinator] !=
				DELETED.[MarketingCoordinator]
			)
		) 
		END		
		
      If UPDATE([ProposalManager])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'ProposalManager',
      CONVERT(NVARCHAR(2000),DELETED.[ProposalManager],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProposalManager],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[ProposalManager] Is Null And
				DELETED.[ProposalManager] Is Not Null
			) Or
			(
				INSERTED.[ProposalManager] Is Not Null And
				DELETED.[ProposalManager] Is Null
			) Or
			(
				INSERTED.[ProposalManager] !=
				DELETED.[ProposalManager]
			)
		) 
		END		
		
      If UPDATE([BusinessDeveloperLead])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'BusinessDeveloperLead',
      CONVERT(NVARCHAR(2000),DELETED.[BusinessDeveloperLead],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BusinessDeveloperLead],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[BusinessDeveloperLead] Is Null And
				DELETED.[BusinessDeveloperLead] Is Not Null
			) Or
			(
				INSERTED.[BusinessDeveloperLead] Is Not Null And
				DELETED.[BusinessDeveloperLead] Is Null
			) Or
			(
				INSERTED.[BusinessDeveloperLead] !=
				DELETED.[BusinessDeveloperLead]
			)
		) 
		END		
		
      If UPDATE([SFID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'SFID',
      CONVERT(NVARCHAR(2000),DELETED.[SFID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SFID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[SFID] Is Null And
				DELETED.[SFID] Is Not Null
			) Or
			(
				INSERTED.[SFID] Is Not Null And
				DELETED.[SFID] Is Null
			) Or
			(
				INSERTED.[SFID] !=
				DELETED.[SFID]
			)
		) 
		END		
		
      If UPDATE([SFLastModifiedDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'SFLastModifiedDate',
      CONVERT(NVARCHAR(2000),DELETED.[SFLastModifiedDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SFLastModifiedDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[SFLastModifiedDate] Is Null And
				DELETED.[SFLastModifiedDate] Is Not Null
			) Or
			(
				INSERTED.[SFLastModifiedDate] Is Not Null And
				DELETED.[SFLastModifiedDate] Is Null
			) Or
			(
				INSERTED.[SFLastModifiedDate] !=
				DELETED.[SFLastModifiedDate]
			)
		) 
		END		
		
      If UPDATE([TotalContractValue])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'TotalContractValue',
      CONVERT(NVARCHAR(2000),DELETED.[TotalContractValue],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TotalContractValue],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[TotalContractValue] Is Null And
				DELETED.[TotalContractValue] Is Not Null
			) Or
			(
				INSERTED.[TotalContractValue] Is Not Null And
				DELETED.[TotalContractValue] Is Null
			) Or
			(
				INSERTED.[TotalContractValue] !=
				DELETED.[TotalContractValue]
			)
		) 
		END		
		
      If UPDATE([PeriodOfPerformance])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PeriodOfPerformance',
      CONVERT(NVARCHAR(2000),DELETED.[PeriodOfPerformance],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PeriodOfPerformance],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[PeriodOfPerformance] Is Null And
				DELETED.[PeriodOfPerformance] Is Not Null
			) Or
			(
				INSERTED.[PeriodOfPerformance] Is Not Null And
				DELETED.[PeriodOfPerformance] Is Null
			) Or
			(
				INSERTED.[PeriodOfPerformance] !=
				DELETED.[PeriodOfPerformance]
			)
		) 
		END		
		
      If UPDATE([LostTo])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'LostTo',
      CONVERT(NVARCHAR(2000),DELETED.[LostTo],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LostTo],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[LostTo] Is Null And
				DELETED.[LostTo] Is Not Null
			) Or
			(
				INSERTED.[LostTo] Is Not Null And
				DELETED.[LostTo] Is Null
			) Or
			(
				INSERTED.[LostTo] !=
				DELETED.[LostTo]
			)
		) 
		END		
		
      If UPDATE([PreAwardWBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PreAwardWBS1',
      CONVERT(NVARCHAR(2000),DELETED.[PreAwardWBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PreAwardWBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[PreAwardWBS1] Is Null And
				DELETED.[PreAwardWBS1] Is Not Null
			) Or
			(
				INSERTED.[PreAwardWBS1] Is Not Null And
				DELETED.[PreAwardWBS1] Is Null
			) Or
			(
				INSERTED.[PreAwardWBS1] !=
				DELETED.[PreAwardWBS1]
			)
		) 
		END		
		
      If UPDATE([UtilizationScheduleFlg])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'UtilizationScheduleFlg',
      CONVERT(NVARCHAR(2000),DELETED.[UtilizationScheduleFlg],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UtilizationScheduleFlg],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[UtilizationScheduleFlg] Is Null And
				DELETED.[UtilizationScheduleFlg] Is Not Null
			) Or
			(
				INSERTED.[UtilizationScheduleFlg] Is Not Null And
				DELETED.[UtilizationScheduleFlg] Is Null
			) Or
			(
				INSERTED.[UtilizationScheduleFlg] !=
				DELETED.[UtilizationScheduleFlg]
			)
		) 
		END		
		
      If UPDATE([PIMID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'PIMID',
      CONVERT(NVARCHAR(2000),DELETED.[PIMID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PIMID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[PIMID] Is Null And
				DELETED.[PIMID] Is Not Null
			) Or
			(
				INSERTED.[PIMID] Is Not Null And
				DELETED.[PIMID] Is Null
			) Or
			(
				INSERTED.[PIMID] !=
				DELETED.[PIMID]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_PRTemplate] ON [dbo].[PRTemplate]
GO
ALTER TABLE [dbo].[PRTemplate] ADD CONSTRAINT [PRTemplatePK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
