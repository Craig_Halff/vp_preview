CREATE TABLE [dbo].[CCG_TR_ConfigPredefComments]
(
[CommentId] [int] NOT NULL IDENTITY(1, 1),
[CommentType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL,
[Comment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_TR_ConfigPredefComments] ADD CONSTRAINT [PK_CCG_TR_ConfigPredefComments] PRIMARY KEY CLUSTERED ([CommentId]) ON [PRIMARY]
GO
