CREATE TABLE [dbo].[CFGMainDataDefaultTaxCodes]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__CFGMainData__Seq__79FF45CA] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGMainDataDefaultTaxCodes] ADD CONSTRAINT [CFGMainDataDefaultTaxCodesPK] PRIMARY KEY CLUSTERED ([Company], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
