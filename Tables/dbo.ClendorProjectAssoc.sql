CREATE TABLE [dbo].[ClendorProjectAssoc]
(
[PKey] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Role] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoleDescription] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TeamStatus] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrimaryInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ClendorPr__Prima__1CE0CFB8] DEFAULT ('N'),
[ClientConfidential] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ClendorPr__Clien__1DD4F3F1] DEFAULT ('N'),
[VendorInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ClendorPr__Vendo__1EC9182A] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_ClendorProjectAssoc]
      ON [dbo].[ClendorProjectAssoc]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ClendorProjectAssoc'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'PKey',CONVERT(NVARCHAR(2000),[PKey],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'ClientID',CONVERT(NVARCHAR(2000),[ClientID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'WBS1',CONVERT(NVARCHAR(2000),[WBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'WBS2',CONVERT(NVARCHAR(2000),[WBS2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'WBS3',CONVERT(NVARCHAR(2000),[WBS3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'Role',CONVERT(NVARCHAR(2000),[Role],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'RoleDescription','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'TeamStatus',CONVERT(NVARCHAR(2000),[TeamStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'Address',CONVERT(NVARCHAR(2000),[Address],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'PrimaryInd',CONVERT(NVARCHAR(2000),[PrimaryInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'ClientConfidential',CONVERT(NVARCHAR(2000),[ClientConfidential],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'ClientInd',CONVERT(NVARCHAR(2000),[ClientInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'VendorInd',CONVERT(NVARCHAR(2000),[VendorInd],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_ClendorProjectAssoc]
      ON [dbo].[ClendorProjectAssoc]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ClendorProjectAssoc'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'PKey',NULL,CONVERT(NVARCHAR(2000),[PKey],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'ClientID',NULL,CONVERT(NVARCHAR(2000),[ClientID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'WBS1',NULL,CONVERT(NVARCHAR(2000),[WBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'WBS2',NULL,CONVERT(NVARCHAR(2000),[WBS2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'WBS3',NULL,CONVERT(NVARCHAR(2000),[WBS3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'Role',NULL,CONVERT(NVARCHAR(2000),[Role],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'RoleDescription',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'TeamStatus',NULL,CONVERT(NVARCHAR(2000),[TeamStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'Address',NULL,CONVERT(NVARCHAR(2000),[Address],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'PrimaryInd',NULL,CONVERT(NVARCHAR(2000),[PrimaryInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'ClientConfidential',NULL,CONVERT(NVARCHAR(2000),[ClientConfidential],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'ClientInd',NULL,CONVERT(NVARCHAR(2000),[ClientInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'VendorInd',NULL,CONVERT(NVARCHAR(2000),[VendorInd],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_ClendorProjectAssoc]
      ON [dbo].[ClendorProjectAssoc]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ClendorProjectAssoc'
    
      If UPDATE([PKey])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'PKey',
      CONVERT(NVARCHAR(2000),DELETED.[PKey],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PKey],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[PKey] Is Null And
				DELETED.[PKey] Is Not Null
			) Or
			(
				INSERTED.[PKey] Is Not Null And
				DELETED.[PKey] Is Null
			) Or
			(
				INSERTED.[PKey] !=
				DELETED.[PKey]
			)
		) 
		END		
		
      If UPDATE([ClientID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'ClientID',
      CONVERT(NVARCHAR(2000),DELETED.[ClientID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClientID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[ClientID] Is Null And
				DELETED.[ClientID] Is Not Null
			) Or
			(
				INSERTED.[ClientID] Is Not Null And
				DELETED.[ClientID] Is Null
			) Or
			(
				INSERTED.[ClientID] !=
				DELETED.[ClientID]
			)
		) 
		END		
		
      If UPDATE([WBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'WBS1',
      CONVERT(NVARCHAR(2000),DELETED.[WBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[WBS1] Is Null And
				DELETED.[WBS1] Is Not Null
			) Or
			(
				INSERTED.[WBS1] Is Not Null And
				DELETED.[WBS1] Is Null
			) Or
			(
				INSERTED.[WBS1] !=
				DELETED.[WBS1]
			)
		) 
		END		
		
      If UPDATE([WBS2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'WBS2',
      CONVERT(NVARCHAR(2000),DELETED.[WBS2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[WBS2] Is Null And
				DELETED.[WBS2] Is Not Null
			) Or
			(
				INSERTED.[WBS2] Is Not Null And
				DELETED.[WBS2] Is Null
			) Or
			(
				INSERTED.[WBS2] !=
				DELETED.[WBS2]
			)
		) 
		END		
		
      If UPDATE([WBS3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'WBS3',
      CONVERT(NVARCHAR(2000),DELETED.[WBS3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[WBS3] Is Null And
				DELETED.[WBS3] Is Not Null
			) Or
			(
				INSERTED.[WBS3] Is Not Null And
				DELETED.[WBS3] Is Null
			) Or
			(
				INSERTED.[WBS3] !=
				DELETED.[WBS3]
			)
		) 
		END		
		
      If UPDATE([Role])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'Role',
      CONVERT(NVARCHAR(2000),DELETED.[Role],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Role],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[Role] Is Null And
				DELETED.[Role] Is Not Null
			) Or
			(
				INSERTED.[Role] Is Not Null And
				DELETED.[Role] Is Null
			) Or
			(
				INSERTED.[Role] !=
				DELETED.[Role]
			)
		) 
		END		
		
      If UPDATE([RoleDescription])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'RoleDescription',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([TeamStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'TeamStatus',
      CONVERT(NVARCHAR(2000),DELETED.[TeamStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TeamStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[TeamStatus] Is Null And
				DELETED.[TeamStatus] Is Not Null
			) Or
			(
				INSERTED.[TeamStatus] Is Not Null And
				DELETED.[TeamStatus] Is Null
			) Or
			(
				INSERTED.[TeamStatus] !=
				DELETED.[TeamStatus]
			)
		) 
		END		
		
      If UPDATE([Address])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'Address',
      CONVERT(NVARCHAR(2000),DELETED.[Address],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Address],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[Address] Is Null And
				DELETED.[Address] Is Not Null
			) Or
			(
				INSERTED.[Address] Is Not Null And
				DELETED.[Address] Is Null
			) Or
			(
				INSERTED.[Address] !=
				DELETED.[Address]
			)
		) 
		END		
		
      If UPDATE([PrimaryInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'PrimaryInd',
      CONVERT(NVARCHAR(2000),DELETED.[PrimaryInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PrimaryInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[PrimaryInd] Is Null And
				DELETED.[PrimaryInd] Is Not Null
			) Or
			(
				INSERTED.[PrimaryInd] Is Not Null And
				DELETED.[PrimaryInd] Is Null
			) Or
			(
				INSERTED.[PrimaryInd] !=
				DELETED.[PrimaryInd]
			)
		) 
		END		
		
      If UPDATE([ClientConfidential])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'ClientConfidential',
      CONVERT(NVARCHAR(2000),DELETED.[ClientConfidential],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClientConfidential],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[ClientConfidential] Is Null And
				DELETED.[ClientConfidential] Is Not Null
			) Or
			(
				INSERTED.[ClientConfidential] Is Not Null And
				DELETED.[ClientConfidential] Is Null
			) Or
			(
				INSERTED.[ClientConfidential] !=
				DELETED.[ClientConfidential]
			)
		) 
		END		
		
      If UPDATE([ClientInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'ClientInd',
      CONVERT(NVARCHAR(2000),DELETED.[ClientInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClientInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[ClientInd] Is Null And
				DELETED.[ClientInd] Is Not Null
			) Or
			(
				INSERTED.[ClientInd] Is Not Null And
				DELETED.[ClientInd] Is Null
			) Or
			(
				INSERTED.[ClientInd] !=
				DELETED.[ClientInd]
			)
		) 
		END		
		
      If UPDATE([VendorInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'VendorInd',
      CONVERT(NVARCHAR(2000),DELETED.[VendorInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[VendorInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[VendorInd] Is Null And
				DELETED.[VendorInd] Is Not Null
			) Or
			(
				INSERTED.[VendorInd] Is Not Null And
				DELETED.[VendorInd] Is Null
			) Or
			(
				INSERTED.[VendorInd] !=
				DELETED.[VendorInd]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[ClendorProjectAssoc] ADD CONSTRAINT [ClendorProjectAssocPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [ClendorProjectAssocIDX] ON [dbo].[ClendorProjectAssoc] ([ClientID], [WBS1], [WBS2], [WBS3]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [ClendorProjectAssocWBS1WBS2WBS3ClientIDIDX] ON [dbo].[ClendorProjectAssoc] ([WBS1], [WBS2], [WBS3], [ClientID]) INCLUDE ([Address], [Role], [ClientInd]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
