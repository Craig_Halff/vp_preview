CREATE TABLE [dbo].[FW_Files]
(
[FileID] [uniqueidentifier] NOT NULL,
[FileName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContentType] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileApplicationType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDescription] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileSize] [int] NOT NULL CONSTRAINT [DF__FW_Files___FileS__4146B6B2] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_FW_Files]
      ON [dbo].[FW_Files]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'FW_Files'
    
	    if @app not in ('Clients','Vendors','Projects','MktCampaigns','Contacts','Equipments','Employees') AND @app not like 'UDIC_%'
			return
	
      If UPDATE([FileDescription])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FileID],121),'FileDescription',
      CONVERT(NVARCHAR(2000),DELETED.[FileDescription],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FileDescription],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[FileID] = DELETED.[FileID] AND 
		(
			(
				INSERTED.[FileDescription] Is Null And
				DELETED.[FileDescription] Is Not Null
			) Or
			(
				INSERTED.[FileDescription] Is Not Null And
				DELETED.[FileDescription] Is Null
			) Or
			(
				INSERTED.[FileDescription] !=
				DELETED.[FileDescription]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[FW_Files] ADD CONSTRAINT [FW_FilesPK] PRIMARY KEY CLUSTERED ([FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
