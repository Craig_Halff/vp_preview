CREATE TABLE [dbo].[KeyConvertWorkPROrg]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NewOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Timeframe] [smallint] NOT NULL CONSTRAINT [DF__KeyConver__Timef__17A582A1] DEFAULT ((1)),
[StartPeriod] [int] NOT NULL CONSTRAINT [DF__KeyConver__Start__1899A6DA] DEFAULT ((0)),
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[KeyConvertWorkPROrg] ADD CONSTRAINT [KeyConvertWorkPROrgPK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
