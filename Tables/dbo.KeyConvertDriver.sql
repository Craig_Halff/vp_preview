CREATE TABLE [dbo].[KeyConvertDriver]
(
[PKey] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KeyConvert__PKey__0D27F42E] DEFAULT (newid()),
[Entity] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TableName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ColumnName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SkipValue] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[KeyConvertDriver] ADD CONSTRAINT [KeyConvertDriverPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
