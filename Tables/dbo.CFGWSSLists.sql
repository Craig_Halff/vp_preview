CREATE TABLE [dbo].[CFGWSSLists]
(
[WebID] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ListID] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ListViewURL] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListTitle] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllIC] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGWSSLis__AllIC__65242475] DEFAULT ('Y'),
[Clients] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGWSSLis__Clien__661848AE] DEFAULT ('N'),
[Contacts] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGWSSLis__Conta__670C6CE7] DEFAULT ('N'),
[Leads] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGWSSLis__Leads__68009120] DEFAULT ('N'),
[MktCampaigns] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGWSSLis__MktCa__68F4B559] DEFAULT ('N'),
[Employees] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGWSSLis__Emplo__69E8D992] DEFAULT ('N'),
[Projects] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGWSSLis__Proje__6ADCFDCB] DEFAULT ('N'),
[Opportunities] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGWSSLis__Oppor__6BD12204] DEFAULT ('N'),
[Vendors] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGWSSLis__Vendo__6CC5463D] DEFAULT ('N'),
[TextLibrary] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGWSSLis__TextL__6DB96A76] DEFAULT ('N'),
[ICXML] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Enabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGWSSLis__Enabl__6EAD8EAF] DEFAULT ('Y'),
[Versioning] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGWSSLists] ADD CONSTRAINT [CFGWSSListsPK] PRIMARY KEY NONCLUSTERED ([WebID], [ListID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
