CREATE TABLE [dbo].[ReportWBSList]
(
[SessionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastRun] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportWBSList] ADD CONSTRAINT [ReportWBSListPK] PRIMARY KEY NONCLUSTERED ([SessionID], [WBS1], [WBS2], [WBS3]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ReportWBSListLastRunIDX] ON [dbo].[ReportWBSList] ([LastRun]) ON [PRIMARY]
GO
