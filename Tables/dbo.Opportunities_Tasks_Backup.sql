CREATE TABLE [dbo].[Opportunities_Tasks_Backup]
(
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustTaskSort] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTaskPhaseCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTaskPhaseName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTaskCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTaskName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTaskFeeDirLab] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustT__6351B5F3] DEFAULT ((0)),
[CustTaskFeeDirExp] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustT__6445DA2C] DEFAULT ((0)),
[CustTaskConsultFee] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustT__6539FE65] DEFAULT ((0)),
[CustTaskReimbAllowExp] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustT__662E229E] DEFAULT ((0)),
[CustTaskReimbAllowCons] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustT__672246D7] DEFAULT ((0)),
[CustTaskTotal] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustT__68166B10] DEFAULT ((0)),
[CustTaskFunctionalArea] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTaskExpenseFee] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustT__690A8F49] DEFAULT ((0)),
[CustTaskConsultantFee] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustT__69FEB382] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Opportunities_Tasks_Backup] ADD CONSTRAINT [Opportunities_TasksPK] PRIMARY KEY NONCLUSTERED ([OpportunityID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
