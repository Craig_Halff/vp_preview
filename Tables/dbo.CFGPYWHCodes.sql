CREATE TABLE [dbo].[CFGPYWHCodes]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultWH] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYWHCo__Defau__022A7777] DEFAULT ('N'),
[Suppress] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYWHCo__Suppr__031E9BB0] DEFAULT ('N'),
[Type] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Locale] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Method] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYWHCo__AmtPc__0412BFE9] DEFAULT ((0)),
[Exclude401k] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYWHCo__Exclu__0506E422] DEFAULT ('N'),
[ExcludeCafeteria] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYWHCo__Exclu__05FB085B] DEFAULT ('N'),
[PrintOnW2] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYWHCo__Print__06EF2C94] DEFAULT ('N'),
[WageBase] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYWHCo__WageB__07E350CD] DEFAULT ((0)),
[W2LimitReset] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYWHCo__W2Lim__08D77506] DEFAULT ('N'),
[BonusAutoZero] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYWHCo__Bonus__09CB993F] DEFAULT ('N'),
[PrintOnCheck] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYWHCo__Print__0ABFBD78] DEFAULT ('N'),
[Limit] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYWHCo__Limit__0BB3E1B1] DEFAULT ((0)),
[PayrollWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayrollWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayrollWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sequence] [int] NOT NULL CONSTRAINT [DF__CFGPYWHCo__Seque__0CA805EA] DEFAULT ((0)),
[ExcludeOtherPay1] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYWHCo__Exclu__0D9C2A23] DEFAULT ('N'),
[ExcludeOtherPay2] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYWHCo__Exclu__0E904E5C] DEFAULT ('N'),
[ExcludeOtherPay3] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYWHCo__Exclu__0F847295] DEFAULT ('N'),
[ExcludeOtherPay4] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYWHCo__Exclu__107896CE] DEFAULT ('N'),
[ExcludeOtherPay5] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYWHCo__Exclu__116CBB07] DEFAULT ('N'),
[LimitLinkCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPYWHCodes] ADD CONSTRAINT [CFGPYWHCodesPK] PRIMARY KEY CLUSTERED ([Company], [Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
