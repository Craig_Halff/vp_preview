CREATE TABLE [dbo].[RPPlannedExpenses]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExpenseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodCount] [int] NOT NULL CONSTRAINT [DF__RPPlanned__Perio__6F0E4CB9] DEFAULT ((1)),
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Perio__700270F2] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Perio__70F6952B] DEFAULT ((0)),
[PeriodRev] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Perio__71EAB964] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPPlannedExpenses] ADD CONSTRAINT [RPPlannedExpensesPK] PRIMARY KEY NONCLUSTERED ([TimePhaseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPPlannedExpensesPTEIDX] ON [dbo].[RPPlannedExpenses] ([PlanID], [TaskID], [ExpenseID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPPlannedExpensesAllIDX] ON [dbo].[RPPlannedExpenses] ([PlanID], [TimePhaseID], [TaskID], [ExpenseID], [StartDate], [EndDate], [PeriodCost], [PeriodBill], [PeriodRev]) ON [PRIMARY]
GO
