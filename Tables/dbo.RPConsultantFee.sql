CREATE TABLE [dbo].[RPConsultantFee]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__Perio__68CB637E] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPConsult__Perio__69BF87B7] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPConsultantFee] ADD CONSTRAINT [RPConsultantFeePK] PRIMARY KEY NONCLUSTERED ([TimePhaseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPConsultantFeePTIDX] ON [dbo].[RPConsultantFee] ([PlanID], [TaskID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPConsultantFeeAllIDX] ON [dbo].[RPConsultantFee] ([PlanID], [TimePhaseID], [TaskID], [StartDate], [EndDate], [PeriodCost], [PeriodBill]) ON [PRIMARY]
GO
