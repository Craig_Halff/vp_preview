CREATE TABLE [dbo].[ekDetailCCTransactionAssoc]
(
[DetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ekDetailC__Statu__14374675] DEFAULT ('N'),
[CreateDate] [datetime] NOT NULL CONSTRAINT [DF__ekDetailC__Creat__152B6AAE] DEFAULT (getutcdate()),
[PersonalAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ekDetailC__Perso__161F8EE7] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ekDetailCCTransactionAssoc] ADD CONSTRAINT [ekDetailCCTransactionAssocPK] PRIMARY KEY NONCLUSTERED ([DetailPKey], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
