CREATE TABLE [dbo].[FW_DetailProfiles]
(
[QueueID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProcessID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DetailID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__FW_DetailPr__Seq__3B8DDD5C] DEFAULT ((0)),
[ProgID] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeStarted] [datetime] NULL,
[TimeEnded] [datetime] NULL,
[ProcessStatus] [smallint] NOT NULL CONSTRAINT [DF__FW_Detail__Proce__3C820195] DEFAULT ((0)),
[AlertPending] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TerminationMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[xmlParams] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateTimeFormat] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumberFormat] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContextState] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_DetailProfiles] ADD CONSTRAINT [FW_DetailProfilesPK] PRIMARY KEY NONCLUSTERED ([QueueID], [ProcessID], [DetailID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
