CREATE TABLE [dbo].[CFGPYAccrualsData]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HasCarryoverLimit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYAccr__HasCa__2E3E03DF] DEFAULT ('N'),
[CarryoverLimit] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYAccr__Carry__2F322818] DEFAULT ((0)),
[Maximum] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYAccr__Maxim__30264C51] DEFAULT ((0)),
[PreAccrue] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYAccr__PreAc__311A708A] DEFAULT ('N'),
[PrintOnCheck] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYAccr__Print__320E94C3] DEFAULT ('N'),
[ShowOnTK] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYAccr__ShowO__3302B8FC] DEFAULT ('N'),
[ScheduleID] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckBenefitHours] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYAccr__Check__33F6DD35] DEFAULT ('N'),
[EnableApprovalWorkflow] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPYAccr__Enabl__34EB016E] DEFAULT ('N'),
[ApprovalWorkflow] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1ExcludeWhere] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1ExcludeSearch] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPYAccrualsData] ADD CONSTRAINT [CFGPYAccrualsDataPK] PRIMARY KEY CLUSTERED ([Company], [Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
