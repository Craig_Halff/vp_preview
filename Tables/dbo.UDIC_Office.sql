CREATE TABLE [dbo].[UDIC_Office]
(
[UDIC_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustOfficeNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustOfficeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustAddress] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCity] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustState] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustZip] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustMainPhone] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustOperationsManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__UDIC_Offi__CustS__2184BDED] DEFAULT ('A'),
[CustLeaseEffectiveDate] [datetime] NULL,
[CustLeaseExpirationDate] [datetime] NULL,
[CustLeaseHolder] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLeaseHolderContact] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLeaseTerm] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_Offi__CustL__2278E226] DEFAULT ((0)),
[CustLeaseMonthlyAmount] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_Offi__CustL__236D065F] DEFAULT ((0)),
[CustFullTimeEmployees] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_Offi__CustF__24612A98] DEFAULT ((0)),
[CustPartTimeEmployees] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_Offi__CustP__25554ED1] DEFAULT ((0)),
[CustContractEmployees] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_Offi__CustC__2649730A] DEFAULT ((0)),
[CustTotalEmployees] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_Offi__CustT__273D9743] DEFAULT ((0)),
[CustDateEstablished] [datetime] NULL,
[CustOfficeAbbreviation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustOpsTeamMember] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_UDIC_Office]
      ON [dbo].[UDIC_Office]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'UDIC_Office'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'UDIC_UID',CONVERT(NVARCHAR(2000),[UDIC_UID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustomCurrencyCode',CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustOfficeNumber',CONVERT(NVARCHAR(2000),[CustOfficeNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustOfficeName',CONVERT(NVARCHAR(2000),[CustOfficeName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustAddress',CONVERT(NVARCHAR(2000),[CustAddress],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCity',CONVERT(NVARCHAR(2000),[CustCity],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustState',CONVERT(NVARCHAR(2000),[CustState],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustZip',CONVERT(NVARCHAR(2000),[CustZip],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustMainPhone',CONVERT(NVARCHAR(2000),[CustMainPhone],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustOperationsManager',CONVERT(NVARCHAR(2000),DELETED.[CustOperationsManager],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustOperationsManager = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustStatus',CONVERT(NVARCHAR(2000),[CustStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustLeaseEffectiveDate',CONVERT(NVARCHAR(2000),[CustLeaseEffectiveDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustLeaseExpirationDate',CONVERT(NVARCHAR(2000),[CustLeaseExpirationDate],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustLeaseHolder',CONVERT(NVARCHAR(2000),DELETED.[CustLeaseHolder],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join VE as oldDesc   on DELETED.CustLeaseHolder = oldDesc.Vendor

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustLeaseHolderContact',CONVERT(NVARCHAR(2000),[CustLeaseHolderContact],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustLeaseTerm',CONVERT(NVARCHAR(2000),[CustLeaseTerm],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustLeaseMonthlyAmount',CONVERT(NVARCHAR(2000),[CustLeaseMonthlyAmount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustFullTimeEmployees',CONVERT(NVARCHAR(2000),[CustFullTimeEmployees],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustPartTimeEmployees',CONVERT(NVARCHAR(2000),[CustPartTimeEmployees],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustContractEmployees',CONVERT(NVARCHAR(2000),[CustContractEmployees],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustTotalEmployees',CONVERT(NVARCHAR(2000),[CustTotalEmployees],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustDateEstablished',CONVERT(NVARCHAR(2000),[CustDateEstablished],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustOfficeAbbreviation',CONVERT(NVARCHAR(2000),[CustOfficeAbbreviation],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustOpsTeamMember',CONVERT(NVARCHAR(2000),DELETED.[CustOpsTeamMember],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustOpsTeamMember = oldDesc.Employee

      
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_UDIC_Office]
      ON [dbo].[UDIC_Office]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'UDIC_Office'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'UDIC_UID',NULL,CONVERT(NVARCHAR(2000),[UDIC_UID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustomCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustOfficeNumber',NULL,CONVERT(NVARCHAR(2000),[CustOfficeNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustOfficeName',NULL,CONVERT(NVARCHAR(2000),[CustOfficeName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustAddress',NULL,CONVERT(NVARCHAR(2000),[CustAddress],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCity',NULL,CONVERT(NVARCHAR(2000),[CustCity],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustState',NULL,CONVERT(NVARCHAR(2000),[CustState],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustZip',NULL,CONVERT(NVARCHAR(2000),[CustZip],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustMainPhone',NULL,CONVERT(NVARCHAR(2000),[CustMainPhone],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustOperationsManager',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustOperationsManager],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustOperationsManager = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustStatus',NULL,CONVERT(NVARCHAR(2000),[CustStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustLeaseEffectiveDate',NULL,CONVERT(NVARCHAR(2000),[CustLeaseEffectiveDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustLeaseExpirationDate',NULL,CONVERT(NVARCHAR(2000),[CustLeaseExpirationDate],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustLeaseHolder',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustLeaseHolder],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  VE as newDesc  on INSERTED.CustLeaseHolder = newDesc.Vendor

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustLeaseHolderContact',NULL,CONVERT(NVARCHAR(2000),[CustLeaseHolderContact],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustLeaseTerm',NULL,CONVERT(NVARCHAR(2000),[CustLeaseTerm],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustLeaseMonthlyAmount',NULL,CONVERT(NVARCHAR(2000),[CustLeaseMonthlyAmount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustFullTimeEmployees',NULL,CONVERT(NVARCHAR(2000),[CustFullTimeEmployees],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPartTimeEmployees',NULL,CONVERT(NVARCHAR(2000),[CustPartTimeEmployees],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustContractEmployees',NULL,CONVERT(NVARCHAR(2000),[CustContractEmployees],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustTotalEmployees',NULL,CONVERT(NVARCHAR(2000),[CustTotalEmployees],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustDateEstablished',NULL,CONVERT(NVARCHAR(2000),[CustDateEstablished],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustOfficeAbbreviation',NULL,CONVERT(NVARCHAR(2000),[CustOfficeAbbreviation],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustOpsTeamMember',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustOpsTeamMember],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustOpsTeamMember = newDesc.Employee

     
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_UDIC_Office]
      ON [dbo].[UDIC_Office]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'UDIC_Office'
    
      If UPDATE([UDIC_UID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'UDIC_UID',
      CONVERT(NVARCHAR(2000),DELETED.[UDIC_UID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UDIC_UID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[UDIC_UID] Is Null And
				DELETED.[UDIC_UID] Is Not Null
			) Or
			(
				INSERTED.[UDIC_UID] Is Not Null And
				DELETED.[UDIC_UID] Is Null
			) Or
			(
				INSERTED.[UDIC_UID] !=
				DELETED.[UDIC_UID]
			)
		) 
		END		
		
      If UPDATE([CustomCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustomCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustomCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustomCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustomCurrencyCode] Is Null And
				DELETED.[CustomCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] Is Not Null And
				DELETED.[CustomCurrencyCode] Is Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] !=
				DELETED.[CustomCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([CustOfficeNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustOfficeNumber',
      CONVERT(NVARCHAR(2000),DELETED.[CustOfficeNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustOfficeNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustOfficeNumber] Is Null And
				DELETED.[CustOfficeNumber] Is Not Null
			) Or
			(
				INSERTED.[CustOfficeNumber] Is Not Null And
				DELETED.[CustOfficeNumber] Is Null
			) Or
			(
				INSERTED.[CustOfficeNumber] !=
				DELETED.[CustOfficeNumber]
			)
		) 
		END		
		
      If UPDATE([CustOfficeName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustOfficeName',
      CONVERT(NVARCHAR(2000),DELETED.[CustOfficeName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustOfficeName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustOfficeName] Is Null And
				DELETED.[CustOfficeName] Is Not Null
			) Or
			(
				INSERTED.[CustOfficeName] Is Not Null And
				DELETED.[CustOfficeName] Is Null
			) Or
			(
				INSERTED.[CustOfficeName] !=
				DELETED.[CustOfficeName]
			)
		) 
		END		
		
      If UPDATE([CustAddress])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustAddress',
      CONVERT(NVARCHAR(2000),DELETED.[CustAddress],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustAddress],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustAddress] Is Null And
				DELETED.[CustAddress] Is Not Null
			) Or
			(
				INSERTED.[CustAddress] Is Not Null And
				DELETED.[CustAddress] Is Null
			) Or
			(
				INSERTED.[CustAddress] !=
				DELETED.[CustAddress]
			)
		) 
		END		
		
      If UPDATE([CustCity])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCity',
      CONVERT(NVARCHAR(2000),DELETED.[CustCity],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCity],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCity] Is Null And
				DELETED.[CustCity] Is Not Null
			) Or
			(
				INSERTED.[CustCity] Is Not Null And
				DELETED.[CustCity] Is Null
			) Or
			(
				INSERTED.[CustCity] !=
				DELETED.[CustCity]
			)
		) 
		END		
		
      If UPDATE([CustState])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustState',
      CONVERT(NVARCHAR(2000),DELETED.[CustState],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustState],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustState] Is Null And
				DELETED.[CustState] Is Not Null
			) Or
			(
				INSERTED.[CustState] Is Not Null And
				DELETED.[CustState] Is Null
			) Or
			(
				INSERTED.[CustState] !=
				DELETED.[CustState]
			)
		) 
		END		
		
      If UPDATE([CustZip])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustZip',
      CONVERT(NVARCHAR(2000),DELETED.[CustZip],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustZip],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustZip] Is Null And
				DELETED.[CustZip] Is Not Null
			) Or
			(
				INSERTED.[CustZip] Is Not Null And
				DELETED.[CustZip] Is Null
			) Or
			(
				INSERTED.[CustZip] !=
				DELETED.[CustZip]
			)
		) 
		END		
		
      If UPDATE([CustMainPhone])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustMainPhone',
      CONVERT(NVARCHAR(2000),DELETED.[CustMainPhone],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustMainPhone],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustMainPhone] Is Null And
				DELETED.[CustMainPhone] Is Not Null
			) Or
			(
				INSERTED.[CustMainPhone] Is Not Null And
				DELETED.[CustMainPhone] Is Null
			) Or
			(
				INSERTED.[CustMainPhone] !=
				DELETED.[CustMainPhone]
			)
		) 
		END		
		
     If UPDATE([CustOperationsManager])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustOperationsManager',
     CONVERT(NVARCHAR(2000),DELETED.[CustOperationsManager],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustOperationsManager],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustOperationsManager] Is Null And
				DELETED.[CustOperationsManager] Is Not Null
			) Or
			(
				INSERTED.[CustOperationsManager] Is Not Null And
				DELETED.[CustOperationsManager] Is Null
			) Or
			(
				INSERTED.[CustOperationsManager] !=
				DELETED.[CustOperationsManager]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustOperationsManager = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustOperationsManager = newDesc.Employee
		END		
		
      If UPDATE([CustStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustStatus',
      CONVERT(NVARCHAR(2000),DELETED.[CustStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustStatus] Is Null And
				DELETED.[CustStatus] Is Not Null
			) Or
			(
				INSERTED.[CustStatus] Is Not Null And
				DELETED.[CustStatus] Is Null
			) Or
			(
				INSERTED.[CustStatus] !=
				DELETED.[CustStatus]
			)
		) 
		END		
		
      If UPDATE([CustLeaseEffectiveDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustLeaseEffectiveDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustLeaseEffectiveDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLeaseEffectiveDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustLeaseEffectiveDate] Is Null And
				DELETED.[CustLeaseEffectiveDate] Is Not Null
			) Or
			(
				INSERTED.[CustLeaseEffectiveDate] Is Not Null And
				DELETED.[CustLeaseEffectiveDate] Is Null
			) Or
			(
				INSERTED.[CustLeaseEffectiveDate] !=
				DELETED.[CustLeaseEffectiveDate]
			)
		) 
		END		
		
      If UPDATE([CustLeaseExpirationDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustLeaseExpirationDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustLeaseExpirationDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLeaseExpirationDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustLeaseExpirationDate] Is Null And
				DELETED.[CustLeaseExpirationDate] Is Not Null
			) Or
			(
				INSERTED.[CustLeaseExpirationDate] Is Not Null And
				DELETED.[CustLeaseExpirationDate] Is Null
			) Or
			(
				INSERTED.[CustLeaseExpirationDate] !=
				DELETED.[CustLeaseExpirationDate]
			)
		) 
		END		
		
     If UPDATE([CustLeaseHolder])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustLeaseHolder',
     CONVERT(NVARCHAR(2000),DELETED.[CustLeaseHolder],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustLeaseHolder],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustLeaseHolder] Is Null And
				DELETED.[CustLeaseHolder] Is Not Null
			) Or
			(
				INSERTED.[CustLeaseHolder] Is Not Null And
				DELETED.[CustLeaseHolder] Is Null
			) Or
			(
				INSERTED.[CustLeaseHolder] !=
				DELETED.[CustLeaseHolder]
			)
		) left join VE as oldDesc  on DELETED.CustLeaseHolder = oldDesc.Vendor  left join  VE as newDesc  on INSERTED.CustLeaseHolder = newDesc.Vendor
		END		
		
      If UPDATE([CustLeaseHolderContact])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustLeaseHolderContact',
      CONVERT(NVARCHAR(2000),DELETED.[CustLeaseHolderContact],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLeaseHolderContact],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustLeaseHolderContact] Is Null And
				DELETED.[CustLeaseHolderContact] Is Not Null
			) Or
			(
				INSERTED.[CustLeaseHolderContact] Is Not Null And
				DELETED.[CustLeaseHolderContact] Is Null
			) Or
			(
				INSERTED.[CustLeaseHolderContact] !=
				DELETED.[CustLeaseHolderContact]
			)
		) 
		END		
		
      If UPDATE([CustLeaseTerm])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustLeaseTerm',
      CONVERT(NVARCHAR(2000),DELETED.[CustLeaseTerm],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLeaseTerm],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustLeaseTerm] Is Null And
				DELETED.[CustLeaseTerm] Is Not Null
			) Or
			(
				INSERTED.[CustLeaseTerm] Is Not Null And
				DELETED.[CustLeaseTerm] Is Null
			) Or
			(
				INSERTED.[CustLeaseTerm] !=
				DELETED.[CustLeaseTerm]
			)
		) 
		END		
		
      If UPDATE([CustLeaseMonthlyAmount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustLeaseMonthlyAmount',
      CONVERT(NVARCHAR(2000),DELETED.[CustLeaseMonthlyAmount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLeaseMonthlyAmount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustLeaseMonthlyAmount] Is Null And
				DELETED.[CustLeaseMonthlyAmount] Is Not Null
			) Or
			(
				INSERTED.[CustLeaseMonthlyAmount] Is Not Null And
				DELETED.[CustLeaseMonthlyAmount] Is Null
			) Or
			(
				INSERTED.[CustLeaseMonthlyAmount] !=
				DELETED.[CustLeaseMonthlyAmount]
			)
		) 
		END		
		
      If UPDATE([CustFullTimeEmployees])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustFullTimeEmployees',
      CONVERT(NVARCHAR(2000),DELETED.[CustFullTimeEmployees],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFullTimeEmployees],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustFullTimeEmployees] Is Null And
				DELETED.[CustFullTimeEmployees] Is Not Null
			) Or
			(
				INSERTED.[CustFullTimeEmployees] Is Not Null And
				DELETED.[CustFullTimeEmployees] Is Null
			) Or
			(
				INSERTED.[CustFullTimeEmployees] !=
				DELETED.[CustFullTimeEmployees]
			)
		) 
		END		
		
      If UPDATE([CustPartTimeEmployees])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPartTimeEmployees',
      CONVERT(NVARCHAR(2000),DELETED.[CustPartTimeEmployees],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPartTimeEmployees],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustPartTimeEmployees] Is Null And
				DELETED.[CustPartTimeEmployees] Is Not Null
			) Or
			(
				INSERTED.[CustPartTimeEmployees] Is Not Null And
				DELETED.[CustPartTimeEmployees] Is Null
			) Or
			(
				INSERTED.[CustPartTimeEmployees] !=
				DELETED.[CustPartTimeEmployees]
			)
		) 
		END		
		
      If UPDATE([CustContractEmployees])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustContractEmployees',
      CONVERT(NVARCHAR(2000),DELETED.[CustContractEmployees],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustContractEmployees],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustContractEmployees] Is Null And
				DELETED.[CustContractEmployees] Is Not Null
			) Or
			(
				INSERTED.[CustContractEmployees] Is Not Null And
				DELETED.[CustContractEmployees] Is Null
			) Or
			(
				INSERTED.[CustContractEmployees] !=
				DELETED.[CustContractEmployees]
			)
		) 
		END		
		
      If UPDATE([CustTotalEmployees])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustTotalEmployees',
      CONVERT(NVARCHAR(2000),DELETED.[CustTotalEmployees],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTotalEmployees],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustTotalEmployees] Is Null And
				DELETED.[CustTotalEmployees] Is Not Null
			) Or
			(
				INSERTED.[CustTotalEmployees] Is Not Null And
				DELETED.[CustTotalEmployees] Is Null
			) Or
			(
				INSERTED.[CustTotalEmployees] !=
				DELETED.[CustTotalEmployees]
			)
		) 
		END		
		
      If UPDATE([CustDateEstablished])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustDateEstablished',
      CONVERT(NVARCHAR(2000),DELETED.[CustDateEstablished],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustDateEstablished],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustDateEstablished] Is Null And
				DELETED.[CustDateEstablished] Is Not Null
			) Or
			(
				INSERTED.[CustDateEstablished] Is Not Null And
				DELETED.[CustDateEstablished] Is Null
			) Or
			(
				INSERTED.[CustDateEstablished] !=
				DELETED.[CustDateEstablished]
			)
		) 
		END		
		
      If UPDATE([CustOfficeAbbreviation])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustOfficeAbbreviation',
      CONVERT(NVARCHAR(2000),DELETED.[CustOfficeAbbreviation],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustOfficeAbbreviation],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustOfficeAbbreviation] Is Null And
				DELETED.[CustOfficeAbbreviation] Is Not Null
			) Or
			(
				INSERTED.[CustOfficeAbbreviation] Is Not Null And
				DELETED.[CustOfficeAbbreviation] Is Null
			) Or
			(
				INSERTED.[CustOfficeAbbreviation] !=
				DELETED.[CustOfficeAbbreviation]
			)
		) 
		END		
		
     If UPDATE([CustOpsTeamMember])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustOpsTeamMember',
     CONVERT(NVARCHAR(2000),DELETED.[CustOpsTeamMember],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustOpsTeamMember],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustOpsTeamMember] Is Null And
				DELETED.[CustOpsTeamMember] Is Not Null
			) Or
			(
				INSERTED.[CustOpsTeamMember] Is Not Null And
				DELETED.[CustOpsTeamMember] Is Null
			) Or
			(
				INSERTED.[CustOpsTeamMember] !=
				DELETED.[CustOpsTeamMember]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustOpsTeamMember = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustOpsTeamMember = newDesc.Employee
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[UDIC_Office] ADD CONSTRAINT [UDIC_OfficePK] PRIMARY KEY CLUSTERED ([UDIC_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
