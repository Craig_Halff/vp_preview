CREATE TABLE [dbo].[BIEDTax]
(
[Period] [int] NOT NULL CONSTRAINT [DF__BIEDTax_N__Perio__07B86844] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__BIEDTax_N__PostS__08AC8C7D] DEFAULT ((0)),
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReverseCharge] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BIEDTax_N__Rever__09A0B0B6] DEFAULT ('N'),
[Seq] [smallint] NOT NULL CONSTRAINT [DF__BIEDTax_New__Seq__0A94D4EF] DEFAULT ((0)),
[TaxAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BIEDTax_N__TaxAm__0B88F928] DEFAULT ((0)),
[TaxCBAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BIEDTax_N__TaxCB__0C7D1D61] DEFAULT ((0)),
[CompoundOnTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BIEDTax_N__Amoun__0D71419A] DEFAULT ((0)),
[CBAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BIEDTax_N__CBAmo__0E6565D3] DEFAULT ((0)),
[TaxAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BIEDTax_N__TaxAm__0F598A0C] DEFAULT ((0)),
[TaxCBAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BIEDTax_N__TaxCB__104DAE45] DEFAULT ((0)),
[TaxAmountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BIEDTax_N__TaxAm__1141D27E] DEFAULT ((0)),
[TaxCBAmountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BIEDTax_N__TaxCB__1235F6B7] DEFAULT ((0)),
[TaxAmountSourceCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BIEDTax_N__TaxAm__132A1AF0] DEFAULT ((0)),
[NonRecoverTaxPercent] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BIEDTax_N__NonRe__141E3F29] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BIEDTax] ADD CONSTRAINT [BIEDTaxPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [PKey], [TaxCode], [ReverseCharge]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
