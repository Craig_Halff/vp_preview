CREATE TABLE [dbo].[ActuateReportServers]
(
[ReportServer] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActuateReportServers] ADD CONSTRAINT [ActuateReportServersPK] PRIMARY KEY NONCLUSTERED ([ReportServer]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
