CREATE TABLE [dbo].[SEApprovalWorkflow]
(
[Role] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Application] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Access] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEApprova__Acces__14C9E14D] DEFAULT ('A')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SEApprovalWorkflow] ADD CONSTRAINT [SEApprovalWorkflowPK] PRIMARY KEY NONCLUSTERED ([Role], [Application]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
