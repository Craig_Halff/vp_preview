CREATE TABLE [dbo].[APApprovalDocuments]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[APApprovalDocuments] ADD CONSTRAINT [APApprovalDocumentsPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
