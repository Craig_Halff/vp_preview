CREATE TABLE [dbo].[CFGLeadStatusReasonDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGLeadStat__Seq__4E20C38C] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGLeadStatusReasonDescriptions] ADD CONSTRAINT [CFGLeadStatusReasonDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
