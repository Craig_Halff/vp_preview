CREATE TABLE [dbo].[CFGCitizenshipDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGCitizens__Seq__556CDF99] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCitizenshipDescriptions] ADD CONSTRAINT [CFGCitizenshipDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
