CREATE TABLE [dbo].[CFGAutoNumMappedValues]
(
[RuleID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NavID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Item] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ValueInVision] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ValueInVisionDesc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValueforAutoNum] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAutoNumMappedValues] ADD CONSTRAINT [CFGAutoNumMappedValuesPK] PRIMARY KEY CLUSTERED ([RuleID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
