CREATE TABLE [dbo].[CCG_Email_Messages]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[SourceApplication] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Sender] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ToList] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CCList] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BCCList] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Subject] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GroupSubject] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Body] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaxDelay] [int] NULL CONSTRAINT [DF__CCG_Email__MaxDe__0A61976C] DEFAULT ([dbo].[fnCCG_Email_DefaultDelay]()),
[Priority] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_CCG_Email_Messages_CreateDateTime] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[setPriority] ON [dbo].[CCG_Email_Messages] 
AFTER INSERT   
AS 
	IF EXISTS(Select 1 from INSERTED i where CHARINDEX('- RQ', i.Subject) > 0 or CHARINDEX('- PD', i.Subject) > 0) 
		update e set e.Priority = 'H' from CCG_Email_Messages e inner join inserted i on i.Seq = e.Seq and i.CreateDateTime = e.CreateDateTime
GO
ALTER TABLE [dbo].[CCG_Email_Messages] ADD CONSTRAINT [PK_CCG_Email_Messages] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
