CREATE TABLE [dbo].[LB]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LaborCode] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Pct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LB_New__Pct__3FB373FB] DEFAULT ((0)),
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LB_New__Rate__40A79834] DEFAULT ((0)),
[BillRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LB_New__BillRate__419BBC6D] DEFAULT ((0)),
[HrsBud] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LB_New__HrsBud__428FE0A6] DEFAULT ((0)),
[AmtBud] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LB_New__AmtBud__438404DF] DEFAULT ((0)),
[BillBud] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LB_New__BillBud__44782918] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[EtcHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LB_New__EtcHrs__456C4D51] DEFAULT ((0)),
[EtcAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LB_New__EtcAmt__4660718A] DEFAULT ((0)),
[EacHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LB_New__EacHrs__475495C3] DEFAULT ((0)),
[EacAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LB_New__EacAmt__4848B9FC] DEFAULT ((0)),
[BillEtcAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LB_New__BillEtcA__493CDE35] DEFAULT ((0)),
[BillEacAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LB_New__BillEacA__4A31026E] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LB] ADD CONSTRAINT [LBPK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [LaborCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
