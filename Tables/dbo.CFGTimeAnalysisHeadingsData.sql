CREATE TABLE [dbo].[CFGTimeAnalysisHeadingsData]
(
[ReportColumn] [smallint] NOT NULL CONSTRAINT [DF__CFGTimeAn__Repor__55B597A7] DEFAULT ((0)),
[Benefit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTimeAn__Benef__56A9BBE0] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGTimeAnalysisHeadingsData] ADD CONSTRAINT [CFGTimeAnalysisHeadingsDataPK] PRIMARY KEY CLUSTERED ([ReportColumn]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CFGTimeAnalysisHeadingsAllIDX] ON [dbo].[CFGTimeAnalysisHeadingsData] ([ReportColumn], [Benefit]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
