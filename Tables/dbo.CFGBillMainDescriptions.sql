CREATE TABLE [dbo].[CFGBillMainDescriptions]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FooterMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeeLabel] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LabLabel] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConLabel] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExpLabel] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitLabel] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddOnLabel] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxLabel] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InterestLabel] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OvtIndicator] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGBillMainDescriptions] ADD CONSTRAINT [CFGBillMainDescriptionsPK] PRIMARY KEY CLUSTERED ([Company], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
