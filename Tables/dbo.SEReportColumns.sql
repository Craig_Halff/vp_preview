CREATE TABLE [dbo].[SEReportColumns]
(
[Role] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportCustom] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportFolder] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportType] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_SEReportColumns]
      ON [dbo].[SEReportColumns]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SEReportColumns'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ReportCustom],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ReportFolder],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ReportType],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Name],121),'Role',CONVERT(NVARCHAR(2000),[Role],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ReportCustom],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ReportFolder],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ReportType],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Name],121),'ReportCustom',CONVERT(NVARCHAR(2000),[ReportCustom],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ReportCustom],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ReportFolder],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ReportType],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Name],121),'ReportFolder',CONVERT(NVARCHAR(2000),[ReportFolder],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ReportCustom],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ReportFolder],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ReportType],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Name],121),'ReportType',CONVERT(NVARCHAR(2000),[ReportType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Role],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ReportCustom],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ReportFolder],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ReportType],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Name],121),'Name',CONVERT(NVARCHAR(2000),[Name],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_SEReportColumns]
      ON [dbo].[SEReportColumns]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SEReportColumns'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportCustom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportFolder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportType],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'Role',NULL,CONVERT(NVARCHAR(2000),[Role],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportCustom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportFolder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportType],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'ReportCustom',NULL,CONVERT(NVARCHAR(2000),[ReportCustom],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportCustom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportFolder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportType],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'ReportFolder',NULL,CONVERT(NVARCHAR(2000),[ReportFolder],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportCustom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportFolder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportType],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'ReportType',NULL,CONVERT(NVARCHAR(2000),[ReportType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportCustom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportFolder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportType],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'Name',NULL,CONVERT(NVARCHAR(2000),[Name],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_SEReportColumns]
      ON [dbo].[SEReportColumns]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SEReportColumns'
    
      If UPDATE([Role])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportCustom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportFolder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportType],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'Role',
      CONVERT(NVARCHAR(2000),DELETED.[Role],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Role],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[ReportCustom] = DELETED.[ReportCustom] AND INSERTED.[ReportFolder] = DELETED.[ReportFolder] AND INSERTED.[ReportType] = DELETED.[ReportType] AND INSERTED.[Name] = DELETED.[Name] AND 
		(
			(
				INSERTED.[Role] Is Null And
				DELETED.[Role] Is Not Null
			) Or
			(
				INSERTED.[Role] Is Not Null And
				DELETED.[Role] Is Null
			) Or
			(
				INSERTED.[Role] !=
				DELETED.[Role]
			)
		) 
		END		
		
      If UPDATE([ReportCustom])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportCustom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportFolder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportType],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'ReportCustom',
      CONVERT(NVARCHAR(2000),DELETED.[ReportCustom],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReportCustom],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[ReportCustom] = DELETED.[ReportCustom] AND INSERTED.[ReportFolder] = DELETED.[ReportFolder] AND INSERTED.[ReportType] = DELETED.[ReportType] AND INSERTED.[Name] = DELETED.[Name] AND 
		(
			(
				INSERTED.[ReportCustom] Is Null And
				DELETED.[ReportCustom] Is Not Null
			) Or
			(
				INSERTED.[ReportCustom] Is Not Null And
				DELETED.[ReportCustom] Is Null
			) Or
			(
				INSERTED.[ReportCustom] !=
				DELETED.[ReportCustom]
			)
		) 
		END		
		
      If UPDATE([ReportFolder])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportCustom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportFolder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportType],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'ReportFolder',
      CONVERT(NVARCHAR(2000),DELETED.[ReportFolder],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReportFolder],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[ReportCustom] = DELETED.[ReportCustom] AND INSERTED.[ReportFolder] = DELETED.[ReportFolder] AND INSERTED.[ReportType] = DELETED.[ReportType] AND INSERTED.[Name] = DELETED.[Name] AND 
		(
			(
				INSERTED.[ReportFolder] Is Null And
				DELETED.[ReportFolder] Is Not Null
			) Or
			(
				INSERTED.[ReportFolder] Is Not Null And
				DELETED.[ReportFolder] Is Null
			) Or
			(
				INSERTED.[ReportFolder] !=
				DELETED.[ReportFolder]
			)
		) 
		END		
		
      If UPDATE([ReportType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportCustom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportFolder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportType],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'ReportType',
      CONVERT(NVARCHAR(2000),DELETED.[ReportType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReportType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[ReportCustom] = DELETED.[ReportCustom] AND INSERTED.[ReportFolder] = DELETED.[ReportFolder] AND INSERTED.[ReportType] = DELETED.[ReportType] AND INSERTED.[Name] = DELETED.[Name] AND 
		(
			(
				INSERTED.[ReportType] Is Null And
				DELETED.[ReportType] Is Not Null
			) Or
			(
				INSERTED.[ReportType] Is Not Null And
				DELETED.[ReportType] Is Null
			) Or
			(
				INSERTED.[ReportType] !=
				DELETED.[ReportType]
			)
		) 
		END		
		
      If UPDATE([Name])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Role],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportCustom],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportFolder],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ReportType],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'Name',
      CONVERT(NVARCHAR(2000),DELETED.[Name],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Name],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Role] = DELETED.[Role] AND INSERTED.[ReportCustom] = DELETED.[ReportCustom] AND INSERTED.[ReportFolder] = DELETED.[ReportFolder] AND INSERTED.[ReportType] = DELETED.[ReportType] AND INSERTED.[Name] = DELETED.[Name] AND 
		(
			(
				INSERTED.[Name] Is Null And
				DELETED.[Name] Is Not Null
			) Or
			(
				INSERTED.[Name] Is Not Null And
				DELETED.[Name] Is Null
			) Or
			(
				INSERTED.[Name] !=
				DELETED.[Name]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[SEReportColumns] ADD CONSTRAINT [SEReportColumnsPK] PRIMARY KEY NONCLUSTERED ([Role], [ReportCustom], [ReportFolder], [ReportType], [Name]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
