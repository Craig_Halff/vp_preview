CREATE TABLE [dbo].[FW_UserActivity]
(
[SessionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserID] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastApp] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartTime] [datetime] NULL,
[LastAccess] [datetime] NULL,
[Message] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QueueID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppServer] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcessID] [int] NOT NULL CONSTRAINT [DF__FW_UserAc__Proce__11629966] DEFAULT ((0)),
[ThreadID] [int] NOT NULL CONSTRAINT [DF__FW_UserAc__Threa__1256BD9F] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_UserActivity] ADD CONSTRAINT [FW_UserActivityPK] PRIMARY KEY CLUSTERED ([SessionID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FW_UserActivityUserIDIDX] ON [dbo].[FW_UserActivity] ([UserID]) ON [PRIMARY]
GO
