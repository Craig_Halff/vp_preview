CREATE TABLE [dbo].[ClientsideSettings]
(
[Type] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Page] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SessionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TypeKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[XML] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClientsideSettings] ADD CONSTRAINT [ClientsideSettingsPK] PRIMARY KEY NONCLUSTERED ([Type], [UserName], [Page], [SessionID], [TypeKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
