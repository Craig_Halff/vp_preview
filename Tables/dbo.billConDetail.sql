CREATE TABLE [dbo].[billConDetail]
(
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OriginalTable] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OriginalPeriod] [int] NOT NULL CONSTRAINT [DF__billConDe__Origi__606990F9] DEFAULT ((0)),
[OriginalPostSeq] [int] NOT NULL CONSTRAINT [DF__billConDe__Origi__615DB532] DEFAULT ((0)),
[OriginalPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BillWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortKey] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortLabel] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransDate] [datetime] NULL,
[Description1] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description2] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billConDe__Amoun__6251D96B] DEFAULT ((0)),
[CostAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billConDe__CostA__6345FDA4] DEFAULT ((0)),
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL,
[Method] [smallint] NOT NULL CONSTRAINT [DF__billConDe__Metho__643A21DD] DEFAULT ((0)),
[Multiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billConDe__Multi__652E4616] DEFAULT ((0)),
[ExpenseDetail] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__billConDe__Expen__66226A4F] DEFAULT ('N'),
[ShowMult] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__billConDe__ShowM__67168E88] DEFAULT ('N'),
[DiffMult] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__billConDe__DiffM__680AB2C1] DEFAULT ('N'),
[SortKey2] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortLabel2] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShowPODetail] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__billConDe__ShowP__68FED6FA] DEFAULT ('N'),
[PONumber] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UOM] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Quantity] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billConDe__Quant__69F2FB33] DEFAULT ((0)),
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billConDet__Rate__6AE71F6C] DEFAULT ((0)),
[RateFlag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[billConDetail] ADD CONSTRAINT [billConDetailPK] PRIMARY KEY NONCLUSTERED ([Invoice], [MainWBS1], [OriginalTable], [OriginalPeriod], [OriginalPostSeq], [OriginalPKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
