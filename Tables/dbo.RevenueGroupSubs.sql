CREATE TABLE [dbo].[RevenueGroupSubs]
(
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SubWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RevenueGroupSubs] ADD CONSTRAINT [RevenueGroupSubsPK] PRIMARY KEY NONCLUSTERED ([MainWBS1], [SubWBS1]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
