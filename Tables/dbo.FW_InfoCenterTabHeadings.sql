CREATE TABLE [dbo].[FW_InfoCenterTabHeadings]
(
[InfocenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabHeading] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SysTabHeading] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_InfoCenterTabHeadings] ADD CONSTRAINT [FW_InfoCenterTabHeadingsPK] PRIMARY KEY NONCLUSTERED ([InfocenterArea], [TabID], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
