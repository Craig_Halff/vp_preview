CREATE TABLE [dbo].[CFGInvMap]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ARAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ARColumn] [smallint] NOT NULL CONSTRAINT [DF__CFGInvMap__ARCol__364939FB] DEFAULT ((0)),
[WBS1Column] [smallint] NOT NULL CONSTRAINT [DF__CFGInvMap__WBS1C__373D5E34] DEFAULT ((0)),
[RetainageAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevCategory] [smallint] NOT NULL CONSTRAINT [DF__CFGInvMap__RevCa__3831826D] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGInvMap] ADD CONSTRAINT [CFGInvMapPK] PRIMARY KEY CLUSTERED ([Company], [Account]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
