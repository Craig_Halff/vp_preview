CREATE TABLE [dbo].[BTBG]
(
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConsolidatePrint] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BTBG_New__Consol__301B5F59] DEFAULT ('N'),
[SeparateTerms] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BTBG_New__Separa__310F8392] DEFAULT ('N'),
[ConsolidatePost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BTBG_New__Consol__3203A7CB] DEFAULT ('N'),
[OverallLimit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BTBG_New__Overal__32F7CC04] DEFAULT ('N'),
[PrintAR] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrintBTD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrintInterest] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrintTax] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrintRet] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrintAdd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BTBG_New__PrintA__33EBF03D] DEFAULT ('P'),
[ConsolidateTaxPost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BTBG_New__Consol__34E01476] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTBG] ADD CONSTRAINT [BTBGPK] PRIMARY KEY NONCLUSTERED ([MainWBS1]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
