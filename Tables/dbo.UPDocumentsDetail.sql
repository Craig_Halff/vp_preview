CREATE TABLE [dbo].[UPDocumentsDetail]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL,
[DetailPKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UPDocumentsDetail] ADD CONSTRAINT [upDocumentsDetailPK] PRIMARY KEY NONCLUSTERED ([Batch], [MasterPKey], [FileID], [DetailPKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
