CREATE TABLE [dbo].[exDetailCustomFields]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[exDetailCustomFields] ADD CONSTRAINT [exDetailCustomFieldsPK] PRIMARY KEY NONCLUSTERED ([Batch], [MasterPKey], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
