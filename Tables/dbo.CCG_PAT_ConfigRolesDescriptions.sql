CREATE TABLE [dbo].[CCG_PAT_ConfigRolesDescriptions]
(
[Role] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColumnLabel] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RoleDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_ConfigRolesDescriptions] ADD CONSTRAINT [CCG_PAT_ConfigRolesDescriptionsPK] PRIMARY KEY NONCLUSTERED ([Role], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Contains descriptions for roles as used for multi-language companies', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRolesDescriptions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The language-specific label for the role', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRolesDescriptions', 'COLUMN', N'ColumnLabel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Role name/key', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRolesDescriptions', 'COLUMN', N'Role'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The language-specific description of the role', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRolesDescriptions', 'COLUMN', N'RoleDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The UI culture code (language/region) of this description', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRolesDescriptions', 'COLUMN', N'UICultureName'
GO
