CREATE TABLE [dbo].[Projects_OMRelatedDocuments]
(
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[custOMDocumentName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMDocumentURL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Projects_O__WBS1__7142449C] DEFAULT (' '),
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Projects_O__WBS2__723668D5] DEFAULT (' '),
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Projects_O__WBS3__732A8D0E] DEFAULT (' ')
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

		CREATE TRIGGER [dbo].[VisionAudit_Delete_Projects_OMRelatedDocuments]
		ON [dbo].[Projects_OMRelatedDocuments]
		FOR Delete
		NOT FOR REPLICATION
		AS BEGIN
		SET NOCOUNT ON
		DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
		SET @VisionAuditUser = dbo.FW_GetUsername()

		IF @VisionAuditUser = ''
		RETURN

		SET @source = dbo.GetVisionAuditSource()
		SET @app = (SELECT TOP 1 lastapp FROM FW_Useractivity WHERE userid = @VisionAuditUser ORDER BY lastaccess DESC)
		SET @now = dbo.GetVisionAuditTime()
		SET @table = 'Projects_OMRelatedDocuments'
	
		DECLARE @noAuditDetails varchar(1)
		SET @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		IF EXISTS(SELECT AuditKeyValuesDelete FROM FW_CFGSystem WHERE AuditKeyValuesDelete = 'Y' and @noAuditDetails = 'Y')
		BEGIN
		DECLARE @placeholder varchar(1)
		END
		ELSE
		BEGIN
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'Seq', CONVERT(NVARCHAR(2000),[Seq],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'custOMDocumentName', CONVERT(NVARCHAR(2000),[custOMDocumentName],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'custOMDocumentURL', CONVERT(NVARCHAR(2000),[custOMDocumentURL],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'WBS1', CONVERT(NVARCHAR(2000),[WBS1],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'WBS2', CONVERT(NVARCHAR(2000),[WBS2],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'WBS3', CONVERT(NVARCHAR(2000),[WBS3],121), NULL, @source, @app
		FROM DELETED
	END
		SET NOCOUNT OFF 
		END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_Projects_OMRelatedDocuments] ON [dbo].[Projects_OMRelatedDocuments]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

		CREATE TRIGGER [dbo].[VisionAudit_Insert_Projects_OMRelatedDocuments]
		ON [dbo].[Projects_OMRelatedDocuments]
		FOR Insert
		NOT FOR REPLICATION
		AS BEGIN
		SET NOCOUNT ON
		DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
		SET @VisionAuditUser = dbo.FW_GetUsername()

		IF @VisionAuditUser = ''
		RETURN

		SET @source = dbo.GetVisionAuditSource()
		SET @app = (SELECT TOP 1 lastapp FROM FW_Useractivity WHERE userid = @VisionAuditUser ORDER BY lastaccess DESC)
		SET @now = dbo.GetVisionAuditTime()
		SET @table = 'Projects_OMRelatedDocuments'
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'Seq', NULL, CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'custOMDocumentName', NULL, CONVERT(NVARCHAR(2000),[custOMDocumentName],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'custOMDocumentURL', NULL, CONVERT(NVARCHAR(2000),[custOMDocumentURL],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS1', NULL, CONVERT(NVARCHAR(2000),[WBS1],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS2', NULL, CONVERT(NVARCHAR(2000),[WBS2],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS3', NULL, CONVERT(NVARCHAR(2000),[WBS3],121), @source, @app
		FROM INSERTED
	
		SET NOCOUNT OFF 
		END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_Projects_OMRelatedDocuments] ON [dbo].[Projects_OMRelatedDocuments]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

		CREATE TRIGGER [dbo].[VisionAudit_Update_Projects_OMRelatedDocuments]
		ON [dbo].[Projects_OMRelatedDocuments]
		FOR Update
		NOT FOR REPLICATION
		AS BEGIN
		SET NOCOUNT ON
		DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
		SET @VisionAuditUser = dbo.FW_GetUsername()

		IF @VisionAuditUser = ''
		RETURN

		SET @source = dbo.GetVisionAuditSource()
		SET @app = (SELECT TOP 1 lastapp FROM FW_Useractivity WHERE userid = @VisionAuditUser ORDER BY lastaccess DESC)
		SET @now = dbo.GetVisionAuditTime()
		SET @table = 'Projects_OMRelatedDocuments'
	
		IF UPDATE([Seq])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'Seq',
		CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
		CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] IS NULL AND
				DELETED.[Seq] IS NOT NULL
			) OR
			(
				INSERTED.[Seq] IS NOT NULL AND
				DELETED.[Seq] IS NULL
			) OR
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
	
		IF UPDATE([custOMDocumentName])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'custOMDocumentName',
		CONVERT(NVARCHAR(2000),DELETED.[custOMDocumentName],121),
		CONVERT(NVARCHAR(2000),INSERTED.[custOMDocumentName],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[custOMDocumentName] IS NULL AND
				DELETED.[custOMDocumentName] IS NOT NULL
			) OR
			(
				INSERTED.[custOMDocumentName] IS NOT NULL AND
				DELETED.[custOMDocumentName] IS NULL
			) OR
			(
				INSERTED.[custOMDocumentName] !=
				DELETED.[custOMDocumentName]
			)
		) 
		END		
	
		IF UPDATE([custOMDocumentURL])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'custOMDocumentURL',
		CONVERT(NVARCHAR(2000),DELETED.[custOMDocumentURL],121),
		CONVERT(NVARCHAR(2000),INSERTED.[custOMDocumentURL],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[custOMDocumentURL] IS NULL AND
				DELETED.[custOMDocumentURL] IS NOT NULL
			) OR
			(
				INSERTED.[custOMDocumentURL] IS NOT NULL AND
				DELETED.[custOMDocumentURL] IS NULL
			) OR
			(
				INSERTED.[custOMDocumentURL] !=
				DELETED.[custOMDocumentURL]
			)
		) 
		END		
	
		IF UPDATE([WBS1])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS1',
		CONVERT(NVARCHAR(2000),DELETED.[WBS1],121),
		CONVERT(NVARCHAR(2000),INSERTED.[WBS1],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS1] IS NULL AND
				DELETED.[WBS1] IS NOT NULL
			) OR
			(
				INSERTED.[WBS1] IS NOT NULL AND
				DELETED.[WBS1] IS NULL
			) OR
			(
				INSERTED.[WBS1] !=
				DELETED.[WBS1]
			)
		) 
		END		
	
		IF UPDATE([WBS2])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS2',
		CONVERT(NVARCHAR(2000),DELETED.[WBS2],121),
		CONVERT(NVARCHAR(2000),INSERTED.[WBS2],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS2] IS NULL AND
				DELETED.[WBS2] IS NOT NULL
			) OR
			(
				INSERTED.[WBS2] IS NOT NULL AND
				DELETED.[WBS2] IS NULL
			) OR
			(
				INSERTED.[WBS2] !=
				DELETED.[WBS2]
			)
		) 
		END		
	
		IF UPDATE([WBS3])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS3',
		CONVERT(NVARCHAR(2000),DELETED.[WBS3],121),
		CONVERT(NVARCHAR(2000),INSERTED.[WBS3],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS3] IS NULL AND
				DELETED.[WBS3] IS NOT NULL
			) OR
			(
				INSERTED.[WBS3] IS NOT NULL AND
				DELETED.[WBS3] IS NULL
			) OR
			(
				INSERTED.[WBS3] !=
				DELETED.[WBS3]
			)
		) 
		END		
	
		SET NOCOUNT OFF 
		END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_Projects_OMRelatedDocuments] ON [dbo].[Projects_OMRelatedDocuments]
GO
ALTER TABLE [dbo].[Projects_OMRelatedDocuments] ADD CONSTRAINT [Projects_OMRelatedDocumentsPK] PRIMARY KEY NONCLUSTERED ([Seq], [WBS1], [WBS2], [WBS3]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
