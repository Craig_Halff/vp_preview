CREATE TABLE [dbo].[tempSE]
(
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Role] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisableLogin] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastSF254] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastSF255] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SF254Pref] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SF255Pref] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDict] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ColorScheme] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartPage] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartPageDesc] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PopupAlertsEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EMailAlertsEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MailboxAlias] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MailboxServer] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SyncCalendarWhere] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SyncContactsWhere] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SyncTasksWhere] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastCustomProposal] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoginAttempts] [smallint] NOT NULL,
[LastPasswordChange] [datetime] NULL,
[AutoHide] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultPrinter] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoRetrieve] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportServer] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Domain] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProgressiveViewing] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultFont] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultPageWidth] [decimal] (19, 4) NOT NULL,
[DefaultPageHeight] [decimal] (19, 4) NOT NULL,
[DefaultTopMargin] [decimal] (19, 4) NOT NULL,
[DefaultBottomMargin] [decimal] (19, 4) NOT NULL,
[DefaultLeftMargin] [decimal] (19, 4) NOT NULL,
[DefaultRightMargin] [decimal] (19, 4) NOT NULL,
[SupportUsername] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupportPassword] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShortDateFormat] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MediumDateFormat] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LongDateFormat] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateSeparator] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeFormat] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[decimalSymbol] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThousandsSeparator] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoLoginDefaultCompany] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AutoLoginCurrentPeriod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastSF330] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoundsLikeSearch] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WindowsUsername] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectDownload] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoRetrieveEM] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContactDownload] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientDownload] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActivityDownload] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OpportunityDownload] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultCountry] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultLanguage] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PageSize] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitOfMeasure] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KonaUsername] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KonaPassword] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForceChangePassword] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DelegateInvoiceApproval] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DelegateEmployee] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FlatDashboardStyling] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LoginAlertDateDismissed] [datetime] NULL,
[KonaAccessToken] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KonaRefreshToken] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IQAccessToken] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IQRefreshToken] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnableICDashpart] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ODBCEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ODBCUsername] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ODBCPassword] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APIRefreshToken] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APITicket] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APIRefreshTokenIssued] [datetime] NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
