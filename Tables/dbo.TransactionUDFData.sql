CREATE TABLE [dbo].[TransactionUDFData]
(
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UDFID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Required] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Transacti__Requi__26F449D7] DEFAULT ('N'),
[FieldType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LimitToList] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Transacti__Limit__27E86E10] DEFAULT ('Y'),
[Decimals] [smallint] NOT NULL CONSTRAINT [DF__Transacti__Decim__28DC9249] DEFAULT ((0)),
[MinValue] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaxValue] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultValue] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransactionUDFData] ADD CONSTRAINT [TransactionUDFDataPK] PRIMARY KEY NONCLUSTERED ([TransType], [UDFID], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
