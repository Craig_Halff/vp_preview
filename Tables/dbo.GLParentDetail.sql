CREATE TABLE [dbo].[GLParentDetail]
(
[TableNo] [smallint] NOT NULL CONSTRAINT [DF__GLParentD__Table__713394EA] DEFAULT ((0)),
[GLGroup] [smallint] NOT NULL CONSTRAINT [DF__GLParentD__GLGro__7227B923] DEFAULT ((0)),
[DetailGroupID] [smallint] NOT NULL CONSTRAINT [DF__GLParentD__Detai__731BDD5C] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GLParentDetail] ADD CONSTRAINT [GLParentDetailPK] PRIMARY KEY CLUSTERED ([TableNo], [GLGroup], [DetailGroupID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
