CREATE TABLE [dbo].[ApprovalAssignmentHierarchy]
(
[ApprovalAssignment_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ItemType_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Workflow_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RouteToID] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AssignmentOrder] [int] NOT NULL CONSTRAINT [DF__ApprovalA__Assig__2A428A72] DEFAULT ((0)),
[AssignTo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssignToDisplay] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoNotUse] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ApprovalA__DoNot__2B36AEAB] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ApprovalAssignmentHierarchy] ADD CONSTRAINT [ApprovalAssignmentHierarchyPK] PRIMARY KEY NONCLUSTERED ([ApprovalAssignment_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
