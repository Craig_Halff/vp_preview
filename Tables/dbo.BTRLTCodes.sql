CREATE TABLE [dbo].[BTRLTCodes]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__BTRLTCode__Table__06AF3572] DEFAULT ((0)),
[RateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LaborCodeMask] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BTRLTCodes__Rate__07A359AB] DEFAULT ((0)),
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EffectiveDate] [datetime] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTRLTCodes] ADD CONSTRAINT [BTRLTCodesPK] PRIMARY KEY NONCLUSTERED ([TableNo], [RateID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [BTRLTCodesLaborCodeIDX] ON [dbo].[BTRLTCodes] ([TableNo], [LaborCodeMask]) ON [PRIMARY]
GO
