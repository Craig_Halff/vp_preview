CREATE TABLE [dbo].[apppChecksE]
(
[Period] [int] NOT NULL CONSTRAINT [DF__apppCheck__Perio__14534953] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__apppCheck__PostS__15476D8C] DEFAULT ((0)),
[Seq] [int] NOT NULL CONSTRAINT [DF__apppChecksE__Seq__163B91C5] DEFAULT ((0)),
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Addenda] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__apppCheck__Adden__172FB5FE] DEFAULT ('N'),
[Remittance] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__apppCheck__Remit__1823DA37] DEFAULT ('N'),
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apppCheck__Amoun__1917FE70] DEFAULT ((0)),
[SourceBankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckRefNo] [bigint] NOT NULL CONSTRAINT [DF__apppCheck__Check__1A0C22A9] DEFAULT ((0)),
[Sequence] [int] NOT NULL CONSTRAINT [DF__apppCheck__Seque__1B0046E2] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[apppChecksE] ADD CONSTRAINT [apppChecksEPK] PRIMARY KEY NONCLUSTERED ([Period], [PostSeq], [Seq], [Vendor]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
