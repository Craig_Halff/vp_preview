CREATE TABLE [dbo].[HAI_RevenueImport]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LaborAmt] [decimal] (19, 4) NULL,
[ExpenseAmt] [decimal] (19, 4) NULL,
[ConsultantAmt] [decimal] (19, 4) NULL,
[Modified] [datetime2] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HAI_RevenueImport] ADD CONSTRAINT [PK_HAI_RevenueImport] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HAI_RevenueImport] ADD CONSTRAINT [DF_HAI_RevenueImport_WBS1WBS2WBS3] UNIQUE NONCLUSTERED ([WBS1], [WBS2], [WBS3]) ON [PRIMARY]
GO
