CREATE TABLE [dbo].[CFGPOAccounts]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxReimbAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxDirectAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxIndirectAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShipReimbAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShipDirectAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShipIndirectAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscReimbAccount1] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscDirectAccount1] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscIndirectAccount1] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscReimbAccount2] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscDirectAccount2] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscIndirectAccount2] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscReimbAccount3] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscDirectAccount3] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscIndirectAccount3] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscReimbAccount4] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscDirectAccount4] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscIndirectAccount4] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscReimbAccount5] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscDirectAccount5] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscIndirectAccount5] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxBalanceSheetAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShipBalanceSheetAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscBalanceSheetAccount1] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscBalanceSheetAccount2] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscBalanceSheetAccount3] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscBalanceSheetAccount4] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscBalanceSheetAccount5] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPOAccounts] ADD CONSTRAINT [CFGPOAccountsPK] PRIMARY KEY CLUSTERED ([Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
