CREATE TABLE [dbo].[CFGOrgCodesData]
(
[OrgLevel] [smallint] NOT NULL CONSTRAINT [DF__CFGOrgCod__OrgLe__3F9D9A0B] DEFAULT ((0)),
[Code] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CPOrg] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOrgCod__Statu__4091BE44] DEFAULT ('A')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGOrgCodesData] ADD CONSTRAINT [CFGOrgCodesDataPK] PRIMARY KEY CLUSTERED ([OrgLevel], [Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
