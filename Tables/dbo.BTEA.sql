CREATE TABLE [dbo].[BTEA]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__BTEA_New__TableN__48B202F9] DEFAULT ((0)),
[TableName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AvailableForPlanning] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BTEA_New__Availa__49A62732] DEFAULT ('N'),
[FilterOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterPrincipal] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterProjMgr] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterSupervisor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTEA] ADD CONSTRAINT [BTEAPK] PRIMARY KEY CLUSTERED ([TableNo]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
