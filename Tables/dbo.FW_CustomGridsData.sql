CREATE TABLE [dbo].[FW_CustomGridsData]
(
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TableName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TabID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GridRows] [smallint] NOT NULL CONSTRAINT [DF__FW_Custom__GridR__20D9E720] DEFAULT ((0)),
[Seq] [smallint] NOT NULL CONSTRAINT [DF__FW_CustomGr__Seq__21CE0B59] DEFAULT ((0)),
[GridType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PropertyBag] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CustomGridsData] ADD CONSTRAINT [FW_CustomGridsDataPK] PRIMARY KEY NONCLUSTERED ([InfoCenterArea], [GridID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
