CREATE TABLE [dbo].[CFGARLedgerHeadingsDescriptions]
(
[ReportColumn] [smallint] NOT NULL CONSTRAINT [DF__CFGARLedg__Repor__1B75482E] DEFAULT ((0)),
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGARLedgerHeadingsDescriptions] ADD CONSTRAINT [CFGARLedgerHeadingsDescriptionsPK] PRIMARY KEY CLUSTERED ([ReportColumn], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
