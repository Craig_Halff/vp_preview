CREATE TABLE [dbo].[CustomDashboard]
(
[DashboardID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomDashboard] ADD CONSTRAINT [CustomDashboardPK] PRIMARY KEY CLUSTERED ([DashboardID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
