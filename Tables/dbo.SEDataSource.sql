CREATE TABLE [dbo].[SEDataSource]
(
[Role] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DataSourceID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExportOnly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEDataSou__Expor__04200EE6] DEFAULT ('N'),
[CanSetParamValues] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEDataSou__CanSe__0514331F] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SEDataSource] ADD CONSTRAINT [SEDataSourcePK] PRIMARY KEY CLUSTERED ([Role], [DataSourceID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
