CREATE TABLE [dbo].[CostCTEmpls]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__CostCTEmp__Table__4264F81D] DEFAULT ((0)),
[Category] [smallint] NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CostCTEmpls] ADD CONSTRAINT [CostCTEmplsPK] PRIMARY KEY NONCLUSTERED ([TableNo], [Category], [Employee]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
