CREATE TABLE [dbo].[CCG_EI_FeePctCpl]
(
[WBS1] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL,
[FeePctCpl] [decimal] (19, 4) NULL,
[FeeToDate] [decimal] (19, 4) NULL,
[FeePctCpl1] [decimal] (19, 4) NULL,
[FeeToDate1] [decimal] (19, 4) NULL,
[FeePctCpl2] [decimal] (19, 4) NULL,
[FeeToDate2] [decimal] (19, 4) NULL,
[FeePctCpl3] [decimal] (19, 4) NULL,
[FeeToDate3] [decimal] (19, 4) NULL,
[FeePctCpl4] [decimal] (19, 4) NULL,
[FeeToDate4] [decimal] (19, 4) NULL,
[FeePctCpl5] [decimal] (19, 4) NULL,
[FeeToDate5] [decimal] (19, 4) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_FeePctCpl] ADD CONSTRAINT [PK_CCG_EI_FeePctCpl] PRIMARY KEY CLUSTERED ([WBS1], [WBS2], [WBS3], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Pct Complete Grid - Place to temporarily store user-entered values before committing changes to Vision', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_FeePctCpl', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Fee Percent Complete value entered by an EI user', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_FeePctCpl', 'COLUMN', N'FeePctCpl'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Fee to Date value entered by an EI user', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_FeePctCpl', 'COLUMN', N'FeeToDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Visions BTF.seq field (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_FeePctCpl', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The WBS1 number (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_FeePctCpl', 'COLUMN', N'WBS1'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The WBS2 number (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_FeePctCpl', 'COLUMN', N'WBS2'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The WBS3 number (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_FeePctCpl', 'COLUMN', N'WBS3'
GO
