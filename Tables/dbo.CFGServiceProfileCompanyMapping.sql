CREATE TABLE [dbo].[CFGServiceProfileCompanyMapping]
(
[ServProCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGServiceProfileCompanyMapping] ADD CONSTRAINT [CFGServiceProfileCompanyMappingPK] PRIMARY KEY CLUSTERED ([ServProCode], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
