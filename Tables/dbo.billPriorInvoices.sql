CREATE TABLE [dbo].[billPriorInvoices]
(
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecdSeq] [int] NOT NULL CONSTRAINT [DF__billPrior__RecdS__7253370A] DEFAULT ((0)),
[BillWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNumber] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL,
[PriorAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billPrior__Prior__73475B43] DEFAULT ((0)),
[OtherAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billPrior__Other__743B7F7C] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[billPriorInvoices] ADD CONSTRAINT [billPriorInvoicesPK] PRIMARY KEY NONCLUSTERED ([Invoice], [MainWBS1], [RecdSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
