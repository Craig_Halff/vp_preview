CREATE TABLE [dbo].[SyncLog]
(
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastSyncDate] [datetime] NOT NULL,
[SyncUserID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SyncSessionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TotalContactsNew] [smallint] NOT NULL CONSTRAINT [DF__SyncLog_N__Total__0058D44C] DEFAULT ((0)),
[TotalContactsUpd] [smallint] NOT NULL CONSTRAINT [DF__SyncLog_N__Total__014CF885] DEFAULT ((0)),
[TotalApptsNew] [smallint] NOT NULL CONSTRAINT [DF__SyncLog_N__Total__02411CBE] DEFAULT ((0)),
[TotalApptsUpd] [smallint] NOT NULL CONSTRAINT [DF__SyncLog_N__Total__033540F7] DEFAULT ((0)),
[TotalTasksNew] [smallint] NOT NULL CONSTRAINT [DF__SyncLog_N__Total__04296530] DEFAULT ((0)),
[TotalTasksUpd] [smallint] NOT NULL CONSTRAINT [DF__SyncLog_N__Total__051D8969] DEFAULT ((0)),
[TotalContactsDel] [smallint] NOT NULL CONSTRAINT [DF__SyncLog_N__Total__0611ADA2] DEFAULT ((0)),
[TotalActivitiesDel] [smallint] NOT NULL CONSTRAINT [DF__SyncLog_N__Total__0705D1DB] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SyncLog] ADD CONSTRAINT [SyncLogPK] PRIMARY KEY CLUSTERED ([UserName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
