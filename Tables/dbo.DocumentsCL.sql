CREATE TABLE [dbo].[DocumentsCL]
(
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ListID] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FolderURL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DocID] [int] NOT NULL CONSTRAINT [DF__Documents__DocID__1BD53AE1] DEFAULT ((0)),
[DocURL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_DocumentsCL]
      ON [dbo].[DocumentsCL]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'DocumentsCL'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ListID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FolderURL],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[DocID],121),'ClientID',CONVERT(NVARCHAR(2000),[ClientID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ListID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FolderURL],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[DocID],121),'ListID',CONVERT(NVARCHAR(2000),[ListID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ListID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FolderURL],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[DocID],121),'FolderURL',CONVERT(NVARCHAR(2000),[FolderURL],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ListID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FolderURL],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[DocID],121),'DocID',CONVERT(NVARCHAR(2000),[DocID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ListID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[FolderURL],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[DocID],121),'DocURL',CONVERT(NVARCHAR(2000),[DocURL],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_DocumentsCL]
      ON [dbo].[DocumentsCL]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'DocumentsCL'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FolderURL],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[DocID],121),'ClientID',NULL,CONVERT(NVARCHAR(2000),[ClientID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FolderURL],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[DocID],121),'ListID',NULL,CONVERT(NVARCHAR(2000),[ListID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FolderURL],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[DocID],121),'FolderURL',NULL,CONVERT(NVARCHAR(2000),[FolderURL],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FolderURL],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[DocID],121),'DocID',NULL,CONVERT(NVARCHAR(2000),[DocID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FolderURL],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[DocID],121),'DocURL',NULL,CONVERT(NVARCHAR(2000),[DocURL],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_DocumentsCL]
      ON [dbo].[DocumentsCL]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'DocumentsCL'
    
      If UPDATE([ClientID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FolderURL],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[DocID],121),'ClientID',
      CONVERT(NVARCHAR(2000),DELETED.[ClientID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClientID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[ListID] = DELETED.[ListID] AND INSERTED.[FolderURL] = DELETED.[FolderURL] AND INSERTED.[DocID] = DELETED.[DocID] AND 
		(
			(
				INSERTED.[ClientID] Is Null And
				DELETED.[ClientID] Is Not Null
			) Or
			(
				INSERTED.[ClientID] Is Not Null And
				DELETED.[ClientID] Is Null
			) Or
			(
				INSERTED.[ClientID] !=
				DELETED.[ClientID]
			)
		) 
		END		
		
      If UPDATE([ListID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FolderURL],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[DocID],121),'ListID',
      CONVERT(NVARCHAR(2000),DELETED.[ListID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ListID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[ListID] = DELETED.[ListID] AND INSERTED.[FolderURL] = DELETED.[FolderURL] AND INSERTED.[DocID] = DELETED.[DocID] AND 
		(
			(
				INSERTED.[ListID] Is Null And
				DELETED.[ListID] Is Not Null
			) Or
			(
				INSERTED.[ListID] Is Not Null And
				DELETED.[ListID] Is Null
			) Or
			(
				INSERTED.[ListID] !=
				DELETED.[ListID]
			)
		) 
		END		
		
      If UPDATE([FolderURL])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FolderURL],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[DocID],121),'FolderURL',
      CONVERT(NVARCHAR(2000),DELETED.[FolderURL],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FolderURL],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[ListID] = DELETED.[ListID] AND INSERTED.[FolderURL] = DELETED.[FolderURL] AND INSERTED.[DocID] = DELETED.[DocID] AND 
		(
			(
				INSERTED.[FolderURL] Is Null And
				DELETED.[FolderURL] Is Not Null
			) Or
			(
				INSERTED.[FolderURL] Is Not Null And
				DELETED.[FolderURL] Is Null
			) Or
			(
				INSERTED.[FolderURL] !=
				DELETED.[FolderURL]
			)
		) 
		END		
		
      If UPDATE([DocID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FolderURL],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[DocID],121),'DocID',
      CONVERT(NVARCHAR(2000),DELETED.[DocID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DocID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[ListID] = DELETED.[ListID] AND INSERTED.[FolderURL] = DELETED.[FolderURL] AND INSERTED.[DocID] = DELETED.[DocID] AND 
		(
			(
				INSERTED.[DocID] Is Null And
				DELETED.[DocID] Is Not Null
			) Or
			(
				INSERTED.[DocID] Is Not Null And
				DELETED.[DocID] Is Null
			) Or
			(
				INSERTED.[DocID] !=
				DELETED.[DocID]
			)
		) 
		END		
		
      If UPDATE([DocURL])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ListID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[FolderURL],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[DocID],121),'DocURL',
      CONVERT(NVARCHAR(2000),DELETED.[DocURL],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DocURL],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[ListID] = DELETED.[ListID] AND INSERTED.[FolderURL] = DELETED.[FolderURL] AND INSERTED.[DocID] = DELETED.[DocID] AND 
		(
			(
				INSERTED.[DocURL] Is Null And
				DELETED.[DocURL] Is Not Null
			) Or
			(
				INSERTED.[DocURL] Is Not Null And
				DELETED.[DocURL] Is Null
			) Or
			(
				INSERTED.[DocURL] !=
				DELETED.[DocURL]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[DocumentsCL] ADD CONSTRAINT [DocumentsCLPK] PRIMARY KEY NONCLUSTERED ([ClientID], [ListID], [FolderURL], [DocID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
