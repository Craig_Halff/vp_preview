CREATE TABLE [dbo].[UDIC_PracticeAreaPhoto]
(
[UDIC_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Photo] [image] NULL,
[CreateUser] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL CONSTRAINT [DF__UDIC_Prac__Creat__74FE923E] DEFAULT (getutcdate()),
[ModUser] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL CONSTRAINT [DF__UDIC_Prac__ModDa__75F2B677] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UDIC_PracticeAreaPhoto] ADD CONSTRAINT [UDIC_PracticeAreaPhotoPK] PRIMARY KEY NONCLUSTERED ([UDIC_UID]) ON [PRIMARY]
GO
