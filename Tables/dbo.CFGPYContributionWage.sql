CREATE TABLE [dbo].[CFGPYContributionWage]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodeWage] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPYContributionWage] ADD CONSTRAINT [CFGPYContributionWagePK] PRIMARY KEY CLUSTERED ([Company], [Code], [CodeWage]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
