CREATE TABLE [dbo].[CFGTKCategory]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TKGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Category] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SortSeq] [smallint] NOT NULL CONSTRAINT [DF__CFGTKCate__SortS__04D1D9F8] DEFAULT ((0)),
[Description] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LaborCode] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillCategory] [smallint] NOT NULL CONSTRAINT [DF__CFGTKCate__BillC__05C5FE31] DEFAULT ((0)),
[WBS1Query] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2Query] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3Query] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LaborCodeQuery] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillCategoryQuery] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowStartEndTime] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTKCate__Allow__06BA226A] DEFAULT ('N'),
[CategoryID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGTKCategory] ADD CONSTRAINT [CFGTKCategoryPK] PRIMARY KEY CLUSTERED ([Company], [TKGroup], [Category]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
