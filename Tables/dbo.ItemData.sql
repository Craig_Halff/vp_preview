CREATE TABLE [dbo].[ItemData]
(
[Item] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExtraNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NSN] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InternalNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Manufacturer] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkUrl] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ItemData] ADD CONSTRAINT [ItemDataPK] PRIMARY KEY NONCLUSTERED ([Item]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
