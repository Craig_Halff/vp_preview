CREATE TABLE [dbo].[SF330FirmProfile]
(
[SF330ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProjectCodeDesc] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProjectCode] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevenueIndex] [smallint] NOT NULL CONSTRAINT [DF__SF330Firm__Reven__472056F0] DEFAULT ((0)),
[ProjCodeSeq] [smallint] NOT NULL CONSTRAINT [DF__SF330Firm__ProjC__48147B29] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF330FirmProfile] ADD CONSTRAINT [SF330FirmProfilePK] PRIMARY KEY NONCLUSTERED ([SF330ID], [ID], [ProjectCodeDesc]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
