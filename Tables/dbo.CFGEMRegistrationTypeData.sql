CREATE TABLE [dbo].[CFGEMRegistrationTypeData]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGEMRegistrationTypeData] ADD CONSTRAINT [CFGEMRegistrationTypeDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
