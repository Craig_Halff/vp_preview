CREATE TABLE [dbo].[JEDocuments]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL,
[AssociateNew] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__JEDocumen__Assoc__6EEFE7D6] DEFAULT ('N'),
[Seq] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[JEDocuments] ADD CONSTRAINT [JEDocumentsPK] PRIMARY KEY CLUSTERED ([Batch], [MasterPKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
