CREATE TABLE [dbo].[CFGETInterface]
(
[PKey] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ETServer] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ETDB] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ETUser] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DownloadSSN] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGETInte__Downl__7A695A1E] DEFAULT ('N'),
[TrackingLeave] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGETInte__Track__7B5D7E57] DEFAULT ('N'),
[ETDifferentServer] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGETInte__ETDif__7C51A290] DEFAULT ('N'),
[LeavePlanOption] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGETInte__Leave__7D45C6C9] DEFAULT ('N'),
[LCSplit1] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LCSplit2] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LCSplit3] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LCSplit4] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LCSplit5] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ETLCTable1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ETLCTable2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ETLCTable3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ETLCTable4] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ETLCTable5] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastExportDate] [datetime] NULL,
[ETVersion] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGETInterface] ADD CONSTRAINT [CFGETInterfacePK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
