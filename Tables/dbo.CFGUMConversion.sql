CREATE TABLE [dbo].[CFGUMConversion]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrderUOM] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Factor] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGUMConv__Facto__3D16331B] DEFAULT ((0)),
[InventoryUOM] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGUMConversion] ADD CONSTRAINT [CFGUMConversionPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
