CREATE TABLE [dbo].[HAI_LaborAdj]
(
[Batch] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [float] NULL,
[TransDate] [datetime] NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LaborCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegHrs] [float] NULL,
[OvtHrs] [float] NULL,
[RegAmt] [float] NULL,
[OvtAmt] [float] NULL,
[BillExt] [float] NULL,
[SuppressBill] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransComment] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpecialOvtHrs] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpecialOvtAmt] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillCategory] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegAmtProjectFunctionalCurrency] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OvtAmtProjectFunctionalCurrency] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpecialOvtAmtProjectFunctionalCurrency] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkCompany] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RealizationAmount] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
