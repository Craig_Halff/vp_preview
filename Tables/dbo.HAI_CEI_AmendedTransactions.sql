CREATE TABLE [dbo].[HAI_CEI_AmendedTransactions]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegAmt] [decimal] (19, 4) NOT NULL,
[OvtAmt] [decimal] (19, 4) NOT NULL,
[BillExt] [decimal] (19, 4) NOT NULL,
[AmendedRate] [real] NULL,
[NewBillExt] [real] NULL
) ON [PRIMARY]
GO
