CREATE TABLE [dbo].[CFGOppRevenueTemplateDetail_Backup]
(
[DetailID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TemplateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TimeFrame] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeFromStart] [smallint] NOT NULL CONSTRAINT [DF__CFGOppRev__TimeF__3AD8E4EE] DEFAULT ((0)),
[PercentRevenue] [smallint] NOT NULL CONSTRAINT [DF__CFGOppRev__Perce__3BCD0927] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGOppRevenueTemplateDetail_Backup] ADD CONSTRAINT [CFGOppRevenueTemplateDetailPK] PRIMARY KEY CLUSTERED ([DetailID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
