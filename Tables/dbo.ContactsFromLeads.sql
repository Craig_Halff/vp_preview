CREATE TABLE [dbo].[ContactsFromLeads]
(
[LeadID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampaignID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_ContactsFromLeads]
      ON [dbo].[ContactsFromLeads]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ContactsFromLeads'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LeadID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'LeadID',CONVERT(NVARCHAR(2000),DELETED.[LeadID],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join Contacts as oldDesc  on DELETED.LeadID = oldDesc.ContactID

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LeadID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'ContactID',CONVERT(NVARCHAR(2000),DELETED.[ContactID],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join Contacts as oldDesc  on DELETED.ContactID = oldDesc.ContactID

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LeadID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'CampaignID',CONVERT(NVARCHAR(2000),DELETED.[CampaignID],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join MktCampaign as oldDesc  on DELETED.CampaignID = oldDesc.CampaignID

      
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_ContactsFromLeads] ON [dbo].[ContactsFromLeads]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_ContactsFromLeads]
      ON [dbo].[ContactsFromLeads]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ContactsFromLeads'
    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LeadID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'LeadID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[LeadID],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  Contacts as newDesc  on INSERTED.LeadID = newDesc.ContactID

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LeadID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'ContactID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[ContactID],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  Contacts as newDesc  on INSERTED.ContactID = newDesc.ContactID

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LeadID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'CampaignID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CampaignID],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  MktCampaign as newDesc  on INSERTED.CampaignID = newDesc.CampaignID

     
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_ContactsFromLeads] ON [dbo].[ContactsFromLeads]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_ContactsFromLeads]
      ON [dbo].[ContactsFromLeads]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ContactsFromLeads'
    
     If UPDATE([LeadID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LeadID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'LeadID',
     CONVERT(NVARCHAR(2000),DELETED.[LeadID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[LeadID],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[LeadID] = DELETED.[LeadID] AND INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[LeadID] Is Null And
				DELETED.[LeadID] Is Not Null
			) Or
			(
				INSERTED.[LeadID] Is Not Null And
				DELETED.[LeadID] Is Null
			) Or
			(
				INSERTED.[LeadID] !=
				DELETED.[LeadID]
			)
		) left join Contacts as oldDesc  on DELETED.LeadID = oldDesc.ContactID  left join  Contacts as newDesc  on INSERTED.LeadID = newDesc.ContactID
		END		
		
     If UPDATE([ContactID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LeadID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'ContactID',
     CONVERT(NVARCHAR(2000),DELETED.[ContactID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[ContactID],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[LeadID] = DELETED.[LeadID] AND INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[ContactID] Is Null And
				DELETED.[ContactID] Is Not Null
			) Or
			(
				INSERTED.[ContactID] Is Not Null And
				DELETED.[ContactID] Is Null
			) Or
			(
				INSERTED.[ContactID] !=
				DELETED.[ContactID]
			)
		) left join Contacts as oldDesc  on DELETED.ContactID = oldDesc.ContactID  left join  Contacts as newDesc  on INSERTED.ContactID = newDesc.ContactID
		END		
		
     If UPDATE([CampaignID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LeadID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'CampaignID',
     CONVERT(NVARCHAR(2000),DELETED.[CampaignID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CampaignID],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[LeadID] = DELETED.[LeadID] AND INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[CampaignID] Is Null And
				DELETED.[CampaignID] Is Not Null
			) Or
			(
				INSERTED.[CampaignID] Is Not Null And
				DELETED.[CampaignID] Is Null
			) Or
			(
				INSERTED.[CampaignID] !=
				DELETED.[CampaignID]
			)
		) left join MktCampaign as oldDesc  on DELETED.CampaignID = oldDesc.CampaignID  left join  MktCampaign as newDesc  on INSERTED.CampaignID = newDesc.CampaignID
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_ContactsFromLeads] ON [dbo].[ContactsFromLeads]
GO
ALTER TABLE [dbo].[ContactsFromLeads] ADD CONSTRAINT [ContactsFromLeadsPK] PRIMARY KEY NONCLUSTERED ([LeadID], [ContactID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
