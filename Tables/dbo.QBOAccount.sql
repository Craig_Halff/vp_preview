CREATE TABLE [dbo].[QBOAccount]
(
[QBOID] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__QBOAccoun__Activ__2FBE9A02] DEFAULT ('Y'),
[Classification] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountSubType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QBOLastUpdated] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QBOAccount] ADD CONSTRAINT [QBOAccountPK] PRIMARY KEY CLUSTERED ([QBOID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
