CREATE TABLE [dbo].[CFGCampaignAudienceDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGCampaign__Seq__4071C2B3] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCampaignAudienceDescriptions] ADD CONSTRAINT [CFGCampaignAudienceDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
