CREATE TABLE [dbo].[CustomProposalProjTeam]
(
[CustomPropID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SectionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrderOfAppearance] [smallint] NOT NULL CONSTRAINT [DF__CustomPro__Order__422FEDF3] DEFAULT ((0)),
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomProposalProjTeam] ADD CONSTRAINT [CustomProposalProjTeamPK] PRIMARY KEY NONCLUSTERED ([CustomPropID], [SectionID], [WBS1], [WBS2], [WBS3], [Employee]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
