CREATE TABLE [dbo].[FW_CFGUserSettingsLabels]
(
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ElementID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TopPos] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeftPos] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Height] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Width] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Caption1] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Caption2] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Caption3] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HelpText] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HorizontalSpacing] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerticalSpacing] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstButtonLeft] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstButtonTop] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeftAlignText] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGUserSettingsLabels] ADD CONSTRAINT [FW_CFGUserSettingsLabelsPK] PRIMARY KEY NONCLUSTERED ([InfoCenterArea], [TabID], [GridID], [ElementID], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
