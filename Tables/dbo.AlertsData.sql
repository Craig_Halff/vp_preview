CREATE TABLE [dbo].[AlertsData]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlertID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NavID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProgID] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Active] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__AlertsDat__Activ__3218B664] DEFAULT ('N'),
[Dashboard] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__AlertsDat__Dashb__330CDA9D] DEFAULT ('N'),
[Email] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__AlertsDat__Email__3400FED6] DEFAULT ('N'),
[ConfigXML] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[Config] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AlertsData] ADD CONSTRAINT [AlertsDataPK] PRIMARY KEY NONCLUSTERED ([Company], [AlertID], [NavID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
