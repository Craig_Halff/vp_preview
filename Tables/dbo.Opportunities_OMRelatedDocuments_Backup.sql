CREATE TABLE [dbo].[Opportunities_OMRelatedDocuments_Backup]
(
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[custOMDocumentName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMDocumentURL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Opportunities_OMRelatedDocuments_Backup] ADD CONSTRAINT [Opportunities_OMRelatedDocumentsPK] PRIMARY KEY NONCLUSTERED ([OpportunityID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
