CREATE TABLE [dbo].[TaxLocaleData]
(
[Locale] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[State] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TaxLocaleData] ADD CONSTRAINT [TaxLocaleDataPK] PRIMARY KEY CLUSTERED ([Locale]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
