CREATE TABLE [dbo].[ProjectCustomTabFields]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustHistoryKeep] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryContractAdmin] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryLeadEngineer] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custInvoiceGroup] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custInvoiceFolder] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custInvoiceStageFlow] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustInvoiceFolderFinals] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFinalInvPkgEmailTemplate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFinalInvPkgIncludeInvoice] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustF__246B4C39] DEFAULT ((0)),
[CustFinalInvPkgIncludeInvoiceMarkup] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFinalInvPkgIncludeTimesheetData] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustF__255F7072] DEFAULT ((0)),
[CustFinalInvPkgTimesheetTemplate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFinalInvPkgIncludeSubInvoices] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustF__265394AB] DEFAULT ((0)),
[CustFinalInvPkgSubInvoicesMinimum] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustF__2747B8E4] DEFAULT ((0)),
[CustFinalInvPkgIncludeSubInvoicesMarkup] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFinalInvPkgIncludeDirectSub] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFinalInvPkgIncludeExpReceipts] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustF__283BDD1D] DEFAULT ((0)),
[CustFinalInvPkgExpReceiptsMinimum] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustF__29300156] DEFAULT ((0)),
[CustFinalInvPkgIncludeExpReceiptsMarkup] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFinalInvPkgIncludeDirectExp] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFinalInvPkgSubExpOrder] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustInvoiceStage] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustInvoiceStageDate] [datetime] NULL,
[CustInvoiceReviewer] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCloseDate] [datetime] NULL,
[CustCloseUser] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustConstructionStartDate] [datetime] NULL CONSTRAINT [DF__ProjectCu__CustC__2A24258F] DEFAULT (NULL),
[CustSize] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustS__2B1849C8] DEFAULT ((0)),
[CustSizeUnit] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBillingType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPracticeArea] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFunctionalArea] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryMarketSegment] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryServiceLine] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryWorkType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryTeam] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBillingWeek] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLumpSumPhase] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustL__2C0C6E01] DEFAULT ('N'),
[CustConstructionFeePercent] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustC__2D00923A] DEFAULT ((0)),
[CustConstructionFeeCost] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustC__2DF4B673] DEFAULT ((0)),
[CustHistoryBSTTaxCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustSource] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCostPlusFixedFeePercent] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustC__2EE8DAAC] DEFAULT ((0)),
[CustDirector] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustManagementLeader] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustAdministrativeAssistant] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryBSTProjectManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustInvoiceSuppDocs] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFeeType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLimitatLevelAbove] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustL__2FDCFEE5] DEFAULT ('N'),
[CustTaxCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCostPlusFixedFeeAmount] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustC__30D1231E] DEFAULT ((0)),
[CustShowTaskBreakdown] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustS__31C54757] DEFAULT ('N'),
[CustReimbExpenseMultiplier] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCustomTabFields_CustReimbExpenseMultiplier_Def] DEFAULT ((1.1)),
[CustReimbConsultantMultiplier] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCustomTabFields_CustReimbConsultantMultiplier_Def] DEFAULT ((1.1)),
[CustLaborFringeMultiplier] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustL__34A1B402] DEFAULT ((0)),
[CustLaborOvhProfitMultiplier] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustL__3595D83B] DEFAULT ((0)),
[CustJTDARBalance] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustJ__3689FC74] DEFAULT ((0)),
[CustPracticeAreaLeader] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryBSTSBU] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustRevenueLabor] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustR__377E20AD] DEFAULT ((0)),
[CustRevenueConsultant] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustR__387244E6] DEFAULT ((0)),
[CustRevenueExpense] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustR__3966691F] DEFAULT ((0)),
[CustLastPayment] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustL__3A5A8D58] DEFAULT ((0)),
[CustIncludeInvTemplateFromDate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustI__3B4EB191] DEFAULT ('N'),
[CustFinalInvPkgIncludeExpenseReport] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustF__3C42D5CA] DEFAULT ((0)),
[CustFinalInvPkgIncludeUnits] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustF__3D36FA03] DEFAULT ((0)),
[CustBillingStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBillingTeamLeader] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustUnbilledTransactions] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustU__3E2B1E3C] DEFAULT ((0)),
[CustUnbilledAmountBilling] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustU__3F1F4275] DEFAULT ((0)),
[CustJTDRevenue] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustJ__401366AE] DEFAULT ((0)),
[CustYTDRevenue] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustY__41078AE7] DEFAULT ((0)),
[CustJTDBilled] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustJ__41FBAF20] DEFAULT ((0)),
[CustYTDBilled] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustY__42EFD359] DEFAULT ((0)),
[CustJTDReceived] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustJ__43E3F792] DEFAULT ((0)),
[CustYTDReceived] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustY__44D81BCB] DEFAULT ((0)),
[CustJTDWorkinProcess] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustJ__45CC4004] DEFAULT ((0)),
[CustJTDSpentBilling] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustJ__46C0643D] DEFAULT ((0)),
[CustYTDSpentBilling] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustY__47B48876] DEFAULT ((0)),
[CustJTDVariance] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustJ__48A8ACAF] DEFAULT ((0)),
[CustYTDVariance] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustY__499CD0E8] DEFAULT ((0)),
[CustJTDGoalMultiplier] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustJ__4A90F521] DEFAULT ((0)),
[CustYTDGoalMultiplier] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustY__4B85195A] DEFAULT ((0)),
[CustJTDEffectiveMultiplier] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustJ__4C793D93] DEFAULT ((0)),
[CustYTDEffectiveMultiplier] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustY__4D6D61CC] DEFAULT ((0)),
[CustJTDProfit] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustJ__4E618605] DEFAULT ((0)),
[CustYTDProfit] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustY__4F55AA3E] DEFAULT ((0)),
[CustJTDProfitPercent] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustJ__5049CE77] DEFAULT ((0)),
[CustYTDProfitPercent] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustY__513DF2B0] DEFAULT ((0)),
[CustJTDBilledTax] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustJ__523216E9] DEFAULT ((0)),
[CustLastPaymentComment] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustNearmapUsage] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEACInputLevel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTeamPracticeArea] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustNearmapLastCharge] [datetime] NULL,
[CustLastChargeDate] [datetime] NULL,
[CustReimbMileageMultiplier] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustR__53263B22] DEFAULT ((1)),
[CustUnitTableAllLevels] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustU__541A5F5B] DEFAULT ('N'),
[CustEffortatCompletion] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEfforttoComplete] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustWriteoffAdjmt] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFeeTypeRollup] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPercentComplete] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryTrackHTTProjectManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryTrackHTTDrafter] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryTrackHalffProject] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryTrackHalffDiscipline] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryTrackDisciplineProjectManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryTrackClientProjectNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryTrackClientConfigurationNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryTrackCapitalProject] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustH__550E8394] DEFAULT ('N'),
[CustHistoryTrackClientOMNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryTrackClientCostCenter] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryTrackClientProjectManager] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryTrackClientDeadline] [datetime] NULL,
[CustHistoryTrackInternalDeadline] [datetime] NULL,
[CustHistoryTrackStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustQAManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustQAReview1Goal] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustQAReview2Goal] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustQAReview3Goal] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustQAReview1Date] [datetime] NULL CONSTRAINT [DF__ProjectCu__CustQ__5602A7CD] DEFAULT (NULL),
[CustQAReview2Date] [datetime] NULL CONSTRAINT [DF__ProjectCu__CustQ__56F6CC06] DEFAULT (NULL),
[CustQAReview3Date] [datetime] NULL CONSTRAINT [DF__ProjectCu__CustQ__57EAF03F] DEFAULT (NULL),
[CustQAReview1PercentComplete] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustQAReview2PercentComplete] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustQAReview3PercentComplete] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustQAWarning] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustQAReviewRequired] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustQ__58DF1478] DEFAULT ('N'),
[CustLastWeek1SpentBilling] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustL__59D338B1] DEFAULT ((0)),
[CustLastWeek2SpentBilling] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustL__5AC75CEA] DEFAULT ((0)),
[CustClientProjectContact] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLegalInvolvement] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustL__5BBB8123] DEFAULT ('No'),
[CustQADelegate] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLocationURL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFinalPackageEmail] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFinalPackageCCEmail] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFinalPackageEmailInvRev] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustF__5CAFA55C] DEFAULT ('N'),
[CustPerDiemReimbursed] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustMasterContractType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustOriginalFeeDirLab] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustO__5DA3C995] DEFAULT ((0)),
[CustOriginalFeeDirExp] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustO__5E97EDCE] DEFAULT ((0)),
[CustOriginalConsultFee] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustO__5F8C1207] DEFAULT ((0)),
[CustOriginalReimbAllowExp] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustO__60803640] DEFAULT ((0)),
[CustOriginalReimbAllowCons] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustO__61745A79] DEFAULT ((0)),
[CustOriginalFeeTotal] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustO__62687EB2] DEFAULT ((0)),
[CustMarketingManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCorporateMarketing] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustClientReferenceNo1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustClientReferenceNo2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBillingTermDescription] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustMarketingName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustMarketable] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustM__635CA2EB] DEFAULT ('Y'),
[CustEstimatedConstructionCost] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustE__6450C724] DEFAULT ((0)),
[CustActualConstructionCost] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustA__6544EB5D] DEFAULT ((0)),
[CustBillingTermNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustShowCPPrevious] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustS__66390F96] DEFAULT ('Y'),
[CustHistorySource] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustGeoCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryProjectAccountant] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryOperationsManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryTeamLeader] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDesignOnly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustD__672D33CF] DEFAULT ('N'),
[CustInvoiceAsGenesisHalff] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustI__68215808] DEFAULT ('N'),
[CustMarketingFirstReviewComplete] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustM__69157C41] DEFAULT ('N'),
[CustMarketingFirstReviewCompleteDate] [datetime] NULL CONSTRAINT [DF__ProjectCu__CustM__6A09A07A] DEFAULT (getdate()),
[CustMarketingFirstReviewCompleteEmployee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustMarketingFinalReviewComplete] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustM__6AFDC4B3] DEFAULT ('N'),
[CustMarketingFinalReviewCompleteDate] [datetime] NULL,
[CustMarketingFinalReviewCompleteEmployee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustRoleAdd] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustMasterContractProject] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustProgressReportNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustProgressReportEditDate] [datetime] NULL,
[CustOperationsManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPAResponsibleForCollections] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustP__6BF1E8EC] DEFAULT ('N'),
[CustWorkShare] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustW__6CE60D25] DEFAULT ('N'),
[CustManualOHRateOverride] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustM__6DDA315E] DEFAULT ('N'),
[CustSelectionProcess] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCollectionsResponsibility] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustInvoiceAsHTT] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustI__6ECE5597] DEFAULT ('N'),
[CustInvoiceAsCEI] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustI__6FC279D0] DEFAULT ('N'),
[CustInvoiceTemplate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustRestartDate] [datetime] NULL,
[CustContractAttachmentsReviewed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustC__70B69E09] DEFAULT ('N'),
[CustProjectType] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustService] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustSelectedFirm] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustOffice] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustShortlisted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustS__71AAC242] DEFAULT ('N'),
[CustInterviewCoordinator] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustMarketingNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustMultiTeamPursuit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustM__729EE67B] DEFAULT ('N'),
[CustCreateProjectStructure] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustC__73930AB4] DEFAULT ('N'),
[CustTotalCompensation] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustT__74872EED] DEFAULT ((0)),
[CustCollectionsRespSetup] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustAdministrativeAssistant1] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBiller] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBillingCompany] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBillingContact] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBillingType1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustClientAdvocate] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustClientProjectContact1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustClientSolicitationNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCollectionsResponsibility1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCompanyStateCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustContractExecuted] [datetime] NULL,
[CustContractType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCopy] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustC__34042C32] DEFAULT ('N'),
[CustCorporateMarketing1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCreateNextSuffix] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustC__35EC74A4] DEFAULT ('N'),
[CustCreateProjectStructure1] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustC__37D4BD16] DEFAULT ('N'),
[CustCreateRegularProject] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustC__39BD0588] DEFAULT ('N'),
[CustDescApproach] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDescAvailability] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDescCompetitor] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDescExperience] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDescRelationship] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDescStaff] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDirector1] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEstimatedConsultantFee] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustE__3BA54DFA] DEFAULT ((0)),
[CustEstimatedExpenseFee] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustE__3D8D966C] DEFAULT ((0)),
[CustEstimatedLaborFee] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustE__3F75DEDE] DEFAULT ((0)),
[CustEvalClientNum] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustE__415E2750] DEFAULT ((0)),
[CustEvalRateApproach] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEvalRateApproachNum] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustE__43466FC2] DEFAULT ((0)),
[CustEvalRateAvailability] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEvalRateAvailabilityNum] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustE__452EB834] DEFAULT ((0)),
[CustEvalRateCompetitor] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEvalRateCompetitorNum] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustE__471700A6] DEFAULT ((0)),
[CustEvalRateExperience] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEvalRateExperienceNum] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustE__48FF4918] DEFAULT ((0)),
[CustEvalRateOverall] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustE__4AE7918A] DEFAULT ((0)),
[CustEvalRateRelationship] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEvalRateStaff] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEvalRateStaffNum] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustE__4CCFD9FC] DEFAULT ((0)),
[CustFederalInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustF__4EB8226E] DEFAULT ('N'),
[CustFeeProposalSubmitted] [datetime] NULL,
[CustFeeTypes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustInterviewed] [datetime] NULL,
[CustLaborFringeMultiplier1] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustL__50A06AE0] DEFAULT ((1.5)),
[CustLaborOvhProfitMultiplier1] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustL__5288B352] DEFAULT ((2.3)),
[CustLaborRateSchedule] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustL__5470FBC4] DEFAULT ('N'),
[CustLocationURL1] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLSSingleLineProjectBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustL__56594436] DEFAULT ('N'),
[CustManagementLeader1] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustMarketable1] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustM__58418CA8] DEFAULT ('N'),
[CustMarketingCloseOutComplete] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustM__5A29D51A] DEFAULT ('N'),
[CustMarketingCoordinator] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustMarketingInitialReviewComplete] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustM__5C121D8C] DEFAULT ('N'),
[CustMarketingLead] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustMarketingManager1] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustMarketingName1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustNearmapUsage1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMBuyingOrganization] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMContractType] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMContractVehicles] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMCRMLastUpdateDate] [datetime] NULL,
[custOMCRMLastUpdateTime] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMDescription] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMDuration] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMGovtResponseDate] [datetime] NULL,
[custOMGovtResponseTime] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMLastUpdateTime] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMNAICSCode] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMOppLastUpdateDt] [datetime] NULL,
[custOMOppManagerStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMOpportunityID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMOpportunityTitle] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMOppURL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMPartnerResponseDate] [datetime] NULL,
[custOMPartnerResponseTime] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMPlaceofPerformance] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMPrimaryContact] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMSelectedBy] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMSocioEconomicStatus] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMSolicitationDate] [datetime] NULL,
[custOMSolicitationNum] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMTeamingOppSource] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMTypeofAward] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[custOMValue] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__custO__5DFA65FE] DEFAULT ((0)),
[CustOperationsManager1] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPAResponsibleforAR] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustP__5FE2AE70] DEFAULT ('N'),
[CustPracticeArea1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPracticeAreaLeader1] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustProjectType1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustReimbConsultantMultiplier1] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustR__61CAF6E2] DEFAULT ((1.1)),
[CustReimbExpenseMultiplier1] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustR__63B33F54] DEFAULT ((1.1)),
[CustRevenueConsultFee] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustR__659B87C6] DEFAULT ((0)),
[CustRevenueFeeDirExp] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustR__6783D038] DEFAULT ((0)),
[CustRevenueFeeDirLab] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustR__696C18AA] DEFAULT ((0)),
[CustRevenueReimbAllowCons] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustR__6B54611C] DEFAULT ((0)),
[CustRevenueReimbAllowExp] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustR__6D3CA98E] DEFAULT ((0)),
[CustRFPSubmitted] [datetime] NULL,
[CustRFQAnticipated] [datetime] NULL,
[CustRFQRelease] [datetime] NULL,
[CustRFQSelected] [datetime] NULL,
[CustRFQSubmitted] [datetime] NULL,
[CustSelectionProcess1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustService1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustShortlisted1] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustS__6F24F200] DEFAULT ('N'),
[CustStrategicPursuit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ProjectCu__CustS__710D3A72] DEFAULT ('N'),
[CustStrategicPursuitsManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustSubmittalDeadline] [datetime] NULL,
[CustTaxCode1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTeamPracticeArea1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTotalLaborMultiplier] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCollectionsResponsibilitySetup] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustSetupLaborFringeMultiplier] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCustomTabFields_CustSetupLaborFringeMultiplier_Def] DEFAULT ((1.5)),
[CustSetupReimbConsultantMultiplier] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCustomTabFields_CustSetupReimbConsultantMultiplier_Def] DEFAULT ((1.1)),
[CustEstStartDate] [datetime] NULL,
[CustEvalCreator] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPursuitComponents] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPursuitPlan] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ProjectCu__CustP__4DE4C51D] DEFAULT ('N'),
[CustFinancialPotential] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFeeTypeRollupDuplicate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCurrentlyWorking] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustObstacles] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustWriteoffAdjmtDuplicate] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustW__034CB195] DEFAULT ((0)),
[CustSetupLaborOverheadProfitMultiplier] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCustomTabFields_CustSetupLaborOverheadProfitMultiplier_Def] DEFAULT ((2.3)),
[CustInterviewDate] [datetime] NULL,
[CustEffortatCompletionDuplicate] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustE__1847CE7B] DEFAULT ((0)),
[CustClientMeetingCount] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEstCompletionDate] [datetime] NULL,
[CustLocalOffice] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPursuitEffort] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustGoNoGoEmp] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTeaming] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustEfforttoCompleteDuplicate] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustE__582D4966] DEFAULT ((0)),
[CustContractAwarded] [datetime] NULL,
[CustClientMeeting] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustWinDiff] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustSetupTaxCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustSetupReimbExpenseMultiplier] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ProjectCu__CustS__76B1D086] DEFAULT ((1.1)),
[CustPriorWork] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustSOQSubmitted] [datetime] NULL,
[CustSetupNearmapUsage] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustContractSalesBalanced] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ProjectCu__CustC__5C1DD5DB] DEFAULT ('N'),
[CustPursuitComponentSOQ] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ProjectCu__CustP__14622EFE] DEFAULT ('N'),
[CustPursuitComponentProposal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ProjectCu__CustP__1C0350C6] DEFAULT ('N'),
[CustPursuitComponentInterview] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ProjectCu__CustP__23A4728E] DEFAULT ('N')
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_ProjectCustomTabFields]
      ON [dbo].[ProjectCustomTabFields]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ProjectCustomTabFields'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'WBS1',CONVERT(NVARCHAR(2000),[WBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'WBS2',CONVERT(NVARCHAR(2000),[WBS2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'WBS3',CONVERT(NVARCHAR(2000),[WBS3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryKeep',CONVERT(NVARCHAR(2000),[CustHistoryKeep],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryContractAdmin',CONVERT(NVARCHAR(2000),DELETED.[CustHistoryContractAdmin],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryContractAdmin = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryLeadEngineer',CONVERT(NVARCHAR(2000),DELETED.[CustHistoryLeadEngineer],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryLeadEngineer = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custInvoiceGroup',CONVERT(NVARCHAR(2000),[custInvoiceGroup],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custInvoiceFolder',CONVERT(NVARCHAR(2000),[custInvoiceFolder],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custInvoiceStageFlow',CONVERT(NVARCHAR(2000),[custInvoiceStageFlow],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustInvoiceFolderFinals',CONVERT(NVARCHAR(2000),[CustInvoiceFolderFinals],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalInvPkgEmailTemplate',CONVERT(NVARCHAR(2000),[CustFinalInvPkgEmailTemplate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalInvPkgIncludeInvoice',CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeInvoice],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalInvPkgIncludeInvoiceMarkup',CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeInvoiceMarkup],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalInvPkgIncludeTimesheetData',CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeTimesheetData],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalInvPkgTimesheetTemplate',CONVERT(NVARCHAR(2000),[CustFinalInvPkgTimesheetTemplate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalInvPkgIncludeSubInvoices',CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeSubInvoices],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalInvPkgSubInvoicesMinimum',CONVERT(NVARCHAR(2000),[CustFinalInvPkgSubInvoicesMinimum],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalInvPkgIncludeSubInvoicesMarkup',CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeSubInvoicesMarkup],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalInvPkgIncludeDirectSub',CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeDirectSub],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalInvPkgIncludeExpReceipts',CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeExpReceipts],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalInvPkgExpReceiptsMinimum',CONVERT(NVARCHAR(2000),[CustFinalInvPkgExpReceiptsMinimum],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalInvPkgIncludeExpReceiptsMarkup',CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeExpReceiptsMarkup],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalInvPkgIncludeDirectExp',CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeDirectExp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalInvPkgSubExpOrder',CONVERT(NVARCHAR(2000),[CustFinalInvPkgSubExpOrder],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustInvoiceStage',CONVERT(NVARCHAR(2000),[CustInvoiceStage],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustInvoiceStageDate',CONVERT(NVARCHAR(2000),[CustInvoiceStageDate],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustInvoiceReviewer',CONVERT(NVARCHAR(2000),DELETED.[CustInvoiceReviewer],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustInvoiceReviewer = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCloseDate',CONVERT(NVARCHAR(2000),[CustCloseDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCloseUser',CONVERT(NVARCHAR(2000),[CustCloseUser],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustConstructionStartDate',CONVERT(NVARCHAR(2000),[CustConstructionStartDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustSize',CONVERT(NVARCHAR(2000),[CustSize],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustSizeUnit',CONVERT(NVARCHAR(2000),[CustSizeUnit],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustBillingType',CONVERT(NVARCHAR(2000),[CustBillingType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustPracticeArea',CONVERT(NVARCHAR(2000),[CustPracticeArea],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFunctionalArea',CONVERT(NVARCHAR(2000),[CustFunctionalArea],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryId',CONVERT(NVARCHAR(2000),[CustHistoryId],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryMarketSegment',CONVERT(NVARCHAR(2000),[CustHistoryMarketSegment],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryServiceLine',CONVERT(NVARCHAR(2000),[CustHistoryServiceLine],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryWorkType',CONVERT(NVARCHAR(2000),[CustHistoryWorkType],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryTeam',CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTeam],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryTeam = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustBillingWeek',CONVERT(NVARCHAR(2000),[CustBillingWeek],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLumpSumPhase',CONVERT(NVARCHAR(2000),[CustLumpSumPhase],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustConstructionFeePercent',CONVERT(NVARCHAR(2000),[CustConstructionFeePercent],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustConstructionFeeCost',CONVERT(NVARCHAR(2000),[CustConstructionFeeCost],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryBSTTaxCode',CONVERT(NVARCHAR(2000),[CustHistoryBSTTaxCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustSource',CONVERT(NVARCHAR(2000),[CustSource],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCostPlusFixedFeePercent',CONVERT(NVARCHAR(2000),[CustCostPlusFixedFeePercent],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustDirector',CONVERT(NVARCHAR(2000),DELETED.[CustDirector],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustDirector = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustManagementLeader',CONVERT(NVARCHAR(2000),DELETED.[CustManagementLeader],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustManagementLeader = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustAdministrativeAssistant',CONVERT(NVARCHAR(2000),DELETED.[CustAdministrativeAssistant],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustAdministrativeAssistant = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryBSTProjectManager',CONVERT(NVARCHAR(2000),DELETED.[CustHistoryBSTProjectManager],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryBSTProjectManager = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustInvoiceSuppDocs',CONVERT(NVARCHAR(2000),[CustInvoiceSuppDocs],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFeeType',CONVERT(NVARCHAR(2000),[CustFeeType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLimitatLevelAbove',CONVERT(NVARCHAR(2000),[CustLimitatLevelAbove],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustTaxCode',CONVERT(NVARCHAR(2000),[CustTaxCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCostPlusFixedFeeAmount',CONVERT(NVARCHAR(2000),[CustCostPlusFixedFeeAmount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustShowTaskBreakdown',CONVERT(NVARCHAR(2000),[CustShowTaskBreakdown],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustReimbExpenseMultiplier',CONVERT(NVARCHAR(2000),[CustReimbExpenseMultiplier],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustReimbConsultantMultiplier',CONVERT(NVARCHAR(2000),[CustReimbConsultantMultiplier],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLaborFringeMultiplier',CONVERT(NVARCHAR(2000),[CustLaborFringeMultiplier],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLaborOvhProfitMultiplier',CONVERT(NVARCHAR(2000),[CustLaborOvhProfitMultiplier],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustJTDARBalance',CONVERT(NVARCHAR(2000),[CustJTDARBalance],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustPracticeAreaLeader',CONVERT(NVARCHAR(2000),DELETED.[CustPracticeAreaLeader],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustPracticeAreaLeader = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryBSTSBU',CONVERT(NVARCHAR(2000),[CustHistoryBSTSBU],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustRevenueLabor',CONVERT(NVARCHAR(2000),[CustRevenueLabor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustRevenueConsultant',CONVERT(NVARCHAR(2000),[CustRevenueConsultant],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustRevenueExpense',CONVERT(NVARCHAR(2000),[CustRevenueExpense],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLastPayment',CONVERT(NVARCHAR(2000),[CustLastPayment],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustIncludeInvTemplateFromDate',CONVERT(NVARCHAR(2000),[CustIncludeInvTemplateFromDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalInvPkgIncludeExpenseReport',CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeExpenseReport],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalInvPkgIncludeUnits',CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeUnits],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustBillingStatus',CONVERT(NVARCHAR(2000),[CustBillingStatus],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustBillingTeamLeader',CONVERT(NVARCHAR(2000),DELETED.[CustBillingTeamLeader],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustBillingTeamLeader = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustUnbilledTransactions',CONVERT(NVARCHAR(2000),[CustUnbilledTransactions],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustUnbilledAmountBilling',CONVERT(NVARCHAR(2000),[CustUnbilledAmountBilling],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustJTDRevenue',CONVERT(NVARCHAR(2000),[CustJTDRevenue],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustYTDRevenue',CONVERT(NVARCHAR(2000),[CustYTDRevenue],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustJTDBilled',CONVERT(NVARCHAR(2000),[CustJTDBilled],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustYTDBilled',CONVERT(NVARCHAR(2000),[CustYTDBilled],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustJTDReceived',CONVERT(NVARCHAR(2000),[CustJTDReceived],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustYTDReceived',CONVERT(NVARCHAR(2000),[CustYTDReceived],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustJTDWorkinProcess',CONVERT(NVARCHAR(2000),[CustJTDWorkinProcess],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustJTDSpentBilling',CONVERT(NVARCHAR(2000),[CustJTDSpentBilling],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustYTDSpentBilling',CONVERT(NVARCHAR(2000),[CustYTDSpentBilling],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustJTDVariance',CONVERT(NVARCHAR(2000),[CustJTDVariance],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustYTDVariance',CONVERT(NVARCHAR(2000),[CustYTDVariance],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustJTDGoalMultiplier',CONVERT(NVARCHAR(2000),[CustJTDGoalMultiplier],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustYTDGoalMultiplier',CONVERT(NVARCHAR(2000),[CustYTDGoalMultiplier],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustJTDEffectiveMultiplier',CONVERT(NVARCHAR(2000),[CustJTDEffectiveMultiplier],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustYTDEffectiveMultiplier',CONVERT(NVARCHAR(2000),[CustYTDEffectiveMultiplier],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustJTDProfit',CONVERT(NVARCHAR(2000),[CustJTDProfit],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustYTDProfit',CONVERT(NVARCHAR(2000),[CustYTDProfit],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustJTDProfitPercent',CONVERT(NVARCHAR(2000),[CustJTDProfitPercent],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustYTDProfitPercent',CONVERT(NVARCHAR(2000),[CustYTDProfitPercent],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustJTDBilledTax',CONVERT(NVARCHAR(2000),[CustJTDBilledTax],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLastPaymentComment',CONVERT(NVARCHAR(2000),[CustLastPaymentComment],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustNearmapUsage',CONVERT(NVARCHAR(2000),[CustNearmapUsage],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEACInputLevel',CONVERT(NVARCHAR(2000),[CustEACInputLevel],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustTeamPracticeArea',CONVERT(NVARCHAR(2000),[CustTeamPracticeArea],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustNearmapLastCharge',CONVERT(NVARCHAR(2000),[CustNearmapLastCharge],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLastChargeDate',CONVERT(NVARCHAR(2000),[CustLastChargeDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustReimbMileageMultiplier',CONVERT(NVARCHAR(2000),[CustReimbMileageMultiplier],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustUnitTableAllLevels',CONVERT(NVARCHAR(2000),[CustUnitTableAllLevels],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEffortatCompletion',CONVERT(NVARCHAR(2000),[CustEffortatCompletion],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEfforttoComplete',CONVERT(NVARCHAR(2000),[CustEfforttoComplete],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustWriteoffAdjmt',CONVERT(NVARCHAR(2000),[CustWriteoffAdjmt],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFeeTypeRollup',CONVERT(NVARCHAR(2000),[CustFeeTypeRollup],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustPercentComplete',CONVERT(NVARCHAR(2000),[CustPercentComplete],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryTrackHTTProjectManager',CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackHTTProjectManager],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryTrackHTTProjectManager = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryTrackHTTDrafter',CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackHTTDrafter],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryTrackHTTDrafter = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryTrackHalffProject',CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackHalffProject],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join PR as oldDesc  on DELETED.CustHistoryTrackHalffProject = oldDesc.WBS1

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryTrackHalffDiscipline',CONVERT(NVARCHAR(2000),[CustHistoryTrackHalffDiscipline],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryTrackDisciplineProjectManager',CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackDisciplineProjectManager],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryTrackDisciplineProjectManager = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryTrackClientProjectNumber',CONVERT(NVARCHAR(2000),[CustHistoryTrackClientProjectNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryTrackClientConfigurationNumber',CONVERT(NVARCHAR(2000),[CustHistoryTrackClientConfigurationNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryTrackCapitalProject',CONVERT(NVARCHAR(2000),[CustHistoryTrackCapitalProject],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryTrackClientOMNumber',CONVERT(NVARCHAR(2000),[CustHistoryTrackClientOMNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryTrackClientCostCenter',CONVERT(NVARCHAR(2000),[CustHistoryTrackClientCostCenter],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryTrackClientProjectManager',CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackClientProjectManager],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join Contacts as oldDesc  on DELETED.CustHistoryTrackClientProjectManager = oldDesc.ContactID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryTrackClientDeadline',CONVERT(NVARCHAR(2000),[CustHistoryTrackClientDeadline],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryTrackInternalDeadline',CONVERT(NVARCHAR(2000),[CustHistoryTrackInternalDeadline],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryTrackStatus',CONVERT(NVARCHAR(2000),[CustHistoryTrackStatus],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustQAManager',CONVERT(NVARCHAR(2000),DELETED.[CustQAManager],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustQAManager = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustQAReview1Goal',CONVERT(NVARCHAR(2000),[CustQAReview1Goal],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustQAReview2Goal',CONVERT(NVARCHAR(2000),[CustQAReview2Goal],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustQAReview3Goal',CONVERT(NVARCHAR(2000),[CustQAReview3Goal],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustQAReview1Date',CONVERT(NVARCHAR(2000),[CustQAReview1Date],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustQAReview2Date',CONVERT(NVARCHAR(2000),[CustQAReview2Date],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustQAReview3Date',CONVERT(NVARCHAR(2000),[CustQAReview3Date],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustQAReview1PercentComplete',CONVERT(NVARCHAR(2000),[CustQAReview1PercentComplete],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustQAReview2PercentComplete',CONVERT(NVARCHAR(2000),[CustQAReview2PercentComplete],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustQAReview3PercentComplete',CONVERT(NVARCHAR(2000),[CustQAReview3PercentComplete],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustQAWarning',CONVERT(NVARCHAR(2000),[CustQAWarning],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustQAReviewRequired',CONVERT(NVARCHAR(2000),[CustQAReviewRequired],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLastWeek1SpentBilling',CONVERT(NVARCHAR(2000),[CustLastWeek1SpentBilling],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLastWeek2SpentBilling',CONVERT(NVARCHAR(2000),[CustLastWeek2SpentBilling],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustClientProjectContact',CONVERT(NVARCHAR(2000),DELETED.[CustClientProjectContact],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join Contacts as oldDesc  on DELETED.CustClientProjectContact = oldDesc.ContactID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLegalInvolvement',CONVERT(NVARCHAR(2000),[CustLegalInvolvement],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustQADelegate',CONVERT(NVARCHAR(2000),DELETED.[CustQADelegate],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustQADelegate = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLocationURL','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalPackageEmail',CONVERT(NVARCHAR(2000),[CustFinalPackageEmail],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalPackageCCEmail',CONVERT(NVARCHAR(2000),[CustFinalPackageCCEmail],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinalPackageEmailInvRev',CONVERT(NVARCHAR(2000),[CustFinalPackageEmailInvRev],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustPerDiemReimbursed',CONVERT(NVARCHAR(2000),[CustPerDiemReimbursed],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMasterContractType',CONVERT(NVARCHAR(2000),[CustMasterContractType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustOriginalFeeDirLab',CONVERT(NVARCHAR(2000),[CustOriginalFeeDirLab],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustOriginalFeeDirExp',CONVERT(NVARCHAR(2000),[CustOriginalFeeDirExp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustOriginalConsultFee',CONVERT(NVARCHAR(2000),[CustOriginalConsultFee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustOriginalReimbAllowExp',CONVERT(NVARCHAR(2000),[CustOriginalReimbAllowExp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustOriginalReimbAllowCons',CONVERT(NVARCHAR(2000),[CustOriginalReimbAllowCons],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustOriginalFeeTotal',CONVERT(NVARCHAR(2000),[CustOriginalFeeTotal],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketingManager',CONVERT(NVARCHAR(2000),DELETED.[CustMarketingManager],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustMarketingManager = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCorporateMarketing',CONVERT(NVARCHAR(2000),[CustCorporateMarketing],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustClientReferenceNo1',CONVERT(NVARCHAR(2000),[CustClientReferenceNo1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustClientReferenceNo2',CONVERT(NVARCHAR(2000),[CustClientReferenceNo2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustBillingTermDescription','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketingName',CONVERT(NVARCHAR(2000),[CustMarketingName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketable',CONVERT(NVARCHAR(2000),[CustMarketable],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEstimatedConstructionCost',CONVERT(NVARCHAR(2000),[CustEstimatedConstructionCost],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustActualConstructionCost',CONVERT(NVARCHAR(2000),[CustActualConstructionCost],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustBillingTermNotes','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustShowCPPrevious',CONVERT(NVARCHAR(2000),[CustShowCPPrevious],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistorySource',CONVERT(NVARCHAR(2000),[CustHistorySource],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustGeoCode',CONVERT(NVARCHAR(2000),[CustGeoCode],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryProjectAccountant',CONVERT(NVARCHAR(2000),DELETED.[CustHistoryProjectAccountant],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryProjectAccountant = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryOperationsManager',CONVERT(NVARCHAR(2000),DELETED.[CustHistoryOperationsManager],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryOperationsManager = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustHistoryTeamLeader',CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTeamLeader],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryTeamLeader = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustDesignOnly',CONVERT(NVARCHAR(2000),[CustDesignOnly],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustInvoiceAsGenesisHalff',CONVERT(NVARCHAR(2000),[CustInvoiceAsGenesisHalff],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketingFirstReviewComplete',CONVERT(NVARCHAR(2000),[CustMarketingFirstReviewComplete],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketingFirstReviewCompleteDate',CONVERT(NVARCHAR(2000),[CustMarketingFirstReviewCompleteDate],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketingFirstReviewCompleteEmployee',CONVERT(NVARCHAR(2000),DELETED.[CustMarketingFirstReviewCompleteEmployee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustMarketingFirstReviewCompleteEmployee = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketingFinalReviewComplete',CONVERT(NVARCHAR(2000),[CustMarketingFinalReviewComplete],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketingFinalReviewCompleteDate',CONVERT(NVARCHAR(2000),[CustMarketingFinalReviewCompleteDate],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketingFinalReviewCompleteEmployee',CONVERT(NVARCHAR(2000),DELETED.[CustMarketingFinalReviewCompleteEmployee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustMarketingFinalReviewCompleteEmployee = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustRoleAdd',CONVERT(NVARCHAR(2000),[CustRoleAdd],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMasterContractProject',CONVERT(NVARCHAR(2000),DELETED.[CustMasterContractProject],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join PR as oldDesc  on DELETED.CustMasterContractProject = oldDesc.WBS1

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustProgressReportNotes','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustProgressReportEditDate',CONVERT(NVARCHAR(2000),[CustProgressReportEditDate],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustOperationsManager',CONVERT(NVARCHAR(2000),DELETED.[CustOperationsManager],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustOperationsManager = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustPAResponsibleForCollections',CONVERT(NVARCHAR(2000),[CustPAResponsibleForCollections],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustWorkShare',CONVERT(NVARCHAR(2000),[CustWorkShare],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustManualOHRateOverride',CONVERT(NVARCHAR(2000),[CustManualOHRateOverride],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustSelectionProcess',CONVERT(NVARCHAR(2000),[CustSelectionProcess],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCollectionsResponsibility',CONVERT(NVARCHAR(2000),[CustCollectionsResponsibility],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustInvoiceAsHTT',CONVERT(NVARCHAR(2000),[CustInvoiceAsHTT],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustInvoiceAsCEI',CONVERT(NVARCHAR(2000),[CustInvoiceAsCEI],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustInvoiceTemplate',CONVERT(NVARCHAR(2000),[CustInvoiceTemplate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustRestartDate',CONVERT(NVARCHAR(2000),[CustRestartDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustContractAttachmentsReviewed',CONVERT(NVARCHAR(2000),[CustContractAttachmentsReviewed],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustProjectType',CONVERT(NVARCHAR(2000),DELETED.[CustProjectType],121),NULL, IsNull(oldDesc.CustProjectTypeName, N''), NULL, @source,@app
        FROM DELETED left join UDIC_ProjectType as oldDesc  on DELETED.CustProjectType = oldDesc.UDIC_UID

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustService',CONVERT(NVARCHAR(2000),DELETED.[CustService],121),NULL, IsNull(oldDesc.CustServiceName, N''), NULL, @source,@app
        FROM DELETED left join UDIC_Service as oldDesc  on DELETED.CustService = oldDesc.UDIC_UID

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustSelectedFirm',CONVERT(NVARCHAR(2000),DELETED.[CustSelectedFirm],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join Clendor as oldDesc  on DELETED.CustSelectedFirm = oldDesc.ClientID

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustOffice',CONVERT(NVARCHAR(2000),DELETED.[CustOffice],121),NULL, IsNull(oldDesc.CustOfficeName, N''), NULL, @source,@app
        FROM DELETED left join UDIC_Office as oldDesc  on DELETED.CustOffice = oldDesc.UDIC_UID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustShortlisted',CONVERT(NVARCHAR(2000),[CustShortlisted],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustInterviewCoordinator',CONVERT(NVARCHAR(2000),DELETED.[CustInterviewCoordinator],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustInterviewCoordinator = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketingNotes','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMultiTeamPursuit',CONVERT(NVARCHAR(2000),[CustMultiTeamPursuit],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCreateProjectStructure',CONVERT(NVARCHAR(2000),[CustCreateProjectStructure],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustTotalCompensation',CONVERT(NVARCHAR(2000),[CustTotalCompensation],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCollectionsRespSetup',CONVERT(NVARCHAR(2000),[CustCollectionsRespSetup],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustAdministrativeAssistant1',CONVERT(NVARCHAR(2000),DELETED.[CustAdministrativeAssistant1],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustAdministrativeAssistant1 = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustBiller',CONVERT(NVARCHAR(2000),DELETED.[CustBiller],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustBiller = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustBillingCompany',CONVERT(NVARCHAR(2000),DELETED.[CustBillingCompany],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join Clendor as oldDesc  on DELETED.CustBillingCompany = oldDesc.ClientID

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustBillingContact',CONVERT(NVARCHAR(2000),DELETED.[CustBillingContact],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join Contacts as oldDesc  on DELETED.CustBillingContact = oldDesc.ContactID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustBillingType1',CONVERT(NVARCHAR(2000),[CustBillingType1],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustClientAdvocate',CONVERT(NVARCHAR(2000),DELETED.[CustClientAdvocate],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustClientAdvocate = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustClientProjectContact1',CONVERT(NVARCHAR(2000),DELETED.[CustClientProjectContact1],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join Contacts as oldDesc  on DELETED.CustClientProjectContact1 = oldDesc.ContactID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustClientSolicitationNumber',CONVERT(NVARCHAR(2000),[CustClientSolicitationNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCollectionsResponsibility1',CONVERT(NVARCHAR(2000),[CustCollectionsResponsibility1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCompanyStateCode',CONVERT(NVARCHAR(2000),[CustCompanyStateCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustContractExecuted',CONVERT(NVARCHAR(2000),[CustContractExecuted],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustContractType',CONVERT(NVARCHAR(2000),[CustContractType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCopy',CONVERT(NVARCHAR(2000),[CustCopy],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCorporateMarketing1',CONVERT(NVARCHAR(2000),[CustCorporateMarketing1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCreateNextSuffix',CONVERT(NVARCHAR(2000),[CustCreateNextSuffix],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCreateProjectStructure1',CONVERT(NVARCHAR(2000),[CustCreateProjectStructure1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCreateRegularProject',CONVERT(NVARCHAR(2000),[CustCreateRegularProject],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustDescApproach','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustDescAvailability','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustDescCompetitor','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustDescExperience','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustDescRelationship','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustDescStaff','[text]',NULL, @source,@app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustDirector1',CONVERT(NVARCHAR(2000),DELETED.[CustDirector1],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustDirector1 = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEstimatedConsultantFee',CONVERT(NVARCHAR(2000),[CustEstimatedConsultantFee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEstimatedExpenseFee',CONVERT(NVARCHAR(2000),[CustEstimatedExpenseFee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEstimatedLaborFee',CONVERT(NVARCHAR(2000),[CustEstimatedLaborFee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEvalClientNum',CONVERT(NVARCHAR(2000),[CustEvalClientNum],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEvalRateApproach',CONVERT(NVARCHAR(2000),[CustEvalRateApproach],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEvalRateApproachNum',CONVERT(NVARCHAR(2000),[CustEvalRateApproachNum],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEvalRateAvailability',CONVERT(NVARCHAR(2000),[CustEvalRateAvailability],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEvalRateAvailabilityNum',CONVERT(NVARCHAR(2000),[CustEvalRateAvailabilityNum],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEvalRateCompetitor',CONVERT(NVARCHAR(2000),[CustEvalRateCompetitor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEvalRateCompetitorNum',CONVERT(NVARCHAR(2000),[CustEvalRateCompetitorNum],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEvalRateExperience',CONVERT(NVARCHAR(2000),[CustEvalRateExperience],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEvalRateExperienceNum',CONVERT(NVARCHAR(2000),[CustEvalRateExperienceNum],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEvalRateOverall',CONVERT(NVARCHAR(2000),[CustEvalRateOverall],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEvalRateRelationship',CONVERT(NVARCHAR(2000),[CustEvalRateRelationship],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEvalRateStaff',CONVERT(NVARCHAR(2000),[CustEvalRateStaff],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEvalRateStaffNum',CONVERT(NVARCHAR(2000),[CustEvalRateStaffNum],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFederalInd',CONVERT(NVARCHAR(2000),[CustFederalInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFeeProposalSubmitted',CONVERT(NVARCHAR(2000),[CustFeeProposalSubmitted],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFeeTypes',CONVERT(NVARCHAR(2000),[CustFeeTypes],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustInterviewed',CONVERT(NVARCHAR(2000),[CustInterviewed],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLaborFringeMultiplier1',CONVERT(NVARCHAR(2000),[CustLaborFringeMultiplier1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLaborOvhProfitMultiplier1',CONVERT(NVARCHAR(2000),[CustLaborOvhProfitMultiplier1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLaborRateSchedule',CONVERT(NVARCHAR(2000),[CustLaborRateSchedule],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLocationURL1','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLSSingleLineProjectBill',CONVERT(NVARCHAR(2000),[CustLSSingleLineProjectBill],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustManagementLeader1',CONVERT(NVARCHAR(2000),DELETED.[CustManagementLeader1],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustManagementLeader1 = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketable1',CONVERT(NVARCHAR(2000),[CustMarketable1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketingCloseOutComplete',CONVERT(NVARCHAR(2000),[CustMarketingCloseOutComplete],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketingCoordinator',CONVERT(NVARCHAR(2000),DELETED.[CustMarketingCoordinator],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustMarketingCoordinator = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketingInitialReviewComplete',CONVERT(NVARCHAR(2000),[CustMarketingInitialReviewComplete],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketingLead',CONVERT(NVARCHAR(2000),DELETED.[CustMarketingLead],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustMarketingLead = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketingManager1',CONVERT(NVARCHAR(2000),DELETED.[CustMarketingManager1],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustMarketingManager1 = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustMarketingName1',CONVERT(NVARCHAR(2000),[CustMarketingName1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustNearmapUsage1',CONVERT(NVARCHAR(2000),[CustNearmapUsage1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMBuyingOrganization','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMContractType','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMContractVehicles','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMCRMLastUpdateDate',CONVERT(NVARCHAR(2000),[custOMCRMLastUpdateDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMCRMLastUpdateTime',CONVERT(NVARCHAR(2000),[custOMCRMLastUpdateTime],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMDescription','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMDuration',CONVERT(NVARCHAR(2000),[custOMDuration],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMGovtResponseDate',CONVERT(NVARCHAR(2000),[custOMGovtResponseDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMGovtResponseTime',CONVERT(NVARCHAR(2000),[custOMGovtResponseTime],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMLastUpdateTime',CONVERT(NVARCHAR(2000),[custOMLastUpdateTime],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMNAICSCode','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMOppLastUpdateDt',CONVERT(NVARCHAR(2000),[custOMOppLastUpdateDt],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMOppManagerStatus',CONVERT(NVARCHAR(2000),[custOMOppManagerStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMOpportunityID',CONVERT(NVARCHAR(2000),[custOMOpportunityID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMOpportunityTitle',CONVERT(NVARCHAR(2000),[custOMOpportunityTitle],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMOppURL','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMPartnerResponseDate',CONVERT(NVARCHAR(2000),[custOMPartnerResponseDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMPartnerResponseTime',CONVERT(NVARCHAR(2000),[custOMPartnerResponseTime],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMPlaceofPerformance','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMPrimaryContact','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMSelectedBy',CONVERT(NVARCHAR(2000),[custOMSelectedBy],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMSocioEconomicStatus','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMSolicitationDate',CONVERT(NVARCHAR(2000),[custOMSolicitationDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMSolicitationNum',CONVERT(NVARCHAR(2000),[custOMSolicitationNum],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMTeamingOppSource',CONVERT(NVARCHAR(2000),[custOMTeamingOppSource],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMTypeofAward',CONVERT(NVARCHAR(2000),[custOMTypeofAward],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'custOMValue',CONVERT(NVARCHAR(2000),[custOMValue],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustOperationsManager1',CONVERT(NVARCHAR(2000),DELETED.[CustOperationsManager1],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustOperationsManager1 = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustPAResponsibleforAR',CONVERT(NVARCHAR(2000),[CustPAResponsibleforAR],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustPracticeArea1',CONVERT(NVARCHAR(2000),[CustPracticeArea1],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustPracticeAreaLeader1',CONVERT(NVARCHAR(2000),DELETED.[CustPracticeAreaLeader1],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustPracticeAreaLeader1 = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustProjectType1',CONVERT(NVARCHAR(2000),DELETED.[CustProjectType1],121),NULL, IsNull(oldDesc.CustProjectTypeName, N''), NULL, @source,@app
        FROM DELETED left join UDIC_ProjectType as oldDesc  on DELETED.CustProjectType1 = oldDesc.UDIC_UID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustReimbConsultantMultiplier1',CONVERT(NVARCHAR(2000),[CustReimbConsultantMultiplier1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustReimbExpenseMultiplier1',CONVERT(NVARCHAR(2000),[CustReimbExpenseMultiplier1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustRevenueConsultFee',CONVERT(NVARCHAR(2000),[CustRevenueConsultFee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustRevenueFeeDirExp',CONVERT(NVARCHAR(2000),[CustRevenueFeeDirExp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustRevenueFeeDirLab',CONVERT(NVARCHAR(2000),[CustRevenueFeeDirLab],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustRevenueReimbAllowCons',CONVERT(NVARCHAR(2000),[CustRevenueReimbAllowCons],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustRevenueReimbAllowExp',CONVERT(NVARCHAR(2000),[CustRevenueReimbAllowExp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustRFPSubmitted',CONVERT(NVARCHAR(2000),[CustRFPSubmitted],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustRFQAnticipated',CONVERT(NVARCHAR(2000),[CustRFQAnticipated],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustRFQRelease',CONVERT(NVARCHAR(2000),[CustRFQRelease],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustRFQSelected',CONVERT(NVARCHAR(2000),[CustRFQSelected],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustRFQSubmitted',CONVERT(NVARCHAR(2000),[CustRFQSubmitted],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustSelectionProcess1',CONVERT(NVARCHAR(2000),[CustSelectionProcess1],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustService1',CONVERT(NVARCHAR(2000),DELETED.[CustService1],121),NULL, IsNull(oldDesc.CustServiceName, N''), NULL, @source,@app
        FROM DELETED left join UDIC_Service as oldDesc  on DELETED.CustService1 = oldDesc.UDIC_UID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustShortlisted1',CONVERT(NVARCHAR(2000),[CustShortlisted1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustStrategicPursuit',CONVERT(NVARCHAR(2000),[CustStrategicPursuit],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustStrategicPursuitsManager',CONVERT(NVARCHAR(2000),DELETED.[CustStrategicPursuitsManager],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustStrategicPursuitsManager = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustSubmittalDeadline',CONVERT(NVARCHAR(2000),[CustSubmittalDeadline],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustTaxCode1',CONVERT(NVARCHAR(2000),[CustTaxCode1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustTeamPracticeArea1',CONVERT(NVARCHAR(2000),[CustTeamPracticeArea1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustTotalLaborMultiplier',CONVERT(NVARCHAR(2000),[CustTotalLaborMultiplier],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCollectionsResponsibilitySetup',CONVERT(NVARCHAR(2000),[CustCollectionsResponsibilitySetup],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustSetupLaborFringeMultiplier',CONVERT(NVARCHAR(2000),[CustSetupLaborFringeMultiplier],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustSetupReimbConsultantMultiplier',CONVERT(NVARCHAR(2000),[CustSetupReimbConsultantMultiplier],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEstStartDate',CONVERT(NVARCHAR(2000),[CustEstStartDate],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEvalCreator',CONVERT(NVARCHAR(2000),DELETED.[CustEvalCreator],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEvalCreator = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustPursuitComponents',CONVERT(NVARCHAR(2000),[CustPursuitComponents],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustPursuitPlan',CONVERT(NVARCHAR(2000),[CustPursuitPlan],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFinancialPotential','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustFeeTypeRollupDuplicate',CONVERT(NVARCHAR(2000),[CustFeeTypeRollupDuplicate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustCurrentlyWorking',CONVERT(NVARCHAR(2000),[CustCurrentlyWorking],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustObstacles','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustWriteoffAdjmtDuplicate',CONVERT(NVARCHAR(2000),[CustWriteoffAdjmtDuplicate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustSetupLaborOverheadProfitMultiplier',CONVERT(NVARCHAR(2000),[CustSetupLaborOverheadProfitMultiplier],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustInterviewDate',CONVERT(NVARCHAR(2000),[CustInterviewDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEffortatCompletionDuplicate',CONVERT(NVARCHAR(2000),[CustEffortatCompletionDuplicate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustClientMeetingCount',CONVERT(NVARCHAR(2000),[CustClientMeetingCount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEstCompletionDate',CONVERT(NVARCHAR(2000),[CustEstCompletionDate],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustLocalOffice',CONVERT(NVARCHAR(2000),DELETED.[CustLocalOffice],121),NULL, IsNull(oldDesc.CustOfficeName, N''), NULL, @source,@app
        FROM DELETED left join UDIC_Office as oldDesc  on DELETED.CustLocalOffice = oldDesc.UDIC_UID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustPursuitEffort',CONVERT(NVARCHAR(2000),[CustPursuitEffort],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustGoNoGoEmp','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustTeaming','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustEfforttoCompleteDuplicate',CONVERT(NVARCHAR(2000),[CustEfforttoCompleteDuplicate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustContractAwarded',CONVERT(NVARCHAR(2000),[CustContractAwarded],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustClientMeeting',CONVERT(NVARCHAR(2000),[CustClientMeeting],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustWinDiff','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustSetupTaxCode',CONVERT(NVARCHAR(2000),[CustSetupTaxCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustSetupReimbExpenseMultiplier',CONVERT(NVARCHAR(2000),[CustSetupReimbExpenseMultiplier],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustPriorWork',CONVERT(NVARCHAR(2000),[CustPriorWork],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustSOQSubmitted',CONVERT(NVARCHAR(2000),[CustSOQSubmitted],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustSetupNearmapUsage',CONVERT(NVARCHAR(2000),[CustSetupNearmapUsage],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustContractSalesBalanced',CONVERT(NVARCHAR(2000),[CustContractSalesBalanced],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustPursuitComponentSOQ',CONVERT(NVARCHAR(2000),[CustPursuitComponentSOQ],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustPursuitComponentProposal',CONVERT(NVARCHAR(2000),[CustPursuitComponentProposal],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121),'CustPursuitComponentInterview',CONVERT(NVARCHAR(2000),[CustPursuitComponentInterview],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_ProjectCustomTabFields]
      ON [dbo].[ProjectCustomTabFields]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ProjectCustomTabFields'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'WBS1',NULL,CONVERT(NVARCHAR(2000),[WBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'WBS2',NULL,CONVERT(NVARCHAR(2000),[WBS2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'WBS3',NULL,CONVERT(NVARCHAR(2000),[WBS3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryKeep',NULL,CONVERT(NVARCHAR(2000),[CustHistoryKeep],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryContractAdmin',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryContractAdmin],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryContractAdmin = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryLeadEngineer',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryLeadEngineer],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryLeadEngineer = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custInvoiceGroup',NULL,CONVERT(NVARCHAR(2000),[custInvoiceGroup],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custInvoiceFolder',NULL,CONVERT(NVARCHAR(2000),[custInvoiceFolder],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custInvoiceStageFlow',NULL,CONVERT(NVARCHAR(2000),[custInvoiceStageFlow],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceFolderFinals',NULL,CONVERT(NVARCHAR(2000),[CustInvoiceFolderFinals],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgEmailTemplate',NULL,CONVERT(NVARCHAR(2000),[CustFinalInvPkgEmailTemplate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeInvoice',NULL,CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeInvoice],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeInvoiceMarkup',NULL,CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeInvoiceMarkup],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeTimesheetData',NULL,CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeTimesheetData],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgTimesheetTemplate',NULL,CONVERT(NVARCHAR(2000),[CustFinalInvPkgTimesheetTemplate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeSubInvoices',NULL,CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeSubInvoices],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgSubInvoicesMinimum',NULL,CONVERT(NVARCHAR(2000),[CustFinalInvPkgSubInvoicesMinimum],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeSubInvoicesMarkup',NULL,CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeSubInvoicesMarkup],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeDirectSub',NULL,CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeDirectSub],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeExpReceipts',NULL,CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeExpReceipts],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgExpReceiptsMinimum',NULL,CONVERT(NVARCHAR(2000),[CustFinalInvPkgExpReceiptsMinimum],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeExpReceiptsMarkup',NULL,CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeExpReceiptsMarkup],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeDirectExp',NULL,CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeDirectExp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgSubExpOrder',NULL,CONVERT(NVARCHAR(2000),[CustFinalInvPkgSubExpOrder],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceStage',NULL,CONVERT(NVARCHAR(2000),[CustInvoiceStage],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceStageDate',NULL,CONVERT(NVARCHAR(2000),[CustInvoiceStageDate],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceReviewer',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustInvoiceReviewer],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustInvoiceReviewer = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCloseDate',NULL,CONVERT(NVARCHAR(2000),[CustCloseDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCloseUser',NULL,CONVERT(NVARCHAR(2000),[CustCloseUser],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustConstructionStartDate',NULL,CONVERT(NVARCHAR(2000),[CustConstructionStartDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSize',NULL,CONVERT(NVARCHAR(2000),[CustSize],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSizeUnit',NULL,CONVERT(NVARCHAR(2000),[CustSizeUnit],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingType',NULL,CONVERT(NVARCHAR(2000),[CustBillingType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPracticeArea',NULL,CONVERT(NVARCHAR(2000),[CustPracticeArea],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFunctionalArea',NULL,CONVERT(NVARCHAR(2000),[CustFunctionalArea],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryId',NULL,CONVERT(NVARCHAR(2000),[CustHistoryId],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryMarketSegment',NULL,CONVERT(NVARCHAR(2000),[CustHistoryMarketSegment],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryServiceLine',NULL,CONVERT(NVARCHAR(2000),[CustHistoryServiceLine],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryWorkType',NULL,CONVERT(NVARCHAR(2000),[CustHistoryWorkType],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTeam',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTeam],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryTeam = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingWeek',NULL,CONVERT(NVARCHAR(2000),[CustBillingWeek],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLumpSumPhase',NULL,CONVERT(NVARCHAR(2000),[CustLumpSumPhase],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustConstructionFeePercent',NULL,CONVERT(NVARCHAR(2000),[CustConstructionFeePercent],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustConstructionFeeCost',NULL,CONVERT(NVARCHAR(2000),[CustConstructionFeeCost],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryBSTTaxCode',NULL,CONVERT(NVARCHAR(2000),[CustHistoryBSTTaxCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSource',NULL,CONVERT(NVARCHAR(2000),[CustSource],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCostPlusFixedFeePercent',NULL,CONVERT(NVARCHAR(2000),[CustCostPlusFixedFeePercent],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDirector',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustDirector],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustDirector = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustManagementLeader',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustManagementLeader],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustManagementLeader = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustAdministrativeAssistant',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustAdministrativeAssistant],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustAdministrativeAssistant = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryBSTProjectManager',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryBSTProjectManager],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryBSTProjectManager = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceSuppDocs',NULL,CONVERT(NVARCHAR(2000),[CustInvoiceSuppDocs],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFeeType',NULL,CONVERT(NVARCHAR(2000),[CustFeeType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLimitatLevelAbove',NULL,CONVERT(NVARCHAR(2000),[CustLimitatLevelAbove],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustTaxCode',NULL,CONVERT(NVARCHAR(2000),[CustTaxCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCostPlusFixedFeeAmount',NULL,CONVERT(NVARCHAR(2000),[CustCostPlusFixedFeeAmount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustShowTaskBreakdown',NULL,CONVERT(NVARCHAR(2000),[CustShowTaskBreakdown],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustReimbExpenseMultiplier',NULL,CONVERT(NVARCHAR(2000),[CustReimbExpenseMultiplier],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustReimbConsultantMultiplier',NULL,CONVERT(NVARCHAR(2000),[CustReimbConsultantMultiplier],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLaborFringeMultiplier',NULL,CONVERT(NVARCHAR(2000),[CustLaborFringeMultiplier],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLaborOvhProfitMultiplier',NULL,CONVERT(NVARCHAR(2000),[CustLaborOvhProfitMultiplier],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDARBalance',NULL,CONVERT(NVARCHAR(2000),[CustJTDARBalance],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPracticeAreaLeader',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustPracticeAreaLeader],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustPracticeAreaLeader = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryBSTSBU',NULL,CONVERT(NVARCHAR(2000),[CustHistoryBSTSBU],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRevenueLabor',NULL,CONVERT(NVARCHAR(2000),[CustRevenueLabor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRevenueConsultant',NULL,CONVERT(NVARCHAR(2000),[CustRevenueConsultant],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRevenueExpense',NULL,CONVERT(NVARCHAR(2000),[CustRevenueExpense],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLastPayment',NULL,CONVERT(NVARCHAR(2000),[CustLastPayment],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustIncludeInvTemplateFromDate',NULL,CONVERT(NVARCHAR(2000),[CustIncludeInvTemplateFromDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeExpenseReport',NULL,CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeExpenseReport],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeUnits',NULL,CONVERT(NVARCHAR(2000),[CustFinalInvPkgIncludeUnits],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingStatus',NULL,CONVERT(NVARCHAR(2000),[CustBillingStatus],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingTeamLeader',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustBillingTeamLeader],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustBillingTeamLeader = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustUnbilledTransactions',NULL,CONVERT(NVARCHAR(2000),[CustUnbilledTransactions],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustUnbilledAmountBilling',NULL,CONVERT(NVARCHAR(2000),[CustUnbilledAmountBilling],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDRevenue',NULL,CONVERT(NVARCHAR(2000),[CustJTDRevenue],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDRevenue',NULL,CONVERT(NVARCHAR(2000),[CustYTDRevenue],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDBilled',NULL,CONVERT(NVARCHAR(2000),[CustJTDBilled],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDBilled',NULL,CONVERT(NVARCHAR(2000),[CustYTDBilled],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDReceived',NULL,CONVERT(NVARCHAR(2000),[CustJTDReceived],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDReceived',NULL,CONVERT(NVARCHAR(2000),[CustYTDReceived],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDWorkinProcess',NULL,CONVERT(NVARCHAR(2000),[CustJTDWorkinProcess],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDSpentBilling',NULL,CONVERT(NVARCHAR(2000),[CustJTDSpentBilling],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDSpentBilling',NULL,CONVERT(NVARCHAR(2000),[CustYTDSpentBilling],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDVariance',NULL,CONVERT(NVARCHAR(2000),[CustJTDVariance],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDVariance',NULL,CONVERT(NVARCHAR(2000),[CustYTDVariance],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDGoalMultiplier',NULL,CONVERT(NVARCHAR(2000),[CustJTDGoalMultiplier],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDGoalMultiplier',NULL,CONVERT(NVARCHAR(2000),[CustYTDGoalMultiplier],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDEffectiveMultiplier',NULL,CONVERT(NVARCHAR(2000),[CustJTDEffectiveMultiplier],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDEffectiveMultiplier',NULL,CONVERT(NVARCHAR(2000),[CustYTDEffectiveMultiplier],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDProfit',NULL,CONVERT(NVARCHAR(2000),[CustJTDProfit],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDProfit',NULL,CONVERT(NVARCHAR(2000),[CustYTDProfit],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDProfitPercent',NULL,CONVERT(NVARCHAR(2000),[CustJTDProfitPercent],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDProfitPercent',NULL,CONVERT(NVARCHAR(2000),[CustYTDProfitPercent],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDBilledTax',NULL,CONVERT(NVARCHAR(2000),[CustJTDBilledTax],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLastPaymentComment',NULL,CONVERT(NVARCHAR(2000),[CustLastPaymentComment],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustNearmapUsage',NULL,CONVERT(NVARCHAR(2000),[CustNearmapUsage],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEACInputLevel',NULL,CONVERT(NVARCHAR(2000),[CustEACInputLevel],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustTeamPracticeArea',NULL,CONVERT(NVARCHAR(2000),[CustTeamPracticeArea],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustNearmapLastCharge',NULL,CONVERT(NVARCHAR(2000),[CustNearmapLastCharge],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLastChargeDate',NULL,CONVERT(NVARCHAR(2000),[CustLastChargeDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustReimbMileageMultiplier',NULL,CONVERT(NVARCHAR(2000),[CustReimbMileageMultiplier],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustUnitTableAllLevels',NULL,CONVERT(NVARCHAR(2000),[CustUnitTableAllLevels],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEffortatCompletion',NULL,CONVERT(NVARCHAR(2000),[CustEffortatCompletion],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEfforttoComplete',NULL,CONVERT(NVARCHAR(2000),[CustEfforttoComplete],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustWriteoffAdjmt',NULL,CONVERT(NVARCHAR(2000),[CustWriteoffAdjmt],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFeeTypeRollup',NULL,CONVERT(NVARCHAR(2000),[CustFeeTypeRollup],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPercentComplete',NULL,CONVERT(NVARCHAR(2000),[CustPercentComplete],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackHTTProjectManager',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackHTTProjectManager],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryTrackHTTProjectManager = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackHTTDrafter',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackHTTDrafter],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryTrackHTTDrafter = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackHalffProject',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackHalffProject],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  PR as newDesc  on INSERTED.CustHistoryTrackHalffProject = newDesc.WBS1

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackHalffDiscipline',NULL,CONVERT(NVARCHAR(2000),[CustHistoryTrackHalffDiscipline],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackDisciplineProjectManager',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackDisciplineProjectManager],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryTrackDisciplineProjectManager = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackClientProjectNumber',NULL,CONVERT(NVARCHAR(2000),[CustHistoryTrackClientProjectNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackClientConfigurationNumber',NULL,CONVERT(NVARCHAR(2000),[CustHistoryTrackClientConfigurationNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackCapitalProject',NULL,CONVERT(NVARCHAR(2000),[CustHistoryTrackCapitalProject],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackClientOMNumber',NULL,CONVERT(NVARCHAR(2000),[CustHistoryTrackClientOMNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackClientCostCenter',NULL,CONVERT(NVARCHAR(2000),[CustHistoryTrackClientCostCenter],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackClientProjectManager',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackClientProjectManager],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  Contacts as newDesc  on INSERTED.CustHistoryTrackClientProjectManager = newDesc.ContactID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackClientDeadline',NULL,CONVERT(NVARCHAR(2000),[CustHistoryTrackClientDeadline],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackInternalDeadline',NULL,CONVERT(NVARCHAR(2000),[CustHistoryTrackInternalDeadline],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackStatus',NULL,CONVERT(NVARCHAR(2000),[CustHistoryTrackStatus],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAManager',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustQAManager],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustQAManager = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview1Goal',NULL,CONVERT(NVARCHAR(2000),[CustQAReview1Goal],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview2Goal',NULL,CONVERT(NVARCHAR(2000),[CustQAReview2Goal],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview3Goal',NULL,CONVERT(NVARCHAR(2000),[CustQAReview3Goal],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview1Date',NULL,CONVERT(NVARCHAR(2000),[CustQAReview1Date],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview2Date',NULL,CONVERT(NVARCHAR(2000),[CustQAReview2Date],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview3Date',NULL,CONVERT(NVARCHAR(2000),[CustQAReview3Date],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview1PercentComplete',NULL,CONVERT(NVARCHAR(2000),[CustQAReview1PercentComplete],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview2PercentComplete',NULL,CONVERT(NVARCHAR(2000),[CustQAReview2PercentComplete],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview3PercentComplete',NULL,CONVERT(NVARCHAR(2000),[CustQAReview3PercentComplete],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAWarning',NULL,CONVERT(NVARCHAR(2000),[CustQAWarning],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReviewRequired',NULL,CONVERT(NVARCHAR(2000),[CustQAReviewRequired],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLastWeek1SpentBilling',NULL,CONVERT(NVARCHAR(2000),[CustLastWeek1SpentBilling],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLastWeek2SpentBilling',NULL,CONVERT(NVARCHAR(2000),[CustLastWeek2SpentBilling],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustClientProjectContact',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustClientProjectContact],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  Contacts as newDesc  on INSERTED.CustClientProjectContact = newDesc.ContactID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLegalInvolvement',NULL,CONVERT(NVARCHAR(2000),[CustLegalInvolvement],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQADelegate',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustQADelegate],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustQADelegate = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLocationURL',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalPackageEmail',NULL,CONVERT(NVARCHAR(2000),[CustFinalPackageEmail],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalPackageCCEmail',NULL,CONVERT(NVARCHAR(2000),[CustFinalPackageCCEmail],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalPackageEmailInvRev',NULL,CONVERT(NVARCHAR(2000),[CustFinalPackageEmailInvRev],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPerDiemReimbursed',NULL,CONVERT(NVARCHAR(2000),[CustPerDiemReimbursed],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMasterContractType',NULL,CONVERT(NVARCHAR(2000),[CustMasterContractType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOriginalFeeDirLab',NULL,CONVERT(NVARCHAR(2000),[CustOriginalFeeDirLab],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOriginalFeeDirExp',NULL,CONVERT(NVARCHAR(2000),[CustOriginalFeeDirExp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOriginalConsultFee',NULL,CONVERT(NVARCHAR(2000),[CustOriginalConsultFee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOriginalReimbAllowExp',NULL,CONVERT(NVARCHAR(2000),[CustOriginalReimbAllowExp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOriginalReimbAllowCons',NULL,CONVERT(NVARCHAR(2000),[CustOriginalReimbAllowCons],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOriginalFeeTotal',NULL,CONVERT(NVARCHAR(2000),[CustOriginalFeeTotal],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingManager',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingManager],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustMarketingManager = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCorporateMarketing',NULL,CONVERT(NVARCHAR(2000),[CustCorporateMarketing],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustClientReferenceNo1',NULL,CONVERT(NVARCHAR(2000),[CustClientReferenceNo1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustClientReferenceNo2',NULL,CONVERT(NVARCHAR(2000),[CustClientReferenceNo2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingTermDescription',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingName',NULL,CONVERT(NVARCHAR(2000),[CustMarketingName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketable',NULL,CONVERT(NVARCHAR(2000),[CustMarketable],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEstimatedConstructionCost',NULL,CONVERT(NVARCHAR(2000),[CustEstimatedConstructionCost],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustActualConstructionCost',NULL,CONVERT(NVARCHAR(2000),[CustActualConstructionCost],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingTermNotes',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustShowCPPrevious',NULL,CONVERT(NVARCHAR(2000),[CustShowCPPrevious],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistorySource',NULL,CONVERT(NVARCHAR(2000),[CustHistorySource],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustGeoCode',NULL,CONVERT(NVARCHAR(2000),[CustGeoCode],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryProjectAccountant',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryProjectAccountant],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryProjectAccountant = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryOperationsManager',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryOperationsManager],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryOperationsManager = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTeamLeader',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTeamLeader],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryTeamLeader = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDesignOnly',NULL,CONVERT(NVARCHAR(2000),[CustDesignOnly],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceAsGenesisHalff',NULL,CONVERT(NVARCHAR(2000),[CustInvoiceAsGenesisHalff],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingFirstReviewComplete',NULL,CONVERT(NVARCHAR(2000),[CustMarketingFirstReviewComplete],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingFirstReviewCompleteDate',NULL,CONVERT(NVARCHAR(2000),[CustMarketingFirstReviewCompleteDate],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingFirstReviewCompleteEmployee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingFirstReviewCompleteEmployee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustMarketingFirstReviewCompleteEmployee = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingFinalReviewComplete',NULL,CONVERT(NVARCHAR(2000),[CustMarketingFinalReviewComplete],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingFinalReviewCompleteDate',NULL,CONVERT(NVARCHAR(2000),[CustMarketingFinalReviewCompleteDate],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingFinalReviewCompleteEmployee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingFinalReviewCompleteEmployee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustMarketingFinalReviewCompleteEmployee = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRoleAdd',NULL,CONVERT(NVARCHAR(2000),[CustRoleAdd],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMasterContractProject',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustMasterContractProject],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  PR as newDesc  on INSERTED.CustMasterContractProject = newDesc.WBS1

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustProgressReportNotes',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustProgressReportEditDate',NULL,CONVERT(NVARCHAR(2000),[CustProgressReportEditDate],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOperationsManager',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustOperationsManager],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustOperationsManager = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPAResponsibleForCollections',NULL,CONVERT(NVARCHAR(2000),[CustPAResponsibleForCollections],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustWorkShare',NULL,CONVERT(NVARCHAR(2000),[CustWorkShare],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustManualOHRateOverride',NULL,CONVERT(NVARCHAR(2000),[CustManualOHRateOverride],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSelectionProcess',NULL,CONVERT(NVARCHAR(2000),[CustSelectionProcess],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCollectionsResponsibility',NULL,CONVERT(NVARCHAR(2000),[CustCollectionsResponsibility],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceAsHTT',NULL,CONVERT(NVARCHAR(2000),[CustInvoiceAsHTT],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceAsCEI',NULL,CONVERT(NVARCHAR(2000),[CustInvoiceAsCEI],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceTemplate',NULL,CONVERT(NVARCHAR(2000),[CustInvoiceTemplate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRestartDate',NULL,CONVERT(NVARCHAR(2000),[CustRestartDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustContractAttachmentsReviewed',NULL,CONVERT(NVARCHAR(2000),[CustContractAttachmentsReviewed],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustProjectType',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustProjectType],121), NULL, IsNull(newDesc.CustProjectTypeName, N''), @source, @app
       FROM INSERTED left join  UDIC_ProjectType as newDesc  on INSERTED.CustProjectType = newDesc.UDIC_UID

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustService',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustService],121), NULL, IsNull(newDesc.CustServiceName, N''), @source, @app
       FROM INSERTED left join  UDIC_Service as newDesc  on INSERTED.CustService = newDesc.UDIC_UID

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSelectedFirm',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustSelectedFirm],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  Clendor as newDesc  on INSERTED.CustSelectedFirm = newDesc.ClientID

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOffice',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustOffice],121), NULL, IsNull(newDesc.CustOfficeName, N''), @source, @app
       FROM INSERTED left join  UDIC_Office as newDesc  on INSERTED.CustOffice = newDesc.UDIC_UID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustShortlisted',NULL,CONVERT(NVARCHAR(2000),[CustShortlisted],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInterviewCoordinator',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustInterviewCoordinator],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustInterviewCoordinator = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingNotes',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMultiTeamPursuit',NULL,CONVERT(NVARCHAR(2000),[CustMultiTeamPursuit],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCreateProjectStructure',NULL,CONVERT(NVARCHAR(2000),[CustCreateProjectStructure],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustTotalCompensation',NULL,CONVERT(NVARCHAR(2000),[CustTotalCompensation],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCollectionsRespSetup',NULL,CONVERT(NVARCHAR(2000),[CustCollectionsRespSetup],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustAdministrativeAssistant1',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustAdministrativeAssistant1],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustAdministrativeAssistant1 = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBiller',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustBiller],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustBiller = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingCompany',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustBillingCompany],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  Clendor as newDesc  on INSERTED.CustBillingCompany = newDesc.ClientID

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingContact',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustBillingContact],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  Contacts as newDesc  on INSERTED.CustBillingContact = newDesc.ContactID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingType1',NULL,CONVERT(NVARCHAR(2000),[CustBillingType1],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustClientAdvocate',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustClientAdvocate],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustClientAdvocate = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustClientProjectContact1',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustClientProjectContact1],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  Contacts as newDesc  on INSERTED.CustClientProjectContact1 = newDesc.ContactID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustClientSolicitationNumber',NULL,CONVERT(NVARCHAR(2000),[CustClientSolicitationNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCollectionsResponsibility1',NULL,CONVERT(NVARCHAR(2000),[CustCollectionsResponsibility1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCompanyStateCode',NULL,CONVERT(NVARCHAR(2000),[CustCompanyStateCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustContractExecuted',NULL,CONVERT(NVARCHAR(2000),[CustContractExecuted],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustContractType',NULL,CONVERT(NVARCHAR(2000),[CustContractType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCopy',NULL,CONVERT(NVARCHAR(2000),[CustCopy],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCorporateMarketing1',NULL,CONVERT(NVARCHAR(2000),[CustCorporateMarketing1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCreateNextSuffix',NULL,CONVERT(NVARCHAR(2000),[CustCreateNextSuffix],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCreateProjectStructure1',NULL,CONVERT(NVARCHAR(2000),[CustCreateProjectStructure1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCreateRegularProject',NULL,CONVERT(NVARCHAR(2000),[CustCreateRegularProject],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDescApproach',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDescAvailability',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDescCompetitor',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDescExperience',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDescRelationship',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDescStaff',NULL,'[text]', @source, @app
      FROM INSERTED


    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDirector1',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustDirector1],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustDirector1 = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEstimatedConsultantFee',NULL,CONVERT(NVARCHAR(2000),[CustEstimatedConsultantFee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEstimatedExpenseFee',NULL,CONVERT(NVARCHAR(2000),[CustEstimatedExpenseFee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEstimatedLaborFee',NULL,CONVERT(NVARCHAR(2000),[CustEstimatedLaborFee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalClientNum',NULL,CONVERT(NVARCHAR(2000),[CustEvalClientNum],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateApproach',NULL,CONVERT(NVARCHAR(2000),[CustEvalRateApproach],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateApproachNum',NULL,CONVERT(NVARCHAR(2000),[CustEvalRateApproachNum],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateAvailability',NULL,CONVERT(NVARCHAR(2000),[CustEvalRateAvailability],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateAvailabilityNum',NULL,CONVERT(NVARCHAR(2000),[CustEvalRateAvailabilityNum],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateCompetitor',NULL,CONVERT(NVARCHAR(2000),[CustEvalRateCompetitor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateCompetitorNum',NULL,CONVERT(NVARCHAR(2000),[CustEvalRateCompetitorNum],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateExperience',NULL,CONVERT(NVARCHAR(2000),[CustEvalRateExperience],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateExperienceNum',NULL,CONVERT(NVARCHAR(2000),[CustEvalRateExperienceNum],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateOverall',NULL,CONVERT(NVARCHAR(2000),[CustEvalRateOverall],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateRelationship',NULL,CONVERT(NVARCHAR(2000),[CustEvalRateRelationship],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateStaff',NULL,CONVERT(NVARCHAR(2000),[CustEvalRateStaff],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateStaffNum',NULL,CONVERT(NVARCHAR(2000),[CustEvalRateStaffNum],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFederalInd',NULL,CONVERT(NVARCHAR(2000),[CustFederalInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFeeProposalSubmitted',NULL,CONVERT(NVARCHAR(2000),[CustFeeProposalSubmitted],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFeeTypes',NULL,CONVERT(NVARCHAR(2000),[CustFeeTypes],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInterviewed',NULL,CONVERT(NVARCHAR(2000),[CustInterviewed],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLaborFringeMultiplier1',NULL,CONVERT(NVARCHAR(2000),[CustLaborFringeMultiplier1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLaborOvhProfitMultiplier1',NULL,CONVERT(NVARCHAR(2000),[CustLaborOvhProfitMultiplier1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLaborRateSchedule',NULL,CONVERT(NVARCHAR(2000),[CustLaborRateSchedule],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLocationURL1',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLSSingleLineProjectBill',NULL,CONVERT(NVARCHAR(2000),[CustLSSingleLineProjectBill],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustManagementLeader1',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustManagementLeader1],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustManagementLeader1 = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketable1',NULL,CONVERT(NVARCHAR(2000),[CustMarketable1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingCloseOutComplete',NULL,CONVERT(NVARCHAR(2000),[CustMarketingCloseOutComplete],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingCoordinator',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingCoordinator],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustMarketingCoordinator = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingInitialReviewComplete',NULL,CONVERT(NVARCHAR(2000),[CustMarketingInitialReviewComplete],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingLead',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingLead],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustMarketingLead = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingManager1',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingManager1],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustMarketingManager1 = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingName1',NULL,CONVERT(NVARCHAR(2000),[CustMarketingName1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustNearmapUsage1',NULL,CONVERT(NVARCHAR(2000),[CustNearmapUsage1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMBuyingOrganization',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMContractType',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMContractVehicles',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMCRMLastUpdateDate',NULL,CONVERT(NVARCHAR(2000),[custOMCRMLastUpdateDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMCRMLastUpdateTime',NULL,CONVERT(NVARCHAR(2000),[custOMCRMLastUpdateTime],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMDescription',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMDuration',NULL,CONVERT(NVARCHAR(2000),[custOMDuration],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMGovtResponseDate',NULL,CONVERT(NVARCHAR(2000),[custOMGovtResponseDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMGovtResponseTime',NULL,CONVERT(NVARCHAR(2000),[custOMGovtResponseTime],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMLastUpdateTime',NULL,CONVERT(NVARCHAR(2000),[custOMLastUpdateTime],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMNAICSCode',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMOppLastUpdateDt',NULL,CONVERT(NVARCHAR(2000),[custOMOppLastUpdateDt],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMOppManagerStatus',NULL,CONVERT(NVARCHAR(2000),[custOMOppManagerStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMOpportunityID',NULL,CONVERT(NVARCHAR(2000),[custOMOpportunityID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMOpportunityTitle',NULL,CONVERT(NVARCHAR(2000),[custOMOpportunityTitle],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMOppURL',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMPartnerResponseDate',NULL,CONVERT(NVARCHAR(2000),[custOMPartnerResponseDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMPartnerResponseTime',NULL,CONVERT(NVARCHAR(2000),[custOMPartnerResponseTime],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMPlaceofPerformance',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMPrimaryContact',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMSelectedBy',NULL,CONVERT(NVARCHAR(2000),[custOMSelectedBy],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMSocioEconomicStatus',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMSolicitationDate',NULL,CONVERT(NVARCHAR(2000),[custOMSolicitationDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMSolicitationNum',NULL,CONVERT(NVARCHAR(2000),[custOMSolicitationNum],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMTeamingOppSource',NULL,CONVERT(NVARCHAR(2000),[custOMTeamingOppSource],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMTypeofAward',NULL,CONVERT(NVARCHAR(2000),[custOMTypeofAward],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMValue',NULL,CONVERT(NVARCHAR(2000),[custOMValue],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOperationsManager1',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustOperationsManager1],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustOperationsManager1 = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPAResponsibleforAR',NULL,CONVERT(NVARCHAR(2000),[CustPAResponsibleforAR],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPracticeArea1',NULL,CONVERT(NVARCHAR(2000),[CustPracticeArea1],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPracticeAreaLeader1',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustPracticeAreaLeader1],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustPracticeAreaLeader1 = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustProjectType1',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustProjectType1],121), NULL, IsNull(newDesc.CustProjectTypeName, N''), @source, @app
       FROM INSERTED left join  UDIC_ProjectType as newDesc  on INSERTED.CustProjectType1 = newDesc.UDIC_UID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustReimbConsultantMultiplier1',NULL,CONVERT(NVARCHAR(2000),[CustReimbConsultantMultiplier1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustReimbExpenseMultiplier1',NULL,CONVERT(NVARCHAR(2000),[CustReimbExpenseMultiplier1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRevenueConsultFee',NULL,CONVERT(NVARCHAR(2000),[CustRevenueConsultFee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRevenueFeeDirExp',NULL,CONVERT(NVARCHAR(2000),[CustRevenueFeeDirExp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRevenueFeeDirLab',NULL,CONVERT(NVARCHAR(2000),[CustRevenueFeeDirLab],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRevenueReimbAllowCons',NULL,CONVERT(NVARCHAR(2000),[CustRevenueReimbAllowCons],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRevenueReimbAllowExp',NULL,CONVERT(NVARCHAR(2000),[CustRevenueReimbAllowExp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRFPSubmitted',NULL,CONVERT(NVARCHAR(2000),[CustRFPSubmitted],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRFQAnticipated',NULL,CONVERT(NVARCHAR(2000),[CustRFQAnticipated],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRFQRelease',NULL,CONVERT(NVARCHAR(2000),[CustRFQRelease],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRFQSelected',NULL,CONVERT(NVARCHAR(2000),[CustRFQSelected],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRFQSubmitted',NULL,CONVERT(NVARCHAR(2000),[CustRFQSubmitted],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSelectionProcess1',NULL,CONVERT(NVARCHAR(2000),[CustSelectionProcess1],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustService1',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustService1],121), NULL, IsNull(newDesc.CustServiceName, N''), @source, @app
       FROM INSERTED left join  UDIC_Service as newDesc  on INSERTED.CustService1 = newDesc.UDIC_UID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustShortlisted1',NULL,CONVERT(NVARCHAR(2000),[CustShortlisted1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustStrategicPursuit',NULL,CONVERT(NVARCHAR(2000),[CustStrategicPursuit],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustStrategicPursuitsManager',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustStrategicPursuitsManager],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustStrategicPursuitsManager = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSubmittalDeadline',NULL,CONVERT(NVARCHAR(2000),[CustSubmittalDeadline],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustTaxCode1',NULL,CONVERT(NVARCHAR(2000),[CustTaxCode1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustTeamPracticeArea1',NULL,CONVERT(NVARCHAR(2000),[CustTeamPracticeArea1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustTotalLaborMultiplier',NULL,CONVERT(NVARCHAR(2000),[CustTotalLaborMultiplier],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCollectionsResponsibilitySetup',NULL,CONVERT(NVARCHAR(2000),[CustCollectionsResponsibilitySetup],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSetupLaborFringeMultiplier',NULL,CONVERT(NVARCHAR(2000),[CustSetupLaborFringeMultiplier],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSetupReimbConsultantMultiplier',NULL,CONVERT(NVARCHAR(2000),[CustSetupReimbConsultantMultiplier],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEstStartDate',NULL,CONVERT(NVARCHAR(2000),[CustEstStartDate],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalCreator',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustEvalCreator],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEvalCreator = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPursuitComponents',NULL,CONVERT(NVARCHAR(2000),[CustPursuitComponents],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPursuitPlan',NULL,CONVERT(NVARCHAR(2000),[CustPursuitPlan],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinancialPotential',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFeeTypeRollupDuplicate',NULL,CONVERT(NVARCHAR(2000),[CustFeeTypeRollupDuplicate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCurrentlyWorking',NULL,CONVERT(NVARCHAR(2000),[CustCurrentlyWorking],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustObstacles',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustWriteoffAdjmtDuplicate',NULL,CONVERT(NVARCHAR(2000),[CustWriteoffAdjmtDuplicate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSetupLaborOverheadProfitMultiplier',NULL,CONVERT(NVARCHAR(2000),[CustSetupLaborOverheadProfitMultiplier],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInterviewDate',NULL,CONVERT(NVARCHAR(2000),[CustInterviewDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEffortatCompletionDuplicate',NULL,CONVERT(NVARCHAR(2000),[CustEffortatCompletionDuplicate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustClientMeetingCount',NULL,CONVERT(NVARCHAR(2000),[CustClientMeetingCount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEstCompletionDate',NULL,CONVERT(NVARCHAR(2000),[CustEstCompletionDate],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLocalOffice',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustLocalOffice],121), NULL, IsNull(newDesc.CustOfficeName, N''), @source, @app
       FROM INSERTED left join  UDIC_Office as newDesc  on INSERTED.CustLocalOffice = newDesc.UDIC_UID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPursuitEffort',NULL,CONVERT(NVARCHAR(2000),[CustPursuitEffort],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustGoNoGoEmp',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustTeaming',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEfforttoCompleteDuplicate',NULL,CONVERT(NVARCHAR(2000),[CustEfforttoCompleteDuplicate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustContractAwarded',NULL,CONVERT(NVARCHAR(2000),[CustContractAwarded],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustClientMeeting',NULL,CONVERT(NVARCHAR(2000),[CustClientMeeting],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustWinDiff',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSetupTaxCode',NULL,CONVERT(NVARCHAR(2000),[CustSetupTaxCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSetupReimbExpenseMultiplier',NULL,CONVERT(NVARCHAR(2000),[CustSetupReimbExpenseMultiplier],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPriorWork',NULL,CONVERT(NVARCHAR(2000),[CustPriorWork],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSOQSubmitted',NULL,CONVERT(NVARCHAR(2000),[CustSOQSubmitted],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSetupNearmapUsage',NULL,CONVERT(NVARCHAR(2000),[CustSetupNearmapUsage],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustContractSalesBalanced',NULL,CONVERT(NVARCHAR(2000),[CustContractSalesBalanced],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPursuitComponentSOQ',NULL,CONVERT(NVARCHAR(2000),[CustPursuitComponentSOQ],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPursuitComponentProposal',NULL,CONVERT(NVARCHAR(2000),[CustPursuitComponentProposal],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPursuitComponentInterview',NULL,CONVERT(NVARCHAR(2000),[CustPursuitComponentInterview],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_ProjectCustomTabFields]
      ON [dbo].[ProjectCustomTabFields]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ProjectCustomTabFields'
    
      If UPDATE([WBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'WBS1',
      CONVERT(NVARCHAR(2000),DELETED.[WBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[WBS1] Is Null And
				DELETED.[WBS1] Is Not Null
			) Or
			(
				INSERTED.[WBS1] Is Not Null And
				DELETED.[WBS1] Is Null
			) Or
			(
				INSERTED.[WBS1] !=
				DELETED.[WBS1]
			)
		) 
		END		
		
      If UPDATE([WBS2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'WBS2',
      CONVERT(NVARCHAR(2000),DELETED.[WBS2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[WBS2] Is Null And
				DELETED.[WBS2] Is Not Null
			) Or
			(
				INSERTED.[WBS2] Is Not Null And
				DELETED.[WBS2] Is Null
			) Or
			(
				INSERTED.[WBS2] !=
				DELETED.[WBS2]
			)
		) 
		END		
		
      If UPDATE([WBS3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'WBS3',
      CONVERT(NVARCHAR(2000),DELETED.[WBS3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[WBS3] Is Null And
				DELETED.[WBS3] Is Not Null
			) Or
			(
				INSERTED.[WBS3] Is Not Null And
				DELETED.[WBS3] Is Null
			) Or
			(
				INSERTED.[WBS3] !=
				DELETED.[WBS3]
			)
		) 
		END		
		
      If UPDATE([CustHistoryKeep])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryKeep',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryKeep],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryKeep],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryKeep] Is Null And
				DELETED.[CustHistoryKeep] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryKeep] Is Not Null And
				DELETED.[CustHistoryKeep] Is Null
			) Or
			(
				INSERTED.[CustHistoryKeep] !=
				DELETED.[CustHistoryKeep]
			)
		) 
		END		
		
     If UPDATE([CustHistoryContractAdmin])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryContractAdmin',
     CONVERT(NVARCHAR(2000),DELETED.[CustHistoryContractAdmin],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryContractAdmin],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryContractAdmin] Is Null And
				DELETED.[CustHistoryContractAdmin] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryContractAdmin] Is Not Null And
				DELETED.[CustHistoryContractAdmin] Is Null
			) Or
			(
				INSERTED.[CustHistoryContractAdmin] !=
				DELETED.[CustHistoryContractAdmin]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryContractAdmin = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryContractAdmin = newDesc.Employee
		END		
		
     If UPDATE([CustHistoryLeadEngineer])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryLeadEngineer',
     CONVERT(NVARCHAR(2000),DELETED.[CustHistoryLeadEngineer],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryLeadEngineer],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryLeadEngineer] Is Null And
				DELETED.[CustHistoryLeadEngineer] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryLeadEngineer] Is Not Null And
				DELETED.[CustHistoryLeadEngineer] Is Null
			) Or
			(
				INSERTED.[CustHistoryLeadEngineer] !=
				DELETED.[CustHistoryLeadEngineer]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryLeadEngineer = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryLeadEngineer = newDesc.Employee
		END		
		
      If UPDATE([custInvoiceGroup])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custInvoiceGroup',
      CONVERT(NVARCHAR(2000),DELETED.[custInvoiceGroup],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custInvoiceGroup],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custInvoiceGroup] Is Null And
				DELETED.[custInvoiceGroup] Is Not Null
			) Or
			(
				INSERTED.[custInvoiceGroup] Is Not Null And
				DELETED.[custInvoiceGroup] Is Null
			) Or
			(
				INSERTED.[custInvoiceGroup] !=
				DELETED.[custInvoiceGroup]
			)
		) 
		END		
		
      If UPDATE([custInvoiceFolder])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custInvoiceFolder',
      CONVERT(NVARCHAR(2000),DELETED.[custInvoiceFolder],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custInvoiceFolder],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custInvoiceFolder] Is Null And
				DELETED.[custInvoiceFolder] Is Not Null
			) Or
			(
				INSERTED.[custInvoiceFolder] Is Not Null And
				DELETED.[custInvoiceFolder] Is Null
			) Or
			(
				INSERTED.[custInvoiceFolder] !=
				DELETED.[custInvoiceFolder]
			)
		) 
		END		
		
      If UPDATE([custInvoiceStageFlow])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custInvoiceStageFlow',
      CONVERT(NVARCHAR(2000),DELETED.[custInvoiceStageFlow],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custInvoiceStageFlow],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custInvoiceStageFlow] Is Null And
				DELETED.[custInvoiceStageFlow] Is Not Null
			) Or
			(
				INSERTED.[custInvoiceStageFlow] Is Not Null And
				DELETED.[custInvoiceStageFlow] Is Null
			) Or
			(
				INSERTED.[custInvoiceStageFlow] !=
				DELETED.[custInvoiceStageFlow]
			)
		) 
		END		
		
      If UPDATE([CustInvoiceFolderFinals])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceFolderFinals',
      CONVERT(NVARCHAR(2000),DELETED.[CustInvoiceFolderFinals],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustInvoiceFolderFinals],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustInvoiceFolderFinals] Is Null And
				DELETED.[CustInvoiceFolderFinals] Is Not Null
			) Or
			(
				INSERTED.[CustInvoiceFolderFinals] Is Not Null And
				DELETED.[CustInvoiceFolderFinals] Is Null
			) Or
			(
				INSERTED.[CustInvoiceFolderFinals] !=
				DELETED.[CustInvoiceFolderFinals]
			)
		) 
		END		
		
      If UPDATE([CustFinalInvPkgEmailTemplate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgEmailTemplate',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalInvPkgEmailTemplate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalInvPkgEmailTemplate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalInvPkgEmailTemplate] Is Null And
				DELETED.[CustFinalInvPkgEmailTemplate] Is Not Null
			) Or
			(
				INSERTED.[CustFinalInvPkgEmailTemplate] Is Not Null And
				DELETED.[CustFinalInvPkgEmailTemplate] Is Null
			) Or
			(
				INSERTED.[CustFinalInvPkgEmailTemplate] !=
				DELETED.[CustFinalInvPkgEmailTemplate]
			)
		) 
		END		
		
      If UPDATE([CustFinalInvPkgIncludeInvoice])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeInvoice',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalInvPkgIncludeInvoice],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalInvPkgIncludeInvoice],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalInvPkgIncludeInvoice] Is Null And
				DELETED.[CustFinalInvPkgIncludeInvoice] Is Not Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeInvoice] Is Not Null And
				DELETED.[CustFinalInvPkgIncludeInvoice] Is Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeInvoice] !=
				DELETED.[CustFinalInvPkgIncludeInvoice]
			)
		) 
		END		
		
      If UPDATE([CustFinalInvPkgIncludeInvoiceMarkup])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeInvoiceMarkup',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalInvPkgIncludeInvoiceMarkup],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalInvPkgIncludeInvoiceMarkup],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalInvPkgIncludeInvoiceMarkup] Is Null And
				DELETED.[CustFinalInvPkgIncludeInvoiceMarkup] Is Not Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeInvoiceMarkup] Is Not Null And
				DELETED.[CustFinalInvPkgIncludeInvoiceMarkup] Is Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeInvoiceMarkup] !=
				DELETED.[CustFinalInvPkgIncludeInvoiceMarkup]
			)
		) 
		END		
		
      If UPDATE([CustFinalInvPkgIncludeTimesheetData])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeTimesheetData',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalInvPkgIncludeTimesheetData],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalInvPkgIncludeTimesheetData],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalInvPkgIncludeTimesheetData] Is Null And
				DELETED.[CustFinalInvPkgIncludeTimesheetData] Is Not Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeTimesheetData] Is Not Null And
				DELETED.[CustFinalInvPkgIncludeTimesheetData] Is Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeTimesheetData] !=
				DELETED.[CustFinalInvPkgIncludeTimesheetData]
			)
		) 
		END		
		
      If UPDATE([CustFinalInvPkgTimesheetTemplate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgTimesheetTemplate',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalInvPkgTimesheetTemplate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalInvPkgTimesheetTemplate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalInvPkgTimesheetTemplate] Is Null And
				DELETED.[CustFinalInvPkgTimesheetTemplate] Is Not Null
			) Or
			(
				INSERTED.[CustFinalInvPkgTimesheetTemplate] Is Not Null And
				DELETED.[CustFinalInvPkgTimesheetTemplate] Is Null
			) Or
			(
				INSERTED.[CustFinalInvPkgTimesheetTemplate] !=
				DELETED.[CustFinalInvPkgTimesheetTemplate]
			)
		) 
		END		
		
      If UPDATE([CustFinalInvPkgIncludeSubInvoices])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeSubInvoices',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalInvPkgIncludeSubInvoices],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalInvPkgIncludeSubInvoices],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalInvPkgIncludeSubInvoices] Is Null And
				DELETED.[CustFinalInvPkgIncludeSubInvoices] Is Not Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeSubInvoices] Is Not Null And
				DELETED.[CustFinalInvPkgIncludeSubInvoices] Is Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeSubInvoices] !=
				DELETED.[CustFinalInvPkgIncludeSubInvoices]
			)
		) 
		END		
		
      If UPDATE([CustFinalInvPkgSubInvoicesMinimum])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgSubInvoicesMinimum',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalInvPkgSubInvoicesMinimum],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalInvPkgSubInvoicesMinimum],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalInvPkgSubInvoicesMinimum] Is Null And
				DELETED.[CustFinalInvPkgSubInvoicesMinimum] Is Not Null
			) Or
			(
				INSERTED.[CustFinalInvPkgSubInvoicesMinimum] Is Not Null And
				DELETED.[CustFinalInvPkgSubInvoicesMinimum] Is Null
			) Or
			(
				INSERTED.[CustFinalInvPkgSubInvoicesMinimum] !=
				DELETED.[CustFinalInvPkgSubInvoicesMinimum]
			)
		) 
		END		
		
      If UPDATE([CustFinalInvPkgIncludeSubInvoicesMarkup])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeSubInvoicesMarkup',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalInvPkgIncludeSubInvoicesMarkup],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalInvPkgIncludeSubInvoicesMarkup],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalInvPkgIncludeSubInvoicesMarkup] Is Null And
				DELETED.[CustFinalInvPkgIncludeSubInvoicesMarkup] Is Not Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeSubInvoicesMarkup] Is Not Null And
				DELETED.[CustFinalInvPkgIncludeSubInvoicesMarkup] Is Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeSubInvoicesMarkup] !=
				DELETED.[CustFinalInvPkgIncludeSubInvoicesMarkup]
			)
		) 
		END		
		
      If UPDATE([CustFinalInvPkgIncludeDirectSub])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeDirectSub',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalInvPkgIncludeDirectSub],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalInvPkgIncludeDirectSub],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalInvPkgIncludeDirectSub] Is Null And
				DELETED.[CustFinalInvPkgIncludeDirectSub] Is Not Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeDirectSub] Is Not Null And
				DELETED.[CustFinalInvPkgIncludeDirectSub] Is Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeDirectSub] !=
				DELETED.[CustFinalInvPkgIncludeDirectSub]
			)
		) 
		END		
		
      If UPDATE([CustFinalInvPkgIncludeExpReceipts])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeExpReceipts',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalInvPkgIncludeExpReceipts],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalInvPkgIncludeExpReceipts],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalInvPkgIncludeExpReceipts] Is Null And
				DELETED.[CustFinalInvPkgIncludeExpReceipts] Is Not Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeExpReceipts] Is Not Null And
				DELETED.[CustFinalInvPkgIncludeExpReceipts] Is Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeExpReceipts] !=
				DELETED.[CustFinalInvPkgIncludeExpReceipts]
			)
		) 
		END		
		
      If UPDATE([CustFinalInvPkgExpReceiptsMinimum])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgExpReceiptsMinimum',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalInvPkgExpReceiptsMinimum],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalInvPkgExpReceiptsMinimum],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalInvPkgExpReceiptsMinimum] Is Null And
				DELETED.[CustFinalInvPkgExpReceiptsMinimum] Is Not Null
			) Or
			(
				INSERTED.[CustFinalInvPkgExpReceiptsMinimum] Is Not Null And
				DELETED.[CustFinalInvPkgExpReceiptsMinimum] Is Null
			) Or
			(
				INSERTED.[CustFinalInvPkgExpReceiptsMinimum] !=
				DELETED.[CustFinalInvPkgExpReceiptsMinimum]
			)
		) 
		END		
		
      If UPDATE([CustFinalInvPkgIncludeExpReceiptsMarkup])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeExpReceiptsMarkup',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalInvPkgIncludeExpReceiptsMarkup],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalInvPkgIncludeExpReceiptsMarkup],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalInvPkgIncludeExpReceiptsMarkup] Is Null And
				DELETED.[CustFinalInvPkgIncludeExpReceiptsMarkup] Is Not Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeExpReceiptsMarkup] Is Not Null And
				DELETED.[CustFinalInvPkgIncludeExpReceiptsMarkup] Is Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeExpReceiptsMarkup] !=
				DELETED.[CustFinalInvPkgIncludeExpReceiptsMarkup]
			)
		) 
		END		
		
      If UPDATE([CustFinalInvPkgIncludeDirectExp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeDirectExp',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalInvPkgIncludeDirectExp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalInvPkgIncludeDirectExp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalInvPkgIncludeDirectExp] Is Null And
				DELETED.[CustFinalInvPkgIncludeDirectExp] Is Not Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeDirectExp] Is Not Null And
				DELETED.[CustFinalInvPkgIncludeDirectExp] Is Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeDirectExp] !=
				DELETED.[CustFinalInvPkgIncludeDirectExp]
			)
		) 
		END		
		
      If UPDATE([CustFinalInvPkgSubExpOrder])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgSubExpOrder',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalInvPkgSubExpOrder],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalInvPkgSubExpOrder],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalInvPkgSubExpOrder] Is Null And
				DELETED.[CustFinalInvPkgSubExpOrder] Is Not Null
			) Or
			(
				INSERTED.[CustFinalInvPkgSubExpOrder] Is Not Null And
				DELETED.[CustFinalInvPkgSubExpOrder] Is Null
			) Or
			(
				INSERTED.[CustFinalInvPkgSubExpOrder] !=
				DELETED.[CustFinalInvPkgSubExpOrder]
			)
		) 
		END		
		
      If UPDATE([CustInvoiceStage])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceStage',
      CONVERT(NVARCHAR(2000),DELETED.[CustInvoiceStage],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustInvoiceStage],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustInvoiceStage] Is Null And
				DELETED.[CustInvoiceStage] Is Not Null
			) Or
			(
				INSERTED.[CustInvoiceStage] Is Not Null And
				DELETED.[CustInvoiceStage] Is Null
			) Or
			(
				INSERTED.[CustInvoiceStage] !=
				DELETED.[CustInvoiceStage]
			)
		) 
		END		
		
      If UPDATE([CustInvoiceStageDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceStageDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustInvoiceStageDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustInvoiceStageDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustInvoiceStageDate] Is Null And
				DELETED.[CustInvoiceStageDate] Is Not Null
			) Or
			(
				INSERTED.[CustInvoiceStageDate] Is Not Null And
				DELETED.[CustInvoiceStageDate] Is Null
			) Or
			(
				INSERTED.[CustInvoiceStageDate] !=
				DELETED.[CustInvoiceStageDate]
			)
		) 
		END		
		
     If UPDATE([CustInvoiceReviewer])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceReviewer',
     CONVERT(NVARCHAR(2000),DELETED.[CustInvoiceReviewer],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustInvoiceReviewer],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustInvoiceReviewer] Is Null And
				DELETED.[CustInvoiceReviewer] Is Not Null
			) Or
			(
				INSERTED.[CustInvoiceReviewer] Is Not Null And
				DELETED.[CustInvoiceReviewer] Is Null
			) Or
			(
				INSERTED.[CustInvoiceReviewer] !=
				DELETED.[CustInvoiceReviewer]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustInvoiceReviewer = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustInvoiceReviewer = newDesc.Employee
		END		
		
      If UPDATE([CustCloseDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCloseDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustCloseDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCloseDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCloseDate] Is Null And
				DELETED.[CustCloseDate] Is Not Null
			) Or
			(
				INSERTED.[CustCloseDate] Is Not Null And
				DELETED.[CustCloseDate] Is Null
			) Or
			(
				INSERTED.[CustCloseDate] !=
				DELETED.[CustCloseDate]
			)
		) 
		END		
		
      If UPDATE([CustCloseUser])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCloseUser',
      CONVERT(NVARCHAR(2000),DELETED.[CustCloseUser],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCloseUser],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCloseUser] Is Null And
				DELETED.[CustCloseUser] Is Not Null
			) Or
			(
				INSERTED.[CustCloseUser] Is Not Null And
				DELETED.[CustCloseUser] Is Null
			) Or
			(
				INSERTED.[CustCloseUser] !=
				DELETED.[CustCloseUser]
			)
		) 
		END		
		
      If UPDATE([CustConstructionStartDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustConstructionStartDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustConstructionStartDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustConstructionStartDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustConstructionStartDate] Is Null And
				DELETED.[CustConstructionStartDate] Is Not Null
			) Or
			(
				INSERTED.[CustConstructionStartDate] Is Not Null And
				DELETED.[CustConstructionStartDate] Is Null
			) Or
			(
				INSERTED.[CustConstructionStartDate] !=
				DELETED.[CustConstructionStartDate]
			)
		) 
		END		
		
      If UPDATE([CustSize])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSize',
      CONVERT(NVARCHAR(2000),DELETED.[CustSize],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSize],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustSize] Is Null And
				DELETED.[CustSize] Is Not Null
			) Or
			(
				INSERTED.[CustSize] Is Not Null And
				DELETED.[CustSize] Is Null
			) Or
			(
				INSERTED.[CustSize] !=
				DELETED.[CustSize]
			)
		) 
		END		
		
      If UPDATE([CustSizeUnit])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSizeUnit',
      CONVERT(NVARCHAR(2000),DELETED.[CustSizeUnit],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSizeUnit],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustSizeUnit] Is Null And
				DELETED.[CustSizeUnit] Is Not Null
			) Or
			(
				INSERTED.[CustSizeUnit] Is Not Null And
				DELETED.[CustSizeUnit] Is Null
			) Or
			(
				INSERTED.[CustSizeUnit] !=
				DELETED.[CustSizeUnit]
			)
		) 
		END		
		
      If UPDATE([CustBillingType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingType',
      CONVERT(NVARCHAR(2000),DELETED.[CustBillingType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBillingType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustBillingType] Is Null And
				DELETED.[CustBillingType] Is Not Null
			) Or
			(
				INSERTED.[CustBillingType] Is Not Null And
				DELETED.[CustBillingType] Is Null
			) Or
			(
				INSERTED.[CustBillingType] !=
				DELETED.[CustBillingType]
			)
		) 
		END		
		
      If UPDATE([CustPracticeArea])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPracticeArea',
      CONVERT(NVARCHAR(2000),DELETED.[CustPracticeArea],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPracticeArea],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustPracticeArea] Is Null And
				DELETED.[CustPracticeArea] Is Not Null
			) Or
			(
				INSERTED.[CustPracticeArea] Is Not Null And
				DELETED.[CustPracticeArea] Is Null
			) Or
			(
				INSERTED.[CustPracticeArea] !=
				DELETED.[CustPracticeArea]
			)
		) 
		END		
		
      If UPDATE([CustFunctionalArea])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFunctionalArea',
      CONVERT(NVARCHAR(2000),DELETED.[CustFunctionalArea],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFunctionalArea],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFunctionalArea] Is Null And
				DELETED.[CustFunctionalArea] Is Not Null
			) Or
			(
				INSERTED.[CustFunctionalArea] Is Not Null And
				DELETED.[CustFunctionalArea] Is Null
			) Or
			(
				INSERTED.[CustFunctionalArea] !=
				DELETED.[CustFunctionalArea]
			)
		) 
		END		
		
      If UPDATE([CustHistoryId])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryId',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryId],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryId],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryId] Is Null And
				DELETED.[CustHistoryId] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryId] Is Not Null And
				DELETED.[CustHistoryId] Is Null
			) Or
			(
				INSERTED.[CustHistoryId] !=
				DELETED.[CustHistoryId]
			)
		) 
		END		
		
      If UPDATE([CustHistoryMarketSegment])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryMarketSegment',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryMarketSegment],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryMarketSegment],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryMarketSegment] Is Null And
				DELETED.[CustHistoryMarketSegment] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryMarketSegment] Is Not Null And
				DELETED.[CustHistoryMarketSegment] Is Null
			) Or
			(
				INSERTED.[CustHistoryMarketSegment] !=
				DELETED.[CustHistoryMarketSegment]
			)
		) 
		END		
		
      If UPDATE([CustHistoryServiceLine])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryServiceLine',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryServiceLine],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryServiceLine],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryServiceLine] Is Null And
				DELETED.[CustHistoryServiceLine] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryServiceLine] Is Not Null And
				DELETED.[CustHistoryServiceLine] Is Null
			) Or
			(
				INSERTED.[CustHistoryServiceLine] !=
				DELETED.[CustHistoryServiceLine]
			)
		) 
		END		
		
      If UPDATE([CustHistoryWorkType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryWorkType',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryWorkType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryWorkType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryWorkType] Is Null And
				DELETED.[CustHistoryWorkType] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryWorkType] Is Not Null And
				DELETED.[CustHistoryWorkType] Is Null
			) Or
			(
				INSERTED.[CustHistoryWorkType] !=
				DELETED.[CustHistoryWorkType]
			)
		) 
		END		
		
     If UPDATE([CustHistoryTeam])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTeam',
     CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTeam],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTeam],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryTeam] Is Null And
				DELETED.[CustHistoryTeam] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryTeam] Is Not Null And
				DELETED.[CustHistoryTeam] Is Null
			) Or
			(
				INSERTED.[CustHistoryTeam] !=
				DELETED.[CustHistoryTeam]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryTeam = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryTeam = newDesc.Employee
		END		
		
      If UPDATE([CustBillingWeek])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingWeek',
      CONVERT(NVARCHAR(2000),DELETED.[CustBillingWeek],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBillingWeek],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustBillingWeek] Is Null And
				DELETED.[CustBillingWeek] Is Not Null
			) Or
			(
				INSERTED.[CustBillingWeek] Is Not Null And
				DELETED.[CustBillingWeek] Is Null
			) Or
			(
				INSERTED.[CustBillingWeek] !=
				DELETED.[CustBillingWeek]
			)
		) 
		END		
		
      If UPDATE([CustLumpSumPhase])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLumpSumPhase',
      CONVERT(NVARCHAR(2000),DELETED.[CustLumpSumPhase],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLumpSumPhase],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustLumpSumPhase] Is Null And
				DELETED.[CustLumpSumPhase] Is Not Null
			) Or
			(
				INSERTED.[CustLumpSumPhase] Is Not Null And
				DELETED.[CustLumpSumPhase] Is Null
			) Or
			(
				INSERTED.[CustLumpSumPhase] !=
				DELETED.[CustLumpSumPhase]
			)
		) 
		END		
		
      If UPDATE([CustConstructionFeePercent])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustConstructionFeePercent',
      CONVERT(NVARCHAR(2000),DELETED.[CustConstructionFeePercent],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustConstructionFeePercent],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustConstructionFeePercent] Is Null And
				DELETED.[CustConstructionFeePercent] Is Not Null
			) Or
			(
				INSERTED.[CustConstructionFeePercent] Is Not Null And
				DELETED.[CustConstructionFeePercent] Is Null
			) Or
			(
				INSERTED.[CustConstructionFeePercent] !=
				DELETED.[CustConstructionFeePercent]
			)
		) 
		END		
		
      If UPDATE([CustConstructionFeeCost])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustConstructionFeeCost',
      CONVERT(NVARCHAR(2000),DELETED.[CustConstructionFeeCost],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustConstructionFeeCost],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustConstructionFeeCost] Is Null And
				DELETED.[CustConstructionFeeCost] Is Not Null
			) Or
			(
				INSERTED.[CustConstructionFeeCost] Is Not Null And
				DELETED.[CustConstructionFeeCost] Is Null
			) Or
			(
				INSERTED.[CustConstructionFeeCost] !=
				DELETED.[CustConstructionFeeCost]
			)
		) 
		END		
		
      If UPDATE([CustHistoryBSTTaxCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryBSTTaxCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryBSTTaxCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryBSTTaxCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryBSTTaxCode] Is Null And
				DELETED.[CustHistoryBSTTaxCode] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryBSTTaxCode] Is Not Null And
				DELETED.[CustHistoryBSTTaxCode] Is Null
			) Or
			(
				INSERTED.[CustHistoryBSTTaxCode] !=
				DELETED.[CustHistoryBSTTaxCode]
			)
		) 
		END		
		
      If UPDATE([CustSource])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSource',
      CONVERT(NVARCHAR(2000),DELETED.[CustSource],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSource],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustSource] Is Null And
				DELETED.[CustSource] Is Not Null
			) Or
			(
				INSERTED.[CustSource] Is Not Null And
				DELETED.[CustSource] Is Null
			) Or
			(
				INSERTED.[CustSource] !=
				DELETED.[CustSource]
			)
		) 
		END		
		
      If UPDATE([CustCostPlusFixedFeePercent])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCostPlusFixedFeePercent',
      CONVERT(NVARCHAR(2000),DELETED.[CustCostPlusFixedFeePercent],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCostPlusFixedFeePercent],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCostPlusFixedFeePercent] Is Null And
				DELETED.[CustCostPlusFixedFeePercent] Is Not Null
			) Or
			(
				INSERTED.[CustCostPlusFixedFeePercent] Is Not Null And
				DELETED.[CustCostPlusFixedFeePercent] Is Null
			) Or
			(
				INSERTED.[CustCostPlusFixedFeePercent] !=
				DELETED.[CustCostPlusFixedFeePercent]
			)
		) 
		END		
		
     If UPDATE([CustDirector])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDirector',
     CONVERT(NVARCHAR(2000),DELETED.[CustDirector],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustDirector],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustDirector] Is Null And
				DELETED.[CustDirector] Is Not Null
			) Or
			(
				INSERTED.[CustDirector] Is Not Null And
				DELETED.[CustDirector] Is Null
			) Or
			(
				INSERTED.[CustDirector] !=
				DELETED.[CustDirector]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustDirector = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustDirector = newDesc.Employee
		END		
		
     If UPDATE([CustManagementLeader])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustManagementLeader',
     CONVERT(NVARCHAR(2000),DELETED.[CustManagementLeader],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustManagementLeader],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustManagementLeader] Is Null And
				DELETED.[CustManagementLeader] Is Not Null
			) Or
			(
				INSERTED.[CustManagementLeader] Is Not Null And
				DELETED.[CustManagementLeader] Is Null
			) Or
			(
				INSERTED.[CustManagementLeader] !=
				DELETED.[CustManagementLeader]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustManagementLeader = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustManagementLeader = newDesc.Employee
		END		
		
     If UPDATE([CustAdministrativeAssistant])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustAdministrativeAssistant',
     CONVERT(NVARCHAR(2000),DELETED.[CustAdministrativeAssistant],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustAdministrativeAssistant],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustAdministrativeAssistant] Is Null And
				DELETED.[CustAdministrativeAssistant] Is Not Null
			) Or
			(
				INSERTED.[CustAdministrativeAssistant] Is Not Null And
				DELETED.[CustAdministrativeAssistant] Is Null
			) Or
			(
				INSERTED.[CustAdministrativeAssistant] !=
				DELETED.[CustAdministrativeAssistant]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustAdministrativeAssistant = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustAdministrativeAssistant = newDesc.Employee
		END		
		
     If UPDATE([CustHistoryBSTProjectManager])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryBSTProjectManager',
     CONVERT(NVARCHAR(2000),DELETED.[CustHistoryBSTProjectManager],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryBSTProjectManager],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryBSTProjectManager] Is Null And
				DELETED.[CustHistoryBSTProjectManager] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryBSTProjectManager] Is Not Null And
				DELETED.[CustHistoryBSTProjectManager] Is Null
			) Or
			(
				INSERTED.[CustHistoryBSTProjectManager] !=
				DELETED.[CustHistoryBSTProjectManager]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryBSTProjectManager = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryBSTProjectManager = newDesc.Employee
		END		
		
      If UPDATE([CustInvoiceSuppDocs])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceSuppDocs',
      CONVERT(NVARCHAR(2000),DELETED.[CustInvoiceSuppDocs],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustInvoiceSuppDocs],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustInvoiceSuppDocs] Is Null And
				DELETED.[CustInvoiceSuppDocs] Is Not Null
			) Or
			(
				INSERTED.[CustInvoiceSuppDocs] Is Not Null And
				DELETED.[CustInvoiceSuppDocs] Is Null
			) Or
			(
				INSERTED.[CustInvoiceSuppDocs] !=
				DELETED.[CustInvoiceSuppDocs]
			)
		) 
		END		
		
      If UPDATE([CustFeeType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFeeType',
      CONVERT(NVARCHAR(2000),DELETED.[CustFeeType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFeeType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFeeType] Is Null And
				DELETED.[CustFeeType] Is Not Null
			) Or
			(
				INSERTED.[CustFeeType] Is Not Null And
				DELETED.[CustFeeType] Is Null
			) Or
			(
				INSERTED.[CustFeeType] !=
				DELETED.[CustFeeType]
			)
		) 
		END		
		
      If UPDATE([CustLimitatLevelAbove])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLimitatLevelAbove',
      CONVERT(NVARCHAR(2000),DELETED.[CustLimitatLevelAbove],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLimitatLevelAbove],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustLimitatLevelAbove] Is Null And
				DELETED.[CustLimitatLevelAbove] Is Not Null
			) Or
			(
				INSERTED.[CustLimitatLevelAbove] Is Not Null And
				DELETED.[CustLimitatLevelAbove] Is Null
			) Or
			(
				INSERTED.[CustLimitatLevelAbove] !=
				DELETED.[CustLimitatLevelAbove]
			)
		) 
		END		
		
      If UPDATE([CustTaxCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustTaxCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustTaxCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTaxCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustTaxCode] Is Null And
				DELETED.[CustTaxCode] Is Not Null
			) Or
			(
				INSERTED.[CustTaxCode] Is Not Null And
				DELETED.[CustTaxCode] Is Null
			) Or
			(
				INSERTED.[CustTaxCode] !=
				DELETED.[CustTaxCode]
			)
		) 
		END		
		
      If UPDATE([CustCostPlusFixedFeeAmount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCostPlusFixedFeeAmount',
      CONVERT(NVARCHAR(2000),DELETED.[CustCostPlusFixedFeeAmount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCostPlusFixedFeeAmount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCostPlusFixedFeeAmount] Is Null And
				DELETED.[CustCostPlusFixedFeeAmount] Is Not Null
			) Or
			(
				INSERTED.[CustCostPlusFixedFeeAmount] Is Not Null And
				DELETED.[CustCostPlusFixedFeeAmount] Is Null
			) Or
			(
				INSERTED.[CustCostPlusFixedFeeAmount] !=
				DELETED.[CustCostPlusFixedFeeAmount]
			)
		) 
		END		
		
      If UPDATE([CustShowTaskBreakdown])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustShowTaskBreakdown',
      CONVERT(NVARCHAR(2000),DELETED.[CustShowTaskBreakdown],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustShowTaskBreakdown],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustShowTaskBreakdown] Is Null And
				DELETED.[CustShowTaskBreakdown] Is Not Null
			) Or
			(
				INSERTED.[CustShowTaskBreakdown] Is Not Null And
				DELETED.[CustShowTaskBreakdown] Is Null
			) Or
			(
				INSERTED.[CustShowTaskBreakdown] !=
				DELETED.[CustShowTaskBreakdown]
			)
		) 
		END		
		
      If UPDATE([CustReimbExpenseMultiplier])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustReimbExpenseMultiplier',
      CONVERT(NVARCHAR(2000),DELETED.[CustReimbExpenseMultiplier],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustReimbExpenseMultiplier],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustReimbExpenseMultiplier] Is Null And
				DELETED.[CustReimbExpenseMultiplier] Is Not Null
			) Or
			(
				INSERTED.[CustReimbExpenseMultiplier] Is Not Null And
				DELETED.[CustReimbExpenseMultiplier] Is Null
			) Or
			(
				INSERTED.[CustReimbExpenseMultiplier] !=
				DELETED.[CustReimbExpenseMultiplier]
			)
		) 
		END		
		
      If UPDATE([CustReimbConsultantMultiplier])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustReimbConsultantMultiplier',
      CONVERT(NVARCHAR(2000),DELETED.[CustReimbConsultantMultiplier],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustReimbConsultantMultiplier],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustReimbConsultantMultiplier] Is Null And
				DELETED.[CustReimbConsultantMultiplier] Is Not Null
			) Or
			(
				INSERTED.[CustReimbConsultantMultiplier] Is Not Null And
				DELETED.[CustReimbConsultantMultiplier] Is Null
			) Or
			(
				INSERTED.[CustReimbConsultantMultiplier] !=
				DELETED.[CustReimbConsultantMultiplier]
			)
		) 
		END		
		
      If UPDATE([CustLaborFringeMultiplier])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLaborFringeMultiplier',
      CONVERT(NVARCHAR(2000),DELETED.[CustLaborFringeMultiplier],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLaborFringeMultiplier],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustLaborFringeMultiplier] Is Null And
				DELETED.[CustLaborFringeMultiplier] Is Not Null
			) Or
			(
				INSERTED.[CustLaborFringeMultiplier] Is Not Null And
				DELETED.[CustLaborFringeMultiplier] Is Null
			) Or
			(
				INSERTED.[CustLaborFringeMultiplier] !=
				DELETED.[CustLaborFringeMultiplier]
			)
		) 
		END		
		
      If UPDATE([CustLaborOvhProfitMultiplier])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLaborOvhProfitMultiplier',
      CONVERT(NVARCHAR(2000),DELETED.[CustLaborOvhProfitMultiplier],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLaborOvhProfitMultiplier],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustLaborOvhProfitMultiplier] Is Null And
				DELETED.[CustLaborOvhProfitMultiplier] Is Not Null
			) Or
			(
				INSERTED.[CustLaborOvhProfitMultiplier] Is Not Null And
				DELETED.[CustLaborOvhProfitMultiplier] Is Null
			) Or
			(
				INSERTED.[CustLaborOvhProfitMultiplier] !=
				DELETED.[CustLaborOvhProfitMultiplier]
			)
		) 
		END		
		
      If UPDATE([CustJTDARBalance])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDARBalance',
      CONVERT(NVARCHAR(2000),DELETED.[CustJTDARBalance],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustJTDARBalance],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustJTDARBalance] Is Null And
				DELETED.[CustJTDARBalance] Is Not Null
			) Or
			(
				INSERTED.[CustJTDARBalance] Is Not Null And
				DELETED.[CustJTDARBalance] Is Null
			) Or
			(
				INSERTED.[CustJTDARBalance] !=
				DELETED.[CustJTDARBalance]
			)
		) 
		END		
		
     If UPDATE([CustPracticeAreaLeader])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPracticeAreaLeader',
     CONVERT(NVARCHAR(2000),DELETED.[CustPracticeAreaLeader],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustPracticeAreaLeader],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustPracticeAreaLeader] Is Null And
				DELETED.[CustPracticeAreaLeader] Is Not Null
			) Or
			(
				INSERTED.[CustPracticeAreaLeader] Is Not Null And
				DELETED.[CustPracticeAreaLeader] Is Null
			) Or
			(
				INSERTED.[CustPracticeAreaLeader] !=
				DELETED.[CustPracticeAreaLeader]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustPracticeAreaLeader = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustPracticeAreaLeader = newDesc.Employee
		END		
		
      If UPDATE([CustHistoryBSTSBU])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryBSTSBU',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryBSTSBU],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryBSTSBU],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryBSTSBU] Is Null And
				DELETED.[CustHistoryBSTSBU] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryBSTSBU] Is Not Null And
				DELETED.[CustHistoryBSTSBU] Is Null
			) Or
			(
				INSERTED.[CustHistoryBSTSBU] !=
				DELETED.[CustHistoryBSTSBU]
			)
		) 
		END		
		
      If UPDATE([CustRevenueLabor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRevenueLabor',
      CONVERT(NVARCHAR(2000),DELETED.[CustRevenueLabor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRevenueLabor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustRevenueLabor] Is Null And
				DELETED.[CustRevenueLabor] Is Not Null
			) Or
			(
				INSERTED.[CustRevenueLabor] Is Not Null And
				DELETED.[CustRevenueLabor] Is Null
			) Or
			(
				INSERTED.[CustRevenueLabor] !=
				DELETED.[CustRevenueLabor]
			)
		) 
		END		
		
      If UPDATE([CustRevenueConsultant])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRevenueConsultant',
      CONVERT(NVARCHAR(2000),DELETED.[CustRevenueConsultant],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRevenueConsultant],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustRevenueConsultant] Is Null And
				DELETED.[CustRevenueConsultant] Is Not Null
			) Or
			(
				INSERTED.[CustRevenueConsultant] Is Not Null And
				DELETED.[CustRevenueConsultant] Is Null
			) Or
			(
				INSERTED.[CustRevenueConsultant] !=
				DELETED.[CustRevenueConsultant]
			)
		) 
		END		
		
      If UPDATE([CustRevenueExpense])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRevenueExpense',
      CONVERT(NVARCHAR(2000),DELETED.[CustRevenueExpense],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRevenueExpense],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustRevenueExpense] Is Null And
				DELETED.[CustRevenueExpense] Is Not Null
			) Or
			(
				INSERTED.[CustRevenueExpense] Is Not Null And
				DELETED.[CustRevenueExpense] Is Null
			) Or
			(
				INSERTED.[CustRevenueExpense] !=
				DELETED.[CustRevenueExpense]
			)
		) 
		END		
		
      If UPDATE([CustLastPayment])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLastPayment',
      CONVERT(NVARCHAR(2000),DELETED.[CustLastPayment],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLastPayment],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustLastPayment] Is Null And
				DELETED.[CustLastPayment] Is Not Null
			) Or
			(
				INSERTED.[CustLastPayment] Is Not Null And
				DELETED.[CustLastPayment] Is Null
			) Or
			(
				INSERTED.[CustLastPayment] !=
				DELETED.[CustLastPayment]
			)
		) 
		END		
		
      If UPDATE([CustIncludeInvTemplateFromDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustIncludeInvTemplateFromDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustIncludeInvTemplateFromDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustIncludeInvTemplateFromDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustIncludeInvTemplateFromDate] Is Null And
				DELETED.[CustIncludeInvTemplateFromDate] Is Not Null
			) Or
			(
				INSERTED.[CustIncludeInvTemplateFromDate] Is Not Null And
				DELETED.[CustIncludeInvTemplateFromDate] Is Null
			) Or
			(
				INSERTED.[CustIncludeInvTemplateFromDate] !=
				DELETED.[CustIncludeInvTemplateFromDate]
			)
		) 
		END		
		
      If UPDATE([CustFinalInvPkgIncludeExpenseReport])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeExpenseReport',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalInvPkgIncludeExpenseReport],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalInvPkgIncludeExpenseReport],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalInvPkgIncludeExpenseReport] Is Null And
				DELETED.[CustFinalInvPkgIncludeExpenseReport] Is Not Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeExpenseReport] Is Not Null And
				DELETED.[CustFinalInvPkgIncludeExpenseReport] Is Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeExpenseReport] !=
				DELETED.[CustFinalInvPkgIncludeExpenseReport]
			)
		) 
		END		
		
      If UPDATE([CustFinalInvPkgIncludeUnits])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalInvPkgIncludeUnits',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalInvPkgIncludeUnits],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalInvPkgIncludeUnits],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalInvPkgIncludeUnits] Is Null And
				DELETED.[CustFinalInvPkgIncludeUnits] Is Not Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeUnits] Is Not Null And
				DELETED.[CustFinalInvPkgIncludeUnits] Is Null
			) Or
			(
				INSERTED.[CustFinalInvPkgIncludeUnits] !=
				DELETED.[CustFinalInvPkgIncludeUnits]
			)
		) 
		END		
		
      If UPDATE([CustBillingStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingStatus',
      CONVERT(NVARCHAR(2000),DELETED.[CustBillingStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBillingStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustBillingStatus] Is Null And
				DELETED.[CustBillingStatus] Is Not Null
			) Or
			(
				INSERTED.[CustBillingStatus] Is Not Null And
				DELETED.[CustBillingStatus] Is Null
			) Or
			(
				INSERTED.[CustBillingStatus] !=
				DELETED.[CustBillingStatus]
			)
		) 
		END		
		
     If UPDATE([CustBillingTeamLeader])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingTeamLeader',
     CONVERT(NVARCHAR(2000),DELETED.[CustBillingTeamLeader],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustBillingTeamLeader],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustBillingTeamLeader] Is Null And
				DELETED.[CustBillingTeamLeader] Is Not Null
			) Or
			(
				INSERTED.[CustBillingTeamLeader] Is Not Null And
				DELETED.[CustBillingTeamLeader] Is Null
			) Or
			(
				INSERTED.[CustBillingTeamLeader] !=
				DELETED.[CustBillingTeamLeader]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustBillingTeamLeader = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustBillingTeamLeader = newDesc.Employee
		END		
		
      If UPDATE([CustUnbilledTransactions])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustUnbilledTransactions',
      CONVERT(NVARCHAR(2000),DELETED.[CustUnbilledTransactions],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustUnbilledTransactions],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustUnbilledTransactions] Is Null And
				DELETED.[CustUnbilledTransactions] Is Not Null
			) Or
			(
				INSERTED.[CustUnbilledTransactions] Is Not Null And
				DELETED.[CustUnbilledTransactions] Is Null
			) Or
			(
				INSERTED.[CustUnbilledTransactions] !=
				DELETED.[CustUnbilledTransactions]
			)
		) 
		END		
		
      If UPDATE([CustUnbilledAmountBilling])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustUnbilledAmountBilling',
      CONVERT(NVARCHAR(2000),DELETED.[CustUnbilledAmountBilling],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustUnbilledAmountBilling],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustUnbilledAmountBilling] Is Null And
				DELETED.[CustUnbilledAmountBilling] Is Not Null
			) Or
			(
				INSERTED.[CustUnbilledAmountBilling] Is Not Null And
				DELETED.[CustUnbilledAmountBilling] Is Null
			) Or
			(
				INSERTED.[CustUnbilledAmountBilling] !=
				DELETED.[CustUnbilledAmountBilling]
			)
		) 
		END		
		
      If UPDATE([CustJTDRevenue])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDRevenue',
      CONVERT(NVARCHAR(2000),DELETED.[CustJTDRevenue],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustJTDRevenue],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustJTDRevenue] Is Null And
				DELETED.[CustJTDRevenue] Is Not Null
			) Or
			(
				INSERTED.[CustJTDRevenue] Is Not Null And
				DELETED.[CustJTDRevenue] Is Null
			) Or
			(
				INSERTED.[CustJTDRevenue] !=
				DELETED.[CustJTDRevenue]
			)
		) 
		END		
		
      If UPDATE([CustYTDRevenue])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDRevenue',
      CONVERT(NVARCHAR(2000),DELETED.[CustYTDRevenue],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustYTDRevenue],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustYTDRevenue] Is Null And
				DELETED.[CustYTDRevenue] Is Not Null
			) Or
			(
				INSERTED.[CustYTDRevenue] Is Not Null And
				DELETED.[CustYTDRevenue] Is Null
			) Or
			(
				INSERTED.[CustYTDRevenue] !=
				DELETED.[CustYTDRevenue]
			)
		) 
		END		
		
      If UPDATE([CustJTDBilled])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDBilled',
      CONVERT(NVARCHAR(2000),DELETED.[CustJTDBilled],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustJTDBilled],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustJTDBilled] Is Null And
				DELETED.[CustJTDBilled] Is Not Null
			) Or
			(
				INSERTED.[CustJTDBilled] Is Not Null And
				DELETED.[CustJTDBilled] Is Null
			) Or
			(
				INSERTED.[CustJTDBilled] !=
				DELETED.[CustJTDBilled]
			)
		) 
		END		
		
      If UPDATE([CustYTDBilled])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDBilled',
      CONVERT(NVARCHAR(2000),DELETED.[CustYTDBilled],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustYTDBilled],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustYTDBilled] Is Null And
				DELETED.[CustYTDBilled] Is Not Null
			) Or
			(
				INSERTED.[CustYTDBilled] Is Not Null And
				DELETED.[CustYTDBilled] Is Null
			) Or
			(
				INSERTED.[CustYTDBilled] !=
				DELETED.[CustYTDBilled]
			)
		) 
		END		
		
      If UPDATE([CustJTDReceived])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDReceived',
      CONVERT(NVARCHAR(2000),DELETED.[CustJTDReceived],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustJTDReceived],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustJTDReceived] Is Null And
				DELETED.[CustJTDReceived] Is Not Null
			) Or
			(
				INSERTED.[CustJTDReceived] Is Not Null And
				DELETED.[CustJTDReceived] Is Null
			) Or
			(
				INSERTED.[CustJTDReceived] !=
				DELETED.[CustJTDReceived]
			)
		) 
		END		
		
      If UPDATE([CustYTDReceived])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDReceived',
      CONVERT(NVARCHAR(2000),DELETED.[CustYTDReceived],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustYTDReceived],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustYTDReceived] Is Null And
				DELETED.[CustYTDReceived] Is Not Null
			) Or
			(
				INSERTED.[CustYTDReceived] Is Not Null And
				DELETED.[CustYTDReceived] Is Null
			) Or
			(
				INSERTED.[CustYTDReceived] !=
				DELETED.[CustYTDReceived]
			)
		) 
		END		
		
      If UPDATE([CustJTDWorkinProcess])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDWorkinProcess',
      CONVERT(NVARCHAR(2000),DELETED.[CustJTDWorkinProcess],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustJTDWorkinProcess],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustJTDWorkinProcess] Is Null And
				DELETED.[CustJTDWorkinProcess] Is Not Null
			) Or
			(
				INSERTED.[CustJTDWorkinProcess] Is Not Null And
				DELETED.[CustJTDWorkinProcess] Is Null
			) Or
			(
				INSERTED.[CustJTDWorkinProcess] !=
				DELETED.[CustJTDWorkinProcess]
			)
		) 
		END		
		
      If UPDATE([CustJTDSpentBilling])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDSpentBilling',
      CONVERT(NVARCHAR(2000),DELETED.[CustJTDSpentBilling],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustJTDSpentBilling],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustJTDSpentBilling] Is Null And
				DELETED.[CustJTDSpentBilling] Is Not Null
			) Or
			(
				INSERTED.[CustJTDSpentBilling] Is Not Null And
				DELETED.[CustJTDSpentBilling] Is Null
			) Or
			(
				INSERTED.[CustJTDSpentBilling] !=
				DELETED.[CustJTDSpentBilling]
			)
		) 
		END		
		
      If UPDATE([CustYTDSpentBilling])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDSpentBilling',
      CONVERT(NVARCHAR(2000),DELETED.[CustYTDSpentBilling],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustYTDSpentBilling],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustYTDSpentBilling] Is Null And
				DELETED.[CustYTDSpentBilling] Is Not Null
			) Or
			(
				INSERTED.[CustYTDSpentBilling] Is Not Null And
				DELETED.[CustYTDSpentBilling] Is Null
			) Or
			(
				INSERTED.[CustYTDSpentBilling] !=
				DELETED.[CustYTDSpentBilling]
			)
		) 
		END		
		
      If UPDATE([CustJTDVariance])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDVariance',
      CONVERT(NVARCHAR(2000),DELETED.[CustJTDVariance],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustJTDVariance],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustJTDVariance] Is Null And
				DELETED.[CustJTDVariance] Is Not Null
			) Or
			(
				INSERTED.[CustJTDVariance] Is Not Null And
				DELETED.[CustJTDVariance] Is Null
			) Or
			(
				INSERTED.[CustJTDVariance] !=
				DELETED.[CustJTDVariance]
			)
		) 
		END		
		
      If UPDATE([CustYTDVariance])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDVariance',
      CONVERT(NVARCHAR(2000),DELETED.[CustYTDVariance],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustYTDVariance],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustYTDVariance] Is Null And
				DELETED.[CustYTDVariance] Is Not Null
			) Or
			(
				INSERTED.[CustYTDVariance] Is Not Null And
				DELETED.[CustYTDVariance] Is Null
			) Or
			(
				INSERTED.[CustYTDVariance] !=
				DELETED.[CustYTDVariance]
			)
		) 
		END		
		
      If UPDATE([CustJTDGoalMultiplier])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDGoalMultiplier',
      CONVERT(NVARCHAR(2000),DELETED.[CustJTDGoalMultiplier],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustJTDGoalMultiplier],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustJTDGoalMultiplier] Is Null And
				DELETED.[CustJTDGoalMultiplier] Is Not Null
			) Or
			(
				INSERTED.[CustJTDGoalMultiplier] Is Not Null And
				DELETED.[CustJTDGoalMultiplier] Is Null
			) Or
			(
				INSERTED.[CustJTDGoalMultiplier] !=
				DELETED.[CustJTDGoalMultiplier]
			)
		) 
		END		
		
      If UPDATE([CustYTDGoalMultiplier])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDGoalMultiplier',
      CONVERT(NVARCHAR(2000),DELETED.[CustYTDGoalMultiplier],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustYTDGoalMultiplier],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustYTDGoalMultiplier] Is Null And
				DELETED.[CustYTDGoalMultiplier] Is Not Null
			) Or
			(
				INSERTED.[CustYTDGoalMultiplier] Is Not Null And
				DELETED.[CustYTDGoalMultiplier] Is Null
			) Or
			(
				INSERTED.[CustYTDGoalMultiplier] !=
				DELETED.[CustYTDGoalMultiplier]
			)
		) 
		END		
		
      If UPDATE([CustJTDEffectiveMultiplier])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDEffectiveMultiplier',
      CONVERT(NVARCHAR(2000),DELETED.[CustJTDEffectiveMultiplier],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustJTDEffectiveMultiplier],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustJTDEffectiveMultiplier] Is Null And
				DELETED.[CustJTDEffectiveMultiplier] Is Not Null
			) Or
			(
				INSERTED.[CustJTDEffectiveMultiplier] Is Not Null And
				DELETED.[CustJTDEffectiveMultiplier] Is Null
			) Or
			(
				INSERTED.[CustJTDEffectiveMultiplier] !=
				DELETED.[CustJTDEffectiveMultiplier]
			)
		) 
		END		
		
      If UPDATE([CustYTDEffectiveMultiplier])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDEffectiveMultiplier',
      CONVERT(NVARCHAR(2000),DELETED.[CustYTDEffectiveMultiplier],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustYTDEffectiveMultiplier],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustYTDEffectiveMultiplier] Is Null And
				DELETED.[CustYTDEffectiveMultiplier] Is Not Null
			) Or
			(
				INSERTED.[CustYTDEffectiveMultiplier] Is Not Null And
				DELETED.[CustYTDEffectiveMultiplier] Is Null
			) Or
			(
				INSERTED.[CustYTDEffectiveMultiplier] !=
				DELETED.[CustYTDEffectiveMultiplier]
			)
		) 
		END		
		
      If UPDATE([CustJTDProfit])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDProfit',
      CONVERT(NVARCHAR(2000),DELETED.[CustJTDProfit],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustJTDProfit],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustJTDProfit] Is Null And
				DELETED.[CustJTDProfit] Is Not Null
			) Or
			(
				INSERTED.[CustJTDProfit] Is Not Null And
				DELETED.[CustJTDProfit] Is Null
			) Or
			(
				INSERTED.[CustJTDProfit] !=
				DELETED.[CustJTDProfit]
			)
		) 
		END		
		
      If UPDATE([CustYTDProfit])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDProfit',
      CONVERT(NVARCHAR(2000),DELETED.[CustYTDProfit],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustYTDProfit],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustYTDProfit] Is Null And
				DELETED.[CustYTDProfit] Is Not Null
			) Or
			(
				INSERTED.[CustYTDProfit] Is Not Null And
				DELETED.[CustYTDProfit] Is Null
			) Or
			(
				INSERTED.[CustYTDProfit] !=
				DELETED.[CustYTDProfit]
			)
		) 
		END		
		
      If UPDATE([CustJTDProfitPercent])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDProfitPercent',
      CONVERT(NVARCHAR(2000),DELETED.[CustJTDProfitPercent],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustJTDProfitPercent],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustJTDProfitPercent] Is Null And
				DELETED.[CustJTDProfitPercent] Is Not Null
			) Or
			(
				INSERTED.[CustJTDProfitPercent] Is Not Null And
				DELETED.[CustJTDProfitPercent] Is Null
			) Or
			(
				INSERTED.[CustJTDProfitPercent] !=
				DELETED.[CustJTDProfitPercent]
			)
		) 
		END		
		
      If UPDATE([CustYTDProfitPercent])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustYTDProfitPercent',
      CONVERT(NVARCHAR(2000),DELETED.[CustYTDProfitPercent],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustYTDProfitPercent],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustYTDProfitPercent] Is Null And
				DELETED.[CustYTDProfitPercent] Is Not Null
			) Or
			(
				INSERTED.[CustYTDProfitPercent] Is Not Null And
				DELETED.[CustYTDProfitPercent] Is Null
			) Or
			(
				INSERTED.[CustYTDProfitPercent] !=
				DELETED.[CustYTDProfitPercent]
			)
		) 
		END		
		
      If UPDATE([CustJTDBilledTax])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustJTDBilledTax',
      CONVERT(NVARCHAR(2000),DELETED.[CustJTDBilledTax],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustJTDBilledTax],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustJTDBilledTax] Is Null And
				DELETED.[CustJTDBilledTax] Is Not Null
			) Or
			(
				INSERTED.[CustJTDBilledTax] Is Not Null And
				DELETED.[CustJTDBilledTax] Is Null
			) Or
			(
				INSERTED.[CustJTDBilledTax] !=
				DELETED.[CustJTDBilledTax]
			)
		) 
		END		
		
      If UPDATE([CustLastPaymentComment])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLastPaymentComment',
      CONVERT(NVARCHAR(2000),DELETED.[CustLastPaymentComment],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLastPaymentComment],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustLastPaymentComment] Is Null And
				DELETED.[CustLastPaymentComment] Is Not Null
			) Or
			(
				INSERTED.[CustLastPaymentComment] Is Not Null And
				DELETED.[CustLastPaymentComment] Is Null
			) Or
			(
				INSERTED.[CustLastPaymentComment] !=
				DELETED.[CustLastPaymentComment]
			)
		) 
		END		
		
      If UPDATE([CustNearmapUsage])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustNearmapUsage',
      CONVERT(NVARCHAR(2000),DELETED.[CustNearmapUsage],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustNearmapUsage],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustNearmapUsage] Is Null And
				DELETED.[CustNearmapUsage] Is Not Null
			) Or
			(
				INSERTED.[CustNearmapUsage] Is Not Null And
				DELETED.[CustNearmapUsage] Is Null
			) Or
			(
				INSERTED.[CustNearmapUsage] !=
				DELETED.[CustNearmapUsage]
			)
		) 
		END		
		
      If UPDATE([CustEACInputLevel])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEACInputLevel',
      CONVERT(NVARCHAR(2000),DELETED.[CustEACInputLevel],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEACInputLevel],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEACInputLevel] Is Null And
				DELETED.[CustEACInputLevel] Is Not Null
			) Or
			(
				INSERTED.[CustEACInputLevel] Is Not Null And
				DELETED.[CustEACInputLevel] Is Null
			) Or
			(
				INSERTED.[CustEACInputLevel] !=
				DELETED.[CustEACInputLevel]
			)
		) 
		END		
		
      If UPDATE([CustTeamPracticeArea])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustTeamPracticeArea',
      CONVERT(NVARCHAR(2000),DELETED.[CustTeamPracticeArea],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTeamPracticeArea],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustTeamPracticeArea] Is Null And
				DELETED.[CustTeamPracticeArea] Is Not Null
			) Or
			(
				INSERTED.[CustTeamPracticeArea] Is Not Null And
				DELETED.[CustTeamPracticeArea] Is Null
			) Or
			(
				INSERTED.[CustTeamPracticeArea] !=
				DELETED.[CustTeamPracticeArea]
			)
		) 
		END		
		
      If UPDATE([CustNearmapLastCharge])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustNearmapLastCharge',
      CONVERT(NVARCHAR(2000),DELETED.[CustNearmapLastCharge],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustNearmapLastCharge],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustNearmapLastCharge] Is Null And
				DELETED.[CustNearmapLastCharge] Is Not Null
			) Or
			(
				INSERTED.[CustNearmapLastCharge] Is Not Null And
				DELETED.[CustNearmapLastCharge] Is Null
			) Or
			(
				INSERTED.[CustNearmapLastCharge] !=
				DELETED.[CustNearmapLastCharge]
			)
		) 
		END		
		
      If UPDATE([CustLastChargeDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLastChargeDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustLastChargeDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLastChargeDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustLastChargeDate] Is Null And
				DELETED.[CustLastChargeDate] Is Not Null
			) Or
			(
				INSERTED.[CustLastChargeDate] Is Not Null And
				DELETED.[CustLastChargeDate] Is Null
			) Or
			(
				INSERTED.[CustLastChargeDate] !=
				DELETED.[CustLastChargeDate]
			)
		) 
		END		
		
      If UPDATE([CustReimbMileageMultiplier])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustReimbMileageMultiplier',
      CONVERT(NVARCHAR(2000),DELETED.[CustReimbMileageMultiplier],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustReimbMileageMultiplier],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustReimbMileageMultiplier] Is Null And
				DELETED.[CustReimbMileageMultiplier] Is Not Null
			) Or
			(
				INSERTED.[CustReimbMileageMultiplier] Is Not Null And
				DELETED.[CustReimbMileageMultiplier] Is Null
			) Or
			(
				INSERTED.[CustReimbMileageMultiplier] !=
				DELETED.[CustReimbMileageMultiplier]
			)
		) 
		END		
		
      If UPDATE([CustUnitTableAllLevels])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustUnitTableAllLevels',
      CONVERT(NVARCHAR(2000),DELETED.[CustUnitTableAllLevels],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustUnitTableAllLevels],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustUnitTableAllLevels] Is Null And
				DELETED.[CustUnitTableAllLevels] Is Not Null
			) Or
			(
				INSERTED.[CustUnitTableAllLevels] Is Not Null And
				DELETED.[CustUnitTableAllLevels] Is Null
			) Or
			(
				INSERTED.[CustUnitTableAllLevels] !=
				DELETED.[CustUnitTableAllLevels]
			)
		) 
		END		
		
      If UPDATE([CustEffortatCompletion])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEffortatCompletion',
      CONVERT(NVARCHAR(2000),DELETED.[CustEffortatCompletion],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEffortatCompletion],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEffortatCompletion] Is Null And
				DELETED.[CustEffortatCompletion] Is Not Null
			) Or
			(
				INSERTED.[CustEffortatCompletion] Is Not Null And
				DELETED.[CustEffortatCompletion] Is Null
			) Or
			(
				INSERTED.[CustEffortatCompletion] !=
				DELETED.[CustEffortatCompletion]
			)
		) 
		END		
		
      If UPDATE([CustEfforttoComplete])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEfforttoComplete',
      CONVERT(NVARCHAR(2000),DELETED.[CustEfforttoComplete],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEfforttoComplete],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEfforttoComplete] Is Null And
				DELETED.[CustEfforttoComplete] Is Not Null
			) Or
			(
				INSERTED.[CustEfforttoComplete] Is Not Null And
				DELETED.[CustEfforttoComplete] Is Null
			) Or
			(
				INSERTED.[CustEfforttoComplete] !=
				DELETED.[CustEfforttoComplete]
			)
		) 
		END		
		
      If UPDATE([CustWriteoffAdjmt])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustWriteoffAdjmt',
      CONVERT(NVARCHAR(2000),DELETED.[CustWriteoffAdjmt],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustWriteoffAdjmt],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustWriteoffAdjmt] Is Null And
				DELETED.[CustWriteoffAdjmt] Is Not Null
			) Or
			(
				INSERTED.[CustWriteoffAdjmt] Is Not Null And
				DELETED.[CustWriteoffAdjmt] Is Null
			) Or
			(
				INSERTED.[CustWriteoffAdjmt] !=
				DELETED.[CustWriteoffAdjmt]
			)
		) 
		END		
		
      If UPDATE([CustFeeTypeRollup])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFeeTypeRollup',
      CONVERT(NVARCHAR(2000),DELETED.[CustFeeTypeRollup],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFeeTypeRollup],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFeeTypeRollup] Is Null And
				DELETED.[CustFeeTypeRollup] Is Not Null
			) Or
			(
				INSERTED.[CustFeeTypeRollup] Is Not Null And
				DELETED.[CustFeeTypeRollup] Is Null
			) Or
			(
				INSERTED.[CustFeeTypeRollup] !=
				DELETED.[CustFeeTypeRollup]
			)
		) 
		END		
		
      If UPDATE([CustPercentComplete])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPercentComplete',
      CONVERT(NVARCHAR(2000),DELETED.[CustPercentComplete],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPercentComplete],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustPercentComplete] Is Null And
				DELETED.[CustPercentComplete] Is Not Null
			) Or
			(
				INSERTED.[CustPercentComplete] Is Not Null And
				DELETED.[CustPercentComplete] Is Null
			) Or
			(
				INSERTED.[CustPercentComplete] !=
				DELETED.[CustPercentComplete]
			)
		) 
		END		
		
     If UPDATE([CustHistoryTrackHTTProjectManager])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackHTTProjectManager',
     CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackHTTProjectManager],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackHTTProjectManager],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryTrackHTTProjectManager] Is Null And
				DELETED.[CustHistoryTrackHTTProjectManager] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryTrackHTTProjectManager] Is Not Null And
				DELETED.[CustHistoryTrackHTTProjectManager] Is Null
			) Or
			(
				INSERTED.[CustHistoryTrackHTTProjectManager] !=
				DELETED.[CustHistoryTrackHTTProjectManager]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryTrackHTTProjectManager = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryTrackHTTProjectManager = newDesc.Employee
		END		
		
     If UPDATE([CustHistoryTrackHTTDrafter])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackHTTDrafter',
     CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackHTTDrafter],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackHTTDrafter],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryTrackHTTDrafter] Is Null And
				DELETED.[CustHistoryTrackHTTDrafter] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryTrackHTTDrafter] Is Not Null And
				DELETED.[CustHistoryTrackHTTDrafter] Is Null
			) Or
			(
				INSERTED.[CustHistoryTrackHTTDrafter] !=
				DELETED.[CustHistoryTrackHTTDrafter]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryTrackHTTDrafter = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryTrackHTTDrafter = newDesc.Employee
		END		
		
     If UPDATE([CustHistoryTrackHalffProject])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackHalffProject',
     CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackHalffProject],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackHalffProject],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryTrackHalffProject] Is Null And
				DELETED.[CustHistoryTrackHalffProject] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryTrackHalffProject] Is Not Null And
				DELETED.[CustHistoryTrackHalffProject] Is Null
			) Or
			(
				INSERTED.[CustHistoryTrackHalffProject] !=
				DELETED.[CustHistoryTrackHalffProject]
			)
		) left join PR as oldDesc  on DELETED.CustHistoryTrackHalffProject = oldDesc.WBS1  left join  PR as newDesc  on INSERTED.CustHistoryTrackHalffProject = newDesc.WBS1
		END		
		
      If UPDATE([CustHistoryTrackHalffDiscipline])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackHalffDiscipline',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackHalffDiscipline],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackHalffDiscipline],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryTrackHalffDiscipline] Is Null And
				DELETED.[CustHistoryTrackHalffDiscipline] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryTrackHalffDiscipline] Is Not Null And
				DELETED.[CustHistoryTrackHalffDiscipline] Is Null
			) Or
			(
				INSERTED.[CustHistoryTrackHalffDiscipline] !=
				DELETED.[CustHistoryTrackHalffDiscipline]
			)
		) 
		END		
		
     If UPDATE([CustHistoryTrackDisciplineProjectManager])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackDisciplineProjectManager',
     CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackDisciplineProjectManager],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackDisciplineProjectManager],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryTrackDisciplineProjectManager] Is Null And
				DELETED.[CustHistoryTrackDisciplineProjectManager] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryTrackDisciplineProjectManager] Is Not Null And
				DELETED.[CustHistoryTrackDisciplineProjectManager] Is Null
			) Or
			(
				INSERTED.[CustHistoryTrackDisciplineProjectManager] !=
				DELETED.[CustHistoryTrackDisciplineProjectManager]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryTrackDisciplineProjectManager = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryTrackDisciplineProjectManager = newDesc.Employee
		END		
		
      If UPDATE([CustHistoryTrackClientProjectNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackClientProjectNumber',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackClientProjectNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackClientProjectNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryTrackClientProjectNumber] Is Null And
				DELETED.[CustHistoryTrackClientProjectNumber] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryTrackClientProjectNumber] Is Not Null And
				DELETED.[CustHistoryTrackClientProjectNumber] Is Null
			) Or
			(
				INSERTED.[CustHistoryTrackClientProjectNumber] !=
				DELETED.[CustHistoryTrackClientProjectNumber]
			)
		) 
		END		
		
      If UPDATE([CustHistoryTrackClientConfigurationNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackClientConfigurationNumber',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackClientConfigurationNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackClientConfigurationNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryTrackClientConfigurationNumber] Is Null And
				DELETED.[CustHistoryTrackClientConfigurationNumber] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryTrackClientConfigurationNumber] Is Not Null And
				DELETED.[CustHistoryTrackClientConfigurationNumber] Is Null
			) Or
			(
				INSERTED.[CustHistoryTrackClientConfigurationNumber] !=
				DELETED.[CustHistoryTrackClientConfigurationNumber]
			)
		) 
		END		
		
      If UPDATE([CustHistoryTrackCapitalProject])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackCapitalProject',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackCapitalProject],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackCapitalProject],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryTrackCapitalProject] Is Null And
				DELETED.[CustHistoryTrackCapitalProject] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryTrackCapitalProject] Is Not Null And
				DELETED.[CustHistoryTrackCapitalProject] Is Null
			) Or
			(
				INSERTED.[CustHistoryTrackCapitalProject] !=
				DELETED.[CustHistoryTrackCapitalProject]
			)
		) 
		END		
		
      If UPDATE([CustHistoryTrackClientOMNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackClientOMNumber',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackClientOMNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackClientOMNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryTrackClientOMNumber] Is Null And
				DELETED.[CustHistoryTrackClientOMNumber] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryTrackClientOMNumber] Is Not Null And
				DELETED.[CustHistoryTrackClientOMNumber] Is Null
			) Or
			(
				INSERTED.[CustHistoryTrackClientOMNumber] !=
				DELETED.[CustHistoryTrackClientOMNumber]
			)
		) 
		END		
		
      If UPDATE([CustHistoryTrackClientCostCenter])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackClientCostCenter',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackClientCostCenter],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackClientCostCenter],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryTrackClientCostCenter] Is Null And
				DELETED.[CustHistoryTrackClientCostCenter] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryTrackClientCostCenter] Is Not Null And
				DELETED.[CustHistoryTrackClientCostCenter] Is Null
			) Or
			(
				INSERTED.[CustHistoryTrackClientCostCenter] !=
				DELETED.[CustHistoryTrackClientCostCenter]
			)
		) 
		END		
		
     If UPDATE([CustHistoryTrackClientProjectManager])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackClientProjectManager',
     CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackClientProjectManager],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackClientProjectManager],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryTrackClientProjectManager] Is Null And
				DELETED.[CustHistoryTrackClientProjectManager] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryTrackClientProjectManager] Is Not Null And
				DELETED.[CustHistoryTrackClientProjectManager] Is Null
			) Or
			(
				INSERTED.[CustHistoryTrackClientProjectManager] !=
				DELETED.[CustHistoryTrackClientProjectManager]
			)
		) left join Contacts as oldDesc  on DELETED.CustHistoryTrackClientProjectManager = oldDesc.ContactID  left join  Contacts as newDesc  on INSERTED.CustHistoryTrackClientProjectManager = newDesc.ContactID
		END		
		
      If UPDATE([CustHistoryTrackClientDeadline])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackClientDeadline',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackClientDeadline],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackClientDeadline],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryTrackClientDeadline] Is Null And
				DELETED.[CustHistoryTrackClientDeadline] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryTrackClientDeadline] Is Not Null And
				DELETED.[CustHistoryTrackClientDeadline] Is Null
			) Or
			(
				INSERTED.[CustHistoryTrackClientDeadline] !=
				DELETED.[CustHistoryTrackClientDeadline]
			)
		) 
		END		
		
      If UPDATE([CustHistoryTrackInternalDeadline])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackInternalDeadline',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackInternalDeadline],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackInternalDeadline],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryTrackInternalDeadline] Is Null And
				DELETED.[CustHistoryTrackInternalDeadline] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryTrackInternalDeadline] Is Not Null And
				DELETED.[CustHistoryTrackInternalDeadline] Is Null
			) Or
			(
				INSERTED.[CustHistoryTrackInternalDeadline] !=
				DELETED.[CustHistoryTrackInternalDeadline]
			)
		) 
		END		
		
      If UPDATE([CustHistoryTrackStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTrackStatus',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTrackStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTrackStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryTrackStatus] Is Null And
				DELETED.[CustHistoryTrackStatus] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryTrackStatus] Is Not Null And
				DELETED.[CustHistoryTrackStatus] Is Null
			) Or
			(
				INSERTED.[CustHistoryTrackStatus] !=
				DELETED.[CustHistoryTrackStatus]
			)
		) 
		END		
		
     If UPDATE([CustQAManager])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAManager',
     CONVERT(NVARCHAR(2000),DELETED.[CustQAManager],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustQAManager],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustQAManager] Is Null And
				DELETED.[CustQAManager] Is Not Null
			) Or
			(
				INSERTED.[CustQAManager] Is Not Null And
				DELETED.[CustQAManager] Is Null
			) Or
			(
				INSERTED.[CustQAManager] !=
				DELETED.[CustQAManager]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustQAManager = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustQAManager = newDesc.Employee
		END		
		
      If UPDATE([CustQAReview1Goal])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview1Goal',
      CONVERT(NVARCHAR(2000),DELETED.[CustQAReview1Goal],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustQAReview1Goal],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustQAReview1Goal] Is Null And
				DELETED.[CustQAReview1Goal] Is Not Null
			) Or
			(
				INSERTED.[CustQAReview1Goal] Is Not Null And
				DELETED.[CustQAReview1Goal] Is Null
			) Or
			(
				INSERTED.[CustQAReview1Goal] !=
				DELETED.[CustQAReview1Goal]
			)
		) 
		END		
		
      If UPDATE([CustQAReview2Goal])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview2Goal',
      CONVERT(NVARCHAR(2000),DELETED.[CustQAReview2Goal],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustQAReview2Goal],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustQAReview2Goal] Is Null And
				DELETED.[CustQAReview2Goal] Is Not Null
			) Or
			(
				INSERTED.[CustQAReview2Goal] Is Not Null And
				DELETED.[CustQAReview2Goal] Is Null
			) Or
			(
				INSERTED.[CustQAReview2Goal] !=
				DELETED.[CustQAReview2Goal]
			)
		) 
		END		
		
      If UPDATE([CustQAReview3Goal])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview3Goal',
      CONVERT(NVARCHAR(2000),DELETED.[CustQAReview3Goal],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustQAReview3Goal],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustQAReview3Goal] Is Null And
				DELETED.[CustQAReview3Goal] Is Not Null
			) Or
			(
				INSERTED.[CustQAReview3Goal] Is Not Null And
				DELETED.[CustQAReview3Goal] Is Null
			) Or
			(
				INSERTED.[CustQAReview3Goal] !=
				DELETED.[CustQAReview3Goal]
			)
		) 
		END		
		
      If UPDATE([CustQAReview1Date])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview1Date',
      CONVERT(NVARCHAR(2000),DELETED.[CustQAReview1Date],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustQAReview1Date],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustQAReview1Date] Is Null And
				DELETED.[CustQAReview1Date] Is Not Null
			) Or
			(
				INSERTED.[CustQAReview1Date] Is Not Null And
				DELETED.[CustQAReview1Date] Is Null
			) Or
			(
				INSERTED.[CustQAReview1Date] !=
				DELETED.[CustQAReview1Date]
			)
		) 
		END		
		
      If UPDATE([CustQAReview2Date])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview2Date',
      CONVERT(NVARCHAR(2000),DELETED.[CustQAReview2Date],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustQAReview2Date],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustQAReview2Date] Is Null And
				DELETED.[CustQAReview2Date] Is Not Null
			) Or
			(
				INSERTED.[CustQAReview2Date] Is Not Null And
				DELETED.[CustQAReview2Date] Is Null
			) Or
			(
				INSERTED.[CustQAReview2Date] !=
				DELETED.[CustQAReview2Date]
			)
		) 
		END		
		
      If UPDATE([CustQAReview3Date])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview3Date',
      CONVERT(NVARCHAR(2000),DELETED.[CustQAReview3Date],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustQAReview3Date],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustQAReview3Date] Is Null And
				DELETED.[CustQAReview3Date] Is Not Null
			) Or
			(
				INSERTED.[CustQAReview3Date] Is Not Null And
				DELETED.[CustQAReview3Date] Is Null
			) Or
			(
				INSERTED.[CustQAReview3Date] !=
				DELETED.[CustQAReview3Date]
			)
		) 
		END		
		
      If UPDATE([CustQAReview1PercentComplete])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview1PercentComplete',
      CONVERT(NVARCHAR(2000),DELETED.[CustQAReview1PercentComplete],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustQAReview1PercentComplete],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustQAReview1PercentComplete] Is Null And
				DELETED.[CustQAReview1PercentComplete] Is Not Null
			) Or
			(
				INSERTED.[CustQAReview1PercentComplete] Is Not Null And
				DELETED.[CustQAReview1PercentComplete] Is Null
			) Or
			(
				INSERTED.[CustQAReview1PercentComplete] !=
				DELETED.[CustQAReview1PercentComplete]
			)
		) 
		END		
		
      If UPDATE([CustQAReview2PercentComplete])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview2PercentComplete',
      CONVERT(NVARCHAR(2000),DELETED.[CustQAReview2PercentComplete],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustQAReview2PercentComplete],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustQAReview2PercentComplete] Is Null And
				DELETED.[CustQAReview2PercentComplete] Is Not Null
			) Or
			(
				INSERTED.[CustQAReview2PercentComplete] Is Not Null And
				DELETED.[CustQAReview2PercentComplete] Is Null
			) Or
			(
				INSERTED.[CustQAReview2PercentComplete] !=
				DELETED.[CustQAReview2PercentComplete]
			)
		) 
		END		
		
      If UPDATE([CustQAReview3PercentComplete])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReview3PercentComplete',
      CONVERT(NVARCHAR(2000),DELETED.[CustQAReview3PercentComplete],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustQAReview3PercentComplete],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustQAReview3PercentComplete] Is Null And
				DELETED.[CustQAReview3PercentComplete] Is Not Null
			) Or
			(
				INSERTED.[CustQAReview3PercentComplete] Is Not Null And
				DELETED.[CustQAReview3PercentComplete] Is Null
			) Or
			(
				INSERTED.[CustQAReview3PercentComplete] !=
				DELETED.[CustQAReview3PercentComplete]
			)
		) 
		END		
		
      If UPDATE([CustQAWarning])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAWarning',
      CONVERT(NVARCHAR(2000),DELETED.[CustQAWarning],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustQAWarning],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustQAWarning] Is Null And
				DELETED.[CustQAWarning] Is Not Null
			) Or
			(
				INSERTED.[CustQAWarning] Is Not Null And
				DELETED.[CustQAWarning] Is Null
			) Or
			(
				INSERTED.[CustQAWarning] !=
				DELETED.[CustQAWarning]
			)
		) 
		END		
		
      If UPDATE([CustQAReviewRequired])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQAReviewRequired',
      CONVERT(NVARCHAR(2000),DELETED.[CustQAReviewRequired],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustQAReviewRequired],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustQAReviewRequired] Is Null And
				DELETED.[CustQAReviewRequired] Is Not Null
			) Or
			(
				INSERTED.[CustQAReviewRequired] Is Not Null And
				DELETED.[CustQAReviewRequired] Is Null
			) Or
			(
				INSERTED.[CustQAReviewRequired] !=
				DELETED.[CustQAReviewRequired]
			)
		) 
		END		
		
      If UPDATE([CustLastWeek1SpentBilling])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLastWeek1SpentBilling',
      CONVERT(NVARCHAR(2000),DELETED.[CustLastWeek1SpentBilling],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLastWeek1SpentBilling],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustLastWeek1SpentBilling] Is Null And
				DELETED.[CustLastWeek1SpentBilling] Is Not Null
			) Or
			(
				INSERTED.[CustLastWeek1SpentBilling] Is Not Null And
				DELETED.[CustLastWeek1SpentBilling] Is Null
			) Or
			(
				INSERTED.[CustLastWeek1SpentBilling] !=
				DELETED.[CustLastWeek1SpentBilling]
			)
		) 
		END		
		
      If UPDATE([CustLastWeek2SpentBilling])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLastWeek2SpentBilling',
      CONVERT(NVARCHAR(2000),DELETED.[CustLastWeek2SpentBilling],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLastWeek2SpentBilling],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustLastWeek2SpentBilling] Is Null And
				DELETED.[CustLastWeek2SpentBilling] Is Not Null
			) Or
			(
				INSERTED.[CustLastWeek2SpentBilling] Is Not Null And
				DELETED.[CustLastWeek2SpentBilling] Is Null
			) Or
			(
				INSERTED.[CustLastWeek2SpentBilling] !=
				DELETED.[CustLastWeek2SpentBilling]
			)
		) 
		END		
		
     If UPDATE([CustClientProjectContact])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustClientProjectContact',
     CONVERT(NVARCHAR(2000),DELETED.[CustClientProjectContact],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustClientProjectContact],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustClientProjectContact] Is Null And
				DELETED.[CustClientProjectContact] Is Not Null
			) Or
			(
				INSERTED.[CustClientProjectContact] Is Not Null And
				DELETED.[CustClientProjectContact] Is Null
			) Or
			(
				INSERTED.[CustClientProjectContact] !=
				DELETED.[CustClientProjectContact]
			)
		) left join Contacts as oldDesc  on DELETED.CustClientProjectContact = oldDesc.ContactID  left join  Contacts as newDesc  on INSERTED.CustClientProjectContact = newDesc.ContactID
		END		
		
      If UPDATE([CustLegalInvolvement])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLegalInvolvement',
      CONVERT(NVARCHAR(2000),DELETED.[CustLegalInvolvement],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLegalInvolvement],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustLegalInvolvement] Is Null And
				DELETED.[CustLegalInvolvement] Is Not Null
			) Or
			(
				INSERTED.[CustLegalInvolvement] Is Not Null And
				DELETED.[CustLegalInvolvement] Is Null
			) Or
			(
				INSERTED.[CustLegalInvolvement] !=
				DELETED.[CustLegalInvolvement]
			)
		) 
		END		
		
     If UPDATE([CustQADelegate])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustQADelegate',
     CONVERT(NVARCHAR(2000),DELETED.[CustQADelegate],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustQADelegate],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustQADelegate] Is Null And
				DELETED.[CustQADelegate] Is Not Null
			) Or
			(
				INSERTED.[CustQADelegate] Is Not Null And
				DELETED.[CustQADelegate] Is Null
			) Or
			(
				INSERTED.[CustQADelegate] !=
				DELETED.[CustQADelegate]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustQADelegate = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustQADelegate = newDesc.Employee
		END		
		
      If UPDATE([CustLocationURL])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLocationURL',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustFinalPackageEmail])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalPackageEmail',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalPackageEmail],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalPackageEmail],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalPackageEmail] Is Null And
				DELETED.[CustFinalPackageEmail] Is Not Null
			) Or
			(
				INSERTED.[CustFinalPackageEmail] Is Not Null And
				DELETED.[CustFinalPackageEmail] Is Null
			) Or
			(
				INSERTED.[CustFinalPackageEmail] !=
				DELETED.[CustFinalPackageEmail]
			)
		) 
		END		
		
      If UPDATE([CustFinalPackageCCEmail])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalPackageCCEmail',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalPackageCCEmail],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalPackageCCEmail],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalPackageCCEmail] Is Null And
				DELETED.[CustFinalPackageCCEmail] Is Not Null
			) Or
			(
				INSERTED.[CustFinalPackageCCEmail] Is Not Null And
				DELETED.[CustFinalPackageCCEmail] Is Null
			) Or
			(
				INSERTED.[CustFinalPackageCCEmail] !=
				DELETED.[CustFinalPackageCCEmail]
			)
		) 
		END		
		
      If UPDATE([CustFinalPackageEmailInvRev])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinalPackageEmailInvRev',
      CONVERT(NVARCHAR(2000),DELETED.[CustFinalPackageEmailInvRev],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFinalPackageEmailInvRev],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFinalPackageEmailInvRev] Is Null And
				DELETED.[CustFinalPackageEmailInvRev] Is Not Null
			) Or
			(
				INSERTED.[CustFinalPackageEmailInvRev] Is Not Null And
				DELETED.[CustFinalPackageEmailInvRev] Is Null
			) Or
			(
				INSERTED.[CustFinalPackageEmailInvRev] !=
				DELETED.[CustFinalPackageEmailInvRev]
			)
		) 
		END		
		
      If UPDATE([CustPerDiemReimbursed])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPerDiemReimbursed',
      CONVERT(NVARCHAR(2000),DELETED.[CustPerDiemReimbursed],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPerDiemReimbursed],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustPerDiemReimbursed] Is Null And
				DELETED.[CustPerDiemReimbursed] Is Not Null
			) Or
			(
				INSERTED.[CustPerDiemReimbursed] Is Not Null And
				DELETED.[CustPerDiemReimbursed] Is Null
			) Or
			(
				INSERTED.[CustPerDiemReimbursed] !=
				DELETED.[CustPerDiemReimbursed]
			)
		) 
		END		
		
      If UPDATE([CustMasterContractType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMasterContractType',
      CONVERT(NVARCHAR(2000),DELETED.[CustMasterContractType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustMasterContractType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMasterContractType] Is Null And
				DELETED.[CustMasterContractType] Is Not Null
			) Or
			(
				INSERTED.[CustMasterContractType] Is Not Null And
				DELETED.[CustMasterContractType] Is Null
			) Or
			(
				INSERTED.[CustMasterContractType] !=
				DELETED.[CustMasterContractType]
			)
		) 
		END		
		
      If UPDATE([CustOriginalFeeDirLab])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOriginalFeeDirLab',
      CONVERT(NVARCHAR(2000),DELETED.[CustOriginalFeeDirLab],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustOriginalFeeDirLab],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustOriginalFeeDirLab] Is Null And
				DELETED.[CustOriginalFeeDirLab] Is Not Null
			) Or
			(
				INSERTED.[CustOriginalFeeDirLab] Is Not Null And
				DELETED.[CustOriginalFeeDirLab] Is Null
			) Or
			(
				INSERTED.[CustOriginalFeeDirLab] !=
				DELETED.[CustOriginalFeeDirLab]
			)
		) 
		END		
		
      If UPDATE([CustOriginalFeeDirExp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOriginalFeeDirExp',
      CONVERT(NVARCHAR(2000),DELETED.[CustOriginalFeeDirExp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustOriginalFeeDirExp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustOriginalFeeDirExp] Is Null And
				DELETED.[CustOriginalFeeDirExp] Is Not Null
			) Or
			(
				INSERTED.[CustOriginalFeeDirExp] Is Not Null And
				DELETED.[CustOriginalFeeDirExp] Is Null
			) Or
			(
				INSERTED.[CustOriginalFeeDirExp] !=
				DELETED.[CustOriginalFeeDirExp]
			)
		) 
		END		
		
      If UPDATE([CustOriginalConsultFee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOriginalConsultFee',
      CONVERT(NVARCHAR(2000),DELETED.[CustOriginalConsultFee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustOriginalConsultFee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustOriginalConsultFee] Is Null And
				DELETED.[CustOriginalConsultFee] Is Not Null
			) Or
			(
				INSERTED.[CustOriginalConsultFee] Is Not Null And
				DELETED.[CustOriginalConsultFee] Is Null
			) Or
			(
				INSERTED.[CustOriginalConsultFee] !=
				DELETED.[CustOriginalConsultFee]
			)
		) 
		END		
		
      If UPDATE([CustOriginalReimbAllowExp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOriginalReimbAllowExp',
      CONVERT(NVARCHAR(2000),DELETED.[CustOriginalReimbAllowExp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustOriginalReimbAllowExp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustOriginalReimbAllowExp] Is Null And
				DELETED.[CustOriginalReimbAllowExp] Is Not Null
			) Or
			(
				INSERTED.[CustOriginalReimbAllowExp] Is Not Null And
				DELETED.[CustOriginalReimbAllowExp] Is Null
			) Or
			(
				INSERTED.[CustOriginalReimbAllowExp] !=
				DELETED.[CustOriginalReimbAllowExp]
			)
		) 
		END		
		
      If UPDATE([CustOriginalReimbAllowCons])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOriginalReimbAllowCons',
      CONVERT(NVARCHAR(2000),DELETED.[CustOriginalReimbAllowCons],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustOriginalReimbAllowCons],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustOriginalReimbAllowCons] Is Null And
				DELETED.[CustOriginalReimbAllowCons] Is Not Null
			) Or
			(
				INSERTED.[CustOriginalReimbAllowCons] Is Not Null And
				DELETED.[CustOriginalReimbAllowCons] Is Null
			) Or
			(
				INSERTED.[CustOriginalReimbAllowCons] !=
				DELETED.[CustOriginalReimbAllowCons]
			)
		) 
		END		
		
      If UPDATE([CustOriginalFeeTotal])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOriginalFeeTotal',
      CONVERT(NVARCHAR(2000),DELETED.[CustOriginalFeeTotal],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustOriginalFeeTotal],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustOriginalFeeTotal] Is Null And
				DELETED.[CustOriginalFeeTotal] Is Not Null
			) Or
			(
				INSERTED.[CustOriginalFeeTotal] Is Not Null And
				DELETED.[CustOriginalFeeTotal] Is Null
			) Or
			(
				INSERTED.[CustOriginalFeeTotal] !=
				DELETED.[CustOriginalFeeTotal]
			)
		) 
		END		
		
     If UPDATE([CustMarketingManager])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingManager',
     CONVERT(NVARCHAR(2000),DELETED.[CustMarketingManager],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingManager],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMarketingManager] Is Null And
				DELETED.[CustMarketingManager] Is Not Null
			) Or
			(
				INSERTED.[CustMarketingManager] Is Not Null And
				DELETED.[CustMarketingManager] Is Null
			) Or
			(
				INSERTED.[CustMarketingManager] !=
				DELETED.[CustMarketingManager]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustMarketingManager = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustMarketingManager = newDesc.Employee
		END		
		
      If UPDATE([CustCorporateMarketing])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCorporateMarketing',
      CONVERT(NVARCHAR(2000),DELETED.[CustCorporateMarketing],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCorporateMarketing],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCorporateMarketing] Is Null And
				DELETED.[CustCorporateMarketing] Is Not Null
			) Or
			(
				INSERTED.[CustCorporateMarketing] Is Not Null And
				DELETED.[CustCorporateMarketing] Is Null
			) Or
			(
				INSERTED.[CustCorporateMarketing] !=
				DELETED.[CustCorporateMarketing]
			)
		) 
		END		
		
      If UPDATE([CustClientReferenceNo1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustClientReferenceNo1',
      CONVERT(NVARCHAR(2000),DELETED.[CustClientReferenceNo1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustClientReferenceNo1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustClientReferenceNo1] Is Null And
				DELETED.[CustClientReferenceNo1] Is Not Null
			) Or
			(
				INSERTED.[CustClientReferenceNo1] Is Not Null And
				DELETED.[CustClientReferenceNo1] Is Null
			) Or
			(
				INSERTED.[CustClientReferenceNo1] !=
				DELETED.[CustClientReferenceNo1]
			)
		) 
		END		
		
      If UPDATE([CustClientReferenceNo2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustClientReferenceNo2',
      CONVERT(NVARCHAR(2000),DELETED.[CustClientReferenceNo2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustClientReferenceNo2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustClientReferenceNo2] Is Null And
				DELETED.[CustClientReferenceNo2] Is Not Null
			) Or
			(
				INSERTED.[CustClientReferenceNo2] Is Not Null And
				DELETED.[CustClientReferenceNo2] Is Null
			) Or
			(
				INSERTED.[CustClientReferenceNo2] !=
				DELETED.[CustClientReferenceNo2]
			)
		) 
		END		
		
      If UPDATE([CustBillingTermDescription])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingTermDescription',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustMarketingName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingName',
      CONVERT(NVARCHAR(2000),DELETED.[CustMarketingName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMarketingName] Is Null And
				DELETED.[CustMarketingName] Is Not Null
			) Or
			(
				INSERTED.[CustMarketingName] Is Not Null And
				DELETED.[CustMarketingName] Is Null
			) Or
			(
				INSERTED.[CustMarketingName] !=
				DELETED.[CustMarketingName]
			)
		) 
		END		
		
      If UPDATE([CustMarketable])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketable',
      CONVERT(NVARCHAR(2000),DELETED.[CustMarketable],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustMarketable],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMarketable] Is Null And
				DELETED.[CustMarketable] Is Not Null
			) Or
			(
				INSERTED.[CustMarketable] Is Not Null And
				DELETED.[CustMarketable] Is Null
			) Or
			(
				INSERTED.[CustMarketable] !=
				DELETED.[CustMarketable]
			)
		) 
		END		
		
      If UPDATE([CustEstimatedConstructionCost])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEstimatedConstructionCost',
      CONVERT(NVARCHAR(2000),DELETED.[CustEstimatedConstructionCost],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEstimatedConstructionCost],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEstimatedConstructionCost] Is Null And
				DELETED.[CustEstimatedConstructionCost] Is Not Null
			) Or
			(
				INSERTED.[CustEstimatedConstructionCost] Is Not Null And
				DELETED.[CustEstimatedConstructionCost] Is Null
			) Or
			(
				INSERTED.[CustEstimatedConstructionCost] !=
				DELETED.[CustEstimatedConstructionCost]
			)
		) 
		END		
		
      If UPDATE([CustActualConstructionCost])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustActualConstructionCost',
      CONVERT(NVARCHAR(2000),DELETED.[CustActualConstructionCost],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustActualConstructionCost],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustActualConstructionCost] Is Null And
				DELETED.[CustActualConstructionCost] Is Not Null
			) Or
			(
				INSERTED.[CustActualConstructionCost] Is Not Null And
				DELETED.[CustActualConstructionCost] Is Null
			) Or
			(
				INSERTED.[CustActualConstructionCost] !=
				DELETED.[CustActualConstructionCost]
			)
		) 
		END		
		
      If UPDATE([CustBillingTermNotes])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingTermNotes',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustShowCPPrevious])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustShowCPPrevious',
      CONVERT(NVARCHAR(2000),DELETED.[CustShowCPPrevious],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustShowCPPrevious],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustShowCPPrevious] Is Null And
				DELETED.[CustShowCPPrevious] Is Not Null
			) Or
			(
				INSERTED.[CustShowCPPrevious] Is Not Null And
				DELETED.[CustShowCPPrevious] Is Null
			) Or
			(
				INSERTED.[CustShowCPPrevious] !=
				DELETED.[CustShowCPPrevious]
			)
		) 
		END		
		
      If UPDATE([CustHistorySource])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistorySource',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistorySource],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistorySource],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistorySource] Is Null And
				DELETED.[CustHistorySource] Is Not Null
			) Or
			(
				INSERTED.[CustHistorySource] Is Not Null And
				DELETED.[CustHistorySource] Is Null
			) Or
			(
				INSERTED.[CustHistorySource] !=
				DELETED.[CustHistorySource]
			)
		) 
		END		
		
      If UPDATE([CustGeoCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustGeoCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustGeoCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustGeoCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustGeoCode] Is Null And
				DELETED.[CustGeoCode] Is Not Null
			) Or
			(
				INSERTED.[CustGeoCode] Is Not Null And
				DELETED.[CustGeoCode] Is Null
			) Or
			(
				INSERTED.[CustGeoCode] !=
				DELETED.[CustGeoCode]
			)
		) 
		END		
		
     If UPDATE([CustHistoryProjectAccountant])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryProjectAccountant',
     CONVERT(NVARCHAR(2000),DELETED.[CustHistoryProjectAccountant],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryProjectAccountant],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryProjectAccountant] Is Null And
				DELETED.[CustHistoryProjectAccountant] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryProjectAccountant] Is Not Null And
				DELETED.[CustHistoryProjectAccountant] Is Null
			) Or
			(
				INSERTED.[CustHistoryProjectAccountant] !=
				DELETED.[CustHistoryProjectAccountant]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryProjectAccountant = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryProjectAccountant = newDesc.Employee
		END		
		
     If UPDATE([CustHistoryOperationsManager])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryOperationsManager',
     CONVERT(NVARCHAR(2000),DELETED.[CustHistoryOperationsManager],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryOperationsManager],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryOperationsManager] Is Null And
				DELETED.[CustHistoryOperationsManager] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryOperationsManager] Is Not Null And
				DELETED.[CustHistoryOperationsManager] Is Null
			) Or
			(
				INSERTED.[CustHistoryOperationsManager] !=
				DELETED.[CustHistoryOperationsManager]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryOperationsManager = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryOperationsManager = newDesc.Employee
		END		
		
     If UPDATE([CustHistoryTeamLeader])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustHistoryTeamLeader',
     CONVERT(NVARCHAR(2000),DELETED.[CustHistoryTeamLeader],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryTeamLeader],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustHistoryTeamLeader] Is Null And
				DELETED.[CustHistoryTeamLeader] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryTeamLeader] Is Not Null And
				DELETED.[CustHistoryTeamLeader] Is Null
			) Or
			(
				INSERTED.[CustHistoryTeamLeader] !=
				DELETED.[CustHistoryTeamLeader]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryTeamLeader = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryTeamLeader = newDesc.Employee
		END		
		
      If UPDATE([CustDesignOnly])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDesignOnly',
      CONVERT(NVARCHAR(2000),DELETED.[CustDesignOnly],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustDesignOnly],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustDesignOnly] Is Null And
				DELETED.[CustDesignOnly] Is Not Null
			) Or
			(
				INSERTED.[CustDesignOnly] Is Not Null And
				DELETED.[CustDesignOnly] Is Null
			) Or
			(
				INSERTED.[CustDesignOnly] !=
				DELETED.[CustDesignOnly]
			)
		) 
		END		
		
      If UPDATE([CustInvoiceAsGenesisHalff])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceAsGenesisHalff',
      CONVERT(NVARCHAR(2000),DELETED.[CustInvoiceAsGenesisHalff],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustInvoiceAsGenesisHalff],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustInvoiceAsGenesisHalff] Is Null And
				DELETED.[CustInvoiceAsGenesisHalff] Is Not Null
			) Or
			(
				INSERTED.[CustInvoiceAsGenesisHalff] Is Not Null And
				DELETED.[CustInvoiceAsGenesisHalff] Is Null
			) Or
			(
				INSERTED.[CustInvoiceAsGenesisHalff] !=
				DELETED.[CustInvoiceAsGenesisHalff]
			)
		) 
		END		
		
      If UPDATE([CustMarketingFirstReviewComplete])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingFirstReviewComplete',
      CONVERT(NVARCHAR(2000),DELETED.[CustMarketingFirstReviewComplete],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingFirstReviewComplete],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMarketingFirstReviewComplete] Is Null And
				DELETED.[CustMarketingFirstReviewComplete] Is Not Null
			) Or
			(
				INSERTED.[CustMarketingFirstReviewComplete] Is Not Null And
				DELETED.[CustMarketingFirstReviewComplete] Is Null
			) Or
			(
				INSERTED.[CustMarketingFirstReviewComplete] !=
				DELETED.[CustMarketingFirstReviewComplete]
			)
		) 
		END		
		
      If UPDATE([CustMarketingFirstReviewCompleteDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingFirstReviewCompleteDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustMarketingFirstReviewCompleteDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingFirstReviewCompleteDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMarketingFirstReviewCompleteDate] Is Null And
				DELETED.[CustMarketingFirstReviewCompleteDate] Is Not Null
			) Or
			(
				INSERTED.[CustMarketingFirstReviewCompleteDate] Is Not Null And
				DELETED.[CustMarketingFirstReviewCompleteDate] Is Null
			) Or
			(
				INSERTED.[CustMarketingFirstReviewCompleteDate] !=
				DELETED.[CustMarketingFirstReviewCompleteDate]
			)
		) 
		END		
		
     If UPDATE([CustMarketingFirstReviewCompleteEmployee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingFirstReviewCompleteEmployee',
     CONVERT(NVARCHAR(2000),DELETED.[CustMarketingFirstReviewCompleteEmployee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingFirstReviewCompleteEmployee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMarketingFirstReviewCompleteEmployee] Is Null And
				DELETED.[CustMarketingFirstReviewCompleteEmployee] Is Not Null
			) Or
			(
				INSERTED.[CustMarketingFirstReviewCompleteEmployee] Is Not Null And
				DELETED.[CustMarketingFirstReviewCompleteEmployee] Is Null
			) Or
			(
				INSERTED.[CustMarketingFirstReviewCompleteEmployee] !=
				DELETED.[CustMarketingFirstReviewCompleteEmployee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustMarketingFirstReviewCompleteEmployee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustMarketingFirstReviewCompleteEmployee = newDesc.Employee
		END		
		
      If UPDATE([CustMarketingFinalReviewComplete])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingFinalReviewComplete',
      CONVERT(NVARCHAR(2000),DELETED.[CustMarketingFinalReviewComplete],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingFinalReviewComplete],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMarketingFinalReviewComplete] Is Null And
				DELETED.[CustMarketingFinalReviewComplete] Is Not Null
			) Or
			(
				INSERTED.[CustMarketingFinalReviewComplete] Is Not Null And
				DELETED.[CustMarketingFinalReviewComplete] Is Null
			) Or
			(
				INSERTED.[CustMarketingFinalReviewComplete] !=
				DELETED.[CustMarketingFinalReviewComplete]
			)
		) 
		END		
		
      If UPDATE([CustMarketingFinalReviewCompleteDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingFinalReviewCompleteDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustMarketingFinalReviewCompleteDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingFinalReviewCompleteDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMarketingFinalReviewCompleteDate] Is Null And
				DELETED.[CustMarketingFinalReviewCompleteDate] Is Not Null
			) Or
			(
				INSERTED.[CustMarketingFinalReviewCompleteDate] Is Not Null And
				DELETED.[CustMarketingFinalReviewCompleteDate] Is Null
			) Or
			(
				INSERTED.[CustMarketingFinalReviewCompleteDate] !=
				DELETED.[CustMarketingFinalReviewCompleteDate]
			)
		) 
		END		
		
     If UPDATE([CustMarketingFinalReviewCompleteEmployee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingFinalReviewCompleteEmployee',
     CONVERT(NVARCHAR(2000),DELETED.[CustMarketingFinalReviewCompleteEmployee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingFinalReviewCompleteEmployee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMarketingFinalReviewCompleteEmployee] Is Null And
				DELETED.[CustMarketingFinalReviewCompleteEmployee] Is Not Null
			) Or
			(
				INSERTED.[CustMarketingFinalReviewCompleteEmployee] Is Not Null And
				DELETED.[CustMarketingFinalReviewCompleteEmployee] Is Null
			) Or
			(
				INSERTED.[CustMarketingFinalReviewCompleteEmployee] !=
				DELETED.[CustMarketingFinalReviewCompleteEmployee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustMarketingFinalReviewCompleteEmployee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustMarketingFinalReviewCompleteEmployee = newDesc.Employee
		END		
		
      If UPDATE([CustRoleAdd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRoleAdd',
      CONVERT(NVARCHAR(2000),DELETED.[CustRoleAdd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRoleAdd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustRoleAdd] Is Null And
				DELETED.[CustRoleAdd] Is Not Null
			) Or
			(
				INSERTED.[CustRoleAdd] Is Not Null And
				DELETED.[CustRoleAdd] Is Null
			) Or
			(
				INSERTED.[CustRoleAdd] !=
				DELETED.[CustRoleAdd]
			)
		) 
		END		
		
     If UPDATE([CustMasterContractProject])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMasterContractProject',
     CONVERT(NVARCHAR(2000),DELETED.[CustMasterContractProject],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustMasterContractProject],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMasterContractProject] Is Null And
				DELETED.[CustMasterContractProject] Is Not Null
			) Or
			(
				INSERTED.[CustMasterContractProject] Is Not Null And
				DELETED.[CustMasterContractProject] Is Null
			) Or
			(
				INSERTED.[CustMasterContractProject] !=
				DELETED.[CustMasterContractProject]
			)
		) left join PR as oldDesc  on DELETED.CustMasterContractProject = oldDesc.WBS1  left join  PR as newDesc  on INSERTED.CustMasterContractProject = newDesc.WBS1
		END		
		
      If UPDATE([CustProgressReportNotes])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustProgressReportNotes',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustProgressReportEditDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustProgressReportEditDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustProgressReportEditDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustProgressReportEditDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustProgressReportEditDate] Is Null And
				DELETED.[CustProgressReportEditDate] Is Not Null
			) Or
			(
				INSERTED.[CustProgressReportEditDate] Is Not Null And
				DELETED.[CustProgressReportEditDate] Is Null
			) Or
			(
				INSERTED.[CustProgressReportEditDate] !=
				DELETED.[CustProgressReportEditDate]
			)
		) 
		END		
		
     If UPDATE([CustOperationsManager])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOperationsManager',
     CONVERT(NVARCHAR(2000),DELETED.[CustOperationsManager],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustOperationsManager],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustOperationsManager] Is Null And
				DELETED.[CustOperationsManager] Is Not Null
			) Or
			(
				INSERTED.[CustOperationsManager] Is Not Null And
				DELETED.[CustOperationsManager] Is Null
			) Or
			(
				INSERTED.[CustOperationsManager] !=
				DELETED.[CustOperationsManager]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustOperationsManager = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustOperationsManager = newDesc.Employee
		END		
		
      If UPDATE([CustPAResponsibleForCollections])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPAResponsibleForCollections',
      CONVERT(NVARCHAR(2000),DELETED.[CustPAResponsibleForCollections],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPAResponsibleForCollections],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustPAResponsibleForCollections] Is Null And
				DELETED.[CustPAResponsibleForCollections] Is Not Null
			) Or
			(
				INSERTED.[CustPAResponsibleForCollections] Is Not Null And
				DELETED.[CustPAResponsibleForCollections] Is Null
			) Or
			(
				INSERTED.[CustPAResponsibleForCollections] !=
				DELETED.[CustPAResponsibleForCollections]
			)
		) 
		END		
		
      If UPDATE([CustWorkShare])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustWorkShare',
      CONVERT(NVARCHAR(2000),DELETED.[CustWorkShare],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustWorkShare],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustWorkShare] Is Null And
				DELETED.[CustWorkShare] Is Not Null
			) Or
			(
				INSERTED.[CustWorkShare] Is Not Null And
				DELETED.[CustWorkShare] Is Null
			) Or
			(
				INSERTED.[CustWorkShare] !=
				DELETED.[CustWorkShare]
			)
		) 
		END		
		
      If UPDATE([CustManualOHRateOverride])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustManualOHRateOverride',
      CONVERT(NVARCHAR(2000),DELETED.[CustManualOHRateOverride],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustManualOHRateOverride],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustManualOHRateOverride] Is Null And
				DELETED.[CustManualOHRateOverride] Is Not Null
			) Or
			(
				INSERTED.[CustManualOHRateOverride] Is Not Null And
				DELETED.[CustManualOHRateOverride] Is Null
			) Or
			(
				INSERTED.[CustManualOHRateOverride] !=
				DELETED.[CustManualOHRateOverride]
			)
		) 
		END		
		
      If UPDATE([CustSelectionProcess])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSelectionProcess',
      CONVERT(NVARCHAR(2000),DELETED.[CustSelectionProcess],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSelectionProcess],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustSelectionProcess] Is Null And
				DELETED.[CustSelectionProcess] Is Not Null
			) Or
			(
				INSERTED.[CustSelectionProcess] Is Not Null And
				DELETED.[CustSelectionProcess] Is Null
			) Or
			(
				INSERTED.[CustSelectionProcess] !=
				DELETED.[CustSelectionProcess]
			)
		) 
		END		
		
      If UPDATE([CustCollectionsResponsibility])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCollectionsResponsibility',
      CONVERT(NVARCHAR(2000),DELETED.[CustCollectionsResponsibility],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCollectionsResponsibility],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCollectionsResponsibility] Is Null And
				DELETED.[CustCollectionsResponsibility] Is Not Null
			) Or
			(
				INSERTED.[CustCollectionsResponsibility] Is Not Null And
				DELETED.[CustCollectionsResponsibility] Is Null
			) Or
			(
				INSERTED.[CustCollectionsResponsibility] !=
				DELETED.[CustCollectionsResponsibility]
			)
		) 
		END		
		
      If UPDATE([CustInvoiceAsHTT])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceAsHTT',
      CONVERT(NVARCHAR(2000),DELETED.[CustInvoiceAsHTT],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustInvoiceAsHTT],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustInvoiceAsHTT] Is Null And
				DELETED.[CustInvoiceAsHTT] Is Not Null
			) Or
			(
				INSERTED.[CustInvoiceAsHTT] Is Not Null And
				DELETED.[CustInvoiceAsHTT] Is Null
			) Or
			(
				INSERTED.[CustInvoiceAsHTT] !=
				DELETED.[CustInvoiceAsHTT]
			)
		) 
		END		
		
      If UPDATE([CustInvoiceAsCEI])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceAsCEI',
      CONVERT(NVARCHAR(2000),DELETED.[CustInvoiceAsCEI],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustInvoiceAsCEI],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustInvoiceAsCEI] Is Null And
				DELETED.[CustInvoiceAsCEI] Is Not Null
			) Or
			(
				INSERTED.[CustInvoiceAsCEI] Is Not Null And
				DELETED.[CustInvoiceAsCEI] Is Null
			) Or
			(
				INSERTED.[CustInvoiceAsCEI] !=
				DELETED.[CustInvoiceAsCEI]
			)
		) 
		END		
		
      If UPDATE([CustInvoiceTemplate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInvoiceTemplate',
      CONVERT(NVARCHAR(2000),DELETED.[CustInvoiceTemplate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustInvoiceTemplate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustInvoiceTemplate] Is Null And
				DELETED.[CustInvoiceTemplate] Is Not Null
			) Or
			(
				INSERTED.[CustInvoiceTemplate] Is Not Null And
				DELETED.[CustInvoiceTemplate] Is Null
			) Or
			(
				INSERTED.[CustInvoiceTemplate] !=
				DELETED.[CustInvoiceTemplate]
			)
		) 
		END		
		
      If UPDATE([CustRestartDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRestartDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustRestartDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRestartDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustRestartDate] Is Null And
				DELETED.[CustRestartDate] Is Not Null
			) Or
			(
				INSERTED.[CustRestartDate] Is Not Null And
				DELETED.[CustRestartDate] Is Null
			) Or
			(
				INSERTED.[CustRestartDate] !=
				DELETED.[CustRestartDate]
			)
		) 
		END		
		
      If UPDATE([CustContractAttachmentsReviewed])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustContractAttachmentsReviewed',
      CONVERT(NVARCHAR(2000),DELETED.[CustContractAttachmentsReviewed],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustContractAttachmentsReviewed],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustContractAttachmentsReviewed] Is Null And
				DELETED.[CustContractAttachmentsReviewed] Is Not Null
			) Or
			(
				INSERTED.[CustContractAttachmentsReviewed] Is Not Null And
				DELETED.[CustContractAttachmentsReviewed] Is Null
			) Or
			(
				INSERTED.[CustContractAttachmentsReviewed] !=
				DELETED.[CustContractAttachmentsReviewed]
			)
		) 
		END		
		
     If UPDATE([CustProjectType])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustProjectType',
     CONVERT(NVARCHAR(2000),DELETED.[CustProjectType],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustProjectType],121),
     IsNull(oldDesc.CustProjectTypeName, N''), IsNull(newDesc.CustProjectTypeName, N''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustProjectType] Is Null And
				DELETED.[CustProjectType] Is Not Null
			) Or
			(
				INSERTED.[CustProjectType] Is Not Null And
				DELETED.[CustProjectType] Is Null
			) Or
			(
				INSERTED.[CustProjectType] !=
				DELETED.[CustProjectType]
			)
		) left join UDIC_ProjectType as oldDesc  on DELETED.CustProjectType = oldDesc.UDIC_UID  left join  UDIC_ProjectType as newDesc  on INSERTED.CustProjectType = newDesc.UDIC_UID
		END		
		
     If UPDATE([CustService])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustService',
     CONVERT(NVARCHAR(2000),DELETED.[CustService],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustService],121),
     IsNull(oldDesc.CustServiceName, N''), IsNull(newDesc.CustServiceName, N''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustService] Is Null And
				DELETED.[CustService] Is Not Null
			) Or
			(
				INSERTED.[CustService] Is Not Null And
				DELETED.[CustService] Is Null
			) Or
			(
				INSERTED.[CustService] !=
				DELETED.[CustService]
			)
		) left join UDIC_Service as oldDesc  on DELETED.CustService = oldDesc.UDIC_UID  left join  UDIC_Service as newDesc  on INSERTED.CustService = newDesc.UDIC_UID
		END		
		
     If UPDATE([CustSelectedFirm])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSelectedFirm',
     CONVERT(NVARCHAR(2000),DELETED.[CustSelectedFirm],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustSelectedFirm],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustSelectedFirm] Is Null And
				DELETED.[CustSelectedFirm] Is Not Null
			) Or
			(
				INSERTED.[CustSelectedFirm] Is Not Null And
				DELETED.[CustSelectedFirm] Is Null
			) Or
			(
				INSERTED.[CustSelectedFirm] !=
				DELETED.[CustSelectedFirm]
			)
		) left join Clendor as oldDesc  on DELETED.CustSelectedFirm = oldDesc.ClientID  left join  Clendor as newDesc  on INSERTED.CustSelectedFirm = newDesc.ClientID
		END		
		
     If UPDATE([CustOffice])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOffice',
     CONVERT(NVARCHAR(2000),DELETED.[CustOffice],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustOffice],121),
     IsNull(oldDesc.CustOfficeName, N''), IsNull(newDesc.CustOfficeName, N''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustOffice] Is Null And
				DELETED.[CustOffice] Is Not Null
			) Or
			(
				INSERTED.[CustOffice] Is Not Null And
				DELETED.[CustOffice] Is Null
			) Or
			(
				INSERTED.[CustOffice] !=
				DELETED.[CustOffice]
			)
		) left join UDIC_Office as oldDesc  on DELETED.CustOffice = oldDesc.UDIC_UID  left join  UDIC_Office as newDesc  on INSERTED.CustOffice = newDesc.UDIC_UID
		END		
		
      If UPDATE([CustShortlisted])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustShortlisted',
      CONVERT(NVARCHAR(2000),DELETED.[CustShortlisted],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustShortlisted],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustShortlisted] Is Null And
				DELETED.[CustShortlisted] Is Not Null
			) Or
			(
				INSERTED.[CustShortlisted] Is Not Null And
				DELETED.[CustShortlisted] Is Null
			) Or
			(
				INSERTED.[CustShortlisted] !=
				DELETED.[CustShortlisted]
			)
		) 
		END		
		
     If UPDATE([CustInterviewCoordinator])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInterviewCoordinator',
     CONVERT(NVARCHAR(2000),DELETED.[CustInterviewCoordinator],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustInterviewCoordinator],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustInterviewCoordinator] Is Null And
				DELETED.[CustInterviewCoordinator] Is Not Null
			) Or
			(
				INSERTED.[CustInterviewCoordinator] Is Not Null And
				DELETED.[CustInterviewCoordinator] Is Null
			) Or
			(
				INSERTED.[CustInterviewCoordinator] !=
				DELETED.[CustInterviewCoordinator]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustInterviewCoordinator = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustInterviewCoordinator = newDesc.Employee
		END		
		
      If UPDATE([CustMarketingNotes])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingNotes',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustMultiTeamPursuit])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMultiTeamPursuit',
      CONVERT(NVARCHAR(2000),DELETED.[CustMultiTeamPursuit],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustMultiTeamPursuit],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMultiTeamPursuit] Is Null And
				DELETED.[CustMultiTeamPursuit] Is Not Null
			) Or
			(
				INSERTED.[CustMultiTeamPursuit] Is Not Null And
				DELETED.[CustMultiTeamPursuit] Is Null
			) Or
			(
				INSERTED.[CustMultiTeamPursuit] !=
				DELETED.[CustMultiTeamPursuit]
			)
		) 
		END		
		
      If UPDATE([CustCreateProjectStructure])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCreateProjectStructure',
      CONVERT(NVARCHAR(2000),DELETED.[CustCreateProjectStructure],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCreateProjectStructure],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCreateProjectStructure] Is Null And
				DELETED.[CustCreateProjectStructure] Is Not Null
			) Or
			(
				INSERTED.[CustCreateProjectStructure] Is Not Null And
				DELETED.[CustCreateProjectStructure] Is Null
			) Or
			(
				INSERTED.[CustCreateProjectStructure] !=
				DELETED.[CustCreateProjectStructure]
			)
		) 
		END		
		
      If UPDATE([CustTotalCompensation])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustTotalCompensation',
      CONVERT(NVARCHAR(2000),DELETED.[CustTotalCompensation],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTotalCompensation],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustTotalCompensation] Is Null And
				DELETED.[CustTotalCompensation] Is Not Null
			) Or
			(
				INSERTED.[CustTotalCompensation] Is Not Null And
				DELETED.[CustTotalCompensation] Is Null
			) Or
			(
				INSERTED.[CustTotalCompensation] !=
				DELETED.[CustTotalCompensation]
			)
		) 
		END		
		
      If UPDATE([CustCollectionsRespSetup])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCollectionsRespSetup',
      CONVERT(NVARCHAR(2000),DELETED.[CustCollectionsRespSetup],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCollectionsRespSetup],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCollectionsRespSetup] Is Null And
				DELETED.[CustCollectionsRespSetup] Is Not Null
			) Or
			(
				INSERTED.[CustCollectionsRespSetup] Is Not Null And
				DELETED.[CustCollectionsRespSetup] Is Null
			) Or
			(
				INSERTED.[CustCollectionsRespSetup] !=
				DELETED.[CustCollectionsRespSetup]
			)
		) 
		END		
		
     If UPDATE([CustAdministrativeAssistant1])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustAdministrativeAssistant1',
     CONVERT(NVARCHAR(2000),DELETED.[CustAdministrativeAssistant1],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustAdministrativeAssistant1],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustAdministrativeAssistant1] Is Null And
				DELETED.[CustAdministrativeAssistant1] Is Not Null
			) Or
			(
				INSERTED.[CustAdministrativeAssistant1] Is Not Null And
				DELETED.[CustAdministrativeAssistant1] Is Null
			) Or
			(
				INSERTED.[CustAdministrativeAssistant1] !=
				DELETED.[CustAdministrativeAssistant1]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustAdministrativeAssistant1 = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustAdministrativeAssistant1 = newDesc.Employee
		END		
		
     If UPDATE([CustBiller])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBiller',
     CONVERT(NVARCHAR(2000),DELETED.[CustBiller],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustBiller],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustBiller] Is Null And
				DELETED.[CustBiller] Is Not Null
			) Or
			(
				INSERTED.[CustBiller] Is Not Null And
				DELETED.[CustBiller] Is Null
			) Or
			(
				INSERTED.[CustBiller] !=
				DELETED.[CustBiller]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustBiller = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustBiller = newDesc.Employee
		END		
		
     If UPDATE([CustBillingCompany])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingCompany',
     CONVERT(NVARCHAR(2000),DELETED.[CustBillingCompany],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustBillingCompany],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustBillingCompany] Is Null And
				DELETED.[CustBillingCompany] Is Not Null
			) Or
			(
				INSERTED.[CustBillingCompany] Is Not Null And
				DELETED.[CustBillingCompany] Is Null
			) Or
			(
				INSERTED.[CustBillingCompany] !=
				DELETED.[CustBillingCompany]
			)
		) left join Clendor as oldDesc  on DELETED.CustBillingCompany = oldDesc.ClientID  left join  Clendor as newDesc  on INSERTED.CustBillingCompany = newDesc.ClientID
		END		
		
     If UPDATE([CustBillingContact])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingContact',
     CONVERT(NVARCHAR(2000),DELETED.[CustBillingContact],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustBillingContact],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustBillingContact] Is Null And
				DELETED.[CustBillingContact] Is Not Null
			) Or
			(
				INSERTED.[CustBillingContact] Is Not Null And
				DELETED.[CustBillingContact] Is Null
			) Or
			(
				INSERTED.[CustBillingContact] !=
				DELETED.[CustBillingContact]
			)
		) left join Contacts as oldDesc  on DELETED.CustBillingContact = oldDesc.ContactID  left join  Contacts as newDesc  on INSERTED.CustBillingContact = newDesc.ContactID
		END		
		
      If UPDATE([CustBillingType1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustBillingType1',
      CONVERT(NVARCHAR(2000),DELETED.[CustBillingType1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBillingType1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustBillingType1] Is Null And
				DELETED.[CustBillingType1] Is Not Null
			) Or
			(
				INSERTED.[CustBillingType1] Is Not Null And
				DELETED.[CustBillingType1] Is Null
			) Or
			(
				INSERTED.[CustBillingType1] !=
				DELETED.[CustBillingType1]
			)
		) 
		END		
		
     If UPDATE([CustClientAdvocate])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustClientAdvocate',
     CONVERT(NVARCHAR(2000),DELETED.[CustClientAdvocate],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustClientAdvocate],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustClientAdvocate] Is Null And
				DELETED.[CustClientAdvocate] Is Not Null
			) Or
			(
				INSERTED.[CustClientAdvocate] Is Not Null And
				DELETED.[CustClientAdvocate] Is Null
			) Or
			(
				INSERTED.[CustClientAdvocate] !=
				DELETED.[CustClientAdvocate]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustClientAdvocate = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustClientAdvocate = newDesc.Employee
		END		
		
     If UPDATE([CustClientProjectContact1])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustClientProjectContact1',
     CONVERT(NVARCHAR(2000),DELETED.[CustClientProjectContact1],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustClientProjectContact1],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustClientProjectContact1] Is Null And
				DELETED.[CustClientProjectContact1] Is Not Null
			) Or
			(
				INSERTED.[CustClientProjectContact1] Is Not Null And
				DELETED.[CustClientProjectContact1] Is Null
			) Or
			(
				INSERTED.[CustClientProjectContact1] !=
				DELETED.[CustClientProjectContact1]
			)
		) left join Contacts as oldDesc  on DELETED.CustClientProjectContact1 = oldDesc.ContactID  left join  Contacts as newDesc  on INSERTED.CustClientProjectContact1 = newDesc.ContactID
		END		
		
      If UPDATE([CustClientSolicitationNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustClientSolicitationNumber',
      CONVERT(NVARCHAR(2000),DELETED.[CustClientSolicitationNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustClientSolicitationNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustClientSolicitationNumber] Is Null And
				DELETED.[CustClientSolicitationNumber] Is Not Null
			) Or
			(
				INSERTED.[CustClientSolicitationNumber] Is Not Null And
				DELETED.[CustClientSolicitationNumber] Is Null
			) Or
			(
				INSERTED.[CustClientSolicitationNumber] !=
				DELETED.[CustClientSolicitationNumber]
			)
		) 
		END		
		
      If UPDATE([CustCollectionsResponsibility1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCollectionsResponsibility1',
      CONVERT(NVARCHAR(2000),DELETED.[CustCollectionsResponsibility1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCollectionsResponsibility1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCollectionsResponsibility1] Is Null And
				DELETED.[CustCollectionsResponsibility1] Is Not Null
			) Or
			(
				INSERTED.[CustCollectionsResponsibility1] Is Not Null And
				DELETED.[CustCollectionsResponsibility1] Is Null
			) Or
			(
				INSERTED.[CustCollectionsResponsibility1] !=
				DELETED.[CustCollectionsResponsibility1]
			)
		) 
		END		
		
      If UPDATE([CustCompanyStateCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCompanyStateCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustCompanyStateCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCompanyStateCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCompanyStateCode] Is Null And
				DELETED.[CustCompanyStateCode] Is Not Null
			) Or
			(
				INSERTED.[CustCompanyStateCode] Is Not Null And
				DELETED.[CustCompanyStateCode] Is Null
			) Or
			(
				INSERTED.[CustCompanyStateCode] !=
				DELETED.[CustCompanyStateCode]
			)
		) 
		END		
		
      If UPDATE([CustContractExecuted])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustContractExecuted',
      CONVERT(NVARCHAR(2000),DELETED.[CustContractExecuted],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustContractExecuted],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustContractExecuted] Is Null And
				DELETED.[CustContractExecuted] Is Not Null
			) Or
			(
				INSERTED.[CustContractExecuted] Is Not Null And
				DELETED.[CustContractExecuted] Is Null
			) Or
			(
				INSERTED.[CustContractExecuted] !=
				DELETED.[CustContractExecuted]
			)
		) 
		END		
		
      If UPDATE([CustContractType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustContractType',
      CONVERT(NVARCHAR(2000),DELETED.[CustContractType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustContractType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustContractType] Is Null And
				DELETED.[CustContractType] Is Not Null
			) Or
			(
				INSERTED.[CustContractType] Is Not Null And
				DELETED.[CustContractType] Is Null
			) Or
			(
				INSERTED.[CustContractType] !=
				DELETED.[CustContractType]
			)
		) 
		END		
		
      If UPDATE([CustCopy])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCopy',
      CONVERT(NVARCHAR(2000),DELETED.[CustCopy],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCopy],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCopy] Is Null And
				DELETED.[CustCopy] Is Not Null
			) Or
			(
				INSERTED.[CustCopy] Is Not Null And
				DELETED.[CustCopy] Is Null
			) Or
			(
				INSERTED.[CustCopy] !=
				DELETED.[CustCopy]
			)
		) 
		END		
		
      If UPDATE([CustCorporateMarketing1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCorporateMarketing1',
      CONVERT(NVARCHAR(2000),DELETED.[CustCorporateMarketing1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCorporateMarketing1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCorporateMarketing1] Is Null And
				DELETED.[CustCorporateMarketing1] Is Not Null
			) Or
			(
				INSERTED.[CustCorporateMarketing1] Is Not Null And
				DELETED.[CustCorporateMarketing1] Is Null
			) Or
			(
				INSERTED.[CustCorporateMarketing1] !=
				DELETED.[CustCorporateMarketing1]
			)
		) 
		END		
		
      If UPDATE([CustCreateNextSuffix])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCreateNextSuffix',
      CONVERT(NVARCHAR(2000),DELETED.[CustCreateNextSuffix],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCreateNextSuffix],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCreateNextSuffix] Is Null And
				DELETED.[CustCreateNextSuffix] Is Not Null
			) Or
			(
				INSERTED.[CustCreateNextSuffix] Is Not Null And
				DELETED.[CustCreateNextSuffix] Is Null
			) Or
			(
				INSERTED.[CustCreateNextSuffix] !=
				DELETED.[CustCreateNextSuffix]
			)
		) 
		END		
		
      If UPDATE([CustCreateProjectStructure1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCreateProjectStructure1',
      CONVERT(NVARCHAR(2000),DELETED.[CustCreateProjectStructure1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCreateProjectStructure1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCreateProjectStructure1] Is Null And
				DELETED.[CustCreateProjectStructure1] Is Not Null
			) Or
			(
				INSERTED.[CustCreateProjectStructure1] Is Not Null And
				DELETED.[CustCreateProjectStructure1] Is Null
			) Or
			(
				INSERTED.[CustCreateProjectStructure1] !=
				DELETED.[CustCreateProjectStructure1]
			)
		) 
		END		
		
      If UPDATE([CustCreateRegularProject])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCreateRegularProject',
      CONVERT(NVARCHAR(2000),DELETED.[CustCreateRegularProject],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCreateRegularProject],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCreateRegularProject] Is Null And
				DELETED.[CustCreateRegularProject] Is Not Null
			) Or
			(
				INSERTED.[CustCreateRegularProject] Is Not Null And
				DELETED.[CustCreateRegularProject] Is Null
			) Or
			(
				INSERTED.[CustCreateRegularProject] !=
				DELETED.[CustCreateRegularProject]
			)
		) 
		END		
		
      If UPDATE([CustDescApproach])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDescApproach',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustDescAvailability])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDescAvailability',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustDescCompetitor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDescCompetitor',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustDescExperience])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDescExperience',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustDescRelationship])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDescRelationship',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustDescStaff])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDescStaff',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
     If UPDATE([CustDirector1])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustDirector1',
     CONVERT(NVARCHAR(2000),DELETED.[CustDirector1],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustDirector1],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustDirector1] Is Null And
				DELETED.[CustDirector1] Is Not Null
			) Or
			(
				INSERTED.[CustDirector1] Is Not Null And
				DELETED.[CustDirector1] Is Null
			) Or
			(
				INSERTED.[CustDirector1] !=
				DELETED.[CustDirector1]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustDirector1 = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustDirector1 = newDesc.Employee
		END		
		
      If UPDATE([CustEstimatedConsultantFee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEstimatedConsultantFee',
      CONVERT(NVARCHAR(2000),DELETED.[CustEstimatedConsultantFee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEstimatedConsultantFee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEstimatedConsultantFee] Is Null And
				DELETED.[CustEstimatedConsultantFee] Is Not Null
			) Or
			(
				INSERTED.[CustEstimatedConsultantFee] Is Not Null And
				DELETED.[CustEstimatedConsultantFee] Is Null
			) Or
			(
				INSERTED.[CustEstimatedConsultantFee] !=
				DELETED.[CustEstimatedConsultantFee]
			)
		) 
		END		
		
      If UPDATE([CustEstimatedExpenseFee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEstimatedExpenseFee',
      CONVERT(NVARCHAR(2000),DELETED.[CustEstimatedExpenseFee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEstimatedExpenseFee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEstimatedExpenseFee] Is Null And
				DELETED.[CustEstimatedExpenseFee] Is Not Null
			) Or
			(
				INSERTED.[CustEstimatedExpenseFee] Is Not Null And
				DELETED.[CustEstimatedExpenseFee] Is Null
			) Or
			(
				INSERTED.[CustEstimatedExpenseFee] !=
				DELETED.[CustEstimatedExpenseFee]
			)
		) 
		END		
		
      If UPDATE([CustEstimatedLaborFee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEstimatedLaborFee',
      CONVERT(NVARCHAR(2000),DELETED.[CustEstimatedLaborFee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEstimatedLaborFee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEstimatedLaborFee] Is Null And
				DELETED.[CustEstimatedLaborFee] Is Not Null
			) Or
			(
				INSERTED.[CustEstimatedLaborFee] Is Not Null And
				DELETED.[CustEstimatedLaborFee] Is Null
			) Or
			(
				INSERTED.[CustEstimatedLaborFee] !=
				DELETED.[CustEstimatedLaborFee]
			)
		) 
		END		
		
      If UPDATE([CustEvalClientNum])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalClientNum',
      CONVERT(NVARCHAR(2000),DELETED.[CustEvalClientNum],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEvalClientNum],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEvalClientNum] Is Null And
				DELETED.[CustEvalClientNum] Is Not Null
			) Or
			(
				INSERTED.[CustEvalClientNum] Is Not Null And
				DELETED.[CustEvalClientNum] Is Null
			) Or
			(
				INSERTED.[CustEvalClientNum] !=
				DELETED.[CustEvalClientNum]
			)
		) 
		END		
		
      If UPDATE([CustEvalRateApproach])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateApproach',
      CONVERT(NVARCHAR(2000),DELETED.[CustEvalRateApproach],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEvalRateApproach],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEvalRateApproach] Is Null And
				DELETED.[CustEvalRateApproach] Is Not Null
			) Or
			(
				INSERTED.[CustEvalRateApproach] Is Not Null And
				DELETED.[CustEvalRateApproach] Is Null
			) Or
			(
				INSERTED.[CustEvalRateApproach] !=
				DELETED.[CustEvalRateApproach]
			)
		) 
		END		
		
      If UPDATE([CustEvalRateApproachNum])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateApproachNum',
      CONVERT(NVARCHAR(2000),DELETED.[CustEvalRateApproachNum],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEvalRateApproachNum],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEvalRateApproachNum] Is Null And
				DELETED.[CustEvalRateApproachNum] Is Not Null
			) Or
			(
				INSERTED.[CustEvalRateApproachNum] Is Not Null And
				DELETED.[CustEvalRateApproachNum] Is Null
			) Or
			(
				INSERTED.[CustEvalRateApproachNum] !=
				DELETED.[CustEvalRateApproachNum]
			)
		) 
		END		
		
      If UPDATE([CustEvalRateAvailability])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateAvailability',
      CONVERT(NVARCHAR(2000),DELETED.[CustEvalRateAvailability],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEvalRateAvailability],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEvalRateAvailability] Is Null And
				DELETED.[CustEvalRateAvailability] Is Not Null
			) Or
			(
				INSERTED.[CustEvalRateAvailability] Is Not Null And
				DELETED.[CustEvalRateAvailability] Is Null
			) Or
			(
				INSERTED.[CustEvalRateAvailability] !=
				DELETED.[CustEvalRateAvailability]
			)
		) 
		END		
		
      If UPDATE([CustEvalRateAvailabilityNum])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateAvailabilityNum',
      CONVERT(NVARCHAR(2000),DELETED.[CustEvalRateAvailabilityNum],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEvalRateAvailabilityNum],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEvalRateAvailabilityNum] Is Null And
				DELETED.[CustEvalRateAvailabilityNum] Is Not Null
			) Or
			(
				INSERTED.[CustEvalRateAvailabilityNum] Is Not Null And
				DELETED.[CustEvalRateAvailabilityNum] Is Null
			) Or
			(
				INSERTED.[CustEvalRateAvailabilityNum] !=
				DELETED.[CustEvalRateAvailabilityNum]
			)
		) 
		END		
		
      If UPDATE([CustEvalRateCompetitor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateCompetitor',
      CONVERT(NVARCHAR(2000),DELETED.[CustEvalRateCompetitor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEvalRateCompetitor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEvalRateCompetitor] Is Null And
				DELETED.[CustEvalRateCompetitor] Is Not Null
			) Or
			(
				INSERTED.[CustEvalRateCompetitor] Is Not Null And
				DELETED.[CustEvalRateCompetitor] Is Null
			) Or
			(
				INSERTED.[CustEvalRateCompetitor] !=
				DELETED.[CustEvalRateCompetitor]
			)
		) 
		END		
		
      If UPDATE([CustEvalRateCompetitorNum])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateCompetitorNum',
      CONVERT(NVARCHAR(2000),DELETED.[CustEvalRateCompetitorNum],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEvalRateCompetitorNum],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEvalRateCompetitorNum] Is Null And
				DELETED.[CustEvalRateCompetitorNum] Is Not Null
			) Or
			(
				INSERTED.[CustEvalRateCompetitorNum] Is Not Null And
				DELETED.[CustEvalRateCompetitorNum] Is Null
			) Or
			(
				INSERTED.[CustEvalRateCompetitorNum] !=
				DELETED.[CustEvalRateCompetitorNum]
			)
		) 
		END		
		
      If UPDATE([CustEvalRateExperience])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateExperience',
      CONVERT(NVARCHAR(2000),DELETED.[CustEvalRateExperience],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEvalRateExperience],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEvalRateExperience] Is Null And
				DELETED.[CustEvalRateExperience] Is Not Null
			) Or
			(
				INSERTED.[CustEvalRateExperience] Is Not Null And
				DELETED.[CustEvalRateExperience] Is Null
			) Or
			(
				INSERTED.[CustEvalRateExperience] !=
				DELETED.[CustEvalRateExperience]
			)
		) 
		END		
		
      If UPDATE([CustEvalRateExperienceNum])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateExperienceNum',
      CONVERT(NVARCHAR(2000),DELETED.[CustEvalRateExperienceNum],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEvalRateExperienceNum],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEvalRateExperienceNum] Is Null And
				DELETED.[CustEvalRateExperienceNum] Is Not Null
			) Or
			(
				INSERTED.[CustEvalRateExperienceNum] Is Not Null And
				DELETED.[CustEvalRateExperienceNum] Is Null
			) Or
			(
				INSERTED.[CustEvalRateExperienceNum] !=
				DELETED.[CustEvalRateExperienceNum]
			)
		) 
		END		
		
      If UPDATE([CustEvalRateOverall])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateOverall',
      CONVERT(NVARCHAR(2000),DELETED.[CustEvalRateOverall],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEvalRateOverall],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEvalRateOverall] Is Null And
				DELETED.[CustEvalRateOverall] Is Not Null
			) Or
			(
				INSERTED.[CustEvalRateOverall] Is Not Null And
				DELETED.[CustEvalRateOverall] Is Null
			) Or
			(
				INSERTED.[CustEvalRateOverall] !=
				DELETED.[CustEvalRateOverall]
			)
		) 
		END		
		
      If UPDATE([CustEvalRateRelationship])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateRelationship',
      CONVERT(NVARCHAR(2000),DELETED.[CustEvalRateRelationship],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEvalRateRelationship],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEvalRateRelationship] Is Null And
				DELETED.[CustEvalRateRelationship] Is Not Null
			) Or
			(
				INSERTED.[CustEvalRateRelationship] Is Not Null And
				DELETED.[CustEvalRateRelationship] Is Null
			) Or
			(
				INSERTED.[CustEvalRateRelationship] !=
				DELETED.[CustEvalRateRelationship]
			)
		) 
		END		
		
      If UPDATE([CustEvalRateStaff])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateStaff',
      CONVERT(NVARCHAR(2000),DELETED.[CustEvalRateStaff],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEvalRateStaff],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEvalRateStaff] Is Null And
				DELETED.[CustEvalRateStaff] Is Not Null
			) Or
			(
				INSERTED.[CustEvalRateStaff] Is Not Null And
				DELETED.[CustEvalRateStaff] Is Null
			) Or
			(
				INSERTED.[CustEvalRateStaff] !=
				DELETED.[CustEvalRateStaff]
			)
		) 
		END		
		
      If UPDATE([CustEvalRateStaffNum])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalRateStaffNum',
      CONVERT(NVARCHAR(2000),DELETED.[CustEvalRateStaffNum],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEvalRateStaffNum],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEvalRateStaffNum] Is Null And
				DELETED.[CustEvalRateStaffNum] Is Not Null
			) Or
			(
				INSERTED.[CustEvalRateStaffNum] Is Not Null And
				DELETED.[CustEvalRateStaffNum] Is Null
			) Or
			(
				INSERTED.[CustEvalRateStaffNum] !=
				DELETED.[CustEvalRateStaffNum]
			)
		) 
		END		
		
      If UPDATE([CustFederalInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFederalInd',
      CONVERT(NVARCHAR(2000),DELETED.[CustFederalInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFederalInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFederalInd] Is Null And
				DELETED.[CustFederalInd] Is Not Null
			) Or
			(
				INSERTED.[CustFederalInd] Is Not Null And
				DELETED.[CustFederalInd] Is Null
			) Or
			(
				INSERTED.[CustFederalInd] !=
				DELETED.[CustFederalInd]
			)
		) 
		END		
		
      If UPDATE([CustFeeProposalSubmitted])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFeeProposalSubmitted',
      CONVERT(NVARCHAR(2000),DELETED.[CustFeeProposalSubmitted],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFeeProposalSubmitted],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFeeProposalSubmitted] Is Null And
				DELETED.[CustFeeProposalSubmitted] Is Not Null
			) Or
			(
				INSERTED.[CustFeeProposalSubmitted] Is Not Null And
				DELETED.[CustFeeProposalSubmitted] Is Null
			) Or
			(
				INSERTED.[CustFeeProposalSubmitted] !=
				DELETED.[CustFeeProposalSubmitted]
			)
		) 
		END		
		
      If UPDATE([CustFeeTypes])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFeeTypes',
      CONVERT(NVARCHAR(2000),DELETED.[CustFeeTypes],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFeeTypes],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFeeTypes] Is Null And
				DELETED.[CustFeeTypes] Is Not Null
			) Or
			(
				INSERTED.[CustFeeTypes] Is Not Null And
				DELETED.[CustFeeTypes] Is Null
			) Or
			(
				INSERTED.[CustFeeTypes] !=
				DELETED.[CustFeeTypes]
			)
		) 
		END		
		
      If UPDATE([CustInterviewed])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInterviewed',
      CONVERT(NVARCHAR(2000),DELETED.[CustInterviewed],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustInterviewed],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustInterviewed] Is Null And
				DELETED.[CustInterviewed] Is Not Null
			) Or
			(
				INSERTED.[CustInterviewed] Is Not Null And
				DELETED.[CustInterviewed] Is Null
			) Or
			(
				INSERTED.[CustInterviewed] !=
				DELETED.[CustInterviewed]
			)
		) 
		END		
		
      If UPDATE([CustLaborFringeMultiplier1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLaborFringeMultiplier1',
      CONVERT(NVARCHAR(2000),DELETED.[CustLaborFringeMultiplier1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLaborFringeMultiplier1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustLaborFringeMultiplier1] Is Null And
				DELETED.[CustLaborFringeMultiplier1] Is Not Null
			) Or
			(
				INSERTED.[CustLaborFringeMultiplier1] Is Not Null And
				DELETED.[CustLaborFringeMultiplier1] Is Null
			) Or
			(
				INSERTED.[CustLaborFringeMultiplier1] !=
				DELETED.[CustLaborFringeMultiplier1]
			)
		) 
		END		
		
      If UPDATE([CustLaborOvhProfitMultiplier1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLaborOvhProfitMultiplier1',
      CONVERT(NVARCHAR(2000),DELETED.[CustLaborOvhProfitMultiplier1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLaborOvhProfitMultiplier1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustLaborOvhProfitMultiplier1] Is Null And
				DELETED.[CustLaborOvhProfitMultiplier1] Is Not Null
			) Or
			(
				INSERTED.[CustLaborOvhProfitMultiplier1] Is Not Null And
				DELETED.[CustLaborOvhProfitMultiplier1] Is Null
			) Or
			(
				INSERTED.[CustLaborOvhProfitMultiplier1] !=
				DELETED.[CustLaborOvhProfitMultiplier1]
			)
		) 
		END		
		
      If UPDATE([CustLaborRateSchedule])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLaborRateSchedule',
      CONVERT(NVARCHAR(2000),DELETED.[CustLaborRateSchedule],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLaborRateSchedule],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustLaborRateSchedule] Is Null And
				DELETED.[CustLaborRateSchedule] Is Not Null
			) Or
			(
				INSERTED.[CustLaborRateSchedule] Is Not Null And
				DELETED.[CustLaborRateSchedule] Is Null
			) Or
			(
				INSERTED.[CustLaborRateSchedule] !=
				DELETED.[CustLaborRateSchedule]
			)
		) 
		END		
		
      If UPDATE([CustLocationURL1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLocationURL1',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustLSSingleLineProjectBill])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLSSingleLineProjectBill',
      CONVERT(NVARCHAR(2000),DELETED.[CustLSSingleLineProjectBill],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLSSingleLineProjectBill],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustLSSingleLineProjectBill] Is Null And
				DELETED.[CustLSSingleLineProjectBill] Is Not Null
			) Or
			(
				INSERTED.[CustLSSingleLineProjectBill] Is Not Null And
				DELETED.[CustLSSingleLineProjectBill] Is Null
			) Or
			(
				INSERTED.[CustLSSingleLineProjectBill] !=
				DELETED.[CustLSSingleLineProjectBill]
			)
		) 
		END		
		
     If UPDATE([CustManagementLeader1])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustManagementLeader1',
     CONVERT(NVARCHAR(2000),DELETED.[CustManagementLeader1],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustManagementLeader1],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustManagementLeader1] Is Null And
				DELETED.[CustManagementLeader1] Is Not Null
			) Or
			(
				INSERTED.[CustManagementLeader1] Is Not Null And
				DELETED.[CustManagementLeader1] Is Null
			) Or
			(
				INSERTED.[CustManagementLeader1] !=
				DELETED.[CustManagementLeader1]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustManagementLeader1 = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustManagementLeader1 = newDesc.Employee
		END		
		
      If UPDATE([CustMarketable1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketable1',
      CONVERT(NVARCHAR(2000),DELETED.[CustMarketable1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustMarketable1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMarketable1] Is Null And
				DELETED.[CustMarketable1] Is Not Null
			) Or
			(
				INSERTED.[CustMarketable1] Is Not Null And
				DELETED.[CustMarketable1] Is Null
			) Or
			(
				INSERTED.[CustMarketable1] !=
				DELETED.[CustMarketable1]
			)
		) 
		END		
		
      If UPDATE([CustMarketingCloseOutComplete])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingCloseOutComplete',
      CONVERT(NVARCHAR(2000),DELETED.[CustMarketingCloseOutComplete],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingCloseOutComplete],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMarketingCloseOutComplete] Is Null And
				DELETED.[CustMarketingCloseOutComplete] Is Not Null
			) Or
			(
				INSERTED.[CustMarketingCloseOutComplete] Is Not Null And
				DELETED.[CustMarketingCloseOutComplete] Is Null
			) Or
			(
				INSERTED.[CustMarketingCloseOutComplete] !=
				DELETED.[CustMarketingCloseOutComplete]
			)
		) 
		END		
		
     If UPDATE([CustMarketingCoordinator])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingCoordinator',
     CONVERT(NVARCHAR(2000),DELETED.[CustMarketingCoordinator],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingCoordinator],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMarketingCoordinator] Is Null And
				DELETED.[CustMarketingCoordinator] Is Not Null
			) Or
			(
				INSERTED.[CustMarketingCoordinator] Is Not Null And
				DELETED.[CustMarketingCoordinator] Is Null
			) Or
			(
				INSERTED.[CustMarketingCoordinator] !=
				DELETED.[CustMarketingCoordinator]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustMarketingCoordinator = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustMarketingCoordinator = newDesc.Employee
		END		
		
      If UPDATE([CustMarketingInitialReviewComplete])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingInitialReviewComplete',
      CONVERT(NVARCHAR(2000),DELETED.[CustMarketingInitialReviewComplete],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingInitialReviewComplete],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMarketingInitialReviewComplete] Is Null And
				DELETED.[CustMarketingInitialReviewComplete] Is Not Null
			) Or
			(
				INSERTED.[CustMarketingInitialReviewComplete] Is Not Null And
				DELETED.[CustMarketingInitialReviewComplete] Is Null
			) Or
			(
				INSERTED.[CustMarketingInitialReviewComplete] !=
				DELETED.[CustMarketingInitialReviewComplete]
			)
		) 
		END		
		
     If UPDATE([CustMarketingLead])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingLead',
     CONVERT(NVARCHAR(2000),DELETED.[CustMarketingLead],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingLead],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMarketingLead] Is Null And
				DELETED.[CustMarketingLead] Is Not Null
			) Or
			(
				INSERTED.[CustMarketingLead] Is Not Null And
				DELETED.[CustMarketingLead] Is Null
			) Or
			(
				INSERTED.[CustMarketingLead] !=
				DELETED.[CustMarketingLead]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustMarketingLead = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustMarketingLead = newDesc.Employee
		END		
		
     If UPDATE([CustMarketingManager1])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingManager1',
     CONVERT(NVARCHAR(2000),DELETED.[CustMarketingManager1],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingManager1],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMarketingManager1] Is Null And
				DELETED.[CustMarketingManager1] Is Not Null
			) Or
			(
				INSERTED.[CustMarketingManager1] Is Not Null And
				DELETED.[CustMarketingManager1] Is Null
			) Or
			(
				INSERTED.[CustMarketingManager1] !=
				DELETED.[CustMarketingManager1]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustMarketingManager1 = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustMarketingManager1 = newDesc.Employee
		END		
		
      If UPDATE([CustMarketingName1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustMarketingName1',
      CONVERT(NVARCHAR(2000),DELETED.[CustMarketingName1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustMarketingName1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustMarketingName1] Is Null And
				DELETED.[CustMarketingName1] Is Not Null
			) Or
			(
				INSERTED.[CustMarketingName1] Is Not Null And
				DELETED.[CustMarketingName1] Is Null
			) Or
			(
				INSERTED.[CustMarketingName1] !=
				DELETED.[CustMarketingName1]
			)
		) 
		END		
		
      If UPDATE([CustNearmapUsage1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustNearmapUsage1',
      CONVERT(NVARCHAR(2000),DELETED.[CustNearmapUsage1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustNearmapUsage1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustNearmapUsage1] Is Null And
				DELETED.[CustNearmapUsage1] Is Not Null
			) Or
			(
				INSERTED.[CustNearmapUsage1] Is Not Null And
				DELETED.[CustNearmapUsage1] Is Null
			) Or
			(
				INSERTED.[CustNearmapUsage1] !=
				DELETED.[CustNearmapUsage1]
			)
		) 
		END		
		
      If UPDATE([custOMBuyingOrganization])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMBuyingOrganization',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([custOMContractType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMContractType',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([custOMContractVehicles])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMContractVehicles',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([custOMCRMLastUpdateDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMCRMLastUpdateDate',
      CONVERT(NVARCHAR(2000),DELETED.[custOMCRMLastUpdateDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMCRMLastUpdateDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMCRMLastUpdateDate] Is Null And
				DELETED.[custOMCRMLastUpdateDate] Is Not Null
			) Or
			(
				INSERTED.[custOMCRMLastUpdateDate] Is Not Null And
				DELETED.[custOMCRMLastUpdateDate] Is Null
			) Or
			(
				INSERTED.[custOMCRMLastUpdateDate] !=
				DELETED.[custOMCRMLastUpdateDate]
			)
		) 
		END		
		
      If UPDATE([custOMCRMLastUpdateTime])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMCRMLastUpdateTime',
      CONVERT(NVARCHAR(2000),DELETED.[custOMCRMLastUpdateTime],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMCRMLastUpdateTime],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMCRMLastUpdateTime] Is Null And
				DELETED.[custOMCRMLastUpdateTime] Is Not Null
			) Or
			(
				INSERTED.[custOMCRMLastUpdateTime] Is Not Null And
				DELETED.[custOMCRMLastUpdateTime] Is Null
			) Or
			(
				INSERTED.[custOMCRMLastUpdateTime] !=
				DELETED.[custOMCRMLastUpdateTime]
			)
		) 
		END		
		
      If UPDATE([custOMDescription])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMDescription',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([custOMDuration])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMDuration',
      CONVERT(NVARCHAR(2000),DELETED.[custOMDuration],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMDuration],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMDuration] Is Null And
				DELETED.[custOMDuration] Is Not Null
			) Or
			(
				INSERTED.[custOMDuration] Is Not Null And
				DELETED.[custOMDuration] Is Null
			) Or
			(
				INSERTED.[custOMDuration] !=
				DELETED.[custOMDuration]
			)
		) 
		END		
		
      If UPDATE([custOMGovtResponseDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMGovtResponseDate',
      CONVERT(NVARCHAR(2000),DELETED.[custOMGovtResponseDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMGovtResponseDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMGovtResponseDate] Is Null And
				DELETED.[custOMGovtResponseDate] Is Not Null
			) Or
			(
				INSERTED.[custOMGovtResponseDate] Is Not Null And
				DELETED.[custOMGovtResponseDate] Is Null
			) Or
			(
				INSERTED.[custOMGovtResponseDate] !=
				DELETED.[custOMGovtResponseDate]
			)
		) 
		END		
		
      If UPDATE([custOMGovtResponseTime])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMGovtResponseTime',
      CONVERT(NVARCHAR(2000),DELETED.[custOMGovtResponseTime],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMGovtResponseTime],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMGovtResponseTime] Is Null And
				DELETED.[custOMGovtResponseTime] Is Not Null
			) Or
			(
				INSERTED.[custOMGovtResponseTime] Is Not Null And
				DELETED.[custOMGovtResponseTime] Is Null
			) Or
			(
				INSERTED.[custOMGovtResponseTime] !=
				DELETED.[custOMGovtResponseTime]
			)
		) 
		END		
		
      If UPDATE([custOMLastUpdateTime])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMLastUpdateTime',
      CONVERT(NVARCHAR(2000),DELETED.[custOMLastUpdateTime],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMLastUpdateTime],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMLastUpdateTime] Is Null And
				DELETED.[custOMLastUpdateTime] Is Not Null
			) Or
			(
				INSERTED.[custOMLastUpdateTime] Is Not Null And
				DELETED.[custOMLastUpdateTime] Is Null
			) Or
			(
				INSERTED.[custOMLastUpdateTime] !=
				DELETED.[custOMLastUpdateTime]
			)
		) 
		END		
		
      If UPDATE([custOMNAICSCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMNAICSCode',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([custOMOppLastUpdateDt])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMOppLastUpdateDt',
      CONVERT(NVARCHAR(2000),DELETED.[custOMOppLastUpdateDt],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMOppLastUpdateDt],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMOppLastUpdateDt] Is Null And
				DELETED.[custOMOppLastUpdateDt] Is Not Null
			) Or
			(
				INSERTED.[custOMOppLastUpdateDt] Is Not Null And
				DELETED.[custOMOppLastUpdateDt] Is Null
			) Or
			(
				INSERTED.[custOMOppLastUpdateDt] !=
				DELETED.[custOMOppLastUpdateDt]
			)
		) 
		END		
		
      If UPDATE([custOMOppManagerStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMOppManagerStatus',
      CONVERT(NVARCHAR(2000),DELETED.[custOMOppManagerStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMOppManagerStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMOppManagerStatus] Is Null And
				DELETED.[custOMOppManagerStatus] Is Not Null
			) Or
			(
				INSERTED.[custOMOppManagerStatus] Is Not Null And
				DELETED.[custOMOppManagerStatus] Is Null
			) Or
			(
				INSERTED.[custOMOppManagerStatus] !=
				DELETED.[custOMOppManagerStatus]
			)
		) 
		END		
		
      If UPDATE([custOMOpportunityID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMOpportunityID',
      CONVERT(NVARCHAR(2000),DELETED.[custOMOpportunityID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMOpportunityID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMOpportunityID] Is Null And
				DELETED.[custOMOpportunityID] Is Not Null
			) Or
			(
				INSERTED.[custOMOpportunityID] Is Not Null And
				DELETED.[custOMOpportunityID] Is Null
			) Or
			(
				INSERTED.[custOMOpportunityID] !=
				DELETED.[custOMOpportunityID]
			)
		) 
		END		
		
      If UPDATE([custOMOpportunityTitle])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMOpportunityTitle',
      CONVERT(NVARCHAR(2000),DELETED.[custOMOpportunityTitle],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMOpportunityTitle],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMOpportunityTitle] Is Null And
				DELETED.[custOMOpportunityTitle] Is Not Null
			) Or
			(
				INSERTED.[custOMOpportunityTitle] Is Not Null And
				DELETED.[custOMOpportunityTitle] Is Null
			) Or
			(
				INSERTED.[custOMOpportunityTitle] !=
				DELETED.[custOMOpportunityTitle]
			)
		) 
		END		
		
      If UPDATE([custOMOppURL])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMOppURL',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([custOMPartnerResponseDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMPartnerResponseDate',
      CONVERT(NVARCHAR(2000),DELETED.[custOMPartnerResponseDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMPartnerResponseDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMPartnerResponseDate] Is Null And
				DELETED.[custOMPartnerResponseDate] Is Not Null
			) Or
			(
				INSERTED.[custOMPartnerResponseDate] Is Not Null And
				DELETED.[custOMPartnerResponseDate] Is Null
			) Or
			(
				INSERTED.[custOMPartnerResponseDate] !=
				DELETED.[custOMPartnerResponseDate]
			)
		) 
		END		
		
      If UPDATE([custOMPartnerResponseTime])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMPartnerResponseTime',
      CONVERT(NVARCHAR(2000),DELETED.[custOMPartnerResponseTime],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMPartnerResponseTime],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMPartnerResponseTime] Is Null And
				DELETED.[custOMPartnerResponseTime] Is Not Null
			) Or
			(
				INSERTED.[custOMPartnerResponseTime] Is Not Null And
				DELETED.[custOMPartnerResponseTime] Is Null
			) Or
			(
				INSERTED.[custOMPartnerResponseTime] !=
				DELETED.[custOMPartnerResponseTime]
			)
		) 
		END		
		
      If UPDATE([custOMPlaceofPerformance])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMPlaceofPerformance',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([custOMPrimaryContact])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMPrimaryContact',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([custOMSelectedBy])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMSelectedBy',
      CONVERT(NVARCHAR(2000),DELETED.[custOMSelectedBy],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMSelectedBy],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMSelectedBy] Is Null And
				DELETED.[custOMSelectedBy] Is Not Null
			) Or
			(
				INSERTED.[custOMSelectedBy] Is Not Null And
				DELETED.[custOMSelectedBy] Is Null
			) Or
			(
				INSERTED.[custOMSelectedBy] !=
				DELETED.[custOMSelectedBy]
			)
		) 
		END		
		
      If UPDATE([custOMSocioEconomicStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMSocioEconomicStatus',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([custOMSolicitationDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMSolicitationDate',
      CONVERT(NVARCHAR(2000),DELETED.[custOMSolicitationDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMSolicitationDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMSolicitationDate] Is Null And
				DELETED.[custOMSolicitationDate] Is Not Null
			) Or
			(
				INSERTED.[custOMSolicitationDate] Is Not Null And
				DELETED.[custOMSolicitationDate] Is Null
			) Or
			(
				INSERTED.[custOMSolicitationDate] !=
				DELETED.[custOMSolicitationDate]
			)
		) 
		END		
		
      If UPDATE([custOMSolicitationNum])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMSolicitationNum',
      CONVERT(NVARCHAR(2000),DELETED.[custOMSolicitationNum],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMSolicitationNum],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMSolicitationNum] Is Null And
				DELETED.[custOMSolicitationNum] Is Not Null
			) Or
			(
				INSERTED.[custOMSolicitationNum] Is Not Null And
				DELETED.[custOMSolicitationNum] Is Null
			) Or
			(
				INSERTED.[custOMSolicitationNum] !=
				DELETED.[custOMSolicitationNum]
			)
		) 
		END		
		
      If UPDATE([custOMTeamingOppSource])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMTeamingOppSource',
      CONVERT(NVARCHAR(2000),DELETED.[custOMTeamingOppSource],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMTeamingOppSource],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMTeamingOppSource] Is Null And
				DELETED.[custOMTeamingOppSource] Is Not Null
			) Or
			(
				INSERTED.[custOMTeamingOppSource] Is Not Null And
				DELETED.[custOMTeamingOppSource] Is Null
			) Or
			(
				INSERTED.[custOMTeamingOppSource] !=
				DELETED.[custOMTeamingOppSource]
			)
		) 
		END		
		
      If UPDATE([custOMTypeofAward])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMTypeofAward',
      CONVERT(NVARCHAR(2000),DELETED.[custOMTypeofAward],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMTypeofAward],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMTypeofAward] Is Null And
				DELETED.[custOMTypeofAward] Is Not Null
			) Or
			(
				INSERTED.[custOMTypeofAward] Is Not Null And
				DELETED.[custOMTypeofAward] Is Null
			) Or
			(
				INSERTED.[custOMTypeofAward] !=
				DELETED.[custOMTypeofAward]
			)
		) 
		END		
		
      If UPDATE([custOMValue])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'custOMValue',
      CONVERT(NVARCHAR(2000),DELETED.[custOMValue],121),
      CONVERT(NVARCHAR(2000),INSERTED.[custOMValue],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[custOMValue] Is Null And
				DELETED.[custOMValue] Is Not Null
			) Or
			(
				INSERTED.[custOMValue] Is Not Null And
				DELETED.[custOMValue] Is Null
			) Or
			(
				INSERTED.[custOMValue] !=
				DELETED.[custOMValue]
			)
		) 
		END		
		
     If UPDATE([CustOperationsManager1])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustOperationsManager1',
     CONVERT(NVARCHAR(2000),DELETED.[CustOperationsManager1],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustOperationsManager1],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustOperationsManager1] Is Null And
				DELETED.[CustOperationsManager1] Is Not Null
			) Or
			(
				INSERTED.[CustOperationsManager1] Is Not Null And
				DELETED.[CustOperationsManager1] Is Null
			) Or
			(
				INSERTED.[CustOperationsManager1] !=
				DELETED.[CustOperationsManager1]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustOperationsManager1 = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustOperationsManager1 = newDesc.Employee
		END		
		
      If UPDATE([CustPAResponsibleforAR])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPAResponsibleforAR',
      CONVERT(NVARCHAR(2000),DELETED.[CustPAResponsibleforAR],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPAResponsibleforAR],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustPAResponsibleforAR] Is Null And
				DELETED.[CustPAResponsibleforAR] Is Not Null
			) Or
			(
				INSERTED.[CustPAResponsibleforAR] Is Not Null And
				DELETED.[CustPAResponsibleforAR] Is Null
			) Or
			(
				INSERTED.[CustPAResponsibleforAR] !=
				DELETED.[CustPAResponsibleforAR]
			)
		) 
		END		
		
      If UPDATE([CustPracticeArea1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPracticeArea1',
      CONVERT(NVARCHAR(2000),DELETED.[CustPracticeArea1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPracticeArea1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustPracticeArea1] Is Null And
				DELETED.[CustPracticeArea1] Is Not Null
			) Or
			(
				INSERTED.[CustPracticeArea1] Is Not Null And
				DELETED.[CustPracticeArea1] Is Null
			) Or
			(
				INSERTED.[CustPracticeArea1] !=
				DELETED.[CustPracticeArea1]
			)
		) 
		END		
		
     If UPDATE([CustPracticeAreaLeader1])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPracticeAreaLeader1',
     CONVERT(NVARCHAR(2000),DELETED.[CustPracticeAreaLeader1],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustPracticeAreaLeader1],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustPracticeAreaLeader1] Is Null And
				DELETED.[CustPracticeAreaLeader1] Is Not Null
			) Or
			(
				INSERTED.[CustPracticeAreaLeader1] Is Not Null And
				DELETED.[CustPracticeAreaLeader1] Is Null
			) Or
			(
				INSERTED.[CustPracticeAreaLeader1] !=
				DELETED.[CustPracticeAreaLeader1]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustPracticeAreaLeader1 = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustPracticeAreaLeader1 = newDesc.Employee
		END		
		
     If UPDATE([CustProjectType1])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustProjectType1',
     CONVERT(NVARCHAR(2000),DELETED.[CustProjectType1],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustProjectType1],121),
     IsNull(oldDesc.CustProjectTypeName, N''), IsNull(newDesc.CustProjectTypeName, N''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustProjectType1] Is Null And
				DELETED.[CustProjectType1] Is Not Null
			) Or
			(
				INSERTED.[CustProjectType1] Is Not Null And
				DELETED.[CustProjectType1] Is Null
			) Or
			(
				INSERTED.[CustProjectType1] !=
				DELETED.[CustProjectType1]
			)
		) left join UDIC_ProjectType as oldDesc  on DELETED.CustProjectType1 = oldDesc.UDIC_UID  left join  UDIC_ProjectType as newDesc  on INSERTED.CustProjectType1 = newDesc.UDIC_UID
		END		
		
      If UPDATE([CustReimbConsultantMultiplier1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustReimbConsultantMultiplier1',
      CONVERT(NVARCHAR(2000),DELETED.[CustReimbConsultantMultiplier1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustReimbConsultantMultiplier1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustReimbConsultantMultiplier1] Is Null And
				DELETED.[CustReimbConsultantMultiplier1] Is Not Null
			) Or
			(
				INSERTED.[CustReimbConsultantMultiplier1] Is Not Null And
				DELETED.[CustReimbConsultantMultiplier1] Is Null
			) Or
			(
				INSERTED.[CustReimbConsultantMultiplier1] !=
				DELETED.[CustReimbConsultantMultiplier1]
			)
		) 
		END		
		
      If UPDATE([CustReimbExpenseMultiplier1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustReimbExpenseMultiplier1',
      CONVERT(NVARCHAR(2000),DELETED.[CustReimbExpenseMultiplier1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustReimbExpenseMultiplier1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustReimbExpenseMultiplier1] Is Null And
				DELETED.[CustReimbExpenseMultiplier1] Is Not Null
			) Or
			(
				INSERTED.[CustReimbExpenseMultiplier1] Is Not Null And
				DELETED.[CustReimbExpenseMultiplier1] Is Null
			) Or
			(
				INSERTED.[CustReimbExpenseMultiplier1] !=
				DELETED.[CustReimbExpenseMultiplier1]
			)
		) 
		END		
		
      If UPDATE([CustRevenueConsultFee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRevenueConsultFee',
      CONVERT(NVARCHAR(2000),DELETED.[CustRevenueConsultFee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRevenueConsultFee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustRevenueConsultFee] Is Null And
				DELETED.[CustRevenueConsultFee] Is Not Null
			) Or
			(
				INSERTED.[CustRevenueConsultFee] Is Not Null And
				DELETED.[CustRevenueConsultFee] Is Null
			) Or
			(
				INSERTED.[CustRevenueConsultFee] !=
				DELETED.[CustRevenueConsultFee]
			)
		) 
		END		
		
      If UPDATE([CustRevenueFeeDirExp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRevenueFeeDirExp',
      CONVERT(NVARCHAR(2000),DELETED.[CustRevenueFeeDirExp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRevenueFeeDirExp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustRevenueFeeDirExp] Is Null And
				DELETED.[CustRevenueFeeDirExp] Is Not Null
			) Or
			(
				INSERTED.[CustRevenueFeeDirExp] Is Not Null And
				DELETED.[CustRevenueFeeDirExp] Is Null
			) Or
			(
				INSERTED.[CustRevenueFeeDirExp] !=
				DELETED.[CustRevenueFeeDirExp]
			)
		) 
		END		
		
      If UPDATE([CustRevenueFeeDirLab])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRevenueFeeDirLab',
      CONVERT(NVARCHAR(2000),DELETED.[CustRevenueFeeDirLab],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRevenueFeeDirLab],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustRevenueFeeDirLab] Is Null And
				DELETED.[CustRevenueFeeDirLab] Is Not Null
			) Or
			(
				INSERTED.[CustRevenueFeeDirLab] Is Not Null And
				DELETED.[CustRevenueFeeDirLab] Is Null
			) Or
			(
				INSERTED.[CustRevenueFeeDirLab] !=
				DELETED.[CustRevenueFeeDirLab]
			)
		) 
		END		
		
      If UPDATE([CustRevenueReimbAllowCons])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRevenueReimbAllowCons',
      CONVERT(NVARCHAR(2000),DELETED.[CustRevenueReimbAllowCons],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRevenueReimbAllowCons],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustRevenueReimbAllowCons] Is Null And
				DELETED.[CustRevenueReimbAllowCons] Is Not Null
			) Or
			(
				INSERTED.[CustRevenueReimbAllowCons] Is Not Null And
				DELETED.[CustRevenueReimbAllowCons] Is Null
			) Or
			(
				INSERTED.[CustRevenueReimbAllowCons] !=
				DELETED.[CustRevenueReimbAllowCons]
			)
		) 
		END		
		
      If UPDATE([CustRevenueReimbAllowExp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRevenueReimbAllowExp',
      CONVERT(NVARCHAR(2000),DELETED.[CustRevenueReimbAllowExp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRevenueReimbAllowExp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustRevenueReimbAllowExp] Is Null And
				DELETED.[CustRevenueReimbAllowExp] Is Not Null
			) Or
			(
				INSERTED.[CustRevenueReimbAllowExp] Is Not Null And
				DELETED.[CustRevenueReimbAllowExp] Is Null
			) Or
			(
				INSERTED.[CustRevenueReimbAllowExp] !=
				DELETED.[CustRevenueReimbAllowExp]
			)
		) 
		END		
		
      If UPDATE([CustRFPSubmitted])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRFPSubmitted',
      CONVERT(NVARCHAR(2000),DELETED.[CustRFPSubmitted],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRFPSubmitted],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustRFPSubmitted] Is Null And
				DELETED.[CustRFPSubmitted] Is Not Null
			) Or
			(
				INSERTED.[CustRFPSubmitted] Is Not Null And
				DELETED.[CustRFPSubmitted] Is Null
			) Or
			(
				INSERTED.[CustRFPSubmitted] !=
				DELETED.[CustRFPSubmitted]
			)
		) 
		END		
		
      If UPDATE([CustRFQAnticipated])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRFQAnticipated',
      CONVERT(NVARCHAR(2000),DELETED.[CustRFQAnticipated],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRFQAnticipated],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustRFQAnticipated] Is Null And
				DELETED.[CustRFQAnticipated] Is Not Null
			) Or
			(
				INSERTED.[CustRFQAnticipated] Is Not Null And
				DELETED.[CustRFQAnticipated] Is Null
			) Or
			(
				INSERTED.[CustRFQAnticipated] !=
				DELETED.[CustRFQAnticipated]
			)
		) 
		END		
		
      If UPDATE([CustRFQRelease])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRFQRelease',
      CONVERT(NVARCHAR(2000),DELETED.[CustRFQRelease],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRFQRelease],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustRFQRelease] Is Null And
				DELETED.[CustRFQRelease] Is Not Null
			) Or
			(
				INSERTED.[CustRFQRelease] Is Not Null And
				DELETED.[CustRFQRelease] Is Null
			) Or
			(
				INSERTED.[CustRFQRelease] !=
				DELETED.[CustRFQRelease]
			)
		) 
		END		
		
      If UPDATE([CustRFQSelected])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRFQSelected',
      CONVERT(NVARCHAR(2000),DELETED.[CustRFQSelected],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRFQSelected],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustRFQSelected] Is Null And
				DELETED.[CustRFQSelected] Is Not Null
			) Or
			(
				INSERTED.[CustRFQSelected] Is Not Null And
				DELETED.[CustRFQSelected] Is Null
			) Or
			(
				INSERTED.[CustRFQSelected] !=
				DELETED.[CustRFQSelected]
			)
		) 
		END		
		
      If UPDATE([CustRFQSubmitted])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustRFQSubmitted',
      CONVERT(NVARCHAR(2000),DELETED.[CustRFQSubmitted],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRFQSubmitted],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustRFQSubmitted] Is Null And
				DELETED.[CustRFQSubmitted] Is Not Null
			) Or
			(
				INSERTED.[CustRFQSubmitted] Is Not Null And
				DELETED.[CustRFQSubmitted] Is Null
			) Or
			(
				INSERTED.[CustRFQSubmitted] !=
				DELETED.[CustRFQSubmitted]
			)
		) 
		END		
		
      If UPDATE([CustSelectionProcess1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSelectionProcess1',
      CONVERT(NVARCHAR(2000),DELETED.[CustSelectionProcess1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSelectionProcess1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustSelectionProcess1] Is Null And
				DELETED.[CustSelectionProcess1] Is Not Null
			) Or
			(
				INSERTED.[CustSelectionProcess1] Is Not Null And
				DELETED.[CustSelectionProcess1] Is Null
			) Or
			(
				INSERTED.[CustSelectionProcess1] !=
				DELETED.[CustSelectionProcess1]
			)
		) 
		END		
		
     If UPDATE([CustService1])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustService1',
     CONVERT(NVARCHAR(2000),DELETED.[CustService1],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustService1],121),
     IsNull(oldDesc.CustServiceName, N''), IsNull(newDesc.CustServiceName, N''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustService1] Is Null And
				DELETED.[CustService1] Is Not Null
			) Or
			(
				INSERTED.[CustService1] Is Not Null And
				DELETED.[CustService1] Is Null
			) Or
			(
				INSERTED.[CustService1] !=
				DELETED.[CustService1]
			)
		) left join UDIC_Service as oldDesc  on DELETED.CustService1 = oldDesc.UDIC_UID  left join  UDIC_Service as newDesc  on INSERTED.CustService1 = newDesc.UDIC_UID
		END		
		
      If UPDATE([CustShortlisted1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustShortlisted1',
      CONVERT(NVARCHAR(2000),DELETED.[CustShortlisted1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustShortlisted1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustShortlisted1] Is Null And
				DELETED.[CustShortlisted1] Is Not Null
			) Or
			(
				INSERTED.[CustShortlisted1] Is Not Null And
				DELETED.[CustShortlisted1] Is Null
			) Or
			(
				INSERTED.[CustShortlisted1] !=
				DELETED.[CustShortlisted1]
			)
		) 
		END		
		
      If UPDATE([CustStrategicPursuit])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustStrategicPursuit',
      CONVERT(NVARCHAR(2000),DELETED.[CustStrategicPursuit],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustStrategicPursuit],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustStrategicPursuit] Is Null And
				DELETED.[CustStrategicPursuit] Is Not Null
			) Or
			(
				INSERTED.[CustStrategicPursuit] Is Not Null And
				DELETED.[CustStrategicPursuit] Is Null
			) Or
			(
				INSERTED.[CustStrategicPursuit] !=
				DELETED.[CustStrategicPursuit]
			)
		) 
		END		
		
     If UPDATE([CustStrategicPursuitsManager])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustStrategicPursuitsManager',
     CONVERT(NVARCHAR(2000),DELETED.[CustStrategicPursuitsManager],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustStrategicPursuitsManager],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustStrategicPursuitsManager] Is Null And
				DELETED.[CustStrategicPursuitsManager] Is Not Null
			) Or
			(
				INSERTED.[CustStrategicPursuitsManager] Is Not Null And
				DELETED.[CustStrategicPursuitsManager] Is Null
			) Or
			(
				INSERTED.[CustStrategicPursuitsManager] !=
				DELETED.[CustStrategicPursuitsManager]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustStrategicPursuitsManager = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustStrategicPursuitsManager = newDesc.Employee
		END		
		
      If UPDATE([CustSubmittalDeadline])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSubmittalDeadline',
      CONVERT(NVARCHAR(2000),DELETED.[CustSubmittalDeadline],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSubmittalDeadline],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustSubmittalDeadline] Is Null And
				DELETED.[CustSubmittalDeadline] Is Not Null
			) Or
			(
				INSERTED.[CustSubmittalDeadline] Is Not Null And
				DELETED.[CustSubmittalDeadline] Is Null
			) Or
			(
				INSERTED.[CustSubmittalDeadline] !=
				DELETED.[CustSubmittalDeadline]
			)
		) 
		END		
		
      If UPDATE([CustTaxCode1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustTaxCode1',
      CONVERT(NVARCHAR(2000),DELETED.[CustTaxCode1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTaxCode1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustTaxCode1] Is Null And
				DELETED.[CustTaxCode1] Is Not Null
			) Or
			(
				INSERTED.[CustTaxCode1] Is Not Null And
				DELETED.[CustTaxCode1] Is Null
			) Or
			(
				INSERTED.[CustTaxCode1] !=
				DELETED.[CustTaxCode1]
			)
		) 
		END		
		
      If UPDATE([CustTeamPracticeArea1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustTeamPracticeArea1',
      CONVERT(NVARCHAR(2000),DELETED.[CustTeamPracticeArea1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTeamPracticeArea1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustTeamPracticeArea1] Is Null And
				DELETED.[CustTeamPracticeArea1] Is Not Null
			) Or
			(
				INSERTED.[CustTeamPracticeArea1] Is Not Null And
				DELETED.[CustTeamPracticeArea1] Is Null
			) Or
			(
				INSERTED.[CustTeamPracticeArea1] !=
				DELETED.[CustTeamPracticeArea1]
			)
		) 
		END		
		
      If UPDATE([CustTotalLaborMultiplier])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustTotalLaborMultiplier',
      CONVERT(NVARCHAR(2000),DELETED.[CustTotalLaborMultiplier],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTotalLaborMultiplier],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustTotalLaborMultiplier] Is Null And
				DELETED.[CustTotalLaborMultiplier] Is Not Null
			) Or
			(
				INSERTED.[CustTotalLaborMultiplier] Is Not Null And
				DELETED.[CustTotalLaborMultiplier] Is Null
			) Or
			(
				INSERTED.[CustTotalLaborMultiplier] !=
				DELETED.[CustTotalLaborMultiplier]
			)
		) 
		END		
		
      If UPDATE([CustCollectionsResponsibilitySetup])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCollectionsResponsibilitySetup',
      CONVERT(NVARCHAR(2000),DELETED.[CustCollectionsResponsibilitySetup],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCollectionsResponsibilitySetup],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCollectionsResponsibilitySetup] Is Null And
				DELETED.[CustCollectionsResponsibilitySetup] Is Not Null
			) Or
			(
				INSERTED.[CustCollectionsResponsibilitySetup] Is Not Null And
				DELETED.[CustCollectionsResponsibilitySetup] Is Null
			) Or
			(
				INSERTED.[CustCollectionsResponsibilitySetup] !=
				DELETED.[CustCollectionsResponsibilitySetup]
			)
		) 
		END		
		
      If UPDATE([CustSetupLaborFringeMultiplier])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSetupLaborFringeMultiplier',
      CONVERT(NVARCHAR(2000),DELETED.[CustSetupLaborFringeMultiplier],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSetupLaborFringeMultiplier],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustSetupLaborFringeMultiplier] Is Null And
				DELETED.[CustSetupLaborFringeMultiplier] Is Not Null
			) Or
			(
				INSERTED.[CustSetupLaborFringeMultiplier] Is Not Null And
				DELETED.[CustSetupLaborFringeMultiplier] Is Null
			) Or
			(
				INSERTED.[CustSetupLaborFringeMultiplier] !=
				DELETED.[CustSetupLaborFringeMultiplier]
			)
		) 
		END		
		
      If UPDATE([CustSetupReimbConsultantMultiplier])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSetupReimbConsultantMultiplier',
      CONVERT(NVARCHAR(2000),DELETED.[CustSetupReimbConsultantMultiplier],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSetupReimbConsultantMultiplier],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustSetupReimbConsultantMultiplier] Is Null And
				DELETED.[CustSetupReimbConsultantMultiplier] Is Not Null
			) Or
			(
				INSERTED.[CustSetupReimbConsultantMultiplier] Is Not Null And
				DELETED.[CustSetupReimbConsultantMultiplier] Is Null
			) Or
			(
				INSERTED.[CustSetupReimbConsultantMultiplier] !=
				DELETED.[CustSetupReimbConsultantMultiplier]
			)
		) 
		END		
		
      If UPDATE([CustEstStartDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEstStartDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustEstStartDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEstStartDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEstStartDate] Is Null And
				DELETED.[CustEstStartDate] Is Not Null
			) Or
			(
				INSERTED.[CustEstStartDate] Is Not Null And
				DELETED.[CustEstStartDate] Is Null
			) Or
			(
				INSERTED.[CustEstStartDate] !=
				DELETED.[CustEstStartDate]
			)
		) 
		END		
		
     If UPDATE([CustEvalCreator])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEvalCreator',
     CONVERT(NVARCHAR(2000),DELETED.[CustEvalCreator],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustEvalCreator],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEvalCreator] Is Null And
				DELETED.[CustEvalCreator] Is Not Null
			) Or
			(
				INSERTED.[CustEvalCreator] Is Not Null And
				DELETED.[CustEvalCreator] Is Null
			) Or
			(
				INSERTED.[CustEvalCreator] !=
				DELETED.[CustEvalCreator]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustEvalCreator = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustEvalCreator = newDesc.Employee
		END		
		
      If UPDATE([CustPursuitComponents])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPursuitComponents',
      CONVERT(NVARCHAR(2000),DELETED.[CustPursuitComponents],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPursuitComponents],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustPursuitComponents] Is Null And
				DELETED.[CustPursuitComponents] Is Not Null
			) Or
			(
				INSERTED.[CustPursuitComponents] Is Not Null And
				DELETED.[CustPursuitComponents] Is Null
			) Or
			(
				INSERTED.[CustPursuitComponents] !=
				DELETED.[CustPursuitComponents]
			)
		) 
		END		
		
      If UPDATE([CustPursuitPlan])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPursuitPlan',
      CONVERT(NVARCHAR(2000),DELETED.[CustPursuitPlan],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPursuitPlan],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustPursuitPlan] Is Null And
				DELETED.[CustPursuitPlan] Is Not Null
			) Or
			(
				INSERTED.[CustPursuitPlan] Is Not Null And
				DELETED.[CustPursuitPlan] Is Null
			) Or
			(
				INSERTED.[CustPursuitPlan] !=
				DELETED.[CustPursuitPlan]
			)
		) 
		END		
		
      If UPDATE([CustFinancialPotential])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFinancialPotential',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustFeeTypeRollupDuplicate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustFeeTypeRollupDuplicate',
      CONVERT(NVARCHAR(2000),DELETED.[CustFeeTypeRollupDuplicate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustFeeTypeRollupDuplicate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustFeeTypeRollupDuplicate] Is Null And
				DELETED.[CustFeeTypeRollupDuplicate] Is Not Null
			) Or
			(
				INSERTED.[CustFeeTypeRollupDuplicate] Is Not Null And
				DELETED.[CustFeeTypeRollupDuplicate] Is Null
			) Or
			(
				INSERTED.[CustFeeTypeRollupDuplicate] !=
				DELETED.[CustFeeTypeRollupDuplicate]
			)
		) 
		END		
		
      If UPDATE([CustCurrentlyWorking])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustCurrentlyWorking',
      CONVERT(NVARCHAR(2000),DELETED.[CustCurrentlyWorking],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCurrentlyWorking],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustCurrentlyWorking] Is Null And
				DELETED.[CustCurrentlyWorking] Is Not Null
			) Or
			(
				INSERTED.[CustCurrentlyWorking] Is Not Null And
				DELETED.[CustCurrentlyWorking] Is Null
			) Or
			(
				INSERTED.[CustCurrentlyWorking] !=
				DELETED.[CustCurrentlyWorking]
			)
		) 
		END		
		
      If UPDATE([CustObstacles])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustObstacles',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustWriteoffAdjmtDuplicate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustWriteoffAdjmtDuplicate',
      CONVERT(NVARCHAR(2000),DELETED.[CustWriteoffAdjmtDuplicate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustWriteoffAdjmtDuplicate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustWriteoffAdjmtDuplicate] Is Null And
				DELETED.[CustWriteoffAdjmtDuplicate] Is Not Null
			) Or
			(
				INSERTED.[CustWriteoffAdjmtDuplicate] Is Not Null And
				DELETED.[CustWriteoffAdjmtDuplicate] Is Null
			) Or
			(
				INSERTED.[CustWriteoffAdjmtDuplicate] !=
				DELETED.[CustWriteoffAdjmtDuplicate]
			)
		) 
		END		
		
      If UPDATE([CustSetupLaborOverheadProfitMultiplier])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSetupLaborOverheadProfitMultiplier',
      CONVERT(NVARCHAR(2000),DELETED.[CustSetupLaborOverheadProfitMultiplier],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSetupLaborOverheadProfitMultiplier],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustSetupLaborOverheadProfitMultiplier] Is Null And
				DELETED.[CustSetupLaborOverheadProfitMultiplier] Is Not Null
			) Or
			(
				INSERTED.[CustSetupLaborOverheadProfitMultiplier] Is Not Null And
				DELETED.[CustSetupLaborOverheadProfitMultiplier] Is Null
			) Or
			(
				INSERTED.[CustSetupLaborOverheadProfitMultiplier] !=
				DELETED.[CustSetupLaborOverheadProfitMultiplier]
			)
		) 
		END		
		
      If UPDATE([CustInterviewDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustInterviewDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustInterviewDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustInterviewDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustInterviewDate] Is Null And
				DELETED.[CustInterviewDate] Is Not Null
			) Or
			(
				INSERTED.[CustInterviewDate] Is Not Null And
				DELETED.[CustInterviewDate] Is Null
			) Or
			(
				INSERTED.[CustInterviewDate] !=
				DELETED.[CustInterviewDate]
			)
		) 
		END		
		
      If UPDATE([CustEffortatCompletionDuplicate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEffortatCompletionDuplicate',
      CONVERT(NVARCHAR(2000),DELETED.[CustEffortatCompletionDuplicate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEffortatCompletionDuplicate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEffortatCompletionDuplicate] Is Null And
				DELETED.[CustEffortatCompletionDuplicate] Is Not Null
			) Or
			(
				INSERTED.[CustEffortatCompletionDuplicate] Is Not Null And
				DELETED.[CustEffortatCompletionDuplicate] Is Null
			) Or
			(
				INSERTED.[CustEffortatCompletionDuplicate] !=
				DELETED.[CustEffortatCompletionDuplicate]
			)
		) 
		END		
		
      If UPDATE([CustClientMeetingCount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustClientMeetingCount',
      CONVERT(NVARCHAR(2000),DELETED.[CustClientMeetingCount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustClientMeetingCount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustClientMeetingCount] Is Null And
				DELETED.[CustClientMeetingCount] Is Not Null
			) Or
			(
				INSERTED.[CustClientMeetingCount] Is Not Null And
				DELETED.[CustClientMeetingCount] Is Null
			) Or
			(
				INSERTED.[CustClientMeetingCount] !=
				DELETED.[CustClientMeetingCount]
			)
		) 
		END		
		
      If UPDATE([CustEstCompletionDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEstCompletionDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustEstCompletionDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEstCompletionDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEstCompletionDate] Is Null And
				DELETED.[CustEstCompletionDate] Is Not Null
			) Or
			(
				INSERTED.[CustEstCompletionDate] Is Not Null And
				DELETED.[CustEstCompletionDate] Is Null
			) Or
			(
				INSERTED.[CustEstCompletionDate] !=
				DELETED.[CustEstCompletionDate]
			)
		) 
		END		
		
     If UPDATE([CustLocalOffice])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustLocalOffice',
     CONVERT(NVARCHAR(2000),DELETED.[CustLocalOffice],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustLocalOffice],121),
     IsNull(oldDesc.CustOfficeName, N''), IsNull(newDesc.CustOfficeName, N''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustLocalOffice] Is Null And
				DELETED.[CustLocalOffice] Is Not Null
			) Or
			(
				INSERTED.[CustLocalOffice] Is Not Null And
				DELETED.[CustLocalOffice] Is Null
			) Or
			(
				INSERTED.[CustLocalOffice] !=
				DELETED.[CustLocalOffice]
			)
		) left join UDIC_Office as oldDesc  on DELETED.CustLocalOffice = oldDesc.UDIC_UID  left join  UDIC_Office as newDesc  on INSERTED.CustLocalOffice = newDesc.UDIC_UID
		END		
		
      If UPDATE([CustPursuitEffort])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPursuitEffort',
      CONVERT(NVARCHAR(2000),DELETED.[CustPursuitEffort],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPursuitEffort],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustPursuitEffort] Is Null And
				DELETED.[CustPursuitEffort] Is Not Null
			) Or
			(
				INSERTED.[CustPursuitEffort] Is Not Null And
				DELETED.[CustPursuitEffort] Is Null
			) Or
			(
				INSERTED.[CustPursuitEffort] !=
				DELETED.[CustPursuitEffort]
			)
		) 
		END		
		
      If UPDATE([CustGoNoGoEmp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustGoNoGoEmp',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustTeaming])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustTeaming',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustEfforttoCompleteDuplicate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustEfforttoCompleteDuplicate',
      CONVERT(NVARCHAR(2000),DELETED.[CustEfforttoCompleteDuplicate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustEfforttoCompleteDuplicate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustEfforttoCompleteDuplicate] Is Null And
				DELETED.[CustEfforttoCompleteDuplicate] Is Not Null
			) Or
			(
				INSERTED.[CustEfforttoCompleteDuplicate] Is Not Null And
				DELETED.[CustEfforttoCompleteDuplicate] Is Null
			) Or
			(
				INSERTED.[CustEfforttoCompleteDuplicate] !=
				DELETED.[CustEfforttoCompleteDuplicate]
			)
		) 
		END		
		
      If UPDATE([CustContractAwarded])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustContractAwarded',
      CONVERT(NVARCHAR(2000),DELETED.[CustContractAwarded],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustContractAwarded],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustContractAwarded] Is Null And
				DELETED.[CustContractAwarded] Is Not Null
			) Or
			(
				INSERTED.[CustContractAwarded] Is Not Null And
				DELETED.[CustContractAwarded] Is Null
			) Or
			(
				INSERTED.[CustContractAwarded] !=
				DELETED.[CustContractAwarded]
			)
		) 
		END		
		
      If UPDATE([CustClientMeeting])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustClientMeeting',
      CONVERT(NVARCHAR(2000),DELETED.[CustClientMeeting],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustClientMeeting],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustClientMeeting] Is Null And
				DELETED.[CustClientMeeting] Is Not Null
			) Or
			(
				INSERTED.[CustClientMeeting] Is Not Null And
				DELETED.[CustClientMeeting] Is Null
			) Or
			(
				INSERTED.[CustClientMeeting] !=
				DELETED.[CustClientMeeting]
			)
		) 
		END		
		
      If UPDATE([CustWinDiff])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustWinDiff',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustSetupTaxCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSetupTaxCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustSetupTaxCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSetupTaxCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustSetupTaxCode] Is Null And
				DELETED.[CustSetupTaxCode] Is Not Null
			) Or
			(
				INSERTED.[CustSetupTaxCode] Is Not Null And
				DELETED.[CustSetupTaxCode] Is Null
			) Or
			(
				INSERTED.[CustSetupTaxCode] !=
				DELETED.[CustSetupTaxCode]
			)
		) 
		END		
		
      If UPDATE([CustSetupReimbExpenseMultiplier])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSetupReimbExpenseMultiplier',
      CONVERT(NVARCHAR(2000),DELETED.[CustSetupReimbExpenseMultiplier],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSetupReimbExpenseMultiplier],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustSetupReimbExpenseMultiplier] Is Null And
				DELETED.[CustSetupReimbExpenseMultiplier] Is Not Null
			) Or
			(
				INSERTED.[CustSetupReimbExpenseMultiplier] Is Not Null And
				DELETED.[CustSetupReimbExpenseMultiplier] Is Null
			) Or
			(
				INSERTED.[CustSetupReimbExpenseMultiplier] !=
				DELETED.[CustSetupReimbExpenseMultiplier]
			)
		) 
		END		
		
      If UPDATE([CustPriorWork])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPriorWork',
      CONVERT(NVARCHAR(2000),DELETED.[CustPriorWork],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPriorWork],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustPriorWork] Is Null And
				DELETED.[CustPriorWork] Is Not Null
			) Or
			(
				INSERTED.[CustPriorWork] Is Not Null And
				DELETED.[CustPriorWork] Is Null
			) Or
			(
				INSERTED.[CustPriorWork] !=
				DELETED.[CustPriorWork]
			)
		) 
		END		
		
      If UPDATE([CustSOQSubmitted])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSOQSubmitted',
      CONVERT(NVARCHAR(2000),DELETED.[CustSOQSubmitted],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSOQSubmitted],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustSOQSubmitted] Is Null And
				DELETED.[CustSOQSubmitted] Is Not Null
			) Or
			(
				INSERTED.[CustSOQSubmitted] Is Not Null And
				DELETED.[CustSOQSubmitted] Is Null
			) Or
			(
				INSERTED.[CustSOQSubmitted] !=
				DELETED.[CustSOQSubmitted]
			)
		) 
		END		
		
      If UPDATE([CustSetupNearmapUsage])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustSetupNearmapUsage',
      CONVERT(NVARCHAR(2000),DELETED.[CustSetupNearmapUsage],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustSetupNearmapUsage],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustSetupNearmapUsage] Is Null And
				DELETED.[CustSetupNearmapUsage] Is Not Null
			) Or
			(
				INSERTED.[CustSetupNearmapUsage] Is Not Null And
				DELETED.[CustSetupNearmapUsage] Is Null
			) Or
			(
				INSERTED.[CustSetupNearmapUsage] !=
				DELETED.[CustSetupNearmapUsage]
			)
		) 
		END		
		
      If UPDATE([CustContractSalesBalanced])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustContractSalesBalanced',
      CONVERT(NVARCHAR(2000),DELETED.[CustContractSalesBalanced],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustContractSalesBalanced],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustContractSalesBalanced] Is Null And
				DELETED.[CustContractSalesBalanced] Is Not Null
			) Or
			(
				INSERTED.[CustContractSalesBalanced] Is Not Null And
				DELETED.[CustContractSalesBalanced] Is Null
			) Or
			(
				INSERTED.[CustContractSalesBalanced] !=
				DELETED.[CustContractSalesBalanced]
			)
		) 
		END		
		
      If UPDATE([CustPursuitComponentSOQ])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPursuitComponentSOQ',
      CONVERT(NVARCHAR(2000),DELETED.[CustPursuitComponentSOQ],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPursuitComponentSOQ],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustPursuitComponentSOQ] Is Null And
				DELETED.[CustPursuitComponentSOQ] Is Not Null
			) Or
			(
				INSERTED.[CustPursuitComponentSOQ] Is Not Null And
				DELETED.[CustPursuitComponentSOQ] Is Null
			) Or
			(
				INSERTED.[CustPursuitComponentSOQ] !=
				DELETED.[CustPursuitComponentSOQ]
			)
		) 
		END		
		
      If UPDATE([CustPursuitComponentProposal])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPursuitComponentProposal',
      CONVERT(NVARCHAR(2000),DELETED.[CustPursuitComponentProposal],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPursuitComponentProposal],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustPursuitComponentProposal] Is Null And
				DELETED.[CustPursuitComponentProposal] Is Not Null
			) Or
			(
				INSERTED.[CustPursuitComponentProposal] Is Not Null And
				DELETED.[CustPursuitComponentProposal] Is Null
			) Or
			(
				INSERTED.[CustPursuitComponentProposal] !=
				DELETED.[CustPursuitComponentProposal]
			)
		) 
		END		
		
      If UPDATE([CustPursuitComponentInterview])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121),'CustPursuitComponentInterview',
      CONVERT(NVARCHAR(2000),DELETED.[CustPursuitComponentInterview],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPursuitComponentInterview],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND 
		(
			(
				INSERTED.[CustPursuitComponentInterview] Is Null And
				DELETED.[CustPursuitComponentInterview] Is Not Null
			) Or
			(
				INSERTED.[CustPursuitComponentInterview] Is Not Null And
				DELETED.[CustPursuitComponentInterview] Is Null
			) Or
			(
				INSERTED.[CustPursuitComponentInterview] !=
				DELETED.[CustPursuitComponentInterview]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[ProjectCustomTabFields] ADD CONSTRAINT [ProjectCustomTabFieldsPK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
