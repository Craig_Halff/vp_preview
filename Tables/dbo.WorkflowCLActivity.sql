CREATE TABLE [dbo].[WorkflowCLActivity]
(
[ActivityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrimaryInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowCLActivity] ADD CONSTRAINT [WorkflowCLActivityPK] PRIMARY KEY NONCLUSTERED ([ActivityID], [ClientID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
