CREATE TABLE [dbo].[CustomProposalClientGraphics]
(
[CustomPropID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SectionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LinkID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilePath] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderOfAppearance] [smallint] NOT NULL CONSTRAINT [DF__CustomPro__Order__1098925F] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomProposalClientGraphics] ADD CONSTRAINT [CustomProposalClientGraphicsPK] PRIMARY KEY NONCLUSTERED ([CustomPropID], [SectionID], [ClientID], [LinkID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
