CREATE TABLE [dbo].[FirmProjectCodesSF330]
(
[FirmID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProjectCode] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__FirmProject__Seq__774DC1A8] DEFAULT ((0)),
[RevenueIndex] [smallint] NOT NULL CONSTRAINT [DF__FirmProje__Reven__7841E5E1] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FirmProjectCodesSF330] ADD CONSTRAINT [FirmProjectCodesSF330PK] PRIMARY KEY NONCLUSTERED ([FirmID], [ProjectCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
