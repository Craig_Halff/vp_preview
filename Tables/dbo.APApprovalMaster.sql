CREATE TABLE [dbo].[APApprovalMaster]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ApprovalID] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__APApprova__Statu__4CCCACA0] DEFAULT ('N'),
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LiabCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayTerms] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayDate] [datetime] NULL,
[Invoice] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL,
[InvoiceAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__APApprova__Invoi__4DC0D0D9] DEFAULT ((0)),
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RouteType] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RouteToEmployee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RouteToOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedDate] [datetime] NULL,
[opBatch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[opVoucherDate] [datetime] NULL,
[opBank] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[opNextVoucherNumber] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[opDiary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[opPOPeriod] [int] NOT NULL CONSTRAINT [DF__APApprova__opPOP__4EB4F512] DEFAULT ((0)),
[opPOPostSeq] [int] NOT NULL CONSTRAINT [DF__APApprova__opPOP__4FA9194B] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[APApprovalMaster] ADD CONSTRAINT [APApprovalMasterPK] PRIMARY KEY NONCLUSTERED ([MasterPKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
