CREATE TABLE [dbo].[RPAccordionFormat]
(
[AccordionFormatID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[MajorScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPAccordi__Major__7A2AF9AA] DEFAULT ('m'),
[MinorScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPAccordi__Minor__7B1F1DE3] DEFAULT ('w'),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPAccordionFormat] ADD CONSTRAINT [RPAccordionFormatPK] PRIMARY KEY NONCLUSTERED ([AccordionFormatID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPAccordionFormatPlanIDIDX] ON [dbo].[RPAccordionFormat] ([PlanID]) ON [PRIMARY]
GO
