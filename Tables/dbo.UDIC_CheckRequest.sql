CREATE TABLE [dbo].[UDIC_CheckRequest]
(
[UDIC_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCheckRequestPayeeAddress] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCheckRequestPayee] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCheckRequestPayeePhoneNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCheckRequestDate] [datetime] NULL,
[CustCheckRequestDueDate] [datetime] NULL,
[CustCheckRequestNotes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCheckRequestMail] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__UDIC_Chec__CustC__71D5AACB] DEFAULT ('N'),
[CustCheckRequestReturnto] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCheckRequestApprover] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCheckRequestRequestedby] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCheckRequestStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__UDIC_Chec__CustC__72C9CF04] DEFAULT ('BatchLoad'),
[CustCheckRequestFilePath] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCheckRequestApprovedby] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_UDIC_CheckRequest]
      ON [dbo].[UDIC_CheckRequest]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_CheckRequest'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'UDIC_UID',CONVERT(NVARCHAR(2000),[UDIC_UID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustomCurrencyCode',CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustNumber',CONVERT(NVARCHAR(2000),[CustNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCheckRequestPayeeAddress',CONVERT(NVARCHAR(2000),[CustCheckRequestPayeeAddress],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCheckRequestPayee',CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestPayee],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join VE as oldDesc   on DELETED.CustCheckRequestPayee = oldDesc.Vendor

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCheckRequestPayeePhoneNumber',CONVERT(NVARCHAR(2000),[CustCheckRequestPayeePhoneNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCheckRequestDate',CONVERT(NVARCHAR(2000),[CustCheckRequestDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCheckRequestDueDate',CONVERT(NVARCHAR(2000),[CustCheckRequestDueDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCheckRequestNotes',CONVERT(NVARCHAR(2000),[CustCheckRequestNotes],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCheckRequestMail',CONVERT(NVARCHAR(2000),[CustCheckRequestMail],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCheckRequestReturnto',CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestReturnto],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustCheckRequestReturnto = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCheckRequestApprover',CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestApprover],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustCheckRequestApprover = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCheckRequestRequestedby',CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestRequestedby],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustCheckRequestRequestedby = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCheckRequestStatus',CONVERT(NVARCHAR(2000),[CustCheckRequestStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCheckRequestFilePath',CONVERT(NVARCHAR(2000),[CustCheckRequestFilePath],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCheckRequestApprovedby',CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestApprovedby],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustCheckRequestApprovedby = oldDesc.Employee

      
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_UDIC_CheckRequest]
      ON [dbo].[UDIC_CheckRequest]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_CheckRequest'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'UDIC_UID',NULL,CONVERT(NVARCHAR(2000),[UDIC_UID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustomCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustNumber',NULL,CONVERT(NVARCHAR(2000),[CustNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestPayeeAddress',NULL,CONVERT(NVARCHAR(2000),[CustCheckRequestPayeeAddress],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestPayee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestPayee],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  VE as newDesc  on INSERTED.CustCheckRequestPayee = newDesc.Vendor

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestPayeePhoneNumber',NULL,CONVERT(NVARCHAR(2000),[CustCheckRequestPayeePhoneNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestDate',NULL,CONVERT(NVARCHAR(2000),[CustCheckRequestDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestDueDate',NULL,CONVERT(NVARCHAR(2000),[CustCheckRequestDueDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestNotes',NULL,CONVERT(NVARCHAR(2000),[CustCheckRequestNotes],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestMail',NULL,CONVERT(NVARCHAR(2000),[CustCheckRequestMail],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestReturnto',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestReturnto],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustCheckRequestReturnto = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestApprover',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestApprover],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustCheckRequestApprover = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestRequestedby',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestRequestedby],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustCheckRequestRequestedby = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestStatus',NULL,CONVERT(NVARCHAR(2000),[CustCheckRequestStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestFilePath',NULL,CONVERT(NVARCHAR(2000),[CustCheckRequestFilePath],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestApprovedby',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestApprovedby],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustCheckRequestApprovedby = newDesc.Employee

     
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_UDIC_CheckRequest]
      ON [dbo].[UDIC_CheckRequest]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_CheckRequest'
    
      If UPDATE([UDIC_UID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'UDIC_UID',
      CONVERT(NVARCHAR(2000),DELETED.[UDIC_UID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UDIC_UID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[UDIC_UID] Is Null And
				DELETED.[UDIC_UID] Is Not Null
			) Or
			(
				INSERTED.[UDIC_UID] Is Not Null And
				DELETED.[UDIC_UID] Is Null
			) Or
			(
				INSERTED.[UDIC_UID] !=
				DELETED.[UDIC_UID]
			)
		) 
		END		
		
      If UPDATE([CustomCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustomCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustomCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustomCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustomCurrencyCode] Is Null And
				DELETED.[CustomCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] Is Not Null And
				DELETED.[CustomCurrencyCode] Is Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] !=
				DELETED.[CustomCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([CustNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustNumber',
      CONVERT(NVARCHAR(2000),DELETED.[CustNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustNumber] Is Null And
				DELETED.[CustNumber] Is Not Null
			) Or
			(
				INSERTED.[CustNumber] Is Not Null And
				DELETED.[CustNumber] Is Null
			) Or
			(
				INSERTED.[CustNumber] !=
				DELETED.[CustNumber]
			)
		) 
		END		
		
      If UPDATE([CustCheckRequestPayeeAddress])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestPayeeAddress',
      CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestPayeeAddress],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestPayeeAddress],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCheckRequestPayeeAddress] Is Null And
				DELETED.[CustCheckRequestPayeeAddress] Is Not Null
			) Or
			(
				INSERTED.[CustCheckRequestPayeeAddress] Is Not Null And
				DELETED.[CustCheckRequestPayeeAddress] Is Null
			) Or
			(
				INSERTED.[CustCheckRequestPayeeAddress] !=
				DELETED.[CustCheckRequestPayeeAddress]
			)
		) 
		END		
		
     If UPDATE([CustCheckRequestPayee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestPayee',
     CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestPayee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestPayee],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCheckRequestPayee] Is Null And
				DELETED.[CustCheckRequestPayee] Is Not Null
			) Or
			(
				INSERTED.[CustCheckRequestPayee] Is Not Null And
				DELETED.[CustCheckRequestPayee] Is Null
			) Or
			(
				INSERTED.[CustCheckRequestPayee] !=
				DELETED.[CustCheckRequestPayee]
			)
		) left join VE as oldDesc  on DELETED.CustCheckRequestPayee = oldDesc.Vendor  left join  VE as newDesc  on INSERTED.CustCheckRequestPayee = newDesc.Vendor
		END		
		
      If UPDATE([CustCheckRequestPayeePhoneNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestPayeePhoneNumber',
      CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestPayeePhoneNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestPayeePhoneNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCheckRequestPayeePhoneNumber] Is Null And
				DELETED.[CustCheckRequestPayeePhoneNumber] Is Not Null
			) Or
			(
				INSERTED.[CustCheckRequestPayeePhoneNumber] Is Not Null And
				DELETED.[CustCheckRequestPayeePhoneNumber] Is Null
			) Or
			(
				INSERTED.[CustCheckRequestPayeePhoneNumber] !=
				DELETED.[CustCheckRequestPayeePhoneNumber]
			)
		) 
		END		
		
      If UPDATE([CustCheckRequestDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCheckRequestDate] Is Null And
				DELETED.[CustCheckRequestDate] Is Not Null
			) Or
			(
				INSERTED.[CustCheckRequestDate] Is Not Null And
				DELETED.[CustCheckRequestDate] Is Null
			) Or
			(
				INSERTED.[CustCheckRequestDate] !=
				DELETED.[CustCheckRequestDate]
			)
		) 
		END		
		
      If UPDATE([CustCheckRequestDueDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestDueDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestDueDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestDueDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCheckRequestDueDate] Is Null And
				DELETED.[CustCheckRequestDueDate] Is Not Null
			) Or
			(
				INSERTED.[CustCheckRequestDueDate] Is Not Null And
				DELETED.[CustCheckRequestDueDate] Is Null
			) Or
			(
				INSERTED.[CustCheckRequestDueDate] !=
				DELETED.[CustCheckRequestDueDate]
			)
		) 
		END		
		
      If UPDATE([CustCheckRequestNotes])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestNotes',
      CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestNotes],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestNotes],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCheckRequestNotes] Is Null And
				DELETED.[CustCheckRequestNotes] Is Not Null
			) Or
			(
				INSERTED.[CustCheckRequestNotes] Is Not Null And
				DELETED.[CustCheckRequestNotes] Is Null
			) Or
			(
				INSERTED.[CustCheckRequestNotes] !=
				DELETED.[CustCheckRequestNotes]
			)
		) 
		END		
		
      If UPDATE([CustCheckRequestMail])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestMail',
      CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestMail],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestMail],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCheckRequestMail] Is Null And
				DELETED.[CustCheckRequestMail] Is Not Null
			) Or
			(
				INSERTED.[CustCheckRequestMail] Is Not Null And
				DELETED.[CustCheckRequestMail] Is Null
			) Or
			(
				INSERTED.[CustCheckRequestMail] !=
				DELETED.[CustCheckRequestMail]
			)
		) 
		END		
		
     If UPDATE([CustCheckRequestReturnto])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestReturnto',
     CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestReturnto],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestReturnto],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCheckRequestReturnto] Is Null And
				DELETED.[CustCheckRequestReturnto] Is Not Null
			) Or
			(
				INSERTED.[CustCheckRequestReturnto] Is Not Null And
				DELETED.[CustCheckRequestReturnto] Is Null
			) Or
			(
				INSERTED.[CustCheckRequestReturnto] !=
				DELETED.[CustCheckRequestReturnto]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustCheckRequestReturnto = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustCheckRequestReturnto = newDesc.Employee
		END		
		
     If UPDATE([CustCheckRequestApprover])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestApprover',
     CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestApprover],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestApprover],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCheckRequestApprover] Is Null And
				DELETED.[CustCheckRequestApprover] Is Not Null
			) Or
			(
				INSERTED.[CustCheckRequestApprover] Is Not Null And
				DELETED.[CustCheckRequestApprover] Is Null
			) Or
			(
				INSERTED.[CustCheckRequestApprover] !=
				DELETED.[CustCheckRequestApprover]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustCheckRequestApprover = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustCheckRequestApprover = newDesc.Employee
		END		
		
     If UPDATE([CustCheckRequestRequestedby])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestRequestedby',
     CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestRequestedby],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestRequestedby],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCheckRequestRequestedby] Is Null And
				DELETED.[CustCheckRequestRequestedby] Is Not Null
			) Or
			(
				INSERTED.[CustCheckRequestRequestedby] Is Not Null And
				DELETED.[CustCheckRequestRequestedby] Is Null
			) Or
			(
				INSERTED.[CustCheckRequestRequestedby] !=
				DELETED.[CustCheckRequestRequestedby]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustCheckRequestRequestedby = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustCheckRequestRequestedby = newDesc.Employee
		END		
		
      If UPDATE([CustCheckRequestStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestStatus',
      CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCheckRequestStatus] Is Null And
				DELETED.[CustCheckRequestStatus] Is Not Null
			) Or
			(
				INSERTED.[CustCheckRequestStatus] Is Not Null And
				DELETED.[CustCheckRequestStatus] Is Null
			) Or
			(
				INSERTED.[CustCheckRequestStatus] !=
				DELETED.[CustCheckRequestStatus]
			)
		) 
		END		
		
      If UPDATE([CustCheckRequestFilePath])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestFilePath',
      CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestFilePath],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestFilePath],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCheckRequestFilePath] Is Null And
				DELETED.[CustCheckRequestFilePath] Is Not Null
			) Or
			(
				INSERTED.[CustCheckRequestFilePath] Is Not Null And
				DELETED.[CustCheckRequestFilePath] Is Null
			) Or
			(
				INSERTED.[CustCheckRequestFilePath] !=
				DELETED.[CustCheckRequestFilePath]
			)
		) 
		END		
		
     If UPDATE([CustCheckRequestApprovedby])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCheckRequestApprovedby',
     CONVERT(NVARCHAR(2000),DELETED.[CustCheckRequestApprovedby],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustCheckRequestApprovedby],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCheckRequestApprovedby] Is Null And
				DELETED.[CustCheckRequestApprovedby] Is Not Null
			) Or
			(
				INSERTED.[CustCheckRequestApprovedby] Is Not Null And
				DELETED.[CustCheckRequestApprovedby] Is Null
			) Or
			(
				INSERTED.[CustCheckRequestApprovedby] !=
				DELETED.[CustCheckRequestApprovedby]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustCheckRequestApprovedby = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustCheckRequestApprovedby = newDesc.Employee
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[UDIC_CheckRequest] ADD CONSTRAINT [UDIC_CheckRequestPK] PRIMARY KEY CLUSTERED ([UDIC_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
