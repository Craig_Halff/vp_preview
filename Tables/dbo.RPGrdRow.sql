CREATE TABLE [dbo].[RPGrdRow]
(
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RowName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VisibleFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGrdRow___Visib__1C4B0784] DEFAULT ('Y'),
[RowSeq] [smallint] NOT NULL CONSTRAINT [DF__RPGrdRow___RowSe__1D3F2BBD] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPGrdRow] ADD CONSTRAINT [RPGrdRowPK] PRIMARY KEY NONCLUSTERED ([UserName], [GridName], [RowName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
