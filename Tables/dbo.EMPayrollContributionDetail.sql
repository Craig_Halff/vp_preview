CREATE TABLE [dbo].[EMPayrollContributionDetail]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__EMPayroll__Perio__65CE2FEB] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__EMPayroll__PostS__66C25424] DEFAULT ((0)),
[Method] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__AmtPc__67B6785D] DEFAULT ((0)),
[Suppress] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Suppr__68AA9C96] DEFAULT ('A'),
[Limit] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Limit__699EC0CF] DEFAULT ((0)),
[GrossPayBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Gross__6A92E508] DEFAULT ((0)),
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Amoun__6B870941] DEFAULT ((0)),
[Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Overr__6C7B2D7A] DEFAULT ('N'),
[AdjustedGrossPayBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Adjus__6D6F51B3] DEFAULT ((0)),
[TaxablePayBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Taxab__6E6375EC] DEFAULT ((0)),
[Amt401K] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Amt40__6F579A25] DEFAULT ((0)),
[Amt125] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Amt12__704BBE5E] DEFAULT ((0)),
[Exclude401k] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Exclu__713FE297] DEFAULT ('N'),
[ExcludeCafeteria] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Exclu__723406D0] DEFAULT ('N'),
[ExcludeOtherPay1] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Exclu__73282B09] DEFAULT ('N'),
[ExcludeOtherPay2] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Exclu__741C4F42] DEFAULT ('N'),
[ExcludeOtherPay3] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Exclu__7510737B] DEFAULT ('N'),
[ExcludeOtherPay4] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Exclu__760497B4] DEFAULT ('N'),
[ExcludeOtherPay5] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Exclu__76F8BBED] DEFAULT ('N'),
[OtherPay1] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__77ECE026] DEFAULT ((0)),
[OtherPay2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__78E1045F] DEFAULT ((0)),
[OtherPay3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__79D52898] DEFAULT ((0)),
[OtherPay4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__7AC94CD1] DEFAULT ((0)),
[OtherPay5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__7BBD710A] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EMPayrollContributionDetail] ADD CONSTRAINT [EMPayrollContributionDetailPK] PRIMARY KEY NONCLUSTERED ([Employee], [Code], [Period], [PostSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
