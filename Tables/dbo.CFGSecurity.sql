CREATE TABLE [dbo].[CFGSecurity]
(
[PKey] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MinimumPasswordLength] [smallint] NOT NULL CONSTRAINT [DF__CFGSecuri__Minim__241E3C13] DEFAULT ((0)),
[RequireDigit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGSecuri__Requi__2512604C] DEFAULT ('N'),
[RequireSpecial] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGSecuri__Requi__26068485] DEFAULT ('N'),
[ExpireDays] [smallint] NOT NULL CONSTRAINT [DF__CFGSecuri__Expir__26FAA8BE] DEFAULT ((0)),
[RepeatDays] [smallint] NOT NULL CONSTRAINT [DF__CFGSecuri__Repea__27EECCF7] DEFAULT ((0)),
[InvalidLoginAttempts] [smallint] NOT NULL CONSTRAINT [DF__CFGSecuri__Inval__28E2F130] DEFAULT ((0)),
[RepeatCount] [smallint] NOT NULL CONSTRAINT [DF__CFGSecuri__Repea__64F17C50] DEFAULT ((10)),
[PreventUsernameAsPassword] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGSecuri__Preve__65E5A089] DEFAULT ('Y')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGSecurity] ADD CONSTRAINT [CFGSecurityPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
