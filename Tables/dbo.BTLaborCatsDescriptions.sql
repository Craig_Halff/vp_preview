CREATE TABLE [dbo].[BTLaborCatsDescriptions]
(
[Category] [smallint] NOT NULL CONSTRAINT [DF__BTLaborCa__Categ__776CF1E2] DEFAULT ((0)),
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTLaborCatsDescriptions] ADD CONSTRAINT [BTLaborCatsDescriptionsPK] PRIMARY KEY CLUSTERED ([Category], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
