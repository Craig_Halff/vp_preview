CREATE TABLE [dbo].[CFGPYContributionRate]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__CFGPYCont__Table__02F25272] DEFAULT ((0)),
[RateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RateOver] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYCont__RateO__03E676AB] DEFAULT ((0)),
[RateUnder] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYCont__RateU__04DA9AE4] DEFAULT ((0)),
[Perc] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGPYContr__Perc__05CEBF1D] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPYContributionRate] ADD CONSTRAINT [CFGPYContributionRatePK] PRIMARY KEY CLUSTERED ([TableNo], [RateID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
