CREATE TABLE [dbo].[tkComments]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__tkComments___Seq__23A21089] DEFAULT ((0)),
[TransComment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tkComments] ADD CONSTRAINT [tkCommentsPK] PRIMARY KEY NONCLUSTERED ([Company], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
