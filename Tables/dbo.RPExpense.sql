CREATE TABLE [dbo].[RPExpense]
(
[ExpenseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcctName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BaselineExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPExpense__Basel__76255E9C] DEFAULT ((0)),
[BaselineExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPExpense__Basel__771982D5] DEFAULT ((0)),
[PlannedExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPExpense__Plann__780DA70E] DEFAULT ((0)),
[PlannedExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPExpense__Plann__7901CB47] DEFAULT ((0)),
[PlannedDirExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPExpense__Plann__79F5EF80] DEFAULT ((0)),
[PlannedDirExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPExpense__Plann__7AEA13B9] DEFAULT ((0)),
[PctCompleteExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPExpense__PctCo__7BDE37F2] DEFAULT ((0)),
[PctCompleteExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPExpense__PctCo__7CD25C2B] DEFAULT ((0)),
[WeightedExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPExpense__Weigh__7DC68064] DEFAULT ((0)),
[WeightedExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPExpense__Weigh__7EBAA49D] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[DirectAcctFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPExpense__Direc__7FAEC8D6] DEFAULT ('N'),
[ExpBillRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPExpense__ExpBi__00A2ED0F] DEFAULT ((0)),
[ExpRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPExpense__ExpRe__01971148] DEFAULT ((0)),
[SortSeq] [smallint] NOT NULL CONSTRAINT [DF__RPExpense__SortS__028B3581] DEFAULT ((0)),
[LabParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPExpense__LabPa__037F59BA] DEFAULT ('N'),
[ExpParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPExpense__ExpPa__04737DF3] DEFAULT ('N'),
[ConParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPExpense__ConPa__0567A22C] DEFAULT ('N'),
[UntParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPExpense__UntPa__065BC665] DEFAULT ('N'),
[BaselineStart] [datetime] NULL,
[BaselineFinish] [datetime] NULL,
[ConVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPExpense__ConVS__074FEA9E] DEFAULT ('N'),
[ExpVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPExpense__ExpVS__08440ED7] DEFAULT ('Y'),
[LabVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPExpense__LabVS__09383310] DEFAULT ('N'),
[UntVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPExpense__UntVS__0A2C5749] DEFAULT ('N'),
[BaselineDirExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPExpense__Basel__0B207B82] DEFAULT ((0)),
[BaselineDirExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPExpense__Basel__0C149FBB] DEFAULT ((0)),
[JTDExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPExpense__JTDEx__0D08C3F4] DEFAULT ((0)),
[JTDExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPExpense__JTDEx__0DFCE82D] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPExpense] ADD CONSTRAINT [RPExpensePK] PRIMARY KEY NONCLUSTERED ([ExpenseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPExpenseWBS1WBS2WBS3AccountVendorIDX] ON [dbo].[RPExpense] ([ExpenseID], [PlanID], [TaskID], [WBS1], [WBS2], [WBS3], [Account], [Vendor]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [RPExpensePlanIDIDX] ON [dbo].[RPExpense] ([PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPExpenseTaskIDIDX] ON [dbo].[RPExpense] ([TaskID]) ON [PRIMARY]
GO
