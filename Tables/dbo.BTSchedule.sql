CREATE TABLE [dbo].[BTSchedule]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__BTSchedule___Seq__14FD54C9] DEFAULT ((0)),
[PhaseSeq] [int] NOT NULL CONSTRAINT [DF__BTSchedul__Phase__15F17902] DEFAULT ((0)),
[ScheduleDate] [datetime] NOT NULL,
[PctCompl] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BTSchedul__PctCo__16E59D3B] DEFAULT ((0)),
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BTSchedul__Amoun__17D9C174] DEFAULT ((0)),
[Billed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BTSchedul__Bille__18CDE5AD] DEFAULT ('N'),
[BillingCategory] [smallint] NOT NULL CONSTRAINT [DF__BTSchedul__Billi__19C209E6] DEFAULT ((0)),
[Category] [smallint] NOT NULL CONSTRAINT [DF__BTSchedul__Categ__1AB62E1F] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTSchedule] ADD CONSTRAINT [BTSchedulePK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
