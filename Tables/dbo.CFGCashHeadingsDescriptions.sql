CREATE TABLE [dbo].[CFGCashHeadingsDescriptions]
(
[ReportColumn] [smallint] NOT NULL CONSTRAINT [DF__CFGCashHe__Repor__4DCBBDD1] DEFAULT ((0)),
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Heading] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubTotalLabel] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCashHeadingsDescriptions] ADD CONSTRAINT [CFGCashHeadingsDescriptionsPK] PRIMARY KEY CLUSTERED ([ReportColumn], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
