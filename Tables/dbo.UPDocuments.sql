CREATE TABLE [dbo].[UPDocuments]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL,
[AssociateNew] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__UPDocumen__Assoc__242C206E] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__UPDocuments__Seq__252044A7] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UPDocuments] ADD CONSTRAINT [upDocumentsPK] PRIMARY KEY NONCLUSTERED ([Batch], [MasterPKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
