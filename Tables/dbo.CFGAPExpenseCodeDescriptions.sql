CREATE TABLE [dbo].[CFGAPExpenseCodeDescriptions]
(
[code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAPExpenseCodeDescriptions] ADD CONSTRAINT [CFGAPExpenseCodeDescriptionsPK] PRIMARY KEY CLUSTERED ([code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
