CREATE TABLE [dbo].[CFGOppRevenueDefaults_Backup]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultAllocMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOppRev__Defau__38F09C7C] DEFAULT ('e'),
[DefaultAllocTemplate] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGOppRevenueDefaults_Backup] ADD CONSTRAINT [CFGOppRevenueDefaultsPK] PRIMARY KEY CLUSTERED ([Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
