CREATE TABLE [dbo].[ARPreInvoice]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PreInvoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__ARPreInvo__Perio__4BA37E3D] DEFAULT ((0)),
[InvoiceDate] [datetime] NULL,
[Cancelled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ARPreInvo__Cance__4C97A276] DEFAULT ('N'),
[AppliedInvoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppliedPeriod] [int] NOT NULL CONSTRAINT [DF__ARPreInvo__Appli__4D8BC6AF] DEFAULT ((0)),
[AppliedPostSeq] [int] NOT NULL CONSTRAINT [DF__ARPreInvo__Appli__4E7FEAE8] DEFAULT ((0)),
[Note] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ARPreInvoice] ADD CONSTRAINT [ARPreInvoicePK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [PreInvoice]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
