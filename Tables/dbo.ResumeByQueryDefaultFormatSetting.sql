CREATE TABLE [dbo].[ResumeByQueryDefaultFormatSetting]
(
[UserID] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultForCustomProposal] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultForSF255] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ResumeByQueryDefaultFormatSetting] ADD CONSTRAINT [ResumeByQueryDefaultFormatSettingPK] PRIMARY KEY NONCLUSTERED ([UserID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
