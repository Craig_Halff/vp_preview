CREATE TABLE [dbo].[CCG_PAT_HistoryDelegation]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[Id] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Delegate] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FromDate] [datetime] NOT NULL,
[ToDate] [datetime] NOT NULL,
[ActionTaken] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionDate] [datetime] NOT NULL,
[ActionTakenBy] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionRecipient] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_HistoryDelegation] ADD CONSTRAINT [PK_CCG_PAT_HistoryDelegation] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_CCG_PAT_HistoryDelegation] ON [dbo].[CCG_PAT_HistoryDelegation] ([FromDate], [ToDate]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Historical tracking of delegation requests and approvals', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryDelegation', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date when this action occurred', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryDelegation', 'COLUMN', N'ActionDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryDelegation', 'COLUMN', N'ActionRecipient'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Short description of type of delegation action that is captured', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryDelegation', 'COLUMN', N'ActionTaken'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Employee id of the action initiator', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryDelegation', 'COLUMN', N'ActionTakenBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The employee id of the delegate', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryDelegation', 'COLUMN', N'Delegate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Long description of the new delegation settings', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryDelegation', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The employee id of the delegator (one for which items will be delegated to another)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryDelegation', 'COLUMN', N'Employee'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The start date of the delegation period', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryDelegation', 'COLUMN', N'FromDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - The id of the delegation record for which this history record refers', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryDelegation', 'COLUMN', N'Id'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryDelegation', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The end date of the delegation period', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryDelegation', 'COLUMN', N'ToDate'
GO
