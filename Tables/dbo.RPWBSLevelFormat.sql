CREATE TABLE [dbo].[RPWBSLevelFormat]
(
[WBSFormatID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FmtLevel] [int] NOT NULL CONSTRAINT [DF__RPWBSLeve__FmtLe__1E8855B1] DEFAULT ((0)),
[WBSType] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBSMatch] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPWBSLeve__WBSMa__1F7C79EA] DEFAULT ('Y'),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPWBSLevelFormat] ADD CONSTRAINT [RPWBSLevelFormatPK] PRIMARY KEY NONCLUSTERED ([WBSFormatID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPWBSLevelFormatIDTypeMatchIDX] ON [dbo].[RPWBSLevelFormat] ([PlanID], [WBSType], [WBSMatch]) ON [PRIMARY]
GO
