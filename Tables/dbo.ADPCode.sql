CREATE TABLE [dbo].[ADPCode]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodeType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FieldID] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HoursType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LaborCodeMask] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Expression] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateNumber] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMLookupExpr] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMWhereClause] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMSearch] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ADPCode] ADD CONSTRAINT [ADPCodePK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
