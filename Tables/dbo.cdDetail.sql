CREATE TABLE [dbo].[cdDetail]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CheckNo] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__cdDetail_Ne__Seq__501E1A97] DEFAULT ((0)),
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__cdDetail___Amoun__51123ED0] DEFAULT ((0)),
[NetAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__cdDetail___NetAm__52066309] DEFAULT ((0)),
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__cdDetail___Curre__52FA8742] DEFAULT ((0)),
[LinkCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cdDetail] ADD CONSTRAINT [cdDetailPK] PRIMARY KEY NONCLUSTERED ([Batch], [CheckNo], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
