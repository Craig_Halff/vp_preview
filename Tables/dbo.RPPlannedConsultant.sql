CREATE TABLE [dbo].[RPPlannedConsultant]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConsultantID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodCount] [int] NOT NULL CONSTRAINT [DF__RPPlanned__Perio__68614F2A] DEFAULT ((1)),
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Perio__69557363] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Perio__6A49979C] DEFAULT ((0)),
[PeriodRev] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Perio__6B3DBBD5] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPPlannedConsultant] ADD CONSTRAINT [RPPlannedConsultantPK] PRIMARY KEY NONCLUSTERED ([TimePhaseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPPlannedConsultantPTCIDX] ON [dbo].[RPPlannedConsultant] ([PlanID], [TaskID], [ConsultantID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPPlannedConsultantAllIDX] ON [dbo].[RPPlannedConsultant] ([PlanID], [TimePhaseID], [TaskID], [ConsultantID], [StartDate], [EndDate], [PeriodCost], [PeriodBill], [PeriodRev]) ON [PRIMARY]
GO
