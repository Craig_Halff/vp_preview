CREATE TABLE [dbo].[AbraDefaultMapping]
(
[ImportExport] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__AbraDefau__Impor__6B863DEA] DEFAULT ('N'),
[FieldName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Expression] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AbraDefaultMapping] ADD CONSTRAINT [AbraDefaultMappingPK] PRIMARY KEY NONCLUSTERED ([ImportExport], [FieldName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
