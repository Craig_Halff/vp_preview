CREATE TABLE [dbo].[VESubscr]
(
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VESubscr] ADD CONSTRAINT [VESubscrPK] PRIMARY KEY NONCLUSTERED ([PartKey], [Vendor], [UserName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
