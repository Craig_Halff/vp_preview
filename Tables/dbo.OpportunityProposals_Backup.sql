CREATE TABLE [dbo].[OpportunityProposals_Backup]
(
[ProposalID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Number] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAdvertised] [datetime] NULL,
[SubmittalDate] [datetime] NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkedProposal] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkedGovtProposal] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DueDate] [datetime] NULL,
[AwardDate] [datetime] NULL,
[Fee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportunity__Fee__716ACB20] DEFAULT ((0)),
[LinkedSF330] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OpportunityProposals_Backup] ADD CONSTRAINT [OpportunityProposalsPK] PRIMARY KEY NONCLUSTERED ([ProposalID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
