CREATE TABLE [dbo].[Projects_ProjectTypesBreakdownTemplate]
(
[WBS1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustBreakdownProjectType] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBreakdownFee] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Projects___CustB__1E485E8F] DEFAULT ((0)),
[CustBreakdownUnitType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBreakdownUnitValue] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Projects___CustB__1F3C82C8] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_Projects_ProjectTypesBreakdownTemplate]
      ON [dbo].[Projects_ProjectTypesBreakdownTemplate]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Projects_ProjectTypesBreakdownTemplate'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'WBS1',CONVERT(NVARCHAR(2000),[WBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'WBS2',CONVERT(NVARCHAR(2000),[WBS2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'WBS3',CONVERT(NVARCHAR(2000),[WBS3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Seq',CONVERT(NVARCHAR(2000),[Seq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustBreakdownProjectType',CONVERT(NVARCHAR(2000),[CustBreakdownProjectType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustBreakdownFee',CONVERT(NVARCHAR(2000),[CustBreakdownFee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustBreakdownUnitType',CONVERT(NVARCHAR(2000),[CustBreakdownUnitType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustBreakdownUnitValue',CONVERT(NVARCHAR(2000),[CustBreakdownUnitValue],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_Projects_ProjectTypesBreakdownTemplate]
      ON [dbo].[Projects_ProjectTypesBreakdownTemplate]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Projects_ProjectTypesBreakdownTemplate'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS1',NULL,CONVERT(NVARCHAR(2000),[WBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS2',NULL,CONVERT(NVARCHAR(2000),[WBS2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS3',NULL,CONVERT(NVARCHAR(2000),[WBS3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',NULL,CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBreakdownProjectType',NULL,CONVERT(NVARCHAR(2000),[CustBreakdownProjectType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBreakdownFee',NULL,CONVERT(NVARCHAR(2000),[CustBreakdownFee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBreakdownUnitType',NULL,CONVERT(NVARCHAR(2000),[CustBreakdownUnitType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBreakdownUnitValue',NULL,CONVERT(NVARCHAR(2000),[CustBreakdownUnitValue],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_Projects_ProjectTypesBreakdownTemplate]
      ON [dbo].[Projects_ProjectTypesBreakdownTemplate]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Projects_ProjectTypesBreakdownTemplate'
    
      If UPDATE([WBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS1',
      CONVERT(NVARCHAR(2000),DELETED.[WBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS1] Is Null And
				DELETED.[WBS1] Is Not Null
			) Or
			(
				INSERTED.[WBS1] Is Not Null And
				DELETED.[WBS1] Is Null
			) Or
			(
				INSERTED.[WBS1] !=
				DELETED.[WBS1]
			)
		) 
		END		
		
      If UPDATE([WBS2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS2',
      CONVERT(NVARCHAR(2000),DELETED.[WBS2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS2] Is Null And
				DELETED.[WBS2] Is Not Null
			) Or
			(
				INSERTED.[WBS2] Is Not Null And
				DELETED.[WBS2] Is Null
			) Or
			(
				INSERTED.[WBS2] !=
				DELETED.[WBS2]
			)
		) 
		END		
		
      If UPDATE([WBS3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'WBS3',
      CONVERT(NVARCHAR(2000),DELETED.[WBS3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS3] Is Null And
				DELETED.[WBS3] Is Not Null
			) Or
			(
				INSERTED.[WBS3] Is Not Null And
				DELETED.[WBS3] Is Null
			) Or
			(
				INSERTED.[WBS3] !=
				DELETED.[WBS3]
			)
		) 
		END		
		
      If UPDATE([Seq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',
      CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] Is Null And
				DELETED.[Seq] Is Not Null
			) Or
			(
				INSERTED.[Seq] Is Not Null And
				DELETED.[Seq] Is Null
			) Or
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
		
      If UPDATE([CustBreakdownProjectType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBreakdownProjectType',
      CONVERT(NVARCHAR(2000),DELETED.[CustBreakdownProjectType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBreakdownProjectType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustBreakdownProjectType] Is Null And
				DELETED.[CustBreakdownProjectType] Is Not Null
			) Or
			(
				INSERTED.[CustBreakdownProjectType] Is Not Null And
				DELETED.[CustBreakdownProjectType] Is Null
			) Or
			(
				INSERTED.[CustBreakdownProjectType] !=
				DELETED.[CustBreakdownProjectType]
			)
		) 
		END		
		
      If UPDATE([CustBreakdownFee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBreakdownFee',
      CONVERT(NVARCHAR(2000),DELETED.[CustBreakdownFee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBreakdownFee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustBreakdownFee] Is Null And
				DELETED.[CustBreakdownFee] Is Not Null
			) Or
			(
				INSERTED.[CustBreakdownFee] Is Not Null And
				DELETED.[CustBreakdownFee] Is Null
			) Or
			(
				INSERTED.[CustBreakdownFee] !=
				DELETED.[CustBreakdownFee]
			)
		) 
		END		
		
      If UPDATE([CustBreakdownUnitType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBreakdownUnitType',
      CONVERT(NVARCHAR(2000),DELETED.[CustBreakdownUnitType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBreakdownUnitType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustBreakdownUnitType] Is Null And
				DELETED.[CustBreakdownUnitType] Is Not Null
			) Or
			(
				INSERTED.[CustBreakdownUnitType] Is Not Null And
				DELETED.[CustBreakdownUnitType] Is Null
			) Or
			(
				INSERTED.[CustBreakdownUnitType] !=
				DELETED.[CustBreakdownUnitType]
			)
		) 
		END		
		
      If UPDATE([CustBreakdownUnitValue])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBreakdownUnitValue',
      CONVERT(NVARCHAR(2000),DELETED.[CustBreakdownUnitValue],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBreakdownUnitValue],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustBreakdownUnitValue] Is Null And
				DELETED.[CustBreakdownUnitValue] Is Not Null
			) Or
			(
				INSERTED.[CustBreakdownUnitValue] Is Not Null And
				DELETED.[CustBreakdownUnitValue] Is Null
			) Or
			(
				INSERTED.[CustBreakdownUnitValue] !=
				DELETED.[CustBreakdownUnitValue]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[Projects_ProjectTypesBreakdownTemplate] ADD CONSTRAINT [Projects_ProjectTypesBreakdownTemplatePK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
