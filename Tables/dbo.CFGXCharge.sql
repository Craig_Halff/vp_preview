CREATE TABLE [dbo].[CFGXCharge]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[XChargeRegLAEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGXCharg__XChar__764EB077] DEFAULT ('N'),
[XChargeRegJEEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGXCharg__XChar__7742D4B0] DEFAULT ('N'),
[XChargeOHLAEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGXCharg__XChar__7836F8E9] DEFAULT ('N'),
[XChargeOHJEEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGXCharg__XChar__792B1D22] DEFAULT ('N'),
[XChargeRegMethod] [smallint] NOT NULL CONSTRAINT [DF__CFGXCharg__XChar__7A1F415B] DEFAULT ((0)),
[XChargeRegMult] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGXCharg__XChar__7B136594] DEFAULT ((0)),
[XChargeOHMult] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGXCharg__XChar__7C0789CD] DEFAULT ((0)),
[XChargeRegCreditAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XChargeRegDebitAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XChargeOHCreditAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XChargeOHDebitAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGXCharge] ADD CONSTRAINT [CFGXChargePK] PRIMARY KEY CLUSTERED ([Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
