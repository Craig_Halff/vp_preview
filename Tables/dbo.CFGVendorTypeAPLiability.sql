CREATE TABLE [dbo].[CFGVendorTypeAPLiability]
(
[Type] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LiabCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGVendorTypeAPLiability] ADD CONSTRAINT [CFGVendorTypeAPLiabilityPK] PRIMARY KEY CLUSTERED ([Type], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
