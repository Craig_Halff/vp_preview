CREATE TABLE [dbo].[SF254]
(
[SF254ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PersonResponsible] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DatePrepared] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmNameInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearPresentFirmEstablished] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentCompany] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FormerParentCompany] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OwnershipType] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentCompanySubmittalInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF254_New__Paren__7704743C] DEFAULT ('N'),
[BranchSubmittalInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF254_New__Branc__77F89875] DEFAULT ('N'),
[SmallBusinessInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF254_New__Small__78ECBCAE] DEFAULT ('N'),
[SmallDisadvantagedInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF254_New__Small__79E0E0E7] DEFAULT ('N'),
[WomanOwnedInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF254_New__Woman__7AD50520] DEFAULT ('N'),
[InterestInForeignWork] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF254_New__Inter__7BC92959] DEFAULT ('N'),
[PrincipalInfo1] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrincipalInfo2] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TotalPersonnelAllOffices] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PresentOfficesInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PresentOfficesComment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisciplineComment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProfileFeeBeginDate] [datetime] NULL,
[ProfileCodeComment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SignatorInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SignatureDate] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultFont] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultFontSize] [smallint] NOT NULL CONSTRAINT [DF__SF254_New__Defau__7CBD4D92] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF254] ADD CONSTRAINT [SF254PK] PRIMARY KEY CLUSTERED ([SF254ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
