CREATE TABLE [dbo].[SF255]
(
[SF255ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PersonResponsible] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAdvertised] [datetime] NULL,
[ProposalDueDate] [datetime] NULL,
[ProjectInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CBDDate] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgencyInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmNameAndInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkingFirmNameAndInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrincipalInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JointVentureFirms] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JointVenturePrevInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF255_New__Joint__1494D723] DEFAULT ('N'),
[DisciplineComment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdditionalInformation] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SignatorInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SignatureDate] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultFont] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultFontSize] [smallint] NOT NULL CONSTRAINT [DF__SF255_New__Defau__1588FB5C] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF255] ADD CONSTRAINT [SF255PK] PRIMARY KEY CLUSTERED ([SF255ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
