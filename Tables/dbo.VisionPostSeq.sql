CREATE TABLE [dbo].[VisionPostSeq]
(
[Period] [int] NOT NULL,
[MaxPostSeq] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VisionPostSeq] ADD CONSTRAINT [VisionPostSeqPK] PRIMARY KEY CLUSTERED ([Period]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
