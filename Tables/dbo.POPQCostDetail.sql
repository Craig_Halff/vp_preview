CREATE TABLE [dbo].[POPQCostDetail]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PQDetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Line] [int] NOT NULL CONSTRAINT [DF__POPQCostDe__Line__62488321] DEFAULT ((0)),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Billable] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POPQCostD__Billa__633CA75A] DEFAULT ('N'),
[Pct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POPQCostDet__Pct__6430CB93] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POPQCostDetail] ADD CONSTRAINT [POPQCostDetailPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [PQDetailPKey], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
