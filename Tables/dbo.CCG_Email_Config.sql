CREATE TABLE [dbo].[CCG_Email_Config]
(
[Interval] [int] NOT NULL,
[SMTPServer] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SMTPPort] [int] NOT NULL,
[Username] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UseTLS] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdvancedOptions] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
