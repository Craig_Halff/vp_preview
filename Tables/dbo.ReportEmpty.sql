CREATE TABLE [dbo].[ReportEmpty]
(
[ROIName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartTime] [datetime] NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportEmpty] ADD CONSTRAINT [ReportEmptyPK] PRIMARY KEY NONCLUSTERED ([ROIName], [StartTime]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
