CREATE TABLE [dbo].[WorkflowContactActivity]
(
[ActivityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrimaryInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowContactActivity] ADD CONSTRAINT [WorkflowContactActivityPK] PRIMARY KEY CLUSTERED ([ActivityID], [ContactID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
