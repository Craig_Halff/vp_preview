CREATE TABLE [dbo].[CFGProjectMilestoneData]
(
[Code] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SystemInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGProjec__Syste__3FF501CB] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGProjectMilestoneData] ADD CONSTRAINT [CFGProjectMilestoneDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
