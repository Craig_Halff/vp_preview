CREATE TABLE [dbo].[AR]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__AR_New__Period__44F680AE] DEFAULT ((0)),
[InvoiceDate] [datetime] NULL,
[PaidPeriod] [int] NOT NULL CONSTRAINT [DF__AR_New__PaidPeri__45EAA4E7] DEFAULT ((0)),
[LinkCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvBalanceSourceCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__AR_New__InvBalan__46DEC920] DEFAULT ((0)),
[DueDate] [datetime] NULL,
[RetainageDate] [datetime] NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AR] ADD CONSTRAINT [ARPK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [Invoice]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ARPaidPeriodIDX] ON [dbo].[AR] ([PaidPeriod]) ON [PRIMARY]
GO
