CREATE TABLE [dbo].[CR]
(
[Period] [int] NOT NULL CONSTRAINT [DF__CR_New__Period__538F841F] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__CR_New__PostSeq__5483A858] DEFAULT ((0)),
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransDate] [datetime] NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CR_New__Amount__5577CC91] DEFAULT ((0)),
[Description] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatementDate] [datetime] NULL,
[Cleared] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CR_New__Cleared__566BF0CA] DEFAULT ('N'),
[Voided] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CR_New__Voided__57601503] DEFAULT ('N'),
[CloseBalance] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CR_New__CloseBal__5854393C] DEFAULT ((0)),
[VoidPeriod] [int] NOT NULL CONSTRAINT [DF__CR_New__VoidPeri__59485D75] DEFAULT ((0)),
[VoidPostSeq] [int] NOT NULL CONSTRAINT [DF__CR_New__VoidPost__5A3C81AE] DEFAULT ((0)),
[VoidPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VoidAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CR_New__VoidAmou__5B30A5E7] DEFAULT ((0)),
[Reference] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WTorDD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InProcessAccount] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CR_New__InProces__5C24CA20] DEFAULT ('N'),
[InProcessAccountCleared] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CR_New__InProces__5D18EE59] DEFAULT ('N'),
[InProcessAccountClearedPeriod] [int] NOT NULL CONSTRAINT [DF__CR_New__InProces__5E0D1292] DEFAULT ((0)),
[InProcessAccountClearedPostSeq] [int] NOT NULL CONSTRAINT [DF__CR_New__InProces__5F0136CB] DEFAULT ((0)),
[SEPAIndicator] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatementNumber] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RemittanceSentDate] [datetime] NULL,
[Closed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CR_New__Closed__5FF55B04] DEFAULT ('N'),
[BankAmount01] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CR_New__BankAmou__60E97F3D] DEFAULT ((0)),
[BankAmount02] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CR_New__BankAmou__61DDA376] DEFAULT ((0)),
[BankAmount03] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CR_New__BankAmou__62D1C7AF] DEFAULT ((0)),
[BankAmount04] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CR_New__BankAmou__63C5EBE8] DEFAULT ((0)),
[LastCreateEntries] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CR] ADD CONSTRAINT [CRPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CRPeriodIDX] ON [dbo].[CR] ([Period]) ON [PRIMARY]
GO
