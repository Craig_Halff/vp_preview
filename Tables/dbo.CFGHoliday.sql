CREATE TABLE [dbo].[CFGHoliday]
(
[HolidayID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HolidayDate] [datetime] NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGHoliday] ADD CONSTRAINT [CFGHolidayPK] PRIMARY KEY CLUSTERED ([HolidayID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
