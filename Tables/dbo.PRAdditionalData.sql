CREATE TABLE [dbo].[PRAdditionalData]
(
[PRAdditionalDataID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRAdditio__PRAdd__186F89A8] DEFAULT (left(replace(newid(),'-',''),(32))),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Photo] [varbinary] (max) NULL,
[TempPhoto] [varbinary] (max) NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KonaSpace] [int] NOT NULL CONSTRAINT [DF__PRAdditio__KonaS__1963ADE1] DEFAULT ((0)),
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PRAdditionalData] ADD CONSTRAINT [PRAdditionalDataPK] PRIMARY KEY NONCLUSTERED ([PRAdditionalDataID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [PRAdditionalDataWBS1WBS2WBS3IDX] ON [dbo].[PRAdditionalData] ([WBS1], [WBS2], [WBS3]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
