CREATE TABLE [dbo].[WorkflowActions]
(
[ActionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionOrder] [int] NOT NULL CONSTRAINT [DF__WorkflowA__Actio__0EFBF95E] DEFAULT ((0)),
[EventID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__WorkflowA__Activ__0FF01D97] DEFAULT ('Y'),
[PRLevel] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReadOnly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__WorkflowA__ReadO__10E441D0] DEFAULT ('N'),
[ConditionMet] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__WorkflowA__Condi__11D86609] DEFAULT ('Y')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowActions] ADD CONSTRAINT [WorkflowActionsPK] PRIMARY KEY NONCLUSTERED ([ActionID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [WorkflowActionsActionTypeActiveIDX] ON [dbo].[WorkflowActions] ([ActionType], [Active]) INCLUDE ([EventID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [WorkflowActionsEventIDActionTypeIDX] ON [dbo].[WorkflowActions] ([EventID], [ActionType], [Active]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [WorkflowActionsEventIDActiveIDX] ON [dbo].[WorkflowActions] ([EventID], [Active], [ConditionMet]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
