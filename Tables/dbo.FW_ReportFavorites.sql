CREATE TABLE [dbo].[FW_ReportFavorites]
(
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportType] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportPath] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportName] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OptionName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LookupName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Options] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ByRole] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_Report__ByRol__77A2C763] DEFAULT ('N'),
[Favorite_UID] [varchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Owner_ID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_ReportFavorites] ADD CONSTRAINT [FW_ReportFavoritesPK] PRIMARY KEY NONCLUSTERED ([Username], [Name], [ReportType]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
