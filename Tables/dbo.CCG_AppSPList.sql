CREATE TABLE [dbo].[CCG_AppSPList]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[ProcedureName] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Product] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NOT NULL CONSTRAINT [DF__CCG_AppSP__Creat__328D541C] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_AppSPList] ADD CONSTRAINT [PK_CCG_EI_AppSPList] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
