CREATE TABLE [dbo].[CFGClientRoleDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGClientRo__Seq__5EF649D3] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGClientRoleDescriptions] ADD CONSTRAINT [CFGClientRoleDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
