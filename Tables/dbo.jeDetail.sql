CREATE TABLE [dbo].[jeDetail]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__jeDetail_Ne__Seq__7273FDF2] DEFAULT ((0)),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DebitAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__jeDetail___Debit__7368222B] DEFAULT ((0)),
[CreditAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__jeDetail___Credi__745C4664] DEFAULT ((0)),
[Description] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuppressBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__jeDetail___Suppr__75506A9D] DEFAULT ('N'),
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__jeDetail___Curre__76448ED6] DEFAULT ((0)),
[LinkCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DebitAmountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__jeDetail___Debit__7738B30F] DEFAULT ((0)),
[CreditAmountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__jeDetail___Credi__782CD748] DEFAULT ((0)),
[DebitAmountProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__jeDetail___Debit__7920FB81] DEFAULT ((0)),
[CreditAmountProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__jeDetail___Credi__7A151FBA] DEFAULT ((0)),
[GainsAndLossesType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SkipGL] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__jeDetail___SkipG__7B0943F3] DEFAULT ('N'),
[AutoEntry] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__jeDetail___AutoE__7BFD682C] DEFAULT ('N'),
[DebitAmountBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__jeDetail___Debit__7CF18C65] DEFAULT ((0)),
[CreditAmountBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__jeDetail___Credi__7DE5B09E] DEFAULT ((0)),
[EquipmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[jeDetail] ADD CONSTRAINT [jeDetailPK] PRIMARY KEY CLUSTERED ([Batch], [RefNo], [PKey]) ON [PRIMARY]
GO
