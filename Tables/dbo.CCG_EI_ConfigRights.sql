CREATE TABLE [dbo].[CCG_EI_ConfigRights]
(
[StageFlow] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Role] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Stage] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CanView] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CanModify] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CanSet] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PackageView] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PackageModify] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SendEmailOnStageSet] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CCSender] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Renotify] [tinyint] NULL,
[EmailSubject] [nvarchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailSubjectBatch] [nvarchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_ConfigRights] ADD CONSTRAINT [PK_CCG_EI_ConfigRights] PRIMARY KEY CLUSTERED ([StageFlow], [Role], [Stage]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Defines the access rights of each StageFlow/Role/Stage combination', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRights', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'When using this Stage Flow, can the Role change this Stage to another stage? Y / N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRights', 'COLUMN', N'CanModify'
GO
EXEC sp_addextendedproperty N'MS_Description', N'When using this Stage Flow, can the Role set the stage to this Stage? Y / N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRights', 'COLUMN', N'CanSet'
GO
EXEC sp_addextendedproperty N'MS_Description', N'When using this Stage Flow, can the Role view this Stage? Y / N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRights', 'COLUMN', N'CanView'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Should the sender be CC''ed? Y / N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRights', 'COLUMN', N'CCSender'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The body of the notification email', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRights', 'COLUMN', N'EmailMessage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The subject line of the notification email', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRights', 'COLUMN', N'EmailSubject'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The subject line of the notification email when batched', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRights', 'COLUMN', N'EmailSubjectBatch'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Right to use the packaging interface controls or view the packaging in read only mode', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRights', 'COLUMN', N'PackageModify'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Right to View package interface', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRights', 'COLUMN', N'PackageView'
GO
EXEC sp_addextendedproperty N'MS_Description', N'(Optional) The number of days between re-notification', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRights', 'COLUMN', N'Renotify'
GO
EXEC sp_addextendedproperty N'MS_Description', N'EI Role Key (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRights', 'COLUMN', N'Role'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Should a notification email be automatically sent when this Stage is selected? Y / N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRights', 'COLUMN', N'SendEmailOnStageSet'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stage (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRights', 'COLUMN', N'Stage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The Stage Flow (FK) for this rights record', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigRights', 'COLUMN', N'StageFlow'
GO
