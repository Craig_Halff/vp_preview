CREATE TABLE [dbo].[MktCampaignSubscr]
(
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampaignID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MktCampaignSubscr] ADD CONSTRAINT [MktCampaignSubscrPK] PRIMARY KEY NONCLUSTERED ([PartKey], [CampaignID], [UserName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
