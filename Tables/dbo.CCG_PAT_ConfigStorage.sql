CREATE TABLE [dbo].[CCG_PAT_ConfigStorage]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UseType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StorageType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Server] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Catalog] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Root] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserName] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowDelete] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevisionType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MarkupType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateChanged] [datetime] NULL,
[ChangedBy] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Detail] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_ConfigStorage] ADD CONSTRAINT [PK_CCG_PAT_ConfigStorage] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Configuration of storage types and locations used in PAT', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Should deletion be allowed in this storage type? NEVER / YES', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', 'COLUMN', N'AllowDelete'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Optional. Database name for TFS', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', 'COLUMN', N'Catalog'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Last modified username', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', 'COLUMN', N'ChangedBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Last modified date', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', 'COLUMN', N'DateChanged'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Storage type name', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', 'COLUMN', N'Detail'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The storage method for markup of documents ( only XFDF_DB in 4.0. EMBEDDED / XFDF_FILE for future use)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', 'COLUMN', N'MarkupType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Optional. Password required for authentication when accessing this storage type (SQL password, encrypted DBUP, or secret key for S3)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', 'COLUMN', N'Password'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The method/type of versioning (NONE / MARKUP / FILE). Leave NULL for standard versioning in CCG_PAT_XFDF table.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', 'COLUMN', N'RevisionType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Optional. Table name for TFS', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', 'COLUMN', N'Root'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Optional. Server to use for this storage type', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', 'COLUMN', N'Server'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Status of the storage type - (A)ctive / (I)nactive', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', 'COLUMN', N'Status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Basic storage type (e.g., S3 / FILESTREAM)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', 'COLUMN', N'StorageType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Optional. Login user required for authentication when accessing this storage type (''DBUP'' if password is encrypted DBUP, token for S3)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', 'COLUMN', N'UserName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The way in which this storage type is used (DEFAULT / ARCHIVE)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStorage', 'COLUMN', N'UseType'
GO
