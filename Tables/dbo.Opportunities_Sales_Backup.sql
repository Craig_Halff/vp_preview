CREATE TABLE [dbo].[Opportunities_Sales_Backup]
(
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustSalesTeam] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustSalesAmount] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustS__5F81250F] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Opportunities_Sales_Backup] ADD CONSTRAINT [Opportunities_SalesPK] PRIMARY KEY NONCLUSTERED ([OpportunityID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
