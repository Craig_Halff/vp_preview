CREATE TABLE [dbo].[InvoiceDraftAnnotationActions]
(
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Username] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DraftPDFAnnotationXMLContent] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileID] [uniqueidentifier] NULL,
[Timestamp] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[InvoiceDraftAnnotationActions] ADD CONSTRAINT [InvoiceDraftAnnotationActionsPK] PRIMARY KEY CLUSTERED ([Invoice], [WBS1], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
