CREATE TABLE [dbo].[RPJTDLabor]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AssignmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPJTDLabo__Perio__60F5378C] DEFAULT ((0)),
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPJTDLabo__Perio__61E95BC5] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPJTDLabo__Perio__62DD7FFE] DEFAULT ((0)),
[PostedFlg] [smallint] NOT NULL CONSTRAINT [DF__RPJTDLabo__Poste__63D1A437] DEFAULT ((1)),
[JTDDate] [datetime] NULL,
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPJTDLabor] ADD CONSTRAINT [RPJTDLaborPK] PRIMARY KEY NONCLUSTERED ([TimePhaseID], [TaskID], [PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPJTDLaborPTAIDX] ON [dbo].[RPJTDLabor] ([PlanID], [TaskID], [AssignmentID], [StartDate], [EndDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPJTDLaborAllIDX] ON [dbo].[RPJTDLabor] ([PlanID], [TaskID], [AssignmentID], [StartDate], [EndDate], [PeriodHrs], [PeriodCost], [PeriodBill]) ON [PRIMARY]
GO
