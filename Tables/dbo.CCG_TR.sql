CREATE TABLE [dbo].[CCG_TR]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OriginalTable] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OriginalPeriod] [int] NOT NULL,
[OriginalPostSeq] [int] NOT NULL,
[OriginalPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OriginalWBS1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OriginalWBS2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OriginalWBS3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IncludeFuture] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillNonBillable] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewBillStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewTransDate] [datetime] NULL,
[NewComment] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewDesc1] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewDesc2] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewWBS1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewWBS2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewWBS3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewRegHrs] [decimal] (19, 5) NULL,
[NewOvtHrs] [decimal] (19, 5) NULL,
[NewSpecialOvtHrs] [decimal] (19, 5) NULL,
[NewCost] [decimal] (19, 4) NULL,
[NewUnitQuantity] [decimal] (19, 4) NULL,
[ModComment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ApprComment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprUser] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_TR] ADD CONSTRAINT [PK_CCG_TR] PRIMARY KEY CLUSTERED ([Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX_CCG_TR_OriginalTableFields] ON [dbo].[CCG_TR] ([OriginalTable], [OriginalPeriod], [OriginalPostSeq], [OriginalPKey]) ON [PRIMARY]
GO
