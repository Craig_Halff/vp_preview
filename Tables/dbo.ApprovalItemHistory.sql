CREATE TABLE [dbo].[ApprovalItemHistory]
(
[ApprovalItemHistory_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Item_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WorkflowStep] [int] NOT NULL CONSTRAINT [DF__ApprovalI__Workf__3890A9C9] DEFAULT ((0)),
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DueDate] [datetime] NULL,
[DateStarted] [datetime] NULL,
[DateCompleted] [datetime] NULL,
[OldStatus] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewStatus] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Responsibility] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ApprovalItemHistory] ADD CONSTRAINT [ApprovalItemHistoryPK] PRIMARY KEY CLUSTERED ([ApprovalItemHistory_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ApprovalItemHistoryItem_UIDIDX] ON [dbo].[ApprovalItemHistory] ([Item_UID]) INCLUDE ([ApprovalItemHistory_UID], [DateCompleted]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
