CREATE TABLE [dbo].[cvDetailTax]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CheckNo] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__cvDetailT__TaxAm__788BFEA4] DEFAULT ((0)),
[CompoundOnTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__cvDetailTax__Seq__798022DD] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cvDetailTax] ADD CONSTRAINT [cvDetailTaxPK] PRIMARY KEY NONCLUSTERED ([Batch], [CheckNo], [PKey], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
