CREATE TABLE [dbo].[PNAssignment]
(
[AssignmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LaborCode] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Category] [smallint] NOT NULL CONSTRAINT [DF__PNAssignm__Categ__305C21D2] DEFAULT ((0)),
[ResourceID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActivityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__CostR__3150460B] DEFAULT ((0)),
[BillingRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__Billi__32446A44] DEFAULT ((0)),
[PctCompleteLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__PctCo__33388E7D] DEFAULT ((0)),
[PctCompleteLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__PctCo__342CB2B6] DEFAULT ((0)),
[BaselineLaborHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__Basel__3520D6EF] DEFAULT ((0)),
[BaselineLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__Basel__3614FB28] DEFAULT ((0)),
[BaselineLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__Basel__37091F61] DEFAULT ((0)),
[BaselineRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__Basel__37FD439A] DEFAULT ((0)),
[BaselineStart] [datetime] NULL,
[BaselineFinish] [datetime] NULL,
[PlannedLaborHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__Plann__38F167D3] DEFAULT ((0)),
[PlannedLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__Plann__39E58C0C] DEFAULT ((0)),
[PlannedLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__Plann__3AD9B045] DEFAULT ((0)),
[LabRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__LabRe__3BCDD47E] DEFAULT ((0)),
[WeightedLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__Weigh__3CC1F8B7] DEFAULT ((0)),
[WeightedLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__Weigh__3DB61CF0] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[SortSeq] [smallint] NOT NULL CONSTRAINT [DF__PNAssignm__SortS__3EAA4129] DEFAULT ((0)),
[LabParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNAssignm__LabPa__3F9E6562] DEFAULT ('N'),
[ExpParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNAssignm__ExpPa__4092899B] DEFAULT ('N'),
[ConParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNAssignm__ConPa__4186ADD4] DEFAULT ('N'),
[UntParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNAssignm__UntPa__427AD20D] DEFAULT ('N'),
[GRLBCD] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNAssignm__ConVS__436EF646] DEFAULT ('N'),
[ExpVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNAssignm__ExpVS__44631A7F] DEFAULT ('N'),
[LabVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNAssignm__LabVS__45573EB8] DEFAULT ('Y'),
[UntVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNAssignm__UntVS__464B62F1] DEFAULT ('N'),
[JTDLabHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__JTDLa__473F872A] DEFAULT ((0)),
[JTDLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__JTDLa__4833AB63] DEFAULT ((0)),
[JTDLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNAssignm__JTDLa__4927CF9C] DEFAULT ((0)),
[GenericResourceID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[HardBooked] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNAssignm__HardB__602CC733] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PNAssignment] ADD CONSTRAINT [PNAssignmentPK] PRIMARY KEY NONCLUSTERED ([AssignmentID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [PNAssignmentPlanIDIDX] ON [dbo].[PNAssignment] ([PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNAssignmentAllIDX] ON [dbo].[PNAssignment] ([PlanID], [TaskID], [AssignmentID], [WBS1], [WBS2], [WBS3], [LaborCode], [Category], [ResourceID], [GenericResourceID], [GRLBCD], [StartDate], [EndDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNAssignmentTaskIDIDX] ON [dbo].[PNAssignment] ([TaskID]) ON [PRIMARY]
GO
