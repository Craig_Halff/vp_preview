CREATE TABLE [dbo].[Opportunities_Phases_Backup]
(
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustPhaseCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPhaseName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPhaseFeeDirLab] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustP__503EE17F] DEFAULT ((0)),
[CustPhaseFeeDirExp] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustP__513305B8] DEFAULT ((0)),
[CustPhaseConsultFee] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustP__522729F1] DEFAULT ((0)),
[CustPhaseReimbAllowExp] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustP__531B4E2A] DEFAULT ((0)),
[CustPhaseReimbAllowCons] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustP__540F7263] DEFAULT ((0)),
[CustPhaseTotal] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustP__5503969C] DEFAULT ((0)),
[CustPhaseBillingType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPhaseFunctionalArea] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPhasePercent] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustP__55F7BAD5] DEFAULT ((0)),
[CustPhaseTax] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__Opportuni__CustP__56EBDF0E] DEFAULT ('N'),
[CustPhaseExpenseFee] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustP__57E00347] DEFAULT ((0)),
[CustPhaseConsutantFee] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__CustP__58D42780] DEFAULT ((0)),
[CustPhaseStartDate] [datetime] NULL CONSTRAINT [DF__Opportuni__CustP__59C84BB9] DEFAULT (getdate()),
[CustPhaseEndDate] [datetime] NULL CONSTRAINT [DF__Opportuni__CustP__5ABC6FF2] DEFAULT (getdate()),
[CustPhaseInitialDistribution] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__Opportuni__CustP__5BB0942B] DEFAULT ('Linear')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Opportunities_Phases_Backup] ADD CONSTRAINT [Opportunities_PhasesPK] PRIMARY KEY NONCLUSTERED ([OpportunityID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
