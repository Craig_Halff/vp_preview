CREATE TABLE [dbo].[CFGPYWHMethodDescriptions]
(
[Method] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPYWHMethodDescriptions] ADD CONSTRAINT [CFGPYWHMethodDescriptionsPK] PRIMARY KEY CLUSTERED ([Method], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
