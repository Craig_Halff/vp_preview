CREATE TABLE [dbo].[CFGContractTypeGovConData]
(
[Code] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGContractTypeGovConData] ADD CONSTRAINT [CFGContractTypeGovConDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
