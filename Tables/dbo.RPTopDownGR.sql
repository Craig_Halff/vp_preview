CREATE TABLE [dbo].[RPTopDownGR]
(
[TopDownGRID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TopDownID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Category] [smallint] NOT NULL CONSTRAINT [DF__RPTopDown__Categ__7956D102] DEFAULT ((0)),
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPTopDownG__Rate__7A4AF53B] DEFAULT ((0)),
[Hours] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPTopDown__Hours__7B3F1974] DEFAULT ((0)),
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPTopDown__Amoun__7C333DAD] DEFAULT ((0)),
[NumResources] [smallint] NULL,
[LaborCode] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SeqNo] [smallint] NOT NULL CONSTRAINT [DF__RPTopDown__SeqNo__7D2761E6] DEFAULT ((0)),
[GenericResourceID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPTopDownGR] ADD CONSTRAINT [RPTopDownGRPK] PRIMARY KEY NONCLUSTERED ([TopDownGRID], [TopDownID], [PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
