CREATE TABLE [dbo].[FW_DashPartLinks]
(
[Dashpart_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URLLinkTitle] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_DashPartLinks] ADD CONSTRAINT [FW_DashPartLinksPK] PRIMARY KEY NONCLUSTERED ([Dashpart_UID], [ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
