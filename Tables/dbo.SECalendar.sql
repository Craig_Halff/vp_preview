CREATE TABLE [dbo].[SECalendar]
(
[PrimaryUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccessUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrimaryEmployee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccessEmployee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccessRight] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SECalenda__Acces__16B229BF] DEFAULT ('R'),
[ShowInBD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SECalenda__ShowI__6120EB6C] DEFAULT ('Y')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SECalendar] ADD CONSTRAINT [SECalendarPK] PRIMARY KEY NONCLUSTERED ([PrimaryUser], [AccessUser]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
