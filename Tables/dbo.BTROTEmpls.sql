CREATE TABLE [dbo].[BTROTEmpls]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__BTROTEmpl__Table__0B73EA8F] DEFAULT ((0)),
[RateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BTROTEmpls__Rate__0C680EC8] DEFAULT ((0)),
[RateType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Category] [smallint] NOT NULL CONSTRAINT [DF__BTROTEmpl__Categ__0D5C3301] DEFAULT ((0)),
[EffectiveDate] [datetime] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTROTEmpls] ADD CONSTRAINT [BTROTEmplsPK] PRIMARY KEY NONCLUSTERED ([TableNo], [RateID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [BTROTEmplsEmployeeIDX] ON [dbo].[BTROTEmpls] ([TableNo], [Employee]) ON [PRIMARY]
GO
