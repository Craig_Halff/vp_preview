CREATE TABLE [dbo].[AuthorizationLog]
(
[ApplicationID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BO] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TableName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuthorizationLog] ADD CONSTRAINT [AuthorizationLogPK] PRIMARY KEY NONCLUSTERED ([ApplicationID], [BO], [TableName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
