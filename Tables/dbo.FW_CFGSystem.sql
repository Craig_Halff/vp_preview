CREATE TABLE [dbo].[FW_CFGSystem]
(
[PKey] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Version] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentPeriod] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__Curre__0EF0410F] DEFAULT ((0)),
[PeriodsPerYear] [smallint] NOT NULL CONSTRAINT [DF__FW_CFGSys__Perio__0FE46548] DEFAULT ((0)),
[CashBasis] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__CashB__10D88981] DEFAULT ('N'),
[RetainageEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Retai__11CCADBA] DEFAULT ('N'),
[RetainersEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Retai__12C0D1F3] DEFAULT ('N'),
[AlertsPollingInterval] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__Alert__13B4F62C] DEFAULT ((60000)),
[EnableAutoRetrieve] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Enabl__14A91A65] DEFAULT ('N'),
[LookupLimitError] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Looku__159D3E9E] DEFAULT ('N'),
[LookupLimit] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__Looku__169162D7] DEFAULT ((0)),
[AppLogo] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportVersions] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__Repor__17858710] DEFAULT ((0)),
[ReportRetentionPeriod] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__Repor__1879AB49] DEFAULT ((0)),
[AppURL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShareCalendar] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Share__196DCF82] DEFAULT ('Y'),
[SingleUserLogin] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Singl__1A61F3BB] DEFAULT ('N'),
[DisableAllLogins] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Disab__1B5617F4] DEFAULT ('N'),
[SendErrors] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__SendE__1C4A3C2D] DEFAULT ('Y'),
[MulticompanyEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Multi__1D3E6066] DEFAULT ('N'),
[MulticurrencyEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Multi__1E32849F] DEFAULT ('N'),
[AuditingEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__1F26A8D8] DEFAULT ('N'),
[AuditHistoryLength] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__201ACD11] DEFAULT ((30)),
[DashboardLookupLimit] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__Dashb__210EF14A] DEFAULT ((0)),
[ReportLogEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Repor__22031583] DEFAULT ('N'),
[RequireVoucherNumbers] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Requi__22F739BC] DEFAULT ('N'),
[BalanceSheet] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Balan__23EB5DF5] DEFAULT ('N'),
[LastVoucherAP] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__FW_CFGSys__LastV__24DF822E] DEFAULT ((0)),
[LastVoucherEX] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__LastV__25D3A667] DEFAULT ((0)),
[DefaultOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoSumComp] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__AutoS__26C7CAA0] DEFAULT ('N'),
[CheckPayee] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Check__27BBEED9] DEFAULT ('2'),
[WorkflowLogEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Workf__28B01312] DEFAULT ('N'),
[InternetDomain] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InternetUsername] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InternetPassword] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportAtBilling] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Repor__2A985B84] DEFAULT ('N'),
[CashBasisTimePost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__CashB__2B8C7FBD] DEFAULT ('N'),
[ETEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ETEna__2C80A3F6] DEFAULT ('N'),
[SpecialOvertimeEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Speci__2D74C82F] DEFAULT ('N'),
[BudgetByVendor] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Budge__2E68EC68] DEFAULT ('N'),
[BudgetETC] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Budge__2F5D10A1] DEFAULT ('N'),
[BudgetEAC] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Budge__305134DA] DEFAULT ('N'),
[BudgetStartEndDates] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Budge__31455913] DEFAULT ('N'),
[BudgetJTD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Budge__32397D4C] DEFAULT ('N'),
[EnableJEPostBTD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Enabl__332DA185] DEFAULT ('N'),
[EnableEMProjectAssocUpdate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Enabl__3421C5BE] DEFAULT ('N'),
[EnableVEProjectAssocUpdate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Enabl__3515E9F7] DEFAULT ('N'),
[OvtPctEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__OvtPc__360A0E30] DEFAULT ('N'),
[EnableCostRateTable] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Enabl__36FE3269] DEFAULT ('N'),
[ReportAtBurden] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Repor__37F256A2] DEFAULT ('N'),
[EnablePayRateTable] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Enabl__38E67ADB] DEFAULT ('N'),
[ReportAtBillingInBillingCurr] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Repor__39DA9F14] DEFAULT ('Y'),
[CheckPWPLevel] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Check__426FE515] DEFAULT ('3'),
[PRSummaryLastUpdate] [datetime] NULL,
[WSSServer] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WSSAdminPort] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__WSSAd__4364094E] DEFAULT ('0'),
[WSSVisionSite] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WSSUseDocMgt] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__WSSUs__44582D87] DEFAULT ('N'),
[DisplayCostPointOrg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Displ__454C51C0] DEFAULT ('N'),
[DefaultAttachmentSaveLocation] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConversionStatus] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingLabRegReclassOnly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__464075F9] DEFAULT ('Y'),
[ICBillingLabRegLAEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__47349A32] DEFAULT ('N'),
[ICBillingLabRegJEEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__4828BE6B] DEFAULT ('N'),
[ICBillingLabOHReclassOnly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__491CE2A4] DEFAULT ('Y'),
[ICBillingLabOHLAEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__4A1106DD] DEFAULT ('N'),
[ICBillingLabOHJEEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__4B052B16] DEFAULT ('N'),
[ICBillingLabPromoReclassOnly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__4BF94F4F] DEFAULT ('Y'),
[ICBillingLabPromoLAEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__4CED7388] DEFAULT ('N'),
[ICBillingLabPromoJEEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__4DE197C1] DEFAULT ('N'),
[ICBillingExpRegReclassOnly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__4ED5BBFA] DEFAULT ('Y'),
[ICBillingExpRegCostJEEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__4FC9E033] DEFAULT ('N'),
[ICBillingExpRegJEEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__50BE046C] DEFAULT ('N'),
[ICBillingExpOHReclassOnly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__51B228A5] DEFAULT ('Y'),
[ICBillingExpOHCostJEEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__52A64CDE] DEFAULT ('N'),
[ICBillingExpOHJEEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__539A7117] DEFAULT ('N'),
[ICBillingExpPromoReclassOnly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__548E9550] DEFAULT ('Y'),
[ICBillingExpPromoCostJEEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__5582B989] DEFAULT ('N'),
[ICBillingExpPromoJEEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__5676DDC2] DEFAULT ('N'),
[ICBillingBSOtherReclassOnly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__576B01FB] DEFAULT ('Y'),
[ICBillingBSOtherJEEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__585F2634] DEFAULT ('N'),
[ICBillingDetailedSubledgers] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICBil__59534A6D] DEFAULT ('N'),
[ReplaceExchRateWS] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Repla__5A476EA6] DEFAULT ('N'),
[TSAccessible] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__TSAcc__5B3B92DF] DEFAULT ('N'),
[ERAccessible] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ERAcc__5C2FB718] DEFAULT ('N'),
[AuditingEnabledBT] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__5D23DB51] DEFAULT ('N'),
[AuditingEnabledCA] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__5E17FF8A] DEFAULT ('N'),
[AuditingEnabledCL] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__5F0C23C3] DEFAULT ('N'),
[AuditingEnabledContact] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__600047FC] DEFAULT ('N'),
[AuditingEnabledEM] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__60F46C35] DEFAULT ('N'),
[AuditingEnabledMkt] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__62DCB4A7] DEFAULT ('N'),
[AuditingEnabledPR] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__64C4FD19] DEFAULT ('N'),
[AuditingEnabledText] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__65B92152] DEFAULT ('N'),
[AuditingEnabledUN] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__66AD458B] DEFAULT ('N'),
[AuditKeyValuesDelete] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__68958DFD] DEFAULT ('N'),
[DuplicateVendorInvoiceWarning] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Dupli__6989B236] DEFAULT ('N'),
[PrintInvoiceLeadingZeros] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Print__6A7DD66F] DEFAULT ('Y'),
[PrintVoucherLeadingZeros] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Print__6B71FAA8] DEFAULT ('Y'),
[UseProjectTerms] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__UsePr__6C661EE1] DEFAULT ('N'),
[DefaultSystemFont] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultSystemFontSize] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__Defau__6D5A431A] DEFAULT ((0)),
[WSSSitePort] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__WSSSi__6E4E6753] DEFAULT ((80)),
[APExpenseCodeEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__APExp__6F428B8C] DEFAULT ('N'),
[APExpenseCodeRequired] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__APExp__7036AFC5] DEFAULT ('N'),
[GridViewColumnsLimit] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__GridV__712AD3FE] DEFAULT ((0)),
[GridViewRecordsLimit] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__GridV__721EF837] DEFAULT ((0)),
[PlanningCheckOutFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Plann__73131C70] DEFAULT ('N'),
[DefAcctGroupTable] [smallint] NOT NULL CONSTRAINT [DF__FW_CFGSys__DefAc__740740A9] DEFAULT ((0)),
[EnableInstantMessaging] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Enabl__74FB64E2] DEFAULT ('N'),
[LastLabelUpdateDate] [datetime] NOT NULL,
[GLSummaryLastUpdate] [datetime] NULL,
[SyncProjToContractFees] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__SyncP__75EF891B] DEFAULT ('N'),
[RealizationByEmployeeEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Reali__76E3AD54] DEFAULT ('N'),
[RealizationByEmployeeMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RealizationByEmployeeFrequency] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuditingEnabledConfiguration] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__77D7D18D] DEFAULT ('N'),
[AuditingEnabledRoles] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__78CBF5C6] DEFAULT ('N'),
[AuditingEnabledScreenDesigner] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__79C019FF] DEFAULT ('N'),
[AuditingEnabledUsers] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__7AB43E38] DEFAULT ('N'),
[AuditingEnabledExchangeRates] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__7BA86271] DEFAULT ('N'),
[AnalysisCubePivotCurrency] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Analy__7C9C86AA] DEFAULT (' '),
[AnalysisCubeLastUpdated] [datetime] NULL,
[HelpLocation] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__HelpL__7D90AAE3] DEFAULT ('H'),
[IncClientInContactLookup] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__IncCl__7E84CF1C] DEFAULT ('N'),
[AuditingEnabledActivity] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__7F78F355] DEFAULT ('N'),
[KonaAcctKey] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuditingEnabledUserActivity] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Audit__006D178E] DEFAULT ('N'),
[WebServiceTimeout] [smallint] NOT NULL CONSTRAINT [DF__FW_CFGSys__WebSe__01613BC7] DEFAULT ((100)),
[KonaAcctSecret] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MarketType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowFileSave] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Allow__02556000] DEFAULT ('N'),
[SFTPHostName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFTPUsername] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFTPPassword] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SessionTimeout] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__Sessi__03498439] DEFAULT ((0)),
[WebAPIGenerateCustomPackages] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__WebAP__0531CCAB] DEFAULT ('Y'),
[HideKonaMenu] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__HideK__0625F0E4] DEFAULT ('N'),
[HideiAccessMenu] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Hidei__071A151D] DEFAULT ('N'),
[FileSizeUploadLimit] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__FileS__080E3956] DEFAULT ((50)),
[WAADTenant] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WAADClientID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WAADServerClientID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WAADServerClientSecret] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WAADAuthType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HMRCURL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HMRCClientID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HMRCClientSecret] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmRestrictTerminated] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__EmRes__09025D8F] DEFAULT ('N'),
[IndustryType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TwoFactorAuth] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__TwoFa__588BA56B] DEFAULT ('N'),
[AjeraSync] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Ajera__63FD5817] DEFAULT ('N'),
[KonaAcctID] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__KonaA__6B9E79DF] DEFAULT ((0)),
[KonaURL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnableLeadQualification] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Enabl__7527E419] DEFAULT ('N'),
[KonaNotificationRole] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TestOrgSetup] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__TestO__6A754B7C] DEFAULT ('Y'),
[UtilizationScheduleFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Utili__6E45DC60] DEFAULT ('N'),
[AutoUtilSchedWithProbCode] [smallint] NOT NULL CONSTRAINT [DF__FW_CFGSys__AutoU__6F3A0099] DEFAULT ((0)),
[EnableContractMgmt] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Enabl__702E24D2] DEFAULT ('N'),
[HeyDeltekEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__HeyDe__78C36AD3] DEFAULT ('N'),
[ICREnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__ICREn__1046BA00] DEFAULT ('N'),
[VPConnectTenant] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VPConnectType] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSystem__VPConnectType__DefA] DEFAULT ('Production'),
[VPConnectStatus] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__VPCon__4E43EC79] DEFAULT ('NotStarted'),
[VPConnectHMAC] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FinalInvoicePDFEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Final__51205924] DEFAULT ('N'),
[VPConnectLocation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__VPCon__5308A196] DEFAULT ('USEast')
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[UpdateGovWinLabel] ON [dbo].[FW_CFGSystem] AFTER UPDATE
AS BEGIN
	Set NOCOUNT ON
	if update(ProductType)
	Begin
		if exists(Select 'x' from inserted where ProductType like '%GovWin%')
		UPDATE FW_CFGLabelData set LabelValue='Capture Manager' where UICultureName='en-US' and LabelName='sysMC' and LabelValue <> 'Capture Manager'
	End
End
GO
ALTER TABLE [dbo].[FW_CFGSystem] ADD CONSTRAINT [FW_CFGSystemPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
