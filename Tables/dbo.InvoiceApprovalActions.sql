CREATE TABLE [dbo].[InvoiceApprovalActions]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PreInvoice] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__InvoiceAp__PreIn__43B90F09] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__InvoiceAppr__Seq__44AD3342] DEFAULT ((0)),
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Username] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionDate] [datetime] NULL,
[Comment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[InvoiceApprovalActions] ADD CONSTRAINT [InvoiceApprovalActionsPK] PRIMARY KEY NONCLUSTERED ([WBS1], [Invoice], [PreInvoice], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
