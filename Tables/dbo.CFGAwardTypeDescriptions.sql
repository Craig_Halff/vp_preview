CREATE TABLE [dbo].[CFGAwardTypeDescriptions]
(
[Code] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGAwardTyp__Seq__3AEDF387] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAwardTypeDescriptions] ADD CONSTRAINT [CFGAwardTypeDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
