CREATE TABLE [dbo].[RPLKCalendarInterval]
(
[Code] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPLKCalendarInterval] ADD CONSTRAINT [RPLKCalendarIntervalPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
