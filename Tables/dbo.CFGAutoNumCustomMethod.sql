CREATE TABLE [dbo].[CFGAutoNumCustomMethod]
(
[RuleID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NavID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AssemblyName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClassName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MethodName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAutoNumCustomMethod] ADD CONSTRAINT [CFGAutoNumCustomMethodPK] PRIMARY KEY CLUSTERED ([RuleID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
