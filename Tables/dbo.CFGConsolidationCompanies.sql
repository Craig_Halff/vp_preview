CREATE TABLE [dbo].[CFGConsolidationCompanies]
(
[ReportingGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGConsolidationCompanies] ADD CONSTRAINT [CFGConsolidationCompaniesPK] PRIMARY KEY CLUSTERED ([ReportingGroup], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
