CREATE TABLE [dbo].[APApprovalPO]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PODetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[APApprovalPO] ADD CONSTRAINT [APApprovalPOPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [PODetailPKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
