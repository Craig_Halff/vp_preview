CREATE TABLE [dbo].[UDIC_QAQCParameters]
(
[UDIC_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustPracticeArea] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustQAManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTotalCompensationThreshold] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_QAQC__CustT__690B5AA0] DEFAULT ((0)),
[CustReview1Goal] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustReview2Goal] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustReview3Goal] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustRecordID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_UDIC_QAQCParameters]
      ON [dbo].[UDIC_QAQCParameters]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_QAQCParameters'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'UDIC_UID',CONVERT(NVARCHAR(2000),[UDIC_UID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustomCurrencyCode',CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustPracticeArea',CONVERT(NVARCHAR(2000),[CustPracticeArea],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustQAManager',CONVERT(NVARCHAR(2000),DELETED.[CustQAManager],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustQAManager = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustTotalCompensationThreshold',CONVERT(NVARCHAR(2000),[CustTotalCompensationThreshold],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustReview1Goal',CONVERT(NVARCHAR(2000),[CustReview1Goal],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustReview2Goal',CONVERT(NVARCHAR(2000),[CustReview2Goal],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustReview3Goal',CONVERT(NVARCHAR(2000),[CustReview3Goal],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustRecordID',CONVERT(NVARCHAR(2000),[CustRecordID],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_UDIC_QAQCParameters]
      ON [dbo].[UDIC_QAQCParameters]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_QAQCParameters'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'UDIC_UID',NULL,CONVERT(NVARCHAR(2000),[UDIC_UID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustomCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPracticeArea',NULL,CONVERT(NVARCHAR(2000),[CustPracticeArea],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustQAManager',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustQAManager],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustQAManager = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustTotalCompensationThreshold',NULL,CONVERT(NVARCHAR(2000),[CustTotalCompensationThreshold],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustReview1Goal',NULL,CONVERT(NVARCHAR(2000),[CustReview1Goal],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustReview2Goal',NULL,CONVERT(NVARCHAR(2000),[CustReview2Goal],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustReview3Goal',NULL,CONVERT(NVARCHAR(2000),[CustReview3Goal],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustRecordID',NULL,CONVERT(NVARCHAR(2000),[CustRecordID],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_UDIC_QAQCParameters]
      ON [dbo].[UDIC_QAQCParameters]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_QAQCParameters'
    
      If UPDATE([UDIC_UID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'UDIC_UID',
      CONVERT(NVARCHAR(2000),DELETED.[UDIC_UID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UDIC_UID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[UDIC_UID] Is Null And
				DELETED.[UDIC_UID] Is Not Null
			) Or
			(
				INSERTED.[UDIC_UID] Is Not Null And
				DELETED.[UDIC_UID] Is Null
			) Or
			(
				INSERTED.[UDIC_UID] !=
				DELETED.[UDIC_UID]
			)
		) 
		END		
		
      If UPDATE([CustomCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustomCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustomCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustomCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustomCurrencyCode] Is Null And
				DELETED.[CustomCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] Is Not Null And
				DELETED.[CustomCurrencyCode] Is Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] !=
				DELETED.[CustomCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([CustPracticeArea])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPracticeArea',
      CONVERT(NVARCHAR(2000),DELETED.[CustPracticeArea],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPracticeArea],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustPracticeArea] Is Null And
				DELETED.[CustPracticeArea] Is Not Null
			) Or
			(
				INSERTED.[CustPracticeArea] Is Not Null And
				DELETED.[CustPracticeArea] Is Null
			) Or
			(
				INSERTED.[CustPracticeArea] !=
				DELETED.[CustPracticeArea]
			)
		) 
		END		
		
     If UPDATE([CustQAManager])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustQAManager',
     CONVERT(NVARCHAR(2000),DELETED.[CustQAManager],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustQAManager],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustQAManager] Is Null And
				DELETED.[CustQAManager] Is Not Null
			) Or
			(
				INSERTED.[CustQAManager] Is Not Null And
				DELETED.[CustQAManager] Is Null
			) Or
			(
				INSERTED.[CustQAManager] !=
				DELETED.[CustQAManager]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustQAManager = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustQAManager = newDesc.Employee
		END		
		
      If UPDATE([CustTotalCompensationThreshold])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustTotalCompensationThreshold',
      CONVERT(NVARCHAR(2000),DELETED.[CustTotalCompensationThreshold],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustTotalCompensationThreshold],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustTotalCompensationThreshold] Is Null And
				DELETED.[CustTotalCompensationThreshold] Is Not Null
			) Or
			(
				INSERTED.[CustTotalCompensationThreshold] Is Not Null And
				DELETED.[CustTotalCompensationThreshold] Is Null
			) Or
			(
				INSERTED.[CustTotalCompensationThreshold] !=
				DELETED.[CustTotalCompensationThreshold]
			)
		) 
		END		
		
      If UPDATE([CustReview1Goal])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustReview1Goal',
      CONVERT(NVARCHAR(2000),DELETED.[CustReview1Goal],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustReview1Goal],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustReview1Goal] Is Null And
				DELETED.[CustReview1Goal] Is Not Null
			) Or
			(
				INSERTED.[CustReview1Goal] Is Not Null And
				DELETED.[CustReview1Goal] Is Null
			) Or
			(
				INSERTED.[CustReview1Goal] !=
				DELETED.[CustReview1Goal]
			)
		) 
		END		
		
      If UPDATE([CustReview2Goal])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustReview2Goal',
      CONVERT(NVARCHAR(2000),DELETED.[CustReview2Goal],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustReview2Goal],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustReview2Goal] Is Null And
				DELETED.[CustReview2Goal] Is Not Null
			) Or
			(
				INSERTED.[CustReview2Goal] Is Not Null And
				DELETED.[CustReview2Goal] Is Null
			) Or
			(
				INSERTED.[CustReview2Goal] !=
				DELETED.[CustReview2Goal]
			)
		) 
		END		
		
      If UPDATE([CustReview3Goal])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustReview3Goal',
      CONVERT(NVARCHAR(2000),DELETED.[CustReview3Goal],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustReview3Goal],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustReview3Goal] Is Null And
				DELETED.[CustReview3Goal] Is Not Null
			) Or
			(
				INSERTED.[CustReview3Goal] Is Not Null And
				DELETED.[CustReview3Goal] Is Null
			) Or
			(
				INSERTED.[CustReview3Goal] !=
				DELETED.[CustReview3Goal]
			)
		) 
		END		
		
      If UPDATE([CustRecordID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustRecordID',
      CONVERT(NVARCHAR(2000),DELETED.[CustRecordID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRecordID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustRecordID] Is Null And
				DELETED.[CustRecordID] Is Not Null
			) Or
			(
				INSERTED.[CustRecordID] Is Not Null And
				DELETED.[CustRecordID] Is Null
			) Or
			(
				INSERTED.[CustRecordID] !=
				DELETED.[CustRecordID]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[UDIC_QAQCParameters] ADD CONSTRAINT [UDIC_QAQCParametersPK] PRIMARY KEY CLUSTERED ([UDIC_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
