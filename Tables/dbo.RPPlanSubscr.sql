CREATE TABLE [dbo].[RPPlanSubscr]
(
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPPlanSubscr] ADD CONSTRAINT [RPPlanSubscrPK] PRIMARY KEY NONCLUSTERED ([PartKey], [PlanID], [Username]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
