CREATE TABLE [dbo].[LDCustomFields]
(
[Period] [int] NOT NULL CONSTRAINT [DF__LDCustomF__Perio__1E5F03D6] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__LDCustomF__PostS__1F53280F] DEFAULT ((0)),
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustTKUD01] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTKUD02] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__LDCustomF__CustT__20474C48] DEFAULT ((0)),
[CustTKUD03] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LDCustomFields] ADD CONSTRAINT [LDCustomFieldsPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
