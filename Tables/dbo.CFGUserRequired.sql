CREATE TABLE [dbo].[CFGUserRequired]
(
[PageName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DivID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InputID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Required] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGUserRequired] ADD CONSTRAINT [CFGUserRequiredPK] PRIMARY KEY NONCLUSTERED ([PageName], [DivID], [InputID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
