CREATE TABLE [dbo].[POCostDetail]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PODetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Line] [int] NOT NULL CONSTRAINT [DF__POCostDeta__Line__38524955] DEFAULT ((0)),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Billable] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POCostDet__Billa__39466D8E] DEFAULT ('N'),
[Pct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCostDetai__Pct__3A3A91C7] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POCostDetail] ADD CONSTRAINT [POCostDetailPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [PODetailPKey], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
