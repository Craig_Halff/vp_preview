CREATE TABLE [dbo].[CFGGridViewFields]
(
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ElementID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Username] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ElementCaption] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__CFGGridView__Seq__6DCE790F] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGGridViewFields] ADD CONSTRAINT [CFGGridViewFieldsPK] PRIMARY KEY NONCLUSTERED ([InfoCenterArea], [TabID], [ElementID], [Username]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
