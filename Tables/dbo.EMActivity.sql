CREATE TABLE [dbo].[EMActivity]
(
[ActivityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Owner] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMActivit__Owner__00EC3A7B] DEFAULT ('N'),
[PopupDismissed] [datetime] NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ReminderInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReminderUnit] [smallint] NULL,
[ReminderMinHrDay] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EMActivity] ADD CONSTRAINT [EMActivityPK] PRIMARY KEY NONCLUSTERED ([ActivityID], [Employee]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [EMActivityEmployeeIDX] ON [dbo].[EMActivity] ([Employee]) ON [PRIMARY]
GO
