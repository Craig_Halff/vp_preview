CREATE TABLE [dbo].[PRSummaryWBSList]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__PRSummary__Perio__6C46EEA7] DEFAULT ((0)),
[sessionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChargeType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[lastRun] [datetime] NULL CONSTRAINT [DF__PRSummary__lastR__6D3B12E0] DEFAULT (getutcdate()),
[lastPostDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PRSummaryWBSList] ADD CONSTRAINT [PRSummaryWBSListPK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [Period], [sessionID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
