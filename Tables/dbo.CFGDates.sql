CREATE TABLE [dbo].[CFGDates]
(
[Period] [int] NOT NULL,
[AccountPdStart] [datetime] NULL,
[AccountPdEnd] [datetime] NULL,
[FYStart] [datetime] NULL,
[FYEnd] [datetime] NULL,
[LastPostSeq] [int] NOT NULL CONSTRAINT [DF__CFGDates__LastPo__607D3EDD] DEFAULT ((0)),
[OHAllocNeeded] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGDates__OHAllo__61716316] DEFAULT ('N'),
[Purged] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGDates__Purged__6265874F] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGDates] ADD CONSTRAINT [CFGDatesPK] PRIMARY KEY CLUSTERED ([Period]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
