CREATE TABLE [dbo].[CFGCreditCardImportDetailData]
(
[PrimaryCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FieldName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Include] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGCredit__Inclu__0EA55CF5] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__CFGCreditCa__Seq__0F99812E] DEFAULT ((0)),
[DisplayInReconciliation] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGCredit__Displ__108DA567] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCreditCardImportDetailData] ADD CONSTRAINT [CFGCreditCardImportDetailDataPK] PRIMARY KEY NONCLUSTERED ([PrimaryCode], [Company], [FieldName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
