CREATE TABLE [dbo].[CLAddress]
(
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Address] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Addressee] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address3] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address4] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIP] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FAX] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMail] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxCountryCode] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxRegistrationNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrimaryInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CLAddress__Prima__14D33797] DEFAULT ('N'),
[Billing] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CLAddress__Billi__15C75BD0] DEFAULT ('N'),
[Accounting] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CLAddress__Accou__16BB8009] DEFAULT ('N'),
[PhoneFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FaxFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLAddressID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__CLAddress__CLAdd__17AFA442] DEFAULT (newid()),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[Payment] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CLAddress__Payme__50EA83A3] DEFAULT ('N'),
[QBOID] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QBOAddressID] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QBOIsBillingAddr] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CLAddress__QBOIs__2C781903] DEFAULT ('N'),
[QBOIsShippingAddr] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CLAddress__QBOIs__2D6C3D3C] DEFAULT ('N'),
[QBOLastUpdated] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_CLAddress]
      ON [dbo].[CLAddress]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CLAddress'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'ClientID',CONVERT(NVARCHAR(2000),DELETED.[ClientID],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join CL as oldDesc  on DELETED.ClientID = oldDesc.ClientID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'Address',CONVERT(NVARCHAR(2000),[Address],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'Addressee',CONVERT(NVARCHAR(2000),[Addressee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'Address1',CONVERT(NVARCHAR(2000),[Address1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'Address2',CONVERT(NVARCHAR(2000),[Address2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'Address3',CONVERT(NVARCHAR(2000),[Address3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'Address4',CONVERT(NVARCHAR(2000),[Address4],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'City',CONVERT(NVARCHAR(2000),[City],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'State',CONVERT(NVARCHAR(2000),DELETED.[State],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGStates as oldDesc  on DELETED.State = oldDesc.Code

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'ZIP',CONVERT(NVARCHAR(2000),[ZIP],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'Country',CONVERT(NVARCHAR(2000),[Country],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'Phone',CONVERT(NVARCHAR(2000),[Phone],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'FAX',CONVERT(NVARCHAR(2000),[FAX],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'EMail',CONVERT(NVARCHAR(2000),[EMail],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'TaxCountryCode',CONVERT(NVARCHAR(2000),[TaxCountryCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'TaxRegistrationNumber',CONVERT(NVARCHAR(2000),[TaxRegistrationNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'PrimaryInd',CONVERT(NVARCHAR(2000),[PrimaryInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'Billing',CONVERT(NVARCHAR(2000),[Billing],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'Accounting',CONVERT(NVARCHAR(2000),[Accounting],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'PhoneFormat',CONVERT(NVARCHAR(2000),[PhoneFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'FaxFormat',CONVERT(NVARCHAR(2000),[FaxFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'CLAddressID',CONVERT(NVARCHAR(2000),[CLAddressID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'Payment',CONVERT(NVARCHAR(2000),[Payment],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'QBOID',CONVERT(NVARCHAR(2000),[QBOID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'QBOAddressID',CONVERT(NVARCHAR(2000),[QBOAddressID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'QBOIsBillingAddr',CONVERT(NVARCHAR(2000),[QBOIsBillingAddr],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'QBOIsShippingAddr',CONVERT(NVARCHAR(2000),[QBOIsShippingAddr],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Address],121),'QBOLastUpdated',CONVERT(NVARCHAR(2000),[QBOLastUpdated],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_CLAddress]
      ON [dbo].[CLAddress]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CLAddress'
    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'ClientID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[ClientID],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  CL as newDesc  on INSERTED.ClientID = newDesc.ClientID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Address',NULL,CONVERT(NVARCHAR(2000),[Address],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Addressee',NULL,CONVERT(NVARCHAR(2000),[Addressee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Address1',NULL,CONVERT(NVARCHAR(2000),[Address1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Address2',NULL,CONVERT(NVARCHAR(2000),[Address2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Address3',NULL,CONVERT(NVARCHAR(2000),[Address3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Address4',NULL,CONVERT(NVARCHAR(2000),[Address4],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'City',NULL,CONVERT(NVARCHAR(2000),[City],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'State',NULL,CONVERT(NVARCHAR(2000),INSERTED.[State],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGStates as newDesc  on INSERTED.State = newDesc.Code

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'ZIP',NULL,CONVERT(NVARCHAR(2000),[ZIP],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Country',NULL,CONVERT(NVARCHAR(2000),[Country],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Phone',NULL,CONVERT(NVARCHAR(2000),[Phone],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'FAX',NULL,CONVERT(NVARCHAR(2000),[FAX],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'EMail',NULL,CONVERT(NVARCHAR(2000),[EMail],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'TaxCountryCode',NULL,CONVERT(NVARCHAR(2000),[TaxCountryCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'TaxRegistrationNumber',NULL,CONVERT(NVARCHAR(2000),[TaxRegistrationNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'PrimaryInd',NULL,CONVERT(NVARCHAR(2000),[PrimaryInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Billing',NULL,CONVERT(NVARCHAR(2000),[Billing],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Accounting',NULL,CONVERT(NVARCHAR(2000),[Accounting],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'PhoneFormat',NULL,CONVERT(NVARCHAR(2000),[PhoneFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'FaxFormat',NULL,CONVERT(NVARCHAR(2000),[FaxFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'CLAddressID',NULL,CONVERT(NVARCHAR(2000),[CLAddressID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Payment',NULL,CONVERT(NVARCHAR(2000),[Payment],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'QBOID',NULL,CONVERT(NVARCHAR(2000),[QBOID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'QBOAddressID',NULL,CONVERT(NVARCHAR(2000),[QBOAddressID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'QBOIsBillingAddr',NULL,CONVERT(NVARCHAR(2000),[QBOIsBillingAddr],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'QBOIsShippingAddr',NULL,CONVERT(NVARCHAR(2000),[QBOIsShippingAddr],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'QBOLastUpdated',NULL,CONVERT(NVARCHAR(2000),[QBOLastUpdated],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_CLAddress]
      ON [dbo].[CLAddress]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CLAddress'
    
     If UPDATE([ClientID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'ClientID',
     CONVERT(NVARCHAR(2000),DELETED.[ClientID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[ClientID],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[ClientID] Is Null And
				DELETED.[ClientID] Is Not Null
			) Or
			(
				INSERTED.[ClientID] Is Not Null And
				DELETED.[ClientID] Is Null
			) Or
			(
				INSERTED.[ClientID] !=
				DELETED.[ClientID]
			)
		) left join CL as oldDesc  on DELETED.ClientID = oldDesc.ClientID  left join  CL as newDesc  on INSERTED.ClientID = newDesc.ClientID
		END		
		
      If UPDATE([Address])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Address',
      CONVERT(NVARCHAR(2000),DELETED.[Address],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Address],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[Address] Is Null And
				DELETED.[Address] Is Not Null
			) Or
			(
				INSERTED.[Address] Is Not Null And
				DELETED.[Address] Is Null
			) Or
			(
				INSERTED.[Address] !=
				DELETED.[Address]
			)
		) 
		END		
		
      If UPDATE([Addressee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Addressee',
      CONVERT(NVARCHAR(2000),DELETED.[Addressee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Addressee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[Addressee] Is Null And
				DELETED.[Addressee] Is Not Null
			) Or
			(
				INSERTED.[Addressee] Is Not Null And
				DELETED.[Addressee] Is Null
			) Or
			(
				INSERTED.[Addressee] !=
				DELETED.[Addressee]
			)
		) 
		END		
		
      If UPDATE([Address1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Address1',
      CONVERT(NVARCHAR(2000),DELETED.[Address1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Address1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[Address1] Is Null And
				DELETED.[Address1] Is Not Null
			) Or
			(
				INSERTED.[Address1] Is Not Null And
				DELETED.[Address1] Is Null
			) Or
			(
				INSERTED.[Address1] !=
				DELETED.[Address1]
			)
		) 
		END		
		
      If UPDATE([Address2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Address2',
      CONVERT(NVARCHAR(2000),DELETED.[Address2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Address2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[Address2] Is Null And
				DELETED.[Address2] Is Not Null
			) Or
			(
				INSERTED.[Address2] Is Not Null And
				DELETED.[Address2] Is Null
			) Or
			(
				INSERTED.[Address2] !=
				DELETED.[Address2]
			)
		) 
		END		
		
      If UPDATE([Address3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Address3',
      CONVERT(NVARCHAR(2000),DELETED.[Address3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Address3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[Address3] Is Null And
				DELETED.[Address3] Is Not Null
			) Or
			(
				INSERTED.[Address3] Is Not Null And
				DELETED.[Address3] Is Null
			) Or
			(
				INSERTED.[Address3] !=
				DELETED.[Address3]
			)
		) 
		END		
		
      If UPDATE([Address4])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Address4',
      CONVERT(NVARCHAR(2000),DELETED.[Address4],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Address4],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[Address4] Is Null And
				DELETED.[Address4] Is Not Null
			) Or
			(
				INSERTED.[Address4] Is Not Null And
				DELETED.[Address4] Is Null
			) Or
			(
				INSERTED.[Address4] !=
				DELETED.[Address4]
			)
		) 
		END		
		
      If UPDATE([City])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'City',
      CONVERT(NVARCHAR(2000),DELETED.[City],121),
      CONVERT(NVARCHAR(2000),INSERTED.[City],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[City] Is Null And
				DELETED.[City] Is Not Null
			) Or
			(
				INSERTED.[City] Is Not Null And
				DELETED.[City] Is Null
			) Or
			(
				INSERTED.[City] !=
				DELETED.[City]
			)
		) 
		END		
		
     If UPDATE([State])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'State',
     CONVERT(NVARCHAR(2000),DELETED.[State],121),
     CONVERT(NVARCHAR(2000),INSERTED.[State],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[State] Is Null And
				DELETED.[State] Is Not Null
			) Or
			(
				INSERTED.[State] Is Not Null And
				DELETED.[State] Is Null
			) Or
			(
				INSERTED.[State] !=
				DELETED.[State]
			)
		) left join CFGStates as oldDesc  on DELETED.State = oldDesc.Code  left join  CFGStates as newDesc  on INSERTED.State = newDesc.Code
		END		
		
      If UPDATE([ZIP])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'ZIP',
      CONVERT(NVARCHAR(2000),DELETED.[ZIP],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ZIP],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[ZIP] Is Null And
				DELETED.[ZIP] Is Not Null
			) Or
			(
				INSERTED.[ZIP] Is Not Null And
				DELETED.[ZIP] Is Null
			) Or
			(
				INSERTED.[ZIP] !=
				DELETED.[ZIP]
			)
		) 
		END		
		
      If UPDATE([Country])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Country',
      CONVERT(NVARCHAR(2000),DELETED.[Country],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Country],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[Country] Is Null And
				DELETED.[Country] Is Not Null
			) Or
			(
				INSERTED.[Country] Is Not Null And
				DELETED.[Country] Is Null
			) Or
			(
				INSERTED.[Country] !=
				DELETED.[Country]
			)
		) 
		END		
		
      If UPDATE([Phone])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Phone',
      CONVERT(NVARCHAR(2000),DELETED.[Phone],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Phone],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[Phone] Is Null And
				DELETED.[Phone] Is Not Null
			) Or
			(
				INSERTED.[Phone] Is Not Null And
				DELETED.[Phone] Is Null
			) Or
			(
				INSERTED.[Phone] !=
				DELETED.[Phone]
			)
		) 
		END		
		
      If UPDATE([FAX])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'FAX',
      CONVERT(NVARCHAR(2000),DELETED.[FAX],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FAX],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[FAX] Is Null And
				DELETED.[FAX] Is Not Null
			) Or
			(
				INSERTED.[FAX] Is Not Null And
				DELETED.[FAX] Is Null
			) Or
			(
				INSERTED.[FAX] !=
				DELETED.[FAX]
			)
		) 
		END		
		
      If UPDATE([EMail])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'EMail',
      CONVERT(NVARCHAR(2000),DELETED.[EMail],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EMail],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[EMail] Is Null And
				DELETED.[EMail] Is Not Null
			) Or
			(
				INSERTED.[EMail] Is Not Null And
				DELETED.[EMail] Is Null
			) Or
			(
				INSERTED.[EMail] !=
				DELETED.[EMail]
			)
		) 
		END		
		
      If UPDATE([TaxCountryCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'TaxCountryCode',
      CONVERT(NVARCHAR(2000),DELETED.[TaxCountryCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TaxCountryCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[TaxCountryCode] Is Null And
				DELETED.[TaxCountryCode] Is Not Null
			) Or
			(
				INSERTED.[TaxCountryCode] Is Not Null And
				DELETED.[TaxCountryCode] Is Null
			) Or
			(
				INSERTED.[TaxCountryCode] !=
				DELETED.[TaxCountryCode]
			)
		) 
		END		
		
      If UPDATE([TaxRegistrationNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'TaxRegistrationNumber',
      CONVERT(NVARCHAR(2000),DELETED.[TaxRegistrationNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TaxRegistrationNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[TaxRegistrationNumber] Is Null And
				DELETED.[TaxRegistrationNumber] Is Not Null
			) Or
			(
				INSERTED.[TaxRegistrationNumber] Is Not Null And
				DELETED.[TaxRegistrationNumber] Is Null
			) Or
			(
				INSERTED.[TaxRegistrationNumber] !=
				DELETED.[TaxRegistrationNumber]
			)
		) 
		END		
		
      If UPDATE([PrimaryInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'PrimaryInd',
      CONVERT(NVARCHAR(2000),DELETED.[PrimaryInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PrimaryInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[PrimaryInd] Is Null And
				DELETED.[PrimaryInd] Is Not Null
			) Or
			(
				INSERTED.[PrimaryInd] Is Not Null And
				DELETED.[PrimaryInd] Is Null
			) Or
			(
				INSERTED.[PrimaryInd] !=
				DELETED.[PrimaryInd]
			)
		) 
		END		
		
      If UPDATE([Billing])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Billing',
      CONVERT(NVARCHAR(2000),DELETED.[Billing],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Billing],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[Billing] Is Null And
				DELETED.[Billing] Is Not Null
			) Or
			(
				INSERTED.[Billing] Is Not Null And
				DELETED.[Billing] Is Null
			) Or
			(
				INSERTED.[Billing] !=
				DELETED.[Billing]
			)
		) 
		END		
		
      If UPDATE([Accounting])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Accounting',
      CONVERT(NVARCHAR(2000),DELETED.[Accounting],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Accounting],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[Accounting] Is Null And
				DELETED.[Accounting] Is Not Null
			) Or
			(
				INSERTED.[Accounting] Is Not Null And
				DELETED.[Accounting] Is Null
			) Or
			(
				INSERTED.[Accounting] !=
				DELETED.[Accounting]
			)
		) 
		END		
		
      If UPDATE([PhoneFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'PhoneFormat',
      CONVERT(NVARCHAR(2000),DELETED.[PhoneFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PhoneFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[PhoneFormat] Is Null And
				DELETED.[PhoneFormat] Is Not Null
			) Or
			(
				INSERTED.[PhoneFormat] Is Not Null And
				DELETED.[PhoneFormat] Is Null
			) Or
			(
				INSERTED.[PhoneFormat] !=
				DELETED.[PhoneFormat]
			)
		) 
		END		
		
      If UPDATE([FaxFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'FaxFormat',
      CONVERT(NVARCHAR(2000),DELETED.[FaxFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FaxFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[FaxFormat] Is Null And
				DELETED.[FaxFormat] Is Not Null
			) Or
			(
				INSERTED.[FaxFormat] Is Not Null And
				DELETED.[FaxFormat] Is Null
			) Or
			(
				INSERTED.[FaxFormat] !=
				DELETED.[FaxFormat]
			)
		) 
		END		
		
      If UPDATE([CLAddressID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'CLAddressID',
      CONVERT(NVARCHAR(2000),DELETED.[CLAddressID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CLAddressID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[CLAddressID] Is Null And
				DELETED.[CLAddressID] Is Not Null
			) Or
			(
				INSERTED.[CLAddressID] Is Not Null And
				DELETED.[CLAddressID] Is Null
			) Or
			(
				INSERTED.[CLAddressID] !=
				DELETED.[CLAddressID]
			)
		) 
		END		
		
      If UPDATE([Payment])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'Payment',
      CONVERT(NVARCHAR(2000),DELETED.[Payment],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Payment],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[Payment] Is Null And
				DELETED.[Payment] Is Not Null
			) Or
			(
				INSERTED.[Payment] Is Not Null And
				DELETED.[Payment] Is Null
			) Or
			(
				INSERTED.[Payment] !=
				DELETED.[Payment]
			)
		) 
		END		
		
      If UPDATE([QBOID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'QBOID',
      CONVERT(NVARCHAR(2000),DELETED.[QBOID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[QBOID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[QBOID] Is Null And
				DELETED.[QBOID] Is Not Null
			) Or
			(
				INSERTED.[QBOID] Is Not Null And
				DELETED.[QBOID] Is Null
			) Or
			(
				INSERTED.[QBOID] !=
				DELETED.[QBOID]
			)
		) 
		END		
		
      If UPDATE([QBOAddressID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'QBOAddressID',
      CONVERT(NVARCHAR(2000),DELETED.[QBOAddressID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[QBOAddressID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[QBOAddressID] Is Null And
				DELETED.[QBOAddressID] Is Not Null
			) Or
			(
				INSERTED.[QBOAddressID] Is Not Null And
				DELETED.[QBOAddressID] Is Null
			) Or
			(
				INSERTED.[QBOAddressID] !=
				DELETED.[QBOAddressID]
			)
		) 
		END		
		
      If UPDATE([QBOIsBillingAddr])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'QBOIsBillingAddr',
      CONVERT(NVARCHAR(2000),DELETED.[QBOIsBillingAddr],121),
      CONVERT(NVARCHAR(2000),INSERTED.[QBOIsBillingAddr],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[QBOIsBillingAddr] Is Null And
				DELETED.[QBOIsBillingAddr] Is Not Null
			) Or
			(
				INSERTED.[QBOIsBillingAddr] Is Not Null And
				DELETED.[QBOIsBillingAddr] Is Null
			) Or
			(
				INSERTED.[QBOIsBillingAddr] !=
				DELETED.[QBOIsBillingAddr]
			)
		) 
		END		
		
      If UPDATE([QBOIsShippingAddr])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'QBOIsShippingAddr',
      CONVERT(NVARCHAR(2000),DELETED.[QBOIsShippingAddr],121),
      CONVERT(NVARCHAR(2000),INSERTED.[QBOIsShippingAddr],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[QBOIsShippingAddr] Is Null And
				DELETED.[QBOIsShippingAddr] Is Not Null
			) Or
			(
				INSERTED.[QBOIsShippingAddr] Is Not Null And
				DELETED.[QBOIsShippingAddr] Is Null
			) Or
			(
				INSERTED.[QBOIsShippingAddr] !=
				DELETED.[QBOIsShippingAddr]
			)
		) 
		END		
		
      If UPDATE([QBOLastUpdated])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Address],121),'QBOLastUpdated',
      CONVERT(NVARCHAR(2000),DELETED.[QBOLastUpdated],121),
      CONVERT(NVARCHAR(2000),INSERTED.[QBOLastUpdated],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND INSERTED.[Address] = DELETED.[Address] AND 
		(
			(
				INSERTED.[QBOLastUpdated] Is Null And
				DELETED.[QBOLastUpdated] Is Not Null
			) Or
			(
				INSERTED.[QBOLastUpdated] Is Not Null And
				DELETED.[QBOLastUpdated] Is Null
			) Or
			(
				INSERTED.[QBOLastUpdated] !=
				DELETED.[QBOLastUpdated]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[CLAddress] ADD CONSTRAINT [CLAddressPK] PRIMARY KEY NONCLUSTERED ([ClientID], [Address]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CLAddressCLAddressIDIDX] ON [dbo].[CLAddress] ([CLAddressID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [CLAddressClientIDAddressIDX] ON [dbo].[CLAddress] ([ClientID], [Address]) INCLUDE ([Address1], [Address2], [Address3], [Address4], [City], [State], [ZIP], [Phone], [PhoneFormat], [Country], [PrimaryInd]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CLAddressCreateDateIDX] ON [dbo].[CLAddress] ([CreateDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CLAddressModDateIDX] ON [dbo].[CLAddress] ([ModDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CLAddressQBOIDIDX] ON [dbo].[CLAddress] ([QBOID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
