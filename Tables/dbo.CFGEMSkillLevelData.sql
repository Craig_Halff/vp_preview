CREATE TABLE [dbo].[CFGEMSkillLevelData]
(
[Code] [smallint] NOT NULL CONSTRAINT [DF__CFGEMSkill__Code__355DD6AE] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGEMSkillLevelData] ADD CONSTRAINT [CFGEMSkillLevelDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
