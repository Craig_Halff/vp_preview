CREATE TABLE [dbo].[xref_Org_Merge]
(
[ExistingOrg] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MergeOrg] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Companies] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Offices] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[teams] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Merge_Companies] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Merge_Offices] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Merge_Teams] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Merge_Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
