CREATE TABLE [dbo].[DataSource]
(
[DataSourceID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DataSourceName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Category] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JsonDefinition] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[TargetID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataPackReference] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsDefault] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__DataSourc__IsDef__543BF19A] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DataSource] ADD CONSTRAINT [DataSourcePK] PRIMARY KEY CLUSTERED ([DataSourceID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
