CREATE TABLE [dbo].[CAB]
(
[BudgetName] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoDistribute] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CAB_New__AutoDis__271C0504] DEFAULT ('N'),
[CompoundPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CAB_New__Compoun__2810293D] DEFAULT ((0)),
[CompoundAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CAB_New__Compoun__29044D76] DEFAULT ((0)),
[AdjustPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CAB_New__AdjustP__29F871AF] DEFAULT ((0)),
[AdjustAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CAB_New__AdjustA__2AEC95E8] DEFAULT ((0)),
[StartPeriod] [smallint] NOT NULL CONSTRAINT [DF__CAB_New__StartPe__2BE0BA21] DEFAULT ((0)),
[EndPeriod] [smallint] NOT NULL CONSTRAINT [DF__CAB_New__EndPeri__2CD4DE5A] DEFAULT ((0)),
[BudgetYear] [smallint] NOT NULL CONSTRAINT [DF__CAB_New__BudgetY__2DC90293] DEFAULT ((0)),
[AvailableForReporting] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CAB_New__Availab__2EBD26CC] DEFAULT ('Y'),
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedDate] [datetime] NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CAB] ADD CONSTRAINT [CABPK] PRIMARY KEY NONCLUSTERED ([BudgetName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
