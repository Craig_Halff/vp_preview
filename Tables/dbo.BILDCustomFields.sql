CREATE TABLE [dbo].[BILDCustomFields]
(
[Period] [int] NOT NULL CONSTRAINT [DF__BILDCusto__Perio__199A4EB9] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__BILDCusto__PostS__1A8E72F2] DEFAULT ((0)),
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustTKUD01] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTKUD02] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__BILDCusto__CustT__1B82972B] DEFAULT ((0)),
[CustTKUD03] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BILDCustomFields] ADD CONSTRAINT [BILDCustomFieldsPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
