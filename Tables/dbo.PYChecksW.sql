CREATE TABLE [dbo].[PYChecksW]
(
[Period] [int] NOT NULL CONSTRAINT [DF__PYChecksW__Perio__3674EDDB] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__PYChecksW__PostS__37691214] DEFAULT ((0)),
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Method] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__AmtPc__385D364D] DEFAULT ((0)),
[Suppress] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksW__Suppr__39515A86] DEFAULT ('N'),
[Limit] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__Limit__3A457EBF] DEFAULT ((0)),
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__Amoun__3B39A2F8] DEFAULT ((0)),
[Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksW__Overr__3C2DC731] DEFAULT ('N'),
[QTDAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__QTDAm__3D21EB6A] DEFAULT ((0)),
[YTDAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__YTDAm__3E160FA3] DEFAULT ((0)),
[TaxablePayBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__Taxab__3F0A33DC] DEFAULT ((0)),
[AdjustedGrossPayBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__Adjus__3FFE5815] DEFAULT ((0)),
[Amt401K] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__Amt40__40F27C4E] DEFAULT ((0)),
[Amt125] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__Amt12__41E6A087] DEFAULT ((0)),
[Exclude401k] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksW__Exclu__42DAC4C0] DEFAULT ('N'),
[ExcludeCafeteria] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksW__Exclu__43CEE8F9] DEFAULT ('N'),
[ExcludeOtherPay1] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksW__Exclu__44C30D32] DEFAULT ('N'),
[ExcludeOtherPay2] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksW__Exclu__45B7316B] DEFAULT ('N'),
[ExcludeOtherPay3] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksW__Exclu__46AB55A4] DEFAULT ('N'),
[ExcludeOtherPay4] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksW__Exclu__479F79DD] DEFAULT ('N'),
[ExcludeOtherPay5] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksW__Exclu__48939E16] DEFAULT ('N'),
[OtherPay1] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__Other__4987C24F] DEFAULT ((0)),
[OtherPay2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__Other__4A7BE688] DEFAULT ((0)),
[OtherPay3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__Other__4B700AC1] DEFAULT ((0)),
[OtherPay4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__Other__4C642EFA] DEFAULT ((0)),
[OtherPay5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__Other__4D585333] DEFAULT ((0)),
[FormW4Version] [smallint] NOT NULL CONSTRAINT [DF__PYChecksW__FormW__4E4C776C] DEFAULT ((0)),
[FormW4Step2] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksW__FormW__4F409BA5] DEFAULT ('N'),
[FormW4Dependents] [smallint] NOT NULL CONSTRAINT [DF__PYChecksW__FormW__5034BFDE] DEFAULT ((0)),
[FormW4DependentsOther] [smallint] NOT NULL CONSTRAINT [DF__PYChecksW__FormW__5128E417] DEFAULT ((0)),
[FormW4OtherTaxCredit] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__FormW__521D0850] DEFAULT ((0)),
[FormW4OtherIncome] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__FormW__53112C89] DEFAULT ((0)),
[FormW4Deductions] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__FormW__540550C2] DEFAULT ((0)),
[FormW4DependentsAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__FormW__54F974FB] DEFAULT ((0)),
[FormW4DependentsOtherAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksW__FormW__55ED9934] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PYChecksW] ADD CONSTRAINT [PYChecksWPK] PRIMARY KEY NONCLUSTERED ([Period], [PostSeq], [Employee], [Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
