CREATE TABLE [dbo].[SF330TemplateFiles]
(
[TemplateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PageDescriptor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TemplateDoc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__SF330Templa__Seq__7A9FFAF6] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF330TemplateFiles] ADD CONSTRAINT [SF330TemplateFilesPK] PRIMARY KEY NONCLUSTERED ([TemplateID], [PageDescriptor]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
