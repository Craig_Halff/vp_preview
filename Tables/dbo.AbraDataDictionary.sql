CREATE TABLE [dbo].[AbraDataDictionary]
(
[Abfield] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Width] [int] NOT NULL CONSTRAINT [DF__AbraDataD__Width__699DF578] DEFAULT ((0)),
[Dec] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AbraDataDictionary] ADD CONSTRAINT [AbraDataDictionaryPK] PRIMARY KEY CLUSTERED ([Abfield]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
