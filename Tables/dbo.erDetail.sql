CREATE TABLE [dbo].[erDetail]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__erDetail_Ne__Seq__0C7DE8B8] DEFAULT ((0)),
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__erDetail___Amoun__0D720CF1] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[erDetail] ADD CONSTRAINT [erDetailPK] PRIMARY KEY NONCLUSTERED ([Batch], [RefNo], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
