CREATE TABLE [dbo].[FW_CustomFile]
(
[Filename] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomFile] [varbinary] (max) NOT NULL,
[Type] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PropertyBag] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CustomFile] ADD CONSTRAINT [FW_CustomFilePK] PRIMARY KEY CLUSTERED ([Filename]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
