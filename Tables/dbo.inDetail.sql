CREATE TABLE [dbo].[inDetail]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__inDetail_Ne__Seq__22581B3E] DEFAULT ((0)),
[InvoiceSection] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__inDetail___Amoun__234C3F77] DEFAULT ((0)),
[RetAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__inDetail___RetAm__244063B0] DEFAULT ((0)),
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__inDetail___TaxBa__253487E9] DEFAULT ((0)),
[Retainer] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__inDetail___Retai__2628AC22] DEFAULT ('N'),
[LinkWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountProjectFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__inDetail___Amoun__271CD05B] DEFAULT ((0)),
[CreditMemoRefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[inDetail] ADD CONSTRAINT [inDetailPK] PRIMARY KEY NONCLUSTERED ([Batch], [Invoice], [WBS1], [WBS2], [WBS3], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
