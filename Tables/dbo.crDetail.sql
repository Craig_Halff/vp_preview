CREATE TABLE [dbo].[crDetail]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__crDetail_Ne__Seq__6D4F5622] DEFAULT ((0)),
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__crDetail___Amoun__6E437A5B] DEFAULT ((0)),
[Interest] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__crDetail___Inter__6F379E94] DEFAULT ('N'),
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__crDetail___TaxBa__702BC2CD] DEFAULT ((0)),
[Retainer] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__crDetail___Retai__711FE706] DEFAULT ('N'),
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__crDetail___Curre__72140B3F] DEFAULT ((0)),
[SourceAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__crDetail___Sourc__73082F78] DEFAULT ((0)),
[SourceExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreInvoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[crDetail] ADD CONSTRAINT [crDetailPK] PRIMARY KEY NONCLUSTERED ([Batch], [RefNo], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
