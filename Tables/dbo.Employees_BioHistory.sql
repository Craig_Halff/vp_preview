CREATE TABLE [dbo].[Employees_BioHistory]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustBioHistoryDate] [datetime] NULL,
[CustBioHistoryCategory] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBioHistoryOld] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBioHistoryNew] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBioHistoryModUser] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBioHistoryExplanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_Employees_BioHistory]
      ON [dbo].[Employees_BioHistory]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Employees_BioHistory'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Employee',CONVERT(NVARCHAR(2000),[Employee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Seq',CONVERT(NVARCHAR(2000),[Seq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustBioHistoryDate',CONVERT(NVARCHAR(2000),[CustBioHistoryDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustBioHistoryCategory',CONVERT(NVARCHAR(2000),[CustBioHistoryCategory],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustBioHistoryOld','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustBioHistoryNew','[text]',NULL, @source,@app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustBioHistoryModUser',CONVERT(NVARCHAR(2000),DELETED.[CustBioHistoryModUser],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustBioHistoryModUser = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustBioHistoryExplanation',CONVERT(NVARCHAR(2000),[CustBioHistoryExplanation],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_Employees_BioHistory] ON [dbo].[Employees_BioHistory]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_Employees_BioHistory]
      ON [dbo].[Employees_BioHistory]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Employees_BioHistory'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Employee',NULL,CONVERT(NVARCHAR(2000),[Employee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',NULL,CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBioHistoryDate',NULL,CONVERT(NVARCHAR(2000),[CustBioHistoryDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBioHistoryCategory',NULL,CONVERT(NVARCHAR(2000),[CustBioHistoryCategory],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBioHistoryOld',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBioHistoryNew',NULL,'[text]', @source, @app
      FROM INSERTED


    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBioHistoryModUser',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustBioHistoryModUser],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustBioHistoryModUser = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBioHistoryExplanation',NULL,CONVERT(NVARCHAR(2000),[CustBioHistoryExplanation],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_Employees_BioHistory] ON [dbo].[Employees_BioHistory]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_Employees_BioHistory]
      ON [dbo].[Employees_BioHistory]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Employees_BioHistory'
    
      If UPDATE([Employee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Employee',
      CONVERT(NVARCHAR(2000),DELETED.[Employee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Employee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Employee] Is Null And
				DELETED.[Employee] Is Not Null
			) Or
			(
				INSERTED.[Employee] Is Not Null And
				DELETED.[Employee] Is Null
			) Or
			(
				INSERTED.[Employee] !=
				DELETED.[Employee]
			)
		) 
		END		
		
      If UPDATE([Seq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',
      CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] Is Null And
				DELETED.[Seq] Is Not Null
			) Or
			(
				INSERTED.[Seq] Is Not Null And
				DELETED.[Seq] Is Null
			) Or
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
		
      If UPDATE([CustBioHistoryDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBioHistoryDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustBioHistoryDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBioHistoryDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustBioHistoryDate] Is Null And
				DELETED.[CustBioHistoryDate] Is Not Null
			) Or
			(
				INSERTED.[CustBioHistoryDate] Is Not Null And
				DELETED.[CustBioHistoryDate] Is Null
			) Or
			(
				INSERTED.[CustBioHistoryDate] !=
				DELETED.[CustBioHistoryDate]
			)
		) 
		END		
		
      If UPDATE([CustBioHistoryCategory])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBioHistoryCategory',
      CONVERT(NVARCHAR(2000),DELETED.[CustBioHistoryCategory],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBioHistoryCategory],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustBioHistoryCategory] Is Null And
				DELETED.[CustBioHistoryCategory] Is Not Null
			) Or
			(
				INSERTED.[CustBioHistoryCategory] Is Not Null And
				DELETED.[CustBioHistoryCategory] Is Null
			) Or
			(
				INSERTED.[CustBioHistoryCategory] !=
				DELETED.[CustBioHistoryCategory]
			)
		) 
		END		
		
      If UPDATE([CustBioHistoryOld])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBioHistoryOld',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CustBioHistoryNew])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBioHistoryNew',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
     If UPDATE([CustBioHistoryModUser])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBioHistoryModUser',
     CONVERT(NVARCHAR(2000),DELETED.[CustBioHistoryModUser],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustBioHistoryModUser],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustBioHistoryModUser] Is Null And
				DELETED.[CustBioHistoryModUser] Is Not Null
			) Or
			(
				INSERTED.[CustBioHistoryModUser] Is Not Null And
				DELETED.[CustBioHistoryModUser] Is Null
			) Or
			(
				INSERTED.[CustBioHistoryModUser] !=
				DELETED.[CustBioHistoryModUser]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustBioHistoryModUser = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustBioHistoryModUser = newDesc.Employee
		END		
		
      If UPDATE([CustBioHistoryExplanation])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustBioHistoryExplanation',
      CONVERT(NVARCHAR(2000),DELETED.[CustBioHistoryExplanation],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBioHistoryExplanation],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustBioHistoryExplanation] Is Null And
				DELETED.[CustBioHistoryExplanation] Is Not Null
			) Or
			(
				INSERTED.[CustBioHistoryExplanation] Is Not Null And
				DELETED.[CustBioHistoryExplanation] Is Null
			) Or
			(
				INSERTED.[CustBioHistoryExplanation] !=
				DELETED.[CustBioHistoryExplanation]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_Employees_BioHistory] ON [dbo].[Employees_BioHistory]
GO
ALTER TABLE [dbo].[Employees_BioHistory] ADD CONSTRAINT [Employees_BioHistoryPK] PRIMARY KEY CLUSTERED ([Employee], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
