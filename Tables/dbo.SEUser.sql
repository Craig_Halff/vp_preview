CREATE TABLE [dbo].[SEUser]
(
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Role] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisableLogin] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser_Ne__Disab__52C713C6] DEFAULT ('N'),
[LastSF254] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastSF255] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SF254Pref] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SF255Pref] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDict] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ColorScheme] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartPage] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartPageDesc] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PopupAlertsEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser_Ne__Popup__53BB37FF] DEFAULT ('N'),
[EMailAlertsEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser_Ne__EMail__54AF5C38] DEFAULT ('N'),
[MailboxAlias] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MailboxServer] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SyncCalendarWhere] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SyncContactsWhere] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SyncTasksWhere] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastCustomProposal] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoginAttempts] [smallint] NOT NULL CONSTRAINT [DF__SEUser_Ne__Login__55A38071] DEFAULT ((0)),
[LastPasswordChange] [datetime] NULL,
[AutoHide] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser_Ne__AutoH__5697A4AA] DEFAULT ('N'),
[DefaultPrinter] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SEUser_DefaultPrinter] DEFAULT ('<local printer>'),
[AutoRetrieve] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser_Ne__AutoR__578BC8E3] DEFAULT ('N'),
[ReportServer] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Domain] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProgressiveViewing] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser_Ne__Progr__587FED1C] DEFAULT ('Y'),
[DefaultFont] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultPageWidth] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__SEUser_Ne__Defau__59741155] DEFAULT ((0)),
[DefaultPageHeight] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__SEUser_Ne__Defau__5A68358E] DEFAULT ((0)),
[DefaultTopMargin] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__SEUser_Ne__Defau__5B5C59C7] DEFAULT ((0)),
[DefaultBottomMargin] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__SEUser_Ne__Defau__5C507E00] DEFAULT ((0)),
[DefaultLeftMargin] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__SEUser_Ne__Defau__5D44A239] DEFAULT ((0)),
[DefaultRightMargin] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__SEUser_Ne__Defau__5E38C672] DEFAULT ((0)),
[SupportUsername] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupportPassword] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShortDateFormat] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MediumDateFormat] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LongDateFormat] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateSeparator] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeFormat] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[decimalSymbol] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThousandsSeparator] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoLoginDefaultCompany] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser_Ne__AutoL__5F2CEAAB] DEFAULT ('N'),
[AutoLoginCurrentPeriod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser_Ne__AutoL__60210EE4] DEFAULT ('N'),
[LastSF330] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoundsLikeSearch] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser_Ne__Sound__6115331D] DEFAULT ('N'),
[WindowsUsername] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectDownload] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoRetrieveEM] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser_Ne__AutoR__62095756] DEFAULT ('N'),
[ContactDownload] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientDownload] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActivityDownload] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OpportunityDownload] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultCountry] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultLanguage] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PageSize] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitOfMeasure] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForceChangePassword] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser_Ne__Force__62FD7B8F] DEFAULT ('N'),
[DelegateInvoiceApproval] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser_Ne__Deleg__63F19FC8] DEFAULT ('N'),
[DelegateEmployee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FlatDashboardStyling] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser_Ne__FlatD__64E5C401] DEFAULT ('N'),
[LoginAlertDateDismissed] [datetime] NULL,
[IQAccessToken] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IQRefreshToken] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnableICDashpart] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser_Ne__Enabl__65D9E83A] DEFAULT ('Y'),
[ODBCEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser_Ne__ODBCE__66CE0C73] DEFAULT ('N'),
[ODBCUsername] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ODBCPassword] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[TwoFactorAuth] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser__TwoFacto__597FC9A4] DEFAULT ('N'),
[TwoFactorAuthConfigured] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser__TwoFacto__055E4BE2] DEFAULT ('N'),
[DisplayUsername] [nvarchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SEUser__Status__0B8C2684] DEFAULT ('A')
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_SEUser] 
      ON [dbo].[SEUser]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SEUser '
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'Username',CONVERT(NVARCHAR(2000),[Username],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'Role',CONVERT(NVARCHAR(2000),[Role],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'Password',CONVERT(NVARCHAR(2000),[Password],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'Employee',CONVERT(NVARCHAR(2000),[Employee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'DisableLogin',CONVERT(NVARCHAR(2000),[DisableLogin],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'LastSF254',CONVERT(NVARCHAR(2000),[LastSF254],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'LastSF255',CONVERT(NVARCHAR(2000),[LastSF255],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'SF254Pref',CONVERT(NVARCHAR(2000),[SF254Pref],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'SF255Pref',CONVERT(NVARCHAR(2000),[SF255Pref],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'CustDict','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'ColorScheme',CONVERT(NVARCHAR(2000),[ColorScheme],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'StartPage',CONVERT(NVARCHAR(2000),[StartPage],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'StartPageDesc',CONVERT(NVARCHAR(2000),[StartPageDesc],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'PopupAlertsEnabled',CONVERT(NVARCHAR(2000),[PopupAlertsEnabled],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'EMailAlertsEnabled',CONVERT(NVARCHAR(2000),[EMailAlertsEnabled],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'MailboxAlias',CONVERT(NVARCHAR(2000),[MailboxAlias],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'MailboxServer',CONVERT(NVARCHAR(2000),[MailboxServer],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'SyncCalendarWhere',CONVERT(NVARCHAR(2000),[SyncCalendarWhere],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'SyncContactsWhere',CONVERT(NVARCHAR(2000),[SyncContactsWhere],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'SyncTasksWhere',CONVERT(NVARCHAR(2000),[SyncTasksWhere],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'LastCustomProposal',CONVERT(NVARCHAR(2000),[LastCustomProposal],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'LoginAttempts',CONVERT(NVARCHAR(2000),[LoginAttempts],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'LastPasswordChange',CONVERT(NVARCHAR(2000),[LastPasswordChange],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'AutoHide',CONVERT(NVARCHAR(2000),[AutoHide],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'DefaultPrinter',CONVERT(NVARCHAR(2000),[DefaultPrinter],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'AutoRetrieve',CONVERT(NVARCHAR(2000),[AutoRetrieve],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'ReportServer',CONVERT(NVARCHAR(2000),[ReportServer],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'Domain',CONVERT(NVARCHAR(2000),[Domain],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'ProgressiveViewing',CONVERT(NVARCHAR(2000),[ProgressiveViewing],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'DefaultFont',CONVERT(NVARCHAR(2000),[DefaultFont],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'DefaultPageWidth',CONVERT(NVARCHAR(2000),[DefaultPageWidth],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'DefaultPageHeight',CONVERT(NVARCHAR(2000),[DefaultPageHeight],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'DefaultTopMargin',CONVERT(NVARCHAR(2000),[DefaultTopMargin],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'DefaultBottomMargin',CONVERT(NVARCHAR(2000),[DefaultBottomMargin],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'DefaultLeftMargin',CONVERT(NVARCHAR(2000),[DefaultLeftMargin],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'DefaultRightMargin',CONVERT(NVARCHAR(2000),[DefaultRightMargin],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'SupportUsername',CONVERT(NVARCHAR(2000),[SupportUsername],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'SupportPassword',CONVERT(NVARCHAR(2000),[SupportPassword],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'ShortDateFormat',CONVERT(NVARCHAR(2000),[ShortDateFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'MediumDateFormat',CONVERT(NVARCHAR(2000),[MediumDateFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'LongDateFormat',CONVERT(NVARCHAR(2000),[LongDateFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'DateSeparator',CONVERT(NVARCHAR(2000),[DateSeparator],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'TimeFormat',CONVERT(NVARCHAR(2000),[TimeFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'decimalSymbol',CONVERT(NVARCHAR(2000),[decimalSymbol],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'ThousandsSeparator',CONVERT(NVARCHAR(2000),[ThousandsSeparator],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'AutoLoginDefaultCompany',CONVERT(NVARCHAR(2000),[AutoLoginDefaultCompany],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'AutoLoginCurrentPeriod',CONVERT(NVARCHAR(2000),[AutoLoginCurrentPeriod],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'LastSF330',CONVERT(NVARCHAR(2000),[LastSF330],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'SoundsLikeSearch',CONVERT(NVARCHAR(2000),[SoundsLikeSearch],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'WindowsUsername',CONVERT(NVARCHAR(2000),[WindowsUsername],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'ProjectDownload','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'AutoRetrieveEM',CONVERT(NVARCHAR(2000),[AutoRetrieveEM],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'ContactDownload','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'ClientDownload','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'ActivityDownload','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'OpportunityDownload','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'DefaultCountry',CONVERT(NVARCHAR(2000),[DefaultCountry],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'DefaultLanguage',CONVERT(NVARCHAR(2000),[DefaultLanguage],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'PageSize',CONVERT(NVARCHAR(2000),[PageSize],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'UnitOfMeasure',CONVERT(NVARCHAR(2000),[UnitOfMeasure],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'ForceChangePassword',CONVERT(NVARCHAR(2000),[ForceChangePassword],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'DelegateInvoiceApproval',CONVERT(NVARCHAR(2000),[DelegateInvoiceApproval],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'DelegateEmployee',CONVERT(NVARCHAR(2000),[DelegateEmployee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'FlatDashboardStyling',CONVERT(NVARCHAR(2000),[FlatDashboardStyling],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'LoginAlertDateDismissed',CONVERT(NVARCHAR(2000),[LoginAlertDateDismissed],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'IQAccessToken',CONVERT(NVARCHAR(2000),[IQAccessToken],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'IQRefreshToken',CONVERT(NVARCHAR(2000),[IQRefreshToken],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'EnableICDashpart',CONVERT(NVARCHAR(2000),[EnableICDashpart],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'ODBCEnabled',CONVERT(NVARCHAR(2000),[ODBCEnabled],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'ODBCUsername',CONVERT(NVARCHAR(2000),[ODBCUsername],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'ODBCPassword',CONVERT(NVARCHAR(2000),[ODBCPassword],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'TwoFactorAuth',CONVERT(NVARCHAR(2000),[TwoFactorAuth],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'TwoFactorAuthConfigured',CONVERT(NVARCHAR(2000),[TwoFactorAuthConfigured],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'DisplayUsername',CONVERT(NVARCHAR(2000),[DisplayUsername],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Username],121),'Status',CONVERT(NVARCHAR(2000),[Status],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_SEUser] 
      ON [dbo].[SEUser]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SEUser '
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'Username',NULL,CONVERT(NVARCHAR(2000),[Username],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'Role',NULL,CONVERT(NVARCHAR(2000),[Role],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'Password',NULL,CONVERT(NVARCHAR(2000),[Password],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'Employee',NULL,CONVERT(NVARCHAR(2000),[Employee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DisableLogin',NULL,CONVERT(NVARCHAR(2000),[DisableLogin],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'LastSF254',NULL,CONVERT(NVARCHAR(2000),[LastSF254],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'LastSF255',NULL,CONVERT(NVARCHAR(2000),[LastSF255],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'SF254Pref',NULL,CONVERT(NVARCHAR(2000),[SF254Pref],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'SF255Pref',NULL,CONVERT(NVARCHAR(2000),[SF255Pref],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'CustDict',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ColorScheme',NULL,CONVERT(NVARCHAR(2000),[ColorScheme],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'StartPage',NULL,CONVERT(NVARCHAR(2000),[StartPage],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'StartPageDesc',NULL,CONVERT(NVARCHAR(2000),[StartPageDesc],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'PopupAlertsEnabled',NULL,CONVERT(NVARCHAR(2000),[PopupAlertsEnabled],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'EMailAlertsEnabled',NULL,CONVERT(NVARCHAR(2000),[EMailAlertsEnabled],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'MailboxAlias',NULL,CONVERT(NVARCHAR(2000),[MailboxAlias],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'MailboxServer',NULL,CONVERT(NVARCHAR(2000),[MailboxServer],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'SyncCalendarWhere',NULL,CONVERT(NVARCHAR(2000),[SyncCalendarWhere],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'SyncContactsWhere',NULL,CONVERT(NVARCHAR(2000),[SyncContactsWhere],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'SyncTasksWhere',NULL,CONVERT(NVARCHAR(2000),[SyncTasksWhere],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'LastCustomProposal',NULL,CONVERT(NVARCHAR(2000),[LastCustomProposal],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'LoginAttempts',NULL,CONVERT(NVARCHAR(2000),[LoginAttempts],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'LastPasswordChange',NULL,CONVERT(NVARCHAR(2000),[LastPasswordChange],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'AutoHide',NULL,CONVERT(NVARCHAR(2000),[AutoHide],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultPrinter',NULL,CONVERT(NVARCHAR(2000),[DefaultPrinter],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'AutoRetrieve',NULL,CONVERT(NVARCHAR(2000),[AutoRetrieve],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ReportServer',NULL,CONVERT(NVARCHAR(2000),[ReportServer],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'Domain',NULL,CONVERT(NVARCHAR(2000),[Domain],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ProgressiveViewing',NULL,CONVERT(NVARCHAR(2000),[ProgressiveViewing],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultFont',NULL,CONVERT(NVARCHAR(2000),[DefaultFont],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultPageWidth',NULL,CONVERT(NVARCHAR(2000),[DefaultPageWidth],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultPageHeight',NULL,CONVERT(NVARCHAR(2000),[DefaultPageHeight],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultTopMargin',NULL,CONVERT(NVARCHAR(2000),[DefaultTopMargin],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultBottomMargin',NULL,CONVERT(NVARCHAR(2000),[DefaultBottomMargin],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultLeftMargin',NULL,CONVERT(NVARCHAR(2000),[DefaultLeftMargin],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultRightMargin',NULL,CONVERT(NVARCHAR(2000),[DefaultRightMargin],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'SupportUsername',NULL,CONVERT(NVARCHAR(2000),[SupportUsername],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'SupportPassword',NULL,CONVERT(NVARCHAR(2000),[SupportPassword],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ShortDateFormat',NULL,CONVERT(NVARCHAR(2000),[ShortDateFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'MediumDateFormat',NULL,CONVERT(NVARCHAR(2000),[MediumDateFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'LongDateFormat',NULL,CONVERT(NVARCHAR(2000),[LongDateFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DateSeparator',NULL,CONVERT(NVARCHAR(2000),[DateSeparator],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'TimeFormat',NULL,CONVERT(NVARCHAR(2000),[TimeFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'decimalSymbol',NULL,CONVERT(NVARCHAR(2000),[decimalSymbol],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ThousandsSeparator',NULL,CONVERT(NVARCHAR(2000),[ThousandsSeparator],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'AutoLoginDefaultCompany',NULL,CONVERT(NVARCHAR(2000),[AutoLoginDefaultCompany],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'AutoLoginCurrentPeriod',NULL,CONVERT(NVARCHAR(2000),[AutoLoginCurrentPeriod],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'LastSF330',NULL,CONVERT(NVARCHAR(2000),[LastSF330],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'SoundsLikeSearch',NULL,CONVERT(NVARCHAR(2000),[SoundsLikeSearch],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'WindowsUsername',NULL,CONVERT(NVARCHAR(2000),[WindowsUsername],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ProjectDownload',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'AutoRetrieveEM',NULL,CONVERT(NVARCHAR(2000),[AutoRetrieveEM],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ContactDownload',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ClientDownload',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ActivityDownload',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'OpportunityDownload',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultCountry',NULL,CONVERT(NVARCHAR(2000),[DefaultCountry],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultLanguage',NULL,CONVERT(NVARCHAR(2000),[DefaultLanguage],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'PageSize',NULL,CONVERT(NVARCHAR(2000),[PageSize],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'UnitOfMeasure',NULL,CONVERT(NVARCHAR(2000),[UnitOfMeasure],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ForceChangePassword',NULL,CONVERT(NVARCHAR(2000),[ForceChangePassword],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DelegateInvoiceApproval',NULL,CONVERT(NVARCHAR(2000),[DelegateInvoiceApproval],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DelegateEmployee',NULL,CONVERT(NVARCHAR(2000),[DelegateEmployee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'FlatDashboardStyling',NULL,CONVERT(NVARCHAR(2000),[FlatDashboardStyling],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'LoginAlertDateDismissed',NULL,CONVERT(NVARCHAR(2000),[LoginAlertDateDismissed],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'IQAccessToken',NULL,CONVERT(NVARCHAR(2000),[IQAccessToken],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'IQRefreshToken',NULL,CONVERT(NVARCHAR(2000),[IQRefreshToken],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'EnableICDashpart',NULL,CONVERT(NVARCHAR(2000),[EnableICDashpart],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ODBCEnabled',NULL,CONVERT(NVARCHAR(2000),[ODBCEnabled],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ODBCUsername',NULL,CONVERT(NVARCHAR(2000),[ODBCUsername],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ODBCPassword',NULL,CONVERT(NVARCHAR(2000),[ODBCPassword],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'TwoFactorAuth',NULL,CONVERT(NVARCHAR(2000),[TwoFactorAuth],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'TwoFactorAuthConfigured',NULL,CONVERT(NVARCHAR(2000),[TwoFactorAuthConfigured],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DisplayUsername',NULL,CONVERT(NVARCHAR(2000),[DisplayUsername],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'Status',NULL,CONVERT(NVARCHAR(2000),[Status],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_SEUser] 
      ON [dbo].[SEUser]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SEUser '
    
      If UPDATE([Username])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'Username',
      CONVERT(NVARCHAR(2000),DELETED.[Username],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Username],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[Username] Is Null And
				DELETED.[Username] Is Not Null
			) Or
			(
				INSERTED.[Username] Is Not Null And
				DELETED.[Username] Is Null
			) Or
			(
				INSERTED.[Username] !=
				DELETED.[Username]
			)
		) 
		END		
		
      If UPDATE([Role])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'Role',
      CONVERT(NVARCHAR(2000),DELETED.[Role],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Role],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[Role] Is Null And
				DELETED.[Role] Is Not Null
			) Or
			(
				INSERTED.[Role] Is Not Null And
				DELETED.[Role] Is Null
			) Or
			(
				INSERTED.[Role] !=
				DELETED.[Role]
			)
		) 
		END		
		
      If UPDATE([Password])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'Password',
      CONVERT(NVARCHAR(2000),DELETED.[Password],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Password],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[Password] Is Null And
				DELETED.[Password] Is Not Null
			) Or
			(
				INSERTED.[Password] Is Not Null And
				DELETED.[Password] Is Null
			) Or
			(
				INSERTED.[Password] !=
				DELETED.[Password]
			)
		) 
		END		
		
      If UPDATE([Employee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'Employee',
      CONVERT(NVARCHAR(2000),DELETED.[Employee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Employee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[Employee] Is Null And
				DELETED.[Employee] Is Not Null
			) Or
			(
				INSERTED.[Employee] Is Not Null And
				DELETED.[Employee] Is Null
			) Or
			(
				INSERTED.[Employee] !=
				DELETED.[Employee]
			)
		) 
		END		
		
      If UPDATE([DisableLogin])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DisableLogin',
      CONVERT(NVARCHAR(2000),DELETED.[DisableLogin],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DisableLogin],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[DisableLogin] Is Null And
				DELETED.[DisableLogin] Is Not Null
			) Or
			(
				INSERTED.[DisableLogin] Is Not Null And
				DELETED.[DisableLogin] Is Null
			) Or
			(
				INSERTED.[DisableLogin] !=
				DELETED.[DisableLogin]
			)
		) 
		END		
		
      If UPDATE([LastSF254])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'LastSF254',
      CONVERT(NVARCHAR(2000),DELETED.[LastSF254],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LastSF254],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[LastSF254] Is Null And
				DELETED.[LastSF254] Is Not Null
			) Or
			(
				INSERTED.[LastSF254] Is Not Null And
				DELETED.[LastSF254] Is Null
			) Or
			(
				INSERTED.[LastSF254] !=
				DELETED.[LastSF254]
			)
		) 
		END		
		
      If UPDATE([LastSF255])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'LastSF255',
      CONVERT(NVARCHAR(2000),DELETED.[LastSF255],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LastSF255],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[LastSF255] Is Null And
				DELETED.[LastSF255] Is Not Null
			) Or
			(
				INSERTED.[LastSF255] Is Not Null And
				DELETED.[LastSF255] Is Null
			) Or
			(
				INSERTED.[LastSF255] !=
				DELETED.[LastSF255]
			)
		) 
		END		
		
      If UPDATE([SF254Pref])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'SF254Pref',
      CONVERT(NVARCHAR(2000),DELETED.[SF254Pref],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SF254Pref],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[SF254Pref] Is Null And
				DELETED.[SF254Pref] Is Not Null
			) Or
			(
				INSERTED.[SF254Pref] Is Not Null And
				DELETED.[SF254Pref] Is Null
			) Or
			(
				INSERTED.[SF254Pref] !=
				DELETED.[SF254Pref]
			)
		) 
		END		
		
      If UPDATE([SF255Pref])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'SF255Pref',
      CONVERT(NVARCHAR(2000),DELETED.[SF255Pref],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SF255Pref],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[SF255Pref] Is Null And
				DELETED.[SF255Pref] Is Not Null
			) Or
			(
				INSERTED.[SF255Pref] Is Not Null And
				DELETED.[SF255Pref] Is Null
			) Or
			(
				INSERTED.[SF255Pref] !=
				DELETED.[SF255Pref]
			)
		) 
		END		
		
      If UPDATE([CustDict])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'CustDict',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([ColorScheme])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ColorScheme',
      CONVERT(NVARCHAR(2000),DELETED.[ColorScheme],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ColorScheme],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[ColorScheme] Is Null And
				DELETED.[ColorScheme] Is Not Null
			) Or
			(
				INSERTED.[ColorScheme] Is Not Null And
				DELETED.[ColorScheme] Is Null
			) Or
			(
				INSERTED.[ColorScheme] !=
				DELETED.[ColorScheme]
			)
		) 
		END		
		
      If UPDATE([StartPage])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'StartPage',
      CONVERT(NVARCHAR(2000),DELETED.[StartPage],121),
      CONVERT(NVARCHAR(2000),INSERTED.[StartPage],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[StartPage] Is Null And
				DELETED.[StartPage] Is Not Null
			) Or
			(
				INSERTED.[StartPage] Is Not Null And
				DELETED.[StartPage] Is Null
			) Or
			(
				INSERTED.[StartPage] !=
				DELETED.[StartPage]
			)
		) 
		END		
		
      If UPDATE([StartPageDesc])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'StartPageDesc',
      CONVERT(NVARCHAR(2000),DELETED.[StartPageDesc],121),
      CONVERT(NVARCHAR(2000),INSERTED.[StartPageDesc],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[StartPageDesc] Is Null And
				DELETED.[StartPageDesc] Is Not Null
			) Or
			(
				INSERTED.[StartPageDesc] Is Not Null And
				DELETED.[StartPageDesc] Is Null
			) Or
			(
				INSERTED.[StartPageDesc] !=
				DELETED.[StartPageDesc]
			)
		) 
		END		
		
      If UPDATE([PopupAlertsEnabled])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'PopupAlertsEnabled',
      CONVERT(NVARCHAR(2000),DELETED.[PopupAlertsEnabled],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PopupAlertsEnabled],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[PopupAlertsEnabled] Is Null And
				DELETED.[PopupAlertsEnabled] Is Not Null
			) Or
			(
				INSERTED.[PopupAlertsEnabled] Is Not Null And
				DELETED.[PopupAlertsEnabled] Is Null
			) Or
			(
				INSERTED.[PopupAlertsEnabled] !=
				DELETED.[PopupAlertsEnabled]
			)
		) 
		END		
		
      If UPDATE([EMailAlertsEnabled])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'EMailAlertsEnabled',
      CONVERT(NVARCHAR(2000),DELETED.[EMailAlertsEnabled],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EMailAlertsEnabled],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[EMailAlertsEnabled] Is Null And
				DELETED.[EMailAlertsEnabled] Is Not Null
			) Or
			(
				INSERTED.[EMailAlertsEnabled] Is Not Null And
				DELETED.[EMailAlertsEnabled] Is Null
			) Or
			(
				INSERTED.[EMailAlertsEnabled] !=
				DELETED.[EMailAlertsEnabled]
			)
		) 
		END		
		
      If UPDATE([MailboxAlias])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'MailboxAlias',
      CONVERT(NVARCHAR(2000),DELETED.[MailboxAlias],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MailboxAlias],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[MailboxAlias] Is Null And
				DELETED.[MailboxAlias] Is Not Null
			) Or
			(
				INSERTED.[MailboxAlias] Is Not Null And
				DELETED.[MailboxAlias] Is Null
			) Or
			(
				INSERTED.[MailboxAlias] !=
				DELETED.[MailboxAlias]
			)
		) 
		END		
		
      If UPDATE([MailboxServer])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'MailboxServer',
      CONVERT(NVARCHAR(2000),DELETED.[MailboxServer],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MailboxServer],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[MailboxServer] Is Null And
				DELETED.[MailboxServer] Is Not Null
			) Or
			(
				INSERTED.[MailboxServer] Is Not Null And
				DELETED.[MailboxServer] Is Null
			) Or
			(
				INSERTED.[MailboxServer] !=
				DELETED.[MailboxServer]
			)
		) 
		END		
		
      If UPDATE([SyncCalendarWhere])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'SyncCalendarWhere',
      CONVERT(NVARCHAR(2000),DELETED.[SyncCalendarWhere],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SyncCalendarWhere],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[SyncCalendarWhere] Is Null And
				DELETED.[SyncCalendarWhere] Is Not Null
			) Or
			(
				INSERTED.[SyncCalendarWhere] Is Not Null And
				DELETED.[SyncCalendarWhere] Is Null
			) Or
			(
				INSERTED.[SyncCalendarWhere] !=
				DELETED.[SyncCalendarWhere]
			)
		) 
		END		
		
      If UPDATE([SyncContactsWhere])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'SyncContactsWhere',
      CONVERT(NVARCHAR(2000),DELETED.[SyncContactsWhere],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SyncContactsWhere],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[SyncContactsWhere] Is Null And
				DELETED.[SyncContactsWhere] Is Not Null
			) Or
			(
				INSERTED.[SyncContactsWhere] Is Not Null And
				DELETED.[SyncContactsWhere] Is Null
			) Or
			(
				INSERTED.[SyncContactsWhere] !=
				DELETED.[SyncContactsWhere]
			)
		) 
		END		
		
      If UPDATE([SyncTasksWhere])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'SyncTasksWhere',
      CONVERT(NVARCHAR(2000),DELETED.[SyncTasksWhere],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SyncTasksWhere],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[SyncTasksWhere] Is Null And
				DELETED.[SyncTasksWhere] Is Not Null
			) Or
			(
				INSERTED.[SyncTasksWhere] Is Not Null And
				DELETED.[SyncTasksWhere] Is Null
			) Or
			(
				INSERTED.[SyncTasksWhere] !=
				DELETED.[SyncTasksWhere]
			)
		) 
		END		
		
      If UPDATE([LastCustomProposal])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'LastCustomProposal',
      CONVERT(NVARCHAR(2000),DELETED.[LastCustomProposal],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LastCustomProposal],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[LastCustomProposal] Is Null And
				DELETED.[LastCustomProposal] Is Not Null
			) Or
			(
				INSERTED.[LastCustomProposal] Is Not Null And
				DELETED.[LastCustomProposal] Is Null
			) Or
			(
				INSERTED.[LastCustomProposal] !=
				DELETED.[LastCustomProposal]
			)
		) 
		END		
		
      If UPDATE([LoginAttempts])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'LoginAttempts',
      CONVERT(NVARCHAR(2000),DELETED.[LoginAttempts],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LoginAttempts],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[LoginAttempts] Is Null And
				DELETED.[LoginAttempts] Is Not Null
			) Or
			(
				INSERTED.[LoginAttempts] Is Not Null And
				DELETED.[LoginAttempts] Is Null
			) Or
			(
				INSERTED.[LoginAttempts] !=
				DELETED.[LoginAttempts]
			)
		) 
		END		
		
      If UPDATE([LastPasswordChange])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'LastPasswordChange',
      CONVERT(NVARCHAR(2000),DELETED.[LastPasswordChange],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LastPasswordChange],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[LastPasswordChange] Is Null And
				DELETED.[LastPasswordChange] Is Not Null
			) Or
			(
				INSERTED.[LastPasswordChange] Is Not Null And
				DELETED.[LastPasswordChange] Is Null
			) Or
			(
				INSERTED.[LastPasswordChange] !=
				DELETED.[LastPasswordChange]
			)
		) 
		END		
		
      If UPDATE([AutoHide])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'AutoHide',
      CONVERT(NVARCHAR(2000),DELETED.[AutoHide],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AutoHide],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[AutoHide] Is Null And
				DELETED.[AutoHide] Is Not Null
			) Or
			(
				INSERTED.[AutoHide] Is Not Null And
				DELETED.[AutoHide] Is Null
			) Or
			(
				INSERTED.[AutoHide] !=
				DELETED.[AutoHide]
			)
		) 
		END		
		
      If UPDATE([DefaultPrinter])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultPrinter',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultPrinter],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultPrinter],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[DefaultPrinter] Is Null And
				DELETED.[DefaultPrinter] Is Not Null
			) Or
			(
				INSERTED.[DefaultPrinter] Is Not Null And
				DELETED.[DefaultPrinter] Is Null
			) Or
			(
				INSERTED.[DefaultPrinter] !=
				DELETED.[DefaultPrinter]
			)
		) 
		END		
		
      If UPDATE([AutoRetrieve])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'AutoRetrieve',
      CONVERT(NVARCHAR(2000),DELETED.[AutoRetrieve],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AutoRetrieve],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[AutoRetrieve] Is Null And
				DELETED.[AutoRetrieve] Is Not Null
			) Or
			(
				INSERTED.[AutoRetrieve] Is Not Null And
				DELETED.[AutoRetrieve] Is Null
			) Or
			(
				INSERTED.[AutoRetrieve] !=
				DELETED.[AutoRetrieve]
			)
		) 
		END		
		
      If UPDATE([ReportServer])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ReportServer',
      CONVERT(NVARCHAR(2000),DELETED.[ReportServer],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReportServer],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[ReportServer] Is Null And
				DELETED.[ReportServer] Is Not Null
			) Or
			(
				INSERTED.[ReportServer] Is Not Null And
				DELETED.[ReportServer] Is Null
			) Or
			(
				INSERTED.[ReportServer] !=
				DELETED.[ReportServer]
			)
		) 
		END		
		
      If UPDATE([Domain])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'Domain',
      CONVERT(NVARCHAR(2000),DELETED.[Domain],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Domain],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[Domain] Is Null And
				DELETED.[Domain] Is Not Null
			) Or
			(
				INSERTED.[Domain] Is Not Null And
				DELETED.[Domain] Is Null
			) Or
			(
				INSERTED.[Domain] !=
				DELETED.[Domain]
			)
		) 
		END		
		
      If UPDATE([ProgressiveViewing])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ProgressiveViewing',
      CONVERT(NVARCHAR(2000),DELETED.[ProgressiveViewing],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProgressiveViewing],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[ProgressiveViewing] Is Null And
				DELETED.[ProgressiveViewing] Is Not Null
			) Or
			(
				INSERTED.[ProgressiveViewing] Is Not Null And
				DELETED.[ProgressiveViewing] Is Null
			) Or
			(
				INSERTED.[ProgressiveViewing] !=
				DELETED.[ProgressiveViewing]
			)
		) 
		END		
		
      If UPDATE([DefaultFont])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultFont',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultFont],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultFont],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[DefaultFont] Is Null And
				DELETED.[DefaultFont] Is Not Null
			) Or
			(
				INSERTED.[DefaultFont] Is Not Null And
				DELETED.[DefaultFont] Is Null
			) Or
			(
				INSERTED.[DefaultFont] !=
				DELETED.[DefaultFont]
			)
		) 
		END		
		
      If UPDATE([DefaultPageWidth])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultPageWidth',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultPageWidth],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultPageWidth],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[DefaultPageWidth] Is Null And
				DELETED.[DefaultPageWidth] Is Not Null
			) Or
			(
				INSERTED.[DefaultPageWidth] Is Not Null And
				DELETED.[DefaultPageWidth] Is Null
			) Or
			(
				INSERTED.[DefaultPageWidth] !=
				DELETED.[DefaultPageWidth]
			)
		) 
		END		
		
      If UPDATE([DefaultPageHeight])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultPageHeight',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultPageHeight],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultPageHeight],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[DefaultPageHeight] Is Null And
				DELETED.[DefaultPageHeight] Is Not Null
			) Or
			(
				INSERTED.[DefaultPageHeight] Is Not Null And
				DELETED.[DefaultPageHeight] Is Null
			) Or
			(
				INSERTED.[DefaultPageHeight] !=
				DELETED.[DefaultPageHeight]
			)
		) 
		END		
		
      If UPDATE([DefaultTopMargin])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultTopMargin',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultTopMargin],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultTopMargin],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[DefaultTopMargin] Is Null And
				DELETED.[DefaultTopMargin] Is Not Null
			) Or
			(
				INSERTED.[DefaultTopMargin] Is Not Null And
				DELETED.[DefaultTopMargin] Is Null
			) Or
			(
				INSERTED.[DefaultTopMargin] !=
				DELETED.[DefaultTopMargin]
			)
		) 
		END		
		
      If UPDATE([DefaultBottomMargin])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultBottomMargin',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultBottomMargin],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultBottomMargin],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[DefaultBottomMargin] Is Null And
				DELETED.[DefaultBottomMargin] Is Not Null
			) Or
			(
				INSERTED.[DefaultBottomMargin] Is Not Null And
				DELETED.[DefaultBottomMargin] Is Null
			) Or
			(
				INSERTED.[DefaultBottomMargin] !=
				DELETED.[DefaultBottomMargin]
			)
		) 
		END		
		
      If UPDATE([DefaultLeftMargin])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultLeftMargin',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultLeftMargin],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultLeftMargin],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[DefaultLeftMargin] Is Null And
				DELETED.[DefaultLeftMargin] Is Not Null
			) Or
			(
				INSERTED.[DefaultLeftMargin] Is Not Null And
				DELETED.[DefaultLeftMargin] Is Null
			) Or
			(
				INSERTED.[DefaultLeftMargin] !=
				DELETED.[DefaultLeftMargin]
			)
		) 
		END		
		
      If UPDATE([DefaultRightMargin])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultRightMargin',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultRightMargin],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultRightMargin],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[DefaultRightMargin] Is Null And
				DELETED.[DefaultRightMargin] Is Not Null
			) Or
			(
				INSERTED.[DefaultRightMargin] Is Not Null And
				DELETED.[DefaultRightMargin] Is Null
			) Or
			(
				INSERTED.[DefaultRightMargin] !=
				DELETED.[DefaultRightMargin]
			)
		) 
		END		
		
      If UPDATE([SupportUsername])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'SupportUsername',
      CONVERT(NVARCHAR(2000),DELETED.[SupportUsername],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SupportUsername],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[SupportUsername] Is Null And
				DELETED.[SupportUsername] Is Not Null
			) Or
			(
				INSERTED.[SupportUsername] Is Not Null And
				DELETED.[SupportUsername] Is Null
			) Or
			(
				INSERTED.[SupportUsername] !=
				DELETED.[SupportUsername]
			)
		) 
		END		
		
      If UPDATE([SupportPassword])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'SupportPassword',
      CONVERT(NVARCHAR(2000),DELETED.[SupportPassword],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SupportPassword],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[SupportPassword] Is Null And
				DELETED.[SupportPassword] Is Not Null
			) Or
			(
				INSERTED.[SupportPassword] Is Not Null And
				DELETED.[SupportPassword] Is Null
			) Or
			(
				INSERTED.[SupportPassword] !=
				DELETED.[SupportPassword]
			)
		) 
		END		
		
      If UPDATE([ShortDateFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ShortDateFormat',
      CONVERT(NVARCHAR(2000),DELETED.[ShortDateFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ShortDateFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[ShortDateFormat] Is Null And
				DELETED.[ShortDateFormat] Is Not Null
			) Or
			(
				INSERTED.[ShortDateFormat] Is Not Null And
				DELETED.[ShortDateFormat] Is Null
			) Or
			(
				INSERTED.[ShortDateFormat] !=
				DELETED.[ShortDateFormat]
			)
		) 
		END		
		
      If UPDATE([MediumDateFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'MediumDateFormat',
      CONVERT(NVARCHAR(2000),DELETED.[MediumDateFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MediumDateFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[MediumDateFormat] Is Null And
				DELETED.[MediumDateFormat] Is Not Null
			) Or
			(
				INSERTED.[MediumDateFormat] Is Not Null And
				DELETED.[MediumDateFormat] Is Null
			) Or
			(
				INSERTED.[MediumDateFormat] !=
				DELETED.[MediumDateFormat]
			)
		) 
		END		
		
      If UPDATE([LongDateFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'LongDateFormat',
      CONVERT(NVARCHAR(2000),DELETED.[LongDateFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LongDateFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[LongDateFormat] Is Null And
				DELETED.[LongDateFormat] Is Not Null
			) Or
			(
				INSERTED.[LongDateFormat] Is Not Null And
				DELETED.[LongDateFormat] Is Null
			) Or
			(
				INSERTED.[LongDateFormat] !=
				DELETED.[LongDateFormat]
			)
		) 
		END		
		
      If UPDATE([DateSeparator])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DateSeparator',
      CONVERT(NVARCHAR(2000),DELETED.[DateSeparator],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DateSeparator],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[DateSeparator] Is Null And
				DELETED.[DateSeparator] Is Not Null
			) Or
			(
				INSERTED.[DateSeparator] Is Not Null And
				DELETED.[DateSeparator] Is Null
			) Or
			(
				INSERTED.[DateSeparator] !=
				DELETED.[DateSeparator]
			)
		) 
		END		
		
      If UPDATE([TimeFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'TimeFormat',
      CONVERT(NVARCHAR(2000),DELETED.[TimeFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TimeFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[TimeFormat] Is Null And
				DELETED.[TimeFormat] Is Not Null
			) Or
			(
				INSERTED.[TimeFormat] Is Not Null And
				DELETED.[TimeFormat] Is Null
			) Or
			(
				INSERTED.[TimeFormat] !=
				DELETED.[TimeFormat]
			)
		) 
		END		
		
      If UPDATE([decimalSymbol])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'decimalSymbol',
      CONVERT(NVARCHAR(2000),DELETED.[decimalSymbol],121),
      CONVERT(NVARCHAR(2000),INSERTED.[decimalSymbol],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[decimalSymbol] Is Null And
				DELETED.[decimalSymbol] Is Not Null
			) Or
			(
				INSERTED.[decimalSymbol] Is Not Null And
				DELETED.[decimalSymbol] Is Null
			) Or
			(
				INSERTED.[decimalSymbol] !=
				DELETED.[decimalSymbol]
			)
		) 
		END		
		
      If UPDATE([ThousandsSeparator])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ThousandsSeparator',
      CONVERT(NVARCHAR(2000),DELETED.[ThousandsSeparator],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ThousandsSeparator],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[ThousandsSeparator] Is Null And
				DELETED.[ThousandsSeparator] Is Not Null
			) Or
			(
				INSERTED.[ThousandsSeparator] Is Not Null And
				DELETED.[ThousandsSeparator] Is Null
			) Or
			(
				INSERTED.[ThousandsSeparator] !=
				DELETED.[ThousandsSeparator]
			)
		) 
		END		
		
      If UPDATE([AutoLoginDefaultCompany])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'AutoLoginDefaultCompany',
      CONVERT(NVARCHAR(2000),DELETED.[AutoLoginDefaultCompany],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AutoLoginDefaultCompany],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[AutoLoginDefaultCompany] Is Null And
				DELETED.[AutoLoginDefaultCompany] Is Not Null
			) Or
			(
				INSERTED.[AutoLoginDefaultCompany] Is Not Null And
				DELETED.[AutoLoginDefaultCompany] Is Null
			) Or
			(
				INSERTED.[AutoLoginDefaultCompany] !=
				DELETED.[AutoLoginDefaultCompany]
			)
		) 
		END		
		
      If UPDATE([AutoLoginCurrentPeriod])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'AutoLoginCurrentPeriod',
      CONVERT(NVARCHAR(2000),DELETED.[AutoLoginCurrentPeriod],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AutoLoginCurrentPeriod],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[AutoLoginCurrentPeriod] Is Null And
				DELETED.[AutoLoginCurrentPeriod] Is Not Null
			) Or
			(
				INSERTED.[AutoLoginCurrentPeriod] Is Not Null And
				DELETED.[AutoLoginCurrentPeriod] Is Null
			) Or
			(
				INSERTED.[AutoLoginCurrentPeriod] !=
				DELETED.[AutoLoginCurrentPeriod]
			)
		) 
		END		
		
      If UPDATE([LastSF330])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'LastSF330',
      CONVERT(NVARCHAR(2000),DELETED.[LastSF330],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LastSF330],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[LastSF330] Is Null And
				DELETED.[LastSF330] Is Not Null
			) Or
			(
				INSERTED.[LastSF330] Is Not Null And
				DELETED.[LastSF330] Is Null
			) Or
			(
				INSERTED.[LastSF330] !=
				DELETED.[LastSF330]
			)
		) 
		END		
		
      If UPDATE([SoundsLikeSearch])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'SoundsLikeSearch',
      CONVERT(NVARCHAR(2000),DELETED.[SoundsLikeSearch],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SoundsLikeSearch],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[SoundsLikeSearch] Is Null And
				DELETED.[SoundsLikeSearch] Is Not Null
			) Or
			(
				INSERTED.[SoundsLikeSearch] Is Not Null And
				DELETED.[SoundsLikeSearch] Is Null
			) Or
			(
				INSERTED.[SoundsLikeSearch] !=
				DELETED.[SoundsLikeSearch]
			)
		) 
		END		
		
      If UPDATE([WindowsUsername])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'WindowsUsername',
      CONVERT(NVARCHAR(2000),DELETED.[WindowsUsername],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WindowsUsername],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[WindowsUsername] Is Null And
				DELETED.[WindowsUsername] Is Not Null
			) Or
			(
				INSERTED.[WindowsUsername] Is Not Null And
				DELETED.[WindowsUsername] Is Null
			) Or
			(
				INSERTED.[WindowsUsername] !=
				DELETED.[WindowsUsername]
			)
		) 
		END		
		
      If UPDATE([ProjectDownload])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ProjectDownload',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([AutoRetrieveEM])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'AutoRetrieveEM',
      CONVERT(NVARCHAR(2000),DELETED.[AutoRetrieveEM],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AutoRetrieveEM],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[AutoRetrieveEM] Is Null And
				DELETED.[AutoRetrieveEM] Is Not Null
			) Or
			(
				INSERTED.[AutoRetrieveEM] Is Not Null And
				DELETED.[AutoRetrieveEM] Is Null
			) Or
			(
				INSERTED.[AutoRetrieveEM] !=
				DELETED.[AutoRetrieveEM]
			)
		) 
		END		
		
      If UPDATE([ContactDownload])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ContactDownload',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([ClientDownload])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ClientDownload',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([ActivityDownload])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ActivityDownload',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([OpportunityDownload])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'OpportunityDownload',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([DefaultCountry])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultCountry',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultCountry],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultCountry],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[DefaultCountry] Is Null And
				DELETED.[DefaultCountry] Is Not Null
			) Or
			(
				INSERTED.[DefaultCountry] Is Not Null And
				DELETED.[DefaultCountry] Is Null
			) Or
			(
				INSERTED.[DefaultCountry] !=
				DELETED.[DefaultCountry]
			)
		) 
		END		
		
      If UPDATE([DefaultLanguage])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DefaultLanguage',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultLanguage],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultLanguage],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[DefaultLanguage] Is Null And
				DELETED.[DefaultLanguage] Is Not Null
			) Or
			(
				INSERTED.[DefaultLanguage] Is Not Null And
				DELETED.[DefaultLanguage] Is Null
			) Or
			(
				INSERTED.[DefaultLanguage] !=
				DELETED.[DefaultLanguage]
			)
		) 
		END		
		
      If UPDATE([PageSize])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'PageSize',
      CONVERT(NVARCHAR(2000),DELETED.[PageSize],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PageSize],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[PageSize] Is Null And
				DELETED.[PageSize] Is Not Null
			) Or
			(
				INSERTED.[PageSize] Is Not Null And
				DELETED.[PageSize] Is Null
			) Or
			(
				INSERTED.[PageSize] !=
				DELETED.[PageSize]
			)
		) 
		END		
		
      If UPDATE([UnitOfMeasure])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'UnitOfMeasure',
      CONVERT(NVARCHAR(2000),DELETED.[UnitOfMeasure],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UnitOfMeasure],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[UnitOfMeasure] Is Null And
				DELETED.[UnitOfMeasure] Is Not Null
			) Or
			(
				INSERTED.[UnitOfMeasure] Is Not Null And
				DELETED.[UnitOfMeasure] Is Null
			) Or
			(
				INSERTED.[UnitOfMeasure] !=
				DELETED.[UnitOfMeasure]
			)
		) 
		END		
		
      If UPDATE([ForceChangePassword])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ForceChangePassword',
      CONVERT(NVARCHAR(2000),DELETED.[ForceChangePassword],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ForceChangePassword],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[ForceChangePassword] Is Null And
				DELETED.[ForceChangePassword] Is Not Null
			) Or
			(
				INSERTED.[ForceChangePassword] Is Not Null And
				DELETED.[ForceChangePassword] Is Null
			) Or
			(
				INSERTED.[ForceChangePassword] !=
				DELETED.[ForceChangePassword]
			)
		) 
		END		
		
      If UPDATE([DelegateInvoiceApproval])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DelegateInvoiceApproval',
      CONVERT(NVARCHAR(2000),DELETED.[DelegateInvoiceApproval],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DelegateInvoiceApproval],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[DelegateInvoiceApproval] Is Null And
				DELETED.[DelegateInvoiceApproval] Is Not Null
			) Or
			(
				INSERTED.[DelegateInvoiceApproval] Is Not Null And
				DELETED.[DelegateInvoiceApproval] Is Null
			) Or
			(
				INSERTED.[DelegateInvoiceApproval] !=
				DELETED.[DelegateInvoiceApproval]
			)
		) 
		END		
		
      If UPDATE([DelegateEmployee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DelegateEmployee',
      CONVERT(NVARCHAR(2000),DELETED.[DelegateEmployee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DelegateEmployee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[DelegateEmployee] Is Null And
				DELETED.[DelegateEmployee] Is Not Null
			) Or
			(
				INSERTED.[DelegateEmployee] Is Not Null And
				DELETED.[DelegateEmployee] Is Null
			) Or
			(
				INSERTED.[DelegateEmployee] !=
				DELETED.[DelegateEmployee]
			)
		) 
		END		
		
      If UPDATE([FlatDashboardStyling])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'FlatDashboardStyling',
      CONVERT(NVARCHAR(2000),DELETED.[FlatDashboardStyling],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FlatDashboardStyling],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[FlatDashboardStyling] Is Null And
				DELETED.[FlatDashboardStyling] Is Not Null
			) Or
			(
				INSERTED.[FlatDashboardStyling] Is Not Null And
				DELETED.[FlatDashboardStyling] Is Null
			) Or
			(
				INSERTED.[FlatDashboardStyling] !=
				DELETED.[FlatDashboardStyling]
			)
		) 
		END		
		
      If UPDATE([LoginAlertDateDismissed])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'LoginAlertDateDismissed',
      CONVERT(NVARCHAR(2000),DELETED.[LoginAlertDateDismissed],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LoginAlertDateDismissed],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[LoginAlertDateDismissed] Is Null And
				DELETED.[LoginAlertDateDismissed] Is Not Null
			) Or
			(
				INSERTED.[LoginAlertDateDismissed] Is Not Null And
				DELETED.[LoginAlertDateDismissed] Is Null
			) Or
			(
				INSERTED.[LoginAlertDateDismissed] !=
				DELETED.[LoginAlertDateDismissed]
			)
		) 
		END		
		
      If UPDATE([IQAccessToken])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'IQAccessToken',
      CONVERT(NVARCHAR(2000),DELETED.[IQAccessToken],121),
      CONVERT(NVARCHAR(2000),INSERTED.[IQAccessToken],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[IQAccessToken] Is Null And
				DELETED.[IQAccessToken] Is Not Null
			) Or
			(
				INSERTED.[IQAccessToken] Is Not Null And
				DELETED.[IQAccessToken] Is Null
			) Or
			(
				INSERTED.[IQAccessToken] !=
				DELETED.[IQAccessToken]
			)
		) 
		END		
		
      If UPDATE([IQRefreshToken])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'IQRefreshToken',
      CONVERT(NVARCHAR(2000),DELETED.[IQRefreshToken],121),
      CONVERT(NVARCHAR(2000),INSERTED.[IQRefreshToken],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[IQRefreshToken] Is Null And
				DELETED.[IQRefreshToken] Is Not Null
			) Or
			(
				INSERTED.[IQRefreshToken] Is Not Null And
				DELETED.[IQRefreshToken] Is Null
			) Or
			(
				INSERTED.[IQRefreshToken] !=
				DELETED.[IQRefreshToken]
			)
		) 
		END		
		
      If UPDATE([EnableICDashpart])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'EnableICDashpart',
      CONVERT(NVARCHAR(2000),DELETED.[EnableICDashpart],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EnableICDashpart],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[EnableICDashpart] Is Null And
				DELETED.[EnableICDashpart] Is Not Null
			) Or
			(
				INSERTED.[EnableICDashpart] Is Not Null And
				DELETED.[EnableICDashpart] Is Null
			) Or
			(
				INSERTED.[EnableICDashpart] !=
				DELETED.[EnableICDashpart]
			)
		) 
		END		
		
      If UPDATE([ODBCEnabled])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ODBCEnabled',
      CONVERT(NVARCHAR(2000),DELETED.[ODBCEnabled],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ODBCEnabled],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[ODBCEnabled] Is Null And
				DELETED.[ODBCEnabled] Is Not Null
			) Or
			(
				INSERTED.[ODBCEnabled] Is Not Null And
				DELETED.[ODBCEnabled] Is Null
			) Or
			(
				INSERTED.[ODBCEnabled] !=
				DELETED.[ODBCEnabled]
			)
		) 
		END		
		
      If UPDATE([ODBCUsername])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ODBCUsername',
      CONVERT(NVARCHAR(2000),DELETED.[ODBCUsername],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ODBCUsername],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[ODBCUsername] Is Null And
				DELETED.[ODBCUsername] Is Not Null
			) Or
			(
				INSERTED.[ODBCUsername] Is Not Null And
				DELETED.[ODBCUsername] Is Null
			) Or
			(
				INSERTED.[ODBCUsername] !=
				DELETED.[ODBCUsername]
			)
		) 
		END		
		
      If UPDATE([ODBCPassword])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'ODBCPassword',
      CONVERT(NVARCHAR(2000),DELETED.[ODBCPassword],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ODBCPassword],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[ODBCPassword] Is Null And
				DELETED.[ODBCPassword] Is Not Null
			) Or
			(
				INSERTED.[ODBCPassword] Is Not Null And
				DELETED.[ODBCPassword] Is Null
			) Or
			(
				INSERTED.[ODBCPassword] !=
				DELETED.[ODBCPassword]
			)
		) 
		END		
		
      If UPDATE([TwoFactorAuth])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'TwoFactorAuth',
      CONVERT(NVARCHAR(2000),DELETED.[TwoFactorAuth],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TwoFactorAuth],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[TwoFactorAuth] Is Null And
				DELETED.[TwoFactorAuth] Is Not Null
			) Or
			(
				INSERTED.[TwoFactorAuth] Is Not Null And
				DELETED.[TwoFactorAuth] Is Null
			) Or
			(
				INSERTED.[TwoFactorAuth] !=
				DELETED.[TwoFactorAuth]
			)
		) 
		END		
		
      If UPDATE([TwoFactorAuthConfigured])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'TwoFactorAuthConfigured',
      CONVERT(NVARCHAR(2000),DELETED.[TwoFactorAuthConfigured],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TwoFactorAuthConfigured],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[TwoFactorAuthConfigured] Is Null And
				DELETED.[TwoFactorAuthConfigured] Is Not Null
			) Or
			(
				INSERTED.[TwoFactorAuthConfigured] Is Not Null And
				DELETED.[TwoFactorAuthConfigured] Is Null
			) Or
			(
				INSERTED.[TwoFactorAuthConfigured] !=
				DELETED.[TwoFactorAuthConfigured]
			)
		) 
		END		
		
      If UPDATE([DisplayUsername])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'DisplayUsername',
      CONVERT(NVARCHAR(2000),DELETED.[DisplayUsername],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DisplayUsername],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[DisplayUsername] Is Null And
				DELETED.[DisplayUsername] Is Not Null
			) Or
			(
				INSERTED.[DisplayUsername] Is Not Null And
				DELETED.[DisplayUsername] Is Null
			) Or
			(
				INSERTED.[DisplayUsername] !=
				DELETED.[DisplayUsername]
			)
		) 
		END		
		
      If UPDATE([Status])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Username],121),'Status',
      CONVERT(NVARCHAR(2000),DELETED.[Status],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Status],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Username] = DELETED.[Username] AND 
		(
			(
				INSERTED.[Status] Is Null And
				DELETED.[Status] Is Not Null
			) Or
			(
				INSERTED.[Status] Is Not Null And
				DELETED.[Status] Is Null
			) Or
			(
				INSERTED.[Status] !=
				DELETED.[Status]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[SEUser] ADD CONSTRAINT [SEUserPK] PRIMARY KEY NONCLUSTERED ([Username]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [SEUserDisplayUsernameIDX] ON [dbo].[SEUser] ([DisplayUsername]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [SEUserEmployeeIDX] ON [dbo].[SEUser] ([Employee]) INCLUDE ([Username]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
