CREATE TABLE [dbo].[CFGIntegrationWS_Backup]
(
[ConfigID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EnableWebService] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WebServiceURL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RowsPerCall] [int] NOT NULL,
[RequestTimeout] [int] NOT NULL,
[ReceiveNewHires] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SendChangesSinceLastSend] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SendEmployees] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SendOrganizations] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AuditUser] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TerminationDate] [datetime] NULL,
[LastSendEmployees] [datetime] NULL,
[LastSendOrganizations] [datetime] NULL,
[UserName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Options] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastReceiveProjects] [datetime] NULL,
[LastReceiveTimesheets] [datetime] NULL,
[LastSyncFields] [datetime] NULL,
[EnablePIMClients] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EnablePIMContacts] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EnablePIMEmployees] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EnablePIMMktCampaigns] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EnablePIMOpportunities] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EnablePIMProjects] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EnablePIMVendors] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OnlySendOrganizationsWithEmployees] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LimitByEmployeeType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SyncProjectsRegular] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SyncProjectsOverhead] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SyncProjectsPromotional] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SyncEmployeesPreferredName] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SyncContactsPreferredName] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PIMAllowFullAccess] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RoleNotification] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
