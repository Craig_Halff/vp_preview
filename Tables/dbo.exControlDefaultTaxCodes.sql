CREATE TABLE [dbo].[exControlDefaultTaxCodes]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__exControlDe__Seq__4C6363A3] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[exControlDefaultTaxCodes] ADD CONSTRAINT [exControlDefaultTaxCodesPK] PRIMARY KEY NONCLUSTERED ([Batch], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
