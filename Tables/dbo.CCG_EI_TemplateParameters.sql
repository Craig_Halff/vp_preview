CREATE TABLE [dbo].[CCG_EI_TemplateParameters]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[TemplateSeq] [int] NOT NULL,
[DateChanged] [datetime] NULL,
[ChangedBy] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_TemplateParameters] ADD CONSTRAINT [PK_CCG_EI_TemplateParameters] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Defines the set of parameters to be used in a packaging template', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_TemplateParameters', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'The user who last changed the parameter', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_TemplateParameters', 'COLUMN', N'ChangedBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date/time when this parameter was last changed', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_TemplateParameters', 'COLUMN', N'DateChanged'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Parameter name', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_TemplateParameters', 'COLUMN', N'Name'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_TemplateParameters', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The type of the parameter (Static / SQL)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_TemplateParameters', 'COLUMN', N'SourceType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'CCG_EI_Templates.Seq (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_TemplateParameters', 'COLUMN', N'TemplateSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The return type of the parameter value (bool / string / currency / date / xml / table / client / employee / contact / project)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_TemplateParameters', 'COLUMN', N'Type'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Either the parameter value directly, or the basis for the parameter value', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_TemplateParameters', 'COLUMN', N'Value'
GO
