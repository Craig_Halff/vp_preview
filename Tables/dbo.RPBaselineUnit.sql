CREATE TABLE [dbo].[RPBaselineUnit]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UnitID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodCount] [int] NOT NULL CONSTRAINT [DF__RPBaselin__Perio__38282C23] DEFAULT ((1)),
[PeriodQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPBaselin__Perio__391C505C] DEFAULT ((0)),
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPBaselin__Perio__3A107495] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPBaselin__Perio__3B0498CE] DEFAULT ((0)),
[PeriodRev] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPBaselin__Perio__3BF8BD07] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPBaselineUnit] ADD CONSTRAINT [RPBaselineUnitPK] PRIMARY KEY NONCLUSTERED ([TimePhaseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPBaselineUnitAllIDX] ON [dbo].[RPBaselineUnit] ([PlanID], [TaskID], [UnitID], [StartDate], [EndDate], [PeriodQty], [PeriodCost], [PeriodBill]) ON [PRIMARY]
GO
