CREATE TABLE [dbo].[billLimDetail]
(
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecdSeq] [int] NOT NULL CONSTRAINT [DF__billLimDe__RecdS__68C9CCD0] DEFAULT ((0)),
[BillWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sequence] [int] NOT NULL CONSTRAINT [DF__billLimDe__Seque__69BDF109] DEFAULT ((0)),
[Section] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billLimDe__Curre__6AB21542] DEFAULT ((0)),
[PriorAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billLimDe__Prior__6BA6397B] DEFAULT ((0)),
[Total] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billLimDe__Total__6C9A5DB4] DEFAULT ((0)),
[Limit] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billLimDe__Limit__6D8E81ED] DEFAULT ((0)),
[Adjustment] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billLimDe__Adjus__6E82A626] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[billLimDetail] ADD CONSTRAINT [billLimDetailPK] PRIMARY KEY NONCLUSTERED ([Invoice], [MainWBS1], [RecdSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
