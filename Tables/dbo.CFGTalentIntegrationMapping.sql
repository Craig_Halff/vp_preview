CREATE TABLE [dbo].[CFGTalentIntegrationMapping]
(
[ConfigID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UseCustomMapping] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTalent__UseCu__7C3C93F7] DEFAULT ('N'),
[XMLMapping] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGTalentIntegrationMapping] ADD CONSTRAINT [CFGTalentIntegrationMappingPK] PRIMARY KEY NONCLUSTERED ([ConfigID], [Type]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
