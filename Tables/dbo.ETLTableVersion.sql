CREATE TABLE [dbo].[ETLTableVersion]
(
[TableName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastProcessedVersion] [bigint] NULL,
[CurrentVersion] [bigint] NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
