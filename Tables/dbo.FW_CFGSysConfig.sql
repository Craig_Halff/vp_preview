CREATE TABLE [dbo].[FW_CFGSysConfig]
(
[PKey] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientNumber] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LookupLastUpdate] [datetime] NULL,
[LookupHashValue] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__Looku__0C13D464] DEFAULT ((0)),
[NavTreeLastUpdate] [datetime] NULL,
[NavTreeHashValue] [int] NOT NULL CONSTRAINT [DF__FW_CFGSys__NavTr__0D07F89D] DEFAULT ((0)),
[NavTreeXML] [varbinary] (max) NULL,
[ClientSecret] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LegacyLicense] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGSys__Legac__7CC905E1] DEFAULT ('N'),
[HMRCUniqueID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__FW_CFGSys__HMRCU__19F01FCB] DEFAULT (newid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGSysConfig] ADD CONSTRAINT [FW_CFGSysConfigPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
