CREATE TABLE [dbo].[CFGAPExpenseCodeAccounts]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReimbAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DirectAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OverheadAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PromotionalAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillByDefault] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAPExpe__BillB__782C0BF1] DEFAULT ('Y'),
[AccountEditable] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAPExpe__Accou__7920302A] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAPExpenseCodeAccounts] ADD CONSTRAINT [CFGAPExpenseCodeAccountsPK] PRIMARY KEY CLUSTERED ([Code], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
