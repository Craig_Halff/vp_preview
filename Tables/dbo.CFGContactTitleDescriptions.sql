CREATE TABLE [dbo].[CFGContactTitleDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGContactT__Seq__78B61BD6] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGContactTitleDescriptions] ADD CONSTRAINT [CFGContactTitleDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
