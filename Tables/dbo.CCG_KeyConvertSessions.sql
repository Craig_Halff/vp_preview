CREATE TABLE [dbo].[CCG_KeyConvertSessions]
(
[Seq] [uniqueidentifier] NOT NULL CONSTRAINT [DF__CCG_KeyConv__Seq__1FC8F4B2] DEFAULT (newid()),
[CreateDate] [datetime] NOT NULL CONSTRAINT [DF__CCG_KeyCo__Creat__20BD18EB] DEFAULT (getdate()),
[CountVendors] [int] NULL,
[CountWBS1s] [int] NULL,
[CountWBS2s] [int] NULL,
[CountWBS3s] [int] NULL,
[CountAccounts] [int] NULL,
[CountEmployees] [int] NULL,
[ErrorsVendors] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_KeyConvertSessions] ADD CONSTRAINT [CCG_KeyConvertSessionsPK] PRIMARY KEY NONCLUSTERED ([Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
