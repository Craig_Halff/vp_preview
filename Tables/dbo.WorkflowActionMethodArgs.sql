CREATE TABLE [dbo].[WorkflowActionMethodArgs]
(
[ActionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ArgName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ArgOrder] [int] NOT NULL CONSTRAINT [DF__WorkflowA__ArgOr__0D13B0EC] DEFAULT ((0)),
[SQLExpression] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SQLIfExpression] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SQLElseExpression] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowActionMethodArgs] ADD CONSTRAINT [WorkflowActionMethodArgsPK] PRIMARY KEY NONCLUSTERED ([ActionID], [ArgName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
