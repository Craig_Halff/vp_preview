CREATE TABLE [dbo].[CCG_PAT_Payable]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[PayableType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ParentSeq] [int] NULL,
[Stage] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_PAT_P__Compa__0A7F62C2] DEFAULT (' '),
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PayableGroup] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayableNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSeq] [int] NULL,
[PONumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POPKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckNumber] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayableDate] [datetime] NULL,
[TransDate] [datetime] NULL,
[PayableFileName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileID] [uniqueidentifier] NULL,
[PayableText] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLastNotificationSent] [datetime] NULL,
[Voucher] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Route] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ControlAmount] [decimal] (19, 5) NULL,
[RecurSeq] [int] NULL,
[SourceType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourcePKey] [varchar] (512) COLLATE SQL_Latin1_General_CP1_CS_AS NULL,
[SourceSeq] [int] NULL,
[SourceBatch] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LiabCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayTerms] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayDate] [datetime] NULL,
[Address] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovedDetail] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StorageSeq] [int] NULL,
[StoragePathOrID] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[CreateUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ModUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_Payable] ADD CONSTRAINT [PK_CCG_PAT_Payable] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IXCustomParentSeq] ON [dbo].[CCG_PAT_Payable] ([ParentSeq]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'List of payable items in the system.  Many of these fields are exported into the Vision AP Voucher / Disbursement.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*FK - Vendor address record', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'Address'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Detail from project amount rows at the time of the last approval.  Can be used to determine if some changes where made since the last approval.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'ApprovedDetail'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*FK - Vision bank code for the item', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'BankCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Transaction batch used when exporting to Vision', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'Batch'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Check number value - Disbursements only', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'CheckNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The company for which this item is associated (in multi-company environments)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'Company'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Optional control amount value for the item', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'ControlAmount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date and time when this payable item was created in PAT (UTC)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'CreateDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The employee id of the one who added this item in PAT', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'CreateUser'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date when the last notification was sent out for this payable item', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'DateLastNotificationSent'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - Vision FW_FILES identifier if liked document exported to Vision TDM', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'FileID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Deprecated - See Project Amount table', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'GLAccount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*FK - Vision liability code', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'LiabCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Last modified date/time (UTC)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'ModDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Last modified employee id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'ModUser'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - Seq number of the parent payable item (if there is one)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'ParentSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of the item', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'PayableDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Filename and/or path for the PDF linked to the item', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'PayableFileName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Optional group assignment', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'PayableGroup'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The payable item number, which may be an invoice number or contract number', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'PayableNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'PayableText'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Payable item type (C = contract / I = invoice / D = Disbursement)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'PayableType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Payment date when terms = DATE', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'PayDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Payment terms - same as Vision codes', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'PayTerms'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Optional identifier for items that are linked to a PO', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'PONumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Optional identifier for items that are linked to a PO', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'POPKey'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Optional identifier for items that are linked to a PO', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'POSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - CCG_PAT_Recur if created from a recurring entry', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'RecurSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Optional route ID if used for item', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'Route'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*PAT assigned batch value to identify records loaded in the same batch', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'SourceBatch'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Identifier for the source type', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'SourcePKey'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Identifier for the source type', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'SourceSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Information about the source of the item (Email / Folder / PO / etc.)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'SourceType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - The current stage for this item', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'Stage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'File Identifier for the storage type defined in ConfigStorage', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'StoragePathOrID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - ConfigStorage', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'StorageSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Transaction date - N/A for all types', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'TransDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*FK - The vendor id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'Vendor'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Voucher number assigned by Vision during export to Vision', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Payable', 'COLUMN', N'Voucher'
GO
