CREATE TABLE [dbo].[CCG_EI_Pending]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[WBS1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NextInvoiceStage] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Parallel] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CanModify] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_EI_Pe__CanMo__149DC9D9] DEFAULT ('N'),
[Role] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateDate] [datetime] NOT NULL CONSTRAINT [DF__CCG_EI_Pe__Creat__1591EE12] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_Pending] ADD CONSTRAINT [PK_CCG_EI_Pending] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CCG_EI_Pending_WBS1_Employee_IDX] ON [dbo].[CCG_EI_Pending] ([WBS1], [Employee]) ON [PRIMARY]
GO
