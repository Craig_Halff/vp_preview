CREATE TABLE [dbo].[CFGStatesData]
(
[ISOCountryCode] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGStatesData] ADD CONSTRAINT [CFGStatesDataPK] PRIMARY KEY CLUSTERED ([ISOCountryCode], [Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
