CREATE TABLE [dbo].[CostLT]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__CostLT_Ne__Table__444D408F] DEFAULT ((0)),
[TableName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CostLT_Ne__RateT__454164C8] DEFAULT ('B'),
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CostLT] ADD CONSTRAINT [CostLTPK] PRIMARY KEY CLUSTERED ([TableNo]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
