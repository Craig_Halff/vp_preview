CREATE TABLE [dbo].[CFGProjectCodeData]
(
[ProjectCode] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Active] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGProjec__Activ__175A9E87] DEFAULT ('Y'),
[ProjectCodeSF330] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGProjectCodeData] ADD CONSTRAINT [CFGProjectCodeDataPK] PRIMARY KEY CLUSTERED ([ProjectCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
