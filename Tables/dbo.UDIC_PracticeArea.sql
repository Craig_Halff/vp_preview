CREATE TABLE [dbo].[UDIC_PracticeArea]
(
[UDIC_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustStrategicPursuitManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustAltStrategicPursuitManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__UDIC_Prac__CustS__47AA66D5] DEFAULT ('A'),
[CustPrimaryPracticeAreaLeader] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_UDIC_PracticeArea]
      ON [dbo].[UDIC_PracticeArea]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'UDIC_PracticeArea'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'UDIC_UID',CONVERT(NVARCHAR(2000),[UDIC_UID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustomCurrencyCode',CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustNumber',CONVERT(NVARCHAR(2000),[CustNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustName',CONVERT(NVARCHAR(2000),[CustName],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustStrategicPursuitManager',CONVERT(NVARCHAR(2000),DELETED.[CustStrategicPursuitManager],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustStrategicPursuitManager = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustAltStrategicPursuitManager',CONVERT(NVARCHAR(2000),DELETED.[CustAltStrategicPursuitManager],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustAltStrategicPursuitManager = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustStatus',CONVERT(NVARCHAR(2000),[CustStatus],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustPrimaryPracticeAreaLeader',CONVERT(NVARCHAR(2000),DELETED.[CustPrimaryPracticeAreaLeader],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustPrimaryPracticeAreaLeader = oldDesc.Employee

      
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_UDIC_PracticeArea]
      ON [dbo].[UDIC_PracticeArea]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'UDIC_PracticeArea'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'UDIC_UID',NULL,CONVERT(NVARCHAR(2000),[UDIC_UID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustomCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustNumber',NULL,CONVERT(NVARCHAR(2000),[CustNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustName',NULL,CONVERT(NVARCHAR(2000),[CustName],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustStrategicPursuitManager',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustStrategicPursuitManager],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustStrategicPursuitManager = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustAltStrategicPursuitManager',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustAltStrategicPursuitManager],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustAltStrategicPursuitManager = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustStatus',NULL,CONVERT(NVARCHAR(2000),[CustStatus],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPrimaryPracticeAreaLeader',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustPrimaryPracticeAreaLeader],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustPrimaryPracticeAreaLeader = newDesc.Employee

     
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_UDIC_PracticeArea]
      ON [dbo].[UDIC_PracticeArea]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'UDIC_PracticeArea'
    
      If UPDATE([UDIC_UID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'UDIC_UID',
      CONVERT(NVARCHAR(2000),DELETED.[UDIC_UID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UDIC_UID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[UDIC_UID] Is Null And
				DELETED.[UDIC_UID] Is Not Null
			) Or
			(
				INSERTED.[UDIC_UID] Is Not Null And
				DELETED.[UDIC_UID] Is Null
			) Or
			(
				INSERTED.[UDIC_UID] !=
				DELETED.[UDIC_UID]
			)
		) 
		END		
		
      If UPDATE([CustomCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustomCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustomCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustomCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustomCurrencyCode] Is Null And
				DELETED.[CustomCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] Is Not Null And
				DELETED.[CustomCurrencyCode] Is Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] !=
				DELETED.[CustomCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([CustNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustNumber',
      CONVERT(NVARCHAR(2000),DELETED.[CustNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustNumber] Is Null And
				DELETED.[CustNumber] Is Not Null
			) Or
			(
				INSERTED.[CustNumber] Is Not Null And
				DELETED.[CustNumber] Is Null
			) Or
			(
				INSERTED.[CustNumber] !=
				DELETED.[CustNumber]
			)
		) 
		END		
		
      If UPDATE([CustName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustName',
      CONVERT(NVARCHAR(2000),DELETED.[CustName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustName] Is Null And
				DELETED.[CustName] Is Not Null
			) Or
			(
				INSERTED.[CustName] Is Not Null And
				DELETED.[CustName] Is Null
			) Or
			(
				INSERTED.[CustName] !=
				DELETED.[CustName]
			)
		) 
		END		
		
     If UPDATE([CustStrategicPursuitManager])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustStrategicPursuitManager',
     CONVERT(NVARCHAR(2000),DELETED.[CustStrategicPursuitManager],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustStrategicPursuitManager],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustStrategicPursuitManager] Is Null And
				DELETED.[CustStrategicPursuitManager] Is Not Null
			) Or
			(
				INSERTED.[CustStrategicPursuitManager] Is Not Null And
				DELETED.[CustStrategicPursuitManager] Is Null
			) Or
			(
				INSERTED.[CustStrategicPursuitManager] !=
				DELETED.[CustStrategicPursuitManager]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustStrategicPursuitManager = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustStrategicPursuitManager = newDesc.Employee
		END		
		
     If UPDATE([CustAltStrategicPursuitManager])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustAltStrategicPursuitManager',
     CONVERT(NVARCHAR(2000),DELETED.[CustAltStrategicPursuitManager],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustAltStrategicPursuitManager],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustAltStrategicPursuitManager] Is Null And
				DELETED.[CustAltStrategicPursuitManager] Is Not Null
			) Or
			(
				INSERTED.[CustAltStrategicPursuitManager] Is Not Null And
				DELETED.[CustAltStrategicPursuitManager] Is Null
			) Or
			(
				INSERTED.[CustAltStrategicPursuitManager] !=
				DELETED.[CustAltStrategicPursuitManager]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustAltStrategicPursuitManager = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustAltStrategicPursuitManager = newDesc.Employee
		END		
		
      If UPDATE([CustStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustStatus',
      CONVERT(NVARCHAR(2000),DELETED.[CustStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustStatus] Is Null And
				DELETED.[CustStatus] Is Not Null
			) Or
			(
				INSERTED.[CustStatus] Is Not Null And
				DELETED.[CustStatus] Is Null
			) Or
			(
				INSERTED.[CustStatus] !=
				DELETED.[CustStatus]
			)
		) 
		END		
		
     If UPDATE([CustPrimaryPracticeAreaLeader])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPrimaryPracticeAreaLeader',
     CONVERT(NVARCHAR(2000),DELETED.[CustPrimaryPracticeAreaLeader],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustPrimaryPracticeAreaLeader],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustPrimaryPracticeAreaLeader] Is Null And
				DELETED.[CustPrimaryPracticeAreaLeader] Is Not Null
			) Or
			(
				INSERTED.[CustPrimaryPracticeAreaLeader] Is Not Null And
				DELETED.[CustPrimaryPracticeAreaLeader] Is Null
			) Or
			(
				INSERTED.[CustPrimaryPracticeAreaLeader] !=
				DELETED.[CustPrimaryPracticeAreaLeader]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustPrimaryPracticeAreaLeader = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustPrimaryPracticeAreaLeader = newDesc.Employee
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[UDIC_PracticeArea] ADD CONSTRAINT [UDIC_PracticeAreaPK] PRIMARY KEY CLUSTERED ([UDIC_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
