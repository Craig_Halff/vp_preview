CREATE TABLE [dbo].[PYChecksCWage]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__PYChecksC__Perio__2CEB83A1] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__PYChecksC__PostS__2DDFA7DA] DEFAULT ((0)),
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodeWage] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksC__Amoun__2ED3CC13] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PYChecksCWage] ADD CONSTRAINT [PYChecksCWagePK] PRIMARY KEY NONCLUSTERED ([Period], [PostSeq], [Employee], [Code], [CodeWage]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
