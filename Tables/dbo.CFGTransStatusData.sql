CREATE TABLE [dbo].[CFGTransStatusData]
(
[Status] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGTransStatusData] ADD CONSTRAINT [CFGTransStatusDataPK] PRIMARY KEY CLUSTERED ([Status]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
