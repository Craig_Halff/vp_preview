CREATE TABLE [dbo].[LedgerCustomFields]
(
[Period] [int] NOT NULL,
[PostSeq] [int] NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LedgerCustomFields] ADD CONSTRAINT [LedgerCustomFieldsPK] PRIMARY KEY NONCLUSTERED ([Period], [PostSeq], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
