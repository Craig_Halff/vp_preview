CREATE TABLE [dbo].[Inventory]
(
[Location] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Item] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[QtyOnHand] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Inventory__QtyOn__3A2FA4CF] DEFAULT ((0)),
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Inventory__Amoun__3B23C908] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Inventory] ADD CONSTRAINT [InventoryPK] PRIMARY KEY NONCLUSTERED ([Location], [Item]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
