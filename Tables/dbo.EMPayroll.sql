CREATE TABLE [dbo].[EMPayroll]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SelPeriod] [int] NOT NULL CONSTRAINT [DF__EMPayroll__SelPe__2123FFE3] DEFAULT ((0)),
[SelPostSeq] [int] NOT NULL CONSTRAINT [DF__EMPayroll__SelPo__2218241C] DEFAULT ((0)),
[UnpaidRegHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Unpai__230C4855] DEFAULT ((0)),
[RegHrsToBePaid] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__RegHr__24006C8E] DEFAULT ((0)),
[RegHrsOverride] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__RegHr__24F490C7] DEFAULT ('N'),
[CurrentRegPay] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Curre__25E8B500] DEFAULT ((0)),
[UnpaidOvtHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Unpai__26DCD939] DEFAULT ((0)),
[OvtHrsToBePaid] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__OvtHr__27D0FD72] DEFAULT ((0)),
[OvtHrsOverride] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__OvtHr__28C521AB] DEFAULT ('N'),
[CurrentOvtPay] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Curre__29B945E4] DEFAULT ((0)),
[CurrentOtherPay] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Curre__2AAD6A1D] DEFAULT ((0)),
[OtherPayOverride] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Other__2BA18E56] DEFAULT ('N'),
[RegHrsAdjust] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__RegHr__2C95B28F] DEFAULT ((0)),
[RegPayAdjust] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__RegPa__2D89D6C8] DEFAULT ((0)),
[OvtHrsAdjust] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__OvtHr__2E7DFB01] DEFAULT ((0)),
[OvtPayAdjust] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__OvtPa__2F721F3A] DEFAULT ((0)),
[OtherPayAdjust] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__30664373] DEFAULT ((0)),
[RegPayBonus] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__RegPa__315A67AC] DEFAULT ((0)),
[OtherPayBonus] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__324E8BE5] DEFAULT ((0)),
[UnpaidSpecialOvtHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Unpai__3342B01E] DEFAULT ((0)),
[SpecialOvtHrsToBePaid] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Speci__3436D457] DEFAULT ((0)),
[SpecialOvtHrsOverride] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Speci__352AF890] DEFAULT ('N'),
[CurrentSpecialOvtPay] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Curre__361F1CC9] DEFAULT ((0)),
[SpecialOvtHrsAdjust] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Speci__37134102] DEFAULT ((0)),
[SpecialOvtPayAdjust] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Speci__3807653B] DEFAULT ((0)),
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentOtherPay2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Curre__38FB8974] DEFAULT ((0)),
[CurrentOtherPay3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Curre__39EFADAD] DEFAULT ((0)),
[CurrentOtherPay4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Curre__3AE3D1E6] DEFAULT ((0)),
[CurrentOtherPay5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Curre__3BD7F61F] DEFAULT ((0)),
[OtherPay2Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Other__3CCC1A58] DEFAULT ('N'),
[OtherPay2Adjust] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__3DC03E91] DEFAULT ((0)),
[OtherPay2Bonus] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__3EB462CA] DEFAULT ((0)),
[OtherPay3Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Other__3FA88703] DEFAULT ('N'),
[OtherPay3Adjust] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__409CAB3C] DEFAULT ((0)),
[OtherPay3Bonus] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__4190CF75] DEFAULT ((0)),
[OtherPay4Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Other__4284F3AE] DEFAULT ('N'),
[OtherPay4Adjust] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__437917E7] DEFAULT ((0)),
[OtherPay4Bonus] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__446D3C20] DEFAULT ((0)),
[OtherPay5Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Other__45616059] DEFAULT ('N'),
[OtherPay5Adjust] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__46558492] DEFAULT ((0)),
[OtherPay5Bonus] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__4749A8CB] DEFAULT ((0)),
[CalcGrossFromNet] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__CalcG__483DCD04] DEFAULT ('N'),
[GrossFromNetAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Gross__4931F13D] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EMPayroll] ADD CONSTRAINT [EMPayrollPK] PRIMARY KEY NONCLUSTERED ([Employee], [EmployeeCompany]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
