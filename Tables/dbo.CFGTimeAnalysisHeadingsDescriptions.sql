CREATE TABLE [dbo].[CFGTimeAnalysisHeadingsDescriptions]
(
[ReportColumn] [smallint] NOT NULL CONSTRAINT [DF__CFGTimeAn__Repor__02E99186] DEFAULT ((0)),
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGTimeAnalysisHeadingsDescriptions] ADD CONSTRAINT [CFGTimeAnalysisHeadingsDescriptionsPK] PRIMARY KEY CLUSTERED ([ReportColumn], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
