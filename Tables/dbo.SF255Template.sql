CREATE TABLE [dbo].[SF255Template]
(
[TemplateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultTemplate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF255Temp__Defau__35F5CAEE] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF255Template] ADD CONSTRAINT [SF255TemplatePK] PRIMARY KEY CLUSTERED ([TemplateID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
