CREATE TABLE [dbo].[SF254FeeSummary]
(
[SF254ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColIndex] [smallint] NOT NULL CONSTRAINT [DF__SF254FeeS__ColIn__6A8762B2] DEFAULT ((0)),
[Year] [smallint] NOT NULL CONSTRAINT [DF__SF254FeeSu__Year__6B7B86EB] DEFAULT ((0)),
[Federal] [smallint] NOT NULL CONSTRAINT [DF__SF254FeeS__Feder__6C6FAB24] DEFAULT ((0)),
[Domestic] [smallint] NOT NULL CONSTRAINT [DF__SF254FeeS__Domes__6D63CF5D] DEFAULT ((0)),
[ForeignWork] [smallint] NOT NULL CONSTRAINT [DF__SF254FeeS__Forei__6E57F396] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF254FeeSummary] ADD CONSTRAINT [SF254FeeSummaryPK] PRIMARY KEY NONCLUSTERED ([SF254ID], [ColIndex]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
