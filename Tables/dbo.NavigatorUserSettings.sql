CREATE TABLE [dbo].[NavigatorUserSettings]
(
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SettingName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SettingValue] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[NavigatorUserSettings] ADD CONSTRAINT [NavigatorUserSettingsPK] PRIMARY KEY NONCLUSTERED ([Username], [SettingName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
