CREATE TABLE [dbo].[InvMaster]
(
[TransactionNo] [int] NOT NULL CONSTRAINT [DF__InvMaster__Trans__3D0C117A] DEFAULT ((0)),
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransDate] [datetime] NULL,
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PerformedBy] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedDate] [datetime] NULL,
[IRMasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[InvMaster] ADD CONSTRAINT [InvMasterPK] PRIMARY KEY NONCLUSTERED ([MasterPKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
