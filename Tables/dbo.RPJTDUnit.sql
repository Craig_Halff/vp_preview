CREATE TABLE [dbo].[RPJTDUnit]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UnitID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPJTDUnit__Perio__67A2351B] DEFAULT ((0)),
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPJTDUnit__Perio__68965954] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPJTDUnit__Perio__698A7D8D] DEFAULT ((0)),
[PostedFlg] [smallint] NOT NULL CONSTRAINT [DF__RPJTDUnit__Poste__6A7EA1C6] DEFAULT ((1)),
[JTDDate] [datetime] NULL,
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPJTDUnit] ADD CONSTRAINT [RPJTDUnitPK] PRIMARY KEY NONCLUSTERED ([TimePhaseID], [TaskID], [PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPJTDUnitPTAIDX] ON [dbo].[RPJTDUnit] ([PlanID], [TaskID], [UnitID], [StartDate], [EndDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPJTDUnitAllIDX] ON [dbo].[RPJTDUnit] ([PlanID], [TaskID], [UnitID], [StartDate], [EndDate], [PeriodQty], [PeriodCost], [PeriodBill]) ON [PRIMARY]
GO
