CREATE TABLE [dbo].[AbsenceRequestsHours]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransDate] [datetime] NOT NULL,
[Hours] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__AbsenceRe__Hours__1DE57479] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AbsenceRequestsHours] ADD CONSTRAINT [AbsenceRequestsHoursPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [TransDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
