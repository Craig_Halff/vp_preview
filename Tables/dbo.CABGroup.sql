CREATE TABLE [dbo].[CABGroup]
(
[GroupBudgetName] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BudgetYear] [smallint] NOT NULL CONSTRAINT [DF__CABGroup___Budge__41CFFB40] DEFAULT ((0)),
[AvailableForReporting] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CABGroup___Avail__42C41F79] DEFAULT ('Y'),
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CABGroup] ADD CONSTRAINT [CABGroupPK] PRIMARY KEY NONCLUSTERED ([GroupBudgetName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
