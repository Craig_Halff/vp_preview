CREATE TABLE [dbo].[CFGOpportunityStageData_Backup]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Closed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGOpport__Close__305B567B] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGOpportunityStageData_Backup] ADD CONSTRAINT [CFGOpportunityStageDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
