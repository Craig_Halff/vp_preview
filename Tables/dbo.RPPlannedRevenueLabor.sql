CREATE TABLE [dbo].[RPPlannedRevenueLabor]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Perio__25EB1705] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Perio__26DF3B3E] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPPlannedRevenueLabor] ADD CONSTRAINT [RPPlannedRevenueLaborPK] PRIMARY KEY CLUSTERED ([TimePhaseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPPlannedRevenuLaborAll2IDX] ON [dbo].[RPPlannedRevenueLabor] ([PlanID], [TaskID], [StartDate], [EndDate], [PeriodCost], [PeriodBill]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPPlannedRevenueLaborAllIDX] ON [dbo].[RPPlannedRevenueLabor] ([PlanID], [TimePhaseID], [TaskID], [StartDate], [EndDate], [PeriodCost], [PeriodBill]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
