CREATE TABLE [dbo].[CFGNonWorkingDay]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MondayInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGNonWor__Monda__0D121A3E] DEFAULT ('N'),
[TuesdayInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGNonWor__Tuesd__0E063E77] DEFAULT ('N'),
[WednesdayInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGNonWor__Wedne__0EFA62B0] DEFAULT ('N'),
[ThursdayInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGNonWor__Thurs__0FEE86E9] DEFAULT ('N'),
[FridayInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGNonWor__Frida__10E2AB22] DEFAULT ('N'),
[SaturdayInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGNonWor__Satur__11D6CF5B] DEFAULT ('Y'),
[SundayInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGNonWor__Sunda__12CAF394] DEFAULT ('Y')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGNonWorkingDay] ADD CONSTRAINT [CFGNonWorkingDayPK] PRIMARY KEY CLUSTERED ([Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
