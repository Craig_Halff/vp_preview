CREATE TABLE [dbo].[CFGAutoNumSproc]
(
[RuleID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StoredProcedure] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NavID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChargeType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAutoNu__Charg__3164894D] DEFAULT (' ')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAutoNumSproc] ADD CONSTRAINT [CFGAutoNumSprocPK] PRIMARY KEY CLUSTERED ([RuleID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
