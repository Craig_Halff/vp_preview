CREATE TABLE [dbo].[CustomProposalSubscr]
(
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomPropID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomProposalSubscr] ADD CONSTRAINT [CustomProposalSubscrPK] PRIMARY KEY NONCLUSTERED ([PartKey], [CustomPropID], [UserName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
