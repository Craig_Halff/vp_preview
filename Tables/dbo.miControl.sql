CREATE TABLE [dbo].[miControl]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PostPeriod] [int] NOT NULL CONSTRAINT [DF__miControl__PostP__778DB8CA] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__miControl__PostS__7881DD03] DEFAULT ((0)),
[Recurring] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__miControl__Recur__7976013C] DEFAULT ('N'),
[Selected] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__miControl__Selec__7A6A2575] DEFAULT ('N'),
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__miControl__Poste__7B5E49AE] DEFAULT ('N'),
[Creator] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__miControl__Perio__7C526DE7] DEFAULT ((0)),
[EndDate] [datetime] NULL,
[Total] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__miControl__Total__7D469220] DEFAULT ((0)),
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedDate] [datetime] NULL,
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[miControl] ADD CONSTRAINT [miControlPK] PRIMARY KEY NONCLUSTERED ([Batch]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
