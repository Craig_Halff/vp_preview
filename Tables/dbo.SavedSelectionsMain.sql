CREATE TABLE [dbo].[SavedSelectionsMain]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[Private] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SavedSele__Priva__3230F259] DEFAULT ('Y'),
[Folder] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkedPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WhereClauseSearch] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SavedSele__Where__122F0272] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SavedSelectionsMain] ADD CONSTRAINT [SavedSelectionsMainPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [SavedSelectionsMainUsernameIDX] ON [dbo].[SavedSelectionsMain] ([Username]) ON [PRIMARY]
GO
