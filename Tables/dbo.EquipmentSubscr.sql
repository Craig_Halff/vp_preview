CREATE TABLE [dbo].[EquipmentSubscr]
(
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EquipmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EquipmentSubscr] ADD CONSTRAINT [EquipmentSubscrPK] PRIMARY KEY NONCLUSTERED ([PartKey], [EquipmentID], [UserName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
