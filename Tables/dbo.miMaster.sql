CREATE TABLE [dbo].[miMaster]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransDate] [datetime] NULL,
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__miMaster___Poste__02FF6B76] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__miMaster_Ne__Seq__03F38FAF] DEFAULT ((0)),
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyExchangeOverrideMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__miMaster___Curre__04E7B3E8] DEFAULT ('N'),
[CurrencyExchangeOverrideDate] [datetime] NULL,
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__miMaster___Curre__05DBD821] DEFAULT ((0)),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__miMaster___Statu__06CFFC5A] DEFAULT ('N'),
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RejectReason] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Diary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaryNo] [int] NOT NULL CONSTRAINT [DF__miMaster___Diary__07C42093] DEFAULT ((0)),
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[miMaster] ADD CONSTRAINT [miMasterPK] PRIMARY KEY NONCLUSTERED ([Batch], [RefNo]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
