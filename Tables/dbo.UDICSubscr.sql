CREATE TABLE [dbo].[UDICSubscr]
(
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UDIC_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UDIC_ID] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UDICSubscr] ADD CONSTRAINT [UDICSubscrPK] PRIMARY KEY NONCLUSTERED ([PartKey], [UDIC_UID], [UDIC_ID], [UserName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
