CREATE TABLE [dbo].[BTEC]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__BTEC_New__TableN__4E6ADC4F] DEFAULT ((0)),
[TableName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AvailableForPlanning] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BTEC_New__Availa__4F5F0088] DEFAULT ('N'),
[FilterOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterPrincipal] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterProjMgr] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterSupervisor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTEC] ADD CONSTRAINT [BTECPK] PRIMARY KEY CLUSTERED ([TableNo]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
