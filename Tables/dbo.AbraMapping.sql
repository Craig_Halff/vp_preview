CREATE TABLE [dbo].[AbraMapping]
(
[ImportExport] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__AbraMappi__Impor__6F56CECE] DEFAULT ('N'),
[Abfield] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Include] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__AbraMappi__Inclu__704AF307] DEFAULT ('N'),
[Expr] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AbraMapping] ADD CONSTRAINT [AbraMappingPK] PRIMARY KEY NONCLUSTERED ([ImportExport], [Abfield]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
