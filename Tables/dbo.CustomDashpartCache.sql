CREATE TABLE [dbo].[CustomDashpartCache]
(
[DashpartCacheID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DashpartID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HashValue] [varbinary] (32) NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[Cache] [varbinary] (max) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomDashpartCache] ADD CONSTRAINT [CustomDashpartCachePK] PRIMARY KEY CLUSTERED ([DashpartCacheID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
