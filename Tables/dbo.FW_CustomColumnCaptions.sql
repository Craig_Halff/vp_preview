CREATE TABLE [dbo].[FW_CustomColumnCaptions]
(
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CustomColumnCaptions] ADD CONSTRAINT [FW_CustomColumnCaptionsPK] PRIMARY KEY NONCLUSTERED ([InfoCenterArea], [GridID], [UICultureName], [Name]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
