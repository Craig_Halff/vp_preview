CREATE TABLE [dbo].[CCG_EI_ConfigStorage]
(
[Seq] [int] NOT NULL IDENTITY(1, 2),
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UseType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StorageType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Server] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Catalog] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Root] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserName] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowDelete] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevisionType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MarkupType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateChanged] [datetime] NULL,
[ChangedBy] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Detail] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_ConfigStorage] ADD CONSTRAINT [PK_CCG_EI_ConfigStorage] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
