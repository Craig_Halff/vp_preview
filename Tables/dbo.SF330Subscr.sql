CREATE TABLE [dbo].[SF330Subscr]
(
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SF330ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF330Subscr] ADD CONSTRAINT [SF330SubscrPK] PRIMARY KEY NONCLUSTERED ([PartKey], [SF330ID], [UserName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
