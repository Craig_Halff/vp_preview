CREATE TABLE [dbo].[CCG_PAT_HistoryUpdateColumns]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[ActionTaken] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionDate] [datetime] NOT NULL,
[ActionTakenBy] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayableSeq] [int] NULL,
[DataType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConfigCustomColumn] [int] NULL,
[ColumnLabel] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldValue] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewValue] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldValueDecimal] [decimal] (19, 5) NULL,
[NewValueDecimal] [decimal] (19, 5) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_HistoryUpdateColumns] ADD CONSTRAINT [PK_CCG_PAT_HistoryUpdateColumns] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Historical tracking of changes to custom columns', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryUpdateColumns', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date when this action occurred', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryUpdateColumns', 'COLUMN', N'ActionDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Short description of type of action that is captured (e.g. Change)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryUpdateColumns', 'COLUMN', N'ActionTaken'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Employee id of the action initiator', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryUpdateColumns', 'COLUMN', N'ActionTakenBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The name of the custom column', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryUpdateColumns', 'COLUMN', N'ColumnLabel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - The Seq number of the custom column for which this action happened', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryUpdateColumns', 'COLUMN', N'ConfigCustomColumn'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The type of data modified (A = alphanumeric / C = currency / N = numeric / D = date / I = integer / P = percent / L = link / V = dropdown / T = time)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryUpdateColumns', 'COLUMN', N'DataType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*String value (if any) of the custom column after action', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryUpdateColumns', 'COLUMN', N'NewValue'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Numeric value (if any) of the custom column after action', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryUpdateColumns', 'COLUMN', N'NewValueDecimal'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*String value (if any) of the custom column before action (relates only to non-numeric custom columns)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryUpdateColumns', 'COLUMN', N'OldValue'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Numeric value (if any) of the custom column before action (relates only to numeric custom column types)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryUpdateColumns', 'COLUMN', N'OldValueDecimal'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - The associated payable sequence number of this action', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryUpdateColumns', 'COLUMN', N'PayableSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_HistoryUpdateColumns', 'COLUMN', N'Seq'
GO
