CREATE TABLE [dbo].[GRSkills]
(
[Code] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Skill] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SkillLevel] [smallint] NOT NULL,
[PrimaryInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GRSkills] ADD CONSTRAINT [GRSkillsPK] PRIMARY KEY NONCLUSTERED ([Code], [Skill]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
