CREATE TABLE [dbo].[OpportunityFEFuncGroups_Backup]
(
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FuncGroupCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FeeBand] [smallint] NOT NULL CONSTRAINT [DF__Opportuni__FeeBa__364A0552] DEFAULT ((0)),
[RateCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeableCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportuni__Charg__373E298B] DEFAULT ((0)),
[Fee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportunity__Fee__38324DC4] DEFAULT ((0)),
[Seq] [smallint] NOT NULL CONSTRAINT [DF__Opportunity__Seq__392671FD] DEFAULT ((0)),
[FeeBandPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportuni__FeeBa__3A1A9636] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OpportunityFEFuncGroups_Backup] ADD CONSTRAINT [OpportunityFEFuncGroupsPK] PRIMARY KEY NONCLUSTERED ([OpportunityID], [FuncGroupCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
