CREATE TABLE [dbo].[CFGBillTaxesICBillingExclusionAccounts]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGBillTaxesICBillingExclusionAccounts] ADD CONSTRAINT [CFGBillTaxesICBillingExclusionAccountsPK] PRIMARY KEY CLUSTERED ([Code], [Account]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
