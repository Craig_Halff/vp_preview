CREATE TABLE [dbo].[CFGBillApprovalActions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionRole] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AlertRole] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AlertType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AlertFrequency] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DashboardSummaryLink] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGBillAp__Dashb__7702DD8E] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGBillApprovalActions] ADD CONSTRAINT [CFGBillApprovalActionsPK] PRIMARY KEY CLUSTERED ([Code], [Action]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
