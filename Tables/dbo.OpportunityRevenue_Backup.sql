CREATE TABLE [dbo].[OpportunityRevenue_Backup]
(
[RevAllocID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevenueDate] [datetime] NULL,
[RevenueAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportuni__Reven__753B5C04] DEFAULT ((0)),
[PercentRevenue] [smallint] NOT NULL CONSTRAINT [DF__Opportuni__Perce__762F803D] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OpportunityRevenue_Backup] ADD CONSTRAINT [OpportunityRevenuePK] PRIMARY KEY NONCLUSTERED ([RevAllocID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
