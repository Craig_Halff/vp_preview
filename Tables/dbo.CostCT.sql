CREATE TABLE [dbo].[CostCT]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__CostCT_Ne__Table__3F888B72] DEFAULT ((0)),
[TableName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CostCT_Ne__RateT__407CAFAB] DEFAULT ('B'),
[AvailableForPlanning] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CostCT] ADD CONSTRAINT [CostCTPK] PRIMARY KEY CLUSTERED ([TableNo]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
