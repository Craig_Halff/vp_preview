CREATE TABLE [dbo].[inControl]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PostPeriod] [int] NOT NULL CONSTRAINT [DF__inControl__PostP__19C2D53D] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__inControl__PostS__1AB6F976] DEFAULT ((0)),
[Recurring] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__inControl__Recur__1BAB1DAF] DEFAULT ('N'),
[Selected] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__inControl__Selec__1C9F41E8] DEFAULT ('N'),
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__inControl__Poste__1D936621] DEFAULT ('N'),
[Creator] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__inControl__Perio__1E878A5A] DEFAULT ((0)),
[EndDate] [datetime] NULL,
[Total] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__inControl__Total__1F7BAE93] DEFAULT ((0)),
[RetTotal] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__inControl__RetTo__206FD2CC] DEFAULT ((0)),
[DefaultTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SubmittedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedDate] [datetime] NULL,
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[inControl] ADD CONSTRAINT [inControlPK] PRIMARY KEY NONCLUSTERED ([Batch]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
