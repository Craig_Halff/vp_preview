CREATE TABLE [dbo].[CFGEMSkillLevelDescriptions]
(
[Code] [smallint] NOT NULL CONSTRAINT [DF__CFGEMSkill__Code__74B080C8] DEFAULT ((0)),
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGEMSkillL__Seq__75A4A501] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGEMSkillLevelDescriptions] ADD CONSTRAINT [CFGEMSkillLevelDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
