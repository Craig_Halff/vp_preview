CREATE TABLE [dbo].[BTLaborCatsData]
(
[Category] [smallint] NOT NULL CONSTRAINT [DF__BTLaborCa__Categ__7584A970] DEFAULT ((0)),
[CategoryCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTLaborCatsData] ADD CONSTRAINT [BTLaborCatsDataPK] PRIMARY KEY CLUSTERED ([Category]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
