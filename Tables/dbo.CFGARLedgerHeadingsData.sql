CREATE TABLE [dbo].[CFGARLedgerHeadingsData]
(
[ReportColumn] [smallint] NOT NULL CONSTRAINT [DF__CFGARLedg__Repor__4EC8A2F6] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGARLedgerHeadingsData] ADD CONSTRAINT [CFGARLedgerHeadingsDataPK] PRIMARY KEY CLUSTERED ([ReportColumn]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
