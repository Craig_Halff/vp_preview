CREATE TABLE [dbo].[billINDetail]
(
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__billINDetai__Seq__1B8A56C7] DEFAULT ((0)),
[InvoiceSection] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billINDet__Amoun__1C7E7B00] DEFAULT ((0)),
[RetAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billINDet__RetAm__1D729F39] DEFAULT ((0)),
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billINDet__TaxBa__1E66C372] DEFAULT ((0)),
[Retainer] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__billINDet__Retai__1F5AE7AB] DEFAULT ('N'),
[CreditMemoRefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[billINDetail] ADD CONSTRAINT [billINDetailPK] PRIMARY KEY NONCLUSTERED ([MainWBS1], [Invoice], [WBS1], [WBS2], [WBS3], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
