CREATE TABLE [dbo].[ImportDetail]
(
[InfoCenter] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FieldName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Include] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ImportDet__Inclu__1409FBE7] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__ImportDetai__Seq__14FE2020] DEFAULT ((0)),
[Expression] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ImportDetail] ADD CONSTRAINT [ImportDetailPK] PRIMARY KEY NONCLUSTERED ([InfoCenter], [UserName], [FieldName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
