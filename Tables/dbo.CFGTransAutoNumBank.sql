CREATE TABLE [dbo].[CFGTransAutoNumBank]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EnableAutoNumReceipts] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransA__Enabl__3480ED1A] DEFAULT ('N'),
[EnableAutoNumPayments] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransA__Enabl__35751153] DEFAULT ('N'),
[NextRefNoReceipts] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NextRefNoPayments] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoNumberPrefix] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoNumberPaymentType] [smallint] NOT NULL CONSTRAINT [DF__CFGTransA__AutoN__76DB2261] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGTransAutoNumBank] ADD CONSTRAINT [CFGTransAutoNumBankPK] PRIMARY KEY CLUSTERED ([Company], [BankCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
