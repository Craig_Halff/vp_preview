CREATE TABLE [dbo].[CFGPrimarySpecialtyDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGPrimaryS__Seq__138A0DA3] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPrimarySpecialtyDescriptions] ADD CONSTRAINT [CFGPrimarySpecialtyDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
