CREATE TABLE [dbo].[LedgerMisc_test]
(
[Period] [int] NOT NULL,
[PostSeq] [int] NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Org] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransType] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefNo] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransDate] [datetime] NULL,
[Desc1] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Desc2] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL,
[CBAmount] [decimal] (19, 4) NOT NULL,
[BillExt] [decimal] (19, 4) NOT NULL,
[ProjectCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AutoEntry] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuppressBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BillStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SkipGL] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceSection] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Line] [smallint] NOT NULL,
[PartialPayment] [decimal] (19, 4) NOT NULL,
[Discount] [decimal] (19, 4) NOT NULL,
[Voucher] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledInvoice] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledPeriod] [int] NOT NULL,
[Unit] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitTable] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitQuantity] [decimal] (19, 4) NOT NULL,
[UnitCostRate] [decimal] (19, 4) NOT NULL,
[UnitBillingRate] [decimal] (19, 4) NOT NULL,
[UnitBillExt] [decimal] (19, 4) NOT NULL,
[XferWBS1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferWBS2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferWBS3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferAccount] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxBasis] [decimal] (19, 4) NOT NULL,
[TaxCBBasis] [decimal] (19, 4) NOT NULL,
[BillTaxCodeOverride] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WrittenOffPeriod] [int] NOT NULL,
[TransactionAmount] [decimal] (19, 4) NOT NULL,
[TransactionCurrencyCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExchangeInfo] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountProjectCurrency] [decimal] (19, 4) NOT NULL,
[ProjectExchangeInfo] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountBillingCurrency] [decimal] (19, 4) NOT NULL,
[BillingExchangeInfo] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoEntryAmount] [decimal] (19, 4) NOT NULL,
[AutoEntryExchangeInfo] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoEntryOrg] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoEntryAccount] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountSourceCurrency] [decimal] (19, 4) NOT NULL,
[SourceExchangeInfo] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PONumber] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitCostRateBillingCurrency] [decimal] (19, 4) NOT NULL,
[LinkCompany] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillTax2CodeOverride] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GainsAndLossesType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountTaxCurrency] [decimal] (19, 4) NOT NULL,
[CBAmountTaxCurrency] [decimal] (19, 4) NOT NULL,
[TaxBasisTaxCurrency] [decimal] (19, 4) NOT NULL,
[TaxCBBasisTaxCurrency] [decimal] (19, 4) NOT NULL,
[TaxBasisFunctionalCurrency] [decimal] (19, 4) NOT NULL,
[TaxCBBasisFunctionalCurrency] [decimal] (19, 4) NOT NULL,
[DiscountFunctionalCurrency] [decimal] (19, 4) NOT NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthorizedBy] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RealizationAmountEmployeeCurrency] [decimal] (19, 4) NOT NULL,
[RealizationAmountProjectCurrency] [decimal] (19, 4) NOT NULL,
[RealizationAmountBillingCurrency] [decimal] (19, 4) NOT NULL,
[NonBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditMemoRefNo] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalAmountSourceCurrency] [decimal] (19, 4) NOT NULL,
[OriginalPaymentCurrencyCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InProcessAccount] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InProcessAccountCleared] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EKOriginalLine] [smallint] NOT NULL,
[InvoiceStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginatingVendor] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Diary] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaryNo] [int] NOT NULL,
[CreditCardPrimaryCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransferredPeriod] [int] NOT NULL,
[TransferredBillStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreInvoice] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EquipmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BookCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmOrg] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
