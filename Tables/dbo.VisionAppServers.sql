CREATE TABLE [dbo].[VisionAppServers]
(
[AppServer] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VisionAppServers] ADD CONSTRAINT [VisionAppServersPK] PRIMARY KEY CLUSTERED ([AppServer]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
