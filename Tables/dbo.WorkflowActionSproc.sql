CREATE TABLE [dbo].[WorkflowActionSproc]
(
[ActionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StoredProcedure] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReloadRecord] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__WorkflowA__Reloa__13C0AE7B] DEFAULT ('N'),
[RunAfterSave] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__WorkflowA__RunAf__14B4D2B4] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowActionSproc] ADD CONSTRAINT [WorkflowActionSprocPK] PRIMARY KEY NONCLUSTERED ([ActionID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
