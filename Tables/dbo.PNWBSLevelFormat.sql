CREATE TABLE [dbo].[PNWBSLevelFormat]
(
[WBSFormatID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FmtLevel] [int] NOT NULL CONSTRAINT [DF__PNWBSLeve__FmtLe__1414E8DF] DEFAULT ((0)),
[WBSType] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBSMatch] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNWBSLeve__WBSMa__15090D18] DEFAULT ('Y'),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PNWBSLevelFormat] ADD CONSTRAINT [PNWBSLevelFormatPK] PRIMARY KEY NONCLUSTERED ([WBSFormatID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNWBSLevelFormatIDTypeMatchIDX] ON [dbo].[PNWBSLevelFormat] ([PlanID], [WBSType], [WBSMatch]) ON [PRIMARY]
GO
