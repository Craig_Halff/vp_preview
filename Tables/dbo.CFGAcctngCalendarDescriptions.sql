CREATE TABLE [dbo].[CFGAcctngCalendarDescriptions]
(
[StartDate] [datetime] NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAcctngCalendarDescriptions] ADD CONSTRAINT [CFGAcctngCalendarDescriptionsPK] PRIMARY KEY CLUSTERED ([StartDate], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
