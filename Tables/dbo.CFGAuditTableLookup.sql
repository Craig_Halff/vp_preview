CREATE TABLE [dbo].[CFGAuditTableLookup]
(
[SourceTable] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceColumn] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LookupTable] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LookupColumn] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LookupDesc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAuditTableLookup] ADD CONSTRAINT [CFGAuditTableLookupPK] PRIMARY KEY CLUSTERED ([SourceTable], [SourceColumn]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
