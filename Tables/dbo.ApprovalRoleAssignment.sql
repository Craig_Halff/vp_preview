CREATE TABLE [dbo].[ApprovalRoleAssignment]
(
[AssignID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RoleID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ApprovalRoleAssignment] ADD CONSTRAINT [ApprovalRoleAssignmentPK] PRIMARY KEY CLUSTERED ([AssignID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
