CREATE TABLE [dbo].[PRLabor]
(
[LaborID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EstimateHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRLabor__Estimat__64326241] DEFAULT ((0)),
[EstimateCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRLabor__Estimat__6526867A] DEFAULT ((0)),
[EstimateBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRLabor__Estimat__661AAAB3] DEFAULT ((0)),
[CostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRLabor__CostRat__670ECEEC] DEFAULT ((0)),
[BillingRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRLabor__Billing__6802F325] DEFAULT ((0)),
[Category] [smallint] NOT NULL CONSTRAINT [DF__PRLabor__Categor__68F7175E] DEFAULT ((0)),
[SeqNo] [smallint] NOT NULL CONSTRAINT [DF__PRLabor__SeqNo__69EB3B97] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[GenericResourceID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PRLabor] ADD CONSTRAINT [PRLaborPK] PRIMARY KEY NONCLUSTERED ([LaborID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
