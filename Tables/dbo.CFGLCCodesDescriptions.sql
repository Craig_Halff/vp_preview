CREATE TABLE [dbo].[CFGLCCodesDescriptions]
(
[Code] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LCLevel] [smallint] NOT NULL CONSTRAINT [DF__CFGLCCode__LCLev__40C6C86E] DEFAULT ((0)),
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGLCCodesDescriptions] ADD CONSTRAINT [CFGLCCodesDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [LCLevel], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CFGLCCodesDescriptionsUICultureIDX] ON [dbo].[CFGLCCodesDescriptions] ([LCLevel], [UICultureName]) INCLUDE ([Code], [Label]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
