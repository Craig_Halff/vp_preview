CREATE TABLE [dbo].[DashLayoutUser]
(
[DashLayoutID] [int] NOT NULL,
[UserID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Col1Width] [smallint] NOT NULL CONSTRAINT [DF__DashLayou__Col1W__15283D52] DEFAULT ((0)),
[Col2Width] [smallint] NOT NULL CONSTRAINT [DF__DashLayou__Col2W__161C618B] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DashLayoutUser] ADD CONSTRAINT [DashLayoutUserPK] PRIMARY KEY CLUSTERED ([DashLayoutID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
