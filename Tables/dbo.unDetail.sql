CREATE TABLE [dbo].[unDetail]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Unit] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__unDetail_Ne__Seq__078FE1C0] DEFAULT ((0)),
[TransDate] [datetime] NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TableNo] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Quantity] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__unDetail___Quant__088405F9] DEFAULT ((0)),
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__unDetail___Curre__09782A32] DEFAULT ((0)),
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[tkPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[unDetail] ADD CONSTRAINT [unDetailPK] PRIMARY KEY NONCLUSTERED ([Batch], [Unit], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
