CREATE TABLE [dbo].[PRRevenue]
(
[RevAllocID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevenueDate] [datetime] NULL,
[RevenueAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRRevenue__Reven__511F8DCD] DEFAULT ((0)),
[PercentRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRRevenue__PercentRevenue] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PRRevenue] ADD CONSTRAINT [PRRevenuePK] PRIMARY KEY NONCLUSTERED ([RevAllocID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
