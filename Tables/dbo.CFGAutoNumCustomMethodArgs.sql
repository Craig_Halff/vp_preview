CREATE TABLE [dbo].[CFGAutoNumCustomMethodArgs]
(
[RuleID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ArgName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ArgOrder] [int] NOT NULL CONSTRAINT [DF__CFGAutoNu__ArgOr__26E6FADA] DEFAULT ((0)),
[NavID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SQLExpression] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SQLIfExpression] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SQLElseExpression] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAutoNumCustomMethodArgs] ADD CONSTRAINT [CFGAutoNumCustomMethodArgsPK] PRIMARY KEY NONCLUSTERED ([RuleID], [ArgName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
