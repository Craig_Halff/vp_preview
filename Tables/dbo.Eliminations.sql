CREATE TABLE [dbo].[Eliminations]
(
[ReportingGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__Eliminati__Perio__61738F22] DEFAULT ((0)),
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Eliminati__Amoun__6267B35B] DEFAULT ((0)),
[CBAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Eliminati__CBAmo__635BD794] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Eliminations] ADD CONSTRAINT [EliminationsPK] PRIMARY KEY NONCLUSTERED ([ReportingGroup], [Company], [Period], [Account]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
