CREATE TABLE [dbo].[MergeTemplates]
(
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MergeTemplateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Template] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TemplateFileName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__MergeTempl__Type__73BD27E6] DEFAULT ('W'),
[FileID] [uniqueidentifier] NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MergeTemplates] ADD CONSTRAINT [MergeTemplatesPK] PRIMARY KEY NONCLUSTERED ([InfoCenterArea], [MergeTemplateID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
