CREATE TABLE [dbo].[CFGNaturalKeys]
(
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[KeyID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__CFGNaturalK__Seq__084D6521] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGNaturalKeys] ADD CONSTRAINT [CFGNaturalKeysPK] PRIMARY KEY NONCLUSTERED ([InfoCenterArea], [KeyID], [ColName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
