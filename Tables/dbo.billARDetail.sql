CREATE TABLE [dbo].[billARDetail]
(
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecdSeq] [int] NOT NULL CONSTRAINT [DF__billARDet__RecdS__50332930] DEFAULT ((0)),
[BillWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNumber] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL,
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Balance] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billARDet__Balan__51274D69] DEFAULT ((0)),
[Invoiced] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billARDet__Invoi__521B71A2] DEFAULT ((0)),
[Received] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billARDet__Recei__530F95DB] DEFAULT ((0)),
[RetainageFormat] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__billARDet__Retai__5403BA14] DEFAULT ('N'),
[Retainage] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billARDet__Retai__54F7DE4D] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[billARDetail] ADD CONSTRAINT [billARDetailPK] PRIMARY KEY NONCLUSTERED ([Invoice], [MainWBS1], [RecdSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
