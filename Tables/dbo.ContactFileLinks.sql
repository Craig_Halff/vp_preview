CREATE TABLE [dbo].[ContactFileLinks]
(
[LinkID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilePath] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Graphic] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ContactFi__Graph__5A718BD8] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_ContactFileLinks]
      ON [dbo].[ContactFileLinks]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ContactFileLinks'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'LinkID',CONVERT(NVARCHAR(2000),[LinkID],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'ContactID',CONVERT(NVARCHAR(2000),DELETED.[ContactID],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join Contacts as oldDesc  on DELETED.ContactID = oldDesc.ContactID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Description',CONVERT(NVARCHAR(2000),[Description],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'FilePath',CONVERT(NVARCHAR(2000),[FilePath],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ContactID],121),'Graphic',CONVERT(NVARCHAR(2000),[Graphic],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_ContactFileLinks] ON [dbo].[ContactFileLinks]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_ContactFileLinks]
      ON [dbo].[ContactFileLinks]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ContactFileLinks'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'LinkID',NULL,CONVERT(NVARCHAR(2000),[LinkID],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'ContactID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[ContactID],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  Contacts as newDesc  on INSERTED.ContactID = newDesc.ContactID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Description',NULL,CONVERT(NVARCHAR(2000),[Description],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FilePath',NULL,CONVERT(NVARCHAR(2000),[FilePath],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Graphic',NULL,CONVERT(NVARCHAR(2000),[Graphic],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_ContactFileLinks] ON [dbo].[ContactFileLinks]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_ContactFileLinks]
      ON [dbo].[ContactFileLinks]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ContactFileLinks'
    
      If UPDATE([LinkID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'LinkID',
      CONVERT(NVARCHAR(2000),DELETED.[LinkID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LinkID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[LinkID] = DELETED.[LinkID] AND INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[LinkID] Is Null And
				DELETED.[LinkID] Is Not Null
			) Or
			(
				INSERTED.[LinkID] Is Not Null And
				DELETED.[LinkID] Is Null
			) Or
			(
				INSERTED.[LinkID] !=
				DELETED.[LinkID]
			)
		) 
		END		
		
     If UPDATE([ContactID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'ContactID',
     CONVERT(NVARCHAR(2000),DELETED.[ContactID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[ContactID],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[LinkID] = DELETED.[LinkID] AND INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[ContactID] Is Null And
				DELETED.[ContactID] Is Not Null
			) Or
			(
				INSERTED.[ContactID] Is Not Null And
				DELETED.[ContactID] Is Null
			) Or
			(
				INSERTED.[ContactID] !=
				DELETED.[ContactID]
			)
		) left join Contacts as oldDesc  on DELETED.ContactID = oldDesc.ContactID  left join  Contacts as newDesc  on INSERTED.ContactID = newDesc.ContactID
		END		
		
      If UPDATE([Description])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Description',
      CONVERT(NVARCHAR(2000),DELETED.[Description],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Description],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[LinkID] = DELETED.[LinkID] AND INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Description] Is Null And
				DELETED.[Description] Is Not Null
			) Or
			(
				INSERTED.[Description] Is Not Null And
				DELETED.[Description] Is Null
			) Or
			(
				INSERTED.[Description] !=
				DELETED.[Description]
			)
		) 
		END		
		
      If UPDATE([FilePath])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'FilePath',
      CONVERT(NVARCHAR(2000),DELETED.[FilePath],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FilePath],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[LinkID] = DELETED.[LinkID] AND INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[FilePath] Is Null And
				DELETED.[FilePath] Is Not Null
			) Or
			(
				INSERTED.[FilePath] Is Not Null And
				DELETED.[FilePath] Is Null
			) Or
			(
				INSERTED.[FilePath] !=
				DELETED.[FilePath]
			)
		) 
		END		
		
      If UPDATE([Graphic])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ContactID],121),'Graphic',
      CONVERT(NVARCHAR(2000),DELETED.[Graphic],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Graphic],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[LinkID] = DELETED.[LinkID] AND INSERTED.[ContactID] = DELETED.[ContactID] AND 
		(
			(
				INSERTED.[Graphic] Is Null And
				DELETED.[Graphic] Is Not Null
			) Or
			(
				INSERTED.[Graphic] Is Not Null And
				DELETED.[Graphic] Is Null
			) Or
			(
				INSERTED.[Graphic] !=
				DELETED.[Graphic]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_ContactFileLinks] ON [dbo].[ContactFileLinks]
GO
ALTER TABLE [dbo].[ContactFileLinks] ADD CONSTRAINT [ContactFileLinksPK] PRIMARY KEY NONCLUSTERED ([LinkID], [ContactID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
