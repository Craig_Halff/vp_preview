CREATE TABLE [dbo].[APApprovalWBS]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExpenseCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NetAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__APApprova__NetAm__5561F2A1] DEFAULT ((0)),
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__APApprova__Amoun__565616DA] DEFAULT ((0)),
[Seq] [int] NOT NULL CONSTRAINT [DF__APApprovalW__Seq__574A3B13] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[APApprovalWBS] ADD CONSTRAINT [APApprovalWBSPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
