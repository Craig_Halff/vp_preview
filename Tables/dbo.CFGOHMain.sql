CREATE TABLE [dbo].[CFGOHMain]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OHProcedure] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OHAllocMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OHBasis] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OHRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGOHMain__OHRat__15A7603F] DEFAULT ((0)),
[OHVarianceWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OHVarianceWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OHVarianceWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OHProvisionalRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGOHMain__OHPro__169B8478] DEFAULT ((0)),
[PlugAmtRev] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGOHMain__PlugA__178FA8B1] DEFAULT ((0)),
[PlugAmtDirLab] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGOHMain__PlugA__1883CCEA] DEFAULT ((0)),
[PlugAmtDirExp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGOHMain__PlugA__1977F123] DEFAULT ((0)),
[PlugAmtIndLab] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGOHMain__PlugA__1A6C155C] DEFAULT ((0)),
[PlugAmtIndExp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGOHMain__PlugA__1B603995] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGOHMain] ADD CONSTRAINT [CFGOHMainPK] PRIMARY KEY CLUSTERED ([Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
