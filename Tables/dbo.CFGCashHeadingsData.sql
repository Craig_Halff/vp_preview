CREATE TABLE [dbo].[CFGCashHeadingsData]
(
[ReportColumn] [smallint] NOT NULL CONSTRAINT [DF__CFGCashHe__Repor__011F1899] DEFAULT ((0)),
[SubTotal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCashHeadingsData] ADD CONSTRAINT [CFGCashHeadingsDataPK] PRIMARY KEY CLUSTERED ([ReportColumn]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
