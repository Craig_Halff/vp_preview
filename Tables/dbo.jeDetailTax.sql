CREATE TABLE [dbo].[jeDetailTax]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxDebitAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__jeDetailT__TaxDe__7FCDF910] DEFAULT ((0)),
[TaxCreditAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__jeDetailT__TaxCr__00C21D49] DEFAULT ((0)),
[CompoundOnTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__jeDetailTax__Seq__01B64182] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[jeDetailTax] ADD CONSTRAINT [jeDetailTaxPK] PRIMARY KEY CLUSTERED ([Batch], [RefNo], [PKey], [TaxCode]) ON [PRIMARY]
GO
