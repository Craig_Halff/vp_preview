CREATE TABLE [dbo].[CFGUnitTypeDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGUnitType__Seq__44B754E3] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGUnitTypeDescriptions] ADD CONSTRAINT [CFGUnitTypeDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
