CREATE TABLE [dbo].[CFGTransApprover]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GlobalApproval] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransA__Globa__3298A4A8] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGTransApprover] ADD CONSTRAINT [CFGTransApproverPK] PRIMARY KEY CLUSTERED ([Company], [Employee]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
