CREATE TABLE [dbo].[POCommitment]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PODetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__Amoun__1F869B8B] DEFAULT ((0)),
[Billable] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POCommitm__Billa__207ABFC4] DEFAULT ('N'),
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BillExt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__BillE__216EE3FD] DEFAULT ((0)),
[SuppressBilling] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POCommitm__Suppr__22630836] DEFAULT ('N'),
[VoucherAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__Vouch__23572C6F] DEFAULT ((0)),
[TransactionCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__Trans__244B50A8] DEFAULT ((0)),
[AmountProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__Amoun__253F74E1] DEFAULT ((0)),
[CurrencyExchangeOverrideRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__Curre__2633991A] DEFAULT ((0)),
[PaymentAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__Payme__2727BD53] DEFAULT ((0)),
[TransactionTaxAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__Trans__281BE18C] DEFAULT ((0)),
[TransactionShipAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__Trans__291005C5] DEFAULT ((0)),
[TransactionMiscAmount1] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__Trans__2A0429FE] DEFAULT ((0)),
[TransactionMiscAmount2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__Trans__2AF84E37] DEFAULT ((0)),
[TransactionMiscAmount3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__Trans__2BEC7270] DEFAULT ((0)),
[TransactionMiscAmount4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__Trans__2CE096A9] DEFAULT ((0)),
[TransactionMiscAmount5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__Trans__2DD4BAE2] DEFAULT ((0)),
[TaxPaymentAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__TaxPa__2EC8DF1B] DEFAULT ((0)),
[ShipPaymentAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__ShipP__2FBD0354] DEFAULT ((0)),
[MiscPaymentAmount1] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__MiscP__30B1278D] DEFAULT ((0)),
[MiscPaymentAmount2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__MiscP__31A54BC6] DEFAULT ((0)),
[MiscPaymentAmount3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__MiscP__32996FFF] DEFAULT ((0)),
[MiscPaymentAmount4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__MiscP__338D9438] DEFAULT ((0)),
[MiscPaymentAmount5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__MiscP__3481B871] DEFAULT ((0)),
[TransactionTax2Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__Trans__3575DCAA] DEFAULT ((0)),
[Tax2PaymentAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POCommitm__Tax2P__366A00E3] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POCommitment] ADD CONSTRAINT [POCommitmentPK] PRIMARY KEY NONCLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [POCommitmentRP1IDX] ON [dbo].[POCommitment] ([MasterPKey], [PODetailPKey], [WBS1], [WBS2], [WBS3], [Account]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [POCommitmentPODetailPKeyIDX] ON [dbo].[POCommitment] ([PODetailPKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [POCommitmentWBSIDX] ON [dbo].[POCommitment] ([WBS1], [WBS2], [WBS3]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
