CREATE TABLE [dbo].[FW_ReportPrinterPaperTrays]
(
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrinterName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RawKind] [int] NOT NULL CONSTRAINT [DF__FW_Report__RawKi__00380D64] DEFAULT ((0)),
[PaperTrayName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_ReportPrinterPaperTrays] ADD CONSTRAINT [FW_ReportPrinterPaperTraysPK] PRIMARY KEY NONCLUSTERED ([UICultureName], [PrinterName], [RawKind]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
