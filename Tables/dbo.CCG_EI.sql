CREATE TABLE [dbo].[CCG_EI]
(
[WBS1] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InvoiceStage] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateChanged] [datetime] NOT NULL,
[ChangedBy] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateLastNotificationSent] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI] ADD CONSTRAINT [PK_CCG_EI] PRIMARY KEY CLUSTERED ([WBS1]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
