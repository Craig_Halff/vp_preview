CREATE TABLE [dbo].[LedgerARTax]
(
[Period] [int] NOT NULL CONSTRAINT [DF__LedgerART__Perio__6D453481] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__LedgerART__PostS__6E3958BA] DEFAULT ((0)),
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReverseCharge] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerART__Rever__6F2D7CF3] DEFAULT ('N'),
[Seq] [smallint] NOT NULL CONSTRAINT [DF__LedgerARTax__Seq__7021A12C] DEFAULT ((0)),
[TaxAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerART__TaxAm__7115C565] DEFAULT ((0)),
[TaxCBAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerART__TaxCB__7209E99E] DEFAULT ((0)),
[CompoundOnTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerART__Amoun__72FE0DD7] DEFAULT ((0)),
[CBAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerART__CBAmo__73F23210] DEFAULT ((0)),
[TaxAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerART__TaxAm__74E65649] DEFAULT ((0)),
[TaxCBAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerART__TaxCB__75DA7A82] DEFAULT ((0)),
[TaxAmountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerART__TaxAm__76CE9EBB] DEFAULT ((0)),
[TaxCBAmountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerART__TaxCB__77C2C2F4] DEFAULT ((0)),
[TaxAmountSourceCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerART__TaxAm__78B6E72D] DEFAULT ((0)),
[NonRecoverTaxPercent] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerART__NonRe__79AB0B66] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LedgerARTax] ADD CONSTRAINT [LedgerARTaxPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [PKey], [TaxCode], [ReverseCharge]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
