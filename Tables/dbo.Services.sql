CREATE TABLE [dbo].[Services]
(
[Service_Category] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Service_SubCategory] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SF330Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
