CREATE TABLE [dbo].[HAI_OpportunityStages]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Code] [int] NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Order] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HAI_OpportunityStages] ADD CONSTRAINT [PK_HAI_OpportunityStages] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
