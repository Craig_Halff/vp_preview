CREATE TABLE [dbo].[RPCompensationFee]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPCompens__Perio__448E0308] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPCompens__Perio__45822741] DEFAULT ((0)),
[PeriodDirLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPCompens__Perio__46764B7A] DEFAULT ((0)),
[PeriodDirExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPCompens__Perio__476A6FB3] DEFAULT ((0)),
[PeriodDirLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPCompens__Perio__485E93EC] DEFAULT ((0)),
[PeriodDirExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPCompens__Perio__4952B825] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPCompensationFee] ADD CONSTRAINT [RPCompensationFeePK] PRIMARY KEY NONCLUSTERED ([TimePhaseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPCompensationFeePTIDX] ON [dbo].[RPCompensationFee] ([PlanID], [TaskID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPCompensationFeeAllIDX] ON [dbo].[RPCompensationFee] ([PlanID], [TimePhaseID], [TaskID], [StartDate], [EndDate], [PeriodCost], [PeriodBill]) ON [PRIMARY]
GO
