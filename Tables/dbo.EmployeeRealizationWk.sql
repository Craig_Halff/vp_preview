CREATE TABLE [dbo].[EmployeeRealizationWk]
(
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BillingCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hours] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EmployeeR__Hours__1BF53672] DEFAULT ((0)),
[CostAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EmployeeR__CostA__1CE95AAB] DEFAULT ((0)),
[BillingAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EmployeeR__Billi__1DDD7EE4] DEFAULT ((0)),
[UnitRealization] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EmployeeR__UnitR__1ED1A31D] DEFAULT ((0)),
[LaborRealization] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EmployeeR__Labor__1FC5C756] DEFAULT ((0)),
[TotalRealization] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EmployeeR__Total__20B9EB8F] DEFAULT ((0)),
[WBS3Hours] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EmployeeR__WBS3H__21AE0FC8] DEFAULT ((0)),
[WBS3CostAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EmployeeR__WBS3C__22A23401] DEFAULT ((0)),
[WBS3BillingAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EmployeeR__WBS3B__2396583A] DEFAULT ((0)),
[WBS3LaborRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EmployeeR__WBS3L__248A7C73] DEFAULT ((0)),
[WBS3Revenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EmployeeR__WBS3R__257EA0AC] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EmployeeRealizationWk] ADD CONSTRAINT [EmployeeRealizationWkPK] PRIMARY KEY NONCLUSTERED ([PKey], [Employee], [WBS1], [WBS2], [WBS3]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
