CREATE TABLE [dbo].[VisionImportAccountBalance_GL]
(
[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Org] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [int] NULL,
[TransDate] [datetime] NULL,
[RefNo] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccrualDebit] [decimal] (19, 4) NULL,
[AccrualCredit] [decimal] (19, 4) NULL,
[CashDebit] [decimal] (19, 4) NULL,
[CashCredit] [decimal] (19, 4) NULL,
[LinkCompany] [varchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
