CREATE TABLE [dbo].[ekDetailTax]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportDate] [datetime] NOT NULL,
[ReportName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__ekDetailTax__Seq__53256FCB] DEFAULT ((0)),
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ekDetailT__TaxAm__54199404] DEFAULT ((0)),
[CompoundOnTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NOT NULL CONSTRAINT [DF__ekDetailT__SortO__550DB83D] DEFAULT ((0)),
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ekDetailT__Maste__03EB04BC] DEFAULT (left(replace(newid(),'-',''),(32))),
[DetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ekDetailT__Detai__04DF28F5] DEFAULT (left(replace(newid(),'-',''),(32)))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ekDetailTax] ADD CONSTRAINT [ekDetailTaxPK] PRIMARY KEY NONCLUSTERED ([Employee], [ReportDate], [ReportName], [Seq], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [EKDetailTaxPKeyIDX] ON [dbo].[ekDetailTax] ([MasterPKey], [DetailPKey], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
