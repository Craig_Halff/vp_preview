CREATE TABLE [dbo].[CFGNAICSDescriptions]
(
[Code] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGNAICSDes__Seq__06651CAF] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGNAICSDescriptions] ADD CONSTRAINT [CFGNAICSDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
