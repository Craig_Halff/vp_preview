CREATE TABLE [dbo].[CFGProjectMilestoneDescriptions]
(
[Code] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGProjectM__Seq__42D16E76] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGProjectMilestoneDescriptions] ADD CONSTRAINT [CFGProjectMilestoneDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
