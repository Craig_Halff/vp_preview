CREATE TABLE [dbo].[CFGCurrencyDownloadLicense]
(
[LicenseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LicenseKey] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LicenseDescription] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LicensePassword] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCurrencyDownloadLicense] ADD CONSTRAINT [CFGCurrencyDownloadLicensePK] PRIMARY KEY CLUSTERED ([LicenseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
