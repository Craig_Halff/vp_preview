CREATE TABLE [dbo].[CustomIntegrationTask]
(
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomIntegrationID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [int] NOT NULL,
[DataSourceID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InfoCenter] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomIntegrationTask] ADD CONSTRAINT [CustomIntegrationTaskPK] PRIMARY KEY CLUSTERED ([TaskID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
