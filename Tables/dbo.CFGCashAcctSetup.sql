CREATE TABLE [dbo].[CFGCashAcctSetup]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EndAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCashAcctSetup] ADD CONSTRAINT [CFGCashAcctSetupPK] PRIMARY KEY NONCLUSTERED ([Company], [Label], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
