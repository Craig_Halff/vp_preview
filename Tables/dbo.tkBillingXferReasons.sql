CREATE TABLE [dbo].[tkBillingXferReasons]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__tkBillingXf__Seq__1FD17FA5] DEFAULT ((0)),
[Reason] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tkBillingXferReasons] ADD CONSTRAINT [tkBillingXferReasonsPK] PRIMARY KEY NONCLUSTERED ([Company], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
