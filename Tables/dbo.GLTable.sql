CREATE TABLE [dbo].[GLTable]
(
[TableNo] [smallint] NOT NULL CONSTRAINT [DF__GLTable_N__Table__2199012F] DEFAULT ((0)),
[TableName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GLTable] ADD CONSTRAINT [GLTablePK] PRIMARY KEY CLUSTERED ([TableNo]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
