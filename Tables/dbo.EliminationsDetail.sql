CREATE TABLE [dbo].[EliminationsDetail]
(
[ReportingGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__Eliminati__Perio__65442006] DEFAULT ((0)),
[SourceTable] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourcePostSeq] [int] NOT NULL CONSTRAINT [DF__Eliminati__Sourc__6638443F] DEFAULT ((0)),
[SourcePKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EliminationsDetail] ADD CONSTRAINT [EliminationsDetailPK] PRIMARY KEY NONCLUSTERED ([ReportingGroup], [Company], [Period], [SourceTable], [SourcePostSeq], [SourcePKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
