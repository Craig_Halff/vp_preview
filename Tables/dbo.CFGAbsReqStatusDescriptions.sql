CREATE TABLE [dbo].[CFGAbsReqStatusDescriptions]
(
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAbsReqStatusDescriptions] ADD CONSTRAINT [CFGAbsReqStatusDescriptionsPK] PRIMARY KEY CLUSTERED ([Status], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
