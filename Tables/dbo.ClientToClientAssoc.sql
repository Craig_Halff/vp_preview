CREATE TABLE [dbo].[ClientToClientAssoc]
(
[FromClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ToClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Relationship] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_ClientToClientAssoc]
      ON [dbo].[ClientToClientAssoc]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ClientToClientAssoc'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToClientID],121),'FromClientID',CONVERT(NVARCHAR(2000),DELETED.[FromClientID],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join CL as oldDesc  on DELETED.FromClientID = oldDesc.ClientID

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToClientID],121),'ToClientID',CONVERT(NVARCHAR(2000),DELETED.[ToClientID],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join CL as oldDesc  on DELETED.ToClientID = oldDesc.ClientID

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToClientID],121),'Relationship',CONVERT(NVARCHAR(2000),DELETED.[Relationship],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGClientRelationship as oldDesc  on DELETED.Relationship = oldDesc.Code

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromClientID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToClientID],121),'Description','[text]',NULL, @source,@app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_ClientToClientAssoc]
      ON [dbo].[ClientToClientAssoc]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ClientToClientAssoc'
    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToClientID],121),'FromClientID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[FromClientID],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  CL as newDesc  on INSERTED.FromClientID = newDesc.ClientID

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToClientID],121),'ToClientID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[ToClientID],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  CL as newDesc  on INSERTED.ToClientID = newDesc.ClientID

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToClientID],121),'Relationship',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Relationship],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGClientRelationship as newDesc  on INSERTED.Relationship = newDesc.Code

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToClientID],121),'Description',NULL,'[text]', @source, @app
      FROM INSERTED


    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_ClientToClientAssoc]
      ON [dbo].[ClientToClientAssoc]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ClientToClientAssoc'
    
     If UPDATE([FromClientID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToClientID],121),'FromClientID',
     CONVERT(NVARCHAR(2000),DELETED.[FromClientID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[FromClientID],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[FromClientID] = DELETED.[FromClientID] AND INSERTED.[ToClientID] = DELETED.[ToClientID] AND 
		(
			(
				INSERTED.[FromClientID] Is Null And
				DELETED.[FromClientID] Is Not Null
			) Or
			(
				INSERTED.[FromClientID] Is Not Null And
				DELETED.[FromClientID] Is Null
			) Or
			(
				INSERTED.[FromClientID] !=
				DELETED.[FromClientID]
			)
		) left join CL as oldDesc  on DELETED.FromClientID = oldDesc.ClientID  left join  CL as newDesc  on INSERTED.FromClientID = newDesc.ClientID
		END		
		
     If UPDATE([ToClientID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToClientID],121),'ToClientID',
     CONVERT(NVARCHAR(2000),DELETED.[ToClientID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[ToClientID],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[FromClientID] = DELETED.[FromClientID] AND INSERTED.[ToClientID] = DELETED.[ToClientID] AND 
		(
			(
				INSERTED.[ToClientID] Is Null And
				DELETED.[ToClientID] Is Not Null
			) Or
			(
				INSERTED.[ToClientID] Is Not Null And
				DELETED.[ToClientID] Is Null
			) Or
			(
				INSERTED.[ToClientID] !=
				DELETED.[ToClientID]
			)
		) left join CL as oldDesc  on DELETED.ToClientID = oldDesc.ClientID  left join  CL as newDesc  on INSERTED.ToClientID = newDesc.ClientID
		END		
		
     If UPDATE([Relationship])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToClientID],121),'Relationship',
     CONVERT(NVARCHAR(2000),DELETED.[Relationship],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Relationship],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[FromClientID] = DELETED.[FromClientID] AND INSERTED.[ToClientID] = DELETED.[ToClientID] AND 
		(
			(
				INSERTED.[Relationship] Is Null And
				DELETED.[Relationship] Is Not Null
			) Or
			(
				INSERTED.[Relationship] Is Not Null And
				DELETED.[Relationship] Is Null
			) Or
			(
				INSERTED.[Relationship] !=
				DELETED.[Relationship]
			)
		) left join CFGClientRelationship as oldDesc  on DELETED.Relationship = oldDesc.Code  left join  CFGClientRelationship as newDesc  on INSERTED.Relationship = newDesc.Code
		END		
		
      If UPDATE([Description])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromClientID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToClientID],121),'Description',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[ClientToClientAssoc] ADD CONSTRAINT [ClientToClientAssocPK] PRIMARY KEY NONCLUSTERED ([FromClientID], [ToClientID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
