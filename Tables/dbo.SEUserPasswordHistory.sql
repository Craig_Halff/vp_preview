CREATE TABLE [dbo].[SEUserPasswordHistory]
(
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateDate] [datetime] NOT NULL CONSTRAINT [DF__SEUserPas__Creat__6C86E5C9] DEFAULT (getutcdate()),
[Password] [nvarchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SEUserPasswordHistory] ADD CONSTRAINT [SEUserPasswordHistoryPK] PRIMARY KEY NONCLUSTERED ([Username], [CreateDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
