CREATE TABLE [dbo].[CFGICBillingTerms]
(
[ICBillingInvoiceCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ICBillingVoucherCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ICBillingInvoiceWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingInvoiceWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingInvoiceWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingInvoiceTaxCode1] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingInvoiceTaxCode2] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingInvoiceTaxCode3] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingLabShowComment] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGICBill__ICBil__069A26D9] DEFAULT ('N'),
[ICBillingLabShowDate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGICBill__ICBil__078E4B12] DEFAULT ('N'),
[ICBillingLabShowMult] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGICBill__ICBil__08826F4B] DEFAULT ('N'),
[ICBillingExpDetail] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGICBill__ICBil__09769384] DEFAULT ('N'),
[ICBillingExpShowMult] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGICBill__ICBil__0A6AB7BD] DEFAULT ('N'),
[ICBillingVoucherWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingVoucherWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingVoucherWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingVoucherExpenseAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingVoucherBSOtherAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingVoucherTaxCode1] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingVoucherTaxCode2] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingVoucherTaxCode3] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingVoucherLiabCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingVoucherBankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingVoucherOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__CFGICBill__ICBil__0B5EDBF6] DEFAULT ((0)),
[ICBillingInvoiceTemplateName] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGICBillingTerms] ADD CONSTRAINT [CFGICBillingTermsPK] PRIMARY KEY CLUSTERED ([ICBillingInvoiceCompany], [ICBillingVoucherCompany]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
