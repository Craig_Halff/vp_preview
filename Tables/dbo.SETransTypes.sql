CREATE TABLE [dbo].[SETransTypes]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SETransTyp__PKey__493DA98C] DEFAULT (left(replace(newid(),'-',''),(32))),
[Role] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransType] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Entry] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SETransTy__Entry__4A31CDC5] DEFAULT ('N'),
[Lists] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SETransTy__Lists__4B25F1FE] DEFAULT ('N'),
[Post] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SETransTyp__Post__4C1A1637] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_SETransTypes]
      ON [dbo].[SETransTypes]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SETransTypes'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'PKey',CONVERT(NVARCHAR(2000),[PKey],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'Role',CONVERT(NVARCHAR(2000),[Role],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'TransType',CONVERT(NVARCHAR(2000),[TransType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'Company',CONVERT(NVARCHAR(2000),[Company],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'Entry',CONVERT(NVARCHAR(2000),[Entry],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'Lists',CONVERT(NVARCHAR(2000),[Lists],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'Post',CONVERT(NVARCHAR(2000),[Post],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_SETransTypes]
      ON [dbo].[SETransTypes]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SETransTypes'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'PKey',NULL,CONVERT(NVARCHAR(2000),[PKey],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'Role',NULL,CONVERT(NVARCHAR(2000),[Role],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'TransType',NULL,CONVERT(NVARCHAR(2000),[TransType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'Company',NULL,CONVERT(NVARCHAR(2000),[Company],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'Entry',NULL,CONVERT(NVARCHAR(2000),[Entry],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'Lists',NULL,CONVERT(NVARCHAR(2000),[Lists],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'Post',NULL,CONVERT(NVARCHAR(2000),[Post],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_SETransTypes]
      ON [dbo].[SETransTypes]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'SETransTypes'
    
      If UPDATE([PKey])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'PKey',
      CONVERT(NVARCHAR(2000),DELETED.[PKey],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PKey],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[PKey] Is Null And
				DELETED.[PKey] Is Not Null
			) Or
			(
				INSERTED.[PKey] Is Not Null And
				DELETED.[PKey] Is Null
			) Or
			(
				INSERTED.[PKey] !=
				DELETED.[PKey]
			)
		) 
		END		
		
      If UPDATE([Role])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'Role',
      CONVERT(NVARCHAR(2000),DELETED.[Role],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Role],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[Role] Is Null And
				DELETED.[Role] Is Not Null
			) Or
			(
				INSERTED.[Role] Is Not Null And
				DELETED.[Role] Is Null
			) Or
			(
				INSERTED.[Role] !=
				DELETED.[Role]
			)
		) 
		END		
		
      If UPDATE([TransType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'TransType',
      CONVERT(NVARCHAR(2000),DELETED.[TransType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TransType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[TransType] Is Null And
				DELETED.[TransType] Is Not Null
			) Or
			(
				INSERTED.[TransType] Is Not Null And
				DELETED.[TransType] Is Null
			) Or
			(
				INSERTED.[TransType] !=
				DELETED.[TransType]
			)
		) 
		END		
		
      If UPDATE([Company])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'Company',
      CONVERT(NVARCHAR(2000),DELETED.[Company],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Company],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[Company] Is Null And
				DELETED.[Company] Is Not Null
			) Or
			(
				INSERTED.[Company] Is Not Null And
				DELETED.[Company] Is Null
			) Or
			(
				INSERTED.[Company] !=
				DELETED.[Company]
			)
		) 
		END		
		
      If UPDATE([Entry])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'Entry',
      CONVERT(NVARCHAR(2000),DELETED.[Entry],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Entry],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[Entry] Is Null And
				DELETED.[Entry] Is Not Null
			) Or
			(
				INSERTED.[Entry] Is Not Null And
				DELETED.[Entry] Is Null
			) Or
			(
				INSERTED.[Entry] !=
				DELETED.[Entry]
			)
		) 
		END		
		
      If UPDATE([Lists])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'Lists',
      CONVERT(NVARCHAR(2000),DELETED.[Lists],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Lists],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[Lists] Is Null And
				DELETED.[Lists] Is Not Null
			) Or
			(
				INSERTED.[Lists] Is Not Null And
				DELETED.[Lists] Is Null
			) Or
			(
				INSERTED.[Lists] !=
				DELETED.[Lists]
			)
		) 
		END		
		
      If UPDATE([Post])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'Post',
      CONVERT(NVARCHAR(2000),DELETED.[Post],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Post],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[Post] Is Null And
				DELETED.[Post] Is Not Null
			) Or
			(
				INSERTED.[Post] Is Not Null And
				DELETED.[Post] Is Null
			) Or
			(
				INSERTED.[Post] !=
				DELETED.[Post]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[SETransTypes] ADD CONSTRAINT [SETransTypesPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [SETransTypesRoleTransTypeCompanyIDX] ON [dbo].[SETransTypes] ([Role], [TransType], [Company]) ON [PRIMARY]
GO
