CREATE TABLE [dbo].[CFGServProCostGroup]
(
[ServProCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CostGroupCode] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Disabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGServPr__Disab__5CC3E89E] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGServProCostGroup] ADD CONSTRAINT [CFGServProCostGroupPK] PRIMARY KEY CLUSTERED ([ServProCode], [CostGroupCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
