CREATE TABLE [dbo].[CFGTransAutoNumType]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NextRefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartVoucher] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGTransA__Start__375D59C5] DEFAULT ((0)),
[EndVoucher] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGTransA__EndVo__38517DFE] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGTransAutoNumType] ADD CONSTRAINT [CFGTransAutoNumTypePK] PRIMARY KEY CLUSTERED ([Company], [TransType]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
