CREATE TABLE [dbo].[CFGCompetitionTypeData]
(
[Code] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCompetitionTypeData] ADD CONSTRAINT [CFGCompetitionTypeDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
