CREATE TABLE [dbo].[OpportunityProjectCodes_Backup]
(
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProjectCode] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Fee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportunity__Fee__6D9A3A3C] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OpportunityProjectCodes_Backup] ADD CONSTRAINT [OpportunityProjectCodesPK] PRIMARY KEY NONCLUSTERED ([OpportunityID], [ProjectCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
