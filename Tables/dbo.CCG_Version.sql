CREATE TABLE [dbo].[CCG_Version]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[Product] [varchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DatabaseVersion] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoftwareVersion] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InstallDate] [datetime] NULL CONSTRAINT [DF__CCG_Versi__Insta__5ED6E7CF] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_Version] ADD CONSTRAINT [PK_CCG_VERSION] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
