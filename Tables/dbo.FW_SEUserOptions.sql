CREATE TABLE [dbo].[FW_SEUserOptions]
(
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OptionName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OptionValue] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_SEUserOptions] ADD CONSTRAINT [FW_SEUserOptionsPK] PRIMARY KEY NONCLUSTERED ([Username], [OptionName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
