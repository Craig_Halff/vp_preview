CREATE TABLE [dbo].[FW_ReportPrinters]
(
[PrinterName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Location] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Model] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrinterDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Scale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_Report__Scale__022055D6] DEFAULT ('N'),
[Collation] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_Report__Colla__03147A0F] DEFAULT ('N'),
[Color] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_Report__Color__04089E48] DEFAULT ('N'),
[PageSizeRawKind] [int] NOT NULL CONSTRAINT [DF__FW_Report__PageS__04FCC281] DEFAULT ((0)),
[PaperTrayRawKind] [int] NOT NULL CONSTRAINT [DF__FW_Report__Paper__05F0E6BA] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_ReportPrinters] ADD CONSTRAINT [FW_ReportPrintersPK] PRIMARY KEY CLUSTERED ([PrinterName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
