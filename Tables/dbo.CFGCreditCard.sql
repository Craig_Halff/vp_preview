CREATE TABLE [dbo].[CFGCreditCard]
(
[PrimaryCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGCredit__Statu__07F85F66] DEFAULT ('A'),
[SecondaryIdentifier] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImportCreditCardCharges] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGCredit__Impor__08EC839F] DEFAULT ('Y'),
[EmployeePaid] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGCredit__Emplo__09E0A7D8] DEFAULT ('N'),
[MapToDescription] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCreditCard] ADD CONSTRAINT [CFGCreditCardPK] PRIMARY KEY CLUSTERED ([PrimaryCode], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
