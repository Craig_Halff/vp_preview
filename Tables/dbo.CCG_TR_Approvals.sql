CREATE TABLE [dbo].[CCG_TR_Approvals]
(
[TableName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL,
[PostSeq] [int] NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SubmittedBy] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedDate] [datetime] NULL,
[ApprovedBy] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovedDate] [datetime] NULL,
[ApprovalComment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncludeFuture] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillNonBillable] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModificationComment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FNStatus] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_TR_Approvals] ADD CONSTRAINT [PK_CCG_TR_Approvals] PRIMARY KEY CLUSTERED ([TableName], [Period], [PostSeq], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
