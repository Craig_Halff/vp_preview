CREATE TABLE [dbo].[SyncActivityLog]
(
[ActivityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Owner] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SyncActivityLog] ADD CONSTRAINT [SyncActivityLogPK] PRIMARY KEY CLUSTERED ([ActivityID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [SyncActivityLogCreateDateIDX] ON [dbo].[SyncActivityLog] ([CreateDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [SyncActivityLogOwnerIDX] ON [dbo].[SyncActivityLog] ([Owner]) ON [PRIMARY]
GO
