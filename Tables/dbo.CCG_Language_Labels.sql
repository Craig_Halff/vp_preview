CREATE TABLE [dbo].[CCG_Language_Labels]
(
[Group1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Id] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Product] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_Language_Labels] ADD CONSTRAINT [PK_CCG_Language_Labels] PRIMARY KEY NONCLUSTERED ([Id], [Product], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
