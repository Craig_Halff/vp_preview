CREATE TABLE [dbo].[CCG_TR_Config]
(
[Version] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WebServiceEnabled] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WebServiceApproach] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WebServiceAsync] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WebServiceUrl] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WebServiceUsername] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WebServiceDatabaseDesc] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NWCredentialsUsername] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NWCredentialsPassword] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NWCredentialsDomain] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
