CREATE TABLE [dbo].[LedgerEXTax]
(
[Period] [int] NOT NULL CONSTRAINT [DF__LedgerEXT__Perio__295A1E88] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__LedgerEXT__PostS__2A4E42C1] DEFAULT ((0)),
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReverseCharge] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerEXT__Rever__2B4266FA] DEFAULT ('N'),
[Seq] [smallint] NOT NULL CONSTRAINT [DF__LedgerEXTax__Seq__2C368B33] DEFAULT ((0)),
[TaxAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEXT__TaxAm__2D2AAF6C] DEFAULT ((0)),
[TaxCBAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEXT__TaxCB__2E1ED3A5] DEFAULT ((0)),
[CompoundOnTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEXT__Amoun__2F12F7DE] DEFAULT ((0)),
[CBAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEXT__CBAmo__30071C17] DEFAULT ((0)),
[TaxAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEXT__TaxAm__30FB4050] DEFAULT ((0)),
[TaxCBAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEXT__TaxCB__31EF6489] DEFAULT ((0)),
[TaxAmountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEXT__TaxAm__32E388C2] DEFAULT ((0)),
[TaxCBAmountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEXT__TaxCB__33D7ACFB] DEFAULT ((0)),
[TaxAmountSourceCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEXT__TaxAm__34CBD134] DEFAULT ((0)),
[NonRecoverTaxPercent] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEXT__NonRe__35BFF56D] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LedgerEXTax] ADD CONSTRAINT [LedgerEXTaxPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [PKey], [TaxCode], [ReverseCharge]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
