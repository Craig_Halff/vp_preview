CREATE TABLE [dbo].[ARC]
(
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommentDate] [datetime] NOT NULL,
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditMemoRefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CommentType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ARC] ADD CONSTRAINT [ARCPK] PRIMARY KEY NONCLUSTERED ([ClientID], [CommentDate], [Username]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ARCWBS1InvoiceIDX] ON [dbo].[ARC] ([WBS1], [Invoice]) ON [PRIMARY]
GO
