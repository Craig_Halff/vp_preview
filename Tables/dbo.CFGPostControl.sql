CREATE TABLE [dbo].[CFGPostControl]
(
[Period] [int] NOT NULL CONSTRAINT [DF__CFGPostCo__Perio__0077392F] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__CFGPostCo__PostS__016B5D68] DEFAULT ((0)),
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostDate] [datetime] NULL,
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Completed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPostCo__Compl__025F81A1] DEFAULT ('N'),
[Canceled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPostCo__Cance__0353A5DA] DEFAULT ('N'),
[PostComment] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecordCount] [int] NOT NULL CONSTRAINT [DF__CFGPostCo__Recor__0447CA13] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PaymentDate] [datetime] NULL,
[CheckDate] [datetime] NULL,
[W2Year] [smallint] NOT NULL CONSTRAINT [DF__CFGPostCo__W2Yea__053BEE4C] DEFAULT ((0)),
[W2Quarter] [smallint] NOT NULL CONSTRAINT [DF__CFGPostCo__W2Qua__06301285] DEFAULT ((0)),
[BenefitAccrualYear] [smallint] NOT NULL CONSTRAINT [DF__CFGPostCo__Benef__072436BE] DEFAULT ((0)),
[PayrollRunType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QueueName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADPExportStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XChargeStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentProcessRun] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Removed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPostCo__Remov__08185AF7] DEFAULT ('N'),
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ASJCStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EFTStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccrualOnHourWorkedStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPostControl] ADD CONSTRAINT [CFGPostControlPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
