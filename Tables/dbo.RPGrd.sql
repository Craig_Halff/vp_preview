CREATE TABLE [dbo].[RPGrd]
(
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VisibleFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGrd_New__Visib__11CD7911] DEFAULT ('Y'),
[GrdSeq] [smallint] NOT NULL CONSTRAINT [DF__RPGrd_New__GrdSe__12C19D4A] DEFAULT ((0)),
[DivID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentDivID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPGrd] ADD CONSTRAINT [RPGrdPK] PRIMARY KEY NONCLUSTERED ([UserName], [GridName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
