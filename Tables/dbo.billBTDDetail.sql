CREATE TABLE [dbo].[billBTDDetail]
(
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecdSeq] [int] NOT NULL CONSTRAINT [DF__billBTDDe__RecdS__56E026BF] DEFAULT ((0)),
[BillWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billBTDDeta__Fee__57D44AF8] DEFAULT ((0)),
[ReimbAllow] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billBTDDe__Reimb__58C86F31] DEFAULT ((0)),
[ConsultFee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billBTDDe__Consu__59BC936A] DEFAULT ((0)),
[Description] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billBTDDe__Curre__5AB0B7A3] DEFAULT ((0)),
[PriorAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billBTDDe__Prior__5BA4DBDC] DEFAULT ((0)),
[Total] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billBTDDe__Total__5C990015] DEFAULT ((0)),
[Section] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReceivedAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billBTDDe__Recei__5D8D244E] DEFAULT ((0)),
[ARBalanceAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billBTDDe__ARBal__5E814887] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[billBTDDetail] ADD CONSTRAINT [billBTDDetailPK] PRIMARY KEY NONCLUSTERED ([Invoice], [MainWBS1], [RecdSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
