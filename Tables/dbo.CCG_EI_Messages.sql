CREATE TABLE [dbo].[CCG_EI_Messages]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[WBS1] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateEntered] [datetime] NOT NULL,
[EnteredBy] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Role] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_Messages] ADD CONSTRAINT [PK_CCG_EI_Messages] PRIMARY KEY CLUSTERED ([Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Message data for EI project specific messaging interface', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Messages', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date and time of the message', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Messages', 'COLUMN', N'DateEntered'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Employee ID who entered the message', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Messages', 'COLUMN', N'EnteredBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Message text', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Messages', 'COLUMN', N'Message'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*EI role of who entered the message', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Messages', 'COLUMN', N'Role'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Messages', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The WBS1 number (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Messages', 'COLUMN', N'WBS1'
GO
