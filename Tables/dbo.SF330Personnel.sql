CREATE TABLE [dbo].[SF330Personnel]
(
[SF330ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmpID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmpSeq] [smallint] NOT NULL CONSTRAINT [DF__SF330Pers__EmpSe__53862DD5] DEFAULT ((0)),
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearsTotal] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearsThisFirm] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoleCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Role] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Education] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Registration] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OtherProfQual] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF330Personnel] ADD CONSTRAINT [SF330PersonnelPK] PRIMARY KEY NONCLUSTERED ([SF330ID], [EmpID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
