CREATE TABLE [dbo].[CFGInvApprover]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AutoAddItem] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInvApp__AutoA__27FB1AA4] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGInvApprover] ADD CONSTRAINT [CFGInvApproverPK] PRIMARY KEY CLUSTERED ([Company], [Employee]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
