CREATE TABLE [dbo].[FW_CFGAttachmentCategoryData]
(
[Code] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Application] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGAttachmentCategoryData] ADD CONSTRAINT [FW_CFGAttachmentCategoryDataPK] PRIMARY KEY NONCLUSTERED ([Code], [Application]) ON [PRIMARY]
GO
