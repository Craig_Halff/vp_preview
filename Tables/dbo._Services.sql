CREATE TABLE [dbo].[_Services]
(
[Service_Category] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Service_SubCategory] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SF330Code] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL
) ON [PRIMARY]
GO
