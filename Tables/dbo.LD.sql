CREATE TABLE [dbo].[LD]
(
[Period] [int] NOT NULL CONSTRAINT [DF__LD_New__Period__4C194AE0] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__LD_New__PostSeq__4D0D6F19] DEFAULT ((0)),
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LaborCode] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransDate] [datetime] NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__RegHrs__4E019352] DEFAULT ((0)),
[OvtHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__OvtHrs__4EF5B78B] DEFAULT ((0)),
[RegAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__RegAmt__4FE9DBC4] DEFAULT ((0)),
[OvtAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__OvtAmt__50DDFFFD] DEFAULT ((0)),
[BillExt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__BillExt__51D22436] DEFAULT ((0)),
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__Rate__52C6486F] DEFAULT ((0)),
[OvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__OvtPct__53BA6CA8] DEFAULT ((0)),
[OvtRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__OvtRate__54AE90E1] DEFAULT ((0)),
[EmType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Pool] [smallint] NOT NULL CONSTRAINT [DF__LD_New__Pool__55A2B51A] DEFAULT ((0)),
[Category] [smallint] NOT NULL CONSTRAINT [DF__LD_New__Category__5696D953] DEFAULT ((0)),
[EmOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DebitLedgerMiscPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditLedgerMiscPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuppressBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LD_New__Suppress__578AFD8C] DEFAULT ('N'),
[BillStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledInvoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledPeriod] [int] NOT NULL CONSTRAINT [DF__LD_New__BilledPe__587F21C5] DEFAULT ((0)),
[XferWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferLaborCode] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LD_New__ProjectC__597345FE] DEFAULT ('N'),
[BillTaxCodeOverride] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpecialOvtHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__SpecialO__5A676A37] DEFAULT ((0)),
[SpecialOvtAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__SpecialO__5B5B8E70] DEFAULT ((0)),
[SpecialOvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__SpecialO__5C4FB2A9] DEFAULT ((0)),
[SpecialOvtRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__SpecialO__5D43D6E2] DEFAULT ((0)),
[SelPeriod] [int] NOT NULL CONSTRAINT [DF__LD_New__SelPerio__5E37FB1B] DEFAULT ((0)),
[SelPostSeq] [int] NOT NULL CONSTRAINT [DF__LD_New__SelPostS__5F2C1F54] DEFAULT ((0)),
[Payrate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__Payrate__6020438D] DEFAULT ((0)),
[PayOvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__PayOvtPc__611467C6] DEFAULT ((0)),
[PaySpecialOvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__PaySpeci__62088BFF] DEFAULT ((0)),
[SelOvtPeriod] [int] NOT NULL CONSTRAINT [DF__LD_New__SelOvtPe__62FCB038] DEFAULT ((0)),
[SelOvtPostSeq] [int] NOT NULL CONSTRAINT [DF__LD_New__SelOvtPo__63F0D471] DEFAULT ((0)),
[WrittenOffPeriod] [int] NOT NULL CONSTRAINT [DF__LD_New__WrittenO__64E4F8AA] DEFAULT ((0)),
[Locale] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegAmtProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__RegAmtPr__65D91CE3] DEFAULT ((0)),
[OvtAmtProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__OvtAmtPr__66CD411C] DEFAULT ((0)),
[SpecialOvtAmtProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__SpecialO__67C16555] DEFAULT ((0)),
[RateProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__RateProj__68B5898E] DEFAULT ((0)),
[OvtRateProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__OvtRateP__69A9ADC7] DEFAULT ((0)),
[SpecialOvtRateProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__SpecialO__6A9DD200] DEFAULT ((0)),
[ProjectExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegAmtBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__RegAmtBi__6B91F639] DEFAULT ((0)),
[OvtAmtBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__OvtAmtBi__6C861A72] DEFAULT ((0)),
[SpecialOvtAmtBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__SpecialO__6D7A3EAB] DEFAULT ((0)),
[RateBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__RateBill__6E6E62E4] DEFAULT ((0)),
[OvtRateBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__OvtRateB__6F62871D] DEFAULT ((0)),
[SpecialOvtRateBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__SpecialO__7056AB56] DEFAULT ((0)),
[BillingExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegAmtEmployeeCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__RegAmtEm__714ACF8F] DEFAULT ((0)),
[OvtAmtEmployeeCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__OvtAmtEm__723EF3C8] DEFAULT ((0)),
[SpecialOvtAmtEmployeeCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__SpecialO__73331801] DEFAULT ((0)),
[RateEmployeeCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__RateEmpl__74273C3A] DEFAULT ((0)),
[OvtRateEmployeeCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__OvtRateE__751B6073] DEFAULT ((0)),
[SpecialOvtRateEmployeeCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__SpecialO__760F84AC] DEFAULT ((0)),
[ExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferCategory] [smallint] NOT NULL CONSTRAINT [DF__LD_New__XferCate__7703A8E5] DEFAULT ((0)),
[CostRateTableUsed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LD_New__CostRate__77F7CD1E] DEFAULT ('N'),
[BillTax2CodeOverride] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RealizationAmountEmployeeCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__Realizat__78EBF157] DEFAULT ((0)),
[RealizationAmountProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__Realizat__79E01590] DEFAULT ((0)),
[RealizationAmountBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LD_New__Realizat__7AD439C9] DEFAULT ((0)),
[NonBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__LD_New__NonBill__7BC85E02] DEFAULT ('N'),
[InvoiceStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimekeeperEndDate] [datetime] NULL,
[TransferredPeriod] [int] NOT NULL CONSTRAINT [DF__LD_New__Transfer__7CBC823B] DEFAULT ((0)),
[TransferredBillStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLInternalKey] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLProcessed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LD_New__TLProces__7DB0A674] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LD] ADD CONSTRAINT [LDPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LDBilledWBS1WBS2WBS3IDX] ON [dbo].[LD] ([BilledWBS1], [BilledWBS2], [BilledWBS3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LDBillStatusIDX] ON [dbo].[LD] ([BillStatus]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LDCategoryIDX] ON [dbo].[LD] ([Category]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LDEMOrgIDX] ON [dbo].[LD] ([EmOrg]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LDEmployeeIDX] ON [dbo].[LD] ([Employee]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LDLaborCodeIDX] ON [dbo].[LD] ([LaborCode]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LDSelOvtPeriodPostSeqEmployeeIDX] ON [dbo].[LD] ([SelOvtPeriod], [SelOvtPostSeq], [Employee]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LDSelPeriodPostSeqEmployeeIDX] ON [dbo].[LD] ([SelPeriod], [SelPostSeq], [Employee]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LDTransDateIDX] ON [dbo].[LD] ([TransDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LDPayrollPeriodIDX] ON [dbo].[LD] ([TransType], [TransDate], [ProjectCost], [Employee]) INCLUDE ([RegHrs], [OvtHrs], [SpecialOvtHrs], [SelPeriod], [SelPostSeq], [SelOvtPeriod], [SelOvtPostSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LDWBS1CategoryIDX] ON [dbo].[LD] ([WBS1], [Category]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LDWBS1LaborCodeIDX] ON [dbo].[LD] ([WBS1], [LaborCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LDWBS1TransDateIDX] ON [dbo].[LD] ([WBS1], [TransDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LDWBS1WBS2WBS3IDX] ON [dbo].[LD] ([WBS1], [WBS2], [WBS3]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LDXferWBS1WBS2WBS3IDX] ON [dbo].[LD] ([XferWBS1], [XferWBS2], [XferWBS3]) ON [PRIMARY]
GO
