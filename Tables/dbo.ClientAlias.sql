CREATE TABLE [dbo].[ClientAlias]
(
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Alias] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_ClientAlias]
      ON [dbo].[ClientAlias]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ClientAlias'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'PKey',CONVERT(NVARCHAR(2000),[PKey],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'ClientID',CONVERT(NVARCHAR(2000),[ClientID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[PKey],121),'Alias',CONVERT(NVARCHAR(2000),[Alias],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_ClientAlias]
      ON [dbo].[ClientAlias]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ClientAlias'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'PKey',NULL,CONVERT(NVARCHAR(2000),[PKey],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'ClientID',NULL,CONVERT(NVARCHAR(2000),[ClientID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'Alias',NULL,CONVERT(NVARCHAR(2000),[Alias],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_ClientAlias]
      ON [dbo].[ClientAlias]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ClientAlias'
    
      If UPDATE([PKey])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'PKey',
      CONVERT(NVARCHAR(2000),DELETED.[PKey],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PKey],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[PKey] Is Null And
				DELETED.[PKey] Is Not Null
			) Or
			(
				INSERTED.[PKey] Is Not Null And
				DELETED.[PKey] Is Null
			) Or
			(
				INSERTED.[PKey] !=
				DELETED.[PKey]
			)
		) 
		END		
		
      If UPDATE([ClientID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'ClientID',
      CONVERT(NVARCHAR(2000),DELETED.[ClientID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClientID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[ClientID] Is Null And
				DELETED.[ClientID] Is Not Null
			) Or
			(
				INSERTED.[ClientID] Is Not Null And
				DELETED.[ClientID] Is Null
			) Or
			(
				INSERTED.[ClientID] !=
				DELETED.[ClientID]
			)
		) 
		END		
		
      If UPDATE([Alias])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[PKey],121),'Alias',
      CONVERT(NVARCHAR(2000),DELETED.[Alias],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Alias],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[PKey] = DELETED.[PKey] AND 
		(
			(
				INSERTED.[Alias] Is Null And
				DELETED.[Alias] Is Not Null
			) Or
			(
				INSERTED.[Alias] Is Not Null And
				DELETED.[Alias] Is Null
			) Or
			(
				INSERTED.[Alias] !=
				DELETED.[Alias]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[ClientAlias] ADD CONSTRAINT [ClientAliasPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [ClientAliasIDX] ON [dbo].[ClientAlias] ([ClientID], [Alias]) ON [PRIMARY]
GO
