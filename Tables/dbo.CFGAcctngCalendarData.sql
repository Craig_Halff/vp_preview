CREATE TABLE [dbo].[CFGAcctngCalendarData]
(
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[Period] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAcctngCalendarData] ADD CONSTRAINT [CFGAcctngCalendarDataPK] PRIMARY KEY CLUSTERED ([StartDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CFGAcctngCalendarDataStartEndDateIDX] ON [dbo].[CFGAcctngCalendarData] ([StartDate], [EndDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
