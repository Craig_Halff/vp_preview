CREATE TABLE [dbo].[cvControlDefaultTaxCodes]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__cvControlDe__Seq__6FF6B8A3] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cvControlDefaultTaxCodes] ADD CONSTRAINT [cvControlDefaultTaxCodesPK] PRIMARY KEY NONCLUSTERED ([Batch], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
