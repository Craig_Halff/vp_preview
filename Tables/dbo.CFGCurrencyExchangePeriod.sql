CREATE TABLE [dbo].[CFGCurrencyExchangePeriod]
(
[FromCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ToCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__CFGCurren__Perio__20C40D30] DEFAULT ((0)),
[PeriodAvgRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__CFGCurren__Perio__21B83169] DEFAULT ((0)),
[PeriodEndRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__CFGCurren__Perio__22AC55A2] DEFAULT ((0)),
[Source] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_CFGCurrencyExchangePeriod]
      ON [dbo].[CFGCurrencyExchangePeriod]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CFGCurrencyExchangePeriod'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121),'FromCode',CONVERT(NVARCHAR(2000),[FromCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121),'ToCode',CONVERT(NVARCHAR(2000),[ToCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121),'Period',CONVERT(NVARCHAR(2000),[Period],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121),'PeriodAvgRate',CONVERT(NVARCHAR(2000),[PeriodAvgRate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121),'PeriodEndRate',CONVERT(NVARCHAR(2000),[PeriodEndRate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Period],121),'Source',CONVERT(NVARCHAR(2000),[Source],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_CFGCurrencyExchangePeriod]
      ON [dbo].[CFGCurrencyExchangePeriod]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CFGCurrencyExchangePeriod'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121),'FromCode',NULL,CONVERT(NVARCHAR(2000),[FromCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121),'ToCode',NULL,CONVERT(NVARCHAR(2000),[ToCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121),'Period',NULL,CONVERT(NVARCHAR(2000),[Period],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121),'PeriodAvgRate',NULL,CONVERT(NVARCHAR(2000),[PeriodAvgRate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121),'PeriodEndRate',NULL,CONVERT(NVARCHAR(2000),[PeriodEndRate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121),'Source',NULL,CONVERT(NVARCHAR(2000),[Source],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_CFGCurrencyExchangePeriod]
      ON [dbo].[CFGCurrencyExchangePeriod]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CFGCurrencyExchangePeriod'
    
      If UPDATE([FromCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121),'FromCode',
      CONVERT(NVARCHAR(2000),DELETED.[FromCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FromCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[FromCode] = DELETED.[FromCode] AND INSERTED.[ToCode] = DELETED.[ToCode] AND INSERTED.[Period] = DELETED.[Period] AND 
		(
			(
				INSERTED.[FromCode] Is Null And
				DELETED.[FromCode] Is Not Null
			) Or
			(
				INSERTED.[FromCode] Is Not Null And
				DELETED.[FromCode] Is Null
			) Or
			(
				INSERTED.[FromCode] !=
				DELETED.[FromCode]
			)
		) 
		END		
		
      If UPDATE([ToCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121),'ToCode',
      CONVERT(NVARCHAR(2000),DELETED.[ToCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ToCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[FromCode] = DELETED.[FromCode] AND INSERTED.[ToCode] = DELETED.[ToCode] AND INSERTED.[Period] = DELETED.[Period] AND 
		(
			(
				INSERTED.[ToCode] Is Null And
				DELETED.[ToCode] Is Not Null
			) Or
			(
				INSERTED.[ToCode] Is Not Null And
				DELETED.[ToCode] Is Null
			) Or
			(
				INSERTED.[ToCode] !=
				DELETED.[ToCode]
			)
		) 
		END		
		
      If UPDATE([Period])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121),'Period',
      CONVERT(NVARCHAR(2000),DELETED.[Period],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Period],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[FromCode] = DELETED.[FromCode] AND INSERTED.[ToCode] = DELETED.[ToCode] AND INSERTED.[Period] = DELETED.[Period] AND 
		(
			(
				INSERTED.[Period] Is Null And
				DELETED.[Period] Is Not Null
			) Or
			(
				INSERTED.[Period] Is Not Null And
				DELETED.[Period] Is Null
			) Or
			(
				INSERTED.[Period] !=
				DELETED.[Period]
			)
		) 
		END		
		
      If UPDATE([PeriodAvgRate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121),'PeriodAvgRate',
      CONVERT(NVARCHAR(2000),DELETED.[PeriodAvgRate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PeriodAvgRate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[FromCode] = DELETED.[FromCode] AND INSERTED.[ToCode] = DELETED.[ToCode] AND INSERTED.[Period] = DELETED.[Period] AND 
		(
			(
				INSERTED.[PeriodAvgRate] Is Null And
				DELETED.[PeriodAvgRate] Is Not Null
			) Or
			(
				INSERTED.[PeriodAvgRate] Is Not Null And
				DELETED.[PeriodAvgRate] Is Null
			) Or
			(
				INSERTED.[PeriodAvgRate] !=
				DELETED.[PeriodAvgRate]
			)
		) 
		END		
		
      If UPDATE([PeriodEndRate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121),'PeriodEndRate',
      CONVERT(NVARCHAR(2000),DELETED.[PeriodEndRate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PeriodEndRate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[FromCode] = DELETED.[FromCode] AND INSERTED.[ToCode] = DELETED.[ToCode] AND INSERTED.[Period] = DELETED.[Period] AND 
		(
			(
				INSERTED.[PeriodEndRate] Is Null And
				DELETED.[PeriodEndRate] Is Not Null
			) Or
			(
				INSERTED.[PeriodEndRate] Is Not Null And
				DELETED.[PeriodEndRate] Is Null
			) Or
			(
				INSERTED.[PeriodEndRate] !=
				DELETED.[PeriodEndRate]
			)
		) 
		END		
		
      If UPDATE([Source])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToCode],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Period],121),'Source',
      CONVERT(NVARCHAR(2000),DELETED.[Source],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Source],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[FromCode] = DELETED.[FromCode] AND INSERTED.[ToCode] = DELETED.[ToCode] AND INSERTED.[Period] = DELETED.[Period] AND 
		(
			(
				INSERTED.[Source] Is Null And
				DELETED.[Source] Is Not Null
			) Or
			(
				INSERTED.[Source] Is Not Null And
				DELETED.[Source] Is Null
			) Or
			(
				INSERTED.[Source] !=
				DELETED.[Source]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[CFGCurrencyExchangePeriod] ADD CONSTRAINT [CFGCurrencyExchangePeriodPK] PRIMARY KEY CLUSTERED ([FromCode], [ToCode], [Period]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
