CREATE TABLE [dbo].[CFGStateID]
(
[State] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StateIDNo] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateYN] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGStateI__State__6FD6BD12] DEFAULT ('Y'),
[PayrollOnly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGStateI__Payro__70CAE14B] DEFAULT ('N'),
[F1099Only] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGStateI__F1099__71BF0584] DEFAULT ('N'),
[W2Name] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGStateID] ADD CONSTRAINT [CFGStateIDPK] PRIMARY KEY CLUSTERED ([Company], [State]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
