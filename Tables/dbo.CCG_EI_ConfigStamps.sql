CREATE TABLE [dbo].[CCG_EI_ConfigStamps]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[Label] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StampType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayOrder] [int] NULL,
[Content] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PositionType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PageSet] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Width] [int] NULL,
[ValidRoles] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SetStage] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SetMessage] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_EI_Co__Statu__3F0A69D5] DEFAULT ('A'),
[ModDate] [datetime] NOT NULL,
[ModUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShowIn] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_ConfigStamps] ADD CONSTRAINT [PK_CCG_EI_ConfigStamps] PRIMARY KEY CLUSTERED ([Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Definitions of Stamps to be used on PDFs', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStamps', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Either the text to use for the stamp or the image path', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStamps', 'COLUMN', N'Content'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The order in which to display this stamp relative to other stamps', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStamps', 'COLUMN', N'DisplayOrder'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The default stamp label to display', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStamps', 'COLUMN', N'Label'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date when this record was last modified', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStamps', 'COLUMN', N'ModDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Vision User who last modified this record', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStamps', 'COLUMN', N'ModUser'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The page location where this stamp should go (First / All)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStamps', 'COLUMN', N'PageSet'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The placement position of the stamp (Free / TopRight / etc)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStamps', 'COLUMN', N'PositionType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStamps', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStamps', 'COLUMN', N'SetMessage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Depricated- See CCG_EI_ConfigStages.RequireStamp', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStamps', 'COLUMN', N'SetStage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_ConfigStamps / ShowIn Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStamps', 'COLUMN', N'ShowIn'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The type of stamp (Text / Image)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStamps', 'COLUMN', N'StampType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Status of this Stamp - [A]ctive / [I]nactive', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStamps', 'COLUMN', N'Status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Semicolon separated list of roles that can use this stamp', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStamps', 'COLUMN', N'ValidRoles'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The width of the stamp as a percent of the whole document (1 - 100)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStamps', 'COLUMN', N'Width'
GO
