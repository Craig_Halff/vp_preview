CREATE TABLE [dbo].[PNPlannedLabor]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AssignmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodCount] [int] NOT NULL CONSTRAINT [DF__PNPlanned__Perio__2668A344] DEFAULT ((1)),
[PeriodHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlanned__Perio__275CC77D] DEFAULT ((0)),
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlanned__Perio__2850EBB6] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlanned__Perio__29450FEF] DEFAULT ((0)),
[CostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlanned__CostR__2A393428] DEFAULT ((0)),
[BillingRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlanned__Billi__2B2D5861] DEFAULT ((0)),
[PeriodRev] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlanned__Perio__2C217C9A] DEFAULT ((0)),
[HardBooked] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlanned__HardB__2D15A0D3] DEFAULT ('N'),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PNPlannedLabor] ADD CONSTRAINT [PNPlannedLaborPK] PRIMARY KEY NONCLUSTERED ([TimePhaseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNPlannedLaborPTAIDX] ON [dbo].[PNPlannedLabor] ([PlanID], [TaskID], [AssignmentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNPlannedLaborAll2IDX] ON [dbo].[PNPlannedLabor] ([PlanID], [TaskID], [StartDate], [EndDate], [AssignmentID], [PeriodHrs], [PeriodCost], [PeriodBill], [PeriodRev]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNPlannedLaborAllIDX] ON [dbo].[PNPlannedLabor] ([PlanID], [TimePhaseID], [TaskID], [AssignmentID], [StartDate], [EndDate], [PeriodHrs], [PeriodCost], [PeriodBill], [PeriodRev], [CostRate], [BillingRate]) ON [PRIMARY]
GO
