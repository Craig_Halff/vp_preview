CREATE TABLE [dbo].[CFGPRResponsibilityDescriptions]
(
[Code] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGPRRespon__Seq__2C55BB6D] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPRResponsibilityDescriptions] ADD CONSTRAINT [CFGPRResponsibilityDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
