CREATE TABLE [dbo].[ChangeDataCapture]
(
[PKey] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Integration] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Direction] [int] NOT NULL CONSTRAINT [DF__ChangeDat__Direc__2BEE091E] DEFAULT ((0)),
[IntegrationEntity] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IntegrationKey] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DPSEntity] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DPSKey] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChangeDate] [datetime] NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Retries] [smallint] NOT NULL CONSTRAINT [DF__ChangeDat__Retri__2CE22D57] DEFAULT ((0)),
[LastProcessDate] [datetime] NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ChangeDataCapture] ADD CONSTRAINT [ChangeDataCapturePK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ChangeDataCaptureSearchIDX] ON [dbo].[ChangeDataCapture] ([Company], [Integration], [Direction], [Status]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ChangeDataCaptureIntegrationIDX] ON [dbo].[ChangeDataCapture] ([Integration]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ChangeDataCaptureStatusIDX] ON [dbo].[ChangeDataCapture] ([Status]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
