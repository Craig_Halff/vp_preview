CREATE TABLE [dbo].[CFGPYContributionRateTable]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__CFGPYCont__Table__49E61E54] DEFAULT ((0)),
[TableName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RateType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPYContributionRateTable] ADD CONSTRAINT [CFGPYContributionRateTablePK] PRIMARY KEY CLUSTERED ([TableNo]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
