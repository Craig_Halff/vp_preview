CREATE TABLE [dbo].[RecurrActivityException]
(
[ActivityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[TranType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RecurrAct__TranT__52147D5D] DEFAULT ('M')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RecurrActivityException] ADD CONSTRAINT [RecurrActivityExceptionPK] PRIMARY KEY NONCLUSTERED ([ActivityID], [StartDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RecurrActivityExceptionTranTypeIDX] ON [dbo].[RecurrActivityException] ([TranType]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
