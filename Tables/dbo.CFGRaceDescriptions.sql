CREATE TABLE [dbo].[CFGRaceDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGRaceDesc__Seq__6CFCB26C] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGRaceDescriptions] ADD CONSTRAINT [CFGRaceDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
