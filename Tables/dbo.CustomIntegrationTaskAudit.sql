CREATE TABLE [dbo].[CustomIntegrationTaskAudit]
(
[TaskAuditID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomIntegrationID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExecutionDate] [datetime] NOT NULL,
[Period] [int] NOT NULL,
[Status] [int] NOT NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomIntegrationTaskAudit] ADD CONSTRAINT [CustomIntegrationTaskAuditPK] PRIMARY KEY CLUSTERED ([TaskAuditID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
