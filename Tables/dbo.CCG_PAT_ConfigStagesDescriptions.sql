CREATE TABLE [dbo].[CCG_PAT_ConfigStagesDescriptions]
(
[Stage] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StageLabel] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StageDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmailSubject] [nvarchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailSubjectBatch] [nvarchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailMessage] [nvarchar] (950) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_ConfigStagesDescriptions] ADD CONSTRAINT [CCG_PAT_ConfigStagesDescriptionsPK] PRIMARY KEY NONCLUSTERED ([Stage], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Descriptions for stages as used for multi-language companies', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStagesDescriptions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The language-specific body of the email notification (if any)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStagesDescriptions', 'COLUMN', N'EmailMessage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The language-specific subject line for email notifications when this stage is used (if any)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStagesDescriptions', 'COLUMN', N'EmailSubject'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The language-specific email subject line to use for batch notifications (if any)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStagesDescriptions', 'COLUMN', N'EmailSubjectBatch'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The stage name/key', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStagesDescriptions', 'COLUMN', N'Stage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The language-specific stage description. Shown in stage tooltips.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStagesDescriptions', 'COLUMN', N'StageDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The language-specific display label for this stage', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStagesDescriptions', 'COLUMN', N'StageLabel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The UI culture code of this description', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStagesDescriptions', 'COLUMN', N'UICultureName'
GO
