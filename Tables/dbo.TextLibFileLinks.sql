CREATE TABLE [dbo].[TextLibFileLinks]
(
[LinkID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilePath] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Graphic] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__TextLibFi__Graph__1648156B] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_TextLibFileLinks]
      ON [dbo].[TextLibFileLinks]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'TextLibFileLinks'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Name],121),'LinkID',CONVERT(NVARCHAR(2000),[LinkID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Name],121),'Name',CONVERT(NVARCHAR(2000),[Name],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Name],121),'Description',CONVERT(NVARCHAR(2000),[Description],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Name],121),'FilePath',CONVERT(NVARCHAR(2000),[FilePath],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Name],121),'Graphic',CONVERT(NVARCHAR(2000),[Graphic],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_TextLibFileLinks] ON [dbo].[TextLibFileLinks]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_TextLibFileLinks]
      ON [dbo].[TextLibFileLinks]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'TextLibFileLinks'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'LinkID',NULL,CONVERT(NVARCHAR(2000),[LinkID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'Name',NULL,CONVERT(NVARCHAR(2000),[Name],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'Description',NULL,CONVERT(NVARCHAR(2000),[Description],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'FilePath',NULL,CONVERT(NVARCHAR(2000),[FilePath],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'Graphic',NULL,CONVERT(NVARCHAR(2000),[Graphic],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_TextLibFileLinks] ON [dbo].[TextLibFileLinks]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_TextLibFileLinks]
      ON [dbo].[TextLibFileLinks]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'TextLibFileLinks'
    
      If UPDATE([LinkID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'LinkID',
      CONVERT(NVARCHAR(2000),DELETED.[LinkID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LinkID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[LinkID] = DELETED.[LinkID] AND INSERTED.[Name] = DELETED.[Name] AND 
		(
			(
				INSERTED.[LinkID] Is Null And
				DELETED.[LinkID] Is Not Null
			) Or
			(
				INSERTED.[LinkID] Is Not Null And
				DELETED.[LinkID] Is Null
			) Or
			(
				INSERTED.[LinkID] !=
				DELETED.[LinkID]
			)
		) 
		END		
		
      If UPDATE([Name])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'Name',
      CONVERT(NVARCHAR(2000),DELETED.[Name],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Name],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[LinkID] = DELETED.[LinkID] AND INSERTED.[Name] = DELETED.[Name] AND 
		(
			(
				INSERTED.[Name] Is Null And
				DELETED.[Name] Is Not Null
			) Or
			(
				INSERTED.[Name] Is Not Null And
				DELETED.[Name] Is Null
			) Or
			(
				INSERTED.[Name] !=
				DELETED.[Name]
			)
		) 
		END		
		
      If UPDATE([Description])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'Description',
      CONVERT(NVARCHAR(2000),DELETED.[Description],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Description],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[LinkID] = DELETED.[LinkID] AND INSERTED.[Name] = DELETED.[Name] AND 
		(
			(
				INSERTED.[Description] Is Null And
				DELETED.[Description] Is Not Null
			) Or
			(
				INSERTED.[Description] Is Not Null And
				DELETED.[Description] Is Null
			) Or
			(
				INSERTED.[Description] !=
				DELETED.[Description]
			)
		) 
		END		
		
      If UPDATE([FilePath])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'FilePath',
      CONVERT(NVARCHAR(2000),DELETED.[FilePath],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FilePath],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[LinkID] = DELETED.[LinkID] AND INSERTED.[Name] = DELETED.[Name] AND 
		(
			(
				INSERTED.[FilePath] Is Null And
				DELETED.[FilePath] Is Not Null
			) Or
			(
				INSERTED.[FilePath] Is Not Null And
				DELETED.[FilePath] Is Null
			) Or
			(
				INSERTED.[FilePath] !=
				DELETED.[FilePath]
			)
		) 
		END		
		
      If UPDATE([Graphic])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[LinkID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Name],121),'Graphic',
      CONVERT(NVARCHAR(2000),DELETED.[Graphic],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Graphic],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[LinkID] = DELETED.[LinkID] AND INSERTED.[Name] = DELETED.[Name] AND 
		(
			(
				INSERTED.[Graphic] Is Null And
				DELETED.[Graphic] Is Not Null
			) Or
			(
				INSERTED.[Graphic] Is Not Null And
				DELETED.[Graphic] Is Null
			) Or
			(
				INSERTED.[Graphic] !=
				DELETED.[Graphic]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_TextLibFileLinks] ON [dbo].[TextLibFileLinks]
GO
ALTER TABLE [dbo].[TextLibFileLinks] ADD CONSTRAINT [TextLibFileLinksPK] PRIMARY KEY NONCLUSTERED ([LinkID], [Name]) ON [PRIMARY]
GO
