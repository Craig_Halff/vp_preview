CREATE TABLE [dbo].[VisionImportEmployees_RoleLog_EmpRoleLogImport]
(
[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustRole] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDate] [datetime] NULL,
[CustAction] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
