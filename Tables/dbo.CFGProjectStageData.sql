CREATE TABLE [dbo].[CFGProjectStageData]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Step] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGProject__Step__17E71071] DEFAULT ('I')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGProjectStageData] ADD CONSTRAINT [CFGProjectStageDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
