CREATE TABLE [dbo].[CFGPrefixDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Prefix] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGPrefixDe__Seq__10ADA0F8] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPrefixDescriptions] ADD CONSTRAINT [CFGPrefixDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
