CREATE TABLE [dbo].[CFGPostControlIntercompany]
(
[Period] [int] NOT NULL CONSTRAINT [DF__CFGPostCo__Perio__0A00A369] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__CFGPostCo__PostS__0AF4C7A2] DEFAULT ((0)),
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PostComment] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecordCount] [int] NOT NULL CONSTRAINT [DF__CFGPostCo__Recor__0BE8EBDB] DEFAULT ((0)),
[XChargeStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICBillingStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPostControlIntercompany] ADD CONSTRAINT [CFGPostControlIntercompanyPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
