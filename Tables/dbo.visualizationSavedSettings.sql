CREATE TABLE [dbo].[visualizationSavedSettings]
(
[favoriteReportType] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[favoriteName] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[userName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[settingName] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[settings] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[visualizationSavedSettings] ADD CONSTRAINT [visualizationSavedSettingsPK] PRIMARY KEY NONCLUSTERED ([favoriteReportType], [favoriteName], [userName], [settingName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
