CREATE TABLE [dbo].[CFGApprovalRoleDescriptions]
(
[RoleID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGApprovalRoleDescriptions] ADD CONSTRAINT [CFGApprovalRoleDescriptionsPK] PRIMARY KEY NONCLUSTERED ([RoleID], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
