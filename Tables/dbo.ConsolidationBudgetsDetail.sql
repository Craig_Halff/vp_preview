CREATE TABLE [dbo].[ConsolidationBudgetsDetail]
(
[BudgetName] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefBudget] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__RefBu__40B1B9D5] DEFAULT ((0)),
[Annual] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Annua__41A5DE0E] DEFAULT ((0)),
[Amount1] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Amoun__429A0247] DEFAULT ((0)),
[Amount2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Amoun__438E2680] DEFAULT ((0)),
[Amount3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Amoun__44824AB9] DEFAULT ((0)),
[Amount4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Amoun__45766EF2] DEFAULT ((0)),
[Amount5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Amoun__466A932B] DEFAULT ((0)),
[Amount6] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Amoun__475EB764] DEFAULT ((0)),
[Amount7] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Amoun__4852DB9D] DEFAULT ((0)),
[Amount8] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Amoun__4946FFD6] DEFAULT ((0)),
[Amount9] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Amoun__4A3B240F] DEFAULT ((0)),
[Amount10] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Amoun__4B2F4848] DEFAULT ((0)),
[Amount11] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Amoun__4C236C81] DEFAULT ((0)),
[Amount12] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Amoun__4D1790BA] DEFAULT ((0)),
[Amount13] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Consolida__Amoun__4E0BB4F3] DEFAULT ((0)),
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConsolidationBudgetsDetail] ADD CONSTRAINT [ConsolidationBudgetsDetailPK] PRIMARY KEY NONCLUSTERED ([BudgetName], [Account]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
