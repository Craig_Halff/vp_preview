CREATE TABLE [dbo].[CLSubscr]
(
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CLSubscr] ADD CONSTRAINT [CLSubscrPK] PRIMARY KEY NONCLUSTERED ([PartKey], [ClientID], [UserName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
