CREATE TABLE [dbo].[CFGAPMain]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultTerms] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrintRemit] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VouchersOnApron] [smallint] NOT NULL CONSTRAINT [DF__CFGAPMain__Vouch__7FCD2DB9] DEFAULT ((0)),
[APCheckTemplate] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MemoPrintOnCheck] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAPMain__MemoP__00C151F2] DEFAULT ('N'),
[StartVoucher] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGAPMain__Start__01B5762B] DEFAULT ((0)),
[EndVoucher] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGAPMain__EndVo__02A99A64] DEFAULT ((0)),
[APCheckFormat] [smallint] NOT NULL CONSTRAINT [DF__CFGAPMain__APChe__039DBE9D] DEFAULT ((0)),
[PrintDateFormatIndicator] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAPMain__Print__0491E2D6] DEFAULT ('N'),
[CurrencyFormat] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateFormat] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnableApprovalWorkflowAP] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAPMain__Enabl__0586070F] DEFAULT ('N'),
[ApprovalWorkflowAP] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAPMain] ADD CONSTRAINT [CFGAPMainPK] PRIMARY KEY CLUSTERED ([Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
