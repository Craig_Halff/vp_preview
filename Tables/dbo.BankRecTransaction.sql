CREATE TABLE [dbo].[BankRecTransaction]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatementDate] [datetime] NULL,
[ImportPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cleared] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BankRecTr__Clear__58095522] DEFAULT ('N'),
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransDate] [datetime] NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BankRecTr__Amoun__58FD795B] DEFAULT ((0)),
[UserDefined1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserDefined2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserDefined3] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserDefined4] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserDefined5] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BankRecTransaction] ADD CONSTRAINT [BankRecTransactionPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
