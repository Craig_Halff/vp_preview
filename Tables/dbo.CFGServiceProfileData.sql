CREATE TABLE [dbo].[CFGServiceProfileData]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FeeBands] [int] NOT NULL,
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SystemProfile] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGServic__Syste__570B0F48] DEFAULT ('N'),
[Disabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGServic__Disab__57FF3381] DEFAULT ('N'),
[GlobalProfile] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGServic__Globa__58F357BA] DEFAULT ('Y'),
[DisableFeeBands] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGServic__Disab__59E77BF3] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGServiceProfileData] ADD CONSTRAINT [CFGServiceProfileDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
