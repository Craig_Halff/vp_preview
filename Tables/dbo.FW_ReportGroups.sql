CREATE TABLE [dbo].[FW_ReportGroups]
(
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GroupID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GroupDefinition] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastUpdate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_ReportGroups] ADD CONSTRAINT [FW_ReportGroupsPK] PRIMARY KEY NONCLUSTERED ([UICultureName], [GroupID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
