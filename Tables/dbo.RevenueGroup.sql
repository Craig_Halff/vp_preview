CREATE TABLE [dbo].[RevenueGroup]
(
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RevType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevType2] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevType3] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevType4] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevType5] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MultAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RevenueGr__MultA__774E8CFF] DEFAULT ((0)),
[RevWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RevenueGroup] ADD CONSTRAINT [RevenueGroupPK] PRIMARY KEY NONCLUSTERED ([MainWBS1]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
