CREATE TABLE [dbo].[FW_TextNotTranslated]
(
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LocalizedValue] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_TextNotTranslated] ADD CONSTRAINT [FW_TextNotTranslatedPK] PRIMARY KEY CLUSTERED ([UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
