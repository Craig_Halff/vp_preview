CREATE TABLE [dbo].[TransactionUDFLabels]
(
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UDFID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToolTip] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransactionUDFLabels] ADD CONSTRAINT [TransactionUDFLabelsPK] PRIMARY KEY NONCLUSTERED ([TransType], [UDFID], [Company], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
