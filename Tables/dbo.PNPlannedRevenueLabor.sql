CREATE TABLE [dbo].[PNPlannedRevenueLabor]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlanned__Perio__20323DAF] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlanned__Perio__212661E8] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PNPlannedRevenueLabor] ADD CONSTRAINT [PNPlannedRevenueLaborPK] PRIMARY KEY CLUSTERED ([TimePhaseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNPlannedRevenuLaborAll2IDX] ON [dbo].[PNPlannedRevenueLabor] ([PlanID], [TaskID], [StartDate], [EndDate], [PeriodCost], [PeriodBill]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNPlannedRevenueLaborAllIDX] ON [dbo].[PNPlannedRevenueLabor] ([PlanID], [TimePhaseID], [TaskID], [StartDate], [EndDate], [PeriodCost], [PeriodBill]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
