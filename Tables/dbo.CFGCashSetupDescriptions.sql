CREATE TABLE [dbo].[CFGCashSetupDescriptions]
(
[Code] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGCashSe__Compa__519C4EB5] DEFAULT ((0)),
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SortSeq] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCashSetupDescriptions] ADD CONSTRAINT [CFGCashSetupDescriptionsPK] PRIMARY KEY NONCLUSTERED ([Code], [Company], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
