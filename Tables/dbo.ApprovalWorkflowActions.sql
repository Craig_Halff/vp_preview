CREATE TABLE [dbo].[ApprovalWorkflowActions]
(
[WorkflowAction_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Workflow_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StepActionType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EventID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Active] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ApprovalW__Activ__421A1403] DEFAULT ('Y'),
[NotificationType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StepControl] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ApprovalW__StepC__430E383C] DEFAULT ('N'),
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ApprovalWorkflowActions] ADD CONSTRAINT [ApprovalWorkflowActionsPK] PRIMARY KEY CLUSTERED ([WorkflowAction_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
