CREATE TABLE [dbo].[CFGScreenDesignerLabels]
(
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ComponentID] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToolTip] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AlternateLabel] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGScreenDesignerLabels] ADD CONSTRAINT [CFGScreenDesignerLabelsPK] PRIMARY KEY NONCLUSTERED ([InfoCenterArea], [GridID], [ComponentID], [UICultureName]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CFGScreenDesignerLabelsInfoCenterAreaGridIDIDX] ON [dbo].[CFGScreenDesignerLabels] ([InfoCenterArea], [GridID], [ComponentID], [UICultureName]) INCLUDE ([AlternateLabel], [Label]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
