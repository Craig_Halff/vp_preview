CREATE TABLE [dbo].[CFGTransCustomFields]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Required] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransC__Requi__17E7DBC8] DEFAULT ('N'),
[AP_Available] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransC__AP_Av__18DC0001] DEFAULT ('N'),
[CD_Available] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransC__CD_Av__19D0243A] DEFAULT ('N'),
[CR_Available] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransC__CR_Av__1AC44873] DEFAULT ('N'),
[CV_Available] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransC__CV_Av__1BB86CAC] DEFAULT ('N'),
[ER_Available] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransC__ER_Av__1CAC90E5] DEFAULT ('N'),
[EX_Available] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransC__EX_Av__1DA0B51E] DEFAULT ('N'),
[IN_Available] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransC__IN_Av__1E94D957] DEFAULT ('N'),
[JE_Available] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransC__JE_Av__1F88FD90] DEFAULT ('N'),
[LA_Available] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransC__LA_Av__207D21C9] DEFAULT ('N'),
[MI_Available] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransC__MI_Av__21714602] DEFAULT ('N'),
[PR_Available] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransC__PR_Av__22656A3B] DEFAULT ('N'),
[TS_Available] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransC__TS_Av__23598E74] DEFAULT ('N'),
[UN_Available] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransC__UN_Av__244DB2AD] DEFAULT ('N'),
[UP_Available] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGTransC__UP_Av__2541D6E6] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGTransCustomFields] ADD CONSTRAINT [CFGTransCustomFieldsPK] PRIMARY KEY CLUSTERED ([Company], [Name]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
