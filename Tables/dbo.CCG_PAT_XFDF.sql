CREATE TABLE [dbo].[CCG_PAT_XFDF]
(
[XFDFID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__CCG_PAT_X__XFDFI__2AEC3254] DEFAULT (newid()),
[RevisionSeq] [int] NOT NULL CONSTRAINT [DF__CCG_PAT_X__Revis__2BE0568D] DEFAULT ((0)),
[PayableSeq] [int] NULL,
[FileName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REVID] [uniqueidentifier] NULL,
[CreateUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[XFDFFILE] [xml] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_XFDF] ADD CONSTRAINT [PK__CCG_PAT___C2CE4E779BD5DD96] PRIMARY KEY CLUSTERED ([XFDFID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IxCCG_PAT_XFDF_PayableSeq] ON [dbo].[CCG_PAT_XFDF] ([PayableSeq]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'PDF markup when storage type uses database storage for markup', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_XFDF', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date/time when this markup was made', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_XFDF', 'COLUMN', N'CreateDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The user of the one who created this markup', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_XFDF', 'COLUMN', N'CreateUser'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_XFDF', 'COLUMN', N'FileName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - Payable, The Payable sequence number for which this PDF corresponds', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_XFDF', 'COLUMN', N'PayableSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - Files, only used when markup linked to a previous file revision.  Null for current file.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_XFDF', 'COLUMN', N'REVID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'0 for current, 1 is added to existing entries when a new row is inserted', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_XFDF', 'COLUMN', N'RevisionSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The XML/XFDF content of the markup', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_XFDF', 'COLUMN', N'XFDFFILE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_XFDF', 'COLUMN', N'XFDFID'
GO
