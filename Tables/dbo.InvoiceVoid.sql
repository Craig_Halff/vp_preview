CREATE TABLE [dbo].[InvoiceVoid]
(
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InvoiceDate] [datetime] NOT NULL,
[VoidDate] [datetime] NOT NULL,
[FileID] [uniqueidentifier] NULL,
[VoidID] [int] NOT NULL IDENTITY(10000, 1),
[CreditMemoRefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[InvoiceVoid] ADD CONSTRAINT [InvoiceVoidPK] PRIMARY KEY CLUSTERED ([VoidID]) ON [PRIMARY]
GO
