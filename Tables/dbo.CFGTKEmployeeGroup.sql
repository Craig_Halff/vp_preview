CREATE TABLE [dbo].[CFGTKEmployeeGroup]
(
[EmployeeGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GroupID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGTKEmployeeGroup] ADD CONSTRAINT [CFGTKEmployeeGroupPK] PRIMARY KEY CLUSTERED ([Company], [EmployeeGroup]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
