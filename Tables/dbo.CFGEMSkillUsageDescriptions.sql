CREATE TABLE [dbo].[CFGEMSkillUsageDescriptions]
(
[Code] [smallint] NOT NULL CONSTRAINT [DF__CFGEMSkill__Code__778CED73] DEFAULT ((0)),
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGEMSkillU__Seq__788111AC] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGEMSkillUsageDescriptions] ADD CONSTRAINT [CFGEMSkillUsageDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
