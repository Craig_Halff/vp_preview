CREATE TABLE [dbo].[OpportunityFromContacts_Backup]
(
[PKey] [int] NOT NULL IDENTITY(1, 1),
[ContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampaignID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OpportunityFromContacts_Backup] ADD CONSTRAINT [OpportunityFromContactsPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [OpportunityFromContactsIDX] ON [dbo].[OpportunityFromContacts_Backup] ([ContactID], [OpportunityID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
