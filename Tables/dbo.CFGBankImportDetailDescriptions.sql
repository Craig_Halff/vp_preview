CREATE TABLE [dbo].[CFGBankImportDetailDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FieldName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FieldLabel] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGBankImportDetailDescriptions] ADD CONSTRAINT [CFGBankImportDetailDescriptionsPK] PRIMARY KEY NONCLUSTERED ([Code], [FieldName], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
