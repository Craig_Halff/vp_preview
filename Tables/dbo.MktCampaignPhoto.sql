CREATE TABLE [dbo].[MktCampaignPhoto]
(
[CampaignID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Photo] [varbinary] (max) NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MktCampaignPhoto] ADD CONSTRAINT [MktCampaignPhotoPK] PRIMARY KEY CLUSTERED ([CampaignID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
