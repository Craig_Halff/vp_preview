CREATE TABLE [dbo].[tkBreakTime]
(
[EndDate] [datetime] NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransDate] [datetime] NOT NULL,
[BreaksTaken] [int] NOT NULL CONSTRAINT [DF__tkBreakTi__Break__21B9C817] DEFAULT ((0)),
[MealStartDateTime] [datetime] NULL,
[MealEndDateTime] [datetime] NULL,
[Meal2StartDateTime] [datetime] NULL,
[Meal2EndDateTime] [datetime] NULL,
[StartDateTime] [datetime] NULL,
[EndDateTime] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tkBreakTime] ADD CONSTRAINT [tkBreakTimePK] PRIMARY KEY NONCLUSTERED ([EndDate], [Employee], [EmployeeCompany], [TransDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
