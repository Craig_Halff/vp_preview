CREATE TABLE [dbo].[CFGPOStatusData]
(
[Screen] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPOStatusData] ADD CONSTRAINT [CFGPOStatusDataPK] PRIMARY KEY CLUSTERED ([Screen], [Status]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
