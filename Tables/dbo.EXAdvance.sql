CREATE TABLE [dbo].[EXAdvance]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__EXAdvance__Perio__1E9C98F3] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__EXAdvance__PostS__1F90BD2C] DEFAULT ((0)),
[Bank] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EXAdvance__Amoun__2084E165] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EXAdvance] ADD CONSTRAINT [exAdvancePK] PRIMARY KEY NONCLUSTERED ([Employee], [Period], [PostSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
