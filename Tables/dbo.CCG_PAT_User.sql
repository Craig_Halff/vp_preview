CREATE TABLE [dbo].[CCG_PAT_User]
(
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OkdLicense] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_User] ADD CONSTRAINT [PK_CCG_PAT_User] PRIMARY KEY CLUSTERED ([Username]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'PAT users', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_User', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Was the license agreement accepted? Y/N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_User', 'COLUMN', N'OkdLicense'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*FK - Vision user with access to PAT', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_User', 'COLUMN', N'Username'
GO
