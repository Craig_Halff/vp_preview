CREATE TABLE [dbo].[ReportPresentationCurrency]
(
[SessionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FromCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PresentationCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Rate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__ReportPres__Rate__6EB946FE] DEFAULT ((0)),
[PresentationCurrencyDecimalPlaces] [smallint] NOT NULL CONSTRAINT [DF__ReportPre__Prese__6FAD6B37] DEFAULT ((0)),
[PresentationCurrencyCurrencySymbol] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Message] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastRun] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportPresentationCurrency] ADD CONSTRAINT [ReportPresentationCurrencyPK] PRIMARY KEY NONCLUSTERED ([SessionID], [Company], [FromCurrencyCode], [PresentationCurrencyCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
