CREATE TABLE [dbo].[CFGContactRelationshipDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGContactR__Seq__7020D5D5] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGContactRelationshipDescriptions] ADD CONSTRAINT [CFGContactRelationshipDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
