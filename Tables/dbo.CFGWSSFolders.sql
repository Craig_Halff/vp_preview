CREATE TABLE [dbo].[CFGWSSFolders]
(
[WebID] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ListID] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FolderURL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FolderName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ParentFolderURL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentFolderName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FolderTreeLevel] [smallint] NOT NULL CONSTRAINT [DF__CFGWSSFol__Folde__61539391] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGWSSFolders] ADD CONSTRAINT [CFGWSSFoldersPK] PRIMARY KEY NONCLUSTERED ([WebID], [ListID], [FolderURL]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
