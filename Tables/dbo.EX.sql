CREATE TABLE [dbo].[EX]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Voucher] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__EX_New__Period__18E3BF9D] DEFAULT ((0)),
[ReportName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportDate] [datetime] NULL,
[Bank] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppliedAdvanceOverride] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EX_New__AppliedA__19D7E3D6] DEFAULT ((0)),
[SelPeriod] [int] NOT NULL CONSTRAINT [DF__EX_New__SelPerio__1ACC080F] DEFAULT ((0)),
[SelPostSeq] [int] NOT NULL CONSTRAINT [DF__EX_New__SelPostS__1BC02C48] DEFAULT ((0)),
[PaidPeriod] [int] NOT NULL CONSTRAINT [DF__EX_New__PaidPeri__1CB45081] DEFAULT ((0)),
[BarCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EX] ADD CONSTRAINT [EXPK] PRIMARY KEY NONCLUSTERED ([Employee], [Voucher]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
