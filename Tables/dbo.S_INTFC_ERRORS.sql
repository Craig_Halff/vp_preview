CREATE TABLE [dbo].[S_INTFC_ERRORS]
(
[ERR_ID] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ERR_MSG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TIME_STAMP] [datetime] NULL,
[MODIFIED_BY] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ROWVERSION] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[S_INTFC_ERRORS] ADD CONSTRAINT [S_INTFC_ERRORSPK] PRIMARY KEY NONCLUSTERED ([ERR_ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
