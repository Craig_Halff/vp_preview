CREATE TABLE [dbo].[Firm]
(
[FirmID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OfficeLocation] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Address1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OfficePhone] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OfficeFax] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Firm_New__Parent__6252A4C2] DEFAULT ('N'),
[ParentCompanyID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateEstablished] [datetime] NULL,
[TotalPersonnel] [int] NOT NULL CONSTRAINT [DF__Firm_New__TotalP__6346C8FB] DEFAULT ((0)),
[OwnershipType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SmallBusinessInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Firm_New__SmallB__643AED34] DEFAULT ('N'),
[SmallDisadvantagedInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Firm_New__SmallD__652F116D] DEFAULT ('N'),
[WomanOwnedInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Firm_New__WomanO__662335A6] DEFAULT ('N'),
[InterestInForeignWorkInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Firm_New__Intere__671759DF] DEFAULT ('N'),
[DunsNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FedRev3YrAverage] [smallint] NOT NULL CONSTRAINT [DF__Firm_New__FedRev__680B7E18] DEFAULT ((0)),
[NonFedRev3YrAverage] [smallint] NOT NULL CONSTRAINT [DF__Firm_New__NonFed__68FFA251] DEFAULT ((0)),
[TotalRev3YrAverage] [smallint] NOT NULL CONSTRAINT [DF__Firm_New__TotalR__69F3C68A] DEFAULT ((0)),
[OfficePhoneFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OfficeFaxFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Firm] ADD CONSTRAINT [FirmPK] PRIMARY KEY NONCLUSTERED ([FirmID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
