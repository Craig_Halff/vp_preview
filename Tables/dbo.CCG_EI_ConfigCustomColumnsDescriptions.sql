CREATE TABLE [dbo].[CCG_EI_ConfigCustomColumnsDescriptions]
(
[Seq] [int] NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GroupName] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_ConfigCustomColumnsDescriptions] ADD CONSTRAINT [PK_CCG_EI_ConfigCustomColumnsDescriptions] PRIMARY KEY CLUSTERED ([Seq], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Contains descriptions for custom columns as used for multi-language companies', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigCustomColumnsDescriptions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*MD - Future Use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigCustomColumnsDescriptions', 'COLUMN', N'GroupName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The header label to use to display the column', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigCustomColumnsDescriptions', 'COLUMN', N'Label'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Foreign Key id of the custom column', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigCustomColumnsDescriptions', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The UI culture code of this label', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigCustomColumnsDescriptions', 'COLUMN', N'UICultureName'
GO
