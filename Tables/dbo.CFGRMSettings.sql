CREATE TABLE [dbo].[CFGRMSettings]
(
[PKey] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OverUtilOption] [smallint] NOT NULL CONSTRAINT [DF__CFGRMSett__OverU__15D01CBC] DEFAULT ((1)),
[EmpUtilPercent] [smallint] NOT NULL CONSTRAINT [DF__CFGRMSett__EmpUt__16C440F5] DEFAULT ((105)),
[EmpUtilPlusPercent] [smallint] NOT NULL CONSTRAINT [DF__CFGRMSett__EmpUt__17B8652E] DEFAULT ((0)),
[EmpUtilMorePercent] [smallint] NOT NULL CONSTRAINT [DF__CFGRMSett__EmpUt__18AC8967] DEFAULT ((0)),
[UnderUtilPercent] [smallint] NOT NULL CONSTRAINT [DF__CFGRMSett__Under__19A0ADA0] DEFAULT ((95)),
[OverSchPercent] [smallint] NOT NULL CONSTRAINT [DF__CFGRMSett__OverS__1A94D1D9] DEFAULT ((105)),
[UnderSchPercent] [smallint] NOT NULL CONSTRAINT [DF__CFGRMSett__Under__1B88F612] DEFAULT ((95)),
[UseBookingForEmpHours] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGRMSett__UseBo__1C7D1A4B] DEFAULT ('N'),
[UseBookingForGenHours] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGRMSett__UseBo__1D713E84] DEFAULT ('N'),
[IncludeSoftBookedHours] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGRMSett__Inclu__1E6562BD] DEFAULT ('N'),
[ProvRateOption] [smallint] NOT NULL CONSTRAINT [DF__CFGRMSett__ProvR__1F5986F6] DEFAULT ((1)),
[Hrdecimals] [smallint] NOT NULL CONSTRAINT [DF__CFGRMSett__Hrdec__204DAB2F] DEFAULT ((0)),
[StartingDayOfWeek] [smallint] NOT NULL CONSTRAINT [DF__CFGRMSett__Start__2141CF68] DEFAULT ((2)),
[JTDDate] [datetime] NULL,
[HardBooked] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGRMSett__HardB__2235F3A1] DEFAULT ('N'),
[AmtDecimals] [smallint] NOT NULL CONSTRAINT [DF__CFGRMSett__AmtDe__6E7AE68A] DEFAULT ((0)),
[DefaultCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGRMSett__Defau__7DBD2A1A] DEFAULT (' '),
[CustGlobalRevenueSetting] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevenueCalculationsOption] [smallint] NOT NULL CONSTRAINT [DF__CFGRMSett__Reven__28C783B0] DEFAULT ((0)),
[QtyDecimals] [smallint] NOT NULL CONSTRAINT [DF__CFGRMSett__QtyDe__779B07C7] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGRMSettings] ADD CONSTRAINT [CFGRMSettingsPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
