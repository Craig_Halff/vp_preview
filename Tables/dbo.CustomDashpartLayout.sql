CREATE TABLE [dbo].[CustomDashpartLayout]
(
[LayoutID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DashpartID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RowPos] [smallint] NOT NULL CONSTRAINT [DF__CustomDas__RowPo__3A71329F] DEFAULT ((0)),
[ColPos] [smallint] NOT NULL CONSTRAINT [DF__CustomDas__ColPo__3B6556D8] DEFAULT ((0)),
[Width] [smallint] NOT NULL CONSTRAINT [DF__CustomDas__Width__3C597B11] DEFAULT ((0)),
[Height] [smallint] NOT NULL CONSTRAINT [DF__CustomDas__Heigh__3D4D9F4A] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomDashpartLayout] ADD CONSTRAINT [CustomDashpartLayoutPK] PRIMARY KEY CLUSTERED ([LayoutID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
