CREATE TABLE [dbo].[CCG_Email_Attachments]
(
[MessageSeq] [int] NOT NULL,
[Filename] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Item] [varbinary] (max) NULL,
[KeepAfterSent] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_Email__KeepA__0D3E0417] DEFAULT ('Y')
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_CCG_Email_Attachments] ON [dbo].[CCG_Email_Attachments] ([MessageSeq]) ON [PRIMARY]
GO
