CREATE TABLE [dbo].[SF255ProjectGraphics]
(
[SF255ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FederalInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF255Proj__Feder__2B783C7B] DEFAULT ('N'),
[LinkID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilePath] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderOfAppearance] [smallint] NOT NULL CONSTRAINT [DF__SF255Proj__Order__2C6C60B4] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF255ProjectGraphics] ADD CONSTRAINT [SF255ProjectGraphicsPK] PRIMARY KEY NONCLUSTERED ([SF255ID], [WBS1], [FederalInd], [LinkID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
