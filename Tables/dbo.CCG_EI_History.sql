CREATE TABLE [dbo].[CCG_EI_History]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[ActionTaken] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionDate] [datetime] NOT NULL,
[ActionTakenBy] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionRecipient] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceStage] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PriorInvoiceStage] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PriorInvoiceStageDateSet] [datetime] NULL,
[DelegateFor] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Records history of significant EI actions taken', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_History', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date and time of the action', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_History', 'COLUMN', N'ActionDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'(Optional) The Vision employee who was the target of this action, if any', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_History', 'COLUMN', N'ActionRecipient'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Short description of action taken that triggered the history record', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_History', 'COLUMN', N'ActionTaken'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The process or Vision employee that did the action', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_History', 'COLUMN', N'ActionTakenBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The delegator employee id, if the action was carried out by a delegate', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_History', 'COLUMN', N'DelegateFor'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The new invoice Stage of the record related to the action', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_History', 'COLUMN', N'InvoiceStage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The previous invoice Stage of the record related to the action, if any', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_History', 'COLUMN', N'PriorInvoiceStage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date when the previous invoice Stage was set', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_History', 'COLUMN', N'PriorInvoiceStageDateSet'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_History', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The project number related to this action', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_History', 'COLUMN', N'WBS1'
GO
