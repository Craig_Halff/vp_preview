CREATE TABLE [dbo].[CCG_PAT_ConfigCustomColumns]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[Label] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DataType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayOrder] [int] NULL,
[InitialValue] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DatabaseField] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ColumnUpdate] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Width] [int] NULL,
[Decimals] [int] NULL,
[ViewRoles] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EditRoles] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequiredRoles] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequiredExpression] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Link] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EditOptions] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnabledWBS1] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnabledWBS2] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnabledWBS3] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_PAT_C__Statu__7D2567A4] DEFAULT ('A'),
[ModDate] [datetime] NOT NULL,
[ModUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DbColumnName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GroupName] [varchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_ConfigCustomColumns] ADD CONSTRAINT [PK_CCG_PAT_ConfigCustomColumns] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'PAT custom column definitions', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The stored procedure to call when updating the custom column', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'ColumnUpdate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The db field in CCG_PAT_CustomColumns to read and write to for update-able fields', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'DatabaseField'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The type of data to be displayed (A = alphanumeric / C = currency / N = numeric / D = date / I = integer / P = percent / L = link / V = dropdown / T = time)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'DataType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The name of the database column to use for this custom column', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'DbColumnName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The number of decimals to display', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'Decimals'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Custom column description as used in header tooltip', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The sort/display order to use to position the custom column in the grid', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'DisplayOrder'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Can this column be edited after export to Vision? Y/N (Input columns only)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'EditOptions'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Vision roles that have edit rights for this custom column', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'EditRoles'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Is this custom column enabled for the top / invoice level? Y/N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'EnabledWBS1'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Is this custom column enabled for the second / line item level? Y/N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'EnabledWBS2'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'EnabledWBS3'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The table function/view to use to populate the display value', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'InitialValue'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Deprecated - see CCG_PAT_ConfigCustomColumnsDescriptions', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'Label'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The db function to use to produce the link (if any) for this custom column', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'Link'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Last modified date', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'ModDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Last modified user', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'ModUser'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Semicolon separated list of values to use in dropdown list for this column (e.g. Value1|Label1;Value2|Label2)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'RequiredExpression'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'RequiredRoles'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Status of the custom column - (A)ctive / (I)nactive', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'Status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Vision roles that have view rights for the custom column', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'ViewRoles'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The width for the custom column (0 = auto)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigCustomColumns', 'COLUMN', N'Width'
GO
