CREATE TABLE [dbo].[ActivitySubscr]
(
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActivityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActivitySubscr] ADD CONSTRAINT [ActivitySubscrPK] PRIMARY KEY NONCLUSTERED ([PartKey], [ActivityID], [UserName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
