CREATE TABLE [dbo].[PRUnitTemplate]
(
[UnitID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Unit] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UnitTable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlannedQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRUnitTem__Plann__0963E6F0] DEFAULT ((0)),
[PlannedCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRUnitTem__Plann__0A580B29] DEFAULT ((0)),
[PlannedBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRUnitTem__Plann__0B4C2F62] DEFAULT ((0)),
[CostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRUnitTem__CostR__0C40539B] DEFAULT ((0)),
[BillingRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRUnitTem__Billi__0D3477D4] DEFAULT ((0)),
[DirectAcctFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRUnitTem__Direc__0E289C0D] DEFAULT ('N'),
[SeqNo] [smallint] NOT NULL CONSTRAINT [DF__PRUnitTem__SeqNo__0F1CC046] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PRUnitTemplate] ADD CONSTRAINT [PRUnitTemplatePK] PRIMARY KEY NONCLUSTERED ([UnitID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
