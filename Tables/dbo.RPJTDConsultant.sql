CREATE TABLE [dbo].[RPJTDConsultant]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConsultantID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPJTDCons__Perio__558384E0] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPJTDCons__Perio__5677A919] DEFAULT ((0)),
[PostedFlg] [smallint] NOT NULL CONSTRAINT [DF__RPJTDCons__Poste__576BCD52] DEFAULT ((1)),
[JTDDate] [datetime] NULL,
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPJTDConsultant] ADD CONSTRAINT [RPJTDConsultantPK] PRIMARY KEY NONCLUSTERED ([TimePhaseID], [TaskID], [PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPJTDConsultantPTAIDX] ON [dbo].[RPJTDConsultant] ([PlanID], [TaskID], [ConsultantID], [StartDate], [EndDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPJTDConsultantAllIDX] ON [dbo].[RPJTDConsultant] ([PlanID], [TaskID], [ConsultantID], [StartDate], [EndDate], [PeriodCost], [PeriodBill]) ON [PRIMARY]
GO
