CREATE TABLE [dbo].[FW_UDICData]
(
[UDIC_ID] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TableName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuditingEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_UDICDa__Audit__0AB59BD7] DEFAULT ('Y'),
[AutoNumSrc] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoNumOverride] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_UDICDa__AutoN__0BA9C010] DEFAULT ('N'),
[AutoNumSeqStart] [int] NOT NULL CONSTRAINT [DF__FW_UDICDa__AutoN__0C9DE449] DEFAULT ((0)),
[AutoNumSeqLen] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__FW_UDICDa__AutoN__0D920882] DEFAULT ((0)),
[AutoNumSeqPos] [smallint] NOT NULL CONSTRAINT [DF__FW_UDICDa__AutoN__0E862CBB] DEFAULT ((1)),
[DisplayInTitle] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_UDICData] ADD CONSTRAINT [FW_UDICDataPK] PRIMARY KEY CLUSTERED ([UDIC_ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
