CREATE TABLE [dbo].[Projects_TasksTemplate]
(
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustTaskSort] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTaskPhaseCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTaskPhaseName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTaskCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTaskName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTaskFeeDirLab] [decimal] (19, 5) NOT NULL,
[CustTaskFeeDirExp] [decimal] (19, 5) NOT NULL,
[CustTaskConsultFee] [decimal] (19, 5) NOT NULL,
[CustTaskReimbAllowExp] [decimal] (19, 5) NOT NULL,
[CustTaskReimbAllowCons] [decimal] (19, 5) NOT NULL,
[CustTaskTotal] [decimal] (19, 5) NOT NULL,
[CustTaskFunctionalArea] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustTaskExpenseFee] [decimal] (19, 5) NOT NULL,
[CustTaskConsultantFee] [decimal] (19, 5) NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Projects_T__WBS1__21E57BF7] DEFAULT (' '),
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Projects_T__WBS2__22D9A030] DEFAULT (' '),
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Projects_T__WBS3__23CDC469] DEFAULT (' ')
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

		CREATE TRIGGER [dbo].[VisionAudit_Delete_Projects_TasksTemplate]
		ON [dbo].[Projects_TasksTemplate]
		FOR Delete
		NOT FOR REPLICATION
		AS BEGIN
		SET NOCOUNT ON
		DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
		SET @VisionAuditUser = dbo.FW_GetUsername()

		IF @VisionAuditUser = ''
		RETURN

		SET @source = dbo.GetVisionAuditSource()
		SET @app = (SELECT TOP 1 lastapp FROM FW_Useractivity WHERE userid = @VisionAuditUser ORDER BY lastaccess DESC)
		SET @now = dbo.GetVisionAuditTime()
		SET @table = 'Projects_TasksTemplate'
	
		DECLARE @noAuditDetails varchar(1)
		SET @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		IF EXISTS(SELECT AuditKeyValuesDelete FROM FW_CFGSystem WHERE AuditKeyValuesDelete = 'Y' and @noAuditDetails = 'Y')
		BEGIN
		DECLARE @placeholder varchar(1)
		END
		ELSE
		BEGIN
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'Seq', CONVERT(NVARCHAR(2000),[Seq],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'CustTaskSort', CONVERT(NVARCHAR(2000),[CustTaskSort],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'CustTaskPhaseCode', CONVERT(NVARCHAR(2000),[CustTaskPhaseCode],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'CustTaskPhaseName', CONVERT(NVARCHAR(2000),[CustTaskPhaseName],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'CustTaskCode', CONVERT(NVARCHAR(2000),[CustTaskCode],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'CustTaskName', CONVERT(NVARCHAR(2000),[CustTaskName],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'CustTaskFeeDirLab', CONVERT(NVARCHAR(2000),[CustTaskFeeDirLab],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'CustTaskFeeDirExp', CONVERT(NVARCHAR(2000),[CustTaskFeeDirExp],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'CustTaskConsultFee', CONVERT(NVARCHAR(2000),[CustTaskConsultFee],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'CustTaskReimbAllowExp', CONVERT(NVARCHAR(2000),[CustTaskReimbAllowExp],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'CustTaskReimbAllowCons', CONVERT(NVARCHAR(2000),[CustTaskReimbAllowCons],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'CustTaskTotal', CONVERT(NVARCHAR(2000),[CustTaskTotal],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'CustTaskFunctionalArea', CONVERT(NVARCHAR(2000),[CustTaskFunctionalArea],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'CustTaskExpenseFee', CONVERT(NVARCHAR(2000),[CustTaskExpenseFee],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'CustTaskConsultantFee', CONVERT(NVARCHAR(2000),[CustTaskConsultantFee],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'WBS1', CONVERT(NVARCHAR(2000),[WBS1],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'WBS2', CONVERT(NVARCHAR(2000),[WBS2],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'WBS3', CONVERT(NVARCHAR(2000),[WBS3],121), NULL, @source, @app
		FROM DELETED
	END
		SET NOCOUNT OFF 
		END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_Projects_TasksTemplate] ON [dbo].[Projects_TasksTemplate]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

		CREATE TRIGGER [dbo].[VisionAudit_Insert_Projects_TasksTemplate]
		ON [dbo].[Projects_TasksTemplate]
		FOR Insert
		NOT FOR REPLICATION
		AS BEGIN
		SET NOCOUNT ON
		DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
		SET @VisionAuditUser = dbo.FW_GetUsername()

		IF @VisionAuditUser = ''
		RETURN

		SET @source = dbo.GetVisionAuditSource()
		SET @app = (SELECT TOP 1 lastapp FROM FW_Useractivity WHERE userid = @VisionAuditUser ORDER BY lastaccess DESC)
		SET @now = dbo.GetVisionAuditTime()
		SET @table = 'Projects_TasksTemplate'
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'Seq', NULL, CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskSort', NULL, CONVERT(NVARCHAR(2000),[CustTaskSort],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskPhaseCode', NULL, CONVERT(NVARCHAR(2000),[CustTaskPhaseCode],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskPhaseName', NULL, CONVERT(NVARCHAR(2000),[CustTaskPhaseName],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskCode', NULL, CONVERT(NVARCHAR(2000),[CustTaskCode],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskName', NULL, CONVERT(NVARCHAR(2000),[CustTaskName],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskFeeDirLab', NULL, CONVERT(NVARCHAR(2000),[CustTaskFeeDirLab],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskFeeDirExp', NULL, CONVERT(NVARCHAR(2000),[CustTaskFeeDirExp],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskConsultFee', NULL, CONVERT(NVARCHAR(2000),[CustTaskConsultFee],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskReimbAllowExp', NULL, CONVERT(NVARCHAR(2000),[CustTaskReimbAllowExp],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskReimbAllowCons', NULL, CONVERT(NVARCHAR(2000),[CustTaskReimbAllowCons],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskTotal', NULL, CONVERT(NVARCHAR(2000),[CustTaskTotal],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskFunctionalArea', NULL, CONVERT(NVARCHAR(2000),[CustTaskFunctionalArea],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskExpenseFee', NULL, CONVERT(NVARCHAR(2000),[CustTaskExpenseFee],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskConsultantFee', NULL, CONVERT(NVARCHAR(2000),[CustTaskConsultantFee],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS1', NULL, CONVERT(NVARCHAR(2000),[WBS1],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS2', NULL, CONVERT(NVARCHAR(2000),[WBS2],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS3', NULL, CONVERT(NVARCHAR(2000),[WBS3],121), @source, @app
		FROM INSERTED
	
		SET NOCOUNT OFF 
		END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_Projects_TasksTemplate] ON [dbo].[Projects_TasksTemplate]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

		CREATE TRIGGER [dbo].[VisionAudit_Update_Projects_TasksTemplate]
		ON [dbo].[Projects_TasksTemplate]
		FOR Update
		NOT FOR REPLICATION
		AS BEGIN
		SET NOCOUNT ON
		DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
		SET @VisionAuditUser = dbo.FW_GetUsername()

		IF @VisionAuditUser = ''
		RETURN

		SET @source = dbo.GetVisionAuditSource()
		SET @app = (SELECT TOP 1 lastapp FROM FW_Useractivity WHERE userid = @VisionAuditUser ORDER BY lastaccess DESC)
		SET @now = dbo.GetVisionAuditTime()
		SET @table = 'Projects_TasksTemplate'
	
		IF UPDATE([Seq])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'Seq',
		CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
		CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] IS NULL AND
				DELETED.[Seq] IS NOT NULL
			) OR
			(
				INSERTED.[Seq] IS NOT NULL AND
				DELETED.[Seq] IS NULL
			) OR
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
	
		IF UPDATE([CustTaskSort])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskSort',
		CONVERT(NVARCHAR(2000),DELETED.[CustTaskSort],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustTaskSort],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustTaskSort] IS NULL AND
				DELETED.[CustTaskSort] IS NOT NULL
			) OR
			(
				INSERTED.[CustTaskSort] IS NOT NULL AND
				DELETED.[CustTaskSort] IS NULL
			) OR
			(
				INSERTED.[CustTaskSort] !=
				DELETED.[CustTaskSort]
			)
		) 
		END		
	
		IF UPDATE([CustTaskPhaseCode])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskPhaseCode',
		CONVERT(NVARCHAR(2000),DELETED.[CustTaskPhaseCode],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustTaskPhaseCode],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustTaskPhaseCode] IS NULL AND
				DELETED.[CustTaskPhaseCode] IS NOT NULL
			) OR
			(
				INSERTED.[CustTaskPhaseCode] IS NOT NULL AND
				DELETED.[CustTaskPhaseCode] IS NULL
			) OR
			(
				INSERTED.[CustTaskPhaseCode] !=
				DELETED.[CustTaskPhaseCode]
			)
		) 
		END		
	
		IF UPDATE([CustTaskPhaseName])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskPhaseName',
		CONVERT(NVARCHAR(2000),DELETED.[CustTaskPhaseName],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustTaskPhaseName],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustTaskPhaseName] IS NULL AND
				DELETED.[CustTaskPhaseName] IS NOT NULL
			) OR
			(
				INSERTED.[CustTaskPhaseName] IS NOT NULL AND
				DELETED.[CustTaskPhaseName] IS NULL
			) OR
			(
				INSERTED.[CustTaskPhaseName] !=
				DELETED.[CustTaskPhaseName]
			)
		) 
		END		
	
		IF UPDATE([CustTaskCode])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskCode',
		CONVERT(NVARCHAR(2000),DELETED.[CustTaskCode],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustTaskCode],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustTaskCode] IS NULL AND
				DELETED.[CustTaskCode] IS NOT NULL
			) OR
			(
				INSERTED.[CustTaskCode] IS NOT NULL AND
				DELETED.[CustTaskCode] IS NULL
			) OR
			(
				INSERTED.[CustTaskCode] !=
				DELETED.[CustTaskCode]
			)
		) 
		END		
	
		IF UPDATE([CustTaskName])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskName',
		CONVERT(NVARCHAR(2000),DELETED.[CustTaskName],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustTaskName],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustTaskName] IS NULL AND
				DELETED.[CustTaskName] IS NOT NULL
			) OR
			(
				INSERTED.[CustTaskName] IS NOT NULL AND
				DELETED.[CustTaskName] IS NULL
			) OR
			(
				INSERTED.[CustTaskName] !=
				DELETED.[CustTaskName]
			)
		) 
		END		
	
		IF UPDATE([CustTaskFeeDirLab])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskFeeDirLab',
		CONVERT(NVARCHAR(2000),DELETED.[CustTaskFeeDirLab],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustTaskFeeDirLab],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustTaskFeeDirLab] IS NULL AND
				DELETED.[CustTaskFeeDirLab] IS NOT NULL
			) OR
			(
				INSERTED.[CustTaskFeeDirLab] IS NOT NULL AND
				DELETED.[CustTaskFeeDirLab] IS NULL
			) OR
			(
				INSERTED.[CustTaskFeeDirLab] !=
				DELETED.[CustTaskFeeDirLab]
			)
		) 
		END		
	
		IF UPDATE([CustTaskFeeDirExp])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskFeeDirExp',
		CONVERT(NVARCHAR(2000),DELETED.[CustTaskFeeDirExp],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustTaskFeeDirExp],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustTaskFeeDirExp] IS NULL AND
				DELETED.[CustTaskFeeDirExp] IS NOT NULL
			) OR
			(
				INSERTED.[CustTaskFeeDirExp] IS NOT NULL AND
				DELETED.[CustTaskFeeDirExp] IS NULL
			) OR
			(
				INSERTED.[CustTaskFeeDirExp] !=
				DELETED.[CustTaskFeeDirExp]
			)
		) 
		END		
	
		IF UPDATE([CustTaskConsultFee])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskConsultFee',
		CONVERT(NVARCHAR(2000),DELETED.[CustTaskConsultFee],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustTaskConsultFee],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustTaskConsultFee] IS NULL AND
				DELETED.[CustTaskConsultFee] IS NOT NULL
			) OR
			(
				INSERTED.[CustTaskConsultFee] IS NOT NULL AND
				DELETED.[CustTaskConsultFee] IS NULL
			) OR
			(
				INSERTED.[CustTaskConsultFee] !=
				DELETED.[CustTaskConsultFee]
			)
		) 
		END		
	
		IF UPDATE([CustTaskReimbAllowExp])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskReimbAllowExp',
		CONVERT(NVARCHAR(2000),DELETED.[CustTaskReimbAllowExp],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustTaskReimbAllowExp],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustTaskReimbAllowExp] IS NULL AND
				DELETED.[CustTaskReimbAllowExp] IS NOT NULL
			) OR
			(
				INSERTED.[CustTaskReimbAllowExp] IS NOT NULL AND
				DELETED.[CustTaskReimbAllowExp] IS NULL
			) OR
			(
				INSERTED.[CustTaskReimbAllowExp] !=
				DELETED.[CustTaskReimbAllowExp]
			)
		) 
		END		
	
		IF UPDATE([CustTaskReimbAllowCons])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskReimbAllowCons',
		CONVERT(NVARCHAR(2000),DELETED.[CustTaskReimbAllowCons],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustTaskReimbAllowCons],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustTaskReimbAllowCons] IS NULL AND
				DELETED.[CustTaskReimbAllowCons] IS NOT NULL
			) OR
			(
				INSERTED.[CustTaskReimbAllowCons] IS NOT NULL AND
				DELETED.[CustTaskReimbAllowCons] IS NULL
			) OR
			(
				INSERTED.[CustTaskReimbAllowCons] !=
				DELETED.[CustTaskReimbAllowCons]
			)
		) 
		END		
	
		IF UPDATE([CustTaskTotal])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskTotal',
		CONVERT(NVARCHAR(2000),DELETED.[CustTaskTotal],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustTaskTotal],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustTaskTotal] IS NULL AND
				DELETED.[CustTaskTotal] IS NOT NULL
			) OR
			(
				INSERTED.[CustTaskTotal] IS NOT NULL AND
				DELETED.[CustTaskTotal] IS NULL
			) OR
			(
				INSERTED.[CustTaskTotal] !=
				DELETED.[CustTaskTotal]
			)
		) 
		END		
	
		IF UPDATE([CustTaskFunctionalArea])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskFunctionalArea',
		CONVERT(NVARCHAR(2000),DELETED.[CustTaskFunctionalArea],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustTaskFunctionalArea],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustTaskFunctionalArea] IS NULL AND
				DELETED.[CustTaskFunctionalArea] IS NOT NULL
			) OR
			(
				INSERTED.[CustTaskFunctionalArea] IS NOT NULL AND
				DELETED.[CustTaskFunctionalArea] IS NULL
			) OR
			(
				INSERTED.[CustTaskFunctionalArea] !=
				DELETED.[CustTaskFunctionalArea]
			)
		) 
		END		
	
		IF UPDATE([CustTaskExpenseFee])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskExpenseFee',
		CONVERT(NVARCHAR(2000),DELETED.[CustTaskExpenseFee],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustTaskExpenseFee],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustTaskExpenseFee] IS NULL AND
				DELETED.[CustTaskExpenseFee] IS NOT NULL
			) OR
			(
				INSERTED.[CustTaskExpenseFee] IS NOT NULL AND
				DELETED.[CustTaskExpenseFee] IS NULL
			) OR
			(
				INSERTED.[CustTaskExpenseFee] !=
				DELETED.[CustTaskExpenseFee]
			)
		) 
		END		
	
		IF UPDATE([CustTaskConsultantFee])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustTaskConsultantFee',
		CONVERT(NVARCHAR(2000),DELETED.[CustTaskConsultantFee],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustTaskConsultantFee],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustTaskConsultantFee] IS NULL AND
				DELETED.[CustTaskConsultantFee] IS NOT NULL
			) OR
			(
				INSERTED.[CustTaskConsultantFee] IS NOT NULL AND
				DELETED.[CustTaskConsultantFee] IS NULL
			) OR
			(
				INSERTED.[CustTaskConsultantFee] !=
				DELETED.[CustTaskConsultantFee]
			)
		) 
		END		
	
		IF UPDATE([WBS1])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS1',
		CONVERT(NVARCHAR(2000),DELETED.[WBS1],121),
		CONVERT(NVARCHAR(2000),INSERTED.[WBS1],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS1] IS NULL AND
				DELETED.[WBS1] IS NOT NULL
			) OR
			(
				INSERTED.[WBS1] IS NOT NULL AND
				DELETED.[WBS1] IS NULL
			) OR
			(
				INSERTED.[WBS1] !=
				DELETED.[WBS1]
			)
		) 
		END		
	
		IF UPDATE([WBS2])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS2',
		CONVERT(NVARCHAR(2000),DELETED.[WBS2],121),
		CONVERT(NVARCHAR(2000),INSERTED.[WBS2],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS2] IS NULL AND
				DELETED.[WBS2] IS NOT NULL
			) OR
			(
				INSERTED.[WBS2] IS NOT NULL AND
				DELETED.[WBS2] IS NULL
			) OR
			(
				INSERTED.[WBS2] !=
				DELETED.[WBS2]
			)
		) 
		END		
	
		IF UPDATE([WBS3])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS3',
		CONVERT(NVARCHAR(2000),DELETED.[WBS3],121),
		CONVERT(NVARCHAR(2000),INSERTED.[WBS3],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS3] IS NULL AND
				DELETED.[WBS3] IS NOT NULL
			) OR
			(
				INSERTED.[WBS3] IS NOT NULL AND
				DELETED.[WBS3] IS NULL
			) OR
			(
				INSERTED.[WBS3] !=
				DELETED.[WBS3]
			)
		) 
		END		
	
		SET NOCOUNT OFF 
		END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_Projects_TasksTemplate] ON [dbo].[Projects_TasksTemplate]
GO
ALTER TABLE [dbo].[Projects_TasksTemplate] ADD CONSTRAINT [Projects_TasksTemplatePK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [Seq]) ON [PRIMARY]
GO
