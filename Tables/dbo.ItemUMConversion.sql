CREATE TABLE [dbo].[ItemUMConversion]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Item] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrderUOM] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Factor] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ItemUMCon__Facto__5A9C7461] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ItemUMConversion] ADD CONSTRAINT [ItemUMConversionPK] PRIMARY KEY NONCLUSTERED ([Company], [Item], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
