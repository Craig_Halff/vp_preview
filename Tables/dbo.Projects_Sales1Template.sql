CREATE TABLE [dbo].[Projects_Sales1Template]
(
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustSalesTeam] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustSalesAmount] [decimal] (19, 5) NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Projects_S__WBS1__12A33867] DEFAULT (' '),
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Projects_S__WBS2__13975CA0] DEFAULT (' '),
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Projects_S__WBS3__148B80D9] DEFAULT (' ')
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

		CREATE TRIGGER [dbo].[VisionAudit_Delete_Projects_Sales1Template]
		ON [dbo].[Projects_Sales1Template]
		FOR Delete
		NOT FOR REPLICATION
		AS BEGIN
		SET NOCOUNT ON
		DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
		SET @VisionAuditUser = dbo.FW_GetUsername()

		IF @VisionAuditUser = ''
		RETURN

		SET @source = dbo.GetVisionAuditSource()
		SET @app = (SELECT TOP 1 lastapp FROM FW_Useractivity WHERE userid = @VisionAuditUser ORDER BY lastaccess DESC)
		SET @now = dbo.GetVisionAuditTime()
		SET @table = 'Projects_Sales1Template'
	
		DECLARE @noAuditDetails varchar(1)
		SET @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		IF EXISTS(SELECT AuditKeyValuesDelete FROM FW_CFGSystem WHERE AuditKeyValuesDelete = 'Y' and @noAuditDetails = 'Y')
		BEGIN
		DECLARE @placeholder varchar(1)
		END
		ELSE
		BEGIN
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'Seq', CONVERT(NVARCHAR(2000),[Seq],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'CustSalesTeam', CONVERT(NVARCHAR(2000),[CustSalesTeam],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'CustSalesAmount', CONVERT(NVARCHAR(2000),[CustSalesAmount],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'WBS1', CONVERT(NVARCHAR(2000),[WBS1],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'WBS2', CONVERT(NVARCHAR(2000),[WBS2],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121), 'WBS3', CONVERT(NVARCHAR(2000),[WBS3],121), NULL, @source, @app
		FROM DELETED
	END
		SET NOCOUNT OFF 
		END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_Projects_Sales1Template] ON [dbo].[Projects_Sales1Template]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

		CREATE TRIGGER [dbo].[VisionAudit_Insert_Projects_Sales1Template]
		ON [dbo].[Projects_Sales1Template]
		FOR Insert
		NOT FOR REPLICATION
		AS BEGIN
		SET NOCOUNT ON
		DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
		SET @VisionAuditUser = dbo.FW_GetUsername()

		IF @VisionAuditUser = ''
		RETURN

		SET @source = dbo.GetVisionAuditSource()
		SET @app = (SELECT TOP 1 lastapp FROM FW_Useractivity WHERE userid = @VisionAuditUser ORDER BY lastaccess DESC)
		SET @now = dbo.GetVisionAuditTime()
		SET @table = 'Projects_Sales1Template'
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'Seq', NULL, CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustSalesTeam', NULL, CONVERT(NVARCHAR(2000),[CustSalesTeam],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustSalesAmount', NULL, CONVERT(NVARCHAR(2000),[CustSalesAmount],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS1', NULL, CONVERT(NVARCHAR(2000),[WBS1],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS2', NULL, CONVERT(NVARCHAR(2000),[WBS2],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS3', NULL, CONVERT(NVARCHAR(2000),[WBS3],121), @source, @app
		FROM INSERTED
	
		SET NOCOUNT OFF 
		END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_Projects_Sales1Template] ON [dbo].[Projects_Sales1Template]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

		CREATE TRIGGER [dbo].[VisionAudit_Update_Projects_Sales1Template]
		ON [dbo].[Projects_Sales1Template]
		FOR Update
		NOT FOR REPLICATION
		AS BEGIN
		SET NOCOUNT ON
		DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
		SET @VisionAuditUser = dbo.FW_GetUsername()

		IF @VisionAuditUser = ''
		RETURN

		SET @source = dbo.GetVisionAuditSource()
		SET @app = (SELECT TOP 1 lastapp FROM FW_Useractivity WHERE userid = @VisionAuditUser ORDER BY lastaccess DESC)
		SET @now = dbo.GetVisionAuditTime()
		SET @table = 'Projects_Sales1Template'
	
		IF UPDATE([Seq])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'Seq',
		CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
		CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] IS NULL AND
				DELETED.[Seq] IS NOT NULL
			) OR
			(
				INSERTED.[Seq] IS NOT NULL AND
				DELETED.[Seq] IS NULL
			) OR
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
	
		IF UPDATE([CustSalesTeam])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustSalesTeam',
		CONVERT(NVARCHAR(2000),DELETED.[CustSalesTeam],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustSalesTeam],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustSalesTeam] IS NULL AND
				DELETED.[CustSalesTeam] IS NOT NULL
			) OR
			(
				INSERTED.[CustSalesTeam] IS NOT NULL AND
				DELETED.[CustSalesTeam] IS NULL
			) OR
			(
				INSERTED.[CustSalesTeam] !=
				DELETED.[CustSalesTeam]
			)
		) 
		END		
	
		IF UPDATE([CustSalesAmount])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'CustSalesAmount',
		CONVERT(NVARCHAR(2000),DELETED.[CustSalesAmount],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustSalesAmount],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustSalesAmount] IS NULL AND
				DELETED.[CustSalesAmount] IS NOT NULL
			) OR
			(
				INSERTED.[CustSalesAmount] IS NOT NULL AND
				DELETED.[CustSalesAmount] IS NULL
			) OR
			(
				INSERTED.[CustSalesAmount] !=
				DELETED.[CustSalesAmount]
			)
		) 
		END		
	
		IF UPDATE([WBS1])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS1',
		CONVERT(NVARCHAR(2000),DELETED.[WBS1],121),
		CONVERT(NVARCHAR(2000),INSERTED.[WBS1],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS1] IS NULL AND
				DELETED.[WBS1] IS NOT NULL
			) OR
			(
				INSERTED.[WBS1] IS NOT NULL AND
				DELETED.[WBS1] IS NULL
			) OR
			(
				INSERTED.[WBS1] !=
				DELETED.[WBS1]
			)
		) 
		END		
	
		IF UPDATE([WBS2])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS2',
		CONVERT(NVARCHAR(2000),DELETED.[WBS2],121),
		CONVERT(NVARCHAR(2000),INSERTED.[WBS2],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS2] IS NULL AND
				DELETED.[WBS2] IS NOT NULL
			) OR
			(
				INSERTED.[WBS2] IS NOT NULL AND
				DELETED.[WBS2] IS NULL
			) OR
			(
				INSERTED.[WBS2] !=
				DELETED.[WBS2]
			)
		) 
		END		
	
		IF UPDATE([WBS3])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121), 'WBS3',
		CONVERT(NVARCHAR(2000),DELETED.[WBS3],121),
		CONVERT(NVARCHAR(2000),INSERTED.[WBS3],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[WBS3] IS NULL AND
				DELETED.[WBS3] IS NOT NULL
			) OR
			(
				INSERTED.[WBS3] IS NOT NULL AND
				DELETED.[WBS3] IS NULL
			) OR
			(
				INSERTED.[WBS3] !=
				DELETED.[WBS3]
			)
		) 
		END		
	
		SET NOCOUNT OFF 
		END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_Projects_Sales1Template] ON [dbo].[Projects_Sales1Template]
GO
ALTER TABLE [dbo].[Projects_Sales1Template] ADD CONSTRAINT [Projects_Sales1TemplatePK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [Seq]) ON [PRIMARY]
GO
