CREATE TABLE [dbo].[AccountCustomTabFields]
(
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustAccountDescription] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPATApprover1] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPATApprover2] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPATApprover3] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPATThreshold1] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__AccountCu__CustP__77EC14CF] DEFAULT ((0)),
[CustPATThreshold2] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__AccountCu__CustP__78E03908] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_AccountCustomTabFields]
      ON [dbo].[AccountCustomTabFields]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'AccountCustomTabFields'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'Account',CONVERT(NVARCHAR(2000),[Account],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'CustAccountDescription','[text]',NULL, @source,@app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'CustPATApprover1',CONVERT(NVARCHAR(2000),DELETED.[CustPATApprover1],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustPATApprover1 = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'CustPATApprover2',CONVERT(NVARCHAR(2000),DELETED.[CustPATApprover2],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustPATApprover2 = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'CustPATApprover3',CONVERT(NVARCHAR(2000),DELETED.[CustPATApprover3],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustPATApprover3 = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'CustPATThreshold1',CONVERT(NVARCHAR(2000),[CustPATThreshold1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'CustPATThreshold2',CONVERT(NVARCHAR(2000),[CustPATThreshold2],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_AccountCustomTabFields] ON [dbo].[AccountCustomTabFields]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_AccountCustomTabFields]
      ON [dbo].[AccountCustomTabFields]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'AccountCustomTabFields'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'Account',NULL,CONVERT(NVARCHAR(2000),[Account],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CustAccountDescription',NULL,'[text]', @source, @app
      FROM INSERTED


    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CustPATApprover1',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustPATApprover1],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustPATApprover1 = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CustPATApprover2',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustPATApprover2],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustPATApprover2 = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CustPATApprover3',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustPATApprover3],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustPATApprover3 = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CustPATThreshold1',NULL,CONVERT(NVARCHAR(2000),[CustPATThreshold1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CustPATThreshold2',NULL,CONVERT(NVARCHAR(2000),[CustPATThreshold2],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_AccountCustomTabFields] ON [dbo].[AccountCustomTabFields]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_AccountCustomTabFields]
      ON [dbo].[AccountCustomTabFields]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'AccountCustomTabFields'
    
      If UPDATE([Account])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'Account',
      CONVERT(NVARCHAR(2000),DELETED.[Account],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Account],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[Account] Is Null And
				DELETED.[Account] Is Not Null
			) Or
			(
				INSERTED.[Account] Is Not Null And
				DELETED.[Account] Is Null
			) Or
			(
				INSERTED.[Account] !=
				DELETED.[Account]
			)
		) 
		END		
		
      If UPDATE([CustAccountDescription])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CustAccountDescription',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
     If UPDATE([CustPATApprover1])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CustPATApprover1',
     CONVERT(NVARCHAR(2000),DELETED.[CustPATApprover1],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustPATApprover1],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[CustPATApprover1] Is Null And
				DELETED.[CustPATApprover1] Is Not Null
			) Or
			(
				INSERTED.[CustPATApprover1] Is Not Null And
				DELETED.[CustPATApprover1] Is Null
			) Or
			(
				INSERTED.[CustPATApprover1] !=
				DELETED.[CustPATApprover1]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustPATApprover1 = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustPATApprover1 = newDesc.Employee
		END		
		
     If UPDATE([CustPATApprover2])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CustPATApprover2',
     CONVERT(NVARCHAR(2000),DELETED.[CustPATApprover2],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustPATApprover2],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[CustPATApprover2] Is Null And
				DELETED.[CustPATApprover2] Is Not Null
			) Or
			(
				INSERTED.[CustPATApprover2] Is Not Null And
				DELETED.[CustPATApprover2] Is Null
			) Or
			(
				INSERTED.[CustPATApprover2] !=
				DELETED.[CustPATApprover2]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustPATApprover2 = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustPATApprover2 = newDesc.Employee
		END		
		
     If UPDATE([CustPATApprover3])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CustPATApprover3',
     CONVERT(NVARCHAR(2000),DELETED.[CustPATApprover3],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustPATApprover3],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[CustPATApprover3] Is Null And
				DELETED.[CustPATApprover3] Is Not Null
			) Or
			(
				INSERTED.[CustPATApprover3] Is Not Null And
				DELETED.[CustPATApprover3] Is Null
			) Or
			(
				INSERTED.[CustPATApprover3] !=
				DELETED.[CustPATApprover3]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustPATApprover3 = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustPATApprover3 = newDesc.Employee
		END		
		
      If UPDATE([CustPATThreshold1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CustPATThreshold1',
      CONVERT(NVARCHAR(2000),DELETED.[CustPATThreshold1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPATThreshold1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[CustPATThreshold1] Is Null And
				DELETED.[CustPATThreshold1] Is Not Null
			) Or
			(
				INSERTED.[CustPATThreshold1] Is Not Null And
				DELETED.[CustPATThreshold1] Is Null
			) Or
			(
				INSERTED.[CustPATThreshold1] !=
				DELETED.[CustPATThreshold1]
			)
		) 
		END		
		
      If UPDATE([CustPATThreshold2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CustPATThreshold2',
      CONVERT(NVARCHAR(2000),DELETED.[CustPATThreshold2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustPATThreshold2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[CustPATThreshold2] Is Null And
				DELETED.[CustPATThreshold2] Is Not Null
			) Or
			(
				INSERTED.[CustPATThreshold2] Is Not Null And
				DELETED.[CustPATThreshold2] Is Null
			) Or
			(
				INSERTED.[CustPATThreshold2] !=
				DELETED.[CustPATThreshold2]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_AccountCustomTabFields] ON [dbo].[AccountCustomTabFields]
GO
ALTER TABLE [dbo].[AccountCustomTabFields] ADD CONSTRAINT [AccountCustomTabFieldsPK] PRIMARY KEY CLUSTERED ([Account]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
