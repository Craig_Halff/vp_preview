CREATE TABLE [dbo].[AnalysisCubesKPIData]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Measure] [nvarchar] (800) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MeasureType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Dimension] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusIndicator] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PeriodMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Method] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Goal] [decimal] (19, 4) NOT NULL,
[GoalLowValue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__AnalysisC__GoalL__415AF9F4] DEFAULT ((0)),
[GoalHighValue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__AnalysisC__GoalH__424F1E2D] DEFAULT ((0)),
[TrendIndicator] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrendComparison] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrendLowValue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__AnalysisC__Trend__43434266] DEFAULT ((0)),
[TrendHighValue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__AnalysisC__Trend__4437669F] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AnalysisCubesKPIData] ADD CONSTRAINT [AnalysisCubesKPIDataPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
