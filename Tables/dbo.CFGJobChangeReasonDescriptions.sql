CREATE TABLE [dbo].[CFGJobChangeReasonDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGJobChang__Seq__63734832] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGJobChangeReasonDescriptions] ADD CONSTRAINT [CFGJobChangeReasonDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
