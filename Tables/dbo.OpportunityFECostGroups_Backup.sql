CREATE TABLE [dbo].[OpportunityFECostGroups_Backup]
(
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FuncGroupCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CostGroupCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChargeableCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportuni__Charg__3461BCE0] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OpportunityFECostGroups_Backup] ADD CONSTRAINT [OpportunityFECostGroupsPK] PRIMARY KEY NONCLUSTERED ([OpportunityID], [FuncGroupCode], [CostGroupCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
