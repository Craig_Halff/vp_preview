CREATE TABLE [dbo].[CFGPOStatusDescriptions]
(
[Screen] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPOStatusDescriptions] ADD CONSTRAINT [CFGPOStatusDescriptionsPK] PRIMARY KEY CLUSTERED ([Screen], [Status], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
