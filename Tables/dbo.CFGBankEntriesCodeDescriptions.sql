CREATE TABLE [dbo].[CFGBankEntriesCodeDescriptions]
(
[EntryCode] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGBankEntriesCodeDescriptions] ADD CONSTRAINT [CFGBankEntriesCodeDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [EntryCode], [UICultureName]) ON [PRIMARY]
GO
