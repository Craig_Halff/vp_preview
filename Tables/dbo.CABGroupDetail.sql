CREATE TABLE [dbo].[CABGroupDetail]
(
[GroupBudgetName] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BudgetName] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CABGroupDetail] ADD CONSTRAINT [CABGroupDetailPK] PRIMARY KEY NONCLUSTERED ([GroupBudgetName], [BudgetName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
