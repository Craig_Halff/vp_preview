CREATE TABLE [dbo].[FirmServiceFees]
(
[FirmID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Year] [smallint] NOT NULL CONSTRAINT [DF__FirmServic__Year__7C1276C5] DEFAULT ((0)),
[Category] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FeeIndex] [smallint] NOT NULL CONSTRAINT [DF__FirmServi__FeeIn__7D069AFE] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FirmServiceFees] ADD CONSTRAINT [FirmServiceFeesPK] PRIMARY KEY NONCLUSTERED ([FirmID], [Year], [Category]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
