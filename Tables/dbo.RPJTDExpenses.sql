CREATE TABLE [dbo].[RPJTDExpenses]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExpenseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPJTDExpe__Perio__5B3C5E36] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPJTDExpe__Perio__5C30826F] DEFAULT ((0)),
[PostedFlg] [smallint] NOT NULL CONSTRAINT [DF__RPJTDExpe__Poste__5D24A6A8] DEFAULT ((1)),
[JTDDate] [datetime] NULL,
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPJTDExpenses] ADD CONSTRAINT [RPJTDExpensesPK] PRIMARY KEY NONCLUSTERED ([TimePhaseID], [TaskID], [PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPJTDExpensesPTAIDX] ON [dbo].[RPJTDExpenses] ([PlanID], [TaskID], [ExpenseID], [StartDate], [EndDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPJTDExpensesAllIDX] ON [dbo].[RPJTDExpenses] ([PlanID], [TaskID], [ExpenseID], [StartDate], [EndDate], [PeriodCost], [PeriodBill]) ON [PRIMARY]
GO
