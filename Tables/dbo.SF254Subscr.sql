CREATE TABLE [dbo].[SF254Subscr]
(
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SF254ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF254Subscr] ADD CONSTRAINT [SF254SubscrPK] PRIMARY KEY NONCLUSTERED ([PartKey], [SF254ID], [UserName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
