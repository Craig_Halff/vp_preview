CREATE TABLE [dbo].[OppUnit_Backup]
(
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UnitID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Unit] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UnitTable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlannedQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OppUnit_N__Plann__7EC4C63E] DEFAULT ((0)),
[PlannedCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OppUnit_N__Plann__7FB8EA77] DEFAULT ((0)),
[PlannedBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OppUnit_N__Plann__00AD0EB0] DEFAULT ((0)),
[CostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OppUnit_N__CostR__01A132E9] DEFAULT ((0)),
[BillingRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__OppUnit_N__Billi__02955722] DEFAULT ((0)),
[DirectAcctFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__OppUnit_N__Direc__03897B5B] DEFAULT ('N'),
[SeqNo] [smallint] NOT NULL CONSTRAINT [DF__OppUnit_N__SeqNo__047D9F94] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OppUnit_Backup] ADD CONSTRAINT [OppUnitPK] PRIMARY KEY NONCLUSTERED ([UnitID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
