CREATE TABLE [dbo].[SF330ProjectFirms]
(
[SF330ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProjID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProjFirmSeq] [smallint] NOT NULL CONSTRAINT [DF__SF330Proj__ProjF__68814ABB] DEFAULT ((0)),
[Name] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Location] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Role] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF330ProjectFirms] ADD CONSTRAINT [SF330ProjectFirmsPK] PRIMARY KEY NONCLUSTERED ([SF330ID], [ProjID], [ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
