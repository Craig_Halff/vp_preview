CREATE TABLE [dbo].[CFGDatesStatus]
(
[Period] [int] NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Closed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGDatesS__Close__267CE686] DEFAULT ('N'),
[OpenedDate] [datetime] NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_CFGDatesStatus]
      ON [dbo].[CFGDatesStatus]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CFGDatesStatus'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Period],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'Period',CONVERT(NVARCHAR(2000),[Period],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Period],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'Company',CONVERT(NVARCHAR(2000),[Company],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Period],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'Closed',CONVERT(NVARCHAR(2000),[Closed],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Period],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Company],121),'OpenedDate',CONVERT(NVARCHAR(2000),[OpenedDate],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_CFGDatesStatus]
      ON [dbo].[CFGDatesStatus]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CFGDatesStatus'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Period',NULL,CONVERT(NVARCHAR(2000),[Period],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Company',NULL,CONVERT(NVARCHAR(2000),[Company],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Closed',NULL,CONVERT(NVARCHAR(2000),[Closed],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'OpenedDate',NULL,CONVERT(NVARCHAR(2000),[OpenedDate],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_CFGDatesStatus]
      ON [dbo].[CFGDatesStatus]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CFGDatesStatus'
    
      If UPDATE([Period])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Period',
      CONVERT(NVARCHAR(2000),DELETED.[Period],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Period],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Period] = DELETED.[Period] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[Period] Is Null And
				DELETED.[Period] Is Not Null
			) Or
			(
				INSERTED.[Period] Is Not Null And
				DELETED.[Period] Is Null
			) Or
			(
				INSERTED.[Period] !=
				DELETED.[Period]
			)
		) 
		END		
		
      If UPDATE([Company])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Company',
      CONVERT(NVARCHAR(2000),DELETED.[Company],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Company],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Period] = DELETED.[Period] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[Company] Is Null And
				DELETED.[Company] Is Not Null
			) Or
			(
				INSERTED.[Company] Is Not Null And
				DELETED.[Company] Is Null
			) Or
			(
				INSERTED.[Company] !=
				DELETED.[Company]
			)
		) 
		END		
		
      If UPDATE([Closed])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'Closed',
      CONVERT(NVARCHAR(2000),DELETED.[Closed],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Closed],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Period] = DELETED.[Period] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[Closed] Is Null And
				DELETED.[Closed] Is Not Null
			) Or
			(
				INSERTED.[Closed] Is Not Null And
				DELETED.[Closed] Is Null
			) Or
			(
				INSERTED.[Closed] !=
				DELETED.[Closed]
			)
		) 
		END		
		
      If UPDATE([OpenedDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Period],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Company],121),'OpenedDate',
      CONVERT(NVARCHAR(2000),DELETED.[OpenedDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OpenedDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Period] = DELETED.[Period] AND INSERTED.[Company] = DELETED.[Company] AND 
		(
			(
				INSERTED.[OpenedDate] Is Null And
				DELETED.[OpenedDate] Is Not Null
			) Or
			(
				INSERTED.[OpenedDate] Is Not Null And
				DELETED.[OpenedDate] Is Null
			) Or
			(
				INSERTED.[OpenedDate] !=
				DELETED.[OpenedDate]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[CFGDatesStatus] ADD CONSTRAINT [CFGDatesStatusPK] PRIMARY KEY CLUSTERED ([Period], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
