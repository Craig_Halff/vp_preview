CREATE TABLE [dbo].[FW_DisplaySettingsData]
(
[Settings_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UISettings] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_DisplaySettingsData] ADD CONSTRAINT [FW_DisplaySettingsDataPK] PRIMARY KEY CLUSTERED ([Settings_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
