CREATE TABLE [dbo].[InvoiceApproval]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PreInvoice] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__InvoiceAp__PreIn__3FE87E25] DEFAULT ('N'),
[InvoiceStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodStartDate] [datetime] NULL,
[PeriodEndDate] [datetime] NULL,
[BillThruDate] [datetime] NULL,
[BillThruPeriod] [int] NOT NULL CONSTRAINT [DF__InvoiceAp__BillT__40DCA25E] DEFAULT ((0)),
[ScheduledThruDate] [datetime] NULL,
[SubmitReturnString] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceAddressee] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__InvoiceAp__Invoi__41D0C697] DEFAULT ('0')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[InvoiceApproval] ADD CONSTRAINT [InvoiceApprovalPK] PRIMARY KEY NONCLUSTERED ([WBS1], [Invoice], [PreInvoice]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
