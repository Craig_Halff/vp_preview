CREATE TABLE [dbo].[_HAI_ClosedInactiveRevImport]
(
[WBS1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Lab_Rev] [decimal] (19, 5) NULL,
[Exp_Rev] [decimal] (19, 5) NULL,
[Con_Rev] [decimal] (19, 5) NULL
) ON [PRIMARY]
GO
