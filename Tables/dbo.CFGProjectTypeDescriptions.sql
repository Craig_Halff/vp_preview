CREATE TABLE [dbo].[CFGProjectTypeDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGProjectT__Seq__20E408C1] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGProjectTypeDescriptions] ADD CONSTRAINT [CFGProjectTypeDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
