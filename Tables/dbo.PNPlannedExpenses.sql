CREATE TABLE [dbo].[PNPlannedExpenses]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExpenseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodCount] [int] NOT NULL CONSTRAINT [DF__PNPlanned__Perio__1FBBA5B5] DEFAULT ((1)),
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlanned__Perio__20AFC9EE] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlanned__Perio__21A3EE27] DEFAULT ((0)),
[PeriodRev] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlanned__Perio__22981260] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PNPlannedExpenses] ADD CONSTRAINT [PNPlannedExpensesPK] PRIMARY KEY NONCLUSTERED ([TimePhaseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNPlannedExpensesPTEIDX] ON [dbo].[PNPlannedExpenses] ([PlanID], [TaskID], [ExpenseID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNPlannedExpensesAllIDX] ON [dbo].[PNPlannedExpenses] ([PlanID], [TimePhaseID], [TaskID], [ExpenseID], [StartDate], [EndDate], [PeriodCost], [PeriodBill], [PeriodRev]) ON [PRIMARY]
GO
