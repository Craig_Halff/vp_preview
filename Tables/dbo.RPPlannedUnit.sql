CREATE TABLE [dbo].[RPPlannedUnit]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UnitID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodCount] [int] NOT NULL CONSTRAINT [DF__RPPlanned__Perio__0038D8BB] DEFAULT ((1)),
[PeriodQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Perio__012CFCF4] DEFAULT ((0)),
[PeriodCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Perio__0221212D] DEFAULT ((0)),
[PeriodBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Perio__03154566] DEFAULT ((0)),
[PeriodRev] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPPlanned__Perio__0409699F] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPPlannedUnit] ADD CONSTRAINT [RPPlannedUnitPK] PRIMARY KEY NONCLUSTERED ([TimePhaseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPPlannedUnitPTUIDX] ON [dbo].[RPPlannedUnit] ([PlanID], [TaskID], [UnitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPPlannedUnitAllIDX] ON [dbo].[RPPlannedUnit] ([PlanID], [TimePhaseID], [TaskID], [UnitID], [StartDate], [EndDate], [PeriodQty], [PeriodCost], [PeriodBill], [PeriodRev]) ON [PRIMARY]
GO
