CREATE TABLE [dbo].[UDIC_Collection_History]
(
[UDIC_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustHistoryActionStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryAction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryActionStart] [datetime] NULL,
[CustHistoryActionOwner] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryCollectionContact] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryResponseDate] [datetime] NULL,
[CustHistoryResponse] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryResponseNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustHistoryContactedbyEmployee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_UDIC_Collection_History]
      ON [dbo].[UDIC_Collection_History]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'UDIC_Collection_History'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'UDIC_UID',CONVERT(NVARCHAR(2000),[UDIC_UID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Seq',CONVERT(NVARCHAR(2000),[Seq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustHistoryActionStatus',CONVERT(NVARCHAR(2000),[CustHistoryActionStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustHistoryAction',CONVERT(NVARCHAR(2000),[CustHistoryAction],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustHistoryActionStart',CONVERT(NVARCHAR(2000),[CustHistoryActionStart],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustHistoryActionOwner',CONVERT(NVARCHAR(2000),DELETED.[CustHistoryActionOwner],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustHistoryActionOwner = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustHistoryCollectionContact',CONVERT(NVARCHAR(2000),DELETED.[CustHistoryCollectionContact],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join Contacts as oldDesc   on DELETED.CustHistoryCollectionContact = oldDesc.ContactID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustHistoryResponseDate',CONVERT(NVARCHAR(2000),[CustHistoryResponseDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustHistoryResponse',CONVERT(NVARCHAR(2000),[CustHistoryResponse],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustHistoryResponseNotes','[text]',NULL, @source,@app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustHistoryContactedbyEmployee',CONVERT(NVARCHAR(2000),DELETED.[CustHistoryContactedbyEmployee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustHistoryContactedbyEmployee = oldDesc.Employee

      
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_UDIC_Collection_History]
      ON [dbo].[UDIC_Collection_History]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'UDIC_Collection_History'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'UDIC_UID',NULL,CONVERT(NVARCHAR(2000),[UDIC_UID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',NULL,CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryActionStatus',NULL,CONVERT(NVARCHAR(2000),[CustHistoryActionStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryAction',NULL,CONVERT(NVARCHAR(2000),[CustHistoryAction],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryActionStart',NULL,CONVERT(NVARCHAR(2000),[CustHistoryActionStart],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryActionOwner',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryActionOwner],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryActionOwner = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryCollectionContact',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryCollectionContact],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  Contacts as newDesc  on INSERTED.CustHistoryCollectionContact = newDesc.ContactID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryResponseDate',NULL,CONVERT(NVARCHAR(2000),[CustHistoryResponseDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryResponse',NULL,CONVERT(NVARCHAR(2000),[CustHistoryResponse],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryResponseNotes',NULL,'[text]', @source, @app
      FROM INSERTED


    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryContactedbyEmployee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryContactedbyEmployee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryContactedbyEmployee = newDesc.Employee

     
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_UDIC_Collection_History]
      ON [dbo].[UDIC_Collection_History]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'UDIC_Collection_History'
    
      If UPDATE([UDIC_UID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'UDIC_UID',
      CONVERT(NVARCHAR(2000),DELETED.[UDIC_UID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UDIC_UID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[UDIC_UID] Is Null And
				DELETED.[UDIC_UID] Is Not Null
			) Or
			(
				INSERTED.[UDIC_UID] Is Not Null And
				DELETED.[UDIC_UID] Is Null
			) Or
			(
				INSERTED.[UDIC_UID] !=
				DELETED.[UDIC_UID]
			)
		) 
		END		
		
      If UPDATE([Seq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',
      CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] Is Null And
				DELETED.[Seq] Is Not Null
			) Or
			(
				INSERTED.[Seq] Is Not Null And
				DELETED.[Seq] Is Null
			) Or
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
		
      If UPDATE([CustHistoryActionStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryActionStatus',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryActionStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryActionStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustHistoryActionStatus] Is Null And
				DELETED.[CustHistoryActionStatus] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryActionStatus] Is Not Null And
				DELETED.[CustHistoryActionStatus] Is Null
			) Or
			(
				INSERTED.[CustHistoryActionStatus] !=
				DELETED.[CustHistoryActionStatus]
			)
		) 
		END		
		
      If UPDATE([CustHistoryAction])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryAction',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryAction],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryAction],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustHistoryAction] Is Null And
				DELETED.[CustHistoryAction] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryAction] Is Not Null And
				DELETED.[CustHistoryAction] Is Null
			) Or
			(
				INSERTED.[CustHistoryAction] !=
				DELETED.[CustHistoryAction]
			)
		) 
		END		
		
      If UPDATE([CustHistoryActionStart])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryActionStart',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryActionStart],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryActionStart],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustHistoryActionStart] Is Null And
				DELETED.[CustHistoryActionStart] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryActionStart] Is Not Null And
				DELETED.[CustHistoryActionStart] Is Null
			) Or
			(
				INSERTED.[CustHistoryActionStart] !=
				DELETED.[CustHistoryActionStart]
			)
		) 
		END		
		
     If UPDATE([CustHistoryActionOwner])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryActionOwner',
     CONVERT(NVARCHAR(2000),DELETED.[CustHistoryActionOwner],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryActionOwner],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustHistoryActionOwner] Is Null And
				DELETED.[CustHistoryActionOwner] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryActionOwner] Is Not Null And
				DELETED.[CustHistoryActionOwner] Is Null
			) Or
			(
				INSERTED.[CustHistoryActionOwner] !=
				DELETED.[CustHistoryActionOwner]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryActionOwner = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryActionOwner = newDesc.Employee
		END		
		
     If UPDATE([CustHistoryCollectionContact])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryCollectionContact',
     CONVERT(NVARCHAR(2000),DELETED.[CustHistoryCollectionContact],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryCollectionContact],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustHistoryCollectionContact] Is Null And
				DELETED.[CustHistoryCollectionContact] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryCollectionContact] Is Not Null And
				DELETED.[CustHistoryCollectionContact] Is Null
			) Or
			(
				INSERTED.[CustHistoryCollectionContact] !=
				DELETED.[CustHistoryCollectionContact]
			)
		) left join Contacts as oldDesc  on DELETED.CustHistoryCollectionContact = oldDesc.ContactID  left join  Contacts as newDesc  on INSERTED.CustHistoryCollectionContact = newDesc.ContactID
		END		
		
      If UPDATE([CustHistoryResponseDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryResponseDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryResponseDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryResponseDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustHistoryResponseDate] Is Null And
				DELETED.[CustHistoryResponseDate] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryResponseDate] Is Not Null And
				DELETED.[CustHistoryResponseDate] Is Null
			) Or
			(
				INSERTED.[CustHistoryResponseDate] !=
				DELETED.[CustHistoryResponseDate]
			)
		) 
		END		
		
      If UPDATE([CustHistoryResponse])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryResponse',
      CONVERT(NVARCHAR(2000),DELETED.[CustHistoryResponse],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryResponse],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustHistoryResponse] Is Null And
				DELETED.[CustHistoryResponse] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryResponse] Is Not Null And
				DELETED.[CustHistoryResponse] Is Null
			) Or
			(
				INSERTED.[CustHistoryResponse] !=
				DELETED.[CustHistoryResponse]
			)
		) 
		END		
		
      If UPDATE([CustHistoryResponseNotes])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryResponseNotes',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
     If UPDATE([CustHistoryContactedbyEmployee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustHistoryContactedbyEmployee',
     CONVERT(NVARCHAR(2000),DELETED.[CustHistoryContactedbyEmployee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustHistoryContactedbyEmployee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustHistoryContactedbyEmployee] Is Null And
				DELETED.[CustHistoryContactedbyEmployee] Is Not Null
			) Or
			(
				INSERTED.[CustHistoryContactedbyEmployee] Is Not Null And
				DELETED.[CustHistoryContactedbyEmployee] Is Null
			) Or
			(
				INSERTED.[CustHistoryContactedbyEmployee] !=
				DELETED.[CustHistoryContactedbyEmployee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustHistoryContactedbyEmployee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustHistoryContactedbyEmployee = newDesc.Employee
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[UDIC_Collection_History] ADD CONSTRAINT [UDIC_Collection_HistoryPK] PRIMARY KEY CLUSTERED ([UDIC_UID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
