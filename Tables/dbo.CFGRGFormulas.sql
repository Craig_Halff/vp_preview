CREATE TABLE [dbo].[CFGRGFormulas]
(
[Method] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGRGFormul__Seq__515235F2] DEFAULT ((0)),
[RevGenField] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGRGFormulas] ADD CONSTRAINT [CFGRGFormulasPK] PRIMARY KEY CLUSTERED ([Method], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
