CREATE TABLE [dbo].[ContractDetailsTemplate]
(
[ContractNumber] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Fee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractDet__Fee__1C3F4F35] DEFAULT ((0)),
[ReimbAllow] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__Reimb__1D33736E] DEFAULT ((0)),
[ConsultFee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__Consu__1E2797A7] DEFAULT ((0)),
[Total] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__Total__1F1BBBE0] DEFAULT ((0)),
[FeeBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__FeeBi__200FE019] DEFAULT ((0)),
[ReimbAllowBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__Reimb__21040452] DEFAULT ((0)),
[ConsultFeeBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__Consu__21F8288B] DEFAULT ((0)),
[TotalBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__Total__22EC4CC4] DEFAULT ((0)),
[FeeFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__FeeFu__23E070FD] DEFAULT ((0)),
[ReimbAllowFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__Reimb__24D49536] DEFAULT ((0)),
[ConsultFeeFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__Consu__25C8B96F] DEFAULT ((0)),
[TotalFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__Total__26BCDDA8] DEFAULT ((0)),
[FeeDirLab] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__FeeDi__27B101E1] DEFAULT ((0)),
[FeeDirExp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__FeeDi__28A5261A] DEFAULT ((0)),
[ReimbAllowExp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__Reimb__29994A53] DEFAULT ((0)),
[ReimbAllowCons] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__Reimb__2A8D6E8C] DEFAULT ((0)),
[FeeDirLabBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__FeeDi__2B8192C5] DEFAULT ((0)),
[FeeDirExpBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__FeeDi__2C75B6FE] DEFAULT ((0)),
[ReimbAllowExpBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__Reimb__2D69DB37] DEFAULT ((0)),
[ReimbAllowConsBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__Reimb__2E5DFF70] DEFAULT ((0)),
[FeeDirLabFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__FeeDi__2F5223A9] DEFAULT ((0)),
[FeeDirExpFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__FeeDi__304647E2] DEFAULT ((0)),
[ReimbAllowExpFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__Reimb__313A6C1B] DEFAULT ((0)),
[ReimbAllowConsFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ContractD__Reimb__322E9054] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ContractDetailsTemplateID] [bigint] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_ContractDetailsTemplate]
      ON [dbo].[ContractDetailsTemplate]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ContractDetailsTemplate'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'ContractNumber',CONVERT(NVARCHAR(2000),[ContractNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'WBS1',CONVERT(NVARCHAR(2000),[WBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'WBS2',CONVERT(NVARCHAR(2000),[WBS2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'WBS3',CONVERT(NVARCHAR(2000),[WBS3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'Fee',CONVERT(NVARCHAR(2000),[Fee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'ReimbAllow',CONVERT(NVARCHAR(2000),[ReimbAllow],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'ConsultFee',CONVERT(NVARCHAR(2000),[ConsultFee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'Total',CONVERT(NVARCHAR(2000),[Total],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'FeeBillingCurrency',CONVERT(NVARCHAR(2000),[FeeBillingCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'ReimbAllowBillingCurrency',CONVERT(NVARCHAR(2000),[ReimbAllowBillingCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'ConsultFeeBillingCurrency',CONVERT(NVARCHAR(2000),[ConsultFeeBillingCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'TotalBillingCurrency',CONVERT(NVARCHAR(2000),[TotalBillingCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'FeeFunctionalCurrency',CONVERT(NVARCHAR(2000),[FeeFunctionalCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'ReimbAllowFunctionalCurrency',CONVERT(NVARCHAR(2000),[ReimbAllowFunctionalCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'ConsultFeeFunctionalCurrency',CONVERT(NVARCHAR(2000),[ConsultFeeFunctionalCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'TotalFunctionalCurrency',CONVERT(NVARCHAR(2000),[TotalFunctionalCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'FeeDirLab',CONVERT(NVARCHAR(2000),[FeeDirLab],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'FeeDirExp',CONVERT(NVARCHAR(2000),[FeeDirExp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'ReimbAllowExp',CONVERT(NVARCHAR(2000),[ReimbAllowExp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'ReimbAllowCons',CONVERT(NVARCHAR(2000),[ReimbAllowCons],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'FeeDirLabBillingCurrency',CONVERT(NVARCHAR(2000),[FeeDirLabBillingCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'FeeDirExpBillingCurrency',CONVERT(NVARCHAR(2000),[FeeDirExpBillingCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'ReimbAllowExpBillingCurrency',CONVERT(NVARCHAR(2000),[ReimbAllowExpBillingCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'ReimbAllowConsBillingCurrency',CONVERT(NVARCHAR(2000),[ReimbAllowConsBillingCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'FeeDirLabFunctionalCurrency',CONVERT(NVARCHAR(2000),[FeeDirLabFunctionalCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'FeeDirExpFunctionalCurrency',CONVERT(NVARCHAR(2000),[FeeDirExpFunctionalCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'ReimbAllowExpFunctionalCurrency',CONVERT(NVARCHAR(2000),[ReimbAllowExpFunctionalCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'ReimbAllowConsFunctionalCurrency',CONVERT(NVARCHAR(2000),[ReimbAllowConsFunctionalCurrency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractDetailsTemplateID],121),'ContractDetailsTemplateID',CONVERT(NVARCHAR(2000),[ContractDetailsTemplateID],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_ContractDetailsTemplate] ON [dbo].[ContractDetailsTemplate]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_ContractDetailsTemplate]
      ON [dbo].[ContractDetailsTemplate]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ContractDetailsTemplate'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ContractNumber',NULL,CONVERT(NVARCHAR(2000),[ContractNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'WBS1',NULL,CONVERT(NVARCHAR(2000),[WBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'WBS2',NULL,CONVERT(NVARCHAR(2000),[WBS2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'WBS3',NULL,CONVERT(NVARCHAR(2000),[WBS3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'Fee',NULL,CONVERT(NVARCHAR(2000),[Fee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllow',NULL,CONVERT(NVARCHAR(2000),[ReimbAllow],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ConsultFee',NULL,CONVERT(NVARCHAR(2000),[ConsultFee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'Total',NULL,CONVERT(NVARCHAR(2000),[Total],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'FeeBillingCurrency',NULL,CONVERT(NVARCHAR(2000),[FeeBillingCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllowBillingCurrency',NULL,CONVERT(NVARCHAR(2000),[ReimbAllowBillingCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ConsultFeeBillingCurrency',NULL,CONVERT(NVARCHAR(2000),[ConsultFeeBillingCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'TotalBillingCurrency',NULL,CONVERT(NVARCHAR(2000),[TotalBillingCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'FeeFunctionalCurrency',NULL,CONVERT(NVARCHAR(2000),[FeeFunctionalCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllowFunctionalCurrency',NULL,CONVERT(NVARCHAR(2000),[ReimbAllowFunctionalCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ConsultFeeFunctionalCurrency',NULL,CONVERT(NVARCHAR(2000),[ConsultFeeFunctionalCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'TotalFunctionalCurrency',NULL,CONVERT(NVARCHAR(2000),[TotalFunctionalCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'FeeDirLab',NULL,CONVERT(NVARCHAR(2000),[FeeDirLab],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'FeeDirExp',NULL,CONVERT(NVARCHAR(2000),[FeeDirExp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllowExp',NULL,CONVERT(NVARCHAR(2000),[ReimbAllowExp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllowCons',NULL,CONVERT(NVARCHAR(2000),[ReimbAllowCons],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'FeeDirLabBillingCurrency',NULL,CONVERT(NVARCHAR(2000),[FeeDirLabBillingCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'FeeDirExpBillingCurrency',NULL,CONVERT(NVARCHAR(2000),[FeeDirExpBillingCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllowExpBillingCurrency',NULL,CONVERT(NVARCHAR(2000),[ReimbAllowExpBillingCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllowConsBillingCurrency',NULL,CONVERT(NVARCHAR(2000),[ReimbAllowConsBillingCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'FeeDirLabFunctionalCurrency',NULL,CONVERT(NVARCHAR(2000),[FeeDirLabFunctionalCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'FeeDirExpFunctionalCurrency',NULL,CONVERT(NVARCHAR(2000),[FeeDirExpFunctionalCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllowExpFunctionalCurrency',NULL,CONVERT(NVARCHAR(2000),[ReimbAllowExpFunctionalCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllowConsFunctionalCurrency',NULL,CONVERT(NVARCHAR(2000),[ReimbAllowConsFunctionalCurrency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ContractDetailsTemplateID',NULL,CONVERT(NVARCHAR(2000),[ContractDetailsTemplateID],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_ContractDetailsTemplate] ON [dbo].[ContractDetailsTemplate]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_ContractDetailsTemplate]
      ON [dbo].[ContractDetailsTemplate]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ContractDetailsTemplate'
    
      If UPDATE([ContractNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ContractNumber',
      CONVERT(NVARCHAR(2000),DELETED.[ContractNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ContractNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[ContractNumber] Is Null And
				DELETED.[ContractNumber] Is Not Null
			) Or
			(
				INSERTED.[ContractNumber] Is Not Null And
				DELETED.[ContractNumber] Is Null
			) Or
			(
				INSERTED.[ContractNumber] !=
				DELETED.[ContractNumber]
			)
		) 
		END		
		
      If UPDATE([WBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'WBS1',
      CONVERT(NVARCHAR(2000),DELETED.[WBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[WBS1] Is Null And
				DELETED.[WBS1] Is Not Null
			) Or
			(
				INSERTED.[WBS1] Is Not Null And
				DELETED.[WBS1] Is Null
			) Or
			(
				INSERTED.[WBS1] !=
				DELETED.[WBS1]
			)
		) 
		END		
		
      If UPDATE([WBS2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'WBS2',
      CONVERT(NVARCHAR(2000),DELETED.[WBS2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[WBS2] Is Null And
				DELETED.[WBS2] Is Not Null
			) Or
			(
				INSERTED.[WBS2] Is Not Null And
				DELETED.[WBS2] Is Null
			) Or
			(
				INSERTED.[WBS2] !=
				DELETED.[WBS2]
			)
		) 
		END		
		
      If UPDATE([WBS3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'WBS3',
      CONVERT(NVARCHAR(2000),DELETED.[WBS3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[WBS3] Is Null And
				DELETED.[WBS3] Is Not Null
			) Or
			(
				INSERTED.[WBS3] Is Not Null And
				DELETED.[WBS3] Is Null
			) Or
			(
				INSERTED.[WBS3] !=
				DELETED.[WBS3]
			)
		) 
		END		
		
      If UPDATE([Fee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'Fee',
      CONVERT(NVARCHAR(2000),DELETED.[Fee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[Fee] Is Null And
				DELETED.[Fee] Is Not Null
			) Or
			(
				INSERTED.[Fee] Is Not Null And
				DELETED.[Fee] Is Null
			) Or
			(
				INSERTED.[Fee] !=
				DELETED.[Fee]
			)
		) 
		END		
		
      If UPDATE([ReimbAllow])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllow',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllow],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllow],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[ReimbAllow] Is Null And
				DELETED.[ReimbAllow] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllow] Is Not Null And
				DELETED.[ReimbAllow] Is Null
			) Or
			(
				INSERTED.[ReimbAllow] !=
				DELETED.[ReimbAllow]
			)
		) 
		END		
		
      If UPDATE([ConsultFee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ConsultFee',
      CONVERT(NVARCHAR(2000),DELETED.[ConsultFee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ConsultFee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[ConsultFee] Is Null And
				DELETED.[ConsultFee] Is Not Null
			) Or
			(
				INSERTED.[ConsultFee] Is Not Null And
				DELETED.[ConsultFee] Is Null
			) Or
			(
				INSERTED.[ConsultFee] !=
				DELETED.[ConsultFee]
			)
		) 
		END		
		
      If UPDATE([Total])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'Total',
      CONVERT(NVARCHAR(2000),DELETED.[Total],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Total],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[Total] Is Null And
				DELETED.[Total] Is Not Null
			) Or
			(
				INSERTED.[Total] Is Not Null And
				DELETED.[Total] Is Null
			) Or
			(
				INSERTED.[Total] !=
				DELETED.[Total]
			)
		) 
		END		
		
      If UPDATE([FeeBillingCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'FeeBillingCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[FeeBillingCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeBillingCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[FeeBillingCurrency] Is Null And
				DELETED.[FeeBillingCurrency] Is Not Null
			) Or
			(
				INSERTED.[FeeBillingCurrency] Is Not Null And
				DELETED.[FeeBillingCurrency] Is Null
			) Or
			(
				INSERTED.[FeeBillingCurrency] !=
				DELETED.[FeeBillingCurrency]
			)
		) 
		END		
		
      If UPDATE([ReimbAllowBillingCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllowBillingCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllowBillingCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllowBillingCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[ReimbAllowBillingCurrency] Is Null And
				DELETED.[ReimbAllowBillingCurrency] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllowBillingCurrency] Is Not Null And
				DELETED.[ReimbAllowBillingCurrency] Is Null
			) Or
			(
				INSERTED.[ReimbAllowBillingCurrency] !=
				DELETED.[ReimbAllowBillingCurrency]
			)
		) 
		END		
		
      If UPDATE([ConsultFeeBillingCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ConsultFeeBillingCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[ConsultFeeBillingCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ConsultFeeBillingCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[ConsultFeeBillingCurrency] Is Null And
				DELETED.[ConsultFeeBillingCurrency] Is Not Null
			) Or
			(
				INSERTED.[ConsultFeeBillingCurrency] Is Not Null And
				DELETED.[ConsultFeeBillingCurrency] Is Null
			) Or
			(
				INSERTED.[ConsultFeeBillingCurrency] !=
				DELETED.[ConsultFeeBillingCurrency]
			)
		) 
		END		
		
      If UPDATE([TotalBillingCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'TotalBillingCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[TotalBillingCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TotalBillingCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[TotalBillingCurrency] Is Null And
				DELETED.[TotalBillingCurrency] Is Not Null
			) Or
			(
				INSERTED.[TotalBillingCurrency] Is Not Null And
				DELETED.[TotalBillingCurrency] Is Null
			) Or
			(
				INSERTED.[TotalBillingCurrency] !=
				DELETED.[TotalBillingCurrency]
			)
		) 
		END		
		
      If UPDATE([FeeFunctionalCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'FeeFunctionalCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[FeeFunctionalCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeFunctionalCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[FeeFunctionalCurrency] Is Null And
				DELETED.[FeeFunctionalCurrency] Is Not Null
			) Or
			(
				INSERTED.[FeeFunctionalCurrency] Is Not Null And
				DELETED.[FeeFunctionalCurrency] Is Null
			) Or
			(
				INSERTED.[FeeFunctionalCurrency] !=
				DELETED.[FeeFunctionalCurrency]
			)
		) 
		END		
		
      If UPDATE([ReimbAllowFunctionalCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllowFunctionalCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllowFunctionalCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllowFunctionalCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[ReimbAllowFunctionalCurrency] Is Null And
				DELETED.[ReimbAllowFunctionalCurrency] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllowFunctionalCurrency] Is Not Null And
				DELETED.[ReimbAllowFunctionalCurrency] Is Null
			) Or
			(
				INSERTED.[ReimbAllowFunctionalCurrency] !=
				DELETED.[ReimbAllowFunctionalCurrency]
			)
		) 
		END		
		
      If UPDATE([ConsultFeeFunctionalCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ConsultFeeFunctionalCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[ConsultFeeFunctionalCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ConsultFeeFunctionalCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[ConsultFeeFunctionalCurrency] Is Null And
				DELETED.[ConsultFeeFunctionalCurrency] Is Not Null
			) Or
			(
				INSERTED.[ConsultFeeFunctionalCurrency] Is Not Null And
				DELETED.[ConsultFeeFunctionalCurrency] Is Null
			) Or
			(
				INSERTED.[ConsultFeeFunctionalCurrency] !=
				DELETED.[ConsultFeeFunctionalCurrency]
			)
		) 
		END		
		
      If UPDATE([TotalFunctionalCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'TotalFunctionalCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[TotalFunctionalCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TotalFunctionalCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[TotalFunctionalCurrency] Is Null And
				DELETED.[TotalFunctionalCurrency] Is Not Null
			) Or
			(
				INSERTED.[TotalFunctionalCurrency] Is Not Null And
				DELETED.[TotalFunctionalCurrency] Is Null
			) Or
			(
				INSERTED.[TotalFunctionalCurrency] !=
				DELETED.[TotalFunctionalCurrency]
			)
		) 
		END		
		
      If UPDATE([FeeDirLab])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'FeeDirLab',
      CONVERT(NVARCHAR(2000),DELETED.[FeeDirLab],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeDirLab],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[FeeDirLab] Is Null And
				DELETED.[FeeDirLab] Is Not Null
			) Or
			(
				INSERTED.[FeeDirLab] Is Not Null And
				DELETED.[FeeDirLab] Is Null
			) Or
			(
				INSERTED.[FeeDirLab] !=
				DELETED.[FeeDirLab]
			)
		) 
		END		
		
      If UPDATE([FeeDirExp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'FeeDirExp',
      CONVERT(NVARCHAR(2000),DELETED.[FeeDirExp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeDirExp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[FeeDirExp] Is Null And
				DELETED.[FeeDirExp] Is Not Null
			) Or
			(
				INSERTED.[FeeDirExp] Is Not Null And
				DELETED.[FeeDirExp] Is Null
			) Or
			(
				INSERTED.[FeeDirExp] !=
				DELETED.[FeeDirExp]
			)
		) 
		END		
		
      If UPDATE([ReimbAllowExp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllowExp',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllowExp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllowExp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[ReimbAllowExp] Is Null And
				DELETED.[ReimbAllowExp] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllowExp] Is Not Null And
				DELETED.[ReimbAllowExp] Is Null
			) Or
			(
				INSERTED.[ReimbAllowExp] !=
				DELETED.[ReimbAllowExp]
			)
		) 
		END		
		
      If UPDATE([ReimbAllowCons])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllowCons',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllowCons],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllowCons],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[ReimbAllowCons] Is Null And
				DELETED.[ReimbAllowCons] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllowCons] Is Not Null And
				DELETED.[ReimbAllowCons] Is Null
			) Or
			(
				INSERTED.[ReimbAllowCons] !=
				DELETED.[ReimbAllowCons]
			)
		) 
		END		
		
      If UPDATE([FeeDirLabBillingCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'FeeDirLabBillingCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[FeeDirLabBillingCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeDirLabBillingCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[FeeDirLabBillingCurrency] Is Null And
				DELETED.[FeeDirLabBillingCurrency] Is Not Null
			) Or
			(
				INSERTED.[FeeDirLabBillingCurrency] Is Not Null And
				DELETED.[FeeDirLabBillingCurrency] Is Null
			) Or
			(
				INSERTED.[FeeDirLabBillingCurrency] !=
				DELETED.[FeeDirLabBillingCurrency]
			)
		) 
		END		
		
      If UPDATE([FeeDirExpBillingCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'FeeDirExpBillingCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[FeeDirExpBillingCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeDirExpBillingCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[FeeDirExpBillingCurrency] Is Null And
				DELETED.[FeeDirExpBillingCurrency] Is Not Null
			) Or
			(
				INSERTED.[FeeDirExpBillingCurrency] Is Not Null And
				DELETED.[FeeDirExpBillingCurrency] Is Null
			) Or
			(
				INSERTED.[FeeDirExpBillingCurrency] !=
				DELETED.[FeeDirExpBillingCurrency]
			)
		) 
		END		
		
      If UPDATE([ReimbAllowExpBillingCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllowExpBillingCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllowExpBillingCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllowExpBillingCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[ReimbAllowExpBillingCurrency] Is Null And
				DELETED.[ReimbAllowExpBillingCurrency] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllowExpBillingCurrency] Is Not Null And
				DELETED.[ReimbAllowExpBillingCurrency] Is Null
			) Or
			(
				INSERTED.[ReimbAllowExpBillingCurrency] !=
				DELETED.[ReimbAllowExpBillingCurrency]
			)
		) 
		END		
		
      If UPDATE([ReimbAllowConsBillingCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllowConsBillingCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllowConsBillingCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllowConsBillingCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[ReimbAllowConsBillingCurrency] Is Null And
				DELETED.[ReimbAllowConsBillingCurrency] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllowConsBillingCurrency] Is Not Null And
				DELETED.[ReimbAllowConsBillingCurrency] Is Null
			) Or
			(
				INSERTED.[ReimbAllowConsBillingCurrency] !=
				DELETED.[ReimbAllowConsBillingCurrency]
			)
		) 
		END		
		
      If UPDATE([FeeDirLabFunctionalCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'FeeDirLabFunctionalCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[FeeDirLabFunctionalCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeDirLabFunctionalCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[FeeDirLabFunctionalCurrency] Is Null And
				DELETED.[FeeDirLabFunctionalCurrency] Is Not Null
			) Or
			(
				INSERTED.[FeeDirLabFunctionalCurrency] Is Not Null And
				DELETED.[FeeDirLabFunctionalCurrency] Is Null
			) Or
			(
				INSERTED.[FeeDirLabFunctionalCurrency] !=
				DELETED.[FeeDirLabFunctionalCurrency]
			)
		) 
		END		
		
      If UPDATE([FeeDirExpFunctionalCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'FeeDirExpFunctionalCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[FeeDirExpFunctionalCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeDirExpFunctionalCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[FeeDirExpFunctionalCurrency] Is Null And
				DELETED.[FeeDirExpFunctionalCurrency] Is Not Null
			) Or
			(
				INSERTED.[FeeDirExpFunctionalCurrency] Is Not Null And
				DELETED.[FeeDirExpFunctionalCurrency] Is Null
			) Or
			(
				INSERTED.[FeeDirExpFunctionalCurrency] !=
				DELETED.[FeeDirExpFunctionalCurrency]
			)
		) 
		END		
		
      If UPDATE([ReimbAllowExpFunctionalCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllowExpFunctionalCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllowExpFunctionalCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllowExpFunctionalCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[ReimbAllowExpFunctionalCurrency] Is Null And
				DELETED.[ReimbAllowExpFunctionalCurrency] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllowExpFunctionalCurrency] Is Not Null And
				DELETED.[ReimbAllowExpFunctionalCurrency] Is Null
			) Or
			(
				INSERTED.[ReimbAllowExpFunctionalCurrency] !=
				DELETED.[ReimbAllowExpFunctionalCurrency]
			)
		) 
		END		
		
      If UPDATE([ReimbAllowConsFunctionalCurrency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ReimbAllowConsFunctionalCurrency',
      CONVERT(NVARCHAR(2000),DELETED.[ReimbAllowConsFunctionalCurrency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReimbAllowConsFunctionalCurrency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[ReimbAllowConsFunctionalCurrency] Is Null And
				DELETED.[ReimbAllowConsFunctionalCurrency] Is Not Null
			) Or
			(
				INSERTED.[ReimbAllowConsFunctionalCurrency] Is Not Null And
				DELETED.[ReimbAllowConsFunctionalCurrency] Is Null
			) Or
			(
				INSERTED.[ReimbAllowConsFunctionalCurrency] !=
				DELETED.[ReimbAllowConsFunctionalCurrency]
			)
		) 
		END		
		
      If UPDATE([ContractDetailsTemplateID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractDetailsTemplateID],121),'ContractDetailsTemplateID',
      CONVERT(NVARCHAR(2000),DELETED.[ContractDetailsTemplateID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ContractDetailsTemplateID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractDetailsTemplateID] = DELETED.[ContractDetailsTemplateID] AND 
		(
			(
				INSERTED.[ContractDetailsTemplateID] Is Null And
				DELETED.[ContractDetailsTemplateID] Is Not Null
			) Or
			(
				INSERTED.[ContractDetailsTemplateID] Is Not Null And
				DELETED.[ContractDetailsTemplateID] Is Null
			) Or
			(
				INSERTED.[ContractDetailsTemplateID] !=
				DELETED.[ContractDetailsTemplateID]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_ContractDetailsTemplate] ON [dbo].[ContractDetailsTemplate]
GO
ALTER TABLE [dbo].[ContractDetailsTemplate] ADD CONSTRAINT [ContractDetailsTemplatePK] PRIMARY KEY CLUSTERED ([ContractDetailsTemplateID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [ContractDetailsTemplateWBS1WBS2WBS3ContractNumberIDX] ON [dbo].[ContractDetailsTemplate] ([WBS1], [WBS2], [WBS3], [ContractNumber]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
