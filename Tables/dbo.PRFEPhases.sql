CREATE TABLE [dbo].[PRFEPhases]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PhaseCode] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FEPercent] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__FEPer__441901BC] DEFAULT ((0)),
[TotalFee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Total__450D25F5] DEFAULT ((0)),
[Percent1] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__46014A2E] DEFAULT ((0)),
[Fee1] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhases__Fee1__46F56E67] DEFAULT ((0)),
[Percent2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__47E992A0] DEFAULT ((0)),
[Fee2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhases__Fee2__48DDB6D9] DEFAULT ((0)),
[Percent3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__49D1DB12] DEFAULT ((0)),
[Fee3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhases__Fee3__4AC5FF4B] DEFAULT ((0)),
[Percent4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__4BBA2384] DEFAULT ((0)),
[Fee4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhases__Fee4__4CAE47BD] DEFAULT ((0)),
[Percent5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__4DA26BF6] DEFAULT ((0)),
[Fee5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhases__Fee5__4E96902F] DEFAULT ((0)),
[Percent6] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__4F8AB468] DEFAULT ((0)),
[Fee6] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhases__Fee6__507ED8A1] DEFAULT ((0)),
[Percent7] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__5172FCDA] DEFAULT ((0)),
[Fee7] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhases__Fee7__52672113] DEFAULT ((0)),
[Percent8] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__535B454C] DEFAULT ((0)),
[Fee8] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhases__Fee8__544F6985] DEFAULT ((0)),
[Percent9] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__55438DBE] DEFAULT ((0)),
[Fee9] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhases__Fee9__5637B1F7] DEFAULT ((0)),
[Percent10] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__572BD630] DEFAULT ((0)),
[Fee10] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Fee10__581FFA69] DEFAULT ((0)),
[Percent11] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__59141EA2] DEFAULT ((0)),
[Fee11] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Fee11__5A0842DB] DEFAULT ((0)),
[Percent12] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__5AFC6714] DEFAULT ((0)),
[Fee12] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Fee12__5BF08B4D] DEFAULT ((0)),
[Percent13] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__5CE4AF86] DEFAULT ((0)),
[Fee13] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Fee13__5DD8D3BF] DEFAULT ((0)),
[Percent14] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__5ECCF7F8] DEFAULT ((0)),
[Fee14] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Fee14__5FC11C31] DEFAULT ((0)),
[Percent15] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__60B5406A] DEFAULT ((0)),
[Fee15] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Fee15__61A964A3] DEFAULT ((0)),
[Percent16] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__629D88DC] DEFAULT ((0)),
[Fee16] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Fee16__6391AD15] DEFAULT ((0)),
[Percent17] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__6485D14E] DEFAULT ((0)),
[Fee17] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Fee17__6579F587] DEFAULT ((0)),
[Percent18] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__666E19C0] DEFAULT ((0)),
[Fee18] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Fee18__67623DF9] DEFAULT ((0)),
[Percent19] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__68566232] DEFAULT ((0)),
[Fee19] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Fee19__694A866B] DEFAULT ((0)),
[Percent20] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Perce__6A3EAAA4] DEFAULT ((0)),
[Fee20] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Fee20__6B32CEDD] DEFAULT ((0)),
[PctComplete1] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__6C26F316] DEFAULT ((0)),
[EarnedFee1] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__6D1B174F] DEFAULT ((0)),
[PctComplete2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__6E0F3B88] DEFAULT ((0)),
[EarnedFee2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__6F035FC1] DEFAULT ((0)),
[PctComplete3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__6FF783FA] DEFAULT ((0)),
[EarnedFee3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__70EBA833] DEFAULT ((0)),
[PctComplete4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__71DFCC6C] DEFAULT ((0)),
[EarnedFee4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__72D3F0A5] DEFAULT ((0)),
[PctComplete5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__73C814DE] DEFAULT ((0)),
[EarnedFee5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__74BC3917] DEFAULT ((0)),
[PctComplete6] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__75B05D50] DEFAULT ((0)),
[EarnedFee6] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__76A48189] DEFAULT ((0)),
[PctComplete7] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__7798A5C2] DEFAULT ((0)),
[EarnedFee7] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__788CC9FB] DEFAULT ((0)),
[PctComplete8] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__7980EE34] DEFAULT ((0)),
[EarnedFee8] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__7A75126D] DEFAULT ((0)),
[PctComplete9] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__7B6936A6] DEFAULT ((0)),
[EarnedFee9] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__7C5D5ADF] DEFAULT ((0)),
[PctComplete10] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__7D517F18] DEFAULT ((0)),
[EarnedFee10] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__7E45A351] DEFAULT ((0)),
[PctComplete11] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__7F39C78A] DEFAULT ((0)),
[EarnedFee11] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__002DEBC3] DEFAULT ((0)),
[PctComplete12] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__01220FFC] DEFAULT ((0)),
[EarnedFee12] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__02163435] DEFAULT ((0)),
[PctComplete13] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__030A586E] DEFAULT ((0)),
[EarnedFee13] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__03FE7CA7] DEFAULT ((0)),
[PctComplete14] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__04F2A0E0] DEFAULT ((0)),
[EarnedFee14] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__05E6C519] DEFAULT ((0)),
[PctComplete15] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__06DAE952] DEFAULT ((0)),
[EarnedFee15] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__07CF0D8B] DEFAULT ((0)),
[PctComplete16] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__08C331C4] DEFAULT ((0)),
[EarnedFee16] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__09B755FD] DEFAULT ((0)),
[PctComplete17] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__0AAB7A36] DEFAULT ((0)),
[EarnedFee17] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__0B9F9E6F] DEFAULT ((0)),
[PctComplete18] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__0C93C2A8] DEFAULT ((0)),
[EarnedFee18] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__0D87E6E1] DEFAULT ((0)),
[PctComplete19] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__0E7C0B1A] DEFAULT ((0)),
[EarnedFee19] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__0F702F53] DEFAULT ((0)),
[PctComplete20] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__PctCo__1064538C] DEFAULT ((0)),
[EarnedFee20] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRFEPhase__Earne__115877C5] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_PRFEPhases]
      ON [dbo].[PRFEPhases]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'PRFEPhases'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'WBS1',CONVERT(NVARCHAR(2000),[WBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PhaseCode',CONVERT(NVARCHAR(2000),[PhaseCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Description',CONVERT(NVARCHAR(2000),[Description],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'FEPercent',CONVERT(NVARCHAR(2000),[FEPercent],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'TotalFee',CONVERT(NVARCHAR(2000),[TotalFee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent1',CONVERT(NVARCHAR(2000),[Percent1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee1',CONVERT(NVARCHAR(2000),[Fee1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent2',CONVERT(NVARCHAR(2000),[Percent2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee2',CONVERT(NVARCHAR(2000),[Fee2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent3',CONVERT(NVARCHAR(2000),[Percent3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee3',CONVERT(NVARCHAR(2000),[Fee3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent4',CONVERT(NVARCHAR(2000),[Percent4],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee4',CONVERT(NVARCHAR(2000),[Fee4],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent5',CONVERT(NVARCHAR(2000),[Percent5],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee5',CONVERT(NVARCHAR(2000),[Fee5],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent6',CONVERT(NVARCHAR(2000),[Percent6],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee6',CONVERT(NVARCHAR(2000),[Fee6],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent7',CONVERT(NVARCHAR(2000),[Percent7],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee7',CONVERT(NVARCHAR(2000),[Fee7],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent8',CONVERT(NVARCHAR(2000),[Percent8],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee8',CONVERT(NVARCHAR(2000),[Fee8],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent9',CONVERT(NVARCHAR(2000),[Percent9],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee9',CONVERT(NVARCHAR(2000),[Fee9],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent10',CONVERT(NVARCHAR(2000),[Percent10],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee10',CONVERT(NVARCHAR(2000),[Fee10],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent11',CONVERT(NVARCHAR(2000),[Percent11],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee11',CONVERT(NVARCHAR(2000),[Fee11],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent12',CONVERT(NVARCHAR(2000),[Percent12],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee12',CONVERT(NVARCHAR(2000),[Fee12],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent13',CONVERT(NVARCHAR(2000),[Percent13],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee13',CONVERT(NVARCHAR(2000),[Fee13],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent14',CONVERT(NVARCHAR(2000),[Percent14],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee14',CONVERT(NVARCHAR(2000),[Fee14],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent15',CONVERT(NVARCHAR(2000),[Percent15],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee15',CONVERT(NVARCHAR(2000),[Fee15],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent16',CONVERT(NVARCHAR(2000),[Percent16],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee16',CONVERT(NVARCHAR(2000),[Fee16],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent17',CONVERT(NVARCHAR(2000),[Percent17],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee17',CONVERT(NVARCHAR(2000),[Fee17],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent18',CONVERT(NVARCHAR(2000),[Percent18],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee18',CONVERT(NVARCHAR(2000),[Fee18],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent19',CONVERT(NVARCHAR(2000),[Percent19],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee19',CONVERT(NVARCHAR(2000),[Fee19],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Percent20',CONVERT(NVARCHAR(2000),[Percent20],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'Fee20',CONVERT(NVARCHAR(2000),[Fee20],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete1',CONVERT(NVARCHAR(2000),[PctComplete1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee1',CONVERT(NVARCHAR(2000),[EarnedFee1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete2',CONVERT(NVARCHAR(2000),[PctComplete2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee2',CONVERT(NVARCHAR(2000),[EarnedFee2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete3',CONVERT(NVARCHAR(2000),[PctComplete3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee3',CONVERT(NVARCHAR(2000),[EarnedFee3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete4',CONVERT(NVARCHAR(2000),[PctComplete4],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee4',CONVERT(NVARCHAR(2000),[EarnedFee4],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete5',CONVERT(NVARCHAR(2000),[PctComplete5],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee5',CONVERT(NVARCHAR(2000),[EarnedFee5],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete6',CONVERT(NVARCHAR(2000),[PctComplete6],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee6',CONVERT(NVARCHAR(2000),[EarnedFee6],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete7',CONVERT(NVARCHAR(2000),[PctComplete7],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee7',CONVERT(NVARCHAR(2000),[EarnedFee7],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete8',CONVERT(NVARCHAR(2000),[PctComplete8],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee8',CONVERT(NVARCHAR(2000),[EarnedFee8],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete9',CONVERT(NVARCHAR(2000),[PctComplete9],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee9',CONVERT(NVARCHAR(2000),[EarnedFee9],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete10',CONVERT(NVARCHAR(2000),[PctComplete10],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee10',CONVERT(NVARCHAR(2000),[EarnedFee10],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete11',CONVERT(NVARCHAR(2000),[PctComplete11],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee11',CONVERT(NVARCHAR(2000),[EarnedFee11],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete12',CONVERT(NVARCHAR(2000),[PctComplete12],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee12',CONVERT(NVARCHAR(2000),[EarnedFee12],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete13',CONVERT(NVARCHAR(2000),[PctComplete13],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee13',CONVERT(NVARCHAR(2000),[EarnedFee13],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete14',CONVERT(NVARCHAR(2000),[PctComplete14],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee14',CONVERT(NVARCHAR(2000),[EarnedFee14],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete15',CONVERT(NVARCHAR(2000),[PctComplete15],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee15',CONVERT(NVARCHAR(2000),[EarnedFee15],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete16',CONVERT(NVARCHAR(2000),[PctComplete16],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee16',CONVERT(NVARCHAR(2000),[EarnedFee16],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete17',CONVERT(NVARCHAR(2000),[PctComplete17],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee17',CONVERT(NVARCHAR(2000),[EarnedFee17],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete18',CONVERT(NVARCHAR(2000),[PctComplete18],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee18',CONVERT(NVARCHAR(2000),[EarnedFee18],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete19',CONVERT(NVARCHAR(2000),[PctComplete19],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee19',CONVERT(NVARCHAR(2000),[EarnedFee19],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'PctComplete20',CONVERT(NVARCHAR(2000),[PctComplete20],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[PhaseCode],121),'EarnedFee20',CONVERT(NVARCHAR(2000),[EarnedFee20],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_PRFEPhases] ON [dbo].[PRFEPhases]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_PRFEPhases]
      ON [dbo].[PRFEPhases]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'PRFEPhases'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'WBS1',NULL,CONVERT(NVARCHAR(2000),[WBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PhaseCode',NULL,CONVERT(NVARCHAR(2000),[PhaseCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Description',NULL,CONVERT(NVARCHAR(2000),[Description],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'FEPercent',NULL,CONVERT(NVARCHAR(2000),[FEPercent],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'TotalFee',NULL,CONVERT(NVARCHAR(2000),[TotalFee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent1',NULL,CONVERT(NVARCHAR(2000),[Percent1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee1',NULL,CONVERT(NVARCHAR(2000),[Fee1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent2',NULL,CONVERT(NVARCHAR(2000),[Percent2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee2',NULL,CONVERT(NVARCHAR(2000),[Fee2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent3',NULL,CONVERT(NVARCHAR(2000),[Percent3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee3',NULL,CONVERT(NVARCHAR(2000),[Fee3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent4',NULL,CONVERT(NVARCHAR(2000),[Percent4],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee4',NULL,CONVERT(NVARCHAR(2000),[Fee4],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent5',NULL,CONVERT(NVARCHAR(2000),[Percent5],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee5',NULL,CONVERT(NVARCHAR(2000),[Fee5],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent6',NULL,CONVERT(NVARCHAR(2000),[Percent6],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee6',NULL,CONVERT(NVARCHAR(2000),[Fee6],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent7',NULL,CONVERT(NVARCHAR(2000),[Percent7],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee7',NULL,CONVERT(NVARCHAR(2000),[Fee7],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent8',NULL,CONVERT(NVARCHAR(2000),[Percent8],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee8',NULL,CONVERT(NVARCHAR(2000),[Fee8],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent9',NULL,CONVERT(NVARCHAR(2000),[Percent9],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee9',NULL,CONVERT(NVARCHAR(2000),[Fee9],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent10',NULL,CONVERT(NVARCHAR(2000),[Percent10],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee10',NULL,CONVERT(NVARCHAR(2000),[Fee10],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent11',NULL,CONVERT(NVARCHAR(2000),[Percent11],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee11',NULL,CONVERT(NVARCHAR(2000),[Fee11],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent12',NULL,CONVERT(NVARCHAR(2000),[Percent12],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee12',NULL,CONVERT(NVARCHAR(2000),[Fee12],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent13',NULL,CONVERT(NVARCHAR(2000),[Percent13],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee13',NULL,CONVERT(NVARCHAR(2000),[Fee13],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent14',NULL,CONVERT(NVARCHAR(2000),[Percent14],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee14',NULL,CONVERT(NVARCHAR(2000),[Fee14],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent15',NULL,CONVERT(NVARCHAR(2000),[Percent15],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee15',NULL,CONVERT(NVARCHAR(2000),[Fee15],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent16',NULL,CONVERT(NVARCHAR(2000),[Percent16],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee16',NULL,CONVERT(NVARCHAR(2000),[Fee16],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent17',NULL,CONVERT(NVARCHAR(2000),[Percent17],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee17',NULL,CONVERT(NVARCHAR(2000),[Fee17],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent18',NULL,CONVERT(NVARCHAR(2000),[Percent18],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee18',NULL,CONVERT(NVARCHAR(2000),[Fee18],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent19',NULL,CONVERT(NVARCHAR(2000),[Percent19],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee19',NULL,CONVERT(NVARCHAR(2000),[Fee19],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent20',NULL,CONVERT(NVARCHAR(2000),[Percent20],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee20',NULL,CONVERT(NVARCHAR(2000),[Fee20],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete1',NULL,CONVERT(NVARCHAR(2000),[PctComplete1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee1',NULL,CONVERT(NVARCHAR(2000),[EarnedFee1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete2',NULL,CONVERT(NVARCHAR(2000),[PctComplete2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee2',NULL,CONVERT(NVARCHAR(2000),[EarnedFee2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete3',NULL,CONVERT(NVARCHAR(2000),[PctComplete3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee3',NULL,CONVERT(NVARCHAR(2000),[EarnedFee3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete4',NULL,CONVERT(NVARCHAR(2000),[PctComplete4],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee4',NULL,CONVERT(NVARCHAR(2000),[EarnedFee4],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete5',NULL,CONVERT(NVARCHAR(2000),[PctComplete5],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee5',NULL,CONVERT(NVARCHAR(2000),[EarnedFee5],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete6',NULL,CONVERT(NVARCHAR(2000),[PctComplete6],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee6',NULL,CONVERT(NVARCHAR(2000),[EarnedFee6],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete7',NULL,CONVERT(NVARCHAR(2000),[PctComplete7],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee7',NULL,CONVERT(NVARCHAR(2000),[EarnedFee7],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete8',NULL,CONVERT(NVARCHAR(2000),[PctComplete8],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee8',NULL,CONVERT(NVARCHAR(2000),[EarnedFee8],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete9',NULL,CONVERT(NVARCHAR(2000),[PctComplete9],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee9',NULL,CONVERT(NVARCHAR(2000),[EarnedFee9],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete10',NULL,CONVERT(NVARCHAR(2000),[PctComplete10],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee10',NULL,CONVERT(NVARCHAR(2000),[EarnedFee10],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete11',NULL,CONVERT(NVARCHAR(2000),[PctComplete11],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee11',NULL,CONVERT(NVARCHAR(2000),[EarnedFee11],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete12',NULL,CONVERT(NVARCHAR(2000),[PctComplete12],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee12',NULL,CONVERT(NVARCHAR(2000),[EarnedFee12],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete13',NULL,CONVERT(NVARCHAR(2000),[PctComplete13],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee13',NULL,CONVERT(NVARCHAR(2000),[EarnedFee13],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete14',NULL,CONVERT(NVARCHAR(2000),[PctComplete14],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee14',NULL,CONVERT(NVARCHAR(2000),[EarnedFee14],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete15',NULL,CONVERT(NVARCHAR(2000),[PctComplete15],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee15',NULL,CONVERT(NVARCHAR(2000),[EarnedFee15],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete16',NULL,CONVERT(NVARCHAR(2000),[PctComplete16],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee16',NULL,CONVERT(NVARCHAR(2000),[EarnedFee16],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete17',NULL,CONVERT(NVARCHAR(2000),[PctComplete17],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee17',NULL,CONVERT(NVARCHAR(2000),[EarnedFee17],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete18',NULL,CONVERT(NVARCHAR(2000),[PctComplete18],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee18',NULL,CONVERT(NVARCHAR(2000),[EarnedFee18],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete19',NULL,CONVERT(NVARCHAR(2000),[PctComplete19],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee19',NULL,CONVERT(NVARCHAR(2000),[EarnedFee19],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete20',NULL,CONVERT(NVARCHAR(2000),[PctComplete20],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee20',NULL,CONVERT(NVARCHAR(2000),[EarnedFee20],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_PRFEPhases] ON [dbo].[PRFEPhases]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_PRFEPhases]
      ON [dbo].[PRFEPhases]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'PRFEPhases'
    
      If UPDATE([WBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'WBS1',
      CONVERT(NVARCHAR(2000),DELETED.[WBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[WBS1] Is Null And
				DELETED.[WBS1] Is Not Null
			) Or
			(
				INSERTED.[WBS1] Is Not Null And
				DELETED.[WBS1] Is Null
			) Or
			(
				INSERTED.[WBS1] !=
				DELETED.[WBS1]
			)
		) 
		END		
		
      If UPDATE([PhaseCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PhaseCode',
      CONVERT(NVARCHAR(2000),DELETED.[PhaseCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PhaseCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PhaseCode] Is Null And
				DELETED.[PhaseCode] Is Not Null
			) Or
			(
				INSERTED.[PhaseCode] Is Not Null And
				DELETED.[PhaseCode] Is Null
			) Or
			(
				INSERTED.[PhaseCode] !=
				DELETED.[PhaseCode]
			)
		) 
		END		
		
      If UPDATE([Description])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Description',
      CONVERT(NVARCHAR(2000),DELETED.[Description],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Description],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Description] Is Null And
				DELETED.[Description] Is Not Null
			) Or
			(
				INSERTED.[Description] Is Not Null And
				DELETED.[Description] Is Null
			) Or
			(
				INSERTED.[Description] !=
				DELETED.[Description]
			)
		) 
		END		
		
      If UPDATE([FEPercent])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'FEPercent',
      CONVERT(NVARCHAR(2000),DELETED.[FEPercent],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FEPercent],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[FEPercent] Is Null And
				DELETED.[FEPercent] Is Not Null
			) Or
			(
				INSERTED.[FEPercent] Is Not Null And
				DELETED.[FEPercent] Is Null
			) Or
			(
				INSERTED.[FEPercent] !=
				DELETED.[FEPercent]
			)
		) 
		END		
		
      If UPDATE([TotalFee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'TotalFee',
      CONVERT(NVARCHAR(2000),DELETED.[TotalFee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TotalFee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[TotalFee] Is Null And
				DELETED.[TotalFee] Is Not Null
			) Or
			(
				INSERTED.[TotalFee] Is Not Null And
				DELETED.[TotalFee] Is Null
			) Or
			(
				INSERTED.[TotalFee] !=
				DELETED.[TotalFee]
			)
		) 
		END		
		
      If UPDATE([Percent1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent1',
      CONVERT(NVARCHAR(2000),DELETED.[Percent1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent1] Is Null And
				DELETED.[Percent1] Is Not Null
			) Or
			(
				INSERTED.[Percent1] Is Not Null And
				DELETED.[Percent1] Is Null
			) Or
			(
				INSERTED.[Percent1] !=
				DELETED.[Percent1]
			)
		) 
		END		
		
      If UPDATE([Fee1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee1',
      CONVERT(NVARCHAR(2000),DELETED.[Fee1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee1] Is Null And
				DELETED.[Fee1] Is Not Null
			) Or
			(
				INSERTED.[Fee1] Is Not Null And
				DELETED.[Fee1] Is Null
			) Or
			(
				INSERTED.[Fee1] !=
				DELETED.[Fee1]
			)
		) 
		END		
		
      If UPDATE([Percent2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent2',
      CONVERT(NVARCHAR(2000),DELETED.[Percent2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent2] Is Null And
				DELETED.[Percent2] Is Not Null
			) Or
			(
				INSERTED.[Percent2] Is Not Null And
				DELETED.[Percent2] Is Null
			) Or
			(
				INSERTED.[Percent2] !=
				DELETED.[Percent2]
			)
		) 
		END		
		
      If UPDATE([Fee2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee2',
      CONVERT(NVARCHAR(2000),DELETED.[Fee2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee2] Is Null And
				DELETED.[Fee2] Is Not Null
			) Or
			(
				INSERTED.[Fee2] Is Not Null And
				DELETED.[Fee2] Is Null
			) Or
			(
				INSERTED.[Fee2] !=
				DELETED.[Fee2]
			)
		) 
		END		
		
      If UPDATE([Percent3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent3',
      CONVERT(NVARCHAR(2000),DELETED.[Percent3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent3] Is Null And
				DELETED.[Percent3] Is Not Null
			) Or
			(
				INSERTED.[Percent3] Is Not Null And
				DELETED.[Percent3] Is Null
			) Or
			(
				INSERTED.[Percent3] !=
				DELETED.[Percent3]
			)
		) 
		END		
		
      If UPDATE([Fee3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee3',
      CONVERT(NVARCHAR(2000),DELETED.[Fee3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee3] Is Null And
				DELETED.[Fee3] Is Not Null
			) Or
			(
				INSERTED.[Fee3] Is Not Null And
				DELETED.[Fee3] Is Null
			) Or
			(
				INSERTED.[Fee3] !=
				DELETED.[Fee3]
			)
		) 
		END		
		
      If UPDATE([Percent4])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent4',
      CONVERT(NVARCHAR(2000),DELETED.[Percent4],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent4],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent4] Is Null And
				DELETED.[Percent4] Is Not Null
			) Or
			(
				INSERTED.[Percent4] Is Not Null And
				DELETED.[Percent4] Is Null
			) Or
			(
				INSERTED.[Percent4] !=
				DELETED.[Percent4]
			)
		) 
		END		
		
      If UPDATE([Fee4])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee4',
      CONVERT(NVARCHAR(2000),DELETED.[Fee4],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee4],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee4] Is Null And
				DELETED.[Fee4] Is Not Null
			) Or
			(
				INSERTED.[Fee4] Is Not Null And
				DELETED.[Fee4] Is Null
			) Or
			(
				INSERTED.[Fee4] !=
				DELETED.[Fee4]
			)
		) 
		END		
		
      If UPDATE([Percent5])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent5',
      CONVERT(NVARCHAR(2000),DELETED.[Percent5],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent5],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent5] Is Null And
				DELETED.[Percent5] Is Not Null
			) Or
			(
				INSERTED.[Percent5] Is Not Null And
				DELETED.[Percent5] Is Null
			) Or
			(
				INSERTED.[Percent5] !=
				DELETED.[Percent5]
			)
		) 
		END		
		
      If UPDATE([Fee5])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee5',
      CONVERT(NVARCHAR(2000),DELETED.[Fee5],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee5],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee5] Is Null And
				DELETED.[Fee5] Is Not Null
			) Or
			(
				INSERTED.[Fee5] Is Not Null And
				DELETED.[Fee5] Is Null
			) Or
			(
				INSERTED.[Fee5] !=
				DELETED.[Fee5]
			)
		) 
		END		
		
      If UPDATE([Percent6])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent6',
      CONVERT(NVARCHAR(2000),DELETED.[Percent6],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent6],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent6] Is Null And
				DELETED.[Percent6] Is Not Null
			) Or
			(
				INSERTED.[Percent6] Is Not Null And
				DELETED.[Percent6] Is Null
			) Or
			(
				INSERTED.[Percent6] !=
				DELETED.[Percent6]
			)
		) 
		END		
		
      If UPDATE([Fee6])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee6',
      CONVERT(NVARCHAR(2000),DELETED.[Fee6],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee6],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee6] Is Null And
				DELETED.[Fee6] Is Not Null
			) Or
			(
				INSERTED.[Fee6] Is Not Null And
				DELETED.[Fee6] Is Null
			) Or
			(
				INSERTED.[Fee6] !=
				DELETED.[Fee6]
			)
		) 
		END		
		
      If UPDATE([Percent7])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent7',
      CONVERT(NVARCHAR(2000),DELETED.[Percent7],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent7],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent7] Is Null And
				DELETED.[Percent7] Is Not Null
			) Or
			(
				INSERTED.[Percent7] Is Not Null And
				DELETED.[Percent7] Is Null
			) Or
			(
				INSERTED.[Percent7] !=
				DELETED.[Percent7]
			)
		) 
		END		
		
      If UPDATE([Fee7])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee7',
      CONVERT(NVARCHAR(2000),DELETED.[Fee7],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee7],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee7] Is Null And
				DELETED.[Fee7] Is Not Null
			) Or
			(
				INSERTED.[Fee7] Is Not Null And
				DELETED.[Fee7] Is Null
			) Or
			(
				INSERTED.[Fee7] !=
				DELETED.[Fee7]
			)
		) 
		END		
		
      If UPDATE([Percent8])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent8',
      CONVERT(NVARCHAR(2000),DELETED.[Percent8],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent8],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent8] Is Null And
				DELETED.[Percent8] Is Not Null
			) Or
			(
				INSERTED.[Percent8] Is Not Null And
				DELETED.[Percent8] Is Null
			) Or
			(
				INSERTED.[Percent8] !=
				DELETED.[Percent8]
			)
		) 
		END		
		
      If UPDATE([Fee8])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee8',
      CONVERT(NVARCHAR(2000),DELETED.[Fee8],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee8],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee8] Is Null And
				DELETED.[Fee8] Is Not Null
			) Or
			(
				INSERTED.[Fee8] Is Not Null And
				DELETED.[Fee8] Is Null
			) Or
			(
				INSERTED.[Fee8] !=
				DELETED.[Fee8]
			)
		) 
		END		
		
      If UPDATE([Percent9])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent9',
      CONVERT(NVARCHAR(2000),DELETED.[Percent9],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent9],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent9] Is Null And
				DELETED.[Percent9] Is Not Null
			) Or
			(
				INSERTED.[Percent9] Is Not Null And
				DELETED.[Percent9] Is Null
			) Or
			(
				INSERTED.[Percent9] !=
				DELETED.[Percent9]
			)
		) 
		END		
		
      If UPDATE([Fee9])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee9',
      CONVERT(NVARCHAR(2000),DELETED.[Fee9],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee9],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee9] Is Null And
				DELETED.[Fee9] Is Not Null
			) Or
			(
				INSERTED.[Fee9] Is Not Null And
				DELETED.[Fee9] Is Null
			) Or
			(
				INSERTED.[Fee9] !=
				DELETED.[Fee9]
			)
		) 
		END		
		
      If UPDATE([Percent10])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent10',
      CONVERT(NVARCHAR(2000),DELETED.[Percent10],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent10],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent10] Is Null And
				DELETED.[Percent10] Is Not Null
			) Or
			(
				INSERTED.[Percent10] Is Not Null And
				DELETED.[Percent10] Is Null
			) Or
			(
				INSERTED.[Percent10] !=
				DELETED.[Percent10]
			)
		) 
		END		
		
      If UPDATE([Fee10])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee10',
      CONVERT(NVARCHAR(2000),DELETED.[Fee10],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee10],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee10] Is Null And
				DELETED.[Fee10] Is Not Null
			) Or
			(
				INSERTED.[Fee10] Is Not Null And
				DELETED.[Fee10] Is Null
			) Or
			(
				INSERTED.[Fee10] !=
				DELETED.[Fee10]
			)
		) 
		END		
		
      If UPDATE([Percent11])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent11',
      CONVERT(NVARCHAR(2000),DELETED.[Percent11],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent11],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent11] Is Null And
				DELETED.[Percent11] Is Not Null
			) Or
			(
				INSERTED.[Percent11] Is Not Null And
				DELETED.[Percent11] Is Null
			) Or
			(
				INSERTED.[Percent11] !=
				DELETED.[Percent11]
			)
		) 
		END		
		
      If UPDATE([Fee11])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee11',
      CONVERT(NVARCHAR(2000),DELETED.[Fee11],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee11],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee11] Is Null And
				DELETED.[Fee11] Is Not Null
			) Or
			(
				INSERTED.[Fee11] Is Not Null And
				DELETED.[Fee11] Is Null
			) Or
			(
				INSERTED.[Fee11] !=
				DELETED.[Fee11]
			)
		) 
		END		
		
      If UPDATE([Percent12])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent12',
      CONVERT(NVARCHAR(2000),DELETED.[Percent12],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent12],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent12] Is Null And
				DELETED.[Percent12] Is Not Null
			) Or
			(
				INSERTED.[Percent12] Is Not Null And
				DELETED.[Percent12] Is Null
			) Or
			(
				INSERTED.[Percent12] !=
				DELETED.[Percent12]
			)
		) 
		END		
		
      If UPDATE([Fee12])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee12',
      CONVERT(NVARCHAR(2000),DELETED.[Fee12],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee12],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee12] Is Null And
				DELETED.[Fee12] Is Not Null
			) Or
			(
				INSERTED.[Fee12] Is Not Null And
				DELETED.[Fee12] Is Null
			) Or
			(
				INSERTED.[Fee12] !=
				DELETED.[Fee12]
			)
		) 
		END		
		
      If UPDATE([Percent13])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent13',
      CONVERT(NVARCHAR(2000),DELETED.[Percent13],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent13],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent13] Is Null And
				DELETED.[Percent13] Is Not Null
			) Or
			(
				INSERTED.[Percent13] Is Not Null And
				DELETED.[Percent13] Is Null
			) Or
			(
				INSERTED.[Percent13] !=
				DELETED.[Percent13]
			)
		) 
		END		
		
      If UPDATE([Fee13])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee13',
      CONVERT(NVARCHAR(2000),DELETED.[Fee13],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee13],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee13] Is Null And
				DELETED.[Fee13] Is Not Null
			) Or
			(
				INSERTED.[Fee13] Is Not Null And
				DELETED.[Fee13] Is Null
			) Or
			(
				INSERTED.[Fee13] !=
				DELETED.[Fee13]
			)
		) 
		END		
		
      If UPDATE([Percent14])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent14',
      CONVERT(NVARCHAR(2000),DELETED.[Percent14],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent14],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent14] Is Null And
				DELETED.[Percent14] Is Not Null
			) Or
			(
				INSERTED.[Percent14] Is Not Null And
				DELETED.[Percent14] Is Null
			) Or
			(
				INSERTED.[Percent14] !=
				DELETED.[Percent14]
			)
		) 
		END		
		
      If UPDATE([Fee14])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee14',
      CONVERT(NVARCHAR(2000),DELETED.[Fee14],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee14],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee14] Is Null And
				DELETED.[Fee14] Is Not Null
			) Or
			(
				INSERTED.[Fee14] Is Not Null And
				DELETED.[Fee14] Is Null
			) Or
			(
				INSERTED.[Fee14] !=
				DELETED.[Fee14]
			)
		) 
		END		
		
      If UPDATE([Percent15])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent15',
      CONVERT(NVARCHAR(2000),DELETED.[Percent15],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent15],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent15] Is Null And
				DELETED.[Percent15] Is Not Null
			) Or
			(
				INSERTED.[Percent15] Is Not Null And
				DELETED.[Percent15] Is Null
			) Or
			(
				INSERTED.[Percent15] !=
				DELETED.[Percent15]
			)
		) 
		END		
		
      If UPDATE([Fee15])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee15',
      CONVERT(NVARCHAR(2000),DELETED.[Fee15],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee15],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee15] Is Null And
				DELETED.[Fee15] Is Not Null
			) Or
			(
				INSERTED.[Fee15] Is Not Null And
				DELETED.[Fee15] Is Null
			) Or
			(
				INSERTED.[Fee15] !=
				DELETED.[Fee15]
			)
		) 
		END		
		
      If UPDATE([Percent16])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent16',
      CONVERT(NVARCHAR(2000),DELETED.[Percent16],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent16],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent16] Is Null And
				DELETED.[Percent16] Is Not Null
			) Or
			(
				INSERTED.[Percent16] Is Not Null And
				DELETED.[Percent16] Is Null
			) Or
			(
				INSERTED.[Percent16] !=
				DELETED.[Percent16]
			)
		) 
		END		
		
      If UPDATE([Fee16])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee16',
      CONVERT(NVARCHAR(2000),DELETED.[Fee16],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee16],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee16] Is Null And
				DELETED.[Fee16] Is Not Null
			) Or
			(
				INSERTED.[Fee16] Is Not Null And
				DELETED.[Fee16] Is Null
			) Or
			(
				INSERTED.[Fee16] !=
				DELETED.[Fee16]
			)
		) 
		END		
		
      If UPDATE([Percent17])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent17',
      CONVERT(NVARCHAR(2000),DELETED.[Percent17],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent17],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent17] Is Null And
				DELETED.[Percent17] Is Not Null
			) Or
			(
				INSERTED.[Percent17] Is Not Null And
				DELETED.[Percent17] Is Null
			) Or
			(
				INSERTED.[Percent17] !=
				DELETED.[Percent17]
			)
		) 
		END		
		
      If UPDATE([Fee17])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee17',
      CONVERT(NVARCHAR(2000),DELETED.[Fee17],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee17],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee17] Is Null And
				DELETED.[Fee17] Is Not Null
			) Or
			(
				INSERTED.[Fee17] Is Not Null And
				DELETED.[Fee17] Is Null
			) Or
			(
				INSERTED.[Fee17] !=
				DELETED.[Fee17]
			)
		) 
		END		
		
      If UPDATE([Percent18])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent18',
      CONVERT(NVARCHAR(2000),DELETED.[Percent18],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent18],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent18] Is Null And
				DELETED.[Percent18] Is Not Null
			) Or
			(
				INSERTED.[Percent18] Is Not Null And
				DELETED.[Percent18] Is Null
			) Or
			(
				INSERTED.[Percent18] !=
				DELETED.[Percent18]
			)
		) 
		END		
		
      If UPDATE([Fee18])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee18',
      CONVERT(NVARCHAR(2000),DELETED.[Fee18],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee18],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee18] Is Null And
				DELETED.[Fee18] Is Not Null
			) Or
			(
				INSERTED.[Fee18] Is Not Null And
				DELETED.[Fee18] Is Null
			) Or
			(
				INSERTED.[Fee18] !=
				DELETED.[Fee18]
			)
		) 
		END		
		
      If UPDATE([Percent19])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent19',
      CONVERT(NVARCHAR(2000),DELETED.[Percent19],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent19],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent19] Is Null And
				DELETED.[Percent19] Is Not Null
			) Or
			(
				INSERTED.[Percent19] Is Not Null And
				DELETED.[Percent19] Is Null
			) Or
			(
				INSERTED.[Percent19] !=
				DELETED.[Percent19]
			)
		) 
		END		
		
      If UPDATE([Fee19])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee19',
      CONVERT(NVARCHAR(2000),DELETED.[Fee19],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee19],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee19] Is Null And
				DELETED.[Fee19] Is Not Null
			) Or
			(
				INSERTED.[Fee19] Is Not Null And
				DELETED.[Fee19] Is Null
			) Or
			(
				INSERTED.[Fee19] !=
				DELETED.[Fee19]
			)
		) 
		END		
		
      If UPDATE([Percent20])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Percent20',
      CONVERT(NVARCHAR(2000),DELETED.[Percent20],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Percent20],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Percent20] Is Null And
				DELETED.[Percent20] Is Not Null
			) Or
			(
				INSERTED.[Percent20] Is Not Null And
				DELETED.[Percent20] Is Null
			) Or
			(
				INSERTED.[Percent20] !=
				DELETED.[Percent20]
			)
		) 
		END		
		
      If UPDATE([Fee20])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'Fee20',
      CONVERT(NVARCHAR(2000),DELETED.[Fee20],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fee20],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[Fee20] Is Null And
				DELETED.[Fee20] Is Not Null
			) Or
			(
				INSERTED.[Fee20] Is Not Null And
				DELETED.[Fee20] Is Null
			) Or
			(
				INSERTED.[Fee20] !=
				DELETED.[Fee20]
			)
		) 
		END		
		
      If UPDATE([PctComplete1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete1',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete1] Is Null And
				DELETED.[PctComplete1] Is Not Null
			) Or
			(
				INSERTED.[PctComplete1] Is Not Null And
				DELETED.[PctComplete1] Is Null
			) Or
			(
				INSERTED.[PctComplete1] !=
				DELETED.[PctComplete1]
			)
		) 
		END		
		
      If UPDATE([EarnedFee1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee1',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee1] Is Null And
				DELETED.[EarnedFee1] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee1] Is Not Null And
				DELETED.[EarnedFee1] Is Null
			) Or
			(
				INSERTED.[EarnedFee1] !=
				DELETED.[EarnedFee1]
			)
		) 
		END		
		
      If UPDATE([PctComplete2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete2',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete2] Is Null And
				DELETED.[PctComplete2] Is Not Null
			) Or
			(
				INSERTED.[PctComplete2] Is Not Null And
				DELETED.[PctComplete2] Is Null
			) Or
			(
				INSERTED.[PctComplete2] !=
				DELETED.[PctComplete2]
			)
		) 
		END		
		
      If UPDATE([EarnedFee2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee2',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee2] Is Null And
				DELETED.[EarnedFee2] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee2] Is Not Null And
				DELETED.[EarnedFee2] Is Null
			) Or
			(
				INSERTED.[EarnedFee2] !=
				DELETED.[EarnedFee2]
			)
		) 
		END		
		
      If UPDATE([PctComplete3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete3',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete3] Is Null And
				DELETED.[PctComplete3] Is Not Null
			) Or
			(
				INSERTED.[PctComplete3] Is Not Null And
				DELETED.[PctComplete3] Is Null
			) Or
			(
				INSERTED.[PctComplete3] !=
				DELETED.[PctComplete3]
			)
		) 
		END		
		
      If UPDATE([EarnedFee3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee3',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee3] Is Null And
				DELETED.[EarnedFee3] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee3] Is Not Null And
				DELETED.[EarnedFee3] Is Null
			) Or
			(
				INSERTED.[EarnedFee3] !=
				DELETED.[EarnedFee3]
			)
		) 
		END		
		
      If UPDATE([PctComplete4])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete4',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete4],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete4],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete4] Is Null And
				DELETED.[PctComplete4] Is Not Null
			) Or
			(
				INSERTED.[PctComplete4] Is Not Null And
				DELETED.[PctComplete4] Is Null
			) Or
			(
				INSERTED.[PctComplete4] !=
				DELETED.[PctComplete4]
			)
		) 
		END		
		
      If UPDATE([EarnedFee4])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee4',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee4],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee4],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee4] Is Null And
				DELETED.[EarnedFee4] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee4] Is Not Null And
				DELETED.[EarnedFee4] Is Null
			) Or
			(
				INSERTED.[EarnedFee4] !=
				DELETED.[EarnedFee4]
			)
		) 
		END		
		
      If UPDATE([PctComplete5])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete5',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete5],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete5],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete5] Is Null And
				DELETED.[PctComplete5] Is Not Null
			) Or
			(
				INSERTED.[PctComplete5] Is Not Null And
				DELETED.[PctComplete5] Is Null
			) Or
			(
				INSERTED.[PctComplete5] !=
				DELETED.[PctComplete5]
			)
		) 
		END		
		
      If UPDATE([EarnedFee5])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee5',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee5],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee5],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee5] Is Null And
				DELETED.[EarnedFee5] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee5] Is Not Null And
				DELETED.[EarnedFee5] Is Null
			) Or
			(
				INSERTED.[EarnedFee5] !=
				DELETED.[EarnedFee5]
			)
		) 
		END		
		
      If UPDATE([PctComplete6])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete6',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete6],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete6],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete6] Is Null And
				DELETED.[PctComplete6] Is Not Null
			) Or
			(
				INSERTED.[PctComplete6] Is Not Null And
				DELETED.[PctComplete6] Is Null
			) Or
			(
				INSERTED.[PctComplete6] !=
				DELETED.[PctComplete6]
			)
		) 
		END		
		
      If UPDATE([EarnedFee6])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee6',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee6],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee6],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee6] Is Null And
				DELETED.[EarnedFee6] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee6] Is Not Null And
				DELETED.[EarnedFee6] Is Null
			) Or
			(
				INSERTED.[EarnedFee6] !=
				DELETED.[EarnedFee6]
			)
		) 
		END		
		
      If UPDATE([PctComplete7])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete7',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete7],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete7],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete7] Is Null And
				DELETED.[PctComplete7] Is Not Null
			) Or
			(
				INSERTED.[PctComplete7] Is Not Null And
				DELETED.[PctComplete7] Is Null
			) Or
			(
				INSERTED.[PctComplete7] !=
				DELETED.[PctComplete7]
			)
		) 
		END		
		
      If UPDATE([EarnedFee7])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee7',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee7],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee7],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee7] Is Null And
				DELETED.[EarnedFee7] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee7] Is Not Null And
				DELETED.[EarnedFee7] Is Null
			) Or
			(
				INSERTED.[EarnedFee7] !=
				DELETED.[EarnedFee7]
			)
		) 
		END		
		
      If UPDATE([PctComplete8])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete8',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete8],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete8],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete8] Is Null And
				DELETED.[PctComplete8] Is Not Null
			) Or
			(
				INSERTED.[PctComplete8] Is Not Null And
				DELETED.[PctComplete8] Is Null
			) Or
			(
				INSERTED.[PctComplete8] !=
				DELETED.[PctComplete8]
			)
		) 
		END		
		
      If UPDATE([EarnedFee8])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee8',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee8],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee8],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee8] Is Null And
				DELETED.[EarnedFee8] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee8] Is Not Null And
				DELETED.[EarnedFee8] Is Null
			) Or
			(
				INSERTED.[EarnedFee8] !=
				DELETED.[EarnedFee8]
			)
		) 
		END		
		
      If UPDATE([PctComplete9])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete9',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete9],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete9],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete9] Is Null And
				DELETED.[PctComplete9] Is Not Null
			) Or
			(
				INSERTED.[PctComplete9] Is Not Null And
				DELETED.[PctComplete9] Is Null
			) Or
			(
				INSERTED.[PctComplete9] !=
				DELETED.[PctComplete9]
			)
		) 
		END		
		
      If UPDATE([EarnedFee9])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee9',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee9],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee9],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee9] Is Null And
				DELETED.[EarnedFee9] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee9] Is Not Null And
				DELETED.[EarnedFee9] Is Null
			) Or
			(
				INSERTED.[EarnedFee9] !=
				DELETED.[EarnedFee9]
			)
		) 
		END		
		
      If UPDATE([PctComplete10])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete10',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete10],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete10],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete10] Is Null And
				DELETED.[PctComplete10] Is Not Null
			) Or
			(
				INSERTED.[PctComplete10] Is Not Null And
				DELETED.[PctComplete10] Is Null
			) Or
			(
				INSERTED.[PctComplete10] !=
				DELETED.[PctComplete10]
			)
		) 
		END		
		
      If UPDATE([EarnedFee10])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee10',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee10],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee10],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee10] Is Null And
				DELETED.[EarnedFee10] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee10] Is Not Null And
				DELETED.[EarnedFee10] Is Null
			) Or
			(
				INSERTED.[EarnedFee10] !=
				DELETED.[EarnedFee10]
			)
		) 
		END		
		
      If UPDATE([PctComplete11])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete11',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete11],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete11],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete11] Is Null And
				DELETED.[PctComplete11] Is Not Null
			) Or
			(
				INSERTED.[PctComplete11] Is Not Null And
				DELETED.[PctComplete11] Is Null
			) Or
			(
				INSERTED.[PctComplete11] !=
				DELETED.[PctComplete11]
			)
		) 
		END		
		
      If UPDATE([EarnedFee11])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee11',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee11],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee11],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee11] Is Null And
				DELETED.[EarnedFee11] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee11] Is Not Null And
				DELETED.[EarnedFee11] Is Null
			) Or
			(
				INSERTED.[EarnedFee11] !=
				DELETED.[EarnedFee11]
			)
		) 
		END		
		
      If UPDATE([PctComplete12])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete12',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete12],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete12],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete12] Is Null And
				DELETED.[PctComplete12] Is Not Null
			) Or
			(
				INSERTED.[PctComplete12] Is Not Null And
				DELETED.[PctComplete12] Is Null
			) Or
			(
				INSERTED.[PctComplete12] !=
				DELETED.[PctComplete12]
			)
		) 
		END		
		
      If UPDATE([EarnedFee12])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee12',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee12],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee12],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee12] Is Null And
				DELETED.[EarnedFee12] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee12] Is Not Null And
				DELETED.[EarnedFee12] Is Null
			) Or
			(
				INSERTED.[EarnedFee12] !=
				DELETED.[EarnedFee12]
			)
		) 
		END		
		
      If UPDATE([PctComplete13])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete13',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete13],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete13],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete13] Is Null And
				DELETED.[PctComplete13] Is Not Null
			) Or
			(
				INSERTED.[PctComplete13] Is Not Null And
				DELETED.[PctComplete13] Is Null
			) Or
			(
				INSERTED.[PctComplete13] !=
				DELETED.[PctComplete13]
			)
		) 
		END		
		
      If UPDATE([EarnedFee13])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee13',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee13],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee13],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee13] Is Null And
				DELETED.[EarnedFee13] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee13] Is Not Null And
				DELETED.[EarnedFee13] Is Null
			) Or
			(
				INSERTED.[EarnedFee13] !=
				DELETED.[EarnedFee13]
			)
		) 
		END		
		
      If UPDATE([PctComplete14])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete14',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete14],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete14],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete14] Is Null And
				DELETED.[PctComplete14] Is Not Null
			) Or
			(
				INSERTED.[PctComplete14] Is Not Null And
				DELETED.[PctComplete14] Is Null
			) Or
			(
				INSERTED.[PctComplete14] !=
				DELETED.[PctComplete14]
			)
		) 
		END		
		
      If UPDATE([EarnedFee14])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee14',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee14],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee14],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee14] Is Null And
				DELETED.[EarnedFee14] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee14] Is Not Null And
				DELETED.[EarnedFee14] Is Null
			) Or
			(
				INSERTED.[EarnedFee14] !=
				DELETED.[EarnedFee14]
			)
		) 
		END		
		
      If UPDATE([PctComplete15])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete15',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete15],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete15],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete15] Is Null And
				DELETED.[PctComplete15] Is Not Null
			) Or
			(
				INSERTED.[PctComplete15] Is Not Null And
				DELETED.[PctComplete15] Is Null
			) Or
			(
				INSERTED.[PctComplete15] !=
				DELETED.[PctComplete15]
			)
		) 
		END		
		
      If UPDATE([EarnedFee15])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee15',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee15],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee15],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee15] Is Null And
				DELETED.[EarnedFee15] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee15] Is Not Null And
				DELETED.[EarnedFee15] Is Null
			) Or
			(
				INSERTED.[EarnedFee15] !=
				DELETED.[EarnedFee15]
			)
		) 
		END		
		
      If UPDATE([PctComplete16])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete16',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete16],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete16],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete16] Is Null And
				DELETED.[PctComplete16] Is Not Null
			) Or
			(
				INSERTED.[PctComplete16] Is Not Null And
				DELETED.[PctComplete16] Is Null
			) Or
			(
				INSERTED.[PctComplete16] !=
				DELETED.[PctComplete16]
			)
		) 
		END		
		
      If UPDATE([EarnedFee16])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee16',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee16],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee16],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee16] Is Null And
				DELETED.[EarnedFee16] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee16] Is Not Null And
				DELETED.[EarnedFee16] Is Null
			) Or
			(
				INSERTED.[EarnedFee16] !=
				DELETED.[EarnedFee16]
			)
		) 
		END		
		
      If UPDATE([PctComplete17])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete17',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete17],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete17],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete17] Is Null And
				DELETED.[PctComplete17] Is Not Null
			) Or
			(
				INSERTED.[PctComplete17] Is Not Null And
				DELETED.[PctComplete17] Is Null
			) Or
			(
				INSERTED.[PctComplete17] !=
				DELETED.[PctComplete17]
			)
		) 
		END		
		
      If UPDATE([EarnedFee17])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee17',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee17],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee17],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee17] Is Null And
				DELETED.[EarnedFee17] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee17] Is Not Null And
				DELETED.[EarnedFee17] Is Null
			) Or
			(
				INSERTED.[EarnedFee17] !=
				DELETED.[EarnedFee17]
			)
		) 
		END		
		
      If UPDATE([PctComplete18])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete18',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete18],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete18],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete18] Is Null And
				DELETED.[PctComplete18] Is Not Null
			) Or
			(
				INSERTED.[PctComplete18] Is Not Null And
				DELETED.[PctComplete18] Is Null
			) Or
			(
				INSERTED.[PctComplete18] !=
				DELETED.[PctComplete18]
			)
		) 
		END		
		
      If UPDATE([EarnedFee18])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee18',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee18],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee18],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee18] Is Null And
				DELETED.[EarnedFee18] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee18] Is Not Null And
				DELETED.[EarnedFee18] Is Null
			) Or
			(
				INSERTED.[EarnedFee18] !=
				DELETED.[EarnedFee18]
			)
		) 
		END		
		
      If UPDATE([PctComplete19])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete19',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete19],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete19],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete19] Is Null And
				DELETED.[PctComplete19] Is Not Null
			) Or
			(
				INSERTED.[PctComplete19] Is Not Null And
				DELETED.[PctComplete19] Is Null
			) Or
			(
				INSERTED.[PctComplete19] !=
				DELETED.[PctComplete19]
			)
		) 
		END		
		
      If UPDATE([EarnedFee19])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee19',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee19],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee19],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee19] Is Null And
				DELETED.[EarnedFee19] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee19] Is Not Null And
				DELETED.[EarnedFee19] Is Null
			) Or
			(
				INSERTED.[EarnedFee19] !=
				DELETED.[EarnedFee19]
			)
		) 
		END		
		
      If UPDATE([PctComplete20])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'PctComplete20',
      CONVERT(NVARCHAR(2000),DELETED.[PctComplete20],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PctComplete20],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[PctComplete20] Is Null And
				DELETED.[PctComplete20] Is Not Null
			) Or
			(
				INSERTED.[PctComplete20] Is Not Null And
				DELETED.[PctComplete20] Is Null
			) Or
			(
				INSERTED.[PctComplete20] !=
				DELETED.[PctComplete20]
			)
		) 
		END		
		
      If UPDATE([EarnedFee20])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[PhaseCode],121),'EarnedFee20',
      CONVERT(NVARCHAR(2000),DELETED.[EarnedFee20],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EarnedFee20],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[PhaseCode] = DELETED.[PhaseCode] AND 
		(
			(
				INSERTED.[EarnedFee20] Is Null And
				DELETED.[EarnedFee20] Is Not Null
			) Or
			(
				INSERTED.[EarnedFee20] Is Not Null And
				DELETED.[EarnedFee20] Is Null
			) Or
			(
				INSERTED.[EarnedFee20] !=
				DELETED.[EarnedFee20]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_PRFEPhases] ON [dbo].[PRFEPhases]
GO
ALTER TABLE [dbo].[PRFEPhases] ADD CONSTRAINT [PRFEPhasesPK] PRIMARY KEY NONCLUSTERED ([WBS1], [PhaseCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
