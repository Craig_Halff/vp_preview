CREATE TABLE [dbo].[BTECCats]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__BTECCats___Table__5423B5A5] DEFAULT ((0)),
[Category] [smallint] NOT NULL CONSTRAINT [DF__BTECCats___Categ__5517D9DE] DEFAULT ((0)),
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Multiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BTECCats___Multi__560BFE17] DEFAULT ((0)),
[SortSeq] [smallint] NOT NULL CONSTRAINT [DF__BTECCats___SortS__57002250] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTECCats] ADD CONSTRAINT [BTECCatsPK] PRIMARY KEY NONCLUSTERED ([TableNo], [Category]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
