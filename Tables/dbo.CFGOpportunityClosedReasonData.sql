CREATE TABLE [dbo].[CFGOpportunityClosedReasonData]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGOpportunityClosedReasonData] ADD CONSTRAINT [CFGOpportunityClosedReasonDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
