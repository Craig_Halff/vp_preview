CREATE TABLE [dbo].[AnalysisCubesServer]
(
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DWServer] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ASServer] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AnalysisCubesServer] ADD CONSTRAINT [AnalysisCubesServerPK] PRIMARY KEY CLUSTERED ([UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
