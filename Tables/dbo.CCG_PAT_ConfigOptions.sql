CREATE TABLE [dbo].[CCG_PAT_ConfigOptions]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[ConfigID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateChanged] [datetime] NULL,
[ChangedBy] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Detail] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_ConfigOptions] ADD CONSTRAINT [PK_CCG_PAT_ConfigOptions] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Configuration options for various areas of the application, such as Batch Import, Export PDF, and Advanced Search.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigOptions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The modification user or the user for which this configuration applies', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigOptions', 'COLUMN', N'ChangedBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Application assign identifier used for some types', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigOptions', 'COLUMN', N'ConfigID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Last modified date', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigOptions', 'COLUMN', N'DateChanged'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Description for the option', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigOptions', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Detail values for the option, usually in XML or name value pair syntax', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigOptions', 'COLUMN', N'Detail'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigOptions', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Type of option (Batch load, Grid Setting, etc)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigOptions', 'COLUMN', N'Type'
GO
