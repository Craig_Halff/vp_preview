CREATE TABLE [dbo].[New Services List]
(
[SF330] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Category] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Subcategory] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Service] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
