CREATE TABLE [dbo].[CFGPRResponsibilityData]
(
[Code] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPRResponsibilityData] ADD CONSTRAINT [CFGPRResponsibilityDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
