CREATE TABLE [dbo].[FW_ReportSubscr]
(
[Dashpart_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DashUserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportType] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_ReportSubscr] ADD CONSTRAINT [FW_ReportSubscrPK] PRIMARY KEY NONCLUSTERED ([Dashpart_UID], [DashUserName], [UserName], [Name], [ReportType]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
