CREATE TABLE [dbo].[LedgerAPTax]
(
[Period] [int] NOT NULL CONSTRAINT [DF__LedgerAPT__Perio__3500DB5E] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__LedgerAPT__PostS__35F4FF97] DEFAULT ((0)),
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReverseCharge] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerAPT__Rever__36E923D0] DEFAULT ('N'),
[Seq] [smallint] NOT NULL CONSTRAINT [DF__LedgerAPTax__Seq__37DD4809] DEFAULT ((0)),
[TaxAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAPT__TaxAm__38D16C42] DEFAULT ((0)),
[TaxCBAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAPT__TaxCB__39C5907B] DEFAULT ((0)),
[CompoundOnTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAPT__Amoun__3AB9B4B4] DEFAULT ((0)),
[CBAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAPT__CBAmo__3BADD8ED] DEFAULT ((0)),
[TaxAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAPT__TaxAm__3CA1FD26] DEFAULT ((0)),
[TaxCBAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAPT__TaxCB__3D96215F] DEFAULT ((0)),
[TaxAmountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAPT__TaxAm__3E8A4598] DEFAULT ((0)),
[TaxCBAmountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAPT__TaxCB__3F7E69D1] DEFAULT ((0)),
[TaxAmountSourceCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAPT__TaxAm__40728E0A] DEFAULT ((0)),
[NonRecoverTaxPercent] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAPT__NonRe__4166B243] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LedgerAPTax] ADD CONSTRAINT [LedgerAPTaxPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [PKey], [TaxCode], [ReverseCharge]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
