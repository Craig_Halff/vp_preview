CREATE TABLE [dbo].[BTECAccts]
(
[TableNo] [int] NOT NULL CONSTRAINT [DF__BTECAccts__Table__514748FA] DEFAULT ((0)),
[Category] [smallint] NOT NULL CONSTRAINT [DF__BTECAccts__Categ__523B6D33] DEFAULT ((0)),
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTECAccts] ADD CONSTRAINT [BTECAcctsPK] PRIMARY KEY NONCLUSTERED ([TableNo], [Category], [Account]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
