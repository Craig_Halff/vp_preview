CREATE TABLE [dbo].[CCG_PAT_ConfigRoles]
(
[Role] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VisionField] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColumnLabel] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColumnOrder] [int] NOT NULL,
[RoleDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MarkupColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HighlightColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HidePdfTools] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdvancedOptions] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowMessaging] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_ConfigRoles] ADD CONSTRAINT [PK_CCG_PAT_ConfigRoles_1] PRIMARY KEY CLUSTERED ([Role]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Role configurations and settings', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoles', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Various role options. Fields and features you want to expose per role.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoles', 'COLUMN', N'AdvancedOptions'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoles', 'COLUMN', N'AllowMessaging'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated - see CCG_PAT_ConfigRolesDescriptions', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoles', 'COLUMN', N'ColumnLabel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The order in which you want to see the roles in dropdown menus', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoles', 'COLUMN', N'ColumnOrder'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Each invoice PDF has normal PDF toolbar options. You can elect to hide some of them from different roles.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoles', 'COLUMN', N'HidePdfTools'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The color for the highlight of each role. Different colors can be used for each role to identify the role that made a text highlight.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoles', 'COLUMN', N'HighlightColor'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The color to use for this role when doing mark-ups to PDFs', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoles', 'COLUMN', N'MarkupColor'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Role name/key', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoles', 'COLUMN', N'Role'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated - see CCG_PAT_ConfigRolesDescriptions', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoles', 'COLUMN', N'RoleDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Status of the role - (A)ctive / (I)nactive', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoles', 'COLUMN', N'Status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Future use', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoles', 'COLUMN', N'VisionField'
GO
