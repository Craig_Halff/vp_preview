CREATE TABLE [dbo].[ETChargeGroupMP]
(
[Seq] [int] NOT NULL,
[Segment1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Segment2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Segment3] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Segment4] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Segment5] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SegmentDesc1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SegmentDesc2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SegmentDesc3] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SegmentDesc4] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SegmentDesc5] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VisionTable1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VisionTable2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VisionTable3] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VisionTable4] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VisionTable5] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WhereClause1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WhereClause2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WhereClause3] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WhereClause4] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WhereClause5] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ETChargeGroupMP] ADD CONSTRAINT [ETChargeGroupMPPK] PRIMARY KEY CLUSTERED ([Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
