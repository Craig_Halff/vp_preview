CREATE TABLE [dbo].[RPTopDown]
(
[TopDownID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FeePercentage] [smallint] NOT NULL CONSTRAINT [DF__RPTopDown__FeePe__7586401E] DEFAULT ((0)),
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPTopDown__Amoun__767A6457] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[SortSeq] [smallint] NOT NULL CONSTRAINT [DF__RPTopDown__SortS__776E8890] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPTopDown] ADD CONSTRAINT [RPTopDownPK] PRIMARY KEY NONCLUSTERED ([TopDownID], [PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
