CREATE TABLE [dbo].[ICBillInvSums]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__ICBillInv__Perio__6BFC0A8D] DEFAULT ((0)),
[RunSeq] [int] NOT NULL CONSTRAINT [DF__ICBillInv__RunSe__6CF02EC6] DEFAULT ((0)),
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecdSeq] [int] NOT NULL CONSTRAINT [DF__ICBillInv__RecdS__6DE452FF] DEFAULT ((0)),
[ArrayType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BillWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VoucherTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Section] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubSection] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BaseAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ICBillInv__BaseA__6ED87738] DEFAULT ((0)),
[FinalAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ICBillInv__Final__6FCC9B71] DEFAULT ((0)),
[InvoiceAmountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ICBillInv__Invoi__70C0BFAA] DEFAULT ((0)),
[VoucherAmountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ICBillInv__Vouch__71B4E3E3] DEFAULT ((0)),
[VoucherTaxAmountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ICBillInv__Vouch__72A9081C] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ICBillInvSums] ADD CONSTRAINT [ICBillInvSumsPK] PRIMARY KEY NONCLUSTERED ([Company], [Period], [RunSeq], [Invoice], [MainWBS1], [RecdSeq], [ArrayType]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
