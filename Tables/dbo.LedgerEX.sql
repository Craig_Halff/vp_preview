CREATE TABLE [dbo].[LedgerEX]
(
[Period] [int] NOT NULL CONSTRAINT [DF__LedgerEX___Perio__7F63E4BC] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__LedgerEX___PostS__005808F5] DEFAULT ((0)),
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransDate] [datetime] NULL,
[Desc1] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Desc2] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___Amoun__014C2D2E] DEFAULT ((0)),
[CBAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___CBAmo__02405167] DEFAULT ((0)),
[BillExt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___BillE__033475A0] DEFAULT ((0)),
[ProjectCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerEX___Proje__042899D9] DEFAULT ('N'),
[AutoEntry] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerEX___AutoE__051CBE12] DEFAULT ('N'),
[SuppressBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerEX___Suppr__0610E24B] DEFAULT ('N'),
[BillStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SkipGL] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerEX___SkipG__07050684] DEFAULT ('N'),
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceSection] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Line] [smallint] NOT NULL CONSTRAINT [DF__LedgerEX_N__Line__07F92ABD] DEFAULT ((0)),
[PartialPayment] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___Parti__08ED4EF6] DEFAULT ((0)),
[Discount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___Disco__09E1732F] DEFAULT ((0)),
[Voucher] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledInvoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledPeriod] [int] NOT NULL CONSTRAINT [DF__LedgerEX___Bille__0AD59768] DEFAULT ((0)),
[Unit] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitTable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitQuantity] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___UnitQ__0BC9BBA1] DEFAULT ((0)),
[UnitCostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___UnitC__0CBDDFDA] DEFAULT ((0)),
[UnitBillingRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___UnitB__0DB20413] DEFAULT ((0)),
[UnitBillExt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___UnitB__0EA6284C] DEFAULT ((0)),
[XferWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___TaxBa__0F9A4C85] DEFAULT ((0)),
[TaxCBBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___TaxCB__108E70BE] DEFAULT ((0)),
[BillTaxCodeOverride] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WrittenOffPeriod] [int] NOT NULL CONSTRAINT [DF__LedgerEX___Writt__118294F7] DEFAULT ((0)),
[TransactionAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___Trans__1276B930] DEFAULT ((0)),
[TransactionCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___Amoun__136ADD69] DEFAULT ((0)),
[ProjectExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___Amoun__145F01A2] DEFAULT ((0)),
[BillingExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoEntryAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___AutoE__155325DB] DEFAULT ((0)),
[AutoEntryExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoEntryOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoEntryAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountSourceCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___Amoun__16474A14] DEFAULT ((0)),
[SourceExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PONumber] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitCostRateBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___UnitC__173B6E4D] DEFAULT ((0)),
[LinkCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillTax2CodeOverride] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GainsAndLossesType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___Amoun__182F9286] DEFAULT ((0)),
[CBAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___CBAmo__1923B6BF] DEFAULT ((0)),
[TaxBasisTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___TaxBa__1A17DAF8] DEFAULT ((0)),
[TaxCBBasisTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___TaxCB__1B0BFF31] DEFAULT ((0)),
[TaxBasisFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___TaxBa__1C00236A] DEFAULT ((0)),
[TaxCBBasisFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___TaxCB__1CF447A3] DEFAULT ((0)),
[DiscountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___Disco__1DE86BDC] DEFAULT ((0)),
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RealizationAmountEmployeeCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___Reali__1EDC9015] DEFAULT ((0)),
[RealizationAmountProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___Reali__1FD0B44E] DEFAULT ((0)),
[RealizationAmountBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___Reali__20C4D887] DEFAULT ((0)),
[NonBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__LedgerEX___NonBi__21B8FCC0] DEFAULT ('N'),
[CreditMemoRefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalAmountSourceCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerEX___Origi__22AD20F9] DEFAULT ((0)),
[OriginalPaymentCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InProcessAccount] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerEX___InPro__23A14532] DEFAULT ('N'),
[InProcessAccountCleared] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerEX___InPro__2495696B] DEFAULT ('N'),
[EKOriginalLine] [smallint] NOT NULL CONSTRAINT [DF__LedgerEX___EKOri__25898DA4] DEFAULT ((0)),
[InvoiceStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginatingVendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Diary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaryNo] [int] NOT NULL CONSTRAINT [DF__LedgerEX___Diary__267DB1DD] DEFAULT ((0)),
[CreditCardPrimaryCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransferredPeriod] [int] NOT NULL CONSTRAINT [DF__LedgerEX___Trans__2771D616] DEFAULT ((0)),
[TransferredBillStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreInvoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EquipmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BookCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastPSAExportDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LedgerEX] ADD CONSTRAINT [LedgerEXPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerEXAccountIDX] ON [dbo].[LedgerEX] ([Account]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerEXSearchIDX] ON [dbo].[LedgerEX] ([BankCode], [RefNo], [TransType], [WBS2], [WBS1], [WBS3], [Period], [PostSeq], [Employee]) INCLUDE ([TransactionAmount], [AmountSourceCurrency]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerEXBilledWBS1WBS2WBS3IDX] ON [dbo].[LedgerEX] ([BilledWBS1], [BilledWBS2], [BilledWBS3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerEXBillStatusIDX] ON [dbo].[LedgerEX] ([BillStatus]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerEXCreditCardPKeyIDX] ON [dbo].[LedgerEX] ([CreditCardPKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerEXEmployeeVoucherIDX] ON [dbo].[LedgerEX] ([Employee], [Voucher]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerEXInvoiceIDX] ON [dbo].[LedgerEX] ([Invoice]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerEXTransTypeSubTypeIDX] ON [dbo].[LedgerEX] ([TransType], [SubType]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerEXVendorVoucherIDX] ON [dbo].[LedgerEX] ([Vendor], [Voucher]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerEXWBS1AccountIDX] ON [dbo].[LedgerEX] ([WBS1], [Account]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerEXWBS1WBS2WBS3IDX] ON [dbo].[LedgerEX] ([WBS1], [WBS2], [WBS3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerEXCoveringIDX] ON [dbo].[LedgerEX] ([WBS1], [WBS2], [WBS3], [Account], [Vendor], [Employee], [Unit], [UnitTable], [TransDate], [ProjectCost], [TransType]) ON [PRIMARY]
GO
