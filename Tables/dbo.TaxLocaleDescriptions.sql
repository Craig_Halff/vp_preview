CREATE TABLE [dbo].[TaxLocaleDescriptions]
(
[Locale] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TaxLocaleDescriptions] ADD CONSTRAINT [TaxLocaleDescriptionsPK] PRIMARY KEY CLUSTERED ([Locale], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
