CREATE TABLE [dbo].[CCG_PAT_Recur]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_PAT_R__Compa__234B108C] DEFAULT (' '),
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Freq] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FreqParam] [int] NULL,
[CopyAmount] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaxRecur] [int] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[ModDate] [datetime] NULL,
[ModUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Information about recurring AP payments', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Recur', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The company that this record is associated with (for multi-company environments)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Recur', 'COLUMN', N'Company'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Re use the amount from the previous entry', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Recur', 'COLUMN', N'CopyAmount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Description for the recurring entry', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Recur', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Recurrence end', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Recur', 'COLUMN', N'EndDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Frequency: e.g. weekly, monthly', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Recur', 'COLUMN', N'Freq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Frequency data', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Recur', 'COLUMN', N'FreqParam'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of occurrences', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Recur', 'COLUMN', N'MaxRecur'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Last modified date/time', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Recur', 'COLUMN', N'ModDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Last modified employee id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Recur', 'COLUMN', N'ModUser'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Recur', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Recurrence start ', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Recur', 'COLUMN', N'StartDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Active or inactive', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Recur', 'COLUMN', N'Status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Vendor ID', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Recur', 'COLUMN', N'Vendor'
GO
