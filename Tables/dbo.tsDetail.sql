CREATE TABLE [dbo].[tsDetail]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__tsDetail_Ne__Seq__5815D8C8] DEFAULT ((0)),
[TransDate] [datetime] NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LaborCode] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___RegHr__5909FD01] DEFAULT ((0)),
[OvtHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___OvtHr__59FE213A] DEFAULT ((0)),
[TransComment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpecialOvtHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___Speci__5AF24573] DEFAULT ((0)),
[BillCategory] [smallint] NOT NULL CONSTRAINT [DF__tsDetail___BillC__5BE669AC] DEFAULT ((0)),
[Locale] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___RegAm__5CDA8DE5] DEFAULT ((0)),
[OvtAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___OvtAm__5DCEB21E] DEFAULT ((0)),
[SpecialOvtAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___Speci__5EC2D657] DEFAULT ((0)),
[ExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegAmtProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___RegAm__5FB6FA90] DEFAULT ((0)),
[OvtAmtProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___OvtAm__60AB1EC9] DEFAULT ((0)),
[SpecialOvtAmtProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___Speci__619F4302] DEFAULT ((0)),
[ProjectExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillExt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___BillE__6293673B] DEFAULT ((0)),
[OvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___OvtPc__63878B74] DEFAULT ((0)),
[SpecialOvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___Speci__647BAFAD] DEFAULT ((0)),
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail_N__Rate__656FD3E6] DEFAULT ((0)),
[OvtRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___OvtRa__6663F81F] DEFAULT ((0)),
[SpecialOvtRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___Speci__67581C58] DEFAULT ((0)),
[RateProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___RateP__684C4091] DEFAULT ((0)),
[OvtRateProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___OvtRa__694064CA] DEFAULT ((0)),
[SpecialOvtRateProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tsDetail___Speci__6A348903] DEFAULT ((0)),
[TLInternalKey] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tsDetail] ADD CONSTRAINT [tsDetailPK] PRIMARY KEY NONCLUSTERED ([Batch], [Employee], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [tsDetailWBS1EMBatchDateIDX] ON [dbo].[tsDetail] ([WBS1], [Employee], [Batch], [TransDate]) ON [PRIMARY]
GO
