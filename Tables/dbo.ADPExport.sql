CREATE TABLE [dbo].[ADPExport]
(
[PKey] [uniqueidentifier] NOT NULL CONSTRAINT [DF__ADPExport___PKey__26A703B8] DEFAULT (newid()),
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompanyCode] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileNumber] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BatchID] [nvarchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BatchDesc] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ADPExport__RegHr__279B27F1] DEFAULT ((0)),
[OvtHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ADPExport__OvtHr__288F4C2A] DEFAULT ((0)),
[Hours3Code] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hours3Amt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ADPExport__Hours__29837063] DEFAULT ((0)),
[Earnings3Code] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Earnings3Amt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ADPExport__Earni__2A77949C] DEFAULT ((0)),
[Hours4Code] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hours4Amt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ADPExport__Hours__2B6BB8D5] DEFAULT ((0)),
[Earnings4Code] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Earnings4Amt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ADPExport__Earni__2C5FDD0E] DEFAULT ((0)),
[Earnings5Code] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Earnings5Amt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ADPExport__Earni__2D540147] DEFAULT ((0)),
[MemoCode] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MemoAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ADPExport__MemoA__2E482580] DEFAULT ((0)),
[ErrorMessage] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateNumber] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ADPExport] ADD CONSTRAINT [ADPExportPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
