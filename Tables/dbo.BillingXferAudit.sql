CREATE TABLE [dbo].[BillingXferAudit]
(
[TableName] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__BillingXf__Perio__22375456] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__BillingXf__PostS__232B788F] DEFAULT ((0)),
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FromTableName] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FromPeriod] [int] NOT NULL CONSTRAINT [DF__BillingXf__FromP__241F9CC8] DEFAULT ((0)),
[FromPostSeq] [int] NOT NULL CONSTRAINT [DF__BillingXf__FromP__2513C101] DEFAULT ((0)),
[FromPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reason] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransferDetails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovedDate] [datetime] NULL,
[BillingSessionPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AlertSentPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BillingXferAudit] ADD CONSTRAINT [BillingXferAuditPK] PRIMARY KEY NONCLUSTERED ([TableName], [Period], [PostSeq], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
