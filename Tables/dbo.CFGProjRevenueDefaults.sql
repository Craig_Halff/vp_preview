CREATE TABLE [dbo].[CFGProjRevenueDefaults]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultAllocMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGProjRe__Defau__2F898FD8] DEFAULT ('e'),
[DefaultAllocTemplate] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Timescale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGProjRe__Times__307DB411] DEFAULT ('d')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGProjRevenueDefaults] ADD CONSTRAINT [CFGProjRevenueDefaultsPK] PRIMARY KEY CLUSTERED ([Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
