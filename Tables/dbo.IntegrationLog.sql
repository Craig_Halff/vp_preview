CREATE TABLE [dbo].[IntegrationLog]
(
[RequestID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CallDate] [datetime] NOT NULL,
[RecordsAdded] [int] NOT NULL CONSTRAINT [DF__IQWebServ__Recor__47899FED] DEFAULT ((0)),
[RecordsUpdated] [int] NOT NULL CONSTRAINT [DF__IQWebServ__Recor__487DC426] DEFAULT ((0)),
[CallUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TerminationMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Errors] [int] NOT NULL CONSTRAINT [DF__Integrati__Error__7AE0BD6F] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IntegrationLog] ADD CONSTRAINT [IQWebServiceLogPK] PRIMARY KEY NONCLUSTERED ([RequestID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
