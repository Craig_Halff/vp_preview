CREATE TABLE [dbo].[CFGServProServPhaseDescriptions]
(
[ServProCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGServProServPhaseDescriptions] ADD CONSTRAINT [CFGServProServPhaseDescriptionsPK] PRIMARY KEY CLUSTERED ([ServProCode], [Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
