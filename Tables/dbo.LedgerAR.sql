CREATE TABLE [dbo].[LedgerAR]
(
[Period] [int] NOT NULL CONSTRAINT [DF__LedgerAR___Perio__434EFAB5] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__LedgerAR___PostS__44431EEE] DEFAULT ((0)),
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransDate] [datetime] NULL,
[Desc1] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Desc2] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___Amoun__45374327] DEFAULT ((0)),
[CBAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___CBAmo__462B6760] DEFAULT ((0)),
[BillExt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___BillE__471F8B99] DEFAULT ((0)),
[ProjectCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerAR___Proje__4813AFD2] DEFAULT ('N'),
[AutoEntry] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerAR___AutoE__4907D40B] DEFAULT ('N'),
[SuppressBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerAR___Suppr__49FBF844] DEFAULT ('N'),
[BillStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SkipGL] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerAR___SkipG__4AF01C7D] DEFAULT ('N'),
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceSection] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Line] [smallint] NOT NULL CONSTRAINT [DF__LedgerAR_N__Line__4BE440B6] DEFAULT ((0)),
[PartialPayment] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___Parti__4CD864EF] DEFAULT ((0)),
[Discount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___Disco__4DCC8928] DEFAULT ((0)),
[Voucher] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledInvoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledPeriod] [int] NOT NULL CONSTRAINT [DF__LedgerAR___Bille__4EC0AD61] DEFAULT ((0)),
[Unit] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitTable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitQuantity] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___UnitQ__4FB4D19A] DEFAULT ((0)),
[UnitCostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___UnitC__50A8F5D3] DEFAULT ((0)),
[UnitBillingRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___UnitB__519D1A0C] DEFAULT ((0)),
[UnitBillExt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___UnitB__52913E45] DEFAULT ((0)),
[XferWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___TaxBa__5385627E] DEFAULT ((0)),
[TaxCBBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___TaxCB__547986B7] DEFAULT ((0)),
[BillTaxCodeOverride] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WrittenOffPeriod] [int] NOT NULL CONSTRAINT [DF__LedgerAR___Writt__556DAAF0] DEFAULT ((0)),
[TransactionAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___Trans__5661CF29] DEFAULT ((0)),
[TransactionCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___Amoun__5755F362] DEFAULT ((0)),
[ProjectExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___Amoun__584A179B] DEFAULT ((0)),
[BillingExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoEntryAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___AutoE__593E3BD4] DEFAULT ((0)),
[AutoEntryExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoEntryOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoEntryAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountSourceCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___Amoun__5A32600D] DEFAULT ((0)),
[SourceExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PONumber] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitCostRateBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___UnitC__5B268446] DEFAULT ((0)),
[LinkCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillTax2CodeOverride] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GainsAndLossesType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___Amoun__5C1AA87F] DEFAULT ((0)),
[CBAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___CBAmo__5D0ECCB8] DEFAULT ((0)),
[TaxBasisTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___TaxBa__5E02F0F1] DEFAULT ((0)),
[TaxCBBasisTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___TaxCB__5EF7152A] DEFAULT ((0)),
[TaxBasisFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___TaxBa__5FEB3963] DEFAULT ((0)),
[TaxCBBasisFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___TaxCB__60DF5D9C] DEFAULT ((0)),
[DiscountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___Disco__61D381D5] DEFAULT ((0)),
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RealizationAmountEmployeeCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___Reali__62C7A60E] DEFAULT ((0)),
[RealizationAmountProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___Reali__63BBCA47] DEFAULT ((0)),
[RealizationAmountBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___Reali__64AFEE80] DEFAULT ((0)),
[NonBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__LedgerAR___NonBi__65A412B9] DEFAULT ('N'),
[CreditMemoRefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalAmountSourceCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAR___Origi__669836F2] DEFAULT ((0)),
[OriginalPaymentCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InProcessAccount] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerAR___InPro__678C5B2B] DEFAULT ('N'),
[InProcessAccountCleared] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerAR___InPro__68807F64] DEFAULT ('N'),
[EKOriginalLine] [smallint] NOT NULL CONSTRAINT [DF__LedgerAR___EKOri__6974A39D] DEFAULT ((0)),
[InvoiceStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginatingVendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Diary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaryNo] [int] NOT NULL CONSTRAINT [DF__LedgerAR___Diary__6A68C7D6] DEFAULT ((0)),
[CreditCardPrimaryCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransferredPeriod] [int] NOT NULL CONSTRAINT [DF__LedgerAR___Trans__6B5CEC0F] DEFAULT ((0)),
[TransferredBillStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreInvoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EquipmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BookCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastPSAExportDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LedgerAR] ADD CONSTRAINT [LedgerARPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerARAccountIDX] ON [dbo].[LedgerAR] ([Account]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerARBilledWBS1WBS2WBS3IDX] ON [dbo].[LedgerAR] ([BilledWBS1], [BilledWBS2], [BilledWBS3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerARBillStatusIDX] ON [dbo].[LedgerAR] ([BillStatus]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerAREmployeeVoucherIDX] ON [dbo].[LedgerAR] ([Employee], [Voucher]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerAREquipIDBookCodeIDX] ON [dbo].[LedgerAR] ([EquipmentID], [BookCode]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerARInvoiceIDX] ON [dbo].[LedgerAR] ([Invoice]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerARTransTypeSubTypeIDX] ON [dbo].[LedgerAR] ([TransType], [SubType]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerARVendorVoucherIDX] ON [dbo].[LedgerAR] ([Vendor], [Voucher]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerARWBS1AccountIDX] ON [dbo].[LedgerAR] ([WBS1], [Account]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerARWBS1WBS2WBS3IDX] ON [dbo].[LedgerAR] ([WBS1], [WBS2], [WBS3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerARCoveringIDX] ON [dbo].[LedgerAR] ([WBS1], [WBS2], [WBS3], [Account], [Vendor], [Employee], [Unit], [UnitTable], [TransDate], [ProjectCost], [TransType]) INCLUDE ([Invoice]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
