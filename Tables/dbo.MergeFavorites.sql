CREATE TABLE [dbo].[MergeFavorites]
(
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MergeTemplateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[selectionName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[selectionXML] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[selectionWhere] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GlobalInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MergeFavorites] ADD CONSTRAINT [MergeFavoritesPK] PRIMARY KEY NONCLUSTERED ([UserName], [MergeTemplateID], [InfoCenterArea]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
