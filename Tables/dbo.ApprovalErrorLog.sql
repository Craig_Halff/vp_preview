CREATE TABLE [dbo].[ApprovalErrorLog]
(
[LogID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LogDate] [datetime] NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApplicationID] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MasterDate] [datetime] NULL,
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MasterStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorType] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ApprovalData] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DetailData] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientData] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecordsFixed] [int] NULL,
[OriginalTimesheetData] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ApprovalErrorLog] ADD CONSTRAINT [ApprovalErrorLogPK] PRIMARY KEY CLUSTERED ([LogID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
