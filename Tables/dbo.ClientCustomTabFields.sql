CREATE TABLE [dbo].[ClientCustomTabFields]
(
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustLegacyId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustConsolidatedClient] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustConsolidatedClientName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustSource] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustFirmType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPerDiemReimbursed] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLegacySource] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustClientAdvocate] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLegacyIdVE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustL__12833CD6] DEFAULT (''),
[CustTaxExempt] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustT__1377610F] DEFAULT ('N'),
[CustACHCheck] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustA__146B8548] DEFAULT (''),
[CustBSTVendorName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustB__155FA981] DEFAULT (''),
[CustBSTVendorAlphaName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustB__1653CDBA] DEFAULT (''),
[CustBSTVendorType] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustB__1747F1F3] DEFAULT (''),
[CustBST1099Type] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustB__183C162C] DEFAULT (''),
[CustConsolidatedVendor] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustC__19303A65] DEFAULT (''),
[CustConsolidatedVendorName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustC__1A245E9E] DEFAULT (''),
[CustLegacySourceVE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustL__1B1882D7] DEFAULT (''),
[CustVirtualCardEnrolled] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustV__1C0CA710] DEFAULT (''),
[CustPayablesFolder] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustP__1D00CB49] DEFAULT (''),
[CustSecure] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustS__1DF4EF82] DEFAULT ('N'),
[CustPATApprover1] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustP__1EE913BB] DEFAULT (''),
[CustPATThreshold1] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ClientCus__CustP__1FDD37F4] DEFAULT ((0)),
[CustPATApprover2] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustP__20D15C2D] DEFAULT (''),
[CustPATThreshold2] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__ClientCus__CustP__21C58066] DEFAULT ((0)),
[CustPATApprover3] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustP__22B9A49F] DEFAULT (''),
[CustPATRouteType] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientCus__CustP__23ADC8D8] DEFAULT (''),
[CustLogVendorInfoVerified] [datetime] NULL CONSTRAINT [DF__ClientCus__CustL__24A1ED11] DEFAULT (getdate())
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

		CREATE TRIGGER [dbo].[VisionAudit_Delete_ClientCustomTabFields]
		ON [dbo].[ClientCustomTabFields]
		FOR Delete
		NOT FOR REPLICATION
		AS BEGIN
		SET NOCOUNT ON
		DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
		SET @VisionAuditUser = dbo.FW_GetUsername()
		SET @source = dbo.GetVisionAuditSource()
		SET @app = (SELECT TOP 1 lastapp FROM FW_Useractivity WHERE userid = @VisionAuditUser ORDER BY lastaccess DESC)

		IF @VisionAuditUser = ''
		RETURN

		SET @now = dbo.GetVisionAuditTime()
		SET @table = 'ClientCustomTabFields'
	
		DECLARE @noAuditDetails varchar(1)
		SET @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		IF EXISTS(SELECT AuditKeyValuesDelete FROM FW_CFGSystem WHERE AuditKeyValuesDelete = 'Y' and @noAuditDetails = 'Y')
		BEGIN
		DECLARE @placeholder varchar(1)
		END
		ELSE
		BEGIN
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'ClientID', CONVERT(NVARCHAR(2000),DELETED.[ClientID],121), NULL, oldDesc.Name, NULL, @source, @app
		FROM DELETED LEFT JOIN CL AS oldDesc ON DELETED.ClientID = oldDesc.ClientID
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustLegacyId', CONVERT(NVARCHAR(2000),[CustLegacyId],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustConsolidatedClient', CONVERT(NVARCHAR(2000),[CustConsolidatedClient],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustConsolidatedClientName', CONVERT(NVARCHAR(2000),[CustConsolidatedClientName],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustSource', CONVERT(NVARCHAR(2000),[CustSource],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustFirmType', CONVERT(NVARCHAR(2000),[CustFirmType],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustPerDiemReimbursed', CONVERT(NVARCHAR(2000),[CustPerDiemReimbursed],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustLegacySource', CONVERT(NVARCHAR(2000),[CustLegacySource],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustClientAdvocate', CONVERT(NVARCHAR(2000),DELETED.[CustClientAdvocate],121), NULL, ISNULL(oldDesc.LastName, '') + ISNULL(', ' + oldDesc.FirstName, ''), NULL, @source, @app
		FROM DELETED LEFT JOIN EM AS oldDesc ON DELETED.CustClientAdvocate = oldDesc.Employee
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustLegacyIdVE', CONVERT(NVARCHAR(2000),[CustLegacyIdVE],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustTaxExempt', CONVERT(NVARCHAR(2000),[CustTaxExempt],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustACHCheck', CONVERT(NVARCHAR(2000),[CustACHCheck],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustBSTVendorName', CONVERT(NVARCHAR(2000),[CustBSTVendorName],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustBSTVendorAlphaName', CONVERT(NVARCHAR(2000),[CustBSTVendorAlphaName],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustBSTVendorType', CONVERT(NVARCHAR(2000),[CustBSTVendorType],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustBST1099Type', CONVERT(NVARCHAR(2000),[CustBST1099Type],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustConsolidatedVendor', CONVERT(NVARCHAR(2000),[CustConsolidatedVendor],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustConsolidatedVendorName', CONVERT(NVARCHAR(2000),[CustConsolidatedVendorName],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustLegacySourceVE', CONVERT(NVARCHAR(2000),[CustLegacySourceVE],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustVirtualCardEnrolled', CONVERT(NVARCHAR(2000),[CustVirtualCardEnrolled],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustPayablesFolder', CONVERT(NVARCHAR(2000),[CustPayablesFolder],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustSecure', CONVERT(NVARCHAR(2000),[CustSecure],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustPATApprover1', CONVERT(NVARCHAR(2000),DELETED.[CustPATApprover1],121), NULL, ISNULL(oldDesc.LastName, '') + ISNULL(', ' + oldDesc.FirstName, ''), NULL, @source, @app
		FROM DELETED LEFT JOIN EM AS oldDesc ON DELETED.CustPATApprover1 = oldDesc.Employee
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustPATThreshold1', CONVERT(NVARCHAR(2000),[CustPATThreshold1],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustPATApprover2', CONVERT(NVARCHAR(2000),DELETED.[CustPATApprover2],121), NULL, ISNULL(oldDesc.LastName, '') + ISNULL(', ' + oldDesc.FirstName, ''), NULL, @source, @app
		FROM DELETED LEFT JOIN EM AS oldDesc ON DELETED.CustPATApprover2 = oldDesc.Employee
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustPATThreshold2', CONVERT(NVARCHAR(2000),[CustPATThreshold2],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustPATApprover3', CONVERT(NVARCHAR(2000),DELETED.[CustPATApprover3],121), NULL, ISNULL(oldDesc.LastName, '') + ISNULL(', ' + oldDesc.FirstName, ''), NULL, @source, @app
		FROM DELETED LEFT JOIN EM AS oldDesc ON DELETED.CustPATApprover3 = oldDesc.Employee
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustPATRouteType', CONVERT(NVARCHAR(2000),[CustPATRouteType],121), NULL, @source, @app
		FROM DELETED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'DELETE', CONVERT(NVARCHAR(255),DELETED.[ClientID],121), 'CustLogVendorInfoVerified', CONVERT(NVARCHAR(2000),[CustLogVendorInfoVerified],121), NULL, @source, @app
		FROM DELETED
	END
		SET NOCOUNT OFF 
		END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

		CREATE TRIGGER [dbo].[VisionAudit_Insert_ClientCustomTabFields]
		ON [dbo].[ClientCustomTabFields]
		FOR Insert
		NOT FOR REPLICATION
		AS BEGIN
		SET NOCOUNT ON
		DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
		SET @VisionAuditUser = dbo.FW_GetUsername()
		SET @source = dbo.GetVisionAuditSource()
		SET @app = (SELECT TOP 1 lastapp FROM FW_Useractivity WHERE userid = @VisionAuditUser ORDER BY lastaccess DESC)

		IF @VisionAuditUser = ''
		RETURN

		SET @now = dbo.GetVisionAuditTime()
		SET @table = 'ClientCustomTabFields'
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'ClientID', NULL, CONVERT(NVARCHAR(2000),INSERTED.[ClientID],121), NULL, newDesc.Name, @source, @app
		FROM INSERTED LEFT JOIN CL AS newDesc ON INSERTED.ClientID = newDesc.ClientID
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustLegacyId', NULL, CONVERT(NVARCHAR(2000),[CustLegacyId],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustConsolidatedClient', NULL, CONVERT(NVARCHAR(2000),[CustConsolidatedClient],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustConsolidatedClientName', NULL, CONVERT(NVARCHAR(2000),[CustConsolidatedClientName],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustSource', NULL, CONVERT(NVARCHAR(2000),[CustSource],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustFirmType', NULL, CONVERT(NVARCHAR(2000),[CustFirmType],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustPerDiemReimbursed', NULL, CONVERT(NVARCHAR(2000),[CustPerDiemReimbursed],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustLegacySource', NULL, CONVERT(NVARCHAR(2000),[CustLegacySource],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustClientAdvocate', NULL, CONVERT(NVARCHAR(2000),INSERTED.[CustClientAdvocate],121), NULL, ISNULL(newDesc.LastName, '') + ISNULL(', ' + newDesc.FirstName, ''), @source, @app
		FROM INSERTED LEFT JOIN EM AS newDesc ON INSERTED.CustClientAdvocate = newDesc.Employee
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustLegacyIdVE', NULL, CONVERT(NVARCHAR(2000),[CustLegacyIdVE],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustTaxExempt', NULL, CONVERT(NVARCHAR(2000),[CustTaxExempt],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustACHCheck', NULL, CONVERT(NVARCHAR(2000),[CustACHCheck],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustBSTVendorName', NULL, CONVERT(NVARCHAR(2000),[CustBSTVendorName],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustBSTVendorAlphaName', NULL, CONVERT(NVARCHAR(2000),[CustBSTVendorAlphaName],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustBSTVendorType', NULL, CONVERT(NVARCHAR(2000),[CustBSTVendorType],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustBST1099Type', NULL, CONVERT(NVARCHAR(2000),[CustBST1099Type],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustConsolidatedVendor', NULL, CONVERT(NVARCHAR(2000),[CustConsolidatedVendor],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustConsolidatedVendorName', NULL, CONVERT(NVARCHAR(2000),[CustConsolidatedVendorName],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustLegacySourceVE', NULL, CONVERT(NVARCHAR(2000),[CustLegacySourceVE],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustVirtualCardEnrolled', NULL, CONVERT(NVARCHAR(2000),[CustVirtualCardEnrolled],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustPayablesFolder', NULL, CONVERT(NVARCHAR(2000),[CustPayablesFolder],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustSecure', NULL, CONVERT(NVARCHAR(2000),[CustSecure],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustPATApprover1', NULL, CONVERT(NVARCHAR(2000),INSERTED.[CustPATApprover1],121), NULL, ISNULL(newDesc.LastName, '') + ISNULL(', ' + newDesc.FirstName, ''), @source, @app
		FROM INSERTED LEFT JOIN EM AS newDesc ON INSERTED.CustPATApprover1 = newDesc.Employee
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustPATThreshold1', NULL, CONVERT(NVARCHAR(2000),[CustPATThreshold1],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustPATApprover2', NULL, CONVERT(NVARCHAR(2000),INSERTED.[CustPATApprover2],121), NULL, ISNULL(newDesc.LastName, '') + ISNULL(', ' + newDesc.FirstName, ''), @source, @app
		FROM INSERTED LEFT JOIN EM AS newDesc ON INSERTED.CustPATApprover2 = newDesc.Employee
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustPATThreshold2', NULL, CONVERT(NVARCHAR(2000),[CustPATThreshold2],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustPATApprover3', NULL, CONVERT(NVARCHAR(2000),INSERTED.[CustPATApprover3],121), NULL, ISNULL(newDesc.LastName, '') + ISNULL(', ' + newDesc.FirstName, ''), @source, @app
		FROM INSERTED LEFT JOIN EM AS newDesc ON INSERTED.CustPATApprover3 = newDesc.Employee
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustPATRouteType', NULL, CONVERT(NVARCHAR(2000),[CustPATRouteType],121), @source, @app
		FROM INSERTED
	
		INSERT INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'INSERT', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustLogVendorInfoVerified', NULL, CONVERT(NVARCHAR(2000),[CustLogVendorInfoVerified],121), @source, @app
		FROM INSERTED
	
		SET NOCOUNT OFF 
		END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

		CREATE TRIGGER [dbo].[VisionAudit_Update_ClientCustomTabFields]
		ON [dbo].[ClientCustomTabFields]
		FOR Update
		NOT FOR REPLICATION
		AS BEGIN
		SET NOCOUNT ON
		DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
		SET @VisionAuditUser = dbo.FW_GetUsername()
		SET @source = dbo.GetVisionAuditSource()
		SET @app = (SELECT TOP 1 lastapp FROM FW_Useractivity WHERE userid = @VisionAuditUser ORDER BY lastaccess DESC)

		IF @VisionAuditUser = ''
		RETURN

		SET @now = dbo.GetVisionAuditTime()
		SET @table = 'ClientCustomTabFields'
	
		IF UPDATE([ClientID])
		BEGIN
		INSERT
		INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'ClientID',
		CONVERT(NVARCHAR(2000),DELETED.[ClientID],121),
		CONVERT(NVARCHAR(2000),INSERTED.[ClientID],121),
		oldDesc.Name, newDesc.Name, @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[ClientID] IS NULL AND
				DELETED.[ClientID] IS NOT NULL
			) OR
			(
				INSERTED.[ClientID] IS NOT NULL AND
				DELETED.[ClientID] IS NULL
			) OR
			(
				INSERTED.[ClientID] !=
				DELETED.[ClientID]
			)
		) LEFT JOIN CL AS oldDesc ON DELETED.ClientID = oldDesc.ClientID LEFT JOIN CL AS newDesc ON INSERTED.ClientID = newDesc.ClientID
		END
	
		IF UPDATE([CustLegacyId])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustLegacyId',
		CONVERT(NVARCHAR(2000),DELETED.[CustLegacyId],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustLegacyId],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustLegacyId] IS NULL AND
				DELETED.[CustLegacyId] IS NOT NULL
			) OR
			(
				INSERTED.[CustLegacyId] IS NOT NULL AND
				DELETED.[CustLegacyId] IS NULL
			) OR
			(
				INSERTED.[CustLegacyId] !=
				DELETED.[CustLegacyId]
			)
		) 
		END		
	
		IF UPDATE([CustConsolidatedClient])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustConsolidatedClient',
		CONVERT(NVARCHAR(2000),DELETED.[CustConsolidatedClient],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustConsolidatedClient],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustConsolidatedClient] IS NULL AND
				DELETED.[CustConsolidatedClient] IS NOT NULL
			) OR
			(
				INSERTED.[CustConsolidatedClient] IS NOT NULL AND
				DELETED.[CustConsolidatedClient] IS NULL
			) OR
			(
				INSERTED.[CustConsolidatedClient] !=
				DELETED.[CustConsolidatedClient]
			)
		) 
		END		
	
		IF UPDATE([CustConsolidatedClientName])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustConsolidatedClientName',
		CONVERT(NVARCHAR(2000),DELETED.[CustConsolidatedClientName],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustConsolidatedClientName],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustConsolidatedClientName] IS NULL AND
				DELETED.[CustConsolidatedClientName] IS NOT NULL
			) OR
			(
				INSERTED.[CustConsolidatedClientName] IS NOT NULL AND
				DELETED.[CustConsolidatedClientName] IS NULL
			) OR
			(
				INSERTED.[CustConsolidatedClientName] !=
				DELETED.[CustConsolidatedClientName]
			)
		) 
		END		
	
		IF UPDATE([CustSource])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustSource',
		CONVERT(NVARCHAR(2000),DELETED.[CustSource],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustSource],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustSource] IS NULL AND
				DELETED.[CustSource] IS NOT NULL
			) OR
			(
				INSERTED.[CustSource] IS NOT NULL AND
				DELETED.[CustSource] IS NULL
			) OR
			(
				INSERTED.[CustSource] !=
				DELETED.[CustSource]
			)
		) 
		END		
	
		IF UPDATE([CustFirmType])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustFirmType',
		CONVERT(NVARCHAR(2000),DELETED.[CustFirmType],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustFirmType],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustFirmType] IS NULL AND
				DELETED.[CustFirmType] IS NOT NULL
			) OR
			(
				INSERTED.[CustFirmType] IS NOT NULL AND
				DELETED.[CustFirmType] IS NULL
			) OR
			(
				INSERTED.[CustFirmType] !=
				DELETED.[CustFirmType]
			)
		) 
		END		
	
		IF UPDATE([CustPerDiemReimbursed])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustPerDiemReimbursed',
		CONVERT(NVARCHAR(2000),DELETED.[CustPerDiemReimbursed],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustPerDiemReimbursed],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustPerDiemReimbursed] IS NULL AND
				DELETED.[CustPerDiemReimbursed] IS NOT NULL
			) OR
			(
				INSERTED.[CustPerDiemReimbursed] IS NOT NULL AND
				DELETED.[CustPerDiemReimbursed] IS NULL
			) OR
			(
				INSERTED.[CustPerDiemReimbursed] !=
				DELETED.[CustPerDiemReimbursed]
			)
		) 
		END		
	
		IF UPDATE([CustLegacySource])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustLegacySource',
		CONVERT(NVARCHAR(2000),DELETED.[CustLegacySource],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustLegacySource],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustLegacySource] IS NULL AND
				DELETED.[CustLegacySource] IS NOT NULL
			) OR
			(
				INSERTED.[CustLegacySource] IS NOT NULL AND
				DELETED.[CustLegacySource] IS NULL
			) OR
			(
				INSERTED.[CustLegacySource] !=
				DELETED.[CustLegacySource]
			)
		) 
		END		
	
		IF UPDATE([CustClientAdvocate])
		BEGIN
		INSERT
		INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustClientAdvocate',
		CONVERT(NVARCHAR(2000),DELETED.[CustClientAdvocate],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustClientAdvocate],121),
		ISNULL(oldDesc.LastName, '') + ISNULL(', ' + oldDesc.FirstName, ''), ISNULL(newDesc.LastName, '') + ISNULL(', ' + newDesc.FirstName, ''), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustClientAdvocate] IS NULL AND
				DELETED.[CustClientAdvocate] IS NOT NULL
			) OR
			(
				INSERTED.[CustClientAdvocate] IS NOT NULL AND
				DELETED.[CustClientAdvocate] IS NULL
			) OR
			(
				INSERTED.[CustClientAdvocate] !=
				DELETED.[CustClientAdvocate]
			)
		) LEFT JOIN EM AS oldDesc ON DELETED.CustClientAdvocate = oldDesc.Employee LEFT JOIN EM AS newDesc ON INSERTED.CustClientAdvocate = newDesc.Employee
		END
	
		IF UPDATE([CustLegacyIdVE])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustLegacyIdVE',
		CONVERT(NVARCHAR(2000),DELETED.[CustLegacyIdVE],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustLegacyIdVE],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustLegacyIdVE] IS NULL AND
				DELETED.[CustLegacyIdVE] IS NOT NULL
			) OR
			(
				INSERTED.[CustLegacyIdVE] IS NOT NULL AND
				DELETED.[CustLegacyIdVE] IS NULL
			) OR
			(
				INSERTED.[CustLegacyIdVE] !=
				DELETED.[CustLegacyIdVE]
			)
		) 
		END		
	
		IF UPDATE([CustTaxExempt])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustTaxExempt',
		CONVERT(NVARCHAR(2000),DELETED.[CustTaxExempt],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustTaxExempt],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustTaxExempt] IS NULL AND
				DELETED.[CustTaxExempt] IS NOT NULL
			) OR
			(
				INSERTED.[CustTaxExempt] IS NOT NULL AND
				DELETED.[CustTaxExempt] IS NULL
			) OR
			(
				INSERTED.[CustTaxExempt] !=
				DELETED.[CustTaxExempt]
			)
		) 
		END		
	
		IF UPDATE([CustACHCheck])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustACHCheck',
		CONVERT(NVARCHAR(2000),DELETED.[CustACHCheck],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustACHCheck],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustACHCheck] IS NULL AND
				DELETED.[CustACHCheck] IS NOT NULL
			) OR
			(
				INSERTED.[CustACHCheck] IS NOT NULL AND
				DELETED.[CustACHCheck] IS NULL
			) OR
			(
				INSERTED.[CustACHCheck] !=
				DELETED.[CustACHCheck]
			)
		) 
		END		
	
		IF UPDATE([CustBSTVendorName])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustBSTVendorName',
		CONVERT(NVARCHAR(2000),DELETED.[CustBSTVendorName],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustBSTVendorName],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustBSTVendorName] IS NULL AND
				DELETED.[CustBSTVendorName] IS NOT NULL
			) OR
			(
				INSERTED.[CustBSTVendorName] IS NOT NULL AND
				DELETED.[CustBSTVendorName] IS NULL
			) OR
			(
				INSERTED.[CustBSTVendorName] !=
				DELETED.[CustBSTVendorName]
			)
		) 
		END		
	
		IF UPDATE([CustBSTVendorAlphaName])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustBSTVendorAlphaName',
		CONVERT(NVARCHAR(2000),DELETED.[CustBSTVendorAlphaName],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustBSTVendorAlphaName],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustBSTVendorAlphaName] IS NULL AND
				DELETED.[CustBSTVendorAlphaName] IS NOT NULL
			) OR
			(
				INSERTED.[CustBSTVendorAlphaName] IS NOT NULL AND
				DELETED.[CustBSTVendorAlphaName] IS NULL
			) OR
			(
				INSERTED.[CustBSTVendorAlphaName] !=
				DELETED.[CustBSTVendorAlphaName]
			)
		) 
		END		
	
		IF UPDATE([CustBSTVendorType])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustBSTVendorType',
		CONVERT(NVARCHAR(2000),DELETED.[CustBSTVendorType],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustBSTVendorType],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustBSTVendorType] IS NULL AND
				DELETED.[CustBSTVendorType] IS NOT NULL
			) OR
			(
				INSERTED.[CustBSTVendorType] IS NOT NULL AND
				DELETED.[CustBSTVendorType] IS NULL
			) OR
			(
				INSERTED.[CustBSTVendorType] !=
				DELETED.[CustBSTVendorType]
			)
		) 
		END		
	
		IF UPDATE([CustBST1099Type])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustBST1099Type',
		CONVERT(NVARCHAR(2000),DELETED.[CustBST1099Type],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustBST1099Type],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustBST1099Type] IS NULL AND
				DELETED.[CustBST1099Type] IS NOT NULL
			) OR
			(
				INSERTED.[CustBST1099Type] IS NOT NULL AND
				DELETED.[CustBST1099Type] IS NULL
			) OR
			(
				INSERTED.[CustBST1099Type] !=
				DELETED.[CustBST1099Type]
			)
		) 
		END		
	
		IF UPDATE([CustConsolidatedVendor])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustConsolidatedVendor',
		CONVERT(NVARCHAR(2000),DELETED.[CustConsolidatedVendor],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustConsolidatedVendor],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustConsolidatedVendor] IS NULL AND
				DELETED.[CustConsolidatedVendor] IS NOT NULL
			) OR
			(
				INSERTED.[CustConsolidatedVendor] IS NOT NULL AND
				DELETED.[CustConsolidatedVendor] IS NULL
			) OR
			(
				INSERTED.[CustConsolidatedVendor] !=
				DELETED.[CustConsolidatedVendor]
			)
		) 
		END		
	
		IF UPDATE([CustConsolidatedVendorName])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustConsolidatedVendorName',
		CONVERT(NVARCHAR(2000),DELETED.[CustConsolidatedVendorName],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustConsolidatedVendorName],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustConsolidatedVendorName] IS NULL AND
				DELETED.[CustConsolidatedVendorName] IS NOT NULL
			) OR
			(
				INSERTED.[CustConsolidatedVendorName] IS NOT NULL AND
				DELETED.[CustConsolidatedVendorName] IS NULL
			) OR
			(
				INSERTED.[CustConsolidatedVendorName] !=
				DELETED.[CustConsolidatedVendorName]
			)
		) 
		END		
	
		IF UPDATE([CustLegacySourceVE])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustLegacySourceVE',
		CONVERT(NVARCHAR(2000),DELETED.[CustLegacySourceVE],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustLegacySourceVE],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustLegacySourceVE] IS NULL AND
				DELETED.[CustLegacySourceVE] IS NOT NULL
			) OR
			(
				INSERTED.[CustLegacySourceVE] IS NOT NULL AND
				DELETED.[CustLegacySourceVE] IS NULL
			) OR
			(
				INSERTED.[CustLegacySourceVE] !=
				DELETED.[CustLegacySourceVE]
			)
		) 
		END		
	
		IF UPDATE([CustVirtualCardEnrolled])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustVirtualCardEnrolled',
		CONVERT(NVARCHAR(2000),DELETED.[CustVirtualCardEnrolled],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustVirtualCardEnrolled],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustVirtualCardEnrolled] IS NULL AND
				DELETED.[CustVirtualCardEnrolled] IS NOT NULL
			) OR
			(
				INSERTED.[CustVirtualCardEnrolled] IS NOT NULL AND
				DELETED.[CustVirtualCardEnrolled] IS NULL
			) OR
			(
				INSERTED.[CustVirtualCardEnrolled] !=
				DELETED.[CustVirtualCardEnrolled]
			)
		) 
		END		
	
		IF UPDATE([CustPayablesFolder])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustPayablesFolder',
		CONVERT(NVARCHAR(2000),DELETED.[CustPayablesFolder],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustPayablesFolder],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustPayablesFolder] IS NULL AND
				DELETED.[CustPayablesFolder] IS NOT NULL
			) OR
			(
				INSERTED.[CustPayablesFolder] IS NOT NULL AND
				DELETED.[CustPayablesFolder] IS NULL
			) OR
			(
				INSERTED.[CustPayablesFolder] !=
				DELETED.[CustPayablesFolder]
			)
		) 
		END		
	
		IF UPDATE([CustSecure])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustSecure',
		CONVERT(NVARCHAR(2000),DELETED.[CustSecure],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustSecure],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustSecure] IS NULL AND
				DELETED.[CustSecure] IS NOT NULL
			) OR
			(
				INSERTED.[CustSecure] IS NOT NULL AND
				DELETED.[CustSecure] IS NULL
			) OR
			(
				INSERTED.[CustSecure] !=
				DELETED.[CustSecure]
			)
		) 
		END		
	
		IF UPDATE([CustPATApprover1])
		BEGIN
		INSERT
		INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustPATApprover1',
		CONVERT(NVARCHAR(2000),DELETED.[CustPATApprover1],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustPATApprover1],121),
		ISNULL(oldDesc.LastName, '') + ISNULL(', ' + oldDesc.FirstName, ''), ISNULL(newDesc.LastName, '') + ISNULL(', ' + newDesc.FirstName, ''), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustPATApprover1] IS NULL AND
				DELETED.[CustPATApprover1] IS NOT NULL
			) OR
			(
				INSERTED.[CustPATApprover1] IS NOT NULL AND
				DELETED.[CustPATApprover1] IS NULL
			) OR
			(
				INSERTED.[CustPATApprover1] !=
				DELETED.[CustPATApprover1]
			)
		) LEFT JOIN EM AS oldDesc ON DELETED.CustPATApprover1 = oldDesc.Employee LEFT JOIN EM AS newDesc ON INSERTED.CustPATApprover1 = newDesc.Employee
		END
	
		IF UPDATE([CustPATThreshold1])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustPATThreshold1',
		CONVERT(NVARCHAR(2000),DELETED.[CustPATThreshold1],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustPATThreshold1],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustPATThreshold1] IS NULL AND
				DELETED.[CustPATThreshold1] IS NOT NULL
			) OR
			(
				INSERTED.[CustPATThreshold1] IS NOT NULL AND
				DELETED.[CustPATThreshold1] IS NULL
			) OR
			(
				INSERTED.[CustPATThreshold1] !=
				DELETED.[CustPATThreshold1]
			)
		) 
		END		
	
		IF UPDATE([CustPATApprover2])
		BEGIN
		INSERT
		INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustPATApprover2',
		CONVERT(NVARCHAR(2000),DELETED.[CustPATApprover2],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustPATApprover2],121),
		ISNULL(oldDesc.LastName, '') + ISNULL(', ' + oldDesc.FirstName, ''), ISNULL(newDesc.LastName, '') + ISNULL(', ' + newDesc.FirstName, ''), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustPATApprover2] IS NULL AND
				DELETED.[CustPATApprover2] IS NOT NULL
			) OR
			(
				INSERTED.[CustPATApprover2] IS NOT NULL AND
				DELETED.[CustPATApprover2] IS NULL
			) OR
			(
				INSERTED.[CustPATApprover2] !=
				DELETED.[CustPATApprover2]
			)
		) LEFT JOIN EM AS oldDesc ON DELETED.CustPATApprover2 = oldDesc.Employee LEFT JOIN EM AS newDesc ON INSERTED.CustPATApprover2 = newDesc.Employee
		END
	
		IF UPDATE([CustPATThreshold2])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustPATThreshold2',
		CONVERT(NVARCHAR(2000),DELETED.[CustPATThreshold2],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustPATThreshold2],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustPATThreshold2] IS NULL AND
				DELETED.[CustPATThreshold2] IS NOT NULL
			) OR
			(
				INSERTED.[CustPATThreshold2] IS NOT NULL AND
				DELETED.[CustPATThreshold2] IS NULL
			) OR
			(
				INSERTED.[CustPATThreshold2] !=
				DELETED.[CustPATThreshold2]
			)
		) 
		END		
	
		IF UPDATE([CustPATApprover3])
		BEGIN
		INSERT
		INTO AuditTrail (
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		OldValueDescription,
		NewValueDescription,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustPATApprover3',
		CONVERT(NVARCHAR(2000),DELETED.[CustPATApprover3],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustPATApprover3],121),
		ISNULL(oldDesc.LastName, '') + ISNULL(', ' + oldDesc.FirstName, ''), ISNULL(newDesc.LastName, '') + ISNULL(', ' + newDesc.FirstName, ''), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustPATApprover3] IS NULL AND
				DELETED.[CustPATApprover3] IS NOT NULL
			) OR
			(
				INSERTED.[CustPATApprover3] IS NOT NULL AND
				DELETED.[CustPATApprover3] IS NULL
			) OR
			(
				INSERTED.[CustPATApprover3] !=
				DELETED.[CustPATApprover3]
			)
		) LEFT JOIN EM AS oldDesc ON DELETED.CustPATApprover3 = oldDesc.Employee LEFT JOIN EM AS newDesc ON INSERTED.CustPATApprover3 = newDesc.Employee
		END
	
		IF UPDATE([CustPATRouteType])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustPATRouteType',
		CONVERT(NVARCHAR(2000),DELETED.[CustPATRouteType],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustPATRouteType],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustPATRouteType] IS NULL AND
				DELETED.[CustPATRouteType] IS NOT NULL
			) OR
			(
				INSERTED.[CustPATRouteType] IS NOT NULL AND
				DELETED.[CustPATRouteType] IS NULL
			) OR
			(
				INSERTED.[CustPATRouteType] !=
				DELETED.[CustPATRouteType]
			)
		) 
		END		
	
		IF UPDATE([CustLogVendorInfoVerified])
		BEGIN
		INSERT
		INTO AuditTrail	(
		ModUser,
		ModDate,
		TableName,
		ActionType,
		PrimaryKey,
		ColumnName,
		OldValue,
		NewValue,
		Source,
		Application
		)
		SELECT
		@VisionAuditUser, @now, @table, 'UPDATE', CONVERT(NVARCHAR(255),INSERTED.[ClientID],121), 'CustLogVendorInfoVerified',
		CONVERT(NVARCHAR(2000),DELETED.[CustLogVendorInfoVerified],121),
		CONVERT(NVARCHAR(2000),INSERTED.[CustLogVendorInfoVerified],121), @source, @app
		FROM INSERTED JOIN DELETED ON
	INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustLogVendorInfoVerified] IS NULL AND
				DELETED.[CustLogVendorInfoVerified] IS NOT NULL
			) OR
			(
				INSERTED.[CustLogVendorInfoVerified] IS NOT NULL AND
				DELETED.[CustLogVendorInfoVerified] IS NULL
			) OR
			(
				INSERTED.[CustLogVendorInfoVerified] !=
				DELETED.[CustLogVendorInfoVerified]
			)
		) 
		END		
	
		SET NOCOUNT OFF 
		END
	
GO
ALTER TABLE [dbo].[ClientCustomTabFields] ADD CONSTRAINT [ClientCustomTabFieldsPK] PRIMARY KEY NONCLUSTERED ([ClientID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
