CREATE TABLE [dbo].[CFGUserLabels]
(
[PageName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DivID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LabelID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserDefinedLabel] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SetFromCtl] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGUserLa__SetFr__469F9D55] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGUserLabels] ADD CONSTRAINT [CFGUserLabelsPK] PRIMARY KEY NONCLUSTERED ([PageName], [DivID], [LabelID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
