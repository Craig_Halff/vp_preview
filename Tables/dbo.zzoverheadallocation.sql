CREATE TABLE [dbo].[zzoverheadallocation]
(
[Company] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL,
[Period] [int] NOT NULL,
[Pass] [smallint] NOT NULL,
[OrgToDistribute] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DistributionTarget] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Org] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Base] [decimal] (19, 4) NOT NULL,
[Expenses] [decimal] (19, 4) NOT NULL,
[Revenue] [decimal] (19, 4) NOT NULL,
[Received] [decimal] (19, 4) NOT NULL,
[Rate] [decimal] (19, 4) NOT NULL,
[Overhead] [decimal] (19, 4) NOT NULL,
[Variance] [decimal] (19, 4) NOT NULL,
[Method] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Basis] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
