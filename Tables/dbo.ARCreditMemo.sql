CREATE TABLE [dbo].[ARCreditMemo]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreditMemoRefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__ARCreditM__Perio__49BB35CB] DEFAULT ((0)),
[CreditMemoDate] [datetime] NULL,
[OriginalInvoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ARCreditMemo] ADD CONSTRAINT [ARCreditMemoPK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [CreditMemoRefNo]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
