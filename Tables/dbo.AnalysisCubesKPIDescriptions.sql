CREATE TABLE [dbo].[AnalysisCubesKPIDescriptions]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[KPILabel] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AnalysisCubesKPIDescriptions] ADD CONSTRAINT [AnalysisCubesKPIDescriptionsPK] PRIMARY KEY NONCLUSTERED ([PKey], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
