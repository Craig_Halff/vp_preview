CREATE TABLE [dbo].[CustomProposalImages]
(
[ElementID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomPropID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Image] [varbinary] (max) NULL,
[TempImage] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CustomPro__TempI__4D840713] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomProposalImages] ADD CONSTRAINT [CustomProposalImagesPK] PRIMARY KEY CLUSTERED ([ElementID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
