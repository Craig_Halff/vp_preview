CREATE TABLE [dbo].[VisionImportAPPayment_APP]
(
[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Voucher] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VoucherLine] [int] NULL,
[CheckNumber] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckDate] [datetime] NULL,
[PaymentAmount] [decimal] (19, 4) NULL
) ON [PRIMARY]
GO
