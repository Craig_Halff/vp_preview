CREATE TABLE [dbo].[billINMaster]
(
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransDate] [datetime] NULL,
[TransComment] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyExchangeOverrideMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__billINMas__Curre__27F02DAC] DEFAULT ('N'),
[CurrencyExchangeOverrideDate] [datetime] NULL,
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__billINMas__Curre__28E451E5] DEFAULT ((0)),
[DueDate] [datetime] NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[billINMaster] ADD CONSTRAINT [billINMasterPK] PRIMARY KEY NONCLUSTERED ([MainWBS1], [Invoice], [WBS1], [WBS2], [WBS3]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
