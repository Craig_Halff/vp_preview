CREATE TABLE [dbo].[EMCompany]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[JobCostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__JobCo__1122A244] DEFAULT ((0)),
[JobCostType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JCOvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__JCOvt__1216C67D] DEFAULT ((0)),
[HoursPerDay] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__Hours__130AEAB6] DEFAULT ((0)),
[HireDate] [datetime] NULL,
[RaiseDate] [datetime] NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Region] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingCategory] [smallint] NOT NULL CONSTRAINT [DF__EMCompany__Billi__13FF0EEF] DEFAULT ((0)),
[TKGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EKGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__PayRa__14F33328] DEFAULT ((0)),
[PayType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayOvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__PayOv__15E75761] DEFAULT ((0)),
[PaySpecialOvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__PaySp__16DB7B9A] DEFAULT ((0)),
[ADPFileNumber] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADPCompanyCode] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADPRateCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProvCostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__ProvC__17CF9FD3] DEFAULT ((0)),
[ProvBillRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__ProvB__18C3C40C] DEFAULT ((0)),
[ProvCostOTPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__ProvC__19B7E845] DEFAULT ((0)),
[ProvBillOTPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__ProvB__1AAC0C7E] DEFAULT ((0)),
[DefaultLC1] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultLC2] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultLC3] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultLC4] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultLC5] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChangeDefaultLC] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__Chang__1BA030B7] DEFAULT ('N'),
[TerminationDate] [datetime] NULL,
[UseTotalHrsAsStd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__UseTo__1C9454F0] DEFAULT ('N'),
[JCSpecialOvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__JCSpe__1D887929] DEFAULT ((0)),
[ProvCostSpecialOTPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__ProvC__1E7C9D62] DEFAULT ((0)),
[ProvBillSpecialOTPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__ProvB__1F70C19B] DEFAULT ((0)),
[YearsOtherFirms] [smallint] NOT NULL CONSTRAINT [DF__EMCompany__Years__2064E5D4] DEFAULT ((0)),
[Supervisor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReadyForProcessing] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__Ready__21590A0D] DEFAULT ('N'),
[CheckHours] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Locale] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocaleMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__Local__224D2E46] DEFAULT ('0'),
[OtherPay] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__Other__2341527F] DEFAULT ((0)),
[OtherPay2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__Other__243576B8] DEFAULT ((0)),
[OtherPay3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__Other__25299AF1] DEFAULT ((0)),
[OtherPay4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__Other__261DBF2A] DEFAULT ((0)),
[OtherPay5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__Other__2711E363] DEFAULT ((0)),
[CostRateMeth] [smallint] NOT NULL CONSTRAINT [DF__EMCompany__CostR__2806079C] DEFAULT ((0)),
[CostRateTableNo] [int] NOT NULL CONSTRAINT [DF__EMCompany__CostR__28FA2BD5] DEFAULT ((0)),
[PayRateMeth] [smallint] NOT NULL CONSTRAINT [DF__EMCompany__PayRa__29EE500E] DEFAULT ((0)),
[PayRateTableNo] [int] NOT NULL CONSTRAINT [DF__EMCompany__PayRa__2AE27447] DEFAULT ((0)),
[PriorYearsFirm] [smallint] NOT NULL CONSTRAINT [DF__EMCompany__Prior__2BD69880] DEFAULT ((0)),
[PaychexCode1] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaychexCode2] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaychexCode3] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncludeLocalJurisOnly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__Inclu__2CCABCB9] DEFAULT ('N'),
[AllowChargeUnits] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__Allow__2DBEE0F2] DEFAULT ('N'),
[RequireStartEndTime] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__Requi__2EB3052B] DEFAULT ('N'),
[AllowBreakTime] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__Allow__2FA72964] DEFAULT ('N'),
[DefaultBreakStartDateTime] [datetime] NULL,
[DefaultBreakEndDateTime] [datetime] NULL,
[PaychexRateNumber] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailPayrollRemittance] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__Email__309B4D9D] DEFAULT ('N'),
[EmailExpenseRemittance] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__Email__318F71D6] DEFAULT ('N'),
[OccupationalCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GeographicCode] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatutoryEmployee] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__Statu__3283960F] DEFAULT ('N'),
[RetirementPlan] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__Retir__3377BA48] DEFAULT ('N'),
[ThirdPartySickPay] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__Third__346BDE81] DEFAULT ('N'),
[ClieOpTransactionType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClieOp] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__ClieO__356002BA] DEFAULT ('N'),
[ClieOpAccount] [nvarchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClieOpAccountType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SEPAIBAN] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SEPABIC] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxRegistrationNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisableTSRevAudit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__Disab__365426F3] DEFAULT ('N'),
[Terminated] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__Termi__37484B2C] DEFAULT ('N'),
[FormW4Version] [smallint] NOT NULL CONSTRAINT [DF__EMCompany__FormW__383C6F65] DEFAULT ((2019)),
[FormW4Step2] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMCompany__FormW__3930939E] DEFAULT ('N'),
[FormW4Dependents] [smallint] NOT NULL CONSTRAINT [DF__EMCompany__FormW__3A24B7D7] DEFAULT ((0)),
[FormW4DependentsOther] [smallint] NOT NULL CONSTRAINT [DF__EMCompany__FormW__3B18DC10] DEFAULT ((0)),
[FormW4OtherTaxCredit] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__FormW__3C0D0049] DEFAULT ((0)),
[FormW4OtherIncome] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__FormW__3D012482] DEFAULT ((0)),
[FormW4Deductions] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMCompany__FormW__3DF548BB] DEFAULT ((0)),
[UIPaymentMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[DefaultTaxLocation] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_EMCompany]
      ON [dbo].[EMCompany]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMCompany'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'Employee',CONVERT(NVARCHAR(2000),[Employee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'EmployeeCompany',CONVERT(NVARCHAR(2000),[EmployeeCompany],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'JobCostRate',CONVERT(NVARCHAR(2000),[JobCostRate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'JobCostType',CONVERT(NVARCHAR(2000),[JobCostType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'JCOvtPct',CONVERT(NVARCHAR(2000),[JCOvtPct],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'HoursPerDay',CONVERT(NVARCHAR(2000),[HoursPerDay],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'HireDate',CONVERT(NVARCHAR(2000),[HireDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'RaiseDate',CONVERT(NVARCHAR(2000),[RaiseDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'Status',CONVERT(NVARCHAR(2000),[Status],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'Type',CONVERT(NVARCHAR(2000),[Type],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'Org',CONVERT(NVARCHAR(2000),[Org],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'Region',CONVERT(NVARCHAR(2000),[Region],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'BillingCategory',CONVERT(NVARCHAR(2000),[BillingCategory],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'TKGroup',CONVERT(NVARCHAR(2000),[TKGroup],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'EKGroup',CONVERT(NVARCHAR(2000),[EKGroup],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'PayRate',CONVERT(NVARCHAR(2000),[PayRate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'PayType',CONVERT(NVARCHAR(2000),[PayType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'PayOvtPct',CONVERT(NVARCHAR(2000),[PayOvtPct],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'PaySpecialOvtPct',CONVERT(NVARCHAR(2000),[PaySpecialOvtPct],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'ADPFileNumber',CONVERT(NVARCHAR(2000),[ADPFileNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'ADPCompanyCode',CONVERT(NVARCHAR(2000),[ADPCompanyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'ADPRateCode',CONVERT(NVARCHAR(2000),[ADPRateCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'ProvCostRate',CONVERT(NVARCHAR(2000),[ProvCostRate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'ProvBillRate',CONVERT(NVARCHAR(2000),[ProvBillRate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'ProvCostOTPct',CONVERT(NVARCHAR(2000),[ProvCostOTPct],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'ProvBillOTPct',CONVERT(NVARCHAR(2000),[ProvBillOTPct],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'DefaultLC1',CONVERT(NVARCHAR(2000),[DefaultLC1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'DefaultLC2',CONVERT(NVARCHAR(2000),[DefaultLC2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'DefaultLC3',CONVERT(NVARCHAR(2000),[DefaultLC3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'DefaultLC4',CONVERT(NVARCHAR(2000),[DefaultLC4],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'DefaultLC5',CONVERT(NVARCHAR(2000),[DefaultLC5],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'ChangeDefaultLC',CONVERT(NVARCHAR(2000),[ChangeDefaultLC],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'TerminationDate',CONVERT(NVARCHAR(2000),[TerminationDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'UseTotalHrsAsStd',CONVERT(NVARCHAR(2000),[UseTotalHrsAsStd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'JCSpecialOvtPct',CONVERT(NVARCHAR(2000),[JCSpecialOvtPct],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'ProvCostSpecialOTPct',CONVERT(NVARCHAR(2000),[ProvCostSpecialOTPct],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'ProvBillSpecialOTPct',CONVERT(NVARCHAR(2000),[ProvBillSpecialOTPct],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'YearsOtherFirms',CONVERT(NVARCHAR(2000),[YearsOtherFirms],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'Supervisor',CONVERT(NVARCHAR(2000),[Supervisor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'ReadyForProcessing',CONVERT(NVARCHAR(2000),[ReadyForProcessing],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'CheckHours',CONVERT(NVARCHAR(2000),[CheckHours],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'Locale',CONVERT(NVARCHAR(2000),[Locale],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'LocaleMethod',CONVERT(NVARCHAR(2000),[LocaleMethod],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'OtherPay',CONVERT(NVARCHAR(2000),[OtherPay],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'OtherPay2',CONVERT(NVARCHAR(2000),[OtherPay2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'OtherPay3',CONVERT(NVARCHAR(2000),[OtherPay3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'OtherPay4',CONVERT(NVARCHAR(2000),[OtherPay4],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'OtherPay5',CONVERT(NVARCHAR(2000),[OtherPay5],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'CostRateMeth',CONVERT(NVARCHAR(2000),[CostRateMeth],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'CostRateTableNo',CONVERT(NVARCHAR(2000),[CostRateTableNo],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'PayRateMeth',CONVERT(NVARCHAR(2000),[PayRateMeth],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'PayRateTableNo',CONVERT(NVARCHAR(2000),[PayRateTableNo],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'PriorYearsFirm',CONVERT(NVARCHAR(2000),[PriorYearsFirm],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'PaychexCode1',CONVERT(NVARCHAR(2000),[PaychexCode1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'PaychexCode2',CONVERT(NVARCHAR(2000),[PaychexCode2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'PaychexCode3',CONVERT(NVARCHAR(2000),[PaychexCode3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'IncludeLocalJurisOnly',CONVERT(NVARCHAR(2000),[IncludeLocalJurisOnly],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'AllowChargeUnits',CONVERT(NVARCHAR(2000),[AllowChargeUnits],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'RequireStartEndTime',CONVERT(NVARCHAR(2000),[RequireStartEndTime],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'AllowBreakTime',CONVERT(NVARCHAR(2000),[AllowBreakTime],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'DefaultBreakStartDateTime',CONVERT(NVARCHAR(2000),[DefaultBreakStartDateTime],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'DefaultBreakEndDateTime',CONVERT(NVARCHAR(2000),[DefaultBreakEndDateTime],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'PaychexRateNumber',CONVERT(NVARCHAR(2000),[PaychexRateNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'EmailPayrollRemittance',CONVERT(NVARCHAR(2000),[EmailPayrollRemittance],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'EmailExpenseRemittance',CONVERT(NVARCHAR(2000),[EmailExpenseRemittance],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'OccupationalCode',CONVERT(NVARCHAR(2000),[OccupationalCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'GeographicCode',CONVERT(NVARCHAR(2000),[GeographicCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'StatutoryEmployee',CONVERT(NVARCHAR(2000),[StatutoryEmployee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'RetirementPlan',CONVERT(NVARCHAR(2000),[RetirementPlan],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'ThirdPartySickPay',CONVERT(NVARCHAR(2000),[ThirdPartySickPay],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'ClieOpTransactionType',CONVERT(NVARCHAR(2000),[ClieOpTransactionType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'ClieOp',CONVERT(NVARCHAR(2000),[ClieOp],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'ClieOpAccount',CONVERT(NVARCHAR(2000),[ClieOpAccount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'ClieOpAccountType',CONVERT(NVARCHAR(2000),[ClieOpAccountType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'SEPAIBAN',CONVERT(NVARCHAR(2000),[SEPAIBAN],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'SEPABIC',CONVERT(NVARCHAR(2000),[SEPABIC],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'TaxRegistrationNumber',CONVERT(NVARCHAR(2000),[TaxRegistrationNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'DisableTSRevAudit',CONVERT(NVARCHAR(2000),[DisableTSRevAudit],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'UIPaymentMethod',CONVERT(NVARCHAR(2000),[UIPaymentMethod],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'DefaultTaxLocation',CONVERT(NVARCHAR(2000),[DefaultTaxLocation],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'Terminated',CONVERT(NVARCHAR(2000),[Terminated],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'FormW4Version',CONVERT(NVARCHAR(2000),[FormW4Version],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'FormW4Step2',CONVERT(NVARCHAR(2000),[FormW4Step2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'FormW4Dependents',CONVERT(NVARCHAR(2000),[FormW4Dependents],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'FormW4DependentsOther',CONVERT(NVARCHAR(2000),[FormW4DependentsOther],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'FormW4OtherTaxCredit',CONVERT(NVARCHAR(2000),[FormW4OtherTaxCredit],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'FormW4OtherIncome',CONVERT(NVARCHAR(2000),[FormW4OtherIncome],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[EmployeeCompany],121),'FormW4Deductions',CONVERT(NVARCHAR(2000),[FormW4Deductions],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_EMCompany] ON [dbo].[EMCompany]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_EMCompany]
      ON [dbo].[EMCompany]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMCompany'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'Employee',NULL,CONVERT(NVARCHAR(2000),[Employee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'EmployeeCompany',NULL,CONVERT(NVARCHAR(2000),[EmployeeCompany],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'JobCostRate',NULL,CONVERT(NVARCHAR(2000),[JobCostRate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'JobCostType',NULL,CONVERT(NVARCHAR(2000),[JobCostType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'JCOvtPct',NULL,CONVERT(NVARCHAR(2000),[JCOvtPct],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'HoursPerDay',NULL,CONVERT(NVARCHAR(2000),[HoursPerDay],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'HireDate',NULL,CONVERT(NVARCHAR(2000),[HireDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'RaiseDate',NULL,CONVERT(NVARCHAR(2000),[RaiseDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'Status',NULL,CONVERT(NVARCHAR(2000),[Status],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'Type',NULL,CONVERT(NVARCHAR(2000),[Type],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'Org',NULL,CONVERT(NVARCHAR(2000),[Org],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'Region',NULL,CONVERT(NVARCHAR(2000),[Region],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'BillingCategory',NULL,CONVERT(NVARCHAR(2000),[BillingCategory],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'TKGroup',NULL,CONVERT(NVARCHAR(2000),[TKGroup],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'EKGroup',NULL,CONVERT(NVARCHAR(2000),[EKGroup],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PayRate',NULL,CONVERT(NVARCHAR(2000),[PayRate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PayType',NULL,CONVERT(NVARCHAR(2000),[PayType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PayOvtPct',NULL,CONVERT(NVARCHAR(2000),[PayOvtPct],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PaySpecialOvtPct',NULL,CONVERT(NVARCHAR(2000),[PaySpecialOvtPct],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ADPFileNumber',NULL,CONVERT(NVARCHAR(2000),[ADPFileNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ADPCompanyCode',NULL,CONVERT(NVARCHAR(2000),[ADPCompanyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ADPRateCode',NULL,CONVERT(NVARCHAR(2000),[ADPRateCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ProvCostRate',NULL,CONVERT(NVARCHAR(2000),[ProvCostRate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ProvBillRate',NULL,CONVERT(NVARCHAR(2000),[ProvBillRate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ProvCostOTPct',NULL,CONVERT(NVARCHAR(2000),[ProvCostOTPct],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ProvBillOTPct',NULL,CONVERT(NVARCHAR(2000),[ProvBillOTPct],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DefaultLC1',NULL,CONVERT(NVARCHAR(2000),[DefaultLC1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DefaultLC2',NULL,CONVERT(NVARCHAR(2000),[DefaultLC2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DefaultLC3',NULL,CONVERT(NVARCHAR(2000),[DefaultLC3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DefaultLC4',NULL,CONVERT(NVARCHAR(2000),[DefaultLC4],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DefaultLC5',NULL,CONVERT(NVARCHAR(2000),[DefaultLC5],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ChangeDefaultLC',NULL,CONVERT(NVARCHAR(2000),[ChangeDefaultLC],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'TerminationDate',NULL,CONVERT(NVARCHAR(2000),[TerminationDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'UseTotalHrsAsStd',NULL,CONVERT(NVARCHAR(2000),[UseTotalHrsAsStd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'JCSpecialOvtPct',NULL,CONVERT(NVARCHAR(2000),[JCSpecialOvtPct],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ProvCostSpecialOTPct',NULL,CONVERT(NVARCHAR(2000),[ProvCostSpecialOTPct],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ProvBillSpecialOTPct',NULL,CONVERT(NVARCHAR(2000),[ProvBillSpecialOTPct],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'YearsOtherFirms',NULL,CONVERT(NVARCHAR(2000),[YearsOtherFirms],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'Supervisor',NULL,CONVERT(NVARCHAR(2000),[Supervisor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ReadyForProcessing',NULL,CONVERT(NVARCHAR(2000),[ReadyForProcessing],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'CheckHours',NULL,CONVERT(NVARCHAR(2000),[CheckHours],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'Locale',NULL,CONVERT(NVARCHAR(2000),[Locale],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'LocaleMethod',NULL,CONVERT(NVARCHAR(2000),[LocaleMethod],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'OtherPay',NULL,CONVERT(NVARCHAR(2000),[OtherPay],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'OtherPay2',NULL,CONVERT(NVARCHAR(2000),[OtherPay2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'OtherPay3',NULL,CONVERT(NVARCHAR(2000),[OtherPay3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'OtherPay4',NULL,CONVERT(NVARCHAR(2000),[OtherPay4],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'OtherPay5',NULL,CONVERT(NVARCHAR(2000),[OtherPay5],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'CostRateMeth',NULL,CONVERT(NVARCHAR(2000),[CostRateMeth],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'CostRateTableNo',NULL,CONVERT(NVARCHAR(2000),[CostRateTableNo],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PayRateMeth',NULL,CONVERT(NVARCHAR(2000),[PayRateMeth],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PayRateTableNo',NULL,CONVERT(NVARCHAR(2000),[PayRateTableNo],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PriorYearsFirm',NULL,CONVERT(NVARCHAR(2000),[PriorYearsFirm],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PaychexCode1',NULL,CONVERT(NVARCHAR(2000),[PaychexCode1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PaychexCode2',NULL,CONVERT(NVARCHAR(2000),[PaychexCode2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PaychexCode3',NULL,CONVERT(NVARCHAR(2000),[PaychexCode3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'IncludeLocalJurisOnly',NULL,CONVERT(NVARCHAR(2000),[IncludeLocalJurisOnly],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'AllowChargeUnits',NULL,CONVERT(NVARCHAR(2000),[AllowChargeUnits],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'RequireStartEndTime',NULL,CONVERT(NVARCHAR(2000),[RequireStartEndTime],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'AllowBreakTime',NULL,CONVERT(NVARCHAR(2000),[AllowBreakTime],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DefaultBreakStartDateTime',NULL,CONVERT(NVARCHAR(2000),[DefaultBreakStartDateTime],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DefaultBreakEndDateTime',NULL,CONVERT(NVARCHAR(2000),[DefaultBreakEndDateTime],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PaychexRateNumber',NULL,CONVERT(NVARCHAR(2000),[PaychexRateNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'EmailPayrollRemittance',NULL,CONVERT(NVARCHAR(2000),[EmailPayrollRemittance],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'EmailExpenseRemittance',NULL,CONVERT(NVARCHAR(2000),[EmailExpenseRemittance],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'OccupationalCode',NULL,CONVERT(NVARCHAR(2000),[OccupationalCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'GeographicCode',NULL,CONVERT(NVARCHAR(2000),[GeographicCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'StatutoryEmployee',NULL,CONVERT(NVARCHAR(2000),[StatutoryEmployee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'RetirementPlan',NULL,CONVERT(NVARCHAR(2000),[RetirementPlan],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ThirdPartySickPay',NULL,CONVERT(NVARCHAR(2000),[ThirdPartySickPay],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ClieOpTransactionType',NULL,CONVERT(NVARCHAR(2000),[ClieOpTransactionType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ClieOp',NULL,CONVERT(NVARCHAR(2000),[ClieOp],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ClieOpAccount',NULL,CONVERT(NVARCHAR(2000),[ClieOpAccount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ClieOpAccountType',NULL,CONVERT(NVARCHAR(2000),[ClieOpAccountType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'SEPAIBAN',NULL,CONVERT(NVARCHAR(2000),[SEPAIBAN],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'SEPABIC',NULL,CONVERT(NVARCHAR(2000),[SEPABIC],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'TaxRegistrationNumber',NULL,CONVERT(NVARCHAR(2000),[TaxRegistrationNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DisableTSRevAudit',NULL,CONVERT(NVARCHAR(2000),[DisableTSRevAudit],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'UIPaymentMethod',NULL,CONVERT(NVARCHAR(2000),[UIPaymentMethod],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DefaultTaxLocation',NULL,CONVERT(NVARCHAR(2000),[DefaultTaxLocation],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'Terminated',NULL,CONVERT(NVARCHAR(2000),[Terminated],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'FormW4Version',NULL,CONVERT(NVARCHAR(2000),[FormW4Version],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'FormW4Step2',NULL,CONVERT(NVARCHAR(2000),[FormW4Step2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'FormW4Dependents',NULL,CONVERT(NVARCHAR(2000),[FormW4Dependents],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'FormW4DependentsOther',NULL,CONVERT(NVARCHAR(2000),[FormW4DependentsOther],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'FormW4OtherTaxCredit',NULL,CONVERT(NVARCHAR(2000),[FormW4OtherTaxCredit],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'FormW4OtherIncome',NULL,CONVERT(NVARCHAR(2000),[FormW4OtherIncome],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'FormW4Deductions',NULL,CONVERT(NVARCHAR(2000),[FormW4Deductions],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_EMCompany] ON [dbo].[EMCompany]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_EMCompany]
      ON [dbo].[EMCompany]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMCompany'
    
      If UPDATE([Employee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'Employee',
      CONVERT(NVARCHAR(2000),DELETED.[Employee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Employee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[Employee] Is Null And
				DELETED.[Employee] Is Not Null
			) Or
			(
				INSERTED.[Employee] Is Not Null And
				DELETED.[Employee] Is Null
			) Or
			(
				INSERTED.[Employee] !=
				DELETED.[Employee]
			)
		) 
		END		
		
      If UPDATE([EmployeeCompany])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'EmployeeCompany',
      CONVERT(NVARCHAR(2000),DELETED.[EmployeeCompany],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EmployeeCompany],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[EmployeeCompany] Is Null And
				DELETED.[EmployeeCompany] Is Not Null
			) Or
			(
				INSERTED.[EmployeeCompany] Is Not Null And
				DELETED.[EmployeeCompany] Is Null
			) Or
			(
				INSERTED.[EmployeeCompany] !=
				DELETED.[EmployeeCompany]
			)
		) 
		END		
		
      If UPDATE([JobCostRate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'JobCostRate',
      CONVERT(NVARCHAR(2000),DELETED.[JobCostRate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[JobCostRate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[JobCostRate] Is Null And
				DELETED.[JobCostRate] Is Not Null
			) Or
			(
				INSERTED.[JobCostRate] Is Not Null And
				DELETED.[JobCostRate] Is Null
			) Or
			(
				INSERTED.[JobCostRate] !=
				DELETED.[JobCostRate]
			)
		) 
		END		
		
      If UPDATE([JobCostType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'JobCostType',
      CONVERT(NVARCHAR(2000),DELETED.[JobCostType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[JobCostType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[JobCostType] Is Null And
				DELETED.[JobCostType] Is Not Null
			) Or
			(
				INSERTED.[JobCostType] Is Not Null And
				DELETED.[JobCostType] Is Null
			) Or
			(
				INSERTED.[JobCostType] !=
				DELETED.[JobCostType]
			)
		) 
		END		
		
      If UPDATE([JCOvtPct])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'JCOvtPct',
      CONVERT(NVARCHAR(2000),DELETED.[JCOvtPct],121),
      CONVERT(NVARCHAR(2000),INSERTED.[JCOvtPct],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[JCOvtPct] Is Null And
				DELETED.[JCOvtPct] Is Not Null
			) Or
			(
				INSERTED.[JCOvtPct] Is Not Null And
				DELETED.[JCOvtPct] Is Null
			) Or
			(
				INSERTED.[JCOvtPct] !=
				DELETED.[JCOvtPct]
			)
		) 
		END		
		
      If UPDATE([HoursPerDay])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'HoursPerDay',
      CONVERT(NVARCHAR(2000),DELETED.[HoursPerDay],121),
      CONVERT(NVARCHAR(2000),INSERTED.[HoursPerDay],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[HoursPerDay] Is Null And
				DELETED.[HoursPerDay] Is Not Null
			) Or
			(
				INSERTED.[HoursPerDay] Is Not Null And
				DELETED.[HoursPerDay] Is Null
			) Or
			(
				INSERTED.[HoursPerDay] !=
				DELETED.[HoursPerDay]
			)
		) 
		END		
		
      If UPDATE([HireDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'HireDate',
      CONVERT(NVARCHAR(2000),DELETED.[HireDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[HireDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[HireDate] Is Null And
				DELETED.[HireDate] Is Not Null
			) Or
			(
				INSERTED.[HireDate] Is Not Null And
				DELETED.[HireDate] Is Null
			) Or
			(
				INSERTED.[HireDate] !=
				DELETED.[HireDate]
			)
		) 
		END		
		
      If UPDATE([RaiseDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'RaiseDate',
      CONVERT(NVARCHAR(2000),DELETED.[RaiseDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RaiseDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[RaiseDate] Is Null And
				DELETED.[RaiseDate] Is Not Null
			) Or
			(
				INSERTED.[RaiseDate] Is Not Null And
				DELETED.[RaiseDate] Is Null
			) Or
			(
				INSERTED.[RaiseDate] !=
				DELETED.[RaiseDate]
			)
		) 
		END		
		
      If UPDATE([Status])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'Status',
      CONVERT(NVARCHAR(2000),DELETED.[Status],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Status],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[Status] Is Null And
				DELETED.[Status] Is Not Null
			) Or
			(
				INSERTED.[Status] Is Not Null And
				DELETED.[Status] Is Null
			) Or
			(
				INSERTED.[Status] !=
				DELETED.[Status]
			)
		) 
		END		
		
      If UPDATE([Type])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'Type',
      CONVERT(NVARCHAR(2000),DELETED.[Type],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Type],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[Type] Is Null And
				DELETED.[Type] Is Not Null
			) Or
			(
				INSERTED.[Type] Is Not Null And
				DELETED.[Type] Is Null
			) Or
			(
				INSERTED.[Type] !=
				DELETED.[Type]
			)
		) 
		END		
		
      If UPDATE([Org])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'Org',
      CONVERT(NVARCHAR(2000),DELETED.[Org],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Org],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[Org] Is Null And
				DELETED.[Org] Is Not Null
			) Or
			(
				INSERTED.[Org] Is Not Null And
				DELETED.[Org] Is Null
			) Or
			(
				INSERTED.[Org] !=
				DELETED.[Org]
			)
		) 
		END		
		
      If UPDATE([Region])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'Region',
      CONVERT(NVARCHAR(2000),DELETED.[Region],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Region],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[Region] Is Null And
				DELETED.[Region] Is Not Null
			) Or
			(
				INSERTED.[Region] Is Not Null And
				DELETED.[Region] Is Null
			) Or
			(
				INSERTED.[Region] !=
				DELETED.[Region]
			)
		) 
		END		
		
      If UPDATE([BillingCategory])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'BillingCategory',
      CONVERT(NVARCHAR(2000),DELETED.[BillingCategory],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BillingCategory],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[BillingCategory] Is Null And
				DELETED.[BillingCategory] Is Not Null
			) Or
			(
				INSERTED.[BillingCategory] Is Not Null And
				DELETED.[BillingCategory] Is Null
			) Or
			(
				INSERTED.[BillingCategory] !=
				DELETED.[BillingCategory]
			)
		) 
		END		
		
      If UPDATE([TKGroup])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'TKGroup',
      CONVERT(NVARCHAR(2000),DELETED.[TKGroup],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TKGroup],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[TKGroup] Is Null And
				DELETED.[TKGroup] Is Not Null
			) Or
			(
				INSERTED.[TKGroup] Is Not Null And
				DELETED.[TKGroup] Is Null
			) Or
			(
				INSERTED.[TKGroup] !=
				DELETED.[TKGroup]
			)
		) 
		END		
		
      If UPDATE([EKGroup])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'EKGroup',
      CONVERT(NVARCHAR(2000),DELETED.[EKGroup],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EKGroup],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[EKGroup] Is Null And
				DELETED.[EKGroup] Is Not Null
			) Or
			(
				INSERTED.[EKGroup] Is Not Null And
				DELETED.[EKGroup] Is Null
			) Or
			(
				INSERTED.[EKGroup] !=
				DELETED.[EKGroup]
			)
		) 
		END		
		
      If UPDATE([PayRate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PayRate',
      CONVERT(NVARCHAR(2000),DELETED.[PayRate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PayRate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[PayRate] Is Null And
				DELETED.[PayRate] Is Not Null
			) Or
			(
				INSERTED.[PayRate] Is Not Null And
				DELETED.[PayRate] Is Null
			) Or
			(
				INSERTED.[PayRate] !=
				DELETED.[PayRate]
			)
		) 
		END		
		
      If UPDATE([PayType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PayType',
      CONVERT(NVARCHAR(2000),DELETED.[PayType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PayType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[PayType] Is Null And
				DELETED.[PayType] Is Not Null
			) Or
			(
				INSERTED.[PayType] Is Not Null And
				DELETED.[PayType] Is Null
			) Or
			(
				INSERTED.[PayType] !=
				DELETED.[PayType]
			)
		) 
		END		
		
      If UPDATE([PayOvtPct])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PayOvtPct',
      CONVERT(NVARCHAR(2000),DELETED.[PayOvtPct],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PayOvtPct],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[PayOvtPct] Is Null And
				DELETED.[PayOvtPct] Is Not Null
			) Or
			(
				INSERTED.[PayOvtPct] Is Not Null And
				DELETED.[PayOvtPct] Is Null
			) Or
			(
				INSERTED.[PayOvtPct] !=
				DELETED.[PayOvtPct]
			)
		) 
		END		
		
      If UPDATE([PaySpecialOvtPct])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PaySpecialOvtPct',
      CONVERT(NVARCHAR(2000),DELETED.[PaySpecialOvtPct],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PaySpecialOvtPct],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[PaySpecialOvtPct] Is Null And
				DELETED.[PaySpecialOvtPct] Is Not Null
			) Or
			(
				INSERTED.[PaySpecialOvtPct] Is Not Null And
				DELETED.[PaySpecialOvtPct] Is Null
			) Or
			(
				INSERTED.[PaySpecialOvtPct] !=
				DELETED.[PaySpecialOvtPct]
			)
		) 
		END		
		
      If UPDATE([ADPFileNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ADPFileNumber',
      CONVERT(NVARCHAR(2000),DELETED.[ADPFileNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ADPFileNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[ADPFileNumber] Is Null And
				DELETED.[ADPFileNumber] Is Not Null
			) Or
			(
				INSERTED.[ADPFileNumber] Is Not Null And
				DELETED.[ADPFileNumber] Is Null
			) Or
			(
				INSERTED.[ADPFileNumber] !=
				DELETED.[ADPFileNumber]
			)
		) 
		END		
		
      If UPDATE([ADPCompanyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ADPCompanyCode',
      CONVERT(NVARCHAR(2000),DELETED.[ADPCompanyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ADPCompanyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[ADPCompanyCode] Is Null And
				DELETED.[ADPCompanyCode] Is Not Null
			) Or
			(
				INSERTED.[ADPCompanyCode] Is Not Null And
				DELETED.[ADPCompanyCode] Is Null
			) Or
			(
				INSERTED.[ADPCompanyCode] !=
				DELETED.[ADPCompanyCode]
			)
		) 
		END		
		
      If UPDATE([ADPRateCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ADPRateCode',
      CONVERT(NVARCHAR(2000),DELETED.[ADPRateCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ADPRateCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[ADPRateCode] Is Null And
				DELETED.[ADPRateCode] Is Not Null
			) Or
			(
				INSERTED.[ADPRateCode] Is Not Null And
				DELETED.[ADPRateCode] Is Null
			) Or
			(
				INSERTED.[ADPRateCode] !=
				DELETED.[ADPRateCode]
			)
		) 
		END		
		
      If UPDATE([ProvCostRate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ProvCostRate',
      CONVERT(NVARCHAR(2000),DELETED.[ProvCostRate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProvCostRate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[ProvCostRate] Is Null And
				DELETED.[ProvCostRate] Is Not Null
			) Or
			(
				INSERTED.[ProvCostRate] Is Not Null And
				DELETED.[ProvCostRate] Is Null
			) Or
			(
				INSERTED.[ProvCostRate] !=
				DELETED.[ProvCostRate]
			)
		) 
		END		
		
      If UPDATE([ProvBillRate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ProvBillRate',
      CONVERT(NVARCHAR(2000),DELETED.[ProvBillRate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProvBillRate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[ProvBillRate] Is Null And
				DELETED.[ProvBillRate] Is Not Null
			) Or
			(
				INSERTED.[ProvBillRate] Is Not Null And
				DELETED.[ProvBillRate] Is Null
			) Or
			(
				INSERTED.[ProvBillRate] !=
				DELETED.[ProvBillRate]
			)
		) 
		END		
		
      If UPDATE([ProvCostOTPct])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ProvCostOTPct',
      CONVERT(NVARCHAR(2000),DELETED.[ProvCostOTPct],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProvCostOTPct],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[ProvCostOTPct] Is Null And
				DELETED.[ProvCostOTPct] Is Not Null
			) Or
			(
				INSERTED.[ProvCostOTPct] Is Not Null And
				DELETED.[ProvCostOTPct] Is Null
			) Or
			(
				INSERTED.[ProvCostOTPct] !=
				DELETED.[ProvCostOTPct]
			)
		) 
		END		
		
      If UPDATE([ProvBillOTPct])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ProvBillOTPct',
      CONVERT(NVARCHAR(2000),DELETED.[ProvBillOTPct],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProvBillOTPct],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[ProvBillOTPct] Is Null And
				DELETED.[ProvBillOTPct] Is Not Null
			) Or
			(
				INSERTED.[ProvBillOTPct] Is Not Null And
				DELETED.[ProvBillOTPct] Is Null
			) Or
			(
				INSERTED.[ProvBillOTPct] !=
				DELETED.[ProvBillOTPct]
			)
		) 
		END		
		
      If UPDATE([DefaultLC1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DefaultLC1',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultLC1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultLC1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[DefaultLC1] Is Null And
				DELETED.[DefaultLC1] Is Not Null
			) Or
			(
				INSERTED.[DefaultLC1] Is Not Null And
				DELETED.[DefaultLC1] Is Null
			) Or
			(
				INSERTED.[DefaultLC1] !=
				DELETED.[DefaultLC1]
			)
		) 
		END		
		
      If UPDATE([DefaultLC2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DefaultLC2',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultLC2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultLC2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[DefaultLC2] Is Null And
				DELETED.[DefaultLC2] Is Not Null
			) Or
			(
				INSERTED.[DefaultLC2] Is Not Null And
				DELETED.[DefaultLC2] Is Null
			) Or
			(
				INSERTED.[DefaultLC2] !=
				DELETED.[DefaultLC2]
			)
		) 
		END		
		
      If UPDATE([DefaultLC3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DefaultLC3',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultLC3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultLC3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[DefaultLC3] Is Null And
				DELETED.[DefaultLC3] Is Not Null
			) Or
			(
				INSERTED.[DefaultLC3] Is Not Null And
				DELETED.[DefaultLC3] Is Null
			) Or
			(
				INSERTED.[DefaultLC3] !=
				DELETED.[DefaultLC3]
			)
		) 
		END		
		
      If UPDATE([DefaultLC4])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DefaultLC4',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultLC4],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultLC4],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[DefaultLC4] Is Null And
				DELETED.[DefaultLC4] Is Not Null
			) Or
			(
				INSERTED.[DefaultLC4] Is Not Null And
				DELETED.[DefaultLC4] Is Null
			) Or
			(
				INSERTED.[DefaultLC4] !=
				DELETED.[DefaultLC4]
			)
		) 
		END		
		
      If UPDATE([DefaultLC5])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DefaultLC5',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultLC5],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultLC5],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[DefaultLC5] Is Null And
				DELETED.[DefaultLC5] Is Not Null
			) Or
			(
				INSERTED.[DefaultLC5] Is Not Null And
				DELETED.[DefaultLC5] Is Null
			) Or
			(
				INSERTED.[DefaultLC5] !=
				DELETED.[DefaultLC5]
			)
		) 
		END		
		
      If UPDATE([ChangeDefaultLC])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ChangeDefaultLC',
      CONVERT(NVARCHAR(2000),DELETED.[ChangeDefaultLC],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ChangeDefaultLC],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[ChangeDefaultLC] Is Null And
				DELETED.[ChangeDefaultLC] Is Not Null
			) Or
			(
				INSERTED.[ChangeDefaultLC] Is Not Null And
				DELETED.[ChangeDefaultLC] Is Null
			) Or
			(
				INSERTED.[ChangeDefaultLC] !=
				DELETED.[ChangeDefaultLC]
			)
		) 
		END		
		
      If UPDATE([TerminationDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'TerminationDate',
      CONVERT(NVARCHAR(2000),DELETED.[TerminationDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TerminationDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[TerminationDate] Is Null And
				DELETED.[TerminationDate] Is Not Null
			) Or
			(
				INSERTED.[TerminationDate] Is Not Null And
				DELETED.[TerminationDate] Is Null
			) Or
			(
				INSERTED.[TerminationDate] !=
				DELETED.[TerminationDate]
			)
		) 
		END		
		
      If UPDATE([UseTotalHrsAsStd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'UseTotalHrsAsStd',
      CONVERT(NVARCHAR(2000),DELETED.[UseTotalHrsAsStd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UseTotalHrsAsStd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[UseTotalHrsAsStd] Is Null And
				DELETED.[UseTotalHrsAsStd] Is Not Null
			) Or
			(
				INSERTED.[UseTotalHrsAsStd] Is Not Null And
				DELETED.[UseTotalHrsAsStd] Is Null
			) Or
			(
				INSERTED.[UseTotalHrsAsStd] !=
				DELETED.[UseTotalHrsAsStd]
			)
		) 
		END		
		
      If UPDATE([JCSpecialOvtPct])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'JCSpecialOvtPct',
      CONVERT(NVARCHAR(2000),DELETED.[JCSpecialOvtPct],121),
      CONVERT(NVARCHAR(2000),INSERTED.[JCSpecialOvtPct],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[JCSpecialOvtPct] Is Null And
				DELETED.[JCSpecialOvtPct] Is Not Null
			) Or
			(
				INSERTED.[JCSpecialOvtPct] Is Not Null And
				DELETED.[JCSpecialOvtPct] Is Null
			) Or
			(
				INSERTED.[JCSpecialOvtPct] !=
				DELETED.[JCSpecialOvtPct]
			)
		) 
		END		
		
      If UPDATE([ProvCostSpecialOTPct])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ProvCostSpecialOTPct',
      CONVERT(NVARCHAR(2000),DELETED.[ProvCostSpecialOTPct],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProvCostSpecialOTPct],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[ProvCostSpecialOTPct] Is Null And
				DELETED.[ProvCostSpecialOTPct] Is Not Null
			) Or
			(
				INSERTED.[ProvCostSpecialOTPct] Is Not Null And
				DELETED.[ProvCostSpecialOTPct] Is Null
			) Or
			(
				INSERTED.[ProvCostSpecialOTPct] !=
				DELETED.[ProvCostSpecialOTPct]
			)
		) 
		END		
		
      If UPDATE([ProvBillSpecialOTPct])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ProvBillSpecialOTPct',
      CONVERT(NVARCHAR(2000),DELETED.[ProvBillSpecialOTPct],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProvBillSpecialOTPct],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[ProvBillSpecialOTPct] Is Null And
				DELETED.[ProvBillSpecialOTPct] Is Not Null
			) Or
			(
				INSERTED.[ProvBillSpecialOTPct] Is Not Null And
				DELETED.[ProvBillSpecialOTPct] Is Null
			) Or
			(
				INSERTED.[ProvBillSpecialOTPct] !=
				DELETED.[ProvBillSpecialOTPct]
			)
		) 
		END		
		
      If UPDATE([YearsOtherFirms])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'YearsOtherFirms',
      CONVERT(NVARCHAR(2000),DELETED.[YearsOtherFirms],121),
      CONVERT(NVARCHAR(2000),INSERTED.[YearsOtherFirms],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[YearsOtherFirms] Is Null And
				DELETED.[YearsOtherFirms] Is Not Null
			) Or
			(
				INSERTED.[YearsOtherFirms] Is Not Null And
				DELETED.[YearsOtherFirms] Is Null
			) Or
			(
				INSERTED.[YearsOtherFirms] !=
				DELETED.[YearsOtherFirms]
			)
		) 
		END		
		
      If UPDATE([Supervisor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'Supervisor',
      CONVERT(NVARCHAR(2000),DELETED.[Supervisor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Supervisor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[Supervisor] Is Null And
				DELETED.[Supervisor] Is Not Null
			) Or
			(
				INSERTED.[Supervisor] Is Not Null And
				DELETED.[Supervisor] Is Null
			) Or
			(
				INSERTED.[Supervisor] !=
				DELETED.[Supervisor]
			)
		) 
		END		
		
      If UPDATE([ReadyForProcessing])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ReadyForProcessing',
      CONVERT(NVARCHAR(2000),DELETED.[ReadyForProcessing],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReadyForProcessing],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[ReadyForProcessing] Is Null And
				DELETED.[ReadyForProcessing] Is Not Null
			) Or
			(
				INSERTED.[ReadyForProcessing] Is Not Null And
				DELETED.[ReadyForProcessing] Is Null
			) Or
			(
				INSERTED.[ReadyForProcessing] !=
				DELETED.[ReadyForProcessing]
			)
		) 
		END		
		
      If UPDATE([CheckHours])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'CheckHours',
      CONVERT(NVARCHAR(2000),DELETED.[CheckHours],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CheckHours],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[CheckHours] Is Null And
				DELETED.[CheckHours] Is Not Null
			) Or
			(
				INSERTED.[CheckHours] Is Not Null And
				DELETED.[CheckHours] Is Null
			) Or
			(
				INSERTED.[CheckHours] !=
				DELETED.[CheckHours]
			)
		) 
		END		
		
      If UPDATE([Locale])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'Locale',
      CONVERT(NVARCHAR(2000),DELETED.[Locale],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Locale],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[Locale] Is Null And
				DELETED.[Locale] Is Not Null
			) Or
			(
				INSERTED.[Locale] Is Not Null And
				DELETED.[Locale] Is Null
			) Or
			(
				INSERTED.[Locale] !=
				DELETED.[Locale]
			)
		) 
		END		
		
      If UPDATE([LocaleMethod])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'LocaleMethod',
      CONVERT(NVARCHAR(2000),DELETED.[LocaleMethod],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LocaleMethod],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[LocaleMethod] Is Null And
				DELETED.[LocaleMethod] Is Not Null
			) Or
			(
				INSERTED.[LocaleMethod] Is Not Null And
				DELETED.[LocaleMethod] Is Null
			) Or
			(
				INSERTED.[LocaleMethod] !=
				DELETED.[LocaleMethod]
			)
		) 
		END		
		
      If UPDATE([OtherPay])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'OtherPay',
      CONVERT(NVARCHAR(2000),DELETED.[OtherPay],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OtherPay],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[OtherPay] Is Null And
				DELETED.[OtherPay] Is Not Null
			) Or
			(
				INSERTED.[OtherPay] Is Not Null And
				DELETED.[OtherPay] Is Null
			) Or
			(
				INSERTED.[OtherPay] !=
				DELETED.[OtherPay]
			)
		) 
		END		
		
      If UPDATE([OtherPay2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'OtherPay2',
      CONVERT(NVARCHAR(2000),DELETED.[OtherPay2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OtherPay2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[OtherPay2] Is Null And
				DELETED.[OtherPay2] Is Not Null
			) Or
			(
				INSERTED.[OtherPay2] Is Not Null And
				DELETED.[OtherPay2] Is Null
			) Or
			(
				INSERTED.[OtherPay2] !=
				DELETED.[OtherPay2]
			)
		) 
		END		
		
      If UPDATE([OtherPay3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'OtherPay3',
      CONVERT(NVARCHAR(2000),DELETED.[OtherPay3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OtherPay3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[OtherPay3] Is Null And
				DELETED.[OtherPay3] Is Not Null
			) Or
			(
				INSERTED.[OtherPay3] Is Not Null And
				DELETED.[OtherPay3] Is Null
			) Or
			(
				INSERTED.[OtherPay3] !=
				DELETED.[OtherPay3]
			)
		) 
		END		
		
      If UPDATE([OtherPay4])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'OtherPay4',
      CONVERT(NVARCHAR(2000),DELETED.[OtherPay4],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OtherPay4],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[OtherPay4] Is Null And
				DELETED.[OtherPay4] Is Not Null
			) Or
			(
				INSERTED.[OtherPay4] Is Not Null And
				DELETED.[OtherPay4] Is Null
			) Or
			(
				INSERTED.[OtherPay4] !=
				DELETED.[OtherPay4]
			)
		) 
		END		
		
      If UPDATE([OtherPay5])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'OtherPay5',
      CONVERT(NVARCHAR(2000),DELETED.[OtherPay5],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OtherPay5],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[OtherPay5] Is Null And
				DELETED.[OtherPay5] Is Not Null
			) Or
			(
				INSERTED.[OtherPay5] Is Not Null And
				DELETED.[OtherPay5] Is Null
			) Or
			(
				INSERTED.[OtherPay5] !=
				DELETED.[OtherPay5]
			)
		) 
		END		
		
      If UPDATE([CostRateMeth])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'CostRateMeth',
      CONVERT(NVARCHAR(2000),DELETED.[CostRateMeth],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CostRateMeth],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[CostRateMeth] Is Null And
				DELETED.[CostRateMeth] Is Not Null
			) Or
			(
				INSERTED.[CostRateMeth] Is Not Null And
				DELETED.[CostRateMeth] Is Null
			) Or
			(
				INSERTED.[CostRateMeth] !=
				DELETED.[CostRateMeth]
			)
		) 
		END		
		
      If UPDATE([CostRateTableNo])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'CostRateTableNo',
      CONVERT(NVARCHAR(2000),DELETED.[CostRateTableNo],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CostRateTableNo],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[CostRateTableNo] Is Null And
				DELETED.[CostRateTableNo] Is Not Null
			) Or
			(
				INSERTED.[CostRateTableNo] Is Not Null And
				DELETED.[CostRateTableNo] Is Null
			) Or
			(
				INSERTED.[CostRateTableNo] !=
				DELETED.[CostRateTableNo]
			)
		) 
		END		
		
      If UPDATE([PayRateMeth])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PayRateMeth',
      CONVERT(NVARCHAR(2000),DELETED.[PayRateMeth],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PayRateMeth],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[PayRateMeth] Is Null And
				DELETED.[PayRateMeth] Is Not Null
			) Or
			(
				INSERTED.[PayRateMeth] Is Not Null And
				DELETED.[PayRateMeth] Is Null
			) Or
			(
				INSERTED.[PayRateMeth] !=
				DELETED.[PayRateMeth]
			)
		) 
		END		
		
      If UPDATE([PayRateTableNo])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PayRateTableNo',
      CONVERT(NVARCHAR(2000),DELETED.[PayRateTableNo],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PayRateTableNo],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[PayRateTableNo] Is Null And
				DELETED.[PayRateTableNo] Is Not Null
			) Or
			(
				INSERTED.[PayRateTableNo] Is Not Null And
				DELETED.[PayRateTableNo] Is Null
			) Or
			(
				INSERTED.[PayRateTableNo] !=
				DELETED.[PayRateTableNo]
			)
		) 
		END		
		
      If UPDATE([PriorYearsFirm])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PriorYearsFirm',
      CONVERT(NVARCHAR(2000),DELETED.[PriorYearsFirm],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PriorYearsFirm],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[PriorYearsFirm] Is Null And
				DELETED.[PriorYearsFirm] Is Not Null
			) Or
			(
				INSERTED.[PriorYearsFirm] Is Not Null And
				DELETED.[PriorYearsFirm] Is Null
			) Or
			(
				INSERTED.[PriorYearsFirm] !=
				DELETED.[PriorYearsFirm]
			)
		) 
		END		
		
      If UPDATE([PaychexCode1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PaychexCode1',
      CONVERT(NVARCHAR(2000),DELETED.[PaychexCode1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PaychexCode1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[PaychexCode1] Is Null And
				DELETED.[PaychexCode1] Is Not Null
			) Or
			(
				INSERTED.[PaychexCode1] Is Not Null And
				DELETED.[PaychexCode1] Is Null
			) Or
			(
				INSERTED.[PaychexCode1] !=
				DELETED.[PaychexCode1]
			)
		) 
		END		
		
      If UPDATE([PaychexCode2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PaychexCode2',
      CONVERT(NVARCHAR(2000),DELETED.[PaychexCode2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PaychexCode2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[PaychexCode2] Is Null And
				DELETED.[PaychexCode2] Is Not Null
			) Or
			(
				INSERTED.[PaychexCode2] Is Not Null And
				DELETED.[PaychexCode2] Is Null
			) Or
			(
				INSERTED.[PaychexCode2] !=
				DELETED.[PaychexCode2]
			)
		) 
		END		
		
      If UPDATE([PaychexCode3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PaychexCode3',
      CONVERT(NVARCHAR(2000),DELETED.[PaychexCode3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PaychexCode3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[PaychexCode3] Is Null And
				DELETED.[PaychexCode3] Is Not Null
			) Or
			(
				INSERTED.[PaychexCode3] Is Not Null And
				DELETED.[PaychexCode3] Is Null
			) Or
			(
				INSERTED.[PaychexCode3] !=
				DELETED.[PaychexCode3]
			)
		) 
		END		
		
      If UPDATE([IncludeLocalJurisOnly])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'IncludeLocalJurisOnly',
      CONVERT(NVARCHAR(2000),DELETED.[IncludeLocalJurisOnly],121),
      CONVERT(NVARCHAR(2000),INSERTED.[IncludeLocalJurisOnly],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[IncludeLocalJurisOnly] Is Null And
				DELETED.[IncludeLocalJurisOnly] Is Not Null
			) Or
			(
				INSERTED.[IncludeLocalJurisOnly] Is Not Null And
				DELETED.[IncludeLocalJurisOnly] Is Null
			) Or
			(
				INSERTED.[IncludeLocalJurisOnly] !=
				DELETED.[IncludeLocalJurisOnly]
			)
		) 
		END		
		
      If UPDATE([AllowChargeUnits])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'AllowChargeUnits',
      CONVERT(NVARCHAR(2000),DELETED.[AllowChargeUnits],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowChargeUnits],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[AllowChargeUnits] Is Null And
				DELETED.[AllowChargeUnits] Is Not Null
			) Or
			(
				INSERTED.[AllowChargeUnits] Is Not Null And
				DELETED.[AllowChargeUnits] Is Null
			) Or
			(
				INSERTED.[AllowChargeUnits] !=
				DELETED.[AllowChargeUnits]
			)
		) 
		END		
		
      If UPDATE([RequireStartEndTime])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'RequireStartEndTime',
      CONVERT(NVARCHAR(2000),DELETED.[RequireStartEndTime],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RequireStartEndTime],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[RequireStartEndTime] Is Null And
				DELETED.[RequireStartEndTime] Is Not Null
			) Or
			(
				INSERTED.[RequireStartEndTime] Is Not Null And
				DELETED.[RequireStartEndTime] Is Null
			) Or
			(
				INSERTED.[RequireStartEndTime] !=
				DELETED.[RequireStartEndTime]
			)
		) 
		END		
		
      If UPDATE([AllowBreakTime])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'AllowBreakTime',
      CONVERT(NVARCHAR(2000),DELETED.[AllowBreakTime],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllowBreakTime],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[AllowBreakTime] Is Null And
				DELETED.[AllowBreakTime] Is Not Null
			) Or
			(
				INSERTED.[AllowBreakTime] Is Not Null And
				DELETED.[AllowBreakTime] Is Null
			) Or
			(
				INSERTED.[AllowBreakTime] !=
				DELETED.[AllowBreakTime]
			)
		) 
		END		
		
      If UPDATE([DefaultBreakStartDateTime])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DefaultBreakStartDateTime',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultBreakStartDateTime],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultBreakStartDateTime],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[DefaultBreakStartDateTime] Is Null And
				DELETED.[DefaultBreakStartDateTime] Is Not Null
			) Or
			(
				INSERTED.[DefaultBreakStartDateTime] Is Not Null And
				DELETED.[DefaultBreakStartDateTime] Is Null
			) Or
			(
				INSERTED.[DefaultBreakStartDateTime] !=
				DELETED.[DefaultBreakStartDateTime]
			)
		) 
		END		
		
      If UPDATE([DefaultBreakEndDateTime])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DefaultBreakEndDateTime',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultBreakEndDateTime],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultBreakEndDateTime],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[DefaultBreakEndDateTime] Is Null And
				DELETED.[DefaultBreakEndDateTime] Is Not Null
			) Or
			(
				INSERTED.[DefaultBreakEndDateTime] Is Not Null And
				DELETED.[DefaultBreakEndDateTime] Is Null
			) Or
			(
				INSERTED.[DefaultBreakEndDateTime] !=
				DELETED.[DefaultBreakEndDateTime]
			)
		) 
		END		
		
      If UPDATE([PaychexRateNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'PaychexRateNumber',
      CONVERT(NVARCHAR(2000),DELETED.[PaychexRateNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PaychexRateNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[PaychexRateNumber] Is Null And
				DELETED.[PaychexRateNumber] Is Not Null
			) Or
			(
				INSERTED.[PaychexRateNumber] Is Not Null And
				DELETED.[PaychexRateNumber] Is Null
			) Or
			(
				INSERTED.[PaychexRateNumber] !=
				DELETED.[PaychexRateNumber]
			)
		) 
		END		
		
      If UPDATE([EmailPayrollRemittance])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'EmailPayrollRemittance',
      CONVERT(NVARCHAR(2000),DELETED.[EmailPayrollRemittance],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EmailPayrollRemittance],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[EmailPayrollRemittance] Is Null And
				DELETED.[EmailPayrollRemittance] Is Not Null
			) Or
			(
				INSERTED.[EmailPayrollRemittance] Is Not Null And
				DELETED.[EmailPayrollRemittance] Is Null
			) Or
			(
				INSERTED.[EmailPayrollRemittance] !=
				DELETED.[EmailPayrollRemittance]
			)
		) 
		END		
		
      If UPDATE([EmailExpenseRemittance])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'EmailExpenseRemittance',
      CONVERT(NVARCHAR(2000),DELETED.[EmailExpenseRemittance],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EmailExpenseRemittance],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[EmailExpenseRemittance] Is Null And
				DELETED.[EmailExpenseRemittance] Is Not Null
			) Or
			(
				INSERTED.[EmailExpenseRemittance] Is Not Null And
				DELETED.[EmailExpenseRemittance] Is Null
			) Or
			(
				INSERTED.[EmailExpenseRemittance] !=
				DELETED.[EmailExpenseRemittance]
			)
		) 
		END		
		
      If UPDATE([OccupationalCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'OccupationalCode',
      CONVERT(NVARCHAR(2000),DELETED.[OccupationalCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OccupationalCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[OccupationalCode] Is Null And
				DELETED.[OccupationalCode] Is Not Null
			) Or
			(
				INSERTED.[OccupationalCode] Is Not Null And
				DELETED.[OccupationalCode] Is Null
			) Or
			(
				INSERTED.[OccupationalCode] !=
				DELETED.[OccupationalCode]
			)
		) 
		END		
		
      If UPDATE([GeographicCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'GeographicCode',
      CONVERT(NVARCHAR(2000),DELETED.[GeographicCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[GeographicCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[GeographicCode] Is Null And
				DELETED.[GeographicCode] Is Not Null
			) Or
			(
				INSERTED.[GeographicCode] Is Not Null And
				DELETED.[GeographicCode] Is Null
			) Or
			(
				INSERTED.[GeographicCode] !=
				DELETED.[GeographicCode]
			)
		) 
		END		
		
      If UPDATE([StatutoryEmployee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'StatutoryEmployee',
      CONVERT(NVARCHAR(2000),DELETED.[StatutoryEmployee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[StatutoryEmployee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[StatutoryEmployee] Is Null And
				DELETED.[StatutoryEmployee] Is Not Null
			) Or
			(
				INSERTED.[StatutoryEmployee] Is Not Null And
				DELETED.[StatutoryEmployee] Is Null
			) Or
			(
				INSERTED.[StatutoryEmployee] !=
				DELETED.[StatutoryEmployee]
			)
		) 
		END		
		
      If UPDATE([RetirementPlan])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'RetirementPlan',
      CONVERT(NVARCHAR(2000),DELETED.[RetirementPlan],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RetirementPlan],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[RetirementPlan] Is Null And
				DELETED.[RetirementPlan] Is Not Null
			) Or
			(
				INSERTED.[RetirementPlan] Is Not Null And
				DELETED.[RetirementPlan] Is Null
			) Or
			(
				INSERTED.[RetirementPlan] !=
				DELETED.[RetirementPlan]
			)
		) 
		END		
		
      If UPDATE([ThirdPartySickPay])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ThirdPartySickPay',
      CONVERT(NVARCHAR(2000),DELETED.[ThirdPartySickPay],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ThirdPartySickPay],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[ThirdPartySickPay] Is Null And
				DELETED.[ThirdPartySickPay] Is Not Null
			) Or
			(
				INSERTED.[ThirdPartySickPay] Is Not Null And
				DELETED.[ThirdPartySickPay] Is Null
			) Or
			(
				INSERTED.[ThirdPartySickPay] !=
				DELETED.[ThirdPartySickPay]
			)
		) 
		END		
		
      If UPDATE([ClieOpTransactionType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ClieOpTransactionType',
      CONVERT(NVARCHAR(2000),DELETED.[ClieOpTransactionType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClieOpTransactionType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[ClieOpTransactionType] Is Null And
				DELETED.[ClieOpTransactionType] Is Not Null
			) Or
			(
				INSERTED.[ClieOpTransactionType] Is Not Null And
				DELETED.[ClieOpTransactionType] Is Null
			) Or
			(
				INSERTED.[ClieOpTransactionType] !=
				DELETED.[ClieOpTransactionType]
			)
		) 
		END		
		
      If UPDATE([ClieOp])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ClieOp',
      CONVERT(NVARCHAR(2000),DELETED.[ClieOp],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClieOp],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[ClieOp] Is Null And
				DELETED.[ClieOp] Is Not Null
			) Or
			(
				INSERTED.[ClieOp] Is Not Null And
				DELETED.[ClieOp] Is Null
			) Or
			(
				INSERTED.[ClieOp] !=
				DELETED.[ClieOp]
			)
		) 
		END		
		
      If UPDATE([ClieOpAccount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ClieOpAccount',
      CONVERT(NVARCHAR(2000),DELETED.[ClieOpAccount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClieOpAccount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[ClieOpAccount] Is Null And
				DELETED.[ClieOpAccount] Is Not Null
			) Or
			(
				INSERTED.[ClieOpAccount] Is Not Null And
				DELETED.[ClieOpAccount] Is Null
			) Or
			(
				INSERTED.[ClieOpAccount] !=
				DELETED.[ClieOpAccount]
			)
		) 
		END		
		
      If UPDATE([ClieOpAccountType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'ClieOpAccountType',
      CONVERT(NVARCHAR(2000),DELETED.[ClieOpAccountType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClieOpAccountType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[ClieOpAccountType] Is Null And
				DELETED.[ClieOpAccountType] Is Not Null
			) Or
			(
				INSERTED.[ClieOpAccountType] Is Not Null And
				DELETED.[ClieOpAccountType] Is Null
			) Or
			(
				INSERTED.[ClieOpAccountType] !=
				DELETED.[ClieOpAccountType]
			)
		) 
		END		
		
      If UPDATE([SEPAIBAN])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'SEPAIBAN',
      CONVERT(NVARCHAR(2000),DELETED.[SEPAIBAN],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SEPAIBAN],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[SEPAIBAN] Is Null And
				DELETED.[SEPAIBAN] Is Not Null
			) Or
			(
				INSERTED.[SEPAIBAN] Is Not Null And
				DELETED.[SEPAIBAN] Is Null
			) Or
			(
				INSERTED.[SEPAIBAN] !=
				DELETED.[SEPAIBAN]
			)
		) 
		END		
		
      If UPDATE([SEPABIC])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'SEPABIC',
      CONVERT(NVARCHAR(2000),DELETED.[SEPABIC],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SEPABIC],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[SEPABIC] Is Null And
				DELETED.[SEPABIC] Is Not Null
			) Or
			(
				INSERTED.[SEPABIC] Is Not Null And
				DELETED.[SEPABIC] Is Null
			) Or
			(
				INSERTED.[SEPABIC] !=
				DELETED.[SEPABIC]
			)
		) 
		END		
		
      If UPDATE([TaxRegistrationNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'TaxRegistrationNumber',
      CONVERT(NVARCHAR(2000),DELETED.[TaxRegistrationNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TaxRegistrationNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[TaxRegistrationNumber] Is Null And
				DELETED.[TaxRegistrationNumber] Is Not Null
			) Or
			(
				INSERTED.[TaxRegistrationNumber] Is Not Null And
				DELETED.[TaxRegistrationNumber] Is Null
			) Or
			(
				INSERTED.[TaxRegistrationNumber] !=
				DELETED.[TaxRegistrationNumber]
			)
		) 
		END		
		
      If UPDATE([DisableTSRevAudit])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DisableTSRevAudit',
      CONVERT(NVARCHAR(2000),DELETED.[DisableTSRevAudit],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DisableTSRevAudit],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[DisableTSRevAudit] Is Null And
				DELETED.[DisableTSRevAudit] Is Not Null
			) Or
			(
				INSERTED.[DisableTSRevAudit] Is Not Null And
				DELETED.[DisableTSRevAudit] Is Null
			) Or
			(
				INSERTED.[DisableTSRevAudit] !=
				DELETED.[DisableTSRevAudit]
			)
		) 
		END		
		
      If UPDATE([UIPaymentMethod])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'UIPaymentMethod',
      CONVERT(NVARCHAR(2000),DELETED.[UIPaymentMethod],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UIPaymentMethod],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[UIPaymentMethod] Is Null And
				DELETED.[UIPaymentMethod] Is Not Null
			) Or
			(
				INSERTED.[UIPaymentMethod] Is Not Null And
				DELETED.[UIPaymentMethod] Is Null
			) Or
			(
				INSERTED.[UIPaymentMethod] !=
				DELETED.[UIPaymentMethod]
			)
		) 
		END		
		
      If UPDATE([DefaultTaxLocation])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'DefaultTaxLocation',
      CONVERT(NVARCHAR(2000),DELETED.[DefaultTaxLocation],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DefaultTaxLocation],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[DefaultTaxLocation] Is Null And
				DELETED.[DefaultTaxLocation] Is Not Null
			) Or
			(
				INSERTED.[DefaultTaxLocation] Is Not Null And
				DELETED.[DefaultTaxLocation] Is Null
			) Or
			(
				INSERTED.[DefaultTaxLocation] !=
				DELETED.[DefaultTaxLocation]
			)
		) 
		END		
		
      If UPDATE([Terminated])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'Terminated',
      CONVERT(NVARCHAR(2000),DELETED.[Terminated],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Terminated],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[Terminated] Is Null And
				DELETED.[Terminated] Is Not Null
			) Or
			(
				INSERTED.[Terminated] Is Not Null And
				DELETED.[Terminated] Is Null
			) Or
			(
				INSERTED.[Terminated] !=
				DELETED.[Terminated]
			)
		) 
		END		
		
      If UPDATE([FormW4Version])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'FormW4Version',
      CONVERT(NVARCHAR(2000),DELETED.[FormW4Version],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FormW4Version],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[FormW4Version] Is Null And
				DELETED.[FormW4Version] Is Not Null
			) Or
			(
				INSERTED.[FormW4Version] Is Not Null And
				DELETED.[FormW4Version] Is Null
			) Or
			(
				INSERTED.[FormW4Version] !=
				DELETED.[FormW4Version]
			)
		) 
		END		
		
      If UPDATE([FormW4Step2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'FormW4Step2',
      CONVERT(NVARCHAR(2000),DELETED.[FormW4Step2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FormW4Step2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[FormW4Step2] Is Null And
				DELETED.[FormW4Step2] Is Not Null
			) Or
			(
				INSERTED.[FormW4Step2] Is Not Null And
				DELETED.[FormW4Step2] Is Null
			) Or
			(
				INSERTED.[FormW4Step2] !=
				DELETED.[FormW4Step2]
			)
		) 
		END		
		
      If UPDATE([FormW4Dependents])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'FormW4Dependents',
      CONVERT(NVARCHAR(2000),DELETED.[FormW4Dependents],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FormW4Dependents],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[FormW4Dependents] Is Null And
				DELETED.[FormW4Dependents] Is Not Null
			) Or
			(
				INSERTED.[FormW4Dependents] Is Not Null And
				DELETED.[FormW4Dependents] Is Null
			) Or
			(
				INSERTED.[FormW4Dependents] !=
				DELETED.[FormW4Dependents]
			)
		) 
		END		
		
      If UPDATE([FormW4DependentsOther])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'FormW4DependentsOther',
      CONVERT(NVARCHAR(2000),DELETED.[FormW4DependentsOther],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FormW4DependentsOther],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[FormW4DependentsOther] Is Null And
				DELETED.[FormW4DependentsOther] Is Not Null
			) Or
			(
				INSERTED.[FormW4DependentsOther] Is Not Null And
				DELETED.[FormW4DependentsOther] Is Null
			) Or
			(
				INSERTED.[FormW4DependentsOther] !=
				DELETED.[FormW4DependentsOther]
			)
		) 
		END		
		
      If UPDATE([FormW4OtherTaxCredit])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'FormW4OtherTaxCredit',
      CONVERT(NVARCHAR(2000),DELETED.[FormW4OtherTaxCredit],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FormW4OtherTaxCredit],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[FormW4OtherTaxCredit] Is Null And
				DELETED.[FormW4OtherTaxCredit] Is Not Null
			) Or
			(
				INSERTED.[FormW4OtherTaxCredit] Is Not Null And
				DELETED.[FormW4OtherTaxCredit] Is Null
			) Or
			(
				INSERTED.[FormW4OtherTaxCredit] !=
				DELETED.[FormW4OtherTaxCredit]
			)
		) 
		END		
		
      If UPDATE([FormW4OtherIncome])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'FormW4OtherIncome',
      CONVERT(NVARCHAR(2000),DELETED.[FormW4OtherIncome],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FormW4OtherIncome],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[FormW4OtherIncome] Is Null And
				DELETED.[FormW4OtherIncome] Is Not Null
			) Or
			(
				INSERTED.[FormW4OtherIncome] Is Not Null And
				DELETED.[FormW4OtherIncome] Is Null
			) Or
			(
				INSERTED.[FormW4OtherIncome] !=
				DELETED.[FormW4OtherIncome]
			)
		) 
		END		
		
      If UPDATE([FormW4Deductions])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[EmployeeCompany],121),'FormW4Deductions',
      CONVERT(NVARCHAR(2000),DELETED.[FormW4Deductions],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FormW4Deductions],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND INSERTED.[EmployeeCompany] = DELETED.[EmployeeCompany] AND 
		(
			(
				INSERTED.[FormW4Deductions] Is Null And
				DELETED.[FormW4Deductions] Is Not Null
			) Or
			(
				INSERTED.[FormW4Deductions] Is Not Null And
				DELETED.[FormW4Deductions] Is Null
			) Or
			(
				INSERTED.[FormW4Deductions] !=
				DELETED.[FormW4Deductions]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_EMCompany] ON [dbo].[EMCompany]
GO
ALTER TABLE [dbo].[EMCompany] ADD CONSTRAINT [EMCompanyPK] PRIMARY KEY NONCLUSTERED ([Employee], [EmployeeCompany]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [EMCompanyEmployeeEmployeeCompanyIDX] ON [dbo].[EMCompany] ([Employee], [EmployeeCompany]) INCLUDE ([Org]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [EMCompanyTimeAnalysisIDX] ON [dbo].[EMCompany] ([Employee], [HireDate], [Org], [TerminationDate]) ON [PRIMARY]
GO
