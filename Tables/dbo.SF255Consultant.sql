CREATE TABLE [dbo].[SF255Consultant]
(
[SF255ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__SF255Consul__Seq__19598C40] DEFAULT ((0)),
[Consultant] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifiedInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF255Cons__Verif__1A4DB079] DEFAULT ('N'),
[ConsultantInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpecialtyDescription] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PriorWorkInd] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF255Consultant] ADD CONSTRAINT [SF255ConsultantPK] PRIMARY KEY NONCLUSTERED ([SF255ID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
