CREATE TABLE [dbo].[PRMilestone]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecordID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EndDate] [datetime] NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_PRMilestone]
      ON [dbo].[PRMilestone]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'PRMilestone'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'WBS1',CONVERT(NVARCHAR(2000),[WBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'WBS2',CONVERT(NVARCHAR(2000),[WBS2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'WBS3',CONVERT(NVARCHAR(2000),[WBS3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'RecordID',CONVERT(NVARCHAR(2000),[RecordID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'Code',CONVERT(NVARCHAR(2000),[Code],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'EndDate',CONVERT(NVARCHAR(2000),[EndDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[RecordID],121),'Notes','[text]',NULL, @source,@app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_PRMilestone] ON [dbo].[PRMilestone]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_PRMilestone]
      ON [dbo].[PRMilestone]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'PRMilestone'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'WBS1',NULL,CONVERT(NVARCHAR(2000),[WBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'WBS2',NULL,CONVERT(NVARCHAR(2000),[WBS2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'WBS3',NULL,CONVERT(NVARCHAR(2000),[WBS3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'RecordID',NULL,CONVERT(NVARCHAR(2000),[RecordID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Code',NULL,CONVERT(NVARCHAR(2000),[Code],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'EndDate',NULL,CONVERT(NVARCHAR(2000),[EndDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Notes',NULL,'[text]', @source, @app
      FROM INSERTED


    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_PRMilestone] ON [dbo].[PRMilestone]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_PRMilestone]
      ON [dbo].[PRMilestone]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'PRMilestone'
    
      If UPDATE([WBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'WBS1',
      CONVERT(NVARCHAR(2000),DELETED.[WBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[WBS1] Is Null And
				DELETED.[WBS1] Is Not Null
			) Or
			(
				INSERTED.[WBS1] Is Not Null And
				DELETED.[WBS1] Is Null
			) Or
			(
				INSERTED.[WBS1] !=
				DELETED.[WBS1]
			)
		) 
		END		
		
      If UPDATE([WBS2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'WBS2',
      CONVERT(NVARCHAR(2000),DELETED.[WBS2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[WBS2] Is Null And
				DELETED.[WBS2] Is Not Null
			) Or
			(
				INSERTED.[WBS2] Is Not Null And
				DELETED.[WBS2] Is Null
			) Or
			(
				INSERTED.[WBS2] !=
				DELETED.[WBS2]
			)
		) 
		END		
		
      If UPDATE([WBS3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'WBS3',
      CONVERT(NVARCHAR(2000),DELETED.[WBS3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[WBS3] Is Null And
				DELETED.[WBS3] Is Not Null
			) Or
			(
				INSERTED.[WBS3] Is Not Null And
				DELETED.[WBS3] Is Null
			) Or
			(
				INSERTED.[WBS3] !=
				DELETED.[WBS3]
			)
		) 
		END		
		
      If UPDATE([RecordID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'RecordID',
      CONVERT(NVARCHAR(2000),DELETED.[RecordID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecordID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[RecordID] Is Null And
				DELETED.[RecordID] Is Not Null
			) Or
			(
				INSERTED.[RecordID] Is Not Null And
				DELETED.[RecordID] Is Null
			) Or
			(
				INSERTED.[RecordID] !=
				DELETED.[RecordID]
			)
		) 
		END		
		
      If UPDATE([Code])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Code',
      CONVERT(NVARCHAR(2000),DELETED.[Code],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Code],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[Code] Is Null And
				DELETED.[Code] Is Not Null
			) Or
			(
				INSERTED.[Code] Is Not Null And
				DELETED.[Code] Is Null
			) Or
			(
				INSERTED.[Code] !=
				DELETED.[Code]
			)
		) 
		END		
		
      If UPDATE([EndDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'EndDate',
      CONVERT(NVARCHAR(2000),DELETED.[EndDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EndDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[WBS1] = DELETED.[WBS1] AND INSERTED.[WBS2] = DELETED.[WBS2] AND INSERTED.[WBS3] = DELETED.[WBS3] AND INSERTED.[RecordID] = DELETED.[RecordID] AND 
		(
			(
				INSERTED.[EndDate] Is Null And
				DELETED.[EndDate] Is Not Null
			) Or
			(
				INSERTED.[EndDate] Is Not Null And
				DELETED.[EndDate] Is Null
			) Or
			(
				INSERTED.[EndDate] !=
				DELETED.[EndDate]
			)
		) 
		END		
		
      If UPDATE([Notes])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[WBS1],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS2],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[WBS3],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[RecordID],121),'Notes',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_PRMilestone] ON [dbo].[PRMilestone]
GO
ALTER TABLE [dbo].[PRMilestone] ADD CONSTRAINT [PRMilestonePK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [RecordID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
