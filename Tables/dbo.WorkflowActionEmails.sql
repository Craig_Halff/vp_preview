CREATE TABLE [dbo].[WorkflowActionEmails]
(
[ActionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ToListEmails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToListUsers] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToListRoles] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToListSpecial] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToListSpecialDisplay] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCListEmails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCListUsers] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCListRoles] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCListSpecial] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCListSpecialDisplay] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BCCListEmails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BCCListUsers] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BCCListRoles] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BCCListSpecial] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BCCListSpecialDisplay] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailSubject] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailContent] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CombineByUser] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__WorkflowA__Combi__084EFBCF] DEFAULT ('N'),
[AddAttachment] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__WorkflowA__AddAt__09432008] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowActionEmails] ADD CONSTRAINT [WorkflowActionEmailsPK] PRIMARY KEY NONCLUSTERED ([ActionID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
