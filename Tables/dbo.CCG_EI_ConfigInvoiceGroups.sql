CREATE TABLE [dbo].[CCG_EI_ConfigInvoiceGroups]
(
[InvoiceGroup] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ThruPeriod] [int] NULL,
[ThruDate] [datetime] NULL,
[CascadeMenu] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_ConfigInvoiceGroups] ADD CONSTRAINT [PK_CCG_EI_ConfigInvoiceGroups] PRIMARY KEY CLUSTERED ([InvoiceGroup]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Defines the set of invoice groups to be used in EI', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigInvoiceGroups', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Optional sub menu to group this entry into', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigInvoiceGroups', 'COLUMN', N'CascadeMenu'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The name of the invoice group', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigInvoiceGroups', 'COLUMN', N'InvoiceGroup'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The default Thru Date filter to use for the invoice group', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigInvoiceGroups', 'COLUMN', N'ThruDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The default Thru Period filter to use for the invoice group', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigInvoiceGroups', 'COLUMN', N'ThruPeriod'
GO
