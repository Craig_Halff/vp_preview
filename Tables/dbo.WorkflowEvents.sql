CREATE TABLE [dbo].[WorkflowEvents]
(
[EventID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ApplicationName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TableName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EventType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__WorkflowE__Activ__38F2332A] DEFAULT ('Y'),
[PRLevel] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EventOrder] [int] NOT NULL CONSTRAINT [DF__WorkflowE__Event__39E65763] DEFAULT ((0)),
[ApplicationType] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReadOnly] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__WorkflowE__ReadO__3ADA7B9C] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowEvents] ADD CONSTRAINT [WorkflowEventsPK] PRIMARY KEY NONCLUSTERED ([EventID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
