CREATE TABLE [dbo].[SavedInvoices]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL IDENTITY(1, 1),
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PreInvoice] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SavedInvo__PreIn__0881D9ED] DEFAULT ('N'),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileType] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceContents] [image] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SavedInvoices] ADD CONSTRAINT [SavedInvoicesPK] PRIMARY KEY NONCLUSTERED ([WBS1], [Seq], [Invoice]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [SavedInvoicesFileTypeIDX] ON [dbo].[SavedInvoices] ([WBS1], [Invoice], [FileType]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
