CREATE TABLE [dbo].[PRExpense]
(
[ExpenseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OppExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRExpense__OppEx__755CEE43] DEFAULT ((0)),
[OppExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRExpense__OppEx__7651127C] DEFAULT ((0)),
[DirectAcctFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRExpense__Direc__774536B5] DEFAULT ('N'),
[SeqNo] [smallint] NOT NULL CONSTRAINT [DF__PRExpense__SeqNo__78395AEE] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PRExpense] ADD CONSTRAINT [PRExpensePK] PRIMARY KEY NONCLUSTERED ([ExpenseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
