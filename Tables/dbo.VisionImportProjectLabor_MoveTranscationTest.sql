CREATE TABLE [dbo].[VisionImportProjectLabor_MoveTranscationTest]
(
[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LaborCode] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransDate] [datetime] NULL,
[Period] [int] NULL,
[RegHrs] [decimal] (19, 4) NULL,
[OvtHrs] [decimal] (19, 4) NULL,
[RegAmt] [decimal] (19, 4) NULL,
[OvtAmt] [decimal] (19, 4) NULL,
[TransComment] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillExt] [decimal] (19, 4) NULL,
[BillStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpecialOvtHrs] [decimal] (19, 4) NULL,
[SpecialOvtAmt] [decimal] (19, 4) NULL,
[SpecialOvtPct] [decimal] (19, 4) NULL,
[SpecialOvtRate] [decimal] (19, 4) NULL,
[Category] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RealizationAmount] [decimal] (19, 4) NULL
) ON [PRIMARY]
GO
