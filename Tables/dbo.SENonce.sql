CREATE TABLE [dbo].[SENonce]
(
[Nonce] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SessionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExpiresAt] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SENonce] ADD CONSTRAINT [SENoncePK] PRIMARY KEY NONCLUSTERED ([Nonce]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
