CREATE TABLE [dbo].[FW_DashPartLookupOptions]
(
[DashpartLookupOptions_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Dashpart_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[XML] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_DashPartLookupOptions] ADD CONSTRAINT [FW_DashPartLookupOptionsPK] PRIMARY KEY CLUSTERED ([DashpartLookupOptions_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
