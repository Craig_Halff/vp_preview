CREATE TABLE [dbo].[FW_ReportArchiveHistory]
(
[HistoryID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportPath] [nvarchar] (850) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedDate] [datetime] NULL,
[ExpirationDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_ReportArchiveHistory] ADD CONSTRAINT [FW_ReportArchiveHistoryPK] PRIMARY KEY CLUSTERED ([HistoryID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
