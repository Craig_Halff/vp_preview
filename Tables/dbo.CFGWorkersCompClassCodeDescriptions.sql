CREATE TABLE [dbo].[CFGWorkersCompClassCodeDescriptions]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGWorkersC__Seq__76861CA6] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGWorkersCompClassCodeDescriptions] ADD CONSTRAINT [CFGWorkersCompClassCodeDescriptionsPK] PRIMARY KEY CLUSTERED ([PKey], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
