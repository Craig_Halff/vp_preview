CREATE TABLE [dbo].[CFGOrganizationStatusDescriptions]
(
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGOrganizationStatusDescriptions] ADD CONSTRAINT [CFGOrganizationStatusDescriptionsPK] PRIMARY KEY CLUSTERED ([Status], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
