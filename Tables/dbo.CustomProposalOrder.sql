CREATE TABLE [dbo].[CustomProposalOrder]
(
[CustomPropID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SectionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SectionName] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SectionDescription] [nvarchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InfoCenterArea] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MergedFileName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SectionOrder] [smallint] NOT NULL CONSTRAINT [DF__CustomPro__Secti__36BE3B47] DEFAULT ((0)),
[MergeTemplateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomProposalOrder] ADD CONSTRAINT [CustomProposalOrderPK] PRIMARY KEY NONCLUSTERED ([CustomPropID], [SectionID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
