CREATE TABLE [dbo].[CFGConsolidationEliminations]
(
[ReportingGroup] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGConsolidationEliminations] ADD CONSTRAINT [CFGConsolidationEliminationsPK] PRIMARY KEY CLUSTERED ([ReportingGroup], [Account]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
