CREATE TABLE [dbo].[CFGAuditTableConfig]
(
[TableName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AuditArea] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AuditType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAuditTableConfig] ADD CONSTRAINT [CFGAuditTableConfigPK] PRIMARY KEY CLUSTERED ([TableName], [AuditArea]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
