CREATE TABLE [dbo].[AnalysisCubesFilteringData]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__AnalysisCu__Type__3C9644D7] DEFAULT ('D'),
[Cubes] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Include] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__AnalysisC__Inclu__3D8A6910] DEFAULT ('N'),
[GroupName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FolderName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CombinedID] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultFieldName] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Subtype] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__AnalysisC__Subty__3E7E8D49] DEFAULT ('S')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AnalysisCubesFilteringData] ADD CONSTRAINT [AnalysisCubesFilteringDataPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
