CREATE TABLE [dbo].[ReceiveDetail]
(
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[POPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AcceptedQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ReceiveDe__Accep__615F4BE0] DEFAULT ((0)),
[AcceptedAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ReceiveDe__Accep__62537019] DEFAULT ((0)),
[RejectedQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ReceiveDe__Rejec__63479452] DEFAULT ((0)),
[RejectedAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ReceiveDe__Rejec__643BB88B] DEFAULT ((0)),
[RejectedQuality] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Closed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ReceiveDe__Close__652FDCC4] DEFAULT ('N'),
[InventoryQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ReceiveDe__Inven__662400FD] DEFAULT ((0)),
[Location] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReceiveDetail] ADD CONSTRAINT [ReceiveDetailPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [POPKey], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
