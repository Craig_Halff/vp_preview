CREATE TABLE [dbo].[POPRVendorDetail]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PRDetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Line] [int] NOT NULL CONSTRAINT [DF__POPRVendor__Line__0E27055F] DEFAULT ((0)),
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Address] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VendorItem] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuotePrice] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POPRVendo__Quote__0F1B2998] DEFAULT ((0)),
[UOM] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuoteDate] [datetime] NULL,
[QuotePerson] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EndDate] [datetime] NULL,
[Selected] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POPRVendo__Selec__100F4DD1] DEFAULT ('N'),
[Printed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POPRVendo__Print__1103720A] DEFAULT ('N'),
[VendorResponse01] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorResponse02] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorResponse03] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorResponse04] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorResponse05] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorResponse06] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorResponse07] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorResponse08] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorResponse09] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorResponse10] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Response01ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Response02ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Response03ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Response04ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Response05ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Response06ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Response07ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Response08ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Response09ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Response10ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorResponseApplyToAllItems] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POPRVendo__Vendo__11F79643] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POPRVendorDetail] ADD CONSTRAINT [POPRVendorDetailPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [PRDetailPKey], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
