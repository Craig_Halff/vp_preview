CREATE TABLE [dbo].[exMaster]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportDate] [datetime] NULL,
[ReportName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__exMaster___Poste__58C93A88] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__exMaster_Ne__Seq__59BD5EC1] DEFAULT ((0)),
[AdvanceAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__exMaster___Advan__5AB182FA] DEFAULT ((0)),
[BarCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentExchangeOverrideMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__exMaster___Payme__5BA5A733] DEFAULT ('N'),
[PaymentExchangeOverrideDate] [datetime] NULL,
[PaymentExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__exMaster___Payme__5C99CB6C] DEFAULT ((0)),
[CurrencyExchangeOverrideMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__exMaster___Curre__5D8DEFA5] DEFAULT ('N'),
[CurrencyExchangeOverrideDate] [datetime] NULL,
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__exMaster___Curre__5E8213DE] DEFAULT ((0)),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__exMaster___Statu__5F763817] DEFAULT ('N'),
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RejectReason] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[exMaster] ADD CONSTRAINT [exMasterPK] PRIMARY KEY NONCLUSTERED ([Batch], [MasterPKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
