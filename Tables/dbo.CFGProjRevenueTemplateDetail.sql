CREATE TABLE [dbo].[CFGProjRevenueTemplateDetail]
(
[DetailID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TemplateID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TimeFrame] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeFromStart] [smallint] NOT NULL CONSTRAINT [DF__CFGProjRe__TimeF__13E17563] DEFAULT ((0)),
[PercentRevenue] [smallint] NOT NULL CONSTRAINT [DF__CFGProjRe__Perce__14D5999C] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGProjRevenueTemplateDetail] ADD CONSTRAINT [CFGProjRevenueTemplateDetailPK] PRIMARY KEY CLUSTERED ([DetailID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
