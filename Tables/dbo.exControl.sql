CREATE TABLE [dbo].[exControl]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PostPeriod] [int] NOT NULL CONSTRAINT [DF__exControl__PostP__42D9F969] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__exControl__PostS__43CE1DA2] DEFAULT ((0)),
[Recurring] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__exControl__Recur__44C241DB] DEFAULT ('N'),
[Selected] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__exControl__Selec__45B66614] DEFAULT ('N'),
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__exControl__Poste__46AA8A4D] DEFAULT ('N'),
[Creator] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__exControl__Perio__479EAE86] DEFAULT ((0)),
[EndDate] [datetime] NULL,
[Total] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__exControl__Total__4892D2BF] DEFAULT ((0)),
[AdvanceAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__exControl__Advan__4986F6F8] DEFAULT ((0)),
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedDate] [datetime] NULL,
[Diary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaryNo] [int] NOT NULL CONSTRAINT [DF__exControl__Diary__4A7B1B31] DEFAULT ((0)),
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[exControl] ADD CONSTRAINT [exControlPK] PRIMARY KEY NONCLUSTERED ([Batch]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
