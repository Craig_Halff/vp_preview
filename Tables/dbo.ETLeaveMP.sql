CREATE TABLE [dbo].[ETLeaveMP]
(
[Seq] [int] NOT NULL,
[LeavePlanName] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectTask] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaveDate] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaveHrs] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VisionTable] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ETLeaveMP] ADD CONSTRAINT [ETLeaveMPPK] PRIMARY KEY CLUSTERED ([Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
