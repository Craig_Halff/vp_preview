CREATE TABLE [dbo].[CCG_PAT_Pending]
(
[Seq] [int] NOT NULL IDENTITY(1, 1),
[PayableSeq] [int] NOT NULL,
[Stage] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Route] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NOT NULL,
[Parallel] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLastNotificationSent] [datetime] NULL,
[CreateDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_Pending] ADD CONSTRAINT [PK_CCG_PAT_Pending] PRIMARY KEY CLUSTERED ([Seq]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IxCustomPayseq] ON [dbo].[CCG_PAT_Pending] ([PayableSeq]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Pending approvals in the system', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Pending', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date and time when this record was created in PAT (UTC)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Pending', 'COLUMN', N'CreateDateTime'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Last time the employee was notified they have a pending approval (UTC)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Pending', 'COLUMN', N'DateLastNotificationSent'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Optional description', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Pending', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*FK - Vision employee ID', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Pending', 'COLUMN', N'Employee'
GO
EXEC sp_addextendedproperty N'MS_Description', N'If set this approval can happen in parallel to others with the same sort order', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Pending', 'COLUMN', N'Parallel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - CCG_PAT_Payable', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Pending', 'COLUMN', N'PayableSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - Route ID if record came from a predefined route', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Pending', 'COLUMN', N'Route'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Pending', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Order this approval relative to others for the same item', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Pending', 'COLUMN', N'SortOrder'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Last stage set', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Pending', 'COLUMN', N'Stage'
GO
