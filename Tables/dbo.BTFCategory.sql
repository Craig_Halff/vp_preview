CREATE TABLE [dbo].[BTFCategory]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__BTFCategory__Seq__664265E0] DEFAULT ((0)),
[Category] [smallint] NOT NULL CONSTRAINT [DF__BTFCatego__Categ__67368A19] DEFAULT ((0)),
[PctComplete] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BTFCatego__PctCo__682AAE52] DEFAULT ((0)),
[PctOfFee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BTFCatego__PctOf__691ED28B] DEFAULT ((0)),
[Fee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BTFCategory__Fee__6A12F6C4] DEFAULT ((0)),
[BilledJTD] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BTFCatego__Bille__6B071AFD] DEFAULT ((0)),
[FeeToDate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__BTFCatego__FeeTo__6BFB3F36] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTFCategory] ADD CONSTRAINT [BTFCategoryPK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [Seq], [Category]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
