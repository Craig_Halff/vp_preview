CREATE TABLE [dbo].[BankRecAssoc]
(
[CRPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankRecPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MatchingStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BankRecAs__Match__7FEAFD3E] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BankRecAssoc] ADD CONSTRAINT [BankRecAssocPK] PRIMARY KEY NONCLUSTERED ([CRPKey], [BankRecPKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
