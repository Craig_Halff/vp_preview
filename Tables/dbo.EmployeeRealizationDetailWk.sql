CREATE TABLE [dbo].[EmployeeRealizationDetailWk]
(
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OriginalTable] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OriginalPeriod] [int] NOT NULL CONSTRAINT [DF__EmployeeR__Origi__154838E3] DEFAULT ((0)),
[OriginalPostSeq] [int] NOT NULL CONSTRAINT [DF__EmployeeR__Origi__163C5D1C] DEFAULT ((0)),
[OriginalPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Hours] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EmployeeR__Hours__17308155] DEFAULT ((0)),
[CostAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EmployeeR__CostA__1824A58E] DEFAULT ((0)),
[BillingAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EmployeeR__Billi__1918C9C7] DEFAULT ((0)),
[UnitRealization] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EmployeeR__UnitR__1A0CEE00] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EmployeeRealizationDetailWk] ADD CONSTRAINT [EmployeeRealizationDetailWkPK] PRIMARY KEY NONCLUSTERED ([Invoice], [MainWBS1], [OriginalTable], [OriginalPeriod], [OriginalPostSeq], [OriginalPKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
