CREATE TABLE [dbo].[CFGClientCurrentStatusDescriptions]
(
[Code] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrentStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGClientCu__Seq__58494C44] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGClientCurrentStatusDescriptions] ADD CONSTRAINT [CFGClientCurrentStatusDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
