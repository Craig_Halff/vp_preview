CREATE TABLE [dbo].[RPReimbAllowance_Backup]
(
[TimePhaseID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PeriodCost] [decimal] (19, 4) NOT NULL,
[PeriodBill] [decimal] (19, 4) NOT NULL,
[PeriodExpCost] [decimal] (19, 4) NOT NULL,
[PeriodConCost] [decimal] (19, 4) NOT NULL,
[PeriodExpBill] [decimal] (19, 4) NOT NULL,
[PeriodConBill] [decimal] (19, 4) NOT NULL,
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
