CREATE TABLE [dbo].[WorkflowActionMethod]
(
[ActionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AssemblyName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClassName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MethodName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductMethod] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RunAfterSave] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__WorkflowA__RunAf__0B2B687A] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowActionMethod] ADD CONSTRAINT [WorkflowActionMethodPK] PRIMARY KEY NONCLUSTERED ([ActionID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
