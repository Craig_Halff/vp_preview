CREATE TABLE [dbo].[CFGAPDiscount]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApplyToTaxes] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAPDisc__Apply__7643C37F] DEFAULT ('N'),
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAPDiscount] ADD CONSTRAINT [CFGAPDiscountPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
