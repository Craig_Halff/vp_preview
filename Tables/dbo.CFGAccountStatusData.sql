CREATE TABLE [dbo].[CFGAccountStatusData]
(
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAccoun__Statu__11BF94B6] DEFAULT ('N'),
[WarningMsg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAccountStatusData] ADD CONSTRAINT [CFGAccountStatusDataPK] PRIMARY KEY CLUSTERED ([Status]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
