CREATE TABLE [dbo].[CFGInitializationSetup]
(
[InitType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitT__7D53E11D] DEFAULT ('PIM'),
[InitMaxBatch] [int] NOT NULL CONSTRAINT [DF__CFGInitia__InitM__7E480556] DEFAULT ((1)),
[InitWBS1sMaxBatch] [int] NOT NULL CONSTRAINT [DF__CFGInitia__InitW__7F3C298F] DEFAULT ((1)),
[InitWBS1s] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitW__00304DC8] DEFAULT ('N'),
[InitWBS1sActive] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitW__01247201] DEFAULT ('N'),
[InitWBS1sInactive] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitW__0218963A] DEFAULT ('N'),
[InitWBS1sDormant] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitW__030CBA73] DEFAULT ('N'),
[InitWBS1sValidStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitWBS1sValidDate] [datetime] NULL,
[InitWBS1sInitStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitWBS1sInitDate] [datetime] NULL,
[InitFirms] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitF__0400DEAC] DEFAULT ('N'),
[InitFirmsActive] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitF__04F502E5] DEFAULT ('N'),
[InitFirmsInactive] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitF__05E9271E] DEFAULT ('N'),
[InitFirmsDormant] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitF__06DD4B57] DEFAULT ('N'),
[InitFirmsValidStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitFirmsValidDate] [datetime] NULL,
[InitFirmsInitStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitFirmsInitDate] [datetime] NULL,
[InitContacts] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitC__07D16F90] DEFAULT ('N'),
[InitContactsActive] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitC__08C593C9] DEFAULT ('N'),
[InitContactsInactive] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitC__09B9B802] DEFAULT ('N'),
[InitContactsTerminated] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitC__0AADDC3B] DEFAULT ('N'),
[InitContactsValidStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitContactsValidDate] [datetime] NULL,
[InitContactsInitStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitContactsInitDate] [datetime] NULL,
[InitEmployees] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitE__0BA20074] DEFAULT ('N'),
[InitEmployeesActive] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitE__0C9624AD] DEFAULT ('N'),
[InitEmployeesInactive] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitE__0D8A48E6] DEFAULT ('N'),
[InitEmployeesTerminated] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitE__0E7E6D1F] DEFAULT ('N'),
[InitEmployeesValidStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitEmployeesValidDate] [datetime] NULL,
[InitEmployeesInitStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitEmployeesInitDate] [datetime] NULL,
[InitMktCampaigns] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitM__0F729158] DEFAULT ('N'),
[InitMktCampaignsActive] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitM__1066B591] DEFAULT ('N'),
[InitMktCampaignsInactive] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGInitia__InitM__115AD9CA] DEFAULT ('N'),
[InitMktCampaignsValidStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitMktCampaignsValidDate] [datetime] NULL,
[InitMktCampaignsInitStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitMktCampaignsInitDate] [datetime] NULL,
[InitRefreshPIMInitStatus] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitRefreshPIMInitDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGInitializationSetup] ADD CONSTRAINT [CFGInitializationSetupPK] PRIMARY KEY CLUSTERED ([InitType]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
