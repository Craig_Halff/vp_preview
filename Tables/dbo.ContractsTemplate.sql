CREATE TABLE [dbo].[ContractsTemplate]
(
[WBS1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContractNumber] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContractStatus] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeeIncludeInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Contracts__FeeIn__3AC3D655] DEFAULT ('N'),
[RequestDate] [datetime] NULL,
[ApprovedDate] [datetime] NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__Contracts__Perio__3BB7FA8E] DEFAULT ((0)),
[ContractType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ContractsTemplateID] [bigint] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_ContractsTemplate]
      ON [dbo].[ContractsTemplate]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ContractsTemplate'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractsTemplateID],121),'WBS1',CONVERT(NVARCHAR(2000),[WBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractsTemplateID],121),'ContractNumber',CONVERT(NVARCHAR(2000),[ContractNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractsTemplateID],121),'ContractStatus',CONVERT(NVARCHAR(2000),[ContractStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractsTemplateID],121),'FeeIncludeInd',CONVERT(NVARCHAR(2000),[FeeIncludeInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractsTemplateID],121),'RequestDate',CONVERT(NVARCHAR(2000),[RequestDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractsTemplateID],121),'ApprovedDate',CONVERT(NVARCHAR(2000),[ApprovedDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractsTemplateID],121),'Period',CONVERT(NVARCHAR(2000),[Period],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractsTemplateID],121),'ContractType',CONVERT(NVARCHAR(2000),[ContractType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractsTemplateID],121),'Notes','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ContractsTemplateID],121),'ContractsTemplateID',CONVERT(NVARCHAR(2000),[ContractsTemplateID],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_ContractsTemplate] ON [dbo].[ContractsTemplate]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_ContractsTemplate]
      ON [dbo].[ContractsTemplate]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ContractsTemplate'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'WBS1',NULL,CONVERT(NVARCHAR(2000),[WBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'ContractNumber',NULL,CONVERT(NVARCHAR(2000),[ContractNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'ContractStatus',NULL,CONVERT(NVARCHAR(2000),[ContractStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'FeeIncludeInd',NULL,CONVERT(NVARCHAR(2000),[FeeIncludeInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'RequestDate',NULL,CONVERT(NVARCHAR(2000),[RequestDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'ApprovedDate',NULL,CONVERT(NVARCHAR(2000),[ApprovedDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'Period',NULL,CONVERT(NVARCHAR(2000),[Period],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'ContractType',NULL,CONVERT(NVARCHAR(2000),[ContractType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'Notes',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'ContractsTemplateID',NULL,CONVERT(NVARCHAR(2000),[ContractsTemplateID],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_ContractsTemplate] ON [dbo].[ContractsTemplate]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_ContractsTemplate]
      ON [dbo].[ContractsTemplate]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ContractsTemplate'
    
      If UPDATE([WBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'WBS1',
      CONVERT(NVARCHAR(2000),DELETED.[WBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractsTemplateID] = DELETED.[ContractsTemplateID] AND 
		(
			(
				INSERTED.[WBS1] Is Null And
				DELETED.[WBS1] Is Not Null
			) Or
			(
				INSERTED.[WBS1] Is Not Null And
				DELETED.[WBS1] Is Null
			) Or
			(
				INSERTED.[WBS1] !=
				DELETED.[WBS1]
			)
		) 
		END		
		
      If UPDATE([ContractNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'ContractNumber',
      CONVERT(NVARCHAR(2000),DELETED.[ContractNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ContractNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractsTemplateID] = DELETED.[ContractsTemplateID] AND 
		(
			(
				INSERTED.[ContractNumber] Is Null And
				DELETED.[ContractNumber] Is Not Null
			) Or
			(
				INSERTED.[ContractNumber] Is Not Null And
				DELETED.[ContractNumber] Is Null
			) Or
			(
				INSERTED.[ContractNumber] !=
				DELETED.[ContractNumber]
			)
		) 
		END		
		
      If UPDATE([ContractStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'ContractStatus',
      CONVERT(NVARCHAR(2000),DELETED.[ContractStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ContractStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractsTemplateID] = DELETED.[ContractsTemplateID] AND 
		(
			(
				INSERTED.[ContractStatus] Is Null And
				DELETED.[ContractStatus] Is Not Null
			) Or
			(
				INSERTED.[ContractStatus] Is Not Null And
				DELETED.[ContractStatus] Is Null
			) Or
			(
				INSERTED.[ContractStatus] !=
				DELETED.[ContractStatus]
			)
		) 
		END		
		
      If UPDATE([FeeIncludeInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'FeeIncludeInd',
      CONVERT(NVARCHAR(2000),DELETED.[FeeIncludeInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FeeIncludeInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractsTemplateID] = DELETED.[ContractsTemplateID] AND 
		(
			(
				INSERTED.[FeeIncludeInd] Is Null And
				DELETED.[FeeIncludeInd] Is Not Null
			) Or
			(
				INSERTED.[FeeIncludeInd] Is Not Null And
				DELETED.[FeeIncludeInd] Is Null
			) Or
			(
				INSERTED.[FeeIncludeInd] !=
				DELETED.[FeeIncludeInd]
			)
		) 
		END		
		
      If UPDATE([RequestDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'RequestDate',
      CONVERT(NVARCHAR(2000),DELETED.[RequestDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RequestDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractsTemplateID] = DELETED.[ContractsTemplateID] AND 
		(
			(
				INSERTED.[RequestDate] Is Null And
				DELETED.[RequestDate] Is Not Null
			) Or
			(
				INSERTED.[RequestDate] Is Not Null And
				DELETED.[RequestDate] Is Null
			) Or
			(
				INSERTED.[RequestDate] !=
				DELETED.[RequestDate]
			)
		) 
		END		
		
      If UPDATE([ApprovedDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'ApprovedDate',
      CONVERT(NVARCHAR(2000),DELETED.[ApprovedDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ApprovedDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractsTemplateID] = DELETED.[ContractsTemplateID] AND 
		(
			(
				INSERTED.[ApprovedDate] Is Null And
				DELETED.[ApprovedDate] Is Not Null
			) Or
			(
				INSERTED.[ApprovedDate] Is Not Null And
				DELETED.[ApprovedDate] Is Null
			) Or
			(
				INSERTED.[ApprovedDate] !=
				DELETED.[ApprovedDate]
			)
		) 
		END		
		
      If UPDATE([Period])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'Period',
      CONVERT(NVARCHAR(2000),DELETED.[Period],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Period],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractsTemplateID] = DELETED.[ContractsTemplateID] AND 
		(
			(
				INSERTED.[Period] Is Null And
				DELETED.[Period] Is Not Null
			) Or
			(
				INSERTED.[Period] Is Not Null And
				DELETED.[Period] Is Null
			) Or
			(
				INSERTED.[Period] !=
				DELETED.[Period]
			)
		) 
		END		
		
      If UPDATE([ContractType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'ContractType',
      CONVERT(NVARCHAR(2000),DELETED.[ContractType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ContractType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractsTemplateID] = DELETED.[ContractsTemplateID] AND 
		(
			(
				INSERTED.[ContractType] Is Null And
				DELETED.[ContractType] Is Not Null
			) Or
			(
				INSERTED.[ContractType] Is Not Null And
				DELETED.[ContractType] Is Null
			) Or
			(
				INSERTED.[ContractType] !=
				DELETED.[ContractType]
			)
		) 
		END		
		
      If UPDATE([Notes])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'Notes',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([ContractsTemplateID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ContractsTemplateID],121),'ContractsTemplateID',
      CONVERT(NVARCHAR(2000),DELETED.[ContractsTemplateID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ContractsTemplateID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ContractsTemplateID] = DELETED.[ContractsTemplateID] AND 
		(
			(
				INSERTED.[ContractsTemplateID] Is Null And
				DELETED.[ContractsTemplateID] Is Not Null
			) Or
			(
				INSERTED.[ContractsTemplateID] Is Not Null And
				DELETED.[ContractsTemplateID] Is Null
			) Or
			(
				INSERTED.[ContractsTemplateID] !=
				DELETED.[ContractsTemplateID]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_ContractsTemplate] ON [dbo].[ContractsTemplate]
GO
ALTER TABLE [dbo].[ContractsTemplate] ADD CONSTRAINT [ContractsTemplatePK] PRIMARY KEY CLUSTERED ([ContractsTemplateID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [ContractsTemplateWBS1ContractNumberIDX] ON [dbo].[ContractsTemplate] ([WBS1], [ContractNumber]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
