CREATE TABLE [dbo].[CFGCitizenshipData]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGCitize__Statu__7FA5728C] DEFAULT ('Y')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGCitizenshipData] ADD CONSTRAINT [CFGCitizenshipDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
