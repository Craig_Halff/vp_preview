CREATE TABLE [dbo].[CFGClientStatusDescriptions]
(
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGClientStatusDescriptions__Status__DefA] DEFAULT ('A'),
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGClientStatusDescriptions] ADD CONSTRAINT [CFGClientStatusDescriptionsPK] PRIMARY KEY CLUSTERED ([Status], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
