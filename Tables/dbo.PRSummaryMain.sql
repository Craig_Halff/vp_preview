CREATE TABLE [dbo].[PRSummaryMain]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__PRSummary__Perio__7328F660] DEFAULT ((0)),
[ChargeType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Revenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Reven__741D1A99] DEFAULT ((0)),
[Billed] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Bille__75113ED2] DEFAULT ((0)),
[BilledLab] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Bille__7605630B] DEFAULT ((0)),
[BilledCons] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Bille__76F98744] DEFAULT ((0)),
[BilledExp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Bille__77EDAB7D] DEFAULT ((0)),
[BilledFee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Bille__78E1CFB6] DEFAULT ((0)),
[BilledUnit] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Bille__79D5F3EF] DEFAULT ((0)),
[BilledAddOn] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Bille__7ACA1828] DEFAULT ((0)),
[BilledTaxes] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Bille__7BBE3C61] DEFAULT ((0)),
[BilledInterest] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Bille__7CB2609A] DEFAULT ((0)),
[BilledOther] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Bille__7DA684D3] DEFAULT ((0)),
[AR] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummaryMai__AR__7E9AA90C] DEFAULT ((0)),
[Received] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Recei__7F8ECD45] DEFAULT ((0)),
[LaborCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Labor__0082F17E] DEFAULT ((0)),
[LaborBilling] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Labor__017715B7] DEFAULT ((0)),
[Unbilled] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Unbil__026B39F0] DEFAULT ((0)),
[SpentCostLessOH] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Spent__035F5E29] DEFAULT ((0)),
[SpentBilling] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Spent__04538262] DEFAULT ((0)),
[GrossMargin] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Gross__0547A69B] DEFAULT ((0)),
[NetRevenueCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__NetRe__063BCAD4] DEFAULT ((0)),
[NetRevenueBilling] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__NetRe__072FEF0D] DEFAULT ((0)),
[ProfitCostLessOH] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Profi__08241346] DEFAULT ((0)),
[ProfitBilling] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Profi__0918377F] DEFAULT ((0)),
[Overhead] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Overh__0A0C5BB8] DEFAULT ((0)),
[RevenueBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Reven__0B007FF1] DEFAULT ((0)),
[RevenueProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Reven__0BF4A42A] DEFAULT ((0)),
[ARProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__ARPro__0CE8C863] DEFAULT ((0)),
[ARBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__ARBil__0DDCEC9C] DEFAULT ((0)),
[ReceivedProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Recei__0ED110D5] DEFAULT ((0)),
[ReceivedBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Recei__0FC5350E] DEFAULT ((0)),
[LaborCostProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Labor__10B95947] DEFAULT ((0)),
[LaborCostBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Labor__11AD7D80] DEFAULT ((0)),
[SpentCostLessOHProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Spent__12A1A1B9] DEFAULT ((0)),
[SpentCostLessOHBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Spent__1395C5F2] DEFAULT ((0)),
[GrossMarginProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Gross__1489EA2B] DEFAULT ((0)),
[GrossMarginBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Gross__157E0E64] DEFAULT ((0)),
[NetRevenueCostProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__NetRe__1672329D] DEFAULT ((0)),
[NetRevenueCostBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__NetRe__176656D6] DEFAULT ((0)),
[ProfitCostLessOHProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Profi__185A7B0F] DEFAULT ((0)),
[ProfitCostLessOHBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Profi__194E9F48] DEFAULT ((0)),
[OverheadProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRSummary__Overh__1A42C381] DEFAULT ((0)),
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PRSummaryMain] ADD CONSTRAINT [PRSummaryMainPK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [Period]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
