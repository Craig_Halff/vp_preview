CREATE TABLE [dbo].[CA]
(
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CashBasisAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FASAccount] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Detail] [smallint] NOT NULL CONSTRAINT [DF__CA_New__Detail__22574FE7] DEFAULT ((1)),
[GlobalAccount] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CA_New__GlobalAc__234B7420] DEFAULT ('Y'),
[AccountCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [smallint] NOT NULL CONSTRAINT [DF__CA_New__Type__243F9859] DEFAULT ((0)),
[UnrealizedLossAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnrealizedGainAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CashBasisRevaluation] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CA_New__CashBasi__2533BC92] DEFAULT ('N'),
[QBOAccountID] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_CA]
      ON [dbo].[CA]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CA'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 )
begin

      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'Account',CONVERT(NVARCHAR(2000),[Account],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'Name',CONVERT(NVARCHAR(2000),[Name],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'Status',CONVERT(NVARCHAR(2000),DELETED.[Status],121),NULL, oldDesc.Label, NULL, @source,@app
        FROM DELETED left join CFGAccountStatus as oldDesc  on DELETED.Status = oldDesc.Status

      
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'Account',CONVERT(NVARCHAR(2000),[Account],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'Name',CONVERT(NVARCHAR(2000),[Name],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'CashBasisAccount',CONVERT(NVARCHAR(2000),[CashBasisAccount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'FASAccount',CONVERT(NVARCHAR(2000),[FASAccount],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'Status',CONVERT(NVARCHAR(2000),DELETED.[Status],121),NULL, oldDesc.Label, NULL, @source,@app
        FROM DELETED left join CFGAccountStatus as oldDesc  on DELETED.Status = oldDesc.Status

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'Detail',CONVERT(NVARCHAR(2000),[Detail],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'GlobalAccount',CONVERT(NVARCHAR(2000),[GlobalAccount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'AccountCurrencyCode',CONVERT(NVARCHAR(2000),[AccountCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'CustomCurrencyCode',CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'Type',CONVERT(NVARCHAR(2000),[Type],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'UnrealizedLossAccount',CONVERT(NVARCHAR(2000),[UnrealizedLossAccount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'UnrealizedGainAccount',CONVERT(NVARCHAR(2000),[UnrealizedGainAccount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'CashBasisRevaluation',CONVERT(NVARCHAR(2000),[CashBasisRevaluation],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Account],121),'QBOAccountID',CONVERT(NVARCHAR(2000),[QBOAccountID],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_CA] ON [dbo].[CA]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_CA]
      ON [dbo].[CA]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CA'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'Account',NULL,CONVERT(NVARCHAR(2000),[Account],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'Name',NULL,CONVERT(NVARCHAR(2000),[Name],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CashBasisAccount',NULL,CONVERT(NVARCHAR(2000),[CashBasisAccount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'FASAccount',NULL,CONVERT(NVARCHAR(2000),[FASAccount],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'Status',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Status],121), NULL, newDesc.Label, @source, @app
       FROM INSERTED left join  CFGAccountStatus as newDesc  on INSERTED.Status = newDesc.Status

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'Detail',NULL,CONVERT(NVARCHAR(2000),[Detail],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'GlobalAccount',NULL,CONVERT(NVARCHAR(2000),[GlobalAccount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'AccountCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[AccountCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CustomCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'Type',NULL,CONVERT(NVARCHAR(2000),[Type],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'UnrealizedLossAccount',NULL,CONVERT(NVARCHAR(2000),[UnrealizedLossAccount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'UnrealizedGainAccount',NULL,CONVERT(NVARCHAR(2000),[UnrealizedGainAccount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CashBasisRevaluation',NULL,CONVERT(NVARCHAR(2000),[CashBasisRevaluation],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'QBOAccountID',NULL,CONVERT(NVARCHAR(2000),[QBOAccountID],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_CA] ON [dbo].[CA]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_CA]
      ON [dbo].[CA]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'CA'
    
      If UPDATE([Account])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'Account',
      CONVERT(NVARCHAR(2000),DELETED.[Account],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Account],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[Account] Is Null And
				DELETED.[Account] Is Not Null
			) Or
			(
				INSERTED.[Account] Is Not Null And
				DELETED.[Account] Is Null
			) Or
			(
				INSERTED.[Account] !=
				DELETED.[Account]
			)
		) 
		END		
		
      If UPDATE([Name])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'Name',
      CONVERT(NVARCHAR(2000),DELETED.[Name],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Name],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[Name] Is Null And
				DELETED.[Name] Is Not Null
			) Or
			(
				INSERTED.[Name] Is Not Null And
				DELETED.[Name] Is Null
			) Or
			(
				INSERTED.[Name] !=
				DELETED.[Name]
			)
		) 
		END		
		
      If UPDATE([CashBasisAccount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CashBasisAccount',
      CONVERT(NVARCHAR(2000),DELETED.[CashBasisAccount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CashBasisAccount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[CashBasisAccount] Is Null And
				DELETED.[CashBasisAccount] Is Not Null
			) Or
			(
				INSERTED.[CashBasisAccount] Is Not Null And
				DELETED.[CashBasisAccount] Is Null
			) Or
			(
				INSERTED.[CashBasisAccount] !=
				DELETED.[CashBasisAccount]
			)
		) 
		END		
		
      If UPDATE([FASAccount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'FASAccount',
      CONVERT(NVARCHAR(2000),DELETED.[FASAccount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FASAccount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[FASAccount] Is Null And
				DELETED.[FASAccount] Is Not Null
			) Or
			(
				INSERTED.[FASAccount] Is Not Null And
				DELETED.[FASAccount] Is Null
			) Or
			(
				INSERTED.[FASAccount] !=
				DELETED.[FASAccount]
			)
		) 
		END		
		
     If UPDATE([Status])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'Status',
     CONVERT(NVARCHAR(2000),DELETED.[Status],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Status],121),
     oldDesc.Label, newDesc.Label, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[Status] Is Null And
				DELETED.[Status] Is Not Null
			) Or
			(
				INSERTED.[Status] Is Not Null And
				DELETED.[Status] Is Null
			) Or
			(
				INSERTED.[Status] !=
				DELETED.[Status]
			)
		) left join CFGAccountStatus as oldDesc  on DELETED.Status = oldDesc.Status  left join  CFGAccountStatus as newDesc  on INSERTED.Status = newDesc.Status
		END		
		
      If UPDATE([Detail])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'Detail',
      CONVERT(NVARCHAR(2000),DELETED.[Detail],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Detail],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[Detail] Is Null And
				DELETED.[Detail] Is Not Null
			) Or
			(
				INSERTED.[Detail] Is Not Null And
				DELETED.[Detail] Is Null
			) Or
			(
				INSERTED.[Detail] !=
				DELETED.[Detail]
			)
		) 
		END		
		
      If UPDATE([GlobalAccount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'GlobalAccount',
      CONVERT(NVARCHAR(2000),DELETED.[GlobalAccount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[GlobalAccount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[GlobalAccount] Is Null And
				DELETED.[GlobalAccount] Is Not Null
			) Or
			(
				INSERTED.[GlobalAccount] Is Not Null And
				DELETED.[GlobalAccount] Is Null
			) Or
			(
				INSERTED.[GlobalAccount] !=
				DELETED.[GlobalAccount]
			)
		) 
		END		
		
      If UPDATE([AccountCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'AccountCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[AccountCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AccountCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[AccountCurrencyCode] Is Null And
				DELETED.[AccountCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[AccountCurrencyCode] Is Not Null And
				DELETED.[AccountCurrencyCode] Is Null
			) Or
			(
				INSERTED.[AccountCurrencyCode] !=
				DELETED.[AccountCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([CustomCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CustomCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustomCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustomCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[CustomCurrencyCode] Is Null And
				DELETED.[CustomCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] Is Not Null And
				DELETED.[CustomCurrencyCode] Is Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] !=
				DELETED.[CustomCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([Type])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'Type',
      CONVERT(NVARCHAR(2000),DELETED.[Type],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Type],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[Type] Is Null And
				DELETED.[Type] Is Not Null
			) Or
			(
				INSERTED.[Type] Is Not Null And
				DELETED.[Type] Is Null
			) Or
			(
				INSERTED.[Type] !=
				DELETED.[Type]
			)
		) 
		END		
		
      If UPDATE([UnrealizedLossAccount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'UnrealizedLossAccount',
      CONVERT(NVARCHAR(2000),DELETED.[UnrealizedLossAccount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UnrealizedLossAccount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[UnrealizedLossAccount] Is Null And
				DELETED.[UnrealizedLossAccount] Is Not Null
			) Or
			(
				INSERTED.[UnrealizedLossAccount] Is Not Null And
				DELETED.[UnrealizedLossAccount] Is Null
			) Or
			(
				INSERTED.[UnrealizedLossAccount] !=
				DELETED.[UnrealizedLossAccount]
			)
		) 
		END		
		
      If UPDATE([UnrealizedGainAccount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'UnrealizedGainAccount',
      CONVERT(NVARCHAR(2000),DELETED.[UnrealizedGainAccount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UnrealizedGainAccount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[UnrealizedGainAccount] Is Null And
				DELETED.[UnrealizedGainAccount] Is Not Null
			) Or
			(
				INSERTED.[UnrealizedGainAccount] Is Not Null And
				DELETED.[UnrealizedGainAccount] Is Null
			) Or
			(
				INSERTED.[UnrealizedGainAccount] !=
				DELETED.[UnrealizedGainAccount]
			)
		) 
		END		
		
      If UPDATE([CashBasisRevaluation])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'CashBasisRevaluation',
      CONVERT(NVARCHAR(2000),DELETED.[CashBasisRevaluation],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CashBasisRevaluation],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[CashBasisRevaluation] Is Null And
				DELETED.[CashBasisRevaluation] Is Not Null
			) Or
			(
				INSERTED.[CashBasisRevaluation] Is Not Null And
				DELETED.[CashBasisRevaluation] Is Null
			) Or
			(
				INSERTED.[CashBasisRevaluation] !=
				DELETED.[CashBasisRevaluation]
			)
		) 
		END		
		
      If UPDATE([QBOAccountID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Account],121),'QBOAccountID',
      CONVERT(NVARCHAR(2000),DELETED.[QBOAccountID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[QBOAccountID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Account] = DELETED.[Account] AND 
		(
			(
				INSERTED.[QBOAccountID] Is Null And
				DELETED.[QBOAccountID] Is Not Null
			) Or
			(
				INSERTED.[QBOAccountID] Is Not Null And
				DELETED.[QBOAccountID] Is Null
			) Or
			(
				INSERTED.[QBOAccountID] !=
				DELETED.[QBOAccountID]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_CA] ON [dbo].[CA]
GO
ALTER TABLE [dbo].[CA] ADD CONSTRAINT [CAPK] PRIMARY KEY CLUSTERED ([Account]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CAAccountTypeIDX] ON [dbo].[CA] ([Account], [Type]) ON [PRIMARY]
GO
