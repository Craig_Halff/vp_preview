CREATE TABLE [dbo].[CFGEmployeeRoleData]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGEmploy__Defau__5FB563E2] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGEmployeeRoleData] ADD CONSTRAINT [CFGEmployeeRoleDataPK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
