CREATE TABLE [dbo].[CFGFASec179Main]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecordID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FYStart] [datetime] NOT NULL,
[FYEnd] [datetime] NOT NULL,
[Limit] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGFASec1__Limit__151D505A] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGFASec179Main] ADD CONSTRAINT [CFGFASec179MainPK] PRIMARY KEY CLUSTERED ([Company], [RecordID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
