CREATE TABLE [dbo].[EMPayrollDetail]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__EMPayroll__Perio__035E92D2] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__EMPayroll__PostS__0452B70B] DEFAULT ((0)),
[RegHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__RegHr__0546DB44] DEFAULT ((0)),
[RegHrsOverride] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__RegHr__063AFF7D] DEFAULT ('N'),
[OvtHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__OvtHr__072F23B6] DEFAULT ((0)),
[OvtHrsOverride] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__OvtHr__082347EF] DEFAULT ('N'),
[RegPay] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__RegPa__09176C28] DEFAULT ((0)),
[OvtPay] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__OvtPa__0A0B9061] DEFAULT ((0)),
[OtherPay] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__0AFFB49A] DEFAULT ((0)),
[OtherPayOverride] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Other__0BF3D8D3] DEFAULT ('N'),
[PayRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__PayRa__0CE7FD0C] DEFAULT ((0)),
[PayType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayOvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__PayOv__0DDC2145] DEFAULT ((0)),
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpecialOvtHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Speci__0ED0457E] DEFAULT ((0)),
[SpecialOvtHrsOverride] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Speci__0FC469B7] DEFAULT ('N'),
[SpecialOvtPay] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Speci__10B88DF0] DEFAULT ((0)),
[PaySpecialOvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__PaySp__11ACB229] DEFAULT ((0)),
[OtherPay2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__12A0D662] DEFAULT ((0)),
[OtherPay2Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Other__1394FA9B] DEFAULT ('N'),
[OtherPay3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__14891ED4] DEFAULT ((0)),
[OtherPay3Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Other__157D430D] DEFAULT ('N'),
[OtherPay4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__16716746] DEFAULT ((0)),
[OtherPay4Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Other__17658B7F] DEFAULT ('N'),
[OtherPay5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Other__1859AFB8] DEFAULT ((0)),
[OtherPay5Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMPayroll__Other__194DD3F1] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EMPayrollDetail] ADD CONSTRAINT [EMPayrollDetailPK] PRIMARY KEY NONCLUSTERED ([Employee], [Period], [PostSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
