CREATE TABLE [dbo].[_BeshDisbursements]
(
[Voucher] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Check] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [int] NULL
) ON [PRIMARY]
GO
