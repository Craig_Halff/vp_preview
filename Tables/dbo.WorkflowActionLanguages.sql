CREATE TABLE [dbo].[WorkflowActionLanguages]
(
[ActionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Language] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowActionLanguages] ADD CONSTRAINT [WorkflowActionLanguagesPK] PRIMARY KEY NONCLUSTERED ([ActionID], [Language]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
