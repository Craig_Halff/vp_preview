CREATE TABLE [dbo].[SF255EmployeeGraphics]
(
[SF255ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LinkID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilePath] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderOfAppearance] [smallint] NOT NULL CONSTRAINT [DF__SF255Empl__Order__22E2F67A] DEFAULT ((0)),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF255EmployeeGraphics] ADD CONSTRAINT [SF255EmployeeGraphicsPK] PRIMARY KEY NONCLUSTERED ([SF255ID], [Employee], [LinkID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
