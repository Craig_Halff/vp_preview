CREATE TABLE [dbo].[PYChecksA]
(
[Period] [int] NOT NULL CONSTRAINT [DF__PYChecksA__Perio__09A24764] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__PYChecksA__PostS__0A966B9D] DEFAULT ((0)),
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Earned] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksA__Earne__0B8A8FD6] DEFAULT ((0)),
[Override] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PYChecksA__Overr__0C7EB40F] DEFAULT ('N'),
[YTDEarned] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksA__YTDEa__0D72D848] DEFAULT ((0)),
[Taken] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksA__Taken__0E66FC81] DEFAULT ((0)),
[YTDTaken] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksA__YTDTa__0F5B20BA] DEFAULT ((0)),
[Balance] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksA__Balan__104F44F3] DEFAULT ((0)),
[HoursToAccrue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksA__Hours__1143692C] DEFAULT ((0)),
[HoursWorked] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PYChecksA__Hours__12378D65] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PYChecksA] ADD CONSTRAINT [PYChecksAPK] PRIMARY KEY NONCLUSTERED ([Period], [PostSeq], [Employee], [Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
