CREATE TABLE [dbo].[CFGAccrualScheduleMasterData]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ScheduleID] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BasedOnHoursWorked] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGAccrua__Based__6AD210D3] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGAccrualScheduleMasterData] ADD CONSTRAINT [CFGAccrualScheduleMasterDataPK] PRIMARY KEY CLUSTERED ([Company], [ScheduleID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
