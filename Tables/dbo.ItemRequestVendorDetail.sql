CREATE TABLE [dbo].[ItemRequestVendorDetail]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IRDetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Line] [int] NOT NULL CONSTRAINT [DF__ItemReques__Line__57C007B6] DEFAULT ((0)),
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Address] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VendorItem] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuotePrice] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__ItemReque__Quote__58B42BEF] DEFAULT ((0)),
[UOM] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuoteDate] [datetime] NULL,
[QuotePerson] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EndDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ItemRequestVendorDetail] ADD CONSTRAINT [ItemRequestVendorDetailPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [IRDetailPKey], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
