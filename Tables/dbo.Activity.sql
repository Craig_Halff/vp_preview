CREATE TABLE [dbo].[Activity]
(
[ActivityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Subject] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[StartTime] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EndDate] [datetime] NULL,
[EndTime] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Duration] [int] NOT NULL CONSTRAINT [DF__Activity___Durat__7AC8817A] DEFAULT ((0)),
[Location] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReminderInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity___Remin__7BBCA5B3] DEFAULT ('Y'),
[ReminderUnit] [smallint] NOT NULL CONSTRAINT [DF__Activity___Remin__7CB0C9EC] DEFAULT ((15)),
[ReminderMinHrDay] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReminderDate] [datetime] NULL,
[ReminderTime] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Priority] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShowTimeAs] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllDayEventInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity___AllDa__7DA4EE25] DEFAULT ('N'),
[CompletionInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity___Compl__7E99125E] DEFAULT ('N'),
[RecurrenceInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity___Recur__7F8D3697] DEFAULT ('N'),
[PrivateInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity___Priva__00815AD0] DEFAULT ('N'),
[TaskStatus] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaskCompletionDate] [datetime] NULL,
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampaignCode] [nvarchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecurrType] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecurrDailyFreq] [smallint] NOT NULL CONSTRAINT [DF__Activity___Recur__01757F09] DEFAULT ((0)),
[RecurrDailyWeekDay] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecurrWeeklyFreq] [smallint] NOT NULL CONSTRAINT [DF__Activity___Recur__0269A342] DEFAULT ((0)),
[RecurrWeeklySun] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity___Recur__035DC77B] DEFAULT ('N'),
[RecurrWeeklyMon] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity___Recur__0451EBB4] DEFAULT ('N'),
[RecurrWeeklyTue] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity___Recur__05460FED] DEFAULT ('N'),
[RecurrWeeklyWed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity___Recur__063A3426] DEFAULT ('N'),
[RecurrWeeklyThu] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity___Recur__072E585F] DEFAULT ('N'),
[RecurrWeeklyFri] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity___Recur__08227C98] DEFAULT ('N'),
[RecurrWeeklySat] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity___Recur__0916A0D1] DEFAULT ('N'),
[RecurrMonthlyFreq] [smallint] NOT NULL CONSTRAINT [DF__Activity___Recur__0A0AC50A] DEFAULT ((0)),
[RecurrMonthlyDay] [smallint] NOT NULL CONSTRAINT [DF__Activity___Recur__0AFEE943] DEFAULT ((0)),
[RecurrYearlyMonth] [smallint] NOT NULL CONSTRAINT [DF__Activity___Recur__0BF30D7C] DEFAULT ((0)),
[RecurrYearlyDay] [smallint] NOT NULL CONSTRAINT [DF__Activity___Recur__0CE731B5] DEFAULT ((0)),
[RecurrStartDate] [datetime] NULL,
[RecurrEndType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecurrEndDate] [datetime] NULL,
[RP] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity_New__RP__0DDB55EE] DEFAULT ('N'),
[EmailAlertSent] [datetime] NULL,
[RecurrID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampaignID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecurrMonthlyOccur] [smallint] NOT NULL CONSTRAINT [DF__Activity___Recur__0ECF7A27] DEFAULT ((0)),
[RecurrMonthlyOccurFreq] [smallint] NOT NULL CONSTRAINT [DF__Activity___Recur__0FC39E60] DEFAULT ((0)),
[RecurrMonthlyOccurDay] [smallint] NOT NULL CONSTRAINT [DF__Activity___Recur__10B7C299] DEFAULT ((0)),
[RecurrMonthlyOccurInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity___Recur__11ABE6D2] DEFAULT ('N'),
[AssignmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConversationID] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecurrYearlyOccurInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity___Recur__12A00B0B] DEFAULT ('N'),
[RecurrYearlyOccur] [smallint] NOT NULL CONSTRAINT [DF__Activity___Recur__13942F44] DEFAULT ((0)),
[RecurrYearlyOccurDay] [smallint] NOT NULL CONSTRAINT [DF__Activity___Recur__1488537D] DEFAULT ((0)),
[RecurrYearlyOccurMonth] [smallint] NOT NULL CONSTRAINT [DF__Activity___Recur__157C77B6] DEFAULT ((0)),
[MaxOccurences] [smallint] NOT NULL CONSTRAINT [DF__Activity___MaxOc__16709BEF] DEFAULT ((0)),
[PIMInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity___PIMIn__1764C028] DEFAULT ('N'),
[KonaTask] [int] NOT NULL CONSTRAINT [DF__Activity___KonaT__1858E461] DEFAULT ((0)),
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactIDForVendor] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[OccurrenceDate] [datetime] NULL,
[TimeZoneOffset] [bigint] NOT NULL CONSTRAINT [DF__Activity__TimeZo__54F0EA08] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_Activity]
      ON [dbo].[Activity]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Activity'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'ActivityID',CONVERT(NVARCHAR(2000),[ActivityID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'ClientID',CONVERT(NVARCHAR(2000),[ClientID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'ContactID',CONVERT(NVARCHAR(2000),[ContactID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'WBS1',CONVERT(NVARCHAR(2000),[WBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'WBS2',CONVERT(NVARCHAR(2000),[WBS2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'WBS3',CONVERT(NVARCHAR(2000),[WBS3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'Employee',CONVERT(NVARCHAR(2000),[Employee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'Type',CONVERT(NVARCHAR(2000),[Type],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'Subject',CONVERT(NVARCHAR(2000),[Subject],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'StartDate',CONVERT(NVARCHAR(2000),[StartDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'StartTime',CONVERT(NVARCHAR(2000),[StartTime],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'EndDate',CONVERT(NVARCHAR(2000),[EndDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'EndTime',CONVERT(NVARCHAR(2000),[EndTime],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'Duration',CONVERT(NVARCHAR(2000),[Duration],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'Location','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'ReminderInd',CONVERT(NVARCHAR(2000),[ReminderInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'ReminderUnit',CONVERT(NVARCHAR(2000),[ReminderUnit],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'ReminderMinHrDay',CONVERT(NVARCHAR(2000),[ReminderMinHrDay],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'ReminderDate',CONVERT(NVARCHAR(2000),[ReminderDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'ReminderTime',CONVERT(NVARCHAR(2000),[ReminderTime],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'Priority',CONVERT(NVARCHAR(2000),[Priority],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'Notes','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'ShowTimeAs',CONVERT(NVARCHAR(2000),[ShowTimeAs],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'AllDayEventInd',CONVERT(NVARCHAR(2000),[AllDayEventInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'CompletionInd',CONVERT(NVARCHAR(2000),[CompletionInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrenceInd',CONVERT(NVARCHAR(2000),[RecurrenceInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'PrivateInd',CONVERT(NVARCHAR(2000),[PrivateInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'TaskStatus',CONVERT(NVARCHAR(2000),[TaskStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'TaskCompletionDate',CONVERT(NVARCHAR(2000),[TaskCompletionDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'OpportunityID',CONVERT(NVARCHAR(2000),[OpportunityID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'CampaignCode',CONVERT(NVARCHAR(2000),[CampaignCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrType',CONVERT(NVARCHAR(2000),[RecurrType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrDailyFreq',CONVERT(NVARCHAR(2000),[RecurrDailyFreq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrDailyWeekDay',CONVERT(NVARCHAR(2000),[RecurrDailyWeekDay],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrWeeklyFreq',CONVERT(NVARCHAR(2000),[RecurrWeeklyFreq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrWeeklySun',CONVERT(NVARCHAR(2000),[RecurrWeeklySun],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrWeeklyMon',CONVERT(NVARCHAR(2000),[RecurrWeeklyMon],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrWeeklyTue',CONVERT(NVARCHAR(2000),[RecurrWeeklyTue],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrWeeklyWed',CONVERT(NVARCHAR(2000),[RecurrWeeklyWed],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrWeeklyThu',CONVERT(NVARCHAR(2000),[RecurrWeeklyThu],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrWeeklyFri',CONVERT(NVARCHAR(2000),[RecurrWeeklyFri],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrWeeklySat',CONVERT(NVARCHAR(2000),[RecurrWeeklySat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrMonthlyFreq',CONVERT(NVARCHAR(2000),[RecurrMonthlyFreq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrMonthlyDay',CONVERT(NVARCHAR(2000),[RecurrMonthlyDay],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrYearlyMonth',CONVERT(NVARCHAR(2000),[RecurrYearlyMonth],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrYearlyDay',CONVERT(NVARCHAR(2000),[RecurrYearlyDay],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrStartDate',CONVERT(NVARCHAR(2000),[RecurrStartDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrEndType',CONVERT(NVARCHAR(2000),[RecurrEndType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrEndDate',CONVERT(NVARCHAR(2000),[RecurrEndDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RP',CONVERT(NVARCHAR(2000),[RP],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'EmailAlertSent',CONVERT(NVARCHAR(2000),[EmailAlertSent],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrID',CONVERT(NVARCHAR(2000),[RecurrID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'CampaignID',CONVERT(NVARCHAR(2000),[CampaignID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'LeadID',CONVERT(NVARCHAR(2000),[LeadID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrMonthlyOccur',CONVERT(NVARCHAR(2000),[RecurrMonthlyOccur],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrMonthlyOccurFreq',CONVERT(NVARCHAR(2000),[RecurrMonthlyOccurFreq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrMonthlyOccurDay',CONVERT(NVARCHAR(2000),[RecurrMonthlyOccurDay],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrMonthlyOccurInd',CONVERT(NVARCHAR(2000),[RecurrMonthlyOccurInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'AssignmentID',CONVERT(NVARCHAR(2000),[AssignmentID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'ConversationID','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrYearlyOccurInd',CONVERT(NVARCHAR(2000),[RecurrYearlyOccurInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrYearlyOccur',CONVERT(NVARCHAR(2000),[RecurrYearlyOccur],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrYearlyOccurDay',CONVERT(NVARCHAR(2000),[RecurrYearlyOccurDay],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'RecurrYearlyOccurMonth',CONVERT(NVARCHAR(2000),[RecurrYearlyOccurMonth],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'MaxOccurences',CONVERT(NVARCHAR(2000),[MaxOccurences],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'PIMInd',CONVERT(NVARCHAR(2000),[PIMInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'KonaTask',CONVERT(NVARCHAR(2000),[KonaTask],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'Vendor',CONVERT(NVARCHAR(2000),[Vendor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'ContactIDForVendor',CONVERT(NVARCHAR(2000),[ContactIDForVendor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'OccurrenceDate',CONVERT(NVARCHAR(2000),[OccurrenceDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ActivityID],121),'TimeZoneOffset',CONVERT(NVARCHAR(2000),[TimeZoneOffset],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_Activity] ON [dbo].[Activity]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_Activity]
      ON [dbo].[Activity]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Activity'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ActivityID',NULL,CONVERT(NVARCHAR(2000),[ActivityID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ClientID',NULL,CONVERT(NVARCHAR(2000),[ClientID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ContactID',NULL,CONVERT(NVARCHAR(2000),[ContactID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'WBS1',NULL,CONVERT(NVARCHAR(2000),[WBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'WBS2',NULL,CONVERT(NVARCHAR(2000),[WBS2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'WBS3',NULL,CONVERT(NVARCHAR(2000),[WBS3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'Employee',NULL,CONVERT(NVARCHAR(2000),[Employee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'Type',NULL,CONVERT(NVARCHAR(2000),[Type],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'Subject',NULL,CONVERT(NVARCHAR(2000),[Subject],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'StartDate',NULL,CONVERT(NVARCHAR(2000),[StartDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'StartTime',NULL,CONVERT(NVARCHAR(2000),[StartTime],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'EndDate',NULL,CONVERT(NVARCHAR(2000),[EndDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'EndTime',NULL,CONVERT(NVARCHAR(2000),[EndTime],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'Duration',NULL,CONVERT(NVARCHAR(2000),[Duration],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'Location',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ReminderInd',NULL,CONVERT(NVARCHAR(2000),[ReminderInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ReminderUnit',NULL,CONVERT(NVARCHAR(2000),[ReminderUnit],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ReminderMinHrDay',NULL,CONVERT(NVARCHAR(2000),[ReminderMinHrDay],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ReminderDate',NULL,CONVERT(NVARCHAR(2000),[ReminderDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ReminderTime',NULL,CONVERT(NVARCHAR(2000),[ReminderTime],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'Priority',NULL,CONVERT(NVARCHAR(2000),[Priority],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'Notes',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ShowTimeAs',NULL,CONVERT(NVARCHAR(2000),[ShowTimeAs],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'AllDayEventInd',NULL,CONVERT(NVARCHAR(2000),[AllDayEventInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'CompletionInd',NULL,CONVERT(NVARCHAR(2000),[CompletionInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrenceInd',NULL,CONVERT(NVARCHAR(2000),[RecurrenceInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'PrivateInd',NULL,CONVERT(NVARCHAR(2000),[PrivateInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'TaskStatus',NULL,CONVERT(NVARCHAR(2000),[TaskStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'TaskCompletionDate',NULL,CONVERT(NVARCHAR(2000),[TaskCompletionDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'OpportunityID',NULL,CONVERT(NVARCHAR(2000),[OpportunityID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'CampaignCode',NULL,CONVERT(NVARCHAR(2000),[CampaignCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrType',NULL,CONVERT(NVARCHAR(2000),[RecurrType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrDailyFreq',NULL,CONVERT(NVARCHAR(2000),[RecurrDailyFreq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrDailyWeekDay',NULL,CONVERT(NVARCHAR(2000),[RecurrDailyWeekDay],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrWeeklyFreq',NULL,CONVERT(NVARCHAR(2000),[RecurrWeeklyFreq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrWeeklySun',NULL,CONVERT(NVARCHAR(2000),[RecurrWeeklySun],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrWeeklyMon',NULL,CONVERT(NVARCHAR(2000),[RecurrWeeklyMon],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrWeeklyTue',NULL,CONVERT(NVARCHAR(2000),[RecurrWeeklyTue],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrWeeklyWed',NULL,CONVERT(NVARCHAR(2000),[RecurrWeeklyWed],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrWeeklyThu',NULL,CONVERT(NVARCHAR(2000),[RecurrWeeklyThu],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrWeeklyFri',NULL,CONVERT(NVARCHAR(2000),[RecurrWeeklyFri],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrWeeklySat',NULL,CONVERT(NVARCHAR(2000),[RecurrWeeklySat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrMonthlyFreq',NULL,CONVERT(NVARCHAR(2000),[RecurrMonthlyFreq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrMonthlyDay',NULL,CONVERT(NVARCHAR(2000),[RecurrMonthlyDay],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrYearlyMonth',NULL,CONVERT(NVARCHAR(2000),[RecurrYearlyMonth],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrYearlyDay',NULL,CONVERT(NVARCHAR(2000),[RecurrYearlyDay],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrStartDate',NULL,CONVERT(NVARCHAR(2000),[RecurrStartDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrEndType',NULL,CONVERT(NVARCHAR(2000),[RecurrEndType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrEndDate',NULL,CONVERT(NVARCHAR(2000),[RecurrEndDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RP',NULL,CONVERT(NVARCHAR(2000),[RP],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'EmailAlertSent',NULL,CONVERT(NVARCHAR(2000),[EmailAlertSent],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrID',NULL,CONVERT(NVARCHAR(2000),[RecurrID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'CampaignID',NULL,CONVERT(NVARCHAR(2000),[CampaignID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'LeadID',NULL,CONVERT(NVARCHAR(2000),[LeadID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrMonthlyOccur',NULL,CONVERT(NVARCHAR(2000),[RecurrMonthlyOccur],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrMonthlyOccurFreq',NULL,CONVERT(NVARCHAR(2000),[RecurrMonthlyOccurFreq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrMonthlyOccurDay',NULL,CONVERT(NVARCHAR(2000),[RecurrMonthlyOccurDay],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrMonthlyOccurInd',NULL,CONVERT(NVARCHAR(2000),[RecurrMonthlyOccurInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'AssignmentID',NULL,CONVERT(NVARCHAR(2000),[AssignmentID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ConversationID',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrYearlyOccurInd',NULL,CONVERT(NVARCHAR(2000),[RecurrYearlyOccurInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrYearlyOccur',NULL,CONVERT(NVARCHAR(2000),[RecurrYearlyOccur],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrYearlyOccurDay',NULL,CONVERT(NVARCHAR(2000),[RecurrYearlyOccurDay],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrYearlyOccurMonth',NULL,CONVERT(NVARCHAR(2000),[RecurrYearlyOccurMonth],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'MaxOccurences',NULL,CONVERT(NVARCHAR(2000),[MaxOccurences],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'PIMInd',NULL,CONVERT(NVARCHAR(2000),[PIMInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'KonaTask',NULL,CONVERT(NVARCHAR(2000),[KonaTask],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'Vendor',NULL,CONVERT(NVARCHAR(2000),[Vendor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ContactIDForVendor',NULL,CONVERT(NVARCHAR(2000),[ContactIDForVendor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'OccurrenceDate',NULL,CONVERT(NVARCHAR(2000),[OccurrenceDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'TimeZoneOffset',NULL,CONVERT(NVARCHAR(2000),[TimeZoneOffset],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_Activity] ON [dbo].[Activity]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_Activity]
      ON [dbo].[Activity]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Activity'
    
      If UPDATE([ActivityID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ActivityID',
      CONVERT(NVARCHAR(2000),DELETED.[ActivityID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ActivityID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[ActivityID] Is Null And
				DELETED.[ActivityID] Is Not Null
			) Or
			(
				INSERTED.[ActivityID] Is Not Null And
				DELETED.[ActivityID] Is Null
			) Or
			(
				INSERTED.[ActivityID] !=
				DELETED.[ActivityID]
			)
		) 
		END		
		
      If UPDATE([ClientID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ClientID',
      CONVERT(NVARCHAR(2000),DELETED.[ClientID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClientID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[ClientID] Is Null And
				DELETED.[ClientID] Is Not Null
			) Or
			(
				INSERTED.[ClientID] Is Not Null And
				DELETED.[ClientID] Is Null
			) Or
			(
				INSERTED.[ClientID] !=
				DELETED.[ClientID]
			)
		) 
		END		
		
      If UPDATE([ContactID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ContactID',
      CONVERT(NVARCHAR(2000),DELETED.[ContactID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ContactID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[ContactID] Is Null And
				DELETED.[ContactID] Is Not Null
			) Or
			(
				INSERTED.[ContactID] Is Not Null And
				DELETED.[ContactID] Is Null
			) Or
			(
				INSERTED.[ContactID] !=
				DELETED.[ContactID]
			)
		) 
		END		
		
      If UPDATE([WBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'WBS1',
      CONVERT(NVARCHAR(2000),DELETED.[WBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[WBS1] Is Null And
				DELETED.[WBS1] Is Not Null
			) Or
			(
				INSERTED.[WBS1] Is Not Null And
				DELETED.[WBS1] Is Null
			) Or
			(
				INSERTED.[WBS1] !=
				DELETED.[WBS1]
			)
		) 
		END		
		
      If UPDATE([WBS2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'WBS2',
      CONVERT(NVARCHAR(2000),DELETED.[WBS2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[WBS2] Is Null And
				DELETED.[WBS2] Is Not Null
			) Or
			(
				INSERTED.[WBS2] Is Not Null And
				DELETED.[WBS2] Is Null
			) Or
			(
				INSERTED.[WBS2] !=
				DELETED.[WBS2]
			)
		) 
		END		
		
      If UPDATE([WBS3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'WBS3',
      CONVERT(NVARCHAR(2000),DELETED.[WBS3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WBS3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[WBS3] Is Null And
				DELETED.[WBS3] Is Not Null
			) Or
			(
				INSERTED.[WBS3] Is Not Null And
				DELETED.[WBS3] Is Null
			) Or
			(
				INSERTED.[WBS3] !=
				DELETED.[WBS3]
			)
		) 
		END		
		
      If UPDATE([Employee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'Employee',
      CONVERT(NVARCHAR(2000),DELETED.[Employee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Employee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[Employee] Is Null And
				DELETED.[Employee] Is Not Null
			) Or
			(
				INSERTED.[Employee] Is Not Null And
				DELETED.[Employee] Is Null
			) Or
			(
				INSERTED.[Employee] !=
				DELETED.[Employee]
			)
		) 
		END		
		
      If UPDATE([Type])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'Type',
      CONVERT(NVARCHAR(2000),DELETED.[Type],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Type],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[Type] Is Null And
				DELETED.[Type] Is Not Null
			) Or
			(
				INSERTED.[Type] Is Not Null And
				DELETED.[Type] Is Null
			) Or
			(
				INSERTED.[Type] !=
				DELETED.[Type]
			)
		) 
		END		
		
      If UPDATE([Subject])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'Subject',
      CONVERT(NVARCHAR(2000),DELETED.[Subject],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Subject],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[Subject] Is Null And
				DELETED.[Subject] Is Not Null
			) Or
			(
				INSERTED.[Subject] Is Not Null And
				DELETED.[Subject] Is Null
			) Or
			(
				INSERTED.[Subject] !=
				DELETED.[Subject]
			)
		) 
		END		
		
      If UPDATE([StartDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'StartDate',
      CONVERT(NVARCHAR(2000),DELETED.[StartDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[StartDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[StartDate] Is Null And
				DELETED.[StartDate] Is Not Null
			) Or
			(
				INSERTED.[StartDate] Is Not Null And
				DELETED.[StartDate] Is Null
			) Or
			(
				INSERTED.[StartDate] !=
				DELETED.[StartDate]
			)
		) 
		END		
		
      If UPDATE([StartTime])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'StartTime',
      CONVERT(NVARCHAR(2000),DELETED.[StartTime],121),
      CONVERT(NVARCHAR(2000),INSERTED.[StartTime],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[StartTime] Is Null And
				DELETED.[StartTime] Is Not Null
			) Or
			(
				INSERTED.[StartTime] Is Not Null And
				DELETED.[StartTime] Is Null
			) Or
			(
				INSERTED.[StartTime] !=
				DELETED.[StartTime]
			)
		) 
		END		
		
      If UPDATE([EndDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'EndDate',
      CONVERT(NVARCHAR(2000),DELETED.[EndDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EndDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[EndDate] Is Null And
				DELETED.[EndDate] Is Not Null
			) Or
			(
				INSERTED.[EndDate] Is Not Null And
				DELETED.[EndDate] Is Null
			) Or
			(
				INSERTED.[EndDate] !=
				DELETED.[EndDate]
			)
		) 
		END		
		
      If UPDATE([EndTime])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'EndTime',
      CONVERT(NVARCHAR(2000),DELETED.[EndTime],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EndTime],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[EndTime] Is Null And
				DELETED.[EndTime] Is Not Null
			) Or
			(
				INSERTED.[EndTime] Is Not Null And
				DELETED.[EndTime] Is Null
			) Or
			(
				INSERTED.[EndTime] !=
				DELETED.[EndTime]
			)
		) 
		END		
		
      If UPDATE([Duration])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'Duration',
      CONVERT(NVARCHAR(2000),DELETED.[Duration],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Duration],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[Duration] Is Null And
				DELETED.[Duration] Is Not Null
			) Or
			(
				INSERTED.[Duration] Is Not Null And
				DELETED.[Duration] Is Null
			) Or
			(
				INSERTED.[Duration] !=
				DELETED.[Duration]
			)
		) 
		END		
		
      If UPDATE([Location])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'Location',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([ReminderInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ReminderInd',
      CONVERT(NVARCHAR(2000),DELETED.[ReminderInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReminderInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[ReminderInd] Is Null And
				DELETED.[ReminderInd] Is Not Null
			) Or
			(
				INSERTED.[ReminderInd] Is Not Null And
				DELETED.[ReminderInd] Is Null
			) Or
			(
				INSERTED.[ReminderInd] !=
				DELETED.[ReminderInd]
			)
		) 
		END		
		
      If UPDATE([ReminderUnit])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ReminderUnit',
      CONVERT(NVARCHAR(2000),DELETED.[ReminderUnit],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReminderUnit],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[ReminderUnit] Is Null And
				DELETED.[ReminderUnit] Is Not Null
			) Or
			(
				INSERTED.[ReminderUnit] Is Not Null And
				DELETED.[ReminderUnit] Is Null
			) Or
			(
				INSERTED.[ReminderUnit] !=
				DELETED.[ReminderUnit]
			)
		) 
		END		
		
      If UPDATE([ReminderMinHrDay])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ReminderMinHrDay',
      CONVERT(NVARCHAR(2000),DELETED.[ReminderMinHrDay],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReminderMinHrDay],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[ReminderMinHrDay] Is Null And
				DELETED.[ReminderMinHrDay] Is Not Null
			) Or
			(
				INSERTED.[ReminderMinHrDay] Is Not Null And
				DELETED.[ReminderMinHrDay] Is Null
			) Or
			(
				INSERTED.[ReminderMinHrDay] !=
				DELETED.[ReminderMinHrDay]
			)
		) 
		END		
		
      If UPDATE([ReminderDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ReminderDate',
      CONVERT(NVARCHAR(2000),DELETED.[ReminderDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReminderDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[ReminderDate] Is Null And
				DELETED.[ReminderDate] Is Not Null
			) Or
			(
				INSERTED.[ReminderDate] Is Not Null And
				DELETED.[ReminderDate] Is Null
			) Or
			(
				INSERTED.[ReminderDate] !=
				DELETED.[ReminderDate]
			)
		) 
		END		
		
      If UPDATE([ReminderTime])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ReminderTime',
      CONVERT(NVARCHAR(2000),DELETED.[ReminderTime],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReminderTime],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[ReminderTime] Is Null And
				DELETED.[ReminderTime] Is Not Null
			) Or
			(
				INSERTED.[ReminderTime] Is Not Null And
				DELETED.[ReminderTime] Is Null
			) Or
			(
				INSERTED.[ReminderTime] !=
				DELETED.[ReminderTime]
			)
		) 
		END		
		
      If UPDATE([Priority])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'Priority',
      CONVERT(NVARCHAR(2000),DELETED.[Priority],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Priority],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[Priority] Is Null And
				DELETED.[Priority] Is Not Null
			) Or
			(
				INSERTED.[Priority] Is Not Null And
				DELETED.[Priority] Is Null
			) Or
			(
				INSERTED.[Priority] !=
				DELETED.[Priority]
			)
		) 
		END		
		
      If UPDATE([Notes])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'Notes',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([ShowTimeAs])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ShowTimeAs',
      CONVERT(NVARCHAR(2000),DELETED.[ShowTimeAs],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ShowTimeAs],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[ShowTimeAs] Is Null And
				DELETED.[ShowTimeAs] Is Not Null
			) Or
			(
				INSERTED.[ShowTimeAs] Is Not Null And
				DELETED.[ShowTimeAs] Is Null
			) Or
			(
				INSERTED.[ShowTimeAs] !=
				DELETED.[ShowTimeAs]
			)
		) 
		END		
		
      If UPDATE([AllDayEventInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'AllDayEventInd',
      CONVERT(NVARCHAR(2000),DELETED.[AllDayEventInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AllDayEventInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[AllDayEventInd] Is Null And
				DELETED.[AllDayEventInd] Is Not Null
			) Or
			(
				INSERTED.[AllDayEventInd] Is Not Null And
				DELETED.[AllDayEventInd] Is Null
			) Or
			(
				INSERTED.[AllDayEventInd] !=
				DELETED.[AllDayEventInd]
			)
		) 
		END		
		
      If UPDATE([CompletionInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'CompletionInd',
      CONVERT(NVARCHAR(2000),DELETED.[CompletionInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CompletionInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[CompletionInd] Is Null And
				DELETED.[CompletionInd] Is Not Null
			) Or
			(
				INSERTED.[CompletionInd] Is Not Null And
				DELETED.[CompletionInd] Is Null
			) Or
			(
				INSERTED.[CompletionInd] !=
				DELETED.[CompletionInd]
			)
		) 
		END		
		
      If UPDATE([RecurrenceInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrenceInd',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrenceInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrenceInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrenceInd] Is Null And
				DELETED.[RecurrenceInd] Is Not Null
			) Or
			(
				INSERTED.[RecurrenceInd] Is Not Null And
				DELETED.[RecurrenceInd] Is Null
			) Or
			(
				INSERTED.[RecurrenceInd] !=
				DELETED.[RecurrenceInd]
			)
		) 
		END		
		
      If UPDATE([PrivateInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'PrivateInd',
      CONVERT(NVARCHAR(2000),DELETED.[PrivateInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PrivateInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[PrivateInd] Is Null And
				DELETED.[PrivateInd] Is Not Null
			) Or
			(
				INSERTED.[PrivateInd] Is Not Null And
				DELETED.[PrivateInd] Is Null
			) Or
			(
				INSERTED.[PrivateInd] !=
				DELETED.[PrivateInd]
			)
		) 
		END		
		
      If UPDATE([TaskStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'TaskStatus',
      CONVERT(NVARCHAR(2000),DELETED.[TaskStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TaskStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[TaskStatus] Is Null And
				DELETED.[TaskStatus] Is Not Null
			) Or
			(
				INSERTED.[TaskStatus] Is Not Null And
				DELETED.[TaskStatus] Is Null
			) Or
			(
				INSERTED.[TaskStatus] !=
				DELETED.[TaskStatus]
			)
		) 
		END		
		
      If UPDATE([TaskCompletionDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'TaskCompletionDate',
      CONVERT(NVARCHAR(2000),DELETED.[TaskCompletionDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TaskCompletionDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[TaskCompletionDate] Is Null And
				DELETED.[TaskCompletionDate] Is Not Null
			) Or
			(
				INSERTED.[TaskCompletionDate] Is Not Null And
				DELETED.[TaskCompletionDate] Is Null
			) Or
			(
				INSERTED.[TaskCompletionDate] !=
				DELETED.[TaskCompletionDate]
			)
		) 
		END		
		
      If UPDATE([OpportunityID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'OpportunityID',
      CONVERT(NVARCHAR(2000),DELETED.[OpportunityID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OpportunityID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[OpportunityID] Is Null And
				DELETED.[OpportunityID] Is Not Null
			) Or
			(
				INSERTED.[OpportunityID] Is Not Null And
				DELETED.[OpportunityID] Is Null
			) Or
			(
				INSERTED.[OpportunityID] !=
				DELETED.[OpportunityID]
			)
		) 
		END		
		
      If UPDATE([CampaignCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'CampaignCode',
      CONVERT(NVARCHAR(2000),DELETED.[CampaignCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CampaignCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[CampaignCode] Is Null And
				DELETED.[CampaignCode] Is Not Null
			) Or
			(
				INSERTED.[CampaignCode] Is Not Null And
				DELETED.[CampaignCode] Is Null
			) Or
			(
				INSERTED.[CampaignCode] !=
				DELETED.[CampaignCode]
			)
		) 
		END		
		
      If UPDATE([RecurrType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrType',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrType] Is Null And
				DELETED.[RecurrType] Is Not Null
			) Or
			(
				INSERTED.[RecurrType] Is Not Null And
				DELETED.[RecurrType] Is Null
			) Or
			(
				INSERTED.[RecurrType] !=
				DELETED.[RecurrType]
			)
		) 
		END		
		
      If UPDATE([RecurrDailyFreq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrDailyFreq',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrDailyFreq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrDailyFreq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrDailyFreq] Is Null And
				DELETED.[RecurrDailyFreq] Is Not Null
			) Or
			(
				INSERTED.[RecurrDailyFreq] Is Not Null And
				DELETED.[RecurrDailyFreq] Is Null
			) Or
			(
				INSERTED.[RecurrDailyFreq] !=
				DELETED.[RecurrDailyFreq]
			)
		) 
		END		
		
      If UPDATE([RecurrDailyWeekDay])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrDailyWeekDay',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrDailyWeekDay],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrDailyWeekDay],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrDailyWeekDay] Is Null And
				DELETED.[RecurrDailyWeekDay] Is Not Null
			) Or
			(
				INSERTED.[RecurrDailyWeekDay] Is Not Null And
				DELETED.[RecurrDailyWeekDay] Is Null
			) Or
			(
				INSERTED.[RecurrDailyWeekDay] !=
				DELETED.[RecurrDailyWeekDay]
			)
		) 
		END		
		
      If UPDATE([RecurrWeeklyFreq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrWeeklyFreq',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrWeeklyFreq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrWeeklyFreq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrWeeklyFreq] Is Null And
				DELETED.[RecurrWeeklyFreq] Is Not Null
			) Or
			(
				INSERTED.[RecurrWeeklyFreq] Is Not Null And
				DELETED.[RecurrWeeklyFreq] Is Null
			) Or
			(
				INSERTED.[RecurrWeeklyFreq] !=
				DELETED.[RecurrWeeklyFreq]
			)
		) 
		END		
		
      If UPDATE([RecurrWeeklySun])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrWeeklySun',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrWeeklySun],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrWeeklySun],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrWeeklySun] Is Null And
				DELETED.[RecurrWeeklySun] Is Not Null
			) Or
			(
				INSERTED.[RecurrWeeklySun] Is Not Null And
				DELETED.[RecurrWeeklySun] Is Null
			) Or
			(
				INSERTED.[RecurrWeeklySun] !=
				DELETED.[RecurrWeeklySun]
			)
		) 
		END		
		
      If UPDATE([RecurrWeeklyMon])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrWeeklyMon',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrWeeklyMon],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrWeeklyMon],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrWeeklyMon] Is Null And
				DELETED.[RecurrWeeklyMon] Is Not Null
			) Or
			(
				INSERTED.[RecurrWeeklyMon] Is Not Null And
				DELETED.[RecurrWeeklyMon] Is Null
			) Or
			(
				INSERTED.[RecurrWeeklyMon] !=
				DELETED.[RecurrWeeklyMon]
			)
		) 
		END		
		
      If UPDATE([RecurrWeeklyTue])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrWeeklyTue',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrWeeklyTue],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrWeeklyTue],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrWeeklyTue] Is Null And
				DELETED.[RecurrWeeklyTue] Is Not Null
			) Or
			(
				INSERTED.[RecurrWeeklyTue] Is Not Null And
				DELETED.[RecurrWeeklyTue] Is Null
			) Or
			(
				INSERTED.[RecurrWeeklyTue] !=
				DELETED.[RecurrWeeklyTue]
			)
		) 
		END		
		
      If UPDATE([RecurrWeeklyWed])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrWeeklyWed',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrWeeklyWed],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrWeeklyWed],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrWeeklyWed] Is Null And
				DELETED.[RecurrWeeklyWed] Is Not Null
			) Or
			(
				INSERTED.[RecurrWeeklyWed] Is Not Null And
				DELETED.[RecurrWeeklyWed] Is Null
			) Or
			(
				INSERTED.[RecurrWeeklyWed] !=
				DELETED.[RecurrWeeklyWed]
			)
		) 
		END		
		
      If UPDATE([RecurrWeeklyThu])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrWeeklyThu',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrWeeklyThu],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrWeeklyThu],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrWeeklyThu] Is Null And
				DELETED.[RecurrWeeklyThu] Is Not Null
			) Or
			(
				INSERTED.[RecurrWeeklyThu] Is Not Null And
				DELETED.[RecurrWeeklyThu] Is Null
			) Or
			(
				INSERTED.[RecurrWeeklyThu] !=
				DELETED.[RecurrWeeklyThu]
			)
		) 
		END		
		
      If UPDATE([RecurrWeeklyFri])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrWeeklyFri',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrWeeklyFri],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrWeeklyFri],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrWeeklyFri] Is Null And
				DELETED.[RecurrWeeklyFri] Is Not Null
			) Or
			(
				INSERTED.[RecurrWeeklyFri] Is Not Null And
				DELETED.[RecurrWeeklyFri] Is Null
			) Or
			(
				INSERTED.[RecurrWeeklyFri] !=
				DELETED.[RecurrWeeklyFri]
			)
		) 
		END		
		
      If UPDATE([RecurrWeeklySat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrWeeklySat',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrWeeklySat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrWeeklySat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrWeeklySat] Is Null And
				DELETED.[RecurrWeeklySat] Is Not Null
			) Or
			(
				INSERTED.[RecurrWeeklySat] Is Not Null And
				DELETED.[RecurrWeeklySat] Is Null
			) Or
			(
				INSERTED.[RecurrWeeklySat] !=
				DELETED.[RecurrWeeklySat]
			)
		) 
		END		
		
      If UPDATE([RecurrMonthlyFreq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrMonthlyFreq',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrMonthlyFreq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrMonthlyFreq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrMonthlyFreq] Is Null And
				DELETED.[RecurrMonthlyFreq] Is Not Null
			) Or
			(
				INSERTED.[RecurrMonthlyFreq] Is Not Null And
				DELETED.[RecurrMonthlyFreq] Is Null
			) Or
			(
				INSERTED.[RecurrMonthlyFreq] !=
				DELETED.[RecurrMonthlyFreq]
			)
		) 
		END		
		
      If UPDATE([RecurrMonthlyDay])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrMonthlyDay',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrMonthlyDay],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrMonthlyDay],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrMonthlyDay] Is Null And
				DELETED.[RecurrMonthlyDay] Is Not Null
			) Or
			(
				INSERTED.[RecurrMonthlyDay] Is Not Null And
				DELETED.[RecurrMonthlyDay] Is Null
			) Or
			(
				INSERTED.[RecurrMonthlyDay] !=
				DELETED.[RecurrMonthlyDay]
			)
		) 
		END		
		
      If UPDATE([RecurrYearlyMonth])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrYearlyMonth',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrYearlyMonth],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrYearlyMonth],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrYearlyMonth] Is Null And
				DELETED.[RecurrYearlyMonth] Is Not Null
			) Or
			(
				INSERTED.[RecurrYearlyMonth] Is Not Null And
				DELETED.[RecurrYearlyMonth] Is Null
			) Or
			(
				INSERTED.[RecurrYearlyMonth] !=
				DELETED.[RecurrYearlyMonth]
			)
		) 
		END		
		
      If UPDATE([RecurrYearlyDay])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrYearlyDay',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrYearlyDay],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrYearlyDay],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrYearlyDay] Is Null And
				DELETED.[RecurrYearlyDay] Is Not Null
			) Or
			(
				INSERTED.[RecurrYearlyDay] Is Not Null And
				DELETED.[RecurrYearlyDay] Is Null
			) Or
			(
				INSERTED.[RecurrYearlyDay] !=
				DELETED.[RecurrYearlyDay]
			)
		) 
		END		
		
      If UPDATE([RecurrStartDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrStartDate',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrStartDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrStartDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrStartDate] Is Null And
				DELETED.[RecurrStartDate] Is Not Null
			) Or
			(
				INSERTED.[RecurrStartDate] Is Not Null And
				DELETED.[RecurrStartDate] Is Null
			) Or
			(
				INSERTED.[RecurrStartDate] !=
				DELETED.[RecurrStartDate]
			)
		) 
		END		
		
      If UPDATE([RecurrEndType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrEndType',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrEndType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrEndType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrEndType] Is Null And
				DELETED.[RecurrEndType] Is Not Null
			) Or
			(
				INSERTED.[RecurrEndType] Is Not Null And
				DELETED.[RecurrEndType] Is Null
			) Or
			(
				INSERTED.[RecurrEndType] !=
				DELETED.[RecurrEndType]
			)
		) 
		END		
		
      If UPDATE([RecurrEndDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrEndDate',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrEndDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrEndDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrEndDate] Is Null And
				DELETED.[RecurrEndDate] Is Not Null
			) Or
			(
				INSERTED.[RecurrEndDate] Is Not Null And
				DELETED.[RecurrEndDate] Is Null
			) Or
			(
				INSERTED.[RecurrEndDate] !=
				DELETED.[RecurrEndDate]
			)
		) 
		END		
		
      If UPDATE([RP])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RP',
      CONVERT(NVARCHAR(2000),DELETED.[RP],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RP],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RP] Is Null And
				DELETED.[RP] Is Not Null
			) Or
			(
				INSERTED.[RP] Is Not Null And
				DELETED.[RP] Is Null
			) Or
			(
				INSERTED.[RP] !=
				DELETED.[RP]
			)
		) 
		END		
		
      If UPDATE([EmailAlertSent])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'EmailAlertSent',
      CONVERT(NVARCHAR(2000),DELETED.[EmailAlertSent],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EmailAlertSent],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[EmailAlertSent] Is Null And
				DELETED.[EmailAlertSent] Is Not Null
			) Or
			(
				INSERTED.[EmailAlertSent] Is Not Null And
				DELETED.[EmailAlertSent] Is Null
			) Or
			(
				INSERTED.[EmailAlertSent] !=
				DELETED.[EmailAlertSent]
			)
		) 
		END		
		
      If UPDATE([RecurrID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrID',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrID] Is Null And
				DELETED.[RecurrID] Is Not Null
			) Or
			(
				INSERTED.[RecurrID] Is Not Null And
				DELETED.[RecurrID] Is Null
			) Or
			(
				INSERTED.[RecurrID] !=
				DELETED.[RecurrID]
			)
		) 
		END		
		
      If UPDATE([CampaignID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'CampaignID',
      CONVERT(NVARCHAR(2000),DELETED.[CampaignID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CampaignID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[CampaignID] Is Null And
				DELETED.[CampaignID] Is Not Null
			) Or
			(
				INSERTED.[CampaignID] Is Not Null And
				DELETED.[CampaignID] Is Null
			) Or
			(
				INSERTED.[CampaignID] !=
				DELETED.[CampaignID]
			)
		) 
		END		
		
      If UPDATE([LeadID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'LeadID',
      CONVERT(NVARCHAR(2000),DELETED.[LeadID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LeadID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[LeadID] Is Null And
				DELETED.[LeadID] Is Not Null
			) Or
			(
				INSERTED.[LeadID] Is Not Null And
				DELETED.[LeadID] Is Null
			) Or
			(
				INSERTED.[LeadID] !=
				DELETED.[LeadID]
			)
		) 
		END		
		
      If UPDATE([RecurrMonthlyOccur])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrMonthlyOccur',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrMonthlyOccur],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrMonthlyOccur],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrMonthlyOccur] Is Null And
				DELETED.[RecurrMonthlyOccur] Is Not Null
			) Or
			(
				INSERTED.[RecurrMonthlyOccur] Is Not Null And
				DELETED.[RecurrMonthlyOccur] Is Null
			) Or
			(
				INSERTED.[RecurrMonthlyOccur] !=
				DELETED.[RecurrMonthlyOccur]
			)
		) 
		END		
		
      If UPDATE([RecurrMonthlyOccurFreq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrMonthlyOccurFreq',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrMonthlyOccurFreq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrMonthlyOccurFreq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrMonthlyOccurFreq] Is Null And
				DELETED.[RecurrMonthlyOccurFreq] Is Not Null
			) Or
			(
				INSERTED.[RecurrMonthlyOccurFreq] Is Not Null And
				DELETED.[RecurrMonthlyOccurFreq] Is Null
			) Or
			(
				INSERTED.[RecurrMonthlyOccurFreq] !=
				DELETED.[RecurrMonthlyOccurFreq]
			)
		) 
		END		
		
      If UPDATE([RecurrMonthlyOccurDay])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrMonthlyOccurDay',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrMonthlyOccurDay],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrMonthlyOccurDay],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrMonthlyOccurDay] Is Null And
				DELETED.[RecurrMonthlyOccurDay] Is Not Null
			) Or
			(
				INSERTED.[RecurrMonthlyOccurDay] Is Not Null And
				DELETED.[RecurrMonthlyOccurDay] Is Null
			) Or
			(
				INSERTED.[RecurrMonthlyOccurDay] !=
				DELETED.[RecurrMonthlyOccurDay]
			)
		) 
		END		
		
      If UPDATE([RecurrMonthlyOccurInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrMonthlyOccurInd',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrMonthlyOccurInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrMonthlyOccurInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrMonthlyOccurInd] Is Null And
				DELETED.[RecurrMonthlyOccurInd] Is Not Null
			) Or
			(
				INSERTED.[RecurrMonthlyOccurInd] Is Not Null And
				DELETED.[RecurrMonthlyOccurInd] Is Null
			) Or
			(
				INSERTED.[RecurrMonthlyOccurInd] !=
				DELETED.[RecurrMonthlyOccurInd]
			)
		) 
		END		
		
      If UPDATE([AssignmentID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'AssignmentID',
      CONVERT(NVARCHAR(2000),DELETED.[AssignmentID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AssignmentID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[AssignmentID] Is Null And
				DELETED.[AssignmentID] Is Not Null
			) Or
			(
				INSERTED.[AssignmentID] Is Not Null And
				DELETED.[AssignmentID] Is Null
			) Or
			(
				INSERTED.[AssignmentID] !=
				DELETED.[AssignmentID]
			)
		) 
		END		
		
      If UPDATE([ConversationID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ConversationID',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([RecurrYearlyOccurInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrYearlyOccurInd',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrYearlyOccurInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrYearlyOccurInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrYearlyOccurInd] Is Null And
				DELETED.[RecurrYearlyOccurInd] Is Not Null
			) Or
			(
				INSERTED.[RecurrYearlyOccurInd] Is Not Null And
				DELETED.[RecurrYearlyOccurInd] Is Null
			) Or
			(
				INSERTED.[RecurrYearlyOccurInd] !=
				DELETED.[RecurrYearlyOccurInd]
			)
		) 
		END		
		
      If UPDATE([RecurrYearlyOccur])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrYearlyOccur',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrYearlyOccur],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrYearlyOccur],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrYearlyOccur] Is Null And
				DELETED.[RecurrYearlyOccur] Is Not Null
			) Or
			(
				INSERTED.[RecurrYearlyOccur] Is Not Null And
				DELETED.[RecurrYearlyOccur] Is Null
			) Or
			(
				INSERTED.[RecurrYearlyOccur] !=
				DELETED.[RecurrYearlyOccur]
			)
		) 
		END		
		
      If UPDATE([RecurrYearlyOccurDay])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrYearlyOccurDay',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrYearlyOccurDay],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrYearlyOccurDay],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrYearlyOccurDay] Is Null And
				DELETED.[RecurrYearlyOccurDay] Is Not Null
			) Or
			(
				INSERTED.[RecurrYearlyOccurDay] Is Not Null And
				DELETED.[RecurrYearlyOccurDay] Is Null
			) Or
			(
				INSERTED.[RecurrYearlyOccurDay] !=
				DELETED.[RecurrYearlyOccurDay]
			)
		) 
		END		
		
      If UPDATE([RecurrYearlyOccurMonth])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'RecurrYearlyOccurMonth',
      CONVERT(NVARCHAR(2000),DELETED.[RecurrYearlyOccurMonth],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RecurrYearlyOccurMonth],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[RecurrYearlyOccurMonth] Is Null And
				DELETED.[RecurrYearlyOccurMonth] Is Not Null
			) Or
			(
				INSERTED.[RecurrYearlyOccurMonth] Is Not Null And
				DELETED.[RecurrYearlyOccurMonth] Is Null
			) Or
			(
				INSERTED.[RecurrYearlyOccurMonth] !=
				DELETED.[RecurrYearlyOccurMonth]
			)
		) 
		END		
		
      If UPDATE([MaxOccurences])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'MaxOccurences',
      CONVERT(NVARCHAR(2000),DELETED.[MaxOccurences],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MaxOccurences],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[MaxOccurences] Is Null And
				DELETED.[MaxOccurences] Is Not Null
			) Or
			(
				INSERTED.[MaxOccurences] Is Not Null And
				DELETED.[MaxOccurences] Is Null
			) Or
			(
				INSERTED.[MaxOccurences] !=
				DELETED.[MaxOccurences]
			)
		) 
		END		
		
      If UPDATE([PIMInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'PIMInd',
      CONVERT(NVARCHAR(2000),DELETED.[PIMInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PIMInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[PIMInd] Is Null And
				DELETED.[PIMInd] Is Not Null
			) Or
			(
				INSERTED.[PIMInd] Is Not Null And
				DELETED.[PIMInd] Is Null
			) Or
			(
				INSERTED.[PIMInd] !=
				DELETED.[PIMInd]
			)
		) 
		END		
		
      If UPDATE([KonaTask])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'KonaTask',
      CONVERT(NVARCHAR(2000),DELETED.[KonaTask],121),
      CONVERT(NVARCHAR(2000),INSERTED.[KonaTask],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[KonaTask] Is Null And
				DELETED.[KonaTask] Is Not Null
			) Or
			(
				INSERTED.[KonaTask] Is Not Null And
				DELETED.[KonaTask] Is Null
			) Or
			(
				INSERTED.[KonaTask] !=
				DELETED.[KonaTask]
			)
		) 
		END		
		
      If UPDATE([Vendor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'Vendor',
      CONVERT(NVARCHAR(2000),DELETED.[Vendor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Vendor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[Vendor] Is Null And
				DELETED.[Vendor] Is Not Null
			) Or
			(
				INSERTED.[Vendor] Is Not Null And
				DELETED.[Vendor] Is Null
			) Or
			(
				INSERTED.[Vendor] !=
				DELETED.[Vendor]
			)
		) 
		END		
		
      If UPDATE([ContactIDForVendor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'ContactIDForVendor',
      CONVERT(NVARCHAR(2000),DELETED.[ContactIDForVendor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ContactIDForVendor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[ContactIDForVendor] Is Null And
				DELETED.[ContactIDForVendor] Is Not Null
			) Or
			(
				INSERTED.[ContactIDForVendor] Is Not Null And
				DELETED.[ContactIDForVendor] Is Null
			) Or
			(
				INSERTED.[ContactIDForVendor] !=
				DELETED.[ContactIDForVendor]
			)
		) 
		END		
		
      If UPDATE([OccurrenceDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'OccurrenceDate',
      CONVERT(NVARCHAR(2000),DELETED.[OccurrenceDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OccurrenceDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[OccurrenceDate] Is Null And
				DELETED.[OccurrenceDate] Is Not Null
			) Or
			(
				INSERTED.[OccurrenceDate] Is Not Null And
				DELETED.[OccurrenceDate] Is Null
			) Or
			(
				INSERTED.[OccurrenceDate] !=
				DELETED.[OccurrenceDate]
			)
		) 
		END		
		
      If UPDATE([TimeZoneOffset])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ActivityID],121),'TimeZoneOffset',
      CONVERT(NVARCHAR(2000),DELETED.[TimeZoneOffset],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TimeZoneOffset],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ActivityID] = DELETED.[ActivityID] AND 
		(
			(
				INSERTED.[TimeZoneOffset] Is Null And
				DELETED.[TimeZoneOffset] Is Not Null
			) Or
			(
				INSERTED.[TimeZoneOffset] Is Not Null And
				DELETED.[TimeZoneOffset] Is Null
			) Or
			(
				INSERTED.[TimeZoneOffset] !=
				DELETED.[TimeZoneOffset]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_Activity] ON [dbo].[Activity]
GO
ALTER TABLE [dbo].[Activity] ADD CONSTRAINT [ActivityPK] PRIMARY KEY NONCLUSTERED ([ActivityID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ActivityAssignmentIDX] ON [dbo].[Activity] ([AssignmentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ActivityClientIDIDX] ON [dbo].[Activity] ([ClientID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ActivityContactIDIDX] ON [dbo].[Activity] ([ContactID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ActivityCreateDateIDX] ON [dbo].[Activity] ([CreateDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ActivityEmployeeIDX] ON [dbo].[Activity] ([Employee]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ActivityLeadIDIDX] ON [dbo].[Activity] ([LeadID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ActivityModDateIDX] ON [dbo].[Activity] ([ModDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ActivityOpportunityIDIDX] ON [dbo].[Activity] ([OpportunityID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ActivityRecurrenceIndIDX] ON [dbo].[Activity] ([RecurrenceInd]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ActivityRecurrIDIDX] ON [dbo].[Activity] ([RecurrID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ActivityWBS1WBS2WBS3IDX] ON [dbo].[Activity] ([WBS1], [WBS2], [WBS3]) ON [PRIMARY]
GO
