CREATE TABLE [dbo].[CFGDiaryAutoNum]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__CFGDiaryA__Perio__2B419BA3] DEFAULT ((0)),
[Diary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NextDiaryNo] [int] NOT NULL CONSTRAINT [DF__CFGDiaryA__NextD__2C35BFDC] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGDiaryAutoNum] ADD CONSTRAINT [CFGDiaryAutoNumPK] PRIMARY KEY CLUSTERED ([Company], [Code], [Period]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
