CREATE TABLE [dbo].[ContactSubscr]
(
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContactSubscr] ADD CONSTRAINT [ContactSubscrPK] PRIMARY KEY NONCLUSTERED ([PartKey], [ContactID], [UserName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
