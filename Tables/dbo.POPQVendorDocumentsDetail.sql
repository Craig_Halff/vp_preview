CREATE TABLE [dbo].[POPQVendorDocumentsDetail]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POPQVendorDocumentsDetail] ADD CONSTRAINT [POPQVendorDocumentsDetailPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [Vendor], [DetailPKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
