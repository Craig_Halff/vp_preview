CREATE TABLE [dbo].[EMPayrollContributionWage]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodeWage] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMPayroll__Amoun__01764A60] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EMPayrollContributionWage] ADD CONSTRAINT [EMPayrollContributionWagePK] PRIMARY KEY NONCLUSTERED ([Employee], [EmployeeCompany], [Code], [CodeWage]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
