CREATE TABLE [dbo].[PNPlan]
(
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlanNumber] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjMgr] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Principal] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Supervisor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[BaselineLaborHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__1FF0AFDF] DEFAULT ((0)),
[BaselineLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__20E4D418] DEFAULT ((0)),
[BaselineLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__21D8F851] DEFAULT ((0)),
[BaselineExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__22CD1C8A] DEFAULT ((0)),
[BaselineExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__23C140C3] DEFAULT ((0)),
[BaselineConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__24B564FC] DEFAULT ((0)),
[BaselineConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__25A98935] DEFAULT ((0)),
[BaselineRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__269DAD6E] DEFAULT ((0)),
[BaselineStart] [datetime] NULL,
[BaselineFinish] [datetime] NULL,
[PlannedLaborHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Plann__2791D1A7] DEFAULT ((0)),
[PlannedLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Plann__2885F5E0] DEFAULT ((0)),
[PlannedLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Plann__297A1A19] DEFAULT ((0)),
[PlannedExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Plann__2A6E3E52] DEFAULT ((0)),
[PlannedExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Plann__2B62628B] DEFAULT ((0)),
[PlannedConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Plann__2C5686C4] DEFAULT ((0)),
[PlannedConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Plann__2D4AAAFD] DEFAULT ((0)),
[LabRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__LabRe__2E3ECF36] DEFAULT ((0)),
[PlannedDirExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Plann__2F32F36F] DEFAULT ((0)),
[PlannedDirExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Plann__302717A8] DEFAULT ((0)),
[PlannedDirConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Plann__311B3BE1] DEFAULT ((0)),
[PlannedDirConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Plann__320F601A] DEFAULT ((0)),
[CostRtMethod] [smallint] NOT NULL CONSTRAINT [DF__PNPlan_Ne__CostR__33038453] DEFAULT ((0)),
[BillingRtMethod] [smallint] NOT NULL CONSTRAINT [DF__PNPlan_Ne__Billi__33F7A88C] DEFAULT ((0)),
[CostRtTableNo] [int] NOT NULL CONSTRAINT [DF__PNPlan_Ne__CostR__34EBCCC5] DEFAULT ((0)),
[BillingRtTableNo] [int] NOT NULL CONSTRAINT [DF__PNPlan_Ne__Billi__35DFF0FE] DEFAULT ((0)),
[CompensationFee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Compe__36D41537] DEFAULT ((0)),
[ConsultantFee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Consu__37C83970] DEFAULT ((0)),
[ContingencyPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Conti__38BC5DA9] DEFAULT ((0)),
[ContingencyAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Conti__39B081E2] DEFAULT ((0)),
[OverheadPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Overh__3AA4A61B] DEFAULT ((0)),
[AvailableFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan_Ne__Avail__3B98CA54] DEFAULT ('Y'),
[GenResTableNo] [int] NOT NULL CONSTRAINT [DF__PNPlan_Ne__GenRe__3C8CEE8D] DEFAULT ((0)),
[UnPostedFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan_Ne__UnPos__3D8112C6] DEFAULT ('N'),
[BudgetType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan_Ne__Budge__3E7536FF] DEFAULT ('C'),
[PctCompleteFormula] [smallint] NOT NULL CONSTRAINT [DF__PNPlan_Ne__PctCo__3F695B38] DEFAULT ((0)),
[PctComplete] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__PctCo__405D7F71] DEFAULT ((0)),
[PctCompleteBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__PctCo__4151A3AA] DEFAULT ((0)),
[PctCompleteLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__PctCo__4245C7E3] DEFAULT ((0)),
[PctCompleteLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__PctCo__4339EC1C] DEFAULT ((0)),
[PctCompleteExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__PctCo__442E1055] DEFAULT ((0)),
[PctCompleteExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__PctCo__4522348E] DEFAULT ((0)),
[PctCompleteConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__PctCo__461658C7] DEFAULT ((0)),
[PctCompleteConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__PctCo__470A7D00] DEFAULT ((0)),
[WeightedLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Weigh__47FEA139] DEFAULT ((0)),
[WeightedLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Weigh__48F2C572] DEFAULT ((0)),
[WeightedExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Weigh__49E6E9AB] DEFAULT ((0)),
[WeightedExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Weigh__4ADB0DE4] DEFAULT ((0)),
[WeightedConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Weigh__4BCF321D] DEFAULT ((0)),
[WeightedConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Weigh__4CC35656] DEFAULT ((0)),
[UtilizationIncludeFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan_Ne__Utili__4DB77A8F] DEFAULT ('Y'),
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReimbAllowance] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Reimb__4EAB9EC8] DEFAULT ((0)),
[Probability] [smallint] NOT NULL CONSTRAINT [DF__PNPlan_Ne__Proba__4F9FC301] DEFAULT ((0)),
[LabMultType] [smallint] NOT NULL CONSTRAINT [DF__PNPlan_Ne__LabMu__5093E73A] DEFAULT ((0)),
[Multiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Multi__51880B73] DEFAULT ((0)),
[ProjectedMultiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Proje__527C2FAC] DEFAULT ((0)),
[ProjectedRatio] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Proje__537053E5] DEFAULT ((0)),
[CalcExpBillAmtFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan_Ne__CalcE__5464781E] DEFAULT ('N'),
[CalcConBillAmtFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan_Ne__CalcC__55589C57] DEFAULT ('N'),
[ExpBillRtMethod] [smallint] NOT NULL CONSTRAINT [DF__PNPlan_Ne__ExpBi__564CC090] DEFAULT ((0)),
[ExpBillRtTableNo] [int] NOT NULL CONSTRAINT [DF__PNPlan_Ne__ExpBi__5740E4C9] DEFAULT ((0)),
[ConBillRtMethod] [smallint] NOT NULL CONSTRAINT [DF__PNPlan_Ne__ConBi__58350902] DEFAULT ((0)),
[ConBillRtTableNo] [int] NOT NULL CONSTRAINT [DF__PNPlan_Ne__ConBi__59292D3B] DEFAULT ((0)),
[GRBillTableNo] [int] NOT NULL CONSTRAINT [DF__PNPlan_Ne__GRBil__5A1D5174] DEFAULT ((0)),
[ExpBillMultiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__ExpBi__5B1175AD] DEFAULT ((0)),
[ConBillMultiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__ConBi__5C0599E6] DEFAULT ((0)),
[ReimbMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan_Ne__Reimb__5CF9BE1F] DEFAULT ('C'),
[ExpRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__ExpRe__5DEDE258] DEFAULT ((0)),
[ConRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__ConRe__5EE20691] DEFAULT ((0)),
[TargetMultCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Targe__5FD62ACA] DEFAULT ((0)),
[TargetMultBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Targe__60CA4F03] DEFAULT ((0)),
[BaselineUntQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__61BE733C] DEFAULT ((0)),
[BaselineUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__62B29775] DEFAULT ((0)),
[BaselineUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__63A6BBAE] DEFAULT ((0)),
[PlannedUntQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Plann__649ADFE7] DEFAULT ((0)),
[PlannedUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Plann__658F0420] DEFAULT ((0)),
[PlannedUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Plann__66832859] DEFAULT ((0)),
[PctCompleteUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__PctCo__67774C92] DEFAULT ((0)),
[PctCompleteUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__PctCo__686B70CB] DEFAULT ((0)),
[PlannedDirUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Plann__695F9504] DEFAULT ((0)),
[PlannedDirUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Plann__6A53B93D] DEFAULT ((0)),
[WeightedUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Weigh__6B47DD76] DEFAULT ((0)),
[WeightedUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Weigh__6C3C01AF] DEFAULT ((0)),
[UntRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__UntRe__6D3025E8] DEFAULT ((0)),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan_Ne__Statu__6E244A21] DEFAULT ('A'),
[PctComplByPeriodFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan_Ne__PctCo__6F186E5A] DEFAULT ('N'),
[RevenueMethod] [smallint] NOT NULL CONSTRAINT [DF__PNPlan_Ne__Reven__700C9293] DEFAULT ((1)),
[AnalysisBasis] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan_Ne__Analy__7100B6CC] DEFAULT ('P'),
[GRMethod] [smallint] NOT NULL CONSTRAINT [DF__PNPlan_Ne__GRMet__71F4DB05] DEFAULT ((0)),
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartingDayOfWeek] [smallint] NOT NULL CONSTRAINT [DF__PNPlan_Ne__Start__72E8FF3E] DEFAULT ((2)),
[CostCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BillingCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CompensationFeeBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Compe__73DD2377] DEFAULT ((0)),
[ConsultantFeeBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Consu__74D147B0] DEFAULT ((0)),
[ReimbAllowanceBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Reimb__75C56BE9] DEFAULT ((0)),
[EVFormula] [smallint] NOT NULL CONSTRAINT [DF__PNPlan_Ne__EVFor__76B99022] DEFAULT ((0)),
[LabBillMultiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__LabBi__77ADB45B] DEFAULT ((1)),
[UntBillMultiplier] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__UntBi__78A1D894] DEFAULT ((1)),
[BaselineDirExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__7995FCCD] DEFAULT ((0)),
[BaselineDirExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__7A8A2106] DEFAULT ((0)),
[BaselineDirConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__7B7E453F] DEFAULT ((0)),
[BaselineDirConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__7C726978] DEFAULT ((0)),
[BaselineDirUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__7D668DB1] DEFAULT ((0)),
[BaselineDirUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Basel__7E5AB1EA] DEFAULT ((0)),
[CommitmentFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan_Ne__Commi__7F4ED623] DEFAULT ('N'),
[TopdownFee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Topdo__0042FA5C] DEFAULT ((0)),
[JTDLabHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__JTDLa__01371E95] DEFAULT ((0)),
[JTDLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__JTDLa__022B42CE] DEFAULT ((0)),
[JTDLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__JTDLa__031F6707] DEFAULT ((0)),
[JTDExpCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__JTDEx__04138B40] DEFAULT ((0)),
[JTDExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__JTDEx__0507AF79] DEFAULT ((0)),
[JTDConCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__JTDCo__05FBD3B2] DEFAULT ((0)),
[JTDConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__JTDCo__06EFF7EB] DEFAULT ((0)),
[JTDUntQty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__JTDUn__07E41C24] DEFAULT ((0)),
[JTDUntCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__JTDUn__08D8405D] DEFAULT ((0)),
[JTDUntBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__JTDUn__09CC6496] DEFAULT ((0)),
[JTDRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__JTDRe__0AC088CF] DEFAULT ((0)),
[NeedsRecalc] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan_Ne__Needs__0BB4AD08] DEFAULT ('N'),
[CheckedOutUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckedOutDate] [datetime] NULL,
[CheckedOutID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastPlanAction] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConWBSLevel] [smallint] NOT NULL CONSTRAINT [DF__PNPlan_Ne__ConWB__0CA8D141] DEFAULT ((3)),
[ExpWBSLevel] [smallint] NOT NULL CONSTRAINT [DF__PNPlan_Ne__ExpWB__0D9CF57A] DEFAULT ((3)),
[CompensationFeeDirLab] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Compe__0E9119B3] DEFAULT ((0)),
[CompensationFeeDirExp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Compe__0F853DEC] DEFAULT ((0)),
[ReimbAllowanceExp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Reimb__10796225] DEFAULT ((0)),
[ReimbAllowanceCon] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Reimb__116D865E] DEFAULT ((0)),
[CompensationFeeDirLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Compe__1261AA97] DEFAULT ((0)),
[CompensationFeeDirExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Compe__1355CED0] DEFAULT ((0)),
[ReimbAllowanceExpBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Reimb__1449F309] DEFAULT ((0)),
[ReimbAllowanceConBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PNPlan_Ne__Reimb__153E1742] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CostGRRtMethod] [smallint] NOT NULL CONSTRAINT [DF__PNPlan__CostGRRt__5C5C364F] DEFAULT ((0)),
[BillGRRtMethod] [smallint] NOT NULL CONSTRAINT [DF__PNPlan__BillGRRt__5D505A88] DEFAULT ((0)),
[VersionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LCLevel1Enabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan__LCLevel1__15B4AF3C] DEFAULT ('Y'),
[LCLevel2Enabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan__LCLevel2__16A8D375] DEFAULT ('Y'),
[LCLevel3Enabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan__LCLevel3__179CF7AE] DEFAULT ('Y'),
[LCLevel4Enabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan__LCLevel4__18911BE7] DEFAULT ('Y'),
[LCLevel5Enabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan__LCLevel5__19854020] DEFAULT ('Y'),
[StoredCurrency] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNPlan__StoredCu__230EAA5A] DEFAULT ('P'),
[UntWBSLevel] [smallint] NOT NULL CONSTRAINT [DF__PNPlan__UntWBSLe__79835039] DEFAULT ((1))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[PNRemoveExpConTrigger] ON [dbo].[PNPlan] AFTER UPDATE 
AS
BEGIN

  SET NOCOUNT ON

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This trigger is fired after an update to PNPlan.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @strPlanID varchar(32)
  DECLARE @strWBS1 Nvarchar(30)
  DECLARE @strCalcExp varchar(1)
  DECLARE @strCalcCon varchar(1)

  DECLARE @bitExpWBSLevelChanged bit
  DECLARE @bitConWBSLevelChanged bit

  DECLARE @tiExpWBSLevel tinyint
  DECLARE @tiConWBSLevel tinyint

  DECLARE @tabWBSTree TABLE(
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(30) COLLATE database_default,
    WBS3 Nvarchar(30) COLLATE database_default,
    WBSLevel tinyint,
    IsLeaf bit
    UNIQUE(WBS1, WBS2, WBS3, WBSLevel, IsLeaf)
  )

  DECLARE @tabExpTask TABLE(
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default
    UNIQUE(PlanID, TaskID)
  )

  DECLARE @tabConTask TABLE(
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default
    UNIQUE(PlanID, TaskID)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (UPDATE(ExpWBSLevel) OR UPDATE(ConWBSLevel))
    BEGIN

      SELECT 
        @strPlanID = I.PlanID,
        @strWBS1 = I.WBS1,
        @bitExpWBSLevelChanged = CASE WHEN ((I.ExpWBSLevel IS NULL AND D.ExpWBSLevel IS NOT NULL) OR (I.ExpWBSLevel IS NOT NULL AND D.ExpWBSLevel IS NULL) OR (I.ExpWBSLevel != D.ExpWBSLevel)) THEN 1 ELSE 0 END,
        @bitConWBSLevelChanged = CASE WHEN ((I.ConWBSLevel IS NULL AND D.ConWBSLevel IS NOT NULL) OR (I.ConWBSLevel IS NOT NULL AND D.ConWBSLevel IS NULL) OR (I.ConWBSLevel != D.ConWBSLevel)) THEN 1 ELSE 0 END,
        @tiExpWBSLevel = I.ExpWBSLevel,
        @tiConWBSLevel = I.ConWBSLevel,
        @strCalcExp = 'N',
        @strCalcCon = 'N'
        FROM INSERTED AS I, DELETED AS D

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabWBSTree (
        WBS1,
        WBS2,
        WBS3,
        WBSLevel,
        IsLeaf
      )
        SELECT
          WBS1,
          WBS2,
          WBS3,
          WBSLevel,
          IsLeaf
          FROM
            (SELECT
               WBS1, WBS2, WBS3,
               CASE 
                 WHEN WBS2 = ' ' AND WBS3 = ' ' THEN 1
                 WHEN WBS2 != ' ' AND WBS3 = ' ' THEN 2
                 WHEN WBS2 != ' ' AND WBS3 != ' ' THEN 3
               END AS WBSLevel,
               CASE WHEN SubLevel = 'N' THEN 1 ELSE 0 END AS IsLeaf
               FROM PR
               WHERE WBS1 = @strWBS1
            ) AS X

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      IF (@bitExpWBSLevelChanged = 1)
        BEGIN

          INSERT @tabExpTask (
            PlanID,
            TaskID
          )
            SELECT DISTINCT
              E.PlanID,
              E.TaskID
              FROM @tabWBSTree AS WT
                INNER JOIN PNExpense AS E ON WT.WBS1 = E.WBS1 AND WT.WBS2 = ISNULL(E.WBS2, ' ') AND WT.WBS3 = ISNULL(E.WBS3, ' ')
              WHERE E.PlanID = @strPlanID AND
                (WT.WBSLevel > @tiExpWBSLevel OR (WT.WBSLevel < @tiExpWBSLevel AND WT.IsLeaf = 0))

          --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
          DELETE PNPlannedExpenses 
            FROM PNPlannedExpenses AS TPD
              INNER JOIN @tabExpTask AS ET ON TPD.PlanID = ET.PlanID AND TPD.TaskID = ET.TaskID

          SET @strCalcExp = CASE WHEN @@ROWCOUNT > 0 THEN 'Y' ELSE @strCalcExp END

          DELETE PNExpense 
            FROM PNExpense AS E
              INNER JOIN @tabExpTask AS ET ON E.PlanID = ET.PlanID AND E.TaskID = ET.TaskID
 
        END /* IF (@bitExpWBSLevelChanged) */
 
      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

       IF (@bitConWBSLevelChanged = 1)
        BEGIN

          INSERT @tabConTask (
            PlanID,
            TaskID
          )
            SELECT DISTINCT
              C.PlanID,
              C.TaskID
              FROM @tabWBSTree AS WT
                INNER JOIN PNConsultant AS C ON WT.WBS1 = C.WBS1 AND WT.WBS2 = ISNULL(C.WBS2, ' ') AND WT.WBS3 = ISNULL(C.WBS3, ' ')
              WHERE C.PlanID = @strPlanID AND
                (WT.WBSLevel > @tiConWBSLevel OR (WT.WBSLevel < @tiConWBSLevel AND WT.IsLeaf = 0))

          --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
          DELETE PNPlannedConsultant 
            FROM PNPlannedConsultant AS TPD
              INNER JOIN @tabConTask AS ET ON TPD.PlanID = ET.PlanID AND TPD.TaskID = ET.TaskID

          SET @strCalcCon = CASE WHEN @@ROWCOUNT > 0 THEN 'Y' ELSE @strCalcCon END

          DELETE PNConsultant 
            FROM PNConsultant AS C
              INNER JOIN @tabConTask AS CT ON C.PlanID = CT.PlanID AND C.TaskID = CT.TaskID
 
        END /* IF (@bitConWBSLevelChanged) */

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Check to see if need to recalculate.

      IF (@strCalcExp = 'Y' OR @strCalcCon = 'Y')
        BEGIN
          EXECUTE dbo.PNDeleteSummaryTPD @strPlanID, 'N', @strCalcExp, @strCalcCon
          EXECUTE dbo.PNCalcTPD @strPlanID, 'N', @strCalcExp, @strCalcCon
          EXECUTE dbo.PNSumUpTPD @strPlanID, 'N', @strCalcExp, @strCalcCon
        END /* IF (@strCalcExp = 'Y' OR @strCalcCon = 'Y') */

    END /* IF (UPDATE(ExpWBSLevel) OR UPDATE(ConWBSLevel)) */
 
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
  SET NOCOUNT OFF

END
GO
ALTER TABLE [dbo].[PNPlan] ADD CONSTRAINT [PNPlanPK] PRIMARY KEY NONCLUSTERED ([PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNPlanOpportunityIDIDX] ON [dbo].[PNPlan] ([OpportunityID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNPlanUtilizationIncludeFlgIDX] ON [dbo].[PNPlan] ([UtilizationIncludeFlg]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNPlanWBS1IDX] ON [dbo].[PNPlan] ([WBS1]) INCLUDE ([PlanID], [LastPlanAction]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
