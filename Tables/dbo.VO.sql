CREATE TABLE [dbo].[VO]
(
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Voucher] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__VO_New__Period__7EC59195] DEFAULT ((0)),
[TransDate] [datetime] NULL,
[InvoiceDate] [datetime] NULL,
[Invoice] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LiabCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SelectedPostSeq] [int] NOT NULL CONSTRAINT [DF__VO_New__Selected__7FB9B5CE] DEFAULT ((0)),
[PaidPeriod] [int] NOT NULL CONSTRAINT [DF__VO_New__PaidPeri__00ADDA07] DEFAULT ((0)),
[SelectedPeriod] [int] NOT NULL CONSTRAINT [DF__VO_New__Selected__01A1FE40] DEFAULT ((0)),
[PayTerms] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentDate] [datetime] NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BarCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentCurrencyCodeModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentCurrencyCodeModDate] [datetime] NULL,
[CreditCardPrimaryCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VO] ADD CONSTRAINT [VOPK] PRIMARY KEY NONCLUSTERED ([Vendor], [Voucher]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [VOPaidPeriodIDX] ON [dbo].[VO] ([PaidPeriod]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [VOSelectedIDX] ON [dbo].[VO] ([SelectedPostSeq]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [VOVendorInvoiceInvoiceDateIDX] ON [dbo].[VO] ([Vendor], [Invoice], [InvoiceDate]) ON [PRIMARY]
GO
