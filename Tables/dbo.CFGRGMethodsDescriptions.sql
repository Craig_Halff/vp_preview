CREATE TABLE [dbo].[CFGRGMethodsDescriptions]
(
[Method] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGRGMethodsDescriptions] ADD CONSTRAINT [CFGRGMethodsDescriptionsPK] PRIMARY KEY CLUSTERED ([Method], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
