CREATE TABLE [dbo].[apDetailTax]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apDetailT__TaxAm__710A0D16] DEFAULT ((0)),
[TaxAmountProjectFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apDetailT__TaxAm__71FE314F] DEFAULT ((0)),
[CompoundOnTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__apDetailTax__Seq__72F25588] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[apDetailTax] ADD CONSTRAINT [apDetailTaxPK] PRIMARY KEY NONCLUSTERED ([Batch], [MasterPKey], [PKey], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
