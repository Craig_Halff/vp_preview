CREATE TABLE [dbo].[CCG_PAT_Renotify]
(
[ID] [uniqueidentifier] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PendingSeq] [int] NULL,
[PayableSeq] [int] NOT NULL,
[ItemDesc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Stage] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateChanged] [datetime] NOT NULL,
[ChangedBy] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateLastNotificationSent] [datetime] NULL,
[EmailSubject] [nvarchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailSubjectBatch] [nvarchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Historical records of reoccurring notifications', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Renotify', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The employee id of the one who last modified the underlying item (payable item or delegation record) that triggered the notification', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Renotify', 'COLUMN', N'ChangedBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date/time when the payable item was last modified', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Renotify', 'COLUMN', N'DateChanged'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date when the previous notification was sent for the pending item', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Renotify', 'COLUMN', N'DateLastNotificationSent'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The notification email body', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Renotify', 'COLUMN', N'EmailMessage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The notification email subject line', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Renotify', 'COLUMN', N'EmailSubject'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The subject line used for batch notifications', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Renotify', 'COLUMN', N'EmailSubjectBatch'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The employee id of the one receiving the notification', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Renotify', 'COLUMN', N'Employee'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Batch renotify id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Renotify', 'COLUMN', N'ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date/time when this notification record was added', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Renotify', 'COLUMN', N'InsertDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Summary of the notification item that triggered the notification (either payable item with the item number, or CCG_PAT_Delegation.Id)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Renotify', 'COLUMN', N'ItemDesc'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - The sequence number of the payable item to which this notification relates (if any). -1 for Pending Delegation notifications', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Renotify', 'COLUMN', N'PayableSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - The pending item sequence (CCG_PAT_Pending.Seq)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Renotify', 'COLUMN', N'PendingSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Either (1) the pending stage (CCG_PAT_Pending.Stage) for the item when this notification was sent, or (2) ''Pending Delegation''', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_Renotify', 'COLUMN', N'Stage'
GO
