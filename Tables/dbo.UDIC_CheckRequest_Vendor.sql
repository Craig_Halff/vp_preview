CREATE TABLE [dbo].[UDIC_CheckRequest_Vendor]
(
[UDIC_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustCRVVendorName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCRVAddress1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCRVAddress2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCRVCity] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCRVState] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCRVZip] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCRVPhone] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCRVVendor] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCRVPaymentTerms] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCRVStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCRVType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_UDIC_CheckRequest_Vendor]
      ON [dbo].[UDIC_CheckRequest_Vendor]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_CheckRequest_Vendor'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'UDIC_UID',CONVERT(NVARCHAR(2000),[UDIC_UID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Seq',CONVERT(NVARCHAR(2000),[Seq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCRVVendorName',CONVERT(NVARCHAR(2000),[CustCRVVendorName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCRVAddress1',CONVERT(NVARCHAR(2000),[CustCRVAddress1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCRVAddress2',CONVERT(NVARCHAR(2000),[CustCRVAddress2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCRVCity',CONVERT(NVARCHAR(2000),[CustCRVCity],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCRVState',CONVERT(NVARCHAR(2000),[CustCRVState],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCRVZip',CONVERT(NVARCHAR(2000),[CustCRVZip],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCRVPhone',CONVERT(NVARCHAR(2000),[CustCRVPhone],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCRVVendor',CONVERT(NVARCHAR(2000),[CustCRVVendor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCRVPaymentTerms',CONVERT(NVARCHAR(2000),[CustCRVPaymentTerms],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCRVStatus',CONVERT(NVARCHAR(2000),[CustCRVStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustCRVType',CONVERT(NVARCHAR(2000),[CustCRVType],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_UDIC_CheckRequest_Vendor]
      ON [dbo].[UDIC_CheckRequest_Vendor]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_CheckRequest_Vendor'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'UDIC_UID',NULL,CONVERT(NVARCHAR(2000),[UDIC_UID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',NULL,CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVVendorName',NULL,CONVERT(NVARCHAR(2000),[CustCRVVendorName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVAddress1',NULL,CONVERT(NVARCHAR(2000),[CustCRVAddress1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVAddress2',NULL,CONVERT(NVARCHAR(2000),[CustCRVAddress2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVCity',NULL,CONVERT(NVARCHAR(2000),[CustCRVCity],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVState',NULL,CONVERT(NVARCHAR(2000),[CustCRVState],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVZip',NULL,CONVERT(NVARCHAR(2000),[CustCRVZip],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVPhone',NULL,CONVERT(NVARCHAR(2000),[CustCRVPhone],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVVendor',NULL,CONVERT(NVARCHAR(2000),[CustCRVVendor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVPaymentTerms',NULL,CONVERT(NVARCHAR(2000),[CustCRVPaymentTerms],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVStatus',NULL,CONVERT(NVARCHAR(2000),[CustCRVStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVType',NULL,CONVERT(NVARCHAR(2000),[CustCRVType],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_UDIC_CheckRequest_Vendor]
      ON [dbo].[UDIC_CheckRequest_Vendor]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_CheckRequest_Vendor'
    
      If UPDATE([UDIC_UID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'UDIC_UID',
      CONVERT(NVARCHAR(2000),DELETED.[UDIC_UID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UDIC_UID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[UDIC_UID] Is Null And
				DELETED.[UDIC_UID] Is Not Null
			) Or
			(
				INSERTED.[UDIC_UID] Is Not Null And
				DELETED.[UDIC_UID] Is Null
			) Or
			(
				INSERTED.[UDIC_UID] !=
				DELETED.[UDIC_UID]
			)
		) 
		END		
		
      If UPDATE([Seq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',
      CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] Is Null And
				DELETED.[Seq] Is Not Null
			) Or
			(
				INSERTED.[Seq] Is Not Null And
				DELETED.[Seq] Is Null
			) Or
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
		
      If UPDATE([CustCRVVendorName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVVendorName',
      CONVERT(NVARCHAR(2000),DELETED.[CustCRVVendorName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCRVVendorName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCRVVendorName] Is Null And
				DELETED.[CustCRVVendorName] Is Not Null
			) Or
			(
				INSERTED.[CustCRVVendorName] Is Not Null And
				DELETED.[CustCRVVendorName] Is Null
			) Or
			(
				INSERTED.[CustCRVVendorName] !=
				DELETED.[CustCRVVendorName]
			)
		) 
		END		
		
      If UPDATE([CustCRVAddress1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVAddress1',
      CONVERT(NVARCHAR(2000),DELETED.[CustCRVAddress1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCRVAddress1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCRVAddress1] Is Null And
				DELETED.[CustCRVAddress1] Is Not Null
			) Or
			(
				INSERTED.[CustCRVAddress1] Is Not Null And
				DELETED.[CustCRVAddress1] Is Null
			) Or
			(
				INSERTED.[CustCRVAddress1] !=
				DELETED.[CustCRVAddress1]
			)
		) 
		END		
		
      If UPDATE([CustCRVAddress2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVAddress2',
      CONVERT(NVARCHAR(2000),DELETED.[CustCRVAddress2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCRVAddress2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCRVAddress2] Is Null And
				DELETED.[CustCRVAddress2] Is Not Null
			) Or
			(
				INSERTED.[CustCRVAddress2] Is Not Null And
				DELETED.[CustCRVAddress2] Is Null
			) Or
			(
				INSERTED.[CustCRVAddress2] !=
				DELETED.[CustCRVAddress2]
			)
		) 
		END		
		
      If UPDATE([CustCRVCity])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVCity',
      CONVERT(NVARCHAR(2000),DELETED.[CustCRVCity],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCRVCity],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCRVCity] Is Null And
				DELETED.[CustCRVCity] Is Not Null
			) Or
			(
				INSERTED.[CustCRVCity] Is Not Null And
				DELETED.[CustCRVCity] Is Null
			) Or
			(
				INSERTED.[CustCRVCity] !=
				DELETED.[CustCRVCity]
			)
		) 
		END		
		
      If UPDATE([CustCRVState])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVState',
      CONVERT(NVARCHAR(2000),DELETED.[CustCRVState],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCRVState],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCRVState] Is Null And
				DELETED.[CustCRVState] Is Not Null
			) Or
			(
				INSERTED.[CustCRVState] Is Not Null And
				DELETED.[CustCRVState] Is Null
			) Or
			(
				INSERTED.[CustCRVState] !=
				DELETED.[CustCRVState]
			)
		) 
		END		
		
      If UPDATE([CustCRVZip])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVZip',
      CONVERT(NVARCHAR(2000),DELETED.[CustCRVZip],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCRVZip],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCRVZip] Is Null And
				DELETED.[CustCRVZip] Is Not Null
			) Or
			(
				INSERTED.[CustCRVZip] Is Not Null And
				DELETED.[CustCRVZip] Is Null
			) Or
			(
				INSERTED.[CustCRVZip] !=
				DELETED.[CustCRVZip]
			)
		) 
		END		
		
      If UPDATE([CustCRVPhone])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVPhone',
      CONVERT(NVARCHAR(2000),DELETED.[CustCRVPhone],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCRVPhone],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCRVPhone] Is Null And
				DELETED.[CustCRVPhone] Is Not Null
			) Or
			(
				INSERTED.[CustCRVPhone] Is Not Null And
				DELETED.[CustCRVPhone] Is Null
			) Or
			(
				INSERTED.[CustCRVPhone] !=
				DELETED.[CustCRVPhone]
			)
		) 
		END		
		
      If UPDATE([CustCRVVendor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVVendor',
      CONVERT(NVARCHAR(2000),DELETED.[CustCRVVendor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCRVVendor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCRVVendor] Is Null And
				DELETED.[CustCRVVendor] Is Not Null
			) Or
			(
				INSERTED.[CustCRVVendor] Is Not Null And
				DELETED.[CustCRVVendor] Is Null
			) Or
			(
				INSERTED.[CustCRVVendor] !=
				DELETED.[CustCRVVendor]
			)
		) 
		END		
		
      If UPDATE([CustCRVPaymentTerms])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVPaymentTerms',
      CONVERT(NVARCHAR(2000),DELETED.[CustCRVPaymentTerms],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCRVPaymentTerms],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCRVPaymentTerms] Is Null And
				DELETED.[CustCRVPaymentTerms] Is Not Null
			) Or
			(
				INSERTED.[CustCRVPaymentTerms] Is Not Null And
				DELETED.[CustCRVPaymentTerms] Is Null
			) Or
			(
				INSERTED.[CustCRVPaymentTerms] !=
				DELETED.[CustCRVPaymentTerms]
			)
		) 
		END		
		
      If UPDATE([CustCRVStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVStatus',
      CONVERT(NVARCHAR(2000),DELETED.[CustCRVStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCRVStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCRVStatus] Is Null And
				DELETED.[CustCRVStatus] Is Not Null
			) Or
			(
				INSERTED.[CustCRVStatus] Is Not Null And
				DELETED.[CustCRVStatus] Is Null
			) Or
			(
				INSERTED.[CustCRVStatus] !=
				DELETED.[CustCRVStatus]
			)
		) 
		END		
		
      If UPDATE([CustCRVType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustCRVType',
      CONVERT(NVARCHAR(2000),DELETED.[CustCRVType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCRVType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustCRVType] Is Null And
				DELETED.[CustCRVType] Is Not Null
			) Or
			(
				INSERTED.[CustCRVType] Is Not Null And
				DELETED.[CustCRVType] Is Null
			) Or
			(
				INSERTED.[CustCRVType] !=
				DELETED.[CustCRVType]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[UDIC_CheckRequest_Vendor] ADD CONSTRAINT [UDIC_CheckRequest_VendorPK] PRIMARY KEY CLUSTERED ([UDIC_UID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
