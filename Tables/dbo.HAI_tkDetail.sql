CREATE TABLE [dbo].[HAI_tkDetail]
(
[EndDate] [datetime] NOT NULL,
[Employee] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeCompany] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__HAI_tkDetai__Seq__3009B415] DEFAULT ((0)),
[TransDate] [datetime] NOT NULL,
[Category] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LaborCode] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__RegHr__30FDD84E] DEFAULT ((0)),
[OvtHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__OvtHr__31F1FC87] DEFAULT ((0)),
[SpecialOvtHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__Speci__32E620C0] DEFAULT ((0)),
[TransComment] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillCategory] [smallint] NOT NULL CONSTRAINT [DF__HAI_tkDet__BillC__33DA44F9] DEFAULT ((0)),
[LineItemApprovedBy] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Locale] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__RegAm__34CE6932] DEFAULT ((0)),
[OvtAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__OvtAm__35C28D6B] DEFAULT ((0)),
[SpecialOvtAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__Speci__36B6B1A4] DEFAULT ((0)),
[ExchangeInfo] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegAmtProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__RegAm__37AAD5DD] DEFAULT ((0)),
[OvtAmtProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__OvtAm__389EFA16] DEFAULT ((0)),
[SpecialOvtAmtProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__Speci__39931E4F] DEFAULT ((0)),
[ProjectExchangeInfo] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillExt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__BillE__3A874288] DEFAULT ((0)),
[OvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__OvtPc__3B7B66C1] DEFAULT ((0)),
[SpecialOvtPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__Speci__3C6F8AFA] DEFAULT ((0)),
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDeta__Rate__3D63AF33] DEFAULT ((0)),
[OvtRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__OvtRa__3E57D36C] DEFAULT ((0)),
[SpecialOvtRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__Speci__3F4BF7A5] DEFAULT ((0)),
[RateProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__RateP__40401BDE] DEFAULT ((0)),
[OvtRateProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__OvtRa__41344017] DEFAULT ((0)),
[SpecialOvtRateProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__HAI_tkDet__Speci__42286450] DEFAULT ((0)),
[StartDateTime] [datetime] NULL,
[EndDateTime] [datetime] NULL,
[LineItemApprovalStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LineItemApprover] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Id] [bigint] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HAI_tkDetail] ADD CONSTRAINT [PK_HAI_tkDetail] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
