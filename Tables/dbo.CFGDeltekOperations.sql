CREATE TABLE [dbo].[CFGDeltekOperations]
(
[LastUpdated] [datetime] NULL,
[VisionUpgrade] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGDeltek__Visio__45E2E54B] DEFAULT ('N'),
[VisionUpgradeDate] [datetime] NULL
) ON [PRIMARY]
GO
