CREATE TABLE [dbo].[billIntDetail]
(
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MainWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecdSeq] [int] NOT NULL CONSTRAINT [DF__billIntDe__RecdS__2ACC9A57] DEFAULT ((0)),
[BillWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Interest] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billIntDe__Inter__2BC0BE90] DEFAULT ((0)),
[Balance] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billIntDe__Balan__2CB4E2C9] DEFAULT ((0)),
[GracePeriod] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billIntDe__Grace__2DA90702] DEFAULT ((0)),
[Rate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__billIntDet__Rate__2E9D2B3B] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[billIntDetail] ADD CONSTRAINT [billIntDetailPK] PRIMARY KEY NONCLUSTERED ([Invoice], [MainWBS1], [RecdSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
