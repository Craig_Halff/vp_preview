CREATE TABLE [dbo].[CFGFARepairTypeData]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InUse] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGFARepa__InUse__1240E3AF] DEFAULT ('Y')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGFARepairTypeData] ADD CONSTRAINT [CFGFARepairTypeDataPK] PRIMARY KEY CLUSTERED ([Code], [Company]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
