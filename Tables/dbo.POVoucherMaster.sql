CREATE TABLE [dbo].[POVoucherMaster]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Address] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayTerms] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentDate] [datetime] NULL,
[Voucher] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InvoiceDate] [datetime] NOT NULL,
[InvoiceAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__POVoucher__Invoi__25FE8EF0] DEFAULT ((0)),
[MatchCode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APMasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POVoucher__Poste__26F2B329] DEFAULT ('N'),
[LiabCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyExchangeOverrideMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POVoucher__Curre__27E6D762] DEFAULT ('N'),
[CurrencyExchangeOverrideDate] [datetime] NULL,
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__POVoucher__Curre__28DAFB9B] DEFAULT ((0)),
[PaymentExchangeOverrideMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POVoucher__Payme__29CF1FD4] DEFAULT ('N'),
[PaymentExchangeOverrideDate] [datetime] NULL,
[PaymentExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__POVoucher__Payme__2AC3440D] DEFAULT ((0)),
[VoucherDate] [datetime] NULL,
[SelectedPeriod] [int] NOT NULL CONSTRAINT [DF__POVoucher__Selec__2BB76846] DEFAULT ((0)),
[SelectedPostSeq] [int] NOT NULL CONSTRAINT [DF__POVoucher__Selec__2CAB8C7F] DEFAULT ((0)),
[PaymentCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POVoucherMaster] ADD CONSTRAINT [POVoucherMasterPK] PRIMARY KEY NONCLUSTERED ([MasterPKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
