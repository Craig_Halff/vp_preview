CREATE TABLE [dbo].[CFGEmployeeStatusData]
(
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGEmployeeStatusData] ADD CONSTRAINT [CFGEmployeeStatusDataPK] PRIMARY KEY CLUSTERED ([Status]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
