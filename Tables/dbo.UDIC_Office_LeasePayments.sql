CREATE TABLE [dbo].[UDIC_Office_LeasePayments]
(
[UDIC_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustLeasePaymentDate] [datetime] NULL,
[CustLeasePaymentAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLeasePaymentAmount] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_Offi__CustL__30C7017D] DEFAULT ((0)),
[CustLeasePaymentComments] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustLeaseHolder] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_UDIC_Office_LeasePayments]
      ON [dbo].[UDIC_Office_LeasePayments]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_Office_LeasePayments'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'UDIC_UID',CONVERT(NVARCHAR(2000),[UDIC_UID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'Seq',CONVERT(NVARCHAR(2000),[Seq],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustLeasePaymentDate',CONVERT(NVARCHAR(2000),[CustLeasePaymentDate],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustLeasePaymentAccount',CONVERT(NVARCHAR(2000),DELETED.[CustLeasePaymentAccount],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join CA as oldDesc   on DELETED.CustLeasePaymentAccount = oldDesc.Account

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustLeasePaymentAmount',CONVERT(NVARCHAR(2000),[CustLeasePaymentAmount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustLeasePaymentComments',CONVERT(NVARCHAR(2000),[CustLeasePaymentComments],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Seq],121),'CustLeaseHolder',CONVERT(NVARCHAR(2000),DELETED.[CustLeaseHolder],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join VE as oldDesc   on DELETED.CustLeaseHolder = oldDesc.Vendor

      
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_UDIC_Office_LeasePayments]
      ON [dbo].[UDIC_Office_LeasePayments]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_Office_LeasePayments'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'UDIC_UID',NULL,CONVERT(NVARCHAR(2000),[UDIC_UID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',NULL,CONVERT(NVARCHAR(2000),[Seq],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustLeasePaymentDate',NULL,CONVERT(NVARCHAR(2000),[CustLeasePaymentDate],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustLeasePaymentAccount',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustLeasePaymentAccount],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  CA as newDesc  on INSERTED.CustLeasePaymentAccount = newDesc.Account

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustLeasePaymentAmount',NULL,CONVERT(NVARCHAR(2000),[CustLeasePaymentAmount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustLeasePaymentComments',NULL,CONVERT(NVARCHAR(2000),[CustLeasePaymentComments],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustLeaseHolder',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustLeaseHolder],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  VE as newDesc  on INSERTED.CustLeaseHolder = newDesc.Vendor

     
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_UDIC_Office_LeasePayments]
      ON [dbo].[UDIC_Office_LeasePayments]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()
      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)

      If @VisionAuditUser = ''
      return

      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UDIC_Office_LeasePayments'
    
      If UPDATE([UDIC_UID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'UDIC_UID',
      CONVERT(NVARCHAR(2000),DELETED.[UDIC_UID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UDIC_UID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[UDIC_UID] Is Null And
				DELETED.[UDIC_UID] Is Not Null
			) Or
			(
				INSERTED.[UDIC_UID] Is Not Null And
				DELETED.[UDIC_UID] Is Null
			) Or
			(
				INSERTED.[UDIC_UID] !=
				DELETED.[UDIC_UID]
			)
		) 
		END		
		
      If UPDATE([Seq])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'Seq',
      CONVERT(NVARCHAR(2000),DELETED.[Seq],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Seq],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[Seq] Is Null And
				DELETED.[Seq] Is Not Null
			) Or
			(
				INSERTED.[Seq] Is Not Null And
				DELETED.[Seq] Is Null
			) Or
			(
				INSERTED.[Seq] !=
				DELETED.[Seq]
			)
		) 
		END		
		
      If UPDATE([CustLeasePaymentDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustLeasePaymentDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustLeasePaymentDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLeasePaymentDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustLeasePaymentDate] Is Null And
				DELETED.[CustLeasePaymentDate] Is Not Null
			) Or
			(
				INSERTED.[CustLeasePaymentDate] Is Not Null And
				DELETED.[CustLeasePaymentDate] Is Null
			) Or
			(
				INSERTED.[CustLeasePaymentDate] !=
				DELETED.[CustLeasePaymentDate]
			)
		) 
		END		
		
     If UPDATE([CustLeasePaymentAccount])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustLeasePaymentAccount',
     CONVERT(NVARCHAR(2000),DELETED.[CustLeasePaymentAccount],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustLeasePaymentAccount],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustLeasePaymentAccount] Is Null And
				DELETED.[CustLeasePaymentAccount] Is Not Null
			) Or
			(
				INSERTED.[CustLeasePaymentAccount] Is Not Null And
				DELETED.[CustLeasePaymentAccount] Is Null
			) Or
			(
				INSERTED.[CustLeasePaymentAccount] !=
				DELETED.[CustLeasePaymentAccount]
			)
		) left join CA as oldDesc  on DELETED.CustLeasePaymentAccount = oldDesc.Account  left join  CA as newDesc  on INSERTED.CustLeasePaymentAccount = newDesc.Account
		END		
		
      If UPDATE([CustLeasePaymentAmount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustLeasePaymentAmount',
      CONVERT(NVARCHAR(2000),DELETED.[CustLeasePaymentAmount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLeasePaymentAmount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustLeasePaymentAmount] Is Null And
				DELETED.[CustLeasePaymentAmount] Is Not Null
			) Or
			(
				INSERTED.[CustLeasePaymentAmount] Is Not Null And
				DELETED.[CustLeasePaymentAmount] Is Null
			) Or
			(
				INSERTED.[CustLeasePaymentAmount] !=
				DELETED.[CustLeasePaymentAmount]
			)
		) 
		END		
		
      If UPDATE([CustLeasePaymentComments])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustLeasePaymentComments',
      CONVERT(NVARCHAR(2000),DELETED.[CustLeasePaymentComments],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustLeasePaymentComments],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustLeasePaymentComments] Is Null And
				DELETED.[CustLeasePaymentComments] Is Not Null
			) Or
			(
				INSERTED.[CustLeasePaymentComments] Is Not Null And
				DELETED.[CustLeasePaymentComments] Is Null
			) Or
			(
				INSERTED.[CustLeasePaymentComments] !=
				DELETED.[CustLeasePaymentComments]
			)
		) 
		END		
		
     If UPDATE([CustLeaseHolder])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Seq],121),'CustLeaseHolder',
     CONVERT(NVARCHAR(2000),DELETED.[CustLeaseHolder],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustLeaseHolder],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND INSERTED.[Seq] = DELETED.[Seq] AND 
		(
			(
				INSERTED.[CustLeaseHolder] Is Null And
				DELETED.[CustLeaseHolder] Is Not Null
			) Or
			(
				INSERTED.[CustLeaseHolder] Is Not Null And
				DELETED.[CustLeaseHolder] Is Null
			) Or
			(
				INSERTED.[CustLeaseHolder] !=
				DELETED.[CustLeaseHolder]
			)
		) left join VE as oldDesc  on DELETED.CustLeaseHolder = oldDesc.Vendor  left join  VE as newDesc  on INSERTED.CustLeaseHolder = newDesc.Vendor
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[UDIC_Office_LeasePayments] ADD CONSTRAINT [UDIC_Office_LeasePaymentsPK] PRIMARY KEY CLUSTERED ([UDIC_UID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
