CREATE TABLE [dbo].[CCG_EI_ConfigStages]
(
[Stage] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StageLabel] [varchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StageDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceType] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RightDoc] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequireStamp] [int] NULL,
[Weight] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_ConfigStages] ADD CONSTRAINT [PK_CCG_EI_ConfigStages] PRIMARY KEY CLUSTERED ([Stage]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'The set of all Stages available within the EI application', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStages', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'(Optional) The specific type of invoice this Stage should apply to (Final / Draft)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStages', 'COLUMN', N'InvoiceType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'(Optional) Stamp to apply to invoice document when the stage is set', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStages', 'COLUMN', N'RequireStamp'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Option for type of document to display in the right PDF view for this stage', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStages', 'COLUMN', N'RightDoc'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stage name (PK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStages', 'COLUMN', N'Stage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Description for stage, used in reporting / informational only', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStages', 'COLUMN', N'StageDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The default display label to use for this Stage', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStages', 'COLUMN', N'StageLabel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Status of the Stage - [A]ctive / [I]nactive', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStages', 'COLUMN', N'Status'
GO
