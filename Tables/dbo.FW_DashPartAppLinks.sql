CREATE TABLE [dbo].[FW_DashPartAppLinks]
(
[ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Dashpart_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDivID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_DashPartAppLinks] ADD CONSTRAINT [FW_DashPartAppLinksPK] PRIMARY KEY NONCLUSTERED ([ID], [Dashpart_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
