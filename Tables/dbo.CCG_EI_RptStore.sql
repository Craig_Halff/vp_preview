CREATE TABLE [dbo].[CCG_EI_RptStore]
(
[RptStoreId] [uniqueidentifier] NOT NULL,
[BeginDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[BeginStage] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EndStage] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Col1Field] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col1Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col1Emp] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col1Value] [float] NULL,
[Col2Field] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col2Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col2Emp] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col2Value] [float] NULL,
[Col3Field] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col3Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col3Emp] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col3Value] [float] NULL,
[Col4Field] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col4Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col4Emp] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col4Value] [float] NULL,
[Col5Field] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col5Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col5Emp] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col5Value] [float] NULL,
[Col6Field] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col6Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col6Emp] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Col6Value] [float] NULL
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [IDX_CCG_EI_RptStore] ON [dbo].[CCG_EI_RptStore] ([RptStoreId]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / BeginDate Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'BeginDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / BeginStage Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'BeginStage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col1Emp Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col1Emp'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col1Field Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col1Field'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col1Name Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col1Name'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col1Value Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col1Value'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col2Emp Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col2Emp'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col2Field Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col2Field'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col2Name Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col2Name'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col2Value Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col2Value'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col3Emp Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col3Emp'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col3Field Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col3Field'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col3Name Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col3Name'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col3Value Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col3Value'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col4Emp Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col4Emp'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col4Field Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col4Field'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col4Name Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col4Name'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col4Value Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col4Value'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col5Emp Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col5Emp'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col5Field Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col5Field'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col5Name Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col5Name'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col5Value Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col5Value'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col6Emp Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col6Emp'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col6Field Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col6Field'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col6Name Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col6Name'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / Col6Value Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'Col6Value'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / EndDate Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'EndDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / EndStage Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'EndStage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_RptStore / RptStoreId Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'RptStoreId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*<CCG_EI_RptStore / WBS1 Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_RptStore', 'COLUMN', N'WBS1'
GO
