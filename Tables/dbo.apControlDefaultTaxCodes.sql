CREATE TABLE [dbo].[apControlDefaultTaxCodes]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__apControlDe__Seq__65985A6A] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[apControlDefaultTaxCodes] ADD CONSTRAINT [apControlDefaultTaxCodesPK] PRIMARY KEY NONCLUSTERED ([Batch], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
