CREATE TABLE [dbo].[LedgerAP]
(
[Period] [int] NOT NULL CONSTRAINT [DF__LedgerAP___Perio__0B0AA192] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__LedgerAP___PostS__0BFEC5CB] DEFAULT ((0)),
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransDate] [datetime] NULL,
[Desc1] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Desc2] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___Amoun__0CF2EA04] DEFAULT ((0)),
[CBAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___CBAmo__0DE70E3D] DEFAULT ((0)),
[BillExt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___BillE__0EDB3276] DEFAULT ((0)),
[ProjectCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerAP___Proje__0FCF56AF] DEFAULT ('N'),
[AutoEntry] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerAP___AutoE__10C37AE8] DEFAULT ('N'),
[SuppressBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerAP___Suppr__11B79F21] DEFAULT ('N'),
[BillStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SkipGL] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerAP___SkipG__12ABC35A] DEFAULT ('N'),
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceSection] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Line] [smallint] NOT NULL CONSTRAINT [DF__LedgerAP_N__Line__139FE793] DEFAULT ((0)),
[PartialPayment] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___Parti__14940BCC] DEFAULT ((0)),
[Discount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___Disco__15883005] DEFAULT ((0)),
[Voucher] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledInvoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledPeriod] [int] NOT NULL CONSTRAINT [DF__LedgerAP___Bille__167C543E] DEFAULT ((0)),
[Unit] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitTable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitQuantity] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___UnitQ__17707877] DEFAULT ((0)),
[UnitCostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___UnitC__18649CB0] DEFAULT ((0)),
[UnitBillingRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___UnitB__1958C0E9] DEFAULT ((0)),
[UnitBillExt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___UnitB__1A4CE522] DEFAULT ((0)),
[XferWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XferAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___TaxBa__1B41095B] DEFAULT ((0)),
[TaxCBBasis] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___TaxCB__1C352D94] DEFAULT ((0)),
[BillTaxCodeOverride] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WrittenOffPeriod] [int] NOT NULL CONSTRAINT [DF__LedgerAP___Writt__1D2951CD] DEFAULT ((0)),
[TransactionAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___Trans__1E1D7606] DEFAULT ((0)),
[TransactionCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___Amoun__1F119A3F] DEFAULT ((0)),
[ProjectExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___Amoun__2005BE78] DEFAULT ((0)),
[BillingExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoEntryAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___AutoE__20F9E2B1] DEFAULT ((0)),
[AutoEntryExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoEntryOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoEntryAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountSourceCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___Amoun__21EE06EA] DEFAULT ((0)),
[SourceExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PONumber] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitCostRateBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___UnitC__22E22B23] DEFAULT ((0)),
[LinkCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillTax2CodeOverride] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GainsAndLossesType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___Amoun__23D64F5C] DEFAULT ((0)),
[CBAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___CBAmo__24CA7395] DEFAULT ((0)),
[TaxBasisTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___TaxBa__25BE97CE] DEFAULT ((0)),
[TaxCBBasisTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___TaxCB__26B2BC07] DEFAULT ((0)),
[TaxBasisFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___TaxBa__27A6E040] DEFAULT ((0)),
[TaxCBBasisFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___TaxCB__289B0479] DEFAULT ((0)),
[DiscountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___Disco__298F28B2] DEFAULT ((0)),
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RealizationAmountEmployeeCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___Reali__2A834CEB] DEFAULT ((0)),
[RealizationAmountProjectCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___Reali__2B777124] DEFAULT ((0)),
[RealizationAmountBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___Reali__2C6B955D] DEFAULT ((0)),
[NonBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__LedgerAP___NonBi__2D5FB996] DEFAULT ('N'),
[CreditMemoRefNo] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalAmountSourceCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerAP___Origi__2E53DDCF] DEFAULT ((0)),
[OriginalPaymentCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InProcessAccount] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerAP___InPro__2F480208] DEFAULT ('N'),
[InProcessAccountCleared] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerAP___InPro__303C2641] DEFAULT ('N'),
[EKOriginalLine] [smallint] NOT NULL CONSTRAINT [DF__LedgerAP___EKOri__31304A7A] DEFAULT ((0)),
[InvoiceStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginatingVendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Diary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaryNo] [int] NOT NULL CONSTRAINT [DF__LedgerAP___Diary__32246EB3] DEFAULT ((0)),
[CreditCardPrimaryCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransferredPeriod] [int] NOT NULL CONSTRAINT [DF__LedgerAP___Trans__331892EC] DEFAULT ((0)),
[TransferredBillStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreInvoice] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EquipmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BookCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmOrg] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastPSAExportDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LedgerAP] ADD CONSTRAINT [LedgerAPPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerAPAccountIDX] ON [dbo].[LedgerAP] ([Account]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerAPBilledWBS1WBS2WBS3IDX] ON [dbo].[LedgerAP] ([BilledWBS1], [BilledWBS2], [BilledWBS3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerAPBillStatusIDX] ON [dbo].[LedgerAP] ([BillStatus]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerAPEmployeeVoucherIDX] ON [dbo].[LedgerAP] ([Employee], [Voucher]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerAPEquipIDBookCodeIDX] ON [dbo].[LedgerAP] ([EquipmentID], [BookCode]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerAPInvoiceIDX] ON [dbo].[LedgerAP] ([Invoice]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerAPSearchIDX] ON [dbo].[LedgerAP] ([TransType], [Period], [PostSeq], [WBS1], [WBS2], [WBS3], [RefNo], [BankCode], [Vendor], [Voucher], [SubType]) INCLUDE ([TransactionAmount], [AmountSourceCurrency]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerAPTransTypeSubTypeIDX] ON [dbo].[LedgerAP] ([TransType], [SubType]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerAPVendorVoucherIDX] ON [dbo].[LedgerAP] ([Vendor], [Voucher]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerAPWBS1AccountIDX] ON [dbo].[LedgerAP] ([WBS1], [Account]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerAPWBS1WBS2WBS3IDX] ON [dbo].[LedgerAP] ([WBS1], [WBS2], [WBS3]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [LedgerAPCoveringIDX] ON [dbo].[LedgerAP] ([WBS1], [WBS2], [WBS3], [Account], [Vendor], [Employee], [Unit], [UnitTable], [TransDate], [ProjectCost], [TransType]) ON [PRIMARY]
GO
