CREATE TABLE [dbo].[RPGrdCol]
(
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColSeq] [smallint] NOT NULL CONSTRAINT [DF__RPGrdCol___ColSe__16922E2E] DEFAULT ((0)),
[Label] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VisibleFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPGrdCol___Visib__17865267] DEFAULT ('Y'),
[DataCol] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Width] [smallint] NOT NULL CONSTRAINT [DF__RPGrdCol___Width__187A76A0] DEFAULT ((0)),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPGrdCol] ADD CONSTRAINT [RPGrdColPK] PRIMARY KEY NONCLUSTERED ([UserName], [GridName], [ColName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
