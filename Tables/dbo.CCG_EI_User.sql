CREATE TABLE [dbo].[CCG_EI_User]
(
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OkdLicense] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_User] ADD CONSTRAINT [PK_CCG_EI_User] PRIMARY KEY CLUSTERED ([Username]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'EI Users', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_User', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Was the license agreement accepted? Y / N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_User', 'COLUMN', N'OkdLicense'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Vision user with access to PAT (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_User', 'COLUMN', N'Username'
GO
