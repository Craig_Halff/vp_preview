CREATE TABLE [dbo].[apDetail]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__apDetail_Ne__Seq__6780A2DC] DEFAULT ((0)),
[Description] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apDetail___Amoun__6874C715] DEFAULT ((0)),
[SuppressBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__apDetail___Suppr__6968EB4E] DEFAULT ('N'),
[NetAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apDetail___NetAm__6A5D0F87] DEFAULT ((0)),
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__apDetail___Curre__6B5133C0] DEFAULT ((0)),
[PONumber] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentExchangeRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__apDetail___Payme__6C4557F9] DEFAULT ((0)),
[PaymentAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apDetail___Payme__6D397C32] DEFAULT ((0)),
[PaymentExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExpenseCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountProjectFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__apDetail___Amoun__6E2DA06B] DEFAULT ((0)),
[OriginatingVendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateAsset] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__apDetail___Creat__6F21C4A4] DEFAULT ('N'),
[AssetType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EquipmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Item] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[apDetail] ADD CONSTRAINT [apDetailPK] PRIMARY KEY NONCLUSTERED ([Batch], [MasterPKey], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
