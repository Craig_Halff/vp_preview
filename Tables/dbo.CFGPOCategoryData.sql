CREATE TABLE [dbo].[CFGPOCategoryData]
(
[Category] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CFGPOCateg__Type__5B45B480] DEFAULT ('M')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGPOCategoryData] ADD CONSTRAINT [CFGPOCategoryDataPK] PRIMARY KEY CLUSTERED ([Category]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
