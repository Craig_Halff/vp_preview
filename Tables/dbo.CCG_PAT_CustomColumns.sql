CREATE TABLE [dbo].[CCG_PAT_CustomColumns]
(
[PayableSeq] [int] NOT NULL,
[Comments] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments_ModEmp] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_CustomColumns] ADD CONSTRAINT [CCG_PAT_CustomColumnsPK] PRIMARY KEY NONCLUSTERED ([PayableSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Current values of custom input columns. (Any number of custom columns are to be expected in this table.)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_CustomColumns', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - The payable seq number associated with this content', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_CustomColumns', 'COLUMN', N'PayableSeq'
GO
