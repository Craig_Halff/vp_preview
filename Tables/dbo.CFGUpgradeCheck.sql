CREATE TABLE [dbo].[CFGUpgradeCheck]
(
[Version] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Accounting] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Planning] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PSA] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeExpense] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CRM] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
