CREATE TABLE [dbo].[WorkflowConditions]
(
[ConditionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColumnName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Operator] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExpectedValue] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExpectedValueDescription] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConditionOrder] [int] NOT NULL CONSTRAINT [DF__WorkflowC__Condi__3615C67F] DEFAULT ((0)),
[ConditionOperator] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SQLExpression] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowConditions] ADD CONSTRAINT [WorkflowConditionsPK] PRIMARY KEY NONCLUSTERED ([ConditionID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [WorkflowConditionsIDIDX] ON [dbo].[WorkflowConditions] ([ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
