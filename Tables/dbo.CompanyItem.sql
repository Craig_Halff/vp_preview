CREATE TABLE [dbo].[CompanyItem]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Item] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Category] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Substitute] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UOM] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastVendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrimaryVendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InventoryInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CompanyIt__Inven__316F7645] DEFAULT ('N'),
[InventoryUOM] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InventoryReorderPoint] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CompanyIt__Inven__32639A7E] DEFAULT ((0)),
[Location] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CompanyItem] ADD CONSTRAINT [CompanyItemPK] PRIMARY KEY NONCLUSTERED ([Company], [Item]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
