CREATE TABLE [dbo].[Clendor]
(
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Client] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__Status__DefA] DEFAULT ('A'),
[ExportInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__ExportI__041521EE] DEFAULT ('N'),
[WebSite] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Memo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__ClientI__05094627] DEFAULT ('Y'),
[VendorInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__VendorI__05FD6A60] DEFAULT ('N'),
[PriorWork] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__PriorWo__06F18E99] DEFAULT ('N'),
[Recommend] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__Recomme__07E5B2D2] DEFAULT ('N'),
[DisadvBusiness] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__DisadvB__08D9D70B] DEFAULT ('N'),
[DisabledVetOwnedSmallBusiness] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__Disable__09CDFB44] DEFAULT ('N'),
[HBCU] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__HBCU__0AC21F7D] DEFAULT ('N'),
[MinorityBusiness] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__Minorit__0BB643B6] DEFAULT ('N'),
[SmallBusiness] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__SmallBu__0CAA67EF] DEFAULT ('N'),
[VetOwnedSmallBusiness] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__VetOwne__0D9E8C28] DEFAULT ('N'),
[WomanOwned] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__WomanOw__0E92B061] DEFAULT ('N'),
[AlaskaNative] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__AlaskaN__0F86D49A] DEFAULT ('N'),
[SpecialtyType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Specialty] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentLevel1] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentLevel2] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentLevel3] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentLevel4] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employees] [int] NOT NULL CONSTRAINT [DF__Clendor__Employe__107AF8D3] DEFAULT ((0)),
[AnnualRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Clendor__AnnualR__116F1D0C] DEFAULT ((0)),
[GovernmentAgency] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__Governm__12634145] DEFAULT ('N'),
[Competitor] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__Competi__1357657E] DEFAULT ('N'),
[EightA] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__EightA__144B89B7] DEFAULT ('N'),
[Hubzone] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__Hubzone__153FADF0] DEFAULT ('N'),
[IQID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Incumbent] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__Incumbe__1633D229] DEFAULT ('N'),
[AjeraSync] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__AjeraSy__1727F662] DEFAULT ('N'),
[TLInternalKey] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLSyncModDate] [datetime] NULL,
[Owner] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Category] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AvailableForCRM] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__AvailableForCRM__DefY] DEFAULT ('Y'),
[ReadyForApproval] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__ReadyFo__19103ED4] DEFAULT ('N'),
[ReadyForProcessing] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Clendor__ReadyFo__1A04630D] DEFAULT ('N'),
[FedID] [nvarchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Market] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TypeOfTIN] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFID] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFLastModifiedDate] [datetime] NULL,
[QBOID] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QBOLastUpdated] [datetime] NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_Clendor]
      ON [dbo].[Clendor]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Clendor'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'ClientID',CONVERT(NVARCHAR(2000),[ClientID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Client',CONVERT(NVARCHAR(2000),[Client],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Name',CONVERT(NVARCHAR(2000),[Name],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Type',CONVERT(NVARCHAR(2000),[Type],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Status',CONVERT(NVARCHAR(2000),[Status],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'ExportInd',CONVERT(NVARCHAR(2000),[ExportInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'WebSite',CONVERT(NVARCHAR(2000),[WebSite],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Memo','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'CurrentStatus',CONVERT(NVARCHAR(2000),[CurrentStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'CustomCurrencyCode',CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'ClientInd',CONVERT(NVARCHAR(2000),[ClientInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'VendorInd',CONVERT(NVARCHAR(2000),[VendorInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'PriorWork',CONVERT(NVARCHAR(2000),[PriorWork],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Recommend',CONVERT(NVARCHAR(2000),[Recommend],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'DisadvBusiness',CONVERT(NVARCHAR(2000),[DisadvBusiness],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'DisabledVetOwnedSmallBusiness',CONVERT(NVARCHAR(2000),[DisabledVetOwnedSmallBusiness],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'HBCU',CONVERT(NVARCHAR(2000),[HBCU],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'MinorityBusiness',CONVERT(NVARCHAR(2000),[MinorityBusiness],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'SmallBusiness',CONVERT(NVARCHAR(2000),[SmallBusiness],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'VetOwnedSmallBusiness',CONVERT(NVARCHAR(2000),[VetOwnedSmallBusiness],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'WomanOwned',CONVERT(NVARCHAR(2000),[WomanOwned],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'AlaskaNative',CONVERT(NVARCHAR(2000),[AlaskaNative],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'SpecialtyType',CONVERT(NVARCHAR(2000),[SpecialtyType],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Specialty','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'ParentID',CONVERT(NVARCHAR(2000),[ParentID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'ParentLevel1',CONVERT(NVARCHAR(2000),[ParentLevel1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'ParentLevel2',CONVERT(NVARCHAR(2000),[ParentLevel2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'ParentLevel3',CONVERT(NVARCHAR(2000),[ParentLevel3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'ParentLevel4',CONVERT(NVARCHAR(2000),[ParentLevel4],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Employees',CONVERT(NVARCHAR(2000),[Employees],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'AnnualRevenue',CONVERT(NVARCHAR(2000),[AnnualRevenue],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'GovernmentAgency',CONVERT(NVARCHAR(2000),[GovernmentAgency],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Competitor',CONVERT(NVARCHAR(2000),[Competitor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'EightA',CONVERT(NVARCHAR(2000),[EightA],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Hubzone',CONVERT(NVARCHAR(2000),[Hubzone],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'IQID',CONVERT(NVARCHAR(2000),[IQID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Incumbent',CONVERT(NVARCHAR(2000),[Incumbent],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'AjeraSync',CONVERT(NVARCHAR(2000),[AjeraSync],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'TLInternalKey',CONVERT(NVARCHAR(2000),[TLInternalKey],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'TLSyncModDate',CONVERT(NVARCHAR(2000),[TLSyncModDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Owner',CONVERT(NVARCHAR(2000),[Owner],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Vendor',CONVERT(NVARCHAR(2000),[Vendor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'SortName',CONVERT(NVARCHAR(2000),[SortName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Category',CONVERT(NVARCHAR(2000),[Category],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'AvailableForCRM',CONVERT(NVARCHAR(2000),[AvailableForCRM],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'ReadyForApproval',CONVERT(NVARCHAR(2000),[ReadyForApproval],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'ReadyForProcessing',CONVERT(NVARCHAR(2000),[ReadyForProcessing],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'FedID',CONVERT(NVARCHAR(2000),[FedID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Org',CONVERT(NVARCHAR(2000),[Org],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'Market',CONVERT(NVARCHAR(2000),[Market],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'TypeOfTIN',CONVERT(NVARCHAR(2000),[TypeOfTIN],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'SFID',CONVERT(NVARCHAR(2000),[SFID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'SFLastModifiedDate',CONVERT(NVARCHAR(2000),[SFLastModifiedDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'QBOID',CONVERT(NVARCHAR(2000),[QBOID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[ClientID],121),'QBOLastUpdated',CONVERT(NVARCHAR(2000),[QBOLastUpdated],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_Clendor]
      ON [dbo].[Clendor]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Clendor'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ClientID',NULL,CONVERT(NVARCHAR(2000),[ClientID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Client',NULL,CONVERT(NVARCHAR(2000),[Client],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Name',NULL,CONVERT(NVARCHAR(2000),[Name],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Type',NULL,CONVERT(NVARCHAR(2000),[Type],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Status',NULL,CONVERT(NVARCHAR(2000),[Status],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ExportInd',NULL,CONVERT(NVARCHAR(2000),[ExportInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'WebSite',NULL,CONVERT(NVARCHAR(2000),[WebSite],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Memo',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'CurrentStatus',NULL,CONVERT(NVARCHAR(2000),[CurrentStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'CustomCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ClientInd',NULL,CONVERT(NVARCHAR(2000),[ClientInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'VendorInd',NULL,CONVERT(NVARCHAR(2000),[VendorInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'PriorWork',NULL,CONVERT(NVARCHAR(2000),[PriorWork],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Recommend',NULL,CONVERT(NVARCHAR(2000),[Recommend],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'DisadvBusiness',NULL,CONVERT(NVARCHAR(2000),[DisadvBusiness],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'DisabledVetOwnedSmallBusiness',NULL,CONVERT(NVARCHAR(2000),[DisabledVetOwnedSmallBusiness],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'HBCU',NULL,CONVERT(NVARCHAR(2000),[HBCU],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'MinorityBusiness',NULL,CONVERT(NVARCHAR(2000),[MinorityBusiness],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'SmallBusiness',NULL,CONVERT(NVARCHAR(2000),[SmallBusiness],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'VetOwnedSmallBusiness',NULL,CONVERT(NVARCHAR(2000),[VetOwnedSmallBusiness],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'WomanOwned',NULL,CONVERT(NVARCHAR(2000),[WomanOwned],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'AlaskaNative',NULL,CONVERT(NVARCHAR(2000),[AlaskaNative],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'SpecialtyType',NULL,CONVERT(NVARCHAR(2000),[SpecialtyType],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Specialty',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ParentID',NULL,CONVERT(NVARCHAR(2000),[ParentID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ParentLevel1',NULL,CONVERT(NVARCHAR(2000),[ParentLevel1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ParentLevel2',NULL,CONVERT(NVARCHAR(2000),[ParentLevel2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ParentLevel3',NULL,CONVERT(NVARCHAR(2000),[ParentLevel3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ParentLevel4',NULL,CONVERT(NVARCHAR(2000),[ParentLevel4],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Employees',NULL,CONVERT(NVARCHAR(2000),[Employees],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'AnnualRevenue',NULL,CONVERT(NVARCHAR(2000),[AnnualRevenue],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'GovernmentAgency',NULL,CONVERT(NVARCHAR(2000),[GovernmentAgency],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Competitor',NULL,CONVERT(NVARCHAR(2000),[Competitor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'EightA',NULL,CONVERT(NVARCHAR(2000),[EightA],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Hubzone',NULL,CONVERT(NVARCHAR(2000),[Hubzone],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'IQID',NULL,CONVERT(NVARCHAR(2000),[IQID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Incumbent',NULL,CONVERT(NVARCHAR(2000),[Incumbent],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'AjeraSync',NULL,CONVERT(NVARCHAR(2000),[AjeraSync],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'TLInternalKey',NULL,CONVERT(NVARCHAR(2000),[TLInternalKey],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'TLSyncModDate',NULL,CONVERT(NVARCHAR(2000),[TLSyncModDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Owner',NULL,CONVERT(NVARCHAR(2000),[Owner],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Vendor',NULL,CONVERT(NVARCHAR(2000),[Vendor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'SortName',NULL,CONVERT(NVARCHAR(2000),[SortName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Category',NULL,CONVERT(NVARCHAR(2000),[Category],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'AvailableForCRM',NULL,CONVERT(NVARCHAR(2000),[AvailableForCRM],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ReadyForApproval',NULL,CONVERT(NVARCHAR(2000),[ReadyForApproval],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ReadyForProcessing',NULL,CONVERT(NVARCHAR(2000),[ReadyForProcessing],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'FedID',NULL,CONVERT(NVARCHAR(2000),[FedID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Org',NULL,CONVERT(NVARCHAR(2000),[Org],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Market',NULL,CONVERT(NVARCHAR(2000),[Market],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'TypeOfTIN',NULL,CONVERT(NVARCHAR(2000),[TypeOfTIN],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'SFID',NULL,CONVERT(NVARCHAR(2000),[SFID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'SFLastModifiedDate',NULL,CONVERT(NVARCHAR(2000),[SFLastModifiedDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'QBOID',NULL,CONVERT(NVARCHAR(2000),[QBOID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'QBOLastUpdated',NULL,CONVERT(NVARCHAR(2000),[QBOLastUpdated],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_Clendor]
      ON [dbo].[Clendor]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'Clendor'
    
      If UPDATE([ClientID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ClientID',
      CONVERT(NVARCHAR(2000),DELETED.[ClientID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClientID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[ClientID] Is Null And
				DELETED.[ClientID] Is Not Null
			) Or
			(
				INSERTED.[ClientID] Is Not Null And
				DELETED.[ClientID] Is Null
			) Or
			(
				INSERTED.[ClientID] !=
				DELETED.[ClientID]
			)
		) 
		END		
		
      If UPDATE([Client])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Client',
      CONVERT(NVARCHAR(2000),DELETED.[Client],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Client],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[Client] Is Null And
				DELETED.[Client] Is Not Null
			) Or
			(
				INSERTED.[Client] Is Not Null And
				DELETED.[Client] Is Null
			) Or
			(
				INSERTED.[Client] !=
				DELETED.[Client]
			)
		) 
		END		
		
      If UPDATE([Name])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Name',
      CONVERT(NVARCHAR(2000),DELETED.[Name],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Name],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[Name] Is Null And
				DELETED.[Name] Is Not Null
			) Or
			(
				INSERTED.[Name] Is Not Null And
				DELETED.[Name] Is Null
			) Or
			(
				INSERTED.[Name] !=
				DELETED.[Name]
			)
		) 
		END		
		
      If UPDATE([Type])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Type',
      CONVERT(NVARCHAR(2000),DELETED.[Type],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Type],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[Type] Is Null And
				DELETED.[Type] Is Not Null
			) Or
			(
				INSERTED.[Type] Is Not Null And
				DELETED.[Type] Is Null
			) Or
			(
				INSERTED.[Type] !=
				DELETED.[Type]
			)
		) 
		END		
		
      If UPDATE([Status])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Status',
      CONVERT(NVARCHAR(2000),DELETED.[Status],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Status],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[Status] Is Null And
				DELETED.[Status] Is Not Null
			) Or
			(
				INSERTED.[Status] Is Not Null And
				DELETED.[Status] Is Null
			) Or
			(
				INSERTED.[Status] !=
				DELETED.[Status]
			)
		) 
		END		
		
      If UPDATE([ExportInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ExportInd',
      CONVERT(NVARCHAR(2000),DELETED.[ExportInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ExportInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[ExportInd] Is Null And
				DELETED.[ExportInd] Is Not Null
			) Or
			(
				INSERTED.[ExportInd] Is Not Null And
				DELETED.[ExportInd] Is Null
			) Or
			(
				INSERTED.[ExportInd] !=
				DELETED.[ExportInd]
			)
		) 
		END		
		
      If UPDATE([WebSite])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'WebSite',
      CONVERT(NVARCHAR(2000),DELETED.[WebSite],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WebSite],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[WebSite] Is Null And
				DELETED.[WebSite] Is Not Null
			) Or
			(
				INSERTED.[WebSite] Is Not Null And
				DELETED.[WebSite] Is Null
			) Or
			(
				INSERTED.[WebSite] !=
				DELETED.[WebSite]
			)
		) 
		END		
		
      If UPDATE([Memo])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Memo',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([CurrentStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'CurrentStatus',
      CONVERT(NVARCHAR(2000),DELETED.[CurrentStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CurrentStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CurrentStatus] Is Null And
				DELETED.[CurrentStatus] Is Not Null
			) Or
			(
				INSERTED.[CurrentStatus] Is Not Null And
				DELETED.[CurrentStatus] Is Null
			) Or
			(
				INSERTED.[CurrentStatus] !=
				DELETED.[CurrentStatus]
			)
		) 
		END		
		
      If UPDATE([CustomCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'CustomCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustomCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustomCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[CustomCurrencyCode] Is Null And
				DELETED.[CustomCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] Is Not Null And
				DELETED.[CustomCurrencyCode] Is Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] !=
				DELETED.[CustomCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([ClientInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ClientInd',
      CONVERT(NVARCHAR(2000),DELETED.[ClientInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClientInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[ClientInd] Is Null And
				DELETED.[ClientInd] Is Not Null
			) Or
			(
				INSERTED.[ClientInd] Is Not Null And
				DELETED.[ClientInd] Is Null
			) Or
			(
				INSERTED.[ClientInd] !=
				DELETED.[ClientInd]
			)
		) 
		END		
		
      If UPDATE([VendorInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'VendorInd',
      CONVERT(NVARCHAR(2000),DELETED.[VendorInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[VendorInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[VendorInd] Is Null And
				DELETED.[VendorInd] Is Not Null
			) Or
			(
				INSERTED.[VendorInd] Is Not Null And
				DELETED.[VendorInd] Is Null
			) Or
			(
				INSERTED.[VendorInd] !=
				DELETED.[VendorInd]
			)
		) 
		END		
		
      If UPDATE([PriorWork])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'PriorWork',
      CONVERT(NVARCHAR(2000),DELETED.[PriorWork],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PriorWork],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[PriorWork] Is Null And
				DELETED.[PriorWork] Is Not Null
			) Or
			(
				INSERTED.[PriorWork] Is Not Null And
				DELETED.[PriorWork] Is Null
			) Or
			(
				INSERTED.[PriorWork] !=
				DELETED.[PriorWork]
			)
		) 
		END		
		
      If UPDATE([Recommend])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Recommend',
      CONVERT(NVARCHAR(2000),DELETED.[Recommend],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Recommend],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[Recommend] Is Null And
				DELETED.[Recommend] Is Not Null
			) Or
			(
				INSERTED.[Recommend] Is Not Null And
				DELETED.[Recommend] Is Null
			) Or
			(
				INSERTED.[Recommend] !=
				DELETED.[Recommend]
			)
		) 
		END		
		
      If UPDATE([DisadvBusiness])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'DisadvBusiness',
      CONVERT(NVARCHAR(2000),DELETED.[DisadvBusiness],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DisadvBusiness],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[DisadvBusiness] Is Null And
				DELETED.[DisadvBusiness] Is Not Null
			) Or
			(
				INSERTED.[DisadvBusiness] Is Not Null And
				DELETED.[DisadvBusiness] Is Null
			) Or
			(
				INSERTED.[DisadvBusiness] !=
				DELETED.[DisadvBusiness]
			)
		) 
		END		
		
      If UPDATE([DisabledVetOwnedSmallBusiness])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'DisabledVetOwnedSmallBusiness',
      CONVERT(NVARCHAR(2000),DELETED.[DisabledVetOwnedSmallBusiness],121),
      CONVERT(NVARCHAR(2000),INSERTED.[DisabledVetOwnedSmallBusiness],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[DisabledVetOwnedSmallBusiness] Is Null And
				DELETED.[DisabledVetOwnedSmallBusiness] Is Not Null
			) Or
			(
				INSERTED.[DisabledVetOwnedSmallBusiness] Is Not Null And
				DELETED.[DisabledVetOwnedSmallBusiness] Is Null
			) Or
			(
				INSERTED.[DisabledVetOwnedSmallBusiness] !=
				DELETED.[DisabledVetOwnedSmallBusiness]
			)
		) 
		END		
		
      If UPDATE([HBCU])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'HBCU',
      CONVERT(NVARCHAR(2000),DELETED.[HBCU],121),
      CONVERT(NVARCHAR(2000),INSERTED.[HBCU],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[HBCU] Is Null And
				DELETED.[HBCU] Is Not Null
			) Or
			(
				INSERTED.[HBCU] Is Not Null And
				DELETED.[HBCU] Is Null
			) Or
			(
				INSERTED.[HBCU] !=
				DELETED.[HBCU]
			)
		) 
		END		
		
      If UPDATE([MinorityBusiness])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'MinorityBusiness',
      CONVERT(NVARCHAR(2000),DELETED.[MinorityBusiness],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MinorityBusiness],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[MinorityBusiness] Is Null And
				DELETED.[MinorityBusiness] Is Not Null
			) Or
			(
				INSERTED.[MinorityBusiness] Is Not Null And
				DELETED.[MinorityBusiness] Is Null
			) Or
			(
				INSERTED.[MinorityBusiness] !=
				DELETED.[MinorityBusiness]
			)
		) 
		END		
		
      If UPDATE([SmallBusiness])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'SmallBusiness',
      CONVERT(NVARCHAR(2000),DELETED.[SmallBusiness],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SmallBusiness],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[SmallBusiness] Is Null And
				DELETED.[SmallBusiness] Is Not Null
			) Or
			(
				INSERTED.[SmallBusiness] Is Not Null And
				DELETED.[SmallBusiness] Is Null
			) Or
			(
				INSERTED.[SmallBusiness] !=
				DELETED.[SmallBusiness]
			)
		) 
		END		
		
      If UPDATE([VetOwnedSmallBusiness])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'VetOwnedSmallBusiness',
      CONVERT(NVARCHAR(2000),DELETED.[VetOwnedSmallBusiness],121),
      CONVERT(NVARCHAR(2000),INSERTED.[VetOwnedSmallBusiness],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[VetOwnedSmallBusiness] Is Null And
				DELETED.[VetOwnedSmallBusiness] Is Not Null
			) Or
			(
				INSERTED.[VetOwnedSmallBusiness] Is Not Null And
				DELETED.[VetOwnedSmallBusiness] Is Null
			) Or
			(
				INSERTED.[VetOwnedSmallBusiness] !=
				DELETED.[VetOwnedSmallBusiness]
			)
		) 
		END		
		
      If UPDATE([WomanOwned])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'WomanOwned',
      CONVERT(NVARCHAR(2000),DELETED.[WomanOwned],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WomanOwned],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[WomanOwned] Is Null And
				DELETED.[WomanOwned] Is Not Null
			) Or
			(
				INSERTED.[WomanOwned] Is Not Null And
				DELETED.[WomanOwned] Is Null
			) Or
			(
				INSERTED.[WomanOwned] !=
				DELETED.[WomanOwned]
			)
		) 
		END		
		
      If UPDATE([AlaskaNative])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'AlaskaNative',
      CONVERT(NVARCHAR(2000),DELETED.[AlaskaNative],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AlaskaNative],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[AlaskaNative] Is Null And
				DELETED.[AlaskaNative] Is Not Null
			) Or
			(
				INSERTED.[AlaskaNative] Is Not Null And
				DELETED.[AlaskaNative] Is Null
			) Or
			(
				INSERTED.[AlaskaNative] !=
				DELETED.[AlaskaNative]
			)
		) 
		END		
		
      If UPDATE([SpecialtyType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'SpecialtyType',
      CONVERT(NVARCHAR(2000),DELETED.[SpecialtyType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SpecialtyType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[SpecialtyType] Is Null And
				DELETED.[SpecialtyType] Is Not Null
			) Or
			(
				INSERTED.[SpecialtyType] Is Not Null And
				DELETED.[SpecialtyType] Is Null
			) Or
			(
				INSERTED.[SpecialtyType] !=
				DELETED.[SpecialtyType]
			)
		) 
		END		
		
      If UPDATE([Specialty])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Specialty',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([ParentID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ParentID',
      CONVERT(NVARCHAR(2000),DELETED.[ParentID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ParentID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[ParentID] Is Null And
				DELETED.[ParentID] Is Not Null
			) Or
			(
				INSERTED.[ParentID] Is Not Null And
				DELETED.[ParentID] Is Null
			) Or
			(
				INSERTED.[ParentID] !=
				DELETED.[ParentID]
			)
		) 
		END		
		
      If UPDATE([ParentLevel1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ParentLevel1',
      CONVERT(NVARCHAR(2000),DELETED.[ParentLevel1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ParentLevel1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[ParentLevel1] Is Null And
				DELETED.[ParentLevel1] Is Not Null
			) Or
			(
				INSERTED.[ParentLevel1] Is Not Null And
				DELETED.[ParentLevel1] Is Null
			) Or
			(
				INSERTED.[ParentLevel1] !=
				DELETED.[ParentLevel1]
			)
		) 
		END		
		
      If UPDATE([ParentLevel2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ParentLevel2',
      CONVERT(NVARCHAR(2000),DELETED.[ParentLevel2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ParentLevel2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[ParentLevel2] Is Null And
				DELETED.[ParentLevel2] Is Not Null
			) Or
			(
				INSERTED.[ParentLevel2] Is Not Null And
				DELETED.[ParentLevel2] Is Null
			) Or
			(
				INSERTED.[ParentLevel2] !=
				DELETED.[ParentLevel2]
			)
		) 
		END		
		
      If UPDATE([ParentLevel3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ParentLevel3',
      CONVERT(NVARCHAR(2000),DELETED.[ParentLevel3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ParentLevel3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[ParentLevel3] Is Null And
				DELETED.[ParentLevel3] Is Not Null
			) Or
			(
				INSERTED.[ParentLevel3] Is Not Null And
				DELETED.[ParentLevel3] Is Null
			) Or
			(
				INSERTED.[ParentLevel3] !=
				DELETED.[ParentLevel3]
			)
		) 
		END		
		
      If UPDATE([ParentLevel4])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ParentLevel4',
      CONVERT(NVARCHAR(2000),DELETED.[ParentLevel4],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ParentLevel4],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[ParentLevel4] Is Null And
				DELETED.[ParentLevel4] Is Not Null
			) Or
			(
				INSERTED.[ParentLevel4] Is Not Null And
				DELETED.[ParentLevel4] Is Null
			) Or
			(
				INSERTED.[ParentLevel4] !=
				DELETED.[ParentLevel4]
			)
		) 
		END		
		
      If UPDATE([Employees])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Employees',
      CONVERT(NVARCHAR(2000),DELETED.[Employees],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Employees],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[Employees] Is Null And
				DELETED.[Employees] Is Not Null
			) Or
			(
				INSERTED.[Employees] Is Not Null And
				DELETED.[Employees] Is Null
			) Or
			(
				INSERTED.[Employees] !=
				DELETED.[Employees]
			)
		) 
		END		
		
      If UPDATE([AnnualRevenue])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'AnnualRevenue',
      CONVERT(NVARCHAR(2000),DELETED.[AnnualRevenue],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AnnualRevenue],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[AnnualRevenue] Is Null And
				DELETED.[AnnualRevenue] Is Not Null
			) Or
			(
				INSERTED.[AnnualRevenue] Is Not Null And
				DELETED.[AnnualRevenue] Is Null
			) Or
			(
				INSERTED.[AnnualRevenue] !=
				DELETED.[AnnualRevenue]
			)
		) 
		END		
		
      If UPDATE([GovernmentAgency])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'GovernmentAgency',
      CONVERT(NVARCHAR(2000),DELETED.[GovernmentAgency],121),
      CONVERT(NVARCHAR(2000),INSERTED.[GovernmentAgency],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[GovernmentAgency] Is Null And
				DELETED.[GovernmentAgency] Is Not Null
			) Or
			(
				INSERTED.[GovernmentAgency] Is Not Null And
				DELETED.[GovernmentAgency] Is Null
			) Or
			(
				INSERTED.[GovernmentAgency] !=
				DELETED.[GovernmentAgency]
			)
		) 
		END		
		
      If UPDATE([Competitor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Competitor',
      CONVERT(NVARCHAR(2000),DELETED.[Competitor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Competitor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[Competitor] Is Null And
				DELETED.[Competitor] Is Not Null
			) Or
			(
				INSERTED.[Competitor] Is Not Null And
				DELETED.[Competitor] Is Null
			) Or
			(
				INSERTED.[Competitor] !=
				DELETED.[Competitor]
			)
		) 
		END		
		
      If UPDATE([EightA])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'EightA',
      CONVERT(NVARCHAR(2000),DELETED.[EightA],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EightA],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[EightA] Is Null And
				DELETED.[EightA] Is Not Null
			) Or
			(
				INSERTED.[EightA] Is Not Null And
				DELETED.[EightA] Is Null
			) Or
			(
				INSERTED.[EightA] !=
				DELETED.[EightA]
			)
		) 
		END		
		
      If UPDATE([Hubzone])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Hubzone',
      CONVERT(NVARCHAR(2000),DELETED.[Hubzone],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Hubzone],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[Hubzone] Is Null And
				DELETED.[Hubzone] Is Not Null
			) Or
			(
				INSERTED.[Hubzone] Is Not Null And
				DELETED.[Hubzone] Is Null
			) Or
			(
				INSERTED.[Hubzone] !=
				DELETED.[Hubzone]
			)
		) 
		END		
		
      If UPDATE([IQID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'IQID',
      CONVERT(NVARCHAR(2000),DELETED.[IQID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[IQID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[IQID] Is Null And
				DELETED.[IQID] Is Not Null
			) Or
			(
				INSERTED.[IQID] Is Not Null And
				DELETED.[IQID] Is Null
			) Or
			(
				INSERTED.[IQID] !=
				DELETED.[IQID]
			)
		) 
		END		
		
      If UPDATE([Incumbent])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Incumbent',
      CONVERT(NVARCHAR(2000),DELETED.[Incumbent],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Incumbent],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[Incumbent] Is Null And
				DELETED.[Incumbent] Is Not Null
			) Or
			(
				INSERTED.[Incumbent] Is Not Null And
				DELETED.[Incumbent] Is Null
			) Or
			(
				INSERTED.[Incumbent] !=
				DELETED.[Incumbent]
			)
		) 
		END		
		
      If UPDATE([AjeraSync])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'AjeraSync',
      CONVERT(NVARCHAR(2000),DELETED.[AjeraSync],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AjeraSync],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[AjeraSync] Is Null And
				DELETED.[AjeraSync] Is Not Null
			) Or
			(
				INSERTED.[AjeraSync] Is Not Null And
				DELETED.[AjeraSync] Is Null
			) Or
			(
				INSERTED.[AjeraSync] !=
				DELETED.[AjeraSync]
			)
		) 
		END		
		
      If UPDATE([TLInternalKey])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'TLInternalKey',
      CONVERT(NVARCHAR(2000),DELETED.[TLInternalKey],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TLInternalKey],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[TLInternalKey] Is Null And
				DELETED.[TLInternalKey] Is Not Null
			) Or
			(
				INSERTED.[TLInternalKey] Is Not Null And
				DELETED.[TLInternalKey] Is Null
			) Or
			(
				INSERTED.[TLInternalKey] !=
				DELETED.[TLInternalKey]
			)
		) 
		END		
		
      If UPDATE([TLSyncModDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'TLSyncModDate',
      CONVERT(NVARCHAR(2000),DELETED.[TLSyncModDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TLSyncModDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[TLSyncModDate] Is Null And
				DELETED.[TLSyncModDate] Is Not Null
			) Or
			(
				INSERTED.[TLSyncModDate] Is Not Null And
				DELETED.[TLSyncModDate] Is Null
			) Or
			(
				INSERTED.[TLSyncModDate] !=
				DELETED.[TLSyncModDate]
			)
		) 
		END		
		
      If UPDATE([Owner])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Owner',
      CONVERT(NVARCHAR(2000),DELETED.[Owner],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Owner],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[Owner] Is Null And
				DELETED.[Owner] Is Not Null
			) Or
			(
				INSERTED.[Owner] Is Not Null And
				DELETED.[Owner] Is Null
			) Or
			(
				INSERTED.[Owner] !=
				DELETED.[Owner]
			)
		) 
		END		
		
      If UPDATE([Vendor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Vendor',
      CONVERT(NVARCHAR(2000),DELETED.[Vendor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Vendor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[Vendor] Is Null And
				DELETED.[Vendor] Is Not Null
			) Or
			(
				INSERTED.[Vendor] Is Not Null And
				DELETED.[Vendor] Is Null
			) Or
			(
				INSERTED.[Vendor] !=
				DELETED.[Vendor]
			)
		) 
		END		
		
      If UPDATE([SortName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'SortName',
      CONVERT(NVARCHAR(2000),DELETED.[SortName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SortName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[SortName] Is Null And
				DELETED.[SortName] Is Not Null
			) Or
			(
				INSERTED.[SortName] Is Not Null And
				DELETED.[SortName] Is Null
			) Or
			(
				INSERTED.[SortName] !=
				DELETED.[SortName]
			)
		) 
		END		
		
      If UPDATE([Category])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Category',
      CONVERT(NVARCHAR(2000),DELETED.[Category],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Category],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[Category] Is Null And
				DELETED.[Category] Is Not Null
			) Or
			(
				INSERTED.[Category] Is Not Null And
				DELETED.[Category] Is Null
			) Or
			(
				INSERTED.[Category] !=
				DELETED.[Category]
			)
		) 
		END		
		
      If UPDATE([AvailableForCRM])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'AvailableForCRM',
      CONVERT(NVARCHAR(2000),DELETED.[AvailableForCRM],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AvailableForCRM],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[AvailableForCRM] Is Null And
				DELETED.[AvailableForCRM] Is Not Null
			) Or
			(
				INSERTED.[AvailableForCRM] Is Not Null And
				DELETED.[AvailableForCRM] Is Null
			) Or
			(
				INSERTED.[AvailableForCRM] !=
				DELETED.[AvailableForCRM]
			)
		) 
		END		
		
      If UPDATE([ReadyForApproval])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ReadyForApproval',
      CONVERT(NVARCHAR(2000),DELETED.[ReadyForApproval],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReadyForApproval],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[ReadyForApproval] Is Null And
				DELETED.[ReadyForApproval] Is Not Null
			) Or
			(
				INSERTED.[ReadyForApproval] Is Not Null And
				DELETED.[ReadyForApproval] Is Null
			) Or
			(
				INSERTED.[ReadyForApproval] !=
				DELETED.[ReadyForApproval]
			)
		) 
		END		
		
      If UPDATE([ReadyForProcessing])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'ReadyForProcessing',
      CONVERT(NVARCHAR(2000),DELETED.[ReadyForProcessing],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReadyForProcessing],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[ReadyForProcessing] Is Null And
				DELETED.[ReadyForProcessing] Is Not Null
			) Or
			(
				INSERTED.[ReadyForProcessing] Is Not Null And
				DELETED.[ReadyForProcessing] Is Null
			) Or
			(
				INSERTED.[ReadyForProcessing] !=
				DELETED.[ReadyForProcessing]
			)
		) 
		END		
		
      If UPDATE([FedID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'FedID',
      CONVERT(NVARCHAR(2000),DELETED.[FedID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FedID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[FedID] Is Null And
				DELETED.[FedID] Is Not Null
			) Or
			(
				INSERTED.[FedID] Is Not Null And
				DELETED.[FedID] Is Null
			) Or
			(
				INSERTED.[FedID] !=
				DELETED.[FedID]
			)
		) 
		END		
		
      If UPDATE([Org])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Org',
      CONVERT(NVARCHAR(2000),DELETED.[Org],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Org],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[Org] Is Null And
				DELETED.[Org] Is Not Null
			) Or
			(
				INSERTED.[Org] Is Not Null And
				DELETED.[Org] Is Null
			) Or
			(
				INSERTED.[Org] !=
				DELETED.[Org]
			)
		) 
		END		
		
      If UPDATE([Market])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'Market',
      CONVERT(NVARCHAR(2000),DELETED.[Market],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Market],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[Market] Is Null And
				DELETED.[Market] Is Not Null
			) Or
			(
				INSERTED.[Market] Is Not Null And
				DELETED.[Market] Is Null
			) Or
			(
				INSERTED.[Market] !=
				DELETED.[Market]
			)
		) 
		END		
		
      If UPDATE([TypeOfTIN])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'TypeOfTIN',
      CONVERT(NVARCHAR(2000),DELETED.[TypeOfTIN],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TypeOfTIN],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[TypeOfTIN] Is Null And
				DELETED.[TypeOfTIN] Is Not Null
			) Or
			(
				INSERTED.[TypeOfTIN] Is Not Null And
				DELETED.[TypeOfTIN] Is Null
			) Or
			(
				INSERTED.[TypeOfTIN] !=
				DELETED.[TypeOfTIN]
			)
		) 
		END		
		
      If UPDATE([SFID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'SFID',
      CONVERT(NVARCHAR(2000),DELETED.[SFID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SFID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[SFID] Is Null And
				DELETED.[SFID] Is Not Null
			) Or
			(
				INSERTED.[SFID] Is Not Null And
				DELETED.[SFID] Is Null
			) Or
			(
				INSERTED.[SFID] !=
				DELETED.[SFID]
			)
		) 
		END		
		
      If UPDATE([SFLastModifiedDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'SFLastModifiedDate',
      CONVERT(NVARCHAR(2000),DELETED.[SFLastModifiedDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SFLastModifiedDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[SFLastModifiedDate] Is Null And
				DELETED.[SFLastModifiedDate] Is Not Null
			) Or
			(
				INSERTED.[SFLastModifiedDate] Is Not Null And
				DELETED.[SFLastModifiedDate] Is Null
			) Or
			(
				INSERTED.[SFLastModifiedDate] !=
				DELETED.[SFLastModifiedDate]
			)
		) 
		END		
		
      If UPDATE([QBOID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'QBOID',
      CONVERT(NVARCHAR(2000),DELETED.[QBOID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[QBOID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[QBOID] Is Null And
				DELETED.[QBOID] Is Not Null
			) Or
			(
				INSERTED.[QBOID] Is Not Null And
				DELETED.[QBOID] Is Null
			) Or
			(
				INSERTED.[QBOID] !=
				DELETED.[QBOID]
			)
		) 
		END		
		
      If UPDATE([QBOLastUpdated])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[ClientID],121),'QBOLastUpdated',
      CONVERT(NVARCHAR(2000),DELETED.[QBOLastUpdated],121),
      CONVERT(NVARCHAR(2000),INSERTED.[QBOLastUpdated],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[ClientID] = DELETED.[ClientID] AND 
		(
			(
				INSERTED.[QBOLastUpdated] Is Null And
				DELETED.[QBOLastUpdated] Is Not Null
			) Or
			(
				INSERTED.[QBOLastUpdated] Is Not Null And
				DELETED.[QBOLastUpdated] Is Null
			) Or
			(
				INSERTED.[QBOLastUpdated] !=
				DELETED.[QBOLastUpdated]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[Clendor] ADD CONSTRAINT [ClendorPK] PRIMARY KEY NONCLUSTERED ([ClientID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ClendorClientIDParentIDIDX] ON [dbo].[Clendor] ([ClientID], [ParentID]) INCLUDE ([Name], [ClientInd]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ClendorQBOIDIDX] ON [dbo].[Clendor] ([QBOID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ClendorSFIDIDX] ON [dbo].[Clendor] ([SFID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ClendorVendorIDX] ON [dbo].[Clendor] ([Vendor]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
