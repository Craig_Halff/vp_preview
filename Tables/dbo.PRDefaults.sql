CREATE TABLE [dbo].[PRDefaults]
(
[Type] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubLevel] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__SubLe__4359E7AD] DEFAULT ('N'),
[Principal] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjMgr] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Supervisor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLAddress] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefaults___Fee__444E0BE6] DEFAULT ((0)),
[ReimbAllow] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Reimb__4542301F] DEFAULT ((0)),
[ConsultFee] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Consu__46365458] DEFAULT ((0)),
[BudOHRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__BudOH__472A7891] DEFAULT ((0)),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MultAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__MultA__481E9CCA] DEFAULT ((0)),
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitTable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PctComp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__PctCo__4912C103] DEFAULT ((0)),
[LabPctComp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__LabPc__4A06E53C] DEFAULT ((0)),
[ExpPctComp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__ExpPc__4AFB0975] DEFAULT ((0)),
[BillByDefault] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__BillB__4BEF2DAE] DEFAULT ('N'),
[BillableWarning] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__Billa__4CE351E7] DEFAULT ('N'),
[Memo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BudgetedFlag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__Budge__4DD77620] DEFAULT ('N'),
[BudgetedLevels] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XCharge] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__XChar__4ECB9A59] DEFAULT ('G'),
[XChargeMethod] [smallint] NOT NULL CONSTRAINT [DF__PRDefault__XChar__4FBFBE92] DEFAULT ((0)),
[XChargeMult] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__XChar__50B3E2CB] DEFAULT ((0)),
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Closed] [int] NOT NULL CONSTRAINT [DF__PRDefault__Close__51A80704] DEFAULT ((0)),
[ReadOnly] [int] NOT NULL CONSTRAINT [DF__PRDefault__ReadO__529C2B3D] DEFAULT ((0)),
[DefaultEffortDriven] [int] NOT NULL CONSTRAINT [DF__PRDefault__Defau__53904F76] DEFAULT ((0)),
[DefaultTaskType] [int] NOT NULL CONSTRAINT [DF__PRDefault__Defau__548473AF] DEFAULT ((0)),
[VersionID] [int] NOT NULL CONSTRAINT [DF__PRDefault__Versi__557897E8] DEFAULT ((0)),
[ContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLBillingAddr] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LongName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address3] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[County] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FederalInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__Feder__566CBC21] DEFAULT ('N'),
[ProjectType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Responsibility] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Referable] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__Refer__5760E05A] DEFAULT ('N'),
[EstCompletionDate] [datetime] NULL,
[ActCompletionDate] [datetime] NULL,
[ContractDate] [datetime] NULL,
[BidDate] [datetime] NULL,
[ComplDateComment] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__FirmC__58550493] DEFAULT ((0)),
[FirmCostComment] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TotalProjectCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Total__594928CC] DEFAULT ((0)),
[TotalCostComment] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientConfidential] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientAlias] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AvailableForCRM] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__Avail__5A3D4D05] DEFAULT ('N'),
[ReadyForApproval] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__Ready__5B31713E] DEFAULT ('N'),
[ReadyForProcessing] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__Ready__5C259577] DEFAULT ('N'),
[BillingClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fax] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMail] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProposalWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CostRateMeth] [smallint] NOT NULL,
[CostRateTableNo] [int] NOT NULL CONSTRAINT [DF__PRDefault__CostR__5D19B9B0] DEFAULT ((0)),
[PayRateMeth] [smallint] NOT NULL,
[PayRateTableNo] [int] NOT NULL CONSTRAINT [DF__PRDefault__PayRa__5E0DDDE9] DEFAULT ((0)),
[Locale] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LineItemApproval] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__LineI__5F020222] DEFAULT ('N'),
[LineItemApprovalEK] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefaults__LineItemApprovalEK__DefN] DEFAULT ('N'),
[BudgetSource] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BudgetLevel] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProfServicesComplDate] [datetime] NULL,
[ConstComplDate] [datetime] NULL,
[ProjectCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RestrictChargeCompanies] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__Restr__60EA4A94] DEFAULT ('N'),
[RevUpsetLimits] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__RevUp__61DE6ECD] DEFAULT ('N'),
[RevUpsetWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevUpsetWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevUpsetIncludeComp] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__RevUp__62D29306] DEFAULT ('N'),
[RevUpsetIncludeCons] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__RevUp__63C6B73F] DEFAULT ('N'),
[RevUpsetIncludeReimb] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__RevUp__64BADB78] DEFAULT ('N'),
[ProjectExchangeRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__PRDefault__Proje__65AEFFB1] DEFAULT ((0)),
[BillingExchangeRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__PRDefault__Billi__66A323EA] DEFAULT ((0)),
[FeeBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__FeeBi__67974823] DEFAULT ((0)),
[ReimbAllowBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Reimb__688B6C5C] DEFAULT ((0)),
[ConsultFeeBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Consu__697F9095] DEFAULT ((0)),
[PORMBRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__PORMB__6A73B4CE] DEFAULT ((0)),
[POCNSRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__POCNS__6B67D907] DEFAULT ((0)),
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TKCheckRPDate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__TKChe__6C5BFD40] DEFAULT ('N'),
[ICBillingLab] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__ICBil__6D502179] DEFAULT ('G'),
[ICBillingLabMethod] [smallint] NOT NULL CONSTRAINT [DF__PRDefault__ICBil__6E4445B2] DEFAULT ((0)),
[ICBillingLabMult] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__ICBil__6F3869EB] DEFAULT ((0)),
[ICBillingExp] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__ICBil__702C8E24] DEFAULT ('G'),
[ICBillingExpMethod] [smallint] NOT NULL CONSTRAINT [DF__PRDefault__ICBil__7120B25D] DEFAULT ((0)),
[ICBillingExpMult] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__ICBil__7214D696] DEFAULT ((0)),
[RequireComments] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__Requi__7308FACF] DEFAULT ('C'),
[TKCheckRPPlannedHrs] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__TKChe__73FD1F08] DEFAULT ('N'),
[BillByDefaultConsultants] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__BillB__74F14341] DEFAULT ('E'),
[BillByDefaultOtherExp] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__BillB__75E5677A] DEFAULT ('E'),
[BillByDefaultORTable] [int] NOT NULL CONSTRAINT [DF__PRDefault__BillB__76D98BB3] DEFAULT ((0)),
[PhoneFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FaxFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevType2] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__RevTy__77CDAFEC] DEFAULT ('N'),
[RevType3] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__RevTy__78C1D425] DEFAULT ('N'),
[RevType4] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__RevTy__79B5F85E] DEFAULT ('N'),
[RevType5] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__RevTy__7AAA1C97] DEFAULT ('N'),
[RevUpsetCategoryToAdjust] [smallint] NOT NULL CONSTRAINT [DF__PRDefault__RevUp__7B9E40D0] DEFAULT ((0)),
[FeeFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__FeeFu__7C926509] DEFAULT ((0)),
[ReimbAllowFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Reimb__7D868942] DEFAULT ((0)),
[ConsultFeeFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Consu__7E7AAD7B] DEFAULT ((0)),
[RevenueMethod] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__Reven__7F6ED1B4] DEFAULT ('U'),
[ICBillingLabTableNo] [int] NOT NULL CONSTRAINT [DF__PRDefault__ICBil__0062F5ED] DEFAULT ((0)),
[ICBillingExpTableNo] [int] NOT NULL CONSTRAINT [DF__PRDefault__ICBil__01571A26] DEFAULT ((0)),
[Biller] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeeDirLab] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__FeeDi__024B3E5F] DEFAULT ((0)),
[FeeDirExp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__FeeDi__033F6298] DEFAULT ((0)),
[ReimbAllowExp] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Reimb__043386D1] DEFAULT ((0)),
[ReimbAllowCons] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Reimb__0527AB0A] DEFAULT ((0)),
[FeeDirLabBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__FeeDi__061BCF43] DEFAULT ((0)),
[FeeDirExpBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__FeeDi__070FF37C] DEFAULT ((0)),
[ReimbAllowExpBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Reimb__080417B5] DEFAULT ((0)),
[ReimbAllowConsBillingCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Reimb__08F83BEE] DEFAULT ((0)),
[FeeDirLabFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__FeeDi__09EC6027] DEFAULT ((0)),
[FeeDirExpFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__FeeDi__0AE08460] DEFAULT ((0)),
[ReimbAllowExpFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Reimb__0BD4A899] DEFAULT ((0)),
[ReimbAllowConsFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Reimb__0CC8CCD2] DEFAULT ((0)),
[RevUpsetIncludeCompDirExp] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__RevUp__0DBCF10B] DEFAULT ('N'),
[RevUpsetIncludeReimbCons] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__RevUp__0EB11544] DEFAULT ('N'),
[AwardType] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Duration] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractTypeGovCon] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompetitionType] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MasterContract] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Solicitation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAICS] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurRole] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AjeraSync] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__Ajera__0FA5397D] DEFAULT ('N'),
[ServProCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FESurchargePct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__FESur__10995DB6] DEFAULT ((0)),
[FESurcharge] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__FESur__118D81EF] DEFAULT ((0)),
[FEAddlExpensesPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__FEAdd__1281A628] DEFAULT ((0)),
[FEAddlExpenses] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__FEAdd__1375CA61] DEFAULT ((0)),
[FEOtherPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__FEOth__1469EE9A] DEFAULT ((0)),
[FEOther] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__FEOth__155E12D3] DEFAULT ((0)),
[ProjectTemplate] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AjeraSpentLabor] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Ajera__1652370C] DEFAULT ((0)),
[AjeraSpentReimbursable] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Ajera__17465B45] DEFAULT ((0)),
[AjeraSpentConsultant] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Ajera__183A7F7E] DEFAULT ((0)),
[AjeraCostLabor] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Ajera__192EA3B7] DEFAULT ((0)),
[AjeraCostReimbursable] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Ajera__1A22C7F0] DEFAULT ((0)),
[AjeraCostConsultant] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Ajera__1B16EC29] DEFAULT ((0)),
[AjeraWIPLabor] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Ajera__1C0B1062] DEFAULT ((0)),
[AjeraWIPReimbursable] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Ajera__1CFF349B] DEFAULT ((0)),
[AjeraWIPConsultant] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Ajera__1DF358D4] DEFAULT ((0)),
[AjeraBilledLabor] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Ajera__1EE77D0D] DEFAULT ((0)),
[AjeraBilledReimbursable] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Ajera__1FDBA146] DEFAULT ((0)),
[AjeraBilledConsultant] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Ajera__20CFC57F] DEFAULT ((0)),
[AjeraReceivedLabor] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Ajera__21C3E9B8] DEFAULT ((0)),
[AjeraReceivedReimbursable] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Ajera__22B80DF1] DEFAULT ((0)),
[AjeraReceivedConsultant] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Ajera__23AC322A] DEFAULT ((0)),
[TLInternalKey] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLProjectID] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLProjectName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLChargeBandInternalKey] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLChargeBandExternalCode] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLSyncModDate] [datetime] NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[PIMID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SiblingWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Stage] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EstStartDate] [datetime] NULL,
[EstEndDate] [datetime] NULL,
[EstFees] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__EstFe__18712056] DEFAULT ((0)),
[EstConstructionCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__EstCo__1965448F] DEFAULT ((0)),
[Revenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Reven__1A5968C8] DEFAULT ((0)),
[Probability] [smallint] NOT NULL CONSTRAINT [DF__PRDefault__Proba__1B4D8D01] DEFAULT ((0)),
[WeightedRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__PRDefault__Weigh__1C41B13A] DEFAULT ((0)),
[CloseDate] [datetime] NULL,
[OpenDate] [datetime] NULL,
[Source] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PublicNoticeDate] [datetime] NULL,
[SolicitationNum] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LabBillTable] [int] NOT NULL CONSTRAINT [DF__PRDefault__LabBi__1D35D573] DEFAULT ((0)),
[LabCostTable] [int] NOT NULL CONSTRAINT [DF__PRDefault__LabCo__1E29F9AC] DEFAULT ((0)),
[AllocMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Timescale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClosedReason] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClosedNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IQID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IQLastUpdate] [datetime] NULL,
[MarketingCoordinator] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProposalManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BusinessDeveloperLead] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFID] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFLastModifiedDate] [datetime] NULL,
[TotalContractValue] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__PRDefault__Total__2012421E] DEFAULT ((0)),
[PeriodOfPerformance] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__PRDefault__Perio__21066657] DEFAULT ((0)),
[LostTo] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreAwardWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UtilizationScheduleFlg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PRDefault__Utili__6D51B827] DEFAULT ('N')
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGScreenDesignerDataTrigger] 
   ON  [dbo].[PRDefaults]
AFTER INSERT, UPDATE 
as
BEGIN
	SET NOCOUNT ON;

	If UPDATE(ActCompletionDate) and exists (Select ActCompletionDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ActCompletionDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ActCompletionDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ActCompletionDate' 
				End 
			else 
				If Not ((Select ActCompletionDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ActCompletionDate', '', (Select ActCompletionDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Address1) and exists (Select Address1 From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Address1') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Address1 From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Address1' 
				End 
			else 
				If Not ((Select Address1 From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Address1', '', (Select Address1 From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Address2) and exists (Select Address2 From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Address2') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Address2 From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Address2' 
				End 
			else 
				If Not ((Select Address2 From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Address2', '', (Select Address2 From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Address3) and exists (Select Address3 From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Address3') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Address3 From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Address3' 
				End 
			else 
				If Not ((Select Address3 From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Address3', '', (Select Address3 From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AjeraBilledConsultant) and exists (Select AjeraBilledConsultant From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraBilledConsultant') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AjeraBilledConsultant From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraBilledConsultant' 
				End 
			else 
				If ((Select AjeraBilledConsultant From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AjeraBilledConsultant', '', (Select AjeraBilledConsultant From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AjeraBilledLabor) and exists (Select AjeraBilledLabor From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraBilledLabor') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AjeraBilledLabor From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraBilledLabor' 
				End 
			else 
				If ((Select AjeraBilledLabor From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AjeraBilledLabor', '', (Select AjeraBilledLabor From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AjeraBilledReimbursable) and exists (Select AjeraBilledReimbursable From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraBilledReimbursable') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AjeraBilledReimbursable From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraBilledReimbursable' 
				End 
			else 
				If ((Select AjeraBilledReimbursable From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AjeraBilledReimbursable', '', (Select AjeraBilledReimbursable From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AjeraCostConsultant) and exists (Select AjeraCostConsultant From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraCostConsultant') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AjeraCostConsultant From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraCostConsultant' 
				End 
			else 
				If ((Select AjeraCostConsultant From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AjeraCostConsultant', '', (Select AjeraCostConsultant From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AjeraCostLabor) and exists (Select AjeraCostLabor From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraCostLabor') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AjeraCostLabor From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraCostLabor' 
				End 
			else 
				If ((Select AjeraCostLabor From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AjeraCostLabor', '', (Select AjeraCostLabor From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AjeraCostReimbursable) and exists (Select AjeraCostReimbursable From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraCostReimbursable') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AjeraCostReimbursable From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraCostReimbursable' 
				End 
			else 
				If ((Select AjeraCostReimbursable From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AjeraCostReimbursable', '', (Select AjeraCostReimbursable From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AjeraReceivedConsultant) and exists (Select AjeraReceivedConsultant From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraReceivedConsultant') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AjeraReceivedConsultant From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraReceivedConsultant' 
				End 
			else 
				If ((Select AjeraReceivedConsultant From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AjeraReceivedConsultant', '', (Select AjeraReceivedConsultant From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AjeraReceivedLabor) and exists (Select AjeraReceivedLabor From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraReceivedLabor') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AjeraReceivedLabor From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraReceivedLabor' 
				End 
			else 
				If ((Select AjeraReceivedLabor From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AjeraReceivedLabor', '', (Select AjeraReceivedLabor From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AjeraReceivedReimbursable) and exists (Select AjeraReceivedReimbursable From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraReceivedReimbursable') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AjeraReceivedReimbursable From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraReceivedReimbursable' 
				End 
			else 
				If ((Select AjeraReceivedReimbursable From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AjeraReceivedReimbursable', '', (Select AjeraReceivedReimbursable From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AjeraSpentConsultant) and exists (Select AjeraSpentConsultant From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraSpentConsultant') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AjeraSpentConsultant From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraSpentConsultant' 
				End 
			else 
				If ((Select AjeraSpentConsultant From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AjeraSpentConsultant', '', (Select AjeraSpentConsultant From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AjeraSpentLabor) and exists (Select AjeraSpentLabor From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraSpentLabor') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AjeraSpentLabor From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraSpentLabor' 
				End 
			else 
				If ((Select AjeraSpentLabor From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AjeraSpentLabor', '', (Select AjeraSpentLabor From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AjeraSpentReimbursable) and exists (Select AjeraSpentReimbursable From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraSpentReimbursable') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AjeraSpentReimbursable From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraSpentReimbursable' 
				End 
			else 
				If ((Select AjeraSpentReimbursable From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AjeraSpentReimbursable', '', (Select AjeraSpentReimbursable From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AjeraSync) and exists (Select AjeraSync From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraSync') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AjeraSync From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraSync' 
				End 
			else 
				If Not ((Select AjeraSync From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AjeraSync', '', (Select AjeraSync From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AjeraWIPConsultant) and exists (Select AjeraWIPConsultant From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraWIPConsultant') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AjeraWIPConsultant From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraWIPConsultant' 
				End 
			else 
				If ((Select AjeraWIPConsultant From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AjeraWIPConsultant', '', (Select AjeraWIPConsultant From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AjeraWIPLabor) and exists (Select AjeraWIPLabor From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraWIPLabor') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AjeraWIPLabor From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraWIPLabor' 
				End 
			else 
				If ((Select AjeraWIPLabor From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AjeraWIPLabor', '', (Select AjeraWIPLabor From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AjeraWIPReimbursable) and exists (Select AjeraWIPReimbursable From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraWIPReimbursable') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AjeraWIPReimbursable From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AjeraWIPReimbursable' 
				End 
			else 
				If ((Select AjeraWIPReimbursable From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AjeraWIPReimbursable', '', (Select AjeraWIPReimbursable From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AllocMethod) and exists (Select AllocMethod From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AllocMethod') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AllocMethod From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AllocMethod' 
				End 
			else 
				If Not ((Select AllocMethod From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AllocMethod', '', (Select AllocMethod From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AvailableForCRM) and exists (Select AvailableForCRM From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AvailableForCRM') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AvailableForCRM From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AvailableForCRM' 
				End 
			else 
				If Not ((Select AvailableForCRM From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AvailableForCRM', '', (Select AvailableForCRM From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(AwardType) and exists (Select AwardType From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AwardType') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select AwardType From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.AwardType' 
				End 
			else 
				If Not ((Select AwardType From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.AwardType', '', (Select AwardType From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BidDate) and exists (Select BidDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BidDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BidDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BidDate' 
				End 
			else 
				If Not ((Select BidDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BidDate', '', (Select BidDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BillableWarning) and exists (Select BillableWarning From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillableWarning') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BillableWarning From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillableWarning' 
				End 
			else 
				If Not ((Select BillableWarning From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BillableWarning', '', (Select BillableWarning From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BillByDefault) and exists (Select BillByDefault From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillByDefault') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BillByDefault From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillByDefault' 
				End 
			else 
				If Not ((Select BillByDefault From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BillByDefault', '', (Select BillByDefault From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BillByDefaultConsultants) and exists (Select BillByDefaultConsultants From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillByDefaultConsultants') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BillByDefaultConsultants From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillByDefaultConsultants' 
				End 
			else 
				If Not ((Select BillByDefaultConsultants From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BillByDefaultConsultants', '', (Select BillByDefaultConsultants From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BillByDefaultORTable) and exists (Select BillByDefaultORTable From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillByDefaultORTable') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BillByDefaultORTable From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillByDefaultORTable' 
				End 
			else 
				If ((Select BillByDefaultORTable From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BillByDefaultORTable', '', (Select BillByDefaultORTable From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BillByDefaultOtherExp) and exists (Select BillByDefaultOtherExp From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillByDefaultOtherExp') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BillByDefaultOtherExp From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillByDefaultOtherExp' 
				End 
			else 
				If Not ((Select BillByDefaultOtherExp From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BillByDefaultOtherExp', '', (Select BillByDefaultOtherExp From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Biller) and exists (Select Biller From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Biller') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Biller From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Biller' 
				End 
			else 
				If Not ((Select Biller From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Biller', '', (Select Biller From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BillingClientID) and exists (Select BillingClientID From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillingClientID') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BillingClientID From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillingClientID' 
				End 
			else 
				If Not ((Select BillingClientID From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BillingClientID', '', (Select BillingClientID From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BillingContactID) and exists (Select BillingContactID From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillingContactID') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BillingContactID From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillingContactID' 
				End 
			else 
				If Not ((Select BillingContactID From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BillingContactID', '', (Select BillingContactID From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BillingCurrencyCode) and exists (Select BillingCurrencyCode From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillingCurrencyCode') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BillingCurrencyCode From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillingCurrencyCode' 
				End 
			else 
				If Not ((Select BillingCurrencyCode From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BillingCurrencyCode', '', (Select BillingCurrencyCode From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BillingExchangeRate) and exists (Select BillingExchangeRate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillingExchangeRate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BillingExchangeRate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillingExchangeRate' 
				End 
			else 
				If ((Select BillingExchangeRate From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BillingExchangeRate', '', (Select BillingExchangeRate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BillWBS1) and exists (Select BillWBS1 From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillWBS1') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BillWBS1 From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillWBS1' 
				End 
			else 
				If Not ((Select BillWBS1 From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BillWBS1', '', (Select BillWBS1 From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BillWBS2) and exists (Select BillWBS2 From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillWBS2') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BillWBS2 From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillWBS2' 
				End 
			else 
				If Not ((Select BillWBS2 From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BillWBS2', '', (Select BillWBS2 From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BillWBS3) and exists (Select BillWBS3 From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillWBS3') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BillWBS3 From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BillWBS3' 
				End 
			else 
				If Not ((Select BillWBS3 From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BillWBS3', '', (Select BillWBS3 From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BudgetedFlag) and exists (Select BudgetedFlag From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BudgetedFlag') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BudgetedFlag From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BudgetedFlag' 
				End 
			else 
				If Not ((Select BudgetedFlag From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BudgetedFlag', '', (Select BudgetedFlag From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BudgetedLevels) and exists (Select BudgetedLevels From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BudgetedLevels') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BudgetedLevels From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BudgetedLevels' 
				End 
			else 
				If Not ((Select BudgetedLevels From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BudgetedLevels', '', (Select BudgetedLevels From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BudgetLevel) and exists (Select BudgetLevel From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BudgetLevel') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BudgetLevel From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BudgetLevel' 
				End 
			else 
				If Not ((Select BudgetLevel From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BudgetLevel', '', (Select BudgetLevel From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BudgetSource) and exists (Select BudgetSource From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BudgetSource') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BudgetSource From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BudgetSource' 
				End 
			else 
				If Not ((Select BudgetSource From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BudgetSource', '', (Select BudgetSource From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BudOHRate) and exists (Select BudOHRate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BudOHRate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BudOHRate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BudOHRate' 
				End 
			else 
				If ((Select BudOHRate From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BudOHRate', '', (Select BudOHRate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(BusinessDeveloperLead) and exists (Select BusinessDeveloperLead From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BusinessDeveloperLead') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select BusinessDeveloperLead From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.BusinessDeveloperLead' 
				End 
			else 
				If Not ((Select BusinessDeveloperLead From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, ParentID, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.BusinessDeveloperLead', '', 'PR.BusinessDeveloperLeadComponent', (Select BusinessDeveloperLead From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ChargeType) and exists (Select ChargeType From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ChargeType') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ChargeType From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ChargeType' 
				End 
			else 
				If Not ((Select ChargeType From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ChargeType', '', (Select ChargeType From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(City) and exists (Select City From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.City') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select City From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.City' 
				End 
			else 
				If Not ((Select City From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.City', '', (Select City From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(CLAddress) and exists (Select CLAddress From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CLAddress') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select CLAddress From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CLAddress' 
				End 
			else 
				If Not ((Select CLAddress From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.CLAddress', '', (Select CLAddress From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(CLBillingAddr) and exists (Select CLBillingAddr From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CLBillingAddr') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select CLBillingAddr From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CLBillingAddr' 
				End 
			else 
				If Not ((Select CLBillingAddr From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.CLBillingAddr', '', (Select CLBillingAddr From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ClientAlias) and exists (Select ClientAlias From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ClientAlias') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ClientAlias From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ClientAlias' 
				End 
			else 
				If Not ((Select ClientAlias From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ClientAlias', '', (Select ClientAlias From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ClientConfidential) and exists (Select ClientConfidential From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ClientConfidential') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ClientConfidential From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ClientConfidential' 
				End 
			else 
				If Not ((Select ClientConfidential From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ClientConfidential', '', (Select ClientConfidential From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ClientID) and exists (Select ClientID From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ClientID') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ClientID From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ClientID' 
				End 
			else 
				If Not ((Select ClientID From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ClientID', '', (Select ClientID From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Closed) and exists (Select Closed From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Closed') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Closed From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Closed' 
				End 
			else 
				If ((Select Closed From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Closed', '', (Select Closed From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(CloseDate) and exists (Select CloseDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CloseDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select CloseDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CloseDate' 
				End 
			else 
				If Not ((Select CloseDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.CloseDate', '', (Select CloseDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ClosedNotes) and exists (Select ClosedNotes From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ClosedNotes') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ClosedNotes From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ClosedNotes' 
				End 
			else 
				If Not ((Select ClosedNotes From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ClosedNotes', '', (Select ClosedNotes From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ClosedReason) and exists (Select ClosedReason From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ClosedReason') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ClosedReason From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ClosedReason' 
				End 
			else 
				If Not ((Select ClosedReason From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ClosedReason', '', (Select ClosedReason From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(CompetitionType) and exists (Select CompetitionType From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CompetitionType') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select CompetitionType From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CompetitionType' 
				End 
			else 
				If Not ((Select CompetitionType From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.CompetitionType', '', (Select CompetitionType From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ComplDateComment) and exists (Select ComplDateComment From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ComplDateComment') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ComplDateComment From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ComplDateComment' 
				End 
			else 
				If Not ((Select ComplDateComment From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ComplDateComment', '', (Select ComplDateComment From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ConstComplDate) and exists (Select ConstComplDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ConstComplDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ConstComplDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ConstComplDate' 
				End 
			else 
				If Not ((Select ConstComplDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ConstComplDate', '', (Select ConstComplDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ConsultFee) and exists (Select ConsultFee From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ConsultFee') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ConsultFee From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ConsultFee' 
				End 
			else 
				If ((Select ConsultFee From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ConsultFee', '', (Select ConsultFee From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ConsultFeeBillingCurrency) and exists (Select ConsultFeeBillingCurrency From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ConsultFeeBillingCurrency') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ConsultFeeBillingCurrency From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ConsultFeeBillingCurrency' 
				End 
			else 
				If ((Select ConsultFeeBillingCurrency From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ConsultFeeBillingCurrency', '', (Select ConsultFeeBillingCurrency From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ConsultFeeFunctionalCurrency) and exists (Select ConsultFeeFunctionalCurrency From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ConsultFeeFunctionalCurrency') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ConsultFeeFunctionalCurrency From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ConsultFeeFunctionalCurrency' 
				End 
			else 
				If ((Select ConsultFeeFunctionalCurrency From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ConsultFeeFunctionalCurrency', '', (Select ConsultFeeFunctionalCurrency From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ContactID) and exists (Select ContactID From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ContactID') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ContactID From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ContactID' 
				End 
			else 
				If Not ((Select ContactID From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ContactID', '', (Select ContactID From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ContractDate) and exists (Select ContractDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ContractDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ContractDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ContractDate' 
				End 
			else 
				If Not ((Select ContractDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ContractDate', '', (Select ContractDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ContractTypeGovCon) and exists (Select ContractTypeGovCon From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ContractTypeGovCon') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ContractTypeGovCon From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ContractTypeGovCon' 
				End 
			else 
				If Not ((Select ContractTypeGovCon From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ContractTypeGovCon', '', (Select ContractTypeGovCon From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(CostRateMeth) and exists (Select CostRateMeth From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CostRateMeth') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select CostRateMeth From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CostRateMeth' 
				End 
			else 
				If ((Select CostRateMeth From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.CostRateMeth', '', (Select CostRateMeth From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(CostRateTableNo) and exists (Select CostRateTableNo From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CostRateTableNo') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select CostRateTableNo From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CostRateTableNo' 
				End 
			else 
				If ((Select CostRateTableNo From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.CostRateTableNo', '', (Select CostRateTableNo From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Country) and exists (Select Country From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Country') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Country From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Country' 
				End 
			else 
				If Not ((Select Country From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Country', '', (Select Country From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(County) and exists (Select County From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.County') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select County From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.County' 
				End 
			else 
				If Not ((Select County From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.County', '', (Select County From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(CreateDate) and exists (Select CreateDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CreateDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select CreateDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CreateDate' 
				End 
			else 
				If Not ((Select CreateDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.CreateDate', '', (Select CreateDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(CreateUser) and exists (Select CreateUser From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CreateUser') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select CreateUser From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CreateUser' 
				End 
			else 
				If Not ((Select CreateUser From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.CreateUser', '', (Select CreateUser From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(CustomCurrencyCode) and exists (Select CustomCurrencyCode From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CustomCurrencyCode') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select CustomCurrencyCode From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.CustomCurrencyCode' 
				End 
			else 
				If Not ((Select CustomCurrencyCode From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.CustomCurrencyCode', '', (Select CustomCurrencyCode From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(DefaultEffortDriven) and exists (Select DefaultEffortDriven From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.DefaultEffortDriven') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select DefaultEffortDriven From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.DefaultEffortDriven' 
				End 
			else 
				If ((Select DefaultEffortDriven From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.DefaultEffortDriven', '', (Select DefaultEffortDriven From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(DefaultTaskType) and exists (Select DefaultTaskType From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.DefaultTaskType') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select DefaultTaskType From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.DefaultTaskType' 
				End 
			else 
				If ((Select DefaultTaskType From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.DefaultTaskType', '', (Select DefaultTaskType From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Description) and exists (Select Description From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Description') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Description From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Description' 
				End 
			else 
				If Not ((Select Description From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Description', '', (Select Description From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Duration) and exists (Select Duration From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Duration') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Duration From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Duration' 
				End 
			else 
				If Not ((Select Duration From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Duration', '', (Select Duration From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(EMail) and exists (Select EMail From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.EMail') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select EMail From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.EMail' 
				End 
			else 
				If Not ((Select EMail From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.EMail', '', (Select EMail From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(EndDate) and exists (Select EndDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.EndDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select EndDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.EndDate' 
				End 
			else 
				If Not ((Select EndDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.EndDate', '', (Select EndDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(EstCompletionDate) and exists (Select EstCompletionDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.EstCompletionDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select EstCompletionDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.EstCompletionDate' 
				End 
			else 
				If Not ((Select EstCompletionDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.EstCompletionDate', '', (Select EstCompletionDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(EstConstructionCost) and exists (Select EstConstructionCost From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.EstConstructionCost') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select EstConstructionCost From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.EstConstructionCost' 
				End 
			else 
				If ((Select EstConstructionCost From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.EstConstructionCost', '', (Select EstConstructionCost From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(EstEndDate) and exists (Select EstEndDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.EstEndDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select EstEndDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.EstEndDate' 
				End 
			else 
				If Not ((Select EstEndDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.EstEndDate', '', (Select EstEndDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(EstFees) and exists (Select EstFees From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.EstFees') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select EstFees From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.EstFees' 
				End 
			else 
				If ((Select EstFees From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.EstFees', '', (Select EstFees From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(EstStartDate) and exists (Select EstStartDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.EstStartDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select EstStartDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.EstStartDate' 
				End 
			else 
				If Not ((Select EstStartDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.EstStartDate', '', (Select EstStartDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ExpPctComp) and exists (Select ExpPctComp From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ExpPctComp') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ExpPctComp From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ExpPctComp' 
				End 
			else 
				If ((Select ExpPctComp From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ExpPctComp', '', (Select ExpPctComp From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Fax) and exists (Select Fax From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Fax') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Fax From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Fax' 
				End 
			else 
				If Not ((Select Fax From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Fax', '', (Select Fax From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FaxFormat) and exists (Select FaxFormat From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FaxFormat') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FaxFormat From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FaxFormat' 
				End 
			else 
				If Not ((Select FaxFormat From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FaxFormat', '', (Select FaxFormat From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FEAddlExpenses) and exists (Select FEAddlExpenses From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FEAddlExpenses') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FEAddlExpenses From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FEAddlExpenses' 
				End 
			else 
				If ((Select FEAddlExpenses From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FEAddlExpenses', '', (Select FEAddlExpenses From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FEAddlExpensesPct) and exists (Select FEAddlExpensesPct From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FEAddlExpensesPct') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FEAddlExpensesPct From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FEAddlExpensesPct' 
				End 
			else 
				If ((Select FEAddlExpensesPct From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FEAddlExpensesPct', '', (Select FEAddlExpensesPct From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FederalInd) and exists (Select FederalInd From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FederalInd') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FederalInd From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FederalInd' 
				End 
			else 
				If Not ((Select FederalInd From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FederalInd', '', (Select FederalInd From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Fee) and exists (Select Fee From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Fee') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Fee From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Fee' 
				End 
			else 
				If ((Select Fee From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Fee', '', (Select Fee From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FeeBillingCurrency) and exists (Select FeeBillingCurrency From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FeeBillingCurrency') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FeeBillingCurrency From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FeeBillingCurrency' 
				End 
			else 
				If ((Select FeeBillingCurrency From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FeeBillingCurrency', '', (Select FeeBillingCurrency From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FeeDirExp) and exists (Select FeeDirExp From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FeeDirExp') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FeeDirExp From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FeeDirExp' 
				End 
			else 
				If ((Select FeeDirExp From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FeeDirExp', '', (Select FeeDirExp From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FeeDirExpBillingCurrency) and exists (Select FeeDirExpBillingCurrency From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FeeDirExpBillingCurrency') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FeeDirExpBillingCurrency From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FeeDirExpBillingCurrency' 
				End 
			else 
				If ((Select FeeDirExpBillingCurrency From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FeeDirExpBillingCurrency', '', (Select FeeDirExpBillingCurrency From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FeeDirExpFunctionalCurrency) and exists (Select FeeDirExpFunctionalCurrency From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FeeDirExpFunctionalCurrency') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FeeDirExpFunctionalCurrency From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FeeDirExpFunctionalCurrency' 
				End 
			else 
				If ((Select FeeDirExpFunctionalCurrency From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FeeDirExpFunctionalCurrency', '', (Select FeeDirExpFunctionalCurrency From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FeeDirLab) and exists (Select FeeDirLab From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FeeDirLab') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FeeDirLab From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FeeDirLab' 
				End 
			else 
				If ((Select FeeDirLab From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FeeDirLab', '', (Select FeeDirLab From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FeeDirLabBillingCurrency) and exists (Select FeeDirLabBillingCurrency From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FeeDirLabBillingCurrency') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FeeDirLabBillingCurrency From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FeeDirLabBillingCurrency' 
				End 
			else 
				If ((Select FeeDirLabBillingCurrency From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FeeDirLabBillingCurrency', '', (Select FeeDirLabBillingCurrency From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FeeDirLabFunctionalCurrency) and exists (Select FeeDirLabFunctionalCurrency From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FeeDirLabFunctionalCurrency') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FeeDirLabFunctionalCurrency From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FeeDirLabFunctionalCurrency' 
				End 
			else 
				If ((Select FeeDirLabFunctionalCurrency From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FeeDirLabFunctionalCurrency', '', (Select FeeDirLabFunctionalCurrency From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FeeFunctionalCurrency) and exists (Select FeeFunctionalCurrency From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FeeFunctionalCurrency') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FeeFunctionalCurrency From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FeeFunctionalCurrency' 
				End 
			else 
				If ((Select FeeFunctionalCurrency From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FeeFunctionalCurrency', '', (Select FeeFunctionalCurrency From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FEOther) and exists (Select FEOther From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FEOther') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FEOther From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FEOther' 
				End 
			else 
				If ((Select FEOther From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FEOther', '', (Select FEOther From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FEOtherPct) and exists (Select FEOtherPct From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FEOtherPct') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FEOtherPct From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FEOtherPct' 
				End 
			else 
				If ((Select FEOtherPct From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FEOtherPct', '', (Select FEOtherPct From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FESurcharge) and exists (Select FESurcharge From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FESurcharge') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FESurcharge From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FESurcharge' 
				End 
			else 
				If ((Select FESurcharge From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FESurcharge', '', (Select FESurcharge From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FESurchargePct) and exists (Select FESurchargePct From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FESurchargePct') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FESurchargePct From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FESurchargePct' 
				End 
			else 
				If ((Select FESurchargePct From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FESurchargePct', '', (Select FESurchargePct From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FirmCost) and exists (Select FirmCost From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FirmCost') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FirmCost From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FirmCost' 
				End 
			else 
				If ((Select FirmCost From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FirmCost', '', (Select FirmCost From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(FirmCostComment) and exists (Select FirmCostComment From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FirmCostComment') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select FirmCostComment From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.FirmCostComment' 
				End 
			else 
				If Not ((Select FirmCostComment From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.FirmCostComment', '', (Select FirmCostComment From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ICBillingExp) and exists (Select ICBillingExp From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ICBillingExp') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ICBillingExp From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ICBillingExp' 
				End 
			else 
				If Not ((Select ICBillingExp From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ICBillingExp', '', (Select ICBillingExp From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ICBillingExpMethod) and exists (Select ICBillingExpMethod From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ICBillingExpMethod') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ICBillingExpMethod From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ICBillingExpMethod' 
				End 
			else 
				If ((Select ICBillingExpMethod From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ICBillingExpMethod', '', (Select ICBillingExpMethod From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ICBillingExpMult) and exists (Select ICBillingExpMult From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ICBillingExpMult') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ICBillingExpMult From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ICBillingExpMult' 
				End 
			else 
				If ((Select ICBillingExpMult From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ICBillingExpMult', '', (Select ICBillingExpMult From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ICBillingExpTableNo) and exists (Select ICBillingExpTableNo From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ICBillingExpTableNo') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ICBillingExpTableNo From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ICBillingExpTableNo' 
				End 
			else 
				If ((Select ICBillingExpTableNo From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ICBillingExpTableNo', '', (Select ICBillingExpTableNo From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ICBillingLab) and exists (Select ICBillingLab From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ICBillingLab') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ICBillingLab From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ICBillingLab' 
				End 
			else 
				If Not ((Select ICBillingLab From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ICBillingLab', '', (Select ICBillingLab From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ICBillingLabMethod) and exists (Select ICBillingLabMethod From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ICBillingLabMethod') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ICBillingLabMethod From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ICBillingLabMethod' 
				End 
			else 
				If ((Select ICBillingLabMethod From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ICBillingLabMethod', '', (Select ICBillingLabMethod From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ICBillingLabMult) and exists (Select ICBillingLabMult From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ICBillingLabMult') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ICBillingLabMult From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ICBillingLabMult' 
				End 
			else 
				If ((Select ICBillingLabMult From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ICBillingLabMult', '', (Select ICBillingLabMult From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ICBillingLabTableNo) and exists (Select ICBillingLabTableNo From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ICBillingLabTableNo') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ICBillingLabTableNo From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ICBillingLabTableNo' 
				End 
			else 
				If ((Select ICBillingLabTableNo From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ICBillingLabTableNo', '', (Select ICBillingLabTableNo From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(IQID) and exists (Select IQID From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.IQID') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select IQID From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.IQID' 
				End 
			else 
				If Not ((Select IQID From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.IQID', '', (Select IQID From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(IQLastUpdate) and exists (Select IQLastUpdate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.IQLastUpdate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select IQLastUpdate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.IQLastUpdate' 
				End 
			else 
				If Not ((Select IQLastUpdate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.IQLastUpdate', '', (Select IQLastUpdate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(LabBillTable) and exists (Select LabBillTable From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.LabBillTable') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select LabBillTable From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.LabBillTable' 
				End 
			else 
				If ((Select LabBillTable From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.LabBillTable', '', (Select LabBillTable From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(LabCostTable) and exists (Select LabCostTable From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.LabCostTable') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select LabCostTable From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.LabCostTable' 
				End 
			else 
				If ((Select LabCostTable From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.LabCostTable', '', (Select LabCostTable From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(LabPctComp) and exists (Select LabPctComp From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.LabPctComp') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select LabPctComp From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.LabPctComp' 
				End 
			else 
				If ((Select LabPctComp From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.LabPctComp', '', (Select LabPctComp From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(LineItemApproval) and exists (Select LineItemApproval From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.LineItemApproval') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select LineItemApproval From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.LineItemApproval' 
				End 
			else 
				If Not ((Select LineItemApproval From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.LineItemApproval', '', (Select LineItemApproval From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(LineItemApprovalEK) and exists (Select LineItemApprovalEK From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.LineItemApprovalEK') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select LineItemApprovalEK From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.LineItemApprovalEK' 
				End 
			else 
				If Not ((Select LineItemApprovalEK From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.LineItemApprovalEK', '', (Select LineItemApprovalEK From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Locale) and exists (Select Locale From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Locale') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Locale From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Locale' 
				End 
			else 
				If Not ((Select Locale From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Locale', '', (Select Locale From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(LongName) and exists (Select LongName From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.LongName') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select LongName From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.LongName' 
				End 
			else 
				If Not ((Select LongName From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.LongName', '', (Select LongName From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(LostTo) and exists (Select LostTo From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.LostTo') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select LostTo From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.LostTo' 
				End 
			else 
				If Not ((Select LostTo From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.LostTo', '', (Select LostTo From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(MarketingCoordinator) and exists (Select MarketingCoordinator From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.MarketingCoordinator') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select MarketingCoordinator From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.MarketingCoordinator' 
				End 
			else 
				If Not ((Select MarketingCoordinator From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, ParentID, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.MarketingCoordinator', '','PR.MarketingCoordinatorComponent', (Select MarketingCoordinator From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(MasterContract) and exists (Select MasterContract From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.MasterContract') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select MasterContract From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.MasterContract' 
				End 
			else 
				If Not ((Select MasterContract From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.MasterContract', '', (Select MasterContract From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Memo) and exists (Select Memo From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Memo') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Memo From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Memo' 
				End 
			else 
				If Not ((Select Memo From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Memo', '', (Select Memo From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(MultAmt) and exists (Select MultAmt From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.MultAmt') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select MultAmt From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.MultAmt' 
				End 
			else 
				If ((Select MultAmt From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.MultAmt', '', (Select MultAmt From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(NAICS) and exists (Select NAICS From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.NAICS') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select NAICS From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.NAICS' 
				End 
			else 
				If Not ((Select NAICS From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.NAICS', '', (Select NAICS From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Name) and exists (Select Name From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Name') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Name From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Name' 
				End 
			else 
				If Not ((Select Name From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Name', '', (Select Name From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(OpenDate) and exists (Select OpenDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.OpenDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select OpenDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.OpenDate' 
				End 
			else 
				If Not ((Select OpenDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.OpenDate', '', (Select OpenDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(OpportunityID) and exists (Select OpportunityID From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.OpportunityID') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select OpportunityID From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.OpportunityID' 
				End 
			else 
				If Not ((Select OpportunityID From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.OpportunityID', '', (Select OpportunityID From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Org) and exists (Select Org From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Org') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Org From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Org' 
				End 
			else 
				If Not ((Select Org From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Org', '', (Select Org From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(PayRateMeth) and exists (Select PayRateMeth From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PayRateMeth') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select PayRateMeth From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PayRateMeth' 
				End 
			else 
				If ((Select PayRateMeth From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.PayRateMeth', '', (Select PayRateMeth From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(PayRateTableNo) and exists (Select PayRateTableNo From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PayRateTableNo') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select PayRateTableNo From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PayRateTableNo' 
				End 
			else 
				If ((Select PayRateTableNo From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.PayRateTableNo', '', (Select PayRateTableNo From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(PctComp) and exists (Select PctComp From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PctComp') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select PctComp From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PctComp' 
				End 
			else 
				If ((Select PctComp From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.PctComp', '', (Select PctComp From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(PeriodOfPerformance) and exists (Select PeriodOfPerformance From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PeriodOfPerformance') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select PeriodOfPerformance From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PeriodOfPerformance' 
				End 
			else 
				If ((Select PeriodOfPerformance From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.PeriodOfPerformance', '', (Select PeriodOfPerformance From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Phone) and exists (Select Phone From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Phone') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Phone From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Phone' 
				End 
			else 
				If Not ((Select Phone From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Phone', '', (Select Phone From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(PhoneFormat) and exists (Select PhoneFormat From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PhoneFormat') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select PhoneFormat From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PhoneFormat' 
				End 
			else 
				If Not ((Select PhoneFormat From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.PhoneFormat', '', (Select PhoneFormat From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(PIMID) and exists (Select PIMID From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PIMID') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select PIMID From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PIMID' 
				End 
			else 
				If Not ((Select PIMID From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.PIMID', '', (Select PIMID From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(PlanID) and exists (Select PlanID From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PlanID') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select PlanID From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PlanID' 
				End 
			else 
				If Not ((Select PlanID From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.PlanID', '', (Select PlanID From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(POCNSRate) and exists (Select POCNSRate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.POCNSRate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select POCNSRate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.POCNSRate' 
				End 
			else 
				If ((Select POCNSRate From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.POCNSRate', '', (Select POCNSRate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(PORMBRate) and exists (Select PORMBRate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PORMBRate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select PORMBRate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PORMBRate' 
				End 
			else 
				If ((Select PORMBRate From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.PORMBRate', '', (Select PORMBRate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(PreAwardWBS1) and exists (Select PreAwardWBS1 From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PreAwardWBS1') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select PreAwardWBS1 From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PreAwardWBS1' 
				End 
			else 
				If Not ((Select PreAwardWBS1 From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.PreAwardWBS1', '', (Select PreAwardWBS1 From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Principal) and exists (Select Principal From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Principal') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Principal From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Principal' 
				End 
			else 
				If Not ((Select Principal From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Principal', '', (Select Principal From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Probability) and exists (Select Probability From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Probability') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Probability From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Probability' 
				End 
			else 
				If ((Select Probability From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Probability', '', (Select Probability From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ProfServicesComplDate) and exists (Select ProfServicesComplDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ProfServicesComplDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ProfServicesComplDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ProfServicesComplDate' 
				End 
			else 
				If Not ((Select ProfServicesComplDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ProfServicesComplDate', '', (Select ProfServicesComplDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ProjectCurrencyCode) and exists (Select ProjectCurrencyCode From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ProjectCurrencyCode') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ProjectCurrencyCode From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ProjectCurrencyCode' 
				End 
			else 
				If Not ((Select ProjectCurrencyCode From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ProjectCurrencyCode', '', (Select ProjectCurrencyCode From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ProjectExchangeRate) and exists (Select ProjectExchangeRate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ProjectExchangeRate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ProjectExchangeRate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ProjectExchangeRate' 
				End 
			else 
				If ((Select ProjectExchangeRate From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ProjectExchangeRate', '', (Select ProjectExchangeRate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ProjectTemplate) and exists (Select ProjectTemplate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ProjectTemplate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ProjectTemplate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ProjectTemplate' 
				End 
			else 
				If Not ((Select ProjectTemplate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ProjectTemplate', '', (Select ProjectTemplate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ProjectType) and exists (Select ProjectType From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ProjectType') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ProjectType From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ProjectType' 
				End 
			else 
				If Not ((Select ProjectType From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ProjectType', '', (Select ProjectType From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ProjMgr) and exists (Select ProjMgr From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ProjMgr') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ProjMgr From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ProjMgr' 
				End 
			else 
				If Not ((Select ProjMgr From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ProjMgr', '', (Select ProjMgr From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ProposalManager) and exists (Select ProposalManager From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ProposalManager') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ProposalManager From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ProposalManager' 
				End 
			else 
				If Not ((Select ProposalManager From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, ParentID, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ProposalManager', '', 'PR.ProposalManagerComponent', (Select ProposalManager From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ProposalWBS1) and exists (Select ProposalWBS1 From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ProposalWBS1') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ProposalWBS1 From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ProposalWBS1' 
				End 
			else 
				If Not ((Select ProposalWBS1 From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ProposalWBS1', '', (Select ProposalWBS1 From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(PublicNoticeDate) and exists (Select PublicNoticeDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PublicNoticeDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select PublicNoticeDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.PublicNoticeDate' 
				End 
			else 
				If Not ((Select PublicNoticeDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.PublicNoticeDate', '', (Select PublicNoticeDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ReadOnly) and exists (Select ReadOnly From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReadOnly') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ReadOnly From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReadOnly' 
				End 
			else 
				If ((Select ReadOnly From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ReadOnly', '', (Select ReadOnly From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ReadyForApproval) and exists (Select ReadyForApproval From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReadyForApproval') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ReadyForApproval From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReadyForApproval' 
				End 
			else 
				If Not ((Select ReadyForApproval From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ReadyForApproval', '', (Select ReadyForApproval From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ReadyForProcessing) and exists (Select ReadyForProcessing From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReadyForProcessing') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ReadyForProcessing From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReadyForProcessing' 
				End 
			else 
				If Not ((Select ReadyForProcessing From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ReadyForProcessing', '', (Select ReadyForProcessing From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Referable) and exists (Select Referable From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Referable') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Referable From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Referable' 
				End 
			else 
				If Not ((Select Referable From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Referable', '', (Select Referable From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ReimbAllow) and exists (Select ReimbAllow From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllow') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ReimbAllow From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllow' 
				End 
			else 
				If ((Select ReimbAllow From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ReimbAllow', '', (Select ReimbAllow From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ReimbAllowBillingCurrency) and exists (Select ReimbAllowBillingCurrency From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllowBillingCurrency') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ReimbAllowBillingCurrency From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllowBillingCurrency' 
				End 
			else 
				If ((Select ReimbAllowBillingCurrency From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ReimbAllowBillingCurrency', '', (Select ReimbAllowBillingCurrency From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ReimbAllowCons) and exists (Select ReimbAllowCons From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllowCons') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ReimbAllowCons From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllowCons' 
				End 
			else 
				If ((Select ReimbAllowCons From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ReimbAllowCons', '', (Select ReimbAllowCons From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ReimbAllowConsBillingCurrency) and exists (Select ReimbAllowConsBillingCurrency From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllowConsBillingCurrency') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ReimbAllowConsBillingCurrency From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllowConsBillingCurrency' 
				End 
			else 
				If ((Select ReimbAllowConsBillingCurrency From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ReimbAllowConsBillingCurrency', '', (Select ReimbAllowConsBillingCurrency From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ReimbAllowConsFunctionalCurrency) and exists (Select ReimbAllowConsFunctionalCurrency From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllowConsFunctionalCurrency') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ReimbAllowConsFunctionalCurrency From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllowConsFunctionalCurrency' 
				End 
			else 
				If ((Select ReimbAllowConsFunctionalCurrency From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ReimbAllowConsFunctionalCurrency', '', (Select ReimbAllowConsFunctionalCurrency From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ReimbAllowExp) and exists (Select ReimbAllowExp From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllowExp') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ReimbAllowExp From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllowExp' 
				End 
			else 
				If ((Select ReimbAllowExp From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ReimbAllowExp', '', (Select ReimbAllowExp From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ReimbAllowExpBillingCurrency) and exists (Select ReimbAllowExpBillingCurrency From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllowExpBillingCurrency') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ReimbAllowExpBillingCurrency From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllowExpBillingCurrency' 
				End 
			else 
				If ((Select ReimbAllowExpBillingCurrency From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ReimbAllowExpBillingCurrency', '', (Select ReimbAllowExpBillingCurrency From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ReimbAllowExpFunctionalCurrency) and exists (Select ReimbAllowExpFunctionalCurrency From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllowExpFunctionalCurrency') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ReimbAllowExpFunctionalCurrency From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllowExpFunctionalCurrency' 
				End 
			else 
				If ((Select ReimbAllowExpFunctionalCurrency From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ReimbAllowExpFunctionalCurrency', '', (Select ReimbAllowExpFunctionalCurrency From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ReimbAllowFunctionalCurrency) and exists (Select ReimbAllowFunctionalCurrency From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllowFunctionalCurrency') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ReimbAllowFunctionalCurrency From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ReimbAllowFunctionalCurrency' 
				End 
			else 
				If ((Select ReimbAllowFunctionalCurrency From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ReimbAllowFunctionalCurrency', '', (Select ReimbAllowFunctionalCurrency From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RequireComments) and exists (Select RequireComments From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RequireComments') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RequireComments From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RequireComments' 
				End 
			else 
				If Not ((Select RequireComments From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RequireComments', '', (Select RequireComments From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Responsibility) and exists (Select Responsibility From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Responsibility') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Responsibility From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Responsibility' 
				End 
			else 
				If Not ((Select Responsibility From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Responsibility', '', (Select Responsibility From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RestrictChargeCompanies) and exists (Select RestrictChargeCompanies From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RestrictChargeCompanies') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RestrictChargeCompanies From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RestrictChargeCompanies' 
				End 
			else 
				If Not ((Select RestrictChargeCompanies From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RestrictChargeCompanies', '', (Select RestrictChargeCompanies From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Revenue) and exists (Select Revenue From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Revenue') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Revenue From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Revenue' 
				End 
			else 
				If ((Select Revenue From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Revenue', '', (Select Revenue From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RevenueMethod) and exists (Select RevenueMethod From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevenueMethod') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RevenueMethod From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevenueMethod' 
				End 
			else 
				If Not ((Select RevenueMethod From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RevenueMethod', '', (Select RevenueMethod From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RevType) and exists (Select RevType From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevType') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RevType From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevType' 
				End 
			else 
				If Not ((Select RevType From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RevType', '', (Select RevType From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RevType2) and exists (Select RevType2 From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevType2') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RevType2 From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevType2' 
				End 
			else 
				If Not ((Select RevType2 From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RevType2', '', (Select RevType2 From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RevType3) and exists (Select RevType3 From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevType3') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RevType3 From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevType3' 
				End 
			else 
				If Not ((Select RevType3 From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RevType3', '', (Select RevType3 From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RevType4) and exists (Select RevType4 From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevType4') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RevType4 From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevType4' 
				End 
			else 
				If Not ((Select RevType4 From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RevType4', '', (Select RevType4 From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RevType5) and exists (Select RevType5 From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevType5') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RevType5 From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevType5' 
				End 
			else 
				If Not ((Select RevType5 From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RevType5', '', (Select RevType5 From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RevUpsetCategoryToAdjust) and exists (Select RevUpsetCategoryToAdjust From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetCategoryToAdjust') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RevUpsetCategoryToAdjust From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetCategoryToAdjust' 
				End 
			else 
				If ((Select RevUpsetCategoryToAdjust From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RevUpsetCategoryToAdjust', '', (Select RevUpsetCategoryToAdjust From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RevUpsetIncludeComp) and exists (Select RevUpsetIncludeComp From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetIncludeComp') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RevUpsetIncludeComp From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetIncludeComp' 
				End 
			else 
				If Not ((Select RevUpsetIncludeComp From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RevUpsetIncludeComp', '', (Select RevUpsetIncludeComp From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RevUpsetIncludeCompDirExp) and exists (Select RevUpsetIncludeCompDirExp From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetIncludeCompDirExp') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RevUpsetIncludeCompDirExp From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetIncludeCompDirExp' 
				End 
			else 
				If Not ((Select RevUpsetIncludeCompDirExp From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RevUpsetIncludeCompDirExp', '', (Select RevUpsetIncludeCompDirExp From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RevUpsetIncludeCons) and exists (Select RevUpsetIncludeCons From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetIncludeCons') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RevUpsetIncludeCons From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetIncludeCons' 
				End 
			else 
				If Not ((Select RevUpsetIncludeCons From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RevUpsetIncludeCons', '', (Select RevUpsetIncludeCons From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RevUpsetIncludeReimb) and exists (Select RevUpsetIncludeReimb From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetIncludeReimb') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RevUpsetIncludeReimb From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetIncludeReimb' 
				End 
			else 
				If Not ((Select RevUpsetIncludeReimb From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RevUpsetIncludeReimb', '', (Select RevUpsetIncludeReimb From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RevUpsetIncludeReimbCons) and exists (Select RevUpsetIncludeReimbCons From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetIncludeReimbCons') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RevUpsetIncludeReimbCons From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetIncludeReimbCons' 
				End 
			else 
				If Not ((Select RevUpsetIncludeReimbCons From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RevUpsetIncludeReimbCons', '', (Select RevUpsetIncludeReimbCons From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RevUpsetLimits) and exists (Select RevUpsetLimits From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetLimits') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RevUpsetLimits From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetLimits' 
				End 
			else 
				If Not ((Select RevUpsetLimits From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RevUpsetLimits', '', (Select RevUpsetLimits From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RevUpsetWBS2) and exists (Select RevUpsetWBS2 From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetWBS2') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RevUpsetWBS2 From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetWBS2' 
				End 
			else 
				If Not ((Select RevUpsetWBS2 From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RevUpsetWBS2', '', (Select RevUpsetWBS2 From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(RevUpsetWBS3) and exists (Select RevUpsetWBS3 From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetWBS3') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select RevUpsetWBS3 From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.RevUpsetWBS3' 
				End 
			else 
				If Not ((Select RevUpsetWBS3 From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.RevUpsetWBS3', '', (Select RevUpsetWBS3 From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(ServProCode) and exists (Select ServProCode From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ServProCode') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select ServProCode From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.ServProCode' 
				End 
			else 
				If Not ((Select ServProCode From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.ServProCode', '', (Select ServProCode From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(SFID) and exists (Select SFID From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.SFID') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select SFID From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.SFID' 
				End 
			else 
				If Not ((Select SFID From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.SFID', '', (Select SFID From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(SFLastModifiedDate) and exists (Select SFLastModifiedDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.SFLastModifiedDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select SFLastModifiedDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.SFLastModifiedDate' 
				End 
			else 
				If Not ((Select SFLastModifiedDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.SFLastModifiedDate', '', (Select SFLastModifiedDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(SiblingWBS1) and exists (Select SiblingWBS1 From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.SiblingWBS1') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select SiblingWBS1 From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.SiblingWBS1' 
				End 
			else 
				If Not ((Select SiblingWBS1 From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.SiblingWBS1', '', (Select SiblingWBS1 From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Solicitation) and exists (Select Solicitation From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Solicitation') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Solicitation From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Solicitation' 
				End 
			else 
				If Not ((Select Solicitation From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Solicitation', '', (Select Solicitation From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(SolicitationNum) and exists (Select SolicitationNum From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.SolicitationNum') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select SolicitationNum From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.SolicitationNum' 
				End 
			else 
				If Not ((Select SolicitationNum From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.SolicitationNum', '', (Select SolicitationNum From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Source) and exists (Select Source From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Source') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Source From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Source' 
				End 
			else 
				If Not ((Select Source From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Source', '', (Select Source From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Stage) and exists (Select Stage From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Stage') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Stage From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Stage' 
				End 
			else 
				If Not ((Select Stage From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Stage', '', (Select Stage From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(StartDate) and exists (Select StartDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.StartDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select StartDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.StartDate' 
				End 
			else 
				If Not ((Select StartDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.StartDate', '', (Select StartDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(State) and exists (Select State From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.State') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select State From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.State' 
				End 
			else 
				If Not ((Select State From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.State', '', (Select State From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Status) and exists (Select Status From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Status') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Status From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Status' 
				End 
			else 
				If Not ((Select Status From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Status', '', (Select Status From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(SubLevel) and exists (Select SubLevel From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.SubLevel') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select SubLevel From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.SubLevel' 
				End 
			else 
				If Not ((Select SubLevel From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.SubLevel', '', (Select SubLevel From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Supervisor) and exists (Select Supervisor From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Supervisor') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Supervisor From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Supervisor' 
				End 
			else 
				If Not ((Select Supervisor From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Supervisor', '', (Select Supervisor From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Timescale) and exists (Select Timescale From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Timescale') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Timescale From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Timescale' 
				End 
			else 
				If Not ((Select Timescale From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Timescale', '', (Select Timescale From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(TKCheckRPDate) and exists (Select TKCheckRPDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TKCheckRPDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select TKCheckRPDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TKCheckRPDate' 
				End 
			else 
				If Not ((Select TKCheckRPDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.TKCheckRPDate', '', (Select TKCheckRPDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(TKCheckRPPlannedHrs) and exists (Select TKCheckRPPlannedHrs From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TKCheckRPPlannedHrs') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select TKCheckRPPlannedHrs From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TKCheckRPPlannedHrs' 
				End 
			else 
				If Not ((Select TKCheckRPPlannedHrs From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.TKCheckRPPlannedHrs', '', (Select TKCheckRPPlannedHrs From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(TLChargeBandExternalCode) and exists (Select TLChargeBandExternalCode From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TLChargeBandExternalCode') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select TLChargeBandExternalCode From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TLChargeBandExternalCode' 
				End 
			else 
				If Not ((Select TLChargeBandExternalCode From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.TLChargeBandExternalCode', '', (Select TLChargeBandExternalCode From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(TLChargeBandInternalKey) and exists (Select TLChargeBandInternalKey From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TLChargeBandInternalKey') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select TLChargeBandInternalKey From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TLChargeBandInternalKey' 
				End 
			else 
				If Not ((Select TLChargeBandInternalKey From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.TLChargeBandInternalKey', '', (Select TLChargeBandInternalKey From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(TLInternalKey) and exists (Select TLInternalKey From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TLInternalKey') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select TLInternalKey From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TLInternalKey' 
				End 
			else 
				If Not ((Select TLInternalKey From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.TLInternalKey', '', (Select TLInternalKey From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(TLProjectID) and exists (Select TLProjectID From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TLProjectID') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select TLProjectID From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TLProjectID' 
				End 
			else 
				If Not ((Select TLProjectID From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.TLProjectID', '', (Select TLProjectID From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(TLProjectName) and exists (Select TLProjectName From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TLProjectName') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select TLProjectName From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TLProjectName' 
				End 
			else 
				If Not ((Select TLProjectName From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.TLProjectName', '', (Select TLProjectName From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(TLSyncModDate) and exists (Select TLSyncModDate From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TLSyncModDate') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select TLSyncModDate From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TLSyncModDate' 
				End 
			else 
				If Not ((Select TLSyncModDate From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.TLSyncModDate', '', (Select TLSyncModDate From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(TotalContractValue) and exists (Select TotalContractValue From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TotalContractValue') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select TotalContractValue From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TotalContractValue' 
				End 
			else 
				If ((Select TotalContractValue From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.TotalContractValue', '', (Select TotalContractValue From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(TotalCostComment) and exists (Select TotalCostComment From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TotalCostComment') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select TotalCostComment From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TotalCostComment' 
				End 
			else 
				If Not ((Select TotalCostComment From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.TotalCostComment', '', (Select TotalCostComment From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(TotalProjectCost) and exists (Select TotalProjectCost From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TotalProjectCost') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select TotalProjectCost From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.TotalProjectCost' 
				End 
			else 
				If ((Select TotalProjectCost From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.TotalProjectCost', '', (Select TotalProjectCost From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Type) and exists (Select Type From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Type') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Type From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Type' 
				End 
			else 
				If Not ((Select Type From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Type', '', (Select Type From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(UnitTable) and exists (Select UnitTable From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.UnitTable') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select UnitTable From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.UnitTable' 
				End 
			else 
				If Not ((Select UnitTable From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.UnitTable', '', (Select UnitTable From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(UtilizationScheduleFlg) and exists (Select UtilizationScheduleFlg From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.UtilizationScheduleFlg') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select UtilizationScheduleFlg From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.UtilizationScheduleFlg' 
				End 
			else 
				If Not ((Select UtilizationScheduleFlg From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, ParentID, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.UtilizationScheduleFlg', '','PR.StageInfo', (Select UtilizationScheduleFlg From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(VersionID) and exists (Select VersionID From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.VersionID') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select VersionID From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.VersionID' 
				End 
			else 
				If ((Select VersionID From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.VersionID', '', (Select VersionID From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(WeightedRevenue) and exists (Select WeightedRevenue From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.WeightedRevenue') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select WeightedRevenue From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.WeightedRevenue' 
				End 
			else 
				If ((Select WeightedRevenue From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.WeightedRevenue', '', (Select WeightedRevenue From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(XCharge) and exists (Select XCharge From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.XCharge') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select XCharge From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.XCharge' 
				End 
			else 
				If Not ((Select XCharge From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.XCharge', '', (Select XCharge From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(XChargeMethod) and exists (Select XChargeMethod From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.XChargeMethod') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select XChargeMethod From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.XChargeMethod' 
				End 
			else 
				If ((Select XChargeMethod From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.XChargeMethod', '', (Select XChargeMethod From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(XChargeMult) and exists (Select XChargeMult From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.XChargeMult') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select XChargeMult From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.XChargeMult' 
				End 
			else 
				If ((Select XChargeMult From inserted) <> 0) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.XChargeMult', '', (Select XChargeMult From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
 
	If UPDATE(Zip) and exists (Select Zip From inserted) 
		Begin 
			if exists (select InfoCenterArea from CFGScreenDesignerData Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Zip') 
				Begin 
					Print 'Update CFGScreenDesignerData Set DefaultValue ... '
					Update CFGScreenDesignerData Set DefaultValue = (Select Zip From inserted), ModUser = (Select ModUser From inserted), ModDate = (Select ModDate From inserted) 
					Where InfoCenterArea = 'Projects' and GridID = 'X' and ComponentID = 'PR.Zip' 
				End 
			else 
				If Not ((Select Zip From inserted) is null) 
					Begin 
						Print 'Insert CFGScreenDesignerData Add DefaultValue ... '
						Insert CFGScreenDesignerData (InfoCenterArea, TabID, GridID, ComponentID, ComponentType, DefaultValue, ModUser, ModDate) 
						Select 'Projects', '', 'X', 'PR.Zip', '', (Select Zip From inserted), (Select ModUser From inserted), (Select ModDate From inserted) 
					End 
		End 
  
 
	SET NOCOUNT OFF
END
GO
ALTER TABLE [dbo].[PRDefaults] ADD CONSTRAINT [PRDefaultsPK] PRIMARY KEY CLUSTERED ([Type]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
