CREATE TABLE [dbo].[FW_CFGEnabledCurrencies]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGEnabledCurrencies] ADD CONSTRAINT [FW_CFGEnabledCurrenciesPK] PRIMARY KEY NONCLUSTERED ([Company], [CurrencyCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
