CREATE TABLE [dbo].[FW_CFGCountryDesc]
(
[ISOCountryCode] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Country] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__FW_CFGCount__Seq__706BB9EF] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGCountryDesc] ADD CONSTRAINT [FW_CFGCountryDescPK] PRIMARY KEY CLUSTERED ([ISOCountryCode], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
