CREATE TABLE [dbo].[unDetailCustomFields]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Unit] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[unDetailCustomFields] ADD CONSTRAINT [unDetailCustomFieldsPK] PRIMARY KEY NONCLUSTERED ([Batch], [Unit], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
