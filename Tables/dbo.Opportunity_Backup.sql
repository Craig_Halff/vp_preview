CREATE TABLE [dbo].[Opportunity_Backup]
(
[OpportunityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Opportunity] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address3] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OpportunityType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Stage] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EstStartDate] [datetime] NULL,
[EstCompletionDate] [datetime] NULL,
[EstFees] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportuni__EstFe__6CDB202D] DEFAULT ((0)),
[EstConstructionCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportuni__EstCo__6DCF4466] DEFAULT ((0)),
[Revenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportuni__Reven__6EC3689F] DEFAULT ((0)),
[Probability] [smallint] NOT NULL CONSTRAINT [DF__Opportuni__Proba__6FB78CD8] DEFAULT ((0)),
[WeightedRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportuni__Weigh__70ABB111] DEFAULT ((0)),
[CloseDate] [datetime] NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OpenDate] [datetime] NULL,
[Source] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRProposalWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjMgr] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Principal] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Supervisor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PublicNoticeDate] [datetime] NULL,
[SolicitationNum] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[County] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LabBillTable] [int] NOT NULL CONSTRAINT [DF__Opportuni__LabBi__719FD54A] DEFAULT ((0)),
[LabCostTable] [int] NOT NULL CONSTRAINT [DF__Opportuni__LabCo__7293F983] DEFAULT ((0)),
[OppCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllocMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Timescale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KonaSpace] [int] NOT NULL CONSTRAINT [DF__Opportuni__KonaS__73881DBC] DEFAULT ((0)),
[ClosedReason] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClosedNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AwardType] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Duration] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContractTypeGovCon] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompetitionType] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MasterContract] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Solicitation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAICS] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurRole] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IQID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServProCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FESurchargePct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportuni__FESur__747C41F5] DEFAULT ((0)),
[FESurcharge] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportuni__FESur__7570662E] DEFAULT ((0)),
[FEAddlExpensesPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportuni__FEAdd__76648A67] DEFAULT ((0)),
[FEAddlExpenses] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportuni__FEAdd__7758AEA0] DEFAULT ((0)),
[FEOtherPct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportuni__FEOth__784CD2D9] DEFAULT ((0)),
[FEOther] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__Opportuni__FEOth__7940F712] DEFAULT ((0)),
[IQLastUpdate] [datetime] NULL,
[MarketingCoordinator] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProposalManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BusinessDeveloperLead] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[SFID] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFLastModifiedDate] [datetime] NULL,
[TotalContractValue] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__Total__07118A2A] DEFAULT ((0)),
[PeriodOfPerformance] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__Opportuni__Perio__0805AE63] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Opportunity_Backup] ADD CONSTRAINT [OpportunityPK] PRIMARY KEY NONCLUSTERED ([OpportunityID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [OpportunitySFIDIDX] ON [dbo].[Opportunity_Backup] ([SFID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
