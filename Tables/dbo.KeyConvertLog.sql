CREATE TABLE [dbo].[KeyConvertLog]
(
[PKey] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KeyConvert__PKey__0F103CA0] DEFAULT (newid()),
[Entity] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldKey] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldKey2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldKey3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewKey] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewKey2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewKey3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBSName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldName] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldName2] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldName3] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewName] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewName2] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewName3] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [int] NOT NULL CONSTRAINT [DF__KeyConver__Perio__100460D9] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__KeyConver__PostS__10F88512] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[KeyConvertLog] ADD CONSTRAINT [KeyConvertLogPK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
