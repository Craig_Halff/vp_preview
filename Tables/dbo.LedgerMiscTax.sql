CREATE TABLE [dbo].[LedgerMiscTax]
(
[Period] [int] NOT NULL CONSTRAINT [DF__LedgerMis__Perio__619E77AB] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__LedgerMis__PostS__62929BE4] DEFAULT ((0)),
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReverseCharge] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__LedgerMis__Rever__6386C01D] DEFAULT ('N'),
[Seq] [smallint] NOT NULL CONSTRAINT [DF__LedgerMiscT__Seq__647AE456] DEFAULT ((0)),
[TaxAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__TaxAm__656F088F] DEFAULT ((0)),
[TaxCBAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__TaxCB__66632CC8] DEFAULT ((0)),
[CompoundOnTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__Amoun__67575101] DEFAULT ((0)),
[CBAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__CBAmo__684B753A] DEFAULT ((0)),
[TaxAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__TaxAm__693F9973] DEFAULT ((0)),
[TaxCBAmountTaxCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__TaxCB__6A33BDAC] DEFAULT ((0)),
[TaxAmountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__TaxAm__6B27E1E5] DEFAULT ((0)),
[TaxCBAmountFunctionalCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__TaxCB__6C1C061E] DEFAULT ((0)),
[TaxAmountSourceCurrency] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__TaxAm__6D102A57] DEFAULT ((0)),
[NonRecoverTaxPercent] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__LedgerMis__NonRe__6E044E90] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LedgerMiscTax] ADD CONSTRAINT [LedgerMiscTaxPK] PRIMARY KEY CLUSTERED ([Period], [PostSeq], [PKey], [TaxCode], [ReverseCharge]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
