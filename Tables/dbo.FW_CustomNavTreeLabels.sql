CREATE TABLE [dbo].[FW_CustomNavTreeLabels]
(
[ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CustomNavTreeLabels] ADD CONSTRAINT [FW_CustomNavTreeLabelsPK] PRIMARY KEY CLUSTERED ([ID], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
