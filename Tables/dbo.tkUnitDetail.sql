CREATE TABLE [dbo].[tkUnitDetail]
(
[EndDate] [datetime] NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__tkUnitDetai__Seq__49C7B971] DEFAULT ((0)),
[TransDate] [datetime] NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitTable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Unit] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Qty] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__tkUnitDetai__Qty__4ABBDDAA] DEFAULT ((0)),
[TransComment] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[tkUnitDetail_TrackPKeyChange]
ON [dbo].[tkUnitDetail]
AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @RowCnt	INT;
DECLARE @MaxRows INT;
DECLARE @delta TABLE (
	rownum INT IDENTITY (1, 1) PRIMARY KEY NOT NULL 
	,transComment VARCHAR(255)
	,PKey VARCHAR(32)
);

DECLARE @TransComment VARCHAR(255)
DECLARE @PKeyNew VARCHAR(32)
DECLARE @PKeyOld VARCHAR(32)

DECLARE @tag VARCHAR(50)
SET @tag = '{ref:'

DECLARE @len INT
DECLARE @begin INT
DECLARE @end INT


BEGIN TRY

	SELECT @RowCnt = 1;

	INSERT INTO @delta 
	(
		transComment
		,PKey
	) 
	SELECT transComment
	, PKey 
	FROM [inserted]

	-- GET THE NUMBER OF ROWS IN THE DELTA TABLE
	SELECT @MaxRows = COUNT(*) FROM @delta;

	WHILE @RowCnt <= @MaxRows
		BEGIN

			SELECT @TransComment = transComment
			, @PKeyNew = PKey 
			FROM @delta 
			WHERE rownum = @RowCnt;

			SET @len = LEN(@TransComment)
			SET @begin = PATINDEX('%' + @tag + '%', @TransComment); 
			SET @end = CHARINDEX('/', @TransComment, @begin);
			--SET @end = PATINDEX('%/%}%', @TransComment);	

			IF @begin > 0 AND @end > 0
			BEGIN
				SET @begin = @begin + LEN(@tag)

				IF @begin <= @len AND @end <= @len  
				BEGIN
					SET @PKeyOld = SUBSTRING(@TransComment, @begin , @end - @begin)

					IF (@PKeyOld <> @PKeyNew)
						UPDATE tkUnitDetail SET PKey = @PKeyOld WHERE PKey = @PKeyNew
				END
			END

			-- NEXT ROW
			SELECT @RowCnt = @RowCnt + 1;
		END

END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
END CATCH

END

GO
ALTER TABLE [dbo].[tkUnitDetail] ADD CONSTRAINT [tkUnitDetailPK] PRIMARY KEY NONCLUSTERED ([EndDate], [Employee], [EmployeeCompany], [Seq], [TransDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
