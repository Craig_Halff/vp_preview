CREATE TABLE [dbo].[ContactActivity]
(
[ActivityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrimaryInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ContactAc__Prima__53C48E49] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContactActivity] ADD CONSTRAINT [ContactActivityPK] PRIMARY KEY NONCLUSTERED ([ActivityID], [ContactID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
