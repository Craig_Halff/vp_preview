CREATE TABLE [dbo].[ApprovalWorkflow]
(
[Workflow_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ItemType_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Step] [int] NOT NULL CONSTRAINT [DF__ApprovalWo__Step__3B6D1674] DEFAULT ((0)),
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssignTo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssignToDisplay] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Completion] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reject] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ApprovalW__Activ__3C613AAD] DEFAULT ('Y'),
[CompletionStatusCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Duration] [int] NOT NULL CONSTRAINT [DF__ApprovalW__Durat__3D555EE6] DEFAULT ((0)),
[ReviewAction] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Condition] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ApprovalW__Condi__3E49831F] DEFAULT ('N'),
[NotMet] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeAssignToDisplay] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeAssignto] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssignmentSelection] [smallint] NOT NULL CONSTRAINT [DF__ApprovalW__Assig__3F3DA758] DEFAULT ((0)),
[ApprovalLevel] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ApprovalW__Appro__4031CB91] DEFAULT ('Master')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ApprovalWorkflow] ADD CONSTRAINT [ApprovalWorkflowPK] PRIMARY KEY CLUSTERED ([Workflow_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
