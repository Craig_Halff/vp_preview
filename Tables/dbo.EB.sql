CREATE TABLE [dbo].[EB]
(
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Pct] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EB_New__Pct__3E2A52E5] DEFAULT ((0)),
[AmtBud] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EB_New__AmtBud__3F1E771E] DEFAULT ((0)),
[BillBud] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EB_New__BillBud__40129B57] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[EtcAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EB_New__EtcAmt__4106BF90] DEFAULT ((0)),
[EacAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EB_New__EacAmt__41FAE3C9] DEFAULT ((0)),
[BillEtcAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EB_New__BillEtcA__42EF0802] DEFAULT ((0)),
[BillEacAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EB_New__BillEacA__43E32C3B] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EB] ADD CONSTRAINT [EBPK] PRIMARY KEY NONCLUSTERED ([WBS1], [WBS2], [WBS3], [Account], [Vendor]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
