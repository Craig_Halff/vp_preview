CREATE TABLE [dbo].[cdDetailTax]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CheckNo] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaxAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__cdDetailT__TaxAm__54E2CFB4] DEFAULT ((0)),
[CompoundOnTaxCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__cdDetailTax__Seq__55D6F3ED] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cdDetailTax] ADD CONSTRAINT [cdDetailTaxPK] PRIMARY KEY NONCLUSTERED ([Batch], [CheckNo], [PKey], [TaxCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
