CREATE TABLE [dbo].[SF330ProposedFirms]
(
[SF330ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FirmSeq] [smallint] NOT NULL CONSTRAINT [DF__SF330Prop__FirmS__72FED92E] DEFAULT ((0)),
[Type] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BranchOffice] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Role] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OfficeLocation] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF330ProposedFirms] ADD CONSTRAINT [SF330ProposedFirmsPK] PRIMARY KEY NONCLUSTERED ([SF330ID], [ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
