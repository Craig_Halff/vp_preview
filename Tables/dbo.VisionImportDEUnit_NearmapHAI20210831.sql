CREATE TABLE [dbo].[VisionImportDEUnit_NearmapHAI20210831]
(
[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransDate] [datetime] NULL,
[WBS1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Unit] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitTable] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Quantity] [decimal] (19, 4) NULL,
[DetailDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employee] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeCompany] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
