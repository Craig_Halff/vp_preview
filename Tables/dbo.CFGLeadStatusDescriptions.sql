CREATE TABLE [dbo].[CFGLeadStatusDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGLeadStat__Seq__4B4456E1] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGLeadStatusDescriptions] ADD CONSTRAINT [CFGLeadStatusDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
