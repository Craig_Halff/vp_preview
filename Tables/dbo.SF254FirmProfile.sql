CREATE TABLE [dbo].[SF254FirmProfile]
(
[SF254ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__SF254FirmPr__Seq__045E6F5A] DEFAULT ((0)),
[ProfileCode] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IncludeCommentInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF254Firm__Inclu__05529393] DEFAULT ('N'),
[NumberOfProjects] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TotalGrossFees] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF254FirmProfile] ADD CONSTRAINT [SF254FirmProfilePK] PRIMARY KEY NONCLUSTERED ([SF254ID], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
