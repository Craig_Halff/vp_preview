CREATE TABLE [dbo].[ContactToContactAssoc]
(
[FromContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ToContactID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Relationship] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_ContactToContactAssoc]
      ON [dbo].[ContactToContactAssoc]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ContactToContactAssoc'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromContactID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToContactID],121),'FromContactID',CONVERT(NVARCHAR(2000),DELETED.[FromContactID],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join Contacts as oldDesc  on DELETED.FromContactID = oldDesc.ContactID

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromContactID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToContactID],121),'ToContactID',CONVERT(NVARCHAR(2000),DELETED.[ToContactID],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join Contacts as oldDesc  on DELETED.ToContactID = oldDesc.ContactID

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromContactID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToContactID],121),'Relationship',CONVERT(NVARCHAR(2000),DELETED.[Relationship],121),NULL, oldDesc.Description, NULL, @source,@app
        FROM DELETED left join CFGContactRelationship as oldDesc  on DELETED.Relationship = oldDesc.Code

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[FromContactID],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[ToContactID],121),'Description','[text]',NULL, @source,@app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_ContactToContactAssoc] ON [dbo].[ContactToContactAssoc]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_ContactToContactAssoc]
      ON [dbo].[ContactToContactAssoc]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ContactToContactAssoc'
    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToContactID],121),'FromContactID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[FromContactID],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  Contacts as newDesc  on INSERTED.FromContactID = newDesc.ContactID

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToContactID],121),'ToContactID',NULL,CONVERT(NVARCHAR(2000),INSERTED.[ToContactID],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  Contacts as newDesc  on INSERTED.ToContactID = newDesc.ContactID

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToContactID],121),'Relationship',NULL,CONVERT(NVARCHAR(2000),INSERTED.[Relationship],121), NULL, newDesc.Description, @source, @app
       FROM INSERTED left join  CFGContactRelationship as newDesc  on INSERTED.Relationship = newDesc.Code

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[FromContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToContactID],121),'Description',NULL,'[text]', @source, @app
      FROM INSERTED


    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_ContactToContactAssoc] ON [dbo].[ContactToContactAssoc]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_ContactToContactAssoc]
      ON [dbo].[ContactToContactAssoc]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'ContactToContactAssoc'
    
     If UPDATE([FromContactID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToContactID],121),'FromContactID',
     CONVERT(NVARCHAR(2000),DELETED.[FromContactID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[FromContactID],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[FromContactID] = DELETED.[FromContactID] AND INSERTED.[ToContactID] = DELETED.[ToContactID] AND 
		(
			(
				INSERTED.[FromContactID] Is Null And
				DELETED.[FromContactID] Is Not Null
			) Or
			(
				INSERTED.[FromContactID] Is Not Null And
				DELETED.[FromContactID] Is Null
			) Or
			(
				INSERTED.[FromContactID] !=
				DELETED.[FromContactID]
			)
		) left join Contacts as oldDesc  on DELETED.FromContactID = oldDesc.ContactID  left join  Contacts as newDesc  on INSERTED.FromContactID = newDesc.ContactID
		END		
		
     If UPDATE([ToContactID])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToContactID],121),'ToContactID',
     CONVERT(NVARCHAR(2000),DELETED.[ToContactID],121),
     CONVERT(NVARCHAR(2000),INSERTED.[ToContactID],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[FromContactID] = DELETED.[FromContactID] AND INSERTED.[ToContactID] = DELETED.[ToContactID] AND 
		(
			(
				INSERTED.[ToContactID] Is Null And
				DELETED.[ToContactID] Is Not Null
			) Or
			(
				INSERTED.[ToContactID] Is Not Null And
				DELETED.[ToContactID] Is Null
			) Or
			(
				INSERTED.[ToContactID] !=
				DELETED.[ToContactID]
			)
		) left join Contacts as oldDesc  on DELETED.ToContactID = oldDesc.ContactID  left join  Contacts as newDesc  on INSERTED.ToContactID = newDesc.ContactID
		END		
		
     If UPDATE([Relationship])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToContactID],121),'Relationship',
     CONVERT(NVARCHAR(2000),DELETED.[Relationship],121),
     CONVERT(NVARCHAR(2000),INSERTED.[Relationship],121),
     oldDesc.Description, newDesc.Description, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[FromContactID] = DELETED.[FromContactID] AND INSERTED.[ToContactID] = DELETED.[ToContactID] AND 
		(
			(
				INSERTED.[Relationship] Is Null And
				DELETED.[Relationship] Is Not Null
			) Or
			(
				INSERTED.[Relationship] Is Not Null And
				DELETED.[Relationship] Is Null
			) Or
			(
				INSERTED.[Relationship] !=
				DELETED.[Relationship]
			)
		) left join CFGContactRelationship as oldDesc  on DELETED.Relationship = oldDesc.Code  left join  CFGContactRelationship as newDesc  on INSERTED.Relationship = newDesc.Code
		END		
		
      If UPDATE([Description])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[FromContactID],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[ToContactID],121),'Description',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_ContactToContactAssoc] ON [dbo].[ContactToContactAssoc]
GO
ALTER TABLE [dbo].[ContactToContactAssoc] ADD CONSTRAINT [ContactToContactAssocPK] PRIMARY KEY NONCLUSTERED ([FromContactID], [ToContactID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
