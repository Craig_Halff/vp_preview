CREATE TABLE [dbo].[ApprovalItem]
(
[Item_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ItemType_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ApplicationID] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApplicationKey] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsSuspended] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ApprovalI__IsSus__2F073F8F] DEFAULT ('N'),
[WorkflowStep] [int] NOT NULL CONSTRAINT [DF__ApprovalI__Workf__2FFB63C8] DEFAULT ((0)),
[StepStartDate] [datetime] NULL,
[StatusCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemParent_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssignUSR_ID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssignUSRLabel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsClosed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ApprovalI__IsClo__30EF8801] DEFAULT ('N'),
[ClosedDate] [datetime] NULL,
[DueDate] [datetime] NULL,
[Comments] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReminderDate] [datetime] NULL,
[ParentWorkflowStep] [int] NOT NULL CONSTRAINT [DF__ApprovalI__Paren__31E3AC3A] DEFAULT ((0)),
[EmailNotify] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ApprovalI__Email__32D7D073] DEFAULT ('N'),
[DashboardNotify] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ApprovalI__Dashb__33CBF4AC] DEFAULT ('N'),
[IsDelegate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ApprovalI__IsDel__34C018E5] DEFAULT ('N'),
[StatusChangeDate] [datetime] NULL,
[NotificationAlertType] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsUpdated] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ApprovalI__IsUpd__35B43D1E] DEFAULT ('N'),
[WorkflowFlag] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ItemLink_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssignUSRLabelSystem] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovalLevel] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ApprovalI__Appro__36A86157] DEFAULT ('Master'),
[StashStatusCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ApprovalItem] ADD CONSTRAINT [ApprovalItemPK] PRIMARY KEY CLUSTERED ([Item_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ApprovalItemApplicationIDApprovalLevelItemParentIDX] ON [dbo].[ApprovalItem] ([ApplicationID], [ApprovalLevel], [ItemParent_UID], [ApplicationKey]) INCLUDE ([IsUpdated], [StatusCode], [WorkflowStep]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ApprovalItemApplicationIDApprovalLevelItemTypeIDX] ON [dbo].[ApprovalItem] ([ApplicationID], [ApprovalLevel], [ItemType_UID]) INCLUDE ([ApplicationKey], [CreateDate], [Item_UID], [ItemParent_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ApprovalItemApplicationKeyIDX] ON [dbo].[ApprovalItem] ([ApplicationKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ApprovalItemAssignUSR_IDIDX] ON [dbo].[ApprovalItem] ([AssignUSR_ID], [ItemType_UID], [ApplicationID], [IsClosed], [ApprovalLevel]) INCLUDE ([ApplicationKey], [WorkflowFlag]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ApprovalItemItemParent_UIDIDX] ON [dbo].[ApprovalItem] ([ItemParent_UID], [AssignUSR_ID], [IsClosed], [ParentWorkflowStep], [WorkflowStep], [ApprovalLevel], [ApplicationID]) INCLUDE ([ApplicationKey], [CreateDate], [DueDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
