CREATE TABLE [dbo].[FW_DisplaySettingsDesc]
(
[Settings_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_DisplaySettingsDesc] ADD CONSTRAINT [FW_DisplaySettingsDescPK] PRIMARY KEY CLUSTERED ([Settings_UID], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
