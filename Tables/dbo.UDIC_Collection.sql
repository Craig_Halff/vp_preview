CREATE TABLE [dbo].[UDIC_Collection]
(
[UDIC_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CustInvoiceNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCollectionStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustRequestedActionStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustRequestedAction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustRequestedActionStart] [datetime] NULL,
[CustRequestedActionOwner] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustCollectionContact] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustResponseDate] [datetime] NULL,
[CustResponse] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustResponseNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustContactedbyEmployee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustProject] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBillingClient] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBillingPrimaryContact] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBillingContact] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustInvoiceDate] [datetime] NULL,
[CustDaysOutstanding] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_Coll__CustD__7B5F1505] DEFAULT ((0)),
[CustDaysSinceLastCharge] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_Coll__CustD__7C53393E] DEFAULT ((0)),
[CustInvoiceTotal] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_Coll__CustI__7D475D77] DEFAULT ((0)),
[CustReceived] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_Coll__CustR__7E3B81B0] DEFAULT ((0)),
[CustBalance] [decimal] (19, 5) NOT NULL CONSTRAINT [DF__UDIC_Coll__CustB__7F2FA5E9] DEFAULT ((0)),
[CustManagementLeader] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustOperationsManager] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustDirector] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustPrincipal] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustProjMgr] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustBiller] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustProjectNo] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_UDIC_Collection]
      ON [dbo].[UDIC_Collection]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'UDIC_Collection'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'UDIC_UID',CONVERT(NVARCHAR(2000),[UDIC_UID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustomCurrencyCode',CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustInvoiceNumber',CONVERT(NVARCHAR(2000),[CustInvoiceNumber],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCollectionStatus',CONVERT(NVARCHAR(2000),[CustCollectionStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustRequestedActionStatus',CONVERT(NVARCHAR(2000),[CustRequestedActionStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustRequestedAction',CONVERT(NVARCHAR(2000),[CustRequestedAction],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustRequestedActionStart',CONVERT(NVARCHAR(2000),[CustRequestedActionStart],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustRequestedActionOwner',CONVERT(NVARCHAR(2000),DELETED.[CustRequestedActionOwner],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustRequestedActionOwner = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustCollectionContact',CONVERT(NVARCHAR(2000),DELETED.[CustCollectionContact],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join Contacts as oldDesc   on DELETED.CustCollectionContact = oldDesc.ContactID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustResponseDate',CONVERT(NVARCHAR(2000),[CustResponseDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustResponse',CONVERT(NVARCHAR(2000),[CustResponse],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustResponseNotes','[text]',NULL, @source,@app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustContactedbyEmployee',CONVERT(NVARCHAR(2000),DELETED.[CustContactedbyEmployee],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustContactedbyEmployee = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustProject',CONVERT(NVARCHAR(2000),DELETED.[CustProject],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join PR as oldDesc   on DELETED.CustProject = oldDesc.WBS1

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustBillingClient',CONVERT(NVARCHAR(2000),DELETED.[CustBillingClient],121),NULL, oldDesc.Name, NULL, @source,@app
        FROM DELETED left join CL as oldDesc   on DELETED.CustBillingClient = oldDesc.ClientID

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustBillingPrimaryContact',CONVERT(NVARCHAR(2000),DELETED.[CustBillingPrimaryContact],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join Contacts as oldDesc   on DELETED.CustBillingPrimaryContact = oldDesc.ContactID

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustBillingContact',CONVERT(NVARCHAR(2000),DELETED.[CustBillingContact],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join Contacts as oldDesc   on DELETED.CustBillingContact = oldDesc.ContactID

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustInvoiceDate',CONVERT(NVARCHAR(2000),[CustInvoiceDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustDaysOutstanding',CONVERT(NVARCHAR(2000),[CustDaysOutstanding],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustDaysSinceLastCharge',CONVERT(NVARCHAR(2000),[CustDaysSinceLastCharge],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustInvoiceTotal',CONVERT(NVARCHAR(2000),[CustInvoiceTotal],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustReceived',CONVERT(NVARCHAR(2000),[CustReceived],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustBalance',CONVERT(NVARCHAR(2000),[CustBalance],121),NULL, @source, @app
      FROM DELETED
    
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustManagementLeader',CONVERT(NVARCHAR(2000),DELETED.[CustManagementLeader],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustManagementLeader = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustOperationsManager',CONVERT(NVARCHAR(2000),DELETED.[CustOperationsManager],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustOperationsManager = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustDirector',CONVERT(NVARCHAR(2000),DELETED.[CustDirector],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustDirector = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustPrincipal',CONVERT(NVARCHAR(2000),DELETED.[CustPrincipal],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustPrincipal = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustProjMgr',CONVERT(NVARCHAR(2000),DELETED.[CustProjMgr],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustProjMgr = oldDesc.Employee

      
        INSERT INTO AuditTrail	(
        ModUser,
        ModDate,
        TableName,
        ActionType,
        PrimaryKey,
        ColumnName,
        OldValue,
        NewValue,
        OldValueDescription,
        NewValueDescription,
        Source,
        Application
        )
        SELECT
        @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustBiller',CONVERT(NVARCHAR(2000),DELETED.[CustBiller],121),NULL, IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), NULL, @source,@app
        FROM DELETED left join EMMain as oldDesc   WITH (NOLOCK)  on DELETED.CustBiller = oldDesc.Employee

      
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UDIC_UID],121),'CustProjectNo',CONVERT(NVARCHAR(2000),[CustProjectNo],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_UDIC_Collection]
      ON [dbo].[UDIC_Collection]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'UDIC_Collection'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'UDIC_UID',NULL,CONVERT(NVARCHAR(2000),[UDIC_UID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustomCurrencyCode',NULL,CONVERT(NVARCHAR(2000),[CustomCurrencyCode],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustInvoiceNumber',NULL,CONVERT(NVARCHAR(2000),[CustInvoiceNumber],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCollectionStatus',NULL,CONVERT(NVARCHAR(2000),[CustCollectionStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustRequestedActionStatus',NULL,CONVERT(NVARCHAR(2000),[CustRequestedActionStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustRequestedAction',NULL,CONVERT(NVARCHAR(2000),[CustRequestedAction],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustRequestedActionStart',NULL,CONVERT(NVARCHAR(2000),[CustRequestedActionStart],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustRequestedActionOwner',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustRequestedActionOwner],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustRequestedActionOwner = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCollectionContact',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustCollectionContact],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  Contacts as newDesc  on INSERTED.CustCollectionContact = newDesc.ContactID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustResponseDate',NULL,CONVERT(NVARCHAR(2000),[CustResponseDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustResponse',NULL,CONVERT(NVARCHAR(2000),[CustResponse],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustResponseNotes',NULL,'[text]', @source, @app
      FROM INSERTED


    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustContactedbyEmployee',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustContactedbyEmployee],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustContactedbyEmployee = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustProject',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustProject],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  PR as newDesc  on INSERTED.CustProject = newDesc.WBS1

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustBillingClient',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustBillingClient],121), NULL, newDesc.Name, @source, @app
       FROM INSERTED left join  CL as newDesc  on INSERTED.CustBillingClient = newDesc.ClientID

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustBillingPrimaryContact',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustBillingPrimaryContact],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  Contacts as newDesc  on INSERTED.CustBillingPrimaryContact = newDesc.ContactID

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustBillingContact',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustBillingContact],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  Contacts as newDesc  on INSERTED.CustBillingContact = newDesc.ContactID

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustInvoiceDate',NULL,CONVERT(NVARCHAR(2000),[CustInvoiceDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustDaysOutstanding',NULL,CONVERT(NVARCHAR(2000),[CustDaysOutstanding],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustDaysSinceLastCharge',NULL,CONVERT(NVARCHAR(2000),[CustDaysSinceLastCharge],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustInvoiceTotal',NULL,CONVERT(NVARCHAR(2000),[CustInvoiceTotal],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustReceived',NULL,CONVERT(NVARCHAR(2000),[CustReceived],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustBalance',NULL,CONVERT(NVARCHAR(2000),[CustBalance],121), @source, @app
      FROM INSERTED

    
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustManagementLeader',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustManagementLeader],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustManagementLeader = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustOperationsManager',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustOperationsManager],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustOperationsManager = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustDirector',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustDirector],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustDirector = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPrincipal',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustPrincipal],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustPrincipal = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustProjMgr',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustProjMgr],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustProjMgr = newDesc.Employee

     
       INSERT INTO AuditTrail	(
       ModUser,
       ModDate,
       TableName,
       ActionType,
       PrimaryKey,
       ColumnName,
       OldValue,
       NewValue,
       OldValueDescription,
       NewValueDescription,
       Source,
       Application
       )
       SELECT
       @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustBiller',NULL,CONVERT(NVARCHAR(2000),INSERTED.[CustBiller],121), NULL, IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
       FROM INSERTED left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustBiller = newDesc.Employee

     
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustProjectNo',NULL,CONVERT(NVARCHAR(2000),[CustProjectNo],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_UDIC_Collection]
      ON [dbo].[UDIC_Collection]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return


      set @table = 'UDIC_Collection'
    
      If UPDATE([UDIC_UID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'UDIC_UID',
      CONVERT(NVARCHAR(2000),DELETED.[UDIC_UID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UDIC_UID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[UDIC_UID] Is Null And
				DELETED.[UDIC_UID] Is Not Null
			) Or
			(
				INSERTED.[UDIC_UID] Is Not Null And
				DELETED.[UDIC_UID] Is Null
			) Or
			(
				INSERTED.[UDIC_UID] !=
				DELETED.[UDIC_UID]
			)
		) 
		END		
		
      If UPDATE([CustomCurrencyCode])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustomCurrencyCode',
      CONVERT(NVARCHAR(2000),DELETED.[CustomCurrencyCode],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustomCurrencyCode],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustomCurrencyCode] Is Null And
				DELETED.[CustomCurrencyCode] Is Not Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] Is Not Null And
				DELETED.[CustomCurrencyCode] Is Null
			) Or
			(
				INSERTED.[CustomCurrencyCode] !=
				DELETED.[CustomCurrencyCode]
			)
		) 
		END		
		
      If UPDATE([CustInvoiceNumber])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustInvoiceNumber',
      CONVERT(NVARCHAR(2000),DELETED.[CustInvoiceNumber],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustInvoiceNumber],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustInvoiceNumber] Is Null And
				DELETED.[CustInvoiceNumber] Is Not Null
			) Or
			(
				INSERTED.[CustInvoiceNumber] Is Not Null And
				DELETED.[CustInvoiceNumber] Is Null
			) Or
			(
				INSERTED.[CustInvoiceNumber] !=
				DELETED.[CustInvoiceNumber]
			)
		) 
		END		
		
      If UPDATE([CustCollectionStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCollectionStatus',
      CONVERT(NVARCHAR(2000),DELETED.[CustCollectionStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustCollectionStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCollectionStatus] Is Null And
				DELETED.[CustCollectionStatus] Is Not Null
			) Or
			(
				INSERTED.[CustCollectionStatus] Is Not Null And
				DELETED.[CustCollectionStatus] Is Null
			) Or
			(
				INSERTED.[CustCollectionStatus] !=
				DELETED.[CustCollectionStatus]
			)
		) 
		END		
		
      If UPDATE([CustRequestedActionStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustRequestedActionStatus',
      CONVERT(NVARCHAR(2000),DELETED.[CustRequestedActionStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRequestedActionStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustRequestedActionStatus] Is Null And
				DELETED.[CustRequestedActionStatus] Is Not Null
			) Or
			(
				INSERTED.[CustRequestedActionStatus] Is Not Null And
				DELETED.[CustRequestedActionStatus] Is Null
			) Or
			(
				INSERTED.[CustRequestedActionStatus] !=
				DELETED.[CustRequestedActionStatus]
			)
		) 
		END		
		
      If UPDATE([CustRequestedAction])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustRequestedAction',
      CONVERT(NVARCHAR(2000),DELETED.[CustRequestedAction],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRequestedAction],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustRequestedAction] Is Null And
				DELETED.[CustRequestedAction] Is Not Null
			) Or
			(
				INSERTED.[CustRequestedAction] Is Not Null And
				DELETED.[CustRequestedAction] Is Null
			) Or
			(
				INSERTED.[CustRequestedAction] !=
				DELETED.[CustRequestedAction]
			)
		) 
		END		
		
      If UPDATE([CustRequestedActionStart])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustRequestedActionStart',
      CONVERT(NVARCHAR(2000),DELETED.[CustRequestedActionStart],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustRequestedActionStart],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustRequestedActionStart] Is Null And
				DELETED.[CustRequestedActionStart] Is Not Null
			) Or
			(
				INSERTED.[CustRequestedActionStart] Is Not Null And
				DELETED.[CustRequestedActionStart] Is Null
			) Or
			(
				INSERTED.[CustRequestedActionStart] !=
				DELETED.[CustRequestedActionStart]
			)
		) 
		END		
		
     If UPDATE([CustRequestedActionOwner])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustRequestedActionOwner',
     CONVERT(NVARCHAR(2000),DELETED.[CustRequestedActionOwner],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustRequestedActionOwner],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustRequestedActionOwner] Is Null And
				DELETED.[CustRequestedActionOwner] Is Not Null
			) Or
			(
				INSERTED.[CustRequestedActionOwner] Is Not Null And
				DELETED.[CustRequestedActionOwner] Is Null
			) Or
			(
				INSERTED.[CustRequestedActionOwner] !=
				DELETED.[CustRequestedActionOwner]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustRequestedActionOwner = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustRequestedActionOwner = newDesc.Employee
		END		
		
     If UPDATE([CustCollectionContact])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustCollectionContact',
     CONVERT(NVARCHAR(2000),DELETED.[CustCollectionContact],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustCollectionContact],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustCollectionContact] Is Null And
				DELETED.[CustCollectionContact] Is Not Null
			) Or
			(
				INSERTED.[CustCollectionContact] Is Not Null And
				DELETED.[CustCollectionContact] Is Null
			) Or
			(
				INSERTED.[CustCollectionContact] !=
				DELETED.[CustCollectionContact]
			)
		) left join Contacts as oldDesc  on DELETED.CustCollectionContact = oldDesc.ContactID  left join  Contacts as newDesc  on INSERTED.CustCollectionContact = newDesc.ContactID
		END		
		
      If UPDATE([CustResponseDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustResponseDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustResponseDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustResponseDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustResponseDate] Is Null And
				DELETED.[CustResponseDate] Is Not Null
			) Or
			(
				INSERTED.[CustResponseDate] Is Not Null And
				DELETED.[CustResponseDate] Is Null
			) Or
			(
				INSERTED.[CustResponseDate] !=
				DELETED.[CustResponseDate]
			)
		) 
		END		
		
      If UPDATE([CustResponse])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustResponse',
      CONVERT(NVARCHAR(2000),DELETED.[CustResponse],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustResponse],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustResponse] Is Null And
				DELETED.[CustResponse] Is Not Null
			) Or
			(
				INSERTED.[CustResponse] Is Not Null And
				DELETED.[CustResponse] Is Null
			) Or
			(
				INSERTED.[CustResponse] !=
				DELETED.[CustResponse]
			)
		) 
		END		
		
      If UPDATE([CustResponseNotes])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustResponseNotes',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
     If UPDATE([CustContactedbyEmployee])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustContactedbyEmployee',
     CONVERT(NVARCHAR(2000),DELETED.[CustContactedbyEmployee],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustContactedbyEmployee],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustContactedbyEmployee] Is Null And
				DELETED.[CustContactedbyEmployee] Is Not Null
			) Or
			(
				INSERTED.[CustContactedbyEmployee] Is Not Null And
				DELETED.[CustContactedbyEmployee] Is Null
			) Or
			(
				INSERTED.[CustContactedbyEmployee] !=
				DELETED.[CustContactedbyEmployee]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustContactedbyEmployee = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustContactedbyEmployee = newDesc.Employee
		END		
		
     If UPDATE([CustProject])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustProject',
     CONVERT(NVARCHAR(2000),DELETED.[CustProject],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustProject],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustProject] Is Null And
				DELETED.[CustProject] Is Not Null
			) Or
			(
				INSERTED.[CustProject] Is Not Null And
				DELETED.[CustProject] Is Null
			) Or
			(
				INSERTED.[CustProject] !=
				DELETED.[CustProject]
			)
		) left join PR as oldDesc  on DELETED.CustProject = oldDesc.WBS1  left join  PR as newDesc  on INSERTED.CustProject = newDesc.WBS1
		END		
		
     If UPDATE([CustBillingClient])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustBillingClient',
     CONVERT(NVARCHAR(2000),DELETED.[CustBillingClient],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustBillingClient],121),
     oldDesc.Name, newDesc.Name, @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustBillingClient] Is Null And
				DELETED.[CustBillingClient] Is Not Null
			) Or
			(
				INSERTED.[CustBillingClient] Is Not Null And
				DELETED.[CustBillingClient] Is Null
			) Or
			(
				INSERTED.[CustBillingClient] !=
				DELETED.[CustBillingClient]
			)
		) left join CL as oldDesc  on DELETED.CustBillingClient = oldDesc.ClientID  left join  CL as newDesc  on INSERTED.CustBillingClient = newDesc.ClientID
		END		
		
     If UPDATE([CustBillingPrimaryContact])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustBillingPrimaryContact',
     CONVERT(NVARCHAR(2000),DELETED.[CustBillingPrimaryContact],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustBillingPrimaryContact],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustBillingPrimaryContact] Is Null And
				DELETED.[CustBillingPrimaryContact] Is Not Null
			) Or
			(
				INSERTED.[CustBillingPrimaryContact] Is Not Null And
				DELETED.[CustBillingPrimaryContact] Is Null
			) Or
			(
				INSERTED.[CustBillingPrimaryContact] !=
				DELETED.[CustBillingPrimaryContact]
			)
		) left join Contacts as oldDesc  on DELETED.CustBillingPrimaryContact = oldDesc.ContactID  left join  Contacts as newDesc  on INSERTED.CustBillingPrimaryContact = newDesc.ContactID
		END		
		
     If UPDATE([CustBillingContact])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustBillingContact',
     CONVERT(NVARCHAR(2000),DELETED.[CustBillingContact],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustBillingContact],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustBillingContact] Is Null And
				DELETED.[CustBillingContact] Is Not Null
			) Or
			(
				INSERTED.[CustBillingContact] Is Not Null And
				DELETED.[CustBillingContact] Is Null
			) Or
			(
				INSERTED.[CustBillingContact] !=
				DELETED.[CustBillingContact]
			)
		) left join Contacts as oldDesc  on DELETED.CustBillingContact = oldDesc.ContactID  left join  Contacts as newDesc  on INSERTED.CustBillingContact = newDesc.ContactID
		END		
		
      If UPDATE([CustInvoiceDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustInvoiceDate',
      CONVERT(NVARCHAR(2000),DELETED.[CustInvoiceDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustInvoiceDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustInvoiceDate] Is Null And
				DELETED.[CustInvoiceDate] Is Not Null
			) Or
			(
				INSERTED.[CustInvoiceDate] Is Not Null And
				DELETED.[CustInvoiceDate] Is Null
			) Or
			(
				INSERTED.[CustInvoiceDate] !=
				DELETED.[CustInvoiceDate]
			)
		) 
		END		
		
      If UPDATE([CustDaysOutstanding])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustDaysOutstanding',
      CONVERT(NVARCHAR(2000),DELETED.[CustDaysOutstanding],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustDaysOutstanding],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustDaysOutstanding] Is Null And
				DELETED.[CustDaysOutstanding] Is Not Null
			) Or
			(
				INSERTED.[CustDaysOutstanding] Is Not Null And
				DELETED.[CustDaysOutstanding] Is Null
			) Or
			(
				INSERTED.[CustDaysOutstanding] !=
				DELETED.[CustDaysOutstanding]
			)
		) 
		END		
		
      If UPDATE([CustDaysSinceLastCharge])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustDaysSinceLastCharge',
      CONVERT(NVARCHAR(2000),DELETED.[CustDaysSinceLastCharge],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustDaysSinceLastCharge],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustDaysSinceLastCharge] Is Null And
				DELETED.[CustDaysSinceLastCharge] Is Not Null
			) Or
			(
				INSERTED.[CustDaysSinceLastCharge] Is Not Null And
				DELETED.[CustDaysSinceLastCharge] Is Null
			) Or
			(
				INSERTED.[CustDaysSinceLastCharge] !=
				DELETED.[CustDaysSinceLastCharge]
			)
		) 
		END		
		
      If UPDATE([CustInvoiceTotal])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustInvoiceTotal',
      CONVERT(NVARCHAR(2000),DELETED.[CustInvoiceTotal],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustInvoiceTotal],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustInvoiceTotal] Is Null And
				DELETED.[CustInvoiceTotal] Is Not Null
			) Or
			(
				INSERTED.[CustInvoiceTotal] Is Not Null And
				DELETED.[CustInvoiceTotal] Is Null
			) Or
			(
				INSERTED.[CustInvoiceTotal] !=
				DELETED.[CustInvoiceTotal]
			)
		) 
		END		
		
      If UPDATE([CustReceived])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustReceived',
      CONVERT(NVARCHAR(2000),DELETED.[CustReceived],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustReceived],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustReceived] Is Null And
				DELETED.[CustReceived] Is Not Null
			) Or
			(
				INSERTED.[CustReceived] Is Not Null And
				DELETED.[CustReceived] Is Null
			) Or
			(
				INSERTED.[CustReceived] !=
				DELETED.[CustReceived]
			)
		) 
		END		
		
      If UPDATE([CustBalance])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustBalance',
      CONVERT(NVARCHAR(2000),DELETED.[CustBalance],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustBalance],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustBalance] Is Null And
				DELETED.[CustBalance] Is Not Null
			) Or
			(
				INSERTED.[CustBalance] Is Not Null And
				DELETED.[CustBalance] Is Null
			) Or
			(
				INSERTED.[CustBalance] !=
				DELETED.[CustBalance]
			)
		) 
		END		
		
     If UPDATE([CustManagementLeader])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustManagementLeader',
     CONVERT(NVARCHAR(2000),DELETED.[CustManagementLeader],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustManagementLeader],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustManagementLeader] Is Null And
				DELETED.[CustManagementLeader] Is Not Null
			) Or
			(
				INSERTED.[CustManagementLeader] Is Not Null And
				DELETED.[CustManagementLeader] Is Null
			) Or
			(
				INSERTED.[CustManagementLeader] !=
				DELETED.[CustManagementLeader]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustManagementLeader = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustManagementLeader = newDesc.Employee
		END		
		
     If UPDATE([CustOperationsManager])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustOperationsManager',
     CONVERT(NVARCHAR(2000),DELETED.[CustOperationsManager],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustOperationsManager],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustOperationsManager] Is Null And
				DELETED.[CustOperationsManager] Is Not Null
			) Or
			(
				INSERTED.[CustOperationsManager] Is Not Null And
				DELETED.[CustOperationsManager] Is Null
			) Or
			(
				INSERTED.[CustOperationsManager] !=
				DELETED.[CustOperationsManager]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustOperationsManager = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustOperationsManager = newDesc.Employee
		END		
		
     If UPDATE([CustDirector])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustDirector',
     CONVERT(NVARCHAR(2000),DELETED.[CustDirector],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustDirector],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustDirector] Is Null And
				DELETED.[CustDirector] Is Not Null
			) Or
			(
				INSERTED.[CustDirector] Is Not Null And
				DELETED.[CustDirector] Is Null
			) Or
			(
				INSERTED.[CustDirector] !=
				DELETED.[CustDirector]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustDirector = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustDirector = newDesc.Employee
		END		
		
     If UPDATE([CustPrincipal])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustPrincipal',
     CONVERT(NVARCHAR(2000),DELETED.[CustPrincipal],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustPrincipal],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustPrincipal] Is Null And
				DELETED.[CustPrincipal] Is Not Null
			) Or
			(
				INSERTED.[CustPrincipal] Is Not Null And
				DELETED.[CustPrincipal] Is Null
			) Or
			(
				INSERTED.[CustPrincipal] !=
				DELETED.[CustPrincipal]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustPrincipal = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustPrincipal = newDesc.Employee
		END		
		
     If UPDATE([CustProjMgr])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustProjMgr',
     CONVERT(NVARCHAR(2000),DELETED.[CustProjMgr],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustProjMgr],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustProjMgr] Is Null And
				DELETED.[CustProjMgr] Is Not Null
			) Or
			(
				INSERTED.[CustProjMgr] Is Not Null And
				DELETED.[CustProjMgr] Is Null
			) Or
			(
				INSERTED.[CustProjMgr] !=
				DELETED.[CustProjMgr]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustProjMgr = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustProjMgr = newDesc.Employee
		END		
		
     If UPDATE([CustBiller])
     BEGIN
     INSERT
     INTO AuditTrail
     (	   ModUser,
     ModDate,
     TableName,
     ActionType,
     PrimaryKey,
     ColumnName,
     OldValue,
     NewValue,
     OldValueDescription,
     NewValueDescription,
     Source,
     Application
     )
     SELECT
     @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustBiller',
     CONVERT(NVARCHAR(2000),DELETED.[CustBiller],121),
     CONVERT(NVARCHAR(2000),INSERTED.[CustBiller],121),
     IsNull(oldDesc.LastName, '') + IsNull(', ' + oldDesc.FirstName, ''), IsNull(newDesc.LastName, '') + IsNull(', ' + newDesc.FirstName, ''), @source, @app
     FROM INSERTED join DELETED on

   INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustBiller] Is Null And
				DELETED.[CustBiller] Is Not Null
			) Or
			(
				INSERTED.[CustBiller] Is Not Null And
				DELETED.[CustBiller] Is Null
			) Or
			(
				INSERTED.[CustBiller] !=
				DELETED.[CustBiller]
			)
		) left join EMMain as oldDesc  WITH (NOLOCK)  on DELETED.CustBiller = oldDesc.Employee  left join  EMMain as newDesc  WITH (NOLOCK)  on INSERTED.CustBiller = newDesc.Employee
		END		
		
      If UPDATE([CustProjectNo])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UDIC_UID],121),'CustProjectNo',
      CONVERT(NVARCHAR(2000),DELETED.[CustProjectNo],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CustProjectNo],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UDIC_UID] = DELETED.[UDIC_UID] AND 
		(
			(
				INSERTED.[CustProjectNo] Is Null And
				DELETED.[CustProjectNo] Is Not Null
			) Or
			(
				INSERTED.[CustProjectNo] Is Not Null And
				DELETED.[CustProjectNo] Is Null
			) Or
			(
				INSERTED.[CustProjectNo] !=
				DELETED.[CustProjectNo]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[UDIC_Collection] ADD CONSTRAINT [UDIC_CollectionPK] PRIMARY KEY CLUSTERED ([UDIC_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [udic_Collections_IDX3] ON [dbo].[UDIC_Collection] ([CustCollectionStatus], [CustPrincipal]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [udic_Collection_IDX2] ON [dbo].[UDIC_Collection] ([CustCollectionStatus], [CustRequestedAction]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [udic_Collection_IDX3] ON [dbo].[UDIC_Collection] ([CustCollectionStatus], [CustRequestedAction]) ON [PRIMARY]
GO
