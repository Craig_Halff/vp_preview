CREATE TABLE [dbo].[ImportMaster]
(
[InfoCenter] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Delimiter] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Qualifier] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateOrder] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateDelimiter] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadingZeros] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ImportMas__Leadi__16E66892] DEFAULT ('N'),
[FourDigit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ImportMas__FourD__17DA8CCB] DEFAULT ('N'),
[TxtFile] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TxtFileValue] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[decimalSymbol] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ODBCDSN] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DSNTableName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InputTableName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VisionTableName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ImportMaster] ADD CONSTRAINT [ImportMasterPK] PRIMARY KEY NONCLUSTERED ([InfoCenter], [UserName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
