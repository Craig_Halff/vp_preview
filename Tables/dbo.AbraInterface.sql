CREATE TABLE [dbo].[AbraInterface]
(
[PKey] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ImpAfter] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__AbraInter__ImpAf__6D6E865C] DEFAULT ('N'),
[ImpDate] [datetime] NULL,
[LookupExpr] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TxtFile] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AbraInterface] ADD CONSTRAINT [AbraInterfacePK] PRIMARY KEY CLUSTERED ([PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
