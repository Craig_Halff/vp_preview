CREATE TABLE [dbo].[DashboardURLParts]
(
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PartName] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[XML] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Global] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ByRole] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DashboardURLParts] ADD CONSTRAINT [DashboardURLPartsPK] PRIMARY KEY NONCLUSTERED ([UserName], [PartKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
