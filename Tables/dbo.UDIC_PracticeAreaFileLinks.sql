CREATE TABLE [dbo].[UDIC_PracticeAreaFileLinks]
(
[LinkID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UDIC_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilePath] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Graphic] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__UDIC_Prac__Graph__4C6F1BF2] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UDIC_PracticeAreaFileLinks] ADD CONSTRAINT [UDIC_PracticeAreaFileLinksPK] PRIMARY KEY NONCLUSTERED ([LinkID], [UDIC_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
