CREATE TABLE [dbo].[UNUnit]
(
[UnitTable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Unit] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SingleLabel] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PluralLabel] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__UN_New__BillingR__75713185] DEFAULT ((0)),
[Format] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConsolBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__UN_New__ConsolBi__766555BE] DEFAULT ('N'),
[ShowDate] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__UN_New__ShowDate__775979F7] DEFAULT ('N'),
[ShowCalc] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__UN_New__ShowCalc__784D9E30] DEFAULT ('N'),
[PostAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__UN_New__CostRate__7941C269] DEFAULT ((0)),
[RegAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OHAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditWBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditWBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditWBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditAccount] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConsolCost] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__UN_New__ConsolCo__7A35E6A2] DEFAULT ('N'),
[AvailableForTimesheet] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__UN_New__Availabl__7C1E2F14] DEFAULT ('N'),
[Item] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeSpecificRevenue] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__UN_New__Employee__7D12534D] DEFAULT ('N'),
[UnitType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_UNUnit]
      ON [dbo].[UNUnit]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UNUnit'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'UnitTable',CONVERT(NVARCHAR(2000),[UnitTable],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'Unit',CONVERT(NVARCHAR(2000),[Unit],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'Name',CONVERT(NVARCHAR(2000),[Name],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'SingleLabel',CONVERT(NVARCHAR(2000),[SingleLabel],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'PluralLabel',CONVERT(NVARCHAR(2000),[PluralLabel],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'BillingRate',CONVERT(NVARCHAR(2000),[BillingRate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'Format',CONVERT(NVARCHAR(2000),[Format],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'ConsolBill',CONVERT(NVARCHAR(2000),[ConsolBill],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'ShowDate',CONVERT(NVARCHAR(2000),[ShowDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'ShowCalc',CONVERT(NVARCHAR(2000),[ShowCalc],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'PostAccount',CONVERT(NVARCHAR(2000),[PostAccount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'CostRate',CONVERT(NVARCHAR(2000),[CostRate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'RegAccount',CONVERT(NVARCHAR(2000),[RegAccount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'OHAccount',CONVERT(NVARCHAR(2000),[OHAccount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'CreditWBS1',CONVERT(NVARCHAR(2000),[CreditWBS1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'CreditWBS2',CONVERT(NVARCHAR(2000),[CreditWBS2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'CreditWBS3',CONVERT(NVARCHAR(2000),[CreditWBS3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'CreditAccount',CONVERT(NVARCHAR(2000),[CreditAccount],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'ConsolCost',CONVERT(NVARCHAR(2000),[ConsolCost],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'AvailableForTimesheet',CONVERT(NVARCHAR(2000),[AvailableForTimesheet],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'Item',CONVERT(NVARCHAR(2000),[Item],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'EmployeeSpecificRevenue',CONVERT(NVARCHAR(2000),[EmployeeSpecificRevenue],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),DELETED.[Unit],121),'UnitType',CONVERT(NVARCHAR(2000),[UnitType],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_UNUnit]
      ON [dbo].[UNUnit]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UNUnit'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'UnitTable',NULL,CONVERT(NVARCHAR(2000),[UnitTable],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'Unit',NULL,CONVERT(NVARCHAR(2000),[Unit],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'Name',NULL,CONVERT(NVARCHAR(2000),[Name],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'SingleLabel',NULL,CONVERT(NVARCHAR(2000),[SingleLabel],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'PluralLabel',NULL,CONVERT(NVARCHAR(2000),[PluralLabel],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'BillingRate',NULL,CONVERT(NVARCHAR(2000),[BillingRate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'Format',NULL,CONVERT(NVARCHAR(2000),[Format],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'ConsolBill',NULL,CONVERT(NVARCHAR(2000),[ConsolBill],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'ShowDate',NULL,CONVERT(NVARCHAR(2000),[ShowDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'ShowCalc',NULL,CONVERT(NVARCHAR(2000),[ShowCalc],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'PostAccount',NULL,CONVERT(NVARCHAR(2000),[PostAccount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'CostRate',NULL,CONVERT(NVARCHAR(2000),[CostRate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'RegAccount',NULL,CONVERT(NVARCHAR(2000),[RegAccount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'OHAccount',NULL,CONVERT(NVARCHAR(2000),[OHAccount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'CreditWBS1',NULL,CONVERT(NVARCHAR(2000),[CreditWBS1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'CreditWBS2',NULL,CONVERT(NVARCHAR(2000),[CreditWBS2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'CreditWBS3',NULL,CONVERT(NVARCHAR(2000),[CreditWBS3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'CreditAccount',NULL,CONVERT(NVARCHAR(2000),[CreditAccount],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'ConsolCost',NULL,CONVERT(NVARCHAR(2000),[ConsolCost],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'AvailableForTimesheet',NULL,CONVERT(NVARCHAR(2000),[AvailableForTimesheet],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'Item',NULL,CONVERT(NVARCHAR(2000),[Item],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'EmployeeSpecificRevenue',NULL,CONVERT(NVARCHAR(2000),[EmployeeSpecificRevenue],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'UnitType',NULL,CONVERT(NVARCHAR(2000),[UnitType],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_UNUnit]
      ON [dbo].[UNUnit]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'UNUnit'
    
      If UPDATE([UnitTable])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'UnitTable',
      CONVERT(NVARCHAR(2000),DELETED.[UnitTable],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UnitTable],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[UnitTable] Is Null And
				DELETED.[UnitTable] Is Not Null
			) Or
			(
				INSERTED.[UnitTable] Is Not Null And
				DELETED.[UnitTable] Is Null
			) Or
			(
				INSERTED.[UnitTable] !=
				DELETED.[UnitTable]
			)
		) 
		END		
		
      If UPDATE([Unit])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'Unit',
      CONVERT(NVARCHAR(2000),DELETED.[Unit],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Unit],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[Unit] Is Null And
				DELETED.[Unit] Is Not Null
			) Or
			(
				INSERTED.[Unit] Is Not Null And
				DELETED.[Unit] Is Null
			) Or
			(
				INSERTED.[Unit] !=
				DELETED.[Unit]
			)
		) 
		END		
		
      If UPDATE([Name])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'Name',
      CONVERT(NVARCHAR(2000),DELETED.[Name],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Name],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[Name] Is Null And
				DELETED.[Name] Is Not Null
			) Or
			(
				INSERTED.[Name] Is Not Null And
				DELETED.[Name] Is Null
			) Or
			(
				INSERTED.[Name] !=
				DELETED.[Name]
			)
		) 
		END		
		
      If UPDATE([SingleLabel])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'SingleLabel',
      CONVERT(NVARCHAR(2000),DELETED.[SingleLabel],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SingleLabel],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[SingleLabel] Is Null And
				DELETED.[SingleLabel] Is Not Null
			) Or
			(
				INSERTED.[SingleLabel] Is Not Null And
				DELETED.[SingleLabel] Is Null
			) Or
			(
				INSERTED.[SingleLabel] !=
				DELETED.[SingleLabel]
			)
		) 
		END		
		
      If UPDATE([PluralLabel])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'PluralLabel',
      CONVERT(NVARCHAR(2000),DELETED.[PluralLabel],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PluralLabel],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[PluralLabel] Is Null And
				DELETED.[PluralLabel] Is Not Null
			) Or
			(
				INSERTED.[PluralLabel] Is Not Null And
				DELETED.[PluralLabel] Is Null
			) Or
			(
				INSERTED.[PluralLabel] !=
				DELETED.[PluralLabel]
			)
		) 
		END		
		
      If UPDATE([BillingRate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'BillingRate',
      CONVERT(NVARCHAR(2000),DELETED.[BillingRate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BillingRate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[BillingRate] Is Null And
				DELETED.[BillingRate] Is Not Null
			) Or
			(
				INSERTED.[BillingRate] Is Not Null And
				DELETED.[BillingRate] Is Null
			) Or
			(
				INSERTED.[BillingRate] !=
				DELETED.[BillingRate]
			)
		) 
		END		
		
      If UPDATE([Format])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'Format',
      CONVERT(NVARCHAR(2000),DELETED.[Format],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Format],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[Format] Is Null And
				DELETED.[Format] Is Not Null
			) Or
			(
				INSERTED.[Format] Is Not Null And
				DELETED.[Format] Is Null
			) Or
			(
				INSERTED.[Format] !=
				DELETED.[Format]
			)
		) 
		END		
		
      If UPDATE([ConsolBill])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'ConsolBill',
      CONVERT(NVARCHAR(2000),DELETED.[ConsolBill],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ConsolBill],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[ConsolBill] Is Null And
				DELETED.[ConsolBill] Is Not Null
			) Or
			(
				INSERTED.[ConsolBill] Is Not Null And
				DELETED.[ConsolBill] Is Null
			) Or
			(
				INSERTED.[ConsolBill] !=
				DELETED.[ConsolBill]
			)
		) 
		END		
		
      If UPDATE([ShowDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'ShowDate',
      CONVERT(NVARCHAR(2000),DELETED.[ShowDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ShowDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[ShowDate] Is Null And
				DELETED.[ShowDate] Is Not Null
			) Or
			(
				INSERTED.[ShowDate] Is Not Null And
				DELETED.[ShowDate] Is Null
			) Or
			(
				INSERTED.[ShowDate] !=
				DELETED.[ShowDate]
			)
		) 
		END		
		
      If UPDATE([ShowCalc])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'ShowCalc',
      CONVERT(NVARCHAR(2000),DELETED.[ShowCalc],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ShowCalc],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[ShowCalc] Is Null And
				DELETED.[ShowCalc] Is Not Null
			) Or
			(
				INSERTED.[ShowCalc] Is Not Null And
				DELETED.[ShowCalc] Is Null
			) Or
			(
				INSERTED.[ShowCalc] !=
				DELETED.[ShowCalc]
			)
		) 
		END		
		
      If UPDATE([PostAccount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'PostAccount',
      CONVERT(NVARCHAR(2000),DELETED.[PostAccount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PostAccount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[PostAccount] Is Null And
				DELETED.[PostAccount] Is Not Null
			) Or
			(
				INSERTED.[PostAccount] Is Not Null And
				DELETED.[PostAccount] Is Null
			) Or
			(
				INSERTED.[PostAccount] !=
				DELETED.[PostAccount]
			)
		) 
		END		
		
      If UPDATE([CostRate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'CostRate',
      CONVERT(NVARCHAR(2000),DELETED.[CostRate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CostRate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[CostRate] Is Null And
				DELETED.[CostRate] Is Not Null
			) Or
			(
				INSERTED.[CostRate] Is Not Null And
				DELETED.[CostRate] Is Null
			) Or
			(
				INSERTED.[CostRate] !=
				DELETED.[CostRate]
			)
		) 
		END		
		
      If UPDATE([RegAccount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'RegAccount',
      CONVERT(NVARCHAR(2000),DELETED.[RegAccount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[RegAccount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[RegAccount] Is Null And
				DELETED.[RegAccount] Is Not Null
			) Or
			(
				INSERTED.[RegAccount] Is Not Null And
				DELETED.[RegAccount] Is Null
			) Or
			(
				INSERTED.[RegAccount] !=
				DELETED.[RegAccount]
			)
		) 
		END		
		
      If UPDATE([OHAccount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'OHAccount',
      CONVERT(NVARCHAR(2000),DELETED.[OHAccount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[OHAccount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[OHAccount] Is Null And
				DELETED.[OHAccount] Is Not Null
			) Or
			(
				INSERTED.[OHAccount] Is Not Null And
				DELETED.[OHAccount] Is Null
			) Or
			(
				INSERTED.[OHAccount] !=
				DELETED.[OHAccount]
			)
		) 
		END		
		
      If UPDATE([CreditWBS1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'CreditWBS1',
      CONVERT(NVARCHAR(2000),DELETED.[CreditWBS1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CreditWBS1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[CreditWBS1] Is Null And
				DELETED.[CreditWBS1] Is Not Null
			) Or
			(
				INSERTED.[CreditWBS1] Is Not Null And
				DELETED.[CreditWBS1] Is Null
			) Or
			(
				INSERTED.[CreditWBS1] !=
				DELETED.[CreditWBS1]
			)
		) 
		END		
		
      If UPDATE([CreditWBS2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'CreditWBS2',
      CONVERT(NVARCHAR(2000),DELETED.[CreditWBS2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CreditWBS2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[CreditWBS2] Is Null And
				DELETED.[CreditWBS2] Is Not Null
			) Or
			(
				INSERTED.[CreditWBS2] Is Not Null And
				DELETED.[CreditWBS2] Is Null
			) Or
			(
				INSERTED.[CreditWBS2] !=
				DELETED.[CreditWBS2]
			)
		) 
		END		
		
      If UPDATE([CreditWBS3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'CreditWBS3',
      CONVERT(NVARCHAR(2000),DELETED.[CreditWBS3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CreditWBS3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[CreditWBS3] Is Null And
				DELETED.[CreditWBS3] Is Not Null
			) Or
			(
				INSERTED.[CreditWBS3] Is Not Null And
				DELETED.[CreditWBS3] Is Null
			) Or
			(
				INSERTED.[CreditWBS3] !=
				DELETED.[CreditWBS3]
			)
		) 
		END		
		
      If UPDATE([CreditAccount])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'CreditAccount',
      CONVERT(NVARCHAR(2000),DELETED.[CreditAccount],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CreditAccount],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[CreditAccount] Is Null And
				DELETED.[CreditAccount] Is Not Null
			) Or
			(
				INSERTED.[CreditAccount] Is Not Null And
				DELETED.[CreditAccount] Is Null
			) Or
			(
				INSERTED.[CreditAccount] !=
				DELETED.[CreditAccount]
			)
		) 
		END		
		
      If UPDATE([ConsolCost])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'ConsolCost',
      CONVERT(NVARCHAR(2000),DELETED.[ConsolCost],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ConsolCost],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[ConsolCost] Is Null And
				DELETED.[ConsolCost] Is Not Null
			) Or
			(
				INSERTED.[ConsolCost] Is Not Null And
				DELETED.[ConsolCost] Is Null
			) Or
			(
				INSERTED.[ConsolCost] !=
				DELETED.[ConsolCost]
			)
		) 
		END		
		
      If UPDATE([AvailableForTimesheet])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'AvailableForTimesheet',
      CONVERT(NVARCHAR(2000),DELETED.[AvailableForTimesheet],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AvailableForTimesheet],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[AvailableForTimesheet] Is Null And
				DELETED.[AvailableForTimesheet] Is Not Null
			) Or
			(
				INSERTED.[AvailableForTimesheet] Is Not Null And
				DELETED.[AvailableForTimesheet] Is Null
			) Or
			(
				INSERTED.[AvailableForTimesheet] !=
				DELETED.[AvailableForTimesheet]
			)
		) 
		END		
		
      If UPDATE([Item])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'Item',
      CONVERT(NVARCHAR(2000),DELETED.[Item],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Item],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[Item] Is Null And
				DELETED.[Item] Is Not Null
			) Or
			(
				INSERTED.[Item] Is Not Null And
				DELETED.[Item] Is Null
			) Or
			(
				INSERTED.[Item] !=
				DELETED.[Item]
			)
		) 
		END		
		
      If UPDATE([EmployeeSpecificRevenue])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'EmployeeSpecificRevenue',
      CONVERT(NVARCHAR(2000),DELETED.[EmployeeSpecificRevenue],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EmployeeSpecificRevenue],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[EmployeeSpecificRevenue] Is Null And
				DELETED.[EmployeeSpecificRevenue] Is Not Null
			) Or
			(
				INSERTED.[EmployeeSpecificRevenue] Is Not Null And
				DELETED.[EmployeeSpecificRevenue] Is Null
			) Or
			(
				INSERTED.[EmployeeSpecificRevenue] !=
				DELETED.[EmployeeSpecificRevenue]
			)
		) 
		END		
		
      If UPDATE([UnitType])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[UnitTable],121) + '|' + CONVERT(NVARCHAR(255),INSERTED.[Unit],121),'UnitType',
      CONVERT(NVARCHAR(2000),DELETED.[UnitType],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UnitType],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[UnitTable] = DELETED.[UnitTable] AND INSERTED.[Unit] = DELETED.[Unit] AND 
		(
			(
				INSERTED.[UnitType] Is Null And
				DELETED.[UnitType] Is Not Null
			) Or
			(
				INSERTED.[UnitType] Is Not Null And
				DELETED.[UnitType] Is Null
			) Or
			(
				INSERTED.[UnitType] !=
				DELETED.[UnitType]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
ALTER TABLE [dbo].[UNUnit] ADD CONSTRAINT [UNPK] PRIMARY KEY NONCLUSTERED ([UnitTable], [Unit]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [UNUnitNameUnitUnitTableIDX] ON [dbo].[UNUnit] ([Name], [Unit], [UnitTable]) INCLUDE ([SingleLabel], [PluralLabel], [BillingRate], [Format], [ConsolBill], [ShowDate], [ShowCalc], [PostAccount], [CostRate], [RegAccount], [OHAccount], [CreditWBS1], [CreditWBS2], [CreditWBS3], [CreditAccount], [ConsolCost], [AvailableForTimesheet], [Item], [EmployeeSpecificRevenue], [UnitType]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [UNUnitIDX] ON [dbo].[UNUnit] ([Unit], [UnitTable], [Name]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
