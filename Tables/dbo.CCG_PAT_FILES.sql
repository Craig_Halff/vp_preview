CREATE TABLE [dbo].[CCG_PAT_FILES]
(
[REVID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__CCG_PAT_F__REVID__280FC5A9] DEFAULT (newid()),
[RevisionSeq] [int] NOT NULL,
[StorageSeq] [int] NULL,
[StoragePathOrID] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayableSeq] [int] NULL,
[FileName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileState] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_FILES] ADD CONSTRAINT [PK__CCG_PAT___42C4FB00CFCB0981] PRIMARY KEY CLUSTERED ([REVID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IxCCG_PAT_FILES_PayableSeq] ON [dbo].[CCG_PAT_FILES] ([PayableSeq]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Revision history for files linked to payable items', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_FILES', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Original filename', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_FILES', 'COLUMN', N'FileName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'NULL or ACTIVE (DEFAULT),DELETED,REPLACED', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_FILES', 'COLUMN', N'FileState'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Last modified date/time', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_FILES', 'COLUMN', N'ModDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Last modified user', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_FILES', 'COLUMN', N'ModUser'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - Payable', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_FILES', 'COLUMN', N'PayableSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Key', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_FILES', 'COLUMN', N'REVID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'0 for current, 1 is added to existing entries when a new row is inserted', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_FILES', 'COLUMN', N'RevisionSeq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'File identifier for storage type', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_FILES', 'COLUMN', N'StoragePathOrID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK - ConfigStorage', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_FILES', 'COLUMN', N'StorageSeq'
GO
