CREATE TABLE [dbo].[CFGServProFeeBands]
(
[ServProCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Pkey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChargeableCosts] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGServPr__Charg__5EAC3110] DEFAULT ((0)),
[FeeBand1] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGServPr__FeeBa__5FA05549] DEFAULT ((0)),
[FeeBand2] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGServPr__FeeBa__60947982] DEFAULT ((0)),
[FeeBand3] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGServPr__FeeBa__61889DBB] DEFAULT ((0)),
[FeeBand4] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGServPr__FeeBa__627CC1F4] DEFAULT ((0)),
[FeeBand5] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGServPr__FeeBa__6370E62D] DEFAULT ((0)),
[FeeBand6] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGServPr__FeeBa__64650A66] DEFAULT ((0)),
[FeeBand7] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGServPr__FeeBa__65592E9F] DEFAULT ((0)),
[FeeBand8] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGServPr__FeeBa__664D52D8] DEFAULT ((0)),
[FeeBand9] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGServPr__FeeBa__67417711] DEFAULT ((0)),
[FeeBandMax] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__CFGServPr__FeeBa__68359B4A] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGServProFeeBands] ADD CONSTRAINT [CFGServProFeeBandsPK] PRIMARY KEY CLUSTERED ([ServProCode], [Pkey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
