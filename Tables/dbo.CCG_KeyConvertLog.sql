CREATE TABLE [dbo].[CCG_KeyConvertLog]
(
[PKey] [uniqueidentifier] NOT NULL,
[SessionSeq] [uniqueidentifier] NOT NULL,
[Products] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateDate] [datetime] NOT NULL CONSTRAINT [DF__CCG_KeyCo__Creat__1CEC8807] DEFAULT (getdate()),
[Entity] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldKey2] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldKey3] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewKey2] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewKey3] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_KeyConvertLog] ADD CONSTRAINT [CCG_KeyConvertLogPK] PRIMARY KEY NONCLUSTERED ([PKey], [SessionSeq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
