CREATE TABLE [dbo].[CustomRepOptCaptions]
(
[ReportName] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColName] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColLabel] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultValue] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomRepOptCaptions] ADD CONSTRAINT [CustomRepOptCaptionsPK] PRIMARY KEY NONCLUSTERED ([ReportName], [UICultureName], [TabID], [ColName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
