CREATE TABLE [dbo].[PNAccordionFormat]
(
[AccordionFormatID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[MajorScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNAccordi__Major__2B976CB5] DEFAULT ('m'),
[MinorScale] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PNAccordi__Minor__2C8B90EE] DEFAULT ('w'),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PNAccordionFormat] ADD CONSTRAINT [PNAccordionFormatPK] PRIMARY KEY NONCLUSTERED ([AccordionFormatID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [PNAccordionFormatPlanIDIDX] ON [dbo].[PNAccordionFormat] ([PlanID]) ON [PRIMARY]
GO
