CREATE TABLE [dbo].[EMMain]
(
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HomeCompany] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingPool] [smallint] NOT NULL CONSTRAINT [DF__EMMain_Ne__Billi__13CA04C5] DEFAULT ((0)),
[SSN] [nvarchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address3] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIP] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HomePhone] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fax] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMail] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TKAdminLevel] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TKAdminEdit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMMain_Ne__TKAdm__14BE28FE] DEFAULT ('N'),
[EKAdminLevel] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EKAdminEdit] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMMain_Ne__EKAdm__16A67170] DEFAULT ('N'),
[Memo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeePhoto] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Salutation] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suffix] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [nvarchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExportInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMMain_Ne__Expor__179A95A9] DEFAULT ('N'),
[WorkPhone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MobilePhone] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AvailableForCRM] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMMain_Ne__Avail__188EB9E2] DEFAULT ('N'),
[ReadyForApproval] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMMain_Ne__Ready__1982DE1B] DEFAULT ('N'),
[PreferredName] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HomePhoneFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FaxFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkPhoneFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MobilePhoneFormat] [nvarchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Language] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TargetRatio] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMMain_Ne__Targe__1A770254] DEFAULT ((0)),
[UtilizationRatio] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__EMMain_Ne__Utili__1B6B268D] DEFAULT ((0)),
[ConsultantInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__EMMain_Ne__Consu__1C5F4AC6] DEFAULT ('N'),
[ClientVendorInd] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__EMMain_Ne__Clien__1D536EFF] DEFAULT ('C'),
[ClientID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TalentUserID] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Location] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLInternalKey] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProfessionalSuffix] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TalentModDate] [datetime] NULL,
[TLSyncModDate] [datetime] NULL,
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[PIMID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_EMMain_PIMID] DEFAULT (replace(newid(),'-','')),
[KonaUsername] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KonaAccessToken] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KonaRefreshToken] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KonaUserID] [int] NOT NULL CONSTRAINT [DF__EMMain__KonaUser__7BD4E1A8] DEFAULT ((0)),
[CitizenshipStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QBOID] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QBOAddressID] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QBOLastUpdated] [datetime] NULL,
[QBOVendorID] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Delete_EMMain]
      ON [dbo].[EMMain]
      For Delete
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMMain'
    
		 
		 declare @noAuditDetails varchar(1)
		 set @noAuditDetails = dbo.GetVisionAuditingDetail()
   
		 if exists(select AuditKeyValuesDelete FROM FW_CFGSystem where AuditKeyValuesDelete= 'Y' 
		 and @noAuditDetails='Y')
begin
declare @placeholder varchar(1)
end
else
begin
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'Employee',CONVERT(NVARCHAR(2000),[Employee],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'HomeCompany',CONVERT(NVARCHAR(2000),[HomeCompany],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'LastName',CONVERT(NVARCHAR(2000),[LastName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'FirstName',CONVERT(NVARCHAR(2000),[FirstName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'MiddleName',CONVERT(NVARCHAR(2000),[MiddleName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'BillingPool',CONVERT(NVARCHAR(2000),[BillingPool],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'SSN',CONVERT(NVARCHAR(2000),[SSN],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'Address1',CONVERT(NVARCHAR(2000),[Address1],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'Address2',CONVERT(NVARCHAR(2000),[Address2],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'Address3',CONVERT(NVARCHAR(2000),[Address3],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'City',CONVERT(NVARCHAR(2000),[City],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'State',CONVERT(NVARCHAR(2000),[State],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'ZIP',CONVERT(NVARCHAR(2000),[ZIP],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'Country',CONVERT(NVARCHAR(2000),[Country],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'HomePhone',CONVERT(NVARCHAR(2000),[HomePhone],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'Fax',CONVERT(NVARCHAR(2000),[Fax],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'EMail',CONVERT(NVARCHAR(2000),[EMail],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'TKAdminLevel',CONVERT(NVARCHAR(2000),[TKAdminLevel],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'TKAdminEdit',CONVERT(NVARCHAR(2000),[TKAdminEdit],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'EKAdminLevel',CONVERT(NVARCHAR(2000),[EKAdminLevel],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'EKAdminEdit',CONVERT(NVARCHAR(2000),[EKAdminEdit],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'Memo','[text]',NULL, @source,@app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'EmployeePhoto',CONVERT(NVARCHAR(2000),[EmployeePhoto],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'Salutation',CONVERT(NVARCHAR(2000),[Salutation],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'Suffix',CONVERT(NVARCHAR(2000),[Suffix],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'Title',CONVERT(NVARCHAR(2000),[Title],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'ExportInd',CONVERT(NVARCHAR(2000),[ExportInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'WorkPhone',CONVERT(NVARCHAR(2000),[WorkPhone],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'MobilePhone',CONVERT(NVARCHAR(2000),[MobilePhone],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'AvailableForCRM',CONVERT(NVARCHAR(2000),[AvailableForCRM],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'ReadyForApproval',CONVERT(NVARCHAR(2000),[ReadyForApproval],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'PreferredName',CONVERT(NVARCHAR(2000),[PreferredName],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'HomePhoneFormat',CONVERT(NVARCHAR(2000),[HomePhoneFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'FaxFormat',CONVERT(NVARCHAR(2000),[FaxFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'WorkPhoneFormat',CONVERT(NVARCHAR(2000),[WorkPhoneFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'MobilePhoneFormat',CONVERT(NVARCHAR(2000),[MobilePhoneFormat],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'Language',CONVERT(NVARCHAR(2000),[Language],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'TargetRatio',CONVERT(NVARCHAR(2000),[TargetRatio],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'UtilizationRatio',CONVERT(NVARCHAR(2000),[UtilizationRatio],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'ConsultantInd',CONVERT(NVARCHAR(2000),[ConsultantInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'ClientVendorInd',CONVERT(NVARCHAR(2000),[ClientVendorInd],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'ClientID',CONVERT(NVARCHAR(2000),[ClientID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'Vendor',CONVERT(NVARCHAR(2000),[Vendor],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'TalentUserID',CONVERT(NVARCHAR(2000),[TalentUserID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'Location',CONVERT(NVARCHAR(2000),[Location],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'TLInternalKey',CONVERT(NVARCHAR(2000),[TLInternalKey],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'ProfessionalSuffix',CONVERT(NVARCHAR(2000),[ProfessionalSuffix],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'TalentModDate',CONVERT(NVARCHAR(2000),[TalentModDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'TLSyncModDate',CONVERT(NVARCHAR(2000),[TLSyncModDate],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'KonaUsername',CONVERT(NVARCHAR(2000),[KonaUsername],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'KonaAccessToken',CONVERT(NVARCHAR(2000),[KonaAccessToken],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'KonaRefreshToken',CONVERT(NVARCHAR(2000),[KonaRefreshToken],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'KonaUserID',CONVERT(NVARCHAR(2000),[KonaUserID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'CitizenshipStatus',CONVERT(NVARCHAR(2000),[CitizenshipStatus],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'QBOID',CONVERT(NVARCHAR(2000),[QBOID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'QBOAddressID',CONVERT(NVARCHAR(2000),[QBOAddressID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'QBOLastUpdated',CONVERT(NVARCHAR(2000),[QBOLastUpdated],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'QBOVendorID',CONVERT(NVARCHAR(2000),[QBOVendorID],121),NULL, @source, @app
      FROM DELETED
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'DELETE',CONVERT(NVARCHAR(255),DELETED.[Employee],121),'PIMID',CONVERT(NVARCHAR(2000),[PIMID],121),NULL, @source, @app
      FROM DELETED
    
end
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Delete_EMMain] ON [dbo].[EMMain]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Insert_EMMain]
      ON [dbo].[EMMain]
      For Insert
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMMain'
    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Employee',NULL,CONVERT(NVARCHAR(2000),[Employee],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'HomeCompany',NULL,CONVERT(NVARCHAR(2000),[HomeCompany],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'LastName',NULL,CONVERT(NVARCHAR(2000),[LastName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'FirstName',NULL,CONVERT(NVARCHAR(2000),[FirstName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'MiddleName',NULL,CONVERT(NVARCHAR(2000),[MiddleName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'BillingPool',NULL,CONVERT(NVARCHAR(2000),[BillingPool],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'SSN',NULL,CONVERT(NVARCHAR(2000),[SSN],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Address1',NULL,CONVERT(NVARCHAR(2000),[Address1],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Address2',NULL,CONVERT(NVARCHAR(2000),[Address2],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Address3',NULL,CONVERT(NVARCHAR(2000),[Address3],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'City',NULL,CONVERT(NVARCHAR(2000),[City],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'State',NULL,CONVERT(NVARCHAR(2000),[State],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'ZIP',NULL,CONVERT(NVARCHAR(2000),[ZIP],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Country',NULL,CONVERT(NVARCHAR(2000),[Country],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'HomePhone',NULL,CONVERT(NVARCHAR(2000),[HomePhone],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Fax',NULL,CONVERT(NVARCHAR(2000),[Fax],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'EMail',NULL,CONVERT(NVARCHAR(2000),[EMail],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'TKAdminLevel',NULL,CONVERT(NVARCHAR(2000),[TKAdminLevel],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'TKAdminEdit',NULL,CONVERT(NVARCHAR(2000),[TKAdminEdit],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'EKAdminLevel',NULL,CONVERT(NVARCHAR(2000),[EKAdminLevel],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'EKAdminEdit',NULL,CONVERT(NVARCHAR(2000),[EKAdminEdit],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Memo',NULL,'[text]', @source, @app
      FROM INSERTED


    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'EmployeePhoto',NULL,CONVERT(NVARCHAR(2000),[EmployeePhoto],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Salutation',NULL,CONVERT(NVARCHAR(2000),[Salutation],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Suffix',NULL,CONVERT(NVARCHAR(2000),[Suffix],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Title',NULL,CONVERT(NVARCHAR(2000),[Title],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'ExportInd',NULL,CONVERT(NVARCHAR(2000),[ExportInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'WorkPhone',NULL,CONVERT(NVARCHAR(2000),[WorkPhone],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'MobilePhone',NULL,CONVERT(NVARCHAR(2000),[MobilePhone],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'AvailableForCRM',NULL,CONVERT(NVARCHAR(2000),[AvailableForCRM],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'ReadyForApproval',NULL,CONVERT(NVARCHAR(2000),[ReadyForApproval],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'PreferredName',NULL,CONVERT(NVARCHAR(2000),[PreferredName],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'HomePhoneFormat',NULL,CONVERT(NVARCHAR(2000),[HomePhoneFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'FaxFormat',NULL,CONVERT(NVARCHAR(2000),[FaxFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'WorkPhoneFormat',NULL,CONVERT(NVARCHAR(2000),[WorkPhoneFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'MobilePhoneFormat',NULL,CONVERT(NVARCHAR(2000),[MobilePhoneFormat],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Language',NULL,CONVERT(NVARCHAR(2000),[Language],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'TargetRatio',NULL,CONVERT(NVARCHAR(2000),[TargetRatio],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'UtilizationRatio',NULL,CONVERT(NVARCHAR(2000),[UtilizationRatio],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'ConsultantInd',NULL,CONVERT(NVARCHAR(2000),[ConsultantInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'ClientVendorInd',NULL,CONVERT(NVARCHAR(2000),[ClientVendorInd],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'ClientID',NULL,CONVERT(NVARCHAR(2000),[ClientID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Vendor',NULL,CONVERT(NVARCHAR(2000),[Vendor],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'TalentUserID',NULL,CONVERT(NVARCHAR(2000),[TalentUserID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Location',NULL,CONVERT(NVARCHAR(2000),[Location],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'TLInternalKey',NULL,CONVERT(NVARCHAR(2000),[TLInternalKey],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'ProfessionalSuffix',NULL,CONVERT(NVARCHAR(2000),[ProfessionalSuffix],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'TalentModDate',NULL,CONVERT(NVARCHAR(2000),[TalentModDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'TLSyncModDate',NULL,CONVERT(NVARCHAR(2000),[TLSyncModDate],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'KonaUsername',NULL,CONVERT(NVARCHAR(2000),[KonaUsername],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'KonaAccessToken',NULL,CONVERT(NVARCHAR(2000),[KonaAccessToken],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'KonaRefreshToken',NULL,CONVERT(NVARCHAR(2000),[KonaRefreshToken],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'KonaUserID',NULL,CONVERT(NVARCHAR(2000),[KonaUserID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CitizenshipStatus',NULL,CONVERT(NVARCHAR(2000),[CitizenshipStatus],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'QBOID',NULL,CONVERT(NVARCHAR(2000),[QBOID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'QBOAddressID',NULL,CONVERT(NVARCHAR(2000),[QBOAddressID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'QBOLastUpdated',NULL,CONVERT(NVARCHAR(2000),[QBOLastUpdated],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'QBOVendorID',NULL,CONVERT(NVARCHAR(2000),[QBOVendorID],121), @source, @app
      FROM INSERTED

    
      INSERT INTO AuditTrail	(
      ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'INSERT',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'PIMID',NULL,CONVERT(NVARCHAR(2000),[PIMID],121), @source, @app
      FROM INSERTED

    
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Insert_EMMain] ON [dbo].[EMMain]
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

      CREATE TRIGGER [dbo].[VisionAudit_Update_EMMain]
      ON [dbo].[EMMain]
      For Update
      NOT FOR REPLICATION
      AS BEGIN
      SET NOCOUNT ON
      DECLARE @VisionAuditUser Nvarchar(50), @table varchar(500), @now datetime, @source Nvarchar(3), @app Nvarchar(50)
      set @VisionAuditUser = dbo.FW_GetUsername()

      If @VisionAuditUser = ''
      return

      set @source = dbo.GetVisionAuditSource()
      set @app = (select top 1 lastapp from FW_Useractivity where userid = @VisionAuditUser order by lastaccess desc)
      set @now = dbo.GetVisionAuditTime()

      If @now = '1900-01-01 00:00:00.000'
      return

      set @table = 'EMMain'
    
      If UPDATE([Employee])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Employee',
      CONVERT(NVARCHAR(2000),DELETED.[Employee],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Employee],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[Employee] Is Null And
				DELETED.[Employee] Is Not Null
			) Or
			(
				INSERTED.[Employee] Is Not Null And
				DELETED.[Employee] Is Null
			) Or
			(
				INSERTED.[Employee] !=
				DELETED.[Employee]
			)
		) 
		END		
		
      If UPDATE([HomeCompany])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'HomeCompany',
      CONVERT(NVARCHAR(2000),DELETED.[HomeCompany],121),
      CONVERT(NVARCHAR(2000),INSERTED.[HomeCompany],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[HomeCompany] Is Null And
				DELETED.[HomeCompany] Is Not Null
			) Or
			(
				INSERTED.[HomeCompany] Is Not Null And
				DELETED.[HomeCompany] Is Null
			) Or
			(
				INSERTED.[HomeCompany] !=
				DELETED.[HomeCompany]
			)
		) 
		END		
		
      If UPDATE([LastName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'LastName',
      CONVERT(NVARCHAR(2000),DELETED.[LastName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[LastName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[LastName] Is Null And
				DELETED.[LastName] Is Not Null
			) Or
			(
				INSERTED.[LastName] Is Not Null And
				DELETED.[LastName] Is Null
			) Or
			(
				INSERTED.[LastName] !=
				DELETED.[LastName]
			)
		) 
		END		
		
      If UPDATE([FirstName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'FirstName',
      CONVERT(NVARCHAR(2000),DELETED.[FirstName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FirstName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[FirstName] Is Null And
				DELETED.[FirstName] Is Not Null
			) Or
			(
				INSERTED.[FirstName] Is Not Null And
				DELETED.[FirstName] Is Null
			) Or
			(
				INSERTED.[FirstName] !=
				DELETED.[FirstName]
			)
		) 
		END		
		
      If UPDATE([MiddleName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'MiddleName',
      CONVERT(NVARCHAR(2000),DELETED.[MiddleName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MiddleName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[MiddleName] Is Null And
				DELETED.[MiddleName] Is Not Null
			) Or
			(
				INSERTED.[MiddleName] Is Not Null And
				DELETED.[MiddleName] Is Null
			) Or
			(
				INSERTED.[MiddleName] !=
				DELETED.[MiddleName]
			)
		) 
		END		
		
      If UPDATE([BillingPool])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'BillingPool',
      CONVERT(NVARCHAR(2000),DELETED.[BillingPool],121),
      CONVERT(NVARCHAR(2000),INSERTED.[BillingPool],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[BillingPool] Is Null And
				DELETED.[BillingPool] Is Not Null
			) Or
			(
				INSERTED.[BillingPool] Is Not Null And
				DELETED.[BillingPool] Is Null
			) Or
			(
				INSERTED.[BillingPool] !=
				DELETED.[BillingPool]
			)
		) 
		END		
		
      If UPDATE([SSN])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'SSN',
      CONVERT(NVARCHAR(2000),DELETED.[SSN],121),
      CONVERT(NVARCHAR(2000),INSERTED.[SSN],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[SSN] Is Null And
				DELETED.[SSN] Is Not Null
			) Or
			(
				INSERTED.[SSN] Is Not Null And
				DELETED.[SSN] Is Null
			) Or
			(
				INSERTED.[SSN] !=
				DELETED.[SSN]
			)
		) 
		END		
		
      If UPDATE([Address1])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Address1',
      CONVERT(NVARCHAR(2000),DELETED.[Address1],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Address1],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[Address1] Is Null And
				DELETED.[Address1] Is Not Null
			) Or
			(
				INSERTED.[Address1] Is Not Null And
				DELETED.[Address1] Is Null
			) Or
			(
				INSERTED.[Address1] !=
				DELETED.[Address1]
			)
		) 
		END		
		
      If UPDATE([Address2])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Address2',
      CONVERT(NVARCHAR(2000),DELETED.[Address2],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Address2],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[Address2] Is Null And
				DELETED.[Address2] Is Not Null
			) Or
			(
				INSERTED.[Address2] Is Not Null And
				DELETED.[Address2] Is Null
			) Or
			(
				INSERTED.[Address2] !=
				DELETED.[Address2]
			)
		) 
		END		
		
      If UPDATE([Address3])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Address3',
      CONVERT(NVARCHAR(2000),DELETED.[Address3],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Address3],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[Address3] Is Null And
				DELETED.[Address3] Is Not Null
			) Or
			(
				INSERTED.[Address3] Is Not Null And
				DELETED.[Address3] Is Null
			) Or
			(
				INSERTED.[Address3] !=
				DELETED.[Address3]
			)
		) 
		END		
		
      If UPDATE([City])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'City',
      CONVERT(NVARCHAR(2000),DELETED.[City],121),
      CONVERT(NVARCHAR(2000),INSERTED.[City],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[City] Is Null And
				DELETED.[City] Is Not Null
			) Or
			(
				INSERTED.[City] Is Not Null And
				DELETED.[City] Is Null
			) Or
			(
				INSERTED.[City] !=
				DELETED.[City]
			)
		) 
		END		
		
      If UPDATE([State])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'State',
      CONVERT(NVARCHAR(2000),DELETED.[State],121),
      CONVERT(NVARCHAR(2000),INSERTED.[State],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[State] Is Null And
				DELETED.[State] Is Not Null
			) Or
			(
				INSERTED.[State] Is Not Null And
				DELETED.[State] Is Null
			) Or
			(
				INSERTED.[State] !=
				DELETED.[State]
			)
		) 
		END		
		
      If UPDATE([ZIP])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'ZIP',
      CONVERT(NVARCHAR(2000),DELETED.[ZIP],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ZIP],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[ZIP] Is Null And
				DELETED.[ZIP] Is Not Null
			) Or
			(
				INSERTED.[ZIP] Is Not Null And
				DELETED.[ZIP] Is Null
			) Or
			(
				INSERTED.[ZIP] !=
				DELETED.[ZIP]
			)
		) 
		END		
		
      If UPDATE([Country])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Country',
      CONVERT(NVARCHAR(2000),DELETED.[Country],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Country],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[Country] Is Null And
				DELETED.[Country] Is Not Null
			) Or
			(
				INSERTED.[Country] Is Not Null And
				DELETED.[Country] Is Null
			) Or
			(
				INSERTED.[Country] !=
				DELETED.[Country]
			)
		) 
		END		
		
      If UPDATE([HomePhone])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'HomePhone',
      CONVERT(NVARCHAR(2000),DELETED.[HomePhone],121),
      CONVERT(NVARCHAR(2000),INSERTED.[HomePhone],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[HomePhone] Is Null And
				DELETED.[HomePhone] Is Not Null
			) Or
			(
				INSERTED.[HomePhone] Is Not Null And
				DELETED.[HomePhone] Is Null
			) Or
			(
				INSERTED.[HomePhone] !=
				DELETED.[HomePhone]
			)
		) 
		END		
		
      If UPDATE([Fax])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Fax',
      CONVERT(NVARCHAR(2000),DELETED.[Fax],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Fax],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[Fax] Is Null And
				DELETED.[Fax] Is Not Null
			) Or
			(
				INSERTED.[Fax] Is Not Null And
				DELETED.[Fax] Is Null
			) Or
			(
				INSERTED.[Fax] !=
				DELETED.[Fax]
			)
		) 
		END		
		
      If UPDATE([EMail])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'EMail',
      CONVERT(NVARCHAR(2000),DELETED.[EMail],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EMail],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[EMail] Is Null And
				DELETED.[EMail] Is Not Null
			) Or
			(
				INSERTED.[EMail] Is Not Null And
				DELETED.[EMail] Is Null
			) Or
			(
				INSERTED.[EMail] !=
				DELETED.[EMail]
			)
		) 
		END		
		
      If UPDATE([TKAdminLevel])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'TKAdminLevel',
      CONVERT(NVARCHAR(2000),DELETED.[TKAdminLevel],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TKAdminLevel],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[TKAdminLevel] Is Null And
				DELETED.[TKAdminLevel] Is Not Null
			) Or
			(
				INSERTED.[TKAdminLevel] Is Not Null And
				DELETED.[TKAdminLevel] Is Null
			) Or
			(
				INSERTED.[TKAdminLevel] !=
				DELETED.[TKAdminLevel]
			)
		) 
		END		
		
      If UPDATE([TKAdminEdit])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'TKAdminEdit',
      CONVERT(NVARCHAR(2000),DELETED.[TKAdminEdit],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TKAdminEdit],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[TKAdminEdit] Is Null And
				DELETED.[TKAdminEdit] Is Not Null
			) Or
			(
				INSERTED.[TKAdminEdit] Is Not Null And
				DELETED.[TKAdminEdit] Is Null
			) Or
			(
				INSERTED.[TKAdminEdit] !=
				DELETED.[TKAdminEdit]
			)
		) 
		END		
		
      If UPDATE([EKAdminLevel])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'EKAdminLevel',
      CONVERT(NVARCHAR(2000),DELETED.[EKAdminLevel],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EKAdminLevel],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[EKAdminLevel] Is Null And
				DELETED.[EKAdminLevel] Is Not Null
			) Or
			(
				INSERTED.[EKAdminLevel] Is Not Null And
				DELETED.[EKAdminLevel] Is Null
			) Or
			(
				INSERTED.[EKAdminLevel] !=
				DELETED.[EKAdminLevel]
			)
		) 
		END		
		
      If UPDATE([EKAdminEdit])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'EKAdminEdit',
      CONVERT(NVARCHAR(2000),DELETED.[EKAdminEdit],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EKAdminEdit],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[EKAdminEdit] Is Null And
				DELETED.[EKAdminEdit] Is Not Null
			) Or
			(
				INSERTED.[EKAdminEdit] Is Not Null And
				DELETED.[EKAdminEdit] Is Null
			) Or
			(
				INSERTED.[EKAdminEdit] !=
				DELETED.[EKAdminEdit]
			)
		) 
		END		
		
      If UPDATE([Memo])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Memo',
      '[text]',
      '[text]', @source,@app
      FROM INSERTED
      END
    
      If UPDATE([EmployeePhoto])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'EmployeePhoto',
      CONVERT(NVARCHAR(2000),DELETED.[EmployeePhoto],121),
      CONVERT(NVARCHAR(2000),INSERTED.[EmployeePhoto],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[EmployeePhoto] Is Null And
				DELETED.[EmployeePhoto] Is Not Null
			) Or
			(
				INSERTED.[EmployeePhoto] Is Not Null And
				DELETED.[EmployeePhoto] Is Null
			) Or
			(
				INSERTED.[EmployeePhoto] !=
				DELETED.[EmployeePhoto]
			)
		) 
		END		
		
      If UPDATE([Salutation])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Salutation',
      CONVERT(NVARCHAR(2000),DELETED.[Salutation],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Salutation],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[Salutation] Is Null And
				DELETED.[Salutation] Is Not Null
			) Or
			(
				INSERTED.[Salutation] Is Not Null And
				DELETED.[Salutation] Is Null
			) Or
			(
				INSERTED.[Salutation] !=
				DELETED.[Salutation]
			)
		) 
		END		
		
      If UPDATE([Suffix])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Suffix',
      CONVERT(NVARCHAR(2000),DELETED.[Suffix],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Suffix],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[Suffix] Is Null And
				DELETED.[Suffix] Is Not Null
			) Or
			(
				INSERTED.[Suffix] Is Not Null And
				DELETED.[Suffix] Is Null
			) Or
			(
				INSERTED.[Suffix] !=
				DELETED.[Suffix]
			)
		) 
		END		
		
      If UPDATE([Title])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Title',
      CONVERT(NVARCHAR(2000),DELETED.[Title],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Title],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[Title] Is Null And
				DELETED.[Title] Is Not Null
			) Or
			(
				INSERTED.[Title] Is Not Null And
				DELETED.[Title] Is Null
			) Or
			(
				INSERTED.[Title] !=
				DELETED.[Title]
			)
		) 
		END		
		
      If UPDATE([ExportInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'ExportInd',
      CONVERT(NVARCHAR(2000),DELETED.[ExportInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ExportInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[ExportInd] Is Null And
				DELETED.[ExportInd] Is Not Null
			) Or
			(
				INSERTED.[ExportInd] Is Not Null And
				DELETED.[ExportInd] Is Null
			) Or
			(
				INSERTED.[ExportInd] !=
				DELETED.[ExportInd]
			)
		) 
		END		
		
      If UPDATE([WorkPhone])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'WorkPhone',
      CONVERT(NVARCHAR(2000),DELETED.[WorkPhone],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WorkPhone],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[WorkPhone] Is Null And
				DELETED.[WorkPhone] Is Not Null
			) Or
			(
				INSERTED.[WorkPhone] Is Not Null And
				DELETED.[WorkPhone] Is Null
			) Or
			(
				INSERTED.[WorkPhone] !=
				DELETED.[WorkPhone]
			)
		) 
		END		
		
      If UPDATE([MobilePhone])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'MobilePhone',
      CONVERT(NVARCHAR(2000),DELETED.[MobilePhone],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MobilePhone],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[MobilePhone] Is Null And
				DELETED.[MobilePhone] Is Not Null
			) Or
			(
				INSERTED.[MobilePhone] Is Not Null And
				DELETED.[MobilePhone] Is Null
			) Or
			(
				INSERTED.[MobilePhone] !=
				DELETED.[MobilePhone]
			)
		) 
		END		
		
      If UPDATE([AvailableForCRM])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'AvailableForCRM',
      CONVERT(NVARCHAR(2000),DELETED.[AvailableForCRM],121),
      CONVERT(NVARCHAR(2000),INSERTED.[AvailableForCRM],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[AvailableForCRM] Is Null And
				DELETED.[AvailableForCRM] Is Not Null
			) Or
			(
				INSERTED.[AvailableForCRM] Is Not Null And
				DELETED.[AvailableForCRM] Is Null
			) Or
			(
				INSERTED.[AvailableForCRM] !=
				DELETED.[AvailableForCRM]
			)
		) 
		END		
		
      If UPDATE([ReadyForApproval])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'ReadyForApproval',
      CONVERT(NVARCHAR(2000),DELETED.[ReadyForApproval],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ReadyForApproval],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[ReadyForApproval] Is Null And
				DELETED.[ReadyForApproval] Is Not Null
			) Or
			(
				INSERTED.[ReadyForApproval] Is Not Null And
				DELETED.[ReadyForApproval] Is Null
			) Or
			(
				INSERTED.[ReadyForApproval] !=
				DELETED.[ReadyForApproval]
			)
		) 
		END		
		
      If UPDATE([PreferredName])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'PreferredName',
      CONVERT(NVARCHAR(2000),DELETED.[PreferredName],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PreferredName],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[PreferredName] Is Null And
				DELETED.[PreferredName] Is Not Null
			) Or
			(
				INSERTED.[PreferredName] Is Not Null And
				DELETED.[PreferredName] Is Null
			) Or
			(
				INSERTED.[PreferredName] !=
				DELETED.[PreferredName]
			)
		) 
		END		
		
      If UPDATE([HomePhoneFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'HomePhoneFormat',
      CONVERT(NVARCHAR(2000),DELETED.[HomePhoneFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[HomePhoneFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[HomePhoneFormat] Is Null And
				DELETED.[HomePhoneFormat] Is Not Null
			) Or
			(
				INSERTED.[HomePhoneFormat] Is Not Null And
				DELETED.[HomePhoneFormat] Is Null
			) Or
			(
				INSERTED.[HomePhoneFormat] !=
				DELETED.[HomePhoneFormat]
			)
		) 
		END		
		
      If UPDATE([FaxFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'FaxFormat',
      CONVERT(NVARCHAR(2000),DELETED.[FaxFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[FaxFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[FaxFormat] Is Null And
				DELETED.[FaxFormat] Is Not Null
			) Or
			(
				INSERTED.[FaxFormat] Is Not Null And
				DELETED.[FaxFormat] Is Null
			) Or
			(
				INSERTED.[FaxFormat] !=
				DELETED.[FaxFormat]
			)
		) 
		END		
		
      If UPDATE([WorkPhoneFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'WorkPhoneFormat',
      CONVERT(NVARCHAR(2000),DELETED.[WorkPhoneFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[WorkPhoneFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[WorkPhoneFormat] Is Null And
				DELETED.[WorkPhoneFormat] Is Not Null
			) Or
			(
				INSERTED.[WorkPhoneFormat] Is Not Null And
				DELETED.[WorkPhoneFormat] Is Null
			) Or
			(
				INSERTED.[WorkPhoneFormat] !=
				DELETED.[WorkPhoneFormat]
			)
		) 
		END		
		
      If UPDATE([MobilePhoneFormat])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'MobilePhoneFormat',
      CONVERT(NVARCHAR(2000),DELETED.[MobilePhoneFormat],121),
      CONVERT(NVARCHAR(2000),INSERTED.[MobilePhoneFormat],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[MobilePhoneFormat] Is Null And
				DELETED.[MobilePhoneFormat] Is Not Null
			) Or
			(
				INSERTED.[MobilePhoneFormat] Is Not Null And
				DELETED.[MobilePhoneFormat] Is Null
			) Or
			(
				INSERTED.[MobilePhoneFormat] !=
				DELETED.[MobilePhoneFormat]
			)
		) 
		END		
		
      If UPDATE([Language])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Language',
      CONVERT(NVARCHAR(2000),DELETED.[Language],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Language],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[Language] Is Null And
				DELETED.[Language] Is Not Null
			) Or
			(
				INSERTED.[Language] Is Not Null And
				DELETED.[Language] Is Null
			) Or
			(
				INSERTED.[Language] !=
				DELETED.[Language]
			)
		) 
		END		
		
      If UPDATE([TargetRatio])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'TargetRatio',
      CONVERT(NVARCHAR(2000),DELETED.[TargetRatio],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TargetRatio],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[TargetRatio] Is Null And
				DELETED.[TargetRatio] Is Not Null
			) Or
			(
				INSERTED.[TargetRatio] Is Not Null And
				DELETED.[TargetRatio] Is Null
			) Or
			(
				INSERTED.[TargetRatio] !=
				DELETED.[TargetRatio]
			)
		) 
		END		
		
      If UPDATE([UtilizationRatio])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'UtilizationRatio',
      CONVERT(NVARCHAR(2000),DELETED.[UtilizationRatio],121),
      CONVERT(NVARCHAR(2000),INSERTED.[UtilizationRatio],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[UtilizationRatio] Is Null And
				DELETED.[UtilizationRatio] Is Not Null
			) Or
			(
				INSERTED.[UtilizationRatio] Is Not Null And
				DELETED.[UtilizationRatio] Is Null
			) Or
			(
				INSERTED.[UtilizationRatio] !=
				DELETED.[UtilizationRatio]
			)
		) 
		END		
		
      If UPDATE([ConsultantInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'ConsultantInd',
      CONVERT(NVARCHAR(2000),DELETED.[ConsultantInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ConsultantInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[ConsultantInd] Is Null And
				DELETED.[ConsultantInd] Is Not Null
			) Or
			(
				INSERTED.[ConsultantInd] Is Not Null And
				DELETED.[ConsultantInd] Is Null
			) Or
			(
				INSERTED.[ConsultantInd] !=
				DELETED.[ConsultantInd]
			)
		) 
		END		
		
      If UPDATE([ClientVendorInd])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'ClientVendorInd',
      CONVERT(NVARCHAR(2000),DELETED.[ClientVendorInd],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClientVendorInd],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[ClientVendorInd] Is Null And
				DELETED.[ClientVendorInd] Is Not Null
			) Or
			(
				INSERTED.[ClientVendorInd] Is Not Null And
				DELETED.[ClientVendorInd] Is Null
			) Or
			(
				INSERTED.[ClientVendorInd] !=
				DELETED.[ClientVendorInd]
			)
		) 
		END		
		
      If UPDATE([ClientID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'ClientID',
      CONVERT(NVARCHAR(2000),DELETED.[ClientID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ClientID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[ClientID] Is Null And
				DELETED.[ClientID] Is Not Null
			) Or
			(
				INSERTED.[ClientID] Is Not Null And
				DELETED.[ClientID] Is Null
			) Or
			(
				INSERTED.[ClientID] !=
				DELETED.[ClientID]
			)
		) 
		END		
		
      If UPDATE([Vendor])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Vendor',
      CONVERT(NVARCHAR(2000),DELETED.[Vendor],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Vendor],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[Vendor] Is Null And
				DELETED.[Vendor] Is Not Null
			) Or
			(
				INSERTED.[Vendor] Is Not Null And
				DELETED.[Vendor] Is Null
			) Or
			(
				INSERTED.[Vendor] !=
				DELETED.[Vendor]
			)
		) 
		END		
		
      If UPDATE([TalentUserID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'TalentUserID',
      CONVERT(NVARCHAR(2000),DELETED.[TalentUserID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TalentUserID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[TalentUserID] Is Null And
				DELETED.[TalentUserID] Is Not Null
			) Or
			(
				INSERTED.[TalentUserID] Is Not Null And
				DELETED.[TalentUserID] Is Null
			) Or
			(
				INSERTED.[TalentUserID] !=
				DELETED.[TalentUserID]
			)
		) 
		END		
		
      If UPDATE([Location])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'Location',
      CONVERT(NVARCHAR(2000),DELETED.[Location],121),
      CONVERT(NVARCHAR(2000),INSERTED.[Location],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[Location] Is Null And
				DELETED.[Location] Is Not Null
			) Or
			(
				INSERTED.[Location] Is Not Null And
				DELETED.[Location] Is Null
			) Or
			(
				INSERTED.[Location] !=
				DELETED.[Location]
			)
		) 
		END		
		
      If UPDATE([TLInternalKey])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'TLInternalKey',
      CONVERT(NVARCHAR(2000),DELETED.[TLInternalKey],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TLInternalKey],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[TLInternalKey] Is Null And
				DELETED.[TLInternalKey] Is Not Null
			) Or
			(
				INSERTED.[TLInternalKey] Is Not Null And
				DELETED.[TLInternalKey] Is Null
			) Or
			(
				INSERTED.[TLInternalKey] !=
				DELETED.[TLInternalKey]
			)
		) 
		END		
		
      If UPDATE([ProfessionalSuffix])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'ProfessionalSuffix',
      CONVERT(NVARCHAR(2000),DELETED.[ProfessionalSuffix],121),
      CONVERT(NVARCHAR(2000),INSERTED.[ProfessionalSuffix],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[ProfessionalSuffix] Is Null And
				DELETED.[ProfessionalSuffix] Is Not Null
			) Or
			(
				INSERTED.[ProfessionalSuffix] Is Not Null And
				DELETED.[ProfessionalSuffix] Is Null
			) Or
			(
				INSERTED.[ProfessionalSuffix] !=
				DELETED.[ProfessionalSuffix]
			)
		) 
		END		
		
      If UPDATE([TalentModDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'TalentModDate',
      CONVERT(NVARCHAR(2000),DELETED.[TalentModDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TalentModDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[TalentModDate] Is Null And
				DELETED.[TalentModDate] Is Not Null
			) Or
			(
				INSERTED.[TalentModDate] Is Not Null And
				DELETED.[TalentModDate] Is Null
			) Or
			(
				INSERTED.[TalentModDate] !=
				DELETED.[TalentModDate]
			)
		) 
		END		
		
      If UPDATE([TLSyncModDate])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'TLSyncModDate',
      CONVERT(NVARCHAR(2000),DELETED.[TLSyncModDate],121),
      CONVERT(NVARCHAR(2000),INSERTED.[TLSyncModDate],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[TLSyncModDate] Is Null And
				DELETED.[TLSyncModDate] Is Not Null
			) Or
			(
				INSERTED.[TLSyncModDate] Is Not Null And
				DELETED.[TLSyncModDate] Is Null
			) Or
			(
				INSERTED.[TLSyncModDate] !=
				DELETED.[TLSyncModDate]
			)
		) 
		END		
		
      If UPDATE([KonaUsername])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'KonaUsername',
      CONVERT(NVARCHAR(2000),DELETED.[KonaUsername],121),
      CONVERT(NVARCHAR(2000),INSERTED.[KonaUsername],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[KonaUsername] Is Null And
				DELETED.[KonaUsername] Is Not Null
			) Or
			(
				INSERTED.[KonaUsername] Is Not Null And
				DELETED.[KonaUsername] Is Null
			) Or
			(
				INSERTED.[KonaUsername] !=
				DELETED.[KonaUsername]
			)
		) 
		END		
		
      If UPDATE([KonaAccessToken])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'KonaAccessToken',
      CONVERT(NVARCHAR(2000),DELETED.[KonaAccessToken],121),
      CONVERT(NVARCHAR(2000),INSERTED.[KonaAccessToken],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[KonaAccessToken] Is Null And
				DELETED.[KonaAccessToken] Is Not Null
			) Or
			(
				INSERTED.[KonaAccessToken] Is Not Null And
				DELETED.[KonaAccessToken] Is Null
			) Or
			(
				INSERTED.[KonaAccessToken] !=
				DELETED.[KonaAccessToken]
			)
		) 
		END		
		
      If UPDATE([KonaRefreshToken])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'KonaRefreshToken',
      CONVERT(NVARCHAR(2000),DELETED.[KonaRefreshToken],121),
      CONVERT(NVARCHAR(2000),INSERTED.[KonaRefreshToken],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[KonaRefreshToken] Is Null And
				DELETED.[KonaRefreshToken] Is Not Null
			) Or
			(
				INSERTED.[KonaRefreshToken] Is Not Null And
				DELETED.[KonaRefreshToken] Is Null
			) Or
			(
				INSERTED.[KonaRefreshToken] !=
				DELETED.[KonaRefreshToken]
			)
		) 
		END		
		
      If UPDATE([KonaUserID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'KonaUserID',
      CONVERT(NVARCHAR(2000),DELETED.[KonaUserID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[KonaUserID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[KonaUserID] Is Null And
				DELETED.[KonaUserID] Is Not Null
			) Or
			(
				INSERTED.[KonaUserID] Is Not Null And
				DELETED.[KonaUserID] Is Null
			) Or
			(
				INSERTED.[KonaUserID] !=
				DELETED.[KonaUserID]
			)
		) 
		END		
		
      If UPDATE([CitizenshipStatus])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'CitizenshipStatus',
      CONVERT(NVARCHAR(2000),DELETED.[CitizenshipStatus],121),
      CONVERT(NVARCHAR(2000),INSERTED.[CitizenshipStatus],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[CitizenshipStatus] Is Null And
				DELETED.[CitizenshipStatus] Is Not Null
			) Or
			(
				INSERTED.[CitizenshipStatus] Is Not Null And
				DELETED.[CitizenshipStatus] Is Null
			) Or
			(
				INSERTED.[CitizenshipStatus] !=
				DELETED.[CitizenshipStatus]
			)
		) 
		END		
		
      If UPDATE([QBOID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'QBOID',
      CONVERT(NVARCHAR(2000),DELETED.[QBOID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[QBOID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[QBOID] Is Null And
				DELETED.[QBOID] Is Not Null
			) Or
			(
				INSERTED.[QBOID] Is Not Null And
				DELETED.[QBOID] Is Null
			) Or
			(
				INSERTED.[QBOID] !=
				DELETED.[QBOID]
			)
		) 
		END		
		
      If UPDATE([QBOAddressID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'QBOAddressID',
      CONVERT(NVARCHAR(2000),DELETED.[QBOAddressID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[QBOAddressID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[QBOAddressID] Is Null And
				DELETED.[QBOAddressID] Is Not Null
			) Or
			(
				INSERTED.[QBOAddressID] Is Not Null And
				DELETED.[QBOAddressID] Is Null
			) Or
			(
				INSERTED.[QBOAddressID] !=
				DELETED.[QBOAddressID]
			)
		) 
		END		
		
      If UPDATE([QBOLastUpdated])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'QBOLastUpdated',
      CONVERT(NVARCHAR(2000),DELETED.[QBOLastUpdated],121),
      CONVERT(NVARCHAR(2000),INSERTED.[QBOLastUpdated],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[QBOLastUpdated] Is Null And
				DELETED.[QBOLastUpdated] Is Not Null
			) Or
			(
				INSERTED.[QBOLastUpdated] Is Not Null And
				DELETED.[QBOLastUpdated] Is Null
			) Or
			(
				INSERTED.[QBOLastUpdated] !=
				DELETED.[QBOLastUpdated]
			)
		) 
		END		
		
      If UPDATE([QBOVendorID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'QBOVendorID',
      CONVERT(NVARCHAR(2000),DELETED.[QBOVendorID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[QBOVendorID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[QBOVendorID] Is Null And
				DELETED.[QBOVendorID] Is Not Null
			) Or
			(
				INSERTED.[QBOVendorID] Is Not Null And
				DELETED.[QBOVendorID] Is Null
			) Or
			(
				INSERTED.[QBOVendorID] !=
				DELETED.[QBOVendorID]
			)
		) 
		END		
		
      If UPDATE([PIMID])
      BEGIN
      INSERT
      INTO AuditTrail
      (	ModUser,
      ModDate,
      TableName,
      ActionType,
      PrimaryKey,
      ColumnName,
      OldValue,
      NewValue,
      Source,
      Application
      )
      SELECT
      @VisionAuditUser, @now,@table,'UPDATE',CONVERT(NVARCHAR(255),INSERTED.[Employee],121),'PIMID',
      CONVERT(NVARCHAR(2000),DELETED.[PIMID],121),
      CONVERT(NVARCHAR(2000),INSERTED.[PIMID],121), @source,@app
      FROM INSERTED join DELETED on

    INSERTED.[Employee] = DELETED.[Employee] AND 
		(
			(
				INSERTED.[PIMID] Is Null And
				DELETED.[PIMID] Is Not Null
			) Or
			(
				INSERTED.[PIMID] Is Not Null And
				DELETED.[PIMID] Is Null
			) Or
			(
				INSERTED.[PIMID] !=
				DELETED.[PIMID]
			)
		) 
		END		
		
	SET NOCOUNT OFF 
	END
	
GO
DISABLE TRIGGER [dbo].[VisionAudit_Update_EMMain] ON [dbo].[EMMain]
GO
ALTER TABLE [dbo].[EMMain] ADD CONSTRAINT [EMMainPK] PRIMARY KEY NONCLUSTERED ([Employee]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [EMMainEmployeeIDX] ON [dbo].[EMMain] ([Employee]) INCLUDE ([LastName], [FirstName], [EMail], [Suffix], [Title], [WorkPhone], [PreferredName], [WorkPhoneFormat], [HomeCompany]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [EMMainQBOIDIDX] ON [dbo].[EMMain] ([QBOID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
