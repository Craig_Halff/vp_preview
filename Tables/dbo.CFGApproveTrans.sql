CREATE TABLE [dbo].[CFGApproveTrans]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransactionType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGApproveTrans] ADD CONSTRAINT [CFGApproveTransPK] PRIMARY KEY NONCLUSTERED ([Company], [TransactionType]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
