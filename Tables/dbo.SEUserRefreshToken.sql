CREATE TABLE [dbo].[SEUserRefreshToken]
(
[APIRefreshToken] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Username] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[APITicket] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateDate] [datetime] NOT NULL,
[APIRefreshTokenExpires] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SEUserRefreshToken] ADD CONSTRAINT [SEUserRefreshTokenPK] PRIMARY KEY CLUSTERED ([APIRefreshToken]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
