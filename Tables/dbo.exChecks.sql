CREATE TABLE [dbo].[exChecks]
(
[Period] [int] NOT NULL CONSTRAINT [DF__exChecks___Perio__226D29D7] DEFAULT ((0)),
[PostSeq] [int] NOT NULL CONSTRAINT [DF__exChecks___PostS__23614E10] DEFAULT ((0)),
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__exChecks_Ne__Seq__24557249] DEFAULT ((0)),
[Voucher] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Printed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__exChecks___Print__25499682] DEFAULT ('N'),
[Assigned] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__exChecks___Assig__263DBABB] DEFAULT ('N'),
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sequence] [int] NOT NULL CONSTRAINT [DF__exChecks___Seque__2731DEF4] DEFAULT ((0)),
[CheckNo] [bigint] NOT NULL CONSTRAINT [DF__exChecks___Check__2826032D] DEFAULT ((0)),
[CheckDate] [datetime] NULL,
[ReportName] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportDate] [datetime] NULL,
[AppliedAdvance] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__exChecks___Appli__291A2766] DEFAULT ((0)),
[Org] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DetailType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__exChecks___Amoun__2A0E4B9F] DEFAULT ((0)),
[CheckAmt] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__exChecks___Check__2B026FD8] DEFAULT ((0)),
[EFT] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailRemittance] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExportText] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__exChecks___Expor__2BF69411] DEFAULT ('N'),
[CheckNoRef] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SEPAIndicator] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EXPeriod] [int] NOT NULL CONSTRAINT [DF__exChecks___EXPer__2CEAB84A] DEFAULT ((0)),
[EXPostSeq] [int] NOT NULL CONSTRAINT [DF__exChecks___EXPos__2DDEDC83] DEFAULT ((0)),
[EXPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Diary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[exChecks] ADD CONSTRAINT [exChecksPK] PRIMARY KEY NONCLUSTERED ([Period], [PostSeq], [Employee], [Seq]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
