CREATE TABLE [dbo].[ReceiveDocumentsDetail]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DetailPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[POPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReceiveDocumentsDetail] ADD CONSTRAINT [ReceiveDocumentsDetailPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [DetailPKey], [POPKey], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
