CREATE TABLE [dbo].[FW_CFGEnabledLanguages]
(
[Language] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrimaryLanguage] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__FW_CFGEna__Prima__7B55BEBB] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_CFGEnabledLanguages] ADD CONSTRAINT [FW_CFGEnabledLanguagesPK] PRIMARY KEY CLUSTERED ([Language]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
