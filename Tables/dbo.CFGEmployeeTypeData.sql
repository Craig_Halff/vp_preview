CREATE TABLE [dbo].[CFGEmployeeTypeData]
(
[Type] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SortOrder] [smallint] NOT NULL CONSTRAINT [DF__CFGEmploy__SortO__270FB757] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGEmployeeTypeData] ADD CONSTRAINT [CFGEmployeeTypeDataPK] PRIMARY KEY CLUSTERED ([Type]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
