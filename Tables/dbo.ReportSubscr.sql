CREATE TABLE [dbo].[ReportSubscr]
(
[PartKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DashUserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportType] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportSubscr] ADD CONSTRAINT [ReportSubscrPK] PRIMARY KEY NONCLUSTERED ([PartKey], [DashUserName], [UserName], [Name], [ReportType]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
