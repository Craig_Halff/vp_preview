CREATE TABLE [dbo].[CFGOppEstClause_Backup]
(
[Code] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Clause] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGOppEstClause_Backup] ADD CONSTRAINT [CFGOppEstClausePK] PRIMARY KEY CLUSTERED ([Code]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
