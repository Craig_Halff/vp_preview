CREATE TABLE [dbo].[RPAssignment]
(
[AssignmentID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PlanID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TaskID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LaborCode] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Category] [smallint] NOT NULL CONSTRAINT [DF__RPAssignm__Categ__7EEFAEC7] DEFAULT ((0)),
[ResourceID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActivityID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CostRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__CostR__7FE3D300] DEFAULT ((0)),
[BillingRate] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__Billi__00D7F739] DEFAULT ((0)),
[PctCompleteLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__PctCo__01CC1B72] DEFAULT ((0)),
[PctCompleteLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__PctCo__02C03FAB] DEFAULT ((0)),
[BaselineLaborHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__Basel__03B463E4] DEFAULT ((0)),
[BaselineLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__Basel__04A8881D] DEFAULT ((0)),
[BaselineLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__Basel__059CAC56] DEFAULT ((0)),
[BaselineRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__Basel__0690D08F] DEFAULT ((0)),
[BaselineStart] [datetime] NULL,
[BaselineFinish] [datetime] NULL,
[PlannedLaborHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__Plann__0784F4C8] DEFAULT ((0)),
[PlannedLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__Plann__08791901] DEFAULT ((0)),
[PlannedLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__Plann__096D3D3A] DEFAULT ((0)),
[LabRevenue] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__LabRe__0A616173] DEFAULT ((0)),
[WeightedLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__Weigh__0B5585AC] DEFAULT ((0)),
[WeightedLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__Weigh__0C49A9E5] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[SortSeq] [smallint] NOT NULL CONSTRAINT [DF__RPAssignm__SortS__0D3DCE1E] DEFAULT ((0)),
[LabParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPAssignm__LabPa__0E31F257] DEFAULT ('N'),
[ExpParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPAssignm__ExpPa__0F261690] DEFAULT ('N'),
[ConParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPAssignm__ConPa__101A3AC9] DEFAULT ('N'),
[UntParentState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPAssignm__UntPa__110E5F02] DEFAULT ('N'),
[GRLBCD] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPAssignm__ConVS__1202833B] DEFAULT ('N'),
[ExpVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPAssignm__ExpVS__12F6A774] DEFAULT ('N'),
[LabVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPAssignm__LabVS__13EACBAD] DEFAULT ('Y'),
[UntVState] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPAssignm__UntVS__14DEEFE6] DEFAULT ('N'),
[JTDLabHrs] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__JTDLa__15D3141F] DEFAULT ((0)),
[JTDLabCost] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__JTDLa__16C73858] DEFAULT ((0)),
[JTDLabBill] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__RPAssignm__JTDLa__17BB5C91] DEFAULT ((0)),
[GenericResourceID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HardBooked] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RPAssignm__HardB__18AF80CA] DEFAULT ('N'),
[CreateUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RPAssignment] ADD CONSTRAINT [RPAssignmentPK] PRIMARY KEY NONCLUSTERED ([AssignmentID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [RPAssignmentPlanIDIDX] ON [dbo].[RPAssignment] ([PlanID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPAssignmentAllIDX] ON [dbo].[RPAssignment] ([PlanID], [TaskID], [AssignmentID], [WBS1], [WBS2], [WBS3], [LaborCode], [Category], [ResourceID], [GenericResourceID], [GRLBCD], [StartDate], [EndDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RPAssignmentTaskIDIDX] ON [dbo].[RPAssignment] ([TaskID]) ON [PRIMARY]
GO
