CREATE TABLE [dbo].[CCG_PAT_ConfigRoutesDescriptions]
(
[Route] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RouteLabel] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RouteDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_ConfigRoutesDescriptions] ADD CONSTRAINT [CCG_PAT_ConfigRoutesDescriptionsPK] PRIMARY KEY NONCLUSTERED ([Route], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Contains basic descriptions for routes as used for multi-language companies', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoutesDescriptions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'The name/key of the route', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoutesDescriptions', 'COLUMN', N'Route'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The language-specific route description. Future use.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoutesDescriptions', 'COLUMN', N'RouteDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The language-specific display label of the route', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoutesDescriptions', 'COLUMN', N'RouteLabel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The UI culture code of this description', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigRoutesDescriptions', 'COLUMN', N'UICultureName'
GO
