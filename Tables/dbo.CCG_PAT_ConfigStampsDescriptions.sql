CREATE TABLE [dbo].[CCG_PAT_ConfigStampsDescriptions]
(
[Seq] [int] NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Content] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_PAT_ConfigStampsDescriptions] ADD CONSTRAINT [CCG_PAT_ConfigStampsDescriptionsPK] PRIMARY KEY NONCLUSTERED ([Seq], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Descriptions for stamps as used for multi-language companies', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStampsDescriptions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The language-specific content of the stamp.  The verbiage of the text of the stamp or the UNC path to the image. Placeholders allowed.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStampsDescriptions', 'COLUMN', N'Content'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The language-specific label of this stamp', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStampsDescriptions', 'COLUMN', N'Label'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key id', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStampsDescriptions', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The UI culture code of this description', 'SCHEMA', N'dbo', 'TABLE', N'CCG_PAT_ConfigStampsDescriptions', 'COLUMN', N'UICultureName'
GO
