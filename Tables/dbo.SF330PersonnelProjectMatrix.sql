CREATE TABLE [dbo].[SF330PersonnelProjectMatrix]
(
[SF330ID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmpID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProjID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmpSeq] [smallint] NOT NULL CONSTRAINT [DF__SF330Pers__EmpSe__5B274F9D] DEFAULT ((0)),
[ProjSeq] [smallint] NOT NULL CONSTRAINT [DF__SF330Pers__ProjS__5C1B73D6] DEFAULT ((0)),
[ProjKey] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Edited] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SF330Pers__Edite__5D0F980F] DEFAULT ('N'),
[CreateUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SF330PersonnelProjectMatrix] ADD CONSTRAINT [SF330PersonnelProjectMatrixPK] PRIMARY KEY NONCLUSTERED ([SF330ID], [EmpID], [ProjID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
