CREATE TABLE [dbo].[tsDetailCustomFields]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Employee] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tsDetailCustomFields] ADD CONSTRAINT [tsDetailCustomFieldsPK] PRIMARY KEY NONCLUSTERED ([Batch], [Employee], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
