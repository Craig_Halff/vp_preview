CREATE TABLE [dbo].[FW_EmailLog]
(
[LogID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LogUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LogDate] [datetime] NOT NULL,
[FromEmail] [nvarchar] (320) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Subject] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToEmails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCEmails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BCCEmails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumberAttachments] [int] NOT NULL CONSTRAINT [DF__FW_EmailL__Numbe__72D652AA] DEFAULT ((0)),
[EmailSize] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__FW_EmailL__Email__18FBFB92] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FW_EmailLog] ADD CONSTRAINT [FW_EmailLogPK] PRIMARY KEY CLUSTERED ([LogID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
