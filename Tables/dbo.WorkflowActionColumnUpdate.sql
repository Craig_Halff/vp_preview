CREATE TABLE [dbo].[WorkflowActionColumnUpdate]
(
[ActionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TableName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColumnName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NewValue] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewValueDescription] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClearField] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__WorkflowA__Clear__038A46B2] DEFAULT ('N'),
[SQLExpression] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SQLIfExpression] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SQLElseExpression] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CascadeWBSChanges] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__WorkflowA__Casca__047E6AEB] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowActionColumnUpdate] ADD CONSTRAINT [WorkflowActionColumnUpdatePK] PRIMARY KEY NONCLUSTERED ([ActionID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
