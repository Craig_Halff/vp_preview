CREATE TABLE [dbo].[JobTitles]
(
[Title] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[JobTitles] ADD CONSTRAINT [JobTitlesPK] PRIMARY KEY CLUSTERED ([Title]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
