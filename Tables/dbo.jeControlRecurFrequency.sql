CREATE TABLE [dbo].[jeControlRecurFrequency]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__jeControlRe__Seq__6F979147] DEFAULT ((0)),
[Date] [datetime] NULL,
[Processed] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__jeControl__Proce__708BB580] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[jeControlRecurFrequency] ADD CONSTRAINT [jeControlRecurFrequencyPK] PRIMARY KEY CLUSTERED ([Batch], [Seq]) ON [PRIMARY]
GO
