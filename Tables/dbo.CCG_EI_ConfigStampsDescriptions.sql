CREATE TABLE [dbo].[CCG_EI_ConfigStampsDescriptions]
(
[Seq] [int] NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Content] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Width] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CCG_EI_ConfigStampsDescriptions] ADD CONSTRAINT [PK_CCG_EI_ConfigStampsDescriptions] PRIMARY KEY CLUSTERED ([Seq], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Contains descriptions for Stamps as used for multi-language companies', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStampsDescriptions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The text to use for the stamp if specific to this culture code', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStampsDescriptions', 'COLUMN', N'Content'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*The stamp label to display for this culture code', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStampsDescriptions', 'COLUMN', N'Label'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stamp key id (FK)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStampsDescriptions', 'COLUMN', N'Seq'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The UI culture code of this label', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStampsDescriptions', 'COLUMN', N'UICultureName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The width of the stamp as a percent of the whole document (1 - 100)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_ConfigStampsDescriptions', 'COLUMN', N'Width'
GO
