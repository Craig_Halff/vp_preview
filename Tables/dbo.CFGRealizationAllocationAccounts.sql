CREATE TABLE [dbo].[CFGRealizationAllocationAccounts]
(
[Company] [nvarchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGRealizationAllocationAccounts] ADD CONSTRAINT [CFGRealizationAllocationAccountsPK] PRIMARY KEY CLUSTERED ([Company], [Account]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
