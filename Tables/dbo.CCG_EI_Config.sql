CREATE TABLE [dbo].[CCG_EI_Config]
(
[Version] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Series] [int] NOT NULL,
[ActivationPassword] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LogLevel] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LogFile] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InvoiceNumberLength] [int] NOT NULL,
[ProjectNameLength] [int] NOT NULL,
[DraftInvoiceWildcardString] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UnbilledDetailWildcardString] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FinalInvoiceWildcardString] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcctRoles] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ROAcctRoles] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupportMessages] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConfigEmployees] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NotificationInterval] [tinyint] NOT NULL,
[SupportPeriodBillThru] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupportDateBillThru] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterPDF] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterSQL] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterPDFDays] [int] NULL,
[SubgridMode] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdvanceOnStageChange] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShowResetVisible] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FreezeColumns] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterBillGroup] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HideDormant] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupportBuiltInFeePctCpl] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowFeePctCplReduction] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaveFeePctCplImmediately] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SyncInvoiceStageToVision] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailLink] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [EmailLinkDefault] DEFAULT (''),
[EnforceWindowsUsernameInLink] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EncTimeout] [int] NOT NULL,
[SupportPackaging] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocsEnabled] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocsPackaging] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocsStorage] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdvancedOptions] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PctCompleteEnableUnitsColumns] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowFeeToExceed100Pct] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GridMultiProjectSelectMode] [bit] NULL,
[MainGridFiltering] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShowUnbilledOnlyInCEMenus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Delegation] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_EI_Co__Deleg__4510894A] DEFAULT ('N'),
[ParallelApproval] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_EI_Co__Paral__4604AD83] DEFAULT ('N'),
[EmployeeSelfDelegation] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_EI_Co__Emplo__46F8D1BC] DEFAULT ('N'),
[DelegationApproval] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_EI_Co__Deleg__47ECF5F5] DEFAULT ('N'),
[DelegationBySupervisor] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_EI_Co__Deleg__48E11A2E] DEFAULT ('N'),
[DelegationEmployeesList] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DelegationDualAccess] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_EI_Co__Deleg__49D53E67] DEFAULT ('N'),
[DocsDefaultExtn] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyCCParams] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__CCG_EI_Co__Verif__177A3684] DEFAULT ('Y')
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'General configuration settings for the entire EI application', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'*A semicolon separated list of Vision security roles that define the users that function as “accounting users”', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'AcctRoles'
GO
EXEC sp_addextendedproperty N'MS_Description', N'EI application activation password - provided by EleVia', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'ActivationPassword'
GO
EXEC sp_addextendedproperty N'MS_Description', N'One or more name value pairs of advanced configuration options', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'AdvancedOptions'
GO
EXEC sp_addextendedproperty N'MS_Description', N'When enabled the currently selected project will advnaced to the next row after a stage change', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'AdvanceOnStageChange'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Allow reductions in built in Fee Percent Complete interface', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'AllowFeePctCplReduction'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_Config / AllowFeeToExceed100Pct Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'AllowFeeToExceed100Pct'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*A semicolon separated list of employee numbers designating who will have access to the Configuration menu', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'ConfigEmployees'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Is delegation enabled? Y / N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'Delegation'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Do delegations need to be approved? Y / N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'DelegationApproval'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Can supervisors approve delegation requests for their employees? Y / N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'DelegationBySupervisor'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Is dual access allowed for delegations? Y / N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'DelegationDualAccess'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Custom CSV list of employees who can do approvals for delegations', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'DelegationEmployeesList'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_Config / DocsEnabled Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'DocsEnabled'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_Config / DocsPackaging Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'DocsPackaging'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_Config / DocsStorage Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'DocsStorage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The wildcard string to use to find matches of draft invoice filenames', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'DraftInvoiceWildcardString'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*Email setup - HTML code for optional link in email notifications', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'EmailLink'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Can employees request/make delegations for themselves? Y / N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'EmployeeSelfDelegation'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of days an encoded email link is valid', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'EncTimeout'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_Config / EnforceWindowsUsernameInLink Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'EnforceWindowsUsernameInLink'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_Config / FilterBillGroup Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'FilterBillGroup'
GO
EXEC sp_addextendedproperty N'MS_Description', N'When ''Y'', then EI checkes to see each projects has a file newer than ''FilterPDFDays'' old.  Can significantly slow the grid load times for large lists or when slower file I\O.', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'FilterPDF'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of days, used with ''FilterPDF''', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'FilterPDFDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'When ''Y'', then EI utilizes the SQL table function spCCG_EI_ProjectFilter to determine which rows to show in main EI grid', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'FilterSQL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The wildcard string to use to find matches of final invoice filenames', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'FinalInvoiceWildcardString'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_Config / FreezeColumns Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'FreezeColumns'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_Config / GridMultiProjectSelectMode Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'GridMultiProjectSelectMode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Determines whether or not dormant projects are visible in main grid (Y / N)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'HideDormant'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The length of the invoice numbers to be displayed in the application', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'InvoiceNumberLength'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The full path and name of the EI log file', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'LogFile'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The log level to use while logging (OFF / ERROR / WARNING / INFO / DEBUG)', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'LogLevel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_Config / MainGridFiltering Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'MainGridFiltering'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The number of days between re-notifications being sent to approvers/reviewers to remind them they still have invoices to review or approve', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'NotificationInterval'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Is parallel approval enabled? Y / N', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'ParallelApproval'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Enables unit fields in built in Fee Percent Complete interface', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'PctCompleteEnableUnitsColumns'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The number of characters to be displayed from the project’s long name', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'ProjectNameLength'
GO
EXEC sp_addextendedproperty N'MS_Description', N'*<CCG_EI_Config / ROAcctRoles Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'ROAcctRoles'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Immediately save updates from reviewers or let accounting approve first when using built in Fee Percent Complete interface', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'SaveFeePctCplImmediately'
GO
EXEC sp_addextendedproperty N'MS_Description', N'EI series number', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'Series'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_Config / ShowResetVisible Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'ShowResetVisible'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_Config / ShowUnbilledOnlyInCEMenus Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'ShowUnbilledOnlyInCEMenus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'What type of data to show when expanding the project ', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'SubgridMode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Enables the built in Fee Percent Complete interface to update related Vision billing terms data directly from EI', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'SupportBuiltInFeePctCpl'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Enables the Thru Date field above the main grid', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'SupportDateBillThru'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Enable the messaging option in EI for project specific messages', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'SupportMessages'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Enables the Packaging interface for EI', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'SupportPackaging'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Enables the Thru Period field above the main grid', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'SupportPeriodBillThru'
GO
EXEC sp_addextendedproperty N'MS_Description', N'<CCG_EI_Config / SyncInvoiceStageToVision Description>', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'SyncInvoiceStageToVision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The wildcard string to use to find matches of unbilled detail filenames', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'UnbilledDetailWildcardString'
GO
EXEC sp_addextendedproperty N'MS_Description', N'EI version number for the database', 'SCHEMA', N'dbo', 'TABLE', N'CCG_EI_Config', 'COLUMN', N'Version'
GO
