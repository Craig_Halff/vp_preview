CREATE TABLE [dbo].[CFGClientRelationshipDescriptions]
(
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UICultureName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [smallint] NOT NULL CONSTRAINT [DF__CFGClientRe__Seq__5C19DD28] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGClientRelationshipDescriptions] ADD CONSTRAINT [CFGClientRelationshipDescriptionsPK] PRIMARY KEY CLUSTERED ([Code], [UICultureName]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
