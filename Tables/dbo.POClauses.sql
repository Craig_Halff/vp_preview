CREATE TABLE [dbo].[POClauses]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__POClauses_N__Seq__18D99DFC] DEFAULT ((0)),
[Type] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Clause] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POClauses] ADD CONSTRAINT [POClausesPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
