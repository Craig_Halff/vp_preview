CREATE TABLE [dbo].[WorkflowColumnCache]
(
[WorkflowColumnsCache_UID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ApplicationType] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApplicationName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastRefresh] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkflowColumnCache] ADD CONSTRAINT [WorkflowColumnCachePK] PRIMARY KEY NONCLUSTERED ([WorkflowColumnsCache_UID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
