CREATE TABLE [dbo].[apMaster]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InvoiceDate] [datetime] NULL,
[Invoice] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransDate] [datetime] NULL,
[LiabCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayTerms] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayDate] [datetime] NULL,
[Address] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Posted] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__apMaster___Poste__7C7BBFC2] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__apMaster_Ne__Seq__7D6FE3FB] DEFAULT ((0)),
[Voucher] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyExchangeOverrideMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__apMaster___Curre__7E640834] DEFAULT ('N'),
[CurrencyExchangeOverrideDate] [datetime] NULL,
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__apMaster___Curre__7F582C6D] DEFAULT ((0)),
[BarCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentExchangeOverrideMethod] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__apMaster___Payme__004C50A6] DEFAULT ('N'),
[PaymentExchangeOverrideDate] [datetime] NULL,
[PaymentExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__apMaster___Payme__014074DF] DEFAULT ((0)),
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__apMaster___Statu__02349918] DEFAULT ('N'),
[AuthorizedBy] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RejectReason] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentCurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Diary] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaryNo] [int] NOT NULL CONSTRAINT [DF__apMaster___Diary__0328BD51] DEFAULT ((0)),
[ModUser] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[AllowAssetEntries] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__apMaster__AllowA__73FEB5B6] DEFAULT ('N')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[apMaster] ADD CONSTRAINT [apMasterPK] PRIMARY KEY NONCLUSTERED ([Batch], [MasterPKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [apMasterBankCodeIDX] ON [dbo].[apMaster] ([BankCode]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [apMasterLiabCodeIDX] ON [dbo].[apMaster] ([LiabCode]) ON [PRIMARY]
GO
