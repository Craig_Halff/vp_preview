CREATE TABLE [dbo].[exDetail]
(
[Batch] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKey] [nvarchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__exDetail_Ne__Seq__4E4BAC15] DEFAULT ((0)),
[TransDate] [datetime] NULL,
[WBS1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WBS3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account] [nvarchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__exDetail___Amoun__4F3FD04E] DEFAULT ((0)),
[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuppressBill] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__exDetail___Suppr__5033F487] DEFAULT ('N'),
[NetAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__exDetail___NetAm__512818C0] DEFAULT ((0)),
[PaymentExchangeRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__exDetail___Payme__521C3CF9] DEFAULT ((0)),
[PaymentAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF__exDetail___Payme__53106132] DEFAULT ((0)),
[PaymentExchangeInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyExchangeOverrideRate] [decimal] (19, 10) NOT NULL CONSTRAINT [DF__exDetail___Curre__5404856B] DEFAULT ((0)),
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginatingVendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[exDetail] ADD CONSTRAINT [exDetailPK] PRIMARY KEY NONCLUSTERED ([Batch], [MasterPKey], [PKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
