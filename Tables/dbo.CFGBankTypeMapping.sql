CREATE TABLE [dbo].[CFGBankTypeMapping]
(
[RecordID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seq] [int] NOT NULL CONSTRAINT [DF__CFGBankType__Seq__742670E3] DEFAULT ((0)),
[BankValue] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VisionField] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGBankTypeMapping] ADD CONSTRAINT [CFGBankTypeMappingPK] PRIMARY KEY CLUSTERED ([RecordID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
