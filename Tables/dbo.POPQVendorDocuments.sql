CREATE TABLE [dbo].[POPQVendorDocuments]
(
[MasterPKey] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Vendor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileID] [uniqueidentifier] NOT NULL,
[AssociateNew] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__POPQVendo__Assoc__755B5795] DEFAULT ('N'),
[Seq] [int] NOT NULL CONSTRAINT [DF__POPQVendorD__Seq__764F7BCE] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POPQVendorDocuments] ADD CONSTRAINT [POPQVendorDocumentsPK] PRIMARY KEY NONCLUSTERED ([MasterPKey], [Vendor], [FileID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
