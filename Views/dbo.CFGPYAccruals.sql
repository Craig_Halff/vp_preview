SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGPYAccruals]
as
SELECT 	dbo.CFGPYAccrualsData.Code,
	dbo.CFGPYAccrualsDescriptions.Description,
	dbo.CFGPYAccrualsData.HasCarryoverLimit,
	dbo.CFGPYAccrualsData.CarryoverLimit,
	dbo.CFGPYAccrualsData.Maximum,
	dbo.CFGPYAccrualsData.PreAccrue,
	dbo.CFGPYAccrualsData.PrintOnCheck,
	dbo.CFGPYAccrualsData.Company,
	dbo.CFGPYAccrualsData.ShowOnTK,
	dbo.CFGPYAccrualsData.ScheduleID,
	dbo.CFGPYAccrualsData.CheckBenefitHours,
	dbo.CFGPYAccrualsData.EnableApprovalWorkflow,
	dbo.CFGPYAccrualsData.ApprovalWorkflow,
	dbo.CFGPYAccrualsData.WBS1ExcludeWhere,
	dbo.CFGPYAccrualsData.WBS1ExcludeSearch
FROM   dbo.CFGPYAccrualsData
 Left JOIN dbo.CFGPYAccrualsDescriptions ON dbo.CFGPYAccrualsData.Code = dbo.CFGPYAccrualsDescriptions.Code
and   dbo.CFGPYAccrualsData.Company = dbo.CFGPYAccrualsDescriptions.Company
and   dbo.CFGPYAccrualsDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGPYAccrualsTrigger] 
   ON  [dbo].[CFGPYAccruals]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)
DECLARE @Company Nvarchar(14)

SELECT top 1 @Code = Code, @Company = Company FROM Deleted

Delete from CFGPYAccrualsDescriptions where CFGPYAccrualsDescriptions.code = @Code and 
            CFGPYAccrualsDescriptions.Company = @Company
Delete from CFGPYAccrualsData where CFGPYAccrualsData.code = @Code and CFGPYAccrualsData.Company = @Company

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGPYAccrualsTrigger] 
   ON  [dbo].[CFGPYAccruals]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGPYAccrualsData]
  (	Code,
	HasCarryoverLimit,
	CarryoverLimit,
	Maximum,
	PreAccrue,
	PrintOnCheck,
	Company,
	ShowOnTK,
	ScheduleID,
	CheckBenefitHours,
	EnableApprovalWorkflow,
	ApprovalWorkflow,
	WBS1ExcludeWhere,
	WBS1ExcludeSearch)
 Select Code,
	isnull(HasCarryoverLimit,'N'),
	isnull(CarryoverLimit,0),
	isnull(Maximum,0),
	isnull(PreAccrue,'N'),
	isnull(PrintOnCheck,'N'),
	Company,
	isnull(ShowOnTK,'N'),
	ScheduleID,
	isnull(CheckBenefitHours,'N'),
	isnull(EnableApprovalWorkflow,'N'),
	ApprovalWorkflow,
	WBS1ExcludeWhere,
	WBS1ExcludeSearch
   from inserted

INSERT INTO [CFGPYAccrualsDescriptions]
  (Code, Company, Description, UICultureName)
Select Code, Company, Description, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGPYAccrualsTrigger] 
   ON  [dbo].[CFGPYAccruals]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(14)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

	
Update CFGPYAccrualsData
  set 	Code = i.code,
	HasCarryoverLimit = i.HasCarryOverLimit,
	CarryoverLimit = i.CarryOverLimit,
	Maximum = i.Maximum,
	PreAccrue = i.PreAccrue,
	PrintOnCheck = i.PrintOnCheck,
	Company = i.Company,
	ShowOnTK= i.ShowOnTk,
	ScheduleID = i.ScheduleID,
	CheckBenefitHours= i.CheckBenefitHours,
	EnableApprovalWorkflow= i.EnableApprovalWorkflow,
	ApprovalWorkflow= i.ApprovalWorkflow,
	WBS1ExcludeWhere= i.WBS1ExcludeWhere,
	WBS1ExcludeSearch= i.WBS1ExcludeSearch
 from inserted i
  inner join CFGPYAccrualsData on CFGPYAccrualsData.Company = i.Company and CFGPYAccrualsData.code = @Code

if (not exists(Select CFGPYAccrualsDescriptions.UICultureName from CFGPYAccrualsDescriptions, Inserted I
  where CFGPYAccrualsDescriptions.Company = i.Company and CFGPYAccrualsDescriptions.code = @code and CFGPYAccrualsDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGPYAccrualsDescriptions
      (Code,
	   Company,
       UICultureName,
       Description)
  Select Code, Company, dbo.FW_GetActiveCultureName(), Description From Inserted 

Else
  Update CFGPYAccrualsDescriptions
      Set code = i.code,
          Company = i.Company,
         Description = i.Description
  From inserted i
  Inner Join CFGPYAccrualsDescriptions on CFGPYAccrualsDescriptions.Company = i.Company and CFGPYAccrualsDescriptions.code = @code and CFGPYAccrualsDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
