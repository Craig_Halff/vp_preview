SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FW_CustomColumnValues]
AS
SELECT DISTINCT 
	dbo.FW_CustomColumnValuesData.InfocenterArea, 
	dbo.FW_CustomColumnValuesData.GridID, 
	dbo.FW_CustomColumnValuesData.ColName, 
	cd.DataValue, 
	cd.UICultureName, 
	cd.Seq, 
	dbo.FW_CustomColumnValuesData.Code, 
	cd.CreateUser, 
	cd.CreateDate, 
	cd.ModUser, 
	cd.ModDate
  FROM  dbo.FW_CustomColumnValuesData LEFT OUTER JOIN
	dbo.FW_CustomColumnValuesData AS cd ON dbo.FW_CustomColumnValuesData.InfocenterArea = cd.InfocenterArea AND 
	dbo.FW_CustomColumnValuesData.GridID = cd.GridID AND dbo.FW_CustomColumnValuesData.ColName = cd.ColName AND 
	dbo.FW_CustomColumnValuesData.Code = cd.Code AND cd.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DelFW_CustomColumnValues] 
  ON  [dbo].[FW_CustomColumnValues]
INSTEAD OF DELETE
AS 
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
DELETE FROM FW_CustomColumnValuesData from deleted WHERE
 FW_CustomColumnValuesData.InfocenterArea = deleted.InfocenterArea AND 
 FW_CustomColumnValuesData.GridID = deleted.GridID AND  
 FW_CustomColumnValuesData.ColName = deleted.ColName and
 FW_CustomColumnValuesData.Code = deleted.Code
 
END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[InsFW_CustomColumnValues] 
  ON  [dbo].[FW_CustomColumnValues]
INSTEAD OF Insert
AS
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
  
DECLARE @GridID nvarchar(250)
DECLARE @ColName nvarchar(250)
declare @Code nvarchar(100)
 
SELECT @GridID =GridID FROM inserted
SELECT @ColName =ColName FROM inserted
SELECT @Code =Code FROM inserted


IF (not exists (SELECT FW_CustomColumnValuesData.UICultureName FROM FW_CustomColumnValuesData, Inserted I
           WHERE  FW_CustomColumnValuesData.InfocenterArea = i.InfocenterArea 
			and FW_CustomColumnValuesData.gridId = @gridId
			and FW_CustomColumnValuesData.ColName = @ColName 
			and FW_CustomColumnValuesData.Code = @Code 
			and FW_CustomColumnValuesData.UICultureName =dbo.FW_GetActiveCultureName()))
       Insert into FW_CustomColumnValuesData 
	(InfocenterArea, GridID, ColName, DataValue, UICultureName, Seq, Code, CreateUser, CreateDate, ModUser, ModDate)
       SELECT 
	 InfocenterArea, GridID, colName, Datavalue, DBO.FW_GetActiveCultureName(), Seq, Code, dbo.FW_GetUserName(), GetUTCDate(),dbo.FW_GetUserName(), GetUTCDate()  FROM inserted
 
ELSE
     UPDATE FW_CustomColumnValuesData
           SET ColName = i.ColName, 
		GridID = i.GridID,
		DataValue = i.Datavalue,
		Seq = isnull(i.Seq,0),
		Code= i.Code,
		ModUser = dbo.FW_GetUserName(),
		ModDate = GetUTCDate()
           FROM inserted i
           INNER JOIN FW_CustomColumnValuesData ON FW_CustomColumnValuesData.InfocenterArea = i.InfocenterArea 
			and FW_CustomColumnValuesData.gridID = @GridID
			and FW_CustomColumnValuesData.colName = @colName 
			and FW_CustomColumnValuesData.Code = @Code 
			and FW_CustomColumnValuesData.UICultureName = dbo.FW_GetActiveCultureName()
			
 
 
 
END

 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[UpdFW_CustomColumnValues] 
  ON  [dbo].[FW_CustomColumnValues]
INSTEAD OF UPDATE
AS 
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
DECLARE @GridID nvarchar(250)
DECLARE @ColName nvarchar(250)
declare @Code nvarchar(100)
IF (UPDATE(GridID))
	SELECT @GridID =GridID FROM deleted
ELSE
	SELECT @GridID =GridID FROM inserted

IF (UPDATE(ColName))
	SELECT @ColName =ColName FROM deleted
ELSE
	SELECT @ColName =ColName FROM inserted

IF (UPDATE(Code))
	SELECT @Code =Code FROM deleted
ELSE
	SELECT @Code =Code FROM inserted



IF (not exists (SELECT FW_CustomColumnValuesData.UICultureName FROM FW_CustomColumnValuesData, Inserted I
           WHERE  FW_CustomColumnValuesData.InfocenterArea = i.InfocenterArea 
			and FW_CustomColumnValuesData.gridId = @gridId
			and FW_CustomColumnValuesData.ColName = @ColName 
			and FW_CustomColumnValuesData.Code = @Code 
			and FW_CustomColumnValuesData.UICultureName =dbo.FW_GetActiveCultureName()))
        Insert into FW_CustomColumnValuesData 
	  (InfocenterArea,gridId, colName,datavalue, UICultureName,seq, code, CreateUser, CreateDate, ModUser, ModDate)
        SELECT 
	   InfocenterArea,gridId, colName, Datavalue,DBO.FW_GetActiveCultureName(),seq,code, dbo.FW_GetUserName(), GetUTCDate(),dbo.FW_GetUserName(), GetUTCDate()  FROM inserted

ELSE
     UPDATE FW_CustomColumnValuesData
           SET ColName = i.ColName, 
		       GridID = i.GridID,
			   DataValue = i.Datavalue,
			   Seq = isnull(i.Seq,0),
			   Code = i.code,
 			   ModUser = dbo.FW_GetUserName(),
		   	   ModDate = GetUTCDate()
           FROM inserted i
           INNER JOIN FW_CustomColumnValuesData ON FW_CustomColumnValuesData.InfocenterArea = i.InfocenterArea 
			and FW_CustomColumnValuesData.gridID = @GridID
			and FW_CustomColumnValuesData.colName = @colName 
			and FW_CustomColumnValuesData.Code = @Code 
			and FW_CustomColumnValuesData.UICultureName = dbo.FW_GetActiveCultureName()
 
 
 
END
 
 --IF the Name has been changed, need to change the Name for all cultures
IF (UPDATE(ColName))
begin
	UPDATE FW_CustomColumnValuesData
		SET ColName = i.ColName
		FROM inserted i 
		INNER JOIN FW_CustomColumnValuesData ON FW_CustomColumnValuesData.InfocenterArea = i.InfocenterArea 
			and FW_CustomColumnValuesData.gridID = @GridID
			and FW_CustomColumnValuesData.colName = @colName 
			and FW_CustomColumnValuesData.Code = @Code 
 
end
GO
