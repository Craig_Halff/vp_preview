SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[AnalysisCubesCalcMeasure]
as
SELECT	dbo.AnalysisCubesCalcMeasureData.PKey,
	dbo.AnalysisCubesCalcMeasureData.MeasureGroup,
	dbo.AnalysisCubesCalcMeasureData.MeasureFolder,
	dbo.AnalysisCubesCalcMeasureData.DataType,
	dbo.AnalysisCubesCalcMeasureData.MeasureExpression,
	dbo.AnalysisCubesCalcMeasureData.AutoOptimize,
	dbo.AnalysisCubesCalcMeasureDescriptions.MeasureName
FROM   dbo.AnalysisCubesCalcMeasureData Left JOIN
dbo.AnalysisCubesCalcMeasureDescriptions ON dbo.AnalysisCubesCalcMeasureData.PKey = dbo.AnalysisCubesCalcMeasureDescriptions.PKey 
and dbo.AnalysisCubesCalcMeasureDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteAnalysisCubesCalcMeasureTrigger] 
   ON  [dbo].[AnalysisCubesCalcMeasure]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @PKey Nvarchar(32)

SELECT top 1 @PKey = PKey FROM Deleted

Delete from AnalysisCubesCalcMeasureDescriptions where PKey = @PKey
Delete from AnalysisCubesCalcMeasureData where PKey = @PKey

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertAnalysisCubesCalcMeasureTrigger] 
   ON  [dbo].[AnalysisCubesCalcMeasure]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [AnalysisCubesCalcMeasureData]
      ( PKey,
	    MeasureGroup,
		MeasureFolder,
		DataType,
		MeasureExpression,
		AutoOptimize
)
Select  PKey,
	    MeasureGroup,
		MeasureFolder,
		DataType,
		MeasureExpression,
		AutoOptimize
from inserted

INSERT INTO [AnalysisCubesCalcMeasureDescriptions]
  (PKey, 
	UICultureName,
	MeasureName
  )
Select PKey, dbo.FW_GetActiveCultureName(), MeasureName from inserted
End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateAnalysisCubesCalcMeasureTrigger] 
   ON  [dbo].[AnalysisCubesCalcMeasure]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @PKey Nvarchar(32)

if update(PKey)
  select @PKey=PKey from deleted
else
  select @PKey=PKey from inserted

Update AnalysisCubesCalcMeasureData
  set 	PKey = i.PKey,
	MeasureGroup = i.MeasureGroup, 
	MeasureFolder = i.MeasureFolder,
	DataType = i.DataType,
	MeasureExpression = i.MeasureExpression,
	AutoOptimize = i.AutoOptimize
from inserted i
  inner join AnalysisCubesCalcMeasureData on AnalysisCubesCalcMeasureData.PKey = @PKey

if (not exists(Select AnalysisCubesCalcMeasureDescriptions.UICultureName from AnalysisCubesCalcMeasureDescriptions
  where AnalysisCubesCalcMeasureDescriptions.PKey = @PKey and AnalysisCubesCalcMeasureDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into AnalysisCubesCalcMeasureDescriptions
      (PKey,
		UICultureName,
		MeasureName
      )
  Select PKey, dbo.FW_GetActiveCultureName(), MeasureName From Inserted 
Else
  Update AnalysisCubesCalcMeasureDescriptions
      Set PKey = i.PKey
      , MeasureName = i.MeasureName
  From inserted i
  Inner Join AnalysisCubesCalcMeasureDescriptions on AnalysisCubesCalcMeasureDescriptions.PKey = @PKey and AnalysisCubesCalcMeasureDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
