SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CustomTablesWithUDIC]
AS
SELECT DISTINCT 
    ICT.InfoCenterArea AS InfoCenterArea, 
    'X' AS GridID, 
    CASE ICT.InfoCenterArea 
       WHEN 'ChartOfAccounts' THEN 'AccountCustomTabFields' 
       WHEN 'Clients' THEN 'ClientCustomTabFields' 
       WHEN 'Contacts' THEN 'ContactCustomTabFields'
       WHEN 'Employees' THEN 'EmployeeCustomTabFields' 
       WHEN 'MktCampaigns' THEN 'MktCampaignCustomTabFields' 
       WHEN 'Opportunities' THEN 'OpportunityCustomTabFields' 
       WHEN 'Projects' THEN 'ProjectCustomTabFields' 
       WHEN 'TextLibrary' THEN 'TextLibraryCustomTabFields' 
       WHEN 'Vendors' THEN 'VendorCustomTabFields' 
       WHEN 'ProjectPlan' THEN 'PlanningCustomTabFields' 
       WHEN 'Activity' THEN 'ActivityCustomTabFields' 
       WHEN 'Equipment' THEN 'EquipmentCustomTabFields' 
       WHEN 'Firms' THEN 'ClientCustomTabFields' 
       WHEN 'OpportunityPlan' THEN 'PlanningCustomTabFields'
       ELSE U.TableName 
       END AS TableName, 
    0 AS SortSeq --> SortSeq can be used to order the tables name in correct sequence: Regular IC, UDIC, Grids 
FROM FW_InfoCenterTabs AS ICT 
LEFT JOIN FW_UDIC AS U ON ICT.InfoCenterArea = U.UDIC_ID
WHERE ICT.InfoCenterArea <> 'Units'
UNION SELECT 'Activity', 'X', 'ActivityCustomTabFields', 0
UNION SELECT 'ChartOfAccounts', 'X', 'AccountCustomTabFields', 0
UNION SELECT 'Clients', 'X', 'ClientCustomTabFields', 0
UNION SELECT 'Contacts', 'X', 'ContactCustomTabFields', 0
UNION SELECT 'Employees', 'X', 'EmployeeCustomTabFields', 0
UNION SELECT 'Equipment', 'X', 'EquipmentCustomTabFields', 0
UNION SELECT 'MktCampaigns', 'X', 'MktCampaignCustomTabFields', 0
UNION SELECT 'Opportunities', 'X', 'OpportunityCustomTabFields', 0
UNION SELECT 'ProjectPlan', 'X', 'PlanningCustomTabFields', 0
UNION SELECT 'Projects', 'X', 'ProjectCustomTabFields', 0
UNION SELECT 'TextLibrary', 'X', 'TextLibraryCustomTabFields', 0
UNION SELECT 'Vendors', 'X', 'VendorCustomTabFields', 0
UNION SELECT 'Firms', 'X', 'ClientCustomTabFields', 0
UNION SELECT 'OpportunityPlan', 'X','PlanningCustomTabFields', 0
UNION SELECT UDIC_ID, 'X', TableName, 0 FROM FW_UDIC
UNION SELECT InfoCenterArea, GridID, TableName, 1 FROM FW_CustomGrids WHERE GridType IS NULL
GO
