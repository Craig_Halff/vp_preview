SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGCampaignAudience]
as
SELECT dbo.CFGCampaignAudienceData.Code, dbo.CFGCampaignAudienceDescriptions.Description, dbo.CFGCampaignAudienceDescriptions.Seq
  FROM dbo.CFGCampaignAudienceData Left JOIN
   dbo.CFGCampaignAudienceDescriptions ON dbo.CFGCampaignAudienceData.Code = dbo.CFGCampaignAudienceDescriptions.Code
   and dbo.CFGCampaignAudienceDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGCampaignAudienceTrigger] 
   ON  [dbo].[CFGCampaignAudience]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGCampaignAudienceDescriptions where code = @Code
Delete from CFGCampaignAudienceData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGCampaignAudienceTrigger] 
   ON  [dbo].[CFGCampaignAudience]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGCampaignAudienceData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGCampaignAudienceDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGCampaignAudienceTrigger] 
   ON  [dbo].[CFGCampaignAudience]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGCampaignAudienceData
  set code = i.code
from inserted i
  inner join CFGCampaignAudienceData on CFGCampaignAudienceData.code = @Code

if (not exists(Select CFGCampaignAudienceDescriptions.UICultureName from CFGCampaignAudienceDescriptions
  where CFGCampaignAudienceDescriptions.code = @code and CFGCampaignAudienceDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGCampaignAudienceDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGCampaignAudienceDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGCampaignAudienceDescriptions on CFGCampaignAudienceDescriptions.code = @code and CFGCampaignAudienceDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
