SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[TaxLocale]
as
SELECT dbo.TaxLocaleData.Locale, dbo.TaxLocaleData.State, dbo.TaxLocaleDescriptions.Description 
FROM         dbo.TaxLocaleData Left JOIN
dbo.TaxLocaleDescriptions ON dbo.TaxLocaleData.Locale = dbo.TaxLocaleDescriptions.Locale 
and dbo.TaxLocaleDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteTaxLocaleTrigger] 
   ON  [dbo].[TaxLocale]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Locale VARCHAR(10)

SELECT top 1 @Locale = Locale FROM Deleted

Delete from TaxLocaleDescriptions where Locale = @Locale
Delete from TaxLocaleData where Locale = @Locale

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertTaxLocaleTrigger] 
   ON  [dbo].[TaxLocale]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [TaxLocaleData]
  ([Locale],State)
Select [Locale],State from inserted

INSERT INTO [TaxLocaleDescriptions]
  ([Locale] 
  ,Description
  ,UICultureName)
Select [Locale],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateTaxLocaleTrigger] 
   ON  [dbo].[TaxLocale]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Locale VARCHAR(10)

if update(Locale)
  select @Locale=Locale from deleted
else
  select @Locale=Locale from inserted

Update TaxLocaleData
  set Locale = i.Locale, State = i.State
from inserted i
  inner join TaxLocaleData on TaxLocaleData.Locale = @Locale

if (not exists(Select TaxLocaleDescriptions.UICultureName from TaxLocaleDescriptions
  where TaxLocaleDescriptions.Locale = @Locale and TaxLocaleDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into TaxLocaleDescriptions
      ([Locale]
      ,[UICultureName]
      ,[Description])
  Select [Locale], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update TaxLocaleDescriptions
      Set Locale = i.Locale
      , [Description] = i.Description
  From inserted i
  Inner Join TaxLocaleDescriptions on TaxLocaleDescriptions.Locale = @Locale and TaxLocaleDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
