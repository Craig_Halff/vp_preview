SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGLCCodes]
as
SELECT 
  dbo.CFGLCCodesData.Code AS Code, 
  dbo.CFGLCCodesData.LCLevel AS LCLevel, 
  COALESCE(dbo.CFGLCCodesDescriptions.Label, dbo.FW_TextNotTranslated.LocalizedValue) AS Label
FROM dbo.CFGLCCodesData
  LEFT JOIN dbo.CFGLCCodesDescriptions ON dbo.CFGLCCodesData.Code = dbo.CFGLCCodesDescriptions.Code
    AND dbo.CFGLCCodesData.LCLevel = dbo.CFGLCCodesDescriptions.LCLevel
    AND dbo.CFGLCCodesDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
  LEFT JOIN dbo.FW_TextNotTranslated ON dbo.FW_TextNotTranslated.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGLCCodesTrigger] 
   ON  [dbo].[CFGLCCodes]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(14)
DECLARE @LCLevel smallint

SELECT top 1 @Code = Code, @LCLevel = LCLevel FROM Deleted

Delete from CFGLCCodesDescriptions where CFGLCCodesDescriptions.code = @Code and 
            CFGLCCodesDescriptions.LCLevel = @LCLevel
Delete from CFGLCCodesData where CFGLCCodesData.code = @Code and CFGLCCodesData.LCLevel = @LCLevel

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGLCCodesTrigger] 
   ON  [dbo].[CFGLCCodes]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGLCCodesData]
  (Code, LCLevel)
Select Code, isnull(LCLevel,0) from inserted

INSERT INTO [CFGLCCodesDescriptions]
  (Code, LCLevel, Label, UICultureName)
Select Code, LCLevel, Label, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGLCCodesTrigger] 
   ON  [dbo].[CFGLCCodes]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(14)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGLCCodesData
  set code = i.code, LCLevel=i.LCLevel
from inserted i
  inner join CFGLCCodesData on CFGLCCodesData.LCLevel = i.LCLevel and CFGLCCodesData.code = @Code

if (not exists(Select CFGLCCodesDescriptions.UICultureName from CFGLCCodesDescriptions, Inserted I
  where CFGLCCodesDescriptions.LCLevel = i.LCLevel and CFGLCCodesDescriptions.code = @code and CFGLCCodesDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGLCCodesDescriptions
      (Code,
	   LCLevel,
       UICultureName,
       Label)
  Select Code, LCLevel, dbo.FW_GetActiveCultureName(), Label From Inserted 

Else
  Update CFGLCCodesDescriptions
      Set code = i.code,
          LCLevel = i.LCLevel,
          Label = i.Label
  From inserted i
  Inner Join CFGLCCodesDescriptions on CFGLCCodesDescriptions.LCLevel = i.LCLevel and CFGLCCodesDescriptions.code = @code and CFGLCCodesDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
