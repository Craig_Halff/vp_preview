SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGScreenDesigner]
as
SELECT dbo.CFGScreenDesignerData.InfocenterArea, dbo.CFGScreenDesignerData.TabID, dbo.CFGScreenDesignerData.GridID, dbo.CFGScreenDesignerData.ComponentID, 
dbo.CFGScreenDesignerData.ButtonType, dbo.CFGScreenDesignerData.RecordLimit, dbo.CFGScreenDesignerData.ColPos, dbo.CFGScreenDesignerData.RowPos, 
dbo.CFGScreenDesignerData.ColWidth, dbo.CFGScreenDesignerData.RowHeight, dbo.CFGScreenDesignerData.LabelPosition, dbo.CFGScreenDesignerData.CreateUser, 
dbo.CFGScreenDesignerData.CreateDate, dbo.CFGScreenDesignerData.ModUser, dbo.CFGScreenDesignerData.ModDate, dbo.CFGScreenDesignerLabels.Label, 
dbo.CFGScreenDesignerLabels.ToolTip, dbo.CFGScreenDesignerData.DesignerCreated, dbo.CFGScreenDesignerData.ComponentType, dbo.CFGScreenDesignerData.LockedFor, 
dbo.CFGScreenDesignerData.RequiredFor, dbo.CFGScreenDesignerData.HiddenFor, dbo.CFGScreenDesignerData.GenericPropValue, dbo.CFGScreenDesignerData.ParentID,
dbo.CFGScreenDesignerData.DefaultValue, dbo.CFGScreenDesignerLabels.AlternateLabel, dbo.CFGScreenDesignerData.PropertyBag, dbo.CFGScreenDesignerLabels.UICultureName
FROM dbo.CFGScreenDesignerData LEFT JOIN dbo.CFGScreenDesignerLabels 
ON dbo.CFGScreenDesignerData.InfocenterArea = dbo.CFGScreenDesignerLabels.InfoCenterArea 
AND dbo.CFGScreenDesignerData.GridID = dbo.CFGScreenDesignerLabels.GridID AND dbo.CFGScreenDesignerData.ComponentID = dbo.CFGScreenDesignerLabels.ComponentID 
AND dbo.CFGScreenDesignerLabels.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DelCFGScreenDesigner] 
ON  [dbo].[CFGScreenDesigner]
INSTEAD OF DELETE
AS 
BEGIN
SET NOCOUNT ON;
DELETE FROM CFGScreenDesignerLabels FROM deleted
WHERE CFGScreenDesignerLabels.InfocenterArea = deleted.InfocenterArea 
AND CFGScreenDesignerLabels.GridID = deleted.GridID 
AND CFGScreenDesignerLabels.ComponentID = deleted.ComponentID

DELETE FROM CFGScreenDesignerData FROM deleted
WHERE CFGScreenDesignerData.InfocenterArea = deleted.InfocenterArea 
AND CFGScreenDesignerData.GridID = deleted.GridID 
AND CFGScreenDesignerData.ComponentID = deleted.ComponentID
END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsCFGScreenDesigner] 
ON  [dbo].[CFGScreenDesigner]
INSTEAD OF Insert
AS
BEGIN
SET NOCOUNT ON;
DECLARE @NewTabID nvarchar(125)
DECLARE @ComponentID AS VARCHAR(125)
DECLARE @ComponentType AS VARCHAR(30)
DECLARE @GridID nvarchar(250)
DECLARE @InfocenterArea varchar(32)
DECLARE @TabIDChangedSP nvarchar(550)
DECLARE @UserName nvarchar(50)

SELECT @GridID = GridID FROM Inserted
SELECT @NewTabID = TabID FROM inserted
SELECT @ComponentID =ComponentID FROM inserted
SELECT @ComponentType = LOWER(ComponentType) FROM inserted
SELECT @InfocenterArea =lower(InfocenterArea) from inserted

set @UserName = dbo.FW_GetUserName()
 
INSERT INTO [CFGScreenDesignerData]([InfocenterArea],[TabID],[GridID],[ComponentID],[ComponentType],[LockedFor],[RequiredFor],[HiddenFor],[DesignerCreated],[RecordLimit]
,[ButtonType],[GenericPropValue],[ParentID],[ColPos],[RowPos],[ColWidth],[RowHeight],[LabelPosition],[DefaultValue],[PropertyBag],[CreateUser],[CreateDate],[ModUser],[ModDate])
SELECT InfocenterArea, [TabID], [GridID], [ComponentID], [ComponentType],[LockedFor],[RequiredFor],[HiddenFor],isnull([DesignerCreated],'N'),isnull([RecordLimit],0),
isnull([ButtonType],0),[GenericPropValue],[ParentID],isnull([ColPos],0),isnull([RowPos],0),isnull([ColWidth],0),isnull([RowHeight],0),[LabelPosition],[DefaultValue], 
[PropertyBag],@UserName,(getutcdate()),@UserName ,(getutcdate()) FROM inserted

INSERT INTO  [CFGScreenDesignerLabels]([InfocenterArea],[GridID],[ComponentID],[UICultureName],[Label],[ToolTip],[AlternateLabel])
SELECT InfocenterArea,[GridID],[ComponentID],DBO.FW_GetActiveCultureName(),[Label],[ToolTip],[AlternateLabel] FROM Inserted

-- Skip for non-tabbed app.
IF (@NewTabID != 'X')
Begin
SELECT @NewTabID = LOWER(@NewTabID)
if EXISTS (SELECT 'X' FROM sys.objects WHERE name = 'Product_TabIDChanged' and type = 'P')
begin
set @TabIDChangedSP=' Product_TabIDChanged ''' + @NewTabID +''','''+ @ComponentID+''','''+@ComponentType+''','''+@GridID+''','''+@InfocenterArea+''''
exec (@TabIDChangedSP)
end
end
END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdCFGScreenDesigner] 
ON  [dbo].[CFGScreenDesigner]
INSTEAD OF UPDATE
AS 
BEGIN
SET NOCOUNT ON;
DECLARE @GridID nvarchar(250)
DECLARE @ComponentID AS VARCHAR(125)
DECLARE @UserName nvarchar(50)

SELECT @GridID =GridID FROM inserted
set @UserName = dbo.FW_GetUserName()

IF (UPDATE([ComponentID]))
	SELECT @ComponentID =[ComponentID] FROM deleted
ELSE
	SELECT @ComponentID =[ComponentID] FROM inserted
    -- Insert statements for trigger here
UPDATE  CFGScreenDesignerData
SET [TabID] =  i.TabID,[GridID] = i.GridID,ComponentID = i.ComponentID,ComponentType = i.ComponentType,[LockedFor] = i.LockedFor ,[RequiredFor] = i.RequiredFor 
,[HiddenFor] = i.HiddenFor,[DesignerCreated] = i.DesignerCreated,[RecordLimit] = i.RecordLimit,[ButtonType] = i.ButtonType,[GenericPropValue] = i.GenericPropValue,
[ParentID] = i.[ParentID],[ColPos] = i.ColPos,[RowPos] = i.RowPos,[ColWidth] = i.ColWidth,[RowHeight] = i.RowHeight,[LabelPosition] = i.LabelPosition,
[DefaultValue] = i.DefaultValue,[PropertyBag] = i.PropertyBag,[CreateUser] = i.CreateUser,[CreateDate] = i.CreateDate,[ModUser] = @UserName,[ModDate] = getutcdate()
FROM inserted i INNER JOIN CFGScreenDesignerData ON CFGScreenDesignerData.InfocenterArea = i.InfocenterArea 
AND CFGScreenDesignerData.GridID = @GridID AND CFGScreenDesignerData.ComponentID =@ComponentID

IF (not exists (SELECT CFGScreenDesignerLabels.UICultureName FROM CFGScreenDesignerLabels, Inserted I
WHERE  CFGScreenDesignerLabels.InfocenterArea = i.InfocenterArea AND CFGScreenDesignerLabels.GridID = @GridID
and CFGScreenDesignerLabels.ComponentID = @ComponentID and CFGScreenDesignerLabels.UICultureName =dbo.FW_GetActiveCultureName()))
INSERT INTO  CFGScreenDesignerLabels([InfocenterArea],[GridID],ComponentID,[UICultureName],Label,ToolTip,AlternateLabel)
SELECT InfocenterArea, [GridID], [ComponentID], DBO.FW_GetActiveCultureName(), Label, ToolTip, AlternateLabel FROM Inserted
ELSE
UPDATE CFGScreenDesignerLabels SET GridID = i.GridID,ComponentID = i.ComponentID,Label = i.Label,ToolTip = i.Tooltip, AlternateLabel = i.AlternateLabel
FROM inserted i INNER JOIN CFGScreenDesignerLabels ON CFGScreenDesignerLabels.InfocenterArea = i.InfocenterArea 
and CFGScreenDesignerLabels.gridId = @gridId and CFGScreenDesignerLabels.ComponentID = @ComponentID and CFGScreenDesignerLabels.UICultureName =dbo.FW_GetActiveCultureName()


END
GO
