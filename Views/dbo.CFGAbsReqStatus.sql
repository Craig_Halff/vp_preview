SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGAbsReqStatus]
as
SELECT  dbo.CFGAbsReqStatusData.Status, dbo.CFGAbsReqStatusDescriptions.Description
FROM   dbo.CFGAbsReqStatusData
 Left JOIN dbo.CFGAbsReqStatusDescriptions ON dbo.CFGAbsReqStatusData.Status = dbo.CFGAbsReqStatusDescriptions.Status
and dbo.CFGAbsReqStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGAbsReqStatusTrigger] 
   ON  [dbo].[CFGAbsReqStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Status varchar(1)

SELECT top 1 @Status = Status FROM Deleted

Delete from CFGAbsReqStatusDescriptions where CFGAbsReqStatusDescriptions.Status = @Status
Delete from CFGAbsReqStatusData where CFGAbsReqStatusData.Status = @Status

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGAbsReqStatusTrigger] 
   ON  [dbo].[CFGAbsReqStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGAbsReqStatusData]
  (Status)
Select Status from inserted

INSERT INTO [CFGAbsReqStatusDescriptions]
  (Status, Description,UICultureName)
Select Status, Description, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGAbsReqStatusTrigger] 
   ON  [dbo].[CFGAbsReqStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

Update CFGAbsReqStatusData
  set Status=i.Status 
from inserted i
  inner join CFGAbsReqStatusData on CFGAbsReqStatusData.Status = i.Status

if (not exists(Select CFGAbsReqStatusDescriptions.UICultureName from CFGAbsReqStatusDescriptions, Inserted I
  where CFGAbsReqStatusDescriptions.Status = i.Status and CFGAbsReqStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGAbsReqStatusDescriptions
      (Status,
       UICultureName,
       Description)
  Select Status, dbo.FW_GetActiveCultureName(), Description From Inserted 

Else
  Update CFGAbsReqStatusDescriptions
      Set Status = i.Status,
          Description = i.Description
  From inserted i
  Inner Join CFGAbsReqStatusDescriptions on CFGAbsReqStatusDescriptions.Status = i.Status and CFGAbsReqStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
