SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGEmployeeType]
as
SELECT dbo.CFGEmployeeTypeData.Type, dbo.CFGEmployeeTypeData.SortOrder, dbo.CFGEmployeeTypeDescriptions.Label
FROM   dbo.CFGEmployeeTypeData
 Left JOIN dbo.CFGEmployeeTypeDescriptions ON dbo.CFGEmployeeTypeData.Type = dbo.CFGEmployeeTypeDescriptions.Type
 and dbo.CFGEmployeeTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGEmployeeTypeTrigger] 
   ON  [dbo].[CFGEmployeeType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Type VARCHAR(1)


SELECT top 1 @Type = Type  FROM Deleted

Delete from CFGEmployeeTypeDescriptions where CFGEmployeeTypeDescriptions.Type = @Type 
Delete from CFGEmployeeTypeData where CFGEmployeeTypeData.Type = @Type 

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGEmployeeTypeTrigger] 
   ON  [dbo].[CFGEmployeeType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGEmployeeTypeData]
  (Type, SortOrder)
Select Type, isnull(SortOrder,0) from inserted

INSERT INTO [CFGEmployeeTypeDescriptions]
  (Type, Label,UICultureName)
Select Type, Label, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGEmployeeTypeTrigger] 
   ON  [dbo].[CFGEmployeeType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Type VARCHAR(1)

if update(Type)
  select @Type=Type from deleted
else
  select @Type=Type from inserted

Update CFGEmployeeTypeData
  set Type = i.Type, SortOrder=i.SortOrder
from inserted i
  inner join CFGEmployeeTypeData on CFGEmployeeTypeData.Type = @Type

if (not exists(Select CFGEmployeeTypeDescriptions.UICultureName from CFGEmployeeTypeDescriptions, Inserted I
  where  CFGEmployeeTypeDescriptions.Type = @Type and CFGEmployeeTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGEmployeeTypeDescriptions
      (Type,
       UICultureName,
       Label)
  Select Type, dbo.FW_GetActiveCultureName(), Label From Inserted 

Else
  Update CFGEmployeeTypeDescriptions
      Set Type = i.Type,
          Label = i.Label
  From inserted i
  Inner Join CFGEmployeeTypeDescriptions on CFGEmployeeTypeDescriptions.Type = @Type and CFGEmployeeTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
