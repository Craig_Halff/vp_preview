SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[VEAddress] AS 
SELECT
       [Clendor].[Vendor]
      ,[CLAddress].[Address]
      ,[CLAddress].[Addressee] as [Payee]
      ,[CLAddress].[Address1]
      ,[CLAddress].[Address2]
      ,[CLAddress].[Address3]
      ,[CLAddress].[Address4]
      ,[CLAddress].[City]
      ,[CLAddress].[State]
      ,[CLAddress].[ZIP]
      ,[CLAddress].[Country]
      ,[CLAddress].[Phone]
      ,[CLAddress].[Fax]
      ,[CLAddress].[EMail]
      ,[CLAddress].[TaxCountryCode]
      ,[CLAddress].[TaxRegistrationNumber]
      ,[CLAddress].[Payment] as [PrimaryInd]
      ,[CLAddress].[Accounting]
      ,[CLAddress].[PhoneFormat]
      ,[CLAddress].[FaxFormat]
      ,[CLAddress].[CLAddressID] as [VEAddressID]
      ,[CLAddress].[CreateUser]
      ,[CLAddress].[CreateDate]
      ,[CLAddress].[ModUser]
      ,[CLAddress].[ModDate]
	from CLAddress
	inner join Clendor on [CLAddress].ClientID = Clendor.ClientID
	where Clendor.Vendor is not null AND Clendor.VendorIND = 'Y'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteVEAddressTrigger] 
   ON  [dbo].[VEAddress]
instead of Delete
AS 
BEGIN
    SET NOCOUNT ON;
    Delete from [CLAddress] where  exists (select 'x' from deleted where deleted.VEAddressID = [CLAddress].CLAddressID)
END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[InsertVEAddressTrigger] 
   ON  [dbo].[VEAddress]
instead of Insert
as
BEGIN
    SET NOCOUNT ON;
	INSERT INTO CLAddress
      ([ClientID]
      ,[CLAddress].[Address]
      ,[CLAddress].[Addressee]
      ,[CLAddress].[Address1]
      ,[CLAddress].[Address2]
      ,[CLAddress].[Address3]
      ,[CLAddress].[Address4]
      ,[CLAddress].[City]
      ,[CLAddress].[State]
      ,[CLAddress].[ZIP]
      ,[CLAddress].[Country]
      ,[CLAddress].[Phone]
      ,[CLAddress].[Fax]
      ,[CLAddress].[EMail]
      ,[CLAddress].[TaxCountryCode]
      ,[CLAddress].[TaxRegistrationNumber]
      ,[CLAddress].[Payment]
      ,[CLAddress].[Accounting]
      ,[CLAddress].[PhoneFormat]
      ,[CLAddress].[FaxFormat]
      ,[CLAddress].[CreateUser]
      ,[CLAddress].[CreateDate]
      ,[CLAddress].[ModUser]
      ,[CLAddress].[ModDate]
      )

       SELECT 
       Clendor.ClientID
      ,inserted.[Address]
      ,inserted.[Payee]
      ,inserted.[Address1]
      ,inserted.[Address2]
      ,inserted.[Address3]
      ,inserted.[Address4]
      ,inserted.[City]
      ,inserted.[State]
      ,inserted.[ZIP]
      ,inserted.[Country]
      ,inserted.[Phone]
      ,inserted.[Fax]
      ,inserted.[EMail]
      ,inserted.[TaxCountryCode]
      ,inserted.[TaxRegistrationNumber]
      ,inserted.[PrimaryInd] --Payment column
      ,isnull(inserted.[Accounting],'N')
      ,inserted.[PhoneFormat]
      ,inserted.[FaxFormat]
      ,inserted.[CreateUser]
	  ,isnull(inserted.CreateDate,getutcdate())
      ,inserted.ModUser
	  ,isnull(inserted.ModDate,getutcdate())
	FROM inserted inner join [Clendor] on inserted.Vendor = [Clendor].[Vendor] and [Clendor].[VendorInd] = 'Y'

end
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[UpdateVEAddressTrigger] 
   ON [dbo].[VEAddress]
INSTEAD OF UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	IF NOT (
			UPDATE ([Vendor])
				OR
			UPDATE ([Address])
			)
	BEGIN
		UPDATE [CLAddress]
		SET [Addressee] = i.[Payee]
			,[Address1] = i.[Address1]
			,[Address2] = i.[Address2]
			,[Address3] = i.[Address3]
			,[Address4] = i.[Address4]
			,[City] = i.[City]
			,[State] = i.[State]
			,[ZIP] = i.[ZIP]
			,[Country] = i.[Country]
			,[Phone] = i.[Phone]
			,[Fax] = i.[Fax]
			,[EMail] = i.[EMail]
			,[TaxCountryCode] = i.[TaxCountryCode]
			,[TaxRegistrationNumber] = i.[TaxRegistrationNumber]
			,[Payment] = i.[PrimaryInd] --Payment column
			,[Accounting] = i.[Accounting]
			,[PhoneFormat] = i.[PhoneFormat]
			,[FaxFormat] = i.[FaxFormat]
			,[CreateUser] = i.[CreateUser]
			,[CreateDate] = i.[CreateDate]
			,[ModUser] = i.[ModUser]
			,[ModDate] = i.[ModDate]
		FROM inserted i
		INNER JOIN Clendor ON Clendor.Vendor = i.Vendor
		WHERE [CLAddress].[CLAddressID] = i.[VEAddressID]
	END
	ELSE
	BEGIN
		UPDATE [CLAddress]
		SET [Address] = i.[Address]
			,[ClientID] = ClendorNew.[ClientID]
			,[Addressee] = i.[Payee]
			,[Address1] = i.[Address1]
			,[Address2] = i.[Address2]
			,[Address3] = i.[Address3]
			,[Address4] = i.[Address4]
			,[City] = i.[City]
			,[State] = i.[State]
			,[ZIP] = i.[ZIP]
			,[Country] = i.[Country]
			,[Phone] = i.[Phone]
			,[Fax] = i.[Fax]
			,[EMail] = i.[EMail]
			,[TaxCountryCode] = i.[TaxCountryCode]
			,[TaxRegistrationNumber] = i.[TaxRegistrationNumber]
			,[Payment] = i.[PrimaryInd] --Payment column
			,[Accounting] = i.[Accounting]
			,[PhoneFormat] = i.[PhoneFormat]
			,[FaxFormat] = i.[FaxFormat]
			,[CreateUser] = i.[CreateUser]
			,[CreateDate] = i.[CreateDate]
			,[ModUser] = i.[ModUser]
			,[ModDate] = i.[ModDate]
		FROM inserted i
		INNER JOIN deleted d ON i.veAddressID = d.veAddressID
		INNER JOIN Clendor ClendorOld ON ClendorOld.Vendor = d.Vendor
		LEFT JOIN Clendor ClendorNew ON ClendorNew.Vendor = i.Vendor
		WHERE [CLAddress].[Address] = d.[Address]
			AND [CLAddress].[ClientID] = ClendorOld.ClientID
	END
END
GO
