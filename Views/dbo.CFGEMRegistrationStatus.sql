SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGEMRegistrationStatus]
as
SELECT dbo.CFGEMRegistrationStatusData.Code, dbo.CFGEMRegistrationStatusDescriptions.Description, dbo.CFGEMRegistrationStatusDescriptions.Seq
FROM   dbo.CFGEMRegistrationStatusData Left JOIN
       dbo.CFGEMRegistrationStatusDescriptions ON dbo.CFGEMRegistrationStatusData.Code = dbo.CFGEMRegistrationStatusDescriptions.Code 
   and dbo.CFGEMRegistrationStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGEMRegistrationStatusTrigger] 
   ON  [dbo].[CFGEMRegistrationStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGEMRegistrationStatusDescriptions where code = @Code
Delete from CFGEMRegistrationStatusData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGEMRegistrationStatusTrigger] 
   ON  [dbo].[CFGEMRegistrationStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGEMRegistrationStatusData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGEMRegistrationStatusDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGEMRegistrationStatusTrigger] 
   ON  [dbo].[CFGEMRegistrationStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGEMRegistrationStatusData
  set code = i.code
from inserted i
  inner join CFGEMRegistrationStatusData on CFGEMRegistrationStatusData.code = @Code

if (not exists(Select CFGEMRegistrationStatusDescriptions.UICultureName from CFGEMRegistrationStatusDescriptions
  where CFGEMRegistrationStatusDescriptions.code = @code and CFGEMRegistrationStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGEMRegistrationStatusDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGEMRegistrationStatusDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGEMRegistrationStatusDescriptions on CFGEMRegistrationStatusDescriptions.code = @code and CFGEMRegistrationStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
