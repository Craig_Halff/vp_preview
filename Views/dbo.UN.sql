SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[UN]
as
SELECT UNTable.UnitTable, UNTable.Company, UNTable.ProjectCurrencyCode, UNTable.BillingCurrencyCode, UNTable.AvailableForPlanning, UNTable.FilterOrg, 
UNTable.FilterPrincipal, UNTable.FilterProjMgr, UNTable.FilterSupervisor, UNTable.FilterCode, UNTable.Status, UNUnit.Unit, UNUnit.Name, UNUnit.SingleLabel, 
UNUnit.PluralLabel, UNUnit.BillingRate, UNUnit.Format, UNUnit.ConsolBill, UNUnit.ShowDate, UNUnit.ShowCalc, UNUnit.PostAccount, UNUnit.CostRate, 
UNUnit.RegAccount, UNUnit.OHAccount, UNUnit.CreditWBS1, UNUnit.CreditWBS2, UNUnit.CreditWBS3, UNUnit.CreditAccount, UNUnit.ConsolCost, 
UNUnit.AvailableForTimesheet, UNUnit.Item, UNUnit.EmployeeSpecificRevenue, UNUnit.UnitType
FROM UNTable, UNUnit WHERE UNTable.UnitTable = UNUnit.UnitTable
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteUNTrigger] 
ON  [dbo].[UN]
INSTEAD OF DELETE
AS 
BEGIN
SET NOCOUNT ON;
DELETE FROM UNUnit FROM deleted
WHERE UNUnit.UnitTable = deleted.UnitTable

DELETE FROM UNTable FROM deleted
WHERE UNTable.UnitTable = deleted.UnitTable
END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertUNTrigger] 
ON  [dbo].[UN]
INSTEAD OF Insert
AS
BEGIN
SET NOCOUNT ON;

INSERT INTO UNTable(UnitTable,Company,ProjectCurrencyCode,BillingCurrencyCode,AvailableForPlanning,FilterOrg,FilterPrincipal,FilterProjMgr,
FilterSupervisor,FilterCode,Status) 
SELECT UnitTable,Company,ProjectCurrencyCode,BillingCurrencyCode,isnull(AvailableForPlanning,'N'),FilterOrg,FilterPrincipal,FilterProjMgr,
FilterSupervisor,FilterCode,Status FROM inserted 
WHERE NOT EXISTS (SELECT 'x' FROM UNTable WHERE UnitTable = inserted.UnitTable and Company=inserted.Company)

INSERT INTO UNUnit(UnitTable,Unit,Name,SingleLabel,PluralLabel,BillingRate,Format,ConsolBill,ShowDate,ShowCalc,PostAccount,CostRate,RegAccount,
OHAccount,CreditWBS1,CreditWBS2,CreditWBS3,CreditAccount,ConsolCost,AvailableForTimesheet,Item,EmployeeSpecificRevenue,UnitType)
SELECT UnitTable,Unit,Name,SingleLabel,PluralLabel,isnull(BillingRate,0),Format,isnull(ConsolBill,'N'),isnull(ShowDate,'N'),isnull(ShowCalc,'N'),
PostAccount,isnull(CostRate,0),RegAccount,OHAccount,CreditWBS1,CreditWBS2,CreditWBS3,CreditAccount,isnull(ConsolCost,'N'),isnull(AvailableForTimesheet,'N'),
Item,isnull(EmployeeSpecificRevenue,'N'),UnitType FROM inserted
END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateUNTrigger] 
ON  [dbo].[UN]
INSTEAD OF UPDATE
AS 
BEGIN
SET NOCOUNT ON;
Declare @UnitTable nvarchar(30)
Declare @Company nvarchar(14)
Declare @ProjectCurrencyCode nvarchar(3)
Declare @BillingCurrencyCode nvarchar(3)
Declare @AvailableForPlanning varchar(1)
Declare @FilterOrg nvarchar(30)
Declare @FilterPrincipal nvarchar(20)
Declare @FilterProjMgr nvarchar(20)
Declare @FilterSupervisor nvarchar(20)
Declare @FilterCode nvarchar(10)
Declare @Status varchar(1)

Declare @Unit nvarchar(30)
Declare @Name nvarchar(40)
Declare @SingleLabel nvarchar(10)
Declare @PluralLabel nvarchar(10)
Declare @BillingRate decimal(19,4)
Declare @Format varchar(1)
Declare @ConsolBill varchar(1)
Declare @ShowDate varchar(1)
Declare @ShowCalc varchar(1)
Declare @PostAccount nvarchar(13)
Declare @CostRate decimal(19,4)
Declare @RegAccount nvarchar(13)
Declare @OHAccount nvarchar(13)
Declare @CreditWBS1 nvarchar(30)
Declare @CreditWBS2 nvarchar(7)
Declare @CreditWBS3 nvarchar(7)
Declare @CreditAccount nvarchar(13)
Declare @ConsolCost varchar(1)
Declare @AvailableForTimesheet varchar(1)
Declare @Item nvarchar(30)
Declare @EmployeeSpecificRevenue varchar(1)
Declare @UnitType nvarchar(10)
SELECT @UnitTable = UnitTable FROM Inserted
SELECT @Company = Company FROM Inserted
SELECT @ProjectCurrencyCode = ProjectCurrencyCode FROM inserted
SELECT @BillingCurrencyCode = BillingCurrencyCode FROM inserted
SELECT @AvailableForPlanning = AvailableForPlanning FROM inserted
SELECT @FilterOrg = FilterOrg from inserted
Select @FilterPrincipal = FilterPrincipal from inserted
SELECT @FilterProjMgr = FilterProjMgr FROM Inserted
SELECT @FilterSupervisor = FilterSupervisor FROM inserted
SELECT @FilterCode = FilterCode FROM inserted
SELECT @Status = Status FROM inserted
SELECT @Unit = Unit from inserted
Select @Name = Name from inserted
SELECT @SingleLabel = SingleLabel FROM Inserted
SELECT @PluralLabel = PluralLabel FROM inserted
SELECT @BillingRate = BillingRate FROM inserted
SELECT @Format = Format FROM inserted
SELECT @ConsolBill = ConsolBill from inserted
Select @ShowDate = ShowDate from inserted
SELECT @ShowCalc = ShowCalc FROM Inserted
SELECT @PostAccount = PostAccount FROM inserted
SELECT @CostRate = CostRate from inserted
Select @RegAccount = RegAccount from inserted
SELECT @OHAccount = OHAccount FROM Inserted
SELECT @CreditWBS1 = CreditWBS1 FROM inserted
SELECT @CreditWBS2 = CreditWBS2 FROM inserted
SELECT @CreditWBS3 = CreditWBS3 FROM Inserted
SELECT @CreditAccount = CreditAccount FROM inserted
SELECT @ConsolCost = ConsolCost from inserted
Select @AvailableForTimesheet = AvailableForTimesheet from inserted
SELECT @Item = Item FROM Inserted
SELECT @EmployeeSpecificRevenue = EmployeeSpecificRevenue FROM inserted
SELECT @UnitType = UnitType FROM inserted

if not exists (SELECT 'x' FROM UNTable WHERE UNTable.UnitTable=@UnitTable)
Begin
INSERT INTO UNTable(UnitTable,Company,ProjectCurrencyCode,BillingCurrencyCode,AvailableForPlanning,FilterOrg,FilterPrincipal,FilterProjMgr,
FilterSupervisor,FilterCode,Status) 
SELECT UnitTable,Company,ProjectCurrencyCode,BillingCurrencyCode,isnull(AvailableForPlanning,'N'),FilterOrg,FilterPrincipal,FilterProjMgr,
FilterSupervisor,FilterCode,Status FROM inserted
End
ELSE
Begin
UPDATE UNTable
SET UnitTable=@UnitTable,Company=@Company,ProjectCurrencyCode=@ProjectCurrencyCode,BillingCurrencyCode=@BillingCurrencyCode,AvailableForPlanning=isnull(@AvailableForPlanning,'N'),
FilterOrg=@FilterOrg,FilterPrincipal=@FilterPrincipal,FilterProjMgr=@FilterProjMgr,FilterSupervisor=@FilterSupervisor,FilterCode=@FilterCode,Status=@Status
FROM UNTable WHERE UNTable.UnitTable=@UnitTable
End
if not exists (SELECT 'x' FROM UNUnit WHERE UNUnit.UnitTable=@UnitTable and UNUnit.Unit=@Unit)
Begin
INSERT INTO UNUnit(UnitTable,Unit,Name,SingleLabel,PluralLabel,BillingRate,Format,ConsolBill,ShowDate,ShowCalc,PostAccount,CostRate,RegAccount,
OHAccount,CreditWBS1,CreditWBS2,CreditWBS3,CreditAccount,ConsolCost,AvailableForTimesheet,Item,EmployeeSpecificRevenue,UnitType)
SELECT UnitTable,Unit,Name,SingleLabel,PluralLabel,isnull(BillingRate,0),Format,isnull(ConsolBill,'N'),isnull(ShowDate,'N'),isnull(ShowCalc,'N'),
PostAccount,isnull(CostRate,0),RegAccount,OHAccount,CreditWBS1,CreditWBS2,CreditWBS3,CreditAccount,isnull(ConsolCost,'N'),isnull(AvailableForTimesheet,'N'),
Item,isnull(EmployeeSpecificRevenue,'N'),UnitType FROM inserted
End
Else
Begin
UPDATE UNUnit
SET UnitTable=@UnitTable,Unit=@Unit,Name=@Name,SingleLabel=@SingleLabel,PluralLabel=@PluralLabel,BillingRate=isnull(@BillingRate,0),Format=@Format,
ConsolBill=isnull(@ConsolBill,'N'),ShowDate=isnull(@ShowDate,'N'),ShowCalc=isnull(@ShowCalc,'N'),PostAccount=@PostAccount,CostRate=isnull(@CostRate,0),
RegAccount=@RegAccount,OHAccount=@OHAccount,CreditWBS1=@CreditWBS1,CreditWBS2=@CreditWBS2,CreditWBS3=@CreditWBS3,CreditAccount=@CreditAccount,
ConsolCost=isnull(@ConsolCost,'N'),AvailableForTimesheet=isnull(@AvailableForTimesheet,'N'),Item=@Item,EmployeeSpecificRevenue=isnull(@EmployeeSpecificRevenue,'N'),
UnitType=@UnitType FROM UNUnit WHERE UNUnit.UnitTable=@UnitTable and UNUnit.Unit=@Unit
End 
END
GO
