SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGCreditCardImportDetail]
as
SELECT dbo.CFGCreditCardImportDetailData.PrimaryCode, dbo.CFGCreditCardImportDetailData.Company, dbo.CFGCreditCardImportDetailData.FieldName, 
       dbo.CFGCreditCardImportDetailData.Include, dbo.CFGCreditCardImportDetailData.Seq, dbo.CFGCreditCardImportDetailData.DisplayInReconciliation,
       dbo.CFGCreditCardImportDetailDescriptions.FieldLabel 
FROM         dbo.CFGCreditCardImportDetailData Left JOIN
dbo.CFGCreditCardImportDetailDescriptions 
ON dbo.CFGCreditCardImportDetailData.PrimaryCode = dbo.CFGCreditCardImportDetailDescriptions.PrimaryCode 
and dbo.CFGCreditCardImportDetailData.Company = dbo.CFGCreditCardImportDetailDescriptions.Company 
and dbo.CFGCreditCardImportDetailData.FieldName = dbo.CFGCreditCardImportDetailDescriptions.FieldName 
and dbo.CFGCreditCardImportDetailDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGCreditCardImportDetailTrigger] 
   ON  [dbo].[CFGCreditCardImportDetail]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @PrimaryCode	Nvarchar(10)
DECLARE @Company	Nvarchar(14)
DECLARE @FieldName	Nvarchar(100)

SELECT top 1 @PrimaryCode=PrimaryCode, @Company=Company, @FieldName=FieldName FROM Deleted

Delete from CFGCreditCardImportDetailDescriptions where PrimaryCode = @PrimaryCode and Company = @Company and FieldName = @FieldName
Delete from CFGCreditCardImportDetailData where PrimaryCode = @PrimaryCode and Company = @Company and FieldName = @FieldName

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGCreditCardImportDetailTrigger] 
   ON  [dbo].[CFGCreditCardImportDetail]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGCreditCardImportDetailData]
  ([PrimaryCode],[Company],[FieldName],[Include],[Seq], [DisplayInReconciliation])	
Select [PrimaryCode],[Company],[FieldName],[Include],[Seq],isnull([DisplayInReconciliation],'N') from inserted

INSERT INTO [CFGCreditCardImportDetailDescriptions]
  ([PrimaryCode],[Company],[FieldName],[UICultureName],[FieldLabel])
Select [PrimaryCode],[Company],[FieldName],dbo.FW_GetActiveCultureName(),[FieldLabel] from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGCreditCardImportDetailTrigger] 
   ON  [dbo].[CFGCreditCardImportDetail]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @PrimaryCode	Nvarchar(10)
DECLARE @Company	Nvarchar(14)
DECLARE @FieldName	Nvarchar(100)

if update(PrimaryCode) or update(Company) or update(FieldName)
  select @PrimaryCode=PrimaryCode, @Company=Company, @FieldName=FieldName from deleted
else
  select @PrimaryCode=PrimaryCode, @Company=Company, @FieldName=FieldName from inserted

Update CFGCreditCardImportDetailData
  set PrimaryCode = i.PrimaryCode,
      Company = i.Company,
      FieldName = i.FieldName,
      Include = i.Include,
      Seq = i.Seq,
      DisplayInReconciliation = i.DisplayInReconciliation
from inserted i
  inner join CFGCreditCardImportDetailData 
     on CFGCreditCardImportDetailData.PrimaryCode = @PrimaryCode 
    and CFGCreditCardImportDetailData.Company = @Company 
    and CFGCreditCardImportDetailData.FieldName = @FieldName

if (not exists(Select CFGCreditCardImportDetailDescriptions.UICultureName from CFGCreditCardImportDetailDescriptions, Inserted i
  where CFGCreditCardImportDetailDescriptions.PrimaryCode = @PrimaryCode 
    and CFGCreditCardImportDetailDescriptions.Company = @Company
    and CFGCreditCardImportDetailDescriptions.FieldName = @FieldName 
    and CFGCreditCardImportDetailDescriptions.UICultureName = dbo.FW_GetActiveCultureName()))
  Insert Into CFGCreditCardImportDetailDescriptions
      ([PrimaryCode]
      ,[Company]
      ,[FieldName]
      ,[UICultureName]
      ,[FieldLabel])
  Select [PrimaryCode],[Company],[FieldName],dbo.FW_GetActiveCultureName(),[FieldLabel] From Inserted 
Else
  Update CFGCreditCardImportDetailDescriptions
     Set [PrimaryCode] = i.PrimaryCode
       , [Company] = i.Company
       , [FieldName] = i.FieldName
       , [FieldLabel] = i.FieldLabel
  From inserted i
  Inner Join CFGCreditCardImportDetailDescriptions 
     on CFGCreditCardImportDetailDescriptions.PrimaryCode = @PrimaryCode 
    and CFGCreditCardImportDetailDescriptions.Company = @Company 
    and CFGCreditCardImportDetailDescriptions.FieldName = @FieldName 
    and CFGCreditCardImportDetailDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

END
GO
