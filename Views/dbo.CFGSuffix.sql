SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGSuffix]
as
SELECT dbo.CFGSuffixData.Code, dbo.CFGSuffixDescriptions.Suffix, dbo.CFGSuffixDescriptions.Seq
FROM         dbo.CFGSuffixData Left JOIN
dbo.CFGSuffixDescriptions ON dbo.CFGSuffixData.Code = dbo.CFGSuffixDescriptions.Code 
and dbo.CFGSuffixDescriptions.UICultureName = dbo.FW_GetActiveCultureName() 

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGSuffixTrigger] 
   ON  [dbo].[CFGSuffix]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(20)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGSuffixDescriptions where code = @Code
Delete from CFGSuffixData where code = @Code

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGSuffixTrigger] 
   ON  [dbo].[CFGSuffix]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGSuffixData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGSuffixDescriptions]
  ([Code] 
  ,Suffix
  ,UICultureName
  ,Seq)
Select [Code],[Suffix], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGSuffixTrigger] 
   ON  [dbo].[CFGSuffix]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(20)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGSuffixData
  set code = i.code
from inserted i
  inner join CFGSuffixData on CFGSuffixData.code = @Code

if (not exists(Select CFGSuffixDescriptions.UICultureName from CFGSuffixDescriptions
  where CFGSuffixDescriptions.code = @code and CFGSuffixDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGSuffixDescriptions
      ([Code]
      ,[UICultureName]
      ,[Suffix]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Suffix],[Seq] From Inserted 

Else
  Update CFGSuffixDescriptions
      Set code = i.code
      , [Suffix] = i.Suffix
      , seq = i.seq
  From inserted i
  Inner Join CFGSuffixDescriptions on CFGSuffixDescriptions.code = @code and CFGSuffixDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
