SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGPOStatus]
as
SELECT dbo.CFGPOStatusData.Screen, dbo.CFGPOStatusData.Status, dbo.CFGPOStatusDescriptions.Description
FROM   dbo.CFGPOStatusData
 Left JOIN dbo.CFGPOStatusDescriptions ON dbo.CFGPOStatusData.Screen = dbo.CFGPOStatusDescriptions.Screen
and   dbo.CFGPOStatusData.Status = dbo.CFGPOStatusDescriptions.Status
and dbo.CFGPOStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGPOStatusTrigger] 
   ON  [dbo].[CFGPOStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Screen Nvarchar(14)
DECLARE @Status smallint

SELECT top 1 @Screen = Screen, @Status = Status FROM Deleted

Delete from CFGPOStatusDescriptions where CFGPOStatusDescriptions.Screen = @Screen and 
            CFGPOStatusDescriptions.Status = @Status
Delete from CFGPOStatusData where CFGPOStatusData.Screen = @Screen and CFGPOStatusData.Status = @Status

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGPOStatusTrigger] 
   ON  [dbo].[CFGPOStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGPOStatusData]
  (Screen, Status)
Select Screen, Status from inserted

INSERT INTO [CFGPOStatusDescriptions]
  (Screen, Status, Description,UICultureName)
Select Screen, Status, Description, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGPOStatusTrigger] 
   ON  [dbo].[CFGPOStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Screen Nvarchar(14)

if update(Screen)
  select @Screen=Screen from deleted
else
  select @Screen=Screen from inserted

Update CFGPOStatusData
  set Screen = i.Screen, Status=i.Status 
from inserted i
  inner join CFGPOStatusData on CFGPOStatusData.Status = i.Status and CFGPOStatusData.Screen = @Screen

if (not exists(Select CFGPOStatusDescriptions.UICultureName from CFGPOStatusDescriptions, Inserted I
  where CFGPOStatusDescriptions.Status = i.Status and CFGPOStatusDescriptions.Screen = @Screen and CFGPOStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGPOStatusDescriptions
      (Screen,
	   Status,
       UICultureName,
       Description)
  Select Screen, Status, dbo.FW_GetActiveCultureName(), Description From Inserted 

Else
  Update CFGPOStatusDescriptions
      Set Screen = i.Screen,
          Status = i.Status,
          Description = i.Description
  From inserted i
  Inner Join CFGPOStatusDescriptions on CFGPOStatusDescriptions.Status = i.Status and CFGPOStatusDescriptions.Screen = @Screen and CFGPOStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
