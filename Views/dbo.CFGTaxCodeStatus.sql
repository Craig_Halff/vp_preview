SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGTaxCodeStatus]
as
SELECT dbo.CFGTaxCodeStatusData.Code, dbo.CFGTaxCodeStatusDescriptions.Description 
FROM         dbo.CFGTaxCodeStatusData Left JOIN
dbo.CFGTaxCodeStatusDescriptions ON dbo.CFGTaxCodeStatusData.Code = dbo.CFGTaxCodeStatusDescriptions.Code 
and dbo.CFGTaxCodeStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGTaxCodeStatusTrigger] 
   ON  [dbo].[CFGTaxCodeStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGTaxCodeStatusDescriptions where Code = @Code
Delete from CFGTaxCodeStatusData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGTaxCodeStatusTrigger] 
   ON  [dbo].[CFGTaxCodeStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGTaxCodeStatusData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGTaxCodeStatusDescriptions]
  ([Code] 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGTaxCodeStatusTrigger] 
   ON  [dbo].[CFGTaxCodeStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGTaxCodeStatusData
  set Code = i.Code
from inserted i
  inner join CFGTaxCodeStatusData on CFGTaxCodeStatusData.Code = @Code

if (not exists(Select CFGTaxCodeStatusDescriptions.UICultureName from CFGTaxCodeStatusDescriptions
  where CFGTaxCodeStatusDescriptions.Code = @Code and CFGTaxCodeStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGTaxCodeStatusDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGTaxCodeStatusDescriptions
      Set Code = i.Code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGTaxCodeStatusDescriptions on CFGTaxCodeStatusDescriptions.Code = @Code and CFGTaxCodeStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
