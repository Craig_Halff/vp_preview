SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGOpportunitySource]
as
SELECT dbo.CFGOpportunitySourceData.Code, dbo.CFGOpportunitySourceDescriptions.Description, dbo.CFGOpportunitySourceDescriptions.Seq
FROM         dbo.CFGOpportunitySourceData Left JOIN
dbo.CFGOpportunitySourceDescriptions ON dbo.CFGOpportunitySourceData.Code = dbo.CFGOpportunitySourceDescriptions.Code 
and dbo.CFGOpportunitySourceDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGOpportunitySourceTrigger] 
   ON  [dbo].[CFGOpportunitySource]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGOpportunitySourceDescriptions where code = @Code
Delete from CFGOpportunitySourceData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGOpportunitySourceTrigger] 
   ON  [dbo].[CFGOpportunitySource]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGOpportunitySourceData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGOpportunitySourceDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],Description, dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGOpportunitySourceTrigger] 
   ON  [dbo].[CFGOpportunitySource]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGOpportunitySourceData
  set code = i.code
from inserted i
  inner join CFGOpportunitySourceData on CFGOpportunitySourceData.code = @Code

if (not exists(Select CFGOpportunitySourceDescriptions.UICultureName from CFGOpportunitySourceDescriptions
  where CFGOpportunitySourceDescriptions.code = @code and CFGOpportunitySourceDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGOpportunitySourceDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGOpportunitySourceDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGOpportunitySourceDescriptions on CFGOpportunitySourceDescriptions.code = @code and CFGOpportunitySourceDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
