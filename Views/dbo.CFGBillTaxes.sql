SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGBillTaxes]
as
SELECT	dbo.CFGBillTaxesData.Code,
	dbo.CFGBillTaxesDescriptions.Description,
	dbo.CFGBillTaxesData.Rate,
	dbo.CFGBillTaxesData.HighRate,
	dbo.CFGBillTaxesData.LowRate,
	dbo.CFGBillTaxesData.CreditAccount,
	dbo.CFGBillTaxesData.DebitAccount,
	dbo.CFGBillTaxesData.ApplyToLab,
	dbo.CFGBillTaxesData.ApplyToCon,
	dbo.CFGBillTaxesData.ApplyToExp,
	dbo.CFGBillTaxesData.ApplyToFee,
	dbo.CFGBillTaxesData.ApplyToUnit,
	dbo.CFGBillTaxesData.ApplyToAddOn,
	dbo.CFGBillTaxesData.ApplyToTax,
	dbo.CFGBillTaxesData.Company,
	dbo.CFGBillTaxesData.ApplyToPO,
	dbo.CFGBillTaxesData.CompoundOnTaxCode,
	dbo.CFGBillTaxesData.ICBillingARAcct,
	dbo.CFGBillTaxesData.TaxCurrencyCode,
	dbo.CFGBillTaxesData.ICBillingApplyToLab,
	dbo.CFGBillTaxesData.ICBillingApplyToExp,
	dbo.CFGBillTaxesData.ICBillingApplyToBalanceSheet,
	dbo.CFGBillTaxesData.NonRecoverPercent,
	dbo.CFGBillTaxesData.NonRecoverCreditAccount,
	dbo.CFGBillTaxesData.NonRecoverDebitAccount,
	dbo.CFGBillTaxesData.NonRecoverCreditWBS1,
	dbo.CFGBillTaxesData.NonRecoverCreditWBS2,
	dbo.CFGBillTaxesData.NonRecoverCreditWBS3,
	dbo.CFGBillTaxesData.NonRecoverDebitWBS1,
	dbo.CFGBillTaxesData.NonRecoverDebitWBS2,
	dbo.CFGBillTaxesData.NonRecoverDebitWBS3,
	dbo.CFGBillTaxesData.EKDetailType,
	dbo.CFGBillTaxesData.Status,
	dbo.CFGBillTaxesData.ApplyToTransactionEntry,
	dbo.CFGBillTaxesData.ApplyToEK,
	dbo.CFGBillTaxesData.TaxRegion,
	dbo.CFGBillTaxesData.ReverseCharge,
	dbo.CFGBillTaxesData.QBOID,
	dbo.CFGBillTaxesData.QBOLastUpdated
FROM   dbo.CFGBillTaxesData Left JOIN
dbo.CFGBillTaxesDescriptions ON dbo.CFGBillTaxesData.Code = dbo.CFGBillTaxesDescriptions.Code 
and dbo.CFGBillTaxesDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGBillTaxesTrigger] 
   ON  [dbo].[CFGBillTaxes]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGBillTaxesDescriptions where Code = @Code
Delete from CFGBillTaxesData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGBillTaxesTrigger] 
   ON  [dbo].[CFGBillTaxes]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGBillTaxesData]
      ( Code,
	Rate,
	HighRate,
	LowRate,
	CreditAccount,
	DebitAccount,
	ApplyToLab,
	ApplyToCon,
	ApplyToExp,
	ApplyToFee,
	ApplyToUnit,
	ApplyToAddOn,
	ApplyToTax,
	Company,
	ApplyToPO,
	CompoundOnTaxCode,
	ICBillingARAcct,
	TaxCurrencyCode,
	ICBillingApplyToLab,
	ICBillingApplyToExp,
	ICBillingApplyToBalanceSheet,
	NonRecoverPercent,
	NonRecoverCreditAccount,
	NonRecoverDebitAccount,
 	NonRecoverCreditWBS1,
	NonRecoverCreditWBS2,
	NonRecoverCreditWBS3,
	NonRecoverDebitWBS1,
	NonRecoverDebitWBS2,
	NonRecoverDebitWBS3,
	EKDetailType,
	Status,
	ApplyToTransactionEntry,
	ApplyToEK,
	TaxRegion,
	ReverseCharge,
	QBOID,
	QBOLastUpdated)
Select  Code,
	Rate,
	HighRate,
	LowRate,
	CreditAccount,
	DebitAccount,
	isnull(ApplyToLab,'N'),
	isnull(ApplyToCon,'N'),
	isnull(ApplyToExp,'N'),
	isnull(ApplyToFee,'N'),
	isnull(ApplyToUnit,'N'),
	isnull(ApplyToAddOn,'N'),
	isnull(ApplyToTax,'N'),
	Company,
	isnull(ApplyToPO,'N'),
	CompoundOnTaxCode,
	ICBillingARAcct,
	TaxCurrencyCode,
	isnull(ICBillingApplyToLab,'N'),
	isnull(ICBillingApplyToExp,'N'),
	isnull(ICBillingApplyToBalanceSheet,'N'),
	isnull(NonRecoverPercent,0),
	NonRecoverCreditAccount,
	NonRecoverDebitAccount,
	NonRecoverCreditWBS1,
	NonRecoverCreditWBS2,
	NonRecoverCreditWBS3,
	NonRecoverDebitWBS1,
	NonRecoverDebitWBS2,
	NonRecoverDebitWBS3,
	EKDetailType,
	Status,
	ApplyToTransactionEntry,
	ApplyToEK,
	TaxRegion,
	isnull(ReverseCharge,'N'),
	QBOID,
	QBOLastUpdated from inserted

INSERT INTO [CFGBillTaxesDescriptions]
  (Code 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGBillTaxesTrigger] 
   ON  [dbo].[CFGBillTaxes]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGBillTaxesData
  set 	Code = i.Code,
	Rate = i.Rate, 
	HighRate = i.HighRate,
	LowRate = i.LowRate,
	CreditAccount = i.CreditAccount,
	DebitAccount = i.DebitAccount,
	ApplyToLab = i.ApplyToLab,
	ApplyToCon = i.ApplyToCon,
	ApplyToExp = i.ApplyToExp,
	ApplyToFee = i.ApplyToFee,
	ApplyToUnit = i.ApplyToUnit,
	ApplyToAddOn = i.ApplyToAddOn,
	ApplyToTax = i.ApplyToTax,
	Company = i.Company,
	ApplyToPO = i.ApplyToPO,
	CompoundOnTaxCode = i.CompoundOnTaxCode,
	ICBillingARAcct = i.ICBillingARAcct,
	TaxCurrencyCode = i.TaxCurrencyCode,
	ICBillingApplyToLab = i.ICBillingApplyToLab,
	ICBillingApplyToExp = i.ICBillingApplyToExp,
	ICBillingApplyToBalanceSheet = i.ICBillingApplyToBalanceSheet,
    	NonRecoverPercent = i.NonRecoverPercent,
    	NonRecoverCreditAccount = i.NonRecoverCreditAccount,
    	NonRecoverDebitAccount = i.NonRecoverDebitAccount,
    	NonRecoverCreditWBS1 = i.NonRecoverCreditWBS1,
    	NonRecoverCreditWBS2 = i.NonRecoverCreditWBS2,
    	NonRecoverCreditWBS3 = i.NonRecoverCreditWBS3,
    	NonRecoverDebitWBS1 = i.NonRecoverDebitWBS1,
    	NonRecoverDebitWBS2 = i.NonRecoverDebitWBS2,
    	NonRecoverDebitWBS3 = i.NonRecoverDebitWBS3,
	EKDetailType = i.EKDetailType,
	Status = i.Status,
	ApplyToTransactionEntry = i.ApplyToTransactionEntry,
	ApplyToEK = i.ApplyToEK,
	TaxRegion = i.TaxRegion,
	ReverseCharge = i.ReverseCharge,
	QBOID = i.QBOID,
	QBOLastUpdated = i.QBOLastUpdated
from inserted i
  inner join CFGBillTaxesData on CFGBillTaxesData.Code = @Code

if (not exists(Select CFGBillTaxesDescriptions.UICultureName from CFGBillTaxesDescriptions
  where CFGBillTaxesDescriptions.Code = @Code and CFGBillTaxesDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGBillTaxesDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGBillTaxesDescriptions
      Set Code = i.Code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGBillTaxesDescriptions on CFGBillTaxesDescriptions.Code = @Code and CFGBillTaxesDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
