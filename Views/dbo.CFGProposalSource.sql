SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGProposalSource]
as
SELECT dbo.CFGProposalSourceData.Code, dbo.CFGProposalSourceDescriptions.Description, dbo.CFGProposalSourceDescriptions.Seq
FROM         dbo.CFGProposalSourceData Left JOIN
dbo.CFGProposalSourceDescriptions ON dbo.CFGProposalSourceData.Code = dbo.CFGProposalSourceDescriptions.Code 
and dbo.CFGProposalSourceDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGProposalSourceTrigger] 
   ON  [dbo].[CFGProposalSource]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGProposalSourceDescriptions where code = @Code
Delete from CFGProposalSourceData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGProposalSourceTrigger] 
   ON  [dbo].[CFGProposalSource]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGProposalSourceData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGProposalSourceDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGProposalSourceTrigger] 
   ON  [dbo].[CFGProposalSource]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGProposalSourceData
  set code = i.code
from inserted i
  inner join CFGProposalSourceData on CFGProposalSourceData.code = @Code

if (not exists(Select CFGProposalSourceDescriptions.UICultureName from CFGProposalSourceDescriptions
  where CFGProposalSourceDescriptions.code = @code and CFGProposalSourceDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGProposalSourceDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGProposalSourceDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGProposalSourceDescriptions on CFGProposalSourceDescriptions.code = @code and CFGProposalSourceDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
