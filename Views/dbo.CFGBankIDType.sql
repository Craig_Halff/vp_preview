SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGBankIDType]
as
SELECT dbo.CFGBankIDTypeData.Code, dbo.CFGBankIDTypeDescriptions.Description, dbo.CFGBankIDTypeDescriptions.Seq
FROM         dbo.CFGBankIDTypeData Left JOIN
dbo.CFGBankIDTypeDescriptions ON dbo.CFGBankIDTypeData.Code = dbo.CFGBankIDTypeDescriptions.Code 
and dbo.CFGBankIDTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGBankIDTypeTrigger] 
   ON  [dbo].[CFGBankIDType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(20)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGBankIDTypeDescriptions where Code = @Code
Delete from CFGBankIDTypeData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGBankIDTypeTrigger] 
   ON  [dbo].[CFGBankIDType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGBankIDTypeData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGBankIDTypeDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(), isnull(seq,0) from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGBankIDTypeTrigger] 
   ON  [dbo].[CFGBankIDType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(20)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGBankIDTypeData
  set Code = i.Code
from inserted i
  inner join CFGBankIDTypeData on CFGBankIDTypeData.Code = @Code

if (not exists(Select CFGBankIDTypeDescriptions.UICultureName from CFGBankIDTypeDescriptions
  where CFGBankIDTypeDescriptions.Code = @Code and CFGBankIDTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGBankIDTypeDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGBankIDTypeDescriptions
      Set Code = i.Code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGBankIDTypeDescriptions on CFGBankIDTypeDescriptions.Code = @Code and CFGBankIDTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
