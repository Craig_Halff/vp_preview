SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGCompetitionType]
as
SELECT dbo.CFGCompetitionTypeData.Code, dbo.CFGCompetitionTypeDescriptions.Description, dbo.CFGCompetitionTypeDescriptions.Seq
FROM         dbo.CFGCompetitionTypeData Left JOIN
dbo.CFGCompetitionTypeDescriptions ON dbo.CFGCompetitionTypeData.Code = dbo.CFGCompetitionTypeDescriptions.Code 
and dbo.CFGCompetitionTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGCompetitionTypeTrigger] 
   ON  [dbo].[CFGCompetitionType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGCompetitionTypeDescriptions where code = @Code
Delete from CFGCompetitionTypeData where code = @Code

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGCompetitionTypeTrigger] 
   ON  [dbo].[CFGCompetitionType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO CFGCompetitionTypeData
  (Code)
Select Code from inserted

INSERT INTO CFGCompetitionTypeDescriptions
  (Code 
  ,Description
  ,UICultureName
  ,Seq)
Select Code,Description, dbo.FW_GetActiveCultureName(),@seq from inserted

End

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGCompetitionTypeTrigger] 
   ON  [dbo].[CFGCompetitionType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGCompetitionTypeData
  set code = i.code
from inserted i
  inner join CFGCompetitionTypeData on CFGCompetitionTypeData.code = @Code

if (not exists(Select CFGCompetitionTypeDescriptions.UICultureName from CFGCompetitionTypeDescriptions
  where CFGCompetitionTypeDescriptions.code = @code and CFGCompetitionTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGCompetitionTypeDescriptions
      (Code
      ,UICultureName
      ,Description
      ,Seq)
  Select Code, dbo.FW_GetActiveCultureName(),Description,Seq From Inserted 

Else
  Update CFGCompetitionTypeDescriptions
      Set code = i.code
      , Description = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGCompetitionTypeDescriptions on CFGCompetitionTypeDescriptions.code = @code and CFGCompetitionTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END

GO
