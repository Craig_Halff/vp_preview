SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGApprovalRole]
as
SELECT dbo.CFGApprovalRoleData.RoleID, dbo.CFGApprovalRoleDescriptions.[Description]
FROM dbo.CFGApprovalRoleData
LEFT JOIN dbo.CFGApprovalRoleDescriptions ON dbo.CFGApprovalRoleDescriptions.RoleID = dbo.CFGApprovalRoleData.RoleID
      AND dbo.CFGApprovalRoleDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGApprovalRoleTrigger] 
       ON  [dbo].[CFGApprovalRole]
INSTEAD OF DELETE
AS 
BEGIN
       SET NOCOUNT ON;
       DECLARE @RoleID NVARCHAR(32)

       SELECT TOP 1 @RoleID = RoleID FROM Deleted

       DELETE FROM CFGApprovalRoleDescriptions WHERE RoleID = @RoleID
       DELETE FROM CFGApprovalRoleData WHERE RoleID = @RoleID
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[InsertCFGApprovalRoleTrigger] 
       ON  [dbo].[CFGApprovalRole]
INSTEAD OF INSERT
AS
BEGIN
       SET NOCOUNT ON;

       INSERT INTO [CFGApprovalRoleData] ([RoleID]) 
       SELECT [RoleID] FROM Inserted

       INSERT INTO [CFGApprovalRoleDescriptions] ([RoleID], Description, UICultureName)
       SELECT [RoleID], [Description], dbo.FW_GetActiveCultureName() FROM Inserted

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[UpdateCFGApprovalRoleTrigger] 
       ON  [dbo].[CFGApprovalRole]
INSTEAD OF UPDATE
AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @RoleID NVARCHAR(10)

       IF UPDATE(RoleID)
              SELECT @RoleID = RoleID FROM Deleted
       ELSE
              SELECT @RoleID = RoleID FROM Inserted

       UPDATE CFGApprovalRoleData
       SET RoleID = I.RoleID
       FROM Inserted I
       INNER JOIN CFGApprovalRoleData on CFGApprovalRoleData.RoleID = @RoleID

       IF (NOT EXISTS(SELECT CFGApprovalRoleDescriptions.UICultureName FROM CFGApprovalRoleDescriptions
              WHERE CFGApprovalRoleDescriptions.RoleID = @RoleID AND CFGApprovalRoleDescriptions.UICultureName = dbo.FW_GetActiveCultureName() ))
              INSERT INTO CFGApprovalRoleDescriptions ([RoleID], [UICultureName], [Description])
              SELECT [RoleID], dbo.FW_GetActiveCultureName(), [Description] FROM Inserted
       ELSE
              UPDATE CFGApprovalRoleDescriptions
              SET RoleID = I.RoleID,
                     [Description] = I.Description
              FROM Inserted I
              INNER JOIN CFGApprovalRoleDescriptions ON CFGApprovalRoleDescriptions.RoleID = @RoleID
                     AND CFGApprovalRoleDescriptions.UICultureName = dbo.FW_GetActiveCultureName() 
END
GO
