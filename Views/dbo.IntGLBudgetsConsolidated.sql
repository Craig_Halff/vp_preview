SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[IntGLBudgetsConsolidated] AS
  SELECT ConsolidationBudgets.BudgetName, ConsolidationBudgets.ReportingGroup, ConsolidationBudgets.Company, (ConsolidationBudgets.BudgetYear * 100) + 1 AS period, Account AS account, ConsolidationBudgetsDetail.Amount1 AS budgetAmount, 0 AS annualbudgetAmount
    FROM ConsolidationBudgets INNER JOIN ConsolidationBudgetsDetail ON ConsolidationBudgets.BudgetName = ConsolidationBudgetsDetail.BudgetName
   WHERE ConsolidationBudgetsDetail.Amount1 < -0.001 OR ConsolidationBudgetsDetail.Amount1 > 0.001
 UNION ALL
 SELECT ConsolidationBudgets.BudgetName, ConsolidationBudgets.ReportingGroup, ConsolidationBudgets.Company, (ConsolidationBudgets.BudgetYear * 100) + 2 AS period, Account AS account, ConsolidationBudgetsDetail.Amount2 AS budgetAmount, 0 AS annualbudgetAmount
   FROM ConsolidationBudgets INNER JOIN ConsolidationBudgetsDetail ON ConsolidationBudgets.BudgetName = ConsolidationBudgetsDetail.BudgetName
  WHERE ConsolidationBudgetsDetail.Amount2 < -0.001 OR ConsolidationBudgetsDetail.Amount2 > 0.001
 UNION ALL
 SELECT ConsolidationBudgets.BudgetName, ConsolidationBudgets.ReportingGroup, ConsolidationBudgets.Company, (ConsolidationBudgets.BudgetYear * 100) + 3 AS period, Account AS account, ConsolidationBudgetsDetail.Amount3 AS budgetAmount, 0 AS annualbudgetAmount
   FROM ConsolidationBudgets INNER JOIN ConsolidationBudgetsDetail ON ConsolidationBudgets.BudgetName = ConsolidationBudgetsDetail.BudgetName
  WHERE ConsolidationBudgetsDetail.Amount3 < -0.001 OR ConsolidationBudgetsDetail.Amount3 > 0.001
 UNION ALL
 SELECT ConsolidationBudgets.BudgetName, ConsolidationBudgets.ReportingGroup, ConsolidationBudgets.Company, (ConsolidationBudgets.BudgetYear * 100) + 4 AS period, Account AS account, ConsolidationBudgetsDetail.Amount4 AS budgetAmount, 0 AS annualbudgetAmount
   FROM ConsolidationBudgets INNER JOIN ConsolidationBudgetsDetail ON ConsolidationBudgets.BudgetName = ConsolidationBudgetsDetail.BudgetName
  WHERE ConsolidationBudgetsDetail.Amount4 < -0.001 OR ConsolidationBudgetsDetail.Amount4 > 0.001
 UNION ALL
 SELECT ConsolidationBudgets.BudgetName, ConsolidationBudgets.ReportingGroup, ConsolidationBudgets.Company, (ConsolidationBudgets.BudgetYear * 100) + 5 AS period, Account AS account, ConsolidationBudgetsDetail.Amount5 AS budgetAmount, 0 AS annualbudgetAmount
   FROM ConsolidationBudgets INNER JOIN ConsolidationBudgetsDetail ON ConsolidationBudgets.BudgetName = ConsolidationBudgetsDetail.BudgetName
  WHERE ConsolidationBudgetsDetail.Amount5 < -0.001 OR ConsolidationBudgetsDetail.Amount5 > 0.001
 UNION ALL
 SELECT ConsolidationBudgets.BudgetName, ConsolidationBudgets.ReportingGroup, ConsolidationBudgets.Company, (ConsolidationBudgets.BudgetYear * 100) + 6 AS period, Account AS account, ConsolidationBudgetsDetail.Amount6 AS budgetAmount, 0 AS annualbudgetAmount
   FROM ConsolidationBudgets INNER JOIN ConsolidationBudgetsDetail ON ConsolidationBudgets.BudgetName = ConsolidationBudgetsDetail.BudgetName
  WHERE ConsolidationBudgetsDetail.Amount6 < -0.001 OR ConsolidationBudgetsDetail.Amount6 > 0.001
 UNION ALL
 SELECT ConsolidationBudgets.BudgetName, ConsolidationBudgets.ReportingGroup, ConsolidationBudgets.Company, (ConsolidationBudgets.BudgetYear * 100) + 7 AS period, Account AS account, ConsolidationBudgetsDetail.Amount7 AS budgetAmount, 0 AS annualbudgetAmount
   FROM ConsolidationBudgets INNER JOIN ConsolidationBudgetsDetail ON ConsolidationBudgets.BudgetName = ConsolidationBudgetsDetail.BudgetName
  WHERE ConsolidationBudgetsDetail.Amount7 < -0.001 OR ConsolidationBudgetsDetail.Amount7 > 0.001
 UNION ALL
 SELECT ConsolidationBudgets.BudgetName, ConsolidationBudgets.ReportingGroup, ConsolidationBudgets.Company, (ConsolidationBudgets.BudgetYear * 100) + 8 AS period, Account AS account, ConsolidationBudgetsDetail.Amount8 AS budgetAmount, 0 AS annualbudgetAmount
   FROM ConsolidationBudgets INNER JOIN ConsolidationBudgetsDetail ON ConsolidationBudgets.BudgetName = ConsolidationBudgetsDetail.BudgetName
  WHERE ConsolidationBudgetsDetail.Amount8 < -0.001 OR ConsolidationBudgetsDetail.Amount8 > 0.001
 UNION ALL
 SELECT ConsolidationBudgets.BudgetName, ConsolidationBudgets.ReportingGroup, ConsolidationBudgets.Company, (ConsolidationBudgets.BudgetYear * 100) + 9 AS period, Account AS account, ConsolidationBudgetsDetail.Amount9 AS budgetAmount, 0 AS annualbudgetAmount
   FROM ConsolidationBudgets INNER JOIN ConsolidationBudgetsDetail ON ConsolidationBudgets.BudgetName = ConsolidationBudgetsDetail.BudgetName
  WHERE ConsolidationBudgetsDetail.Amount9 < -0.001 OR ConsolidationBudgetsDetail.Amount9 > 0.001
 UNION ALL
 SELECT ConsolidationBudgets.BudgetName, ConsolidationBudgets.ReportingGroup, ConsolidationBudgets.Company, (ConsolidationBudgets.BudgetYear * 100) + 10 AS period, Account AS account, ConsolidationBudgetsDetail.Amount10 AS budgetAmount, 0 AS annualbudgetAmount
   FROM ConsolidationBudgets INNER JOIN ConsolidationBudgetsDetail ON ConsolidationBudgets.BudgetName = ConsolidationBudgetsDetail.BudgetName
  WHERE ConsolidationBudgetsDetail.Amount10 < -0.001 OR ConsolidationBudgetsDetail.Amount10 > 0.001
 UNION ALL
 SELECT ConsolidationBudgets.BudgetName, ConsolidationBudgets.ReportingGroup, ConsolidationBudgets.Company, (ConsolidationBudgets.BudgetYear * 100) + 11 AS period, Account AS account, ConsolidationBudgetsDetail.Amount11 AS budgetAmount, 0 AS annualbudgetAmount
   FROM ConsolidationBudgets INNER JOIN ConsolidationBudgetsDetail ON ConsolidationBudgets.BudgetName = ConsolidationBudgetsDetail.BudgetName
  WHERE ConsolidationBudgetsDetail.Amount11 < -0.001 OR ConsolidationBudgetsDetail.Amount11 > 0.001
 UNION ALL
 SELECT ConsolidationBudgets.BudgetName, ConsolidationBudgets.ReportingGroup, ConsolidationBudgets.Company, (ConsolidationBudgets.BudgetYear * 100) + 12 AS period, Account AS account, ConsolidationBudgetsDetail.Amount12 AS budgetAmount, 0 AS annualbudgetAmount
   FROM ConsolidationBudgets INNER JOIN ConsolidationBudgetsDetail ON ConsolidationBudgets.BudgetName = ConsolidationBudgetsDetail.BudgetName
  WHERE ConsolidationBudgetsDetail.Amount12 < -0.001 OR ConsolidationBudgetsDetail.Amount12 > 0.001
 UNION ALL
 SELECT ConsolidationBudgets.BudgetName, ConsolidationBudgets.ReportingGroup, ConsolidationBudgets.Company, (ConsolidationBudgets.BudgetYear * 100) + 13 AS period, Account AS account, ConsolidationBudgetsDetail.Amount13 AS budgetAmount, 0 AS annualbudgetAmount
   FROM ConsolidationBudgets INNER JOIN ConsolidationBudgetsDetail ON ConsolidationBudgets.BudgetName = ConsolidationBudgetsDetail.BudgetName
  WHERE ConsolidationBudgetsDetail.Amount13 < -0.001 OR ConsolidationBudgetsDetail.Amount13 > 0.001
 UNION ALL
 SELECT ConsolidationBudgets.BudgetName, ConsolidationBudgets.ReportingGroup, ConsolidationBudgets.Company, (ConsolidationBudgets.BudgetYear * 100) + 00 AS period, Account AS account, 0 AS budgetAmount, ConsolidationBudgetsDetail.Annual AS annualbudgetAmount
   FROM ConsolidationBudgets INNER JOIN ConsolidationBudgetsDetail ON ConsolidationBudgets.BudgetName = ConsolidationBudgetsDetail.BudgetName
  WHERE ConsolidationBudgetsDetail.Annual < -0.001 OR ConsolidationBudgetsDetail.Annual > 0.001
GO
