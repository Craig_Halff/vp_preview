SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGUnitType]
as
SELECT dbo.CFGUnitTypeData.Code, dbo.CFGUnitTypeDescriptions.Description, dbo.CFGUnitTypeDescriptions.Seq
FROM         dbo.CFGUnitTypeData Left JOIN
dbo.CFGUnitTypeDescriptions ON dbo.CFGUnitTypeData.Code = dbo.CFGUnitTypeDescriptions.Code 
and dbo.CFGUnitTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGUnitTypeTrigger] 
   ON  [dbo].[CFGUnitType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGUnitTypeDescriptions where code = @Code
Delete from CFGUnitTypeData where code = @Code

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGUnitTypeTrigger] 
   ON  [dbo].[CFGUnitType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGUnitTypeData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGUnitTypeDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),seq from inserted

End
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGUnitTypeTrigger] 
   ON  [dbo].[CFGUnitType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGUnitTypeData
  set code = i.code
from inserted i
  inner join CFGUnitTypeData on CFGUnitTypeData.code = @Code

if (not exists(Select CFGUnitTypeDescriptions.UICultureName from CFGUnitTypeDescriptions
  where CFGUnitTypeDescriptions.code = @code and CFGUnitTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGUnitTypeDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGUnitTypeDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGUnitTypeDescriptions on CFGUnitTypeDescriptions.code = @code and CFGUnitTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
