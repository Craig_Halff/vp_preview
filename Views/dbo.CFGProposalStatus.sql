SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGProposalStatus]
as
SELECT dbo.CFGProposalStatusData.Code, dbo.CFGProposalStatusDescriptions.Description, dbo.CFGProposalStatusDescriptions.Seq
FROM         dbo.CFGProposalStatusData Left JOIN
dbo.CFGProposalStatusDescriptions ON dbo.CFGProposalStatusData.Code = dbo.CFGProposalStatusDescriptions.Code 
and dbo.CFGProposalStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGProposalStatusTrigger] 
   ON  [dbo].[CFGProposalStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGProposalStatusDescriptions where code = @Code
Delete from CFGProposalStatusData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGProposalStatusTrigger] 
   ON  [dbo].[CFGProposalStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGProposalStatusData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGProposalStatusDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGProposalStatusTrigger] 
   ON  [dbo].[CFGProposalStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGProposalStatusData
  set code = i.code
from inserted i
  inner join CFGProposalStatusData on CFGProposalStatusData.code = @Code

if (not exists(Select CFGProposalStatusDescriptions.UICultureName from CFGProposalStatusDescriptions
  where CFGProposalStatusDescriptions.code = @code and CFGProposalStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGProposalStatusDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGProposalStatusDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGProposalStatusDescriptions on CFGProposalStatusDescriptions.code = @code and CFGProposalStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
