SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGStates]
as
SELECT dbo.CFGStatesData.Code, dbo.CFGStatesDescriptions.Description, dbo.CFGStatesDescriptions.Seq,dbo.CFGStatesData.ISOCountryCode
FROM         dbo.CFGStatesData Left JOIN
dbo.CFGStatesDescriptions ON dbo.CFGStatesData.Code = dbo.CFGStatesDescriptions.Code 
and dbo.CFGStatesData.ISOCountryCode = dbo.CFGStatesDescriptions.ISOCountryCode
and dbo.CFGStatesDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGStatesTrigger] 
   ON  [dbo].[CFGStates]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)
DECLARE @CountryCode Nvarchar(2)

SELECT top 1 @Code = Code, @CountryCode=ISOCountryCode  FROM Deleted

Delete from CFGStatesDescriptions where code = @Code and ISOCountryCode=@CountryCode
Delete from CFGStatesData where code = @Code and ISOCountryCode=@CountryCode

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGStatesTrigger] 
   ON  [dbo].[CFGStates]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGStatesData]
  ([Code],ISOCountryCode)
Select [Code],ISOCountryCode from inserted

INSERT INTO [CFGStatesDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq
  ,ISOCountryCode)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),seq, ISOCountryCode from inserted

End
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGStatesTrigger] 
   ON  [dbo].[CFGStates]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)
DECLARE @CountryCode Nvarchar(2)

if update(Code)
  select @Code=Code, @CountryCode=ISOCountryCode from deleted
else
  select @Code=Code, @CountryCode=ISOCountryCode from inserted

Update CFGStatesData
  set code = i.code, ISOCountryCode=i.ISOCountryCode
from inserted i
  inner join CFGStatesData on CFGStatesData.code = @Code and CFGStatesData.ISOCountryCode=@CountryCode

if (not exists(Select CFGStatesDescriptions.UICultureName from CFGStatesDescriptions
  where CFGStatesDescriptions.code = @code and CFGStatesDescriptions.ISOCountryCode=@CountryCode and CFGStatesDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGStatesDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq]
      ,ISOCountryCode)
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq],ISOCountryCode From Inserted 

Else
  Update CFGStatesDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
      , ISOCountryCode= i.ISOCountryCode
  From inserted i
  Inner Join CFGStatesDescriptions on CFGStatesDescriptions.code = @code and CFGStatesDescriptions.ISOCountryCode=@CountryCode and CFGStatesDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
