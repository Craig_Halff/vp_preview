SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGContactType]
as
SELECT dbo.CFGContactTypeData.Code, dbo.CFGContactTypeDescriptions.Description 
FROM         dbo.CFGContactTypeData Left JOIN
dbo.CFGContactTypeDescriptions ON dbo.CFGContactTypeData.Code = dbo.CFGContactTypeDescriptions.Code 
and dbo.CFGContactTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGContactTypeTrigger] 
   ON  [dbo].[CFGContactType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(20)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGContactTypeDescriptions where Code = @Code
Delete from CFGContactTypeData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGContactTypeTrigger] 
   ON  [dbo].[CFGContactType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGContactTypeData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGContactTypeDescriptions]
  ([Code] 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGContactTypeTrigger] 
   ON  [dbo].[CFGContactType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(20)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGContactTypeData
  set Code = i.Code
from inserted i
  inner join CFGContactTypeData on CFGContactTypeData.Code = @Code

if (not exists(Select CFGContactTypeDescriptions.UICultureName from CFGContactTypeDescriptions
  where CFGContactTypeDescriptions.Code = @Code and CFGContactTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGContactTypeDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGContactTypeDescriptions
      Set Code = i.Code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGContactTypeDescriptions on CFGContactTypeDescriptions.Code = @Code and CFGContactTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
