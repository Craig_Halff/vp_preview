SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGContractTypeGovCon]
as
SELECT dbo.CFGContractTypeGovConData.Code, dbo.CFGContractTypeGovConDescriptions.Description, dbo.CFGContractTypeGovConDescriptions.Seq
FROM         dbo.CFGContractTypeGovConData Left JOIN
dbo.CFGContractTypeGovConDescriptions ON dbo.CFGContractTypeGovConData.Code = dbo.CFGContractTypeGovConDescriptions.Code 
and dbo.CFGContractTypeGovConDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGContractTypeGovConTrigger] 
   ON  [dbo].[CFGContractTypeGovCon]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGContractTypeGovConDescriptions where code = @Code
Delete from CFGContractTypeGovConData where code = @Code

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGContractTypeGovConTrigger] 
   ON  [dbo].[CFGContractTypeGovCon]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO CFGContractTypeGovConData
  (Code)
Select Code from inserted

INSERT INTO CFGContractTypeGovConDescriptions
  (Code 
  ,Description
  ,UICultureName
  ,Seq)
Select Code,Description, dbo.FW_GetActiveCultureName(),@seq from inserted

End

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGContractTypeGovConTrigger] 
   ON  [dbo].[CFGContractTypeGovCon]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGContractTypeGovConData
  set code = i.code
from inserted i
  inner join CFGContractTypeGovConData on CFGContractTypeGovConData.code = @Code

if (not exists(Select CFGContractTypeGovConDescriptions.UICultureName from CFGContractTypeGovConDescriptions
  where CFGContractTypeGovConDescriptions.code = @code and CFGContractTypeGovConDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGContractTypeGovConDescriptions
      (Code
      ,UICultureName
      ,Description
      ,Seq)
  Select Code, dbo.FW_GetActiveCultureName(),Description,Seq From Inserted 

Else
  Update CFGContractTypeGovConDescriptions
      Set code = i.code
      , Description = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGContractTypeGovConDescriptions on CFGContractTypeGovConDescriptions.code = @code and CFGContractTypeGovConDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END

GO
