SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGAccountGroupTableCode]
as
SELECT dbo.CFGAccountGroupTableCodeData.Code, dbo.CFGAccountGroupTableCodeDescriptions.Description, dbo.CFGAccountGroupTableCodeDescriptions.Seq
FROM         dbo.CFGAccountGroupTableCodeData Left JOIN
dbo.CFGAccountGroupTableCodeDescriptions ON dbo.CFGAccountGroupTableCodeData.Code = dbo.CFGAccountGroupTableCodeDescriptions.Code 
and dbo.CFGAccountGroupTableCodeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGAccountGroupTableCodeTrigger] 
   ON  [dbo].[CFGAccountGroupTableCode]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGAccountGroupTableCodeDescriptions where code = @Code
Delete from CFGAccountGroupTableCodeData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGAccountGroupTableCodeTrigger] 
   ON  [dbo].[CFGAccountGroupTableCode]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGAccountGroupTableCodeData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGAccountGroupTableCodeDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGAccountGroupTableCodeTrigger] 
   ON  [dbo].[CFGAccountGroupTableCode]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGAccountGroupTableCodeData
  set code = i.code
from inserted i
  inner join CFGAccountGroupTableCodeData on CFGAccountGroupTableCodeData.code = @Code

if (not exists(Select CFGAccountGroupTableCodeDescriptions.UICultureName from CFGAccountGroupTableCodeDescriptions
  where CFGAccountGroupTableCodeDescriptions.code = @code and CFGAccountGroupTableCodeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGAccountGroupTableCodeDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGAccountGroupTableCodeDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGAccountGroupTableCodeDescriptions on CFGAccountGroupTableCodeDescriptions.code = @code and CFGAccountGroupTableCodeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
