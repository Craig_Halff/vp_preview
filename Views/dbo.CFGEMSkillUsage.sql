SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGEMSkillUsage]
as
SELECT dbo.CFGEMSkillUsageData.Code, dbo.CFGEMSkillUsageDescriptions.Description, dbo.CFGEMSkillUsageDescriptions.Seq
FROM         dbo.CFGEMSkillUsageData Left JOIN
dbo.CFGEMSkillUsageDescriptions ON dbo.CFGEMSkillUsageData.Code = dbo.CFGEMSkillUsageDescriptions.Code 
and dbo.CFGEMSkillUsageDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGEMSkillUsageTrigger] 
   ON  [dbo].[CFGEMSkillUsage]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code smallint

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGEMSkillUsageDescriptions where code = @Code
Delete from CFGEMSkillUsageData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGEMSkillUsageTrigger] 
   ON  [dbo].[CFGEMSkillUsage]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGEMSkillUsageData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGEMSkillUsageDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGEMSkillUsageTrigger] 
   ON  [dbo].[CFGEMSkillUsage]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code smallint

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGEMSkillUsageData
  set code = i.code
from inserted i
  inner join CFGEMSkillUsageData on CFGEMSkillUsageData.code = @Code

if (not exists(Select CFGEMSkillUsageDescriptions.UICultureName from CFGEMSkillUsageDescriptions
  where CFGEMSkillUsageDescriptions.code = @code and CFGEMSkillUsageDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGEMSkillUsageDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGEMSkillUsageDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGEMSkillUsageDescriptions on CFGEMSkillUsageDescriptions.code = @code and CFGEMSkillUsageDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
