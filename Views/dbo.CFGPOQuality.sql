SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGPOQuality]
as
SELECT dbo.CFGPOQualityData.Code, dbo.CFGPOQualityDescriptions.Description 
FROM         dbo.CFGPOQualityData Left JOIN
dbo.CFGPOQualityDescriptions ON dbo.CFGPOQualityData.Code = dbo.CFGPOQualityDescriptions.Code 
and dbo.CFGPOQualityDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGPOQualityTrigger] 
   ON  [dbo].[CFGPOQuality]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(20)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGPOQualityDescriptions where Code = @Code
Delete from CFGPOQualityData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGPOQualityTrigger] 
   ON  [dbo].[CFGPOQuality]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGPOQualityData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGPOQualityDescriptions]
  ([Code] 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGPOQualityTrigger] 
   ON  [dbo].[CFGPOQuality]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(20)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGPOQualityData
  set Code = i.Code
from inserted i
  inner join CFGPOQualityData on CFGPOQualityData.Code = @Code

if (not exists(Select CFGPOQualityDescriptions.UICultureName from CFGPOQualityDescriptions
  where CFGPOQualityDescriptions.Code = @Code and CFGPOQualityDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGPOQualityDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGPOQualityDescriptions
      Set Code = i.Code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGPOQualityDescriptions on CFGPOQualityDescriptions.Code = @Code and CFGPOQualityDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
