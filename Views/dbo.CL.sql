SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CL] AS 
SELECT Clendor.ClientID
      , Clendor.Client 
      , Clendor.Name 
      , Clendor.Type 
      , Clendor.Status 
      , Clendor.ExportInd 
      , Clendor.WebSite 
      , Clendor.Memo 
      , Clendor.CurrentStatus 
      , Clendor.CustomCurrencyCode 
      , Clendor.ClientInd 
      , Clendor.VendorInd 
      , Clendor.Vendor AS LinkedVendor
      , Clendor.PriorWork 
      , Clendor.Recommend 
      , Clendor.DisadvBusiness 
      , Clendor.DisabledVetOwnedSmallBusiness 
      , Clendor.HBCU 
      , Clendor.MinorityBusiness 
      , Clendor.SmallBusiness 
      , Clendor.VetOwnedSmallBusiness 
      , Clendor.WomanOwned 
      , Clendor.AlaskaNative 
      , Clendor.SpecialtyType 
      , Clendor.Specialty 
      , Clendor.ParentID 
      , Clendor.ParentLevel1 
      , Clendor.ParentLevel2 
      , Clendor.ParentLevel3 
      , Clendor.ParentLevel4 
      , Clendor.CreateUser 
      , Clendor.CreateDate 
      , Clendor.ModUser 
      , Clendor.ModDate 
      , Clendor.Employees 
      , Clendor.AnnualRevenue 
      , Clendor.GovernmentAgency 
      , Clendor.Competitor 
      , Clendor.EightA 
      , Clendor.Hubzone 
      , Clendor.IQID 
      , Clendor.Incumbent 
      , Clendor.AjeraSync 
      , Clendor.TLInternalKey 
      , Clendor.TLSyncModDate 
      , Clendor.Owner
	  , Clendor.QBOId
	  , Clendor.QBOLastUpdated
  FROM Clendor where Clendor.ClientInd = 'Y'
  Or (Vendor is null and Clendor.VendorInd = 'Y')
  or (Clendor.VendorInd = 'N' and Clendor.ClientInd = 'N')
GO
