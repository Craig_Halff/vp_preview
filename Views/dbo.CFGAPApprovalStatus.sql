SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGAPApprovalStatus]
AS
SELECT
dbo.CFGAPApprovalStatusData.Code,
dbo.CFGAPApprovalStatusDescriptions.Description
FROM dbo.CFGAPApprovalStatusData
LEFT JOIN dbo.CFGAPApprovalStatusDescriptions ON dbo.CFGAPApprovalStatusDescriptions.Code = dbo.CFGAPApprovalStatusData.Code
AND dbo.CFGAPApprovalStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGAPApprovalStatusTrigger] 
   ON  [dbo].[CFGAPApprovalStatus]
INSTEAD OF Delete
AS 
BEGIN
   SET NOCOUNT ON;

   DELETE FROM CFGAPApprovalStatusDescriptions FROM deleted WHERE CFGAPApprovalStatusDescriptions.code = deleted.Code
   DELETE FROM CFGAPApprovalStatusData FROM deleted WHERE CFGAPApprovalStatusData.code = deleted.Code
END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGAPApprovalStatusTrigger] 
   ON  [dbo].[CFGAPApprovalStatus]
INSTEAD OF Insert
as
BEGIN
   SET NOCOUNT ON;

   INSERT INTO CFGAPApprovalStatusData(Code)
   select Code from inserted

   INSERT INTO CFGAPApprovalStatusDescriptions(Code, UICultureName, Description)
   Select Code, dbo.FW_GetActiveCultureName(), Description from inserted

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGAPApprovalStatusTrigger] 
  ON  [dbo].[CFGAPApprovalStatus]
INSTEAD OF Update
as
BEGIN
   SET NOCOUNT ON;

   DECLARE @Code Nvarchar(30)

   if update(Code)
	select @Code=Code FROM deleted
   else
	select @Code=Code from inserted

   Update CFGAPApprovalStatusData
     set Code = i.Code
     from inserted i inner join CFGAPApprovalStatusData on CFGAPApprovalStatusData.Code = @Code

   if (not exists(Select CFGAPApprovalStatusDescriptions.UICultureName from CFGAPApprovalStatusDescriptions WHERE CFGAPApprovalStatusDescriptions.code = @Code and CFGAPApprovalStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
	Insert Into CFGAPApprovalStatusDescriptions(Code, UICultureName, Description)
	Select Code, dbo.FW_GetActiveCultureName(), Description From Inserted 
   else
	Update CFGAPApprovalStatusDescriptions
	   Set Code = i.Code, Description = i.Description
	  From inserted i
	Inner Join CFGAPApprovalStatusDescriptions on CFGAPApprovalStatusDescriptions.code = @Code and CFGAPApprovalStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END

GO
