SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGFABook]
as
SELECT 	dbo.CFGFABookData.Code,
	dbo.CFGFABookData.Company,
	dbo.CFGFABookDescriptions.Name,
	dbo.CFGFABookDescriptions.Description,
	dbo.CFGFABookData.Section179,
	dbo.CFGFABookData.InUse
FROM   dbo.CFGFABookData
 Left JOIN dbo.CFGFABookDescriptions ON dbo.CFGFABookData.Code = dbo.CFGFABookDescriptions.Code
and   dbo.CFGFABookData.Company = dbo.CFGFABookDescriptions.Company
and   dbo.CFGFABookDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGFABookTrigger] 
   ON  [dbo].[CFGFABook]
instead of Delete
AS 
BEGIN
SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)
DECLARE @Company Nvarchar(14)

SELECT top 1 @Code = Code, @Company = Company FROM Deleted

Delete from CFGFABookDescriptions where CFGFABookDescriptions.code = @Code and 
            CFGFABookDescriptions.Company = @Company
Delete from CFGFABookData where CFGFABookData.code = @Code and CFGFABookData.Company = @Company

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGFABookTrigger] 
   ON  [dbo].[CFGFABook]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGFABookData]
  (	Code,
	Company,
	Section179,
	InUse)
Select 	Code,
	Company,
	isnull(Section179,'N'),
	isnull(InUse,'N')
 from inserted

INSERT INTO [CFGFABookDescriptions]
  (Code, Company, Name, Description, UICultureName)
Select Code, Company, Name, Description, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGFABookTrigger] 
   ON  [dbo].[CFGFABook]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(14)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

	
Update CFGFABookData
  set 	Code = i.code,
	Section179 = i.Section179,
	Company = i.Company,
	InUse= i.InUse
 from inserted i
  inner join CFGFABookData on CFGFABookData.Company = i.Company and CFGFABookData.code = @Code

if (not exists(Select CFGFABookDescriptions.UICultureName from CFGFABookDescriptions, Inserted I
  where CFGFABookDescriptions.Company = i.Company and CFGFABookDescriptions.code = @code and CFGFABookDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGFABookDescriptions
      (Code,
	   Company,
       UICultureName,
	   Name,
       Description)
  Select Code, Company, dbo.FW_GetActiveCultureName(), Name, Description From Inserted 

Else
  Update CFGFABookDescriptions
      Set code = i.code,
          Company = i.Company,
		  Name = i.Name,
         Description = i.Description
  From inserted i
  Inner Join CFGFABookDescriptions on CFGFABookDescriptions.Company = i.Company and CFGFABookDescriptions.code = @code and CFGFABookDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
