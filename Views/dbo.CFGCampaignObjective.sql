SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[CFGCampaignObjective]
as
SELECT dbo.CFGCampaignObjectiveData.Code, dbo.CFGCampaignObjectiveDescriptions.Description, dbo.CFGCampaignObjectiveDescriptions.Seq
  FROM dbo.CFGCampaignObjectiveData Left JOIN
       dbo.CFGCampaignObjectiveDescriptions ON dbo.CFGCampaignObjectiveData.Code = dbo.CFGCampaignObjectiveDescriptions.Code
   and dbo.CFGCampaignObjectiveDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGCampaignObjectiveTrigger] 
   ON  [dbo].[CFGCampaignObjective]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGCampaignObjectiveDescriptions where code = @Code
Delete from CFGCampaignObjectiveData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGCampaignObjectiveTrigger] 
   ON  [dbo].[CFGCampaignObjective]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGCampaignObjectiveData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGCampaignObjectiveDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGCampaignObjectiveTrigger] 
   ON  [dbo].[CFGCampaignObjective]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGCampaignObjectiveData
  set code = i.code
from inserted i
  inner join CFGCampaignObjectiveData on CFGCampaignObjectiveData.code = @Code

if (not exists(Select CFGCampaignObjectiveDescriptions.UICultureName from CFGCampaignObjectiveDescriptions
  where CFGCampaignObjectiveDescriptions.code = @code and CFGCampaignObjectiveDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGCampaignObjectiveDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGCampaignObjectiveDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGCampaignObjectiveDescriptions on CFGCampaignObjectiveDescriptions.code = @code and CFGCampaignObjectiveDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
