SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGVendorRole]
as
SELECT dbo.CFGVendorRoleData.Code, dbo.CFGVendorRoleDescriptions.Description, dbo.CFGVendorRoleDescriptions.Seq
FROM         dbo.CFGVendorRoleData Left JOIN
dbo.CFGVendorRoleDescriptions ON dbo.CFGVendorRoleData.Code = dbo.CFGVendorRoleDescriptions.Code 
and dbo.CFGVendorRoleDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGVendorRoleTrigger] 
   ON  [dbo].[CFGVendorRole]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGVendorRoleDescriptions where code = @Code
Delete from CFGVendorRoleData where code = @Code

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGVendorRoleTrigger] 
   ON  [dbo].[CFGVendorRole]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGVendorRoleData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGVendorRoleDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGVendorRoleTrigger] 
   ON  [dbo].[CFGVendorRole]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGVendorRoleData
  set code = i.code
from inserted i
  inner join CFGVendorRoleData on CFGVendorRoleData.code = @Code

if (not exists(Select CFGVendorRoleDescriptions.UICultureName from CFGVendorRoleDescriptions
  where CFGVendorRoleDescriptions.code = @code and CFGVendorRoleDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGVendorRoleDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGVendorRoleDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGVendorRoleDescriptions on CFGVendorRoleDescriptions.code = @code and CFGVendorRoleDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
