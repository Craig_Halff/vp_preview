SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGEMRegistration]
as
SELECT dbo.CFGEMRegistrationData.Code, dbo.CFGEMRegistrationDescriptions.Description, dbo.CFGEMRegistrationDescriptions.Seq
FROM         dbo.CFGEMRegistrationData Left JOIN
dbo.CFGEMRegistrationDescriptions ON dbo.CFGEMRegistrationData.Code = dbo.CFGEMRegistrationDescriptions.Code 
and dbo.CFGEMRegistrationDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGEMRegistrationTrigger] 
   ON  [dbo].[CFGEMRegistration]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGEMRegistrationDescriptions where code = @Code
Delete from CFGEMRegistrationData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGEMRegistrationTrigger] 
   ON  [dbo].[CFGEMRegistration]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGEMRegistrationData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGEMRegistrationDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGEMRegistrationTrigger] 
   ON  [dbo].[CFGEMRegistration]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGEMRegistrationData
  set code = i.code
from inserted i
  inner join CFGEMRegistrationData on CFGEMRegistrationData.code = @Code

if (not exists(Select CFGEMRegistrationDescriptions.UICultureName from CFGEMRegistrationDescriptions
  where CFGEMRegistrationDescriptions.code = @code and CFGEMRegistrationDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGEMRegistrationDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGEMRegistrationDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGEMRegistrationDescriptions on CFGEMRegistrationDescriptions.code = @code and CFGEMRegistrationDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
