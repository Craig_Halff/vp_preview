SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FW_InfoCenterTabs]
AS
SELECT     dbo.FW_InfoCenterTabsData.InfocenterArea, dbo.FW_InfoCenterTabsData.TabID, dbo.FW_InfoCenterTabsData.PageID, dbo.FW_InfoCenterTabsData.TabType, dbo.FW_InfoCenterTabsData.Seq, dbo.FW_InfoCenterTabsData.Installed, 
		   dbo.FW_InfoCenterTabsData.HiddenFor, dbo.FW_InfoCenterTabsData.Platform, dbo.FW_InfoCenterTabHeadings.UICultureName, dbo.FW_InfoCenterTabHeadings.TabHeading, dbo.FW_InfoCenterTabHeadings.SysTabHeading
FROM         dbo.FW_InfoCenterTabsData LEFT JOIN
                      dbo.FW_InfoCenterTabHeadings ON dbo.FW_InfoCenterTabHeadings.TabID = dbo.FW_InfoCenterTabsData.TabID AND 
                      dbo.FW_InfoCenterTabHeadings.InfocenterArea = dbo.FW_InfoCenterTabsData.InfocenterArea AND dbo.FW_InfoCenterTabHeadings.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DelFW_InfoCenterTabs] 
  ON  [dbo].[FW_InfoCenterTabs]
INSTEAD OF DELETE
AS 
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
DELETE FROM FW_InfoCenterTabHeadings FROM deleted
 WHERE FW_InfoCenterTabHeadings.InfocenterArea = deleted.InfocenterArea 
   AND FW_InfoCenterTabHeadings.TabID = deleted.TabID
      
DELETE FROM FW_InfoCenterTabsData FROM deleted
 WHERE FW_InfoCenterTabsData.InfocenterArea = deleted.InfocenterArea 
   AND FW_InfoCenterTabsData.TabID = deleted.TabID
  
END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[InsFW_InfoCenterTabs] 
  ON  [dbo].[FW_InfoCenterTabs]
INSTEAD OF Insert
AS
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
   Insert into FW_InfoCenterTabsData
	(InfocenterArea, Tabid, PageID, TabType, Seq, Installed, HiddenFor, Platform)
   SELECT 
	 InfocenterArea, Tabid, PageID, TabType, Seq, Installed, HiddenFor, isnull(Platform,'WinUI') FROM inserted
 
   Insert into FW_InfoCenterTabHeadings
	(InfocenterArea, Tabid, UICultureName, TabHeading, SysTabHeading)
   SELECT 
	 InfocenterArea, Tabid, dbo.FW_GetActiveCultureName(), TabHeading, SysTabHeading FROM Inserted
 
 
END


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[UpdFW_InfoCenterTabs] 
  ON  [dbo].[FW_InfoCenterTabs]
INSTEAD OF UPDATE
AS 
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
DECLARE @TabID nvarchar(250)
IF (UPDATE(TabID))
	SELECT @Tabid =TabID FROM deleted
ELSE
	SELECT @Tabid =TabID FROM inserted


UPDATE FW_InfoCenterTabsData
	SET PageID=i.PageID, TabType = i.TabType, Seq = isnull(i.Seq,0), Installed = i.Installed, TabId = i.TabID, HiddenFor=i.HiddenFor, Platform=i.Platform
 FROM inserted i
    INNER JOIN FW_InfoCenterTabsData ON FW_InfoCenterTabsData.InfocenterArea = i.InfocenterArea AND FW_InfoCenterTabsData.TabID = @TabID


IF (not exists (SELECT FW_InfoCenterTabHeadings.UICultureName FROM FW_InfoCenterTabHeadings, Inserted I
           WHERE  FW_InfoCenterTabHeadings.InfocenterArea = i.InfocenterArea AND FW_InfoCenterTabHeadings.TabID = @TabID
     AND FW_InfoCenterTabHeadings.UICultureName =dbo.FW_GetActiveCultureName()))
     Insert into FW_InfoCenterTabHeadings
	(InfocenterArea, TabID, UICultureName, TabHeading, SystabHeading)
     SELECT 
	 InfocenterArea, TabID, dbo.FW_GetActiveCultureName(), TabHeading, SystabHeading FROM inserted
 
ELSE
     UPDATE FW_InfoCenterTabHeadings
           SET TabHeading = i.tabheading, 
		       TabId = i.TabID
           FROM inserted i
           INNER JOIN FW_InfoCenterTabHeadings ON FW_InfoCenterTabHeadings.InfocenterArea = i.InfocenterArea AND FW_InfoCenterTabHeadings.TabID = @Tabid
           AND FW_InfoCenterTabHeadings.UICultureName = dbo.FW_GetActiveCultureName()
 
 
 
END 
GO
