SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGEMLocation]
as
SELECT dbo.CFGEMLocationData.Code, dbo.CFGEMLocationDescriptions.Description, dbo.CFGEMLocationDescriptions.Seq
FROM   dbo.CFGEMLocationData Left JOIN
       dbo.CFGEMLocationDescriptions ON dbo.CFGEMLocationData.Code = dbo.CFGEMLocationDescriptions.Code 
   and dbo.CFGEMLocationDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGEMLocationTrigger] 
   ON  [dbo].[CFGEMLocation]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGEMLocationDescriptions where code = @Code
Delete from CFGEMLocationData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGEMLocationTrigger] 
   ON  [dbo].[CFGEMLocation]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGEMLocationData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGEMLocationDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGEMLocationTrigger] 
   ON  [dbo].[CFGEMLocation]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGEMLocationData
  set code = i.code
from inserted i
  inner join CFGEMLocationData on CFGEMLocationData.code = @Code

if (not exists(Select CFGEMLocationDescriptions.UICultureName from CFGEMLocationDescriptions
  where CFGEMLocationDescriptions.code = @code and CFGEMLocationDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGEMLocationDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGEMLocationDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGEMLocationDescriptions on CFGEMLocationDescriptions.code = @code and CFGEMLocationDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
