SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGJobLevel]
AS
SELECT
	dbo.CFGJobLevelData.Code,
	dbo.CFGJobLevelDescriptions.[Description],
	dbo.CFGJobLevelDescriptions.Seq,
	dbo.CFGJobLevelData.Status,
	dbo.CFGJobLevelData.Predefined
FROM dbo.CFGJobLevelData
LEFT JOIN dbo.CFGJobLevelDescriptions ON dbo.CFGJobLevelDescriptions.Code = dbo.CFGJobLevelData.Code
	AND dbo.CFGJobLevelDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGJobLevelTrigger]
	ON  [dbo].[CFGJobLevel]
INSTEAD OF DELETE
AS
BEGIN
	SET NOCOUNT ON;
             
	DECLARE @Code NVARCHAR(10)
 
	SELECT TOP 1 @Code = Code FROM Deleted
 
	DELETE FROM CFGJobLevelDescriptions WHERE Code = @Code
	DELETE FROM CFGJobLevelData WHERE Code = @Code
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[InsertCFGJobLevelTrigger]
	ON  [dbo].[CFGJobLevel]
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON;
 
	INSERT INTO [CFGJobLevelData] ([Code], [Status], [Predefined])
	SELECT [Code], [Status], ISNULL([Predefined], 'N') FROM Inserted
 
	INSERT INTO [CFGJobLevelDescriptions] ([Code], Description, UICultureName, Seq)
	SELECT [Code], [Description], dbo.FW_GetActiveCultureName(), ISNULL(Seq, 0) FROM Inserted
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[UpdateCFGJobLevelTrigger]
	ON  [dbo].[CFGJobLevel]
INSTEAD OF UPDATE
AS
BEGIN
	SET NOCOUNT ON;
 
	DECLARE @Code NVARCHAR(10)
 
	IF UPDATE(Code)
		SELECT @Code = Code FROM Deleted
	ELSE
		SELECT @Code = Code FROM Inserted
 
	UPDATE CFGJobLevelData
	SET Code = I.Code,
		Status = I.Status,
		Predefined = ISNULL(I.Predefined, 'N')
	FROM Inserted I
	INNER JOIN CFGJobLevelData on CFGJobLevelData.Code = @Code
 
	IF (NOT EXISTS(
		SELECT CFGJobLevelDescriptions.UICultureName
		FROM CFGJobLevelDescriptions
		WHERE CFGJobLevelDescriptions.Code = @Code
			AND CFGJobLevelDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
	))
		INSERT INTO CFGJobLevelDescriptions ([Code], [UICultureName], [Description], [Seq])
		SELECT [Code], dbo.FW_GetActiveCultureName(), [Description], ISNULL([Seq], 0) FROM Inserted
	ELSE
		UPDATE CFGJobLevelDescriptions
		SET Code = I.Code,
			[Description] = I.Description,
			[Seq] = I.Seq
		FROM Inserted I
		INNER JOIN CFGJobLevelDescriptions ON CFGJobLevelDescriptions.Code = @Code
			AND CFGJobLevelDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
END
GO
