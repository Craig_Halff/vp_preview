SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGAcctngCalendar]
as
SELECT 
  dbo.CFGAcctngCalendarData.StartDate, 
  dbo.CFGAcctngCalendarData.EndDate, 
  dbo.CFGAcctngCalendarData.Period, 
  ISNULL(dbo.CFGAcctngCalendarDescriptions.Label, '?????') as Label
FROM dbo.CFGAcctngCalendarData
  Left JOIN dbo.CFGAcctngCalendarDescriptions ON 
    dbo.CFGAcctngCalendarData.StartDate = dbo.CFGAcctngCalendarDescriptions.StartDate and 
    dbo.CFGAcctngCalendarDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGAcctngCalendarTrigger] 
   ON  [dbo].[CFGAcctngCalendar]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @StartDate datetime

SELECT top 1 @StartDate = StartDate FROM Deleted

Delete from CFGAcctngCalendarDescriptions where StartDate = @StartDate
Delete from CFGAcctngCalendarData where StartDate = @StartDate

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGAcctngCalendarTrigger] 
   ON  [dbo].[CFGAcctngCalendar]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGAcctngCalendarData]
  ([StartDate], [EndDate], [Period])
Select [StartDate], [EndDate], [Period] from inserted

INSERT INTO [CFGAcctngCalendarDescriptions]
  ([StartDate], [UICultureName], [Label]) 
Select [StartDate], dbo.FW_GetActiveCultureName(), [Label] from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGAcctngCalendarTrigger] 
   ON  [dbo].[CFGAcctngCalendar]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @StartDate datetime

if update(StartDate)
  select @StartDate=StartDate from deleted
else
  select @StartDate=StartDate from inserted

Update CFGAcctngCalendarData
  set StartDate = i.StartDate
from inserted i
  inner join CFGAcctngCalendarData on CFGAcctngCalendarData.StartDate = @StartDate

if (not exists(Select CFGAcctngCalendarDescriptions.UICultureName from CFGAcctngCalendarDescriptions
  where CFGAcctngCalendarDescriptions.StartDate = @StartDate and CFGAcctngCalendarDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGAcctngCalendarDescriptions
      ([StartDate]
      ,[UICultureName]
      ,[Label])
  Select [StartDate], dbo.FW_GetActiveCultureName(),[Label] From Inserted 

Else
  Update CFGAcctngCalendarDescriptions
      Set StartDate = i.StartDate
      , [Label] = i.Label
  From inserted i
  Inner Join CFGAcctngCalendarDescriptions on CFGAcctngCalendarDescriptions.StartDate = @StartDate and CFGAcctngCalendarDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
