SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGClientType]
as
SELECT dbo.CFGClientTypeData.Code, dbo.CFGClientTypeDescriptions.Description, dbo.CFGClientTypeDescriptions.Seq
FROM         dbo.CFGClientTypeData Left JOIN
dbo.CFGClientTypeDescriptions ON dbo.CFGClientTypeData.Code = dbo.CFGClientTypeDescriptions.Code 
and dbo.CFGClientTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGClientTypeTrigger] 
   ON  [dbo].[CFGClientType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGClientTypeDescriptions where code = @Code
Delete from CFGClientTypeData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGClientTypeTrigger] 
   ON  [dbo].[CFGClientType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGClientTypeData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGClientTypeDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGClientTypeTrigger] 
   ON  [dbo].[CFGClientType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGClientTypeData
  set code = i.code
from inserted i
  inner join CFGClientTypeData on CFGClientTypeData.code = @Code

if (not exists(Select CFGClientTypeDescriptions.UICultureName from CFGClientTypeDescriptions
  where CFGClientTypeDescriptions.code = @code and CFGClientTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGClientTypeDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGClientTypeDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGClientTypeDescriptions on CFGClientTypeDescriptions.code = @code and CFGClientTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
