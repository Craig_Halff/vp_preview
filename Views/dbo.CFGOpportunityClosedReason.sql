SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGOpportunityClosedReason]
as
SELECT dbo.CFGOpportunityClosedReasonData.Code, dbo.CFGOpportunityClosedReasonDescriptions.[Description], dbo.CFGOpportunityClosedReasonDescriptions.Seq 
FROM dbo.CFGOpportunityClosedReasonData
LEFT JOIN dbo.CFGOpportunityClosedReasonDescriptions ON dbo.CFGOpportunityClosedReasonDescriptions.Code = dbo.CFGOpportunityClosedReasonData.Code
      AND dbo.CFGOpportunityClosedReasonDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGOpportunityClosedReasonTrigger] 
       ON  [dbo].[CFGOpportunityClosedReason]
INSTEAD OF DELETE
AS 
BEGIN
       SET NOCOUNT ON;
              
       DECLARE @Code NVARCHAR(10)

       SELECT TOP 1 @Code = Code FROM Deleted

       DELETE FROM CFGOpportunityClosedReasonDescriptions WHERE Code = @Code
       DELETE FROM CFGOpportunityClosedReasonData WHERE Code = @Code
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[InsertCFGOpportunityClosedReasonTrigger] 
       ON  [dbo].[CFGOpportunityClosedReason]
INSTEAD OF INSERT
AS
BEGIN
       SET NOCOUNT ON;

       INSERT INTO [CFGOpportunityClosedReasonData] ([Code])
       SELECT [Code] FROM Inserted

       INSERT INTO [CFGOpportunityClosedReasonDescriptions] ([Code], Description, UICultureName, Seq)
       SELECT [Code], [Description], dbo.FW_GetActiveCultureName(), ISNULL(Seq, 0) FROM Inserted
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[UpdateCFGOpportunityClosedReasonTrigger] 
       ON  [dbo].[CFGOpportunityClosedReason]
INSTEAD OF UPDATE
AS
BEGIN
       SET NOCOUNT ON;

       DECLARE @Code NVARCHAR(10)

       IF UPDATE(Code)
              SELECT @Code = Code FROM Deleted
       ELSE
              SELECT @Code = Code FROM Inserted

       UPDATE CFGOpportunityClosedReasonData
       SET Code = I.Code
       FROM Inserted I
       INNER JOIN CFGOpportunityClosedReasonData on CFGOpportunityClosedReasonData.Code = @Code

       IF (NOT EXISTS(SELECT CFGOpportunityClosedReasonDescriptions.UICultureName FROM CFGOpportunityClosedReasonDescriptions
              WHERE CFGOpportunityClosedReasonDescriptions.Code = @Code AND CFGOpportunityClosedReasonDescriptions.UICultureName = dbo.FW_GetActiveCultureName() ))
              INSERT INTO CFGOpportunityClosedReasonDescriptions ([Code], [UICultureName], [Description], [Seq])
              SELECT [Code], dbo.FW_GetActiveCultureName(), [Description], ISNULL([Seq], 0) FROM Inserted
       ELSE
              UPDATE CFGOpportunityClosedReasonDescriptions
              SET Code = I.Code,
                     [Description] = I.Description,
                     [Seq] = I.Seq
              FROM Inserted I
              INNER JOIN CFGOpportunityClosedReasonDescriptions ON CFGOpportunityClosedReasonDescriptions.Code = @Code
                     AND CFGOpportunityClosedReasonDescriptions.UICultureName = dbo.FW_GetActiveCultureName() 
END
GO
