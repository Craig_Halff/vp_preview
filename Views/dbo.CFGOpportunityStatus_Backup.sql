SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGOpportunityStatus_Backup]
as
SELECT dbo.CFGOpportunityStatusData.Code, dbo.CFGOpportunityStatusDescriptions.Description 
FROM         dbo.CFGOpportunityStatusData Left JOIN
dbo.CFGOpportunityStatusDescriptions ON dbo.CFGOpportunityStatusData.Code = dbo.CFGOpportunityStatusDescriptions.Code 
and dbo.CFGOpportunityStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGOpportunityStatusTrigger] 
   ON  [dbo].[CFGOpportunityStatus_Backup]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGOpportunityStatusDescriptions where Code = @Code
Delete from CFGOpportunityStatusData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGOpportunityStatusTrigger] 
   ON  [dbo].[CFGOpportunityStatus_Backup]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGOpportunityStatusData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGOpportunityStatusDescriptions]
  ([Code] 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGOpportunityStatusTrigger] 
   ON  [dbo].[CFGOpportunityStatus_Backup]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGOpportunityStatusData
  set Code = i.Code
from inserted i
  inner join CFGOpportunityStatusData on CFGOpportunityStatusData.Code = @Code

if (not exists(Select CFGOpportunityStatusDescriptions.UICultureName from CFGOpportunityStatusDescriptions
  where CFGOpportunityStatusDescriptions.Code = @Code and CFGOpportunityStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGOpportunityStatusDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGOpportunityStatusDescriptions
      Set Code = i.Code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGOpportunityStatusDescriptions on CFGOpportunityStatusDescriptions.Code = @Code and CFGOpportunityStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
