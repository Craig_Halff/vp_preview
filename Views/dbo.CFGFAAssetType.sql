SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGFAAssetType]
as
SELECT 	dbo.CFGFAAssetTypeData.Code,
	dbo.CFGFAAssetTypeData.Company,
	dbo.CFGFAAssetTypeData.PropertyType,
	dbo.CFGFAAssetTypeData.DepMethod,
	dbo.CFGFAAssetTypeData.UsefulLifeYr,
	dbo.CFGFAAssetTypeData.AssetAccount,
	dbo.CFGFAAssetTypeData.AccuDepAccount,
	dbo.CFGFAAssetTypeData.DepExpAccount,
	dbo.CFGFAAssetTypeDescriptions.Description,
	dbo.CFGFAAssetTypeData.InUse
FROM   dbo.CFGFAAssetTypeData
 Left JOIN dbo.CFGFAAssetTypeDescriptions ON dbo.CFGFAAssetTypeData.Code = dbo.CFGFAAssetTypeDescriptions.Code
and   dbo.CFGFAAssetTypeData.Company = dbo.CFGFAAssetTypeDescriptions.Company
and   dbo.CFGFAAssetTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGFAAssetTypeTrigger] 
   ON  [dbo].[CFGFAAssetType]
instead of Delete
AS 
BEGIN
SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)
DECLARE @Company Nvarchar(14)

SELECT top 1 @Code = Code, @Company = Company FROM Deleted

Delete from CFGFAAssetTypeDescriptions where CFGFAAssetTypeDescriptions.code = @Code and 
            CFGFAAssetTypeDescriptions.Company = @Company
Delete from CFGFAAssetTypeData where CFGFAAssetTypeData.code = @Code and CFGFAAssetTypeData.Company = @Company

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGFAAssetTypeTrigger] 
   ON  [dbo].[CFGFAAssetType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGFAAssetTypeData]
  (	Code,
	Company,
	PropertyType,
	DepMethod,
	UsefulLifeYr,
	AssetAccount,
	AccuDepAccount,
	DepExpAccount,
	InUse)
Select 	Code,
	Company,
	PropertyType,
	DepMethod,
	UsefulLifeYr,
	AssetAccount,
	AccuDepAccount,
	DepExpAccount,
	isnull(InUse,'N')
 from inserted

INSERT INTO [CFGFAAssetTypeDescriptions]
  (Code, Company, Description, UICultureName)
Select Code, Company, Description, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGFAAssetTypeTrigger] 
   ON  [dbo].[CFGFAAssetType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(14)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

	
Update CFGFAAssetTypeData
  set 	Code = i.code,
	Company = i.Company,
	PropertyType = i.PropertyType,
	DepMethod = i.DepMethod,
	UsefulLifeYr = i.UsefulLifeYr,
	AssetAccount = i.AssetAccount,
	AccuDepAccount = i.AccuDepAccount,
	DepExpAccount = i.DepExpAccount,
	InUse= i.InUse
 from inserted i
  inner join CFGFAAssetTypeData on CFGFAAssetTypeData.Company = i.Company and CFGFAAssetTypeData.code = @Code

if (not exists(Select CFGFAAssetTypeDescriptions.UICultureName from CFGFAAssetTypeDescriptions, Inserted I
  where CFGFAAssetTypeDescriptions.Company = i.Company and CFGFAAssetTypeDescriptions.code = @code and CFGFAAssetTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGFAAssetTypeDescriptions
      (Code,
	   Company,
       UICultureName,
       Description)
  Select Code, Company, dbo.FW_GetActiveCultureName(), Description From Inserted 

Else
  Update CFGFAAssetTypeDescriptions
      Set code = i.code,
          Company = i.Company,
         Description = i.Description
  From inserted i
  Inner Join CFGFAAssetTypeDescriptions on CFGFAAssetTypeDescriptions.Company = i.Company and CFGFAAssetTypeDescriptions.code = @code and CFGFAAssetTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
