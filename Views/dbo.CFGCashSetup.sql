SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGCashSetup]
as
SELECT dbo.CFGCashSetupData.Code, dbo.CFGCashSetupData.Company,  dbo.CFGCashSetupDescriptions.Label,
dbo.CFGCashSetupData.ReportColumn, dbo.CFGCashSetupDescriptions.SortSeq, dbo.CFGCashSetupData.SystemAccounts
FROM   dbo.CFGCashSetupData
 Left JOIN dbo.CFGCashSetupDescriptions ON dbo.CFGCashSetupData.code = dbo.CFGCashSetupDescriptions.code
and   dbo.CFGCashSetupData.Company = dbo.CFGCashSetupDescriptions.Company
and dbo.CFGCashSetupDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGCashSetupTrigger] 
   ON  [dbo].[CFGCashSetup]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @code Nvarchar(100)
DECLARE @Company Nvarchar(14)

SELECT top 1 @code = code, @Company = Company FROM Deleted

Delete from CFGCashSetupDescriptions where CFGCashSetupDescriptions.code = @code and 
            CFGCashSetupDescriptions.Company = @Company
Delete from CFGCashSetupData where CFGCashSetupData.code = @code and CFGCashSetupData.Company = @Company

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGCashSetupTrigger] 
   ON  [dbo].[CFGCashSetup]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;
/* note that the code field is being updated with the label field */
/* for a new record both fields are the same */

INSERT INTO [CFGCashSetupData]
  (code, Company, ReportColumn, SystemAccounts )
Select Label, Company, isnull(ReportColumn,0), SystemAccounts from inserted

INSERT INTO [CFGCashSetupDescriptions]
  (code, Company, label, SortSeq, UICultureName)
Select label, Company, Label, SortSeq, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGCashSetupTrigger] 
   ON  [dbo].[CFGCashSetup]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @code Nvarchar(100)

if update(code)
  select @code=code from deleted
else
  select @code=code from inserted

Update CFGCashSetupData
  set code = i.code, Company=i.Company, ReportColumn = i.ReportColumn, SystemAccounts = i.SystemAccounts
from inserted i
  inner join CFGCashSetupData on CFGCashSetupData.Company = i.Company and CFGCashSetupData.code = @code

if (not exists(Select CFGCashSetupDescriptions.UICultureName from CFGCashSetupDescriptions, Inserted I
  where CFGCashSetupDescriptions.Company = i.Company and CFGCashSetupDescriptions.code = @code and CFGCashSetupDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGCashSetupDescriptions
      (code,
	   Company,
       UICultureName,
	   SortSeq,
       Label)
  Select code, Company, dbo.FW_GetActiveCultureName(), SortSeq, label From Inserted 

Else
  Update CFGCashSetupDescriptions
      Set code = i.code,
          Company = i.Company,
          Label = i.Label,
		  SortSeq = i.SortSeq	
  From inserted i
  Inner Join CFGCashSetupDescriptions on CFGCashSetupDescriptions.Company = i.Company and CFGCashSetupDescriptions.code = @code and CFGCashSetupDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
