SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGContractStatus]
as
SELECT dbo.CFGContractStatusData.Code, dbo.CFGContractStatusDescriptions.Description, dbo.CFGContractStatusDescriptions.Seq
FROM         dbo.CFGContractStatusData Left JOIN
dbo.CFGContractStatusDescriptions ON dbo.CFGContractStatusData.Code = dbo.CFGContractStatusDescriptions.Code 
and dbo.CFGContractStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGContractStatusTrigger] 
   ON  [dbo].[CFGContractStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGContractStatusDescriptions where code = @Code
Delete from CFGContractStatusData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGContractStatusTrigger] 
   ON  [dbo].[CFGContractStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGContractStatusData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGContractStatusDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGContractStatusTrigger] 
   ON  [dbo].[CFGContractStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGContractStatusData
  set code = i.code
from inserted i
  inner join CFGContractStatusData on CFGContractStatusData.code = @Code

if (not exists(Select CFGContractStatusDescriptions.UICultureName from CFGContractStatusDescriptions
  where CFGContractStatusDescriptions.code = @code and CFGContractStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGContractStatusDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGContractStatusDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGContractStatusDescriptions on CFGContractStatusDescriptions.code = @code and CFGContractStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
