SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGClientHierarchy]
as
SELECT dbo.CFGClientHierarchyData.pkey, dbo.CFGClientHierarchyData.level, dbo.CFGClientHierarchyDescriptions.Description 
FROM         dbo.CFGClientHierarchyData Left JOIN
dbo.CFGClientHierarchyDescriptions ON dbo.CFGClientHierarchyData.Pkey = dbo.CFGClientHierarchyDescriptions.Pkey 
and dbo.CFGClientHierarchyDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGClientHierarchyTrigger] 
   ON  [dbo].[CFGClientHierarchy]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Pkey VARCHAR(32)

SELECT top 1 @Pkey = Pkey FROM Deleted

Delete from CFGClientHierarchyDescriptions where Pkey = @Pkey
Delete from CFGClientHierarchyData where Pkey = @Pkey

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGClientHierarchyTrigger] 
   ON  [dbo].[CFGClientHierarchy]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGClientHierarchyData]
  ([Pkey],Level)
Select [Pkey], isnull(level,0) from inserted

INSERT INTO [CFGClientHierarchyDescriptions]
  ([Pkey] 
  ,Description
  ,UICultureName)
Select [Pkey],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGClientHierarchyTrigger] 
   ON  [dbo].[CFGClientHierarchy]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @PKey VARCHAR(32)

if update(PKey)
  select @PKey=PKey from deleted
else
  select @PKey=PKey from inserted

Update CFGClientHierarchyData
  set PKey = i.PKey, Level = isnull(i.Level,0)
from inserted i
  inner join CFGClientHierarchyData on CFGClientHierarchyData.PKey = @PKey

if (not exists(Select CFGClientHierarchyDescriptions.UICultureName from CFGClientHierarchyDescriptions
  where CFGClientHierarchyDescriptions.PKey = @PKey and CFGClientHierarchyDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGClientHierarchyDescriptions
      ([PKey]
      ,[UICultureName]
      ,[Description])
  Select [PKey], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGClientHierarchyDescriptions
      Set PKey = i.PKey
      , [Description] = i.Description
  From inserted i
  Inner Join CFGClientHierarchyDescriptions on CFGClientHierarchyDescriptions.PKey = @PKey and CFGClientHierarchyDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
