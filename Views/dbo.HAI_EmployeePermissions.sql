SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[HAI_EmployeePermissions]
AS
SELECT        Employee, CAST(Org AS NVARCHAR(10)) AS Org, CAST(Office AS NVARCHAR(10)) AS Office, CAST(Practice AS NVARCHAR(75)) AS Practice, CAST(Team AS NVARCHAR(10)) AS Team, CAST(OfficeDelegate AS NVARCHAR(10)) 
                         AS OfficeDelegate, CAST(PracticeDelegate AS NVARCHAR(75)) AS PracticeDelegate, CAST(TeamDelegate AS NVARCHAR(10)) AS TeamDelegate
FROM            (SELECT        A.Employee, A.Org, A.Office, NULL AS Practice, NULL AS Team, NULL AS OfficeDelegate, NULL AS PracticeDelegate, NULL AS TeamDelegate
                          FROM            (SELECT        Employee, Org, SUBSTRING(Org, 4, 2) AS Office
                                                    FROM            dbo.EMCompany WITH (NOLOCK)) AS A INNER JOIN
                                                        (SELECT        Employee, CustOperationsManager
                                                          FROM            dbo.EmployeeCustomTabFields WITH (NOLOCK)
                                                          WHERE        (CustOperationsManager = 'Y')) AS B ON B.Employee = A.Employee
                          UNION
                          SELECT        A_3.Employee, A_3.Org, A_3.Office, B_3.Practice, NULL AS Team, NULL AS OfficeDelegate, NULL AS PracticeDelegate, NULL AS TeamDelegate
                          FROM            (SELECT        Employee, Org, NULL AS Office
                                                    FROM            dbo.EMCompany AS EMCompany_3 WITH (NOLOCK)) AS A_3 INNER JOIN
                                                       (SELECT        Employee, CustPracticeAreaLeader AS Practice
                                                         FROM            dbo.Employees_PracticeAreas WITH (NOLOCK)) AS B_3 ON B_3.Employee = A_3.Employee
                          UNION
                          SELECT        A_2.Employee, A_2.Org, A_2.Office, NULL AS Practice, B_2.Team, NULL AS OfficeDelegate, NULL AS PracticeDelegate, NULL AS TeamDelegate
                          FROM            (SELECT        Employee, Org, NULL AS Office
                                                    FROM            dbo.EMCompany AS EMCompany_2 WITH (NOLOCK)) AS A_2 INNER JOIN
                                                       (SELECT        CustTeamLeader AS Employee, SUBSTRING(CustOrganization, 1, 3) + '%' + SUBSTRING(CustOrganization, 6, 4) AS Team
                                                         FROM            dbo.UDIC_OrganizationStructure WITH (NOLOCK)) AS B_2 ON B_2.Employee = A_2.Employee
                          UNION
                          SELECT        A_1.Employee, A_1.Org, A_1.Office, NULL AS Practice, NULL AS Team, B_1.Office AS OfficeDelegate, NULL AS PracticeDelegate, NULL AS TeamDelegate
                          FROM            (SELECT        Employee, Org, NULL AS Office
                                                    FROM            dbo.EMCompany AS EMCompany_1 WITH (NOLOCK)) AS A_1 INNER JOIN
                                                       (SELECT        Employee, Office
                                                         FROM            dbo.HAI_TSR_Permissions WITH (NOLOCK)
                                                         WHERE        (Office IS NOT NULL)) AS B_1 ON B_1.Employee = A_1.Employee
                          UNION
                          SELECT        A_1_2.Employee, B_1_2.Org, A_1_2.Office, NULL AS Practice, NULL AS Team, NULL AS OfficeDelegate, B_1_2.Practice AS PracticeDelegate, NULL AS TeamDelegate
                          FROM            (SELECT        Employee, Org, NULL AS Office
                                                    FROM            dbo.EMCompany AS EMCompany_1 WITH (NOLOCK)) AS A_1_2 INNER JOIN
                                                       (SELECT        Employee, Team AS Org, Practice
                                                         FROM            dbo.HAI_TSR_Permissions AS HAI_TSR_Permissions_2 WITH (NOLOCK)
                                                         WHERE        (Practice IS NOT NULL)) AS B_1_2 ON B_1_2.Employee = A_1_2.Employee
                          UNION
                          SELECT        A_1_1.Employee, A_1_1.Org, A_1_1.Office, NULL AS Practice, NULL AS Team, NULL AS OfficeDelegate, NULL AS PracticeDelegate, B_1_1.Team AS TeamDelegate
                          FROM            (SELECT        Employee, Org, NULL AS Office
                                                    FROM            dbo.EMCompany AS EMCompany_1 WITH (NOLOCK)) AS A_1_1 INNER JOIN
                                                       (SELECT        Employee, Team
                                                         FROM            dbo.HAI_TSR_Permissions AS HAI_TSR_Permissions_1 WITH (NOLOCK)
                                                         WHERE        (Team IS NOT NULL) AND (LEN(Team) > 2)) AS B_1_1 ON B_1_1.Employee = A_1_1.Employee) AS alpha
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "alpha"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'HAI_EmployeePermissions', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'HAI_EmployeePermissions', NULL, NULL
GO
