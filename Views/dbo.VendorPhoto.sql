SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE View  [dbo].[VendorPhoto] as
Select
	ClientPhoto.PKey,
	Clendor.Vendor AS VendorID,
	ClientPhoto.Photo,
	ClientPhoto.CreateUser,
	ClientPhoto.CreateDate,
	ClientPhoto.ModUser,
	ClientPhoto.ModDate
	from ClientPhoto
	inner join Clendor on ClientPhoto.ClientID = Clendor.ClientID
	Where Clendor.Vendor is not null and Clendor.VendorInd = 'Y'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteVendorPhotoTrigger] 
   ON  [dbo].[VendorPhoto]
instead of Delete
AS 
BEGIN
	Set nocount on;
    Delete from [ClientPhoto] where  exists (select 'x' from deleted left join Clendor on deleted.VendorID = Clendor.Vendor)
END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertVendorPhotoTrigger]
   ON  [dbo].[VendorPhoto]
instead of Insert
as
BEGIN
	set nocount on;
	INSERT INTO [ClientPhoto]
	   (ClientID,
		Photo,
		CreateUser,
		CreateDate,
		ModUser,
		ModDate)
	SELECT 
		Clendor.ClientID,
		i.Photo,
		i.CreateUser,
		i.CreateDate,
		i.ModUser,
		i.ModDate
	FROM inserted i inner  join Clendor on i.VendorID = Clendor.Vendor

end
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateVendorPhotoTrigger] 
   ON  [dbo].[VendorPhoto]
instead of Update
as
BEGIN
SET NOCOUNT ON;
if Not (Update(VendorID))
	begin
		UPDATE [ClientPhoto] SET
			ClientID = Clendor.ClientID,
			Photo = i.Photo,
			CreateUser = i.CreateUser,
			CreateDate = i.CreateDate,
			ModUser = i.ModUser,
			ModDate = i.ModDate
		FROM inserted i
			inner join Clendor on Clendor.Vendor = i.VendorID 
			where [ClientPhoto].Photo = i.Photo and 
			[ClientPhoto].clientid = Clendor.ClientID
	end
else
  BEGIN
  		UPDATE [ClientPhoto] SET
			ClientID = ClendorNew.ClientID,
			Photo = i.Photo,
			CreateUser = i.CreateUser,
			CreateDate = i.CreateDate,
			ModUser = i.ModUser,
			ModDate = i.ModDate
		FROM inserted i
			inner join deleted d on i.PKey = d.PKey
			inner join Clendor ClendorOld on ClendorOld.Vendor = d.VendorID 
			left join Clendor ClendorNew on ClendorNew.Vendor = i.VendorID 
		where [ClientPhoto].clientid = ClendorOld.ClientID
  END
END

GO
