SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGEEOCategory]
AS
SELECT
	dbo.CFGEEOCategoryData.Code,
	dbo.CFGEEOCategoryDescriptions.[Description],
	dbo.CFGEEOCategoryDescriptions.Seq,
	dbo.CFGEEOCategoryData.Status,
	dbo.CFGEEOCategoryData.Predefined
FROM dbo.CFGEEOCategoryData
LEFT JOIN dbo.CFGEEOCategoryDescriptions ON dbo.CFGEEOCategoryDescriptions.Code = dbo.CFGEEOCategoryData.Code
	AND dbo.CFGEEOCategoryDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGEEOCategoryTrigger]
	ON  [dbo].[CFGEEOCategory]
INSTEAD OF DELETE
AS
BEGIN
	SET NOCOUNT ON;
             
	DECLARE @Code NVARCHAR(10)
 
	SELECT TOP 1 @Code = Code FROM Deleted
 
	DELETE FROM CFGEEOCategoryDescriptions WHERE Code = @Code
	DELETE FROM CFGEEOCategoryData WHERE Code = @Code
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[InsertCFGEEOCategoryTrigger]
	ON  [dbo].[CFGEEOCategory]
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON;
 
	INSERT INTO [CFGEEOCategoryData] ([Code], [Status], [Predefined])
	SELECT [Code], [Status], ISNULL([Predefined], 'N') FROM Inserted
 
	INSERT INTO [CFGEEOCategoryDescriptions] ([Code], Description, UICultureName, Seq)
	SELECT [Code], [Description], dbo.FW_GetActiveCultureName(), ISNULL(Seq, 0) FROM Inserted
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[UpdateCFGEEOCategoryTrigger]
	ON  [dbo].[CFGEEOCategory]
INSTEAD OF UPDATE
AS
BEGIN
	SET NOCOUNT ON;
 
	DECLARE @Code NVARCHAR(10)
 
	IF UPDATE(Code)
		SELECT @Code = Code FROM Deleted
	ELSE
		SELECT @Code = Code FROM Inserted
 
	UPDATE CFGEEOCategoryData
	SET Code = I.Code,
		Status = I.Status,
		Predefined = ISNULL(I.Predefined, 'N')
	FROM Inserted I
	INNER JOIN CFGEEOCategoryData on CFGEEOCategoryData.Code = @Code
 
	IF (NOT EXISTS(
		SELECT CFGEEOCategoryDescriptions.UICultureName
		FROM CFGEEOCategoryDescriptions
		WHERE CFGEEOCategoryDescriptions.Code = @Code
			AND CFGEEOCategoryDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
	))
		INSERT INTO CFGEEOCategoryDescriptions ([Code], [UICultureName], [Description], [Seq])
		SELECT [Code], dbo.FW_GetActiveCultureName(), [Description], ISNULL([Seq], 0) FROM Inserted
	ELSE
		UPDATE CFGEEOCategoryDescriptions
		SET Code = I.Code,
			[Description] = I.Description,
			[Seq] = I.Seq
		FROM Inserted I
		INNER JOIN CFGEEOCategoryDescriptions ON CFGEEOCategoryDescriptions.Code = @Code
			AND CFGEEOCategoryDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
END
GO
