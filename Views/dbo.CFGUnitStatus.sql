SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGUnitStatus]
as
SELECT dbo.CFGUnitStatusData.Code, dbo.CFGUnitStatusDescriptions.Description 
FROM         dbo.CFGUnitStatusData Left JOIN
dbo.CFGUnitStatusDescriptions ON dbo.CFGUnitStatusData.Code = dbo.CFGUnitStatusDescriptions.Code 
and dbo.CFGUnitStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGUnitStatusTrigger] 
   ON  [dbo].[CFGUnitStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGUnitStatusDescriptions where Code = @Code
Delete from CFGUnitStatusData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGUnitStatusTrigger] 
   ON  [dbo].[CFGUnitStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGUnitStatusData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGUnitStatusDescriptions]
  ([Code] 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGUnitStatusTrigger] 
   ON  [dbo].[CFGUnitStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGUnitStatusData
  set Code = i.Code
from inserted i
  inner join CFGUnitStatusData on CFGUnitStatusData.Code = @Code

if (not exists(Select CFGUnitStatusDescriptions.UICultureName from CFGUnitStatusDescriptions
  where CFGUnitStatusDescriptions.Code = @Code and CFGUnitStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGUnitStatusDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGUnitStatusDescriptions
      Set Code = i.Code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGUnitStatusDescriptions on CFGUnitStatusDescriptions.Code = @Code and CFGUnitStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
