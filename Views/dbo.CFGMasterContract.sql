SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGMasterContract]
as
SELECT dbo.CFGMasterContractData.Code, dbo.CFGMasterContractDescriptions.Description, dbo.CFGMasterContractDescriptions.Seq
FROM         dbo.CFGMasterContractData Left JOIN
dbo.CFGMasterContractDescriptions ON dbo.CFGMasterContractData.Code = dbo.CFGMasterContractDescriptions.Code 
and dbo.CFGMasterContractDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGMasterContractTrigger] 
   ON  [dbo].[CFGMasterContract]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGMasterContractDescriptions where code = @Code
Delete from CFGMasterContractData where code = @Code

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGMasterContractTrigger] 
   ON  [dbo].[CFGMasterContract]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO CFGMasterContractData
  (Code)
Select Code from inserted

INSERT INTO CFGMasterContractDescriptions
  (Code 
  ,Description
  ,UICultureName
  ,Seq)
Select Code,Description, dbo.FW_GetActiveCultureName(),@seq from inserted

End

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGMasterContractTrigger] 
   ON  [dbo].[CFGMasterContract]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGMasterContractData
  set code = i.code
from inserted i
  inner join CFGMasterContractData on CFGMasterContractData.code = @Code

if (not exists(Select CFGMasterContractDescriptions.UICultureName from CFGMasterContractDescriptions
  where CFGMasterContractDescriptions.code = @code and CFGMasterContractDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGMasterContractDescriptions
      (Code
      ,UICultureName
      ,Description
      ,Seq)
  Select Code, dbo.FW_GetActiveCultureName(),Description,Seq From Inserted 

Else
  Update CFGMasterContractDescriptions
      Set code = i.code
      , Description = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGMasterContractDescriptions on CFGMasterContractDescriptions.code = @code and CFGMasterContractDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END

GO
