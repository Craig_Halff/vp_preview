SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGCampaignStatus]
as
SELECT dbo.CFGCampaignStatusData.Code, dbo.CFGCampaignStatusDescriptions.Description, dbo.CFGCampaignStatusDescriptions.Seq
  FROM dbo.CFGCampaignStatusData Left JOIN
       dbo.CFGCampaignStatusDescriptions ON dbo.CFGCampaignStatusData.Code = dbo.CFGCampaignStatusDescriptions.Code
   and dbo.CFGCampaignStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGCampaignStatusTrigger] 
   ON  [dbo].[CFGCampaignStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGCampaignStatusDescriptions where code = @Code
Delete from CFGCampaignStatusData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGCampaignStatusTrigger] 
   ON  [dbo].[CFGCampaignStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGCampaignStatusData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGCampaignStatusDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGCampaignStatusTrigger] 
   ON  [dbo].[CFGCampaignStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGCampaignStatusData
  set code = i.code
from inserted i
  inner join CFGCampaignStatusData on CFGCampaignStatusData.code = @Code

if (not exists(Select CFGCampaignStatusDescriptions.UICultureName from CFGCampaignStatusDescriptions
  where CFGCampaignStatusDescriptions.code = @code and CFGCampaignStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGCampaignStatusDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGCampaignStatusDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGCampaignStatusDescriptions on CFGCampaignStatusDescriptions.code = @code and CFGCampaignStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
