SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGProbability]
as
SELECT dbo.CFGProbabilityData.Probability, dbo.CFGProbabilityDescriptions.Description, dbo.CFGProbabilityDescriptions.Seq
FROM         dbo.CFGProbabilityData Left JOIN
dbo.CFGProbabilityDescriptions ON dbo.CFGProbabilityData.Probability = dbo.CFGProbabilityDescriptions.Probability 
and dbo.CFGProbabilityDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGProbabilityTrigger] 
   ON  [dbo].[CFGProbability]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Probability FROM Deleted

Delete from CFGProbabilityDescriptions where Probability = @Code
Delete from CFGProbabilityData where Probability = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGProbabilityTrigger] 
   ON  [dbo].[CFGProbability]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGProbabilityData]
  ([Probability])
Select [Probability] from inserted

INSERT INTO [CFGProbabilityDescriptions]
  ([Probability] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Probability],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGProbabilityTrigger] 
   ON  [dbo].[CFGProbability]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Probability)
  select @Code=Probability from deleted
else
  select @Code=Probability from inserted

Update CFGProbabilityData
  set Probability = i.Probability
from inserted i
  inner join CFGProbabilityData on CFGProbabilityData.Probability = @Code

if (not exists(Select CFGProbabilityDescriptions.UICultureName from CFGProbabilityDescriptions
  where CFGProbabilityDescriptions.Probability = @code and CFGProbabilityDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGProbabilityDescriptions
      ([Probability]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Probability], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGProbabilityDescriptions
      Set Probability = i.Probability
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGProbabilityDescriptions on CFGProbabilityDescriptions.Probability = @code and CFGProbabilityDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
