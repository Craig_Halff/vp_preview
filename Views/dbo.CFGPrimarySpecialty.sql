SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGPrimarySpecialty]
as
SELECT dbo.CFGPrimarySpecialtyData.Code, dbo.CFGPrimarySpecialtyDescriptions.Description, dbo.CFGPrimarySpecialtyDescriptions.Seq
FROM         dbo.CFGPrimarySpecialtyData Left JOIN
dbo.CFGPrimarySpecialtyDescriptions ON dbo.CFGPrimarySpecialtyData.Code = dbo.CFGPrimarySpecialtyDescriptions.Code 
and dbo.CFGPrimarySpecialtyDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGPrimarySpecialtyTrigger] 
   ON  [dbo].[CFGPrimarySpecialty]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGPrimarySpecialtyDescriptions where code = @Code
Delete from CFGPrimarySpecialtyData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGPrimarySpecialtyTrigger] 
   ON  [dbo].[CFGPrimarySpecialty]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGPrimarySpecialtyData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGPrimarySpecialtyDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGPrimarySpecialtyTrigger] 
   ON  [dbo].[CFGPrimarySpecialty]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGPrimarySpecialtyData
  set code = i.code
from inserted i
  inner join CFGPrimarySpecialtyData on CFGPrimarySpecialtyData.code = @Code

if (not exists(Select CFGPrimarySpecialtyDescriptions.UICultureName from CFGPrimarySpecialtyDescriptions
  where CFGPrimarySpecialtyDescriptions.code = @code and CFGPrimarySpecialtyDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGPrimarySpecialtyDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGPrimarySpecialtyDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGPrimarySpecialtyDescriptions on CFGPrimarySpecialtyDescriptions.code = @code and CFGPrimarySpecialtyDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
