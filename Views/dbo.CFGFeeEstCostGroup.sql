SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGFeeEstCostGroup]
as
SELECT dbo.CFGFeeEstCostGroupData.Code,dbo.CFGFeeEstCostGroupData.Disabled, ISNULL(dbo.CFGFeeEstCostGroupDescriptions.Description, dbo.FW_TextNotTranslated.LocalizedValue) AS Description
FROM         dbo.CFGFeeEstCostGroupData Left JOIN
dbo.CFGFeeEstCostGroupDescriptions ON dbo.CFGFeeEstCostGroupData.Code = dbo.CFGFeeEstCostGroupDescriptions.Code 
and dbo.CFGFeeEstCostGroupDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
	 LEFT JOIN dbo.FW_TextNotTranslated ON dbo.FW_TextNotTranslated.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGFeeEstCostGroupTrigger] 
   ON  [dbo].[CFGFeeEstCostGroup]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @code Nvarchar(100)

SELECT top 1 @code = code FROM Deleted

Delete from CFGFeeEstCostGroupDescriptions where CFGFeeEstCostGroupDescriptions.code = @code
Delete from CFGFeeEstCostGroupData where CFGFeeEstCostGroupData.code = @code 

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[InsertCFGFeeEstCostGroupTrigger] 
   ON  [dbo].[CFGFeeEstCostGroup]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;


INSERT INTO CFGFeeEstCostGroupData
  (code, Disabled )
Select Code, Disabled from inserted

INSERT INTO CFGFeeEstCostGroupDescriptions
  (code, UICultureName, Description)
Select Code, dbo.FW_GetActiveCultureName(), Description from inserted

End

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[UpdateCFGFeeEstCostGroupTrigger] 
   ON  [dbo].[CFGFeeEstCostGroup]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @code Nvarchar(100)

if update(code)
  select @code=code from deleted
else
  select @code=code from inserted

Update CFGFeeEstCostGroupData
  set code = i.code, Disabled=i.Disabled
from inserted i
  inner join CFGFeeEstCostGroupData on CFGFeeEstCostGroupData.code = @code

if (not exists(Select CFGFeeEstCostGroupDescriptions.UICultureName from CFGFeeEstCostGroupDescriptions, Inserted I
  where CFGFeeEstCostGroupDescriptions.code = @code and CFGFeeEstCostGroupDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGFeeEstCostGroupDescriptions
      (code,
       UICultureName,
         Description)
  Select code, dbo.FW_GetActiveCultureName(),Description From Inserted 

Else
  Update CFGFeeEstCostGroupDescriptions
      Set code = i.code,
          Description = i.Description
  From inserted i
  Inner Join CFGFeeEstCostGroupDescriptions on CFGFeeEstCostGroupDescriptions.code = @code and CFGFeeEstCostGroupDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END

GO
