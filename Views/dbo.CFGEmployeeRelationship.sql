SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGEmployeeRelationship]
as
SELECT dbo.CFGEmployeeRelationshipData.Code, dbo.CFGEmployeeRelationshipDescriptions.Description, dbo.CFGEmployeeRelationshipDescriptions.Seq
FROM         dbo.CFGEmployeeRelationshipData Left JOIN
dbo.CFGEmployeeRelationshipDescriptions ON dbo.CFGEmployeeRelationshipData.Code = dbo.CFGEmployeeRelationshipDescriptions.Code 
and dbo.CFGEmployeeRelationshipDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGEmployeeRelationshipTrigger] 
   ON  [dbo].[CFGEmployeeRelationship]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGEmployeeRelationshipDescriptions where code = @Code
Delete from CFGEmployeeRelationshipData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGEmployeeRelationshipTrigger] 
   ON  [dbo].[CFGEmployeeRelationship]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGEmployeeRelationshipData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGEmployeeRelationshipDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGEmployeeRelationshipTrigger] 
   ON  [dbo].[CFGEmployeeRelationship]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGEmployeeRelationshipData
  set code = i.code
from inserted i
  inner join CFGEmployeeRelationshipData on CFGEmployeeRelationshipData.code = @Code

if (not exists(Select CFGEmployeeRelationshipDescriptions.UICultureName from CFGEmployeeRelationshipDescriptions
  where CFGEmployeeRelationshipDescriptions.code = @code and CFGEmployeeRelationshipDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGEmployeeRelationshipDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGEmployeeRelationshipDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGEmployeeRelationshipDescriptions on CFGEmployeeRelationshipDescriptions.code = @code and CFGEmployeeRelationshipDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
