SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGCampaignType]
as
SELECT dbo.CFGCampaignTypeData.Code, dbo.CFGCampaignTypeDescriptions.Description, dbo.CFGCampaignTypeDescriptions.Seq
  FROM dbo.CFGCampaignTypeData Left JOIN
       dbo.CFGCampaignTypeDescriptions ON dbo.CFGCampaignTypeData.Code = dbo.CFGCampaignTypeDescriptions.Code
   and dbo.CFGCampaignTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGCampaignTypeTrigger] 
   ON  [dbo].[CFGCampaignType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGCampaignTypeDescriptions where code = @Code
Delete from CFGCampaignTypeData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGCampaignTypeTrigger] 
   ON  [dbo].[CFGCampaignType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGCampaignTypeData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGCampaignTypeDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGCampaignTypeTrigger] 
   ON  [dbo].[CFGCampaignType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGCampaignTypeData
  set code = i.code
from inserted i
  inner join CFGCampaignTypeData on CFGCampaignTypeData.code = @Code

if (not exists(Select CFGCampaignTypeDescriptions.UICultureName from CFGCampaignTypeDescriptions
  where CFGCampaignTypeDescriptions.code = @code and CFGCampaignTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGCampaignTypeDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGCampaignTypeDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGCampaignTypeDescriptions on CFGCampaignTypeDescriptions.code = @code and CFGCampaignTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
