SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGCommonStatus]
as
SELECT dbo.CFGCommonStatusData.Code, dbo.CFGCommonStatusDescriptions.Description 
FROM         dbo.CFGCommonStatusData Left JOIN
dbo.CFGCommonStatusDescriptions ON dbo.CFGCommonStatusData.Code = dbo.CFGCommonStatusDescriptions.Code 
and dbo.CFGCommonStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGCommonStatusTrigger]
   ON  [dbo].[CFGCommonStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGCommonStatusDescriptions where Code = @Code
Delete from CFGCommonStatusData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGCommonStatusTrigger] 
   ON  [dbo].[CFGCommonStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGCommonStatusData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGCommonStatusDescriptions]
  ([Code] ,Description,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGCommonStatusTrigger] 
   ON  [dbo].[CFGCommonStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGCommonStatusData
  set Code = i.Code
from inserted i
  inner join CFGCommonStatusData on CFGCommonStatusData.Code = @Code

if (not exists(Select CFGCommonStatusDescriptions.UICultureName from CFGCommonStatusDescriptions
  where CFGCommonStatusDescriptions.Code = @Code and CFGCommonStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGCommonStatusDescriptions
      ([Code],[UICultureName],[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGCommonStatusDescriptions
      Set [Code] = i.[Code], [Description] = i.[Description]
  From inserted i
  Inner Join CFGCommonStatusDescriptions on CFGCommonStatusDescriptions.Code = @Code and CFGCommonStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
