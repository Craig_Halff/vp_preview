SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[TransactionUDF] 
AS 
SELECT dbo.TransactionUDFData.UDFID, 
       dbo.TransactionUDFData.TransType, 
       dbo.TransactionUDFData.Company, 
       dbo.TransactionUDFData.Status,
       dbo.TransactionUDFData.Required, 
       dbo.TransactionUDFData.FieldType, 
       dbo.TransactionUDFData.LimitToList, 
       dbo.TransactionUDFData.Decimals, 
       dbo.TransactionUDFData.MinValue, 
       dbo.TransactionUDFData.MaxValue, 
       dbo.TransactionUDFData.DefaultValue,           
       dbo.TransactionUDFLabels.UICultureName, 
       dbo.TransactionUDFLabels.Label, 
       dbo.TransactionUDFLabels.ToolTip 
  FROM dbo.TransactionUDFData 
  LEFT JOIN dbo.TransactionUDFLabels on dbo.TransactionUDFData.UDFID = dbo.TransactionUDFLabels.UDFID
   AND dbo.TransactionUDFData.TransType = dbo.TransactionUDFLabels.TransType
   AND dbo.transactionUDFData.Company = dbo.TransactionUDFLabels.Company 
   AND dbo.TransactionUDFLabels.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteTransactionUDFTrigger] 
   ON  [dbo].[TransactionUDF]
INSTEAD OF DELETE
AS 
BEGIN
SET NOCOUNT ON;

Delete from TransactionUDFLabels From Deleted
where TransactionUDFLabels.UDFID = Deleted.UDFID 
  and TransactionUDFLabels.Transtype = Deleted.TransType 
  and TransactionUDFLabels.Company = Deleted.Company

Delete from TransactionUDFData From Deleted
where TransactionUDFData.UDFID = Deleted.UDFID 
  and TransactionUDFData.TransType = Deleted.TransType
  and TransactionUDFData.Company = Deleted.Company 

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertTransactionUDFTrigger] 
   ON  [dbo].[TransactionUDF]
INSTEAD OF Insert
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @UDFID as nvarchar(125)
    DECLARE @TransType as nvarchar(2)
    DECLARE @Company as nvarchar(1)
    Declare @UICultureName as nvarchar(10)

    SELECT @UDFID = UDFID,
           @TransType = TransType, 
           @Company = Company, 
           @UICultureName = UICultureName
      FROM Inserted 

    INSERT INTO [TransactionUDFData] 
      (UDFID, TransType, Status, Required, FieldType, Company, LimitToList, Decimals, MinValue, MaxValue, DefaultValue) 
    Select UDFID, isnull(TransType, 'TS'), isnull(Status, 'A'), isnull(Required, 'N'), FieldType, Company, isnull(LimitToList,'Y'), Decimals, MinValue, MaxValue, DefaultValue from inserted 

    INSERT INTO [TransactionUDFLabels]
      (UDFID, TransType, Company, UICultureName, Label, ToolTip)
    Select UDFID, isnull(Transtype, 'TS'), Company, UICultureName, Label, ToolTip from inserted

End
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateTransactionUDFTrigger] 
   ON  [dbo].[TransactionUDF]
INSTEAD OF UPDATE
AS 
BEGIN
SET NOCOUNT ON;

DECLARE @UDFID     Nvarchar(125)
DECLARE @TransType Nvarchar(2)                                            
DECLARE @Company   Nvarchar(14)

if update(UDFID)
  select @UDFID=UDFID, @TransType=TransType, @Company=Company from deleted
else
  select @UDFID=UDFID, @TransType=TransType, @Company=Company from inserted

Update TransactionUDFData
   set  UDFID = i.UDFID,
        TransType = i.TransType, 
        Status = i.Status,
        Required = i.Required, 
        FieldType = i.FieldType, 
        Company = i.Company,
        LimitToList = i.LimitToList, 
        Decimals = i.Decimals, 
        MinValue = i.MinValue, 
        MaxValue = i.MaxValue, 
        DefaultValue = i.DefaultValue 
   from inserted i
   inner join TransactionUDFData on TransactionUDFData.Company = @Company and TransactionUDFData.UDFID = @UDFID and TransactionUDFData.TransType = @TransType 

if (not exists(Select TransactionUDFLabels.UICultureName from TransactionUDFLabels, Inserted I
  where TransactionUDFLabels.TransType = @TransType and TransactionUDFLabels.Company = @Company and TransactionUDFLabels.UDFID = @UDFID 
  and TransactionUDFLabels.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into TransactionUDFLabels
      (UDFID,
       TransType, 
       Company,
       UICultureName,
       Label, 
       ToolTip)
  Select UDFID, TransType, Company, dbo.FW_GetActiveCultureName(), Label, ToolTip From Inserted 

Else
  Update TransactionUDFLabels
      Set UDFID = i.UDFID,
          TransType = i.TransTYpe, 
          Company = i.Company,
          Label = i.Label, 
          ToolTip = i.ToolTip 
  From inserted i
  Inner Join TransactionUDFLabels on TransactionUDFLabels.TransType = i.TransType and TransactionUDFLabels.Company = i.Company 
  and TransactionUDFLabels.UDFID = @UDFID and TransactionUDFLabels.UICultureName=dbo.FW_GetActiveCultureName() 

END
GO
