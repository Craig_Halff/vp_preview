SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGEMSkill]
as
SELECT dbo.CFGEMSkillData.CodeSF330, dbo.CFGEMSkillData.Code, dbo.CFGEMSkillDescriptions.Description, dbo.CFGEMSkillDescriptions.Seq
FROM         dbo.CFGEMSkillData Left JOIN
dbo.CFGEMSkillDescriptions ON dbo.CFGEMSkillData.Code = dbo.CFGEMSkillDescriptions.Code 
and dbo.CFGEMSkillDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGEMSkillTrigger] 
   ON  [dbo].[CFGEMSkill]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGEMSkillDescriptions where code = @Code
Delete from CFGEMSkillData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGEMSkillTrigger] 
   ON  [dbo].[CFGEMSkill]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGEMSkillData]
  ([Code],CodeSF330)
Select [Code],CodeSF330 from inserted

INSERT INTO [CFGEMSkillDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),isnull(seq,0) from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGEMSkillTrigger] 
   ON  [dbo].[CFGEMSkill]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGEMSkillData
  set code = i.code, CodeSF330=i.CodeSF330
from inserted i
  inner join CFGEMSkillData on CFGEMSkillData.code = @Code

if (not exists(Select CFGEMSkillDescriptions.UICultureName from CFGEMSkillDescriptions
  where CFGEMSkillDescriptions.code = @code and CFGEMSkillDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGEMSkillDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],isnull(Seq,0) From Inserted 

Else
  Update CFGEMSkillDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGEMSkillDescriptions on CFGEMSkillDescriptions.code = @code and CFGEMSkillDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
