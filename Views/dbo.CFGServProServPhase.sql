SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGServProServPhase]
as
SELECT	dbo.CFGServProServPhaseData.ServProCode, dbo.CFGServProServPhaseData.Code,dbo.CFGServProServPhaseData.StdPct, 
	dbo.CFGServProServPhaseData.Disabled, ISNULL(dbo.CFGServProServPhaseDescriptions.Description, dbo.FW_TextNotTranslated.LocalizedValue) AS Description
  FROM	dbo.CFGServProServPhaseData Left JOIN 
	dbo.CFGServProServPhaseDescriptions ON dbo.CFGServProServPhaseData.ServProCode = dbo.CFGServProServPhaseDescriptions.ServProCode and
	dbo.CFGServProServPhaseData.Code = dbo.CFGServProServPhaseDescriptions.Code and 
	dbo.CFGServProServPhaseDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
	 LEFT JOIN dbo.FW_TextNotTranslated ON dbo.FW_TextNotTranslated.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGServProServPhaseTrigger] 
   ON [dbo].[CFGServProServPhase]
instead of Delete
AS 
BEGIN
   SET NOCOUNT ON;

   DECLARE @ServProCode Nvarchar(10)
   DECLARE @Code Nvarchar(7)

   SELECT top 1 @ServProCode = ServProCode, @Code = Code FROM Deleted

   DELETE from CFGServProServPhaseDescriptions where CFGServProServPhaseDescriptions.ServProCode = @ServProCode and CFGServProServPhaseDescriptions.Code = @Code
   DELETE from CFGServProServPhaseData where CFGServProServPhaseData.ServProCode = @ServProCode and CFGServProServPhaseData.Code = @Code 

END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[InsertCFGServProServPhaseTrigger] 
   ON [dbo].[CFGServProServPhase]
instead of Insert
as
BEGIN
   SET NOCOUNT ON;
 
   INSERT INTO CFGServProServPhaseData(ServProCode, Code, StdPct, Disabled )
   SELECT ServProCode, Code, StdPct, Disabled from inserted

   INSERT INTO CFGServProServPhaseDescriptions(ServProCode, Code, UICultureName, Description)
   SELECT ServProCode, Code, dbo.FW_GetActiveCultureName(), Description from inserted

END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[UpdateCFGServProServPhaseTrigger] 
   ON [dbo].[CFGServProServPhase]
instead of Update
as
BEGIN
   SET NOCOUNT ON;

   DECLARE @ServProCode Nvarchar(10)
   DECLARE @Code Nvarchar(7)

   if update(Code)
	select @ServProCode = ServProCode, @Code=Code from deleted
   else
	select @ServProCode = ServProCode, @Code=Code from inserted

   UPDATE CFGServProServPhaseData
      SET ServProCode = i.ServProCode, Code = i.Code, StdPct = i.StdPct, Disabled = i.Disabled
     FROM inserted i
   inner join CFGServProServPhaseData on CFGServProServPhaseData.ServProCode = @ServProCode and CFGServProServPhaseData.Code = @Code

   if not exists(Select CFGServProServPhaseDescriptions.UICultureName from CFGServProServPhaseDescriptions, Inserted I where CFGServProServPhaseDescriptions.ServProCode = @ServProCode and CFGServProServPhaseDescriptions.Code = @Code and CFGServProServPhaseDescriptions.UICultureName =dbo.FW_GetActiveCultureName())
	INSERT INTO CFGServProServPhaseDescriptions (ServProCode, Code, UICultureName, Description)
	Select ServProCode, Code, dbo.FW_GetActiveCultureName(),Description From Inserted 
   else
	Update CFGServProServPhaseDescriptions
	   Set ServProCode = i.ServProCode, Code = i.Code, Description = i.Description
	  From inserted i
	Inner Join CFGServProServPhaseDescriptions on CFGServProServPhaseDescriptions.ServProCode = @ServProCode and CFGServProServPhaseDescriptions.Code = @Code and CFGServProServPhaseDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END

GO
