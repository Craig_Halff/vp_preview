SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGBillMain] AS
SELECT
    CFGBillMainData.Company,
    PrintName,
    PrintByline,
    PrintAddress,
    PrintWBS1No,
    PrintInvoiceNo,
    CombineWBS1Invoice,
    PrintWBS1Name,
    PrintAROnly,
    AddressStart,
    Page2Start,
    NumberMeth,
    NextInvoice,
    NextInvoiceMods,
    NonBillLaborCode,
    ReleaseHolds,
    UserEnteredPriorFee,
    LabAccount,
    ConAccount,
    ExpAccount,
    UnitAccount,
    FeeAccount,
    AddOnAccount,
    IntAccount,
    PrintTotalNowDue,
    ExtendAddOn,
    ShowNonbillables,
    ShowNonbillablesMods,
    DisableFinalLimits,
    DisableFinalLimitsMods,
    EffectiveDateEnabled,
    InvoiceAddressee,
    PrintContactTitle,
    PrintContactPrefix,
    PrintAssignedInvoiceNo,
    ScheduledBilling,
    FeeByCategoryEnabled,
    FeeByDetailEnabled,
    FeeByDetailMethod,
    FeeByDetailOverUnderAccount,
    FeeByDetailLabor,
    FeeByDetailDirCons,
    FeeByDetailReimbCons,
    FeeByDetailDirExp,
    FeeByDetailReimbExp,
    FeeByDetailDirUnit,
    FeeByDetailReimbUnit,
    InvoiceApprovalEnabled,
    DefaultInvoiceApprovalCode,
    PreInvoiceEnabled,
    PreInvoiceRecAccount,
    FooterMessage,
    FeeLabel,
    LabLabel,
    ConLabel,
    ExpLabel,
    UnitLabel,
    AddOnLabel,
    TaxLabel,
    InterestLabel,
    OvtIndicator,
    UseMarkupDraftInvoice,
    PrintContactFirstName,
    PrintContactPreferredName,
    PrintContactMiddleName,
    PrintContactLastName,
    PrintContactSuffix
FROM CFGBillMainData LEFT JOIN CFGBillMainDescriptions
    ON CFGBillMainData.Company = CFGBillMainDescriptions.Company
    AND CFGBillMainDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGBillMainTrigger] 
   ON  [dbo].[CFGBillMain]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Company Nvarchar(14)

if update(Company)
  select @Company=Company from deleted
else
  select @Company=Company from inserted

Update CFGBillMainData
  set Company = i.Company,
      PrintName = i.PrintName,
      PrintByline = i.PrintByline,
      PrintAddress = i.PrintAddress,
      PrintWBS1No = i.PrintWBS1No,
      PrintInvoiceNo = i.PrintInvoiceNo,
      CombineWBS1Invoice = i.CombineWBS1Invoice,
      PrintWBS1Name = i.PrintWBS1Name,
      PrintAROnly = i.PrintAROnly,
      AddressStart = i.AddressStart,
      Page2Start = i.Page2Start,
      NumberMeth = i.NumberMeth,
      NextInvoice = i.NextInvoice,
      NextInvoiceMods = i.NextInvoiceMods,
      NonBillLaborCode = i.NonBillLaborCode,
      ReleaseHolds = i.ReleaseHolds,
      UserEnteredPriorFee = i.UserEnteredPriorFee,
      LabAccount = i.LabAccount,
      ConAccount = i.ConAccount,
      ExpAccount = i.ExpAccount,
      UnitAccount = i.UnitAccount,
      FeeAccount = i.FeeAccount,
      AddOnAccount = i.AddOnAccount,
      IntAccount = i.IntAccount,
      PrintTotalNowDue = i.PrintTotalNowDue,
      ExtendAddOn = i.ExtendAddOn,
      ShowNonbillables = i.ShowNonbillables,
      ShowNonbillablesMods = i.ShowNonbillablesMods,
      DisableFinalLimits = i.DisableFinalLimits,
      DisableFinalLimitsMods = i.DisableFinalLimitsMods,
      EffectiveDateEnabled = i.EffectiveDateEnabled,
      InvoiceAddressee = i.InvoiceAddressee,
      PrintContactTitle = i.PrintContactTitle,
      PrintContactPrefix = i.PrintContactPrefix,
      PrintAssignedInvoiceNo = i.PrintAssignedInvoiceNo,
      ScheduledBilling = i.ScheduledBilling,
      FeeByCategoryEnabled = i.FeeByCategoryEnabled,
      FeeByDetailEnabled = i.FeeByDetailEnabled,
      FeeByDetailMethod = i.FeeByDetailMethod,
      FeeByDetailOverUnderAccount = i.FeeByDetailOverUnderAccount,
      FeeByDetailLabor = i.FeeByDetailLabor,
      FeeByDetailDirCons = i.FeeByDetailDirCons,
      FeeByDetailReimbCons = i.FeeByDetailReimbCons,
      FeeByDetailDirExp = i.FeeByDetailDirExp,
      FeeByDetailReimbExp = i.FeeByDetailReimbExp,
      FeeByDetailDirUnit = i.FeeByDetailDirUnit,
      FeeByDetailReimbUnit = i.FeeByDetailReimbUnit,
      InvoiceApprovalEnabled = i.InvoiceApprovalEnabled,
      DefaultInvoiceApprovalCode = i.DefaultInvoiceApprovalCode,
      PreInvoiceEnabled = i.PreInvoiceEnabled,
      PreInvoiceRecAccount = i.PreInvoiceRecAccount,
      UseMarkupDraftInvoice = i.UseMarkupDraftInvoice,
      PrintContactFirstName = i.PrintContactFirstName,
      PrintContactPreferredName = i.PrintContactPreferredName,
      PrintContactMiddleName = i.PrintContactMiddleName,
      PrintContactLastName = i.PrintContactLastName,
      PrintContactSuffix = i.PrintContactSuffix
from inserted i
  inner join CFGBillMainData on CFGBillMainData.Company = @Company

Update CFGBillMainDescriptions
    Set Company = i.Company,
        FooterMessage = i.FooterMessage,
        FeeLabel = i.FeeLabel,
        LabLabel = i.LabLabel,
        ConLabel = i.ConLabel,
        ExpLabel = i.ExpLabel,
        UnitLabel = i.UnitLabel,
        AddOnLabel = i.AddOnLabel,
        TaxLabel = i.TaxLabel,
        InterestLabel = i.InterestLabel,
        OvtIndicator = i.OvtIndicator
From inserted i
Inner Join CFGBillMainDescriptions on CFGBillMainDescriptions.Company = @Company and CFGBillMainDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
