SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[PRClientAssocTemplate]
AS
SELECT [PKey]
	,[ClendorProjectAssocTemplate].[ClientID]
	,[ClendorProjectAssocTemplate].[WBS1]
	,[ClendorProjectAssocTemplate].[PrimaryInd]
	,[ClendorProjectAssocTemplate].[Role]
	,[ClendorProjectAssocTemplate].[RoleDescription]
	,[ClendorProjectAssocTemplate].[Address]
	,[ClendorProjectAssocTemplate].[ClientConfidential]
	,[ClendorProjectAssocTemplate].[ClientInd]
	,[ClendorProjectAssocTemplate].[VendorInd]
	,[ClendorProjectAssocTemplate].[CreateUser]
	,[ClendorProjectAssocTemplate].[CreateDate]
	,[ClendorProjectAssocTemplate].[ModUser]
	,[ClendorProjectAssocTemplate].[ModDate]
FROM [ClendorProjectAssocTemplate]
JOIN Clendor ON [ClendorProjectAssocTemplate].[ClientID] = [Clendor].[ClientID]
	and [Clendor].[ClientInd] = 'Y'
	and [ClendorProjectAssocTemplate].[WBS2] = ' '
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeletePRClientAssocTemplateTrigger] 
   ON  [dbo].[PRClientAssocTemplate]
instead of Delete
AS 
BEGIN
    SET NOCOUNT ON;

DELETE FROM [ClendorProjectAssocTemplate]
WHERE EXISTS (
		SELECT 'x'
		FROM deleted
		JOIN [Clendor] ON deleted.[ClientID] = [Clendor].[ClientID]
		WHERE deleted.[wbs1] = ClendorProjectAssocTemplate.[wbs1]
			AND deleted.[ClientID] = [ClendorProjectAssocTemplate].[ClientID]
		)


END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertPRClientAssocTemplateTrigger] 
   ON  [dbo].[PRClientAssocTemplate]
instead of Insert
as
BEGIN
    SET NOCOUNT ON;
	INSERT INTO ClendorProjectAssocTemplate
	   ([clientid]
	   ,[WBS1]
	   ,[WBS2]
	   ,[WBS3]
	   ,[Role]
	   ,[RoleDescription]
	   ,[ClientConfidential]
	   ,[ClientInd]
	   ,[VendorInd]
	   ,[Address]
	   ,[CreateUser]
	   ,[CreateDate]
	   ,[ModUser]
	   ,[ModDate]
      )

	SELECT 
	  inserted.[ClientID]
	  ,inserted.[WBS1]
	  ,' ' AS [WBS2]
	  ,' ' AS [WBS3]
	  ,inserted.[Role]
	  ,inserted.[RoleDescription]
	  ,inserted.[ClientConfidential]
	  ,isnull(inserted.[ClientInd],'N')
	  ,isnull(inserted.[VendorInd],'N')
	  ,inserted.[Address]
	  ,inserted.[CreateUser]
	  ,isnull(inserted.[CreateDate],getutcdate())
	  ,inserted.[ModUser]
	  ,isnull(inserted.[ModDate],getutcdate())
	FROM inserted inner join [Clendor] on inserted.[ClientID] = [Clendor].[ClientID]

end
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdatePRClientAssocTemplateTrigger] 
   ON  [dbo].[PRClientAssocTemplate]
instead of Update
as
BEGIN
SET NOCOUNT ON;
if Not Update(ClientID) or Update(wbs1)
	begin
		UPDATE [ClendorProjectAssocTemplate] SET
			 [Role] = i.[Role]
			,[RoleDescription] = i.[RoleDescription]
			,[Address] = i.[Address]
    			,[ClientConfidential] = i.[ClientConfidential]
			,[ClientInd] = i.[ClientInd]
			,[VendorInd] = i.[VendorInd]
			,[CreateUser] = i.[CreateUser]
			,[CreateDate] = i.[CreateDate]
			,[ModUser] = i.[ModUser]
			,[ModDate] = i.[ModDate]
			FROM inserted i
			inner join [Clendor] on [Clendor].[ClientID] = i.[ClientID] 
			where [ClendorProjectAssocTemplate].[wbs1] = i.[wbs1] and 
			[ClendorProjectAssocTemplate].clientid = i.[ClientID]
	end
else
    BEGIN
		UPDATE [ClendorProjectAssocTemplate]
		SET  [wbs1] = i.[wbs1]
		,[ClientID] = ClendorNew.[ClientID]
		,[Role] = i.[Role]
		,[Address] = i.[Address]
		,[RoleDescription] = i.[RoleDescription]
    		,[ClientConfidential] = i.[ClientConfidential]
	        ,[ClientInd] = i.[ClientInd]
	        ,[VendorInd] = i.[VendorInd]
		,[CreateUser] = i.[CreateUser]
		,[CreateDate] = i.[CreateDate]
		,[ModUser] = i.[ModUser]
		,[ModDate] = i.[ModDate]
		FROM inserted i
		INNER JOIN deleted d ON i.[Pkey] = d.[PKey]
		INNER JOIN Clendor ClendorOld ON ClendorOld.ClientID = d.ClientID
		LEFT JOIN Clendor ClendorNew ON ClendorNew.ClientID = i.ClientID
		WHERE [ClendorProjectAssocTemplate].wbs1 = d.wbs1
			AND [ClendorProjectAssocTemplate].clientid = ClendorOld.ClientID

  END
END

GO
