SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGOrgCodes]
as
SELECT dbo.CFGOrgCodesData.Code, dbo.CFGOrgCodesData.OrgLevel, dbo.CFGOrgCodesData.CPOrg, dbo.CFGOrgCodesDescriptions.Label,dbo.CFGOrgCodesData.Status
FROM   dbo.CFGOrgCodesData
 Left JOIN dbo.CFGOrgCodesDescriptions ON dbo.CFGOrgCodesData.Code = dbo.CFGOrgCodesDescriptions.Code
and   dbo.CFGOrgCodesData.OrgLevel = dbo.CFGOrgCodesDescriptions.OrgLevel
and dbo.CFGOrgCodesDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGOrgCodesTrigger] 
   ON  [dbo].[CFGOrgCodes]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(14)
DECLARE @OrgLevel smallint

SELECT top 1 @Code = Code, @OrgLevel = OrgLevel FROM Deleted

Delete from CFGOrgCodesDescriptions where CFGOrgCodesDescriptions.Code = @Code and 
            CFGOrgCodesDescriptions.OrgLevel = @OrgLevel
Delete from CFGOrgCodesData where CFGOrgCodesData.Code = @Code and CFGOrgCodesData.OrgLevel = @OrgLevel

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGOrgCodesTrigger] 
   ON  [dbo].[CFGOrgCodes]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGOrgCodesData]
  (Code, OrgLevel, CPOrg, Status)
Select Code, isnull(OrgLevel,0), CPOrg, isnull(Status,'A') from inserted

INSERT INTO [CFGOrgCodesDescriptions]
  (Code, OrgLevel, Label, UICultureName)
Select Code, isnull(OrgLevel,0), Label, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGOrgCodesTrigger] 
   ON  [dbo].[CFGOrgCodes]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(14)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted


Update CFGOrgCodesData
  set code = i.code, OrgLevel=i.OrgLevel, CPOrg = i.CPorg, Status = i.Status
from inserted i
  inner join CFGOrgCodesData on CFGOrgCodesData.OrgLevel = i.orgLevel and CFGOrgCodesData.code = @Code

if (not exists(Select CFGOrgCodesDescriptions.UICultureName from CFGOrgCodesDescriptions, Inserted I
  where CFGOrgCodesDescriptions.OrgLevel = i.orgLevel and CFGOrgCodesDescriptions.code = @code and CFGOrgCodesDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGOrgCodesDescriptions
      (Code,
	   OrgLevel,
       UICultureName,
       Label)
  Select Code, isnull(OrgLevel,0), dbo.FW_GetActiveCultureName(), Label From Inserted 

Else
  Update CFGOrgCodesDescriptions
      Set code = i.code,
          orgLevel = i.OrgLevel,
          Label = i.Label
  From inserted i
  Inner Join CFGOrgCodesDescriptions on CFGOrgCodesDescriptions.OrgLevel = i.orgLevel and CFGOrgCodesDescriptions.code = @code and CFGOrgCodesDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
