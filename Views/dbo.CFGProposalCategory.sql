SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[CFGProposalCategory]
as
SELECT dbo.CFGProposalCategoryData.Code, dbo.CFGProposalCategoryDescriptions.Description, dbo.CFGProposalCategoryDescriptions.Seq
FROM         dbo.CFGProposalCategoryData Left JOIN
dbo.CFGProposalCategoryDescriptions ON dbo.CFGProposalCategoryData.Code = dbo.CFGProposalCategoryDescriptions.Code 
and dbo.CFGProposalCategoryDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGProposalCategoryTrigger] 
   ON  [dbo].[CFGProposalCategory]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGProposalCategoryDescriptions where code = @Code
Delete from CFGProposalCategoryData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGProposalCategoryTrigger] 
   ON  [dbo].[CFGProposalCategory]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGProposalCategoryData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGProposalCategoryDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGProposalCategoryTrigger] 
   ON  [dbo].[CFGProposalCategory]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGProposalCategoryData
  set code = i.code
from inserted i
  inner join CFGProposalCategoryData on CFGProposalCategoryData.code = @Code

if (not exists(Select CFGProposalCategoryDescriptions.UICultureName from CFGProposalCategoryDescriptions
  where CFGProposalCategoryDescriptions.code = @code and CFGProposalCategoryDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGProposalCategoryDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGProposalCategoryDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGProposalCategoryDescriptions on CFGProposalCategoryDescriptions.code = @code and CFGProposalCategoryDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
