SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGLeadRating]
as
SELECT dbo.CFGLeadRatingData.Code, dbo.CFGLeadRatingDescriptions.Description, dbo.CFGLeadRatingDescriptions.Seq
FROM         dbo.CFGLeadRatingData Left JOIN
dbo.CFGLeadRatingDescriptions ON dbo.CFGLeadRatingData.Code = dbo.CFGLeadRatingDescriptions.Code 
and dbo.CFGLeadRatingDescriptions.UICultureName = dbo.FW_GetActiveCultureName() 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGLeadRatingTrigger] 
   ON  [dbo].[CFGLeadRating]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGLeadRatingDescriptions where code = @Code
Delete from CFGLeadRatingData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGLeadRatingTrigger] 
   ON  [dbo].[CFGLeadRating]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGLeadRatingData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGLeadRatingDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGLeadRatingTrigger] 
   ON  [dbo].[CFGLeadRating]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGLeadRatingData
  set code = i.code
from inserted i
  inner join CFGLeadRatingData on CFGLeadRatingData.code = @Code

if (not exists(Select CFGLeadRatingDescriptions.UICultureName from CFGLeadRatingDescriptions
  where CFGLeadRatingDescriptions.code = @code and CFGLeadRatingDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGLeadRatingDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGLeadRatingDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGLeadRatingDescriptions on CFGLeadRatingDescriptions.code = @code and CFGLeadRatingDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
