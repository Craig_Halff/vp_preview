SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FW_CustomColumns]
AS
SELECT dbo.FW_CustomColumnsData.ColumnID,
       dbo.FW_CustomColumnsData.InfocenterArea,
       dbo.FW_CustomColumnsData.GridID,
       dbo.FW_CustomColumnsData.Name,
       dbo.FW_CustomColumnsData.TabID,
       dbo.FW_CustomColumnsData.DataType,
       dbo.FW_CustomColumnsData.DisplayWidth,
       dbo.FW_CustomColumnsData.Lines,
       dbo.FW_CustomColumnsData.sort,
       dbo.FW_CustomColumnsData.LimitToList,
       dbo.FW_CustomColumnsData.Total,
       dbo.FW_CustomColumnsData.Decimals,
       dbo.FW_CustomColumnsData.MinValue,
       dbo.FW_CustomColumnsData.MaxValue,
       dbo.FW_CustomColumnsData.Required,
       dbo.FW_CustomColumnsData.ReqWBSLevel,
       dbo.FW_CustomColumnsData.Seq,
       dbo.FW_CustomColumnsData.CurrencyCode,
       dbo.FW_CustomColumnsData.FieldType,
       dbo.FW_CustomColumnsData.PropertyBag,
       dbo.FW_CustomColumnsData.AvailableForCubes,
       dbo.FW_CustomColumnCaptions.UICultureName,
       dbo.FW_CustomColumnCaptions.Label,
       dbo.FW_CustomColumnsData.DefaultValue,
       dbo.FW_CustomColumnsData.SearchBy,
       dbo.FW_CustomColumnsData.SearchResults
FROM   dbo.FW_CustomColumnsData
       LEFT JOIN dbo.FW_CustomColumnCaptions
        ON dbo.FW_CustomColumnCaptions.GridID = dbo.FW_CustomColumnsData.GridID
            AND dbo.FW_CustomColumnCaptions.InfocenterArea = dbo.FW_CustomColumnsData.InfocenterArea
            AND dbo.FW_CustomColumnCaptions.Name = dbo.FW_CustomColumnsData.Name
            AND dbo.FW_CustomColumnCaptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DelFW_CustomColumns] 
  ON  [dbo].[FW_CustomColumns]
INSTEAD OF DELETE
AS 
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
DELETE FROM FW_CustomColumnCaptions from deleted
 WHERE FW_CustomColumnCaptions.InfocenterArea = deleted.InfocenterArea 
   AND FW_CustomColumnCaptions.GridID = deleted.GridID 
   AND FW_CustomColumnCaptions.Name = deleted.Name
      
DELETE FROM FW_CustomColumnsData from deleted
 WHERE FW_CustomColumnsData.InfocenterArea = deleted.InfocenterArea 
   AND FW_CustomColumnsData.GridID = deleted.GridID 
   AND FW_CustomColumnsData.Name = deleted.Name 
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[InsFW_CustomColumns] 
  ON  [dbo].[FW_CustomColumns]
INSTEAD OF Insert
AS
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;

     Insert into FW_CustomColumnsData 
	(ColumnID, InfocenterArea, Gridid, [Name], TabId, DataType, DisplayWidth, Lines, Sort, LimitToList, Total, Decimals, MinValue, MaxValue, [Required], ReqWBSLevel, Seq, CurrencyCode, FieldType, PropertyBag, AvailableForCubes, SearchBy, SearchResults, DefaultValue)
     SELECT 
	 ColumnID, InfocenterArea, Gridid, [Name], TabId, DataType, DisplayWidth, isnull(Lines,0), isnull(Sort,'N'), isnull(LimitToList,'N'), isnull(Total,'N'), Decimals, MinValue, MaxValue, [Required], ReqWBSLevel, Seq, CurrencyCode, FieldType, PropertyBag, isnull(AvailableForCubes,'N'), isnull(SearchBy,'N'), isnull(SearchResults,'N'), DefaultValue FROM inserted
       
     Insert into FW_CustomColumnCaptions 
	(InfocenterArea, Gridid, UICultureName, [Name], Label)
     SELECT 
	 InfocenterArea, Gridid, DBO.FW_GetActiveCultureName(), [Name], Label FROM Inserted
 
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[UpdFW_CustomColumns] 
  ON  [dbo].[FW_CustomColumns]
INSTEAD OF UPDATE
AS 
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
DECLARE @Name nvarchar(250)
DECLARE @GridID nvarchar(250)
IF (UPDATE(GridID))
	SELECT @GridID =GridID FROM deleted
ELSE
	SELECT @GridID =GridID FROM inserted

IF (UPDATE([Name]))
	SELECT @Name =[Name] FROM deleted
ELSE
	SELECT @Name =[Name] FROM inserted


UPDATE FW_CustomColumnsData
	SET [Name] = i.Name, 
		Seq = isnull(i.Seq,0), 
		GridID = i.GridID, 
		TabID = i.TabID,
		DataType = i.DataType, 
		DisplayWidth =i.DisplayWidth, 
		Lines = i.lines,
		Sort = i.Sort,
		LimitToList=i.LimitToList,
		Total =i.Total,
		Decimals = i.Decimals,
		MinValue= i.MinValue,
		MaxValue= i.MaxValue,
		[Required]= i.[Required],
		ReqWBSLevel= i.ReqWBSLevel, 
		CurrencyCode= i.CurrencyCode, 
		FieldType = i.FieldType, 
		PropertyBag = i.PropertyBag,
		AvailableForCubes = i.AvailableForCubes,
		SearchBy = i.SearchBy,
		SearchResults = i.SearchResults,
		DefaultValue = i.DefaultValue
 FROM inserted i
    INNER JOIN FW_CustomColumnsData ON FW_CustomColumnsData.InfocenterArea = i.InfocenterArea AND FW_CustomColumnsData.GridID = @GridID AND FW_CustomColumnsData.Name = @Name

IF (not exists (SELECT FW_CustomColumnCaptions.UICultureName FROM FW_CustomColumnCaptions, Inserted I
           WHERE  FW_CustomColumnCaptions.InfocenterArea = i.InfocenterArea AND FW_CustomColumnCaptions.gridId = @gridId
				and FW_CustomColumnCaptions.Name = @Name AND FW_CustomColumnCaptions.UICultureName =dbo.FW_GetActiveCultureName()))
     Insert into FW_CustomColumnCaptions 
	(InfocenterArea, GridID, UICultureName, Name, Label)
     SELECT 
	 InfocenterArea, GridID, dbo.FW_GetActiveCultureName(), Name, Label FROM inserted
 
ELSE
     UPDATE FW_CustomColumnCaptions
           SET [Name] = i.Name, 
		GridID = i.GridID,
		Label = i.Label
           FROM inserted i
           INNER JOIN FW_CustomColumnCaptions ON FW_CustomColumnCaptions.InfocenterArea = i.InfocenterArea AND FW_CustomColumnCaptions.gridID = @GridID
            AND FW_CustomColumnCaptions.Name = @Name AND FW_CustomColumnCaptions.UICultureName = dbo.FW_GetActiveCultureName()
 
END
--IF the Name has been changed, need to change the Name for all cultures
IF (UPDATE([Name]))
begin
	UPDATE FW_CustomColumnCaptions
		SET [Name] = i.Name
		FROM inserted i 
        INNER JOIN FW_CustomColumnCaptions ON FW_CustomColumnCaptions.InfocenterArea = i.InfocenterArea AND FW_CustomColumnCaptions.gridID = @GridID
		and FW_CustomColumnCaptions.Name = @Name 
 
end
GO
