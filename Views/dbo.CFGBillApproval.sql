SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGBillApproval]
as
SELECT dbo.CFGBillApprovalData.Code, dbo.CFGBillApprovalDescriptions.Description
  FROM dbo.CFGBillApprovalData 
  LEFT JOIN dbo.CFGBillApprovalDescriptions ON dbo.CFGBillApprovalData.Code = dbo.CFGBillApprovalDescriptions.Code 
        AND dbo.CFGBillApprovalDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGBillApprovalTrigger] 
   ON  [dbo].[CFGBillApproval]
INSTEAD OF DELETE
AS 
BEGIN

SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGBillApprovalDescriptions where code = @Code
Delete from CFGBillApprovalData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGBillApprovalTrigger] 
   ON  [dbo].[CFGBillApproval]
INSTEAD OF INSERT
AS
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGBillApprovalData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGBillApprovalDescriptions]
  ([Code] 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGBillApprovalTrigger] 
   ON  [dbo].[CFGBillApproval]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGBillApprovalData
  set code = i.code
from inserted i
  inner join CFGBillApprovalData on CFGBillApprovalData.code = @Code

if (not exists(Select CFGBillApprovalDescriptions.UICultureName from CFGBillApprovalDescriptions
  where CFGBillApprovalDescriptions.code = @code and CFGBillApprovalDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGBillApprovalDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGBillApprovalDescriptions
      Set code = i.code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGBillApprovalDescriptions on CFGBillApprovalDescriptions.code = @code and CFGBillApprovalDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
