SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGARLedgerHeadings]
as
SELECT dbo.CFGARLedgerHeadingsData.ReportColumn, dbo.CFGARLedgerHeadingsDescriptions.Label 
FROM         dbo.CFGARLedgerHeadingsData Left JOIN
dbo.CFGARLedgerHeadingsDescriptions ON dbo.CFGARLedgerHeadingsData.ReportColumn = dbo.CFGARLedgerHeadingsDescriptions.ReportColumn 
and dbo.CFGARLedgerHeadingsDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGARLedgerHeadingsTrigger] 
   ON  [dbo].[CFGARLedgerHeadings]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @ReportColumn smallint

SELECT top 1 @ReportColumn = ReportColumn FROM Deleted

Delete from CFGARLedgerHeadingsDescriptions where ReportColumn = @ReportColumn
Delete from CFGARLedgerHeadingsData where ReportColumn = @ReportColumn

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGARLedgerHeadingsTrigger] 
   ON  [dbo].[CFGARLedgerHeadings]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGARLedgerHeadingsData]
  ([ReportColumn])
Select [ReportColumn] from inserted

INSERT INTO [CFGARLedgerHeadingsDescriptions]
  ([ReportColumn] 
  ,Label
  ,UICultureName)
Select [ReportColumn],[Label], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGARLedgerHeadingsTrigger] 
   ON  [dbo].[CFGARLedgerHeadings]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @ReportColumn VARCHAR(32)

if update(ReportColumn)
  select @ReportColumn=ReportColumn from deleted
else
  select @ReportColumn=ReportColumn from inserted

Update CFGARLedgerHeadingsData
  set ReportColumn = i.ReportColumn
from inserted i
  inner join CFGARLedgerHeadingsData on CFGARLedgerHeadingsData.ReportColumn = @ReportColumn

if (not exists(Select CFGARLedgerHeadingsDescriptions.UICultureName from CFGARLedgerHeadingsDescriptions
  where CFGARLedgerHeadingsDescriptions.ReportColumn = @ReportColumn and CFGARLedgerHeadingsDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGARLedgerHeadingsDescriptions
      ([ReportColumn]
      ,[UICultureName]
      ,[Label])
  Select [ReportColumn], dbo.FW_GetActiveCultureName(),[Label] From Inserted 

Else
  Update CFGARLedgerHeadingsDescriptions
      Set ReportColumn = i.ReportColumn
      , [Label] = i.Label
  From inserted i
  Inner Join CFGARLedgerHeadingsDescriptions on CFGARLedgerHeadingsDescriptions.ReportColumn = @ReportColumn and CFGARLedgerHeadingsDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
