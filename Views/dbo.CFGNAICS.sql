SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGNAICS]
as
SELECT dbo.CFGNAICSData.Code, dbo.CFGNAICSDescriptions.Description, dbo.CFGNAICSDescriptions.Seq
FROM         dbo.CFGNAICSData Left JOIN
dbo.CFGNAICSDescriptions ON dbo.CFGNAICSData.Code = dbo.CFGNAICSDescriptions.Code 
and dbo.CFGNAICSDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGNAICSTrigger] 
   ON  [dbo].[CFGNAICS]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGNAICSDescriptions where code = @Code
Delete from CFGNAICSData where code = @Code

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGNAICSTrigger] 
   ON  [dbo].[CFGNAICS]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO CFGNAICSData
  (Code)
Select Code from inserted

INSERT INTO CFGNAICSDescriptions
  (Code 
  ,Description
  ,UICultureName
  ,Seq)
Select Code,Description, dbo.FW_GetActiveCultureName(),@seq from inserted

End

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGNAICSTrigger] 
   ON  [dbo].[CFGNAICS]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGNAICSData
  set code = i.code
from inserted i
  inner join CFGNAICSData on CFGNAICSData.code = @Code

if (not exists(Select CFGNAICSDescriptions.UICultureName from CFGNAICSDescriptions
  where CFGNAICSDescriptions.code = @code and CFGNAICSDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGNAICSDescriptions
      (Code
      ,UICultureName
      ,Description
      ,Seq)
  Select Code, dbo.FW_GetActiveCultureName(),Description,Seq From Inserted 

Else
  Update CFGNAICSDescriptions
      Set code = i.code
      , Description = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGNAICSDescriptions on CFGNAICSDescriptions.code = @code and CFGNAICSDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END

GO
