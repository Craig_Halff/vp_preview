SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CustomRepLookup]
AS
SELECT     ReportName, TabID, ColName, Value, UICultureName, Seq, CreateUser, CreateDate, ModDate, ModUser
FROM         dbo.CustomRepLookupData
WHERE     (UICultureName = dbo.FW_GetActiveCultureName())

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[DeleteCustomRepLookupTrigger] 
   ON  [dbo].[CustomRepLookup]
instead of Delete
AS 
BEGIN
     -- SET NOCOUNT ON added to prevent extra result sets from
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
DECLARE @ReportName VARCHAR(50)
DECLARE @TabID Nvarchar(100)
DECLARE @Colname Nvarchar(100)
DECLARE @Value Nvarchar(100)

SELECT top 1 @TabID = TabId, @ReportName= ReportName, @Colname=Colname, @Value =[Value] FROM Deleted
 
 
Delete from CustomRepLookupData where
 CustomRepLookupData.ReportName = @ReportName and 
 CustomRepLookupData.TabId = @TabID and  
 CustomRepLookupData.ColName = @ColName and
 CustomRepLookupData.Value = @Value
      
  
 
END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[InsertCustomRepLookupTrigger] 
   ON  [dbo].[CustomRepLookup]
instead of Insert
as
BEGIN
     -- SET NOCOUNT ON added to prevent extra result sets from
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
  
DECLARE @TabID Nvarchar(100)
DECLARE @Colname Nvarchar(30)
DECLARE @Value Nvarchar(70)

SELECT top 1 @TabID = TabId,  @Colname=Colname, @Value =[Value] FROM Inserted
 

if (not exists (Select CustomRepLookupData.UICultureName from CustomRepLookupData, Inserted I
           Where  CustomRepLookupData.ReportName = i.reportName 
			and CustomRepLookupData.TabId = @Tabid
			and CustomRepLookupData.ColName = @ColName 
			and CustomRepLookupData.Value = @Value 
			and CustomRepLookupData.UICulturename =dbo.FW_GetActiveCultureName()))
     Insert into CustomRepLookupData (Reportname, Tabid, colname,value, UICulturename, seq,createuser,createdate,moddate, moduser)
           Select reportName,Tabid, colName, [value],dbo.FW_GetActiveCultureName(),seq, createuser, createDate,ModDate,ModUser from inserted
 

Else
     update CustomRepLookupData
           set ColName = i.ColName, 
		       TabID = i.TabID,
			   [Value] = i.value,
			   Seq = i.Seq,
			   CreateUser = i.CreateUser,
			   CreateDate = i.CreateDate,
			   ModDate = i.ModDate,
	           ModUser = i.ModUser
           from inserted i
           inner join CustomRepLookupData on CustomRepLookupData.ReportName = i.reportName 
			and CustomRepLookupData.TabId = @Tabid
			and CustomRepLookupData.ColName = @ColName 
			and CustomRepLookupData.Value = @Value 
			and CustomRepLookupData.UICulturename =dbo.FW_GetActiveCultureName()
 

END
 
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE  TRIGGER [dbo].[UpdateCustomRepLookupTrigger] 
   ON  [dbo].[CustomRepLookup]
instead of Update
AS 
BEGIN
     -- SET NOCOUNT ON added to prevent extra result sets from
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
DECLARE @TabID Nvarchar(100)
DECLARE @Colname Nvarchar(30)
DECLARE @Value Nvarchar(70)

if update(TabId)
	select @TabID =TabId from deleted
else
	select @TabID =TabId from inserted

if update(ColName)
	select @ColName =ColName from deleted
else
	select @ColName =ColName from inserted

if update([Value])
	select @Value =[Value] from deleted
else
	select @Value =[Value] from inserted

if (not exists (Select CustomRepLookupData.UICultureName from CustomRepLookupData, Inserted I
           Where  CustomRepLookupData.ReportName = i.reportName 
			and CustomRepLookupData.TabId = @Tabid
			and CustomRepLookupData.ColName = @ColName 
			and CustomRepLookupData.Value = @Value 
			and CustomRepLookupData.UICulturename =dbo.FW_GetActiveCultureName()))
     Insert into CustomRepLookupData (Reportname, Tabid, colname,value, UICulturename, seq,createuser,createdate,moddate, moduser)
           Select reportName,Tabid, colName, [value],dbo.FW_GetActiveCultureName(),seq, createuser, createDate,ModDate,ModUser from inserted
 

Else
     update CustomRepLookupData
           set ColName = i.ColName, 
		       TabID = i.TabID,
			   [Value] = i.value,
			   Seq = i.Seq,
			   CreateUser = i.CreateUser,
			   CreateDate = i.CreateDate,
			   ModDate = i.ModDate,
	           ModUser = i.ModUser
           from inserted i
           inner join CustomRepLookupData on CustomRepLookupData.ReportName = i.reportName 
			and CustomRepLookupData.TabId = @Tabid
			and CustomRepLookupData.ColName = @ColName 
			and CustomRepLookupData.Value = @Value 
			and CustomRepLookupData.UICulturename =dbo.FW_GetActiveCultureName()
 

END
 

GO
