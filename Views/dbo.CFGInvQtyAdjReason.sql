SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGInvQtyAdjReason]
as
SELECT dbo.CFGInvQtyAdjReasonData.Code, dbo.CFGInvQtyAdjReasonDescriptions.Description 
FROM         dbo.CFGInvQtyAdjReasonData Left JOIN
dbo.CFGInvQtyAdjReasonDescriptions ON dbo.CFGInvQtyAdjReasonData.Code = dbo.CFGInvQtyAdjReasonDescriptions.Code 
and dbo.CFGInvQtyAdjReasonDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGInvQtyAdjReasonTrigger] 
   ON  [dbo].[CFGInvQtyAdjReason]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(20)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGInvQtyAdjReasonDescriptions where Code = @Code
Delete from CFGInvQtyAdjReasonData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGInvQtyAdjReasonTrigger] 
   ON  [dbo].[CFGInvQtyAdjReason]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGInvQtyAdjReasonData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGInvQtyAdjReasonDescriptions]
  ([Code] 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGInvQtyAdjReasonTrigger] 
   ON  [dbo].[CFGInvQtyAdjReason]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(20)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGInvQtyAdjReasonData
  set Code = i.Code
from inserted i
  inner join CFGInvQtyAdjReasonData on CFGInvQtyAdjReasonData.Code = @Code

if (not exists(Select CFGInvQtyAdjReasonDescriptions.UICultureName from CFGInvQtyAdjReasonDescriptions
  where CFGInvQtyAdjReasonDescriptions.Code = @Code and CFGInvQtyAdjReasonDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGInvQtyAdjReasonDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGInvQtyAdjReasonDescriptions
      Set Code = i.Code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGInvQtyAdjReasonDescriptions on CFGInvQtyAdjReasonDescriptions.Code = @Code and CFGInvQtyAdjReasonDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
