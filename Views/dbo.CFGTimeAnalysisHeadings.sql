SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGTimeAnalysisHeadings]
as
SELECT dbo.CFGTimeAnalysisHeadingsData.ReportColumn, dbo.CFGTimeAnalysisHeadingsData.Benefit, dbo.CFGTimeAnalysisHeadingsDescriptions.Label
FROM   dbo.CFGTimeAnalysisHeadingsData
 Left JOIN dbo.CFGTimeAnalysisHeadingsDescriptions ON dbo.CFGTimeAnalysisHeadingsData.ReportColumn =  dbo.CFGTimeAnalysisHeadingsDescriptions.ReportColumn
 and dbo.CFGTimeAnalysisHeadingsDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGTimeAnalysisHeadingsTrigger] 
   ON  [dbo].[CFGTimeAnalysisHeadings]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @ReportColumn smallint


SELECT top 1 @ReportColumn =  ReportColumn  FROM Deleted

Delete from CFGTimeAnalysisHeadingsDescriptions where CFGTimeAnalysisHeadingsDescriptions.ReportColumn =  @ReportColumn 
Delete from CFGTimeAnalysisHeadingsData where CFGTimeAnalysisHeadingsData.ReportColumn =  @ReportColumn 

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGTimeAnalysisHeadingsTrigger] 
   ON  [dbo].[CFGTimeAnalysisHeadings]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGTimeAnalysisHeadingsData]
  (ReportColumn, Benefit)
Select isnull(ReportColumn,0), isnull(Benefit,'N') from inserted

INSERT INTO [CFGTimeAnalysisHeadingsDescriptions]
  (ReportColumn, Label,UICultureName)
Select isnull(ReportColumn,0), Label, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGTimeAnalysisHeadingsTrigger] 
   ON  [dbo].[CFGTimeAnalysisHeadings]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @ReportColumn smallint

if update(ReportColumn)
  select @ReportColumn=ReportColumn from deleted
else
  select @ReportColumn=ReportColumn from inserted

Update CFGTimeAnalysisHeadingsData
  set ReportColumn =  i.ReportColumn, Benefit=i.Benefit
from inserted i
  inner join CFGTimeAnalysisHeadingsData on CFGTimeAnalysisHeadingsData.ReportColumn =  @ReportColumn

if (not exists(Select CFGTimeAnalysisHeadingsDescriptions.UICultureName from CFGTimeAnalysisHeadingsDescriptions, Inserted I
  where  CFGTimeAnalysisHeadingsDescriptions.ReportColumn =  @ReportColumn and CFGTimeAnalysisHeadingsDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGTimeAnalysisHeadingsDescriptions
      (ReportColumn,
       UICultureName,
       Label)
  Select ReportColumn, dbo.FW_GetActiveCultureName(), Label From Inserted 

Else
  Update CFGTimeAnalysisHeadingsDescriptions
      Set ReportColumn =  i.ReportColumn,
          Label = i.Label
  From inserted i
  Inner Join CFGTimeAnalysisHeadingsDescriptions on CFGTimeAnalysisHeadingsDescriptions.ReportColumn =  @ReportColumn and CFGTimeAnalysisHeadingsDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
