SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGProjectStatus]
as
SELECT dbo.CFGProjectStatusData.Code, dbo.CFGProjectStatusDescriptions.Description 
FROM         dbo.CFGProjectStatusData Left JOIN
dbo.CFGProjectStatusDescriptions ON dbo.CFGProjectStatusData.Code = dbo.CFGProjectStatusDescriptions.Code 
and dbo.CFGProjectStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGProjectStatusTrigger] 
   ON  [dbo].[CFGProjectStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGProjectStatusDescriptions where Code = @Code
Delete from CFGProjectStatusData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGProjectStatusTrigger] 
   ON  [dbo].[CFGProjectStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGProjectStatusData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGProjectStatusDescriptions]
  ([Code] 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGProjectStatusTrigger] 
   ON  [dbo].[CFGProjectStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGProjectStatusData
  set Code = i.Code
from inserted i
  inner join CFGProjectStatusData on CFGProjectStatusData.Code = @Code

if (not exists(Select CFGProjectStatusDescriptions.UICultureName from CFGProjectStatusDescriptions
  where CFGProjectStatusDescriptions.Code = @Code and CFGProjectStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGProjectStatusDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGProjectStatusDescriptions
      Set Code = i.Code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGProjectStatusDescriptions on CFGProjectStatusDescriptions.Code = @Code and CFGProjectStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
