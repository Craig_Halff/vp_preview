SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGBankEntries]
as
SELECT  dbo.CFGBankEntriesCodeData.EntryCode, 
	dbo.CFGBankEntriesCodeData.Code, 
	dbo.CFGBankEntriesCodeData.Account, 
	dbo.CFGBankEntriesCodeData.WBS1, 
	dbo.CFGBankEntriesCodeData.WBS2, 
	dbo.CFGBankEntriesCodeData.WBS3, 
	dbo.CFGBankEntriesCodeData.TransactionType, 
	dbo.CFGBankEntriesCodeDescriptions.Description
  FROM	dbo.CFGBankEntriesCodeData Left JOIN
	dbo.CFGBankEntriesCodeDescriptions ON dbo.CFGBankEntriesCodeData.Code = dbo.CFGBankEntriesCodeDescriptions.Code 
   and	dbo.CFGBankEntriesCodeData.EntryCode = dbo.CFGBankEntriesCodeDescriptions.EntryCode
   and	dbo.CFGBankEntriesCodeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGBankEntriesTrigger] 
   ON  [dbo].[CFGBankEntries]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @EntryCode Nvarchar(2)
DECLARE @Code Nvarchar(10)

SELECT top 1 @EntryCode = EntryCode, @Code = Code FROM Deleted

Delete from CFGBankEntriesCodeDescriptions where CFGBankEntriesCodeDescriptions.Code = @Code and CFGBankEntriesCodeDescriptions.EntryCode = @EntryCode
Delete from CFGBankEntriesCodeData where CFGBankEntriesCodeData.Code = @Code and CFGBankEntriesCodeData.EntryCode = @EntryCode

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGBankEntriesTrigger] 
   ON  [dbo].[CFGBankEntries]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO CFGBankEntriesCodeData  
      (EntryCode,Code,Account,WBS1,WBS2,WBS3,TransactionType)   
Select EntryCode, Code, Account, WBS1, WBS2, WBS3, TransactionType from inserted

INSERT INTO CFGBankEntriesCodeDescriptions
      (EntryCode,Code,UICultureName,Description)
Select EntryCode,Code,dbo.FW_GetActiveCultureName(),Description from inserted

End

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGBankEntriesTrigger] 
   ON  [dbo].[CFGBankEntries]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)
DECLARE @EntryCode Nvarchar(2)

if update(Code) or update(EntryCode)
  select @Code=Code, @EntryCode=EntryCode from deleted
else
  select @Code=Code, @EntryCode=EntryCode from inserted

Update CFGBankEntriesCodeData
    set Code = i.Code,
	EntryCode = i.EntryCode,
	Account = i.Account,
	WBS1 = i.WBS1,
	WBS2 = i.WBS2,
	WBS3 = i.WBS3,
	TransactionType = i.TransactionType
from inserted i
  inner join CFGBankEntriesCodeData on CFGBankEntriesCodeData.EntryCode = @EntryCode and CFGBankEntriesCodeData.Code = i.Code

if (not exists(Select CFGBankEntriesCodeDescriptions.UICultureName from CFGBankEntriesCodeDescriptions, Inserted i
  where CFGBankEntriesCodeDescriptions.Code = i.Code
    and CFGBankEntriesCodeDescriptions.EntryCode = @EntryCode 
    and CFGBankEntriesCodeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()))
  Insert Into CFGBankEntriesCodeDescriptions
    (EntryCode
	,Code
	,UICultureName
	,Description)
  Select EntryCode,Code,dbo.FW_GetActiveCultureName(),Description From Inserted 
Else
  Update CFGBankEntriesCodeDescriptions
     Set EntryCode = i.EntryCode
	,Code = i.Code
	,Description = i.Description
  From inserted i
  Inner Join CFGBankEntriesCodeDescriptions
     on CFGBankEntriesCodeDescriptions.Code = i.Code 
	and CFGBankEntriesCodeDescriptions.EntryCode = i.EntryCode 
	and CFGBankEntriesCodeDescriptions.UICultureName = dbo.FW_GetActiveCultureName() 

END
GO
