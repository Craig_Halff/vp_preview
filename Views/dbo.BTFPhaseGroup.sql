SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[BTFPhaseGroup]
as
SELECT dbo.BTFPhaseGroupData.WBS1, dbo.BTFPhaseGroupData.WBS2, dbo.BTFPhaseGroupData.WBS3, dbo.BTFPhaseGroupData.Seq, dbo.BTFPhaseGroupData.PhaseGroup,
	dbo.BTFPhaseGroupData.PrintHeader, dbo.BTFPhaseGroupData.PrintSubTotal, dbo.BTFPhaseGroupData.HideDetail, dbo.BTFPhaseGroupData.AddSpaceAfter,
	dbo.BTFPhaseGroupDescriptions.Description
FROM dbo.BTFPhaseGroupData Left JOIN
dbo.BTFPhaseGroupDescriptions ON dbo.BTFPhaseGroupData.WBS1 = dbo.BTFPhaseGroupDescriptions.WBS1
and dbo.BTFPhaseGroupData.WBS2 = dbo.BTFPhaseGroupDescriptions.WBS2
and dbo.BTFPhaseGroupData.WBS3 = dbo.BTFPhaseGroupDescriptions.WBS3
and dbo.BTFPhaseGroupData.Seq = dbo.BTFPhaseGroupDescriptions.Seq
and dbo.BTFPhaseGroupData.PhaseGroup = dbo.BTFPhaseGroupDescriptions.PhaseGroup
and dbo.BTFPhaseGroupDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteBTFPhaseGroupTrigger] 
   ON  [dbo].[BTFPhaseGroup]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @WBS1		Nvarchar(30)
DECLARE @WBS2		Nvarchar(7)
DECLARE @WBS3		Nvarchar(7)
DECLARE @Seq		int
DECLARE @PhaseGroup	Nvarchar(10)
 
SELECT top 1 @WBS1 = WBS1, @WBS2 = WBS2, @WBS3 = WBS3, @Seq = Seq, @PhaseGroup = PhaseGroup FROM Deleted
 
Delete from BTFPhaseGroupDescriptions where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3 and Seq = @Seq and PhaseGroup = @PhaseGroup
Delete from BTFPhaseGroupData where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3 and Seq = @Seq and PhaseGroup = @PhaseGroup
 
END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertBTFPhaseGroupTrigger] 
   ON  [dbo].[BTFPhaseGroup]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO BTFPhaseGroupData
  (WBS1, WBS2 ,WBS3, Seq, PhaseGroup, PrintHeader, PrintSubTotal, HideDetail, AddSpaceAfter)
Select WBS1, WBS2, WBS3, Seq, PhaseGroup, PrintHeader, PrintSubTotal, HideDetail, AddSpaceAfter from inserted
 
INSERT INTO BTFPhaseGroupDescriptions
  (WBS1, WBS2, WBS3, Seq, PhaseGroup, UICultureName, Description)
Select WBS1, WBS2, WBS3, Seq, PhaseGroup, dbo.FW_GetActiveCultureName(), Description from inserted
 
End
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateBTFPhaseGroupTrigger] 
   ON  [dbo].[BTFPhaseGroup]
instead of Update
as
BEGIN
     SET NOCOUNT ON;
 
DECLARE @WBS1		Nvarchar(30)
DECLARE @WBS2		Nvarchar(7)
DECLARE @WBS3		Nvarchar(7)
DECLARE @Seq		int
DECLARE @PhaseGroup	Nvarchar(10)
 
if update(WBS1)
  select @WBS1=WBS1 from deleted
else
  select @WBS1=WBS1 from inserted
 
if update(WBS2)
  select @WBS2=WBS2 from deleted
else
  select @WBS2=WBS2 from inserted
 
if update(WBS3)
  select @WBS3=WBS3 from deleted
else
  select @WBS3=WBS3 from inserted
 
if update(Seq)
  select @Seq=Seq from deleted
else
  select @Seq=Seq from inserted
 
if update(PhaseGroup)
  select @PhaseGroup=PhaseGroup from deleted
else
  select @PhaseGroup=PhaseGroup from inserted

Update BTFPhaseGroupData
  set WBS1 = i.WBS1, WBS2 = i.WBS2, WBS3 = i.WBS3, Seq = i.Seq, PhaseGroup = i.PhaseGroup, PrintHeader = i.PrintHeader, PrintSubTotal = i.PrintSubTotal, HideDetail = i.HideDetail, AddSpaceAfter = i.AddSpaceAfter
from inserted i
  inner join BTFPhaseGroupData on BTFPhaseGroupData.WBS1 = @WBS1 and BTFPhaseGroupData.WBS2 = @WBS2
       and BTFPhaseGroupData.WBS3 = @WBS3 and BTFPhaseGroupData.Seq = @Seq and BTFPhaseGroupData.PhaseGroup = @PhaseGroup
 
if (not exists(Select BTFPhaseGroupDescriptions.UICultureName from BTFPhaseGroupDescriptions
  where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3 and Seq = @Seq and PhaseGroup = @PhaseGroup and BTFPhaseGroupDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  INSERT INTO BTFPhaseGroupDescriptions
       (WBS1, WBS2, WBS3, Seq, PhaseGroup, UICultureName, Description)
  Select WBS1, WBS2, WBS3, Seq, PhaseGroup, dbo.FW_GetActiveCultureName(), Description from inserted

Else
  Update BTFPhaseGroupDescriptions
      Set WBS1 = i.WBS1, WBS2 = i.WBS2, WBS3 = i.WBS3, Seq = i.Seq, PhaseGroup = i.PhaseGroup, Description = i.Description
  From inserted i
  Inner Join BTFPhaseGroupDescriptions on BTFPhaseGroupDescriptions.WBS1 = @WBS1 and BTFPhaseGroupDescriptions.WBS2 = @WBS2 and BTFPhaseGroupDescriptions.WBS3 = @WBS3
       and BTFPhaseGroupDescriptions.Seq = @Seq and BTFPhaseGroupDescriptions.PhaseGroup = @PhaseGroup and BTFPhaseGroupDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 
END 

GO
