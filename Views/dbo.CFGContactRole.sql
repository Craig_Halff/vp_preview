SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGContactRole]
as
SELECT dbo.CFGContactRoleData.Code, dbo.CFGContactRoleDescriptions.Description, dbo.CFGContactRoleDescriptions.Seq
FROM         dbo.CFGContactRoleData Left JOIN
dbo.CFGContactRoleDescriptions ON dbo.CFGContactRoleData.Code = dbo.CFGContactRoleDescriptions.Code 
and dbo.CFGContactRoleDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGContactRoleTrigger] 
   ON  [dbo].[CFGContactRole]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGContactRoleDescriptions where code = @Code
Delete from CFGContactRoleData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGContactRoleTrigger] 
   ON  [dbo].[CFGContactRole]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGContactRoleData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGContactRoleDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGContactRoleTrigger] 
   ON  [dbo].[CFGContactRole]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGContactRoleData
  set code = i.code
from inserted i
  inner join CFGContactRoleData on CFGContactRoleData.code = @Code

if (not exists(Select CFGContactRoleDescriptions.UICultureName from CFGContactRoleDescriptions
  where CFGContactRoleDescriptions.code = @code and CFGContactRoleDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGContactRoleDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGContactRoleDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGContactRoleDescriptions on CFGContactRoleDescriptions.code = @code and CFGContactRoleDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
