SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[EM] AS 
SELECT 
    EMMain.Employee         ,
    EMMain.HomeCompany      ,
    EMMain.LastName         ,
    EMMain.FirstName        ,
    EMMain.MiddleName       ,
    EMMain.BillingPool      ,
    EMMain.SSN              ,
    EMMain.Address1         ,
    EMMain.Address2         ,
    EMMain.Address3         ,
    EMMain.City             ,
    EMMain.State            ,
    EMMain.ZIP              ,
    EMMain.Country          ,
    EMMain.HomePhone        ,
    EMMain.Fax              ,
    EMMain.EMail            ,
    EMMain.TKAdminLevel     ,
    EMMain.TKAdminEdit      ,
    EMMain.EKAdminLevel     ,
    EMMain.EKAdminEdit      ,
    EMMain.Memo             ,
    EMMain.EmployeePhoto    ,
    EMMain.Salutation       ,
    EMMain.Suffix           ,
    EMMain.Title            ,
    EMMain.ExportInd        ,
    EMMain.WorkPhone        ,
    EMMain.MobilePhone      ,
    EMMain.AvailableForCRM  ,
    EMMain.ReadyForApproval ,
    EMMain.PreferredName    ,
    EMMain.HomePhoneFormat  ,
    EMMain.FaxFormat        ,
    EMMain.WorkPhoneFormat  ,
    EMMain.MobilePhoneFormat,
    EMMain.Language         ,
    EMMain.TargetRatio      ,
    EMMain.UtilizationRatio ,
    EMMain.ConsultantInd    ,
    EMMain.ClientVendorInd  ,
    EMMain.ClientID         ,
    EMMain.Vendor           ,
    EMMain.TalentUserID     ,
    EMMain.Location         ,
    EMMain.TLInternalKey    ,
    EMMain.ProfessionalSuffix,
    EMMain.TalentModDate    ,
    EMMain.TLSyncModDate    ,
    EMMain.KonaUsername     ,
    EMMain.KonaAccessToken  ,
    EMMain.KonaRefreshToken ,
    EMMain.KonaUserID       ,
    EMMain.CitizenshipStatus,
	EMMain.QBOID            ,
	EMMain.QBOAddressID     ,
	EMMain.QBOLastUpdated   ,
	EMMain.QBOVendorID		,
	EMMain.PIMID			,
    EMMain.CreateUser       ,
    EMMain.CreateDate       ,
    EMMain.ModUser          ,
    EMMain.ModDate          , 
    EMCompany.EmployeeCompany,
    EMCompany.JobCostRate   ,
    EMCompany.JobCostType   ,
    EMCompany.JCOvtPct      ,
    EMCompany.HoursPerDay   ,
    EMCompany.HireDate      ,
    EMCompany.RaiseDate     ,
    EMCompany.Status        ,
    EMCompany.Type          ,
    EMCompany.Org           ,
    EMCompany.Region        ,
    EMCompany.BillingCategory,
    EMCompany.TKGroup       ,
    EMCompany.EKGroup       ,
    EMCompany.PayRate       ,
    EMCompany.PayType       ,
    EMCompany.PayOvtPct     ,
    EMCompany.PaySpecialOvtPct,
    EMCompany.ADPFileNumber ,
    EMCompany.ADPCompanyCode,
    EMCompany.ADPRateCode   ,
    EMCompany.ProvCostRate  ,
    EMCompany.ProvBillRate  ,
    EMCompany.ProvCostOTPct ,
    EMCompany.ProvBillOTPct ,
    EMCompany.DefaultLC1    ,
    EMCompany.DefaultLC2    ,
    EMCompany.DefaultLC3    ,
    EMCompany.DefaultLC4    ,
    EMCompany.DefaultLC5    ,
    EMCompany.ChangeDefaultLC,
    EMCompany.TerminationDate,
    EMCompany.UseTotalHrsAsStd,
    EMCompany.JCSpecialOvtPct,
    EMCompany.ProvCostSpecialOTPct,
    EMCompany.ProvBillSpecialOTPct,
    EMCompany.YearsOtherFirms,
    EMCompany.Supervisor    ,
    EMCompany.ReadyForProcessing,
    EMCompany.CheckHours    ,
    EMCompany.Locale        ,
    EMCompany.LocaleMethod  ,
    EMCompany.OtherPay      ,
    EMCompany.OtherPay2     ,
    EMCompany.OtherPay3     ,
    EMCompany.OtherPay4     ,
    EMCompany.OtherPay5     ,
    EMCompany.CostRateMeth  ,
    EMCompany.CostRateTableNo,
    EMCompany.PayRateMeth   ,
    EMCompany.PayRateTableNo,
    EMCompany.PriorYearsFirm,
    EMCompany.PaychexCode1  ,
    EMCompany.PaychexCode2  ,
    EMCompany.PaychexCode3  ,
    EMCompany.IncludeLocalJurisOnly,
    EMCompany.AllowChargeUnits,
    EMCompany.RequireStartEndTime,
    EMCompany.AllowBreakTime,
    EMCompany.DefaultBreakStartDateTime,
    EMCompany.DefaultBreakEndDateTime,
    EMCompany.PaychexRateNumber,
    EMCompany.EmailPayrollRemittance,
    EMCompany.EmailExpenseRemittance,
    EMCompany.OccupationalCode, 
    EMCompany.GeographicCode,
    EMCompany.StatutoryEmployee,
    EMCompany.RetirementPlan,
    EMCompany.ThirdPartySickPay,
    EMCompany.ClieOpTransactionType,
    EMCompany.ClieOp        , 
    EMCompany.ClieOpAccount ,
    EMCompany.ClieOpAccountType,
    EMCompany.SEPAIBAN      ,
    EMCompany.SEPABIC       ,
    EMCompany.TaxRegistrationNumber,
    EMCompany.DisableTSRevAudit,
    --EMCompany.TaxRegistrationCountry,
    --EMCompany.Class         ,
    --EMCompany.JobLevel      ,
    --EMCompany.FLSAStatus    ,
    --EMCompany.SOCCode       ,
    --EMCompany.WorkSchedule  ,
    --EMCompany.RehireDate    ,
    --EMCompany.SeniorityDate ,
    --EMCompany.TerminationReason,
    --EMCompany.TerminationType,
    --EMCompany.LastDayWorked ,
    --EMCompany.LastDayPaid   ,
    --EMCompany.ExitInterviewer,
    --EMCompany.RehireEligible ,
	EMCompany.UIPaymentMethod,
    EMCompany.DefaultTaxLocation,
	EMCompany.Terminated,
	EMCompany.FormW4Version     ,
	EMCompany.FormW4Step2       ,
	EMCompany.FormW4Dependents  ,
	EMCompany.FormW4DependentsOther,
	EMCompany.FormW4OtherTaxCredit,
	EMCompany.FormW4OtherIncome ,
	EMCompany.FormW4Deductions	
  FROM EMMain, EMCompany 
 WHERE EMMain.Employee = EMCompany.Employee
   AND EMMain.HomeCompany = EMCompany.EmployeeCompany
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteEM] 
   ON  [dbo].[EM]
instead of Delete
AS 
BEGIN
    SET NOCOUNT ON;
    DECLARE @Employee	Nvarchar(30)

    SELECT top 1 @Employee = Employee FROM Deleted

    Delete from EMMain where Employee = @Employee
    Delete from EMCompany where Employee = @Employee

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertEM] 
   ON  [dbo].[EM]
instead of Insert
as
BEGIN
    SET NOCOUNT ON;

    DECLARE @Employee		Nvarchar(30)
    SELECT @Employee = Employee from inserted

    if not exists (select 'x' from EMMain where Employee = @Employee)
    INSERT INTO EMMain
     (Employee,HomeCompany,LastName,FirstName,MiddleName,BillingPool,SSN,Address1,Address2,Address3,City,State,ZIP,Country,HomePhone,Fax,EMail,TKAdminLevel,TKAdminEdit,
      EKAdminLevel,EKAdminEdit,Memo,EmployeePhoto,Salutation,Suffix,Title,ExportInd,WorkPhone,MobilePhone,AvailableForCRM,ReadyForApproval,PreferredName,
      HomePhoneFormat,FaxFormat,WorkPhoneFormat,MobilePhoneFormat,Language,TargetRatio,UtilizationRatio,ConsultantInd,ClientVendorInd,ClientID,Vendor,TalentUserID,
      Location,TLInternalKey,ProfessionalSuffix,TalentModDate,TLSyncModDate,KonaUsername,KonaAccessToken,KonaRefreshToken,KonaUserID,CitizenshipStatus,QBOID,QBOAddressID,QBOLastUpdated,QBOVendorID,PIMID,CreateUser,CreateDate,ModUser,ModDate)
    SELECT
      Employee,HomeCompany,LastName,FirstName,MiddleName,isnull(BillingPool,0),SSN,Address1,Address2,Address3,City,State,ZIP,Country,HomePhone,Fax,EMail,TKAdminLevel,isnull(TKAdminEdit,'N'),
      EKAdminLevel,isnull(EKAdminEdit,'N'),Memo,EmployeePhoto,Salutation,Suffix,Title,isnull(ExportInd,'N'),WorkPhone,MobilePhone,isnull(AvailableForCRM,'N'),isnull(ReadyForApproval,'N'),
      PreferredName,HomePhoneFormat,FaxFormat,WorkPhoneFormat,MobilePhoneFormat,Language,isnull(TargetRatio,0),isnull(UtilizationRatio,0),isnull(ConsultantInd,'N'),isnull(ClientVendorInd,'C'),ClientID,
      Vendor,TalentUserID,Location,TLInternalKey,ProfessionalSuffix,isnull(TalentModDate,getutcdate()),TLSyncModDate,KonaUsername,KonaAccessToken,KonaRefreshToken,isnull(KonaUserID,0),
      CitizenshipStatus,QBOID,QBOAddressID,QBOLastUpdated,QBOVendorID,isnull(PIMID,replace(newid(),'-', '')),CreateUser,isnull(CreateDate,getutcdate()),ModUser,isnull(ModDate,getutcdate())
    FROM inserted

    INSERT INTO EMCompany
     (Employee,EmployeeCompany,JobCostRate,JobCostType,JCOvtPct,HoursPerDay,HireDate,RaiseDate,Status,Type,Org,Region,BillingCategory,TKGroup,EKGroup,PayRate,PayType,PayOvtPct,PaySpecialOvtPct,
      ADPFileNumber,ADPCompanyCode,ADPRateCode,ProvCostRate,ProvBillRate,ProvCostOTPct,ProvBillOTPct,DefaultLC1,DefaultLC2,DefaultLC3,DefaultLC4,DefaultLC5,ChangeDefaultLC,TerminationDate,
      UseTotalHrsAsStd,JCSpecialOvtPct,ProvCostSpecialOTPct,ProvBillSpecialOTPct,YearsOtherFirms,Supervisor,ReadyForProcessing,CheckHours,Locale,LocaleMethod,OtherPay,OtherPay2,OtherPay3,
      OtherPay4,OtherPay5,CostRateMeth,CostRateTableNo,PayRateMeth,PayRateTableNo,PriorYearsFirm,PaychexCode1,PaychexCode2,PaychexCode3,IncludeLocalJurisOnly,AllowChargeUnits,RequireStartEndTime,
      AllowBreakTime,DefaultBreakStartDateTime,DefaultBreakEndDateTime,PaychexRateNumber,EmailPayrollRemittance,EmailExpenseRemittance,OccupationalCode,GeographicCode,StatutoryEmployee,
      RetirementPlan,ThirdPartySickPay,ClieOpTransactionType,ClieOp,ClieOpAccount,ClieOpAccountType,SEPAIBAN,SEPABIC,TaxRegistrationNumber,DisableTSRevAudit,UIPaymentMethod,DefaultTaxLocation,Terminated,CreateUser,CreateDate,ModUser,ModDate,
     FormW4Version,FormW4Step2,FormW4Dependents,FormW4DependentsOther,FormW4OtherTaxCredit,FormW4OtherIncome,FormW4Deductions)
    SELECT
      Employee,EmployeeCompany,isnull(JobCostRate,0),JobCostType,isnull(JCOvtPct,0),isnull(HoursPerDay,0),HireDate,RaiseDate,Status,Type,Org,Region,isnull(BillingCategory,0),TKGroup,EKGroup,isnull(PayRate,0),PayType,isnull(PayOvtPct,0),isnull(PaySpecialOvtPct,0),
      ADPFileNumber,ADPCompanyCode,ADPRateCode,isnull(ProvCostRate,0),isnull(ProvBillRate,0),isnull(ProvCostOTPct,0),isnull(ProvBillOTPct,0),DefaultLC1,DefaultLC2,DefaultLC3,DefaultLC4,DefaultLC5,isnull(ChangeDefaultLC,'N'),TerminationDate,
      isnull(UseTotalHrsAsStd,'N'),isnull(JCSpecialOvtPct,0),isnull(ProvCostSpecialOTPct,0),isnull(ProvBillSpecialOTPct,0),isnull(YearsOtherFirms,0),Supervisor,isnull(ReadyForProcessing,'N'),CheckHours,Locale,isnull(LocaleMethod,'0'),isnull(OtherPay,0),isnull(OtherPay2,0),isnull(OtherPay3,0),
      isnull(OtherPay4,0),isnull(OtherPay5,0),isnull(CostRateMeth,0),isnull(CostRateTableNo,0),isnull(PayRateMeth,0),isnull(PayRateTableNo,0),isnull(PriorYearsFirm,0),PaychexCode1,PaychexCode2,PaychexCode3,isnull(IncludeLocalJurisOnly,'N'),isnull(AllowChargeUnits,'N'),isnull(RequireStartEndTime,'N'),
      isnull(AllowBreakTime,'N'),DefaultBreakStartDateTime,DefaultBreakEndDateTime,PaychexRateNumber,isnull(EmailPayrollRemittance,'N'),isnull(EmailExpenseRemittance,'N'),OccupationalCode,GeographicCode,isnull(StatutoryEmployee,'N'),
      isnull(RetirementPlan,'N'),isnull(ThirdPartySickPay,'N'),ClieOpTransactionType,isnull(ClieOp,'N'),ClieOpAccount,ClieOpAccountType,SEPAIBAN,SEPABIC,TaxRegistrationNumber,isnull(DisableTSRevAudit,'N')
      ,UIPaymentMethod,DefaultTaxLocation,isnull(Terminated,'N'),CreateUser,isnull(CreateDate,getutcdate()),ModUser,isnull(ModDate,getutcdate()),
     isnull(FormW4Version,2019),isnull(FormW4Step2,'N'),isnull(FormW4Dependents,0),isnull(FormW4DependentsOther,0),isnull(FormW4OtherTaxCredit,0),isnull(FormW4OtherIncome,0),isnull(FormW4Deductions,0)
    FROM inserted
End

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateEM] 
   ON  [dbo].[EM]
instead of Update
as
BEGIN
  SET NOCOUNT ON;

  DECLARE @Employee		Nvarchar(30),
	  @EmployeeCompany	Nvarchar(12)

  DECLARE EMUpdCursor CURSOR FOR
	select Employee, EmployeeCompany from inserted
  
  OPEN EMUpdCursor
  FETCH EMUpdCursor into @Employee, @EmployeeCompany
  WHILE @@FETCH_STATUS = 0
  BEGIN
    UPDATE EMMain SET
--    Employee = i.Employee,
      HomeCompany = i.HomeCompany,
      LastName = i.LastName,
      FirstName = i.FirstName,
      MiddleName = i.MiddleName,
      BillingPool = i.BillingPool,
      SSN = i.SSN,
      Address1 = i.Address1,
      Address2 = i.Address2,
      Address3 = i.Address3,
      City = i.City,
      State = i.State,
      ZIP = i.ZIP,
      Country = i.Country,
      HomePhone = i.HomePhone,
      Fax = i.Fax,
      EMail = i.EMail,
      TKAdminLevel = i.TKAdminLevel,
      TKAdminEdit = i.TKAdminEdit,
      EKAdminLevel = i.EKAdminLevel,
      EKAdminEdit = i.EKAdminEdit,
--    Memo = i.Memo,
      EmployeePhoto = i.EmployeePhoto,
      Salutation = i.Salutation,
      Suffix = i.Suffix,
      Title = i.Title,
      ExportInd = i.ExportInd,
      WorkPhone = i.WorkPhone,
      MobilePhone = i.MobilePhone,
      AvailableForCRM = i.AvailableForCRM,
      ReadyForApproval = i.ReadyForApproval,
      PreferredName = i.PreferredName,
      HomePhoneFormat = i.HomePhoneFormat,
      FaxFormat = i.FaxFormat,
      WorkPhoneFormat = i.WorkPhoneFormat,
      MobilePhoneFormat = i.MobilePhoneFormat,
      Language = i.Language,
      TargetRatio = i.TargetRatio,
      UtilizationRatio = i.UtilizationRatio,
      ConsultantInd = i.ConsultantInd,
      ClientVendorInd = i.ClientVendorInd,
      ClientID = i.ClientID,
      Vendor = i.Vendor,
      TalentUserID = i.TalentUserID,
      Location = i.Location,
      TLInternalKey = i.TLInternalKey,
      ProfessionalSuffix = i.ProfessionalSuffix,
      TalentModDate = i.TalentModDate,
      TLSyncModDate = i.TLSyncModDate,
      KonaUsername = i.KonaUsername, 
      KonaAccessToken = i.KonaAccessToken, 
      KonaRefreshToken = i.KonaRefreshToken, 
      KonaUserID = i.KonaUserID,
      CitizenshipStatus= i.CitizenshipStatus, 
      QBOID = i.QBOID,
      QBOAddressID = i.QBOAddressID,
      QBOLastUpdated = i.QBOLastUpdated,
	  QBOVendorID = i.QBOVendorID,
	  PIMID= i.PIMID,
      CreateUser = i.CreateUser,
      CreateDate = i.CreateDate,
      ModUser = i.ModUser,
      ModDate = i.ModDate
    FROM inserted i
    inner join EMMain on EMMain.Employee = i.Employee 
    and i.Employee = @Employee 

    if update([Memo])
      UPDATE EMMain SET Memo = i.Memo
	    FROM inserted i
      inner join EMMain on EMMain.Employee = i.Employee 
      and i.Employee = @Employee

    if not exists(select 'x' from EMCompany where EMCompany.Employee = @Employee and EMCompany.EmployeeCompany = @EmployeeCompany)
    INSERT INTO EMCompany
     (Employee,EmployeeCompany,JobCostRate,JobCostType,JCOvtPct,HoursPerDay,HireDate,RaiseDate,Status,Type,Org,Region,BillingCategory,TKGroup,EKGroup,PayRate,PayType,PayOvtPct,PaySpecialOvtPct,
      ADPFileNumber,ADPCompanyCode,ADPRateCode,ProvCostRate,ProvBillRate,ProvCostOTPct,ProvBillOTPct,DefaultLC1,DefaultLC2,DefaultLC3,DefaultLC4,DefaultLC5,ChangeDefaultLC,TerminationDate,
      UseTotalHrsAsStd,JCSpecialOvtPct,ProvCostSpecialOTPct,ProvBillSpecialOTPct,YearsOtherFirms,Supervisor,ReadyForProcessing,CheckHours,Locale,LocaleMethod,OtherPay,OtherPay2,OtherPay3,
      OtherPay4,OtherPay5,CostRateMeth,CostRateTableNo,PayRateMeth,PayRateTableNo,PriorYearsFirm,PaychexCode1,PaychexCode2,PaychexCode3,IncludeLocalJurisOnly,AllowChargeUnits,RequireStartEndTime,
      AllowBreakTime,DefaultBreakStartDateTime,DefaultBreakEndDateTime,PaychexRateNumber,EmailPayrollRemittance,EmailExpenseRemittance,OccupationalCode,GeographicCode,StatutoryEmployee,
      RetirementPlan,ThirdPartySickPay,ClieOpTransactionType,ClieOp,ClieOpAccount,ClieOpAccountType,SEPAIBAN,SEPABIC,TaxRegistrationNumber,DisableTSRevAudit,UIPaymentMethod,DefaultTaxLocation,Terminated,CreateUser,CreateDate,ModUser,ModDate,
	 FormW4Version,FormW4Step2,FormW4Dependents,FormW4DependentsOther,FormW4OtherTaxCredit,FormW4OtherIncome,FormW4Deductions)
    SELECT
      Employee,EmployeeCompany,JobCostRate,JobCostType,JCOvtPct,HoursPerDay,HireDate,RaiseDate,Status,Type,Org,Region,BillingCategory,TKGroup,EKGroup,PayRate,PayType,PayOvtPct,PaySpecialOvtPct,
      ADPFileNumber,ADPCompanyCode,ADPRateCode,ProvCostRate,ProvBillRate,ProvCostOTPct,ProvBillOTPct,DefaultLC1,DefaultLC2,DefaultLC3,DefaultLC4,DefaultLC5,ChangeDefaultLC,TerminationDate,
      UseTotalHrsAsStd,JCSpecialOvtPct,ProvCostSpecialOTPct,ProvBillSpecialOTPct,YearsOtherFirms,Supervisor,ReadyForProcessing,CheckHours,Locale,LocaleMethod,OtherPay,OtherPay2,OtherPay3,
      OtherPay4,OtherPay5,CostRateMeth,CostRateTableNo,PayRateMeth,PayRateTableNo,PriorYearsFirm,PaychexCode1,PaychexCode2,PaychexCode3,IncludeLocalJurisOnly,AllowChargeUnits,RequireStartEndTime,
      AllowBreakTime,DefaultBreakStartDateTime,DefaultBreakEndDateTime,PaychexRateNumber,EmailPayrollRemittance,EmailExpenseRemittance,OccupationalCode,GeographicCode,StatutoryEmployee,
      RetirementPlan,ThirdPartySickPay,ClieOpTransactionType,ClieOp,ClieOpAccount,ClieOpAccountType,SEPAIBAN,SEPABIC,TaxRegistrationNumber,DisableTSRevAudit,UIPaymentMethod,DefaultTaxLocation,Terminated,CreateUser,CreateDate,ModUser,ModDate,
     FormW4Version,FormW4Step2,FormW4Dependents,FormW4DependentsOther,FormW4OtherTaxCredit,FormW4OtherIncome,FormW4Deductions
    FROM inserted i where i.Employee = @Employee and i.EmployeeCompany = @EmployeeCompany
    else
      UPDATE EMCompany SET
--      Employee = i.Employee,
--      EmployeeCompany = i.EmployeeCompany,
        JobCostRate = i.JobCostRate,
        JobCostType = i.JobCostType,
        JCOvtPct = i.JCOvtPct,
        HoursPerDay = i.HoursPerDay,
        HireDate = i.HireDate,
        RaiseDate = i.RaiseDate,
        Status = i.Status,
        Type = i.Type,
        Org = i.Org,
        Region = i.Region,
        BillingCategory = i.BillingCategory,
        TKGroup = i.TKGroup,
        EKGroup = i.EKGroup,
        PayRate = i.PayRate,
        PayType = i.PayType,
        PayOvtPct = i.PayOvtPct,
        PaySpecialOvtPct = i.PaySpecialOvtPct,
        ADPFileNumber = i.ADPFileNumber,
        ADPCompanyCode = i.ADPCompanyCode,
        ADPRateCode = i.ADPRateCode,
        ProvCostRate = i.ProvCostRate,
        ProvBillRate = i.ProvBillRate,
        ProvCostOTPct = i.ProvCostOTPct,
        ProvBillOTPct = i.ProvBillOTPct,
        DefaultLC1 = i.DefaultLC1,
        DefaultLC2 = i.DefaultLC2,
        DefaultLC3 = i.DefaultLC3,
        DefaultLC4 = i.DefaultLC4,
        DefaultLC5 = i.DefaultLC5,
        ChangeDefaultLC = i.ChangeDefaultLC,
        TerminationDate = i.TerminationDate,
        UseTotalHrsAsStd = i.UseTotalHrsAsStd,
        JCSpecialOvtPct = i.JCSpecialOvtPct,
        ProvCostSpecialOTPct = i.ProvCostSpecialOTPct,
        ProvBillSpecialOTPct = i.ProvBillSpecialOTPct,
        YearsOtherFirms = i.YearsOtherFirms,
        Supervisor = i.Supervisor,
        ReadyForProcessing = i.ReadyForProcessing,
        CheckHours = i.CheckHours,
        Locale = i.Locale,
        LocaleMethod = i.LocaleMethod,
        OtherPay = i.OtherPay,
        OtherPay2 = i.OtherPay2,
        OtherPay3 = i.OtherPay3,
        OtherPay4 = i.OtherPay4,
        OtherPay5 = i.OtherPay5,
        CostRateMeth = i.CostRateMeth,
        CostRateTableNo = i.CostRateTableNo,
        PayRateMeth = i.PayRateMeth,
        PayRateTableNo = i.PayRateTableNo,
        PriorYearsFirm = i.PriorYearsFirm,
        PaychexCode1 = i.PaychexCode1,
        PaychexCode2 = i.PaychexCode2,
        PaychexCode3 = i.PaychexCode3,
        IncludeLocalJurisOnly = i.IncludeLocalJurisOnly,
        AllowChargeUnits = i.AllowChargeUnits,
        RequireStartEndTime = i.RequireStartEndTime,
        AllowBreakTime = i.AllowBreakTime,
        DefaultBreakStartDateTime = i.DefaultBreakStartDateTime,
        DefaultBreakEndDateTime = i.DefaultBreakEndDateTime,
        PaychexRateNumber = i.PaychexRateNumber,
        EmailPayrollRemittance = i.EmailPayrollRemittance,
        EmailExpenseRemittance = i.EmailExpenseRemittance,
        OccupationalCode = i.OccupationalCode,
        GeographicCode = i.GeographicCode,
        StatutoryEmployee = i.StatutoryEmployee,
        RetirementPlan = i.RetirementPlan,
        ThirdPartySickPay = i.ThirdPartySickPay,
        ClieOpTransactionType = i.ClieOpTransactionType,
        ClieOp = i.ClieOp,
        ClieOpAccount = i.ClieOpAccount,
        ClieOpAccountType = i.ClieOpAccountType,
        SEPAIBAN = i.SEPAIBAN,
        SEPABIC = i.SEPABIC,
        TaxRegistrationNumber = i.TaxRegistrationNumber,
        DisableTSRevAudit = i.DisableTSRevAudit,
		UIPaymentMethod = i.UIPaymentMethod,
        DefaultTaxLocation = i.DefaultTaxLocation,
		Terminated = i.Terminated,
		CreateUser = i.CreateUser,
		CreateDate = i.CreateDate,
		ModUser = i.ModUser,
		ModDate = i.ModDate,
    FormW4Version = i.FormW4Version,
	FormW4Step2 = i.FormW4Step2,
	FormW4Dependents = i.FormW4Dependents,
	FormW4DependentsOther = i.FormW4DependentsOther,
	FormW4OtherTaxCredit = i.FormW4OtherTaxCredit,
	FormW4OtherIncome = i.FormW4OtherIncome,
	FormW4Deductions = i.FormW4Deductions
      FROM inserted i 
      inner join EMCompany on EMCompany.Employee = i.Employee and EMCompany.EmployeeCompany = i.EmployeeCompany
      where i.Employee = @Employee and i.EmployeeCompany = @EmployeeCompany

      FETCH EMUpdCursor into @Employee, @EmployeeCompany
   END
   CLOSE EMUpdCursor
   DEALLOCATE EMUpdCursor
END
GO
