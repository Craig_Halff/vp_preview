SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGProjectType]
as
SELECT dbo.CFGProjectTypeData.Code, dbo.CFGProjectTypeDescriptions.Description, dbo.CFGProjectTypeDescriptions.Seq
FROM         dbo.CFGProjectTypeData Left JOIN
dbo.CFGProjectTypeDescriptions ON dbo.CFGProjectTypeData.Code = dbo.CFGProjectTypeDescriptions.Code 
and dbo.CFGProjectTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGProjectTypeTrigger] 
   ON  [dbo].[CFGProjectType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGProjectTypeDescriptions where code = @Code
Delete from CFGProjectTypeData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGProjectTypeTrigger] 
   ON  [dbo].[CFGProjectType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGProjectTypeData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGProjectTypeDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGProjectTypeTrigger] 
   ON  [dbo].[CFGProjectType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGProjectTypeData
  set code = i.code
from inserted i
  inner join CFGProjectTypeData on CFGProjectTypeData.code = @Code

if (not exists(Select CFGProjectTypeDescriptions.UICultureName from CFGProjectTypeDescriptions
  where CFGProjectTypeDescriptions.code = @code and CFGProjectTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGProjectTypeDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGProjectTypeDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGProjectTypeDescriptions on CFGProjectTypeDescriptions.code = @code and CFGProjectTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
