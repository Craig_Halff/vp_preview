SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGContactRelationship]
as
SELECT dbo.CFGContactRelationshipData.Code, dbo.CFGContactRelationshipData.InverseCode,
       dbo.CFGContactRelationshipDescriptions.Description, dbo.CFGContactRelationshipDescriptions.Seq
  FROM dbo.CFGContactRelationshipData Left JOIN
       dbo.CFGContactRelationshipDescriptions ON dbo.CFGContactRelationshipData.Code = dbo.CFGContactRelationshipDescriptions.Code and
       dbo.CFGContactRelationshipDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGContactRelationshipTrigger] 
   ON  [dbo].[CFGContactRelationship]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGContactRelationshipDescriptions where code = @Code
Delete from CFGContactRelationshipData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGContactRelationshipTrigger] 
   ON  [dbo].[CFGContactRelationship]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGContactRelationshipData]
  ([Code], [InverseCode])
Select [Code], [InverseCode] from inserted

INSERT INTO [CFGContactRelationshipDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGContactRelationshipTrigger] 
   ON  [dbo].[CFGContactRelationship]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGContactRelationshipData
  set Code = i.Code,
      InverseCode = i.InverseCode	
from inserted i
  inner join CFGContactRelationshipData on CFGContactRelationshipData.code = @Code

if (not exists(Select CFGContactRelationshipDescriptions.UICultureName from CFGContactRelationshipDescriptions
  where CFGContactRelationshipDescriptions.code = @code and CFGContactRelationshipDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGContactRelationshipDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGContactRelationshipDescriptions
      Set Code = i.Code
      , [Description] = i.Description
      , Seq = i.Seq
  From inserted i
  Inner Join CFGContactRelationshipDescriptions on CFGContactRelationshipDescriptions.code = @code and CFGContactRelationshipDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
