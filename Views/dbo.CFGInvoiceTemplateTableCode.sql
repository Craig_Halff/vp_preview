SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGInvoiceTemplateTableCode]
as
SELECT dbo.CFGInvoiceTemplateTableCodeData.Code, dbo.CFGInvoiceTemplateTableCodeDescriptions.Description, dbo.CFGInvoiceTemplateTableCodeDescriptions.Seq
FROM         dbo.CFGInvoiceTemplateTableCodeData Left JOIN
dbo.CFGInvoiceTemplateTableCodeDescriptions ON dbo.CFGInvoiceTemplateTableCodeData.Code = dbo.CFGInvoiceTemplateTableCodeDescriptions.Code 
and dbo.CFGInvoiceTemplateTableCodeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGInvoiceTemplateTableCodeTrigger] 
   ON  [dbo].[CFGInvoiceTemplateTableCode]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGInvoiceTemplateTableCodeDescriptions where code = @Code
Delete from CFGInvoiceTemplateTableCodeData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGInvoiceTemplateTableCodeTrigger] 
   ON  [dbo].[CFGInvoiceTemplateTableCode]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGInvoiceTemplateTableCodeData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGInvoiceTemplateTableCodeDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGInvoiceTemplateTableCodeTrigger] 
   ON  [dbo].[CFGInvoiceTemplateTableCode]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGInvoiceTemplateTableCodeData
  set code = i.code
from inserted i
  inner join CFGInvoiceTemplateTableCodeData on CFGInvoiceTemplateTableCodeData.code = @Code

if (not exists(Select CFGInvoiceTemplateTableCodeDescriptions.UICultureName from CFGInvoiceTemplateTableCodeDescriptions
  where CFGInvoiceTemplateTableCodeDescriptions.code = @code and CFGInvoiceTemplateTableCodeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGInvoiceTemplateTableCodeDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGInvoiceTemplateTableCodeDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGInvoiceTemplateTableCodeDescriptions on CFGInvoiceTemplateTableCodeDescriptions.code = @code and CFGInvoiceTemplateTableCodeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
