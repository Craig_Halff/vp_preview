SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGJobChangeReason]
AS
SELECT
	dbo.CFGJobChangeReasonData.Code,
	dbo.CFGJobChangeReasonDescriptions.[Description],
	dbo.CFGJobChangeReasonDescriptions.Seq,
	dbo.CFGJobChangeReasonData.Status,
	dbo.CFGJobChangeReasonData.Predefined
FROM dbo.CFGJobChangeReasonData
LEFT JOIN dbo.CFGJobChangeReasonDescriptions ON dbo.CFGJobChangeReasonDescriptions.Code = dbo.CFGJobChangeReasonData.Code
	AND dbo.CFGJobChangeReasonDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGJobChangeReasonTrigger]
	ON  [dbo].[CFGJobChangeReason]
INSTEAD OF DELETE
AS
BEGIN
	SET NOCOUNT ON;
             
	DECLARE @Code NVARCHAR(10)
 
	SELECT TOP 1 @Code = Code FROM Deleted
 
	DELETE FROM CFGJobChangeReasonDescriptions WHERE Code = @Code
	DELETE FROM CFGJobChangeReasonData WHERE Code = @Code
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[InsertCFGJobChangeReasonTrigger]
	ON  [dbo].[CFGJobChangeReason]
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON;
 
	INSERT INTO [CFGJobChangeReasonData] ([Code], [Status], [Predefined])
	SELECT [Code], [Status], ISNULL([Predefined], 'N') FROM Inserted
 
	INSERT INTO [CFGJobChangeReasonDescriptions] ([Code], Description, UICultureName, Seq)
	SELECT [Code], [Description], dbo.FW_GetActiveCultureName(), ISNULL(Seq, 0) FROM Inserted
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[UpdateCFGJobChangeReasonTrigger]
	ON  [dbo].[CFGJobChangeReason]
INSTEAD OF UPDATE
AS
BEGIN
	SET NOCOUNT ON;
 
	DECLARE @Code NVARCHAR(10)
 
	IF UPDATE(Code)
		SELECT @Code = Code FROM Deleted
	ELSE
		SELECT @Code = Code FROM Inserted
 
	UPDATE CFGJobChangeReasonData
	SET Code = I.Code,
		Status = I.Status,
		Predefined = ISNULL(I.Predefined, 'N')
	FROM Inserted I
	INNER JOIN CFGJobChangeReasonData on CFGJobChangeReasonData.Code = @Code
 
	IF (NOT EXISTS(
		SELECT CFGJobChangeReasonDescriptions.UICultureName
		FROM CFGJobChangeReasonDescriptions
		WHERE CFGJobChangeReasonDescriptions.Code = @Code
			AND CFGJobChangeReasonDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
	))
		INSERT INTO CFGJobChangeReasonDescriptions ([Code], [UICultureName], [Description], [Seq])
		SELECT [Code], dbo.FW_GetActiveCultureName(), [Description], ISNULL([Seq], 0) FROM Inserted
	ELSE
		UPDATE CFGJobChangeReasonDescriptions
		SET Code = I.Code,
			[Description] = I.Description,
			[Seq] = I.Seq
		FROM Inserted I
		INNER JOIN CFGJobChangeReasonDescriptions ON CFGJobChangeReasonDescriptions.Code = @Code
			AND CFGJobChangeReasonDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
END
GO
