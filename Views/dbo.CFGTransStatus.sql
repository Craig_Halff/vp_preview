SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGTransStatus]
as
SELECT dbo.CFGTransStatusData.Status, dbo.CFGTransStatusDescriptions.Description 
FROM         dbo.CFGTransStatusData Left JOIN
dbo.CFGTransStatusDescriptions ON dbo.CFGTransStatusData.Status = dbo.CFGTransStatusDescriptions.Status 
and dbo.CFGTransStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGTransStatusTrigger] 
   ON  [dbo].[CFGTransStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Status Nvarchar(1)

SELECT top 1 @Status = Status FROM Deleted

Delete from CFGTransStatusDescriptions where Status = @Status
Delete from CFGTransStatusData where Status = @Status

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGTransStatusTrigger] 
   ON  [dbo].[CFGTransStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGTransStatusData]
  ([Status])
Select [Status] from inserted

INSERT INTO [CFGTransStatusDescriptions]
  ([Status] 
  ,Description
  ,UICultureName)
Select [Status],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGTransStatusTrigger] 
   ON  [dbo].[CFGTransStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Status Nvarchar(1)

if update(Status)
  select @Status=Status from deleted
else
  select @Status=Status from inserted

Update CFGTransStatusData
  set Status = i.Status
from inserted i
  inner join CFGTransStatusData on CFGTransStatusData.Status = @Status

if (not exists(Select CFGTransStatusDescriptions.UICultureName from CFGTransStatusDescriptions
  where CFGTransStatusDescriptions.Status = @Status and CFGTransStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGTransStatusDescriptions
      ([Status]
      ,[UICultureName]
      ,[Description])
  Select [Status], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGTransStatusDescriptions
      Set Status = i.Status
      , [Description] = i.Description
  From inserted i
  Inner Join CFGTransStatusDescriptions on CFGTransStatusDescriptions.Status = @Status and CFGTransStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
