SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FW_CFGDataPack] AS
SELECT
    FW_CFGDataPackData.DataPackName,
    FW_CFGDataPackData.Category,
    FW_CFGDataPackDesc.Description,
    FW_CFGDataPackData.JsonDefinition,
    FW_CFGDataPackData.IsCustom,
    FW_CFGDataPackData.CreateUser,
    FW_CFGDataPackData.CreateDate,
    FW_CFGDataPackData.ModUser,
    FW_CFGDataPackData.ModDate
FROM
    FW_CFGDataPackData
    LEFT JOIN FW_CFGDataPackDesc  ON  
	   FW_CFGDataPackData.DataPackName = FW_CFGDataPackDesc.DataPackName AND 
	   FW_CFGDataPackDesc.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DelFW_CFGDataPack] ON [dbo].[FW_CFGDataPack]
INSTEAD OF DELETE
AS 
BEGIN
    SET NOCOUNT ON;

    DELETE FROM FW_CFGDataPackDesc FROM deleted WHERE FW_CFGDataPackDesc.[DataPackName] = deleted.[DataPackName]
    DELETE FROM FW_CFGDataPackData FROM deleted WHERE FW_CFGDataPackData.[DataPackName] = deleted.[DataPackName]
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[InsFW_CFGDataPack] ON [dbo].[FW_CFGDataPack]
INSTEAD OF INSERT
AS 
BEGIN
    SET NOCOUNT ON;

    INSERT INTO FW_CFGDataPackData ([DataPackName], Category, JsonDefinition, IsCustom, CreateUser, CreateDate, ModUser, ModDate )
    SELECT [DataPackName], Category, JsonDefinition, IsCustom, CreateUser, CreateDate, ModUser, ModDate FROM INSERTED 

    INSERT INTO FW_CFGDataPackDesc ([DataPackName], UICultureName, [Description])
    SELECT [DataPackName], dbo.FW_GetActiveCultureName(), [Description] FROM INSERTED
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[UpdFW_CFGDataPack] ON [dbo].[FW_CFGDataPack]
INSTEAD OF UPDATE
AS 
BEGIN
    SET NOCOUNT ON;

    DECLARE @Code nvarchar(100)

    IF (UPDATE(DataPackName))
	   SELECT @Code=[DataPackName] FROM deleted
    ELSE
	   SELECT @Code=[DataPackName] FROM inserted

    UPDATE FW_CFGDataPackData
    SET	  DataPackName = i.DataPackName, 
		  Category = i.Category,
		  JsonDefinition = i.JsonDefinition,
		  IsCustom = i.IsCustom,
		  CreateUser = i.CreateUser,
		  CreateDate = i.CreateDate,
		  ModUser = i.ModUser,
		  ModDate = i.ModDate
    FROM	  inserted i
		  INNER JOIN FW_CFGDataPackData ON FW_CFGDataPackData.DataPackName = @Code

    IF (not exists(SELECT FW_CFGDataPackDesc.UICultureName FROM FW_CFGDataPackDesc WHERE FW_CFGDataPackDesc.DataPackName = @code AND FW_CFGDataPackDesc.UICultureName =dbo.FW_GetActiveCultureName()))
	   INSERT INTO FW_CFGDataPackDesc (
		  [DataPackName],
		  [UICultureName],
		  [Description])
	   SELECT [DataPackName], dbo.FW_GetActiveCultureName(), [Description] FROM Inserted 
    ELSE
	   UPDATE FW_CFGDataPackDesc
	   SET	 [DataPackName] = i.[DataPackName],
			 [Description] = i.[Description]
	   FROM	 inserted i
			 INNER JOIN FW_CFGDataPackDesc ON FW_CFGDataPackDesc.DataPackName = @code AND FW_CFGDataPackDesc.UICultureName =dbo.FW_GetActiveCultureName() 
END
GO
