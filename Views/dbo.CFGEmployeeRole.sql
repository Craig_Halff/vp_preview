SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGEmployeeRole]
as
SELECT dbo.CFGEmployeeRoleData.DefaultInd, dbo.CFGEmployeeRoleData.Code, dbo.FW_CFGLabelData.LabelValue as Description, 
case dbo.CFGEmployeeRoleData.Code when 'sysPR' then -6 when 'sysPM' then -5 when 'sysSP' then -4 when 'sysPRM' then -3 when 'sysBDL' then -2 when 'sysMC' then -1 else 0 end as Seq
FROM dbo.CFGEmployeeRoleData JOIN dbo.FW_CFGLabelData
  ON dbo.CFGEmployeeRoleData.Code = dbo.FW_CFGLabelData.LabelName
 and dbo.FW_CFGLabelData.UICultureName = dbo.FW_GetActiveCultureName()
UNION
SELECT dbo.CFGEmployeeRoleData.DefaultInd, dbo.CFGEmployeeRoleData.Code, dbo.CFGEmployeeRoleDescriptions.Description, dbo.CFGEmployeeRoleDescriptions.Seq
FROM dbo.CFGEmployeeRoleData Left JOIN dbo.CFGEmployeeRoleDescriptions 
  ON dbo.CFGEmployeeRoleData.Code = dbo.CFGEmployeeRoleDescriptions.Code 
 and dbo.CFGEmployeeRoleDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
--excluding sysPR/sysPM/sysSP because these label desscriptions are from CFGLabels
WHERE dbo.CFGEmployeeRoleData.code NOT IN ('sysPR','sysPM','sysSP','sysPRM','sysBDL','sysMC')
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGEmployeeRoleTrigger] 
   ON  [dbo].[CFGEmployeeRole]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGEmployeeRoleDescriptions where code = @Code
Delete from CFGEmployeeRoleData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGEmployeeRoleTrigger] 
   ON  [dbo].[CFGEmployeeRole]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGEmployeeRoleData]
  ([Code], [DefaultInd])
Select [Code], isnull(DefaultInd, 'N') from inserted

INSERT INTO [CFGEmployeeRoleDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(), isnull(seq,0) from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGEmployeeRoleTrigger] 
   ON  [dbo].[CFGEmployeeRole]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGEmployeeRoleData
  set code = i.code, DefaultInd= i.defaultInd
from inserted i
  inner join CFGEmployeeRoleData on CFGEmployeeRoleData.code = @Code

if (not exists(Select CFGEmployeeRoleDescriptions.UICultureName from CFGEmployeeRoleDescriptions
  where CFGEmployeeRoleDescriptions.code = @code and CFGEmployeeRoleDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGEmployeeRoleDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGEmployeeRoleDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGEmployeeRoleDescriptions on CFGEmployeeRoleDescriptions.code = @code and CFGEmployeeRoleDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
