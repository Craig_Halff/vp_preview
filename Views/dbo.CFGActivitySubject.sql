SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGActivitySubject]
AS
SELECT dbo.CFGActivitySubjectData.*
  FROM dbo.CFGActivitySubjectData
 WHERE dbo.CFGActivitySubjectData.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[DeleteCFGActivitySubjectTrigger] 
   ON  [dbo].[CFGActivitySubject]

instead of Delete
AS 
BEGIN
     -- SET NOCOUNT ON added to prevent extra result sets from
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
DECLARE @PKey Nvarchar(50)
 
SELECT top 1 @PKey = Subject FROM Deleted
 
Delete from CFGActivitySubjectData where CFGActivitySubjectData.Subject = @PKey  

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[InsertCFGActivitySubjectTrigger] 
  ON  [dbo].[CFGActivitySubject]
instead of Insert
as
BEGIN
     -- SET NOCOUNT ON added to prevent extra result sets from
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
DECLARE @PKey Nvarchar(50)
 
select @PKEY =Subject from inserted
 
if (not exists (Select CFGActivitySubjectData.UICultureName from CFGActivitySubjectData, Inserted I
           Where  CFGActivitySubjectData.Subject = @PKEY  and  CFGActivitySubjectData.UICultureName = dbo.FW_GetActiveCultureName()))
	Begin
		 
       Insert into CFGActivitySubjectData  (Subject, UICultureName, Seq, IsMilestone)
           Select Subject,dbo.FW_GetActiveCultureName(),isnull(Seq,0),isnull(IsMilestone,'N') from inserted
 
 end

Else
     update CFGActivitySubjectData
             SET [Seq] = isnull(i.Seq,0), [IsMilestone] = isnull(i.IsMilestone,'N')
           from inserted i
           inner join CFGActivitySubjectData on CFGActivitySubjectData.Subject = @PKEY

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create  TRIGGER [dbo].[UpdateCFGActivitySubjectTrigger] 
  ON  [dbo].[CFGActivitySubject]
instead of Update
AS 
BEGIN
     -- SET NOCOUNT ON added to prevent extra result sets from
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
  
DECLARE @PKEY Nvarchar(50)
 
if update(Subject)
	select @PKEY = Subject from deleted
else
	select @PKEY = Subject from inserted
 
if (not exists (Select CFGActivitySubjectData.UICultureName from CFGActivitySubjectData, Inserted I
           Where  CFGActivitySubjectData.Subject = @PKEY  and CFGActivitySubjectData.UICultureName = dbo.FW_GetActiveCultureName()))
	begin
	
      		 Insert into CFGActivitySubjectData  (Subject, UICultureName, Seq, IsMilestone)
           		Select Subject,dbo.FW_GetActiveCultureName(),Seq,IsMilestone from inserted
	end
Else
     update CFGActivitySubjectData
             SET [Seq] = i.Seq, [IsMilestone] = i.IsMilestone
           from inserted i
           inner join CFGActivitySubjectData on CFGActivitySubjectData.Subject = @PKEY

END
GO
