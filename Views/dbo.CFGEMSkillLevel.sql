SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGEMSkillLevel]
as
SELECT dbo.CFGEMSkillLevelData.Code, dbo.CFGEMSkillLevelDescriptions.Description, dbo.CFGEMSkillLevelDescriptions.Seq
FROM         dbo.CFGEMSkillLevelData Left JOIN
dbo.CFGEMSkillLevelDescriptions ON dbo.CFGEMSkillLevelData.Code = dbo.CFGEMSkillLevelDescriptions.Code 
and dbo.CFGEMSkillLevelDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGEMSkillLevelTrigger] 
   ON  [dbo].[CFGEMSkillLevel]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code smallint

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGEMSkillLevelDescriptions where code = @Code
Delete from CFGEMSkillLevelData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGEMSkillLevelTrigger] 
   ON  [dbo].[CFGEMSkillLevel]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGEMSkillLevelData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGEMSkillLevelDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGEMSkillLevelTrigger] 
   ON  [dbo].[CFGEMSkillLevel]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code smallint

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGEMSkillLevelData
  set code = i.code
from inserted i
  inner join CFGEMSkillLevelData on CFGEMSkillLevelData.code = @Code

if (not exists(Select CFGEMSkillLevelDescriptions.UICultureName from CFGEMSkillLevelDescriptions
  where CFGEMSkillLevelDescriptions.code = @code and CFGEMSkillLevelDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGEMSkillLevelDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGEMSkillLevelDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGEMSkillLevelDescriptions on CFGEMSkillLevelDescriptions.code = @code and CFGEMSkillLevelDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
