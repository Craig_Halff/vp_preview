SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[Alerts]
as
SELECT dbo.AlertsData.AlertID, dbo.AlertsData.NavID, dbo.AlertsData.ProgID,
	   dbo.AlertsData.active, dbo.AlertsData.dashboard, dbo.AlertsData.email,
       dbo.AlertsData.config,dbo.AlertsData.configXML,dbo.AlertsData.modDate,dbo.AlertsData.Company,
       dbo.AlertsDescriptions.AlertName
FROM   dbo.AlertsData
 Left JOIN dbo.AlertsDescriptions ON dbo.AlertsData.AlertID = dbo.AlertsDescriptions.AlertID
 and dbo.AlertsDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteAlertsTrigger] 
   ON  [dbo].[Alerts]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @AlertID Nvarchar(50)

SELECT top 1 @AlertID = AlertID FROM Deleted

Delete from AlertsDescriptions where AlertsDescriptions.AlertID = @AlertID
       

Delete from AlertsData where AlertsData.AlertID = @AlertID
         
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertAlertsTrigger] 
   ON  [dbo].[Alerts]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [AlertsData]
  (AlertID,
   NavID,
   ProgID,
   Active, 
   Dashboard,
   Email,
   Config,
   ConfigXML,
   ModDate,
   Company)
Select AlertID,
 NavID,
 ProgID,
 isnull(Active, 'N'),
 isnull(Dashboard, 'N'),
 isnull(Email, 'N'),
 Config,
 ConfigXML,
 isnull(ModDate,getdate()),
 Company
 from inserted

if (not exists(Select AlertsDescriptions.UICultureName from AlertsDescriptions, Inserted I
  where  AlertsDescriptions.AlertID = I.AlertID
        and AlertsDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
	INSERT INTO [AlertsDescriptions]
	  (AlertID,  AlertName, UICultureName)
	Select AlertID,  AlertName, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateAlertsTrigger] 
   ON  [dbo].[Alerts]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @AlertID Nvarchar(50)

if update(AlertID)
  select @AlertID=AlertID from deleted
else
  select @AlertID=AlertID from inserted

Update AlertsData
  set AlertID = i.AlertID, NavID=i.NavID, Company = i.Company, 
      ProgID = i.ProgID, Active = i.Active, Dashboard= i.Dashboard, Email = i.Email, Config=i.Config, ConfigXML= i.ConfigXML , ModDate = i.ModDate
from inserted i
  inner join AlertsData on AlertsData.NavID = i.NavID
		and  AlertsData.company = i.company
		and  AlertsData.Alertid = @AlertID

if (not exists(Select AlertsDescriptions.UICultureName from AlertsDescriptions, Inserted I
  where  AlertsDescriptions.AlertID = @AlertID
        and AlertsDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into AlertsDescriptions
      (AlertID,
       UICultureName,
       AlertName)
  Select AlertID,  dbo.FW_GetActiveCultureName(), AlertName From Inserted 

Else
  Update AlertsDescriptions
      Set AlertID = i.AlertID,
          AlertName = i.AlertName
  From inserted i
  Inner Join AlertsDescriptions on  AlertsDescriptions.AlertID = @AlertID
         and AlertsDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
