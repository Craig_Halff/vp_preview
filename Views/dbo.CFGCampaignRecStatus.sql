SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGCampaignRecStatus]
as
SELECT dbo.CFGCampaignRecStatusData.Code, dbo.CFGCampaignRecStatusDescriptions.Description
  FROM dbo.CFGCampaignRecStatusData Left JOIN
       dbo.CFGCampaignRecStatusDescriptions ON dbo.CFGCampaignRecStatusData.Code = dbo.CFGCampaignRecStatusDescriptions.Code
   and dbo.CFGCampaignRecStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGCampaignRecStatusTrigger] 
   ON  [dbo].[CFGCampaignRecStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGCampaignRecStatusDescriptions where Code = @Code
Delete from CFGCampaignRecStatusData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGCampaignRecStatusTrigger] 
   ON  [dbo].[CFGCampaignRecStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGCampaignRecStatusData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGCampaignRecStatusDescriptions]
  ([Code] 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGCampaignRecStatusTrigger] 
   ON  [dbo].[CFGCampaignRecStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGCampaignRecStatusData
  set Code = i.Code
from inserted i
  inner join CFGCampaignRecStatusData on CFGCampaignRecStatusData.Code = @Code

if (not exists(Select CFGCampaignRecStatusDescriptions.UICultureName from CFGCampaignRecStatusDescriptions
  where CFGCampaignRecStatusDescriptions.Code = @Code and CFGCampaignRecStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGCampaignRecStatusDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGCampaignRecStatusDescriptions
      Set Code = i.Code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGCampaignRecStatusDescriptions on CFGCampaignRecStatusDescriptions.Code = @Code and CFGCampaignRecStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
