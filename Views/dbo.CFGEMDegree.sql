SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGEMDegree]
as
SELECT dbo.CFGEMDegreeData.Code, dbo.CFGEMDegreeDescriptions.Description, dbo.CFGEMDegreeDescriptions.Seq
FROM         dbo.CFGEMDegreeData Left JOIN
dbo.CFGEMDegreeDescriptions ON dbo.CFGEMDegreeData.Code = dbo.CFGEMDegreeDescriptions.Code 
and dbo.CFGEMDegreeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGEMDegreeTrigger] 
   ON  [dbo].[CFGEMDegree]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGEMDegreeDescriptions where code = @Code
Delete from CFGEMDegreeData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGEMDegreeTrigger] 
   ON  [dbo].[CFGEMDegree]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGEMDegreeData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGEMDegreeDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGEMDegreeTrigger] 
   ON  [dbo].[CFGEMDegree]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGEMDegreeData
  set code = i.code
from inserted i
  inner join CFGEMDegreeData on CFGEMDegreeData.code = @Code

if (not exists(Select CFGEMDegreeDescriptions.UICultureName from CFGEMDegreeDescriptions
  where CFGEMDegreeDescriptions.code = @code and CFGEMDegreeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGEMDegreeDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGEMDegreeDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGEMDegreeDescriptions on CFGEMDegreeDescriptions.code = @code and CFGEMDegreeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
