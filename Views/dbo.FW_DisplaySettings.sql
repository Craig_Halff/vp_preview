SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FW_DisplaySettings]
AS
SELECT dbo.FW_DisplaySettingsData.Settings_UID, dbo.FW_DisplaySettingsData.Code, dbo.FW_DisplaySettingsData.UISettings, 
       dbo.FW_DisplaySettingsDesc.UICultureName, dbo.FW_DisplaySettingsDesc.Description
  FROM dbo.FW_DisplaySettingsData 
       LEFT OUTER JOIN dbo.FW_DisplaySettingsDesc ON 
		dbo.FW_DisplaySettingsData.Settings_UID = dbo.FW_DisplaySettingsDesc.Settings_UID AND 
		dbo.FW_DisplaySettingsDesc.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DelFW_DisplaySettings] 
  ON  [dbo].[FW_DisplaySettings]
INSTEAD OF DELETE
AS 
BEGIN
   SET NOCOUNT ON;

DELETE FROM FW_DisplaySettingsDesc FROM deleted WHERE FW_DisplaySettingsDesc.Settings_UID = deleted.Settings_UID
DELETE FROM FW_DisplaySettingsData FROM deleted WHERE FW_DisplaySettingsData.Settings_UID = deleted.Settings_UID

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[InsFW_DisplaySettings] 
  ON  [dbo].[FW_DisplaySettings]
INSTEAD OF Insert
AS
BEGIN
     SET NOCOUNT ON;

INSERT INTO FW_DisplaySettingsData
  (Settings_UID
   ,Code
   ,UISettings)
SELECT Settings_UID, Code, UISettings FROM inserted

INSERT INTO FW_DisplaySettingsDesc
  (Settings_UID 
  ,UICultureName
  ,Description)
SELECT Settings_UID, dbo.FW_GetActiveCultureName(),Description FROM inserted

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[UpdFW_DisplaySettings] 
  ON  [dbo].[FW_DisplaySettings]
INSTEAD OF UPDATE
AS 
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
DECLARE @Settings_UID varchar(32)
IF (UPDATE(Settings_UID))
	SELECT @Settings_UID = Settings_UID FROM deleted
ELSE
	SELECT @Settings_UID = Settings_UID FROM inserted


UPDATE FW_DisplaySettingsData
	SET Code = i.Code, UISettings = i.UISettings
 FROM inserted i
    INNER JOIN FW_DisplaySettingsData ON FW_DisplaySettingsData.Settings_UID = @Settings_UID


IF (not exists (SELECT 'x' FROM FW_DisplaySettingsDesc
           WHERE  FW_DisplaySettingsDesc.Settings_UID = @Settings_UID
     AND FW_DisplaySettingsDesc.UICultureName = dbo.FW_GetActiveCultureName()))
     Insert into FW_DisplaySettingsDesc
	(Settings_UID, UICultureName, Description)
     SELECT 
	 Settings_UID, dbo.FW_GetActiveCultureName(), Description FROM inserted
 
ELSE
     UPDATE FW_DisplaySettingsDesc
           SET Description = i.Description
           FROM inserted i
           INNER JOIN FW_DisplaySettingsDesc ON FW_DisplaySettingsDesc.Settings_UID = @Settings_UID
           AND FW_DisplaySettingsDesc.UICultureName = dbo.FW_GetActiveCultureName()
 
END
GO
