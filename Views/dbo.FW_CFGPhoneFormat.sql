SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FW_CFGPhoneFormat]
AS
SELECT dbo.FW_CFGPhoneFormatData.ISOCountryCode,dbo.FW_CFGPhoneFormatData.PhoneFormat, dbo.FW_CFGPhoneFormatData.PKey, dbo.FW_CFGPhoneFormatDesc.Description, dbo.FW_CFGPhoneFormatDesc.Seq
FROM         dbo.FW_CFGPhoneFormatData LEFT JOIN
dbo.FW_CFGPhoneFormatDesc ON dbo.FW_CFGPhoneFormatData.PKey = dbo.FW_CFGPhoneFormatDesc.PKey
and dbo.FW_CFGPhoneFormatDesc.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DelFW_CFGPhoneFormat] 
  ON  [dbo].[FW_CFGPhoneFormat]
INSTEAD OF DELETE
AS 
BEGIN
     SET NOCOUNT ON;

DELETE FROM FW_CFGPhoneFormatDesc FROM deleted WHERE FW_CFGPhoneFormatDesc.Pkey = deleted.Pkey
DELETE FROM FW_CFGPhoneFormatData FROM deleted WHERE FW_CFGPhoneFormatData.Pkey = deleted.Pkey

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[InsFW_CFGPhoneFormat] 
  ON  [dbo].[FW_CFGPhoneFormat]
INSTEAD OF Insert
AS
BEGIN
     SET NOCOUNT ON;

DECLARE @seq AS smallint
SELECT @seq=isnull(seq,0) FROM inserted

INSERT INTO [FW_CFGPhoneFormatData]
  ([Pkey], ISOCountryCode, PhoneFormat)
SELECT [Pkey],ISOCountryCode, PhoneFormat FROM inserted

INSERT INTO [FW_CFGPhoneFormatDesc]
  ([Pkey] 
  ,Description
  ,UICultureName
  ,Seq)
SELECT [Pkey],[Description], dbo.FW_GetActiveCultureName(),@seq FROM inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[UpdFW_CFGPhoneFormat] 
  ON  [dbo].[FW_CFGPhoneFormat]
INSTEAD OF UPDATE
AS
BEGIN
     SET NOCOUNT ON;

DECLARE @Code nvarchar(32)

IF (UPDATE(Pkey))
  SELECT @Code= Pkey FROM deleted
ELSE
  SELECT @Code= Pkey FROM inserted

UPDATE FW_CFGPhoneFormatData
  SET  Pkey = i.Pkey, ISOCountryCode = i.ISOCountryCode, PhoneFormat=i.PhoneFormat
FROM inserted i
  INNER JOIN FW_CFGPhoneFormatData ON FW_CFGPhoneFormatData.Pkey = @Code

IF (not exists(SELECT FW_CFGPhoneFormatDesc.UICultureName FROM FW_CFGPhoneFormatDesc
  WHERE FW_CFGPhoneFormatDesc.Pkey = @code AND FW_CFGPhoneFormatDesc.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into FW_CFGPhoneFormatDesc
      ([Pkey]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  SELECT [Pkey], dbo.FW_GetActiveCultureName(),[Description],isnull([Seq],0) FROM Inserted 

ELSE
  UPDATE FW_CFGPhoneFormatDesc
      SET Pkey = i.Pkey
      , [Description] = i.Description
      , Seq = isnull(i.Seq,0)
  FROM inserted i
  INNER JOIN FW_CFGPhoneFormatDesc ON FW_CFGPhoneFormatDesc.Pkey = @code AND FW_CFGPhoneFormatDesc.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
