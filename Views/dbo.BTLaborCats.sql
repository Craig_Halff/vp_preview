SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[BTLaborCats]
as
SELECT dbo.BTLaborCatsData.Category, dbo.BTLaborCatsData.CategoryCode, dbo.BTLaborCatsDescriptions.Description 
FROM   dbo.BTLaborCatsData Left JOIN
       dbo.BTLaborCatsDescriptions ON dbo.BTLaborCatsData.Category = dbo.BTLaborCatsDescriptions.Category 
   and dbo.BTLaborCatsDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteBTLaborCatsTrigger] 
   ON  [dbo].[BTLaborCats]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Category smallint

SELECT top 1 @Category = Category FROM Deleted

Delete from BTLaborCatsDescriptions where Category = @Category
Delete from BTLaborCatsData where Category = @Category

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertBTLaborCatsTrigger] 
   ON  [dbo].[BTLaborCats]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [BTLaborCatsData]
  ([Category], [CategoryCode])
Select [Category], [CategoryCode] from inserted

INSERT INTO [BTLaborCatsDescriptions]
  ([Category] 
  ,Description
  ,UICultureName)
Select [Category],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateBTLaborCatsTrigger] 
   ON  [dbo].[BTLaborCats]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Category smallint

if update(Category)
  select @Category=Category from deleted
else
  select @Category=Category from inserted

Update BTLaborCatsData
  set Category = i.Category, CategoryCode = i.CategoryCode
from inserted i
  inner join BTLaborCatsData on BTLaborCatsData.Category = @Category

if (not exists(Select BTLaborCatsDescriptions.UICultureName from BTLaborCatsDescriptions
  where BTLaborCatsDescriptions.Category = @Category and BTLaborCatsDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into BTLaborCatsDescriptions
      ([Category]
      ,[UICultureName]
      ,[Description])
  Select [Category], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update BTLaborCatsDescriptions
      Set Category = i.Category
      , [Description] = i.Description
  From inserted i
  Inner Join BTLaborCatsDescriptions on BTLaborCatsDescriptions.Category = @Category and BTLaborCatsDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
