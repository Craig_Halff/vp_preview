SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGFADepMethod]
as
SELECT 	dbo.CFGFADepMethodData.Code,
	dbo.CFGFADepMethodData.Company,
	dbo.CFGFADepMethodDescriptions.Description,
	dbo.CFGFADepMethodData.UsefulLifeYr,
	dbo.CFGFADepMethodData.RecoveryYr,
	dbo.CFGFADepMethodData.InUse
FROM   dbo.CFGFADepMethodData
 Left JOIN dbo.CFGFADepMethodDescriptions ON dbo.CFGFADepMethodData.Code = dbo.CFGFADepMethodDescriptions.Code
and   dbo.CFGFADepMethodData.Company = dbo.CFGFADepMethodDescriptions.Company
and   dbo.CFGFADepMethodDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGFADepMethodTrigger] 
   ON  [dbo].[CFGFADepMethod]
instead of Delete
AS 
BEGIN
SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)
DECLARE @Company Nvarchar(14)

SELECT top 1 @Code = Code, @Company = Company FROM Deleted

Delete from CFGFADepMethodDescriptions where CFGFADepMethodDescriptions.code = @Code and 
            CFGFADepMethodDescriptions.Company = @Company
Delete from CFGFADepMethodData where CFGFADepMethodData.code = @Code and CFGFADepMethodData.Company = @Company

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGFADepMethodTrigger] 
   ON  [dbo].[CFGFADepMethod]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGFADepMethodData]
  (	Code,
	Company,
	UsefulLifeYr,
	RecoveryYr, 
	InUse)
Select 	Code,
	Company,
	isnull(UsefulLifeYr,0),
	isnull(RecoveryYr,0),
	isnull(InUse,'N')
 from inserted

INSERT INTO [CFGFADepMethodDescriptions]
  (Code, Company, Description, UICultureName)
Select Code, Company, Description, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGFADepMethodTrigger] 
   ON  [dbo].[CFGFADepMethod]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(14)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

	
Update CFGFADepMethodData
  set 	Code = i.code,
	UsefulLifeYr = i.UsefulLifeYr,
	RecoveryYr = i.RecoveryYr,
	Company = i.Company,
	InUse= i.InUse
 from inserted i
  inner join CFGFADepMethodData on CFGFADepMethodData.Company = i.Company and CFGFADepMethodData.code = @Code

if (not exists(Select CFGFADepMethodDescriptions.UICultureName from CFGFADepMethodDescriptions, Inserted I
  where CFGFADepMethodDescriptions.Company = i.Company and CFGFADepMethodDescriptions.code = @code and CFGFADepMethodDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGFADepMethodDescriptions
      (Code,
	   Company,
       UICultureName,
       Description)
  Select Code, Company, dbo.FW_GetActiveCultureName(), Description From Inserted 

Else
  Update CFGFADepMethodDescriptions
      Set code = i.code,
          Company = i.Company,
         Description = i.Description
  From inserted i
  Inner Join CFGFADepMethodDescriptions on CFGFADepMethodDescriptions.Company = i.Company and CFGFADepMethodDescriptions.code = @code and CFGFADepMethodDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
