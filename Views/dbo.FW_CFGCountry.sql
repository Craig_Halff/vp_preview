SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FW_CFGCountry]
AS
SELECT dbo.FW_CFGCountryData.ISOCountryCode, dbo.FW_CFGCountryData.DisplayState, dbo.FW_CFGCountryData.AddressFormat, dbo.FW_CFGCountryData.TaxAuthority, dbo.FW_CFGCountryDesc.Country, dbo.FW_CFGCountryDesc.Seq
FROM         dbo.FW_CFGCountryData LEFT JOIN
dbo.FW_CFGCountryDesc ON dbo.FW_CFGCountryData.ISOCountryCode = dbo.FW_CFGCountryDesc.ISOCountryCode 
and dbo.FW_CFGCountryDesc.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DelFW_CFGCountry] 
  ON  [dbo].[FW_CFGCountry]
INSTEAD OF DELETE
AS 
BEGIN
     SET NOCOUNT ON;

DELETE FROM FW_CFGCountryDesc FROM deleted WHERE FW_CFGCountryDesc.ISOCountryCode = deleted.ISOCountryCode
DELETE FROM FW_CFGCountryData FROM deleted WHERE FW_CFGCountryData.ISOCountryCode = deleted.ISOCountryCode

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[InsFW_CFGCountry] 
  ON  [dbo].[FW_CFGCountry]
INSTEAD OF Insert
AS
BEGIN
     SET NOCOUNT ON;

DECLARE @seq AS smallint
SELECT @seq=isnull(seq,0) FROM inserted

INSERT INTO [FW_CFGCountryData]
  ([ISOCountryCode]
   ,DisplayState
   ,AddressFormat
   ,TaxAuthority)
SELECT [ISOCountryCode], isnull(DisplayState,'C'), isnull(AddressFormat,1),[TaxAuthority] FROM inserted

INSERT INTO [FW_CFGCountryDesc]
  ([ISOCountryCode] 
  ,Country
  ,UICultureName
  ,Seq)
SELECT [ISOCountryCode],[Country], dbo.FW_GetActiveCultureName(),@seq FROM inserted


End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[UpdFW_CFGCountry] 
  ON  [dbo].[FW_CFGCountry]
INSTEAD OF UPDATE
AS
BEGIN
     SET NOCOUNT ON;

DECLARE @Code nvarchar(10)

IF (UPDATE(ISOCountryCode))
  SELECT @Code=ISOCountryCode FROM deleted
ELSE
  SELECT @Code=ISOCountryCode FROM inserted


UPDATE FW_CFGCountryData
  SET ISOCountryCode = i.ISOCountryCode, DisplayState=i.DisplayState, AddressFormat=i.AddressFormat, TaxAuthority=i.TaxAuthority
FROM inserted i
  INNER JOIN FW_CFGCountryData ON FW_CFGCountryData.ISOCountryCode = @Code

IF (not exists(SELECT FW_CFGCountryDesc.UICultureName FROM FW_CFGCountryDesc
  WHERE FW_CFGCountryDesc.ISOCountryCode = @code AND FW_CFGCountryDesc.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into FW_CFGCountryDesc
      ([ISOCountryCode]
      ,[UICultureName]
      ,[Country]
      ,[Seq])
  SELECT [ISOCountryCode], dbo.FW_GetActiveCultureName(),[Country],isnull([Seq],0) FROM Inserted 

ELSE
  UPDATE FW_CFGCountryDesc
      SET ISOCountryCode = i.ISOCountryCode
      , [Country] = i.Country
      , Seq = isnull(i.Seq,0)
  FROM inserted i
  INNER JOIN FW_CFGCountryDesc ON FW_CFGCountryDesc.ISOCountryCode = @code AND FW_CFGCountryDesc.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
