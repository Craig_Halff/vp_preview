SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGEMRegistrationType]
as
SELECT dbo.CFGEMRegistrationTypeData.Code, dbo.CFGEMRegistrationTypeDescriptions.Description, dbo.CFGEMRegistrationTypeDescriptions.Seq
FROM   dbo.CFGEMRegistrationTypeData Left JOIN
       dbo.CFGEMRegistrationTypeDescriptions ON dbo.CFGEMRegistrationTypeData.Code = dbo.CFGEMRegistrationTypeDescriptions.Code 
   and dbo.CFGEMRegistrationTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGEMRegistrationTypeTrigger] 
   ON  [dbo].[CFGEMRegistrationType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGEMRegistrationTypeDescriptions where code = @Code
Delete from CFGEMRegistrationTypeData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGEMRegistrationTypeTrigger] 
   ON  [dbo].[CFGEMRegistrationType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGEMRegistrationTypeData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGEMRegistrationTypeDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGEMRegistrationTypeTrigger] 
   ON  [dbo].[CFGEMRegistrationType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGEMRegistrationTypeData
  set code = i.code
from inserted i
  inner join CFGEMRegistrationTypeData on CFGEMRegistrationTypeData.code = @Code

if (not exists(Select CFGEMRegistrationTypeDescriptions.UICultureName from CFGEMRegistrationTypeDescriptions
  where CFGEMRegistrationTypeDescriptions.code = @code and CFGEMRegistrationTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGEMRegistrationTypeDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGEMRegistrationTypeDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGEMRegistrationTypeDescriptions on CFGEMRegistrationTypeDescriptions.code = @code and CFGEMRegistrationTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
