SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGUnitTableCode]
as
SELECT dbo.CFGUnitTableCodeData.Code, dbo.CFGUnitTableCodeDescriptions.Description, dbo.CFGUnitTableCodeDescriptions.Seq
FROM         dbo.CFGUnitTableCodeData Left JOIN
dbo.CFGUnitTableCodeDescriptions ON dbo.CFGUnitTableCodeData.Code = dbo.CFGUnitTableCodeDescriptions.Code 
and dbo.CFGUnitTableCodeDescriptions.UICultureName = dbo.FW_GetActiveCultureName() 

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGUnitTableCodeTrigger] 
   ON  [dbo].[CFGUnitTableCode]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGUnitTableCodeDescriptions where code = @Code
Delete from CFGUnitTableCodeData where code = @Code

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGUnitTableCodeTrigger] 
   ON  [dbo].[CFGUnitTableCode]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGUnitTableCodeData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGUnitTableCodeDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGUnitTableCodeTrigger] 
   ON  [dbo].[CFGUnitTableCode]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGUnitTableCodeData
  set code = i.code
from inserted i
  inner join CFGUnitTableCodeData on CFGUnitTableCodeData.code = @Code

if (not exists(Select CFGUnitTableCodeDescriptions.UICultureName from CFGUnitTableCodeDescriptions
  where CFGUnitTableCodeDescriptions.code = @code and CFGUnitTableCodeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGUnitTableCodeDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGUnitTableCodeDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGUnitTableCodeDescriptions on CFGUnitTableCodeDescriptions.code = @code and CFGUnitTableCodeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
