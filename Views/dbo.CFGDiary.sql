SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGDiary]
as
SELECT dbo.CFGDiaryData.Code, 
	ISNULL(dbo.CFGDiaryDescriptions.Description, dbo.FW_TextNotTranslated.LocalizedValue) AS Description, 
	dbo.CFGDiaryDescriptions.Seq
FROM dbo.CFGDiaryData
     LEFT JOIN dbo.CFGDiaryDescriptions ON dbo.CFGDiaryData.Code = dbo.CFGDiaryDescriptions.Code AND dbo.CFGDiaryDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
	 LEFT JOIN dbo.FW_TextNotTranslated ON dbo.FW_TextNotTranslated.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGDiaryTrigger] 
   ON  [dbo].[CFGDiary]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGDiaryDescriptions where code = @Code
Delete from CFGDiaryData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGDiaryTrigger] 
   ON  [dbo].[CFGDiary]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGDiaryData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGDiaryDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),isnull(Seq,0) from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGDiaryTrigger] 
   ON  [dbo].[CFGDiary]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGDiaryData
  set Code = i.Code
from inserted i
  inner join CFGDiaryData on CFGDiaryData.code = @Code

if (not exists(Select CFGDiaryDescriptions.UICultureName from CFGDiaryDescriptions
  where CFGDiaryDescriptions.code = @code and CFGDiaryDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGDiaryDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description], isnull([Seq],0) From Inserted 

Else
  Update CFGDiaryDescriptions
      Set Code = i.Code
      , [Description] = i.Description
      , [Seq] = i.Seq
  From inserted i
  Inner Join CFGDiaryDescriptions on CFGDiaryDescriptions.code = @Code and CFGDiaryDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
