SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[VEProjectAssocTemplate]
AS
SELECT [PKEY]
	,[Clendor].[Vendor] AS [Vendor]
	,[ClendorProjectAssocTemplate].[WBS1]
	,[ClendorProjectAssocTemplate].[WBS2]
	,[ClendorProjectAssocTemplate].[WBS3]
	,[ClendorProjectAssocTemplate].[ROLE]
	,[ClendorProjectAssocTemplate].[RoleDescription]
	,[ClendorProjectAssocTemplate].[TeamStatus]
	,[ClendorProjectAssocTemplate].[Address] AS [VEAddress]
    ,[ClendorProjectAssocTemplate].[VendorInd]
	,[ClendorProjectAssocTemplate].[CreateUser]
	,[ClendorProjectAssocTemplate].[CreateDate]
	,[ClendorProjectAssocTemplate].[ModUser]
	,[ClendorProjectAssocTemplate].[ModDate]
FROM [ClendorProjectAssocTemplate]
INNER JOIN [Clendor] ON [ClendorProjectAssocTemplate].[ClientID] = [Clendor].[Clientid]
	AND [Clendor].[Vendor] IS NOT NULL
	AND [Clendor].[VendorInd] = 'Y'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteVEProjectAssocTemplateTrigger] 
   ON  [dbo].[VEProjectAssocTemplate]
instead of Delete
AS 
BEGIN
    SET NOCOUNT ON;

DELETE FROM [ClendorProjectAssocTemplate]
WHERE EXISTS (
		SELECT 'x'
		FROM deleted
		LEFT JOIN [Clendor] ON deleted.vendor = [Clendor].[vendor]
			 AND [Clendor].[VendorInd] = 'Y'
		WHERE deleted.wbs1 = ClendorProjectAssocTemplate.wbs1
			AND deleted.wbs2 = ClendorProjectAssocTemplate.wbs2
			AND deleted.wbs3 = ClendorProjectAssocTemplate.wbs3
			AND [Clendor].[ClientID] = [ClendorProjectAssocTemplate].[ClientID]
		)


END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertVEProjectAssocTemplateTrigger] 
   ON  [dbo].[VEProjectAssocTemplate]
instead of Insert
as
BEGIN
    SET NOCOUNT ON;
	INSERT INTO ClendorProjectAssocTemplate
	   ([clientid]
	   ,[WBS1]
      ,[WBS2]
      ,[WBS3]
      ,[Role]
      ,[RoleDescription]
      ,[TeamStatus]
      ,[Address]
      ,[VendorInd]
      ,[CreateUser]
      ,[CreateDate]
      ,[ModUser]
      ,[ModDate]
      )

	SELECT 
	  Clendor.clientID
      ,inserted.WBS1
      ,inserted.WBS2
      ,inserted.WBS3
      ,inserted.Role
      ,inserted.RoleDescription
      ,inserted.TeamStatus
      ,inserted.VEAddress
      ,'Y'
      ,inserted.CreateUser
	  ,isnull(inserted.CreateDate,getutcdate())
      ,inserted.ModUser
	  ,isnull(inserted.ModDate,getutcdate())
	FROM inserted inner join [Clendor] on inserted.Vendor = [Clendor].[Vendor] and [Clendor].[Vendorind] = 'Y'

end
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateVEProjectAssocTemplateTrigger] 
   ON  [dbo].[VEProjectAssocTemplate]
instead of Update
as
BEGIN
SET NOCOUNT ON;
if Not Update(Vendor) or Update(wbs1) or update(wbs2) or update(wbs3)
	begin
		UPDATE [ClendorProjectAssocTemplate] SET
			[Role] = i.[Role]
			,[RoleDescription] = i.[RoleDescription]
			,[Address] = i.[VEAddress]
			,[TeamStatus] = i.[TeamStatus]
			,[VendorInd] = i.[VendorInd]
			,CreateUser = i.CreateUser
			,CreateDate = i.CreateDate
			,ModUser = i.ModUser
			,ModDate = i.ModDate
		FROM inserted i
			inner join Clendor on Clendor.vendor = i.vendor 
			where [ClendorProjectAssocTemplate].wbs1 = i.wbs1 and 
			[ClendorProjectAssocTemplate].wbs2 = i.wbs2 and 
			[ClendorProjectAssocTemplate].wbs3 = i.wbs3 and 
			[ClendorProjectAssocTemplate].clientid = Clendor.ClientID
	end
else
  BEGIN
	UPDATE [ClendorProjectAssocTemplate]
	SET  [wbs1] = i.[wbs1]
		,[wbs2] = i.[wbs2]
		,[wbs3] = i.[wbs3]
		,[ClientID] = ClendorNew.[ClientID]
		,[Role] = i.[Role]
		,[Address] = i.[VEAddress]
		,[RoleDescription] = i.[RoleDescription]
		,[VendorInd] = i.[VendorInd]
		,[CreateUser] = i.[CreateUser]
		,[CreateDate] = i.[CreateDate]
		,[ModUser] = i.[ModUser]
		,[ModDate] = i.[ModDate]
	FROM inserted i
	INNER JOIN deleted d ON i.[Pkey] = d.[PKey]
	INNER JOIN Clendor ClendorOld ON ClendorOld.vendor = d.vendor
	LEFT JOIN Clendor ClendorNew ON ClendorNew.vendor = i.vendor
	WHERE [ClendorProjectAssocTemplate].wbs1 = d.wbs1
		AND [ClendorProjectAssocTemplate].wbs2 = d.wbs2
		AND [ClendorProjectAssocTemplate].wbs3 = d.wbs3
		AND [ClendorProjectAssocTemplate].clientid = ClendorOld.ClientID

  END
END
GO
