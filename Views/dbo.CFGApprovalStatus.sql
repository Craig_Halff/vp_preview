SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGApprovalStatus]
as
SELECT dbo.CFGApprovalStatusData.Code, dbo.CFGApprovalStatusData.System, dbo.CFGApprovalStatusData.Active, dbo.CFGApprovalStatusDescriptions.Description, dbo.CFGApprovalStatusDescriptions.Caption, dbo.CFGApprovalStatusDescriptions.Seq
FROM dbo.CFGApprovalStatusData Left JOIN
dbo.CFGApprovalStatusDescriptions ON dbo.CFGApprovalStatusData.Code = dbo.CFGApprovalStatusDescriptions.Code 
and dbo.CFGApprovalStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName() 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGApprovalStatusTrigger] 
   ON  [dbo].[CFGApprovalStatus]
INSTEAD OF Delete
AS 
BEGIN
   SET NOCOUNT ON;

   DELETE FROM CFGApprovalStatusDescriptions FROM deleted WHERE CFGApprovalStatusDescriptions.code = deleted.Code
   DELETE FROM CFGApprovalStatusData FROM deleted WHERE CFGApprovalStatusData.code = deleted.Code
END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGApprovalStatusTrigger] 
   ON  [dbo].[CFGApprovalStatus]
INSTEAD OF Insert
as
BEGIN
   SET NOCOUNT ON;

   DECLARE @Seq as smallint
   select @Seq=isnull(Seq,0) from inserted

   INSERT INTO CFGApprovalStatusData(Code, System, Active)
   select Code, isnull(System, 'N'), isnull(Active, 'Y') from inserted

   INSERT INTO CFGApprovalStatusDescriptions(Code, Description, UICultureName, Caption, Seq)
   Select Code, Description, dbo.FW_GetActiveCultureName(), Caption, @Seq from inserted

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGApprovalStatusTrigger] 
  ON  [dbo].[CFGApprovalStatus]
INSTEAD OF Update
as
BEGIN
   SET NOCOUNT ON;

   DECLARE @Code Nvarchar(30)


   if update(Code)
	select @Code=Code FROM deleted
   else
	select @Code=Code from inserted

   Update CFGApprovalStatusData
      set code = i.code, System = i.System, Active = i.Active
     from inserted i inner join CFGApprovalStatusData on CFGApprovalStatusData.code = @Code

   if (not exists(Select CFGApprovalStatusDescriptions.UICultureName from CFGApprovalStatusDescriptions WHERE CFGApprovalStatusDescriptions.code = @code and CFGApprovalStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
	Insert Into CFGApprovalStatusDescriptions(Code, UICultureName, Description, Caption, Seq)
	Select Code, dbo.FW_GetActiveCultureName(),Description,Caption,isnull(Seq,0) From Inserted 
   else
	Update CFGApprovalStatusDescriptions
	   Set code = i.code, Description = i.Description, Caption = i.Caption, Seq = i.Seq
	  From inserted i
	Inner Join CFGApprovalStatusDescriptions on CFGApprovalStatusDescriptions.code = @code and CFGApprovalStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END

GO
