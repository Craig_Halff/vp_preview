SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CustomTables] AS
SELECT InfoCenterArea,GridID,TableName
FROM FW_CustomGrids
WHERE NOT (InfoCenterArea LIKE 'UDIC_%')
UNION ALL
SELECT 'ChartOfAccounts', 'X', 'AccountCustomTabFields'
UNION ALL
SELECT 'Firms', 'X', 'ClientCustomTabFields'
UNION ALL
SELECT 'Contacts', 'X', 'ContactCustomTabFields'
UNION ALL
SELECT 'Employees', 'X', 'EmployeeCustomTabFields'
UNION ALL
SELECT 'MktCampaigns', 'X', 'MktCampaignCustomTabFields'
UNION ALL
SELECT 'Opportunities', 'X', 'OpportunityCustomTabFields'
UNION ALL
SELECT 'Projects', 'X', 'ProjectCustomTabFields'
UNION ALL
SELECT 'TextLibrary', 'X', 'TextLibraryCustomTabFields'
UNION ALL
SELECT 'ProjectPlan', 'X', 'PlanningCustomTabFields'
UNION ALL
SELECT 'Activity', 'X', 'ActivityCustomTabFields'
UNION ALL
SELECT 'Equipment', 'X', 'EquipmentCustomTabFields'
GO
