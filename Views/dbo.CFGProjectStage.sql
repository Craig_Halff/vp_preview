SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[CFGProjectStage]
as
SELECT dbo.CFGProjectStageData.Code, dbo.CFGProjectStageData.Step, dbo.CFGProjectStageDescriptions.Description, dbo.CFGProjectStageDescriptions.Seq
FROM dbo.CFGProjectStageData Left JOIN
dbo.CFGProjectStageDescriptions ON dbo.CFGProjectStageData.Code = dbo.CFGProjectStageDescriptions.Code 
and dbo.CFGProjectStageDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[DeleteCFGProjectStageTrigger] 
ON [dbo].[CFGProjectStage]
instead of Delete
AS 
BEGIN
SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGProjectStageDescriptions where code = @Code
Delete from CFGProjectStageData where code = @Code
Update CFGVisionSystem SET DefaultOppStage = NULL WHERE DefaultOppStage = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGProjectStageTrigger] 
ON [dbo].[CFGProjectStage]
instead of Insert
as
BEGIN
SET NOCOUNT ON;

DECLARE @Seq as smallint
select @Seq=isnull(Seq,0) from inserted

INSERT INTO [CFGProjectStageData]
([Code]
,Step)
Select [Code], isnull(Step,'I') from inserted

INSERT INTO [CFGProjectStageDescriptions]
([Code] 
,Description
,UICultureName
,Seq)
Select [Code],Description, dbo.FW_GetActiveCultureName(),@Seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
Create TRIGGER [dbo].[UpdateCFGProjectStageTrigger] 
ON [dbo].[CFGProjectStage]
instead of Update
as
BEGIN
SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
select @Code=Code from deleted
else
select @Code=Code from inserted

Update CFGProjectStageData
set Code = i.Code,
Step = i.Step
from inserted i
inner join CFGProjectStageData on CFGProjectStageData.code = @Code

if (not exists(Select CFGProjectStageDescriptions.UICultureName from CFGProjectStageDescriptions
where CFGProjectStageDescriptions.Code = @Code and CFGProjectStageDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
Insert Into CFGProjectStageDescriptions
([Code]
,[UICultureName]
,[Description]
,[Seq])
Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
Update CFGProjectStageDescriptions
Set Code = i.Code
, [Description] = i.Description
, Seq = i.Seq
From inserted i
Inner Join CFGProjectStageDescriptions on CFGProjectStageDescriptions.Code = @Code and CFGProjectStageDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 
END

GO
