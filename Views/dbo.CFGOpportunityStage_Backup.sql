SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGOpportunityStage_Backup]
as
SELECT dbo.CFGOpportunityStageData.Code,  dbo.CFGOpportunityStageData.Closed, dbo.CFGOpportunityStageDescriptions.Description, dbo.CFGOpportunityStageDescriptions.Seq
FROM         dbo.CFGOpportunityStageData Left JOIN
dbo.CFGOpportunityStageDescriptions ON dbo.CFGOpportunityStageData.Code = dbo.CFGOpportunityStageDescriptions.Code 
and dbo.CFGOpportunityStageDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGOpportunityStageTrigger] 
   ON  [dbo].[CFGOpportunityStage_Backup]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGOpportunityStageDescriptions where code = @Code
Delete from CFGOpportunityStageData where code = @Code

END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGOpportunityStageTrigger] 
   ON  [dbo].[CFGOpportunityStage_Backup]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Seq as smallint
select @Seq=isnull(Seq,0) from inserted

INSERT INTO [CFGOpportunityStageData]
  ([Code]
  ,Closed)
Select [Code], isnull(Closed,'N') from inserted

INSERT INTO [CFGOpportunityStageDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],Description, dbo.FW_GetActiveCultureName(),@Seq from inserted

End

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGOpportunityStageTrigger] 
   ON  [dbo].[CFGOpportunityStage_Backup]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGOpportunityStageData
  set Code = i.Code,
      Closed = i.Closed
from inserted i
  inner join CFGOpportunityStageData on CFGOpportunityStageData.code = @Code

if (not exists(Select CFGOpportunityStageDescriptions.UICultureName from CFGOpportunityStageDescriptions
  where CFGOpportunityStageDescriptions.Code = @Code and CFGOpportunityStageDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGOpportunityStageDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGOpportunityStageDescriptions
      Set Code = i.Code
      , [Description] = i.Description
      , Seq = i.Seq
  From inserted i
  Inner Join CFGOpportunityStageDescriptions on CFGOpportunityStageDescriptions.Code = @Code and CFGOpportunityStageDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END

GO
