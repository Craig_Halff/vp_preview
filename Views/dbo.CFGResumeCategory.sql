SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGResumeCategory]
as
SELECT dbo.CFGResumeCategoryData.Code, dbo.CFGResumeCategoryDescriptions.Description, dbo.CFGResumeCategoryDescriptions.Seq
FROM         dbo.CFGResumeCategoryData Left JOIN
dbo.CFGResumeCategoryDescriptions ON dbo.CFGResumeCategoryData.Code = dbo.CFGResumeCategoryDescriptions.Code 
and dbo.CFGResumeCategoryDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGResumeCategoryTrigger] 
   ON  [dbo].[CFGResumeCategory]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGResumeCategoryDescriptions where code = @Code
Delete from CFGResumeCategoryData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGResumeCategoryTrigger] 
   ON  [dbo].[CFGResumeCategory]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGResumeCategoryData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGResumeCategoryDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGResumeCategoryTrigger] 
   ON  [dbo].[CFGResumeCategory]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGResumeCategoryData
  set code = i.code
from inserted i
  inner join CFGResumeCategoryData on CFGResumeCategoryData.code = @Code

if (not exists(Select CFGResumeCategoryDescriptions.UICultureName from CFGResumeCategoryDescriptions
  where CFGResumeCategoryDescriptions.code = @code and CFGResumeCategoryDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGResumeCategoryDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGResumeCategoryDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGResumeCategoryDescriptions on CFGResumeCategoryDescriptions.code = @code and CFGResumeCategoryDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
