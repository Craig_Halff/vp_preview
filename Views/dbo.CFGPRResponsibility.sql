SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGPRResponsibility]
as
SELECT dbo.CFGPRResponsibilityData.Code, dbo.CFGPRResponsibilityDescriptions.Description, dbo.CFGPRResponsibilityDescriptions.Seq
FROM         dbo.CFGPRResponsibilityData Left JOIN
dbo.CFGPRResponsibilityDescriptions ON dbo.CFGPRResponsibilityData.Code = dbo.CFGPRResponsibilityDescriptions.Code 
and dbo.CFGPRResponsibilityDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGPRResponsibilityTrigger] 
   ON  [dbo].[CFGPRResponsibility]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(20)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGPRResponsibilityDescriptions where code = @Code
Delete from CFGPRResponsibilityData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGPRResponsibilityTrigger] 
   ON  [dbo].[CFGPRResponsibility]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGPRResponsibilityData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGPRResponsibilityDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGPRResponsibilityTrigger] 
   ON  [dbo].[CFGPRResponsibility]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(20)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGPRResponsibilityData
  set code = i.code
from inserted i
  inner join CFGPRResponsibilityData on CFGPRResponsibilityData.code = @Code

if (not exists(Select CFGPRResponsibilityDescriptions.UICultureName from CFGPRResponsibilityDescriptions
  where CFGPRResponsibilityDescriptions.code = @code and CFGPRResponsibilityDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGPRResponsibilityDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGPRResponsibilityDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGPRResponsibilityDescriptions on CFGPRResponsibilityDescriptions.code = @code and CFGPRResponsibilityDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
