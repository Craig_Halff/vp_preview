SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGCounty]
as
SELECT dbo.CFGCountyData.Code, dbo.CFGCountyDescriptions.Description, dbo.CFGCountyDescriptions.Seq
FROM         dbo.CFGCountyData Left JOIN
dbo.CFGCountyDescriptions ON dbo.CFGCountyData.Code = dbo.CFGCountyDescriptions.Code 
and dbo.CFGCountyDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGCountyTrigger] 
   ON  [dbo].[CFGCounty]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGCountyDescriptions where code = @Code
Delete from CFGCountyData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGCountyTrigger] 
   ON  [dbo].[CFGCounty]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGCountyData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGCountyDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGCountyTrigger] 
   ON  [dbo].[CFGCounty]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGCountyData
  set code = i.code
from inserted i
  inner join CFGCountyData on CFGCountyData.code = @Code

if (not exists(Select CFGCountyDescriptions.UICultureName from CFGCountyDescriptions
  where CFGCountyDescriptions.code = @code and CFGCountyDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGCountyDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGCountyDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGCountyDescriptions on CFGCountyDescriptions.code = @code and CFGCountyDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
