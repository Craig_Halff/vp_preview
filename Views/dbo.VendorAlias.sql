SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE View  [dbo].[VendorAlias] as
Select
	ClientAlias.PKey,
	Clendor.Vendor,
	ClientAlias.Alias,
	ClientAlias.CreateUser,
	ClientAlias.CreateDate,
	ClientAlias.ModUser,
	ClientAlias.ModDate
	from ClientAlias
	inner join Clendor on ClientAlias.ClientID = Clendor.ClientID
	Where Clendor.Vendor is not null and Clendor.VendorInd = 'Y'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteVendorAliasTrigger] 
   ON  [dbo].[VendorAlias]
instead of Delete
AS 
BEGIN
	Set nocount on;
    Delete from [ClientAlias] where  exists (select 'x' from deleted left join Clendor on deleted.vendor = Clendor.vendor
	where deleted.Alias = [ClientAlias].Alias)
END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertVendorAliasTrigger] 
   ON  [dbo].[VendorAlias]
instead of Insert
as
BEGIN
	set nocount on;
	INSERT INTO [ClientAlias]
	   (ClientID,
		Alias,
		CreateUser,
		CreateDate,
		ModUser,
		ModDate)
	SELECT 
		Clendor.ClientID,
		i.Alias,
		i.CreateUser,
		i.CreateDate,
		i.ModUser,
		i.ModDate
	FROM inserted i inner  join Clendor on i.Vendor = Clendor.vendor

end
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateVendorAliasTrigger] 
   ON  [dbo].[VendorAlias]
instead of Update
as
BEGIN
SET NOCOUNT ON;
if Not (Update(Vendor) or Update(Alias))
	begin
		UPDATE [ClientAlias] SET
			ClientID = Clendor.ClientID,
			Alias = i.Alias,
			CreateUser = i.CreateUser,
			CreateDate = i.CreateDate,
			ModUser = i.ModUser,
			ModDate = i.ModDate
		FROM inserted i
			inner join Clendor on Clendor.vendor = i.vendor 
			where [ClientAlias].Alias = i.Alias and 
			[ClientAlias].clientid = Clendor.ClientID
	end
else
  BEGIN
  		UPDATE [ClientAlias] SET
			ClientID = ClendorNew.ClientID,
			Alias = i.Alias,
			CreateUser = i.CreateUser,
			CreateDate = i.CreateDate,
			ModUser = i.ModUser,
			ModDate = i.ModDate
		FROM inserted i
			inner join deleted d on i.PKey = d.PKey
			inner join Clendor ClendorOld on ClendorOld.vendor = d.vendor 
			left join Clendor ClendorNew on ClendorNew.vendor = i.vendor 
		where [ClientAlias].Alias = d.Alias and 
			[ClientAlias].clientid = ClendorOld.ClientID
  END
END

GO
