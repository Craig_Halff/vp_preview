SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FW_CustomNavtree]
AS
  SELECT data.*,
         labels.Label, labels.UICultureName
  FROM   dbo.FW_CustomNavtreeData data
         LEFT OUTER JOIN dbo.FW_CustomNavtreeLabels labels
          ON labels.ID = data.ID AND labels.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DelFW_CustomNavtree] 
  ON  [dbo].[FW_CustomNavtree]
INSTEAD OF DELETE
AS 
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
     DELETE FROM FW_CustomNavtreeData   WHERE ID IN ( SELECT ID FROM Deleted) 
     DELETE FROM FW_CustomNavtreeLabels WHERE ID IN ( SELECT ID FROM Deleted) 
     SET NOCOUNT OFF  --- restore
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[InsFW_CustomNavtree]
  ON  [dbo].[FW_CustomNavtree]
INSTEAD OF INSERT
AS 
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;

     INSERT INTO FW_CustomNavtreeData 
               ( ID, Seq, NavLevel, ActionType, Page, Args, ParentNode, PreviousSibling, CustomNode, FileName, Folder, Users, OptionOpen, ReportFavoriteName, RebuildReport, HelpPage, Icon )
          SELECT ID, Seq, NavLevel, ActionType, Page, Args, ParentNode, PreviousSibling, CustomNode, FileName, Folder, Users, OptionOpen, ReportFavoriteName, RebuildReport, HelpPage, Icon FROM Inserted

     INSERT INTO FW_CustomNavtreeLabels
               ( ID, Label, UICultureName) 
          SELECT ID, Label, dbo.FW_GetActiveCultureName() FROM Inserted

     SET NOCOUNT OFF  --- restore
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[UpdFW_CustomNavtree] 
  ON  [dbo].[FW_CustomNavtree]
INSTEAD OF UPDATE
AS 
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;

     --- Currently the user is allowed to change the primary key (ID). triggers actually could be processed 
     ---ON more than one record at a time. However, there's no way to correlate the records FROM Inserted
     --- AND Deleted IF the PK was changed. So, prevent updating multiple records at once AND rely ON the
     --- framework sends updates up one at a time.
     DECLARE @cnt AS Int
     SELECT @cnt = COUNT(*) FROM Inserted
     IF @cnt > 1 
         RAISERROR ('UpdFW_CustomNavtree executed on multiple rows.', -- message text.
                    16, -- Severity.
                    1 -- State.
                    );
     
     DECLARE @oldId AS nvarchar(255)
     DECLARE @newId AS nvarchar(255)
     SELECT @oldId = ID FROM Deleted
     SELECT @newId = ID FROM Inserted

     --- Use old pk value for UPDATE
     UPDATE FW_CustomNavtreeData 
        SET ID = i.ID, 
            Seq = isnull(i.Seq,0), 
            NavLevel = i.NavLevel, 
            ActionType = i.ActionType, 
            Page = i.Page, 
            Args = i.Args, 
            ParentNode = i.ParentNode, 
            PreviousSibling = i.PreviousSibling, 
            CustomNode = i.CustomNode, 
            FileName = i.FileName, 
            Folder = i.Folder, 
            Users = i.Users, 
            OptionOpen = i.OptionOpen, 
            ReportFavoriteName = i.ReportFavoriteName, 
            RebuildReport = i.RebuildReport, 
            HelpPage = i.HelpPage,
            Icon = i.Icon
       FROM Inserted i WHERE FW_CustomNavtreeData.ID = @oldId  --- is only 1 record, no JOIN... INNER JOIN FW_CustomNavtreeData ON FW_CustomNavtreeData.ID = @oldId 

     -- Disallow change to UICultureName
     DECLARE @UIC AS VARCHAR(10)
     SELECT @UIC = UICultureName FROM Deleted

     -- Possible that FW_CustomNavtreeLabels record does not exist since 1:n for locales
     IF (@UIC IS NULL) 
          INSERT INTO FW_CustomNavtreeLabels
                    ( ID, Label, UICultureName) 
               SELECT ID, Label, dbo.FW_GetActiveCultureName() FROM Inserted
     ELSE
          UPDATE FW_CustomNavtreeLabels
             SET ID = i.ID, 
                 Label = i.Label
            FROM Inserted i WHERE FW_CustomNavtreeLabels.ID = @oldId AND FW_CustomNavtreeLabels.UICultureName = @UIC --- is only 1 record, no JOIN...ON FW_CustomNavtreeData.ID = @oldId 

     -- UPDATE other cultures in case PK changed (yes, user can currently change PK)
     IF (@oldId <> @newId) 
        UPDATE FW_CustomNavtreeLabels SET ID = @newId WHERE ID = @oldId AND FW_CustomNavtreeLabels.UICultureName <> @UIC 

     SET NOCOUNT OFF  --- restore
END
GO
