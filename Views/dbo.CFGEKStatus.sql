SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGEKStatus]
as
SELECT dbo.CFGEKStatusData.Code, dbo.CFGEKStatusDescriptions.Description
FROM         dbo.CFGEKStatusData Left JOIN
dbo.CFGEKStatusDescriptions ON dbo.CFGEKStatusData.Code = dbo.CFGEKStatusDescriptions.Code 
and dbo.CFGEKStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGEKStatusTrigger] 
   ON  [dbo].[CFGEKStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGEKStatusDescriptions where code = @Code
Delete from CFGEKStatusData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGEKStatusTrigger] 
   ON  [dbo].[CFGEKStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGEKStatusData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGEKStatusDescriptions]
  ([Code] 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGEKStatusTrigger] 
   ON  [dbo].[CFGEKStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGEKStatusData
  set code = i.code
from inserted i
  inner join CFGEKStatusData on CFGEKStatusData.code = @Code

if (not exists(Select CFGEKStatusDescriptions.UICultureName from CFGEKStatusDescriptions
  where CFGEKStatusDescriptions.code = @code and CFGEKStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGEKStatusDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGEKStatusDescriptions
      Set code = i.code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGEKStatusDescriptions on CFGEKStatusDescriptions.code = @code and CFGEKStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
