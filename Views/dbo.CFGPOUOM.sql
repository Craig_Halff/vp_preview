SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGPOUOM]
as
SELECT dbo.CFGPOUOMData.UOM, dbo.CFGPOUOMDescriptions.Description 
FROM         dbo.CFGPOUOMData Left JOIN
dbo.CFGPOUOMDescriptions ON dbo.CFGPOUOMData.UOM = dbo.CFGPOUOMDescriptions.UOM 
and dbo.CFGPOUOMDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGPOUOMTrigger] 
   ON  [dbo].[CFGPOUOM]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @UOM Nvarchar(20)

SELECT top 1 @UOM = UOM FROM Deleted

Delete from CFGPOUOMDescriptions where UOM = @UOM
Delete from CFGPOUOMData where UOM = @UOM

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGPOUOMTrigger] 
   ON  [dbo].[CFGPOUOM]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGPOUOMData]
  ([UOM])
Select [UOM] from inserted

INSERT INTO [CFGPOUOMDescriptions]
  ([UOM] 
  ,Description
  ,UICultureName)
Select [UOM],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGPOUOMTrigger] 
   ON  [dbo].[CFGPOUOM]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @UOM Nvarchar(20)

if update(UOM)
  select @UOM=UOM from deleted
else
  select @UOM=UOM from inserted

Update CFGPOUOMData
  set UOM = i.UOM
from inserted i
  inner join CFGPOUOMData on CFGPOUOMData.UOM = @UOM

if (not exists(Select CFGPOUOMDescriptions.UICultureName from CFGPOUOMDescriptions
  where CFGPOUOMDescriptions.UOM = @UOM and CFGPOUOMDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGPOUOMDescriptions
      ([UOM]
      ,[UICultureName]
      ,[Description])
  Select [UOM], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGPOUOMDescriptions
      Set UOM = i.UOM
      , [Description] = i.Description
  From inserted i
  Inner Join CFGPOUOMDescriptions on CFGPOUOMDescriptions.UOM = @UOM and CFGPOUOMDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
