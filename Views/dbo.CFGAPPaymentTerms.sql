SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGAPPaymentTerms]
as
SELECT dbo.CFGAPPaymentTermsData.Code, dbo.CFGAPPaymentTermsDescriptions.Description 
FROM         dbo.CFGAPPaymentTermsData Left JOIN
dbo.CFGAPPaymentTermsDescriptions ON dbo.CFGAPPaymentTermsData.Code = dbo.CFGAPPaymentTermsDescriptions.Code 
and dbo.CFGAPPaymentTermsDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGAPPaymentTermsTrigger] 
   ON  [dbo].[CFGAPPaymentTerms]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @code VARCHAR(10)

SELECT top 1 @code = Code FROM Deleted

Delete from CFGAPPaymentTermsDescriptions where Code = @code
Delete from CFGAPPaymentTermsData where Code = @code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGAPPaymentTermsTrigger] 
   ON  [dbo].[CFGAPPaymentTerms]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGAPPaymentTermsData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGAPPaymentTermsDescriptions]
  ([Code] 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGAPPaymentTermsTrigger] 
   ON  [dbo].[CFGAPPaymentTerms]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @code VARCHAR(10)

if update(Code)
  select @code=Code from deleted
else
  select @code=Code from inserted

Update CFGAPPaymentTermsData
  set code = i.Code
from inserted i
  inner join CFGAPPaymentTermsData on CFGAPPaymentTermsData.Code = @code

if (not exists(Select CFGAPPaymentTermsDescriptions.UICultureName from CFGAPPaymentTermsDescriptions
  where CFGAPPaymentTermsDescriptions.Code = @code and CFGAPPaymentTermsDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGAPPaymentTermsDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGAPPaymentTermsDescriptions
      Set Code = i.Code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGAPPaymentTermsDescriptions on CFGAPPaymentTermsDescriptions.Code = @code and CFGAPPaymentTermsDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
