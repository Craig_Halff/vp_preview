SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGLeadStatusReason]
as
SELECT dbo.CFGLeadStatusReasonData.Code, dbo.CFGLeadStatusReasonDescriptions.Description, dbo.CFGLeadStatusReasonDescriptions.Seq 
FROM         dbo.CFGLeadStatusReasonData Left JOIN
dbo.CFGLeadStatusReasonDescriptions ON dbo.CFGLeadStatusReasonData.Code = dbo.CFGLeadStatusReasonDescriptions.Code 
and dbo.CFGLeadStatusReasonDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGLeadStatusReasonTrigger] 
   ON  [dbo].[CFGLeadStatusReason]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code NVARCHAR(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGLeadStatusReasonDescriptions where Code = @Code
Delete from CFGLeadStatusReasonData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGLeadStatusReasonTrigger] 
   ON  [dbo].[CFGLeadStatusReason]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGLeadStatusReasonData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGLeadStatusReasonDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),isnull(Seq,0) from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGLeadStatusReasonTrigger] 
   ON  [dbo].[CFGLeadStatusReason]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code NVARCHAR(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGLeadStatusReasonData
  set Code = i.Code
from inserted i
  inner join CFGLeadStatusReasonData on CFGLeadStatusReasonData.Code = @Code

if (not exists(Select CFGLeadStatusReasonDescriptions.UICultureName from CFGLeadStatusReasonDescriptions
  where CFGLeadStatusReasonDescriptions.Code = @Code and CFGLeadStatusReasonDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGLeadStatusReasonDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description], isnull([Seq],0) From Inserted 

Else
  Update CFGLeadStatusReasonDescriptions
      Set Code = i.Code
      , [Description] = i.Description
      , [Seq] = i.Seq
  From inserted i
  Inner Join CFGLeadStatusReasonDescriptions on CFGLeadStatusReasonDescriptions.Code = @Code and CFGLeadStatusReasonDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
