SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGCubeTranslation]
as
SELECT dbo.CFGCubeTranslationData.Usage, dbo.CFGCubeTranslationData.Code, dbo.CFGCubeTranslationDescriptions.Description 
  FROM dbo.CFGCubeTranslationData Left JOIN 
       dbo.CFGCubeTranslationDescriptions ON dbo.CFGCubeTranslationData.Usage = dbo.CFGCubeTranslationDescriptions.Usage AND dbo.CFGCubeTranslationData.Code = dbo.CFGCubeTranslationDescriptions.Code 
   and dbo.CFGCubeTranslationDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
