SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FW_CFGUserSettings]
AS
SELECT  dbo.FW_CFGUserSettingsData.InfocenterArea, 
	dbo.FW_CFGUserSettingsData.TabID, 
	dbo.FW_CFGUserSettingsData.GridID, 
	dbo.FW_CFGUserSettingsData.ElementID, 
        dbo.FW_CFGUserSettingsData.ElementType, 
	dbo.FW_CFGUserSettingsData.Locked, 
	dbo.FW_CFGUserSettingsData.Hidden, 
	dbo.FW_CFGUserSettingsData.RecordLimit,
        dbo.FW_CFGUserSettingsData.DesignerCreated, 
	dbo.FW_CFGUserSettingsData.Required,
	dbo.FW_CFGUserSettingsData.ButtonType,
	dbo.FW_CFGUserSettingsData.GenericPropValue,
	dbo.FW_CFGUserSettingsData.CreateUser,
	dbo.FW_CFGUserSettingsData.CreateDate,
	dbo.FW_CFGUserSettingsData.ModUser,
	dbo.FW_CFGUserSettingsData.ModDate,
	dbo.FW_CFGUserSettingsLabels.TopPos, 
        dbo.FW_CFGUserSettingsLabels.LeftPos, 
	dbo.FW_CFGUserSettingsLabels.Height, 
	dbo.FW_CFGUserSettingsLabels.Width, 
	dbo.FW_CFGUserSettingsLabels.Caption1, 
        dbo.FW_CFGUserSettingsLabels.Caption2, 
	dbo.FW_CFGUserSettingsLabels.HelpText, 
	dbo.FW_CFGUserSettingsLabels.Caption3, 
        dbo.FW_CFGUserSettingsLabels.HorizontalSpacing, 
	dbo.FW_CFGUserSettingsLabels.VerticalSpacing, 
	dbo.FW_CFGUserSettingsLabels.FirstButtonLeft, 
        dbo.FW_CFGUserSettingsLabels.FirstButtonTop, 
	dbo.FW_CFGUserSettingsLabels.LeftAlignText,
	dbo.FW_CFGUserSettingsData.ColumnName,
	dbo.FW_CFGUserSettingsData.TableName
FROM    dbo.FW_CFGUserSettingsData LEFT OUTER JOIN dbo.FW_CFGUserSettingsLabels ON 
	dbo.FW_CFGUserSettingsData.InfocenterArea = dbo.FW_CFGUserSettingsLabels.InfocenterArea AND 
        dbo.FW_CFGUserSettingsData.TabID = dbo.FW_CFGUserSettingsLabels.TabID AND 
	dbo.FW_CFGUserSettingsData.GridID = dbo.FW_CFGUserSettingsLabels.GridID AND 
        dbo.FW_CFGUserSettingsData.ElementID = dbo.FW_CFGUserSettingsLabels.ElementID AND 
	dbo.FW_CFGUserSettingsLabels.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DelFW_CFGUserSettings] 
  ON  [dbo].[FW_CFGUserSettings]
INSTEAD OF DELETE
AS 
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
DELETE FROM FW_CFGUserSettingsLabels FROM deleted
 WHERE FW_CFGUserSettingsLabels.InfocenterArea = deleted.InfocenterArea 
   AND FW_CFGUserSettingsLabels.TabID = deleted.TabID 
   AND FW_CFGUserSettingsLabels.GridID = deleted.GridID 
   AND FW_CFGUserSettingsLabels.ElementID = deleted.ElementID
      
DELETE FROM FW_CFGUserSettingsData FROM deleted
 WHERE FW_CFGUserSettingsData.InfocenterArea = deleted.InfocenterArea 
   AND FW_CFGUserSettingsData.TabID = deleted.TabID 
   AND FW_CFGUserSettingsData.GridID = deleted.GridID 
   AND FW_CFGUserSettingsData.ElementID = deleted.ElementID
 
END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[InsFW_CFGUserSettings] 
  ON  [dbo].[FW_CFGUserSettings]
INSTEAD OF Insert
AS
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;


DECLARE @NewTabID nvarchar(100)
DECLARE @ElementID AS VARCHAR(100)
DECLARE @ElementType AS VARCHAR(10)
DECLARE @GridID nvarchar(250)
DECLARE @InfocenterArea varchar(32)
DECLARE @TabIDChangedSP nvarchar(550)
DECLARE @UserName nvarchar(50)
DECLARE @ColumnName varchar(125)
DECLARE @TableName varchar(125)


SELECT @GridID = GridID FROM Inserted
SELECT @NewTabID = TabID FROM inserted
SELECT @ElementID =[ElementID] FROM inserted
SELECT @ElementType = LOWER(ElementType) FROM inserted
SELECT @InfocenterArea =lower(InfocenterArea) from inserted
SELECT @ColumnName = ColumnName FROM inserted
SELECT @TableName =TableName FROM inserted

set @UserName = dbo.FW_GetUserName()

 
INSERT INTO [FW_CFGUserSettingsData]
           ([InfocenterArea]
           ,[TabID]
           ,[GridID]
           ,[ElementID]
           ,[ElementType]
           ,[Locked]
           ,[Required]
           ,[Hidden]
           ,[DesignerCreated]
           ,[RecordLimit]
           ,[ButtonType]
           ,[GenericPropValue]
	   ,[ColumnName]
	   ,[TableName]
	   ,[CreateUser]
	   ,[CreateDate]
	   ,[ModUser]
	   ,[ModDate])
           SELECT InfocenterArea, [TabID], [GridID], [ElementID], [ElementType],[Locked],[Required],[Hidden],isnull([DesignerCreated],'N'), 
		  [RecordLimit],[ButtonType],[GenericPropValue],[ColumnName],[TableName], @UserName ,(getutcdate()), @UserName , (getutcdate()) FROM inserted

  INSERT INTO  [FW_CFGUserSettingsLabels]
           ([InfocenterArea]
           ,[TabID]
           ,[GridID]
           ,[ElementID]
           ,[UICultureName]
           ,[TopPos]
           ,[LeftPos]
           ,[Height]
           ,[Width]
           ,[Caption1]
           ,[Caption2]
           ,[Caption3]
           ,[HelpText]
	   ,[VerticalSpacing]
	   ,[HorizontalSpacing]
	   ,[FirstButtonLeft]
	   ,[FirstButtonTop]
	   ,[LeftAlignText])
           SELECT InfocenterArea, [TabID], [GridID], [ElementID], DBO.FW_GetActiveCultureName(), [TopPos],[LeftPos],[Height],[Width],[Caption1],[Caption2],[Caption3],[HelpText],[VerticalSpacing],[HorizontalSpacing],[FirstButtonLeft],[FirstButtonTop],[LeftAlignText] FROM Inserted
 
	-- Skip for non-tabbed app.
	IF (@NewTabID != 'X')
	Begin
 		-- Special handling for other tabs on Activity dialog. Their tab Names don't end in 'Tab'. And they need to be lower case to match their keys.
 		-- Special handling for "Detail" tab on Activity dialog. It's tab key is different than the tab Name.
 		-- Special handling for tabRevenue and tabTimeExp on Projects IC since they don't follow the standard naming covention.
		if (@NewTabID = 'tabRevenue')
			SELECT @NewTabID = 'revenue'
		Else
		Begin
			if (@NewTabID = 'tabTimeExp')
				SELECT @NewTabID = 'timeExpense'
			Else
			Begin
				if (@NewTabID = 'Detail' AND @InfoCenterArea = 'activity')
					SELECT @NewTabID = 'general'
				Else
				Begin
					if (@NewTabID = 'tabAddInfo' AND @InfoCenterArea = 'Vendors')
						SELECT @NewTabID = 'additional'
					Else
					Begin
						if (@NewTabID = 'pageGeneral' AND @InfoCenterArea = 'ProjectPlan')
							SELECT @NewTabID = 'general'
						Else
						Begin
							if (LOWER(RIGHT(@NewTabID, 3)) = 'tab')
								SELECT @NewTabID = LEFT(@NewTabID, LEN(@NewTabID) - 3)
							Else
								SELECT @NewTabID = LOWER(@NewTabID)
						End
					End
				End
			End
		End

		-- UPDATE CustomColumns AND CustomGrids AS well so to keep them in sync.
		IF @ElementType = 'grid'
		Begin
           		if SUBSTRING(@ElementID, 1, 7) = 'gridDiv'
				begin
					update [FW_CustomGridsData]
			  		   set TabID = @NewTabID
			  		  from inserted i
			 		 inner join FW_CustomGridsData on FW_CustomGridsData.infocenterArea = i.infocenterarea and FW_CustomGridsData.gridId = RIGHT(@ElementID, LEN(@ElementID) - 7)
   				 	  and FW_CustomGridsData.tabId <> @NewTabID
		  		end
		End
		ELSE
			begin
				/* IF @GridID ='X'
					SELECT @GridID ='[FORM]' */
					
				UPDATE [FW_CustomColumnsData]
				   SET TabID = @NewTabID
				   FROM inserted i
				   INNER JOIN FW_CustomColumnsData ON FW_CustomColumnsData.InfocenterArea = i.InfocenterArea AND FW_CustomColumnsData.Gridid = @GridID  AND FW_CustomColumnsData.Name = @ElementId
						and FW_CustomColumnsData.tabId <> @NewTabID
			end
			
			if EXISTS (select 'x' from sys.objects where name='Product_TabIDChanged' and type='P')
			begin
 				set @TabIDChangedSP=' Product_TabIDChanged ''' + @NewTabID +''','''+ @ElementID+''','''+@ElementType+''','''+@GridID+''','''+@InfocenterArea+''''
 				exec (@TabIDChangedSP)
			end
	End
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[UpdFW_CFGUserSettings] 
  ON  [dbo].[FW_CFGUserSettings]
INSTEAD OF UPDATE
AS 
BEGIN
	-- SET NOCOUNTON added to prevent extra result sets FROM
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


DECLARE @GridID nvarchar(250)
DECLARE @TabID nvarchar(250)
DECLARE @NewTabID nvarchar(100)
DECLARE @ElementID AS VARCHAR(100)
DECLARE @ElementType AS VARCHAR(10)
DECLARE @TabIDChanged AS BIT
DECLARE @InfocenterArea varchar(32)
DECLARE @TabIDChangedSP nvarchar(550)
DECLARE @UserName nvarchar(50)
DECLARE @ColumnName varchar(125)
DECLARE @TableName varchar(125)

SELECT @GridID =GridID FROM inserted

set @UserName = dbo.FW_GetUserName()



IF (UPDATE(TabID ))
	begin
		SELECT @TabID  =TabID  FROM deleted
		SELECT @NewTabID = TabID FROM inserted
		SELECT @TabIDChanged = 1
	end
ELSE
	SELECT @TabID  =TabID  FROM inserted


IF (UPDATE([ElementID]))
	SELECT @ElementID =[ElementID] FROM deleted
ELSE
	SELECT @ElementID =[ElementID] FROM inserted
    -- Insert statements for trigger here

SELECT @ElementType = LOWER(ElementType) FROM inserted
SELECT @InfocenterArea=LOWER(InfocenterArea) from inserted

UPDATE  FW_CFGUserSettingsData
   SET [TabID] =  i.TabID
      ,[GridID] = i.GridID 
      ,[ElementID] = i.ElementID
      ,[ElementType] = i.ElementType 
      ,[Locked] = i.Locked 
      ,[Required] = i.Required 
      ,[Hidden] = i.Hidden 
      ,[DesignerCreated] = i.DesignerCreated 
      ,[RecordLimit] = i.RecordLimit
      ,[ButtonType] = i.ButtonType
      ,[GenericPropValue] = i.GenericPropValue
      ,[ColumnName] = i.ColumnName
      ,[TableName] = i.TableName
      ,[CreateUser] = i.CreateUser
      ,[CreateDate] = i.CreateDate
      ,[ModUser] = @UserName
      ,[ModDate] = getutcdate()
  FROM inserted i
    INNER JOIN FW_CFGUserSettingsData ON FW_CFGUserSettingsData.InfocenterArea = i.InfocenterArea 
      AND FW_CFGUserSettingsData.GridID = @GridID 
      AND FW_CFGUserSettingsData.ElementID =@ElementID

IF (not exists (SELECT FW_CFGUserSettingsLabels.UICultureName FROM FW_CFGUserSettingsLabels, Inserted I
           WHERE  FW_CFGUserSettingsLabels.InfocenterArea = i.InfocenterArea 
				AND FW_CFGUserSettingsLabels.TabID = @TabID
				AND FW_CFGUserSettingsLabels.GridID = @GridID
				and FW_CFGUserSettingsLabels.ElementID = @ElementID
				and FW_CFGUserSettingsLabels.UICultureName =dbo.FW_GetActiveCultureName()))

	INSERT INTO  [FW_CFGUserSettingsLabels]
           ([InfocenterArea]
           ,[TabID]
           ,[GridID]
           ,[ElementID]
           ,[UICultureName]
           ,[TopPos]
           ,[LeftPos]
           ,[Height]
           ,[Width]
           ,[Caption1]
           ,[Caption2]
           ,[Caption3]
           ,[HelpText]
	   ,[VerticalSpacing]
	   ,[HorizontalSpacing]
	   ,[FirstButtonLeft]
	   ,[FirstButtonTop]
       ,[LeftAlignText])
           SELECT InfocenterArea, [TabID], [GridID], [ElementID], DBO.FW_GetActiveCultureName(), [TopPos],[LeftPos],[Height],[Width],[Caption1],[Caption2],[Caption3],[HelpText],[VerticalSpacing],[HorizontalSpacing],[FirstButtonLeft],[FirstButtonTop],[LeftAlignText] FROM Inserted

ELSE
     UPDATE [FW_CFGUserSettingsLabels]
           SET  TabID = i.TabID, 
		GridID = i.GridID,
		ElementID = i.ElementID,
		TopPos = i.TopPos, 
		LeftPos= i.LeftPos, 
		Height = i.Height, 
		Width  = i.Width, 
		Caption1 = i.Caption1, 
		Caption2 = i.Caption2, 
		Caption3 = i.Caption3, 
		HelpText  = i.HelpText,
		VerticalSpacing = i.VerticalSpacing,
		HorizontalSpacing = i.HorizontalSpacing,
		FirstButtonLeft = i.FirstButtonLeft,
		FirstButtonTop = i.FirstButtonTop,
		LeftAlignText = i.LeftAlignText
           FROM inserted i
           INNER JOIN FW_CFGUserSettingsLabels ON FW_CFGUserSettingsLabels.InfocenterArea = i.InfocenterArea 
		and FW_CFGUserSettingsLabels.TabID = @TabID 
		AND FW_CFGUserSettingsLabels.gridId = @gridId
		and FW_CFGUserSettingsLabels.ElementID = @ElementID
		and FW_CFGUserSettingsLabels.UICultureName =dbo.FW_GetActiveCultureName()


IF (@TabIDChanged = 1)
Begin
	-- IF TabID has changed, then UPDATE Labels table for that element for other UICultures.
	-- Also, SET their top/left positions to the new position.
	UPDATE [FW_CFGUserSettingsLabels]
	   SET	TabID = i.TabID,
		TopPos = i.TopPos, 
		LeftPos= i.LeftPos
	   FROM inserted i
	   INNER JOIN FW_CFGUserSettingsLabels ON FW_CFGUserSettingsLabels.InfocenterArea = i.InfocenterArea 
			and FW_CFGUserSettingsLabels.TabID = @TabID 
			AND FW_CFGUserSettingsLabels.gridId = @gridId
			and FW_CFGUserSettingsLabels.ElementID = @ElementID
			and FW_CFGUserSettingsLabels.UICultureName <> dbo.FW_GetActiveCultureName()

	-- Special handling for other tabs on Activity dialog. Their tab Names don't end in 'Tab'. And they need to be lower case to match their keys.
	-- Special handling for "Detail" tab on Activity dialog. It's tab key is different than the tab Name.
	-- Special handling for tabRevenue and tabTimeExp on Projects IC since they don't follow the standard naming convention.
	if (@NewTabID = 'tabRevenue')
		SELECT @NewTabID = 'revenue'
	Else
	Begin
		if (@NewTabID = 'tabTimeExp')
			SELECT @NewTabID = 'timeExpense'
		Else
		Begin
			if (@NewTabID = 'Detail' AND @InfoCenterArea = 'activity')
				SELECT @NewTabID = 'general'
			Else
			Begin
				if (@NewTabID = 'tabAddInfo' AND @InfoCenterArea = 'Vendors')
					SELECT @NewTabID = 'additional'
				Else
				Begin
					if (@NewTabID = 'pageGeneral' AND @InfoCenterArea = 'ProjectPlan')
						SELECT @NewTabID = 'general'
					Else
					Begin
						if (LOWER(RIGHT(@NewTabID, 3)) = 'tab')
							SELECT @NewTabID = LEFT(@NewTabID, LEN(@NewTabID) - 3)
						Else
							SELECT @NewTabID = LOWER(@NewTabID)
					End
				End
			End
		End
	End


	-- UPDATE CustomColumns AND CustomGrids AS well so to keep them in sync.
	IF @ElementType = 'grid'
	Begin
		if SUBSTRING(@ElementID, 1, 7) = 'gridDiv'
       		 begin
			update [FW_CustomGridsData]
			   set TabID = @NewTabID
			  from inserted i
			 inner join FW_CustomGridsData on FW_CustomGridsData.infocenterArea = i.infocenterarea and FW_CustomGridsData.gridId = RIGHT(@ElementID, LEN(@ElementID) - 7)
		end
	End
	ELSE
		begin
			/*IF @GridID ='X'
				SELECT @GridID ='[FORM]'*/
		
			UPDATE [FW_CustomColumnsData]
			   SET TabID = @NewTabID
			   FROM inserted i
			   INNER JOIN FW_CustomColumnsData ON FW_CustomColumnsData.InfocenterArea = i.InfocenterArea AND FW_CustomColumnsData.Gridid = @GridID AND FW_CustomColumnsData.Name = @ElementId
		end
		
 		if EXISTS (select 'x' from sys.objects where Name='Product_TabIDChanged' and type='P')
		begin
  				set @TabIDChangedSP='Product_TabIDChanged ''' + @NewTabID +''','''+ @ElementID+''','''+@ElementType+''','''+@GridID+''','''+@InfocenterArea+''''
 				exec (@TabIDChangedSP)
		end

End

END
GO
