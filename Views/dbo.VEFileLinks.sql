SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE View  [dbo].[VEFileLinks] as
Select
	ClientFileLinks.LinkID,
	Clendor.Vendor,
	ClientFileLinks.[Description],
	ClientFileLinks.FilePath,
	ClientFileLinks.Graphic,
	ClientFileLinks.CreateUser,
	ClientFileLinks.CreateDate,
	ClientFileLinks.ModUser,
	ClientFileLinks.ModDate
from ClientFileLinks
inner join Clendor on ClientFileLinks.ClientID = Clendor.ClientID
Where Clendor.Vendor is not null and Clendor.VendorInd = 'Y'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteVEFileLinksTrigger] 
   ON  [dbo].[VEFileLinks]
instead of Delete
AS 
BEGIN
	Set nocount on;
    Delete from [ClientFileLinks] where  exists (select 'x' from deleted left join Clendor on deleted.vendor = Clendor.vendor
	where deleted.LinkID = [ClientFileLinks].LinkID)
END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertVEFileLinksTrigger]
   ON  [dbo].[VEFileLinks]
instead of Insert
as
BEGIN
	set nocount on;
	INSERT INTO [ClientFileLinks]
	   (LinkID,
		ClientID,
		[Description],
		FilePath,
		Graphic,
		CreateUser,
		CreateDate,
		ModUser,
		ModDate)
	SELECT 
		i.LinkID,
		Clendor.ClientID,
		i.[Description],
		i.FilePath,
		i.Graphic,
		i.CreateUser,
		i.CreateDate,
		i.ModUser,
		i.ModDate
	FROM inserted i inner  join Clendor on i.Vendor = Clendor.vendor

end
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateVEFileLinksTrigger] 
   ON  [dbo].[VEFileLinks]
instead of Update
as
BEGIN
SET NOCOUNT ON;
if Not (Update(Vendor))
	begin
		UPDATE [ClientFileLinks] SET
			LinkID = I.LinkID,
			[Description] = I.[Description],
			FilePath = I.FilePath,
			Graphic = I.Graphic,
			CreateUser = I.CreateUser,
			CreateDate = I.CreateDate,
			ModUser = I.ModUser,
			ModDate = I.ModDate
		FROM inserted i
			inner join Clendor on Clendor.vendor = i.vendor 
		 where [ClientFileLinks].LinkID = i.LinkID
	end
else
  BEGIN
  		UPDATE [ClientFileLinks] SET
			LinkID = I.LinkID,
			ClientID = ClendorNew.ClientID,
			[Description] = I.[Description],
			FilePath = I.FilePath,
			Graphic = I.Graphic,
			CreateUser = I.CreateUser,
			CreateDate = I.CreateDate,
			ModUser = I.ModUser,
			ModDate = I.ModDate
		FROM inserted i
			inner join deleted d on i.LinkID = d.LinkID
			inner join Clendor ClendorOld on ClendorOld.vendor = d.vendor 
			left join Clendor ClendorNew on ClendorNew.vendor = i.vendor 
		where [ClientFileLinks].LinkID = d.LinkID and 
			[ClientFileLinks].clientid = ClendorOld.ClientID
  END
END

GO
