SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[HAI_TeamDetails]
AS
SELECT        ISNULL(A.TeamID, '') AS TeamID, ISNULL(A.Team, '') AS Team, ISNULL(A.TeamNum, '') AS TeamNum, ISNULL(A.TeamName, '') AS TeamName, ISNULL(A.PracticeArea, '') AS PracticeArea, ISNULL(A.TeamLeaderID, '') 
                         AS TeamLeaderID, ISNULL(A.ManagementLeader, '') AS ManagementLeader, ISNULL(A.AdministrativeAssistant, '') AS AdministrativeAssistant, ISNULL(A.ContractAdmin, '') AS ContractAdmin, ISNULL(A.Director, '') AS Director, 
                         A.CustCrewEntryAccess, ISNULL(A.CustPrimaryTeamLocation, '') AS CustPrimaryTeamLocation, ISNULL(A.OfficeID, '') AS OfficeID, ISNULL(C.Label, '') AS Office, CASE (ISNULL(C.Label, '')) 
                         WHEN 'Corporate' THEN 'RCH' WHEN 'Richardson' THEN 'RCH' WHEN 'Fort Worth' THEN 'FTW' WHEN 'Houston' THEN 'HOU' WHEN 'McAllen' THEN 'MAC' WHEN 'Austin' THEN 'AUS' WHEN 'Frisco' THEN 'FRS' WHEN 'San Antonio'
                          THEN 'SAN' WHEN 'Dallas' THEN 'DAL' WHEN 'Conroe' THEN 'CNR' WHEN 'Midland' THEN 'MID' WHEN 'Shreveport' THEN 'SHR' WHEN 'Oklahoma City' THEN 'OKC' WHEN 'Tyler' THEN 'TYL' WHEN 'Arkansas' THEN 'ARK' ELSE
                          '' END AS OfficeCode, ISNULL(B.FirstName, '') + ' ' + ISNULL(B.LastName, '') AS Leader, ISNULL(B.FirstName, '') AS LeaderFName, ISNULL(B.LastName, '') AS LeaderLName, ISNULL(A.TeamLabel, '') AS TeamLabel, 
                         ISNULL(D.DataValue, '') AS PracticeAreaDesc, A.Company
FROM            (SELECT        SUBSTRING(alpha.CustOrganization, 1, 2) AS Company, alpha.CustOrganization AS TeamID, SUBSTRING(alpha.CustOrganization, 1, 3) + '%' + SUBSTRING(alpha.CustOrganization, 6, 4) AS Team, 
                                                    SUBSTRING(alpha.CustOrganization, 7, 3) AS TeamNum, alpha.CustOrganizationCode AS TeamName, alpha.CustTeamPracticeArea AS PracticeArea, alpha.CustTeamLeader AS TeamLeaderID, 
                                                    alpha.CustManagementLeader AS ManagementLeader, alpha.CustAdministrativeAssistant AS AdministrativeAssistant, alpha.CustContractAdmin AS ContractAdmin, alpha.CustDirector AS Director, 
                                                    alpha.CustCrewEntryAccess, alpha.CustPrimaryTeamLocation, SUBSTRING(alpha.CustOrganization, 4, 2) AS OfficeID, gamma.Label AS TeamLabel
                          FROM            dbo.UDIC_OrganizationStructure AS alpha INNER JOIN
                                                    dbo.CFGOrgCodesData AS beta ON beta.Code = SUBSTRING(alpha.CustOrganization, 7, 3) AND beta.OrgLevel = '3' INNER JOIN
                                                    dbo.CFGOrgCodesDescriptions AS gamma ON gamma.Code = SUBSTRING(alpha.CustOrganization, 7, 3) AND gamma.OrgLevel = '3'
                          WHERE        (beta.Status = 'A') AND (alpha.CustPrimaryTeamLocation = 'Y')) AS A LEFT OUTER JOIN
                             (SELECT        Employee, LastName, FirstName
                               FROM            dbo.EMMain) AS B ON B.Employee = A.TeamLeaderID LEFT OUTER JOIN
                             (SELECT        Code, Label
                               FROM            dbo.CFGOrgCodesDescriptions
                               WHERE        (OrgLevel = '2')) AS C ON C.Code = A.OfficeID LEFT OUTER JOIN
                             (SELECT        Code, DataValue
                               FROM            dbo.FW_CustomColumnValues
                               WHERE        (ColName = 'CustTeamPracticeArea') AND (InfocenterArea = 'UDIC_OrganizationStructure')) AS D ON D.Code = A.PracticeArea
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[22] 4[22] 2[40] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "B"
            Begin Extent = 
               Top = 6
               Left = 306
               Bottom = 119
               Right = 476
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "C"
            Begin Extent = 
               Top = 6
               Left = 514
               Bottom = 102
               Right = 684
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "D"
            Begin Extent = 
               Top = 6
               Left = 722
               Bottom = 102
               Right = 892
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "A"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 268
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'HAI_TeamDetails', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'HAI_TeamDetails', NULL, NULL
GO
