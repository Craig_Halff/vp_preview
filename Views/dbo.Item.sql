SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[Item]
as
SELECT dbo.ItemData.Item, 
dbo.ItemData.ExtraNotes, 
dbo.ItemData.NSN, 
dbo.ItemData.InternalNotes, 
dbo.ItemData.Manufacturer,
dbo.ItemDescriptions.Description,
dbo.ItemData.LinkUrl  
  FROM dbo.ItemData Left JOIN dbo.ItemDescriptions ON dbo.ItemData.Item = dbo.ItemDescriptions.Item and dbo.ItemDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteItemTrigger] 
   ON  [dbo].[Item]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Item Nvarchar(30)

SELECT top 1 @Item = Item FROM Deleted

Delete from ItemDescriptions where Item = @Item
Delete from ItemData where Item = @Item

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertItemTrigger] 
   ON  [dbo].[Item]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [ItemData]
  (Item, ExtraNotes, NSN, InternalNotes, Manufacturer, LinkUrl)
Select Item, ExtraNotes, NSN, InternalNotes, Manufacturer, LinkUrl from inserted

INSERT INTO [ItemDescriptions]
  ([Item] 
  ,Description
  ,UICultureName)
Select [Item],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateItemTrigger] 
   ON  [dbo].[Item]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Item Nvarchar(30)

if update(Item)
  select @Item=Item from deleted
else
  select @Item=Item from inserted

Update ItemData
  set Item = i.Item, ExtraNotes = i.ExtraNotes, NSN = i.NSN, InternalNotes = i.InternalNotes, Manufacturer = i.Manufacturer,LinkUrl = i.LinkUrl
from inserted i
  inner join ItemData on ItemData.Item = @Item

if (not exists(Select ItemDescriptions.UICultureName from ItemDescriptions
  where ItemDescriptions.Item = @Item and ItemDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into ItemDescriptions
      ([Item]
      ,[UICultureName]
      ,[Description])
  Select [Item], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update ItemDescriptions
      Set Item = i.Item
      , [Description] = i.Description
  From inserted i
  Inner Join ItemDescriptions on ItemDescriptions.Item = @Item and ItemDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
