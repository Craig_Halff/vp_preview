SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGDescriptionCategory]
as
SELECT dbo.CFGDescriptionCategoryData.Code, dbo.CFGDescriptionCategoryDescriptions.Description, dbo.CFGDescriptionCategoryDescriptions.Seq
FROM         dbo.CFGDescriptionCategoryData Left JOIN
dbo.CFGDescriptionCategoryDescriptions ON dbo.CFGDescriptionCategoryData.Code = dbo.CFGDescriptionCategoryDescriptions.Code 
and dbo.CFGDescriptionCategoryDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGDescriptionCategoryTrigger] 
   ON  [dbo].[CFGDescriptionCategory]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGDescriptionCategoryDescriptions where code = @Code
Delete from CFGDescriptionCategoryData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGDescriptionCategoryTrigger] 
   ON  [dbo].[CFGDescriptionCategory]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGDescriptionCategoryData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGDescriptionCategoryDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGDescriptionCategoryTrigger] 
   ON  [dbo].[CFGDescriptionCategory]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGDescriptionCategoryData
  set code = i.code
from inserted i
  inner join CFGDescriptionCategoryData on CFGDescriptionCategoryData.code = @Code

if (not exists(Select CFGDescriptionCategoryDescriptions.UICultureName from CFGDescriptionCategoryDescriptions
  where CFGDescriptionCategoryDescriptions.code = @code and CFGDescriptionCategoryDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGDescriptionCategoryDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGDescriptionCategoryDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGDescriptionCategoryDescriptions on CFGDescriptionCategoryDescriptions.code = @code and CFGDescriptionCategoryDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
