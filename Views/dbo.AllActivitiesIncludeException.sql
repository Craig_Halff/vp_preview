SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create View [dbo].[AllActivitiesIncludeException]
As
Select
	AllActivities.ActivityID, ClientID, ContactID, WBS1, WBS2, WBS3, Employee, LeadID, CampaignID, Type, Subject, 
	StartDate, EndDate , StartTime, EndTime, Duration, Location, ReminderInd, 
	ReminderUnit, ReminderMinHrDay, ReminderDate, ReminderTime, Priority, Notes, 
	ShowTimeAs, AllDayEventInd, CompletionInd, RecurrenceInd, PrivateInd, TaskStatus, 
	TaskCompletionDate, OpportunityID, CampaignCode, RecurrType, RecurrDailyFreq, 
	RecurrDailyWeekDay, RecurrWeeklyFreq, RecurrWeeklySun, RecurrWeeklyMon, 
	RecurrWeeklyTue, RecurrWeeklyWed, RecurrWeeklyThu, RecurrWeeklyFri, RecurrWeeklySat, 
	RecurrMonthlyFreq, RecurrMonthlyDay, RecurrMonthlyOccurInd, RecurrMonthlyOccur, 
	RecurrMonthlyOccurDay, RecurrMonthlyOccurFreq, RecurrYearlyMonth, RecurrYearlyDay,
	RecurrEndType, RP, EmailAlertSent,RecurrID, Vendor, ContactIDForVendor, CreateUser, CreateDate,
	ModUser, ModDate, MaxOccurences, OccurrenceDate,ExceptionType,RecurrStartDate , RecurrEndDate,IsNull(timezoneoffset,0) timezoneoffset,
	AssignmentID,ConversationID,RecurrYearlyOccurInd,RecurrYearlyOccur,RecurrYearlyOccurDay,RecurrYearlyOccurMonth ,
	PIMInd,KonaTask
From
AllActivities 
--LEFT Join 
--(
--	Select ActivityID ,
--	Min(AllActivities.StartDate) As RecurrStartDate ,
--	Max(CASE When AllActivities.RecurrEndType <> 'N' Then AllActivities.EndDate ELSE NULL END) As RecurrEndDate
--	From AllActivities 
--	Group By ActivityID	
--) ActivityGroup 
--On AllActivities.ActivityID =ActivityGroup.ActivityID

Union 
Select
	 Activity.ActivityID, ClientID, ContactID, WBS1, WBS2, WBS3, Employee, LeadID, CampaignID, Type, Subject, 
	RecurrActivityException.StartDate,
	dateadd(second,datediff(SECOND,Activity.StartDate,EndDate), RecurrActivityException.StartDate) As EndDate, 
	StartTime, EndTime, Duration, Location, ReminderInd, 
	ReminderUnit, ReminderMinHrDay, ReminderDate, ReminderTime, Priority, Notes, 
	ShowTimeAs, AllDayEventInd, CompletionInd, RecurrenceInd, PrivateInd, TaskStatus, 
	TaskCompletionDate, OpportunityID, CampaignCode, RecurrType, RecurrDailyFreq, 
	RecurrDailyWeekDay, RecurrWeeklyFreq, RecurrWeeklySun, RecurrWeeklyMon, 
	RecurrWeeklyTue, RecurrWeeklyWed, RecurrWeeklyThu, RecurrWeeklyFri, RecurrWeeklySat, 
	RecurrMonthlyFreq, RecurrMonthlyDay, RecurrMonthlyOccurInd, RecurrMonthlyOccur, 
	RecurrMonthlyOccurDay, RecurrMonthlyOccurFreq, RecurrYearlyMonth, RecurrYearlyDay, 
	RecurrEndType, RP, EmailAlertSent, 
	RecurrActivityException.ActivityID AS  RecurrID, Vendor, ContactIDForVendor, RecurrActivityException.CreateUser, RecurrActivityException.CreateDate,
	RecurrActivityException.ModUser, RecurrActivityException.ModDate,
	MaxOccurences, RecurrActivityException.StartDate As OccurrenceDate, RecurrActivityException.TranType  As ExceptionType,
	RecurrStartDate, RecurrEndDate,IsNull(Activity.Timezoneoffset,0) Timezoneoffset,
	AssignmentID,ConversationID,RecurrYearlyOccurInd,RecurrYearlyOccur,RecurrYearlyOccurDay,RecurrYearlyOccurMonth ,
	PIMInd,KonaTask

From Activity
Inner Join RecurrActivityException 
ON  RecurrActivityException.ActivityID=Activity.ActivityID 
Where 
RecurrActivityException.TranType='D'

GO
