SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGVendorStatus]
AS
SELECT dbo.CFGClientStatusData.STATUS
	,dbo.CFGClientStatusDescriptions.Label
FROM dbo.CFGClientStatusData
LEFT JOIN dbo.CFGClientStatusDescriptions ON dbo.CFGClientStatusData.STATUS = dbo.CFGClientStatusDescriptions.STATUS
	AND dbo.CFGClientStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGVendorStatusTrigger] 
   ON  [dbo].[CFGVendorStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Status VARCHAR(1)

SELECT top 1 @Status = Status FROM Deleted

Delete from CFGClientStatusDescriptions where Status = @Status
Delete from CFGClientStatusData where Status = @Status

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGVendorStatusTrigger] 
   ON  [dbo].[CFGVendorStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGClientStatusData]
  (Status)
Select Status from inserted

INSERT INTO [CFGClientStatusDescriptions]
  ([Status] 
  ,Label
  ,UICultureName)
Select [Status],[Label], dbo.FW_GetActiveCultureName() from inserted
End
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGVendorStatusTrigger] 
   ON  [dbo].[CFGVendorStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Status VARCHAR(1)

if update(Status)
  select @Status=Status from deleted
else
  select @Status=Status from inserted

Update CFGClientStatusData
  set Status = i.Status
from inserted i
  inner join CFGClientStatusData on CFGClientStatusData.Status = @Status

if (not exists(Select CFGClientStatusDescriptions.UICultureName from CFGClientStatusDescriptions
  where CFGClientStatusDescriptions.Status = @Status and CFGClientStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGClientStatusDescriptions
      ([Status]
      ,[UICultureName]
      ,[Label])
  Select [Status], dbo.FW_GetActiveCultureName(),[Label] From Inserted 

Else
  Update CFGClientStatusDescriptions
      Set Status = i.Status
      , [Label] = i.Label
  From inserted i
  Inner Join CFGClientStatusDescriptions on CFGClientStatusDescriptions.Status = @Status and CFGClientStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
