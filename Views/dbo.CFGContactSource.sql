SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGContactSource]
as
SELECT dbo.CFGContactSourceData.Code, dbo.CFGContactSourceDescriptions.ContactSource, dbo.CFGContactSourceDescriptions.Seq
FROM         dbo.CFGContactSourceData Left JOIN
dbo.CFGContactSourceDescriptions ON dbo.CFGContactSourceData.Code = dbo.CFGContactSourceDescriptions.Code 
and dbo.CFGContactSourceDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGContactSourceTrigger] 
   ON  [dbo].[CFGContactSource]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGContactSourceDescriptions where code = @Code
Delete from CFGContactSourceData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGContactSourceTrigger] 
   ON  [dbo].[CFGContactSource]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGContactSourceData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGContactSourceDescriptions]
  ([Code] 
  ,ContactSource
  ,UICultureName
  ,Seq)
Select [Code],[ContactSource], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGContactSourceTrigger] 
   ON  [dbo].[CFGContactSource]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGContactSourceData
  set code = i.code
from inserted i
  inner join CFGContactSourceData on CFGContactSourceData.code = @Code

if (not exists(Select CFGContactSourceDescriptions.UICultureName from CFGContactSourceDescriptions
  where CFGContactSourceDescriptions.code = @code and CFGContactSourceDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGContactSourceDescriptions
      ([Code]
      ,[UICultureName]
      ,[ContactSource]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[ContactSource],[Seq] From Inserted 

Else
  Update CFGContactSourceDescriptions
      Set code = i.code
      , [ContactSource] = i.ContactSource
      , seq = i.seq
  From inserted i
  Inner Join CFGContactSourceDescriptions on CFGContactSourceDescriptions.code = @code and CFGContactSourceDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
