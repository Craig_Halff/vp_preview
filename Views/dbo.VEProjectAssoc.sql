SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[VEProjectAssoc]
AS
SELECT [PKEY]
	,[Clendor].[Vendor] AS [Vendor]
	,[ClendorProjectAssoc].[WBS1]
	,[ClendorProjectAssoc].[WBS2]
	,[ClendorProjectAssoc].[WBS3]
	,[ClendorProjectAssoc].[ROLE]
	,[ClendorProjectAssoc].[RoleDescription]
	,[ClendorProjectAssoc].[TeamStatus]
	,[ClendorProjectAssoc].[Address] AS [VEAddress]
    ,[ClendorProjectAssoc].[VendorInd]
	,[ClendorProjectAssoc].[CreateUser]
	,[ClendorProjectAssoc].[CreateDate]
	,[ClendorProjectAssoc].[ModUser]
	,[ClendorProjectAssoc].[ModDate]
FROM [ClendorProjectAssoc]
INNER JOIN [Clendor] ON [ClendorProjectAssoc].[ClientID] = [Clendor].[Clientid]
	AND [Clendor].[Vendor] IS NOT NULL
	AND [Clendor].[VendorInd] = 'Y'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteVEProjectAssocTrigger] 
   ON  [dbo].[VEProjectAssoc]
instead of Delete
AS 
BEGIN
    SET NOCOUNT ON;

DELETE FROM [ClendorProjectAssoc]
WHERE EXISTS (
		SELECT 'x'
		FROM deleted
		LEFT JOIN [Clendor] ON deleted.vendor = [Clendor].[vendor]
			 AND [Clendor].[VendorInd] = 'Y'
		WHERE deleted.wbs1 = ClendorProjectAssoc.wbs1
			AND deleted.wbs2 = ClendorProjectAssoc.wbs2
			AND deleted.wbs3 = ClendorProjectAssoc.wbs3
			AND [Clendor].[ClientID] = [ClendorProjectAssoc].[ClientID]
		)


END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertVEProjectAssocTrigger] 
   ON  [dbo].[VEProjectAssoc]
instead of Insert
as
BEGIN
    SET NOCOUNT ON;
	INSERT INTO ClendorProjectAssoc
	   ([clientid]
	   ,[WBS1]
      ,[WBS2]
      ,[WBS3]
      ,[Role]
      ,[RoleDescription]
      ,[TeamStatus]
      ,[Address]
      ,[VendorInd]
      ,[CreateUser]
      ,[CreateDate]
      ,[ModUser]
      ,[ModDate]
      )

	SELECT 
	  Clendor.clientID
      ,inserted.WBS1
      ,inserted.WBS2
      ,inserted.WBS3
      ,inserted.Role
      ,inserted.RoleDescription
      ,inserted.TeamStatus
      ,inserted.VEAddress
      ,'Y'
      ,inserted.CreateUser
	  ,isnull(inserted.CreateDate,getutcdate())
      ,inserted.ModUser
	  ,isnull(inserted.ModDate,getutcdate())
	FROM inserted inner join [Clendor] on inserted.Vendor = [Clendor].[Vendor] and [Clendor].[Vendorind] = 'Y'

end
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateVEProjectAssocTrigger] 
   ON  [dbo].[VEProjectAssoc]
instead of Update
as
BEGIN
SET NOCOUNT ON;
if Not Update(Vendor) or Update(wbs1) or update(wbs2) or update(wbs3)
	begin
		UPDATE [ClendorProjectAssoc] SET
			[Role] = i.[Role]
			,[RoleDescription] = i.[RoleDescription]
			,[Address] = i.[VEAddress]
			,[TeamStatus] = i.[TeamStatus]
			,[VendorInd] = i.[VendorInd]
			,CreateUser = i.CreateUser
			,CreateDate = i.CreateDate
			,ModUser = i.ModUser
			,ModDate = i.ModDate
		FROM inserted i
			inner join Clendor on Clendor.vendor = i.vendor 
			where [ClendorProjectAssoc].wbs1 = i.wbs1 and 
			[ClendorProjectAssoc].wbs2 = i.wbs2 and 
			[ClendorProjectAssoc].wbs3 = i.wbs3 and 
			[ClendorProjectAssoc].clientid = Clendor.ClientID
	end
else
  BEGIN
	UPDATE [ClendorProjectAssoc]
	SET  [wbs1] = i.[wbs1]
		,[wbs2] = i.[wbs2]
		,[wbs3] = i.[wbs3]
		,[ClientID] = ClendorNew.[ClientID]
		,[Role] = i.[Role]
		,[Address] = i.[VEAddress]
		,[RoleDescription] = i.[RoleDescription]
		,[VendorInd] = i.[VendorInd]
		,[CreateUser] = i.[CreateUser]
		,[CreateDate] = i.[CreateDate]
		,[ModUser] = i.[ModUser]
		,[ModDate] = i.[ModDate]
	FROM inserted i
	INNER JOIN deleted d ON i.[Pkey] = d.[PKey]
	INNER JOIN Clendor ClendorOld ON ClendorOld.vendor = d.vendor
	LEFT JOIN Clendor ClendorNew ON ClendorNew.vendor = i.vendor
	WHERE [ClendorProjectAssoc].wbs1 = d.wbs1
		AND [ClendorProjectAssoc].wbs2 = d.wbs2
		AND [ClendorProjectAssoc].wbs3 = d.wbs3
		AND [ClendorProjectAssoc].clientid = ClendorOld.ClientID

  END
END
GO
