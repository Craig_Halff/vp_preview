SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGCashHeadings]
as
SELECT dbo.CFGCashHeadingsData.ReportColumn, dbo.CFGCashHeadingsData.Subtotal, dbo.CFGCashHeadingsDescriptions.Heading, dbo.CFGCashHeadingsDescriptions.SubTotalLabel
FROM   dbo.CFGCashHeadingsData
 Left JOIN dbo.CFGCashHeadingsDescriptions ON dbo.CFGCashHeadingsData.ReportColumn =  dbo.CFGCashHeadingsDescriptions.ReportColumn
 and dbo.CFGCashHeadingsDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGCashHeadingsTrigger] 
   ON  [dbo].[CFGCashHeadings]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @ReportColumn smallint


SELECT top 1 @ReportColumn =  ReportColumn  FROM Deleted

Delete from CFGCashHeadingsDescriptions where CFGCashHeadingsDescriptions.ReportColumn =  @ReportColumn 
Delete from CFGCashHeadingsData where CFGCashHeadingsData.ReportColumn =  @ReportColumn 

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGCashHeadingsTrigger] 
   ON  [dbo].[CFGCashHeadings]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGCashHeadingsData]
  (ReportColumn, Subtotal)
Select isnull(ReportColumn,0), Subtotal from inserted

INSERT INTO [CFGCashHeadingsDescriptions]
  (ReportColumn, Heading, SubTotalLabel,UICultureName)
Select isnull(ReportColumn,0), Heading, SubTotalLabel, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGCashHeadingsTrigger] 
   ON  [dbo].[CFGCashHeadings]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @ReportColumn smallint

if update(ReportColumn)
  select @ReportColumn=ReportColumn from deleted
else
  select @ReportColumn=ReportColumn from inserted

Update CFGCashHeadingsData
  set ReportColumn =  i.ReportColumn, Subtotal=i.Subtotal
from inserted i
  inner join CFGCashHeadingsData on CFGCashHeadingsData.ReportColumn =  @ReportColumn

if (not exists(Select CFGCashHeadingsDescriptions.UICultureName from CFGCashHeadingsDescriptions, Inserted I
  where  CFGCashHeadingsDescriptions.ReportColumn =  @ReportColumn and CFGCashHeadingsDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGCashHeadingsDescriptions
      (ReportColumn,
       UICultureName,
	   Heading,
       SubTotalLabel)
  Select isnull(ReportColumn,0), dbo.FW_GetActiveCultureName(), Heading, SubTotalLabel From Inserted 

Else
  Update CFGCashHeadingsDescriptions
      Set ReportColumn =  i.ReportColumn,
          SubTotalLabel = i.SubTotalLabel,
		  Heading = i.Heading
  From inserted i
  Inner Join CFGCashHeadingsDescriptions on CFGCashHeadingsDescriptions.ReportColumn =  @ReportColumn and CFGCashHeadingsDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
