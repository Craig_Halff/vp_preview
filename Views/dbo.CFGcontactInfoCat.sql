SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGcontactInfoCat]
as
SELECT dbo.CFGcontactInfoCatData.Code, dbo.CFGcontactInfoCatDescriptions.Category, dbo.CFGcontactInfoCatDescriptions.Seq
FROM         dbo.CFGcontactInfoCatData Left JOIN
dbo.CFGcontactInfoCatDescriptions ON dbo.CFGcontactInfoCatData.Code = dbo.CFGcontactInfoCatDescriptions.Code 
and dbo.CFGcontactInfoCatDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGcontactInfoCatTrigger] 
   ON  [dbo].[CFGcontactInfoCat]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(50)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGcontactInfoCatDescriptions where code = @Code
Delete from CFGcontactInfoCatData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGcontactInfoCatTrigger] 
   ON  [dbo].[CFGcontactInfoCat]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGcontactInfoCatData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGcontactInfoCatDescriptions]
  ([Code] 
  ,Category
  ,UICultureName
  ,Seq)
Select [Code],[Category], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGcontactInfoCatTrigger] 
   ON  [dbo].[CFGcontactInfoCat]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(50)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGcontactInfoCatData
  set code = i.code
from inserted i
  inner join CFGcontactInfoCatData on CFGcontactInfoCatData.code = @Code

if (not exists(Select CFGcontactInfoCatDescriptions.UICultureName from CFGcontactInfoCatDescriptions
  where CFGcontactInfoCatDescriptions.code = @code and CFGcontactInfoCatDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGcontactInfoCatDescriptions
      ([Code]
      ,[UICultureName]
      ,[Category]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Category],[Seq] From Inserted 

Else
  Update CFGcontactInfoCatDescriptions
      Set code = i.code
      , [Category] = i.Category
      , seq = i.seq
  From inserted i
  Inner Join CFGcontactInfoCatDescriptions on CFGcontactInfoCatDescriptions.code = @code and CFGcontactInfoCatDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
