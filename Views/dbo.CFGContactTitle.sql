SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGContactTitle]
as
SELECT dbo.CFGContactTitleData.Code, dbo.CFGContactTitleDescriptions.Title, dbo.CFGContactTitleDescriptions.Seq
FROM         dbo.CFGContactTitleData Left JOIN
dbo.CFGContactTitleDescriptions ON dbo.CFGContactTitleData.Code = dbo.CFGContactTitleDescriptions.Code 
and dbo.CFGContactTitleDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGContactTitleTrigger] 
   ON  [dbo].[CFGContactTitle]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGContactTitleDescriptions where code = @Code
Delete from CFGContactTitleData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGContactTitleTrigger] 
   ON  [dbo].[CFGContactTitle]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGContactTitleData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGContactTitleDescriptions]
  ([Code] 
  ,Title
  ,UICultureName
  ,Seq)
Select [Code],[Title], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGContactTitleTrigger] 
   ON  [dbo].[CFGContactTitle]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGContactTitleData
  set code = i.code
from inserted i
  inner join CFGContactTitleData on CFGContactTitleData.code = @Code

if (not exists(Select CFGContactTitleDescriptions.UICultureName from CFGContactTitleDescriptions
  where CFGContactTitleDescriptions.code = @code and CFGContactTitleDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGContactTitleDescriptions
      ([Code]
      ,[UICultureName]
      ,[Title]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Title],[Seq] From Inserted 

Else
  Update CFGContactTitleDescriptions
      Set code = i.code
      , [Title] = i.Title
      , seq = i.seq
  From inserted i
  Inner Join CFGContactTitleDescriptions on CFGContactTitleDescriptions.code = @code and CFGContactTitleDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
