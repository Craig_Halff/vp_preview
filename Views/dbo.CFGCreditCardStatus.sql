SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGCreditCardStatus]
as
SELECT dbo.CFGCreditCardStatusData.Code, dbo.CFGCreditCardStatusDescriptions.Description 
FROM         dbo.CFGCreditCardStatusData Left JOIN
dbo.CFGCreditCardStatusDescriptions ON dbo.CFGCreditCardStatusData.Code = dbo.CFGCreditCardStatusDescriptions.Code 
and dbo.CFGCreditCardStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGCreditCardStatusTrigger] 
   ON  [dbo].[CFGCreditCardStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGCreditCardStatusDescriptions where Code = @Code
Delete from CFGCreditCardStatusData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGCreditCardStatusTrigger] 
   ON  [dbo].[CFGCreditCardStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGCreditCardStatusData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGCreditCardStatusDescriptions]
  ([Code] 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGCreditCardStatusTrigger] 
   ON  [dbo].[CFGCreditCardStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGCreditCardStatusData
  set Code = i.Code
from inserted i
  inner join CFGCreditCardStatusData on CFGCreditCardStatusData.Code = @Code

if (not exists(Select CFGCreditCardStatusDescriptions.UICultureName from CFGCreditCardStatusDescriptions
  where CFGCreditCardStatusDescriptions.Code = @Code and CFGCreditCardStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGCreditCardStatusDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGCreditCardStatusDescriptions
      Set Code = i.Code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGCreditCardStatusDescriptions on CFGCreditCardStatusDescriptions.Code = @Code and CFGCreditCardStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
