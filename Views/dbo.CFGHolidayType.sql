SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGHolidayType]
as
SELECT dbo.CFGHolidayTypeData.Code, dbo.CFGHolidayTypeDescriptions.Description 
FROM         dbo.CFGHolidayTypeData Left JOIN
dbo.CFGHolidayTypeDescriptions ON dbo.CFGHolidayTypeData.Code = dbo.CFGHolidayTypeDescriptions.Code 
and dbo.CFGHolidayTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGHolidayTypeTrigger] 
   ON  [dbo].[CFGHolidayType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGHolidayTypeDescriptions where Code = @Code
Delete from CFGHolidayTypeData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGHolidayTypeTrigger] 
   ON  [dbo].[CFGHolidayType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGHolidayTypeData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGHolidayTypeDescriptions]
  ([Code] 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGHolidayTypeTrigger] 
   ON  [dbo].[CFGHolidayType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGHolidayTypeData
  set Code = i.Code
from inserted i
  inner join CFGHolidayTypeData on CFGHolidayTypeData.Code = @Code

if (not exists(Select CFGHolidayTypeDescriptions.UICultureName from CFGHolidayTypeDescriptions
  where CFGHolidayTypeDescriptions.Code = @Code and CFGHolidayTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGHolidayTypeDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGHolidayTypeDescriptions
      Set Code = i.Code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGHolidayTypeDescriptions on CFGHolidayTypeDescriptions.Code = @Code and CFGHolidayTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
