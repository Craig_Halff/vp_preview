SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGBankImportDetail]
as
SELECT	dbo.CFGBankImportDetailData.Code, dbo.CFGBankImportDetailData.FieldName, dbo.CFGBankImportDetailData.Include, dbo.CFGBankImportDetailData.Seq, dbo.CFGBankImportDetailDescriptions.FieldLabel 
  FROM	dbo.CFGBankImportDetailData Left JOIN
	dbo.CFGBankImportDetailDescriptions ON dbo.CFGBankImportDetailData.Code = dbo.CFGBankImportDetailDescriptions.Code 
   and	dbo.CFGBankImportDetailData.FieldName = dbo.CFGBankImportDetailDescriptions.FieldName 
   and	dbo.CFGBankImportDetailDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGBankImportDetailTrigger] 
   ON  [dbo].[CFGBankImportDetail]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGBankImportDetailDescriptions where Code = @Code
Delete from CFGBankImportDetailData where Code = @Code

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGBankImportDetailTrigger] 
   ON  [dbo].[CFGBankImportDetail]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO CFGBankImportDetailData
  (Code,FieldName,Include,Seq)   
Select Code,FieldName,Include,Seq from inserted

INSERT INTO CFGBankImportDetailDescriptions
  (Code,FieldName,UICultureName,FieldLabel)
Select Code,FieldName,dbo.FW_GetActiveCultureName(),FieldLabel from inserted

End

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGBankImportDetailTrigger] 
   ON  [dbo].[CFGBankImportDetail]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGBankImportDetailData
  set Code = i.Code,
      FieldName = i.FieldName,
      Include = i.Include,
      Seq = i.Seq
from inserted i
  inner join CFGBankImportDetailData 
     on CFGBankImportDetailData.Code = @Code 
    and CFGBankImportDetailData.FieldName = i.FieldName
if (not exists(Select CFGBankImportDetailDescriptions.UICultureName from CFGBankImportDetailDescriptions, Inserted i
  where CFGBankImportDetailDescriptions.Code = @Code 
    and CFGBankImportDetailDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
    and CFGBankImportDetailDescriptions.FieldName = i.FieldName))
  Insert Into CFGBankImportDetailDescriptions
      (Code
      ,FieldName
      ,UICultureName
      ,FieldLabel)
  Select Code,FieldName,dbo.FW_GetActiveCultureName(),FieldLabel From Inserted 
Else
  Update CFGBankImportDetailDescriptions
     Set Code = i.Code
       , FieldName = i.FieldName
       , FieldLabel = i.FieldLabel
  From inserted i
  Inner Join CFGBankImportDetailDescriptions
     on CFGBankImportDetailDescriptions.Code = i.Code 
    and CFGBankImportDetailDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
    and CFGBankImportDetailDescriptions.FieldName = i.FieldName 

END
GO
