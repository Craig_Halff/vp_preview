SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[AnalysisCubesKPI]
as
SELECT	dbo.AnalysisCubesKPIData.PKey,
	dbo.AnalysisCubesKPIData.Measure,
	dbo.AnalysisCubesKPIData.MeasureType,
	dbo.AnalysisCubesKPIData.Dimension,
	dbo.AnalysisCubesKPIData.StatusIndicator,
	dbo.AnalysisCubesKPIData.PeriodMethod,
	dbo.AnalysisCubesKPIData.Method,
	dbo.AnalysisCubesKPIData.Goal,
	dbo.AnalysisCubesKPIData.GoalLowValue,
	dbo.AnalysisCubesKPIData.GoalHighValue,
	dbo.AnalysisCubesKPIData.TrendIndicator,
	dbo.AnalysisCubesKPIData.TrendComparison,
	dbo.AnalysisCubesKPIData.TrendLowValue,
	dbo.AnalysisCubesKPIData.TrendHighValue,
	dbo.AnalysisCubesKPIDescriptions.KPILabel
FROM   dbo.AnalysisCubesKPIData Left JOIN
dbo.AnalysisCubesKPIDescriptions ON dbo.AnalysisCubesKPIData.PKey = dbo.AnalysisCubesKPIDescriptions.PKey 
and dbo.AnalysisCubesKPIDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteAnalysisCubesKPITrigger] 
   ON  [dbo].[AnalysisCubesKPI]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @PKey Nvarchar(32)

SELECT top 1 @PKey = PKey FROM Deleted

Delete from AnalysisCubesKPIDescriptions where PKey = @PKey
Delete from AnalysisCubesKPIData where PKey = @PKey

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertAnalysisCubesKPITrigger] 
   ON  [dbo].[AnalysisCubesKPI]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [AnalysisCubesKPIData]
      ( PKey,
      Measure,
      MeasureType,
      Dimension,
      StatusIndicator,
      PeriodMethod,
      Method,
      Goal,
      GoalLowValue,
      GoalHighValue,
      TrendIndicator,
      TrendComparison,
      TrendLowValue,
      TrendHighValue
)
Select  PKey,
      Measure,
      MeasureType,
      Dimension,
      StatusIndicator,
      PeriodMethod,
      Method,
      Goal,
      GoalLowValue,
      GoalHighValue,
      TrendIndicator,
      TrendComparison,
      TrendLowValue,
      TrendHighValue
from inserted

INSERT INTO [AnalysisCubesKPIDescriptions]
  (PKey, 
	UICultureName,
	KPILabel
  )
Select PKey, dbo.FW_GetActiveCultureName(), KPILabel from inserted
End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateAnalysisCubesKPITrigger] 
   ON  [dbo].[AnalysisCubesKPI]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @PKey Nvarchar(32)

if update(PKey)
  select @PKey=PKey from deleted
else
  select @PKey=PKey from inserted

Update AnalysisCubesKPIData
  set 	PKey = i.PKey,
	Measure = i.Measure, 
	MeasureType = i.MeasureType,
	Dimension = i.Dimension,
	StatusIndicator = i.StatusIndicator,
	PeriodMethod = i.PeriodMethod,
	Method = i.Method,
	Goal = i.Goal,
	GoalLowValue = i.GoalLowValue,
	GoalHighValue = i.GoalHighValue,
	TrendIndicator = i.TrendIndicator,
	TrendComparison = i.TrendComparison,
	TrendLowValue = i.TrendLowValue,
	TrendHighValue = i.TrendHighValue
from inserted i
  inner join AnalysisCubesKPIData on AnalysisCubesKPIData.PKey = @PKey

if (not exists(Select AnalysisCubesKPIDescriptions.UICultureName from AnalysisCubesKPIDescriptions
  where AnalysisCubesKPIDescriptions.PKey = @PKey and AnalysisCubesKPIDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into AnalysisCubesKPIDescriptions
      (PKey,
		UICultureName,
		KPILabel
      )
  Select PKey, dbo.FW_GetActiveCultureName(), KPILabel From Inserted 

Else
  Update AnalysisCubesKPIDescriptions
      Set PKey = i.PKey
      , KPILabel = i.KPILabel
  From inserted i
  Inner Join AnalysisCubesKPIDescriptions on AnalysisCubesKPIDescriptions.PKey = @PKey and AnalysisCubesKPIDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
