SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[AllEMActivities] AS
SELECT  distinct allAct.ActivityID, ClientID, ContactID, WBS1, WBS2, WBS3, ema.Employee, LeadID, CampaignID, Type, Subject, 
		allAct.StartDate, EndDate, Duration, Location, 
		IsNull(ema.ReminderInd, allAct.ReminderInd) as ReminderInd, 
		IsNull(ema.ReminderUnit, allAct.ReminderUnit) as ReminderUnit, 
		IsNull(ema.ReminderMinHrDay, allAct.ReminderMinHrDay) as ReminderMinHrDay, 
		Case when emao.ReminderDate is NULL then 
		Case IsNull(ema.ReminderMinHrDay, allAct.ReminderMinHrDay) When 'Minutes' Then
		dateadd(mi, (cast(IsNull(ema.ReminderUnit, allAct.ReminderUnit) As int) * -1), allAct.StartDate) 
		When 'Hours' Then 
		dateadd(hh, (cast(IsNull(ema.ReminderUnit, allAct.ReminderUnit) As int) * -1), allAct.startdate) 
		When 'Days' Then 
		dateadd(d, (cast(IsNull(ema.ReminderUnit, allAct.ReminderUnit) As int) * -1), allAct.startdate) 
		End 
		Else emao.ReminderDate End 
		AS ReminderDate, 
		Priority, Notes, 
		ShowTimeAs, AllDayEventInd, CompletionInd, RecurrenceInd, PrivateInd, TaskStatus, 
		TaskCompletionDate, OpportunityID, CampaignCode, RecurrType, RecurrDailyFreq, 
		RecurrDailyWeekDay, RecurrWeeklyFreq, RecurrWeeklySun, RecurrWeeklyMon, 
		RecurrWeeklyTue, RecurrWeeklyWed, RecurrWeeklyThu, RecurrWeeklyFri, RecurrWeeklySat, 
		RecurrMonthlyFreq, RecurrMonthlyDay, RecurrMonthlyOccurInd, RecurrMonthlyOccur, 
		RecurrMonthlyOccurDay, RecurrMonthlyOccurFreq, RecurrYearlyMonth, RecurrYearlyDay, 
		RecurrStartDate, RecurrEndType, RecurrEndDate, RP, EmailAlertSent, 
		RecurrID, Vendor, ContactIDForVendor, 
		Case when emao.PopupDismissed is NULL Then ema.PopupDismissed
		Else emao.PopupDismissed End
		AS PopupDismissed, 
		allAct.CreateUser, allAct.CreateDate, allAct.ModUser, allAct.ModDate
FROM  EMActivity ema WITH (NOLOCK) 
inner join RecurringActivities allAct on ema.ActivityID = allAct.ActivityID 
left outer join EMActivityOccurrence emao WITH (NOLOCK) on emao.ActivityID = allAct.ActivityID and allAct.StartDate = emao.StartDate and emao.Employee = ema.employee

UNION ALL

SELECT  distinct allAct.ActivityID, ClientID, ContactID, WBS1, WBS2, WBS3, ema.Employee, LeadID, CampaignID, Type, Subject, 
		allAct.StartDate, EndDate, Duration, Location, 
		IsNull(ema.ReminderInd, allAct.ReminderInd) as ReminderInd, 
		IsNull(ema.ReminderUnit, allAct.ReminderUnit) as ReminderUnit, 
		IsNull(ema.ReminderMinHrDay, allAct.ReminderMinHrDay) as ReminderMinHrDay, 
		Case when emao.ReminderDate is NULL then 
		Case IsNull(ema.ReminderMinHrDay, allAct.ReminderMinHrDay) When 'Minutes' Then
		dateadd(mi, (cast(IsNull(ema.ReminderUnit, allAct.ReminderUnit) As int) * -1), allAct.StartDate) 
		When 'Hours' Then 
		dateadd(hh, (cast(IsNull(ema.ReminderUnit, allAct.ReminderUnit) As int) * -1), allAct.startdate) 
		When 'Days' Then 
		dateadd(d, (cast(IsNull(ema.ReminderUnit, allAct.ReminderUnit) As int) * -1), allAct.startdate) 
		End 
		Else emao.ReminderDate End 
		AS ReminderDate, 
		Priority, Notes, 
		ShowTimeAs, AllDayEventInd, CompletionInd, RecurrenceInd, PrivateInd, TaskStatus, 
		TaskCompletionDate, OpportunityID, CampaignCode, RecurrType, RecurrDailyFreq, 
		RecurrDailyWeekDay, RecurrWeeklyFreq, RecurrWeeklySun, RecurrWeeklyMon, 
		RecurrWeeklyTue, RecurrWeeklyWed, RecurrWeeklyThu, RecurrWeeklyFri, RecurrWeeklySat, 
		RecurrMonthlyFreq, RecurrMonthlyDay, RecurrMonthlyOccurInd, RecurrMonthlyOccur, 
		RecurrMonthlyOccurDay, RecurrMonthlyOccurFreq, RecurrYearlyMonth, RecurrYearlyDay, 
		RecurrStartDate, RecurrEndType, RecurrEndDate, RP, EmailAlertSent, 
		RecurrID, Vendor, ContactIDForVendor, 
		Case when emao.PopupDismissed is NULL Then ema.PopupDismissed
		Else emao.PopupDismissed End
		AS PopupDismissed, 
		allAct.CreateUser, allAct.CreateDate, allAct.ModUser, allAct.ModDate
FROM  EMActivity ema WITH (NOLOCK) 
inner join (SELECT * FROM Activity WITH (NOLOCK) 
			WHERE (RecurrenceInd = 'N' and ActivityID NOT in (select ActivityID 
                                                              from RecurrActivityException r 
                                                              where r.ActivityID = Activity.ActivityID 
																and datediff(d,r.StartDate,Activity.StartDate) = 0))
			) allAct on ema.ActivityID = allAct.ActivityID 
left outer join EMActivityOccurrence emao WITH (NOLOCK) on emao.ActivityID = allAct.ActivityID and allAct.StartDate = emao.StartDate and emao.Employee = ema.employee

GO
