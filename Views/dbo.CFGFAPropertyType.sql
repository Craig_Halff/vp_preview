SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGFAPropertyType]
as
SELECT 	dbo.CFGFAPropertyTypeData.Code,
	dbo.CFGFAPropertyTypeData.Company,
	dbo.CFGFAPropertyTypeDescriptions.Description,
	dbo.CFGFAPropertyTypeData.InUse
FROM   dbo.CFGFAPropertyTypeData
 Left JOIN dbo.CFGFAPropertyTypeDescriptions ON dbo.CFGFAPropertyTypeData.Code = dbo.CFGFAPropertyTypeDescriptions.Code
and   dbo.CFGFAPropertyTypeData.Company = dbo.CFGFAPropertyTypeDescriptions.Company
and   dbo.CFGFAPropertyTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGFAPropertyTypeTrigger] 
   ON  [dbo].[CFGFAPropertyType]
instead of Delete
AS 
BEGIN
SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)
DECLARE @Company Nvarchar(14)

SELECT top 1 @Code = Code, @Company = Company FROM Deleted

Delete from CFGFAPropertyTypeDescriptions where CFGFAPropertyTypeDescriptions.code = @Code and 
            CFGFAPropertyTypeDescriptions.Company = @Company
Delete from CFGFAPropertyTypeData where CFGFAPropertyTypeData.code = @Code and CFGFAPropertyTypeData.Company = @Company

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGFAPropertyTypeTrigger] 
   ON  [dbo].[CFGFAPropertyType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGFAPropertyTypeData]
  (	Code,
	Company,
	InUse)
Select 	Code,
	Company,
	isnull(InUse,'N')
 from inserted

INSERT INTO [CFGFAPropertyTypeDescriptions]
  (Code, Company, Description, UICultureName)
Select Code, Company, Description, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGFAPropertyTypeTrigger] 
   ON  [dbo].[CFGFAPropertyType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(14)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

	
Update CFGFAPropertyTypeData
  set 	Code = i.code,
	Company = i.Company,
	InUse= i.InUse
 from inserted i
  inner join CFGFAPropertyTypeData on CFGFAPropertyTypeData.Company = i.Company and CFGFAPropertyTypeData.code = @Code

if (not exists(Select CFGFAPropertyTypeDescriptions.UICultureName from CFGFAPropertyTypeDescriptions, Inserted I
  where CFGFAPropertyTypeDescriptions.Company = i.Company and CFGFAPropertyTypeDescriptions.code = @code and CFGFAPropertyTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGFAPropertyTypeDescriptions
      (Code,
	   Company,
       UICultureName,
       Description)
  Select Code, Company, dbo.FW_GetActiveCultureName(), Description From Inserted 

Else
  Update CFGFAPropertyTypeDescriptions
      Set code = i.code,
          Company = i.Company,
         Description = i.Description
  From inserted i
  Inner Join CFGFAPropertyTypeDescriptions on CFGFAPropertyTypeDescriptions.Company = i.Company and CFGFAPropertyTypeDescriptions.code = @code and CFGFAPropertyTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
