SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FW_UDIC]
AS
SELECT  dbo.FW_UDICData.UDIC_ID, dbo.FW_UDICData.TableName, dbo.FW_UDICData.AuditingEnabled, dbo.FW_UDICData.AutoNumSrc, 
	dbo.FW_UDICData.AutoNumOverride, dbo.FW_UDICData.AutoNumSeqStart, dbo.FW_UDICData.AutoNumSeqLen, dbo.FW_UDICData.AutoNumSeqPos, 
        dbo.FW_UDICData.DisplayInTitle, dbo.FW_UDICLocalized.HelpURL, Singular.LabelValue AS SingularLabel, Plural.LabelValue AS PluralLabel
  FROM	dbo.FW_UDICData 
	INNER JOIN dbo.FW_UDICLocalized ON dbo.FW_UDICLocalized.UDIC_ID = dbo.FW_UDICData.UDIC_ID 
	INNER JOIN dbo.FW_CFGLabelData AS Singular ON Singular.LabelName = dbo.FW_UDICLocalized.UDIC_ID AND Singular.UICultureName = dbo.FW_UDICLocalized.UICultureName 
	INNER JOIN dbo.FW_CFGLabelData AS Plural ON Plural.LabelName = dbo.FW_UDICLocalized.UDIC_ID + '_Plural' AND Plural.UICultureName = dbo.FW_UDICLocalized.UICultureName
 WHERE	(dbo.FW_UDICLocalized.UICultureName = dbo.FW_GetActiveCultureName())
GO
