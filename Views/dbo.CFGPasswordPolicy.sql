SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGPasswordPolicy] AS 
SELECT	CFGSecurity.PKey AS PKey, 
	MinimumPasswordLength, 
	RequireDigit, 
	RequireSpecial, 
	ExpireDays, 
	RepeatDays, 
	InvalidLoginAttempts, 
	ISNULL(FW_CFGSystem.TwoFactorAuth, 'N') AS TwoFactorAuth,
	PreventUsernameAsPassword,
	RepeatCount
	FROM  CFGSecurity INNER JOIN FW_CFGSystem ON CFGSecurity.PKey=FW_CFGSystem.PKey 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[UpdateCFGPasswordPolicyTrigger] 
	ON  [dbo].[CFGPasswordPolicy]
INSTEAD OF UPDATE
AS
BEGIN
     SET NOCOUNT ON;

	IF UPDATE(TwoFactorAuth) 
	BEGIN
              UPDATE FW_CFGSystem SET TwoFactorAuth=Inserted.TwoFactorAuth FROM FW_CFGSystem INNER JOIN Inserted ON Inserted.PKey=FW_CFGSystem.PKey
	END

	UPDATE CFGSecurity SET CFGSecurity.PKey=Inserted.PKey, CFGSecurity.MinimumPasswordLength=Inserted.MinimumPasswordLength, 
	CFGSecurity.RequireDigit=Inserted.RequireDigit, CFGSecurity.RequireSpecial=Inserted.RequireSpecial, CFGSecurity.ExpireDays=Inserted.ExpireDays, 
	CFGSecurity.RepeatDays=Inserted.RepeatDays, CFGSecurity.InvalidLoginAttempts=Inserted.InvalidLoginAttempts, 
	CFGSecurity.PreventUsernameAsPassword=Inserted.PreventUsernameAsPassword, CFGSecurity.RepeatCount=Inserted.RepeatCount 
	FROM CFGSecurity INNER JOIN Inserted ON CFGSecurity.PKey=Inserted.PKey
	END
GO
