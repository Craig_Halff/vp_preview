SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGOrganizationStatus]
as
SELECT dbo.CFGOrganizationStatusData.Status,  dbo.CFGOrganizationStatusDescriptions.Label 
FROM         dbo.CFGOrganizationStatusData Left JOIN
dbo.CFGOrganizationStatusDescriptions ON dbo.CFGOrganizationStatusData.Status = dbo.CFGOrganizationStatusDescriptions.Status 
and dbo.CFGOrganizationStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGOrganizationStatus] 
   ON  [dbo].[CFGOrganizationStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Status varchar(1)

SELECT top 1 @Status = Status from Deleted

Delete from CFGOrganizationStatusDescriptions where Status = @Status
Delete from CFGOrganizationStatusData where Status = @Status 

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGOrganizationStatusTrigger] 
   ON  [dbo].[CFGOrganizationStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGOrganizationStatusData]
  ([Status])
Select [Status] from inserted

INSERT INTO [CFGOrganizationStatusDescriptions]
  ([Status], [UICultureName], [Label])
Select [Status], dbo.FW_GetActiveCultureName(), [Label] from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGOrganizationStatusTrigger] 
   ON  [dbo].[CFGOrganizationStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Status varchar(1)

if update(Status)
  select @Status=Status from deleted
else
  select @Status=Status from inserted

Update CFGOrganizationStatusData
  set Status = i.Status
from inserted i
  inner join CFGOrganizationStatusData on CFGOrganizationStatusData.Status = @Status

if (not exists(Select CFGOrganizationStatusDescriptions.UICultureName from CFGOrganizationStatusDescriptions
  where CFGOrganizationStatusDescriptions.Status = @Status and CFGOrganizationStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGOrganizationStatusDescriptions
      ([Status]
      ,[UICultureName]
      ,[Label])
  Select [Status], dbo.FW_GetActiveCultureName(),[Label] From Inserted 

Else
  Update CFGOrganizationStatusDescriptions
      Set Status = i.Status
      , [Label] = i.Label
  From inserted i
  Inner Join CFGOrganizationStatusDescriptions on CFGOrganizationStatusDescriptions.Status = @Status and CFGOrganizationStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
