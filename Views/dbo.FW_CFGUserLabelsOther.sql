SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FW_CFGUserLabelsOther]
AS
SELECT dbo.FW_CFGUserLabelsOtherData.LabelType, dbo.FW_CFGUserLabelsOtherData.TypeID, dbo.FW_CFGUserLabelsOtherData.LabelID,
       dbo.FW_CFGUserLabelsOtherDesc.NewLabel, dbo.FW_CFGUserLabelsOtherDesc.Heading1, dbo.FW_CFGUserLabelsOtherDesc.Heading2,
       dbo.FW_CFGUserLabelsOtherDesc.HeadingAlt1, dbo.FW_CFGUserLabelsOtherDesc.HeadingAlt2
FROM   dbo.FW_CFGUserLabelsOtherData
 LEFT JOIN dbo.FW_CFGUserLabelsOtherDesc ON dbo.FW_CFGUserLabelsOtherData.LabelType = dbo.FW_CFGUserLabelsOtherDesc.LabelType
and   dbo.FW_CFGUserLabelsOtherData.TypeID = dbo.FW_CFGUserLabelsOtherDesc.TypeID
and   dbo.FW_CFGUserLabelsOtherData.LabelID = dbo.FW_CFGUserLabelsOtherDesc.LabelID
and dbo.FW_CFGUserLabelsOtherDesc.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DelFW_CFGUserLabelsOther] 
  ON  [dbo].[FW_CFGUserLabelsOther]
INSTEAD OF DELETE
AS 
BEGIN
     SET NOCOUNT ON;

-- Lookup and report labels are configured by language, so only process a DELETE of the description
-- for the active language, and remove the data table row, only if no descriptions table rows exist
-- for any language

DELETE FROM FW_CFGUserLabelsOtherDesc FROM deleted
 WHERE FW_CFGUserLabelsOtherDesc.LabelType = deleted.LabelType
   AND FW_CFGUserLabelsOtherDesc.TypeID = deleted.TypeID
   AND FW_CFGUserLabelsOtherDesc.LabelID = deleted.LabelID
   AND FW_CFGUserLabelsOtherDesc.UICultureName = dbo.FW_GetActiveCultureName()

IF NOT EXISTS (SELECT 'x' FROM FW_CFGUserLabelsOtherDesc, deleted
               WHERE FW_CFGUserLabelsOtherDesc.LabelType = deleted.LabelType
                 AND FW_CFGUserLabelsOtherDesc.TypeID = deleted.TypeID
                 AND FW_CFGUserLabelsOtherDesc.LabelID = deleted.LabelID)
    DELETE FROM FW_CFGUserLabelsOtherData FROM deleted
     WHERE FW_CFGUserLabelsOtherData.LabelType = deleted.LabelType
       AND FW_CFGUserLabelsOtherData.TypeID = deleted.TypeID
       AND FW_CFGUserLabelsOtherData.LabelID = deleted.LabelID

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[InsFW_CFGUserLabelsOther] 
  ON  [dbo].[FW_CFGUserLabelsOther]
INSTEAD OF Insert
AS
BEGIN
     SET NOCOUNT ON;

INSERT INTO [FW_CFGUserLabelsOtherData]
  (LabelType, TypeID, LabelID)
SELECT LabelType, TypeID, LabelID FROM inserted

INSERT INTO [FW_CFGUserLabelsOtherDesc]
  (LabelType, TypeID, LabelID, newLabel, Heading1, Heading2, HeadingAlt1, HeadingALt2, UICultureName)
SELECT LabelType, TypeID, LabelID, newLabel, Heading1, Heading2, HeadingAlt1, HeadingALt2, dbo.FW_GetActiveCultureName() FROM inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[UpdFW_CFGUserLabelsOther] 
  ON  [dbo].[FW_CFGUserLabelsOther]
INSTEAD OF UPDATE
AS
BEGIN
     SET NOCOUNT ON;

DECLARE @LabelType nvarchar(50)

IF (UPDATE(LabelType))
  SELECT @LabelType=LabelType FROM deleted
ELSE
  SELECT @LabelType=LabelType FROM inserted

UPDATE FW_CFGUserLabelsOtherData
  SET LabelType = i.LabelType, TypeID=i.TypeID, LabelID = i.LabelID
FROM inserted i
  INNER JOIN FW_CFGUserLabelsOtherData ON FW_CFGUserLabelsOtherData.LabelType = @LabelType
 AND FW_CFGUserLabelsOtherData.TypeID = i.Typeid
 AND FW_CFGUserLabelsOtherData.LabelID = i.LabelID

IF (not exists(SELECT FW_CFGUserLabelsOtherDesc.UICultureName FROM FW_CFGUserLabelsOtherDesc, Inserted I
  WHERE FW_CFGUserLabelsOtherDesc.LabelType = @LabelType
   AND FW_CFGUserLabelsOtherDesc.TypeID = i.TypeID
   AND FW_CFGUserLabelsOtherDesc.LabelID = i.LabelID
   AND FW_CFGUserLabelsOtherDesc.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into FW_CFGUserLabelsOtherDesc
      (LabelType,
       TypeID,
       LabelID,
       NewLabel, 
       Heading1,
       Heading2, 
       HeadingAlt1, 
       HeadingAlt2,
       UICultureName)
  SELECT LabelType,
	 TypeID,
	 LabelID,
	 NewLabel, 
	 Heading1,
	 Heading2,
	 HeadingAlt1, 
	 HeadingAlt2,
	 dbo.FW_GetActiveCultureName()
  FROM Inserted 

ELSE
  UPDATE FW_CFGUserLabelsOtherDesc
      SET LabelType = i.LabelType,
          TypeID = i.TypeID,
          LabelID = i.LabelID,
          NewLabel = i.NewLabel,
          Heading1 = i.Heading1,
          Heading2 = i.Heading2, 
          HeadingAlt1 = i.HeadingAlt1, 
          HeadingAlt2 = i.HeadingAlt2
  FROM inserted i
  INNER JOIN FW_CFGUserLabelsOtherDesc ON FW_CFGUserLabelsOtherDesc.LabeLType = @LabelType
        AND FW_CFGUserLabelsOtherDesc.TypeID = i.TypeID
        AND FW_CFGUserLabelsOtherDesc.LabelID = i.LabelID
        AND FW_CFGUserLabelsOtherDesc.UICultureName = dbo.FW_GetActiveCultureName() 
END
GO
