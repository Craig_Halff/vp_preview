SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGSOCCode]
AS
SELECT
	dbo.CFGSOCCodeData.Code,
	dbo.CFGSOCCodeData.EEOCategoryCode,
	dbo.CFGSOCCodeDescriptions.[Description],
	dbo.CFGSOCCodeDescriptions.Seq,
	dbo.CFGSOCCodeData.Status,
	dbo.CFGSOCCodeData.Predefined
FROM dbo.CFGSOCCodeData
LEFT JOIN dbo.CFGSOCCodeDescriptions ON dbo.CFGSOCCodeDescriptions.Code = dbo.CFGSOCCodeData.Code
	AND dbo.CFGSOCCodeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGSOCCodeTrigger]
	ON  [dbo].[CFGSOCCode]
INSTEAD OF DELETE
AS
BEGIN
	SET NOCOUNT ON;
             
	DECLARE @Code NVARCHAR(10)
 
	SELECT TOP 1 @Code = Code FROM Deleted
 
	DELETE FROM CFGSOCCodeDescriptions WHERE Code = @Code
	DELETE FROM CFGSOCCodeData WHERE Code = @Code
END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[InsertCFGSOCCodeTrigger]
	ON  [dbo].[CFGSOCCode]
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON;
 
	INSERT INTO [CFGSOCCodeData] ([Code], [EEOCategoryCode], [Status], [Predefined])
	SELECT [Code], [EEOCategoryCode], [Status], ISNULL([Predefined], 'N') FROM Inserted
 
	INSERT INTO [CFGSOCCodeDescriptions] ([Code], Description, UICultureName, Seq)
	SELECT [Code], [Description], dbo.FW_GetActiveCultureName(), ISNULL(Seq, 0) FROM Inserted
END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[UpdateCFGSOCCodeTrigger]
	ON  [dbo].[CFGSOCCode]
INSTEAD OF UPDATE
AS
BEGIN
	SET NOCOUNT ON;
 
	DECLARE @Code NVARCHAR(10)
 
	IF UPDATE(Code)
		SELECT @Code = Code FROM Deleted
	ELSE
		SELECT @Code = Code FROM Inserted
 
	UPDATE CFGSOCCodeData
	SET Code = I.Code,
		EEOCategoryCode = I.EEOCategoryCode,
		Status = I.Status,
		Predefined = ISNULL(I.Predefined, 'N')
	FROM Inserted I
	INNER JOIN CFGSOCCodeData on CFGSOCCodeData.Code = @Code
 
	IF (NOT EXISTS(
		SELECT CFGSOCCodeDescriptions.UICultureName
		FROM CFGSOCCodeDescriptions
		WHERE CFGSOCCodeDescriptions.Code = @Code
			AND CFGSOCCodeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
	))
		INSERT INTO CFGSOCCodeDescriptions ([Code], [UICultureName], [Description], [Seq])
		SELECT [Code], dbo.FW_GetActiveCultureName(), [Description], ISNULL([Seq], 0) FROM Inserted
	ELSE
		UPDATE CFGSOCCodeDescriptions
		SET Code = I.Code,
			[Description] = I.Description,
			[Seq] = I.Seq
		FROM Inserted I
		INNER JOIN CFGSOCCodeDescriptions ON CFGSOCCodeDescriptions.Code = @Code
			AND CFGSOCCodeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
END
GO
