SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[VendorCustomTabFields] AS 
SELECT Clendor.Vendor, ClientCustomTabFields.* FROM ClientCustomTabFields 
Inner Join Clendor ON ClientCustomTabFields.ClientID = Clendor.ClientID
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertVendorCustomTabFieldsTrigger] 
   ON  [dbo].[VendorCustomTabFields]
instead of Insert
as
BEGIN
    SET NOCOUNT ON;

	Declare @ColumnList varchar(max)
	Declare @ColumnListValue varchar(max)
	Declare @columnName varchar(250)
	Declare @columnDataType nvarchar(32)
	Declare @columnDefaultValue nvarchar(2000)
	DECLARE customFirmColumns CURSOR for 
	
	select FW_CustomColumnsData.name as ColumnName, CFGScreenDesignerData.ComponentType as ColumnDataType, CFGScreenDesignerData.DefaultValue as ColumnDefaultValue
	from CFGScreenDesignerData inner join FW_CustomColumnsData
	on CFGScreenDesignerData.InfocenterArea = FW_CustomColumnsData.InfocenterArea and
	CFGScreenDesignerData.gridid = FW_CustomColumnsData.gridid and
	CFGScreenDesignerData.ComponentID = 'Clendor.'+ FW_CustomColumnsData.name
	where CFGScreenDesignerData.infocenterarea = 'firms' and CFGScreenDesignerData.GridID = 'x'

	set @columnList = ''
	set @columnListValue = ''

	open customFirmColumns
	fetch next from customFirmColumns into @columnName, @columnDataType, @columnDefaultValue
	while (@@FETCH_STATUS = 0)
		begin
			set @columnList = @columnList + ' ,[' + @columnname + ']'
			if @columnDataType = 'date' and ISNULL(@columnDefaultValue,'') = '[TODAY]'
				SET @columnDefaultValue = CONVERT(varchar(10), GETDATE(),121)
			if @columnDataType = 'currency' or @columnDataType = 'numeric'
				begin
					if @columnDefaultValue IS NOT NULL
						set @ColumnListValue = @ColumnListValue + ' ,ISNULL([' + @columnname + '],' + @columnDefaultValue + ')'
					else
						set @ColumnListValue = @ColumnListValue + ' ,ISNULL([' + @columnname + '],0)'
				end
			else if @columnDataType = 'date' or @columnDataType = 'dropdown' or @columnDataType = 'string'
				begin
					if @columnDefaultValue IS NOT NULL
						set @ColumnListValue = @ColumnListValue + ' ,ISNULL([' + @columnname + '],''' + @columnDefaultValue + ''')'
					else
						set @ColumnListValue = @ColumnListValue + ' ,[' + @columnname + ']'
				end
			else
				set @ColumnListValue = @ColumnListValue + ' ,[' + @columnname + ']'
			fetch next from customFirmColumns into @columnName, @columnDataType, @columnDefaultValue
		end

	close customFirmColumns
	deallocate customFirmColumns
	select * into #inserted from inserted
		exec('INSERT INTO ClientCustomTabfields (ClientID ' + @columnList + ',[CreateUser],[CreateDate],[ModUser],[ModDate]) ' +
		'SELECT clendor.ClientID ' + @ColumnListValue + ',#inserted.[CreateUser] ,isnull(#inserted.CreateDate,getutcdate()) ,
		#inserted.[ModUser] ,isnull(#inserted.ModDate,getutcdate()) FROM #inserted inner join Clendor on #inserted.ClientId = Clendor.ClientId ')
end
GO
