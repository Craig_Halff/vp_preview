SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGContractType]
as
SELECT dbo.CFGContractTypeData.Code, dbo.CFGContractTypeDescriptions.Description, dbo.CFGContractTypeDescriptions.Seq
FROM         dbo.CFGContractTypeData Left JOIN
dbo.CFGContractTypeDescriptions ON dbo.CFGContractTypeData.Code = dbo.CFGContractTypeDescriptions.Code 
and dbo.CFGContractTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGContractTypeTrigger] 
   ON  [dbo].[CFGContractType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGContractTypeDescriptions where code = @Code
Delete from CFGContractTypeData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGContractTypeTrigger] 
   ON  [dbo].[CFGContractType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGContractTypeData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGContractTypeDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGContractTypeTrigger] 
   ON  [dbo].[CFGContractType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGContractTypeData
  set code = i.code
from inserted i
  inner join CFGContractTypeData on CFGContractTypeData.code = @Code

if (not exists(Select CFGContractTypeDescriptions.UICultureName from CFGContractTypeDescriptions
  where CFGContractTypeDescriptions.code = @code and CFGContractTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGContractTypeDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGContractTypeDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGContractTypeDescriptions on CFGContractTypeDescriptions.code = @code and CFGContractTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
