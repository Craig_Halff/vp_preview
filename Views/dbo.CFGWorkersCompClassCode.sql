SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGWorkersCompClassCode]
AS
SELECT
	dbo.CFGWorkersCompClassCodeData.PKey,
	dbo.CFGWorkersCompClassCodeData.Code,
	dbo.CFGWorkersCompClassCodeData.State,
	dbo.CFGWorkersCompClassCodeDescriptions.[Description],
	dbo.CFGWorkersCompClassCodeDescriptions.Seq,
	dbo.CFGWorkersCompClassCodeData.Status,
	dbo.CFGWorkersCompClassCodeData.Predefined
FROM dbo.CFGWorkersCompClassCodeData
LEFT JOIN dbo.CFGWorkersCompClassCodeDescriptions ON dbo.CFGWorkersCompClassCodeDescriptions.PKey = dbo.CFGWorkersCompClassCodeData.PKey
	AND dbo.CFGWorkersCompClassCodeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGWorkersCompClassCodeTrigger]
	ON  [dbo].[CFGWorkersCompClassCode]
INSTEAD OF DELETE
AS
BEGIN
	SET NOCOUNT ON;
             
	DECLARE @PKey VARCHAR(32)
 
	SELECT TOP 1 @PKey = PKey FROM Deleted
 
	DELETE FROM CFGWorkersCompClassCodeDescriptions WHERE PKey = @PKey
	DELETE FROM CFGWorkersCompClassCodeData WHERE PKey = @PKey
END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[InsertCFGWorkersCompClassCodeTrigger]
	ON  [dbo].[CFGWorkersCompClassCode]
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON;
 
	INSERT INTO [CFGWorkersCompClassCodeData] ([PKey], [Code], [State], [Status], [Predefined])
	SELECT [PKey], [Code], [State], [Status], ISNULL([Predefined], 'N') FROM Inserted
 
	INSERT INTO [CFGWorkersCompClassCodeDescriptions] ([PKey], Description, UICultureName, Seq)
	SELECT [PKey], [Description], dbo.FW_GetActiveCultureName(), ISNULL(Seq, 0) FROM Inserted
END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[UpdateCFGWorkersCompClassCodeTrigger]
	ON  [dbo].[CFGWorkersCompClassCode]
INSTEAD OF UPDATE
AS
BEGIN
	SET NOCOUNT ON;
 
	DECLARE @PKey VARCHAR(32)
 
	IF UPDATE(PKey)
		SELECT @PKey = PKey FROM Deleted
	ELSE
		SELECT @PKey = PKey FROM Inserted
 
	UPDATE CFGWorkersCompClassCodeData
	SET Code = I.Code,
		State = I.State,
		Status = I.Status,
		Predefined = ISNULL(I.Predefined, 'N')
	FROM Inserted I
	INNER JOIN CFGWorkersCompClassCodeData on CFGWorkersCompClassCodeData.PKey = @PKey
 
	IF (NOT EXISTS(
		SELECT CFGWorkersCompClassCodeDescriptions.UICultureName
		FROM CFGWorkersCompClassCodeDescriptions
		WHERE CFGWorkersCompClassCodeDescriptions.PKey = @PKey
			AND CFGWorkersCompClassCodeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
	))
		INSERT INTO CFGWorkersCompClassCodeDescriptions ([PKey], [UICultureName], [Description], [Seq])
		SELECT [PKey], dbo.FW_GetActiveCultureName(), [Description], ISNULL([Seq], 0) FROM Inserted
	ELSE
		UPDATE CFGWorkersCompClassCodeDescriptions
		SET [Description] = I.Description,
			[Seq] = I.Seq
		FROM Inserted I
		INNER JOIN CFGWorkersCompClassCodeDescriptions ON CFGWorkersCompClassCodeDescriptions.PKey = @PKey
			AND CFGWorkersCompClassCodeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
END
GO
