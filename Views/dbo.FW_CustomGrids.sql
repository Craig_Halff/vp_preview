SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FW_CustomGrids]
AS
SELECT dbo.FW_CustomGridsData.InfocenterArea, dbo.FW_CustomGridsData.GridID, dbo.FW_CustomGridsData.TableName, dbo.FW_CustomGridsData.TabID, 
       dbo.FW_CustomGridsData.GridRows, dbo.FW_CustomGridsData.Seq, dbo.FW_CustomGridCaptions.UICultureName, dbo.FW_CustomGridCaptions.Caption,
       dbo.FW_CustomGridsData.GridType, dbo.FW_CustomGridsData.PropertyBag
  FROM dbo.FW_CustomGridsData LEFT JOIN
       dbo.FW_CustomGridCaptions ON dbo.FW_CustomGridCaptions.InfocenterArea = dbo.FW_CustomGridsData.InfocenterArea AND 
       dbo.FW_CustomGridCaptions.GridID = dbo.FW_CustomGridsData.GridID and
       dbo.FW_CustomGridCaptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DelFW_CustomGrids] 
  ON  [dbo].[FW_CustomGrids]
INSTEAD OF DELETE
AS 
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;

DELETE FROM FW_CustomGridCaptions FROM deleted
 WHERE FW_CustomGridCaptions.InfocenterArea = deleted.InfocenterArea 
   AND FW_CustomGridCaptions.GridID = deleted.GridID
      
DELETE FROM FW_CustomGridsData FROM deleted
 WHERE FW_CustomGridsData.InfocenterArea = deleted.InfocenterArea 
    AND FW_CustomGridsData.GridID = deleted.GridID
  
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[InsFW_CustomGrids] 
  ON  [dbo].[FW_CustomGrids]
INSTEAD OF Insert
AS
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
   Insert into FW_CustomGridsData
	(InfocenterArea, Gridid, TableName, TabId, GridRows, Seq, GridType, PropertyBag)
   SELECT 
	 InfocenterArea, Gridid, TableName, TabId, GridRows, Seq, GridType, PropertyBag FROM inserted
 
   Insert into FW_CustomGridCaptions
	(InfocenterArea, Gridid, UICultureName, Caption)
   SELECT 
	 InfocenterArea, Gridid, DBO.FW_GetActiveCultureName(), Caption FROM Inserted
 
END
 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[UpdFW_CustomGrids] 
  ON  [dbo].[FW_CustomGrids]
INSTEAD OF UPDATE
AS 
BEGIN
     -- SET NOCOUNTON added to prevent extra result sets FROM
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
DECLARE @GridID nvarchar(250)
IF (UPDATE(GridID))
	SELECT @GridID =GridID FROM deleted
ELSE
	SELECT @GridID =GridID FROM inserted


UPDATE FW_CustomGridsData
	SET TableName = i.TableName, Seq = isnull(i.Seq,0), GridRows = i.GridRows, GridID = i.GridID, 
		TabID = i.TabID, GridType = i.GridType, PropertyBag = i.PropertyBag
 FROM inserted i
    INNER JOIN FW_CustomGridsData ON FW_CustomGridsData.InfocenterArea = i.InfocenterArea AND FW_CustomGridsData.GridID = @GridID


IF (not exists (SELECT FW_CustomGridCaptions.UICultureName FROM FW_CustomGridCaptions, Inserted I
           WHERE  FW_CustomGridCaptions.InfocenterArea = i.InfocenterArea AND FW_CustomGridCaptions.gridId = @gridId
     AND FW_CustomGridCaptions.UICultureName =dbo.FW_GetActiveCultureName()))
     Insert into FW_CustomGridCaptions
	(InfocenterArea, GridID, UICultureName, Caption)
     SELECT 
	 InfocenterArea, GridID, dbo.FW_GetActiveCultureName(), Caption FROM inserted
 
ELSE
     UPDATE FW_CustomGridCaptions
           SET Caption = i.Caption, 
		       GridID = i.GridID
           FROM inserted i
           INNER JOIN FW_CustomGridCaptions ON FW_CustomGridCaptions.InfocenterArea = i.InfocenterArea AND FW_CustomGridCaptions.gridID = @GridID
           AND FW_CustomGridCaptions.UICultureName = dbo.FW_GetActiveCultureName()
 
 
 
END
GO
