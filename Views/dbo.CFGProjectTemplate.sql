SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGProjectTemplate]
as
SELECT dbo.CFGProjectTemplateData.Code, dbo.CFGProjectTemplateDescriptions.Description, dbo.CFGProjectTemplateDescriptions.Seq
  FROM dbo.CFGProjectTemplateData Left JOIN
       dbo.CFGProjectTemplateDescriptions ON dbo.CFGProjectTemplateData.Code = dbo.CFGProjectTemplateDescriptions.Code 
   and dbo.CFGProjectTemplateDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGProjectTemplateTrigger] 
   ON  [dbo].[CFGProjectTemplate]
INSTEAD OF Delete
AS 
BEGIN
   SET NOCOUNT ON;

   DELETE FROM CFGProjectTemplateDescriptions FROM deleted WHERE CFGProjectTemplateDescriptions.code = deleted.Code
   DELETE FROM CFGProjectTemplateData FROM deleted WHERE CFGProjectTemplateData.code = deleted.Code
END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGProjectTemplateTrigger] 
   ON  [dbo].[CFGProjectTemplate]
instead of Insert
as
BEGIN
   SET NOCOUNT ON;

   DECLARE @Seq as smallint
   SELECT @Seq=isnull(Seq,0) from inserted

   INSERT INTO CFGProjectTemplateData (Code)
   SELECT Code from inserted

   INSERT INTO CFGProjectTemplateDescriptions (Code, Description, UICultureName, Seq)
   SELECT Code, Description, dbo.FW_GetActiveCultureName(), @Seq from inserted

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGProjectTemplateTrigger] 
   ON  [dbo].[CFGProjectTemplate]
instead of Update
as
BEGIN
   SET NOCOUNT ON;

   DECLARE @Code Nvarchar(10)

   if update(Code)
	select @Code=Code from deleted
   else
	select @Code=Code from inserted

   Update CFGProjectTemplateData
      set Code = i.Code
     from inserted i inner join CFGProjectTemplateData on CFGProjectTemplateData.code = @Code

   if (not exists(Select CFGProjectTemplateDescriptions.UICultureName from CFGProjectTemplateDescriptions where CFGProjectTemplateDescriptions.code = @Code and CFGProjectTemplateDescriptions.UICultureName = dbo.FW_GetActiveCultureName()))
	Insert Into CFGProjectTemplateDescriptions (Code, UICultureName, Description, Seq)
	Select Code, dbo.FW_GetActiveCultureName(), Description, isnull(Seq,0) From Inserted 
   else
	Update CFGProjectTemplateDescriptions
	   Set Code = i.Code, Description = i.Description, Seq = i.Seq
	  From inserted i
	Inner Join CFGProjectTemplateDescriptions on CFGProjectTemplateDescriptions.code = @Code and CFGProjectTemplateDescriptions.UICultureName = dbo.FW_GetActiveCultureName() 

END

GO
