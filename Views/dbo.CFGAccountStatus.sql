SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGAccountStatus]
as
SELECT dbo.CFGAccountStatusData.Status, dbo.CFGAccountStatusData.WarningMsg, dbo.CFGAccountStatusDescriptions.Label 
FROM         dbo.CFGAccountStatusData Left JOIN
dbo.CFGAccountStatusDescriptions ON dbo.CFGAccountStatusData.Status = dbo.CFGAccountStatusDescriptions.Status 
and dbo.CFGAccountStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGAccountStatusTrigger] 
   ON  [dbo].[CFGAccountStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Status VARCHAR(1)

SELECT top 1 @Status = Status FROM Deleted

Delete from CFGAccountStatusDescriptions where Status = @Status
Delete from CFGAccountStatusData where Status = @Status

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGAccountStatusTrigger] 
   ON  [dbo].[CFGAccountStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGAccountStatusData]
  (Status, WarningMsg)
Select isnull(Status, 'N'), WarningMsg from inserted

INSERT INTO [CFGAccountStatusDescriptions]
  ([Status] 
  ,Label
  ,UICultureName)
Select isnull(Status,'N'),[Label], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGAccountStatusTrigger] 
   ON  [dbo].[CFGAccountStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Status VARCHAR(1)

if update(Status)
  select @Status=Status from deleted
else
  select @Status=Status from inserted

Update CFGAccountStatusData
  set Status = i.Status, WarningMsg = i.WarningMsg
from inserted i 
  inner join CFGAccountStatusData on CFGAccountStatusData.Status = @Status

if (not exists(Select CFGAccountStatusDescriptions.UICultureName from CFGAccountStatusDescriptions
  where CFGAccountStatusDescriptions.Status = @Status and CFGAccountStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGAccountStatusDescriptions
      ([Status]
      ,[UICultureName]
      ,[Label])
  Select [Status], dbo.FW_GetActiveCultureName(),[Label] From Inserted 

Else
  Update CFGAccountStatusDescriptions
      Set Status = i.Status
      , [Label] = i.Label
  From inserted i
  Inner Join CFGAccountStatusDescriptions on CFGAccountStatusDescriptions.Status = @Status and CFGAccountStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
