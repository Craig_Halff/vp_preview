SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGClientRole]
as
SELECT dbo.CFGClientRoleData.Code, dbo.CFGClientRoleDescriptions.Description, dbo.CFGClientRoleDescriptions.Seq
FROM         dbo.CFGClientRoleData Left JOIN
dbo.CFGClientRoleDescriptions ON dbo.CFGClientRoleData.Code = dbo.CFGClientRoleDescriptions.Code 
and dbo.CFGClientRoleDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGClientRoleTrigger] 
   ON  [dbo].[CFGClientRole]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGClientRoleDescriptions where code = @Code
Delete from CFGClientRoleData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGClientRoleTrigger] 
   ON  [dbo].[CFGClientRole]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGClientRoleData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGClientRoleDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGClientRoleTrigger] 
   ON  [dbo].[CFGClientRole]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGClientRoleData
  set code = i.code
from inserted i
  inner join CFGClientRoleData on CFGClientRoleData.code = @Code

if (not exists(Select CFGClientRoleDescriptions.UICultureName from CFGClientRoleDescriptions
  where CFGClientRoleDescriptions.code = @code and CFGClientRoleDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGClientRoleDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGClientRoleDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGClientRoleDescriptions on CFGClientRoleDescriptions.code = @code and CFGClientRoleDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
