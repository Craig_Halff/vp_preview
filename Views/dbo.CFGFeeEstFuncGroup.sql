SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGFeeEstFuncGroup]
as
SELECT dbo.CFGFeeEstFuncGroupData.Code,dbo.CFGFeeEstFuncGroupData.Disabled, ISNULL(dbo.CFGFeeEstFuncGroupDescriptions.Description, dbo.FW_TextNotTranslated.LocalizedValue) AS Description
FROM         dbo.CFGFeeEstFuncGroupData Left JOIN
dbo.CFGFeeEstFuncGroupDescriptions ON dbo.CFGFeeEstFuncGroupData.Code = dbo.CFGFeeEstFuncGroupDescriptions.Code 
and dbo.CFGFeeEstFuncGroupDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
	 LEFT JOIN dbo.FW_TextNotTranslated ON dbo.FW_TextNotTranslated.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGFeeEstFuncGroupTrigger] 
   ON  [dbo].[CFGFeeEstFuncGroup]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @code Nvarchar(100)

SELECT top 1 @code = code FROM Deleted

Delete from CFGFeeEstFuncGroupDescriptions where CFGFeeEstFuncGroupDescriptions.code = @code
Delete from CFGFeeEstFuncGroupData where CFGFeeEstFuncGroupData.code = @code 

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[InsertCFGFeeEstFuncGroupTrigger] 
   ON  [dbo].[CFGFeeEstFuncGroup]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;


INSERT INTO CFGFeeEstFuncGroupData
  (code, Disabled )
Select Code, Disabled from inserted

INSERT INTO CFGFeeEstFuncGroupDescriptions
  (code, UICultureName, Description)
Select Code, dbo.FW_GetActiveCultureName(), Description from inserted

End

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[UpdateCFGFeeEstFuncGroupTrigger] 
   ON  [dbo].[CFGFeeEstFuncGroup]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @code Nvarchar(100)

if update(code)
  select @code=code from deleted
else
  select @code=code from inserted

Update CFGFeeEstFuncGroupData
  set code = i.code, Disabled=i.Disabled
from inserted i
  inner join CFGFeeEstFuncGroupData on CFGFeeEstFuncGroupData.code = @code

if (not exists(Select CFGFeeEstFuncGroupDescriptions.UICultureName from CFGFeeEstFuncGroupDescriptions, Inserted I
  where CFGFeeEstFuncGroupDescriptions.code = @code and CFGFeeEstFuncGroupDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGFeeEstFuncGroupDescriptions
      (code,
       UICultureName,
         Description)
  Select code, dbo.FW_GetActiveCultureName(),Description From Inserted 

Else
  Update CFGFeeEstFuncGroupDescriptions
      Set code = i.code,
          Description = i.Description
  From inserted i
  Inner Join CFGFeeEstFuncGroupDescriptions on CFGFeeEstFuncGroupDescriptions.code = @code and CFGFeeEstFuncGroupDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END

GO
