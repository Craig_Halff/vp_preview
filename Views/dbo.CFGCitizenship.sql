SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGCitizenship]
as
SELECT dbo.CFGCitizenshipData.Code, dbo.CFGCitizenshipData.Status, dbo.CFGCitizenshipDescriptions.Description, dbo.CFGCitizenshipDescriptions.Seq
FROM dbo.CFGCitizenshipData 
Left JOIN dbo.CFGCitizenshipDescriptions ON dbo.CFGCitizenshipData.Code = dbo.CFGCitizenshipDescriptions.Code 
and dbo.CFGCitizenshipDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGCitizenshipTrigger] 
   ON  [dbo].[CFGCitizenship]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGCitizenshipDescriptions where Code = @Code
Delete from CFGCitizenshipData where Code = @Code

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGCitizenshipTrigger] 
   ON  [dbo].[CFGCitizenship]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO CFGCitizenshipData
  (Code, Status)
Select Code, Status from inserted

INSERT INTO CFGCitizenshipDescriptions
  (Code 
  ,Description
  ,UICultureName
  ,Seq)
Select Code,Description, dbo.FW_GetActiveCultureName(),@seq from inserted

End

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGCitizenshipTrigger] 
   ON  [dbo].[CFGCitizenship]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGCitizenshipData
  set Code = i.Code,
      Status = i.Status
from inserted i
  inner join CFGCitizenshipData on CFGCitizenshipData.Code = @Code

if (not exists(Select CFGCitizenshipDescriptions.UICultureName from CFGCitizenshipDescriptions
  where CFGCitizenshipDescriptions.Code = @Code and CFGCitizenshipDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGCitizenshipDescriptions
      (Code
      ,UICultureName
      ,Description
      ,Seq)
  Select Code, dbo.FW_GetActiveCultureName(),Description,Seq From Inserted 

Else
  Update CFGCitizenshipDescriptions
      Set Code = i.Code
      , Description = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGCitizenshipDescriptions on CFGCitizenshipDescriptions.Code = @Code and CFGCitizenshipDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END

GO
