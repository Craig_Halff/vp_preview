SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGPYWHMethod]
as
SELECT dbo.CFGPYWHMethodData.Method,  dbo.CFGPYWHMethodDescriptions.Label, dbo.CFGPYWHMethodData.Seq 
FROM         dbo.CFGPYWHMethodData Left JOIN
dbo.CFGPYWHMethodDescriptions ON dbo.CFGPYWHMethodData.Method = dbo.CFGPYWHMethodDescriptions.Method 
and dbo.CFGPYWHMethodDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGPYWHMethodTrigger] 
   ON  [dbo].[CFGPYWHMethod]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Method VARCHAR(1)

SELECT top 1 @Method = Method FROM Deleted

Delete from CFGPYWHMethodDescriptions where Method = @Method
Delete from CFGPYWHMethodData where Method = @Method

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGPYWHMethodTrigger] 
   ON  [dbo].[CFGPYWHMethod]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGPYWHMethodData]
  (method, seq)
Select Method,seq from inserted

INSERT INTO [CFGPYWHMethodDescriptions]
  (Method 
  ,Label
  ,UICultureName)
Select Method,[Label], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGPYWHMethodTrigger] 
   ON  [dbo].[CFGPYWHMethod]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Method VARCHAR(1)

if update(method)
  select @Method=Method from deleted
else
  select @Method=Method from inserted

Update CFGPYWHMethodData
  set method = i.Method, seq = i.seq
from inserted i
  inner join CFGPYWHMethodData on CFGPYWHMethodData.Method = @Method

if (not exists(Select CFGPYWHMethodDescriptions.UICultureName from CFGPYWHMethodDescriptions
  where CFGPYWHMethodDescriptions.Method = @Method and CFGPYWHMethodDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGPYWHMethodDescriptions
      (Method
      ,[UICultureName]
      ,[Label])
  Select method, dbo.FW_GetActiveCultureName(),[Label] From Inserted 

Else
  Update CFGPYWHMethodDescriptions
      Set Method = i.Method
      , [Label] = i.Label
  From inserted i
  Inner Join CFGPYWHMethodDescriptions on CFGPYWHMethodDescriptions.Method = @Method and CFGPYWHMethodDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
