SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CustomRepOpts]
AS
SELECT     dbo.CustomRepOptsData.ColName, dbo.CustomRepOptsData.ColDataType, dbo.CustomRepOptsData.ColSeq, 
                      dbo.CustomRepOptsData.ColDisplayWidth, dbo.CustomRepOptsData.LimitToList, dbo.CustomRepOptsData.ColTotal, 
                      dbo.CustomRepOptsData.CurrencyMask, dbo.CustomRepOptsData.DecimalPlaces, dbo.CustomRepOptsData.MinValue, 
                      dbo.CustomRepOptsData.MaxValue, dbo.CustomRepOptsData.MinDate, dbo.CustomRepOptsData.MaxDate, dbo.CustomRepOptsData.AcctRequired, 
                      dbo.CustomRepOptsData.MktRequired, dbo.CustomRepOptsData.HTMLHeight, dbo.CustomRepOptsData.LayoutColumn, 
                      dbo.CustomRepOptsData.CreateUser, dbo.CustomRepOptsData.CreateDate, dbo.CustomRepOptsData.ModUser, dbo.CustomRepOptsData.ModDate, 
                      dbo.CustomRepOptCaptions.ColLabel, dbo.CustomRepOptsData.TabID, dbo.CustomRepOptsData.ReportName, 
                      dbo.CustomRepOptCaptions.DefaultValue
FROM         dbo.CustomRepOptsData LEFT JOIN
                      dbo.CustomRepOptCaptions ON dbo.CustomRepOptCaptions.ReportName = dbo.CustomRepOptsData.ReportName AND 
                      dbo.CustomRepOptCaptions.TabID = dbo.CustomRepOptsData.TabID AND 
                      dbo.CustomRepOptCaptions.ColName = dbo.CustomRepOptsData.ColName and
			dbo.CustomRepOptCaptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[DeleteCustomRepOptsTrigger] 
   ON  [dbo].[CustomRepOpts]
instead of Delete
AS 
BEGIN
     -- SET NOCOUNT ON added to prevent extra result sets from
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
 
DECLARE @ReportName VARCHAR(50)
DECLARE @TabId Nvarchar(100)
DECLARE @ColName as Nvarchar(128)


SELECT top 1 @ReportName = ReportName, @TabId= TabId, @ColName=ColName  FROM Deleted
 
 
Delete from CustomRepOptCaptions where
 CustomRepOptCaptions.ReportName = @ReportName and CustomRepOptCaptions.TabId = @TabId and CustomRepOptCaptions.ColName = @ColName
      
Delete from CustomRepOptsData where CustomRepOptsData.ReportName = @ReportName and CustomRepOptsData.TabId = @TabId and CustomRepOptsData.ColName = @ColName 
 
 
END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCustomRepOptsTrigger] 
   ON  [dbo].[CustomRepOpts]
instead of Insert
as
BEGIN
     -- SET NOCOUNT ON added to prevent extra result sets from
     -- interfering with SELECT statements.
     SET NOCOUNT ON;


 
   Insert into CustomRepOptsData
           Select ReportName, TabID, ColName, ColDataType, ColSeq,ColDisplayWidth, isnull(LimitToList,'N'),isnull(ColTotal,'N'),CurrencyMask, DecimalPlaces,MinValue,MaxValue,MinDate,MaxDate,AcctRequired,MktRequired,HTMLHeight, LayoutColumn,CreateUser,CreateDate,ModUser,ModDate from inserted
 
   Insert into CustomRepOptCaptions
           Select ReportName, dbo.FW_GetActiveCultureName(), TabID, ColName,ColLabel,DefaultValue from Inserted
 
 
END
 
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create  TRIGGER [dbo].[UpdateCustomRepOptsTrigger] 
   ON  [dbo].[CustomRepOpts]
instead of Update
AS 
BEGIN
     -- SET NOCOUNT ON added to prevent extra result sets from
     -- interfering with SELECT statements.
     SET NOCOUNT ON;
 
DECLARE @ReportName Nvarchar(80)
DECLARE @TabID Nvarchar(100)
DECLARE @ColName Nvarchar(30)
if update(Reportname)
	select @Reportname =Reportname from deleted
else
	select @Reportname =Reportname from inserted

if update(TabID)
	select @TabID =TabID from deleted
else
	select @TabID =TabID from inserted

if update([ColName])
	select @ColName =[ColName] from deleted
else
	select @ColName =[ColName] from inserted


update CustomRepOptsData
	set ReportName = i.ReportName, 
		TabID = i.TabID,
		ColName = i.ColName,
		ColDataType = i.ColDataType, 
		ColDisplayWidth = i.ColDisplayWidth, 
		ColSeq = i.Colseq, 
		LimitToList=i.LimitToList,
		ColTotal =i.ColTotal,
		CurrencyMask = i.CurrencyMask,
		DecimalPlaces = i.DecimalPlaces,	
		MinValue= i.MinValue,
		MaxValue= i.MaxValue,
		MinDate= i.MinDate,
		MaxDate= i.MaxDate,
		AcctRequired= i.AcctRequired,
		MktRequired = i.MktRequired,
		LayOutColumn = i.LayOutColumn,
		CreateUser = i.CreateUser,
		CreateDate = i.CreateDate, 
		ModUser= i.ModUser,
		ModDate = i.ModDate
 from inserted i
    inner join CustomRepOptsData on CustomRepOptsData.ReportName = @ReportName and CustomRepOptsData.TabID = @TabID and CustomRepOptsData.ColName = @ColName


if (not exists (Select CustomRepOptCaptions.UICultureName from CustomRepOptCaptions, Inserted I
           Where  CustomRepOptCaptions.ReportName = @ReportName and CustomRepOptCaptions.TabId = @TabId
				and CustomRepOptCaptions.Colname = @ColName and CustomRepOptCaptions.UICulturename =dbo.FW_GetActiveCultureName()))
     Insert into CustomRepOptCaptions
           Select ReportName,dbo.FW_GetActiveCultureName(),TabID, ColName,ColLabel,DefaultValue from inserted
 
Else
     update CustomRepOptCaptions
           set ReportName = i.ReportName, 
		       TabID = i.TabID,
			   ColName = i.ColName,
			   ColLabel = i.ColLabel, 
 				DefaultValue = i.DefaultValue
           from inserted i
           inner join CustomRepOptCaptions on CustomRepOptCaptions.ReportName = @ReportName and CustomRepOptCaptions.tabID = @tabID
            and CustomRepOptCaptions.ColName = @ColName and CustomRepOptCaptions.UICultureName = dbo.FW_GetActiveCultureName()
 
 
 
END
 
GO
