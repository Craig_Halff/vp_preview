SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGLeadStatus]
as
SELECT dbo.CFGLeadStatusData.Code, dbo.CFGLeadStatusDescriptions.Description, dbo.CFGLeadStatusDescriptions.Seq
FROM         dbo.CFGLeadStatusData Left JOIN
dbo.CFGLeadStatusDescriptions ON dbo.CFGLeadStatusData.Code = dbo.CFGLeadStatusDescriptions.Code 
and dbo.CFGLeadStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGLeadStatusTrigger] 
   ON  [dbo].[CFGLeadStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code NVARCHAR(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGLeadStatusDescriptions where Code = @Code
Delete from CFGLeadStatusData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGLeadStatusTrigger] 
   ON  [dbo].[CFGLeadStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGLeadStatusData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGLeadStatusDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(), isnull(Seq,0) from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGLeadStatusTrigger] 
   ON  [dbo].[CFGLeadStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code NVARCHAR(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGLeadStatusData
  set Code = i.Code
from inserted i
  inner join CFGLeadStatusData on CFGLeadStatusData.Code = @Code

if (not exists(Select CFGLeadStatusDescriptions.UICultureName from CFGLeadStatusDescriptions
  where CFGLeadStatusDescriptions.Code = @Code and CFGLeadStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGLeadStatusDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description], isnull([Seq],0) From Inserted 

Else
  Update CFGLeadStatusDescriptions
      Set Code = i.Code
      , [Description] = i.Description
      , [Seq] = i.Seq
  From inserted i
  Inner Join CFGLeadStatusDescriptions on CFGLeadStatusDescriptions.Code = @Code and CFGLeadStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
