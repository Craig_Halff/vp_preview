SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGProjectMilestone]
as
SELECT dbo.CFGProjectMilestoneData.Code, dbo.CFGProjectMilestoneData.SystemInd, dbo.CFGProjectMilestoneDescriptions.Description, dbo.CFGProjectMilestoneDescriptions.Seq
FROM   dbo.CFGProjectMilestoneData Left JOIN dbo.CFGProjectMilestoneDescriptions 
ON dbo.CFGProjectMilestoneData.Code = dbo.CFGProjectMilestoneDescriptions.Code and dbo.CFGProjectMilestoneDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGProjectMilestoneTrigger] 
   ON  [dbo].[CFGProjectMilestone]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(20)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGProjectMilestoneDescriptions where code = @Code
Delete from CFGProjectMilestoneData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGProjectMilestoneTrigger] 
   ON  [dbo].[CFGProjectMilestone]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO CFGProjectMilestoneData
  (Code,
   SystemInd)
Select Code, SystemInd from inserted

INSERT INTO CFGProjectMilestoneDescriptions
  (Code 
  ,Description
  ,UICultureName
  ,Seq)
Select Code,Description, dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGProjectMilestoneTrigger] 
   ON  [dbo].[CFGProjectMilestone]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(20)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGProjectMilestoneData
  set code = i.code,
	  systemind = i.systemind
from inserted i
  inner join CFGProjectMilestoneData on CFGProjectMilestoneData.code = @Code

if (not exists(Select CFGProjectMilestoneDescriptions.UICultureName from CFGProjectMilestoneDescriptions
  where CFGProjectMilestoneDescriptions.code = @code and CFGProjectMilestoneDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGProjectMilestoneDescriptions
      (Code
      ,UICultureName
      ,Description
      ,Seq)
  Select Code, dbo.FW_GetActiveCultureName(),Description,Seq From Inserted 

Else
  Update CFGProjectMilestoneDescriptions
      Set code = i.code
      , Description = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGProjectMilestoneDescriptions on CFGProjectMilestoneDescriptions.code = @code and CFGProjectMilestoneDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
