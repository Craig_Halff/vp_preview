SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGEmployeeStatus]
as
SELECT dbo.CFGEmployeeStatusData.Status,  dbo.CFGEmployeeStatusDescriptions.Label 
FROM         dbo.CFGEmployeeStatusData Left JOIN
dbo.CFGEmployeeStatusDescriptions ON dbo.CFGEmployeeStatusData.Status = dbo.CFGEmployeeStatusDescriptions.Status 
and dbo.CFGEmployeeStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGEmployeeStatusTrigger] 
   ON  [dbo].[CFGEmployeeStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Status VARCHAR(1)

SELECT top 1 @Status = Status FROM Deleted

Delete from CFGEmployeeStatusDescriptions where Status = @Status
Delete from CFGEmployeeStatusData where Status = @Status

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGEmployeeStatusTrigger] 
   ON  [dbo].[CFGEmployeeStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGEmployeeStatusData]
  (Status)
Select Status from inserted

INSERT INTO [CFGEmployeeStatusDescriptions]
  ([Status] 
  ,Label
  ,UICultureName)
Select [Status],[Label], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGEmployeeStatusTrigger] 
   ON  [dbo].[CFGEmployeeStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Status VARCHAR(1)

if update(Status)
  select @Status=Status from deleted
else
  select @Status=Status from inserted

Update CFGEmployeeStatusData
  set Status = i.Status
from inserted i
  inner join CFGEmployeeStatusData on CFGEmployeeStatusData.Status = @Status

if (not exists(Select CFGEmployeeStatusDescriptions.UICultureName from CFGEmployeeStatusDescriptions
  where CFGEmployeeStatusDescriptions.Status = @Status and CFGEmployeeStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGEmployeeStatusDescriptions
      ([Status]
      ,[UICultureName]
      ,[Label])
  Select [Status], dbo.FW_GetActiveCultureName(),[Label] From Inserted 

Else
  Update CFGEmployeeStatusDescriptions
      Set Status = i.Status
      , [Label] = i.Label
  From inserted i
  Inner Join CFGEmployeeStatusDescriptions on CFGEmployeeStatusDescriptions.Status = @Status and CFGEmployeeStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
