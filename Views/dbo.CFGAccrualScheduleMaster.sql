SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGAccrualScheduleMaster]
as
SELECT dbo.CFGAccrualScheduleMasterData.ScheduleID, dbo.CFGAccrualScheduleMasterData.Company,  dbo.CFGAccrualScheduleMasterData.BasedOnHoursWorked,
	dbo.CFGAccrualScheduleMasterDescriptions.Description
FROM   dbo.CFGAccrualScheduleMasterData
 Left JOIN dbo.CFGAccrualScheduleMasterDescriptions ON dbo.CFGAccrualScheduleMasterData.ScheduleID = dbo.CFGAccrualScheduleMasterDescriptions.ScheduleID
and   dbo.CFGAccrualScheduleMasterData.Company = dbo.CFGAccrualScheduleMasterDescriptions.Company
and dbo.CFGAccrualScheduleMasterDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGAccrualScheduleMasterTrigger] 
   ON  [dbo].[CFGAccrualScheduleMaster]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @ScheduleID Nvarchar(10)
DECLARE @Company Nvarchar(14)

SELECT top 1 @ScheduleID = ScheduleID, @Company = Company FROM Deleted

Delete from CFGAccrualScheduleMasterDescriptions where CFGAccrualScheduleMasterDescriptions.ScheduleID = @ScheduleID and 
            CFGAccrualScheduleMasterDescriptions.Company = @Company
Delete from CFGAccrualScheduleMasterData where CFGAccrualScheduleMasterData.ScheduleID = @ScheduleID and CFGAccrualScheduleMasterData.Company = @Company

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGAccrualScheduleMasterTrigger] 
   ON  [dbo].[CFGAccrualScheduleMaster]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGAccrualScheduleMasterData]
  (ScheduleID, Company, BasedOnHoursWorked)
Select ScheduleID, Company, isnull(BasedOnHoursWorked,'N') from inserted

INSERT INTO [CFGAccrualScheduleMasterDescriptions]
  (ScheduleID, Company, Description, UICultureName)
Select ScheduleID, Company, Description, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGAccrualScheduleMasterTrigger] 
   ON  [dbo].[CFGAccrualScheduleMaster]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @ScheduleID Nvarchar(10)

if update(ScheduleID)
  select @ScheduleID=ScheduleID from deleted
else
  select @ScheduleID=ScheduleID from inserted

Update CFGAccrualScheduleMasterData
  set ScheduleID = i.ScheduleID, Company=i.Company, BasedOnHoursWorked=i.BasedOnHoursWorked
from inserted i
  inner join CFGAccrualScheduleMasterData on CFGAccrualScheduleMasterData.Company = i.Company and CFGAccrualScheduleMasterData.ScheduleID = @ScheduleID

if (not exists(Select CFGAccrualScheduleMasterDescriptions.UICultureName from CFGAccrualScheduleMasterDescriptions, Inserted I
  where CFGAccrualScheduleMasterDescriptions.Company = i.Company and CFGAccrualScheduleMasterDescriptions.ScheduleID = @ScheduleID and CFGAccrualScheduleMasterDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGAccrualScheduleMasterDescriptions
	(ScheduleID,
	Company,
	UICultureName,
	Description)
  Select ScheduleID, Company, dbo.FW_GetActiveCultureName(), Description From Inserted 

Else
  Update CFGAccrualScheduleMasterDescriptions
      Set ScheduleID = i.ScheduleID,
          Company = i.Company,
          Description = i.Description
  From inserted i
  Inner Join CFGAccrualScheduleMasterDescriptions on CFGAccrualScheduleMasterDescriptions.Company = i.Company and CFGAccrualScheduleMasterDescriptions.ScheduleID = @ScheduleID and CFGAccrualScheduleMasterDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
