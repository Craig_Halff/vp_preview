SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FW_CFGLabels]
AS
SELECT     dbo.FW_CFGLabelData.*
FROM         dbo.FW_CFGLabelData
WHERE dbo.FW_CFGLabelData.UICultureName = dbo.FW_GetActiveCultureName()
GO
