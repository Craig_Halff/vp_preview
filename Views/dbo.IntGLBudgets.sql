SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[IntGLBudgets] AS
  SELECT CAB.BudgetName, CAB.Org, (CAB.BudgetYear * 100) + 1 AS period, Account AS account, cabDetail.Amount1 AS budgetAmount, 0 AS annualbudgetAmount
    FROM CAB INNER JOIN cabDetail ON CAB.BudgetName = CABDetail.BudgetName
   WHERE cabDetail.Amount1 < -0.001 OR cabDetail.Amount1 > 0.001
 UNION ALL
 SELECT CAB.BudgetName, CAB.Org, (CAB.BudgetYear * 100) + 2 AS period, Account AS account, cabDetail.Amount2 AS budgetAmount, 0 AS annualbudgetAmount
   FROM CAB INNER JOIN cabDetail ON CAB.BudgetName = CABDetail.BudgetName
  WHERE cabDetail.Amount2 < -0.001 OR cabDetail.Amount2 > 0.001
 UNION ALL
 SELECT CAB.BudgetName, CAB.Org, (CAB.BudgetYear * 100) + 3 AS period, Account AS account, cabDetail.Amount3 AS budgetAmount, 0 AS annualbudgetAmount
   FROM CAB INNER JOIN cabDetail ON CAB.BudgetName = CABDetail.BudgetName
  WHERE cabDetail.Amount3 < -0.001 OR cabDetail.Amount3 > 0.001
 UNION ALL
 SELECT CAB.BudgetName, CAB.Org, (CAB.BudgetYear * 100) + 4 AS period, Account AS account, cabDetail.Amount4 AS budgetAmount, 0 AS annualbudgetAmount
   FROM CAB INNER JOIN cabDetail ON CAB.BudgetName = CABDetail.BudgetName
  WHERE cabDetail.Amount4 < -0.001 OR cabDetail.Amount4 > 0.001
 UNION ALL
 SELECT CAB.BudgetName, CAB.Org, (CAB.BudgetYear * 100) + 5 AS period, Account AS account, cabDetail.Amount5 AS budgetAmount, 0 AS annualbudgetAmount
   FROM CAB INNER JOIN cabDetail ON CAB.BudgetName = CABDetail.BudgetName
  WHERE cabDetail.Amount5 < -0.001 OR cabDetail.Amount5 > 0.001
 UNION ALL
 SELECT CAB.BudgetName, CAB.Org, (CAB.BudgetYear * 100) + 6 AS period, Account AS account, cabDetail.Amount6 AS budgetAmount, 0 AS annualbudgetAmount
   FROM CAB INNER JOIN cabDetail ON CAB.BudgetName = CABDetail.BudgetName
  WHERE cabDetail.Amount6 < -0.001 OR cabDetail.Amount6 > 0.001
 UNION ALL
 SELECT CAB.BudgetName, CAB.Org, (CAB.BudgetYear * 100) + 7 AS period, Account AS account, cabDetail.Amount7 AS budgetAmount, 0 AS annualbudgetAmount
   FROM CAB INNER JOIN cabDetail ON CAB.BudgetName = CABDetail.BudgetName
  WHERE cabDetail.Amount7 < -0.001 OR cabDetail.Amount7 > 0.001
 UNION ALL
 SELECT CAB.BudgetName, CAB.Org, (CAB.BudgetYear * 100) + 8 AS period, Account AS account, cabDetail.Amount8 AS budgetAmount, 0 AS annualbudgetAmount
   FROM CAB INNER JOIN cabDetail ON CAB.BudgetName = CABDetail.BudgetName
  WHERE cabDetail.Amount8 < -0.001 OR cabDetail.Amount8 > 0.001
 UNION ALL
 SELECT CAB.BudgetName, CAB.Org, (CAB.BudgetYear * 100) + 9 AS period, Account AS account, cabDetail.Amount9 AS budgetAmount, 0 AS annualbudgetAmount
   FROM CAB INNER JOIN cabDetail ON CAB.BudgetName = CABDetail.BudgetName
  WHERE cabDetail.Amount9 < -0.001 OR cabDetail.Amount9 > 0.001
 UNION ALL
 SELECT CAB.BudgetName, CAB.Org, (CAB.BudgetYear * 100) + 10 AS period, Account AS account, cabDetail.Amount10 AS budgetAmount, 0 AS annualbudgetAmount
   FROM CAB INNER JOIN cabDetail ON CAB.BudgetName = CABDetail.BudgetName
  WHERE cabDetail.Amount10 < -0.001 OR cabDetail.Amount10 > 0.001
 UNION ALL
 SELECT CAB.BudgetName, CAB.Org, (CAB.BudgetYear * 100) + 11 AS period, Account AS account, cabDetail.Amount11 AS budgetAmount, 0 AS annualbudgetAmount
   FROM CAB INNER JOIN cabDetail ON CAB.BudgetName = CABDetail.BudgetName
  WHERE cabDetail.Amount11 < -0.001 OR cabDetail.Amount11 > 0.001
 UNION ALL
 SELECT CAB.BudgetName, CAB.Org, (CAB.BudgetYear * 100) + 12 AS period, Account AS account, cabDetail.Amount12 AS budgetAmount, 0 AS annualbudgetAmount
   FROM CAB INNER JOIN cabDetail ON CAB.BudgetName = CABDetail.BudgetName
  WHERE cabDetail.Amount12 < -0.001 OR cabDetail.Amount12 > 0.001
 UNION ALL
 SELECT CAB.BudgetName, CAB.Org, (CAB.BudgetYear * 100) + 13 AS period, Account AS account, cabDetail.Amount13 AS budgetAmount, 0 AS annualbudgetAmount
   FROM CAB INNER JOIN cabDetail ON CAB.BudgetName = CABDetail.BudgetName
  WHERE cabDetail.Amount13 < -0.001 OR cabDetail.Amount13 > 0.001
 UNION ALL
 SELECT CAB.BudgetName, CAB.Org, (CAB.BudgetYear * 100) + 00 AS period, Account AS account, 0 AS budgetAmount, cabDetail.Annual AS annualbudgetAmount
   FROM CAB INNER JOIN cabDetail ON CAB.BudgetName = CABDetail.BudgetName
  WHERE cabDetail.Annual < -0.001 OR cabDetail.Annual > 0.001
GO
