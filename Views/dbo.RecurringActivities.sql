SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RecurringActivities] AS
SELECT * FROM (
SELECT ROW_NUMBER() OVER(PARTITION BY ActivityID ORDER BY ActivityID, dateadd(d, datediff(d, dateadd(hh,datediff(hh,getutcdate(),getdate()),startdate), days.day), startdate) ASC) AS Row#, ActivityID, ClientID, ContactID, WBS1, WBS2, WBS3, Employee, LeadID, CampaignID, Type, Subject, 
	dateadd(d, datediff(d, dateadd(hh,datediff(hh,getutcdate(),getdate()),startdate), days.day), startdate) AS StartDate, 
	StartTime, dateadd(d, datediff(d, dateadd(hh,datediff(hh,getutcdate(),getdate()),enddate), days.day), enddate) AS EndDate, EndTime, 
	Duration, Location, ReminderInd, ReminderUnit, ReminderMinHrDay, 
	Case ReminderMinHrDay When 'Minutes' Then
	dateadd(mi, (cast(ReminderUnit As int) * -1), dateadd(d, datediff(d, dateadd(hh,datediff(hh,getutcdate(),getdate()),startdate), days.day), startdate)) 
	When 'Hours' Then 
	dateadd(hh, (cast(ReminderUnit As int) * -1), dateadd(d, datediff(d, dateadd(hh,datediff(hh,getutcdate(),getdate()),startdate), days.day), startdate)) 
	When 'Days' Then 
	dateadd(d, (cast(ReminderUnit As int) * -1), dateadd(d, datediff(d, dateadd(hh,datediff(hh,getutcdate(),getdate()),startdate), days.day), startdate)) 
	End AS ReminderDate,
	ReminderTime, 
	Priority, Notes, ShowTimeAs, AllDayEventInd, CompletionInd, RecurrenceInd, 
	PrivateInd, TaskStatus, TaskCompletionDate, OpportunityID, CampaignCode, RecurrType, 
	RecurrDailyFreq, RecurrDailyWeekDay, RecurrWeeklyFreq, RecurrWeeklySun, 
	RecurrWeeklyMon, RecurrWeeklyTue, RecurrWeeklyWed, RecurrWeeklyThu, RecurrWeeklyFri, 
	RecurrWeeklySat, RecurrMonthlyFreq, RecurrMonthlyDay, RecurrMonthlyOccurInd, 
	RecurrMonthlyOccur, RecurrMonthlyOccurDay, RecurrMonthlyOccurFreq, RecurrYearlyMonth, 
	RecurrYearlyDay, RecurrStartDate, RecurrEndType, RecurrEndDate, RP, EmailAlertSent, 
	RecurrID, Vendor, ContactIDForVendor, CreateUser, CreateDate, ModUser, ModDate, MaxOccurences
  FROM Activity WITH (NOLOCK), Days WITH (NOLOCK)
 WHERE ((RecurrenceInd = 'Y' AND datediff(d,dateadd(hh,datediff(hh,getutcdate(),getdate()),activity.startdate),days.day) >= 0) 
   AND (((RecurrEndType = 'Y' AND RecurrEndDate is NOT null AND ( datediff(d,days.day,RecurrEndDate) >= 0))) 
	OR RecurrEndType = 'N' OR RecurrEndType = 'A'
	   ))
   AND ActivityID not in 
       (select ActivityID from RecurrActivityException r 
	 where r.ActivityID = Activity.ActivityID
	   and datediff(d,dateadd(hh,datediff(hh,getutcdate(),getdate()),r.StartDate),days.day) = 0)
   AND ((RecurrType = 'Weekly' AND case when RecurrWeeklyFreq = 0 then 1 else  ((day(day)-1) / 7)  % RecurrWeeklyFreq end = 0 
	AND ((datepart(dw, days.day) = 1 AND RecurrWeeklySun = 'Y') OR
             (datepart(dw, days.day) = 2 AND RecurrWeeklyMon = 'Y') OR
             (datepart(dw, days.day) = 3 AND RecurrWeeklyTue = 'Y') OR
             (datepart(dw, days.day) = 4 AND RecurrWeeklyWed = 'Y') OR
             (datepart(dw, days.day) = 5 AND RecurrWeeklyThu = 'Y') OR
             (datepart(dw, days.day) = 6 AND RecurrWeeklyFri = 'Y') OR
             (datepart(dw, days.day) = 7 AND RecurrWeeklySat = 'Y'))) OR
         (RecurrType = 'Daily' AND ((RecurrDailyWeekDay = 'N' AND case when RecurrDailyFreq = 0 then 1 else datediff(d, dateadd(hh,datediff(hh,getutcdate(),getdate()),activity.startdate), days.day) % RecurrDailyFreq end = 0) OR
         (RecurrDailyWeekDay = 'Y' AND datepart(dw, days.day) BETWEEN 2 AND 6))) OR
         (RecurrType = 'Monthly' AND ((RecurrMonthlyOccurInd = 'N' AND (case when RecurrMonthlyFreq = 0 then 1 else datediff(m, dateadd(hh,datediff(hh,getutcdate(),getdate()),activity.startdate), days.day) % RecurrMonthlyFreq end = 0 AND day(days.day) = RecurrMonthlyDay AND day(days.day) = RecurrMonthlyDay))
	 	OR (RecurrMonthlyOccurInd = 'Y'   AND	 RecurrMonthlyOccurDay + 1	= datepart(dw,days.day) AND case when RecurrMonthlyOccurFreq = 0 then 1 else datediff(m,dateadd(hh,datediff(hh,getutcdate(),getdate()),activity.startdate),days.day) % RecurrMonthlyOccurFreq END = 0 AND  (Day(days.day) - 1)/7 = RecurrMonthlyOccur))) OR
         (RecurrType = 'Yearly' AND ((RecurrYearlyOccurInd = 'N' AND  month(days.day) = RecurrYearlyMonth AND day(days.day) = RecurrYearlyDay)
		 OR (RecurrYearlyOccurInd = 'Y' and  month(days.day) = RecurrYearlyMonth AND	 RecurrYearlyOccurDay + 1	 = datepart(dw,days.day) 
		 AND  (Day(days.day) - 1)/7 + 1 = RecurrYearlyOccur)))
		 )
) Qry
WHERE	(RecurrEndType = 'A' AND  Row# <= MaxOccurences)
		OR RecurrEndType = 'N'
		OR RecurrEndType = 'Y'
GO
