SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGPYWHType]
as
SELECT dbo.CFGPYWHTypeData.TYPE, dbo.CFGPYWHTypeData.seq, dbo.CFGPYWHTypeDescriptions.Label
FROM   dbo.CFGPYWHTypeData
 Left JOIN dbo.CFGPYWHTypeDescriptions ON dbo.CFGPYWHTypeData.Type = dbo.CFGPYWHTypeDescriptions.Type
 and dbo.CFGPYWHTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGPYWHTypeTrigger] 
   ON  [dbo].[CFGPYWHType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Type VARCHAR(1)


SELECT top 1 @Type = Type  FROM Deleted

Delete from CFGPYWHTypeDescriptions where CFGPYWHTypeDescriptions.Type = @Type 
Delete from CFGPYWHTypeData where CFGPYWHTypeData.Type = @Type 

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGPYWHTypeTrigger] 
   ON  [dbo].[CFGPYWHType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGPYWHTypeData]
  (Type, seq)
Select Type, seq from inserted

INSERT INTO [CFGPYWHTypeDescriptions]
  (Type, Label,UICultureName)
Select Type, Label, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGPYWHTypeTrigger] 
   ON  [dbo].[CFGPYWHType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Type VARCHAR(1)

if update(Type)
  select @Type=Type from deleted
else
  select @Type=Type from inserted

Update CFGPYWHTypeData
  set Type = i.Type, seq=i.seq
from inserted i
  inner join CFGPYWHTypeData on CFGPYWHTypeData.Type = @Type

if (not exists(Select CFGPYWHTypeDescriptions.UICultureName from CFGPYWHTypeDescriptions, Inserted I
  where  CFGPYWHTypeDescriptions.Type = @Type and CFGPYWHTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGPYWHTypeDescriptions
      (Type,
       UICultureName,
       Label)
  Select Type, dbo.FW_GetActiveCultureName(), Label From Inserted 

Else
  Update CFGPYWHTypeDescriptions
      Set Type = i.Type,
          Label = i.Label
  From inserted i
  Inner Join CFGPYWHTypeDescriptions on CFGPYWHTypeDescriptions.Type = @Type and CFGPYWHTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
