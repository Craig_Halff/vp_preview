SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGCampaignAction]
as
SELECT dbo.CFGCampaignActionData.Code, dbo.CFGCampaignActionDescriptions.Description, dbo.CFGCampaignActionDescriptions.Seq
  FROM dbo.CFGCampaignActionData Left JOIN
       dbo.CFGCampaignActionDescriptions ON dbo.CFGCampaignActionData.Code = dbo.CFGCampaignActionDescriptions.Code
   and dbo.CFGCampaignActionDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGCampaignActionTrigger] 
   ON  [dbo].[CFGCampaignAction]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGCampaignActionDescriptions where code = @Code
Delete from CFGCampaignActionData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGCampaignActionTrigger] 
   ON  [dbo].[CFGCampaignAction]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGCampaignActionData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGCampaignActionDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGCampaignActionTrigger] 
   ON  [dbo].[CFGCampaignAction]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGCampaignActionData
  set code = i.code
from inserted i
  inner join CFGCampaignActionData on CFGCampaignActionData.code = @Code

if (not exists(Select CFGCampaignActionDescriptions.UICultureName from CFGCampaignActionDescriptions
  where CFGCampaignActionDescriptions.code = @code and CFGCampaignActionDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGCampaignActionDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGCampaignActionDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGCampaignActionDescriptions on CFGCampaignActionDescriptions.code = @code and CFGCampaignActionDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
