SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE View [dbo].[AllActivities]
AS
SELECT 1 Row#, ActivityID, ClientID, ContactID, WBS1, WBS2, WBS3, Employee, LeadID, CampaignID, Type, Subject, 
	StartDate, EndDate , StartTime, EndTime, Duration, Location, ReminderInd, 
	ReminderUnit, ReminderMinHrDay, ReminderDate, ReminderTime, Priority, Notes, 
	ShowTimeAs, AllDayEventInd, CompletionInd, RecurrenceInd, PrivateInd, TaskStatus, 
	TaskCompletionDate, OpportunityID, CampaignCode, RecurrType, RecurrDailyFreq, 
	RecurrDailyWeekDay, RecurrWeeklyFreq, RecurrWeeklySun, RecurrWeeklyMon, 
	RecurrWeeklyTue, RecurrWeeklyWed, RecurrWeeklyThu, RecurrWeeklyFri, RecurrWeeklySat, 
	RecurrMonthlyFreq, RecurrMonthlyDay, RecurrMonthlyOccurInd, RecurrMonthlyOccur, 
	RecurrMonthlyOccurDay, RecurrMonthlyOccurFreq, RecurrYearlyMonth, RecurrYearlyDay, 
	RecurrStartDate, RecurrEndType, RecurrEndDate, RP, EmailAlertSent, RecurrID, Vendor, 
	ContactIDForVendor, CreateUser, CreateDate, ModUser, ModDate, MaxOccurences, OccurrenceDate , 
	(CASE WHEN RecurrID IS NOT NULL Then 'M' ELSE '' END) ExceptionType , IsNull(timezoneoffset,0) timezoneoffset,
	AssignmentID,ConversationID,RecurrYearlyOccurInd,RecurrYearlyOccur,RecurrYearlyOccurDay,RecurrYearlyOccurMonth ,
	PIMInd,KonaTask
  FROM Activity WITH (NOLOCK)
  WHERE  (RecurrenceInd = 'N' and ActivityID NOT in (select ActivityID 
             from RecurrActivityException r 
             where r.ActivityID = Activity.ActivityID and datediff(second,r.StartDate,Activity.StartDate) = 0))


UNION all
SELECT * FROM (
SELECT ROW_NUMBER() OVER(PARTITION BY ActivityID ORDER BY ActivityID
, UTCStartDAte ASC) AS Row#
, ActivityID, ClientID, ContactID, WBS1, WBS2, WBS3, Employee, LeadID
, CampaignID, Type, Subject, 
	UTCStartDate AS StartDate,
	dateadd(SECOND,datediff(second,startdate,enddate),UTCStartDate) EndDate,
	StartTime, EndTime, Duration, Location, ReminderInd, ReminderUnit, ReminderMinHrDay, 
	Case ReminderMinHrDay When 'Minutes' Then
		dateadd(mi, (cast(ReminderUnit As int) * -1),UTCStartDate)
	When 'Hours' Then 
		dateadd(hh, (cast(ReminderUnit As int) * -1),UTCStartDate)
	When 'Days' Then 
		dateadd(d, (cast(ReminderUnit As int) * -1),UTCStartDate)
	End AS ReminderDate,
	ReminderTime, 
	Priority, Notes, ShowTimeAs, AllDayEventInd, CompletionInd, RecurrenceInd, 
	PrivateInd, TaskStatus, TaskCompletionDate, OpportunityID, CampaignCode, RecurrType, 
	RecurrDailyFreq, RecurrDailyWeekDay, RecurrWeeklyFreq, RecurrWeeklySun, 
	RecurrWeeklyMon, RecurrWeeklyTue, RecurrWeeklyWed, RecurrWeeklyThu, RecurrWeeklyFri, 
	RecurrWeeklySat, RecurrMonthlyFreq, RecurrMonthlyDay, RecurrMonthlyOccurInd, 
	RecurrMonthlyOccur, RecurrMonthlyOccurDay, RecurrMonthlyOccurFreq, RecurrYearlyMonth, 
	RecurrYearlyDay, RecurrStartDate, RecurrEndType, RecurrEndDate, RP, EmailAlertSent, 
	RecurrID, Vendor, ContactIDForVendor, CreateUser, CreateDate, ModUser, ModDate, MaxOccurences , OccurrenceDate,
	(CASE WHEN RecurrID IS NOT NULL Then 'M' ELSE '' END) ExceptionType , IsNull(timezoneoffset,0) timezoneoffset,
	AssignmentID,ConversationID,RecurrYearlyOccurInd,RecurrYearlyOccur,RecurrYearlyOccurDay,RecurrYearlyOccurMonth ,
	PIMInd,KonaTask
   FROM (
		  SELECT Activity.*
		  ,DAYS.Day
		  ,dateadd(d, datediff(d, dateadd(second,datediff(second,getutcdate(),getdate()),startdate), days.day), startdate) UTCStartDate
		  ,dateadd(second,IsNull(timezoneoffset,0),dateadd(d, datediff(d, dateadd(second,datediff(second,getutcdate(),getdate()),startdate), days.day), startdate)) TimezoneStartDate
		  FROM Activity WITH (NOLOCK), DAYS
		  WHERE RecurrenceInd = 'Y' AND datediff(d,dateadd(hh,datediff(hh,getutcdate(),getdate()),activity.startdate),days.day) >= 0
		  AND Activity.ActivityID not in 
			(	
				SELECT ACTIVITYID FROM RECURRACTIVITYEXCEPTION R 
				WHERE R.ACTIVITYID = ACTIVITY.ACTIVITYID
			   AND DATEDIFF(second,dateadd(d, datediff(d, dateadd(second,datediff(second,getutcdate(),getdate()),startdate), days.day), startdate),R.StartDate)=0
		   )
  ) ActivityDays
   WHERE 

 ((((RecurrEndType = 'Y' AND RecurrEndDate is NOT null AND (datediff(d,TimezoneStartDate,RecurrEndDate) >= 0))) OR RecurrEndType = 'N' OR RecurrEndType = 'A'))
 AND ((RecurrType = 'Weekly' 
   AND (datepart(WK,StartDate) % (CASE RecurrWeeklyFreq WHEN 0 THEN 1 Else RecurrWeeklyFreq END)) =(datepart(WK,TimezoneStartDate) % (CASE RecurrWeeklyFreq WHEN 0 THEN 1 Else RecurrWeeklyFreq END))
	AND ((datepart(dw, TimezoneStartDate) = 1 AND RecurrWeeklySun = 'Y') OR
             (datepart(dw, TimezoneStartDate) = 2 AND RecurrWeeklyMon = 'Y') OR
             (datepart(dw, TimezoneStartDate) = 3 AND RecurrWeeklyTue = 'Y') OR
             (datepart(dw, TimezoneStartDate) = 4 AND RecurrWeeklyWed = 'Y') OR
             (datepart(dw, TimezoneStartDate) = 5 AND RecurrWeeklyThu = 'Y') OR
             (datepart(dw, TimezoneStartDate) = 6 AND RecurrWeeklyFri = 'Y') OR
             (datepart(dw, TimezoneStartDate) = 7 AND RecurrWeeklySat = 'Y'))) OR
         (RecurrType = 'Daily' AND ((RecurrDailyWeekDay = 'N' AND case when RecurrDailyFreq = 0 then 1 else datediff(d, dateadd(second,datediff(second,getutcdate(),getdate()),startdate), Day) % RecurrDailyFreq end = 0)
		 OR (RecurrDailyWeekDay = 'Y' AND datepart(dw, TimezoneStartDate) BETWEEN 2 AND 6))) 
		 OR (RecurrType = 'Monthly' AND ((RecurrMonthlyOccurInd = 'N' AND (case when RecurrMonthlyFreq = 0 then 1 else datediff(m, dateadd(second,datediff(SECOND,getutcdate(),getdate()),startdate), Day) % RecurrMonthlyFreq end = 0 AND day(TimezoneStartDate) = RecurrMonthlyDay AND day(TimezoneStartDate) = RecurrMonthlyDay)) 
		 OR (RecurrMonthlyOccurInd = 'Y'   AND	(case when RecurrMonthlyOccurFreq = 0 then 1 else datediff(m,dateadd(SECOND,datediff(SECOND,getutcdate(),getdate()),startdate),day) % RecurrMonthlyOccurFreq END) = 0 AND ((RecurrMonthlyOccur < 5 AND ((RecurrMonthlyOccurDay < 7 AND RecurrMonthlyOccurDay + 1 =  datepart(dw,TimezoneStartDate) AND (Day(TimezoneStartDate) - 1)/7 = RecurrMonthlyOccur + (CASE WHEN RecurrMonthlyOccur > 1 THEN -1 ELSE 0 END)) OR (RecurrMonthlyOccurDay = 7 AND DAY(TimezoneStartDate) = DAY(DateAdd(dd, (RecurrMonthlyOccur + (CASE WHEN RecurrMonthlyOccur > 1 THEN 0 ELSE 1 END)) ,EOMONTH(TimezoneStartDate,-1)))) 
		 OR (RecurrMonthlyOccurDay = 8 AND DAY(TimezoneStartDate)=DAY(DATEADD(DAY,(CASE WHEN datepart(dw,DateAdd(dd,1,EOMONTH(TimezoneStartDate,-1))) <> 7 THEN ((((RecurrMonthlyOccur + (CASE WHEN RecurrMonthlyOccur > 1 THEN 0 ELSE 1 END)) -1) * 7) + (7 - datepart(dw,DateAdd(dd,1,EOMONTH(TimezoneStartDate,-1))))) ELSE (((RecurrMonthlyOccur + (CASE WHEN RecurrMonthlyOccur > 1 THEN 0 ELSE 1 END)) -1 ) * 7) END),DateAdd(dd,1,EOMONTH(TimezoneStartDate,-1))))) OR (RecurrMonthlyOccurDay = 9 AND DAY(TimezoneStartDate)=DAY(IIF((RecurrMonthlyOccur + (CASE WHEN RecurrMonthlyOccur > 1 THEN 0 ELSE 1 END))= 1 , DATEADD(DAY,(CASE datepart(dw,DateAdd(dd,1,EOMONTH(TimezoneStartDate,-1))) WHEN 1 THEN 1 WHEN 7 THEN 2 ELSE 0 END) ,  DateAdd(dd,1,EOMONTH(TimezoneStartDate,-1))), DATEADD(DAY,(((((RecurrMonthlyOccur + (CASE WHEN RecurrMonthlyOccur > 1 THEN 0 ELSE 1 END)) -1) * 7) - (datepart(dw,DateAdd(dd,1,EOMONTH(TimezoneStartDate,-1))) -1)) + 1) ,DateAdd(dd,1,EOMONTH(TimezoneStartDate,-1))))))))
		 OR (RecurrMonthlyOccur = 5 AND ((RecurrMonthlyOccurDay < 7 AND DAY(TimezoneStartDate) = (CASE WHEN DATEDIFF(dd, dateadd(day,(RecurrMonthlyOccurDay + 1)-datepart(dw, TimezoneStartDate), TimezoneStartDate), EOMONTH(TimezoneStartDate,0)) < 7 then day(dateadd(day,(RecurrMonthlyOccurDay + 1)-datepart(dw, TimezoneStartDate), TimezoneStartDate)) else 0 end))	OR (RecurrMonthlyOccurDay = 7 AND DAY(TimezoneStartDate) = Day(EOMONTH(TimezoneStartDate,0))) OR (RecurrMonthlyOccurDay = 8 AND DAY(TimezoneStartDate)=DAY(DATEADD(DAY,(CASE WHEN  DATEPART(DW,EOMONTH(TimezoneStartDate,0)) <> 7 THEN  (DATEPART(DW,EOMONTH(TimezoneStartDate,0)) * -1) ELSE 0 END), EOMONTH(TimezoneStartDate,0)))) OR (RecurrMonthlyOccurDay = 9 AND DAY(TimezoneStartDate)=DAY(DATEADD(DAY,(CASE DATEPART(DW,EOMONTH(TimezoneStartDate,0)) WHEN 1 THEN -2 WHEN 7 THEN -1 ELSE 0 END),EOMONTH(TimezoneStartDate,0))))))))))
		 OR (RecurrType = 'Yearly' AND ((RecurrYearlyOccurInd = 'N' AND  month(TimezoneStartDate) = RecurrYearlyMonth AND day(TimezoneStartDate) = RecurrYearlyDay) OR (RecurrYearlyOccurInd = 'Y' and month(TimezoneStartDate) = RecurrYearlyOccurMonth AND ((RecurrYearlyOccur < 5 AND ((RecurrYearlyOccurDay < 7 AND RecurrYearlyOccurDay + 1 =  datepart(dw,TimezoneStartDate) AND (Day(TimezoneStartDate) - 1)/7 = RecurrYearlyOccur + (CASE WHEN RecurrYearlyOccur > 1 THEN -1 ELSE 0 END)) OR (RecurrYearlyOccurDay = 7 AND DAY(TimezoneStartDate) =DAY(DateAdd(dd, (RecurrYearlyOccur + (CASE WHEN RecurrYearlyOccur > 1 THEN 0 ELSE 1 END)) ,DATEADD(yy, DATEDIFF(yy, 0, TimezoneStartDate) + 1, -1))))
		 OR(RecurrYearlyOccurDay = 8 AND  DAY(TimezoneStartDate)=DAY(DATEADD(DAY,(CASE WHEN datepart(dw,DateAdd(dd,1,EOMONTH(TimezoneStartDate,-1))) <> 7 THEN ((((RecurrYearlyOccur + (CASE WHEN RecurrYearlyOccur > 1 THEN 0 ELSE 1 END)) -1) * 7) + (7 - datepart(dw,DateAdd(dd,1,EOMONTH(TimezoneStartDate,-1))))) ELSE (((RecurrYearlyOccur + (CASE WHEN RecurrYearlyOccur > 1 THEN 0 ELSE 1 END)) -1 ) * 7) END),DateAdd(dd,1,EOMONTH(TimezoneStartDate,-1))))) OR (RecurrYearlyOccurDay = 9 AND DAY(TimezoneStartDate)=DAY(IIF((RecurrYearlyOccur + (CASE WHEN RecurrYearlyOccur > 1 THEN 0 ELSE 1 END))= 1 , DATEADD(DAY,(CASE datepart(dw,DateAdd(dd,1,EOMONTH(TimezoneStartDate,-1))) WHEN 1 THEN 1 WHEN 7 THEN 2 ELSE 0 END) ,  DateAdd(dd,1,EOMONTH(TimezoneStartDate,-1))), DATEADD(DAY,(((((RecurrYearlyOccur + (CASE WHEN RecurrYearlyOccur > 1 THEN 0 ELSE 1 END)) -1) * 7) - (datepart(dw,DateAdd(dd,1,EOMONTH(TimezoneStartDate,-1))) -1)) + 1) ,DateAdd(dd,1,EOMONTH(TimezoneStartDate,-1))))))))
		 OR (RecurrYearlyOccur = 5 AND  ((RecurrYearlyOccurDay < 7 AND DAY(TimezoneStartDate) = (CASE WHEN DATEDIFF(dd, dateadd(day,(RecurrYearlyOccurDay + 1)-datepart(dw, TimezoneStartDate), TimezoneStartDate), EOMONTH(TimezoneStartDate,0)) < 7 then day(dateadd(day,(RecurrYearlyOccurDay + 1)-datepart(dw, TimezoneStartDate), TimezoneStartDate)) else 0 end))	OR (RecurrYearlyOccurDay = 7 AND  DAY(TimezoneStartDate) = DAY(EOMONTH(TimezoneStartDate,0))) OR (RecurrYearlyOccurDay = 8 AND DAY(TimezoneStartDate)=DAY(DATEADD(DAY,(CASE WHEN  DATEPART(DW,EOMONTH(TimezoneStartDate,0)) <> 7 THEN  (DATEPART(DW,EOMONTH(TimezoneStartDate,0)) * -1) ELSE 0 END), EOMONTH(TimezoneStartDate,0)))) OR (RecurrYearlyOccurDay = 9 AND DAY(TimezoneStartDate)=DAY(DATEADD(DAY,(CASE DATEPART(DW,EOMONTH(TimezoneStartDate,0)) WHEN 1 THEN -2 WHEN 7 THEN -1 ELSE 0 END),EOMONTH(TimezoneStartDate,0)))) )))) ))
	)
) Qry
WHERE	(RecurrEndType = 'A' AND  Row# <= (MaxOccurences - (Select count(1) from Activity Child Where Child.RecurrID=Qry.ActivityID)))
		OR RecurrEndType = 'N'
		OR RecurrEndType = 'Y'
GO
