SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGAwardType]
as
SELECT dbo.CFGAwardTypeData.Code, dbo.CFGAwardTypeDescriptions.Description, dbo.CFGAwardTypeDescriptions.Seq
FROM         dbo.CFGAwardTypeData Left JOIN
dbo.CFGAwardTypeDescriptions ON dbo.CFGAwardTypeData.Code = dbo.CFGAwardTypeDescriptions.Code 
and dbo.CFGAwardTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGAwardTypeTrigger] 
   ON  [dbo].[CFGAwardType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGAwardTypeDescriptions where code = @Code
Delete from CFGAwardTypeData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGAwardTypeTrigger] 
   ON  [dbo].[CFGAwardType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO CFGAwardTypeData
  (Code)
Select Code from inserted

INSERT INTO CFGAwardTypeDescriptions
  (Code 
  ,Description
  ,UICultureName
  ,Seq)
Select Code,Description, dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGAwardTypeTrigger] 
   ON  [dbo].[CFGAwardType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGAwardTypeData
  set code = i.code
from inserted i
  inner join CFGAwardTypeData on CFGAwardTypeData.code = @Code

if (not exists(Select CFGAwardTypeDescriptions.UICultureName from CFGAwardTypeDescriptions
  where CFGAwardTypeDescriptions.code = @code and CFGAwardTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGAwardTypeDescriptions
      (Code
      ,UICultureName
      ,Description
      ,Seq)
  Select Code, dbo.FW_GetActiveCultureName(),Description,Seq From Inserted 

Else
  Update CFGAwardTypeDescriptions
      Set code = i.code
      , Description = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGAwardTypeDescriptions on CFGAwardTypeDescriptions.code = @code and CFGAwardTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
