SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGClientCurrentStatus]
as
SELECT dbo.CFGClientCurrentStatusData.Code, dbo.CFGClientCurrentStatusDescriptions.CurrentStatus, dbo.CFGClientCurrentStatusDescriptions.Seq
FROM         dbo.CFGClientCurrentStatusData Left JOIN
dbo.CFGClientCurrentStatusDescriptions ON dbo.CFGClientCurrentStatusData.Code = dbo.CFGClientCurrentStatusDescriptions.Code 
and dbo.CFGClientCurrentStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGClientCurrentStatusTrigger] 
   ON  [dbo].[CFGClientCurrentStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(50)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGClientCurrentStatusDescriptions where code = @Code
Delete from CFGClientCurrentStatusData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGClientCurrentStatusTrigger] 
   ON  [dbo].[CFGClientCurrentStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGClientCurrentStatusData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGClientCurrentStatusDescriptions]
  ([Code] 
  ,CurrentStatus
  ,UICultureName
  ,Seq)
Select [Code],[CurrentStatus], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGClientCurrentStatusTrigger] 
   ON  [dbo].[CFGClientCurrentStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(50)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGClientCurrentStatusData
  set code = i.code
from inserted i
  inner join CFGClientCurrentStatusData on CFGClientCurrentStatusData.code = @Code

if (not exists(Select CFGClientCurrentStatusDescriptions.UICultureName from CFGClientCurrentStatusDescriptions
  where CFGClientCurrentStatusDescriptions.code = @code and CFGClientCurrentStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGClientCurrentStatusDescriptions
      ([Code]
      ,[UICultureName]
      ,[CurrentStatus]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[CurrentStatus],[Seq] From Inserted 

Else
  Update CFGClientCurrentStatusDescriptions
      Set code = i.code
      , [CurrentStatus] = i.CurrentStatus
      , seq = i.seq
  From inserted i
  Inner Join CFGClientCurrentStatusDescriptions on CFGClientCurrentStatusDescriptions.code = @code and CFGClientCurrentStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
