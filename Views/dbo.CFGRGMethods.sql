SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGRGMethods]
as
SELECT dbo.CFGRGMethodsData.Method, dbo.CFGRGMethodsData.Active, dbo.CFGRGMethodsData.Category, dbo.CFGRGMethodsDescriptions.Description 
FROM         dbo.CFGRGMethodsData Left JOIN
dbo.CFGRGMethodsDescriptions ON dbo.CFGRGMethodsData.Method = dbo.CFGRGMethodsDescriptions.Method 
and dbo.CFGRGMethodsDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGRGMethodsTrigger] 
   ON  [dbo].[CFGRGMethods]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Method Nvarchar(10)

SELECT top 1 @Method = Method FROM Deleted

Delete from CFGRGMethodsDescriptions where Method = @Method
Delete from CFGRGMethodsData where Method = @Method

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGRGMethodsTrigger] 
   ON  [dbo].[CFGRGMethods]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGRGMethodsData]
  ([Method],Category)
Select [Method], isnull(Category,0) from inserted

INSERT INTO [CFGRGMethodsDescriptions]
  ([Method] 
  ,Description
  ,UICultureName)
Select [Method],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGRGMethodsTrigger] 
   ON  [dbo].[CFGRGMethods]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Method Nvarchar(10)

if update(Method)
  select @Method=Method from deleted
else
  select @Method=Method from inserted

Update CFGRGMethodsData
  set Method = i.Method, Active = i.Active, Category = i.Category
from inserted i
  inner join CFGRGMethodsData on CFGRGMethodsData.Method = @Method

if (not exists(Select CFGRGMethodsDescriptions.UICultureName from CFGRGMethodsDescriptions
  where CFGRGMethodsDescriptions.Method = @Method and CFGRGMethodsDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGRGMethodsDescriptions
      ([Method]
      ,[UICultureName]
      ,[Description])
  Select [Method], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGRGMethodsDescriptions
      Set Method = i.Method
      , [Description] = i.Description
  From inserted i
  Inner Join CFGRGMethodsDescriptions on CFGRGMethodsDescriptions.Method = @Method and CFGRGMethodsDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
