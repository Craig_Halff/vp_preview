SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGBillingRateTableCode]
as
SELECT dbo.CFGBillingRateTableCodeData.Code, dbo.CFGBillingRateTableCodeDescriptions.Description, dbo.CFGBillingRateTableCodeDescriptions.Seq
FROM         dbo.CFGBillingRateTableCodeData Left JOIN
dbo.CFGBillingRateTableCodeDescriptions ON dbo.CFGBillingRateTableCodeData.Code = dbo.CFGBillingRateTableCodeDescriptions.Code 
and dbo.CFGBillingRateTableCodeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGBillingRateTableCodeTrigger] 
   ON  [dbo].[CFGBillingRateTableCode]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGBillingRateTableCodeDescriptions where code = @Code
Delete from CFGBillingRateTableCodeData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGBillingRateTableCodeTrigger] 
   ON  [dbo].[CFGBillingRateTableCode]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGBillingRateTableCodeData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGBillingRateTableCodeDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGBillingRateTableCodeTrigger] 
   ON  [dbo].[CFGBillingRateTableCode]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGBillingRateTableCodeData
  set code = i.code
from inserted i
  inner join CFGBillingRateTableCodeData on CFGBillingRateTableCodeData.code = @Code

if (not exists(Select CFGBillingRateTableCodeDescriptions.UICultureName from CFGBillingRateTableCodeDescriptions
  where CFGBillingRateTableCodeDescriptions.code = @code and CFGBillingRateTableCodeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGBillingRateTableCodeDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGBillingRateTableCodeDescriptions
      Set code = i.code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGBillingRateTableCodeDescriptions on CFGBillingRateTableCodeDescriptions.code = @code and CFGBillingRateTableCodeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
