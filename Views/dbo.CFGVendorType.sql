SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGVendorType]
as
SELECT dbo.CFGVendorTypeData.type, dbo.CFGVendorTypeDescriptions.label 
FROM   dbo.CFGVendorTypeData Left JOIN
dbo.CFGVendorTypeDescriptions ON dbo.CFGVendorTypeData.type = dbo.CFGVendorTypeDescriptions.type 
and dbo.CFGVendorTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGVendorTypeTrigger] 
   ON  [dbo].[CFGVendorType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @type VARCHAR(1)

SELECT top 1 @type = type FROM Deleted

Delete from CFGVendorTypeDescriptions where type = @type
Delete from CFGVendorTypeData where type = @type

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGVendorTypeTrigger] 
   ON  [dbo].[CFGVendorType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGVendorTypeData] (type)
Select type from inserted

INSERT INTO [CFGVendorTypeDescriptions]
  ([type] 
  ,label
  ,UICultureName)
Select [type],[label], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGVendorTypeTrigger] 
   ON  [dbo].[CFGVendorType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @type VARCHAR(1)

if update(type)
  select @type=type from deleted
else
  select @type=type from inserted


Update CFGVendorTypeData
  set type = i.type
from inserted i
  inner join CFGVendorTypeData on CFGVendorTypeData.type = @type

if (not exists(Select CFGVendorTypeDescriptions.UICultureName from CFGVendorTypeDescriptions
  where CFGVendorTypeDescriptions.type = @type and CFGVendorTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGVendorTypeDescriptions
      ([type]
      ,[UICultureName]
      ,[label])
  Select [type], dbo.FW_GetActiveCultureName(),[label] From Inserted 

Else
  Update CFGVendorTypeDescriptions
      Set type = i.type
      , [label] = i.label
  From inserted i
  Inner Join CFGVendorTypeDescriptions on CFGVendorTypeDescriptions.type = @type and CFGVendorTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
