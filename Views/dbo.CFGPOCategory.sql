SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGPOCategory]
as
SELECT dbo.CFGPOCategoryData.Category, dbo.CFGPOCategoryDescriptions.Description, dbo.CFGPOCategoryData.Type
FROM         dbo.CFGPOCategoryData Left JOIN
dbo.CFGPOCategoryDescriptions ON dbo.CFGPOCategoryData.Category = dbo.CFGPOCategoryDescriptions.Category 
and dbo.CFGPOCategoryDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGPOCategoryTrigger] 
   ON  [dbo].[CFGPOCategory]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Category Nvarchar(20) 

SELECT top 1 @Category = Category FROM Deleted

Delete from CFGPOCategoryDescriptions where Category = @Category
Delete from CFGPOCategoryData where Category = @Category

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGPOCategoryTrigger] 
   ON  [dbo].[CFGPOCategory]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGPOCategoryData]
  ([Category], type)
Select [Category], isnull(type, 'M') from inserted

INSERT INTO [CFGPOCategoryDescriptions]
  ([Category] 
  ,Description
  ,UICultureName)
Select [Category],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGPOCategoryTrigger] 
   ON  [dbo].[CFGPOCategory]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Category VARCHAR(20)

if update(Category)
  select @Category=Category from deleted
else
  select @Category=Category from inserted

Update CFGPOCategoryData
  set Category = i.Category, type = i.type
from inserted i
  inner join CFGPOCategoryData on CFGPOCategoryData.Category = @Category

if (not exists(Select CFGPOCategoryDescriptions.UICultureName from CFGPOCategoryDescriptions
  where CFGPOCategoryDescriptions.Category = @Category and CFGPOCategoryDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGPOCategoryDescriptions
      ([Category]
      ,[UICultureName]
      ,[Description])
  Select [Category], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGPOCategoryDescriptions
      Set Category = i.Category
      , [Description] = i.Description
  From inserted i
  Inner Join CFGPOCategoryDescriptions on CFGPOCategoryDescriptions.Category = @Category and CFGPOCategoryDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
