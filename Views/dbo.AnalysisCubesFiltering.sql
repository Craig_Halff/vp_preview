SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[AnalysisCubesFiltering]
as
SELECT	dbo.AnalysisCubesFilteringData.PKey,
	dbo.AnalysisCubesFilteringData.Type,
	dbo.AnalysisCubesFilteringData.Cubes,
	dbo.AnalysisCubesFilteringData.Include,
	dbo.AnalysisCubesFilteringData.GroupName,
	dbo.AnalysisCubesFilteringDescriptions.GroupNameDescription,
	dbo.AnalysisCubesFilteringData.FolderName,
	dbo.AnalysisCubesFilteringDescriptions.FolderNameDescription,
	dbo.AnalysisCubesFilteringData.CombinedID,
	dbo.AnalysisCubesFilteringData.DefaultFieldName,
	dbo.AnalysisCubesFilteringDescriptions.Label,
	dbo.AnalysisCubesFilteringData.Subtype
FROM   dbo.AnalysisCubesFilteringData Left JOIN
dbo.AnalysisCubesFilteringDescriptions ON dbo.AnalysisCubesFilteringData.PKey = dbo.AnalysisCubesFilteringDescriptions.PKey 
and dbo.AnalysisCubesFilteringDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteAnalysisCubesFilteringTrigger] 
   ON  [dbo].[AnalysisCubesFiltering]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @PKey Nvarchar(32)

SELECT top 1 @PKey = PKey FROM Deleted

Delete from AnalysisCubesFilteringDescriptions where PKey = @PKey
Delete from AnalysisCubesFilteringData where PKey = @PKey

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertAnalysisCubesFilteringTrigger] 
   ON  [dbo].[AnalysisCubesFiltering]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [AnalysisCubesFilteringData]
      ( PKey,
	Type,
	Cubes,
	Include,
	GroupName,
	FolderName,
	CombinedID,
	DefaultFieldName,
	Subtype)
Select  PKey,
	isnull(Type,'D'),
	Cubes,
	isnull(Include,'N'),
	GroupName,
	FolderName,
	CombinedID,
	DefaultFieldName,
	isnull(Subtype,'S')
from inserted

INSERT INTO [AnalysisCubesFilteringDescriptions]
  (PKey, 
	UICultureName,
	GroupNameDescription,
	FolderNameDescription,
	Label
  )
Select PKey, dbo.FW_GetActiveCultureName(), GroupNameDescription, FolderNameDescription, [Label] from inserted
End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateAnalysisCubesFilteringTrigger] 
   ON  [dbo].[AnalysisCubesFiltering]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @PKey Nvarchar(32)

if update(PKey)
  select @PKey=PKey from deleted
else
  select @PKey=PKey from inserted

Update AnalysisCubesFilteringData
  set 	PKey = i.PKey,
	Type = i.Type, 
	Cubes = i.Cubes,
	Include = i.Include,
	GroupName = i.GroupName,
	FolderName = i.FolderName,
	CombinedID = i.CombinedID,
	DefaultFieldName = i.DefaultFieldName,
	Subtype = i.Subtype
from inserted i
  inner join AnalysisCubesFilteringData on AnalysisCubesFilteringData.PKey = @PKey

if (not exists(Select AnalysisCubesFilteringDescriptions.UICultureName from AnalysisCubesFilteringDescriptions
  where AnalysisCubesFilteringDescriptions.PKey = @PKey and AnalysisCubesFilteringDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into AnalysisCubesFilteringDescriptions
      ([PKey],
		[UICultureName],
		[GroupNameDescription],
		[FolderNameDescription],
		[Label]
      )
  Select [PKey], dbo.FW_GetActiveCultureName(), [GroupNameDescription], [FolderNameDescription], [Label] From Inserted 

Else
  Update AnalysisCubesFilteringDescriptions
      Set PKey = i.PKey
      , [GroupNameDescription] = i.[GroupNameDescription]
      , [FolderNameDescription] = i.[FolderNameDescription]
      , [Label] = i.Label
  From inserted i
  Inner Join AnalysisCubesFilteringDescriptions on AnalysisCubesFilteringDescriptions.PKey = @PKey and AnalysisCubesFilteringDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
