SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[ContactStatus]
as
SELECT dbo.ContactStatusData.Code, dbo.ContactStatusDescriptions.Description 
FROM         dbo.ContactStatusData Left JOIN
dbo.ContactStatusDescriptions ON dbo.ContactStatusData.Code = dbo.ContactStatusDescriptions.Code 
and dbo.ContactStatusDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteContactStatusTrigger] 
   ON  [dbo].[ContactStatus]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from ContactStatusDescriptions where Code = @Code
Delete from ContactStatusData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertContactStatusTrigger] 
   ON  [dbo].[ContactStatus]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [ContactStatusData]
  ([Code])
Select [Code] from inserted

INSERT INTO [ContactStatusDescriptions]
  ([Code] 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateContactStatusTrigger] 
   ON  [dbo].[ContactStatus]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update ContactStatusData
  set Code = i.Code
from inserted i
  inner join ContactStatusData on ContactStatusData.Code = @Code

if (not exists(Select ContactStatusDescriptions.UICultureName from ContactStatusDescriptions
  where ContactStatusDescriptions.Code = @Code and ContactStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into ContactStatusDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update ContactStatusDescriptions
      Set Code = i.Code
      , [Description] = i.Description
  From inserted i
  Inner Join ContactStatusDescriptions on ContactStatusDescriptions.Code = @Code and ContactStatusDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
