SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGPOReason]
as
SELECT dbo.CFGPOReasonData.Code, dbo.CFGPOReasonDescriptions.Description 
FROM         dbo.CFGPOReasonData Left JOIN
dbo.CFGPOReasonDescriptions ON dbo.CFGPOReasonData.Code = dbo.CFGPOReasonDescriptions.Code 
and dbo.CFGPOReasonDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGPOReasonTrigger] 
   ON  [dbo].[CFGPOReason]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(20)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGPOReasonDescriptions where Code = @Code
Delete from CFGPOReasonData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGPOReasonTrigger] 
   ON  [dbo].[CFGPOReason]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGPOReasonData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGPOReasonDescriptions]
  ([Code] 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGPOReasonTrigger] 
   ON  [dbo].[CFGPOReason]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(20)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGPOReasonData
  set Code = i.Code
from inserted i
  inner join CFGPOReasonData on CFGPOReasonData.Code = @Code

if (not exists(Select CFGPOReasonDescriptions.UICultureName from CFGPOReasonDescriptions
  where CFGPOReasonDescriptions.Code = @Code and CFGPOReasonDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGPOReasonDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGPOReasonDescriptions
      Set Code = i.Code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGPOReasonDescriptions on CFGPOReasonDescriptions.Code = @Code and CFGPOReasonDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
