SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGChargeType]
as
SELECT dbo.CFGChargeTypeData.Type,  dbo.CFGChargeTypeDescriptions.Label 
FROM         dbo.CFGChargeTypeData Left JOIN
dbo.CFGChargeTypeDescriptions ON dbo.CFGChargeTypeData.Type = dbo.CFGChargeTypeDescriptions.Type 
and dbo.CFGChargeTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGChargeTypeTrigger] 
   ON  [dbo].[CFGChargeType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Type VARCHAR(1)

SELECT top 1 @Type = Type FROM Deleted

Delete from CFGChargeTypeDescriptions where Type = @Type
Delete from CFGChargeTypeData where Type = @Type

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGChargeTypeTrigger] 
   ON  [dbo].[CFGChargeType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGChargeTypeData]
  (Type)
Select Type from inserted

INSERT INTO [CFGChargeTypeDescriptions]
  ([Type] 
  ,Label
  ,UICultureName)
Select [Type],[Label], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGChargeTypeTrigger] 
   ON  [dbo].[CFGChargeType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Type VARCHAR(1)

if update(Type)
  select @Type=Type from deleted
else
  select @Type=Type from inserted

Update CFGChargeTypeData
  set Type = i.Type
from inserted i
  inner join CFGChargeTypeData on CFGChargeTypeData.Type = @Type

if (not exists(Select CFGChargeTypeDescriptions.UICultureName from CFGChargeTypeDescriptions
  where CFGChargeTypeDescriptions.Type = @Type and CFGChargeTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGChargeTypeDescriptions
      ([Type]
      ,[UICultureName]
      ,[Label])
  Select [Type], dbo.FW_GetActiveCultureName(),[Label] From Inserted 

Else
  Update CFGChargeTypeDescriptions
      Set Type = i.Type
      , [Label] = i.Label
  From inserted i
  Inner Join CFGChargeTypeDescriptions on CFGChargeTypeDescriptions.Type = @Type and CFGChargeTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
