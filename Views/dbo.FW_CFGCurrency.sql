SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FW_CFGCurrency]
AS
SELECT dbo.FW_CFGCurrencyData.Code, dbo.FW_CFGCurrencyData.DecimalPlaces, 
dbo.FW_CFGCurrencyData.CurrencySymbol,dbo.FW_CFGCurrencyDesc.Description,
dbo.FW_CFGCurrencyDesc.MajorUnitLabelPlural,  dbo.FW_CFGCurrencyDesc.MajorUnitLabelSingular,  
dbo.FW_CFGCurrencyDesc.MinorUnitLabelPlural,  dbo.FW_CFGCurrencyDesc.MinorUnitLabelSingular  
FROM        dbo.FW_CFGCurrencyData LEFT JOIN
dbo.FW_CFGCurrencyDesc ON dbo.FW_CFGCurrencyData.code = dbo.FW_CFGCurrencyDesc.code 
and dbo.FW_CFGCurrencyDesc.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DelFW_CFGCurrency] 
  ON  [dbo].[FW_CFGCurrency]
INSTEAD OF DELETE
AS 
BEGIN
     SET NOCOUNT ON;

DELETE FROM FW_CFGCurrencyDesc FROM deleted WHERE FW_CFGCurrencyDesc.Code = deleted.Code
DELETE FROM FW_CFGCurrencyData FROM deleted WHERE FW_CFGCurrencyData.Code = deleted.Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[InsFW_CFGCurrency] 
  ON  [dbo].[FW_CFGCurrency]
INSTEAD OF Insert
AS
BEGIN
     SET NOCOUNT ON;

INSERT INTO [FW_CFGCurrencyData]
  ([code],DecimalPlaces, CurrencySymbol)
SELECT [code], isnull(DecimalPlaces,0), CurrencySymbol FROM inserted

INSERT INTO [FW_CFGCurrencyDesc]
  ([code] 
  ,Description
 ,MajorUnitLabelPlural
 ,MajorUnitLabelSingular
 ,MinorUnitLabelPlural
 ,MinorUnitLabelSingular
 ,UICultureName)
SELECT [code],[Description]
 ,MajorUnitLabelPlural
 ,MajorUnitLabelSingular
 ,MinorUnitLabelPlural
 ,MinorUnitLabelSingular
 ,dbo.FW_GetActiveCultureName() FROM inserted
End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[UpdFW_CFGCurrency] 
  ON  [dbo].[FW_CFGCurrency]
INSTEAD OF UPDATE
AS
BEGIN
     SET NOCOUNT ON;

DECLARE @code VARCHAR(3)

IF (UPDATE(code))
  SELECT @code=code FROM deleted
ELSE
  SELECT @code=code FROM inserted

UPDATE FW_CFGCurrencyData
  SET code = i.code, DecimalPlaces = i.DecimalPlaces, CurrencySymbol = i.CurrencySymbol
FROM inserted i
  INNER JOIN FW_CFGCurrencyData ON FW_CFGCurrencyData.code = @code

IF (not exists(SELECT FW_CFGCurrencyDesc.UICultureName FROM FW_CFGCurrencyDesc
  WHERE FW_CFGCurrencyDesc.code = @code AND FW_CFGCurrencyDesc.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into FW_CFGCurrencyDesc
      ([code]
    ,[UICultureName]
    ,[Description]
	,MajorUnitLabelPlural
	,MajorUnitLabelSingular
	,MinorUnitLabelPlural
	,MinorUnitLabelSingular
)
  SELECT [code]
	,dbo.FW_GetActiveCultureName()
	,[Description]
	,MajorUnitLabelPlural
	,MajorUnitLabelSingular
	,MinorUnitLabelPlural
	,MinorUnitLabelSingular FROM Inserted 

ELSE
  UPDATE FW_CFGCurrencyDesc
      SET code = i.code
      , [Description] = i.Description
	,MajorUnitLabelPlural = i.MajorUnitLabelPlural
	,MajorUnitLabelSingular = i.MajorUnitLabelSingular
	,MinorUnitLabelPlural = i.MinorUnitLabelPlural
	,MinorUnitLabelSingular = i.MinorUnitLabelSingular
  FROM inserted i
  INNER JOIN FW_CFGCurrencyDesc ON FW_CFGCurrencyDesc.code = @code AND FW_CFGCurrencyDesc.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
