SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[FW_CFGAttachmentCategory]
AS
SELECT dbo.FW_CFGAttachmentCategoryData.Code, dbo.FW_CFGAttachmentCategoryDesc.Description, dbo.FW_CFGAttachmentCategoryDesc.Seq, dbo.FW_CFGAttachmentCategoryData.Application
FROM dbo.FW_CFGAttachmentCategoryData LEFT JOIN
dbo.FW_CFGAttachmentCategoryDesc ON dbo.FW_CFGAttachmentCategoryData.Code = dbo.FW_CFGAttachmentCategoryDesc.Code and dbo.FW_CFGAttachmentCategoryData.Application = dbo.FW_CFGAttachmentCategoryDesc.Application
and dbo.FW_CFGAttachmentCategoryDesc.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DelFW_CFGAttachmentCategory] 
ON [dbo].[FW_CFGAttachmentCategory]
INSTEAD OF DELETE
AS 
BEGIN
    SET NOCOUNT ON;

DELETE FROM FW_CFGAttachmentCategoryDesc FROM deleted where FW_CFGAttachmentCategoryDesc.Code = deleted.Code and FW_CFGAttachmentCategoryDesc.Application = deleted.Application
DELETE FROM FW_CFGAttachmentCategoryData FROM deleted where FW_CFGAttachmentCategoryData.Code = deleted.Code and FW_CFGAttachmentCategoryData.Application = deleted.Application

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[InsFW_CFGAttachmentCategory]
  ON [dbo].[FW_CFGAttachmentCategory]
INSTEAD OF Insert
AS
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
SELECT @seq=isnull(seq,0) from inserted

INSERT INTO [FW_CFGAttachmentCategoryData]
([Code],[Application])
SELECT [Code],[Application] from inserted

INSERT INTO [FW_CFGAttachmentCategoryDesc]
([Code] 
,Description
,[Application]
,UICultureName
,Seq)
SELECT [Code],[Description], [Application], dbo.FW_GetActiveCultureName(),@seq from inserted

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdFW_CFGAttachmentCategory] 
  ON [dbo].[FW_CFGAttachmentCategory]
INSTEAD OF UPDATE
AS
BEGIN
     SET NOCOUNT ON;
DECLARE @Code nvarchar(15)
DECLARE @Application nvarchar(32)

IF (UPDATE(Code))
SELECT @Code=Code, @Application=[Application] FROM deleted
ELSE
SELECT @Code=Code, @Application=[Application] FROM inserted

UPDATE FW_CFGAttachmentCategoryData
SET Code = i.Code, Application = i.Application
FROM inserted i
INNER JOIN FW_CFGAttachmentCategoryData on FW_CFGAttachmentCategoryData.code = @Code and FW_CFGAttachmentCategoryData.Application = @Application

IF (not exists(SELECT FW_CFGAttachmentCategoryDesc.UICultureName FROM FW_CFGAttachmentCategoryDesc
WHERE FW_CFGAttachmentCategoryDesc.code = @Code and FW_CFGAttachmentCategoryDesc.Application = @Application and FW_CFGAttachmentCategoryDesc.UICultureName =dbo.FW_GetActiveCultureName()))
Insert Into FW_CFGAttachmentCategoryDesc
([Code]
,[UICultureName]
,[Application]
,[Description]
,[Seq])
SELECT [Code], dbo.FW_GetActiveCultureName(),[Application], [Description],isnull([Seq],0) FROM Inserted 
ELSE
UPDATE FW_CFGAttachmentCategoryDesc
SET Code = i.Code
, [Application] = i.Application
, [Description] = i.Description
, Seq = isnull(i.Seq,0)
FROM inserted i
INNER JOIN FW_CFGAttachmentCategoryDesc ON FW_CFGAttachmentCategoryDesc.code = @code and FW_CFGAttachmentCategoryDesc.Application = @Application and FW_CFGAttachmentCategoryDesc.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
