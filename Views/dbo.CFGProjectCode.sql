SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGProjectCode]
as
SELECT dbo.CFGProjectCodeData.Active, dbo.CFGProjectCodeData.ProjectCodeSF330, dbo.CFGProjectCodeData.ProjectCode, dbo.CFGProjectCodeDescriptions.Description, dbo.CFGProjectCodeDescriptions.Seq
FROM         dbo.CFGProjectCodeData Left JOIN
dbo.CFGProjectCodeDescriptions ON dbo.CFGProjectCodeData.ProjectCode = dbo.CFGProjectCodeDescriptions.ProjectCode
and dbo.CFGProjectCodeDescriptions.UICultureName = dbo.FW_GetActiveCultureName() 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGProjectCodeTrigger] 
   ON  [dbo].[CFGProjectCode]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = ProjectCode FROM Deleted

Delete from CFGProjectCodeDescriptions where ProjectCode = @Code
Delete from CFGProjectCodeData where ProjectCode= @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGProjectCodeTrigger] 
   ON  [dbo].[CFGProjectCode]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGProjectCodeData]
  ([ProjectCode],[Active],ProjectCodeSF330)
Select [ProjectCode],isnull(Active,'Y'),ProjectCodeSF330 from inserted

INSERT INTO [CFGProjectCodeDescriptions]
  ([ProjectCode] 
  ,Description
  ,UICultureName
  ,Seq)
Select [ProjectCode],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGProjectCodeTrigger] 
   ON  [dbo].[CFGProjectCode]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(ProjectCode)
  select @Code=ProjectCode from deleted
else
  select @Code=ProjectCode from inserted

Update CFGProjectCodeData
  set ProjectCode = i.ProjectCode, [Active]= i.active, ProjectCodeSF330=i.ProjectCodeSF330
from inserted i
  inner join CFGProjectCodeData on CFGProjectCodeData.ProjectCode = @Code

if (not exists(Select CFGProjectCodeDescriptions.UICultureName from CFGProjectCodeDescriptions
  where CFGProjectCodeDescriptions.ProjectCode = @code and CFGProjectCodeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGProjectCodeDescriptions
      ([ProjectCode]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [ProjectCode], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGProjectCodeDescriptions
      Set ProjectCode = i.ProjectCode
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGProjectCodeDescriptions on CFGProjectCodeDescriptions.ProjectCode = @code and CFGProjectCodeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
