SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGPrefix]
as
SELECT dbo.CFGPrefixData.Code, dbo.CFGPrefixDescriptions.Prefix, dbo.CFGPrefixDescriptions.Seq
FROM         dbo.CFGPrefixData Left JOIN
dbo.CFGPrefixDescriptions ON dbo.CFGPrefixData.Code = dbo.CFGPrefixDescriptions.Code 
and dbo.CFGPrefixDescriptions.UICultureName = dbo.FW_GetActiveCultureName() 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGPrefixTrigger] 
   ON  [dbo].[CFGPrefix]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGPrefixDescriptions where code = @Code
Delete from CFGPrefixData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGPrefixTrigger] 
   ON  [dbo].[CFGPrefix]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGPrefixData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGPrefixDescriptions]
  ([Code] 
  ,Prefix
  ,UICultureName
  ,Seq)
Select [Code],[Prefix], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGPrefixTrigger] 
   ON  [dbo].[CFGPrefix]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGPrefixData
  set code = i.code
from inserted i
  inner join CFGPrefixData on CFGPrefixData.code = @Code

if (not exists(Select CFGPrefixDescriptions.UICultureName from CFGPrefixDescriptions
  where CFGPrefixDescriptions.code = @code and CFGPrefixDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGPrefixDescriptions
      ([Code]
      ,[UICultureName]
      ,[Prefix]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Prefix],[Seq] From Inserted 

Else
  Update CFGPrefixDescriptions
      Set code = i.code
      , [Prefix] = i.Prefix
      , seq = i.seq
  From inserted i
  Inner Join CFGPrefixDescriptions on CFGPrefixDescriptions.code = @code and CFGPrefixDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
