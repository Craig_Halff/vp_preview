SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGActivityType]
as
SELECT dbo.CFGActivityTypeData.Code, dbo.CFGActivityTypeDescriptions.ActivityType, dbo.CFGActivityTypeDescriptions.Seq, dbo.CFGActivityTypeData.OutlookType
FROM         dbo.CFGActivityTypeData Left JOIN
dbo.CFGActivityTypeDescriptions ON dbo.CFGActivityTypeData.Code = dbo.CFGActivityTypeDescriptions.Code 
and dbo.CFGActivityTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGActivityTypeTrigger] 
   ON  [dbo].[CFGActivityType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(15)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGActivityTypeDescriptions where code = @Code
Delete from CFGActivityTypeData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGActivityTypeTrigger] 
   ON  [dbo].[CFGActivityType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGActivityTypeData]
  ([Code],[OutlookType])
Select [Code],[OutlookType] from inserted

INSERT INTO [CFGActivityTypeDescriptions]
  ([Code] 
  ,ActivityType
  ,UICultureName
  ,Seq)
Select [Code],[ActivityType], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGActivityTypeTrigger] 
   ON  [dbo].[CFGActivityType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(15)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGActivityTypeData
  set code = i.code,
   outlookType = i.outlookType
from inserted i
  inner join CFGActivityTypeData on CFGActivityTypeData.code = @Code

if (not exists(Select CFGActivityTypeDescriptions.UICultureName from CFGActivityTypeDescriptions
  where CFGActivityTypeDescriptions.code = @code and CFGActivityTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGActivityTypeDescriptions
      ([Code]
      ,[UICultureName]
      ,[ActivityType]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[ActivityType],[Seq] From Inserted 

Else
  Update CFGActivityTypeDescriptions
      Set code = i.code
      , [ActivityType] = i.ActivityType
      , seq = i.seq
  From inserted i
  Inner Join CFGActivityTypeDescriptions on CFGActivityTypeDescriptions.code = @code and CFGActivityTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
