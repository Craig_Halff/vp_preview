SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGEmployeeTitle]
as
SELECT dbo.CFGEmployeeTitleData.Code, dbo.CFGEmployeeTitleDescriptions.Title, dbo.CFGEmployeeTitleDescriptions.Seq
FROM         dbo.CFGEmployeeTitleData Left JOIN
dbo.CFGEmployeeTitleDescriptions ON dbo.CFGEmployeeTitleData.Code = dbo.CFGEmployeeTitleDescriptions.Code 
and dbo.CFGEmployeeTitleDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGEmployeeTitleTrigger] 
   ON  [dbo].[CFGEmployeeTitle]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGEmployeeTitleDescriptions where code = @Code
Delete from CFGEmployeeTitleData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGEmployeeTitleTrigger] 
   ON  [dbo].[CFGEmployeeTitle]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGEmployeeTitleData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGEmployeeTitleDescriptions]
  ([Code] 
  ,Title
  ,UICultureName
  ,Seq)
Select [Code],[Title], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGEmployeeTitleTrigger] 
   ON  [dbo].[CFGEmployeeTitle]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGEmployeeTitleData
  set code = i.code
from inserted i
  inner join CFGEmployeeTitleData on CFGEmployeeTitleData.code = @Code

if (not exists(Select CFGEmployeeTitleDescriptions.UICultureName from CFGEmployeeTitleDescriptions
  where CFGEmployeeTitleDescriptions.code = @code and CFGEmployeeTitleDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGEmployeeTitleDescriptions
      ([Code]
      ,[UICultureName]
      ,[Title]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Title],[Seq] From Inserted 

Else
  Update CFGEmployeeTitleDescriptions
      Set code = i.code
      , [Title] = i.Title
      , seq = i.seq
  From inserted i
  Inner Join CFGEmployeeTitleDescriptions on CFGEmployeeTitleDescriptions.code = @code and CFGEmployeeTitleDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
