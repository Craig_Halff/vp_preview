SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGAPExpenseCode]
as
SELECT dbo.CFGAPExpenseCodeData.code, dbo.CFGAPExpenseCodeData.type, dbo.CFGAPExpenseCodeDescriptions.Description 
FROM         dbo.CFGAPExpenseCodeData Left JOIN
dbo.CFGAPExpenseCodeDescriptions ON dbo.CFGAPExpenseCodeData.code = dbo.CFGAPExpenseCodeDescriptions.code 
and dbo.CFGAPExpenseCodeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGAPExpenseCodeTrigger] 
   ON  [dbo].[CFGAPExpenseCode]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @code VARCHAR(10)

SELECT top 1 @code = code FROM Deleted

Delete from CFGAPExpenseCodeDescriptions where code = @code
Delete from CFGAPExpenseCodeData where code = @code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGAPExpenseCodeTrigger] 
   ON  [dbo].[CFGAPExpenseCode]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGAPExpenseCodeData]
  ([code],type)
Select [code],type from inserted

INSERT INTO [CFGAPExpenseCodeDescriptions]
  ([code] 
  ,Description
  ,UICultureName)
Select [code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGAPExpenseCodeTrigger] 
   ON  [dbo].[CFGAPExpenseCode]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @code VARCHAR(10)

if update(code)
  select @code=code from deleted
else
  select @code=code from inserted

Update CFGAPExpenseCodeData
  set code = i.code, type = i.type
from inserted i
  inner join CFGAPExpenseCodeData on CFGAPExpenseCodeData.code = @code

if (not exists(Select CFGAPExpenseCodeDescriptions.UICultureName from CFGAPExpenseCodeDescriptions
  where CFGAPExpenseCodeDescriptions.code = @code and CFGAPExpenseCodeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGAPExpenseCodeDescriptions
      ([code]
      ,[UICultureName]
      ,[Description])
  Select [code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGAPExpenseCodeDescriptions
      Set code = i.code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGAPExpenseCodeDescriptions on CFGAPExpenseCodeDescriptions.code = @code and CFGAPExpenseCodeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
