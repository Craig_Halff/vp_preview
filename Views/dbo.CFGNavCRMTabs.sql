SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGNavCRMTabs]
AS
SELECT dbo.CFGNavCRMTabsData.InfocenterArea, dbo.CFGNavCRMTabsData.TabID, dbo.CFGNavCRMTabsData.TabType, dbo.CFGNavCRMTabsData.Seq, 
dbo.CFGNavCRMTabHeadings.UICultureName, dbo.CFGNavCRMTabHeadings.TabHeading 
FROM dbo.CFGNavCRMTabsData 
LEFT JOIN dbo.CFGNavCRMTabHeadings ON dbo.CFGNavCRMTabHeadings.TabID = dbo.CFGNavCRMTabsData.TabID AND 
dbo.CFGNavCRMTabHeadings.InfocenterArea = dbo.CFGNavCRMTabsData.InfocenterArea AND dbo.CFGNavCRMTabHeadings.UICultureName = dbo.FW_GetActiveCultureName()
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGNavCRMTabsTrigger] 
ON [dbo].[CFGNavCRMTabs]
INSTEAD OF DELETE
AS 
BEGIN

SET NOCOUNT ON;
 
DELETE FROM CFGNavCRMTabHeadings FROM deleted
WHERE CFGNavCRMTabHeadings.InfocenterArea = deleted.InfocenterArea 
AND CFGNavCRMTabHeadings.TabID = deleted.TabID
 
DELETE FROM CFGNavCRMTabsData FROM deleted
WHERE CFGNavCRMTabsData.InfocenterArea = deleted.InfocenterArea 
AND CFGNavCRMTabsData.TabID = deleted.TabID

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[InsertCFGNavCRMTabsTrigger] 
ON [dbo].[CFGNavCRMTabs]
INSTEAD OF Insert
AS
BEGIN
 
SET NOCOUNT ON;

Insert into CFGNavCRMTabsData (InfocenterArea, Tabid, TabType, Seq)
SELECT InfocenterArea, Tabid, TabType, Seq FROM inserted

Insert into CFGNavCRMTabHeadings (InfocenterArea, Tabid, UICultureName, TabHeading)
SELECT InfocenterArea, Tabid, dbo.FW_GetActiveCultureName(), TabHeading FROM Inserted

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[UpdateCFGNavCRMTabsTrigger] 
ON [dbo].[CFGNavCRMTabs]
INSTEAD OF UPDATE
AS 
BEGIN

SET NOCOUNT ON;
DECLARE @TabID Nvarchar(250)

IF UPDATE(TabID)
  SELECT @Tabid =TabID FROM deleted
ELSE
  SELECT @Tabid =TabID FROM inserted
 
UPDATE CFGNavCRMTabsData
   SET TabType = i.TabType, Seq = isnull(i.Seq,0), tabId = i.TabID
  FROM inserted i
INNER JOIN CFGNavCRMTabsData ON CFGNavCRMTabsData.InfocenterArea = i.InfocenterArea AND CFGNavCRMTabsData.TabID = @TabID

IF NOT EXISTS (SELECT CFGNavCRMTabHeadings.UICultureName FROM CFGNavCRMTabHeadings, Inserted I
WHERE CFGNavCRMTabHeadings.InfocenterArea = i.InfocenterArea AND CFGNavCRMTabHeadings.TabID = @TabID
AND CFGNavCRMTabHeadings.UICultureName =dbo.FW_GetActiveCultureName())
  Insert into CFGNavCRMTabHeadings (InfocenterArea, TabID, UICultureName, TabHeading)
  SELECT InfocenterArea, TabID, dbo.FW_GetActiveCultureName(), TabHeading FROM inserted
ELSE
  UPDATE CFGNavCRMTabHeadings
  SET TabHeading = i.TabHeading, TabId = i.TabID
  FROM inserted i
  INNER JOIN CFGNavCRMTabHeadings ON CFGNavCRMTabHeadings.InfocenterArea = i.InfocenterArea AND CFGNavCRMTabHeadings.TabID = @Tabid
  AND CFGNavCRMTabHeadings.UICultureName = dbo.FW_GetActiveCultureName()

END 

GO
