SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGFARepairType]
as
SELECT 	dbo.CFGFARepairTypeData.Code,
	dbo.CFGFARepairTypeData.Company,
	dbo.CFGFARepairTypeDescriptions.Description,
	dbo.CFGFARepairTypeData.InUse
FROM   dbo.CFGFARepairTypeData
 Left JOIN dbo.CFGFARepairTypeDescriptions ON dbo.CFGFARepairTypeData.Code = dbo.CFGFARepairTypeDescriptions.Code
and   dbo.CFGFARepairTypeData.Company = dbo.CFGFARepairTypeDescriptions.Company
and   dbo.CFGFARepairTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGFARepairTypeTrigger] 
   ON  [dbo].[CFGFARepairType]
instead of Delete
AS 
BEGIN
SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)
DECLARE @Company Nvarchar(14)

SELECT top 1 @Code = Code, @Company = Company FROM Deleted

Delete from CFGFARepairTypeDescriptions where CFGFARepairTypeDescriptions.code = @Code and 
            CFGFARepairTypeDescriptions.Company = @Company
Delete from CFGFARepairTypeData where CFGFARepairTypeData.code = @Code and CFGFARepairTypeData.Company = @Company

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[InsertCFGFARepairTypeTrigger] 
   ON  [dbo].[CFGFARepairType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGFARepairTypeData]
  (	Code,
	Company,
	InUse)
Select 	Code,
	Company,
	isnull(InUse,'N')
 from inserted

INSERT INTO [CFGFARepairTypeDescriptions]
  (Code, Company, Description, UICultureName)
Select Code, Company, Description, dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create TRIGGER [dbo].[UpdateCFGFARepairTypeTrigger] 
   ON  [dbo].[CFGFARepairType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(14)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

	
Update CFGFARepairTypeData
  set 	Code = i.code,
	Company = i.Company,
	InUse= i.InUse
 from inserted i
  inner join CFGFARepairTypeData on CFGFARepairTypeData.Company = i.Company and CFGFARepairTypeData.code = @Code

if (not exists(Select CFGFARepairTypeDescriptions.UICultureName from CFGFARepairTypeDescriptions, Inserted I
  where CFGFARepairTypeDescriptions.Company = i.Company and CFGFARepairTypeDescriptions.code = @code and CFGFARepairTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGFARepairTypeDescriptions
      (Code,
	   Company,
       UICultureName,
       Description)
  Select Code, Company, dbo.FW_GetActiveCultureName(), Description From Inserted 

Else
  Update CFGFARepairTypeDescriptions
      Set code = i.code,
          Company = i.Company,
         Description = i.Description
  From inserted i
  Inner Join CFGFARepairTypeDescriptions on CFGFARepairTypeDescriptions.Company = i.Company and CFGFARepairTypeDescriptions.code = @code and CFGFARepairTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
