SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[VE] AS 
SELECT [Vendor]
	,Case ClientInd when 'Y' then ClientID else null end as [LinkedClient]
	,[ClientID]
	,[Client]
	,[Name]
	,[Type]
	,[Status]
	,[ExportInd]
	,[WebSite]
	,[Memo]
	,[CreateUser]
	,[CreateDate]
	,[ModUser]
	,[ModDate]
	,[CurrentStatus]
	,[CustomCurrencyCode]
	,[ClientInd]
	,[VendorInd]
	,[PriorWork]
	,[Recommend]
	,[DisadvBusiness]
	,[DisabledVetOwnedSmallBusiness]
	,[HBCU]
	,[MinorityBusiness]
	,[SmallBusiness]
	,[VetOwnedSmallBusiness]
	,[WomanOwned]
	,[AlaskaNative]
	,[SpecialtyType]
	,[Specialty]
	,[ParentID]
	,[ParentLevel1]
	,[ParentLevel2]
	,[ParentLevel3]
	,[ParentLevel4]
	,[Employees]
	,[AnnualRevenue]
	,[GovernmentAgency]
	,[Competitor]
	,[EightA]
	,[Hubzone]
	,[IQID]
	,[Incumbent]
	,[AjeraSync]
	,[TLInternalKey]
	,[TLSyncModDate]
	,[Owner]
	,[SortName]
	,[Category]
	,[AvailableForCRM]
	,[ReadyForApproval]
	,[ReadyForProcessing]
	,[FedID]
	,[Org]
	,[Market]
	,[TypeOfTIN]
	,[SFID]
	,[SFLastModifiedDate]
	,[QBOId]
	,[QBOLastUpdated]
	from Clendor where vendorIND = 'Y'

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteVETrigger] 
   ON  [dbo].[VE]
instead of Delete
AS 
BEGIN
    SET NOCOUNT ON;
    Delete from Clendor where exists (select 'x' from deleted where Clendor.Vendor = deleted.Vendor)
end
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertVETrigger] 
   ON  [dbo].[VE]
instead of Insert
as
BEGIN
    SET NOCOUNT ON;

	INSERT INTO Clendor
	([clientid]
	,[Name]
	,[Type]
	,[Status]
	,[WebSite]
	,[CustomCurrencyCode]
	,[VendorInd]
	,[PriorWork]
	,[Recommend]
	,[DisadvBusiness]
	,[DisabledVetOwnedSmallBusiness]
	,[HBCU]
	,[MinorityBusiness]
	,[SmallBusiness]
	,[VetOwnedSmallBusiness]
	,[WomanOwned]
	,[AlaskaNative]
	,[Specialty]
	,[Employees]
	,[AnnualRevenue]
	,[EightA]
	,[Hubzone]
	,[AjeraSync]
	,[Vendor]
	,[SortName]
	,[Category]
	,[AvailableForCRM]
	,[ReadyForApproval]
	,[ReadyForProcessing]
	,[FedID]
	,[Org]
	,[Market]
	,[TypeOfTIN]
	,[SFID]
	,[SFLastModifiedDate]
	,[QBOId]
	,[QBOLastUpdated]
	,[CreateUser]
	,[CreateDate]
	,[ModUser]
	,[ModDate])

	SELECT 
	replace(NewID(),'-','') AS clientid
	,[Name]
	,[Type]
	,[Status]
	,[WebSite]
	,[CustomCurrencyCode]
	,'Y'
	,isNull([PriorWork], 'N')
	,isNull([Recommend], 'N')
	,isNull([DisadvBusiness], 'N')
	,isNull([DisabledVetOwnedSmallBusiness], 'N')
	,isNull([HBCU], 'N')
	,isNull([MinorityBusiness], 'N')
	,isNull([SmallBusiness], 'N')
	,isNull([VetOwnedSmallBusiness], 'N')
	,isNull([WomanOwned], 'N')
	,isNull([AlaskaNative], 'N')
	,[Specialty]
	,isNull([Employees], 0)
	,isNull([AnnualRevenue], 0)
	,isNull([EightA], 'N')
	,isNull([Hubzone], 'N')
	,isnull(AjeraSync, 'N')
	,[Vendor]
	,[SortName]
	,[Category]
	,isNull([AvailableForCRM], 'N')
	,isNull([ReadyForApproval], 'N')
	,isNull([ReadyForProcessing], 'N')
	,[FedID]
	,[Org]
	,[Market]
	,[TypeOfTIN] 
	,[SFID]
	,[SFLastModifiedDate]
	,[QBOId]
	,[QBOLastUpdated]
	,[CreateUser]
	,isnull(CreateDate,getutcdate())
	,[ModUser]
	,isnull(ModDate,getutcdate())
	FROM inserted
	where not exists (select 'x' from Clendor where Clendor.vendor = inserted.vendor)
end
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateVETrigger] 
   ON  [dbo].[VE]
instead of Update
as
BEGIN
    If Not Update(Vendor)
	begin
		UPDATE Clendor SET
		  Name = i.Name
		  ,Type = i.Type
		  ,Status = i.Status
		  ,WebSite = i.Website
		  ,CustomCurrencyCode = i.CustomCurrencyCode
		  ,VendorInd = 'Y'
		  ,Vendor = i.vendor
		  ,PriorWork = i.PriorWork
		  ,Recommend = i.Recommend
		  ,DisadvBusiness = i.DisadvBusiness
		  ,DisabledVetOwnedSmallBusiness = i.DisabledVetOwnedSmallBusiness
		  ,HBCU = i.HBCU
		  ,MinorityBusiness = i.MinorityBusiness
		  ,SmallBusiness = i.SmallBusiness
		  ,VetOwnedSmallBusiness = i.VetOwnedSmallBusiness
		  ,WomanOwned = i.WomanOwned
		  ,AlaskaNative = i.AlaskaNative
		  ,Specialty = i.Specialty
		  ,Employees = i.Employees
		  ,AnnualRevenue = i.AnnualRevenue
		  ,EightA = i.EightA
		  ,Hubzone = i.Hubzone
		  ,AjeraSync = i.AjeraSync
		  ,SortName = i.SortName
		  ,Category= i.Category
		  ,AvailableForCRM = i.AvailableForCRM
		  ,ReadyForApproval = i.ReadyForApproval
		  ,ReadyForProcessing = i.ReadyForProcessing
		  ,FedID = i.FedID
		  ,Org = i.Org
		  ,Market = i.Market
		  ,TypeOfTIN = i.TypeOfTIN
		  ,SFID = i.SFID
		  ,SFLastModifiedDate = i.SFLastModifiedDate
		  ,QBOId = i.QBOID
		  ,QBOLastUpdated = i.QBOLastUpdated
		  ,CreateUser = i.CreateUser
		  ,CreateDate = i.CreateDate
		  ,ModUser = i.ModUser
		  ,ModDate = i.ModDate
		FROM inserted i
		 inner join Clendor on Clendor.Vendor = i.Vendor
	end
	else
	begin
		UPDATE Clendor SET
		  Name = i.Name
		  ,Type = i.Type
		  ,Status = i.Status
		  ,WebSite = i.Website
		  ,CustomCurrencyCode = i.CustomCurrencyCode
		  ,VendorInd = 'Y'
		  ,Vendor = i.vendor
		  ,PriorWork = i.PriorWork
		  ,Recommend = i.Recommend
		  ,DisadvBusiness = i.DisadvBusiness
		  ,DisabledVetOwnedSmallBusiness = i.DisabledVetOwnedSmallBusiness
		  ,HBCU = i.HBCU
		  ,MinorityBusiness = i.MinorityBusiness
		  ,SmallBusiness = i.SmallBusiness
		  ,VetOwnedSmallBusiness = i.VetOwnedSmallBusiness
		  ,WomanOwned = i.WomanOwned
		  ,AlaskaNative = i.AlaskaNative
		  ,Specialty = i.Specialty
		  ,Employees = i.Employees
		  ,AnnualRevenue = i.AnnualRevenue
		  ,EightA = i.EightA
		  ,Hubzone = i.Hubzone
		  ,AjeraSync = i.AjeraSync
		  ,SortName = i.SortName
		  ,Category= i.Category
		  ,AvailableForCRM = i.AvailableForCRM
		  ,ReadyForApproval = i.ReadyForApproval
		  ,ReadyForProcessing = i.ReadyForProcessing
		  ,FedID = i.FedID
		  ,Org = i.Org
		  ,Market = i.Market
		  ,TypeOfTIN = i.TypeOfTIN
		  ,SFID = i.SFID
		  ,SFLastModifiedDate = i.SFLastModifiedDate
		  ,QBOId = i.QBOID
		  ,QBOLastUpdated = i.QBOLastUpdated
		  ,CreateUser = i.CreateUser
		  ,CreateDate = i.CreateDate
		  ,ModUser = i.ModUser
		  ,ModDate = i.ModDate
		FROM inserted i
		inner join deleted d on i.ClientID = d.ClientID
		inner join Clendor on Clendor.Vendor =  d.ClientID
	end
END
GO
