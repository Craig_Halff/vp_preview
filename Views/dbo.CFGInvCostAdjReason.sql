SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGInvCostAdjReason]
as
SELECT dbo.CFGInvCostAdjReasonData.Code, dbo.CFGInvCostAdjReasonDescriptions.Description 
FROM         dbo.CFGInvCostAdjReasonData Left JOIN
dbo.CFGInvCostAdjReasonDescriptions ON dbo.CFGInvCostAdjReasonData.Code = dbo.CFGInvCostAdjReasonDescriptions.Code 
and dbo.CFGInvCostAdjReasonDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGInvCostAdjReasonTrigger] 
   ON  [dbo].[CFGInvCostAdjReason]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(20)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGInvCostAdjReasonDescriptions where Code = @Code
Delete from CFGInvCostAdjReasonData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGInvCostAdjReasonTrigger] 
   ON  [dbo].[CFGInvCostAdjReason]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGInvCostAdjReasonData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGInvCostAdjReasonDescriptions]
  ([Code] 
  ,Description
  ,UICultureName)
Select [Code],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGInvCostAdjReasonTrigger] 
   ON  [dbo].[CFGInvCostAdjReason]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(20)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGInvCostAdjReasonData
  set Code = i.Code
from inserted i
  inner join CFGInvCostAdjReasonData on CFGInvCostAdjReasonData.Code = @Code

if (not exists(Select CFGInvCostAdjReasonDescriptions.UICultureName from CFGInvCostAdjReasonDescriptions
  where CFGInvCostAdjReasonDescriptions.Code = @Code and CFGInvCostAdjReasonDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGInvCostAdjReasonDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update CFGInvCostAdjReasonDescriptions
      Set Code = i.Code
      , [Description] = i.Description
  From inserted i
  Inner Join CFGInvCostAdjReasonDescriptions on CFGInvCostAdjReasonDescriptions.Code = @Code and CFGInvCostAdjReasonDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
