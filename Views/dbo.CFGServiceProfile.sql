SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGServiceProfile]
as
SELECT	dbo.CFGServiceProfileData.Code,dbo.CFGServiceProfileData.FeeBands,dbo.CFGServiceProfileData.CurrencyCode,dbo.CFGServiceProfileData.SystemProfile,
	dbo.CFGServiceProfileData.Disabled, dbo.CFGServiceProfileData.GlobalProfile,dbo.CFGServiceProfileData.DisableFeeBands,
	ISNULL(dbo.CFGServiceProfileDescriptions.Description, dbo.FW_TextNotTranslated.LocalizedValue) AS Description, ISNULL(dbo.CFGServiceProfileDescriptions.FeeBasisLabel, dbo.FW_TextNotTranslated.LocalizedValue) AS FeeBasisLabel
  FROM	dbo.CFGServiceProfileData Left JOIN 
	dbo.CFGServiceProfileDescriptions ON dbo.CFGServiceProfileData.Code = dbo.CFGServiceProfileDescriptions.Code and
	dbo.CFGServiceProfileDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
	 LEFT JOIN dbo.FW_TextNotTranslated ON dbo.FW_TextNotTranslated.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[DeleteCFGServiceProfileTrigger] 
   ON [dbo].[CFGServiceProfile]
instead of Delete
AS 
BEGIN
   SET NOCOUNT ON;
   DECLARE @Code Nvarchar(10)

   SELECT top 1 @Code = Code FROM Deleted
   
   DELETE from CFGServiceProfileDescriptions where CFGServiceProfileDescriptions.Code = @Code
   DELETE from CFGServiceProfileData where CFGServiceProfileData.Code = @Code 
END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[InsertCFGServiceProfileTrigger] 
	ON [dbo].[CFGServiceProfile]

instead of Insert
as
BEGIN
   SET NOCOUNT ON;
 
   INSERT INTO CFGServiceProfileData (Code, FeeBands, CurrencyCode, SystemProfile, Disabled, GlobalProfile, DisableFeeBands)
   SELECT Code, FeeBands, CurrencyCode, SystemProfile, Disabled, GlobalProfile, DisableFeeBands from inserted

   INSERT INTO CFGServiceProfileDescriptions(Code, UICultureName, Description, FeeBasisLabel)
   SELECT Code, dbo.FW_GetActiveCultureName(), Description, FeeBasisLabel from inserted

END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[UpdateCFGServiceProfileTrigger] 
   ON [dbo].[CFGServiceProfile]
instead of Update
as
BEGIN
   SET NOCOUNT ON;

   DECLARE @Code Nvarchar(10)

   if update(Code)
	SELECT @Code=Code from deleted
   else
	SELECT @Code=Code from inserted

   UPDATE CFGServiceProfileData
      SET Code = i.Code, FeeBands = i.FeeBands, CurrencyCode = i.CurrencyCode, SystemProfile = i.SystemProfile, Disabled = i.Disabled, GlobalProfile = i.GlobalProfile, DisableFeeBands = i.DisableFeeBands
     FROM inserted i inner join CFGServiceProfileData on CFGServiceProfileData.Code = @Code

   if not exists(select CFGServiceProfileDescriptions.UICultureName from CFGServiceProfileDescriptions, Inserted I where CFGServiceProfileDescriptions.Code = @Code and CFGServiceProfileDescriptions.UICultureName =dbo.FW_GetActiveCultureName())
	INSERT INTO CFGServiceProfileDescriptions(Code, UICultureName, Description, FeeBasisLabel)
	SELECT Code, dbo.FW_GetActiveCultureName(), Description, FeeBasisLabel FROM Inserted 
   else
	UPDATE CFGServiceProfileDescriptions
	   SET Code = i.Code, Description = i.Description, FeeBasisLabel = i.FeeBasisLabel
	  FROM inserted i
	Inner Join CFGServiceProfileDescriptions on CFGServiceProfileDescriptions.Code = @Code and CFGServiceProfileDescriptions.UICultureName = dbo.FW_GetActiveCultureName() 

END

GO
