SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[DashParts]
as
SELECT dbo.DashPartsData.PartID, dbo.DashPartsData.Type, dbo.DashPartsData.sql, dbo.DashPartsData.DefaultPrefs, dbo.DashPartsDescriptions.Description 
FROM         dbo.DashPartsData Left JOIN
dbo.DashPartsDescriptions ON dbo.DashPartsData.PartID = dbo.DashPartsDescriptions.PartID 
and dbo.DashPartsDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

	


GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteDashPartsTrigger] 
   ON  [dbo].[DashParts]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @PartID int

SELECT top 1 @PartID = PartID FROM Deleted

Delete from DashPartsDescriptions where PartID = @PartID
Delete from DashPartsData where PartID = @PartID

END
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertDashPartsTrigger] 
   ON  [dbo].[DashParts]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [DashPartsData]
  (PartID, Type, sql, DefaultPrefs)
Select PartID, Type, sql, DefaultPrefs from inserted

INSERT INTO [DashPartsDescriptions]
  ([PartID] 
  ,Description
  ,UICultureName)
Select [PartID],[Description], dbo.FW_GetActiveCultureName() from inserted

End
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateDashPartsTrigger] 
   ON  [dbo].[DashParts]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @PartID INT

if update(PartID)
  select @PartID=PartID from deleted
else
  select @PartID=PartID from inserted

Update DashPartsData
  set PartID = i.PartID
from inserted i
  inner join DashPartsData on DashPartsData.PartID = @PartID

if (not exists(Select DashPartsDescriptions.UICultureName from DashPartsDescriptions
  where DashPartsDescriptions.PartID = @PartID and DashPartsDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into DashPartsDescriptions
      ([PartID]
      ,[UICultureName]
      ,[Description])
  Select [PartID], dbo.FW_GetActiveCultureName(),[Description] From Inserted 

Else
  Update DashPartsDescriptions
      Set PartID = i.PartID
      , [Description] = i.Description
  From inserted i
  Inner Join DashPartsDescriptions on DashPartsDescriptions.PartID = @PartID and DashPartsDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
