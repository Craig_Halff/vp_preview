SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[ClendorProjectAssocReporting]
AS
SELECT ClendorProjectAssoc.PKey
      ,ClendorProjectAssoc.ClientID
      ,PR.WBS1
      ,PR.WBS2
      ,PR.WBS3
      ,ClendorProjectAssoc.Role
      ,ClendorProjectAssoc.RoleDescription
      ,ClendorProjectAssoc.TeamStatus
      ,ClendorProjectAssoc.Address
      ,ClendorProjectAssoc.PrimaryInd
      ,ClendorProjectAssoc.ClientConfidential
      ,ClendorProjectAssoc.ClientInd
      ,ClendorProjectAssoc.VendorInd
      ,ClendorProjectAssoc.CreateUser
      ,ClendorProjectAssoc.CreateDate
      ,ClendorProjectAssoc.ModUser
      ,ClendorProjectAssoc.ModDate
FROM dbo.ClendorProjectAssoc
INNER JOIN PR ON PR.WBS1 = ClendorProjectAssoc.WBS1
and ClendorProjectAssoc.ClientInd = 'Y'
UNION ALL
SELECT ClendorProjectAssoc.PKey
      ,ClendorProjectAssoc.ClientID
      ,PR.WBS1
      ,PR.WBS2
      ,PR.WBS3
      ,ClendorProjectAssoc.Role
      ,ClendorProjectAssoc.RoleDescription
      ,ClendorProjectAssoc.TeamStatus
      ,ClendorProjectAssoc.Address
      ,ClendorProjectAssoc.PrimaryInd
      ,ClendorProjectAssoc.ClientConfidential
      ,ClendorProjectAssoc.ClientInd
      ,ClendorProjectAssoc.VendorInd
      ,ClendorProjectAssoc.CreateUser
      ,ClendorProjectAssoc.CreateDate
      ,ClendorProjectAssoc.ModUser
      ,ClendorProjectAssoc.ModDate
FROM dbo.ClendorProjectAssoc
INNER JOIN PR ON PR.WBS1 = ClendorProjectAssoc.WBS1
and PR.WBS2 = ClendorProjectAssoc.WBS2
and PR.WBS3 = ClendorProjectAssoc.WBS3
and ClendorProjectAssoc.VendorInd = 'Y'
GO
