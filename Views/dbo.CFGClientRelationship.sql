SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGClientRelationship]
as
SELECT dbo.CFGClientRelationshipData.Code, dbo.CFGClientRelationshipData.InverseCode, 
       dbo.CFGClientRelationshipDescriptions.Description, dbo.CFGClientRelationshipDescriptions.Seq
  FROM dbo.CFGClientRelationshipData Left JOIN
       dbo.CFGClientRelationshipDescriptions ON dbo.CFGClientRelationshipData.Code = dbo.CFGClientRelationshipDescriptions.Code and
       dbo.CFGClientRelationshipDescriptions.UICultureName = dbo.FW_GetActiveCultureName()
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGClientRelationshipTrigger] 
   ON  [dbo].[CFGClientRelationship]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(10)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGClientRelationshipDescriptions where code = @Code
Delete from CFGClientRelationshipData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGClientRelationshipTrigger] 
   ON  [dbo].[CFGClientRelationship]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGClientRelationshipData]
  ([Code], [InverseCode])
Select [Code], [InverseCode] from inserted

INSERT INTO [CFGClientRelationshipDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGClientRelationshipTrigger] 
   ON  [dbo].[CFGClientRelationship]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(10)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGClientRelationshipData
  set Code = i.Code,
      InverseCode = i.InverseCode
from inserted i
  inner join CFGClientRelationshipData on CFGClientRelationshipData.code = @Code

if (not exists(Select CFGClientRelationshipDescriptions.UICultureName from CFGClientRelationshipDescriptions
  where CFGClientRelationshipDescriptions.code = @code and CFGClientRelationshipDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGClientRelationshipDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGClientRelationshipDescriptions
      Set Code = i.Code
      , [Description] = i.Description
      , Seq = i.Seq
  From inserted i
  Inner Join CFGClientRelationshipDescriptions on CFGClientRelationshipDescriptions.code = @code and CFGClientRelationshipDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
