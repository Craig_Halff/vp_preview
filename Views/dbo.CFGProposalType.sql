SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGProposalType]
as
SELECT dbo.CFGProposalTypeData.Code, dbo.CFGProposalTypeDescriptions.Type, dbo.CFGProposalTypeDescriptions.Seq
FROM         dbo.CFGProposalTypeData Left JOIN
dbo.CFGProposalTypeDescriptions ON dbo.CFGProposalTypeData.Code = dbo.CFGProposalTypeDescriptions.Code 
and dbo.CFGProposalTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGProposalTypeTrigger] 
   ON  [dbo].[CFGProposalType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(30)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGProposalTypeDescriptions where code = @Code
Delete from CFGProposalTypeData where code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGProposalTypeTrigger] 
   ON  [dbo].[CFGProposalType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

DECLARE @seq as smallint
select @seq=isnull(seq,0) from inserted

INSERT INTO [CFGProposalTypeData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGProposalTypeDescriptions]
  ([Code] 
  ,Type
  ,UICultureName
  ,Seq)
Select [Code],[Type], dbo.FW_GetActiveCultureName(),@seq from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGProposalTypeTrigger] 
   ON  [dbo].[CFGProposalType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(30)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGProposalTypeData
  set code = i.code
from inserted i
  inner join CFGProposalTypeData on CFGProposalTypeData.code = @Code

if (not exists(Select CFGProposalTypeDescriptions.UICultureName from CFGProposalTypeDescriptions
  where CFGProposalTypeDescriptions.code = @code and CFGProposalTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGProposalTypeDescriptions
      ([Code]
      ,[UICultureName]
      ,[Type]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Type],[Seq] From Inserted 

Else
  Update CFGProposalTypeDescriptions
      Set code = i.code
      , [Type] = i.Type
      , seq = i.seq
  From inserted i
  Inner Join CFGProposalTypeDescriptions on CFGProposalTypeDescriptions.code = @code and CFGProposalTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
