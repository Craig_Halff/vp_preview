SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CFGBankAccountIDType]
as
SELECT dbo.CFGBankAccountIDTypeData.Code, dbo.CFGBankAccountIDTypeDescriptions.Description, dbo.CFGBankAccountIDTypeDescriptions.Seq 
FROM         dbo.CFGBankAccountIDTypeData Left JOIN
dbo.CFGBankAccountIDTypeDescriptions ON dbo.CFGBankAccountIDTypeData.Code = dbo.CFGBankAccountIDTypeDescriptions.Code 
and dbo.CFGBankAccountIDTypeDescriptions.UICultureName = dbo.FW_GetActiveCultureName()

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[DeleteCFGBankAccountIDTypeTrigger] 
   ON  [dbo].[CFGBankAccountIDType]
instead of Delete
AS 
BEGIN
     SET NOCOUNT ON;
DECLARE @Code Nvarchar(20)

SELECT top 1 @Code = Code FROM Deleted

Delete from CFGBankAccountIDTypeDescriptions where Code = @Code
Delete from CFGBankAccountIDTypeData where Code = @Code

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[InsertCFGBankAccountIDTypeTrigger] 
   ON  [dbo].[CFGBankAccountIDType]
instead of Insert
as
BEGIN
     SET NOCOUNT ON;

INSERT INTO [CFGBankAccountIDTypeData]
  ([Code])
Select [Code] from inserted

INSERT INTO [CFGBankAccountIDTypeDescriptions]
  ([Code] 
  ,Description
  ,UICultureName
  ,Seq)
Select [Code],[Description], dbo.FW_GetActiveCultureName(), isnull(seq,0) from inserted

End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create TRIGGER [dbo].[UpdateCFGBankAccountIDTypeTrigger] 
   ON  [dbo].[CFGBankAccountIDType]
instead of Update
as
BEGIN
     SET NOCOUNT ON;

DECLARE @Code Nvarchar(20)

if update(Code)
  select @Code=Code from deleted
else
  select @Code=Code from inserted

Update CFGBankAccountIDTypeData
  set Code = i.Code
from inserted i
  inner join CFGBankAccountIDTypeData on CFGBankAccountIDTypeData.Code = @Code

if (not exists(Select CFGBankAccountIDTypeDescriptions.UICultureName from CFGBankAccountIDTypeDescriptions
  where CFGBankAccountIDTypeDescriptions.Code = @Code and CFGBankAccountIDTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName()))
  Insert Into CFGBankAccountIDTypeDescriptions
      ([Code]
      ,[UICultureName]
      ,[Description]
      ,[Seq])
  Select [Code], dbo.FW_GetActiveCultureName(),[Description],[Seq] From Inserted 

Else
  Update CFGBankAccountIDTypeDescriptions
      Set Code = i.Code
      , [Description] = i.Description
      , seq = i.seq
  From inserted i
  Inner Join CFGBankAccountIDTypeDescriptions on CFGBankAccountIDTypeDescriptions.Code = @Code and CFGBankAccountIDTypeDescriptions.UICultureName =dbo.FW_GetActiveCultureName() 

END
GO
