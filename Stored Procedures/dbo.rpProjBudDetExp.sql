SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpProjBudDetExp]
  (
   @company as Nvarchar(4),@ETCDate as datetime,@rptWBSListKey as varchar(32),
   @includeFlags as smallint,
   @wbs1Activity varchar(1) = 'Y',
   @wbs2Activity varchar(1) = 'Y',
   @wbs3Activity varchar(1) = 'Y',
   @workTable varchar(100),
   @adapterTableExp varchar(100),
   @adapterTableCon varchar(100)
  )
as 
begin

declare @etcDateString as varchar(100);
declare @rpPrintDirects as smallint,@rpPrintDirectCons as smallint,@rpPrintReimbs As smallint,@rpPrintReimbCons as smallint,@rpPrintIndirects as smallint
declare @rpPrintUnits as smallint;
declare @expTab as smallint,@conTab as smallint,@untTab as smallint;
declare @temptable as varchar(100);

DECLARE @strJTDDate VARCHAR(10)
Set @strJTDDate = convert(varchar(10), @ETCDate, 126)

set @etcDate = dateadd(d,1,@etcDate)

select 
@expTab = case when expTab = 'Y' then 1 else 0 end, 
@conTab = case when conTab = 'Y' then 1 else 0 end, 
@untTab = case when untTab = 'Y' then 1 else 0 end 
from cfgresourceplanning where company = @company;

select @rpPrintDirects = 0,@rpPrintDirectCons = 0,@rpPrintReimbs = 0,@rpPrintReimbCons = 0,@rpPrintIndirects = 0;
if (@includeFlags >= 16) 
 begin
 set @rpPrintIndirects = 1
 set @includeFlags = @includeFlags - 16
 end
if (@includeFlags >= 8)
 begin
 set @rpPrintReimbCons = 1
 set @includeFlags = @includeFlags - 8
 end
if (@includeFlags >= 4)
 begin
 set @rpPrintReimbs = 1
 set @includeFlags = @includeFlags - 4
 end
if (@includeFlags >= 2)
 begin
 set @rpPrintDirectCons = 1
 set @includeFlags = @includeFlags - 2
 end
if (@includeFlags >= 1)
 begin
 set @rpPrintDirects = 1
 set @includeFlags = @includeFlags - 1
 end
--@includeFlags should be 0 at this point

declare @sqlQuery as varchar(max)
declare @wbsActivityLevel as smallint
set @wbsActivityLevel = 3
if (@wbs1Activity = 'Y')
  begin
  set @wbsActivityLevel = 1
  end

if (@wbs2Activity = 'Y')
  begin
  set @wbsActivityLevel = 2
  end

if (@wbs3Activity = 'Y')
  begin
  set @wbsActivityLevel = 3
  end

if (@rpPrintReimbs > 0) set @rpPrintReimbs = @expTab
if (@rpPrintDirects > 0) set @rpPrintDirects = @expTab
if (@rpPrintIndirects > 0) set @rpPrintIndirects = @expTab
if (@rpPrintReimbCons > 0) set @rpPrintReimbCons = @conTab
if (@rpPrintDirectCons > 0) set @rpPrintDirectCons = @conTab
set @rpPrintUnits = 0
if (@rpPrintReimbs + @rpPrintDirects + @rpPrintIndirects + @rpPRintReimbCons + @rpPRintDirectCons > 0) set @rpPrintUnits = @untTab

set @etcDateString = CAST(@ETCDate as varchar(19));
set @temptable = '##rpProjDetExp_' + @rptWBSListKey;

IF OBJECT_ID('tempdb..' + @temptable) IS NOT NULL	
	Begin
			select @sqlQuery = 'DELETE FROM ' + @temptable  + ' '
			exec (@sqlQuery)
	End
Else
	Begin	

		/* 
		NOTE: @ETCDate should be the client date entered. down below, 1 day is added to the @ETCDate to get the actual start of etc calculations.
		So, if the client types 12/13/06 into the ETC date basis selection, 12/13/06 should be passed in here. From here on, it gets changed to 12/14/06.
		*/
		select @sqlQuery = 'CREATE TABLE ' + @temptable +  '
			  ( wbs1 Nvarchar(30) COLLATE database_default,
				wbs2 Nvarchar(30) COLLATE database_default,
				wbs3 Nvarchar(30) COLLATE database_default,
				laborcode Nvarchar(14) COLLATE database_default,
				recordtype char(1) COLLATE database_default,
				subtype char(1) COLLATE database_default,
				account Nvarchar(13) COLLATE database_default,
				vendor Nvarchar(20) COLLATE database_default,
				unit Nvarchar(11) COLLATE database_default,
				pctcompleteformula smallint,
				targetmultcost decimal(19,4),
				targetmultbill decimal(19,4),
				plannedrevenue decimal(19,4),
				baselinerevenue decimal(19,4),
				plannedhrs decimal (19,4),
				baselinehrs decimal(19,4),
				plannedcost decimal(19,4),
				plannedbill decimal (19,4),
				baselinecost decimal (19,4),
				baselinebill decimal (19,4),
				pctcmpplannedcost decimal (19,4),
				pctcmpplannedbill decimal (19,4),
				pctcmpbaselinecost decimal (19,4),
				pctcmpbaselinebill decimal (19,4),
				pctcmpplanhrscost decimal (19,4),
				pctcmpplanhrsbill decimal (19,4),
				pctcmpbasehrscost decimal (19,4),
				pctcmpbasehrsbill decimal (19,4),
				etcrev decimal (19,4),
				etchrs decimal (19,4),
				etccost decimal (19,4),
				etcbill decimal (19,4)
				primary key (wbs1,wbs2,wbs3,laborcode,recordtype,subtype,account,vendor,unit) 
			  )';
		exec (@sqlQuery);
	End


set @sqlQuery = 'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  insert into ' + @temptable + ' ' + char(13)+char(11)

set @sqlQuery = @sqlQuery + ' select subquery.wbs1, '
+ ' case when subquery.wbs2 = ''<none>'' then '' '' else subquery.wbs2 end wbs2, '
+ ' case when subquery.wbs3 = ''<none>'' then '' '' else subquery.wbs3 end wbs3,laborcode, '
+ ' subquery.recordtype, subquery.subtype, subquery.account, subquery.vendor, subquery.unit, max(subquery.pctcompleteformula) pctcompleteformula, '
+ ' max(subquery.targetmultcost) targetmultcost,max(subquery.targetmultbill) targetmultbill, '
+ ' sum(case when subquery.recordtype = ''3'' then case when subquery.reimbmethod = ''C'' then subquery.plancost else subquery.planbill end else 0 end) planrevenue, '
+ ' sum(case when subquery.recordtype = ''3'' then case when subquery.reimbmethod = ''C'' then subquery.basecost else subquery.basebill end else 0 end) baserevenue, '
+ ' sum(subquery.planhrs) planhrs,sum(subquery.basehrs) basehrs, '
+ ' sum(subquery.plancost) plancost,sum(subquery.planbill) planbill, '
+ ' sum(subquery.basecost) basecost,sum(subquery.basebill) basebill, '
+ ' sum(subquery.pctcmpplancost) pctcmpplancost, sum(subquery.pctcmpplanbill) pctcmpplanbill, '
+ ' sum(subquery.pctcmpbasecost) pctcmpbasecost, sum(subquery.pctcmpbasebill) pctcmpbasebill, '
+ ' sum(subquery.pctcmpplanhrscost) pctcmpplanhrscost, sum(subquery.pctcmpplanhrsbill) pctcmpplanhrsbill, '
+ ' sum(subquery.pctcmpbasehrscost) pctcmpbasehrscost, sum(subquery.pctcmpbasehrsbill) pctcmpbasehrsbill, '
+ ' sum(case when subquery.recordtype = ''3'' then case when subquery.reimbmethod = ''C'' then subquery.etcplancost else subquery.etcplanbill end else 0 end) etcrev, '
+ ' sum(subquery.etcplanhrs) etcplanhrs, '
+ ' sum(subquery.etcplancost) etcplancost, '
+ ' sum(subquery.etcplanbill) etcplanbill '

set @sqlQuery = @sqlQuery + ' from ( ' + char(13)+char(11)
+ '	  /* Consultant */ '
+ '	  	SELECT Isnull(t.wbs1, '''')											wbs1, '
+ '			Isnull(level2.wbs2, '' '')										wbs2, '
+ '			Isnull(level3.wbs3, '' '')										wbs3, '
+ '			Isnull(t.laborcode, '''')											laborcode, '
+ '			CASE WHEN res.directacctflg <> ''Y'' THEN ''5'' ELSE ''3'' END		recordtype, '
+ '			''0''																subtype, '
+ '			Isnull(res.account, '''')											account, '
+ '			Isnull(res.vendor, '''')											vendor, '
+ '			''''																unit, '
+ '			2																	PctcompleteFormula, '
+ '			max(p.targetmultcost)												targetmultcost, '
+ '			max(p.targetmultbill)												targetmultbill, '
+ '			max(p.reimbmethod)													reimbmethod, '
+ '			0																planhrs, '
+ '			0																basehrs, '
+ '			sum(planCalcCon.[PlannedCost])										plancost, '
+ '			sum(planCalcCon.[PlannedBill])										planbill, '
+ '			sum(planCalcCon.[BaselineCost])										basecost, '
+ '			sum(planCalcCon.[BaselineBill])										basebill, '
+ '			sum((planCalcCon.[CalcPctComplCost]/100) * planCalcCon.[PlannedCost])		pctcmpplancost, '
+ '			sum((planCalcCon.[CalcPctComplBill]/100) * planCalcCon.[PlannedBill])		pctcmpplanbill, '
+ '			sum((planCalcCon.[CalcPctComplCost]/100) * planCalcCon.[BaselineCost])		pctcmpbasecost, '
+ '			sum((planCalcCon.[CalcPctComplBill]/100) * planCalcCon.[BaselineBill])		pctcmpbasebill, '
+ '			0																pctcmpplanhrscost, '
+ '			0																pctcmpplanhrsbill, '
+ '			0																pctcmpbasehrscost, '
+ '			0																pctcmpbasehrsbill, '
+ '			0	                                                            etcplanhrs, '
+ '			sum(planCalcCon.[ETCCost])										etcplancost, '
+ '			sum(planCalcCon.[ETCBill])										etcplanbill '
+ '		FROM   rpplan p, '
+ '				rptask t '
+ '				LEFT OUTER JOIN pr AS level2 '
+ '							ON t.wbs1 = level2.wbs1 '
+ '								AND t.wbs2 = level2.wbs2 '
+ '								AND level2.wbs3 = '' '' '
+ '				LEFT OUTER JOIN pr AS level3 '
+ '							ON t.wbs1 = level3.wbs1 '
+ '								AND t.wbs2 = level3.wbs2 '
+ '								AND t.wbs3 = level3.wbs3, '
+ '				rpconsultant res, '
+ '				' + @workTable + ' wbs, '
+ '				' + @adapterTableCon + ' AS planCalcCon '
+ '		WHERE  p.planid = t.planid '
+ '				AND p.utilizationincludeflg = ''Y'' '
+ '				AND t.planid = res.planid '
+ '				AND t.taskid = res.taskid '
+ '				AND res.consultantid IS NOT NULL '
+ '				AND planCalcCon.planid = t.planid '
+ '				AND planCalcCon.taskid = t.taskid '
+ '			    AND Isnull(planCalcCon.account,'''') = Isnull(res.account,'''') '
+ '			    AND Isnull(planCalcCon.vendor,'''') = Isnull(res.vendor,'''') '
+ '				AND Isnull(t.wbs1, '''') > '''' '
+ ' and ((res.directacctflg = ''Y'' and ' + CAST(@rpPrintDirectCons as VARCHAR(1)) + ' <> 0) or (res.directacctflg <> ''Y'' and ' + CAST(@rpPrintReimbCons as VARCHAR(1)) + ' <> 0)) '
+ ' and ((' + CAST(@wbsActivityLevel as VARCHAR(1)) + '=3 and isnull(t.wbs1,'''') = wbs.wbs1 and isnull(level2.wbs2,'''') = wbs.wbs2 and isnull(level3.wbs3,'''') = wbs.wbs3) '
+ ' or (' + CAST(@wbsActivityLevel as VARCHAR(1)) + '=2 and isnull(t.wbs1,'''') = wbs.wbs1 and isnull(level2.wbs2,'''') = wbs.wbs2 and wbs.wbs3='''') '
+ ' or (' + CAST(@wbsActivityLevel as VARCHAR(1)) + '=1 and isnull(t.wbs1,'''') = wbs.wbs1 and wbs.wbs2='''' and wbs.wbs3='''')) '
+ ' and wbs.sessionId = ''' + @rptWBSListKey + ''' '
+ ' group by isnull(t.wbs1,''''),isnull(level2.wbs2,'' ''),isnull(level3.wbs3,'' ''),isnull(t.laborcode,''''),isnull(res.account,''''),isnull(res.vendor,'''') '
+ ' ,case when res.directacctflg <> ''Y'' then ''5'' else ''3'' end ' + char(13)+char(11)
+ ' UNION ALL '
+ '		/* Expenses */ '
+ '		SELECT Isnull(t.wbs1, '''')                                             wbs1, '
+ '			   Isnull(level2.wbs2, '' '')                                       wbs2, '
+ '			   Isnull(level3.wbs3, '' '')                                       wbs3, '
+ '			   Isnull(t.laborcode, '''')                                        laborcode, '
+ '			   CASE WHEN res.directacctflg <> ''Y'' THEN ''5'' ELSE ''3'' END       recordtype, '
+ '			   ''1''                                                            subtype, '
+ '			   Isnull(res.account, '''')                                        account, '
+ '			   Isnull(res.vendor, '''')                                         vendor, '
+ '			   ''''                                                             unit, '
+ '			   2																PctcompleteFormula, '
+ '			   max(p.targetmultcost)                                          targetmultcost, '
+ '			   max(p.targetmultbill)                                          targetmultbill, '
+ '			   max(p.reimbmethod)                                             reimbmethod, '
+ '			   0                                                              planhrs, '
+ '			   0                                                              basehrs, '
+ '			   sum(planCalcExp.[PlannedCost])                          plancost, '
+ '			   sum(planCalcExp.[PlannedBill])                          planbill, '
+ '			   sum(planCalcExp.[BaselineCost])						  basecost, '
+ '			   sum(planCalcExp.[BaselineBill])						  basebill, '
+ '			   sum((planCalcExp.[CalcPctComplCost]/100) * planCalcExp.[PlannedCost])		pctcmpplancost, '
+ '			   sum((planCalcExp.[CalcPctComplBill]/100) * planCalcExp.[PlannedBill])		pctcmpplanbill, '
+ '			   sum((planCalcExp.[CalcPctComplCost]/100) * planCalcExp.[BaselineCost])		pctcmpbasecost, '
+ '			   sum((planCalcExp.[CalcPctComplBill]/100) * planCalcExp.[BaselineBill])		pctcmpbasebill, '
+ '			   0                                                              pctcmpplanhrscost, '
+ '			   0                                                              pctcmpplanhrsbill, '
+ '			   0                                                              pctcmpbasehrscost, '
+ '			   0                                                              pctcmpbasehrsbill, '
+ '			   0                                                              etcplanhrs, '
+ '			   sum(planCalcExp.[ETCCost])										  etcplancost, '
+ '			   sum(planCalcExp.[ETCBill])										  etcplanbill '
+ '		FROM   rpplan p, '
+ '			   rptask t '
+ '			   LEFT OUTER JOIN pr AS level2 '
+ '							ON t.wbs1 = level2.wbs1 '
+ '							   AND t.wbs2 = level2.wbs2 '
+ '							   AND level2.wbs3 = '' '' '
+ '			   LEFT OUTER JOIN pr AS level3 '
+ '							ON t.wbs1 = level3.wbs1 '
+ '							   AND t.wbs2 = level3.wbs2 '
+ '							   AND t.wbs3 = level3.wbs3, '
+ '			   rpexpense res, '
+ '			   ' + @workTable + ' wbs, '
+ '			   ' + @adapterTableExp + ' AS planCalcExp '
+ '		WHERE  p.planid = t.planid '
+ '			   AND p.utilizationincludeflg = ''Y'' '
+ '			   AND t.planid = res.planid '
+ '			   AND t.taskid = res.taskid '
+ '			   AND res.expenseid IS NOT NULL '
+ '			   AND planCalcExp.planid = t.planid '
+ '			   AND planCalcExp.taskid = t.taskid '
+ '			   AND Isnull(planCalcExp.account,'''') = Isnull(res.account,'''') '
+ '			   AND Isnull(planCalcExp.vendor,'''') = Isnull(res.vendor,'''') '
+ '			   AND Isnull(t.wbs1, '''') > '''' '
+ ' and ((' + CAST(@wbsActivityLevel as VARCHAR(1)) + '=3 and isnull(t.wbs1,'''') = wbs.wbs1 and isnull(level2.wbs2,'''') = wbs.wbs2 and isnull(level3.wbs3,'''') = wbs.wbs3) '
+ ' or (' + CAST(@wbsActivityLevel as VARCHAR(1)) + '=2 and isnull(t.wbs1,'''') = wbs.wbs1 and isnull(level2.wbs2,'''') = wbs.wbs2 and wbs.wbs3='''') '
+ ' or (' + CAST(@wbsActivityLevel as VARCHAR(1)) + '=1 and isnull(t.wbs1,'''') = wbs.wbs1 and wbs.wbs2='''' and wbs.wbs3='''')) '
+ ' and ((res.directacctflg = ''Y'' and (' + CAST(@rpPrintDirects as VARCHAR(1)) + ' <> 0 or ' + CAST(@rpPrintIndirects as VARCHAR(1)) + ' <> 0)) or (res.directacctflg <> ''Y'' and (' + CAST(@rpPrintReimbs as VARCHAR(1)) + ' <> 0))) '
+ ' and wbs.sessionId = ''' + @rptWBSListKey + ''' '  + char(13)+char(11)
+ ' group by isnull(t.wbs1,''''),isnull(level2.wbs2,'' ''),isnull(level3.wbs3,'' ''),isnull(t.laborcode,''''),isnull(res.account,''''),isnull(res.vendor,'''') '
+ ' ,case when res.directacctflg <> ''Y'' then ''5'' else ''3'' end ' + char(13)+char(11)
set @sqlQuery = @sqlQuery + '  ' 
+ ' ) subquery ' 
+ ' group by subquery.wbs1, ' 
+ ' case when subquery.wbs2 = ''<none>'' then '' '' else subquery.wbs2 end, ' 
+ ' case when subquery.wbs3 = ''<none>'' then '' '' else subquery.wbs3 end, subquery.laborcode, subquery.recordtype, subquery.subtype, subquery.account, subquery.vendor, subquery.unit ';

print substring(@sqlQuery, 1, 8000)
print substring(@sqlQuery, 8001, 8000)
print substring(@sqlQuery, 16001, 8000)
print substring(@sqlQuery, 24001, 8000)
exec(@sqlQuery);

/*
select * from @t
order by wbs1,wbs2,wbs3
*/

end
GO
