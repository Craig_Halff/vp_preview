SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteCustomProposal] @CustomPropID varchar(32)
AS
    delete from CustomProposalClient where CustomPropID = @CustomPropID
    delete from CustomProposalClientGraphics where CustomPropID = @CustomPropID
    delete from CustomProposalContact where CustomPropID = @CustomPropID
    delete from CustomProposalEmployee where CustomPropID = @CustomPropID
    delete from CustomProposalEmployeeGraphics where CustomPropID = @CustomPropID
    --delete from CustomProposalOpportunity where CustomPropID = @CustomPropID
    --delete from CustomProposalOpportunityGraphics where CustomPropID = @CustomPropID
    delete from CustomProposalProject where CustomPropID = @CustomPropID
    delete from CustomProposalProjectGraphics where CustomPropID = @CustomPropID
    delete from CustomProposalProjTeam where CustomPropID = @CustomPropID
    delete from CustomProposalTextLibrary where CustomPropID = @CustomPropID
    delete from CustomProposalTextLibGraphics where CustomPropID = @CustomPropID
    delete from CustomProposalVendor where CustomPropID = @CustomPropID
    delete from CustomProposalVendorGraphics where CustomPropID = @CustomPropID
    delete from CustomProposalMktCampaign where CustomPropID = @CustomPropID
    delete from CustomProposalMktCampaignGraphics where CustomPropID = @CustomPropID
    delete from CustomProposalOrder where CustomPropID = @CustomPropID
GO
