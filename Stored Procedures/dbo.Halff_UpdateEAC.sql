SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Halff_UpdateEAC]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @tempTable TABLE (
	  WBS1 varchar(30) COLLATE database_default,
	  WBS2 varchar(7) COLLATE database_default,
	  Writeoff decimal(19,5),
	  EAC decimal(19,5),
	  active_level varchar(255) COLLATE database_default
	  PRIMARY KEY (WBS1, WBS2)
	)
	
	INSERT INTO @tempTable
		SELECT [prj_code] AS [WBS1]
		,[phase_code] AS [WBS2]
		,ISNULL([writeoff_effort], 0) AS [Writeoff]
		,ISNULL([eac], 0) AS EAC
		,ISNULL([active_level], '') AS [InputLevel]
		FROM [Backlog].[dbo].[EstimatedEffort_RevenueCalc]
		ORDER BY [prj_code], [phase_code]

	-- Update the project custom fields for EAC, Active level, and Writeoff
	Update p
	Set p.CustEffortatCompletion = FORMAT(b.EAC, '#,0;(#,0)'),
		p.CustEACInputLevel = b.active_level,
		p.CustWriteoffAdjmt = FORMAT(b.Writeoff, '#,0;(#,0)')
	From [ProjectCustomTabFields] p, @tempTable b
	Where p.WBS1 = b.WBS1
		and p.WBS2 = b.WBS2
		and p.WBS3 = ' '


	/*
	-- Update the EAC from the Backlog application
	DECLARE @tempTable TABLE (
	  WBS1 varchar(30) COLLATE database_default,
	  WBS2 varchar(7) COLLATE database_default,
	  Writeoff decimal(19,5),
	  EAC decimal(19,5),
	  active_level varchar(255) COLLATE database_default
	  PRIMARY KEY (WBS1, WBS2)
	)
	
	INSERT INTO @tempTable
	SELECT A.[prj_code] AS [WBS1]
	,A.[phase_code] AS [WBS2]
	,ISNULL(A.[writeoff_effort], 0) AS [Writeoff]
	,ISNULL(A.[EAC], 0) AS EAC
	,ISNULL(B.[active_level], '') AS [InputLevel]
	FROM
	(
		SELECT A.*
		FROM 
		(
			SELECT [prj_code]
			,[phase_code]
			,[writeoff_effort] 
			,[EAC]
			,[period_end]
			FROM [Backlog].[dbo].[EstimatedEffort_Data]
		) AS A 
		INNER JOIN
		(
			SELECT [prj_code]
			,MAX([period_end]) AS [max_period_end]
			FROM [Backlog].[dbo].[EstimatedEffort_Data]
			WHERE prj_code IN 
			(
				SELECT [WBS1]
				FROM [HalffImport].[dbo].[PR]
				WHERE [WBS2]='' 
				AND [WBS3]='' 
				AND [Status]='A'
			)
			GROUP BY [prj_code]
		) AS B
		ON B.[prj_code] = A.[prj_code]
		AND B.[max_period_end] = A.[period_end]
	) AS A
	INNER JOIN
	(
		SELECT A.[prj_code]
		,A.[active_level]
		FROM 
		(
			SELECT [prj_code]
			,[active_level]
			,[period_end]
			FROM [Backlog].[dbo].[EstimatedEffort_Data]
			WHERE phase_code='' 
			AND task_code=''
		) AS A 
		INNER JOIN
		(
			SELECT [prj_code]
			,MAX([period_end]) AS [max_period_end]
			FROM [Backlog].[dbo].[EstimatedEffort_Data]
			WHERE prj_code IN 
			(
				SELECT [WBS1]
				FROM [HalffImport].[dbo].[PR]
				WHERE [WBS2]='' 
				AND [WBS3]='' 
				AND [Status]='A'
			)
			GROUP BY [prj_code]
		) AS B
		ON B.[prj_code] = A.[prj_code]
		AND B.[max_period_end] = A.[period_end]
	) AS B
	ON B.[prj_code] = A.[prj_code]
	ORDER BY A.[prj_code], A.[phase_code]

	-- Update the project custom fields for EAC, Active level, and Writeoff
	Update p
	Set p.CustEffortatCompletion = FORMAT(b.EAC, '#,0;(#,0)'),
		p.CustEACInputLevel = b.active_level,
		p.CustWriteoffAdjmt = FORMAT(b.Writeoff, '#,0;(#,0)')
	From [ProjectCustomTabFields] p, @tempTable b
	Where p.WBS1 = b.WBS1
		and p.WBS2 = b.WBS2
		and p.WBS3 = ' '

	*/
END
GO
