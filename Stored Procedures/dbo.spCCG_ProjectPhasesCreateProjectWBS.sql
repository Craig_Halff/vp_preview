SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   PROCEDURE [dbo].[spCCG_ProjectPhasesCreateProjectWBS] @WBS1 VARCHAR(30)
AS
/*
Copyright 2021 Central Consulting Group.   All rights reserved.
08/16/2021	David Springer
09/22/2021	David Springer
			Added Estimated Revenue fields from standard fee fields
10/18/2021  Craig H. anderson
			Added Contract Award Date updates
10/18/2021	David Springer
			Added Est. Start Date & Est. Completion Date
10/26/2021  Jeremy Baummer
			Added Est. Start Date & Est. Completion Date to Task Level
*/

Declare @BillingType varchar(255), @ExpenseType varchar(255), @WBS2 varchar(7), @WBS3 varchar(7), @Fee float, @ConsultFee float, @ReimbAllow float;

    BEGIN
        Set NoCount On;

        -- Populate Documents Grid
        Insert Into dbo.Projects_FinalInvPkgUserDocuments
            (
                WBS1
              , WBS2
              , WBS3
              , Seq
              , CustOrder
              , CustDocType
              , CustDocument
            )
        Values
        (@WBS1, ' ', ' ', Replace(NewId(), '-', ''), 1, 'Template', 'Invoice');

        -- EnLink Mileage Multiplier
        Update px
        Set px.CustReimbMileageMultiplier = 1.1009
        From dbo.PR p, dbo.ProjectCustomTabFields px
        Where
            p.WBS1 = @WBS1
            And p.WBS1 = px.WBS1
            And p.WBS2 = px.WBS2
            And p.WBS3 = px.WBS3
            And p.BillingClientID = '001776'; -- EnLink

        Update p
        Set p.UnitTable = Case When Left(p.Org, (
                                                    Select Org1Length From dbo.CFGFormat
                                                )) = '01'
                                    And p.ClientID <> '000301' Then '000001 - STANDARD UNIT TABLE'
                              When Left(p.Org, (
                                                   Select Org1Length From dbo.CFGFormat
                                               )) = '01'
                                   And p.ClientID = '000301' Then 'ATMOS MASTER'
                              When Left(p.Org, (
                                                   Select Org1Length From dbo.CFGFormat
                                               )) = '02' Then '000002-GLD STANDARD UNIT TABLE'
                              Else p.UnitTable
                          End
          , p.RequireComments = Case When p.ClientID = '000301'
                                          Or Left(p.Org, (
                                                             Select Org1Length From dbo.CFGFormat
                                                         )) = '03' Then 'Y'
                                    Else p.RequireComments
                                End
        From dbo.PR p
        Where p.WBS1 = @WBS1;

        IF EXISTS (SELECT * FROM dbo.Projects_Phases WHERE WBS1 = @WBS1)
            BEGIN
                Update dbo.PR Set SubLevel = 'Y' Where WBS1 = @WBS1 And WBS2 = ' ';

                --	Create phases from grid
                Insert Into dbo.PR
                    (
                        WBS1
                      , WBS2
                      , WBS3
                      , ProposalWBS1
                      , Name
                      , LongName
                      , FeeDirLab
                      , FeeDirExp
                      , Fee
                      , ConsultFee
                      , ReimbAllowCons
                      , ReimbAllowExp
                      , ReimbAllow
                      , ProjectCurrencyCode
                      , BillingCurrencyCode
                      , UnitTable
                      , ChargeType
                      , Org
                      , SubLevel
                      , Status
                      , Principal
                      , ProjMgr
                      , Supervisor
                      , ClientID
                      , CLAddress
                      , ContactID
                      , BillingClientID
                      , CLBillingAddr
                      , BillingContactID
                      , RevType
                      , RevType2
                      , RevType3
                      , RevenueMethod
                      , RequireComments
                      , AvailableForCRM
                      , ReadyForApproval
                      , ReadyForProcessing
					  , EstStartDate
					  , EstCompletionDate
                    )
                Select p.WBS1
                     , t.CustPhaseCode
                     , ' ' As WBS3
                     , p.ProposalWBS1
                     , Left(t.CustPhaseName, 40)
                     , t.CustPhaseName
                     , t.CustPhaseFeeDirLab
                     , Iif(t.CustPhaseBillingType Like 'Lump%', t.CustPhaseExpenseFee, 0) As DirExp
                     , t.CustPhaseFeeDirLab + Iif(t.CustPhaseBillingType Like 'Lump%', t.CustPhaseExpenseFee, 0) As Fee
                     , Iif(t.CustPhaseBillingType Like 'Lump%', t.CustPhaseConsultantFee, 0) As DirCons
                     , Iif(t.CustPhaseBillingType Like 'Cost Plus%', t.CustPhaseExpenseFee, 0) As ReimbExp
                     , Iif(t.CustPhaseBillingType Like 'Cost Plus%', t.CustPhaseConsultantFee, 0) As ReimbCons
                     , Iif(t.CustPhaseBillingType Like 'Cost Plus%', t.CustPhaseExpenseFee, 0) + Iif(t.CustPhaseBillingType Like 'Cost Plus%', t.CustPhaseConsultantFee, 0) As Reimb
                     , p.ProjectCurrencyCode
                     , p.BillingCurrencyCode
                     , p.UnitTable
                     , p.ChargeType
                     , p.Org
                     , 'N' As SubLevel
                     , 'A' As Status
                     , p.Principal
                     , p.ProjMgr
                     , p.Supervisor
                     , p.ClientID
                     , p.CLAddress
                     , p.ContactID
                     , p.BillingClientID
                     , p.CLBillingAddr
                     , p.BillingContactID
                     , p.RevType
                     , p.RevType2
                     , p.RevType3
                     , p.RevenueMethod
                     , p.RequireComments
                     , p.AvailableForCRM
                     , p.ReadyForApproval
                     , p.ReadyForProcessing
					 , t.CustPhaseStartDate
					 , t.CustPhaseEndDate
                From dbo.Projects_Phases t, dbo.PR p
                Where
                    t.WBS1 = @WBS1
                    And t.WBS2 = ' '
                    And t.WBS1 = p.WBS1
                    And t.WBS2 = p.WBS2
                    And t.CustPhaseCode Is Not Null
                    And t.CustPhaseTotal <> 0;

                --	Add Custom Phase records
                Insert Into dbo.ProjectCustomTabFields
                    (
                        WBS1
                      , WBS2
                      , WBS3
                      , CustManagementLeader
                      , CustPracticeArea
                      , CustPracticeAreaLeader
                      , CustDirector
                      , CustAdministrativeAssistant
                      , CustBillingType
                      , CustLaborFringeMultiplier
                      , CustLaborOvhProfitMultiplier
                      , CustReimbExpenseMultiplier
                      , CustReimbConsultantMultiplier
                      , CustReimbMileageMultiplier
                      , CustTaxCode
                      , CustUnitTableAllLevels
					  , CustEstStartDate
					  , CustEstCompletionDate
                    )
                Select @WBS1
                     , t.CustPhaseCode As WBS2
                     , ' ' As WBS3
                     , px.CustManagementLeader
                     , px.CustPracticeArea
                     , px.CustPracticeAreaLeader
                     , px.CustDirector
                     , px.CustAdministrativeAssistant
                     , Case When px.CustBillingType = 'Sublevel Terms' Then t.CustPhaseBillingType Else Null End As BillingType
                     , Case When px.CustBillingType = 'Sublevel Terms'
                                 And IsNull(t.CustPhaseBillingType, 'Non-Billable') <> 'Non-Billable' Then px.CustLaborFringeMultiplier
                           Else 0
                       End
                     , Case When px.CustBillingType = 'Sublevel Terms'
                                 And IsNull(t.CustPhaseBillingType, 'Non-Billable') <> 'Non-Billable' Then px.CustLaborOvhProfitMultiplier
                           Else 0
                       End
                     , Case When px.CustBillingType = 'Sublevel Terms'
                                 And IsNull(t.CustPhaseBillingType, 'Non-Billable') <> 'Non-Billable' Then px.CustReimbExpenseMultiplier
                           Else 0
                       End
                     , Case When px.CustBillingType = 'Sublevel Terms'
                                 And IsNull(t.CustPhaseBillingType, 'Non-Billable') <> 'Non-Billable' Then px.CustReimbConsultantMultiplier
                           Else 0
                       End
                     , Case When px.CustBillingType = 'Sublevel Terms'
                                 And IsNull(t.CustPhaseBillingType, 'Non-Billable') <> 'Non-Billable' Then px.CustReimbMileageMultiplier
                           Else 0
                       End
                     , Case When px.CustBillingType = 'Sublevel Terms'
                                 And t.CustPhaseTax = 'Y' Then px.CustTaxCode
                           Else Null
                       End As TaxCode
                     , 'Y' As UnitTableAllLevels
					 , t.CustPhaseStartDate
					 , t.CustPhaseEndDate
                From dbo.Projects_Phases t, dbo.ProjectCustomTabFields px
                Where
                    t.WBS1 = @WBS1
                    And t.WBS2 = ' '
                    And t.WBS1 = px.WBS1
                    And t.WBS2 = px.WBS2
                    And t.CustPhaseCode Is Not Null
                    And t.CustPhaseTotal <> 0;

                --	Create tasks from grid
                Insert Into dbo.PR
                    (
                        WBS1
                      , WBS2
                      , WBS3
                      , ProposalWBS1
                      , Name
                      , LongName
                      , FeeDirLab
                      , FeeDirExp
                      , Fee
                      , ConsultFee
                      , ReimbAllowCons
                      , ReimbAllowExp
                      , ReimbAllow
                      , ProjectCurrencyCode
                      , BillingCurrencyCode
                      , UnitTable
                      , ChargeType
                      , Org
                      , SubLevel
                      , Status
                      , Principal
                      , ProjMgr
                      , Supervisor
                      , ClientID
                      , CLAddress
                      , ContactID
                      , BillingClientID
                      , CLBillingAddr
                      , BillingContactID
                      , RevType
                      , RevType2
                      , RevType3
                      , RevenueMethod
                      , RequireComments
                      , AvailableForCRM
                      , ReadyForApproval
                      , ReadyForProcessing
				--Added Est Start & Completion
					  , EstStartDate
					  , EstCompletionDate
                    )
                Select p.WBS1
                     , t.CustTaskPhaseCode
                     , t.CustTaskCode As WBS3
                     , p.ProposalWBS1
                     , Left(t.CustTaskName, 40)
                     , t.CustTaskName
                     , t.CustTaskFeeDirLab
                     , Iif(pp.CustPhaseBillingType Like 'Lump%', t.CustTaskExpenseFee, 0) As DirExp
                     , t.CustTaskFeeDirLab + Iif(pp.CustPhaseBillingType Like 'Lump%', t.CustTaskExpenseFee, 0) As Fee
                     , Iif(pp.CustPhaseBillingType Like 'Lump%', t.CustTaskConsultantFee, 0) As DirCons
                     , Iif(pp.CustPhaseBillingType Like 'Cost Plus%', t.CustTaskExpenseFee, 0) As ReimbExp
                     , Iif(pp.CustPhaseBillingType Like 'Cost Plus%', t.CustTaskConsultantFee, 0) As ReimbCons
                     , Iif(pp.CustPhaseBillingType Like 'Cost Plus%', t.CustTaskExpenseFee, 0) + Iif(pp.CustPhaseBillingType Like 'Cost Plus%', t.CustTaskConsultantFee, 0) As Reimb
                     , p.ProjectCurrencyCode
                     , p.BillingCurrencyCode
                     , p.UnitTable
                     , p.ChargeType
                     , p.Org
                     , 'N' As SubLevel
                     , 'A' As Status
                     , p.Principal
                     , p.ProjMgr
                     , p.Supervisor
                     , p.ClientID
                     , p.CLAddress
                     , p.ContactID
                     , p.BillingClientID
                     , p.CLBillingAddr
                     , p.BillingContactID
                     , p.RevType
                     , p.RevType2
                     , p.RevType3
                     , p.RevenueMethod
                     , p.RequireComments
                     , p.AvailableForCRM
                     , p.ReadyForApproval
                     , p.ReadyForProcessing
				-- Grabbing Est Start and Comp from phase level
					 , pp.CustPhaseStartDate
					 , pp.CustPhaseEndDate
                From dbo.Projects_Tasks t, dbo.Projects_Phases pp, dbo.PR p
                Where
                    t.WBS1 = @WBS1
                    And t.WBS2 = ' '
                    And t.WBS1 = pp.WBS1
                    And t.WBS1 = p.WBS1
                    And t.WBS2 = p.WBS2
                    And t.CustTaskPhaseCode = pp.CustPhaseCode
                    And t.CustTaskCode Is Not Null
                    And t.CustTaskTotal <> 0;

                --	Add Custom Task records
                Insert Into dbo.ProjectCustomTabFields
                    (
                        WBS1
                      , WBS2
                      , WBS3
                      , CustFunctionalArea
                      , CustManagementLeader
                      , CustPracticeArea
                      , CustPracticeAreaLeader
                      , CustDirector
                      , CustAdministrativeAssistant
                      , CustUnitTableAllLevels
					--Adding Cust Task Est Start and comp
					  , CustEstStartDate
					  , CustEstCompletionDate
                    )
                Select @WBS1
                     , t.CustTaskPhaseCode As WBS2
                     , t.CustTaskCode As WBS3
                     , t.CustTaskFunctionalArea
                     , px.CustManagementLeader
                     , px.CustPracticeArea
                     , px.CustPracticeAreaLeader
                     , px.CustDirector
                     , px.CustAdministrativeAssistant
                     , 'Y' As UnitTableAllLevels
					--Adding Est Start and Comp from phases
					 , pp.CustPhaseStartDate
					 , pp.CustPhaseEndDate
                From dbo.Projects_Tasks t, dbo.ProjectCustomTabFields px, dbo.Projects_Phases pp
                Where
                    t.WBS1 = @WBS1
                    And t.WBS2 = ' '
                    And t.WBS1 = px.WBS1
					And t.WBS2 = px.WBS2
					And t.CustTaskPhaseCode = pp.CustPhaseCode
                    And t.CustTaskCode Is Not Null
                    And t.CustTaskTotal <> 0
					And t.WBS1 = pp.WBS1
                    And t.WBS2 = pp.WBS2;

			--	Milestones  -- Phase Level
				Insert Into PRMilestone
				(RecordID, WBS1, WBS2, WBS3, Code, EndDate)
                Select Replace (NewID(), '-', ''), @WBS1, t.CustPhaseCode, ' ' WBS3, 'SysEstStart', t.CustPhaseStartDate
                From dbo.Projects_Phases t
                Where t.WBS1 = @WBS1
                    And t.WBS2 = ' '
                    And t.CustPhaseStartDate Is Not Null
					and t.CustPhaseTotal <> 0
				UNION
                Select Replace (NewID(), '-', ''), @WBS1, t.CustPhaseCode, ' ' WBS3, 'SysEstCompletion', t.CustPhaseEndDate
                From dbo.Projects_Phases t
                Where t.WBS1 = @WBS1
                    And t.WBS2 = ' '
                    And t.CustPhaseEndDate Is Not Null
					and t.CustPhaseTotal <> 0

			--	Milestones  -- Task Level
				Insert Into PRMilestone
				(RecordID, WBS1, WBS2, WBS3, Code, EndDate)
                Select Replace (NewID(), '-', ''), @WBS1, t.CustTaskPhaseCode, ' ' WBS3, 'SysEstStart', p.CustPhaseStartDate
                From dbo.Projects_Tasks t
					INNER JOIN dbo.Projects_Phases p
						ON p.WBS1 = t.WBS1
						AND p.CustPhaseCode = t.CustTaskPhaseCode
				WHERE   t.WBS1 = @WBS1
						And p.CustPhaseEndDate Is NOT Null
						AND p.CustPhaseTotal <> 0
				UNION
                Select Replace (NewID(), '-', ''), @WBS1, t.CustTaskPhaseCode, t.CustTaskCode, 'SysEstCompletion', p.CustPhaseEndDate
                From dbo.Projects_Tasks t
					INNER JOIN dbo.Projects_Phases p
						ON p.WBS1 = t.WBS1
						AND p.CustPhaseCode = t.CustTaskPhaseCode
				WHERE   t.WBS1 = @WBS1
						And p.CustPhaseEndDate Is NOT Null
						AND p.CustPhaseTotal <> 0

                --	Rollup task fees to phase & set Sublevel flag
                Update p
                Set p.FeeDirLab = t.FeeDirLab
                  , p.FeeDirExp = t.FeeDirExp
                  , p.Fee = t.Fee
                  , p.ConsultFee = t.ConsultFee
                  , p.ReimbAllowCons = t.ReimbAllowCons
                  , p.ReimbAllowExp = t.ReimbAllowExp
                  , p.ReimbAllow = t.ReimbAllow
                  , p.SubLevel = 'Y'
                From dbo.PR p
                   , (
                         Select WBS1
                              , WBS2
                              , IsNull(Sum(FeeDirLab), 0) As FeeDirLab
                              , IsNull(Sum(FeeDirExp), 0) As FeeDirExp
                              , IsNull(Sum(Fee), 0) As Fee
                              , IsNull(Sum(ConsultFee), 0) As ConsultFee
                              , IsNull(Sum(ReimbAllowCons), 0) As ReimbAllowCons
                              , IsNull(Sum(ReimbAllowExp), 0) As ReimbAllowExp
                              , IsNull(Sum(ReimbAllow), 0) As ReimbAllow
                         From dbo.PR
                         Where
                             WBS3 <> ' ' -- Task records
                             And FeeDirLab + FeeDirExp + ConsultFee + ReimbAllowCons + ReimbAllowExp <> 0
                         Group By
                             WBS1, WBS2
                     ) t -- Group by Phase
                Where
                    p.WBS1 = @WBS1
                    And p.WBS2 <> ' ' -- Phase level
                    And p.WBS3 = ' ' -- Phase level only
                    And p.WBS1 = t.WBS1
                    And p.WBS2 = t.WBS2;

                --	Rollup phase fees to project & set Sublevel flag
                Update p
                Set p.FeeDirLab = t.FeeDirLab
                  , p.FeeDirExp = t.FeeDirExp
                  , p.Fee = t.Fee
                  , p.ConsultFee = t.ConsultFee
                  , p.ReimbAllowCons = t.ReimbAllowCons
                  , p.ReimbAllowExp = t.ReimbAllowExp
                  , p.ReimbAllow = t.ReimbAllow
                  , p.SubLevel = 'Y'
                From dbo.PR p
                   , (
                         Select WBS1
                              , IsNull(Sum(FeeDirLab), 0) As FeeDirLab
                              , IsNull(Sum(FeeDirExp), 0) As FeeDirExp
                              , IsNull(Sum(Fee), 0) As Fee
                              , IsNull(Sum(ConsultFee), 0) As ConsultFee
                              , IsNull(Sum(ReimbAllowCons), 0) As ReimbAllowCons
                              , IsNull(Sum(ReimbAllowExp), 0) As ReimbAllowExp
                              , IsNull(Sum(ReimbAllow), 0) As ReimbAllow
                         From dbo.PR
                         Where
                             WBS2 <> ' ' -- Phase records
                             And WBS3 = ' ' -- Exclude tasks
                         Group By WBS1
                     ) t -- Group by Project
                Where
                    p.WBS1 = @WBS1
                    And p.WBS1 = t.WBS1
                    And p.WBS2 = ' '; -- Project level only

		--	Set Billing & Functional Currency at all levels
			Update dbo.PR
			Set FeeDirLabBillingCurrency = FeeDirLab
			  , FeeDirLabFunctionalCurrency = FeeDirLab
			  , FeeDirExpBillingCurrency = FeeDirExp
			  , FeeDirExpFunctionalCurrency = FeeDirExp
			  , FeeBillingCurrency = Fee
			  , FeeFunctionalCurrency = Fee
			  , ConsultFeeBillingCurrency = ConsultFee
			  , ConsultFeeFunctionalCurrency = ConsultFee
			  , ReimbAllowConsBillingCurrency = ReimbAllowCons
			  , ReimbAllowConsFunctionalCurrency = ReimbAllowCons
			  , ReimbAllowExpBillingCurrency = ReimbAllowExp
			  , ReimbAllowExpFunctionalCurrency = ReimbAllowExp
			  , ReimbAllowBillingCurrency = ReimbAllow
			  , ReimbAllowFunctionalCurrency = ReimbAllow
			Where WBS1 = @WBS1;

		--	Update Estimated Revenue from standard fee fields
			Update p
			Set p.Revenue = p.FeeDirExp + p.FeeDirLab + p.ConsultFee + p.ReimbAllowCons + p.ReimbAllowExp,
				p.WeightedRevenue = (p.FeeDirExp + p.FeeDirLab + p.ConsultFee + p.ReimbAllowCons + p.ReimbAllowExp) * p.Probability / 100
			From PR p, ProjectCustomTabFields px
			Where p.WBS1 = @WBS1
			  and p.WBS2 = ' '
			  and p.WBS1 = px.WBS1
			  and p.WBS2 = px.WBS2

			Update px
			Set px.CustEstimatedLaborFee = p.FeeDirLab,
				px.CustEstimatedExpenseFee = p.FeeDirExp + p.ReimbAllowExp,
				px.CustEstimatedConsultantFee = p.ConsultFee + p.ReimbAllowCons
			From PR p, ProjectCustomTabFields px
			Where p.WBS1 = @WBS1
			  and p.WBS2 = ' '
			  and p.WBS1 = px.WBS1
			  and p.WBS2 = px.WBS2

			--	Create Billing Terms
			Declare curBillingTypeWBS Insensitive Cursor For
			Select WBS2, WBS3, CustBillingType
			From dbo.ProjectCustomTabFields
			Where
				WBS1 = @WBS1
				And IsNull(CustBillingType, 'Non-Billable') <> 'Non-Billable'
			Order By
				1, 2;

			Open curBillingTypeWBS;
			Fetch Next From curBillingTypeWBS
			Into @WBS2, @WBS3, @BillingType;
			WHILE @@FETCH_STATUS = 0
				BEGIN

					Exec dbo.spCCG_ProjectBillingTermsInsert @WBS1, @WBS2, @WBS3, @BillingType;

					FETCH NEXT FROM curBillingTypeWBS
					INTO @WBS2, @WBS3, @BillingType;

				END;
			CLOSE curBillingTypeWBS;
			DEALLOCATE curBillingTypeWBS;

			--	Create Contract Management grids
			EXEC dbo.spCCG_ProjectContractMgmtInsert @WBS1;

			--	Create Sales grid with Contract No = Original
			INSERT INTO dbo.Projects_Sales
				(
					WBS1
				  , WBS2
				  , WBS3
				  , Seq
				  , CustSalesContract
				  , CustSalesDate
				  , CustSalesTeam
				  , CustSalesAmount
				)
			SELECT WBS1, ' ', ' ', REPLACE(NEWID(), '-', ''), '000', GETDATE(), CustTeamOrg, SUM (CustTeamLaborFee)
			FROM dbo.Projects_CustPhaseTeams
			WHERE
				WBS1 = @WBS1
				AND WBS2 = ' '
			GROUP BY WBS1, CustTeamOrg;

			EXEC dbo.HAI_ProjectAddToProjectStatus @myWBS1 = @WBS1

			DELETE FROM dbo.Projects_Tasks WHERE WBS1 = @WBS1;
			DELETE FROM dbo.Projects_CustPhaseTeams WHERE WBS1 = @WBS1;
			DELETE FROM dbo.Projects_Phases WHERE WBS1 = @WBS1;

			-- Set Contract Awarded date in Milestones
			DECLARE @today DATE = GETDATE();
			EXEC dbo.HAI_ProjectMilestoneDateSync @WBS1 , @Field = 'SysContract', @myDate = @today;
			UPDATE dbo.PR SET ContractDate = @today WHERE WBS1 = @WBS1 AND WBS2 = ' '
			UPDATE dbo.ProjectCustomTabFields SET CustContractAwarded = @today WHERE WBS1 = @WBS1 AND WBS2 = ' '

			UPDATE ProjectCustomTabFields 
			SET CustSetupLaborFringeMultiplier = 0,
				CustSetupLaborOverheadProfitMultiplier = 0,
				CustSetupReimbExpenseMultiplier = 1,
				CustSetupReimbConsultantMultiplier = 1
			WHERE WBS1 = @WBS1

            END; -- Phases exist
    END;
GO
