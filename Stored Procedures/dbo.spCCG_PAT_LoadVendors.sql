SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_LoadVendors] (
	@VendorDisplayFormat	Nvarchar(max),
	@Company				Nvarchar(14),
	@UnionAlias				bit = 1,
	@ApprovedOnly			int = 1
) AS
BEGIN
	-- EXEC [spCCG_PAT_LoadVendors] 'VE.Vendor + '' - '' + Name',null,1,0
	-- EXEC [spCCG_PAT_LoadVendors] 'Name + '' - '' + VE.Vendor','20',1,1
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	DECLARE @sql Nvarchar(max)
	DECLARE @orderByName bit = 0
	DECLARE @displayIsCodeOnly bit = 0

	Declare @MultiCompanyEnabled char(1)
	select @MultiCompanyEnabled = MultiCompanyEnabled from FW_CFGSystem

	if @MultiCompanyEnabled = 'Y' and ISNULL(@Company,'') = ''
		SET @ApprovedOnly = -1--can only do approved only when we have a company

	IF LEFT(ISNULL(@VendorDisplayFormat,'VE.Vendor'),4) = 'Name'
		SET @orderByName = 1

	IF ISNULL(@VendorDisplayFormat,'VE.Vendor') = 'VE.Vendor'
		SET @displayIsCodeOnly = 1

	SET @sql = N'
		select VE.Vendor, ' + ISNULL(@VendorDisplayFormat,'VE.Vendor') + (CASE WHEN @ApprovedOnly =0 THEN ' + case when ISNULL(vea.Vendor,''N'') = ''N'' or ReadyForProcessing = ''N'' then '' (!)'' else '''' end ' ELSE '' END) + '
			from VE ' +
			(CASE WHEN @ApprovedOnly =0 THEN '	left join VEAccounting vea on ve.vendor = vea.vendor and vea.company = ''' + ISNULL(@Company,' ') + ''''
				WHEN @ApprovedOnly = 1 THEN '	inner join VEAccounting vea on ve.vendor = vea.vendor and ReadyForProcessing = ''Y'' and vea.company = ''' + ISNULL(@Company,' ') + ''''
				ELSE '' END) + '
			where Status <> ''D'' and VE.Vendor is not null ' +
		(CASE WHEN @UnionAlias=1 and @displayIsCodeOnly = 0 THEN '
		UNION ALL select VE.Vendor, Alias ' + (CASE WHEN @ApprovedOnly =0 THEN ' + case when ISNULL(vea.Vendor,''N'') = ''N''  or ReadyForProcessing = ''N'' then '' (!)'' else '''' end ' ELSE '' END) + '
			from VendorAlias a
				inner join VE on VE.Vendor = a.Vendor and VE.Status <> ''D''' +
			(CASE WHEN @ApprovedOnly =0 THEN '	left join VEAccounting vea on ve.vendor = vea.vendor and vea.company = ''' + ISNULL(@Company,' ') + ''''
				WHEN @ApprovedOnly = 1 THEN '	inner join VEAccounting vea on ve.vendor = vea.vendor and ReadyForProcessing = ''Y'' and vea.company = ''' + ISNULL(@Company,' ') + ''''
				ELSE '' END) + '
			where Status <> ''D'' and VE.Vendor is not null '
		ELSE ''
		END) +
		'order by ' + (CASE WHEN @orderByName = 1 THEN '2' ELSE '1' END)

		--print @sql
	EXEC(@sql)
END
GO
