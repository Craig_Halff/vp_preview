SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_Config_Load_Settings]
	@isEIUser	bit,
	@user		Nvarchar(32)
AS
BEGIN
	-- Copyright (c) 2020 EleVia Software.  All rights reserved. 
	/*
		[spCCG_PAT_Config_Load_Settings] @isEIUser = '1', @user = 'ADMIN'
	*/
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	DECLARE @sSQL		Nvarchar(max)

	-- CFGMainData
	IF EXISTS (SELECT 1 from sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Clendor]') AND type in (N'U'))		-- IF VP...
	BEGIN
		set @sSQL = N'
			SELECT MIN(FirmName) as FirmName, COUNT(*) as NumFirms, MAX(TaxAuditingEnabled) as TaxAuditingEnabled, 
					MAX(OriginatingVendorEnabled) as OriginatingVendorEnabled, MIN(UseFilestoGroupTransactions) as UseFilestoGroupTransactions
				FROM CFGMainData'
		exec(@sSQL)
	END
	ELSE
		SELECT MIN(FirmName) as FirmName, COUNT(*) as NumFirms, MAX(TaxAuditingEnabled) as TaxAuditingEnabled, 
				MAX(OriginatingVendorEnabled) as OriginatingVendorEnabled, 'Y' as UseFilestoGroupTransactions
			FROM CFGMainData 

	-- MCFirmName
	SELECT seuser.Username, seuser.Employee, em.Org, m.Company, m.FirmName
		FROM CFGMainData m
			INNER JOIN EM on LEFT(EM.Org, LEN(m.Company)) = m.Company
			INNER JOIN SEUser on EM.Employee = SEUser.Employee
		WHERE SEUser.Username = @user

	-- FW_CFGSysConfig
	SELECT ClientNumber FROM FW_CFGSysConfig

	-- CFGSystem
	SELECT requireVoucherNumbers, lastvoucherAP FROM CFGSystem

	-- CCG_PAT_Config
	SELECT * FROM CCG_PAT_Config

	-- CCG_Email_Config
	SELECT * FROM CCG_Email_Config

	IF @isEIUser = 1 BEGIN
		-- CCG_PAT_User
		SELECT * FROM CCG_PAT_User WHERE username = @user

		-- SEUser
		SELECT Role, SEUser.Employee, EM.EMail,
				IsNull(EM.LastName, N'') + N', ' + IsNull(EM.FirstName, N'') as EMName,
				DisableLogin, EM.Language, SEUser.Domain, case when Password = N'Integrated' then 1 else 0 end as [Integrated]
			FROM SEUser
				LEFT JOIN EM on EM.Employee = SEUser.Employee
			WHERE username = @user
	END
END
GO
