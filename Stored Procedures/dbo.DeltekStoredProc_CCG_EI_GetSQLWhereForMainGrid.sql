SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_GetSQLWhereForMainGrid] ( @delegation varchar(1), @sqlPart varchar(max), @sqlPart2 varchar(max))
             AS EXEC spCCG_EI_GetSQLWhereForMainGrid @delegation,@sqlPart,@sqlPart2
GO
