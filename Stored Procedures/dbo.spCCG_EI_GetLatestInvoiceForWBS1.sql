SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_GetLatestInvoiceForWBS1] (@WBS1 Nvarchar(30))
AS BEGIN
	-- Copyright (c) 2020 EleVia Software. All rights reserved. 
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT top 1 invoice
		FROM billInvMaster
		WHERE MainWBS1 = @WBS1 And Left(invoice,1) <> '<'
		order by InvoiceDate desc, Invoice desc, RecdSeq
END
GO
