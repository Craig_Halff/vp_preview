SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_Save_Languages] ( @fieldsSQL nvarchar(max), @updateValuesSql nvarchar(max), @insertValuesSql nvarchar(max), @deleteSql nvarchar(max))
             AS EXEC spCCG_PAT_Config_Save_Languages @fieldsSQL,@updateValuesSql,@insertValuesSql,@deleteSql
GO
