SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[setCurrentActivityOnly] (
@sessionID as varchar(32) out, 
@fromPRStr As Nvarchar(max),
@pdRangeWhere as varchar(400), 
@dtRangeWhere as varchar(400), 
@whereClause as Nvarchar(max), 
@activePeriod as char(6), 
@selBillStatus as varchar(255), 
@flags As smallint
) as 
begin

declare @includeInvoiceAndReceiptActivity as char(1)
declare @doRPLoad as char(1)
declare @doCurrentActivity as char(1)
declare @checkLabor as char(1)
declare @checkExpense as char(1)
declare @unpostedLabor as char(1)
declare @committedExp as char(1)
declare @projectCostOnly as char(1)
declare @debug as char(1)

declare	@deleteCurrentActivity as int
set @deleteCurrentActivity = 0

declare @SQLStr as Nvarchar(max)
declare @s1 as Nvarchar(max)
declare @s2 as Nvarchar(max)
declare @s3 as Nvarchar(max)
declare @s4 as Nvarchar(max)
declare @s5 as Nvarchar(max)
declare @s6 as Nvarchar(max)
declare @s7 as Nvarchar(max)
declare @s8 as Nvarchar(max)
declare @s9 as Nvarchar(max)
declare @s10 as Nvarchar(max)
declare @holdStr as Nvarchar(max)

declare @deleteOnly as char(1)
declare @activeFY as char(6)
declare @workTable varchar(100)
declare @lastRun as varchar(100)
declare @sql as varchar(max)

/*
declare @sessionID as varchar(32) 
declare @fromPRStr As varchar(8000) 
declare @pdRangeWhere as varchar(400) 
declare @dtRangeWhere as varchar(400) 
declare @whereClause as varchar(8000) 
declare @activePeriod as char(6) 
declare @selBillStatus as varchar(255) 
declare @flags As smallint 
set @sessionID = 'test2'
set @fromPRStr = ' '
set @pdRangeWhere = ' '
set @dtRangeWhere = ' '
set @whereClause = ' '
set @activePeriod = ' '
set @selBillStatus = ' '
set @flags = 128
*/

select @includeInvoiceAndReceiptActivity = 'N', @doRPLoad = 'N', @doCurrentActivity = 'N',
@checkLabor = 'N',@checkExpense = 'N',@unpostedLabor = 'N',@committedExp = 'N',@projectCostOnly = 'N', @debug = 'N'
select @s1 = ' ',@s2 = ' ',@s3 = ' ',@s4 = ' ',@s5 = ' ',@s6 = ' ',@s7 = ' ',@s8 = ' ',@s9 = ' ',@s10 = ' ';

if (@flags >= 512)
 begin
 set @includeInvoiceAndReceiptActivity = 'Y'
 set @flags = @flags - 512
 end
if (@flags >= 256)
 begin
 set @flags = @flags - 256
 end
if (@flags >= 128) 
 begin
 set @doRPLoad = 'Y'
 set @flags = @flags - 128
 end
if (@flags >= 64) 
 begin
 set @doCurrentActivity = 'Y'
 set @flags = @flags - 64
 end
if (@flags >= 32) 
 begin
 set @projectCostOnly = 'Y'
 set @flags = @flags - 32
 end
if (@flags >= 16) 
 begin
 set @committedExp = 'Y'
 set @flags = @flags - 16
 end
if (@flags >= 8) 
 begin
 set @unpostedLabor = 'Y'
 set @flags = @flags - 8
 end
if (@flags >= 4) 
 begin
 set @checkExpense = 'Y'
 set @flags = @flags - 4
 end
if (@flags >= 2) 
 begin
 set @checkLabor = 'Y'
 set @flags = @flags - 2
 end
if (@flags >= 1) 
 begin
 set @debug = 'Y'
 set @flags = @flags - 1
 end
if (@debug = 'Y') print '@doRPLoad = ' + @doRPLoad + ', @doCurrentActivity = ' + @doCurrentActivity + ', @projectCostOnly = ' + @projectCostOnly + ', @committedExp = ' + @committedExp + ', @unpostedLabor = ' + @unpostedLabor + ', @checkExpense = ' + @checkExpense + ', @checkLabor = ' + @checkLabor

set @lastRun = cast(getDate() as varchar(100))

if (@activePeriod = '0')
	set @activeFY = '000000'
else
	set @activeFY = substring(@activePeriod,1,4) + '00'
--Always set this to 'N' for now
set @deleteOnly = 'N'

/*
create a temp table that will store the records for the wbs activities
*/

set @worktable = '##RWL_' + @sessionID

IF OBJECT_ID('tempdb..' + @workTable) IS NOT NULL	
	Begin
			select @sql = 'DELETE FROM ' + @workTable  + ' WHERE SessionID = ''' + @sessionID + ''' '
			exec (@sql)
	End
Else
	Begin	
		select @sql = 'CREATE TABLE ' + @worktable +  '(SessionID varchar(32) NOT NULL, WBS1 Nvarchar(30) COLLATE database_default NOT NULL, WBS2 Nvarchar(30)  COLLATE database_default NOT NULL, WBS3 Nvarchar(30)  COLLATE database_default NOT NULL, LastRun datetime NULL '
		select @sql = @sql + 'CONSTRAINT ReportWBSList' + @sessionID + 'PK PRIMARY KEY NONCLUSTERED (SessionID ASC, WBS1 ASC, WBS2 ASC, WBS3 ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]) ON [PRIMARY]'
		exec (@sql)
	End

/*
one of these two variables should always be an empty string, depending on the type of range selected
*/

if (@deleteOnly = 'N') 
  begin
  set @SQLStr = ''
  if (@doRPLoad = 'Y' and @doCurrentActivity <> 'Y')
    begin
    set @s1 = 'SELECT DISTINCT ''' + @sessionID + ''', LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3, ''' + @lastRun + ''' '
    + 'FROM PR, PR AS LEVEL2, PR AS LEVEL3 '
    + 'WHERE LEVEL3.Wbs1 = LEVEL2.Wbs1 AND LEVEL3.Wbs2 = LEVEL2.wbs2 '
    + 'AND LEVEL3.Wbs1 = PR.Wbs1 '
    + 'AND PR.Wbs2 = N'' '' AND PR.Wbs3 = N'' '' AND LEVEL2.Wbs3 = N'' '' '
    if @whereClause > ''
      set @s1 = @s1 + 'AND ' + @whereClause
    end
  else
    begin
    if (@checkLabor = 'Y')
      begin
      /* 
      need to join to the EM table because the labor detail report allows for EM-based where clauses...
      */
      set @s1 = 'SELECT DISTINCT ''' + @sessionID + ''', LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3, ''' + @lastRun + ''' '
      + 'FROM ' + @fromPRStr + ', LD, EM '
      + 'WHERE (((LD.Wbs1 = LEVEL3.Wbs1 AND LD.Wbs2 = LEVEL3.Wbs2 AND LD.Wbs3 = LEVEL3.Wbs3) '
      + 'AND (LD.Wbs1 = LEVEL2.Wbs1 AND LD.Wbs2 = LEVEL2.wbs2) '
      + 'AND (LD.Wbs1 = PR.Wbs1)) '
      + 'AND (PR.Wbs2 = N'' '' AND PR.Wbs3 = N'' '' AND LEVEL2.Wbs3 = N'' '')) '
      + 'AND (LD.Period >= ' + @activeFY + ' OR PR.ChargeType <> ''H'') '
      + 'AND LD.Employee = EM.Employee '
      + @pdRangeWhere + @dtRangeWhere
      if (@projectCostOnly = 'Y')
        set @s1 = @s1 + 'AND (LD.projectCost = ''Y'') '
      if @selBillStatus > ''
        set @s1 = @s1 + 'AND (LD.billStatus IN (' + @selBillStatus + '))'
      if @whereClause > ''
        set @s1 = @s1 + 'AND ' + @whereClause

      set @s1 = @s1 + ' GROUP BY LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3 '
      if (@unpostedLabor = 'Y')
        begin
        set @s2 = @s2 + ' UNION SELECT DISTINCT ''' + @sessionID + ''', LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3, ''' + @lastRun + ''' '
        + 'FROM ' + @fromPRStr + ', tsDetail, tsControl, EM '
        + 'WHERE (((tsDetail.Wbs1 = LEVEL3.Wbs1 AND tsDetail.Wbs2 = LEVEL3.Wbs2 AND tsDetail.Wbs3 = LEVEL3.Wbs3) '
        + 'AND (tsDetail.Wbs1 = LEVEL2.Wbs1 AND tsDetail.Wbs2 = LEVEL2.wbs2) '
        + 'AND (tsDetail.Wbs1 = PR.Wbs1)) '
        + 'AND (PR.Wbs2 = N'' '' AND PR.Wbs3 = N'' '' AND LEVEL2.Wbs3 = N'' '')) '
        + 'AND (tsDetail.Batch = tsControl.Batch) AND tsControl.Posted = ''N'' '
        + 'AND tsDetail.Employee = EM.Employee '
        + replace(@dtRangeWhere,'LD.','tsDetail.')
        if @whereClause > ''
          set @s2 = @s2 + 'AND ' + replace(@whereClause,'LD.','tsDetail.')
        set @s2 = @s2 + ' GROUP BY LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3 '

        set @s3 = @s3 + ' UNION SELECT DISTINCT ''' + @sessionID + ''', LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3, ''' + @lastRun + ''' '
        + 'FROM ' + @fromPRStr + ', tkDetail, tkMaster, EM '
        + 'WHERE (((tkDetail.Wbs1 = LEVEL3.Wbs1 AND tkDetail.Wbs2 = LEVEL3.Wbs2 AND tkDetail.Wbs3 = LEVEL3.Wbs3) '
        + 'AND (tkDetail.Wbs1 = LEVEL2.Wbs1 AND tkDetail.Wbs2 = LEVEL2.wbs2) '
        + 'AND (tkDetail.Wbs1 = PR.Wbs1)) '
        + 'AND (PR.Wbs2 = N'' '' AND PR.Wbs3 = N'' '' AND LEVEL2.Wbs3 = N'' '')) '
        + 'AND tkDetail.EndDate = tkMaster.EndDate AND tkDetail.Employee = tkMaster.Employee '
        + 'AND tkMaster.Submitted <> ''P'' '
        + 'AND tkDetail.Employee = EM.Employee '
        + replace(@dtRangeWhere,'LD.','tkDetail.')
        if @whereClause > ''
          set @s3 = @s3 + 'AND ' + replace(@whereClause, 'tsDetail.', 'tkDetail.')
        set @s3 = @s3 + ' GROUP BY LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3 '
        end -- if(@unpostedLabor = 'Y')
      end  -- if (@checkLabor = 'Y')

    if (@checkExpense = 'Y')
      begin
      set @holdStr = 'SELECT DISTINCT ''' + @sessionID + ''', LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3, ''' + @lastRun + ''' '
      + 'FROM ' + @fromPRStr + ', LedgerAP LEFT JOIN CA ON LedgerAP.Account = CA.Account '
      + 'WHERE (((LedgerAP.Wbs1 = LEVEL3.Wbs1 AND LedgerAP.Wbs2 = LEVEL3.Wbs2 AND LedgerAP.Wbs3 = LEVEL3.Wbs3) '
      + 'AND (LedgerAP.Wbs1 = LEVEL2.Wbs1 AND LedgerAP.Wbs2 = LEVEL2.wbs2) '
      + 'AND (LedgerAP.Wbs1 = PR.Wbs1)) '
      + 'AND (PR.Wbs2 = N'' '' AND PR.Wbs3 = N'' '' AND LEVEL2.Wbs3 = N'' '')) '
      + 'AND (LedgerAP.Period >= ' + @activeFY + ' OR PR.ChargeType <> ''H'') '
      + 'AND (LedgerAP.Amount <> 0 OR LedgerAP.BillExt <> 0 ) '
      + replace(@pdRangeWhere,'LD.','LedgerAP.') + replace (@dtRangeWhere,'LD.','LedgerAP.')

      if (@projectCostOnly = 'Y')
        set @holdStr = @holdStr + 'AND (LedgerAP.projectCost = ''Y'') '
      if @selBillStatus > ''
        set @holdStr = @holdStr + 'AND (LedgerAP.billStatus IN (' + @selBillStatus + '))'
      if @whereClause > ''
        set @holdStr = @holdStr + 'AND ' + @whereClause
      set @holdStr = @holdStr + ' GROUP BY LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3 '

      if (@checkLabor = 'Y')
        set @s4 = @s4 + ' UNION ' + @holdStr
      else
        set @s4 = @s4 + @holdStr
      set @s5 = @s5 + ' UNION ' + replace(@holdStr,'LedgerAP','LedgerAR')
      set @s6 = @s6 + ' UNION ' + replace(@holdStr,'LedgerAP','LedgerEX')
      set @s7 = @s7 + ' UNION ' + replace(@holdStr,'LedgerAP','LedgerMisc')


      --POCommitment
      if (@committedExp = 'Y')
        begin
        set @s8 = @s8 + ' UNION '
        + 'SELECT DISTINCT ''' + @sessionID + ''', LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3, ''' + @lastRun + ''' '
        + 'FROM ' + @fromPRStr + ', POCommitment '
        + 'WHERE (((POCommitment.Wbs1 = LEVEL3.Wbs1 AND POCommitment.Wbs2 = LEVEL3.Wbs2 AND POCommitment.Wbs3 = LEVEL3.Wbs3) '
        + 'AND (POCommitment.Wbs1 = LEVEL2.Wbs1 AND POCommitment.Wbs2 = LEVEL2.wbs2) '
        + 'AND (POCommitment.Wbs1 = PR.Wbs1)) '
        + 'AND (PR.Wbs2 = N'' '' AND PR.Wbs3 = N'' '' AND LEVEL2.Wbs3 = N'' '')) '
        + 'AND (POCommitment.Amount <> 0) '
			
        if @whereClause > ''
          set @s8 = @s8 + 'AND ' + replace(@whereClause,'LedgerAP.','POCommitment.')
        set @s8 = @s8 + ' GROUP BY LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3 '
        end -- if (@committedExp = 'Y')
      end -- if (@checkExpenses = 'Y')
	if @includeInvoiceAndReceiptActivity = 'Y'
      begin
      set @s9 = @s9 + 'SELECT DISTINCT ''' + @sessionID + ''', LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3, ''' + @lastRun + ''' '
      + 'FROM ' + @fromPRStr + ', LedgerAR LEFT JOIN CA ON LedgerAR.Account = CA.Account '
      + ' WHERE '
      + '( '
      + '(LedgerAR.TransType = N''IN'' and ((LedgerAR.subtype <> ''I'' AND LedgerAR.subtype <> ''R'') OR LedgerAR.subtype is NULL) '
      + 'AND (CA.Type >= 4 OR LedgerAR.Account is NULL)) '
      + 'OR '
      + '(LedgerAR.TransType = N''CR'' AND ((LedgerAR.SubType IN (''R'', ''X'') AND LedgerAR.autoentry = ''N'') '
      + 'OR (LedgerAR.SubType = ''T'' AND (LedgerAR.Invoice is NULL OR LedgerAR.Invoice = N'' '')) '
      + 'OR LedgerAR.Subtype = ''R'')) '
      + 'OR '
      + '(CA.Type >= 4 AND CA.Type < 5) '
      + ') '
      + 'AND LEVEL3.WBS1 = LedgerAR.WBS1 AND LEVEL3.WBS2 = LedgerAR.WBS2 AND LEVEL3.WBS3 = LedgerAR.WBS3 '
      + 'AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 '
      + 'AND PR.Wbs2 = N'' '' AND PR.Wbs3 = N'' '' AND LEVEL2.Wbs3 = N'' '' '
      + replace(@pdRangeWhere,'LD.','LedgerAR.') + replace (@dtRangeWhere,'LD.','LedgerAR.')
      if @whereClause > ''
        set @s9 = @s9 + 'AND ' + replace(@whereClause,'LedgerAP.','POCommitment.')
      set @s9 = @s9 + ' GROUP BY LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3 '

      set @s10 = @s10 + ' UNION ' + replace(@s9,'LedgerAR','LedgerMisc')

      --@s10 ALWAYS needs a UNION, so we have to do this after the @s10 find/replace
      if (@s1 > '' or @s2 > '' or @s3 > '' or @s4 > '' or @s5 > '' or @s6 > '' or @s7 > '' or @s8 > '')
        set @s9 = ' UNION ' + @s9
      end
    end --if not (@doRPLoad = 'Y' and @doCurrentActivity <> 'Y')

  if (@s1 > '' or @s2 > '' or @s3 > '' or @s4 > '' or @s5 > '' or @s6 > '' or @s7 > '' or @s8 > '' or @s9 > '' or @s10 > '')
    begin
    if (@debug = 'Y') print 'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; INSERT INTO ' + @worktable + '(sessionID, PR.WBS1, PR.WBS2, PR.WBS3, lastRun) ' + @s1 + @s2 + @s3 + @s4 + @s5 + @s6 + @s7 + @s8 + @s9 + @s10
    exec('SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; INSERT INTO ' + @worktable + '(sessionID, PR.WBS1, PR.WBS2, PR.WBS3, lastRun) ' + @s1 + @s2 + @s3 + @s4 + @s5 + @s6 + @s7 + @s8 + @s9 + @s10)
    --Insert Null WBS2 Rows
    set @SQLStr = 'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; INSERT INTO ' + @worktable + '(sessionID, WBS1, WBS2, WBS3, lastRun) '
    + 'SELECT DISTINCT ''' + @sessionID + ''', WBS1, N'' '', N'' '', ''' + @lastRun + ''' '
    + 'FROM ' + @worktable + ' '
    + 'WHERE WBS2 > N'''' AND sessionID = ''' + @sessionID + ''' AND lastRun = ''' + @lastRun + ''' '
    + ' AND NOT EXISTS (Select ''x'' '
    + '     FROM ' + @worktable + ' AS subList '
    + '     WHERE ' + @worktable + '.WBS1 = subList.WBS1 AND subList.WBS2 = N'' '' AND subList.WBS3 = N'' '''
    + '     AND sessionID = ''' + @sessionID + ''')'
    if (@debug = 'Y') print @sqlstr
    exec(@SQLStr)
    --Insert Null WBS3 Rows
    set @SQLStr = 'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; INSERT INTO ' + @worktable + '(sessionID, WBS1, WBS2, WBS3, lastRun) '
    + 'SELECT DISTINCT ''' + @sessionID + ''', WBS1, WBS2, N'' '', ''' + @lastRun + ''' '
    + 'FROM ' + @worktable + ' '
    + 'WHERE WBS3 > N'''' AND sessionID = ''' + @sessionID + ''' AND lastRun = ''' + @lastRun + ''' '
    + ' AND NOT EXISTS (Select ''x'' '
    + '     FROM ' + @worktable + ' AS subList '
    + '     WHERE ' + @worktable + '.WBS1 = subList.WBS1 AND ' + @worktable + '.WBS2 = subList.WBS2 AND subList.WBS3 = N'' '''
    + '     AND sessionID = ''' + @sessionID + ''')'
    if (@debug = 'Y') print @sqlstr
    exec(@SQLStr)
    end -- if (@SQLStr > '')
  end -- if @deleteOnly = 'N'
  if @debug = 'Y'
    print @sessionID
  
end
GO
