SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Ctrl_GetGroups]
	@KEY_EMPLOYEEID	Nvarchar(20),
	@Language Nvarchar(10) = ''
AS
BEGIN
	-- [DeltekStoredProc_CCG_PAT_Ctrl_GetGroups] '00001'
	SET NOCOUNT ON;
	IF ISNULL(@Language, N'') = N''
		EXEC (N'SELECT * FROM dbo.fnCCG_PAT_GetGroups(N''' + @KEY_EMPLOYEEID + ''', NULL)');
	ELSE
		EXEC (N'SELECT * FROM dbo.fnCCG_PAT_GetGroups(N''' + @KEY_EMPLOYEEID + ''', NULL, ''' + @Language + ''')');
END;


GO
