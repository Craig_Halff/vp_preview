SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[~spCCG_ProjectLaborBillerNotification_20180601] 
AS
/*
Copyright 2017 (c) Central Consulting Group.   All rights reserved.
08/24/2017 David Springer
           Write Biller into EI CCG_Email table with regular projects with BillExt = 0.

- REVISED 1/5/2018 Jackson Gray
- REVISED 5/4/2018 Jackson Gray and Conrad Harrison - Changes and Original Coding Commented
- REVISED 6/6/2018 Conrad Harrison - Changes and Original Coding Commented
- REVISED 7/18/2018 Conrad Harrison - Accounting for refined queries and an email sent to each biller
*/

SET NOCOUNT ON
DECLARE @Biller					varchar (8000),
        @Email					varchar (100),
		@LastName				varchar (50),
		@FirstName				varchar (25),
		@WBS1					varchar (32),
		@WBS2					varchar (7),
		@WBS3					varchar (7),
		@TransDate				datetime,
		@Employee				varchar (255),
		@Category				varchar (8000),
		@Hours					decimal (19,4),
		@Multiplier				decimal (19,4),
		@BilledInvoice			varchar (12),
        @Body					varchar (max),
		
		-- ADDING THREE LINES BELOW
		@BodyMissingFee			varchar (max),
		@BodyNeedToBeMoved		varchar (max),
		@BodyLumpSum			varchar (max),

		@CSS					varchar (max),
        @Loop1					integer,
		@CustFeeType			varchar (12),
		@Org					varchar (12),
		@Amount					decimal (19,4),
		@BillExt				decimal (19,4),
		@BillStatus				varchar (2),
		@RowCount				int,
		-- ADDING THREE LINES BELOW
		@RowCountMissingFee		int,
		@RowCountNeedToBeMoved	int,
		@RowCountLumpSum		int,

		-- ADDING LINES BELOW
		@Fee					varchar (25),
		@FeeType				varchar (25),
		@ChargeType				varchar (25),
		@SubLevel				varchar (25),
		@CustBillingType		varchar (25),
		@Status					varchar (25),
		@CustJTDBilled			varchar (25)


BEGIN

	-- CSS style rules
	Set @CSS = 
'
<style type="text/css">
	p {padding-top:15px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;}
	td, th {border: 1px solid black;padding: 5px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;font-size:10.5pt;}
	th {background-color: #6E6E6E;color: white;text-align:center;}
	.c1, .c2,. c3, .c4, .c5, .c6, .c9 {text-align:left;}
	.c7, .c8 {text-align:right;}
</style>
'

	DECLARE curBiller insensitive cursor for
		Select Distinct IsNull (p.Biller, '001975'), e.EMail, e.LastName, e.FirstName
		From PR p, LD l, EMMain e
		Where p.WBS1 = l.WBS1
		  and p.WBS2 = ' '  --- project level for Biller
		  and p.WBS1 not like 'Z%'
		  and p.ChargeType = 'R'
		  and l.LaborCode = '01'
		  and l.TransDate > '04-24-2017'
		  and p.Biller = e.Employee
		  and l.BillExt = 0

	-- Get each biller
	OPEN curBiller
	FETCH NEXT FROM curBiller INTO @Biller, @Email, @LastName, @FirstName
	Set @Loop1 = @@FETCH_STATUS
	While @Loop1 = 0
	BEGIN

		Declare curProjects insensitive cursor for
		SELECT * 
		FROM 
		(
			   SELECT Biller,
			   CustFeeType,
			   [Org],
			   IsNull(WBS1, '') As WBS1
			   , IsNull(WBS2, '') As WBS2
			   , IsNull(WBS3, '') As WBS3
			   , IsNull(Name, '') As Name
			   , TransDate
			   , IsNull([Labor Category], '') AS [Labor Category]
			   , IsNull([Hours], 0) As [Hours]
			   , IsNull([Amount], 0) As [Amount]
			   , [BillExt]
			   , Round([BillExt] / [Amount], 2) AS [Multiplier]
			   , IsNull(BillStatus, '') As BillStatus
			   , IsNull(BilledInvoice, '') As BilledInvoice
			   FROM 
			   (
					Select distinct p.Biller
					, x.CustFeeType
					, p.org Org
					, l.WBS1
					, l.WBS2
					, l.WBS3
					, l.Name
					, l.TransDate
					, ltrim (str (l.Category)) + ' - ' + c.Description 'Labor Category'
					, l.RegHrs + l.OvtHrs + l.SpecialOvtHrs 'Hours'
					, l.RegAmt + l.OvtAmt + l.SpecialOvtAmt 'Amount'
					, l.BillExt
					, l.BillStatus
					, l.BilledInvoice
					From PR p, ProjectCustomTabFields x, LD l
							Left Join BTLaborCats c on c.Category = l.Category
					Where p.WBS1 = l.WBS1
						and x.WBS1 = l.WBS1
						and x.WBS2 = l.WBS2
						and x.WBS3 = l.WBS3
						and p.WBS2 = ' '  --- project level for Biller
						and p.WBS1 not like 'Z%'
						and p.ChargeType = 'R'
						and p.Biller = @Biller
						and l.LaborCode = '01'
						and l.BillStatus <> 'T'
						and l.TransDate > '2017-04-24'
					) AS A
			   ) AS B
		WHERE B.Multiplier = 0.00 or B.Multiplier = 1.00
		ORDER BY Biller, WBS1, Multiplier ASC
		
		-- Reset RowCount 
		SET @RowCount = 0

		-- Open the table with headers
		Set @Body = 
'<p><strong>' + IsNull(@LastName, '') + ', ' + IsNull(@FirstName, '') + '</strong></p> 
<table cellpadding=''5'' cellspacing=''0'' style=''border:1px solid black;border-collapse:collapse;''>
	<thead>
		<tr>
			<th>Project</th>
			<th>Phase</th>
			<th>Task</th>
			<th>Employee</th>
			<th>Date</th>
			<th>Category</th>
			<th>Hours</th>
			<th>Multiplier</th>
			<th>Billed<br/>Invoice</th>
		</tr>
	</thead>
	<tbody>
'

		Open curProjects
		--COMMENTED OUT @@Cursor_Rows below for now
		--Print @@Cursor_Rows
		Fetch next from curProjects Into @Biller, @CustFeeType, @Org, @WBS1, @WBS2, @WBS3, @Employee, @TransDate, @Category, @Hours, @Amount, @BillExt, @Multiplier, @BillStatus, @BilledInvoice 

		While @@FETCH_STATUS = 0
		Begin
			SET @RowCount = @RowCount + 1

			Set @Body = @Body + 
'		<tr>
			<td class="c1"> ' + @WBS1 + '</td>
			<td class="c2"> ' + @WBS2 + '</td>
			<td class="c3"> ' + @WBS3 + '</td>
			<td class="c4"> ' + @Employee + '</td>
			<td class="c5"> ' + Convert(varchar(50), @TransDate, 101) + '</td>
			<td class="c6"> ' + @Category + '</td>
			<td class="c7"> ' + Cast(Convert(decimal(19,2), @Hours) As varchar(50)) + '</td>
			<td class="c8"> ' + Cast(Convert(decimal(19,2), @Multiplier) As varchar(50)) + '</td>
			<td class="c9"> ' + @BilledInvoice + '</td>
		</tr>
'
			Fetch next from curProjects Into @Biller, @CustFeeType, @Org, @WBS1, @WBS2, @WBS3, @Employee, @TransDate, @Category, @Hours, @Amount, @BillExt, @Multiplier, @BillStatus, @BilledInvoice 

		End
		Close curProjects
		Deallocate curProjects

		-- Close the table
		Set @Body = @Body + 
'	</tbody>
</table>
'

IF @RowCount > 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', 'charrison@halff.com', 'charrison@halff.com', 'charrison@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END
	FETCH NEXT FROM curBiller INTO @Biller, @Email, @LastName, @FirstName
	Set @Loop1 = @@FETCH_STATUS
	END
	CLOSE curBiller
	DEALLOCATE curBiller

-- INSERT MISSING FEE TABLES HERE

--ORIGINAL QUERY
/*
	DECLARE curBillerMissingFee insensitive cursor for
		Select Distinct IsNull (p.Biller, '001975'), e.EMail, e.LastName, e.FirstName
		From PR p, LD l, EMMain e
		Where p.WBS1 = l.WBS1
		  and p.WBS2 = ' '  --- project level for Biller
		  and p.WBS1 not like 'Z%'
		  and p.ChargeType = 'R'
		  and l.LaborCode = '01'
		  and l.TransDate > '04-24-2017'
		  and p.Biller = e.Employee
		  and l.BillExt = 0
*/


	--ADDED IN NEW QUERY HERE
DECLARE curBillerMissingFee insensitive cursor for
		Select p.WBS1, IsNull (p.Biller, '001975') Biller, e.EMail, e.LastName, e.FirstName
		From PR p, EMMain e, ProjectCustomTabFields x
					  Where p.WBS1 not like 'Z%'
						and p.WBS1 not like '%CATC%'
						and p.WBS1 = x.WBS1
						and p.WBS2 = x.WBS2
						and p.WBS3 = x.WBS3
						and p.Biller = e.Employee
						and exists (Select 'x' From PR Where WBS1 = x.WBS1 and WBS2 = ' ' and Status <> 'D')
						and p.ChargeType = 'R'
						and (x.CustFeeType = 'Sublevel Terms' or x.CustFeeType Is Null)
						and p.SubLevel = 'N'
						order by 1

	-- Get each biller
	OPEN curBillerMissingFee
	FETCH NEXT FROM curBillerMissingFee INTO @WBS1, @Biller, @Email, @LastName, @FirstName
	Set @Loop1 = @@FETCH_STATUS
	While @Loop1 = 0
	BEGIN

		Declare curMissingFeeProjects insensitive cursor for
		Select * 
FROM
	(
	select Biller,
	CustFeeType,
	ChargeType,
	[Org],
	IsNull(WBS1, '') As WBS1,
    IsNull(WBS2, '') As WBS2,
    IsNull(WBS3, '') As WBS3,
    SubLevel,
	[Status]

From 
(Select distinct
p.Biller
, x.CustFeeType
, p.ChargeType
, p.org Org
, p.WBS1
, p.WBS2
, p.WBS3
, p.SubLevel
, p.[Status]

		From PR p, EMMain e, ProjectCustomTabFields x
					  Where p.WBS1 not like 'Z%'
						and p.WBS1 not like '%CATC%'
						and p.WBS1 = x.WBS1
						and p.WBS2 = x.WBS2
						and p.WBS3 = x.WBS3
						and p.Biller = e.Employee
						and exists (Select 'x' From PR Where WBS1 = x.WBS1 and WBS2 = ' ' and Status <> 'D')
						and p.ChargeType = 'R'
						and (x.CustFeeType = 'Sublevel Terms' or x.CustFeeType Is Null)
						and p.SubLevel = 'N'
						) AS A
						) AS B 
						ORDER BY WBS1 Desc , WBS2, WBS3, CustFeeType

						--ORIGINAL QUERY
	--FROM PR p, ProjectCustomTabFields x
	--				Where p.WBS1 not like 'Z%'
	--				and p.WBS1 = x.WBS1
	--				and p.WBS2 = x.WBS2
	--				and p.WBS3 = x.WBS3
	--				and exists (Select 'x' From PR Where WBS1 = x.WBS1 and p.WBS2 = ' ' and p.[Status] <> 'D')
	--				and p.ChargeType = 'R'
	--				and (x.CustFeeType = 'Sublevel Terms' or x.CustFeeType Is Null)
	--				and p.SubLevel = 'N'
	--				) AS A
	--				) AS B 

	--				ORDER BY Biller Desc, WBS1 Desc , WBS2, WBS3, CustFeeType
		
		-- Reset RowCount 
		SET @RowCountMissingFee = 0

		-- Open the table with headers
		Set @BodyMissingFee = 
'<p><strong>Missing Fee Types</strong></p> 
<table cellpadding=''5'' cellspacing=''0'' style=''border:1px solid black;border-collapse:collapse;''>
	<thead>
		<tr>
			<th>Project</th>
			<th>Phase</th>
			<th>Task</th>
		</tr>
	</thead>
	<tbody>
'
		Open curMissingFeeProjects
		--COMMENTED OUT @@Cursor_Rows below for now
		--Print @@Cursor_Rows
		Fetch next from curMissingFeeProjects Into @Biller, @CustFeeType, @ChargeType, @Org, @WBS1, @WBS2, @WBS3, @SubLevel, @Status 

		While @@FETCH_STATUS = 0
		Begin
			SET @RowCountMissingFee = @RowCountMissingFee + 1

			Set @BodyMissingFee = @BodyMissingFee + 
'		<tr>
			<td class="c1"> ' + @WBS1 + '</td>
			<td class="c2"> ' + @WBS2 + '</td>
			<td class="c3"> ' + @WBS3 + '</td>
		</tr>
'
		Fetch next from curMissingFeeProjects Into @Biller, @CustFeeType, @ChargeType, @Org, @WBS1, @WBS2, @WBS3, @SubLevel, @Status  

		End
		Close curMissingFeeProjects
		Deallocate curMissingFeeProjects

		-- Close the table
		Set @BodyMissingFee = @BodyMissingFee + 
'	</tbody>
</table>
'
		IF @RowCountMissingFee > 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', 'charrison@halff.com', 'charrison@halff.com', 'charrison@halff.com', 'Incorrect Billing Extension', @CSS + @BodyMissingFee, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END
			
	FETCH NEXT FROM curBillerMissingFee INTO @WBS1, @Biller, @Email, @LastName, @FirstName
	Set @Loop1 = @@FETCH_STATUS
	END
	CLOSE curBillerMissingFee
	DEALLOCATE curBillerMissingFee
-- INSERTING MISSING FEE TABLES ABOVE


-- INSERT INCORRECT BILLING TYPE OR BILLED AMOUNTS NEED TO BE MOVED TABLES HERE

	DECLARE curBillerNeedtoBeMoved insensitive cursor for

	--New Code Inserted Below
	Select  p.WBS1,
			p.Biller
			, x.CustFeeType
			, p.ChargeType
			, x.CustJTDBilled
			, p.org Org
			--, p.WBS1
			, p.WBS2
			, p.WBS3
			, p.SubLevel
			, p.[Status]

     FROM PR p, ProjectCustomTabFields x
                                  Where p.WBS1 not like 'Z%'
                                  and exists (Select 'x' From PR Where WBS1 = x.WBS1 and WBS2 = ' ' and Status <> 'D')
                                  and p.ChargeType = 'R'
                                  and p.WBS1 = x.WBS1
                                  and p.WBS2 = x.WBS2
                                  and p.WBS3 = x.WBS3
                                  and x.CustFeeType = 'Non-Billable'
                                  and x.CustJTDBilled <> 0
		

		--PREVIOUS QUERY CODE BELOW
		
		--Select Distinct IsNull (p.Biller, '001975'), e.EMail, e.LastName, e.FirstName
		--From PR p, LD l, EMMain e
		--Where p.WBS1 = l.WBS1
		--  and p.WBS2 = ' '  --- project level for Biller
		--  and p.WBS1 not like 'Z%'
		--  and p.ChargeType = 'R'
		--  and l.LaborCode = '01'
		--  and l.TransDate > '04-24-2017'
		--  and p.Biller = e.Employee
		--  and l.BillExt = 0

	-- Get each biller
	OPEN curBillerNeedtoBeMoved
	FETCH NEXT FROM curBillerNeedtoBeMoved INTO @WBS1, @Biller, @CustFeeType, @ChargeType, @CustJTDBilled, @Org, @WBS2, @WBS3, @SubLevel, @Status
	--@Biller, @Email, @LastName, @FirstName
	Set @Loop1 = @@FETCH_STATUS
	While @Loop1 = 0
	BEGIN

		Declare curNeedToBeMovedProjects insensitive cursor for
		Select * 
FROM
	(
	--NEW QUERY
		Select  p.WBS1,
			p.Biller
			, x.CustFeeType
			, p.ChargeType
			, x.CustJTDBilled
			, p.org Org
			--, p.WBS1
			, p.WBS2
			, p.WBS3
			, p.SubLevel
			, p.[Status]

     FROM PR p, ProjectCustomTabFields x
                                  Where p.WBS1 not like 'Z%'
                                  and exists (Select 'x' From PR Where WBS1 = x.WBS1 and WBS2 = ' ' and Status <> 'D')
                                  and p.ChargeType = 'R'
                                  and p.WBS1 = x.WBS1
                                  and p.WBS2 = x.WBS2
                                  and p.WBS3 = x.WBS3
                                  and x.CustFeeType = 'Non-Billable'
                                  and x.CustJTDBilled <> 0
								  ) AS A
								  ORDER BY Biller Desc, WBS1 Desc , WBS2, WBS3

	--OLD QUERY BELOW

--	select Biller,
--	CustFeeType,
--	ChargeType,
--	CustJTDBilled,
--	[Org],
--	IsNull(WBS1, '') As WBS1,
--    IsNull(WBS2, '') As WBS2,
--    IsNull(WBS3, '') As WBS3,
--    SubLevel,
--	[Status]

--From 
--(Select distinct
--p.Biller
--, x.CustFeeType
--, p.ChargeType
--, x.CustJTDBilled
--, p.org Org
--, p.WBS1
--, p.WBS2
--, p.WBS3
--, p.SubLevel
--, p.[Status]

--	FROM PR p, ProjectCustomTabFields x
--					Where p.WBS1 not like 'Z%'
--					and exists (Select 'x' From PR Where p.WBS1 = x.WBS1 and p.WBS2 = ' ' and p.[Status] <> 'D')
--					and p.ChargeType = 'R'
--					AND x.CustFeeType = 'Non-Billable'
--					and x.CustJTDBilled <> 0
--					and p.SubLevel = 'N'
					--) AS A
					--) AS B 

					--ORDER BY Biller Desc, WBS1 Desc , WBS2, WBS3
		
		-- Reset RowCount 
		SET @RowCountNeedToBeMoved = 0

		-- Open the table with headers
		Set @BodyNeedToBeMoved = 
'<p><strong>Incorrect Billing Type or Billed Amounts Need to be Moved</strong></p> 
<table cellpadding=''5'' cellspacing=''0'' style=''border:1px solid black;border-collapse:collapse;''>
	<thead>
		<tr>
			<th>Project</th>
			<th>Phase</th>
			<th>Task</th>
			<th>Fee Type</th>
			<th>Billed</th>
		</tr>
	</thead>
	<tbody>
'

		Open curNeedToBeMovedProjects
		--COMMENTED OUT @@Cursor_Rows below for now
		--Print @@Cursor_Rows
		Fetch next from curNeedtoBeMovedProjects Into @WBS1, @Biller, @CustFeeType, @ChargeType, @CustJTDBilled, @Org, @WBS2, @WBS3, @SubLevel, @Status
		--@Biller, @CustFeeType, @ChargeType, @CustJTDBilled, @Org, @WBS1, @WBS2, @WBS3, @SubLevel, @Status  

		While @@FETCH_STATUS = 0
		Begin
			SET @RowCountNeedToBeMoved = @RowCountNeedToBeMoved + 1

			Set @BodyNeedToBeMoved = @BodyNeedToBeMoved + 
'		<tr>
			<td class="c1"> ' + @WBS1 + '</td>
			<td class="c2"> ' + @WBS2 + '</td>
			<td class="c3"> ' + @WBS3 + '</td>
			<td class="c4"> ' + @CustFeeType + '</td>
			<td class="c5"> ' + @CustJTDBilled + '</td>
		</tr>
'
			Fetch next from curNeedToBeMovedProjects Into @WBS1, @Biller, @CustFeeType, @ChargeType, @CustJTDBilled, @Org, @WBS2, @WBS3, @SubLevel, @Status
			--@Biller, @CustFeeType, @ChargeType, @CustJTDBilled, @Org, @WBS1, @WBS2, @WBS3, @SubLevel, @Status  

		End
		Close curNeedToBeMovedProjects
		Deallocate curNeedToBeMovedProjects

		-- Close the table
		Set @BodyNeedToBeMoved = @BodyNeedToBeMoved + 
'	</tbody>
</table>
'
		IF @RowCountNeedToBeMoved > 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', 'charrison@halff.com', 'charrison@halff.com', 'charrison@halff.com', 'Incorrect Billing Extension', @CSS + @BodyNeedToBeMoved, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END

	FETCH NEXT FROM curBillerNeedtoBeMoved INTO @WBS1, @Biller, @CustFeeType, @ChargeType, @CustJTDBilled, @Org, @WBS2, @WBS3, @SubLevel, @Status
	--@Biller, @Email, @LastName, @FirstName
	Set @Loop1 = @@FETCH_STATUS
	END
	CLOSE curBillerNeedtoBeMoved
	DEALLOCATE curBillerNeedtoBeMoved
-- INSERT INCORRECT BILLING TYPE OR BILLED AMOUNTS NEED TO BE MOVED TABLES ABOVE


-- INSERT LUMP SUM LOCATION CLOSED WHEN REMAINING TO BE BILLED TABLES HERE

	DECLARE curBillerLumpSum insensitive cursor for
	--INSERTED NEW CODE HERE
	Select p.WBS1
	, p.WBS2
	, p.WBS3
	, p.Biller
	, p.Fee
	--, x.CustFeeType
	, p.ChargeType
	, p.[Status]
	, x.CustJTDBilled
	, x.CustBillingType

       FROM PR p, ProjectCustomTabFields x
                                  Where p.WBS1 not like 'Z%'
                                  and p.WBS1 = x.WBS1
                                  and p.WBS2 = x.WBS2
                                  and p.WBS3 = x.WBS3
                                  and exists (Select 'x' From PR Where WBS1 = x.WBS1 and WBS2 = ' ' and Status <> 'D')
                                  and p.ChargeType = 'R'
                                  and x.CustBillingType like 'Lump Sum%'
                                  and x.CustJTDBilled <> (p.FeeDirLab + p.FeeDirExp + p.ConsultFee)
                                  and p.Status = 'D'
                                  order by 1, 2, 3


	--PREVIOUS QUERY BELOW
		--Select Distinct IsNull (p.Biller, '001975'), e.EMail, e.LastName, e.FirstName
		--From PR p, LD l, EMMain e
		--Where p.WBS1 = l.WBS1
		--  and p.WBS2 = ' '  --- project level for Biller
		--  and p.WBS1 not like 'Z%'
		--  and p.ChargeType = 'R'
		--  and l.LaborCode = '01'
		--  and l.TransDate > '04-24-2017'
		--  and p.Biller = e.Employee
		--  and l.BillExt = 0

	-- Get each biller
	OPEN curBillerLumpSum
	FETCH NEXT FROM curBillerLumpSum INTO @WBS1, @WBS2, @WBS3, @Biller, @Fee, @ChargeType, @Status, @CustJTDBilled, @CustBillingType
	--@Biller, @Email, @LastName, @FirstName
	Set @Loop1 = @@FETCH_STATUS
	While @Loop1 = 0
	BEGIN

		Declare curLumpSumProjects insensitive cursor for
		Select * 
FROM
	(
	--NEW CODE BELOW
		Select p.WBS1
	, p.WBS2
	, p.WBS3
	, p.Biller
	, p.Fee
	--, x.CustFeeType
	, p.ChargeType
	, p.[Status]
	, x.CustJTDBilled
	, x.CustBillingType

       FROM PR p, ProjectCustomTabFields x
                                  Where p.WBS1 not like 'Z%'
                                  and p.WBS1 = x.WBS1
                                  and p.WBS2 = x.WBS2
                                  and p.WBS3 = x.WBS3
                                  and exists (Select 'x' From PR Where WBS1 = x.WBS1 and WBS2 = ' ' and Status <> 'D')
                                  and p.ChargeType = 'R'
                                  and x.CustBillingType like 'Lump Sum%'
                                  and x.CustJTDBilled <> (p.FeeDirLab + p.FeeDirExp + p.ConsultFee)
                                  and p.Status = 'D'
								  ) AS A
                                  order by 1, 2, 3

--OLD CODE COMMENTED OUT

--	select Biller,
--	Fee,
--	--CustFeeType,
--	ChargeType,
--	CustJTDBilled,
--    CustBillingType,
--	[Org],
--	IsNull(WBS1, '') As WBS1,
--    IsNull(WBS2, '') As WBS2,
--    IsNull(WBS3, '') As WBS3,
--    SubLevel,
--	[Status]

--From 
--(Select distinct
--p.Biller
--, p.Fee
----, x.CustFeeType
--, p.ChargeType
--, p.org Org
--, p.WBS1
--, p.WBS2
--, p.WBS3
--, p.SubLevel
--, p.[Status]
--, x.CustJTDBilled
--, x.CustBillingType

--	FROM PR p, ProjectCustomTabFields x
--					Where p.WBS1 not like 'Z%'
--					and exists (Select 'x' From PR Where p.WBS1 = x.WBS1 and p.WBS2 = ' ' and p.[Status] <> 'D')
--					and p.ChargeType = 'R'
--					and exists (Select 'x' From PR Where p.WBS1 = x.WBS1 and p.WBS2 <> ' ' and p.[Status] = 'D')
--					and (x.CustBillingType = 'Lump Sum by Phase' or x.CustBillingType = 'Lump Sum')
--					and x.CustJTDBilled <> (p.FeeDirLab + p.FeeDirExp + p.ConsultFee)
					--) AS A
					--) AS B 

					--ORDER BY Biller Desc, WBS1 Desc , WBS2, WBS3
		
		-- Reset RowCount 
		SET @RowCountLumpSum = 0

		-- Open the table with headers
		Set @BodyLumpSum = 
'<p><strong>Lump Sum Location Closed When Remaining to be Billed</strong></p> 
<table cellpadding=''5'' cellspacing=''0'' style=''border:1px solid black;border-collapse:collapse;''>
	<thead>
		<tr>
			<th>Project</th>
			<th>Phase</th>
			<th>Task</th>
			<th>Fee</th>
			<th>Billed</th>
		</tr>
	</thead>
	<tbody>
'
		Open curLumpSumProjects
		--COMMENTED OUT @@Cursor_Rows below for now
		--Print @@Cursor_Rows
		Fetch next from curLumpSumProjects Into @WBS1, @WBS2, @WBS3, @Biller, @Fee, @ChargeType, @Status, @CustJTDBilled, @CustBillingType
		--@Biller, @CustFeeType, @ChargeType, @CustJTDBilled, @CustBillingType, @Org, @WBS1, @WBS2, @WBS3, @SubLevel, @Status 

		While @@FETCH_STATUS = 0
		Begin
			SET @RowCountLumpSum = @RowCountLumpSum + 1

			Set @BodyLumpSum = @BodyLumpSum + 
'		<tr>
			<td class="c1"> ' + @WBS1 + '</td>
			<td class="c2"> ' + @WBS2 + '</td>
			<td class="c3"> ' + @WBS3 + '</td>
			<td class="c4"> ' + @ChargeType + '</td>
			<td class="c5"> ' + @CustJTDBilled + '</td>
		</tr>
'
		Fetch next from curLumpSumProjects Into  @WBS1, @WBS2, @WBS3, @Biller, @Fee, @ChargeType, @Status, @CustJTDBilled, @CustBillingType
		--@Biller, @CustFeeType, @ChargeType, @CustJTDBilled, @CustBillingType, @Org, @WBS1, @WBS2, @WBS3, @SubLevel, @Status  

		End
		Close curLumpSumProjects
		Deallocate curLumpSumProjects

		-- Close the table
		Set @BodyLumpSum = @BodyLumpSum + 
'	</tbody>
</table>
'

		IF @RowCountLumpSum > 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', 'charrison@halff.com', 'charrison@halff.com', 'charrison@halff.com', 'Incorrect Billing Extension', @CSS + @BodyLumpSum, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END

	FETCH NEXT FROM curBillerLumpSum INTO @WBS1, @WBS2, @WBS3, @Biller, @Fee, @ChargeType, @Status, @CustJTDBilled, @CustBillingType
	--@Biller, @Email, @LastName, @FirstName
	Set @Loop1 = @@FETCH_STATUS
	END
	CLOSE curBillerLumpSum
	DEALLOCATE curBillerLumpSum
END
GO
