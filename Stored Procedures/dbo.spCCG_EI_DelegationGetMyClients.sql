SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_DelegationGetMyClients] (
	@employee	Nvarchar(20)
)
AS
BEGIN
	/* Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved. */
	-- [spCCG_EI_DelegationGetMyClients] '00005'
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	declare @sql nvarchar(max)
	select @sql = IsNull(STUFF((
		SELECT N' or '+(case when left(VisionField, 4) = 'Cust' then 'PCTF.' when left(VisionField, 3) <> 'PR.' then 'PR.' else '' end)+VisionField+N' = N''' + @employee + N''''
			from CCG_EI_ConfigRoles
			where VisionField <> '' and [Status] = 'A'
			order by VisionField
			FOR XML PATH(''), TYPE
		).value('.', 'nvarchar(MAX)'),1,4,''),'')

	set @sql = N'
		select distinct PR.BillingClientID, CL.Name
			from PR
				inner join ProjectCustomTabFields PCTF on PCTF.WBS1 = PR.WBS1 and PCTF.WBS2 = PR.WBS2 and PCTF.WBS3 = PR.WBS3
				inner join CL on CL.ClientId = PR.BillingClientId
			where (' + @sql + N') and PR.BillingClientID is not null and PR.WBS2 = N'' '' and PR.WBS3 = N'' ''
			order by CL.Name '

	exec(@sql)
END
GO
