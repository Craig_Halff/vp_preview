SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[HAI_ProjectClientAdvocate] @WBS1 varchar (30)
AS
/*
    Copyright (c) 2020 Halff Associates Inc. All rights reserved.
    Sets Client Advocate for Opportunities based on Client change

    20201112	Craig H. Anderson
	20210714	David Springer revised for Vantagepoint
*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;
    BEGIN TRANSACTION;
    UPDATE  px
    SET px.CustClientAdvocate = cx.CustClientAdvocate
    FROM    dbo.ProjectCustomTabFields AS px
    INNER JOIN dbo.PR             AS p
        ON px.WBS1 = p.WBS1
    INNER JOIN dbo.Clendor                      AS c
        ON p.ClientID = c.ClientID
    INNER JOIN dbo.ClientCustomTabFields   AS cx
        ON c.ClientID = cx.ClientID
    WHERE   px.WBS1 = @WBS1;
    COMMIT TRANSACTION;
END;
GO
