SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RMCheckInRevert]
  @strPlanID VARCHAR(32)
AS

BEGIN -- Procedure RMCheckInRevert

  SET NOCOUNT ON
     
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- This Stored Procedure needs to be executed after the changes in each Plan is saved.
-- This Stored Procedure was written specifically for Navigator Plans, and should not be used for Vision Plans.
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  DECLARE @iCount int

  SELECT @iCount = COUNT(*) FROM PNPlan WHERE PlanID = @strPlanID

  IF (@iCount = 0) RETURN

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Revert Navigator Plan so that data are copied fro RP tables to PN tables

  EXECUTE dbo.PNRevert @strPlanID

  -- Check in Plan in both RPPlan and PNPlan

  UPDATE RPPlan SET CheckedOutUser = NULL, CheckedOutDate = NULL, CheckedOutID = NULL
    WHERE PlanID = @strPlanID

  UPDATE PNPlan SET CheckedOutUser = NULL, CheckedOutDate = NULL, CheckedOutID = NULL
    WHERE PlanID = @strPlanID


  SET NOCOUNT OFF

END -- RMCheckInRevert
GO
