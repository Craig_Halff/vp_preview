SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_OpportunityMarketingInvolvement]
AS
BEGIN
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;
    /*****************************************************************************
    Copyright (c) 2020 Halff Associates Inc. All rights reserved.
    Sets CustCorporateMarketing, based on time charged to Opportunity

    09/28/2020	Craig H. Anderson - Created
	******************************************************************************/

    -- Set CustCorporateMarketing = 'Yes' if the Marketing Team has charged to the Opportunity
    BEGIN TRANSACTION;
    UPDATE  f
    SET f.CustCorporateMarketing = 'Yes'
    FROM    dbo.OpportunityCustomTabFields                                        AS f
    INNER JOIN dbo.Opportunity                                                    AS o
        ON f.OpportunityID = o.OpportunityID
    INNER JOIN (SELECT  DISTINCT WBS1 FROM  dbo.LD WHERE RIGHT(EmOrg, 3) = '992') AS l
        ON o.PRProposalWBS1 = l.WBS1
           AND  f.CustCorporateMarketing <> 'Yes';
    COMMIT TRANSACTION;


    -- Set CustCorporateMarketing = 'No' on CLOSED Opportunities if the Marketing Team has not charged to the Opportunity.
    BEGIN TRANSACTION;
    UPDATE  f
    SET f.CustCorporateMarketing = 'No'
    FROM    dbo.OpportunityCustomTabFields AS f
    INNER JOIN dbo.Opportunity             AS o
        ON f.OpportunityID = o.OpportunityID
    INNER JOIN dbo.PR                      AS p
        ON o.PRProposalWBS1 = p.WBS1
           AND  p.Status = 'D'
           AND  p.WBS2 = ' '
    WHERE   NOT EXISTS (
        SELECT  'x'
        FROM    dbo.LD AS l
        WHERE   l.WBS1 = o.PRProposalWBS1
                AND RIGHT(l.EmOrg, 3) = '992'
    )
            AND f.CustCorporateMarketing IN ( NULL, 'Yes' );
    COMMIT TRANSACTION;
END;
GO
