SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Ctrl_ClearBatch]
	@EMPLOYEEID	Nvarchar(20),
	@batch		Nvarchar(32),
	@seq		int
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE CCG_PAT_Payable SET Batch = null, ModUser = @EMPLOYEEID, ModDate = GetUTCDate()
		WHERE Batch = @batch and seq = @seq;

	SELECT @@ROWCOUNT as RowsAffected;
END;
GO
