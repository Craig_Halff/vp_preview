SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[HAI_QAErrorDelegate] 
	-- Add the parameters for the stored procedure here
	@WBS1 VARCHAR(30),
	@CustQAReviewer VARCHAR(20),
	@ModUser VARCHAR(20),
	@Employee VarChar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
Set @Employee = (Select Employee from SEUser u where u.Username = @ModUser)

IF @Employee <> @CustQAReviewer

	BEGIN
			RAISERROR('Only the QA Reviewer can assign/modify the QA Delegate.                                                               ', 16, 1);
	END
	
END
GO
