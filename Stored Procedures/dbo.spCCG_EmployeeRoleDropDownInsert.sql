SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EmployeeRoleDropDownInsert] @RoleAdd VARCHAR(255)
AS
/* 
Copyright (c) 2020 Central Consulting Group.  All rights reserved.
03/25/2020 David Springer
           Populate Employee Role Name drop down list
		   Call this from an Project CHANGE workflow when Role Add has changed
Select * From CFGEmployeeRoleData
Select * From CFGEmployeeRoleDescriptions Order by Seq
*/
DECLARE @Code VARCHAR(10);
BEGIN
    SET NOCOUNT ON;

    SELECT @Code = RIGHT('00' + LTRIM(STR(MAX(Code) + 1)), 3)
    FROM CFGEmployeeRole
    WHERE ISNUMERIC(Code) = 1;

    IF @Code IS NOT NULL
    BEGIN
        INSERT INTO CFGEmployeeRoleData
        (
            Code
        )
        VALUES (@Code);

        INSERT INTO CFGEmployeeRoleDescriptions
        (
            Code
          , UICultureName
          , Description
        )
        VALUES (@Code, 'en-US', @RoleAdd);

        UPDATE c
        SET c.Seq = s.Seq
        FROM CFGEmployeeRoleDescriptions c
           , (
            SELECT Code
                 , ROW_NUMBER() OVER (ORDER BY Description) AS Seq
            FROM CFGEmployeeRoleDescriptions
        ) s
        WHERE c.Code = s.Code
              AND c.Seq <> s.Seq;
    END;

    UPDATE dbo.ProjectCustomTabFields
    SET CustRoleAdd = NULL
    WHERE CustRoleAdd IS NOT NULL;
END;



GO
