SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_All_ErrorReportingDelete] 
	@product varchar(64) = ''
AS
BEGIN
	-- spCCG_All_ErrorReportingDelete 'ARM'
	SET NOCOUNT ON

	IF (@product IS NOT NULL)
		DELETE FROM [dbo].[CCG_All_ErrorReporting] WHERE product = @product
END
GO
