SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_ProjectTasksGridCalculation] @WBS1 varchar (30), @TaskPhaseCode varchar (7)
AS
/*
Copyright 2021 Central Consulting Group.   All rights reserved.
08/16/2021	David Springer
			Validate Task Phase Code exists.
			Sum the Tasks to the Phase grid.
			Call this from a Project Task INSERT, CHANGE & DELETE workflows.
*/
BEGIN
SET NOCOUNT ON

--	Verify if Phase Code exists
	If not exists (Select 'x ' From Projects_Phases
					Where WBS1 = @WBS1
					and CustPhaseCode = @TaskPhaseCode)
		Raiserror ('Halff Custom Error:  Phase Code does not exist in the Phases grid.  Add it there and save, then add it here.                       ', 16, 1)

--	Update Phase from Tasks
	Update p
	Set p.CustPhaseFeeDirLab = IsNull (t.Labor, 0),
		p.CustPhaseExpenseFee = IsNull (t.ExpFee, 0),
		p.CustPhaseConsultantFee = IsNull (t.ConsFee, 0),
		p.CustPhaseTotal = IsNull (t.Total, 0)
	From Projects_Phases p
		Left Join
		(Select WBS1, CustTaskPhaseCode, 
				Sum (CustTaskFeeDirLab) Labor,
				Sum (CustTaskExpenseFee) ExpFee,
				Sum (CustTaskConsultantFee) ConsFee,
				Sum (CustTaskTotal) Total
			From Projects_Tasks
			Where WBS2 = ' '
			Group by WBS1, CustTaskPhaseCode) t on p.WBS1 = t.WBS1 and p.CustPhaseCode = t.custTaskPhaseCode
	Where p.WBS1 = @WBS1
	  and p.WBS2 = ' '
	  and p.CustPhaseCode = @TaskPhaseCode
     
   exec spCCG_ProjectPhasesGridCalculation @WBS1
END
GO
