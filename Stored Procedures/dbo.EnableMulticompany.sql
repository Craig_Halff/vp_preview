SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[EnableMulticompany]
    @Company Nvarchar(14),
    @UseExisting Nvarchar(1),
    @DefaultEMOrg Nvarchar(14),
    @OldOrgLevels integer,
    @NewOrgLevels integer,
    @Org1Start integer,
    @Org1Length integer,
    @Org2Start integer,
    @Org2Length integer,
    @Org3Start integer,
    @Org3Length integer,
    @Org4Start integer,
    @Org4Length integer,
    @Org5Start integer,
    @Org5Length integer,
    @Delimiter Nvarchar(1),
    @ConversionOrg Nvarchar(14),
    @DefaultOrg Nvarchar(14),
    @BalanceSheet Nvarchar(1),
    @acctTypeRevenue integer,
    @acctTypeOtherCharges integer,
	@ErrMsg Nvarchar(1000) output
AS
BEGIN

	set nocount on
	declare	@TableName  		Nvarchar(40),
			@ColumnName 		Nvarchar(40),
			@SqlStr Nvarchar(1000),
			@UpdateValue Nvarchar(500),
			@DefSubcode Nvarchar(14),
			@ConvSubcode Nvarchar(14),
			@i integer,
			@ErrorNum integer
	
	begin tran
	
	if (@Company <> '')
	begin
	
		Update ADPCode Set Company = @Company
		Update AlertsData Set Company = @Company WHERE (NavID <> N'Acct' AND NavID <> N'CRM' AND NavID <> N'Planning')
		Update apControl Set Company = @Company
		Update ApprovalErrorLog Set EmployeeCompany = @Company
		Update BTDefaults Set Company = @Company
		Update BTDefaultsTaxCodes Set Company = @Company
		Update cdControl Set Company = @Company
		Update CFGAccrualScheduleMasterData Set Company = @Company
		Update CFGAccrualScheduleMasterDescriptions Set Company = @Company
		Update CFGAccrualScheduleDetail Set Company = @Company
		Update CFGAPDiscount Set Company = @Company
		Update CFGAPExpenseCodeAccounts Set Company = @Company
		Update CFGAPForm1099 Set Company = @Company
		Update CFGAPLiability Set Company = @Company
		Update CFGAPMain Set Company = @Company
		Update CFGApproveTrans Set Company = @Company
		Update CFGARMap Set Company = @Company
		Update CFGAutoPosting Set Company = @Company
		Update CFGBanks Set Company = @Company
		Update CFGBankTextFormat Set Company = @Company
		Update CFGBillMainData Set Company = @Company
		Update CFGBillMainDescriptions Set Company = @Company
		Update CFGBillTaxesData Set Company = @Company
		Update CFGCashAcctSetup Set Company = @Company
		Update CFGCashSetupData Set Company = @Company
		Update CFGCashSetupDescriptions Set Company = @Company
		Update CFGDatesStatus Set Company = @Company
		Update CFGDiaryAutoNum Set Company = @Company
		Update CFGEKCategory Set Company = @Company
		Update CFGEKCategoryTaxCodes Set Company = @Company
		Update CFGEKEmployeeGroup Set Company = @Company
		Update CFGEKMain Set Company = @Company
		Update CFGEmployeeTypeAccounts Set Company = @Company
		Update FW_CFGEnabledCurrencies Set Company = @Company
		Update CFGHoliday Set Company = @Company
		Update CFGICBilling Set Company = @Company
		Update CFGInvApprover Set Company = @Company
		Update CFGInventoryMain Set Company = @Company
		Update CFGInvLocations Set Company = @Company
		Update CFGInvMap Set Company = @Company
		Update CFGMainData Set Company = @Company
		Update CFGMainDataDefaultTaxCodes Set Company = @Company
		Update CFGNonWorkingDay Set Company = @Company
		Update CFGOHAccounts Set Company = @Company
		Update CFGOHMain Set Company = @Company
		Update CFGOrgSetup Set Company = @Company
		Update CFGOrgSetup Set OrgIntercompany = NULL
		Update CFGPOAccounts Set Company = @Company
		Update CFGPOBillTo Set Company = @Company
		Update CFGPOBuyer Set Company = @Company
		Update CFGPOClause Set Company = @Company
		Update CFGPOFOB Set Company = @Company
		Update CFGPOMain Set Company = @Company
		Update CFGPOShipTo Set Company = @Company
		Update CFGPOShipVia Set Company = @Company
		Update CFGPostControl Set Company = @Company
		Update CFGPYAccrualsData Set Company = @Company
		Update CFGPYAccrualsDescriptions Set Company = @Company
		Update CFGPYContribution Set Company = @Company
		Update CFGPYContributionWage Set Company = @Company
		Update CFGPYFormQuarterly Set Company = @Company
		Update CFGPYFormW2 Set Company = @Company
		Update CFGPYMain Set Company = @Company
		Update CFGPYWHCodes Set Company = @Company
		Update CFGPYWHCodesWage Set Company = @Company
		Update CFGRealizationAllocationAccounts Set Company = @Company
		Update CFGResourcePlanning Set Company = @Company
		Update CFGStateID Set Company = @Company
		Update FW_CFGSystem Set MulticompanyEnabled = 'Y'
		Update CFGIntegrationWS Set PIMAllowFullAccess = 'Y' Where Type = 'PIM'
		Update CFGTimeAnalysis Set Company = @Company
		Update CFGTKCategory Set Company = @Company
		Update CFGTKEmployeeGroup Set Company = @Company
		Update CFGTKMain Set Company = @Company
		Update CFGTransApprover Set Company = @Company
		Update CFGTransAutoNumBank Set Company = @Company		-- new 7.1 table
		Update CFGTransAutoNumType Set Company = @Company		-- new 7.1 table
		Update CFGTransCustomFields Set Company = @Company
		Update CFGTransEmployee Set Company = @Company
		Update CFGVendorTypeAPLiability Set Company = @Company
		
		Update CFGXCharge Set Company = @Company
		Update CompanyItem Set Company = @Company
		Update CompanyPOCategory Set Company = @Company
		Update crControl Set Company = @Company
		Update cvControl Set Company = @Company
		Update FW_DetailJobs Set Company = @Company
		Update FW_DetailProfiles Set Company = @Company
		Update EMEKGroups Set Company = @Company
		Update EMTKGroups Set Company = @Company
		Update erControl Set Company = @Company
		Update exControl Set Company = @Company
		Update Form1099Data Set Company = @Company
		Update FormQuarterlyData Set Company = @Company
		Update FormW2Data Set Company = @Company
		Update inControl Set Company = @Company
		Update InvMaster Set Company = @Company
		Update ItemRequestMaster Set Company = @Company
		Update ItemUMConversion Set Company = @Company
		Update jeControl Set Company = @Company
		Update laControl Set Company = @Company
		Update miControl Set Company = @Company
		Update OverheadAllocation Set Company = @Company
		Update PNPlan Set Company = @Company
		Update POMaster Set Company = @Company
		Update POPQMaster Set Company = @Company
		Update POPRMaster Set Company = @Company
		Update prControl Set Company = @Company
		Update Quantities Set Company = @Company
		Update RPPlan Set Company = @Company
		Insert Into SECompany (Role, Company) Select Role, @Company From SE
		Update tkBillingXferReasons Set Company = @Company
		Update tkComments Set Company = @Company
		Update tkControl Set Company = @Company
		Update tkRevExplanations Set Company = @Company
		Update tsControl Set Company = @Company
		Update UNTable Set Company = @Company
		Update unControl Set Company = @Company
		Update upControl Set Company = @Company
		Update VEAccounting Set Company = @Company
		Update VendorItem Set Company = @Company
		Update VO Set Company = @Company
		Update XChargeWk Set Company = @Company

		Update CFGCreditCard Set Company = @Company
		Update CFGCreditCardSecondary Set Company = @Company
		Update CFGCreditCardImport Set Company = @Company
		Update CFGCreditCardImportDetailData Set Company = @Company
		Update CFGCreditCardImportDetailDescriptions Set Company = @Company
		Update CFGCreditCardSecondaryEmployee Set Company = @Company
		Update CreditCardImport Set Company = @Company
		Update CreditCardTransaction Set Company = @Company

		Update CFGServiceProfileCompanyMapping Set Company = @Company
		Update CFGAssetMain Set Company = @Company
		Update CFGFABookData Set Company = @Company
		Update CFGFABookDescriptions Set Company = @Company
		Update CFGFADepMethodData Set Company = @Company
		Update CFGFADepMethodDescriptions Set Company = @Company
		Update CFGFADepMethodSum Set Company = @Company
		Update CFGFAPropertyTypeData Set Company = @Company
		Update CFGFAPropertyTypeDescriptions Set Company = @Company
		Update CFGFARepairTypeData Set Company = @Company
		Update CFGFARepairTypeDescriptions Set Company = @Company
		Update CFGFAAssetTypeData Set Company = @Company
		Update CFGFAAssetTypeDescriptions Set Company = @Company
		Update CFGFASec179Main Set Company = @Company
		Update APApprovalMaster Set Company = @Company
		Update CABGroup Set Company = @Company
		Update CFGRMSettings Set DefaultCompany = @Company
        Update CFGPREstClause Set Company = @Company
        Update CFGPREstService Set Company = @Company
        Update CFGProjRevenueDefaults Set Company = @Company
        Update TransactionUDFData Set Company = @Company
        Update TransactionUDFLabels Set Company = @Company
        Update CFGQuickBooks Set Company = @Company
        Update CFGProjRevenueTemplateMaster Set Company = @Company

	end

	if (@UseExisting <> 'Y')
	begin
		-- update CFGFormat
		Update CFGFormat Set OrgLevels = @NewOrgLevels,
		                     OrgDelimiter = @Delimiter,
		                     Org1Start = @Org1Start,
		                     Org1Length = @Org1Length,
		                     Org2Start = @Org2Start,
		                     Org2Length = @Org2Length,
		                     Org3Start = @Org3Start,
		                     Org3Length = @Org3Length,
		                     Org4Start = @Org4Start,
		                     Org4Length = @Org4Length,
		                     Org5Start = @Org5Start,
		                     Org5Length = @Org5Length
	
		if (@OldOrgLevels = 0)	-- add subcodes and org combinations
		begin
			--  add subcodes
			set @DefSubcode = substring(@DefaultOrg, @Org1Start, @Org1Length)
			set @ConvSubcode = substring(@ConversionOrg, @Org1Start, @Org1Length)
			set @SQLStr = 'Insert Into CFGOrgCodes (OrgLevel, Code, Label) Values (1, N''' + @DefSubcode + ''', N''<Default>'')'
			execute(@SQLStr)
			if (@DefSubcode <> @ConvSubcode)
			begin
				set @SQLStr = 'Insert Into CFGOrgCodes (OrgLevel, Code, Label) Values (1, N''' + @ConvSubcode + ''', N''<Default>'')'
				execute(@SQLStr)
			end
			if (@NewOrgLevels > 1)
			begin
				set @DefSubcode = substring(@DefaultOrg, @Org2Start, @Org2Length)
				set @ConvSubcode = substring(@ConversionOrg, @Org2Start, @Org2Length)
				set @SQLStr = 'Insert Into CFGOrgCodes (OrgLevel, Code, Label) Values (2, N''' + @DefSubcode + ''', N''<Default>'')'
				execute(@SQLStr)
				if (@DefSubcode <> @ConvSubcode)
				begin
					set @SQLStr = 'Insert Into CFGOrgCodes (OrgLevel, Code, Label) Values (2, N''' + @ConvSubcode + ''', N''<Default>'')'
					execute(@SQLStr)
				end
			end
			if (@NewOrgLevels > 2)
			begin
				set @DefSubcode = substring(@DefaultOrg, @Org3Start, @Org3Length)
				set @ConvSubcode = substring(@ConversionOrg, @Org3Start, @Org3Length)
				set @SQLStr = 'Insert Into CFGOrgCodes (OrgLevel, Code, Label) Values (3, N''' + @DefSubcode + ''', N''<Default>'')'
				execute(@SQLStr)
				if (@DefSubcode <> @ConvSubcode)
				begin
					set @SQLStr = 'Insert Into CFGOrgCodes (OrgLevel, Code, Label) Values (3, N''' + @ConvSubcode + ''', N''<Default>'')'
					execute(@SQLStr)
				end
			end
			if (@NewOrgLevels > 3)
			begin
				set @DefSubcode = substring(@DefaultOrg, @Org4Start, @Org4Length)
				set @ConvSubcode = substring(@ConversionOrg, @Org4Start, @Org4Length)
				set @SQLStr = 'Insert Into CFGOrgCodes (OrgLevel, Code, Label) Values (4, N''' + @DefSubcode + ''', N''<Default>'')'
				execute(@SQLStr)
				if (@DefSubcode <> @ConvSubcode)
				begin
					set @SQLStr = 'Insert Into CFGOrgCodes (OrgLevel, Code, Label) Values (4, N''' + @ConvSubcode + ''', N''<Default>'')'
					execute(@SQLStr)
				end
			end
			if (@NewOrgLevels > 4)
			begin
				set @DefSubcode = substring(@DefaultOrg, @Org5Start, @Org5Length)
				set @ConvSubcode = substring(@ConversionOrg, @Org5Start, @Org5Length)
				set @SQLStr = 'Insert Into CFGOrgCodes (OrgLevel, Code, Label) Values (5, N''' + @DefSubcode + ''', N''<Default>'')'
				execute(@SQLStr)
				if (@DefSubcode <> @ConvSubcode)
				begin
					set @SQLStr = 'Insert Into CFGOrgCodes (OrgLevel, Code, Label) Values (5, N''' + @ConvSubcode + ''', N''<Default>'')'
					execute(@SQLStr)
				end
			end
			
			-- add org combinations
			set @SqlStr = 'Insert Into Organization (Org, Name) Values (N''' + @DefaultOrg + ''', N''<Default>'')'
			execute(@SQLStr)
			if (@DefaultOrg <> @ConversionOrg)
			begin
				set @SqlStr = 'Insert Into Organization (Org, Name) Values (N''' + @ConversionOrg + ''', N''<Default>'')'
				execute(@SQLStr)
			end
		end -- @OldOrgLevels = 0
		else  -- adjust levels and add company subcode
		begin
			set @i = @OldOrgLevels
			while (@i > 0)
			begin
				Update CFGOrgCodesData Set OrgLevel = OrgLevel + 1 Where OrgLevel = @i
				Update CFGOrgCodesDescriptions Set OrgLevel = OrgLevel + 1 Where OrgLevel = @i
				set @i = @i - 1
			end
			Insert Into CFGOrgCodes (OrgLevel, Code, Label) Values (1, @Company, N'<Default>')
		end
	
		execute InsertKeyCvtDriver 'Org' -- populate driver table
	
		declare cvtCursor cursor scroll static for
		select TableName, ColumnName from keyConvertDriver where Entity = N'Org'
			order by ColumnName,TableName
		open cvtCursor
	
		--Drop constraints
		fetch first  from cvtCursor  into
		@TableName,
		@ColumnName
		While (@@Fetch_Status = 0)
		begin
			execute('alter table ' + @TableName + ' nocheck constraint all')
			fetch next from cvtCursor into
			@TableName,
			@ColumnName
		end
	
		--Update tables
		fetch first  from cvtCursor  into
		@TableName,
		@ColumnName
		While (@@FETCH_STATUS = 0 )
		begin
			if (@OldOrgLevels = 0)  -- means orgs are being enabled for first time
			begin
				-- update all tables			
				if (@TableName <> 'Organization')
				begin
					if (@BalanceSheet <> 'Y')
					begin
						if (@TableName in ('BIED', 'LedgerAP', 'LedgerAR', 'LedgerEX', 'LedgerMisc'))
						begin
							set @SQLStr = 'Update ' + @TableName + ' Set ' + @ColumnName + ' = N''' + @ConversionOrg + ''''
							set @SQLStr = @SQLStr + ' From ' + @TableName + ' Inner Join CA On ' + @TableName + '.Account = CA.Account'
							set @SqlStr = @SqlStr + ' Where CA.Type >= ' + convert(Nvarchar, @acctTypeRevenue) + ' And CA.Type < ' + convert(Nvarchar, @acctTypeOtherCharges) 
							execute(@SQLStr)
							set @SQLStr = 'Update ' + @TableName + ' Set ' + @ColumnName + ' = N''' + @DefaultOrg + ''''
							set @SQLStr = @SQLStr + ' From ' + @TableName + ' Inner Join CA On ' + @TableName + '.Account = CA.Account'
							set @SqlStr = @SqlStr + ' Where CA.Type < ' + convert(Nvarchar, @acctTypeRevenue) + ' Or CA.Type >= ' + convert(Nvarchar, @acctTypeOtherCharges)
							execute(@SQLStr)
						end
						else
						begin
							if (@TableName in ('APPPChecks', 'CFGAPLiability', 'CFGBanks', 'EXChecks'))
							begin
								set @SQLStr = 'Update ' + @TableName + ' Set ' + @ColumnName + ' = N''' + @DefaultOrg + ''''
								execute(@SQLStr)
							end
							else
							begin
								if (@TableName <> 'FW_CFGSystem' or @ColumnName <> 'DefaultOrg') -- saveBO will set this column to null
								begin
									set @SQLStr = 'Update ' + @TableName + ' Set ' + @ColumnName + ' = N''' + @ConversionOrg + ''''
									execute(@SQLStr)
								end
							end
						end
					end
					else  -- separate balancesheets
					begin
						set @SQLStr = 'Update ' + @TableName + ' Set ' + @ColumnName + ' = N''' + @ConversionOrg + ''''
						execute(@SQLStr)
					end
				end
			end
			else  -- orgs already enabled, adding multicompany
			begin
				if (@DefaultEMOrg <> '') -- update employee records with existing org before adding company
				begin
					set @SQLStr = 'Update EM Set Org = N''' + @DefaultEMOrg + ''' Where Org Is Null'
					execute(@SQLStr)
				end
				if (@TableName <> 'FW_CFGSystem' or @ColumnName <> 'DefaultOrg') -- saveBO will set this column to null
				begin
					set @SQLStr = 'Update ' + @TableName + ' Set ' + @ColumnName + ' = N''' + @Company + @Delimiter + ''' + ' + @ColumnName + ' Where ' + @ColumnName + ' Is Not Null'
					execute(@SQLStr)
				end
			end
			set @ErrorNum = @@Error
			if (@ErrorNum <> 0)
				begin
					Rollback Transaction
					close cvtCursor
					deallocate cvtCursor
					select @ErrMsg = ' SQL:' + @SqlStr
					set nocount off
					return(@ErrorNum)
				end
			fetch next from cvtCursor into
			@TableName,
			@ColumnName
		end --While
				
		--Apply constraints
		fetch first  from cvtCursor  into
		@TableName,
		@ColumnName
		While (@@FETCH_STATUS = 0)
		begin
			execute('alter table ' + @TableName + ' check constraint all')
			fetch next from cvtCursor into
			@TableName,
			@ColumnName
		end

		close cvtCursor
		deallocate cvtCursor

	end -- @UseExisting <> 'Y'

	if (@Company <> '')
	begin
		Update EMCompany Set Org = upper(Org), EmployeeCompany = upper(left(Org, @Org1Length)) Where Org is not null
		Update EMMain Set HomeCompany = upper(left(Org, @Org1Length)) from EMMain Inner Join EMCompany on EMMain.Employee=EMCompany.Employee and Org is not null

		Update EMAccrual Set EmployeeCompany = EM.HomeCompany from EMAccrual Inner Join EM on EMAccrual.Employee=EM.Employee
		Update EMAccrualDetail Set EmployeeCompany = EM.HomeCompany from EMAccrualDetail Inner Join EM on EMAccrualDetail.Employee=EM.Employee
		Update EMPayroll Set EmployeeCompany = EM.HomeCompany from EMPayroll Inner Join EM on EMPayroll.Employee=EM.Employee
		Update EMPayrollContributionWage Set EmployeeCompany = EM.HomeCompany from EMPayrollContributionWage Inner Join EM on EMPayrollContributionWage.Employee=EM.Employee
		Update EMPayrollWithholdingWage Set EmployeeCompany = EM.HomeCompany from EMPayrollWithholdingWage Inner Join EM on EMPayrollWithholdingWage.Employee=EM.Employee
		Update EMPayrollContribution Set EmployeeCompany = EM.HomeCompany from EMPayrollContribution Inner Join EM on EMPayrollContribution.Employee=EM.Employee
		Update EMPayrollWithholding Set EmployeeCompany = EM.HomeCompany from EMPayrollWithholding Inner Join EM on EMPayrollWithholding.Employee=EM.Employee
		Update EMDirectDeposit Set EmployeeCompany = EM.HomeCompany from EMDirectDeposit Inner Join EM on EMDirectDeposit.Employee=EM.Employee
		Update EMLocale Set EmployeeCompany = EM.HomeCompany from EMLocale Inner Join EM on EMLocale.Employee=EM.Employee
		Update ekMaster Set EmployeeCompany = EM.HomeCompany from ekMaster Inner Join EM on ekMaster.Employee=EM.Employee
		Update tkBreakTime Set EmployeeCompany = EM.HomeCompany from tkBreakTime Inner Join EM on tkBreakTime.Employee=EM.Employee
		Update tkDetail Set EmployeeCompany = EM.HomeCompany from tkDetail Inner Join EM on tkDetail.Employee=EM.Employee
		Update tkMaster Set EmployeeCompany = EM.HomeCompany from tkMaster Inner Join EM on tkMaster.Employee=EM.Employee
		Update tkRevisionDetail Set EmployeeCompany = EM.HomeCompany from tkRevisionDetail Inner Join EM on tkRevisionDetail.Employee=EM.Employee
		Update tkRevisionMaster Set EmployeeCompany = EM.HomeCompany from tkRevisionMaster Inner Join EM on tkRevisionMaster.Employee=EM.Employee
		Update tkUnitDetail Set EmployeeCompany = EM.HomeCompany from tkUnitDetail Inner Join EM on tkUnitDetail.Employee=EM.Employee
		Update unDetail Set EmployeeCompany = EM.HomeCompany from unDetail Inner Join EM on unDetail.Employee=EM.Employee
		Update upDetail Set EmployeeCompany = EM.HomeCompany from upDetail Inner Join EM on upDetail.Employee=EM.Employee
		Update AbsenceRequests Set Company = EM.HomeCompany from AbsenceRequests Inner Join EM on AbsenceRequests.RequestedBy=EM.Employee

		UPDATE ApprovalItem Set ApplicationKey = left(ApprovalItem.ApplicationKey, len(ApprovalItem.ApplicationKey)) + tkMaster.EmployeeCompany
			FROM tkMaster
			JOIN ApprovalItem on ApprovalItem.ApplicationKey = tkMaster.Employee + '|' + CONVERT(VARCHAR, tkMaster.EndDate, 23) + '| '
			and ApprovalItem.ApplicationID = 'TIME' AND ApprovalItem.ApprovalLevel = 'Master'
			WHERE tkmaster.Submitted <> 'P'

		UPDATE ApprovalItem Set ApplicationKey = left(ApprovalItem.ApplicationKey, len(ApprovalItem.ApplicationKey)) + tkMaster.EmployeeCompany
			FROM tkMaster
			JOIN tkDetail on tkMaster.Employee = tkDetail.Employee and tkMaster.Enddate = tkDetail.EndDate and tkMaster.EmployeeCompany = tkDetail.EmployeeCompany
			JOIN ApprovalItem on ApprovalItem.ApplicationKey = RIGHT('0000' + CONVERT(VARCHAR, tkDetail.Seq), 4) + '|' + CONVERT(VARCHAR, tkDetail.TransDate, 23) + '|' + TKDetail.Employee + '|' + CONVERT(VARCHAR, tKDetail.EndDate, 23) + '| '
			and ApprovalItem.ApplicationID = 'TIME' AND ApprovalItem.ApprovalLevel = 'Detail'
			WHERE tkMaster.Submitted <> 'P'

        Update tkCustomFields Set EmployeeCompany = EM.HomeCompany from tkCustomFields Inner Join EM on tkCustomFields.Employee=EM.Employee
	end 
	else
		begin
			Update EMCompany Set Org = upper(Org), EmployeeCompany = ' ' Where Org is not null
			Update EMMain Set HomeCompany = ' '
		end
	
	Update CFGPYAccrualsData set EnableApprovalWorkFlow = A.EnableApprovalWorkFlow, ApprovalWorkFlow = A.ApprovalWorkFlow From CFGPYAccrualsData 
	inner join (select Code, Company, EnableApprovalWorkFlow, ApprovalWorkFlow from CFGPYAccrualsData where EnableApprovalWorkFlow='Y' and ApprovalWorkFlow is not null
	) A on A.Code=CFGPYAccrualsData.Code and A.Company<>CFGPYAccrualsData.Company

	Update CFGAPMain set EnableApprovalWorkFlowAP = A.EnableApprovalWorkFlowAP, ApprovalWorkFlowAP = A.ApprovalWorkFlowAP From CFGAPMain 
	inner join (select Company, EnableApprovalWorkFlowAP, ApprovalWorkFlowAP from CFGAPMain where EnableApprovalWorkFlowAP='Y' and ApprovalWorkFlowAP is not null
	) A on A.Company<>CFGAPMain.Company

	Update CFGMainData set EnableApprovalWorkFlowGLBud = A.EnableApprovalWorkFlowGLBud, ApprovalWorkFlowGLBud = A.ApprovalWorkFlowGLBud From CFGMainData 
	inner join (select Company, EnableApprovalWorkFlowGLBud, ApprovalWorkFlowGLBud from CFGMainData where EnableApprovalWorkFlowGLBud='Y' and ApprovalWorkFlowGLBud is not null
	) A on A.Company<>CFGMainData.Company
				
	commit transaction
	set nocount off
	return (0) -- no error
END
GO
