SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Purpose: To READ data for Halff Tritex Tracker website.
Original: Matthew Cestarte
Date: 2018-04-12 
Modified: Bracken Adams
Date: 2021-01-19
*/
CREATE   PROCEDURE [dbo].[HAI_HTT_DeltekDetailListing]
@Org nvarchar(30),
@Closed int,
@TeamsString nvarchar(100)

AS
BEGIN

declare @teams table (val nvarchar(3));

WITH teamStringSplit(team, remainingString) AS
(
    SELECT
        cast(LEFT(@teamsString, CHARINDEX(',', @teamsString + ',') - 1) as nvarchar(100)),
        cast(STUFF(@teamsString, 1, CHARINDEX(',', @teamsString + ','), '') as nvarchar(100))
    UNION all

    SELECT
        cast(LEFT(remainingString, CHARINDEX(',', remainingString + ',') - 1) as nvarchar(100)),
        cast(STUFF(remainingString, 1, CHARINDEX(',', remainingString + ','), '') as nvarchar(100))
    FROM teamStringSplit
    WHERE
        remainingString > ''
)

insert into @teams
select team
from teamStringSplit

SELECT
[pr].[Wbs1],
[pr].[Org],
[pr].[ClientID],
[pr].[CreateDate] AS [PRCreated],
[pr].[LongName] AS [ProjectName],
[pr].[Status] as [Status],
[cl].[Name] AS [ClientName],
CONCAT([pr].Address1, ' ', [pr].Address2, ' ', [pr].City, ' ', [pr].[State], ' ', [pr].Zip) AS [ProjectAddress]
FROM [dbo].[PR] AS [pr]
INNER JOIN [CL] AS [cl] ON [pr].[ClientID] = [cl].[ClientID]
WHERE [pr].[Closed] = @Closed
AND pr.Org LIKE CONCAT(@Org, '%')
and pr.WBS2 = ''
and RIGHT(pr.Org, 3) IN (select * from @teams)
and pr.WBS1 not like ('P%')

END
GO
