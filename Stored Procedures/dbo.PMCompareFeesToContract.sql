SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PMCompareFeesToContract]
  @txtTask Nvarchar(max),
  @decRoundingDiff decimal(19,4),
  @iRecCount int OUTPUT
AS

BEGIN

  /*----------------------------------------------------------------------------------------------------------------------------
    This procedure will create a table with WBS rows that do not have valid Fees as comparing between Contract Details and Plan.
  */----------------------------------------------------------------------------------------------------------------------------

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
 
  DECLARE @tabTask TABLE
    (TaskID varchar(32) COLLATE database_default, 
     WBSType Nvarchar(4) COLLATE database_default,
     WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(7) COLLATE database_default,
     WBS3 Nvarchar(7) COLLATE database_default,
     CompCost decimal(19,4),
     CompBill decimal(19,4),
     ConsCost decimal(19,4),
     ConsBill decimal(19,4),
     ReimCost decimal(19,4),
     ReimBill decimal(19,4)
     PRIMARY KEY(TaskID, WBSType, WBS1, WBS2, WBS3))

  DECLARE @tabWBS TABLE
    (WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(7) COLLATE database_default,
     WBS3 Nvarchar(7) COLLATE database_default
     PRIMARY KEY(WBS1, WBS2, WBS3))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Load XML into a temp table variable and deallocate the XML DOM maximize performance.

  DECLARE @hDoc int
  
  EXEC sp_xml_preparedocument @hDoc OUTPUT, @txtTask 

  INSERT INTO @tabTask
  SELECT * FROM OPENXML (@hdoc, '/Root/Row', 1)
    WITH(TaskID varchar(32) COLLATE database_default,
         WBSType Nvarchar(4) COLLATE database_default,
         WBS1 Nvarchar(30) COLLATE database_default,
         WBS2 Nvarchar(7) COLLATE database_default,
         WBS3 Nvarchar(7) COLLATE database_default,
         CompCost decimal(19,4),
         CompBill decimal(19,4),
         ConsCost decimal(19,4),
         ConsBill decimal(19,4),
         ReimCost decimal(19,4),
         ReimBill decimal(19,4))
       
  EXEC sp_xml_removedocument @hDoc

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @strRABIBC varchar
  SELECT @strRABIBC = ReportAtBillingInBillingCurr FROM FW_CFGSystem

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT INTO @tabWBS
  SELECT T.WBS1, T.WBS2, T.WBS3
    FROM
      (SELECT 
         WBSType,
         WBS1,
         WBS2,
         WBS3,
         SUM(CompCost) AS CompCost,
         SUM(CompBill) AS CompBill,
         SUM(ConsCost) AS ConsCost,
         SUM(ConsBill) AS ConsBill,
         SUM(ReimCost) AS ReimCost,
         SUM(ReimBill) AS ReimBill
         FROM @tabTask
         GROUP BY WBSType, WBS1, WBS2, WBS3) AS T
      LEFT JOIN
        (SELECT 
           P.WBS1, P.WBS2, P.WBS3, 
           ISNULL(SUM(C.Fee), 0.0000) AS CompCost, 
           ISNULL(SUM(C.FeeBillingCurrency), 0.0000) AS CompBill, 
           ISNULL(SUM(C.ConsultFee), 0.0000) AS ConsCost, 
           ISNULL(SUM(C.ConsultFeeBillingCurrency), 0.0000) AS ConsBill, 
           ISNULL(SUM(C.ReimbAllow), 0.0000) AS ReimCost,
           ISNULL(SUM(C.ReimbAllowBillingCurrency), 0.0000) AS ReimBill
           FROM PR AS P
             LEFT JOIN ContractDetails AS C ON P.WBS1 = C.WBS1 AND P.WBS2 = C.WBS2 AND P.WBS3 = C.WBS3
           WHERE P.WBS2 = ' ' AND P.WBS3 = ' '
           GROUP BY P.WBS1, P.WBS2, P.WBS3
         UNION ALL SELECT 
           P.WBS1, P.WBS2, P.WBS3, 
           ISNULL(SUM(C.Fee), 0.0000) AS CompCost, 
           ISNULL(SUM(C.FeeBillingCurrency), 0.0000) AS CompBill, 
           ISNULL(SUM(C.ConsultFee), 0.0000) AS ConsCost, 
           ISNULL(SUM(C.ConsultFeeBillingCurrency), 0.0000) AS ConsBill, 
           ISNULL(SUM(C.ReimbAllow), 0.0000) AS ReimCost,
           ISNULL(SUM(C.ReimbAllowBillingCurrency), 0.0000) AS ReimBill
           FROM PR AS P
             LEFT JOIN ContractDetails AS C ON P.WBS1 = C.WBS1 AND P.WBS2 = C.WBS2
           WHERE P.WBS2 <> ' ' AND P.WBS3 = ' '
           GROUP BY P.WBS1, P.WBS2, P.WBS3
         UNION ALL SELECT 
           P.WBS1, P.WBS2, P.WBS3, 
           ISNULL(SUM(C.Fee), 0.0000) AS CompCost, 
           ISNULL(SUM(C.FeeBillingCurrency), 0.0000) AS CompBill, 
           ISNULL(SUM(C.ConsultFee), 0.0000) AS ConsCost, 
           ISNULL(SUM(C.ConsultFeeBillingCurrency), 0.0000) AS ConsBill, 
           ISNULL(SUM(C.ReimbAllow), 0.0000) AS ReimCost,
           ISNULL(SUM(C.ReimbAllowBillingCurrency), 0.0000) AS ReimBill
           FROM PR AS P
             LEFT JOIN ContractDetails AS C ON P.WBS1 = C.WBS1 AND P.WBS2 = C.WBS2 AND P.WBS3 = C.WBS3
           WHERE P.WBS2 <> ' ' AND P.WBS3 <> ' '
           GROUP BY P.WBS1, P.WBS2, P.WBS3 
        ) AS C
          ON T.WBS1 = C.WBS1 AND
            ((T.WBSType = N'WBS1' AND C.WBS2 = N' ' AND C.WBS3 = N' ') OR
            (T.WBSType = N'WBS2' AND T.WBS2 = C.WBS2 AND C.WBS3 = N' ') OR
            (T.WBSType = N'WBS3' AND T.WBS2 = C.WBS2 AND T.WBS3 = C.WBS3))
    WHERE
      (T.CompCost - ISNULL(C.CompCost, 0.0000)) > @decRoundingDiff OR
      (T.CompBill - CASE WHEN @strRABIBC = 'Y' THEN ISNULL(C.CompBill, 0.0000) ELSE ISNULL(C.CompCost, 0.0000) END) > @decRoundingDiff OR
      (T.ConsCost - ISNULL(C.ConsCost, 0.0000)) > @decRoundingDiff OR
      (T.ConsBill - CASE WHEN @strRABIBC = 'Y' THEN ISNULL(C.ConsBill, 0.0000) ELSE ISNULL(C.ConsCost, 0.0000) END) > @decRoundingDiff OR
      (T.ReimCost - ISNULL(C.ReimCost, 0.0000)) > @decRoundingDiff OR
      (T.ReimBill - CASE WHEN @strRABIBC = 'Y' THEN ISNULL(C.ReimBill, 0.0000) ELSE ISNULL(C.ReimCost, 0.0000) END) > @decRoundingDiff

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @iRecCount = COUNT(*) FROM @tabWBS

  SET NOCOUNT OFF

END -- sp_PMCompareFeesToContract
GO
