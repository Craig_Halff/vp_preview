SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_PAT_DoPostSaveAction] (
      @PayableSeq int,
      @User varchar(50)= null
)
AS
BEGIN
      set nocount on

      BEGIN TRY
			-- Check Request update
			declare @sourcePKey varchar(50)
			select @sourcePKey = sourcePKEY from CCG_PAT_Payable where Seq = @PayableSeq

			if ISNULL(@sourcePKey,'') <> '' and exists (select [UDIC_UID] from [UDIC_CheckRequest] where UDIC_UID = @sourcePKey)
            BEGIN
                update UDIC_CheckRequest 
					set CustCheckRequestStatus = 'Loaded'
					--other fields can be set if desired
					where UDIC_UID = @sourcePKey 
						and CustCheckRequestStatus in ('BatchLoad')--don't ever go back in the process                     

				update p
					set p.PayTerms = 'Date', p.PayDate = cr.CustCheckRequestDueDate
					from CCG_PAT_Payable p 
						inner join UDIC_CheckRequest cr on cr.UDIC_UID = @sourcePKey
					where p.Seq = @PayableSeq
            END
			if ISNULL(@sourcePKey,'') <> '' and exists (select [UDIC_UID] from [UDIC_PurchaseOrder] where UDIC_UID = @sourcePKey)
            BEGIN
                update [UDIC_PurchaseOrder] 
                set CustStatus = 'Invoiced'
                --other fields can be set if desired
                where UDIC_UID = @sourcePKey 
                and CustStatus in ('Approved')--don't ever go back in the process
                     
            END

      END TRY
      BEGIN CATCH
            
            select -1 as Result, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage

      END CATCH

END
GO
