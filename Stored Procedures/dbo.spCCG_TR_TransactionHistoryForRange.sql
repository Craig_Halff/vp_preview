SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_TR_TransactionHistoryForRange](
	@TableName varchar(32),
	@StartDate datetime,
	@EndDate datetime,
	@WBS1 varchar(32),
	@WBS2 varchar(7),
	@WBS3 varchar(7)
)
AS BEGIN
/*
select top 100 Category, * from LD
	Copyright (c) 2014 Central Consulting Group.  All rights reserved.

	select * from ccg_tr_history where wbs1 = '076135.01'
	select * from ccg_tr_approvalshistory where wbs1 = '076135.01'
	exec [spCCG_TR_TransactionHistoryForRange] '', '2015-07-27', '2015-07-27', '2003005.00', '', ''
*/
	set nocount on
	set @WBS2 = nullif(@WBS2,'')
	set @WBS3 = nullif(@WBS3,'')

	select distinct DateAdd(minute, DateDiff(minute, getutcdate(), getdate()), ActionDate) as ActionDate, EM.LastName + N', ' + IsNull(EM.FirstName,N'') as EmpName, 'Modified' as ActionTaken,
			Case When ActionTaken='T' Then 'Transfered' When ActionTaken='B' Then 'Bill Status Change'
			When ActionTaken='M' and ActionDetail='TransDate' Then 'Trans Date Change'
			When ActionTaken='M' and ActionDetail='Comment'   Then 'Comment Change'
			When ActionTaken='M' and ActionDetail='Desc1'     Then 'Desc 1 Change'
			When ActionTaken='M' and ActionDetail='Desc2'     Then 'Desc 2 Change'
			When ActionTaken='M' and ActionDetail='Cost'      Then 'Partial Cost'
			When ActionTaken='M' and ActionDetail='RegHrs'    Then 'Partial Reg Hours'
			When ActionTaken='M' and ActionDetail='OvtHrs'    Then 'Partial Ovt Hours'
			When ActionTaken='M' and ActionDetail='SpecialOvtHrs' Then 'Partial Ovt2 Hours'
			When ActionTaken='I' and ActionDetail='NonBillable'   Then 'Include Non-Billable'
			When ActionTaken='I' and ActionDetail='Future'        Then 'Include Future'
			When ActionTaken='X' Then 'Cleared Unapproved' End as ActionType,
			b.Amount,
			Case When ActionTaken='Tfails' Then 'From ' + dbo.fnCCG_TR_GetWBS(h.OriginalTable,h.OriginalPeriod,h.OriginalPostSeq,h.OriginalPKey)
				Else 'From "' + OldValue + '" to "' + NewValue + '"'
			End as Details, ModificationComment, ApprovalComment, b.TransDate,
			case when TableName in ('LD','BILD') then BTLaborCats.Description else b.Desc1 end as Descr,
			Tr_EM.LastName + N', ' + IsNull(Tr_EM.FirstName,N'') as [ModEmployee]
		from CCG_TR_History h
			left join EM on EM.Employee=h.ActionTakenBy
			inner join (
				select 'LD' TableName, LD.BillExt as Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, Category, '' as Desc1 from LD
				union all select 'BILD' TableName, BILD.BillExt as Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, Category, '' from BILD
				union all select 'LedgerEX' TableName, Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, '', Desc1 from LedgerEX
				union all select 'LedgerAP' TableName, Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, '', Desc1 from LedgerAP
				union all select 'LedgerAR' TableName, Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, '', Desc1 from LedgerAR
				union all select 'LedgerMisc' TableName, Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, '', Desc1 from LedgerMisc
				union all select 'BIED' TableName, Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, '', Desc1 from BIED
			) b on b.PostSeq = h.OriginalPostSeq and b.PKey = h.OriginalPKey and b.Period = h.OriginalPeriod and b.TableName = h.OriginalTable
			left join BTLaborCats on BTLaborCats.Category=b.Category and TableName in ('LD','BILD')
			left join EM Tr_EM on Tr_EM.Employee = b.Employee
		where (@TableName='' or OriginalTable=@TableName) and TransDate between @StartDate and @EndDate+1 and h.ActionStatus='S'
			and (
				(h.WBS1 = @WBS1 and h.WBS2 = isnull(@WBS2,h.WBS2) and h.WBS3 = isnull(@WBS3,h.WBS3)) or (
					ActionTaken='T' and (
						(h.NewValue = h.WBS1 and h.OldValue = @WBS1 and h.WBS2 = isnull(@WBS2,h.WBS2) and h.WBS3 = isnull(@WBS3,h.WBS3))
						or (h.WBS1 = @WBS1 and h.NewValue = h.WBS2 and h.OldValue = isnull(@WBS2,'') and h.WBS3 = isnull(@WBS3,h.WBS3))
						or (h.WBS1 = @WBS1 and h.WBS2 = isnull(@WBS2,h.WBS2) and h.NewValue = h.WBS3 and h.OldValue = isnull(@WBS3,''))
					)
				)
			)
	UNION ALL
	select distinct DateAdd(minute, DateDiff(minute, getutcdate(), getdate()), SubmittedDate) as ActionDate, EM.LastName + N', ' + IsNull(EM.FirstName,N'') as EmpName,
			'Submitted' as ActionTaken, '' as ActionType,
			b.Amount,
			'' as Details, '' as ModificationComment, '' as ApprovalComment, b.TransDate,
			case when ah.TableName in ('LD','BILD') then BTLaborCats.Description else b.Desc1 end as Descr,
			Tr_EM.LastName + N', ' + IsNull(Tr_EM.FirstName,N'') as [ModEmployee]
		from CCG_TR_ApprovalsHistory ah
			left join EM on EM.Employee = ah.SubmittedBy
			inner join (
				select 'LD' TableName, LD.BillExt as Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, Category, '' as Desc1 from LD			where WBS1 = @WBS1 and (isnull(@WBS2,'') = '' or WBS2 = @WBS2) and (isnull(@WBS3,'') = '' or WBS3 = @WBS3)
				union all select 'BILD' TableName, BILD.BillExt as Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, Category, '' from BILD		where WBS1 = @WBS1 and (isnull(@WBS2,'') = '' or WBS2 = @WBS2) and (isnull(@WBS3,'') = '' or WBS3 = @WBS3)
				union all select 'LedgerEX' TableName, Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, '', Desc1 from LedgerEX				where WBS1 = @WBS1 and (isnull(@WBS2,'') = '' or WBS2 = @WBS2) and (isnull(@WBS3,'') = '' or WBS3 = @WBS3)
				union all select 'LedgerAP' TableName, Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, '', Desc1 from LedgerAP				where WBS1 = @WBS1 and (isnull(@WBS2,'') = '' or WBS2 = @WBS2) and (isnull(@WBS3,'') = '' or WBS3 = @WBS3)
				union all select 'LedgerAR' TableName, Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, '', Desc1 from LedgerAR				where WBS1 = @WBS1 and (isnull(@WBS2,'') = '' or WBS2 = @WBS2) and (isnull(@WBS3,'') = '' or WBS3 = @WBS3)
				union all select 'LedgerMisc' TableName, Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, '', Desc1 from LedgerMisc			where WBS1 = @WBS1 and (isnull(@WBS2,'') = '' or WBS2 = @WBS2) and (isnull(@WBS3,'') = '' or WBS3 = @WBS3)
				union all select 'BIED' TableName, Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, '', Desc1 from BIED						where WBS1 = @WBS1 and (isnull(@WBS2,'') = '' or WBS2 = @WBS2) and (isnull(@WBS3,'') = '' or WBS3 = @WBS3)
			) b on b.PostSeq = ah.PostSeq and b.PKey = ah.PKey and b.Period = ah.Period and b.TableName = ah.TableName
			left join BTLaborCats on BTLaborCats.Category=b.Category and ah.TableName in ('LD','BILD')
			left join EM Tr_EM on Tr_EM.Employee = b.Employee
		where (@TableName='' or ah.TableName=@TableName) and TransDate between @StartDate and @EndDate+1 and ah.SubmittedBy is not null
	UNION ALL
	select distinct DateAdd(minute, DateDiff(minute, getutcdate(), getdate()), ApprovedDate) as ActionDate, EM.LastName + N', ' + IsNull(EM.FirstName,N'') as EmpName, 'Approved' as ActionTaken, '' as ActionType,
			b.Amount,
			'' as Details, '' as ModificationComment, ApprovalComment as ApprovalComment, b.TransDate,
			case when ah.TableName in ('LD','BILD') then BTLaborCats.Description else b.Desc1 end as Descr,
			Tr_EM.LastName + N', ' + IsNull(Tr_EM.FirstName,N'') as [ModEmployee]
		from CCG_TR_ApprovalsHistory ah
			left join EM on EM.Employee = ah.ApprovedBy
			inner join (
				select 'LD' TableName, LD.BillExt as Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, Category, '' as Desc1 from LD			where WBS1 = @WBS1 and (isnull(@WBS2,'') = '' or WBS2 = @WBS2) and (isnull(@WBS3,'') = '' or WBS3 = @WBS3)
				union all select 'BILD' TableName, BILD.BillExt as Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, Category, '' from BILD		where WBS1 = @WBS1 and (isnull(@WBS2,'') = '' or WBS2 = @WBS2) and (isnull(@WBS3,'') = '' or WBS3 = @WBS3)
				union all select 'LedgerEX' TableName, Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, '', Desc1 from LedgerEX				where WBS1 = @WBS1 and (isnull(@WBS2,'') = '' or WBS2 = @WBS2) and (isnull(@WBS3,'') = '' or WBS3 = @WBS3)
				union all select 'LedgerAP' TableName, Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, '', Desc1 from LedgerAP				where WBS1 = @WBS1 and (isnull(@WBS2,'') = '' or WBS2 = @WBS2) and (isnull(@WBS3,'') = '' or WBS3 = @WBS3)
				union all select 'LedgerAR' TableName, Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, '', Desc1 from LedgerAR				where WBS1 = @WBS1 and (isnull(@WBS2,'') = '' or WBS2 = @WBS2) and (isnull(@WBS3,'') = '' or WBS3 = @WBS3)
				union all select 'LedgerMisc' TableName, Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, '', Desc1 from LedgerMisc			where WBS1 = @WBS1 and (isnull(@WBS2,'') = '' or WBS2 = @WBS2) and (isnull(@WBS3,'') = '' or WBS3 = @WBS3)
				union all select 'BIED' TableName, Amount, PostSeq, PKey, Period, WBS1, WBS2, WBS3, TransDate, Employee, '', Desc1 from BIED						where WBS1 = @WBS1 and (isnull(@WBS2,'') = '' or WBS2 = @WBS2) and (isnull(@WBS3,'') = '' or WBS3 = @WBS3)
			) b on b.PostSeq = ah.PostSeq and b.PKey = ah.PKey and b.Period = ah.Period and b.TableName = ah.TableName
			left join BTLaborCats on BTLaborCats.Category=b.Category and ah.TableName in ('LD','BILD')
			left join EM Tr_EM on Tr_EM.Employee = b.Employee
		where (@TableName='' or ah.TableName=@TableName) and TransDate between @StartDate and @EndDate+1 and ah.ApprovedBy is not null
	order by 8 desc
END
GO
