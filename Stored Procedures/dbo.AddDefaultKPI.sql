SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[AddDefaultKPI]
  @UICultureName AS varchar(10) = ''
AS
BEGIN

SET NOCOUNT ON

DECLARE @Edition AS INT

SELECT @Edition = CAST(SERVERPROPERTY('EngineEdition') AS INT)

SET NOCOUNT ON

--Remove default KPI description entries in case system labels have changed
DELETE FROM AnalysisCubesKPIDescriptions WHERE UICultureName = @UICultureName AND PKey LIKE 'DEFAULTKPI%'
DELETE FROM AnalysisCubesKPIData WHERE PKey LIKE 'DEFAULTKPI%'

--Only insert new default KPI keys if it doesn't exist already
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI01','Vision|||INT-Effective Multiplier|0','M','PROrg1','Gauge - Ascending','A','P','3.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI01') --DONE
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI02','Vision|||INT-Gross Margin Percent|0','P','PROrg1','Gauge - Ascending','A','P','30.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI02') --DONE
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI03','Vision|||INT-Variance Percent|0','P','PROrg1','Gauge - Ascending','A','P','30.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI03') --DONE
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI04','Vision|||INT-Realization Ratio|0','M','PROrg1','Gauge - Ascending','A','P','1.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI04') -- DONE
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI05','Vision|||INT-Effective Multiplier|0','M','PRPrincipal','Gauge - Ascending ','A','P','3.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI05')
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI06','Vision|||INT-Gross Margin Percent|0','P','PRPrincipal','Gauge - Ascending','A','P','30.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI06')
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI07','Vision|||INT-Variance Percent|0','P','PRPrincipal','Gauge - Ascending','A','P','30.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI07')
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI08','Vision|||INT-Realization Ratio|0','M','PRPrincipal','Gauge - Ascending','A','P','1.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI08')
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI09','Vision|||INT-Effective Multiplier|0','M','PRProjectManager','Gauge - Ascending','A','P','3.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI09')
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI10','Vision|||INT-Gross Margin Percent|0','P','PRProjectManager','Gauge - Ascending','A','P','30.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI10')
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI11','Vision|||INT-Variance Percent|0','P','PRProjectManager','Gauge - Ascending','A','P','30.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI11')
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI12','Vision|||INT-Realization Ratio|0','M','PRProjectManager','Gauge - Ascending','A','P','1.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI12')
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI13','Vision|Contract Credits||Employee Contract Credit Amount','A','PROrg1','Gauge - Ascending','T','P','5000000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI13')
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI14','Vision|Contract Credits||Employee Contract Credit Amount','A','EMEmployee','Gauge - Ascending','T','P','50000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI14')
   
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI18','Vision|Billed||Billed','A','PlanOrg1','Gauge - Ascending','T','P','1500000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI18') -- DONE
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI19','Vision|Billed||Billed','A','PlanPrincipal','Gauge - Ascending','T','P','200000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI19')
 
IF @Edition = 3
BEGIN
	INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
	SELECT 'DEFAULTKPI20','Vision|||INT-DSO 90|0','A','PROrg1','Gauge - Ascending','A','P','50.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
	WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI20') -- DONE
 
	INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
	SELECT 'DEFAULTKPI21','Vision|||INT-DSO 90|0','A','PRProjectManager','Gauge - Ascending','A','P','50.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
	WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI21')
 
	INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
	SELECT 'DEFAULTKPI22','Vision|||INT-DSO 90|0','A','PRPrincipal','Gauge - Ascending','A','P','50.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
	WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI22')
 
	INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
	SELECT 'DEFAULTKPI23','Vision|||INT-DWO 90|0','A','PROrg1','Gauge - Ascending','A','P','50.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
	WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI23') -- DONE
 
	INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
	SELECT 'DEFAULTKPI24','Vision|||INT-DWO 90|0','A','PRProjectManager','Gauge - Ascending','A','P','50.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
	WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI24')
 
	INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
	SELECT 'DEFAULTKPI25','Vision|||INT-DWO 90|0','A','PRPrincipal','Gauge - Ascending','A','P','50.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
	WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI25')
 
	INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
	SELECT 'DEFAULTKPI26','Vision|||INT-DSO 90|0','A',null,'Gauge - Ascending','A','P','50.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
	WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI26')
 
	INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
	SELECT 'DEFAULTKPI27','Vision|||INT-DWO 90|0','A',null,'Gauge - Ascending','A','P','50.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
	WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI27')
END

 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI29','Vision|Contract Credits||Employee Contract Credit Amount','A',null,'Gauge - Ascending','T','P','5000000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI29')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI30','Vision|||INT-Profit Percent|0','P',null,'Gauge - Ascending','A','P','3.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI30')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI31','Vision|||INT-Effective Multiplier|0','M',null,'Gauge - Ascending','A','P','3.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI31')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI32','Vision|Billed||Billed','A','PROrg1','Gauge - Ascending','T','P','1500000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI32')
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI33','Vision|Billed||Billed','A','PRPrincipal','Gauge - Ascending','T','P','200000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI33')
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI34','Vision|Billed||Billed','A',null,'Gauge - Ascending','T','P','200000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI34')

--------New KPIs
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI35','Vision|||INT-Effective Multiplier|0','M','PROrg2','Gauge - Ascending','A','P','3.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI35')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI36','Vision|||INT-Effective Multiplier|0','M','PROrg3','Gauge - Ascending','A','P','3.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI36')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI37','Vision|||INT-Effective Multiplier|0','M','PROrg4','Gauge - Ascending','A','P','3.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI37')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI38','Vision|||INT-Effective Multiplier|0','M','PROrg5','Gauge - Ascending','A','P','3.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI38')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI39','Vision|||INT-Gross Margin Percent|0','P','PROrg2','Gauge - Ascending','A','P','30.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI39')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI40','Vision|||INT-Gross Margin Percent|0','P','PROrg3','Gauge - Ascending','A','P','30.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI40')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI41','Vision|||INT-Gross Margin Percent|0','P','PROrg4','Gauge - Ascending','A','P','30.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI41')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI42','Vision|||INT-Gross Margin Percent|0','P','PROrg5','Gauge - Ascending','A','P','30.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI42')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI43','Vision|||INT-Variance Percent|0','P','PROrg2','Gauge - Ascending','A','P','30.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI43')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI44','Vision|||INT-Variance Percent|0','P','PROrg3','Gauge - Ascending','A','P','30.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI44')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI45','Vision|||INT-Variance Percent|0','P','PROrg4','Gauge - Ascending','A','P','30.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI45')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI46','Vision|||INT-Variance Percent|0','P','PROrg5','Gauge - Ascending','A','P','30.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI46')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI47','Vision|||INT-Realization Ratio|0','M','PROrg2','Gauge - Ascending','A','P','1.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI47')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI48','Vision|||INT-Realization Ratio|0','M','PROrg3','Gauge - Ascending','A','P','1.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI48')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI49','Vision|||INT-Realization Ratio|0','M','PROrg4','Gauge - Ascending','A','P','1.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI49')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI50','Vision|||INT-Realization Ratio|0','M','PROrg5','Gauge - Ascending','A','P','1.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI50')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI51','Vision|Contract Credits||Employee Contract Credit Amount','A','PROrg2','Gauge - Ascending','T','P','5000000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI51')
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI52','Vision|Contract Credits||Employee Contract Credit Amount','A','PROrg3','Gauge - Ascending','T','P','5000000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI52')
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI53','Vision|Contract Credits||Employee Contract Credit Amount','A','PROrg4','Gauge - Ascending','T','P','5000000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI53')
 
INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI54','Vision|Contract Credits||Employee Contract Credit Amount','A','PROrg5','Gauge - Ascending','T','P','5000000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI54')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI59','Vision|Billed||Billed','A','PlanOrg2','Gauge - Ascending','T','P','1500000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI59')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI60','Vision|Billed||Billed','A','PlanOrg3','Gauge - Ascending','T','P','1500000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI60')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI61','Vision|Billed||Billed','A','PlanOrg4','Gauge - Ascending','T','P','1500000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI61')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI62','Vision|Billed||Billed','A','PlanOrg5','Gauge - Ascending','T','P','1500000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI62')

IF @Edition = 3
BEGIN
	INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
	SELECT 'DEFAULTKPI63','Vision|||INT-DSO 90|0','A','PROrg2','Gauge - Ascending','A','P','50.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
	WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI63')

	INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
	SELECT 'DEFAULTKPI64','Vision|||INT-DSO 90|0','A','PROrg3','Gauge - Ascending','A','P','50.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
	WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI64')

	INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
	SELECT 'DEFAULTKPI65','Vision|||INT-DSO 90|0','A','PROrg4','Gauge - Ascending','A','P','50.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
	WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI65')

	INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
	SELECT 'DEFAULTKPI66','Vision|||INT-DSO 90|0','A','PROrg5','Gauge - Ascending','A','P','50.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
	WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI66')

	INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
	SELECT 'DEFAULTKPI67','Vision|||INT-DWO 90|0','A','PROrg2','Gauge - Ascending','A','P','50.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
	WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI67')

	INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
	SELECT 'DEFAULTKPI68','Vision|||INT-DWO 90|0','A','PROrg3','Gauge - Ascending','A','P','50.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
	WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI68')

	INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
	SELECT 'DEFAULTKPI69','Vision|||INT-DWO 90|0','A','PROrg4','Gauge - Ascending','A','P','50.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
	WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI69')

	INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
	SELECT 'DEFAULTKPI70','Vision|||INT-DWO 90|0','A','PROrg5','Gauge - Ascending','A','P','50.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
	WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI70')
END

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI71','Vision|Billed||Billed','A','PROrg2','Gauge - Ascending','T','P','1500000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI71')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI72','Vision|Billed||Billed','A','PROrg3','Gauge - Ascending','T','P','1500000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI72')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI73','Vision|Billed||Billed','A','PROrg4','Gauge - Ascending','T','P','1500000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI73')

INSERT INTO AnalysisCubesKPIData (PKey, Measure, MeasureType, Dimension, StatusIndicator, PeriodMethod, Method, Goal, GoalLowValue, GoalHighValue, TrendIndicator, TrendComparison, TrendLowValue, TrendHighValue)
SELECT 'DEFAULTKPI74','Vision|Billed||Billed','A','PROrg5','Gauge - Ascending','T','P','1500000.0000','90.0000','120.0000','Standard Arrow','YTY','0.0000','0.0000'
WHERE NOT EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI74')

--Insert KPI descriptions with current system labels
INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI01', @UICultureName, REPLACE('EffectiveMultiplierTargetOrg1~', 'Org1~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org1Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org1Label')),'Org1~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI01') --DONE

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI02', @UICultureName, REPLACE('ProfitPercentexlcudingOHTargetOrg1~', 'Org1~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org1Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org1Label')),'Org1~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI02') --DONE

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI03', @UICultureName, REPLACE('VariancePercentTargetOrg1~', 'Org1~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org1Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org1Label')),'Org1~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI03')  --DONE

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI04', @UICultureName, REPLACE('RealizationRatioTargetOrg1~', 'Org1~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org1Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org1Label')),'Org1~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI04') --DONE

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI05', @UICultureName, REPLACE('EffectiveMultiplierTargetPrincipal~', 'Principal~', (SELECT LabelValue FROM FW_CFGLabelData WHERE FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('sysPR')))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI05')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI06', @UICultureName, REPLACE('ProfitPercentexlcudingOHTargetPrincipal~', 'Principal~', (SELECT LabelValue FROM FW_CFGLabelData WHERE FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('sysPR')))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI06')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI07', @UICultureName, REPLACE('VariancePercentTargetPrincipal~', 'Principal~', (SELECT LabelValue FROM FW_CFGLabelData WHERE FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('sysPR')))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI07')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI08', @UICultureName, REPLACE('RealizationRatioTargetPrincipal~', 'Principal~', (SELECT LabelValue FROM FW_CFGLabelData WHERE FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('sysPR')))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI08')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI09', @UICultureName, REPLACE('EffectiveMultiplierTargetProject Manager~', 'Project Manager~', (SELECT LabelValue FROM FW_CFGLabelData WHERE FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('sysPM')))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI09')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI10', @UICultureName, REPLACE('ProfitPercentexlcudingOHTargetProject Manager~', 'Project Manager~', (SELECT LabelValue FROM FW_CFGLabelData WHERE FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('sysPM')))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI10')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI11', @UICultureName, REPLACE('VariancePercentTargetProject Manager~', 'Project Manager~', (SELECT LabelValue FROM FW_CFGLabelData WHERE FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('sysPM')))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI11')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI12', @UICultureName, REPLACE('RealizationRatioTargetProject Manager~', 'Project Manager~', (SELECT LabelValue FROM FW_CFGLabelData WHERE FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('sysPM')))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI12')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI13', @UICultureName, REPLACE('ContractSalesTargetOrg1~', 'Org1~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org1Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org1Label')),'Org1~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI13') --DONE

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI14', @UICultureName, REPLACE('ContractSalesTargetEmployee~', 'Employee~', (SELECT LabelValue FROM FW_CFGLabelData WHERE FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('employeeLabel')))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI14')

/* Updated: 26 March 2013 by JP de Guzman*/
/* Relabeled DEFAULTKPI18 and DEFAULTKPI19 from RevenueTargetOrg1~ and RevenueTargetPrincipal~ to RevenueTargetPlanOrg1~ and RevenueTargetPlanPrincipal~ respectively */
/* DEFAULTKPI18 and DEFAULTKPI19 are using Plan dimension while DEFAULTKPI32 and DEFAULTKPI33 are using Project. */

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI18', @UICultureName, REPLACE('RevenueTargetPlanOrg1~', 'Org1~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org1Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org1Label')),'Org1~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI18') --DONE

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI19', @UICultureName, REPLACE('RevenueTargetPlanPrincipal~', 'Principal~', (SELECT LabelValue FROM FW_CFGLabelData WHERE FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('sysPR')))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI19')

IF @Edition = 3
BEGIN
	INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
	SELECT 'DEFAULTKPI20', @UICultureName, REPLACE('DSOTargetOrg1~', 'Org1~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org1Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org1Label')),'Org1~'))
	WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI20') --DONE

	INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
	SELECT 'DEFAULTKPI21', @UICultureName, REPLACE('DSOTargetProject Manager~', 'Project Manager~', (SELECT LabelValue FROM FW_CFGLabelData WHERE FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('sysPM')))
	WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI21')

	INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
	SELECT 'DEFAULTKPI22', @UICultureName, REPLACE('DSOTargetPrincipal~', 'Principal~', (SELECT LabelValue FROM FW_CFGLabelData WHERE FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('sysPR')))
	WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI22')

	INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
	SELECT 'DEFAULTKPI23', @UICultureName, REPLACE('DWOTargetOrg1~', 'Org1~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org1Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org1Label')),'Org1~'))
	WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI23') --DONE

	INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
	SELECT 'DEFAULTKPI24', @UICultureName, REPLACE('DWOTargetProject Manager~', 'Project Manager~', (SELECT LabelValue FROM FW_CFGLabelData WHERE FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('sysPM')))
	WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI24')

	INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
	SELECT 'DEFAULTKPI25', @UICultureName, REPLACE('DWOTargetPrincipal~', 'Principal~', (SELECT LabelValue FROM FW_CFGLabelData WHERE FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('sysPR')))
	WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI25')

	INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
	SELECT 'DEFAULTKPI26', @UICultureName, 'DSOTargetNoDimension'
	WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI26')

	INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
	SELECT 'DEFAULTKPI27', @UICultureName, 'DWOTargetNoDimension'
	WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI27')
END


INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI29', @UICultureName, 'ContractSalesTargetNoDimension'
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI29')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI30', @UICultureName, 'ProfitPercentNoDimension'
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI30')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI31', @UICultureName, 'EffectiveMultiplierNoDimension'
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI31')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI32', @UICultureName, REPLACE('RevenueTargetProjectOrg1~', 'Org1~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org1Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org1Label')),'Org1~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI32') --DONE

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI33', @UICultureName, REPLACE('RevenueTargetProjectPrincipal~', 'Principal~', (SELECT LabelValue FROM FW_CFGLabelData WHERE FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('sysPR')))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI33')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI34', @UICultureName, 'RevenueTargetNoDimension'
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI34')


--NEW KPI Descriptions
INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI35', @UICultureName, REPLACE('EffectiveMultiplierTargetOrg2~', 'Org2~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org2Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org2Label')),'Org2~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI35')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI36', @UICultureName, REPLACE('EffectiveMultiplierTargetOrg3~', 'Org3~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org3Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org3Label')),'Org3~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI36')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI37', @UICultureName, REPLACE('EffectiveMultiplierTargetOrg4~', 'Org4~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org4Label')),'Org4~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI37')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI38', @UICultureName, REPLACE('EffectiveMultiplierTargetOrg5~', 'Org5~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org5Label')),'Org5~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI38')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI39', @UICultureName, REPLACE('ProfitPercentexlcudingOHTargetOrg2~', 'Org2~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org2Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org2Label')),'Org2~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI39')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI40', @UICultureName, REPLACE('ProfitPercentexlcudingOHTargetOrg3~', 'Org3~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org3Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org3Label')),'Org3~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI40')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI41', @UICultureName, REPLACE('ProfitPercentexlcudingOHTargetOrg4~', 'Org4~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org4Label')),'Org4~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI41')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI42', @UICultureName, REPLACE('ProfitPercentexlcudingOHTargetOrg5~', 'Org5~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org5Label')),'Org5~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI42')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI43', @UICultureName, REPLACE('VariancePercentTargetOrg2~', 'Org2~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org2Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org2Label')),'Org2~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI43') 

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI44', @UICultureName, REPLACE('VariancePercentTargetOrg3~', 'Org3~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org3Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org3Label')),'Org3~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI44') 

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI45', @UICultureName, REPLACE('VariancePercentTargetOrg4~', 'Org4~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org4Label')),'Org4~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI45') 

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI46', @UICultureName, REPLACE('VariancePercentTargetOrg5~', 'Org5~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org5Label')),'Org5~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI46') 

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI47', @UICultureName, REPLACE('RealizationRatioTargetOrg2~', 'Org2~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org2Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org2Label')),'Org2~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI47')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI48', @UICultureName, REPLACE('RealizationRatioTargetOrg3~', 'Org3~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org3Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org3Label')),'Org3~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI47')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI49', @UICultureName, REPLACE('RealizationRatioTargetOrg4~', 'Org4~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org4Label')),'Org4~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI47')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI50', @UICultureName, REPLACE('RealizationRatioTargetOrg5~', 'Org5~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org5Label')),'Org5~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI50')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI51', @UICultureName, REPLACE('ContractSalesTargetOrg2~', 'Org2~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org2Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org2Label')),'Org2~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI51')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI52', @UICultureName, REPLACE('ContractSalesTargetOrg3~', 'Org3~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org3Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org3Label')),'Org3~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI52')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI53', @UICultureName, REPLACE('ContractSalesTargetOrg4~', 'Org4~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org4Label')),'Org4~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI53')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI54', @UICultureName, REPLACE('ContractSalesTargetOrg5~', 'Org5~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org5Label')),'Org5~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI54')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI59', @UICultureName, REPLACE('RevenueTargetPlanOrg2~', 'Org2~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org2Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org2Label')),'Org2~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI59')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI60', @UICultureName, REPLACE('RevenueTargetPlanOrg3~', 'Org3~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org3Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org3Label')),'Org3~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI60')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI61', @UICultureName, REPLACE('RevenueTargetPlanOrg4~', 'Org4~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org4Label')),'Org4~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI61')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI62', @UICultureName, REPLACE('RevenueTargetPlanOrg5~', 'Org5~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org5Label')),'Org5~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI62')

IF @Edition = 3
BEGIN
	INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
	SELECT 'DEFAULTKPI63', @UICultureName, REPLACE('DSOTargetOrg2~', 'Org2~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org2Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org2Label')),'Org2~'))
	WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI63')

	INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
	SELECT 'DEFAULTKPI64', @UICultureName, REPLACE('DSOTargetOrg3~', 'Org3~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org3Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org3Label')),'Org3~'))
	WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI64')

	INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
	SELECT 'DEFAULTKPI65', @UICultureName, REPLACE('DSOTargetOrg4~', 'Org4~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org4Label')),'Org4~'))
	WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI65')

	INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
	SELECT 'DEFAULTKPI66', @UICultureName, REPLACE('DSOTargetOrg5~', 'Org5~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org5Label')),'Org5~'))
	WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI66')

	INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
	SELECT 'DEFAULTKPI67', @UICultureName, REPLACE('DWOTargetOrg2~', 'Org2~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org2Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org2Label')),'Org2~'))
	WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI67')

	INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
	SELECT 'DEFAULTKPI68', @UICultureName, REPLACE('DWOTargetOrg3~', 'Org3~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org3Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org3Label')),'Org3~'))
	WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI68')

	INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
	SELECT 'DEFAULTKPI69', @UICultureName, REPLACE('DWOTargetOrg4~', 'Org4~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org4Label')),'Org4~'))
	WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI69')

	INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
	SELECT 'DEFAULTKPI70', @UICultureName, REPLACE('DWOTargetOrg5~', 'Org5~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org5Label')),'Org5~'))
	WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI70')

END

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI71', @UICultureName, REPLACE('RevenueTargetProjectOrg2~', 'Org2~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org2Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org2Label')),'Org2~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI71')


INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI72', @UICultureName, REPLACE('RevenueTargetProjectOrg3~', 'Org3~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org3Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org3Label')),'Org3~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI72')


INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI73', @UICultureName, REPLACE('RevenueTargetProjectOrg4~', 'Org4~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org4Label')),'Org4~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI73')

INSERT INTO AnalysisCubesKPIDescriptions (PKey, UICultureName, KPILabel) 
SELECT 'DEFAULTKPI74', @UICultureName, REPLACE('RevenueTargetProjectOrg5~', 'Org5~', ISNULL((SELECT LabelValue FROM FW_CFGLabelData, CFGFormat WHERE CFGFormat.Org4Length <> 0 AND FW_CFGLabelData.UICultureName = @UICultureName AND LabelName IN ('org5Label')),'Org5~'))
WHERE EXISTS (SELECT 'x' FROM AnalysisCubesKPIData WHERE PKey = 'DEFAULTKPI74')

SET NOCOUNT OFF

END
GO
