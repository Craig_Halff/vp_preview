SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_LoadFeeDataCategories] ( @filterBillGrp bit, @filterSQL bit, @isRoleAcct bit, @supportBuiltInFeePctCpl varchar(10), @isSingleProject bit, @invoiceGroup nvarchar(255), @User nvarchar(32), @wbs1List nvarchar(max), @includeDormant varchar(5), @isSingleWBS2 bit, @wbs2 nvarchar(7), @wbs3 nvarchar(7), @seq int, @VISION_LANGUAGE varchar(10)= null)
             AS EXEC spCCG_EI_LoadFeeDataCategories @filterBillGrp,@filterSQL,@isRoleAcct,@supportBuiltInFeePctCpl,@isSingleProject,@invoiceGroup,@User,@wbs1List,@includeDormant,@isSingleWBS2,@wbs2,@wbs3,@seq,@VISION_LANGUAGE
GO
