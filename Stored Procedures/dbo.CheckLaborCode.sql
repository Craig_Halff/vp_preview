SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CheckLaborCode]
@LaborCode Nvarchar(5)
as
begin
declare
   @i As Integer,
   @LCStart1  Integer,
   @LCStart2  Integer,
   @LCStart3  Integer,
   @LCStart4  Integer,
   @LCStart5  Integer,
   @LCLength1  Integer,
   @LCLength2  Integer,
   @LCLength3  Integer,
   @LCLength4  Integer,
   @LCLength5 Integer,  
   @LCLevels Integer
   if LTrim(RTrim(@LaborCode)) = ''
      Return -1
   else
    select
      @LCLength1 = LC1Length,
      @LCLength2 = LC2Length,
      @LCLength3 = LC3Length,
      @LCLength4 = LC4Length,
      @LCLength5 = LC5Length,
      @LCStart1 = LC1Start,
      @LCStart2 = LC2Start,
      @LCStart3 = LC3Start,
      @LCStart4 = LC4Start,
      @LCStart5 = LC5Start,
      @LCLevels = LCLevels
    from CFGFormat

    set @i = 1

    if not(@i > @LCLevels)
    begin
      set @i = @i + 1
      if not exists (select 'x' from CFGLCCodes where LCLevel = 1 And code = substring(@LaborCode,@LCStart1,@LClength1))
         Return -1
    end
    
    if not(@i > @LCLevels)
    begin
      set @i = @i + 1
      if not exists (select 'x' from CFGLCCodes where LCLevel = 2 And code = substring(@LaborCode,@LCStart2,@LClength2))
         Return -1
    end
    
    if not (@i > @LCLevels)
    begin
      set @i = @i + 1
      if not exists (select 'x' from CFGLCCodes where LCLevel = 3 And code = substring(@LaborCode,@LCStart3,@LClength3))
         Return -1
    end
    
    if not (@i > @LCLevels)
    begin
      set @i = @i + 1
      if not exists (select 'x' from CFGLCCodes where LCLevel = 4 And code = substring(@LaborCode,@LCStart4,@LClength4))
         Return -1
    end
    
    if not (@i > @LCLevels)
    begin
      set @i = @i + 1
      if not exists (select 'x' from CFGLCCodes where LCLevel = 5 And code = substring(@LaborCode,@LCStart5,@LClength5))
         Return -1
    end            
    Return 0
end
GO
