SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[setAuditingDetail]
	@strAuditingDetail varchar(1) = 'N'
AS

BEGIN -- Procedure setContextInfo
  DECLARE @strCompany Nvarchar(14)  
  DECLARE @strAuditingEnabled varchar(1)  
  DECLARE @strUserName Nvarchar(32)  
  DECLARE @strCultureName varchar(10) 
  DECLARE @strSource Nvarchar(3)

	SET @strCompany  = dbo.GetActiveCompany()
	SET @strAuditingEnabled   = 'Y'
	SET @strUserName   = dbo.GetVisionAuditUsername()
	SET @strCultureName   = dbo.FW_GetActiveCultureName()
	SET @strSource = dbo.GetVisionAuditSource()


  Execute dbo.setContextInfo @strCompany, @strAuditingEnabled, @strUserName,@strCultureName, @strAuditingDetail,@strSource
 

END -- setAuditingDetail
GO
