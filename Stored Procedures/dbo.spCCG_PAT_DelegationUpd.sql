SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_DelegationUpd] (
	@Id			varchar(32),
	@Username	Nvarchar(32),
	@Employee	Nvarchar(20),
	@Delegate	Nvarchar(20),
	@FromDate	datetime,
	@ToDate		datetime,
	@MaxAmount	money,
	@Dual		char(1),
	@ApprovedBy Nvarchar(20),
	@ApprovedOn datetime,
	@Permanent	char(1))			-- NOT USED LIKE THE OTHER PARAMETERS
AS BEGIN
/*
	Copyright (c) 2020 EleVia Software. All rights reserved.

	exec spCCG_PAT_DelegationIns '1','ADMIN','00001','00003','3/1/2017','3/8/2017',0,'N',null,null,'N'
	select * from CCG_PAT_Delegation
	select * from CCG_PAT_HistoryDelegation
	exec spCCG_PAT_DelegationUpd '1','ADMIN','00001','00003','3/1/2017','3/15/2017',0,'N',null,null,'N'
	select * from CCG_PAT_Delegation
	select * from CCG_PAT_HistoryDelegation
	exec spCCG_PAT_DelegationUpd '1','ADMIN','00001','00003','3/1/2017','3/17/2017',0,'N','00001','3/15/2017','N'
	select * from CCG_PAT_Delegation
	select * from CCG_PAT_HistoryDelegation
*/
	SET NOCOUNT ON
	BEGIN TRY
		DECLARE @EmployeeOrig Nvarchar(20), @DelegateOrig Nvarchar(20), @FromDateOrig datetime, @ToDateOrig datetime
		DECLARE @MaxAmountOrig money, @DualOrig char(1), @ApprovedByOrig Nvarchar(20), @ApprovedOnOrig datetime, @PermanentOrig char(1)

		select @EmployeeOrig=d.Employee, @DelegateOrig=d.Delegate, @FromDateOrig=FromDate, @ToDateOrig=ToDate,
				@MaxAmountOrig=MaxAmount, @DualOrig=Dual, @ApprovedByOrig=d.ApprovedBy, @ApprovedOnOrig=d.ApprovedOn,
				@PermanentOrig='N' --(CASE WHEN pa.Seq IS NULL THEN 'N' ELSE 'Y' END)
			from CCG_PAT_Delegation d
				-- LEFT JOIN CCG_PAT_DelegationPermanentApprovals pa ON pa.Employee = d.Employee AND pa.Delegate = d.Delegate
			where d.Id = @Id

		update CCG_PAT_Delegation
			set Employee=@Employee,Delegate=@Delegate,FromDate=@FromDate,ToDate=@ToDate,MaxAmount=NULLIF(@MaxAmount,0),Dual=@Dual,
				ApprovedBy=@ApprovedBy,ApprovedOn=NULLIF(@ApprovedOn, '')
			where Id=@Id

		/* If any field other than approval info changed, add an Updated record */
		if @Employee<>@EmployeeOrig or @Delegate<>@DelegateOrig or @FromDate<>@FromDateOrig or @ToDate<>@ToDateOrig or @MaxAmount<>@MaxAmountOrig or @Dual<>@DualOrig or @Permanent<>@PermanentOrig
			insert into CCG_PAT_HistoryDelegation (Id,Employee,Delegate,FromDate,ToDate,ActionTaken,ActionDate,ActionTakenBy,ActionRecipient,Description)
				select @Id,@Employee,@Delegate,@FromDate,@ToDate,N'Updated',getutcdate(),Employee,null,N'Max Amt = ' + Cast(@MaxAmount as Nvarchar) + ', Dual = ' + @Dual + ', Permanent = ' + @Permanent
				from SEUser where Username=@Username

		if IsNull(@ApprovedBy,'')<>'' and @ApprovedOn is not null and (IsNull(@ApprovedBy,'')<>IsNull(@ApprovedByOrig,'') or @ApprovedOn<>@ApprovedOnOrig)
		BEGIN
			insert into CCG_PAT_HistoryDelegation (Id,Employee,Delegate,FromDate,ToDate,ActionTaken,ActionDate,ActionTakenBy,ActionRecipient,Description)
				select @Id,@Employee,@Delegate,@FromDate,@ToDate,N'Approved',getutcdate(),Employee,null,N'Approver = ' + @ApprovedBy + ', Approved On = ' + Convert(Nvarchar, @ApprovedOn, 101)
				from SEUser where Username=@Username
		END
		/*
		IF @Permanent = 'Y' AND ISNULL(@PermanentOrig,'') <> 'Y'
			AND NOT EXISTS(SELECT 'x' FROM CCG_PAT_DelegationPermanentApprovals WHERE Employee = @Employee AND Delegate = @Delegate)
			INSERT INTO CCG_PAT_DelegationPermanentApprovals (Employee, Delegate, ApprovedBy, ApprovedOn)
				VALUES (@Employee, @Delegate, @ApprovedBy, NULLIF(@ApprovedOn,''))
		*/

		select 0 as Result, '' as ErrorMessage
	END TRY
	BEGIN CATCH
		select -1 as Result, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage
	END CATCH
END
GO
