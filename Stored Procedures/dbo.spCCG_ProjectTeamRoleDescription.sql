SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectTeamRoleDescription] @WBS1 varchar(32), @DescriptionCategory varchar (255)
AS
/* 
Copyright (c) 2019 Central Consulting Group.  All rights reserved.
11/19/2019	David Springer
			Set Role Description = project description (resume description).
			Call from Project scheduled workflow after timesheets are posted.
			Call from a Project Description INSERT workflow when DescCategory = Resume Description.
*/
BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	Update e
	Set e.RoleDescription = d.Description
	From EMProjectAssoc e, PRDescriptions d WITH (NOLOCK)
	Where e.WBS1 = @WBS1
	  and e.WBS1 = d.WBS1
	  and e.WBS2 = d.WBS2
	  and e.WBS2 = ' '  -- project level only
	  and e.RoleDescription is null
	  and d.DescCategory = @DescriptionCategory
	  and d.Description is not null

END

GO
