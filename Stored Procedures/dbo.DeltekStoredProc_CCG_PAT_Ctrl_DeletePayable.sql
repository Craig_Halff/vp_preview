SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Ctrl_DeletePayable]
	@seq		int,
	@EMPLOYEEID	Nvarchar(32),
	@itemDescr	Nvarchar(max),
	@ModDate	varchar(30)
AS
BEGIN
	SET NOCOUNT ON;

	IF exists(SELECT seq FROM CCG_PAT_Payable WHERE Seq = @seq and ModDate - .00002 > @ModDate)
        RAISERROR ('Stale data detected, please refresh and try again.', 16, 1);

	BEGIN TRY
		BEGIN TRANSACTION;
		DELETE FROM CCG_PAT_Pending WHERE payableseq = @seq;
		DELETE FROM CCG_PAT_ProjectAmount WHERE payableseq = @seq;
		UPDATE CCG_PAT_Payable SET ParentSeq = null WHERE ParentSeq = @seq;
		UPDATE CCG_PAT_Files set FileState = 'DELETED' where PayableSeq = @seq;
		DELETE FROM CCG_PAT_Payable where seq = @seq;

		INSERT INTO CCG_PAT_History(PayableSeq, ActionDate, ActionTaken, ActionTakenBy, [Description])
			SELECT @seq, getutcdate(), N'Payable Deleted', @EMPLOYEEID, LEFT(@itemDescr, 255)
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION;

		RAISERROR ('Transaction Failed.', 16, 1);
	END CATCH
END
GO
