SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyConvertWBS]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@UserName Nvarchar(32) = 'VisionKeyConvert', -- Add by DP 4.0
@Period integer = 0,
@EntityType Nvarchar(20)
as
begin -- Procedure
	set nocount on
	declare @OldWBS1 			Nvarchar(30)
	declare @OldWBS2 			Nvarchar(30)
	declare @OldWBS3 			Nvarchar(30)
	declare @NewWBS1 			Nvarchar(30)
	declare @NewWBS2 			Nvarchar(30)
	declare @NewWBS3 			Nvarchar(30)
	declare @name				Nvarchar(40)
	Declare @Existing 		integer
	declare @Entity 			Nvarchar(20)
	declare @length 			integer
	declare @CvtType 			integer
	declare @KeysFetch 		integer
	declare @RetVal 			integer
	declare @Errmsg2 			Nvarchar(1000),
			@custlabel			Nvarchar(40),
			@CustLabelProj 	Nvarchar(40),
			@custLabelProjPlural		Nvarchar(40),
			@CustLabelPhase 		Nvarchar(40),
			@custLabelPhasePlural	Nvarchar(40),
			@Custlabelplural 	Nvarchar(40)
	declare @NewChargeType 	Nvarchar(1)
	declare @OldChargeType 	Nvarchar(1)
	declare @newSubLevel 	Nvarchar(1)
	declare @oldSubLevel 	Nvarchar(1)
	declare @badProject		Nvarchar(1)
	declare 	@ErrorNum		integer
	declare @diag integer
	declare @message nvarchar(max)
	set @ErrorNum = 0
	set @diag = 0
	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'WBS'
	set @length = 30
	set @Existing = 0
	set @KeysFetch = 0

	execute InsertKeyCvtDriver 'WBS3'

	delete from keyconvertdriver --Remove the following tables from the enable phase/convert
	where entity = N'wbs3' 
	and tableName in (N'billAddDetail',
							N'billARDetail',
							N'billBTDDetail',
							N'billIntDetail',
							N'billInvMaster',
							N'billLimDetail',
							N'billRetDetail',
							N'billTaxDetail',
							N'BTA',
							N'BT',
							N'ICBillInvMaster',
							N'ICBillTaxDetail',
							N'ContractDetails')


	delete from keyconvertdriver -- remove these table/columns from the enable phase/convert
	where entity = N'wbs3' 
	and tableName + ',' + columnName in 
							(N'BTF,WBS2',
							N'BTF,WBS3',
							N'BTSchedule,WBS2',
							N'BTSchedule,WBS3',
							N'BTTaxCodes,WBS2',
							N'BTTaxCodes,WBS3',
							N'BTFCategory,WBS2',
							N'BTFCategory,WBS3',
							N'BTFPhaseGroupData,WBS2',
							N'BTFPhaseGroupData,WBS3',
							N'BTFPhaseGroupDescriptions,WBS2',
							N'BTFPhaseGroupDescriptions,WBS3',
							N'billConDetail,BillWBS2',
							N'billConDetail,BillWBS3',
							N'billExpDetail,BillWBS2',
							N'billExpDetail,BillWBS3',
							N'billFeeDetail,BillWBS2',
							N'billFeeDetail,BillWBS3',
							N'billFeeDetail,WBS2',
							N'billFeeDetail,WBS3',
							N'billLabDetail,BillWBS2',
							N'billLabDetail,BillWBS3',
							N'billUnitDetail,BillWBS2',
							N'billUnitDetail,BillWBS3',
							N'billInvSums,BillWBS2',
							N'billInvSums,BillWBS3',
							N'ICBillLabDetail,BillWBS2',
							N'ICBillLabDetail,BillWBS3',
							N'ICBillExpDetail,BillWBS2',
							N'ICBillExpDetail,BillWBS3',
							N'ICBillInvSums,BillWBS2',
							N'ICBillInvSums,BillWBS3',
							N'RPTask,WBS2',
							N'RPTask,WBS3',
							N'PNTask,WBS2',
							N'PNTask,WBS3')

-- Add by DP 4.0
	delete from keyconvertDriver where entity = N'wbs3' and TableName = N'PR' and ColumnName = N'WBS3'
	DECLARE @OldName Nvarchar(40)
	DECLARE @NewName Nvarchar(40)
	DECLARE @OldName2 Nvarchar(40)
	DECLARE @NewName2 Nvarchar(40)
	DECLARE @OldName3 Nvarchar(40)
	DECLARE @NewName3 Nvarchar(40)
	DECLARE @PKey Nvarchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--

--@Entity -- populate driver table
	select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'wbs3Label'
	select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'wbs3LabelPlural'
	select @custLabelPhase = LabelValue from FW_CFGLabels where LabelName = 'wbs2Label'
	select @custLabelPhasePlural = LabelValue from FW_CFGLabels where LabelName = 'wbs2LabelPlural'
	select @custLabelProj = LabelValue from FW_CFGLabels where LabelName = 'wbs1Label'
	select @custLabelProjPlural = LabelValue from FW_CFGLabels where LabelName = 'wbs1LabelPlural'

	declare KeysCursor cursor for
		select OldKey,
			isNull(OldKey2, ' '),
			isNull(OldKey3,' '),
			isNull(NewKey,OldKey),
			NewKey2,
			isNull(NewKey3,' '),WBSName, Pkey from KeyConvertWork where Entity = @Entity
	open KeysCursor
	fetch next from KeysCursor into 
		@OldWBS1,
		@OldWBS2,
		@OldWBS3,
		@NewWBS1,
		@NewWBS2,
		@NewWBS3,@Name, 
		@PKey

	set @KeysFetch = @@Fetch_Status

While (@KeysFetch = 0)
	begin
		begin tran
-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @Entity)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),'KC','Y',  'Enable ' + @CustLabelPhasePlural + '/' + @Custlabelplural + ' ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, left('Enable ' + @CustLabelPhasePlural + '/' + @Custlabelplural, 40), @CustLabelProj + '/' + @CustLabelPhase + '/' + @Custlabel);
			end

			Set @PostControlFlag = 1
		end
--
	
		if ((select sublevel from pr where WBS1 = @OldWBS1 and WBS2=@OldWBS2 and WBS3 = N' ') = 'Y')
		begin
			SET @message = dbo.GetMessage('MsgNumberHasConvNotContinue',@custlabelProj,@OldWBS1,@custLabelPhasePlural,'','','','','','')
			if (@OldWBS2 = ' ')
			RAISERROR(@message,16,3)
			else
			SET @message = dbo.GetMessage('MsgAlreadyHasConversionWillNotContinue',@custlabelProj,@OldWBS1,@custLabelPhase,@OldWBS2,@CustLabelPlural,'','','','')
			RAISERROR(@message,16,3)
			close keysCursor
			deallocate keysCursor
			if (@@Trancount > 0)
			   ROLLBACK TRANSACTION
			return(50007) -- user defined error
		end


-- Add by DP 4.0
		select * into #TempKeyConvert from PR where WBS1 = @OldWBS1 and WBS2=@OldWBS2 and WBS3 = @OldWBS3
		Update #TempKeyConvert set WBS1=@NewWBS1,  WBS2=@NewWBS2,  WBS3=@NewWBS3
		Insert PR select * from #TempKeyConvert
		Drop Table #TempKeyConvert
		select @OldName = isnull(Name, '') from PR where WBS1 = @OldWBS1 and WBS2=N' ' and WBS3 = N' '
		select @OldName2 = isnull(Name, '') from PR where WBS1 = @OldWBS1 and WBS2=@OldWBS2 and WBS3 = N' '
		select @OldName3 = isnull(Name, '') from PR where WBS1 = @OldWBS1 and WBS2=@OldWBS2 and WBS3 = @OldWBS3
		set @NewName = @OldName
		set @NewName2 = @OldName2
		set @NewName3 = @OldName3
		DECLARE @newTaskID Nvarchar(32)
		Set @newTaskID = REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')

		if (@NewWBS3 > '')
		begin
			select * into #TempRPTask from RPTask where WBS1 = @OldWBS1 and WBS2=@OldWBS2 and WBSType='WBS2'
			-- Move the non-WBS2 (Labor Code) tasks to lower level
			Update RPTask Set WBS1=@NewWBS1,  WBS2=@NewWBS2, WBS3=@NewWBS3, OutlineLevel = OutlineLevel+1, OutlineNumber = ParentOutlineNumber+'.001'+right(OutlineNumber,4), ParentOutlineNumber=ParentOutlineNumber+'.001' 
			where WBS1 = @OldWBS1 and WBS2=@OldWBS2 and WBSType<>'WBS2'
			Update RPTask Set TaskID=@newTaskID where WBS1 = @OldWBS1 and WBS2=@OldWBS2 and WBSType='WBS2'
			Update #TempRPTask set WBS1=@NewWBS1,  WBS2=@NewWBS2, WBS3=@NewWBS3, OutlineLevel = OutlineLevel+1, OutlineNumber = OutlineNumber+'.001', ParentOutlineNumber=OutlineNumber, WBSType='WBS3'
			Insert RPTask select * from #TempRPTask
			Drop Table #TempRPTask

			select * into #TempPNTask from PNTask where WBS1 = @OldWBS1 and WBS2=@OldWBS2 and WBSType='WBS2'
			-- Move the non-WBS2 (Labor Code) tasks to lower level
			Update PNTask Set WBS1=@NewWBS1,  WBS2=@NewWBS2, WBS3=@NewWBS3, OutlineLevel = OutlineLevel+1, OutlineNumber = ParentOutlineNumber+'.001'+right(OutlineNumber,4), ParentOutlineNumber=ParentOutlineNumber+'.001' 
			where WBS1 = @OldWBS1 and WBS2=@OldWBS2 and WBSType<>'WBS2'
			Update PNTask Set TaskID=@newTaskID where WBS1 = @OldWBS1 and WBS2=@OldWBS2 and WBSType='WBS2'
			Update #TempPNTask set WBS1=@NewWBS1,  WBS2=@NewWBS2, WBS3=@NewWBS3, OutlineLevel = OutlineLevel+1, OutlineNumber = OutlineNumber+'.001', ParentOutlineNumber=OutlineNumber, WBSType='WBS3'
			Insert PNTask select * from #TempPNTask
			Drop Table #TempPNTask
		end
		else
		begin
			select * into #TempRPTask2 from RPTask where WBS1 = @OldWBS1 and WBSType='WBS1'
			-- Move the non-WBS1 (Labor Code) tasks to lower level
			Update RPTask Set WBS1=@NewWBS1,  WBS2=@NewWBS2, OutlineLevel = OutlineLevel+1, OutlineNumber = ParentOutlineNumber+'.001'+right(OutlineNumber,4), ParentOutlineNumber=ParentOutlineNumber+'.001' 
			where WBS1 = @OldWBS1 and WBSType<>'WBS1'
			Update RPTask Set TaskID=@newTaskID where WBS1 = @OldWBS1 and WBSType='WBS1'
			Update #TempRPTask2 set WBS1=@NewWBS1,  WBS2=@NewWBS2, OutlineLevel = OutlineLevel+1, OutlineNumber = OutlineNumber+'.001', ParentOutlineNumber=OutlineNumber, WBSType='WBS2'
			Insert RPTask select * from #TempRPTask2
			Drop Table #TempRPTask2

			select * into #TempPNTask2 from PNTask where WBS1 = @OldWBS1 and WBSType='WBS1'
			-- Move the non-WBS1 (Labor Code) tasks to lower level
			Update PNTask Set WBS1=@NewWBS1,  WBS2=@NewWBS2, OutlineLevel = OutlineLevel+1, OutlineNumber = ParentOutlineNumber+'.001'+right(OutlineNumber,4), ParentOutlineNumber=ParentOutlineNumber+'.001' 
			where WBS1 = @OldWBS1 and WBSType<>'WBS1'
			Update PNTask Set TaskID=@newTaskID where WBS1 = @OldWBS1 and WBSType='WBS1'
			Update #TempPNTask2 set WBS1=@NewWBS1,  WBS2=@NewWBS2, OutlineLevel = OutlineLevel+1, OutlineNumber = OutlineNumber+'.001', ParentOutlineNumber=OutlineNumber, WBSType='WBS2'
			Insert PNTask select * from #TempPNTask2
			Drop Table #TempPNTask2
		end


/* Removed by DP 4.0
		exec @RetVal = KeyCvt @Entity = 'WBS3',
									@CvtType = @CvtType,
									@Length = 20,
									@ErrMsg = @ErrMsg2 output,
									@Diag = @Diag,
									@ConstrFlag = 1 -- Drop Constraints		
			alter table billAddDetail nocheck constraint all
			alter table billARDetail nocheck constraint all
			alter table billBTDDetail nocheck constraint all
			alter table billIntDetail nocheck constraint all
			alter table billInvMaster nocheck constraint all
			alter table billLimDetail nocheck constraint all
			alter table billRetDetail nocheck constraint all
			alter table billTaxDetail nocheck constraint all
			alter table BTA nocheck constraint all
			alter table BT nocheck constraint all
			alter table ICBillInvMaster nocheck constraint all
			alter table ICBillTaxDetail nocheck constraint all
			alter table CFGICBillingTerms nocheck constraint all
*/

		exec @RetVal = KeyCvt @Entity = 'WBS3',
									 @CvtType = @CvtType,
									 @Length = 20,
									 @ErrMsg = @ErrMsg2 output,
									 @Diag = @Diag,
									 @NewValue = @NewWBS1,
									 @NewValue2 = @NewWBS2,
									 @NewValue3 = @NewWBS3,
									 @OldValue = @OldWBS1,
									 @OldValue2 = @OldWBS2,  --Equivilant of UpdateTables in Advantage
									 @OldValue3 = @OldWBS3,
									 @ConstrFlag = 3 --Update tables


		if (@RetVal = 0)
			begin
				Update PR set CreateUser=@UserName, CreateDate=getutcdate(), ModUser=@UserName, ModDate=getutcdate() Where WBS1=@NewWBS1 and WBS2=@NewWBS2 and WBS3=@NewWBS3
				
				set @ErrorNum = @@Error + @ErrorNum

				if (@Diag <> 0)
					print 'Zero Pr Amounts'		

				/* Changed at 3.0 
				update pr set
					subLevel = 'Y',
					Fee = 0,
					ReimbAllow= 0,
					ConsultFee= 0,
					BudOHRate= 0,
					MultAmt= 0,
					PctComp= 0,
					LabPctComp= 0,
					ExpPctComp= 0,
					TotalProjectCost= 0,
					FirmCost= 0 
				where wbs1 = @OldWBS1
				and WBS2 = @OldWBS2
				and WBS3 = ' '*/		
				update pr set
					subLevel = 'Y',
					BudOHRate= 0,
					MultAmt= 0
				where wbs1 = @OldWBS1
				and WBS2 = @OldWBS2
				and WBS3 = N' '
				set @ErrorNum = @@Error + @ErrorNum
				update pr set name = @Name, longname = @Name 
					where wbs1 = @NewWBS1 and WBS2 = @NewWBS2 and WBS3 = @NewWBS3
				set @ErrorNum = @@Error + @ErrorNum
--Dave K stuff
				if (@NewWBS3 <> ' ') --enabling tasks for a phase
					begin
						update BT set WBS2ToPost = @NewWBS2, WBS3ToPost = @NewWBS3 
							where WBS1 = @OldWBS1 and WBS2ToPost = @OldWBS2 and WBS3ToPost = @OldWBS3
					end
				update billInvSums set BillWBS2 = @NewWBS2,BillWBS3 = @NewWBS3 
							where BillWBS1 = @OldWBS1 and BillWBS2 = @OldWBS2 and BillWBS3 = @OldWBS3 and
							arrayType = 'P'
				update BTF set PostWBS2 = @NewWBS2, PostWBS3 = @NewWBS3 where
							isNull(PostWBS1,WBS1) = @OldWBS1 and PostWBS2 = @OldWBS2 and PostWBS3 = @OldWBS3 
--

-- Fixed by DP 09/03/2008 to fix ProjectCustomTabFields records. Copy the record back from new WBS1/WBS2/WBS3 to old WBS1/WBS2/WBS3
				Select * into #TempProjectCustomTabFields from ProjectCustomTabFields where wbs1=@NewWBS1 and WBS2=@NewWBS2 and WBS3=@NewWBS3
				Update #TempProjectCustomTabFields Set WBS1 = @OldWBS1, WBS2 = @OldWBS2, WBS3 = @OldWBS3
				insert into ProjectCustomTabFields Select * from #TempProjectCustomTabFields 
				Drop Table #TempProjectCustomTabFields

 -- Added by DP 4.1 : Copy Team tab information instead of moving
				if (@NewWBS3 <> ' ') --enabling tasks for a phase
					begin
						INSERT INTO EMProjectAssoc (RecordID,WBS1,WBS2,WBS3,Employee,Role,RoleDescription,TeamStatus,StartDate,EndDate,CreateUser,ModUser) 
						SELECT replace(convert(Nvarchar(50),newid()),'-',''),WBS1,WBS2,' ',Employee,Role,RoleDescription,TeamStatus,StartDate,EndDate,@UserName,@UserName 
						from EMProjectAssoc where wbs1 = @NewWBS1 and WBS2 = @NewWBS2 and WBS3 = @NewWBS3

						INSERT INTO ClendorProjectAssoc (WBS1,WBS2,WBS3,ClientID,Role,RoleDescription,TeamStatus,Address,PrimaryInd,ClientConfidential,ClientInd,VendorInd,CreateUser,ModUser) 
						SELECT WBS1,WBS2,' ',ClientID,Role,RoleDescription,TeamStatus,Address,PrimaryInd,ClientConfidential,ClientInd,VendorInd,@UserName,@UserName 
						from ClendorProjectAssoc where wbs1 = @NewWBS1 and WBS2 = @NewWBS2 and WBS3 = @NewWBS3
					end
				else
					begin
						INSERT INTO EMProjectAssoc (RecordID,WBS1,WBS2,WBS3,Employee,Role,RoleDescription,TeamStatus,StartDate,EndDate,CreateUser,ModUser) 
						SELECT replace(convert(Nvarchar(50),newid()),'-',''),WBS1,' ',' ',Employee,Role,RoleDescription,TeamStatus,StartDate,EndDate,@UserName,@UserName 
						from EMProjectAssoc where wbs1 = @NewWBS1 and WBS2 = @NewWBS2 and WBS3 = @NewWBS3
						
						INSERT INTO ClendorProjectAssoc (WBS1,WBS2,WBS3,ClientID,Role,RoleDescription,TeamStatus,Address,PrimaryInd,ClientConfidential,ClientInd,VendorInd,CreateUser,ModUser) 
						SELECT WBS1,' ',' ',ClientID,Role,RoleDescription,TeamStatus,Address,PrimaryInd,ClientConfidential,ClientInd,VendorInd,@UserName,@UserName 
						from ClendorProjectAssoc where wbs1 = @NewWBS1 and WBS2 = @NewWBS2 and WBS3 = @NewWBS3
					end

				Update Activity Set wbs1 = @OldWBS1, WBS2 = @OldWBS2, WBS3 = @OldWBS3 where wbs1 = @NewWBS1 and WBS2 = @NewWBS2 and WBS3 = @NewWBS3
				
-- RonK 07/15/09: Update for new Rubik Contract Tables
				declare @AutoSumComp	 	Nvarchar(1)
				set @AutoSumComp = (select AutoSumComp from FW_CFGSystem)
				if (@AutoSumComp = 'Y')
				begin
					if (@NewWBS3 <> ' ')   -- enabling tasks for a phase, just update ContractDetails WBS2 rcds with WBS3
					begin
						Insert into ContractDetails (ContractNumber, WBS1, WBS2, WBS3, Fee, ReimbAllow, ConsultFee, Total, 
							FeeBillingCurrency, ReimbAllowBillingCurrency, ConsultFeeBillingCurrency, TotalBillingCurrency,
							FeeFunctionalCurrency, ReimbAllowFunctionalCurrency, ConsultFeeFunctionalCurrency, TotalFunctionalCurrency,
							FeeDirLab, FeeDirExp, ReimbAllowExp, ReimbAllowCons, FeeDirLabBillingCurrency, FeeDirExpBillingCurrency,
							ReimbAllowExpBillingCurrency, ReimbAllowConsBillingCurrency, FeeDirLabFunctionalCurrency, 
							FeeDirExpFunctionalCurrency, ReimbAllowExpFunctionalCurrency, ReimbAllowConsFunctionalCurrency,
							CreateUser, CreateDate) 
						SELECT Contracts.ContractNumber, Contracts.WBS1, @NewWBS2, @NewWBS3, Fee, ReimbAllow, ConsultFee, Total, 
							FeeBillingCurrency, ReimbAllowBillingCurrency, ConsultFeeBillingCurrency, TotalBillingCurrency,
							FeeFunctionalCurrency, ReimbAllowFunctionalCurrency, ConsultFeeFunctionalCurrency, TotalFunctionalCurrency,
							FeeDirLab, FeeDirExp, ReimbAllowExp, ReimbAllowCons, FeeDirLabBillingCurrency, FeeDirExpBillingCurrency,
							ReimbAllowExpBillingCurrency, ReimbAllowConsBillingCurrency, FeeDirLabFunctionalCurrency, 
							FeeDirExpFunctionalCurrency, ReimbAllowExpFunctionalCurrency, ReimbAllowConsFunctionalCurrency,
							@UserName, getutcdate() 
							from ContractDetails inner join Contracts 
								on (ContractDetails.ContractNumber = Contracts.ContractNumber
			 					  	and ContractDetails.WBS1 = Contracts.WBS1) 
							Where ContractDetails.WBS1 = @NewWBS1 and ContractDetails.WBS2 = @NewWBS2 
									and ContractDetails.WBS3 <= N' ' and Contracts.FeeIncludeInd = 'Y'
					end
					
					else					-- enabling phases for a project, create new WBS2 records for amounts.
					begin
						Insert into ContractDetails (ContractNumber, WBS1, WBS2, WBS3, Fee, ReimbAllow, ConsultFee, Total, 
							FeeBillingCurrency, ReimbAllowBillingCurrency, ConsultFeeBillingCurrency, TotalBillingCurrency,
							FeeFunctionalCurrency, ReimbAllowFunctionalCurrency, ConsultFeeFunctionalCurrency, TotalFunctionalCurrency,
							FeeDirLab, FeeDirExp, ReimbAllowExp, ReimbAllowCons, FeeDirLabBillingCurrency, FeeDirExpBillingCurrency,
							ReimbAllowExpBillingCurrency, ReimbAllowConsBillingCurrency, FeeDirLabFunctionalCurrency, 
							FeeDirExpFunctionalCurrency, ReimbAllowExpFunctionalCurrency, ReimbAllowConsFunctionalCurrency,
							CreateUser, CreateDate) 
						SELECT Contracts.ContractNumber, Contracts.WBS1, @NewWBS2, N' ', Fee, ReimbAllow, ConsultFee, Total, 
							FeeBillingCurrency, ReimbAllowBillingCurrency, ConsultFeeBillingCurrency, TotalBillingCurrency,
							FeeFunctionalCurrency, ReimbAllowFunctionalCurrency, ConsultFeeFunctionalCurrency, TotalFunctionalCurrency,
							FeeDirLab, FeeDirExp, ReimbAllowExp, ReimbAllowCons, FeeDirLabBillingCurrency, FeeDirExpBillingCurrency,
							ReimbAllowExpBillingCurrency, ReimbAllowConsBillingCurrency, FeeDirLabFunctionalCurrency, 
							FeeDirExpFunctionalCurrency, ReimbAllowExpFunctionalCurrency, ReimbAllowConsFunctionalCurrency,
							@UserName, getutcdate() 
							from ContractDetails inner join Contracts 
								on (ContractDetails.ContractNumber = Contracts.ContractNumber
			 					  	and ContractDetails.WBS1 = Contracts.WBS1) 
							Where ContractDetails.WBS1 = @NewWBS1 and ContractDetails.WBS2 <= N' ' 
									and ContractDetails.WBS3 <= N' ' and Contracts.FeeIncludeInd = 'Y'
					end	
				end
				Else
				begin
					Update PR set 
					Fee = 0,
					ReimbAllow= 0,
					ConsultFee= 0,
					FeeBillingCurrency = 0,
					ReimbAllowBillingCurrency= 0,
					ConsultFeeBillingCurrency= 0,
					FeeFunctionalCurrency = 0,
					ReimbAllowFunctionalCurrency= 0,
					ConsultFeeFunctionalCurrency= 0,
					FeeDirLab = 0,
					FeeDirExp = 0,
					ReimbAllowExp = 0,
					ReimbAllowCons = 0,
					FeeDirLabBillingCurrency = 0,
					FeeDirExpBillingCurrency = 0,
					ReimbAllowExpBillingCurrency = 0,
					ReimbAllowConsBillingCurrency = 0,
					FeeDirLabFunctionalCurrency = 0,
					FeeDirExpFunctionalCurrency = 0,
					ReimbAllowExpFunctionalCurrency = 0,
					ReimbAllowConsFunctionalCurrency = 0
					Where WBS1=@NewWBS1 and WBS2=@NewWBS2 and WBS3=@NewWBS3
				end
-- RonK 07/15/09: End Mod				

--Reset Contracts.FeeIncludeInd to 'N'
			Update Contracts set FeeIncludeInd='N'
			from Contracts C 
			Where C.WBS1=@NewWBS1 and C.FeeIncludeInd='Y'

			if (Select SyncProjToContractFees from FW_CFGSystem) = 'Y'
				update PR set Fee = 0, ConsultFee = 0, ReimbAllow = 0, FeeBillingCurrency = 0, ConsultFeeBillingCurrency =0, 
					ReimbAllowBillingCurrency =0,   FeeFunctionalCurrency = 0, ConsultFeeFunctionalCurrency = 0, ReimbAllowFunctionalCurrency = 0 , 
					FeeDirLab = 0 , FeeDirExp = 0, ReimbAllowExp = 0 , ReimbAllowCons = 0, 
					FeeDirLabBillingCurrency = 0 , FeeDirExpBillingCurrency = 0, ReimbAllowExpBillingCurrency = 0 , ReimbAllowConsBillingCurrency = 0,   
					FeeDirLabFunctionalCurrency = 0 , FeeDirExpFunctionalCurrency = 0, ReimbAllowExpFunctionalCurrency = 0 , ReimbAllowConsFunctionalCurrency = 0  
					where WBS1 in (@NewWBS1)
--
			
--

/* Removed by DP 4.0
				exec @RetVal = KeyCvt @Entity = 'WBS3',
											@CvtType = @CvtType,
											@Length = 20,
											@ErrMsg = @ErrMsg2 output,
											@Diag = @Diag,
											@ConstrFlag = 2 -- Enable Constraints
				alter table billAddDetail check constraint all
				alter table billARDetail check constraint all
				alter table billBTDDetail check constraint all
				alter table billIntDetail check constraint all
				alter table billInvMaster check constraint all
				alter table billLimDetail check constraint all
				alter table billRetDetail check constraint all
				alter table billTaxDetail check constraint all
				alter table BTA check constraint all
				alter table BT check constraint all
				alter table ICBillInvMaster check constraint all
				alter table ICBillTaxDetail check constraint all
				alter table CFGICBillingTerms check constraint all
*/
-- Add by DP 4.0
				if (@NewWBS3 <> ' ') --enabling tasks for a phase
				begin
					set @OldWBS3 = ''
					set @OldName3 = ''
					set @NewName3 = @Name
				end
				else	-- enabling phase for a project
				begin
					set @OldWBS2 = ''
					set @OldName2 = ''
					set @NewName2 = @Name
					set @OldWBS3 = ''
					set @OldName3 = ''
					set @NewWBS3 = ''
					set @NewName3 = ''
				end

				declare @NewPlanID varchar(32)
				select @NewPlanID=PlanID from RPPlan where wbs1=@NewWBS1
				PRINT '>>> Start Processing PlanID = "' + @NewPlanID + '", WBS1 = "' + @NewWBS1 + '"'
				Update RPTask Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update RPAssignment Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update RPExpense Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update RPConsultant Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update RPUnit Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update PNTask Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update PNAssignment Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update PNExpense Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update PNConsultant Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				EXECUTE dbo.stRPSyncWBSStructure @NewPlanID
				PRINT '<<< End Processing PlanID = "' + @NewPlanID + '", WBS1 = "' + @NewWBS1 + '"'

				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq, OldKey2, OldKey3, NewKey2, NewKey3, OldName2, OldName3, NewName2, NewName3, WBSName)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq, @OldWBS2, @OldWBS3, @NewWBS2, @NewWBS3, @OldName2, @OldName3, @NewName2, @NewName3, WBSName from keyConvertWork where Pkey = @PKey
				end
--
				delete from keyConvertWork where current of keysCursor
				if ((@ErrorNum + @RetVal) <> 0)
					begin
						if (@Diag <> 0)
							print 'Error :' + convert(Nvarchar,@ErrorNum)
							begin
								set @ErrMsg = @ErrMsg + @ErrMsg2
								rollback transaction
							end
						return(@ErrorNum)
					end
				commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
				rollback transaction
			end
	fetch next from KeysCursor into 
		@OldWBS1,
		@OldWBS2,
		@OldWBS3,
		@NewWBS1,
		@NewWBS2,
		@NewWBS3,@Name, 
		@PKey
	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	return(0)
End -- Procedure
GO
