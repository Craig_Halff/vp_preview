SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[HAI_ProjectSelectionProcessStages]
	@SelectionProcess varchar(25), 
	@Stage			varchar(25)
AS
/* 
	Copyright (c) 2020 Halff Associates, Inc.  All rights reserved.
	10/04/2020	Craig H. Anderson
	07/15/2021	David Springer revised for Vantagepoint

	Compare selected Stage against Selection Process.
*/
DECLARE @error varchar(255) = '';
BEGIN
SET NOCOUNT ON;
    DECLARE @StageName varchar(25) = (
                SELECT  Description
                FROM    dbo.CFGProjectStageDescriptions
                WHERE   Code = @Stage
            );
    IF @StageName IN ( '03', '04' )
       AND  @SelectionProcess NOT IN ( 'RFQ Process', 'RFQ/RFP Process' )
    BEGIN
        SET @error
            = CONCAT(
                  'Project Stage: '
                , @StageName
                , ' is only valid for the "RFQ" or "RFQ+RFP" Selection Process.                                           '
              );
        RAISERROR(@error, 16, 1);
    END;
    ELSE IF @StageName IN ( '05', '06' )
            AND @SelectionProcess NOT IN ( 'RFQ/RFP Process' )
    BEGIN
        SET @error
            = CONCAT(
                  'Project Stage: '
                , @StageName
                , ' is only valid for the "RFQ+RFP" Selection Process.                                           '
              );
        RAISERROR(@error, 16, 1);
    END;
    ELSE IF @StageName IN ( '07', '08' )
            AND @SelectionProcess NOT IN ( 'Fee Proposal', 'Task Work Order' )
    BEGIN
        SET @error
            = CONCAT(
                  'Project Stage: '
                , @StageName
                , ' is only valid for the "Fee Proposal" or "Task Work Order" Selection Process.                                           '
              );
        RAISERROR(@error, 16, 1);
    END;
END;
GO
