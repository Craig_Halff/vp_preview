SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_DelegationDel] (@Id varchar(32), @Username Nvarchar(32))
AS BEGIN
/*
	Copyright (c) 2020 EleVia Software. All rights reserved.

	exec spCCG_PAT_DelegationIns 'd','ADMIN','00001','00003','3/1/2017','3/8/2017',0,'N',null,null,'N'
	select * from CCG_PAT_Delegation
	select * from CCG_PAT_HistoryDelegation
	exec spCCG_PAT_DelegationDel 'd','ADMIN'
	select * from CCG_PAT_Delegation
	select * from CCG_PAT_HistoryDelegation
*/
	set nocount on
	BEGIN TRY
		declare @EmployeeOrig Nvarchar(20), @DelegateOrig Nvarchar(20), @FromDateOrig datetime, @ToDateOrig datetime
		declare @MaxAmountOrig money, @DualOrig char(1), @ApprovedByOrig Nvarchar(20), @ApprovedOnOrig datetime
		select @EmployeeOrig=Employee, @DelegateOrig=Delegate, @FromDateOrig=FromDate, @ToDateOrig=ToDate, @MaxAmountOrig=MaxAmount, @DualOrig=Dual, @ApprovedByOrig=ApprovedBy, @ApprovedOnOrig=ApprovedOn
			from CCG_PAT_Delegation where Id=@Id

		delete from CCG_PAT_Delegation where Id=@Id

		insert into CCG_PAT_HistoryDelegation (Id,Employee,Delegate,FromDate,ToDate,ActionTaken,ActionDate,ActionTakenBy,ActionRecipient,Description)
			select @Id,@EmployeeOrig,@DelegateOrig,@FromDateOrig,@ToDateOrig,N'Deleted',getutcdate(),Employee,null,N'Max Amt = ' + Cast(@MaxAmountOrig as Nvarchar) + ', Dual = ' + @DualOrig
			from SEUser where Username=@Username

		select 0 as Result, '' as ErrorMessage
	END TRY
	BEGIN CATCH
		select -1 as Result, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage
	END CATCH
END
GO
