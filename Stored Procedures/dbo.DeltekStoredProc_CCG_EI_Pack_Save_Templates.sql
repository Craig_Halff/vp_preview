SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Pack_Save_Templates]
	@sqlStatements	Nvarchar(max),
	@delimiter		varchar(20)
AS
BEGIN
	/*
		DeltekStoredProc_CCG_EI_Pack_Save_Templates
			@sqlStatements = '0
select @lastID = 1<<<SQL>>>1
insert into CCG_EI_TemplateParameters(TemplateSeq,Name,[Type],SourceType,[Value],ChangedBy,DateChanged)  values( @lastID,  ''NewParam'' , ''string'' , ''static'' ,''ddd'' ,''ADMIN'' , GetUTCDate() ) ',
			@delimiter = '<<<SQL>>>'
	*/
	SET NOCOUNT ON;
	DECLARE @i			int;
	DECLARE @newI		int;
	DECLARE @execString nvarchar(max);
	DECLARE @safeSql	int;
	DECLARE @delimLen	int;
	DECLARE @level		int;
	DECLARE @lastID		int;

	SET @delimLen = LEN(@delimiter);

	SET @i = 1
	WHILE @i >= 0 AND @i < LEN(@sqlStatements) BEGIN
		SET @newI = CHARINDEX(@delimiter, @sqlStatements, @i + 1);
		IF @newI <= 0 SET @newI = Len(@sqlStatements) + 1;
		SET @level = SUBSTRING(@sqlStatements, @i, 1);
		SET @execString = SUBSTRING(@sqlStatements, @i + 1, @newI - @i - 1);

		--PRINT @level;
		IF @level = 0 BEGIN
			-- Ensure that our data is what is expected
			SET @safeSql = dbo.fnCCG_EI_RegexMatch('\s*(insert into|update|select)<SQL PHRASE>(; SELECT <SQL PHRASE>)?', @execString);
			IF @safeSql = 0 BEGIN
				SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. EXECUTION STRING: ' + @execString;
				RETURN;
			END;

			--PRINT (@execString);
			EXECUTE sp_executesql @execString, N'@lastID INT OUTPUT', @lastID OUTPUT
		END
		ELSE BEGIN
			-- Ensure that our data is what is expected
			SET @safeSql = dbo.fnCCG_EI_RegexMatch('\s*(insert into|update)<SQL PHRASE>', @execString);
			IF @safeSql = 0 BEGIN
				SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. EXECUTION STRING: ' + @execString;
				RETURN;
			END;

			SET @execString = REPLACE(@execString, '@lastID', CAST(ISNULL(@lastID, '') AS VARCHAR(10)));
			--PRINT (@execString);
			EXEC (@execString);
		END

		SET @i = @newI + @delimLen;
	END
END;
GO
