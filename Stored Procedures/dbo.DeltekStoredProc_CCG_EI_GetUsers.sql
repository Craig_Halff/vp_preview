SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_GetUsers] ( @VP3 bit= 0)
             AS EXEC spCCG_EI_GetUsers @VP3
GO
