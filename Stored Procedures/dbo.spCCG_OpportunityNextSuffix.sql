SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_OpportunityNextSuffix] (@OpportunityID varchar (32), @ModUser varchar (100))
AS
/*
Copyright 2017 Central Consulting Group.   All rights reserved.
04/03/2017 David Springer
           Create a new suffix opportunity with data from first opportunity.
08/03/2020	Craig H. Anderson
			Added CustFederalInd to the list of columns copied from the old Opportunity to the new Opporttunity
12/10/2020	Craig H. Anderson
			Modified @Suffix calculation to exlude .999 Projects from inclusion in calculating the MAX suffix.
*/
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

Declare @OppNo    varchar (32),
        @NewOppNo varchar (32),
        @NewOppID varchar (32),
        @Root     varchar (32),
        @Suffix   varchar (32)

BEGIN

   Select @OppNo = Opportunity, @NewOppID = Replace (NewID(), '-', '') 
   From Opportunity 
   Where OpportunityID = @OpportunityID

   Set @Root = left (@OppNo, len (@OppNo) -3)
   Select @Suffix = right ('00' + ltrim (str (MAX (right (Opportunity, 3)) + 1)), 3) From Opportunity Where Opportunity like @Root + '%' AND Opportunity NOT LIKE '%.999'
   Set @NewOppNo = @Root + @Suffix

-- Create Next Opportunity Suffix
   Insert Into Opportunity
   (OpportunityID, Opportunity, CreateDate, CreateUser,
    Name, Org, OpenDate, Status,
    OpportunityType, Source, OurRole, Stage, Principal, ProjMgr, Supervisor,
    ClientID, ContactID)
   Select 
    @NewOppID, @NewOppNo, getDate(), @ModUser,
    Name + ' ?????', Org, getDate(), 'A',
    OpportunityType, Source, OurRole, '01', Principal, ProjMgr, Supervisor,
    ClientID, ContactID
   From Opportunity 
   Where OpportunityID = @OpportunityID

   Insert Into OpportunityCustomTabFields
   (OpportunityID, CustBillingCompany, CustBillingContact, CustPracticeArea,
    CustAdministrativeAssistant, CustBiller, CustManagementLeader, CustDirector, CustCorporateMarketing, CustLocationURL, CustFederalInd)
   Select 
    @NewOppID, CustBillingCompany, CustBillingContact, CustPracticeArea,
       CustAdministrativeAssistant, CustBiller, CustManagementLeader, CustDirector, CustCorporateMarketing, 'https://projectlocator.halff.com/map?project=' + Right(@NewOppNo,9), CustFederalInd
   From OpportunityCustomTabFields
   Where OpportunityID = @OpportunityID


   Insert Into OpportunityClientAssoc 
   (OpportunityID, ClientID, PrimaryInd, Role, ClientInd, VendorInd, CreateUser, CreateDate)
   Select 
    @NewOppID, ClientID, 'Y', 'sysOwner', 'Y', 'N', 'CCG Procedure', getDate()
   From Opportunity 
   Where OpportunityID = @OpportunityID
     and ClientID is not null

   Insert Into OpportunityContactAssoc
   (OpportunityID, ContactID, PrimaryInd, Role, CreateUser, CreateDate)
   Select 
    @NewOppID, ContactID, 'Y', 'sysOwner', 'CCG Procedure', getDate()
   From Opportunity 
   Where OpportunityID = @OpportunityID
     and ContactID is not null

   Insert Into OpportunityClientAssoc 
   (OpportunityID, ClientID, PrimaryInd, Role, ClientInd, VendorInd, CreateUser, CreateDate)
   Select 
    @NewOppID, CustBillingCompany, 'N', '01', 'Y', 'N', 'CCG Procedure', getDate()
   From OpportunityCustomTabFields
   Where OpportunityID = @OpportunityID
     and CustBillingCompany is not null
     and not exists (Select 'x' From OpportunityClientAssoc Where OpportunityID = @OpportunityID and ClientID = CustBillingCompany)

   Insert Into OpportunityContactAssoc
   (OpportunityID, ContactID, PrimaryInd, Role, CreateUser, CreateDate)
   Select 
    @NewOppID, CustBillingContact, 'N', '01', 'CCG Procedure', getDate()
   From OpportunityCustomTabFields
   Where OpportunityID = @OpportunityID
     and CustBillingContact is not null
     and not exists (Select 'x' From OpportunityContactAssoc Where OpportunityID = @OpportunityID and ContactID = CustBillingContact)
END
GO
