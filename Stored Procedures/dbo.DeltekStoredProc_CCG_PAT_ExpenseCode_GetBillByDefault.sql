SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_ExpenseCode_GetBillByDefault]
	@ORTable		int,
	@ExpenseCode	Nvarchar(10)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SET NOCOUNT ON;
	SELECT BillByDefault
        FROM APExpenseCodeOTCodes
        WHERE TableNo = @ORTable and ExpenseCode = @ExpenseCode;
END
GO
