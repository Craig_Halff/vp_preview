SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_GetVendorUDFValue] ( @CustVendorField nvarchar(200), @VendorID nvarchar(50))
             AS EXEC spCCG_PAT_GetVendorUDFValue @CustVendorField,@VendorID
GO
