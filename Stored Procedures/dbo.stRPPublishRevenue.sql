SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPPublishRevenue]
  @strPlanID varchar(32)
AS

BEGIN 
  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	  UPDATE P SET
		P.StoredCurrency = PNPlan.StoredCurrency
		FROM RPPlan AS P
		  INNER JOIN PNPlan ON P.PlanID = PNPlan.PlanID
		WHERE P.PlanID = @strPlanID

		UPDATE P SET
		P.RevenueStartDate = PNTask.RevenueStartDate,
		P.RevenueEndDate = PNTask.RevenueEndDate
		FROM RPTask AS P
		  INNER JOIN PNTask ON P.PlanID = PNTask.PlanID AND P.TaskID = PNTask.TaskID 
		WHERE P.PlanID = @strPlanID

	DELETE RPPlannedRevenueLabor WHERE PlanID = @strPlanID

	INSERT INTO RPPlannedRevenueLabor
			   (TimePhaseID
			   ,TaskID
			   ,PlanID
			   ,StartDate
			   ,EndDate
			   ,PeriodCost
			   ,PeriodBill
			   ,CreateUser
			   ,CreateDate
			   ,ModUser
			   ,ModDate)
	SELECT TimePhaseID
			   ,TaskID
			   ,PlanID
			   ,StartDate
			   ,EndDate
			   ,PeriodCost
			   ,PeriodBill
			   ,CreateUser
			   ,CreateDate
			   ,ModUser
			   ,ModDate
		FROM PNPlannedRevenueLabor  WHERE PlanID = 	@strPlanID
	  SET NOCOUNT OFF

END -- stRPPublish
GO
