SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectReadyForProcessing] (
      @WBS1  varchar (32),     
      @WBS2  varchar (7),
      @WBS3  varchar (7),
      @ReadyForProcessing   varchar(1)
) AS
/*
Copyright 2017 Central Consulting Group.   All rights reserved.
03/10/2017 David Springer
           Call this from a Project CHANGE workflow when Approved for Use in Processing has changed.
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

BEGIN
 -- Project level record changed, so push down change
   IF @WBS2 = ' '
      BEGIN
      Update PR Set ReadyForProcessing = @ReadyForProcessing
      Where WBS1 = @WBS1
      END
 -- Phase level record changed, so push down change
   IF @WBS3 = ' '
      BEGIN
      Update PR Set ReadyForProcessing = @ReadyForProcessing
      Where WBS1 = @WBS1
        and WBS2 = @WBS2
        and WBS3 <> ' '
      END
END
GO
