SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPDeleteAllVisionPlans]
AS

BEGIN -- Procedure stRPDeleteAllVisionPlans

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete all Vision Plans. 
-- The Vision DB must have 1.0 Schema applied already.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @strPlanID varchar(32)
  DECLARE @strPlanName varchar(32)
  DECLARE @strWBS1 nvarchar(30)
  DECLARE @strReason nvarchar(max)

  DECLARE @tabPlan TABLE (
    PlanID varchar(32) COLLATE database_default,
    PlanName nvarchar(255) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    Reason nvarchar(max) COLLATE database_default
    UNIQUE(PlanID)
  )

  DECLARE @tabDelPNPlan TABLE (
    PlanID varchar(32) COLLATE database_default,
    PlanName nvarchar(255) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    Reason nvarchar(max) COLLATE database_default
    UNIQUE(PlanID)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find Vision Plans that does not have PN data and cannot be converted to iAccess format.
  -- Must use a temp table here otherwise the cursor will get confused when Plans are being changed inside a loop. 

  INSERT @tabPlan(
    PlanID,
    PlanName,
    WBS1,
    Reason
  )

    SELECT 
      A.PlanID AS PlanID, 
      A.PlanName AS PlanName, 
      A.WBS1 AS WBS1, 
      MIN(CONVERT(nvarchar(max), B.Reason)) AS Reason
      FROM (
        SELECT 
          RP.PlanID,
          RP.PlanName,
          RP.WBS1
          FROM RPPlan AS RP
            LEFT JOIN PNPlan AS PN ON RP.PlanID = PN.PlanID
          WHERE PN.PlanID IS NULL 
      ) A

        LEFT JOIN (

          SELECT
            PlanID,
            PlanName,
            WBS1,
            Reason,
            COUNT(*) OVER (PARTITION BY PlanID) AS CountDuplicate
            FROM (

              SELECT DISTINCT 
                RP.PlanID AS PlanID,
                RP.PlanName AS PlanName,
                RP.WBS1 AS WBS1,
                REVERSE(STUFF(REVERSE(RTRIM(
                  'This Vision Plan will be deleted because: ' +
                  CASE WHEN V.MappedToMultipleProjs = 1 THEN 'Mapped To Multiple Projects, ' ELSE '' END +
                  CASE WHEN V.HasIllegalLevel = 1 THEN 'Has Illegal Level, ' ELSE '' END +
                  CASE WHEN V.HasResourceAtNonLeaf = 1 THEN 'Has Resource At Non-Leaf, ' ELSE '' END +
                  CASE WHEN V.HasUnmappedWBSRows = 1 THEN 'Has Unmapped WBS Rows, ' ELSE '' END
                )), 1, 1, '')) AS Reason
                FROM RPPlan AS RP
                  INNER JOIN PR ON RP.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
                  LEFT JOIN PNPlan AS PN ON RP.PlanID = PN.PlanID 
                  CROSS APPLY dbo.PM$tabPlanValidate(RP.PlanID) AS V
                WHERE PN.PlanID IS NULL
                  AND (
                    V.MappedToMultipleProjs = 1 OR
                    V.HasIllegalLevel = 1 OR
                    V.HasResourceAtNonLeaf = 1 OR
                    V.HasUnmappedWBSRows = 1
                  )
                  AND RP.PlanID NOT IN (
                    SELECT PlanID
                      FROM (
                        SELECT
                          T.PlanID,
                          PR.WBS1 AS WBS1, 
                          COUNT(*) OVER (PARTITION BY PR.WBS1) AS CountRows
                          FROM (
                            SELECT DISTINCT
                              XT.PlanID,
                              XT.WBS1,
                              XT.WBS2,
                              XT.WBS3
                              FROM RPTask AS XT 
                              WHERE XT.WBSType = 'WBS1' AND XT.WBS2 IS NULL AND XT.WBS3 IS NULL
                          ) AS T
                            INNER JOIN PR ON T.WBS1 = PR.WBS1 AND ISNULL(T.WBS2, ' ') = PR.WBS2 AND ISNULL(T.WBS3, ' ') = PR.WBS3
                          WHERE PR.WBS2 = ' ' AND PR.WBS3 = ' '
                      ) AS X
                      WHERE CountRows > 1
                  )

              UNION ALL
              SELECT DISTINCT 
                RP.PlanID AS PlanID,
                RP.PlanName AS PlanName,
                RP.WBS1 AS WBS1,
                REVERSE(STUFF(REVERSE(RTRIM(
                  'This Vision Plan will be deleted because Project <' +
                  RP.WBS1 + '> is mapped to multiple Plans.'
                )), 1, 1, ''))
                FROM (
                  SELECT *
                    FROM (
                      SELECT
                        T.PlanID,
                        T.PlanName,
                        PR.WBS1 AS WBS1, 
                        COUNT(*) OVER (PARTITION BY PR.WBS1) AS CountRows
                        FROM (
                          SELECT DISTINCT
                            XT.PlanID,
                            XP.PlanName,
                            XT.WBS1,
                            XT.WBS2,
                            XT.WBS3
                            FROM RPTask AS XT 
                              INNER JOIN RPPlan AS XP ON XT.PlanID = XP.PlanID
                            WHERE XT.WBSType = 'WBS1' AND XT.WBS1 <> '<none>' AND XT.WBS2 IS NULL AND XT.WBS3 IS NULL
                        ) AS T
                          INNER JOIN PR ON T.WBS1 = PR.WBS1 AND ISNULL(T.WBS2, ' ') = PR.WBS2 AND ISNULL(T.WBS3, ' ') = PR.WBS3
                        WHERE PR.WBS2 = ' ' AND PR.WBS3 = ' '
                    ) AS X
                    WHERE CountRows > 1
                ) AS RP
                  LEFT JOIN PNPlan AS PN ON RP.PlanID = PN.PlanID 
                WHERE PN.PlanID IS NULL

              UNION ALL
              SELECT DISTINCT 
                RP.PlanID AS PlanID,
                RP.PlanName AS PlanName,
                RP.WBS1 AS WBS1,
                'This Vision Plan will be deleted because there is no Project mapping'
                FROM (
                  SELECT DISTINCT
                    XT.PlanID,
                    XP.PlanName,
                    XT.WBS1,
                    XT.WBS2,
                    XT.WBS3
                    FROM RPTask AS XT 
                      INNER JOIN RPPlan AS XP ON XT.PlanID = XP.PlanID
                    WHERE XT.WBS1 = '<none>' AND XT.WBS2 IS NULL AND XT.WBS3 IS NULL
                  UNION
                  SELECT DISTINCT
                    XT.PlanID,
                    XP.PlanName,
                    XT.WBS1,
                    XT.WBS2,
                    XT.WBS3
                    FROM RPTask AS XT 
                      INNER JOIN RPPlan AS XP ON XT.PlanID = XP.PlanID
                    WHERE XT.WBS1 IS NULL AND XT.WBS2 IS NULL AND XT.WBS3 IS NULL
                ) AS RP
                  LEFT JOIN PNPlan AS PN ON RP.PlanID = PN.PlanID 
                WHERE PN.PlanID IS NULL 

              UNION ALL
              SELECT DISTINCT 
                RP.PlanID AS PlanID,
                RP.PlanName AS PlanName,
                RP.WBS1 AS WBS1,
                'This Vision Plan will be deleted because there is no WBS row'
                FROM (
                  SELECT DISTINCT
                    XP.PlanID,
                    XP.PlanName,
                    XP.WBS1
                    FROM RPPlan AS XP
                      LEFT JOIN RPTask AS XT ON XP.PlanID = XT.PlanID
                    WHERE XT.PlanID IS NULL
                ) AS RP
                  LEFT JOIN PNPlan AS PN ON RP.PlanID = PN.PlanID 
                WHERE PN.PlanID IS NULL 

            ) AS Z

        ) AS B ON A.PlanID = B.PlanID

      GROUP BY A.PlanID, A.PlanName, A.WBS1

  DECLARE csr20170109_RPPlan CURSOR LOCAL FAST_FORWARD FOR
    SELECT PlanID, PlanName, WBS1, Reason
      FROM @tabPlan AS RP

  OPEN csr20170109_RPPlan
  FETCH NEXT FROM csr20170109_RPPlan INTO @strPlanID, @strPlanName, @strWBS1, @strReason

  WHILE (@@FETCH_STATUS = 0)
    BEGIN

      PRINT '>>> Start Processing PlanID = "' + ISNULL(@strPlanID, '') + '", WBS1 = "' + ISNULL(@strWBS1, '') + '"'
      PRINT '+++   ' + @strReason 
      EXECUTE dbo.PMDelPlan @strPlanID
      PRINT '<<< End Processing PlanID = "' + ISNULL(@strPlanID, '') + '", WBS1 = "' + ISNULL(@strWBS1, '') + '"'
      PRINT ''

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      FETCH NEXT FROM csr20170109_RPPlan INTO @strPlanID, @strPlanName, @strWBS1, @strReason

    END -- While

  CLOSE csr20170109_RPPlan
  DEALLOCATE csr20170109_RPPlan

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabDelPNPlan(
    PlanID,
    PlanName,
    WBS1,
    Reason
  )

    SELECT DISTINCT
      P.PlanID AS PlanID,
      P.PlanName AS PlanName,
      P.WBS1 AS WBS1,
      REVERSE(STUFF(REVERSE(RTRIM(
        'This iAccess Plan will be deleted because Project <' +
        P.WBS1 + '> has missing WBS rows in Plan and Plan has no Assignment/Expense/Consultant data.'
      )), 1, 1, '')) AS Reason
      FROM (
        SELECT DISTINCT
          PR.WBS1
          FROM PR
            LEFT JOIN PNTask AS T ON PR.WBS1 = T.WBS1 AND PR.WBS2 = ISNULL(T.WBS2, ' ') AND PR.WBS3 = ISNULL(T.WBS3, ' ')
          WHERE T.PlanID IS NULL
        ) AS X
          INNER JOIN PR ON PR.WBS1 = X.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          INNER JOIN PNPlan AS P ON X.WBS1 = P.WBS1
          LEFT JOIN PNAssignment AS A ON P.PlanID = A.PlanID
          LEFT JOIN PNExpense AS E ON P.PlanID = E.PlanID
          LEFT JOIN PNConsultant AS C ON P.PlanID = C.PlanID
      WHERE P.WBS1 IS NOT NULL AND A.AssignmentID IS NULL AND E.ExpenseID IS NULL AND C.ConsultantID IS NULL 

    UNION

    SELECT DISTINCT
      P.PlanID AS PlanID,
      P.PlanName AS PlanName,
      P.WBS1 AS WBS1,
      'This iAccess Plan will be deleted because the Plan is not associated with a Project.' AS Reason
      FROM PNPlan AS P
        LEFT JOIN PR ON P.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
      WHERE P.WBS1 IS NOT NULL AND PR.WBS1 IS NULL

  DECLARE csr20170623_DelPNPlan CURSOR LOCAL FAST_FORWARD FOR
    SELECT PlanID, PlanName, WBS1, Reason
      FROM @tabDelPNPlan AS PN

  OPEN csr20170623_DelPNPlan
  FETCH NEXT FROM csr20170623_DelPNPlan INTO @strPlanID, @strPlanName, @strWBS1, @strReason

  WHILE (@@FETCH_STATUS = 0)
    BEGIN

      PRINT '>>> Start Processing PlanID = "' + ISNULL(@strPlanID, '') + '", WBS1 = "' + ISNULL(@strWBS1, '') + '"'
      PRINT '+++   ' + @strReason 
      EXECUTE dbo.stRPDelPlan @strPlanID
      PRINT '<<< End Processing PlanID = "' + ISNULL(@strPlanID, '') + '", WBS1 = "' + ISNULL(@strWBS1, '') + '"'
      PRINT ''

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      FETCH NEXT FROM csr20170623_DelPNPlan INTO @strPlanID, @strPlanName, @strWBS1, @strReason

    END -- While

  CLOSE csr20170623_DelPNPlan
  DEALLOCATE csr20170623_DelPNPlan
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

END -- stRPDeleteAllVisionPlans
GO
