SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_HAI_ProjectTypes_GetById]
(
	@id	NVARCHAR(32)
)
AS
BEGIN
  ------------------------------------------------------------------------
  SET NOCOUNT ON;

  ------------------------------------------------------------------------
  SELECT
    PT.UDIC_UID
   ,PT.CustomCurrencyCode
   ,PT.CreateUser
   ,PT.CreateDate
   ,PT.ModUser
   ,PT.ModDate
   ,PT.CustProjectTypeCode
   ,PT.CustProjectTypeName
   ,PT.CustProjectTypeCategory
   ,PT.CustProjectTypeSubCategory
   ,PT.CustSF330Code
   ,PT.CustSortOrder
  FROM
    dbo.UDIC_ProjectType AS PT
  WHERE
	PT.UDIC_UID = @id
  ------------------------------------------------------------------------
END
GO
