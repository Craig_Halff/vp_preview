SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_Pct_Complete_Update_Billing_Terms]
	@wbs1				Nvarchar(32),
	@wbs2				Nvarchar(7),
	@wbs3				Nvarchar(7),
	@KEY_EMPLOYEEID		Nvarchar(30),
	@oldFeeMethod		varchar(50),
	@newFeeMethod		varchar(5),
	@changeFeeMethod	bit,
	@oldFeeBasis		varchar(1),
	@newFeeBasis		varchar(1),
	@changeFeeBasis		bit,
	@oldFeeFactor1		varchar(50),
	@newFeeFactor1		varchar(50),
	@changeFeeFactor1	bit,
	@oldFeeFactor2		varchar(50),
	@newFeeFactor2		varchar(50),
	@changeFeeFactor2	bit,
	@oldFeeLabel		varchar(15),
	@newFeeLabel		varchar(15),
	@changeFeeLabel		bit
AS
BEGIN
	/*
	DEBUG TEST:
	DeltekStoredProc_CCG_EI_Pct_Complete_Update_Billing_Terms
		@wbs1 = '2003005.00', @wbs2 = ' ', @wbs3 = ' ',
		@KEY_EMPLOYEEID = '00001',
		@oldFeeMethod = 'Percent Complete by Phase, as Percent of Fee#', @newFeeMethod = '1', @changeFeeMethod = '1',
		@oldFeeBasis = 'Lump sum#', @newFeeBasis = 'P', @changeFeeBasis = '1',
		@oldFeeFactor1 = '', @newFeeFactor1 = '', @changeFeeFactor1 = '0',
		@oldFeeFactor2 = '', @newFeeFactor2 = '', @changeFeeFactor2 = '0',
		@oldFeeLabel = '', @newFeeLabel = '', @changeFeeLabel = '0'
	*/
	SET NOCOUNT ON
	UPDATE BT
		SET UseClientAddress = UseClientAddress,
			FeeMeth = (CASE WHEN @changeFeeMethod = 1 THEN Cast(@newFeeMethod as smallint) ELSE FeeMeth END),
			FeeBasis = (CASE WHEN @changeFeeBasis = 1 THEN @newFeeBasis ELSE FeeBasis END),
			FeeFactor1 = (CASE WHEN @changeFeeFactor1 = 1 THEN Cast(@newFeeFactor1 as decimal(19,4)) ELSE FeeFactor1 END),
			FeeFactor2 = (CASE WHEN @changeFeeFactor2 = 1 THEN Cast(@newFeeFactor2 as decimal(19,4)) ELSE FeeFactor2 END),
			FeeLabel = (CASE WHEN @changeFeeLabel = 1 THEN @newFeeLabel ELSE FeeLabel END),
			FeeToDate = (CASE WHEN (@changeFeeFactor2 = 1 or @changeFeeFactor1 = 1 or @changeFeeMethod = 1 or @changeFeeBasis = 1) and @newFeeMethod = 1
				then Round(
					(case when @changeFeeFactor1 = 1 then @newFeeFactor1 else FeeFactor1 end)
						* (case when @newFeeBasis = 'L' then 1.0
							else (case when @changeFeeFactor2 = 1 then @newFeeFactor2 else FeeFactor2 end) /
								(case when @newFeeBasis = 'P' then 100.0 else 1.0 end) end)
						* BT.FeePctCpl / 100.0, 2)
				else FeeToDate end)
		WHERE WBS1 = @wbs1 AND WBS2 = @wbs2 AND WBS3 = @wbs3

	-- Update the history
	IF @changeFeeMethod = 1
		INSERT INTO CCG_EI_HistoryUpdateColumns
			(ActionTaken, ActionDate, ActionTakenBy, WBS1, WBS2, WBS3, BTFSeq, DataType,
				ConfigCustomColumn, ColumnLabel, OldValue, NewValue, OldValueDecimal, NewValueDecimal)
			VALUES ('Update', getutcdate(), @KEY_EMPLOYEEID, @wbs1, @wbs2, @wbs3, 0, 'A',
				NULL, 'Fee Method', @oldFeeMethod, Cast(@newFeeMethod AS varchar(50)), NULL, NULL)

	IF @changeFeeBasis = 1
		INSERT INTO CCG_EI_HistoryUpdateColumns
			(ActionTaken, ActionDate, ActionTakenBy, WBS1, WBS2, WBS3, BTFSeq, DataType,
				ConfigCustomColumn, ColumnLabel, OldValue, NewValue, OldValueDecimal, NewValueDecimal)
			VALUES ('Update', getutcdate(), @KEY_EMPLOYEEID, @wbs1, @wbs2, @wbs3, 0, 'A',
				NULL, 'Fee Basis', @oldFeeBasis, @newFeeBasis, NULL, NULL)

	IF @changeFeeFactor1 = 1 BEGIN
		INSERT INTO CCG_EI_HistoryUpdateColumns
			(ActionTaken, ActionDate, ActionTakenBy, WBS1, WBS2, WBS3, BTFSeq, DataType,
				ConfigCustomColumn, ColumnLabel, OldValue, NewValue, OldValueDecimal, NewValueDecimal)
			VALUES ('Update', getutcdate(), @KEY_EMPLOYEEID, @wbs1, @wbs2, @wbs3, 0, 'N',
				NULL,
				CASE @newFeeBasis
					WHEN 'L' THEN 'Fee'
					WHEN 'P' THEN 'Est construction amt'
					ELSE 'Unit quantity'
				END, @oldFeeFactor1, @newFeeFactor1, @oldFeeFactor1, @newFeeFactor1)
	END

	IF @changeFeeFactor2 = 1 BEGIN						-- TODO: Distinguish between FeeFactor1 and FeeFactor2 in the History
		INSERT INTO CCG_EI_HistoryUpdateColumns
			(ActionTaken, ActionDate, ActionTakenBy, WBS1, WBS2, WBS3, BTFSeq, DataType,
				ConfigCustomColumn, ColumnLabel, OldValue, NewValue, OldValueDecimal, NewValueDecimal)
			VALUES ('Update', getutcdate(), @KEY_EMPLOYEEID, @wbs1, @wbs2, @wbs3, 0, 'N',
				NULL,
				CASE @newFeeBasis
					WHEN 'L' THEN 'Fee'
					WHEN 'P' THEN 'Est construction amt'
					ELSE 'Unit quantity'
				END, Cast(@oldFeeFactor2 AS varchar(50)), Cast(@newFeeFactor2 AS varchar(50)), @oldFeeFactor2, @newFeeFactor2)
	END

	IF @changeFeeLabel = 1
		INSERT INTO CCG_EI_HistoryUpdateColumns
			(ActionTaken, ActionDate, ActionTakenBy, WBS1, WBS2, WBS3, BTFSeq, DataType,
				ConfigCustomColumn, ColumnLabel, OldValue, NewValue, OldValueDecimal, NewValueDecimal)
			VALUES ('Update', getutcdate(), @KEY_EMPLOYEEID, @wbs1, @wbs2, @wbs3, 0, 'A',
				NULL, 'Unit Label', @oldFeeLabel, @newFeeLabel, NULL, NULL)

	if @changeFeeMethod = 1 and @newFeeMethod in (1, 2)
		delete from CCG_EI_FeePctCpl where WBS1 = @wbs1 AND WBS2 = @wbs2 AND WBS3 = @wbs3 and Seq > 0

	if (@changeFeeFactor2 = 1 or @changeFeeFactor1 = 1 or @changeFeeMethod = 1 or @changeFeeBasis = 1) and @newFeeMethod in (1, 2)
	begin 
		UPDATE BTF
			SET FeeToDate = Round(
				(case when @changeFeeFactor1 = 1 then @newFeeFactor1 else BT.FeeFactor1 end)
					* (case when @newFeeBasis = 'L' then 1.0
						else (case when @changeFeeFactor2 = 1 then @newFeeFactor2 else BT.FeeFactor2 end) /
							(case when @newFeeBasis = 'P' then 100.0 else 1.0 end) end)
					* BTF.PctComplete / 100.0 * BTF.PctOfFee / 100.0, 2)
			FROM BT INNER JOIN BTF ON BTF.WBS1 = BT.WBS1 and BTF.WBS2 = BT.WBS2 and BTF.WBS3 = BT.WBS3
			WHERE BT.WBS1 = @wbs1 AND BT.WBS2 = @wbs2 AND BT.WBS3 = @wbs3

		update fpc set FeeToDate = Round(
			(case when @changeFeeFactor1 = 1 then @newFeeFactor1 else FeeFactor1 end)
				* (case when @newFeeBasis = 'L' then 1.0
					else (case when @changeFeeFactor2 = 1 then @newFeeFactor2 else FeeFactor2 end) /
						(case when @newFeeBasis = 'P' then 100.0 else 1.0 end) end)
				* fpc.FeePctCpl / 100.0 * isnull(BTF.PctOfFee / 100.0, 1.0), 2)
			from BT
				inner join CCG_EI_FeePctCpl fpc on fpc.WBS1 = BT.wbs1 AND fpc.WBS2 = BT.wbs2 AND fpc.WBS3 = BT.wbs3
				left join BTF ON BTF.WBS1 = BT.WBS1 and BTF.WBS2 = BT.WBS2 and BTF.WBS3 = BT.WBS3 and BTF.Seq = fpc.Seq
			where fpc.WBS1 = @wbs1 AND fpc.WBS2 = @wbs2 AND fpc.WBS3 = @wbs3
	end
END
GO
