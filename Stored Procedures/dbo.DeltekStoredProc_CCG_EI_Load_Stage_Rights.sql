SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Load_Stage_Rights]
	@VISION_LANGUAGE varchar(10)
AS
BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_EI_Load_Stage_Rights] 'en-US'
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT PossibleRows.StageFlow, PossibleRows.Role, PossibleRows.Stage, PossibleRows.StageOrder,
		-- need to spell these out so we don't have duplicates
			ISNULL(T1.CanView, 'N') As CanView, ISNULL(T1.CanSet, 'N') As CanSet,
			ISNULL(T1.CanModify, 'N') As CanModify, ISNULL(T1.SendEmailOnStageSet, 'N') As SendEmailOnStageSet,
			ISNULL(T1.PackageView, 'N') As PackageView, ISNULL(T1.PackageModify, 'N') As PackageModify,
			ISNULL(T1.CCSender, 'N') As CCSender, T1.Renotify,
			IsNull(IsNull(T2.EmailMessage, T3.EmailMessage), '') as EmailMessage,
			IsNull(IsNull(T2.EmailSubject, T3.EmailSubject), '') as EmailSubject,
			IsNull(IsNull(T2.EmailSubjectBatch, T3.EmailSubjectBatch), '') as EmailSubjectBatch
        FROM CCG_EI_ConfigRights T1
			LEFT JOIN CCG_EI_ConfigRightsDescriptions T2
				ON T2.StageFlow = T1.StageFlow and T2.Role = T1.Role and T2.Stage = T1.Stage
					and T2.UICultureName = @VISION_LANGUAGE
			LEFT JOIN CCG_EI_ConfigRightsDescriptions T3
				ON T3.StageFlow = T1.StageFlow and T3.Role = T1.Role and T3.Stage = T1.Stage
					and T3.UICultureName = 'en-US'
			RIGHT JOIN (
				SELECT StageFlow, Role, Stage, StageOrder
					FROM CCG_EI_ConfigStageFlows Flows, CCG_EI_ConfigRoles Roles
					WHERE Flows.Status = 'A' and Roles.Status = 'A' and not Roles.Role = 'Accounting'
			) PossibleRows
				ON T1.StageFlow = PossibleRows.StageFlow and T1.Stage = PossibleRows.Stage
					and T1.Role = PossibleRows.Role
        ORDER BY 1, 2, 4, 3;

END;
GO
