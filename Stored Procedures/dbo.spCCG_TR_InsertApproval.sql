SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_TR_InsertApproval](@TableName varchar(10), @Period int, @PostSeq int, @PKey varchar(32), 
	@SubmittedBy varchar(20), @ApprovedBy varchar(20), @ApprovalComment varchar(255), 
	@IncludeFuture char(1), @BillNonBillable char(1), @FNStatus varchar(2)) 
AS BEGIN

	declare @SubmittedDate datetime, @ApprovedDate datetime, @dt datetime
	declare @ModComment varchar(255), @BillStatus char(1)
	
	if RTrim(@SubmittedBy)			= '' set @SubmittedBy = null
	if RTrim(@ApprovedBy)			= '' set @ApprovedBy  = null
	--if RTrim(@FutureOrNonBillable)= '' set @FutureOrNonBillable = null
	if RTrim(@IncludeFuture)		= '' set @IncludeFuture = null
	if RTrim(@BillNonBillable)		= '' set @BillNonBillable = null
	
	set @dt = getutcdate()
	if @SubmittedBy is not null set @SubmittedDate=@dt
	if @ApprovedBy  is not null set @ApprovedDate =@dt
	if IsNull(@FNStatus,'')='' set @FNStatus = null

	select @ModComment=ModComment, @BillStatus=NewBillStatus from CCG_TR 
	where OriginalTable=@TableName and OriginalPeriod=@Period and OriginalPostSeq=@PostSeq and OriginalPKey=@PKey
	if IsNull(@BillStatus,'')=''
	begin
		
		if @TableName='LD'
			select @BillStatus=BillStatus from   LD where Period=@Period and PostSeq=@PostSeq and PKey=@PKey
		else if @TableName='BILD'
			select @BillStatus=BillStatus from BILD where Period=@Period and PostSeq=@PostSeq and PKey=@PKey
		else if @TableName='LedgerAP'
			select @BillStatus=BillStatus from LedgerAP   where Period=@Period and PostSeq=@PostSeq and PKey=@PKey
		else if @TableName='LedgerEX'
			select @BillStatus=BillStatus from LedgerEX   where Period=@Period and PostSeq=@PostSeq and PKey=@PKey
		else if @TableName='LedgerMisc'
			select @BillStatus=BillStatus from LedgerMisc where Period=@Period and PostSeq=@PostSeq and PKey=@PKey
		else if @TableName='BIED'
			select @BillStatus=BillStatus from BIED       where Period=@Period and PostSeq=@PostSeq and PKey=@PKey
		else if @TableName='LedgerAR'
			select @BillStatus=BillStatus from LedgerAR   where Period=@Period and PostSeq=@PostSeq and PKey=@PKey
	end
	if exists (select 'x' from CCG_TR_Approvals where TableName=@TableName and Period=@Period and PostSeq=@PostSeq and PKey=@PKey)
		update CCG_TR_Approvals set
			SubmittedBy     = Case When @SubmittedBy is null Then SubmittedBy     Else @SubmittedBy     End,
			ModificationComment = @ModComment,
			ApprovedBy      = @ApprovedBy,
			ApprovalComment = @ApprovalComment,
			SubmittedDate   = Case When @SubmittedBy is null Then SubmittedDate   Else @SubmittedDate   End,
			ApprovedDate    = Case When @ApprovedBy  is null Then null            Else @ApprovedDate    End,
			 
			IncludeFuture=@IncludeFuture, BillNonBillable=@BillNonBillable, BillStatus=@BillStatus, FNStatus=@FNStatus
		where TableName=@TableName and Period=@Period and PostSeq=@PostSeq and PKey=@PKey
	else
		insert into CCG_TR_Approvals (TableName,Period,PostSeq,PKey, 
			SubmittedBy,SubmittedDate,ApprovedBy,ApprovedDate,ApprovalComment,IncludeFuture,BillNonBillable,BillStatus,ModificationComment,FNStatus)
		values (@TableName,@Period,@PostSeq,@PKey, 
			@SubmittedBy,@SubmittedDate,@ApprovedBy,@ApprovedDate,@ApprovalComment,@IncludeFuture,@BillNonBillable,@BillStatus,@ModComment,@FNStatus)

	
	insert into CCG_TR_ApprovalsHistory (TableName,Period,PostSeq,PKey, 
		SubmittedBy,SubmittedDate,ApprovedBy,ApprovedDate,ApprovalComment,IncludeFuture,BillNonBillable,FNStatus)
	values (@TableName,@Period,@PostSeq,@PKey, 
		@SubmittedBy,@SubmittedDate,@ApprovedBy,@ApprovedDate,@ApprovalComment,@IncludeFuture,@BillNonBillable,@FNStatus)
END
GO
