SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_CC_CheckIfViewExists] (
	@viewName		Nvarchar(500)
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- EXEC [DeltekStoredProc_CCG_PAT_CC_CheckIfViewExists] 'vwCCG_PAT_PaymentTerms'
	SELECT oname, otype, null,
            MAX(case when UPPER(name) = N'PAYABLESEQ' then system_type_id else null end),
            MAX(case when UPPER(name) = N'WBS1' then system_type_id else null end),
            MAX(case when UPPER(name) = N'WBS2' then system_type_id else null end),
            MAX(case when UPPER(name) = N'WBS3' then system_type_id else null end)
        FROM (
            SELECT o.name as oname, type as otype, c.* FROM
                sys.objects o, sys.columns c
                WHERE o.object_id = c.object_id and
                    o.object_id = OBJECT_ID(N'[' + Replace(@viewName, 'dbo.', 'dbo].[') + ']')
        ) d
        GROUP BY oname, otype;
END


GO
