SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Bracken Adams and Conrad Harrison>
-- Create date: <2018-11-30>
-- Description:	<Updates LedgerMisc.Desc1 by removing the {ref:>
-- =============================================
CREATE PROCEDURE [dbo].[HAI_LedgerMiscDescUpdate] 

AS
BEGIN

	SET NOCOUNT ON;

Update [dbo].[LedgerMisc]
SET Desc1 = 
Case when CHARINDEX('{', Desc1) = 1
	then '' 
	else LEFT(Desc1, CHARINDEX('{', Desc1)-1)
	End
WHERE Desc1 LIKE '%{ref:%'
  and BillStatus = 'B'
  and TransType = 'UN';
END
GO
