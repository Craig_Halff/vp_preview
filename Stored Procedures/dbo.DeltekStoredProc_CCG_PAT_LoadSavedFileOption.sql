SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_LoadSavedFileOption] (
	@key			varchar(50),
	@searchvalue	Nvarchar(max),
	@user			Nvarchar(32) = N''
) AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_LoadSavedFileOption] '', ''

	SET NOCOUNT ON;

	IF ISNULL(@key, '') <> ''
		SELECT *
			FROM CCG_PAT_CONFIGOPTIONS
			WHERE type = 'SavedTextFileOptions'
				and ConfigID = @key;
	ELSE IF ISNULL(@searchvalue, N'') <> N''
		SELECT *
            FROM CCG_PAT_CONFIGOPTIONS
            WHERE type = 'SavedTextFileOptions'
                and ConfigID in (
                    SELECT CONVERT(Nvarchar(50), detail)
                        FROM CCG_PAT_ConfigOptions
                        WHERE Type = 'SavedTextFileDefault'
                            and [Description] = @searchvalue
                )
	ELSE IF ISNULL(@user, N'') <> N''
		 SELECT *
            FROM CCG_PAT_CONFIGOPTIONS
            WHERE type = 'SavedTextFileOptions' or type = 'SavedTextFileOptions.' + @user;
END;

GO
