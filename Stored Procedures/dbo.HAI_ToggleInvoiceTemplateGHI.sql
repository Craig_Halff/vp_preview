SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_ToggleInvoiceTemplateGHI] @project /*N*/VARCHAR(12)
    AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        DECLARE @oldTemplate VARCHAR(25);
        DECLARE @newTemplate VARCHAR(25);
		DECLARE @newRemittanceFile VARCHAR(255);


        SET @oldTemplate = (
                               SELECT TOP (1)
                                   b.Template
                               FROM HalffImport.dbo.BT b
                               WHERE b.WBS1 = @project
                                 AND b.WBS2 = ' '
                               ORDER BY b.WBS1 )
        SET @newTemplate = IIF(@oldTemplate = 'GHI', 'HALFF_SUMMARY', 'GHI');
        SET @newRemittanceFile ='\\HALFF.ad\Offices\ACCT\Client Invoices\Templates\RemittanceChange-2020.pdf';

        BEGIN TRANSACTION
            UPDATE b
            SET b.Template = @newTemplate
            FROM HalffImport.dbo.BT b
            WHERE b.WBS1 = @project
        COMMIT TRANSACTION;

        IF @newTemplate = 'HALFF_SUMMARY'
            BEGIN
                BEGIN TRANSACTION
				DELETE
                FROM dbo.Projects_FinalInvPkgUserDocuments
                WHERE WBS1 = @project
                  AND CustDocument = @newRemittanceFile

                    INSERT INTO dbo.Projects_FinalInvPkgUserDocuments
                    ( WBS1, WBS2, WBS3, Seq, CustOrder, CustDocType, CustDocument, CreateUser, CreateDate, ModUser
                    , ModDate)
                    SELECT
                        WBS1
                      , ' ' -- WBS2
                      , ' ' -- WBS3
                      , replace(NEWID(), '-', '')
                      , 1
                      , 'File'
                      , @newRemittanceFile
                      , 'ADMIN'
                      , GETUTCDATE()
                      , 'ADMIN'
                      , GETUTCDATE()
                    FROM DataMart.dbo.ProjectJTD p
                    WHERE p.WBS1 =@project
                      AND p.ProjectStatus = 'A'
                      AND p.ChargeType = 'R'
                COMMIT TRANSACTION
            END
        ELSE
            BEGIN
                DELETE
                FROM dbo.Projects_FinalInvPkgUserDocuments
                WHERE WBS1 = @project
                  AND CustDocument = @newRemittanceFile
            END
    END
GO
