SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_DelegationUpd] (
	@Id				varchar(32),
	@Username		Nvarchar(32),
	@Employee		Nvarchar(20),
	@Delegate		Nvarchar(20),
	@FromDate		varchar(20),
	@ToDate			varchar(20),
	@ForClientId	varchar(32),
	@ForWBS1		Nvarchar(30),
	@Dual			char(1),
	@ApprovedBy		Nvarchar(20),
	@ApprovedOn		datetime
)
AS
BEGIN
/*
	Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved.

	select * from CCG_EI_Delegation
	select * from CCG_EI_HistoryDelegation
	exec spCCG_EI_DelegationUpd '1','ADMIN','00001','00003','3/1/2017','3/15/2017',0,'N',null,null,'N'
	exec spCCG_EI_DelegationUpd '1','ADMIN','00001','00003','3/1/2017','3/17/2017',0,'N','00001','3/15/2017','N'
*/
	SET NOCOUNT ON
	BEGIN TRANSACTION
	BEGIN TRY
		declare @EmployeeOrig Nvarchar(20), @DelegateOrig Nvarchar(20), @FromDateOrig datetime, @ToDateOrig datetime
		declare @DualOrig char(1), @ApprovedByOrig Nvarchar(20), @ApprovedOnOrig datetime,
				@ForClientIdOrig varchar(32), @ForWBS1Orig Nvarchar(30)

		select @EmployeeOrig=d.Employee, @DelegateOrig=d.Delegate, @FromDateOrig=FromDate, @ToDateOrig=ToDate,
				@DualOrig=Dual, @ApprovedByOrig=d.ApprovedBy, @ApprovedOnOrig=d.ApprovedOn, @ForClientIdOrig=ForClientId, @ForWBS1Orig=ForWBS1
			from CCG_EI_Delegation d
			where d.Id = @Id

		update CCG_EI_Delegation
			set Employee=@Employee,Delegate=@Delegate,FromDate=@FromDate,ToDate=@ToDate, ForClientId=ISNULL(@ForClientId,''), ForWBS1=@ForWBS1,
				Dual=@Dual, ApprovedBy=@ApprovedBy,ApprovedOn=NULLIF(@ApprovedOn, '')
			where Id=@Id

		/* if any field other than approval info changed, add an Updated record */
		if @Employee<>@EmployeeOrig or @Delegate<>@DelegateOrig or @FromDate<>@FromDateOrig or @ToDate<>@ToDateOrig
				or @Dual<>@DualOrig or @ForClientId<>@ForClientIdOrig or @ForWBS1<>@ForWBS1Orig
			insert into CCG_EI_HistoryDelegation (Id,Employee,Delegate,FromDate,ToDate, ForClientId, ForWBS1, ActionTaken,ActionDate,ActionTakenBy,ActionRecipient,Description)
				select @Id,@Employee,@Delegate,@FromDate,@ToDate, isnull(@ForClientId,''), isnull(@ForWBS1,N''), 'Updated',getutcdate(),Employee,null, N'Dual = ' + @Dual
				from SEUser where Username=@Username

		if IsNull(@ApprovedBy,N'')<>N'' and @ApprovedOn is not null and (IsNull(@ApprovedBy,N'')<>IsNull(@ApprovedByOrig,N'') or @ApprovedOn<>@ApprovedOnOrig)
		BEGIN
			insert into CCG_EI_HistoryDelegation (Id,Employee,Delegate,FromDate,ToDate, ForClientId, ForWBS1, ActionTaken,ActionDate,ActionTakenBy,ActionRecipient,Description)
				select @Id,@Employee,@Delegate,@FromDate,@ToDate, isnull(@ForClientId,''), isnull(@ForWBS1,N''), 'Approved',getutcdate(),Employee,null,N'Approver = ' + @ApprovedBy + N', Approved On = ' + Convert(Nvarchar, @ApprovedOn, 101)
				from SEUser where Username=@Username
		END

		COMMIT TRANSACTION
		select 0 as Result, '' as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		select -1 as Result, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage
	END CATCH
END
GO
