SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Get_Menu_Name]
	@VISION_LANGUAGE		varchar(10)
AS
BEGIN
	-- DEBUG TEST: EXEC [dbo].[DeltekStoredProc_CCG_EI_Get_Menu_Name] 'en-US'

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT cg.GridInstanceName, IsNull(cgd.GridInstanceLabel, cg.GridInstanceName) As GridMenuLabelInEI
		From CCG_EI_ConfigGrid cg
			Left Join CCG_GRID_ConfigInstanceDescriptions cgd
				On cgd.GridInstanceName = cg.GridInstanceName And cgd.UICultureName = @VISION_LANGUAGE
		Where cg.GridVisibleInEI = 'Y';

END;

GO
