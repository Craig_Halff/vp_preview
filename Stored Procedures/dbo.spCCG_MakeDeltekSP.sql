SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_MakeDeltekSP] (@SpName Nvarchar(255), @ArgListDefn Nvarchar(max)) /*, @ArgList varchar(255))*/
AS BEGIN
/*
       Copyright (c) 2017 EleVia Software. All rights reserved.
*/
       set nocount on
       declare @DeltekSpName Nvarchar(255) = N'DeltekStoredProc_' + Right(@SpName,Len(@SpName)-2)
       declare @ArgList Nvarchar(max)
       select @ArgList = IsNull(STUFF((SELECT ',' + Value
             from (select top 1000 Replace(Left('@'+Replace(RTrim(LTrim(Value)),',',''),CharIndex(' ','@'+Replace(RTrim(LTrim(Value)),',',''))),' ','') as Value
				from dbo.fnCCG_SplitString(@ArgListDefn, '@') where Len(RTrim(LTrim(Value)))>0
				order by position
			 ) T
             FOR XML PATH(''), TYPE
             ).value('.', 'Nvarchar(MAX)'),1,1,''),'')
       --print @ArgList

       declare @sql Nvarchar(max) = N'
             IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[' + @DeltekSpName + ']'') AND type in (N''P'', N''PC''))
             BEGIN EXEC dbo.sp_executesql @statement = N''CREATE OR ALTER PROCEDURE [dbo].[' + @DeltekSpName + '] AS'' END'
       --print @sql
       execute(@sql)

       if RTrim(@ArgListDefn)<>'' set @ArgListDefn = '(' + @ArgListDefn + ')'
       set @sql = N'
             ALTER PROCEDURE [dbo].[' + @DeltekSpName + '] ' + @ArgListDefn + '
             AS EXEC ' + @SpName + ' ' + @ArgList
       --print @sql
       execute(@sql)
END
GO
