SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_TR_ClearSubmitsApprovals](@InvoiceGroup varchar(255), @WBS1 varchar(30), @CurrentThruDate datetime)
AS BEGIN

	if IsNull(@InvoiceGroup,'')='' and IsNull(@WBS1,'')<>''
	begin
		delete a from CCG_TR_Approvals a inner join LD         l on a.TableName='LD'         and l.Period=a.Period and l.PostSeq=a.PostSeq and l.PKey=a.PKey where l.WBS1=@WBS1 and a.FNStatus is not null
		delete a from CCG_TR_Approvals a inner join BILD       l on a.TableName='BILD'       and l.Period=a.Period and l.PostSeq=a.PostSeq and l.PKey=a.PKey where l.WBS1=@WBS1 and a.FNStatus is not null
		delete a from CCG_TR_Approvals a inner join LedgerMisc l on a.TableName='LedgerMisc' and l.Period=a.Period and l.PostSeq=a.PostSeq and l.PKey=a.PKey where l.WBS1=@WBS1 and a.FNStatus is not null
		delete a from CCG_TR_Approvals a inner join LedgerEX   l on a.TableName='LedgerEX'   and l.Period=a.Period and l.PostSeq=a.PostSeq and l.PKey=a.PKey where l.WBS1=@WBS1 and a.FNStatus is not null
		delete a from CCG_TR_Approvals a inner join LedgerAP   l on a.TableName='LedgerAP'   and l.Period=a.Period and l.PostSeq=a.PostSeq and l.PKey=a.PKey where l.WBS1=@WBS1 and a.FNStatus is not null
		delete a from CCG_TR_Approvals a inner join BIED       l on a.TableName='BIED'       and l.Period=a.Period and l.PostSeq=a.PostSeq and l.PKey=a.PKey where l.WBS1=@WBS1 and a.FNStatus is not null
	end else if IsNull(@InvoiceGroup,'')<>'' begin
		delete a from CCG_TR_Approvals a inner join LD         l on a.TableName='LD'         and l.Period=a.Period and l.PostSeq=a.PostSeq and l.PKey=a.PKey 
			inner join ProjectCustomTabFields PCTF on PCTF.WBS1=l.WBS1 and PCTF.CustInvoiceGroup=@InvoiceGroup and a.FNStatus is not null
		delete a from CCG_TR_Approvals a inner join BILD       l on a.TableName='BILD'       and l.Period=a.Period and l.PostSeq=a.PostSeq and l.PKey=a.PKey 
			inner join ProjectCustomTabFields PCTF on PCTF.WBS1=l.WBS1 and PCTF.CustInvoiceGroup=@InvoiceGroup and a.FNStatus is not null
		delete a from CCG_TR_Approvals a inner join LedgerMisc l on a.TableName='LedgerMisc' and l.Period=a.Period and l.PostSeq=a.PostSeq and l.PKey=a.PKey 
			inner join ProjectCustomTabFields PCTF on PCTF.WBS1=l.WBS1 and PCTF.CustInvoiceGroup=@InvoiceGroup and a.FNStatus is not null
		delete a from CCG_TR_Approvals a inner join LedgerEX   l on a.TableName='LedgerEX'   and l.Period=a.Period and l.PostSeq=a.PostSeq and l.PKey=a.PKey 
			inner join ProjectCustomTabFields PCTF on PCTF.WBS1=l.WBS1 and PCTF.CustInvoiceGroup=@InvoiceGroup and a.FNStatus is not null
		delete a from CCG_TR_Approvals a inner join LedgerAP   l on a.TableName='LedgerAP'   and l.Period=a.Period and l.PostSeq=a.PostSeq and l.PKey=a.PKey 
			inner join ProjectCustomTabFields PCTF on PCTF.WBS1=l.WBS1 and PCTF.CustInvoiceGroup=@InvoiceGroup and a.FNStatus is not null
		delete a from CCG_TR_Approvals a inner join BIED       l on a.TableName='BIED'       and l.Period=a.Period and l.PostSeq=a.PostSeq and l.PKey=a.PKey 
			inner join ProjectCustomTabFields PCTF on PCTF.WBS1=l.WBS1 and PCTF.CustInvoiceGroup=@InvoiceGroup and a.FNStatus is not null
	end
END
GO
