SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpAlignTPD]
  @strPlanID VARCHAR(32)
AS

BEGIN -- Procedure rpAlignTPD

  -->>> This procedure must be called after all of the summary TPD have been deleted.

  SET NOCOUNT ON
  
  DECLARE @intHrDecimals int
  DECLARE @intQtyDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intRtCostDecimals int
  DECLARE @intRtBillDecimals int
  DECLARE @intLabRevDecimals int
  DECLARE @intECURevDecimals int
  
  DECLARE @intBaselineLabCount int
  DECLARE @intBaselineExpCount int
  DECLARE @intBaselineConCount int
  DECLARE @intBaselineUntCount int
  
  DECLARE @intPlannedLabCount int
  DECLARE @intPlannedExpCount int
  DECLARE @intPlannedConCount int
  DECLARE @intPlannedUntCount int
  
  DECLARE @intCompCount int
  DECLARE @intConsCount int
  DECLARE @intReimCount int
  DECLARE @intEVCount int
  
  DECLARE @strCompany Nvarchar(14)
  
  DECLARE @strUnpostedFlg varchar(1)
  DECLARE @sintGRMethod smallint
  DECLARE @sintCostRtMethod smallint
  DECLARE @sintBillingRtMethod smallint
  
  DECLARE @intCostRtTableNo int
  DECLARE @intBillingRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int
  
  DECLARE @strExpTab varchar(1)
  DECLARE @strConTab varchar(1)
  DECLARE @strUntTab varchar(1)
  DECLARE @strFeesByPeriod varchar(1)
  
  -- Declare Temp tables.
  
  DECLARE @tabCalendarInterval
    TABLE(PlanID varchar(32) COLLATE database_default,
          StartDate datetime,
          EndDate datetime,
          PeriodScale varchar(1) COLLATE database_default
          PRIMARY KEY(PlanID, StartDate, EndDate))
          
	DECLARE @tabPLabTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         AssignmentID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodHrs decimal(19,4), 
           PeriodCount smallint, 
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 
  
	DECLARE @tabPExpTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         ExpenseID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PeriodCount smallint, 
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 

	DECLARE @tabPConTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         ConsultantID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PeriodCount smallint, 
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 

	DECLARE @tabPUntTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         UnitID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodQty decimal(19,4), 
           PeriodCount smallint, 
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 
  
	DECLARE @tabCompTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodDirLabCost decimal(19,4), 
           PeriodDirLabBill decimal(19,4), 
           PeriodDirExpCost decimal(19,4), 
           PeriodDirExpBill decimal(19,4), 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4)
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 

	DECLARE @tabConsTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4)
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 

	DECLARE @tabReimTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
		   periodConCost decimal(19,4), 
		   periodConBill decimal(19,4), 
		   periodExpCost decimal(19,4), 
           periodExpBill decimal(19,4), 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4)
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 

	DECLARE @tabEVTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodPct decimal(19,4)
           PRIMARY KEY(RowID, PlanID, EndDate)) 

	DECLARE @tabBLabTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         AssignmentID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodHrs decimal(19,4), 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PeriodRev decimal(19,4), 
           PeriodCount smallint, 
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 
  
	DECLARE @tabBExpTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         ExpenseID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PeriodRev decimal(19,4), 
           PeriodCount smallint, 
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 

	DECLARE @tabBConTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         ConsultantID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PeriodRev decimal(19,4), 
           PeriodCount smallint, 
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 

	DECLARE @tabBUntTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         UnitID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodQty decimal(19,4), 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PeriodRev decimal(19,4), 
           PeriodCount smallint, 
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  
  -- Get Parameters from RPPlan
  
  SELECT
    @strCompany = Company,
    @strUnpostedFlg = UnpostedFlg,
    @sintGRMethod = GRMethod,
    @intGenResTableNo = GenResTableNo,
    @intGRBillTableNo = GRBillTableNo,
    @sintCostRtMethod = CostRtMethod,
    @sintBillingRtMethod = BillingRtMethod,
    @intCostRtTableNo = CostRtTableNo,
    @intBillingRtTableNo = BillingRtTableNo
    FROM RPPlan WHERE PlanID = @strPlanID
    
  -- Get flags to determine which features are being used.
  
  SELECT
     @strExpTab = ExpTab,
     @strConTab = ConTab,
     @strUntTab = UntTab,
     @strFeesByPeriod = FeesByPeriodFlg
     FROM CFGResourcePlanning
     WHERE CFGResourcePlanning.Company = @strCompany
   
  -- Get decimal settings.
  -- At this point, there is no way to know what was the basis used in calculation of Baseline Revenue numbers.
  -- Therefore, we decided to use the current basis to determine number of decimal digits to be used
  -- in the realignment of Baseline Revenue numbers.
  
  SELECT @intHrDecimals = HrDecimals,
         @intQtyDecimals = QtyDecimals,
         @intAmtCostDecimals = AmtCostDecimals,
         @intAmtBillDecimals = AmtBillDecimals,
         @intRtCostDecimals = RtCostDecimals,
         @intRtBillDecimals = RtBillDecimals,
         @intLabRevDecimals = LabRevDecimals, 
         @intECURevDecimals = ECURevDecimals
    FROM dbo.RP$tabDecimals(@strPlanID)
    
  -- Check to see if there is any time-phased data
  
    SELECT @intBaselineLabCount = COUNT(*) FROM RPBaselineLabor WHERE PlanID = @strPlanID
    SELECT @intPlannedLabCount = COUNT(*) FROM RPPlannedLabor WHERE PlanID = @strPlanID
    
    IF (@strExpTab = 'Y')
      BEGIN
        SELECT @intBaselineExpCount = COUNT(*) FROM RPBaselineExpenses WHERE PlanID = @strPlanID
        SELECT @intPlannedExpCount = COUNT(*) FROM RPPlannedExpenses WHERE PlanID = @strPlanID
      END
  
    IF (@strConTab = 'Y')
      BEGIN
        SELECT @intBaselineConCount = COUNT(*) FROM RPBaselineConsultant WHERE PlanID = @strPlanID
        SELECT @intPlannedConCount = COUNT(*) FROM RPPlannedConsultant WHERE PlanID = @strPlanID
      END

    IF (@strUntTab = 'Y')
      BEGIN
        SELECT @intBaselineUntCount = COUNT(*) FROM RPBaselineUnit WHERE PlanID = @strPlanID
        SELECT @intPlannedUntCount = COUNT(*) FROM RPPlannedUnit WHERE PlanID = @strPlanID
      END

    IF (@strFeesByPeriod = 'Y')
      BEGIN
        SELECT @intCompCount = COUNT(*) FROM RPCompensationFee WHERE PlanID = @strPlanID
        SELECT @intConsCount = COUNT(*) FROM RPConsultantFee WHERE PlanID = @strPlanID
        SELECT @intReimCount = COUNT(*) FROM RPReimbAllowance WHERE PlanID = @strPlanID
      END

    SELECT @intEVCount = COUNT(*) FROM RPEVT WHERE PlanID = @strPlanID

  -- Save Calendar Intervals into a temp table and add an overflow Calendar Interval.
  
  INSERT @tabCalendarInterval(PlanID, StartDate, EndDate, PeriodScale)
  SELECT PlanID, StartDate, EndDate, PeriodScale FROM
    (SELECT PlanID AS PlanID, CAST('19900101' AS datetime) AS StartDate,
			 DATEADD(d, -1, MIN(StartDate)) AS EndDate,
			 'o' AS PeriodScale
			 FROM RPCalendarInterval WHERE PlanID = @strPlanID
			 GROUP BY PlanID
		 UNION ALL
     SELECT PlanID AS PlanID, StartDate AS StartDate, EndDate AS EndDate, PeriodScale AS PeriodScale
       FROM RPCalendarInterval WHERE PlanID = @strPlanID
     UNION ALL
     SELECT PlanID AS PlanID, DATEADD(d, 1, MAX(EndDate)) AS StartDate,
       DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate,
       'o' AS PeriodScale
       FROM RPCalendarInterval WHERE PlanID = @strPlanID
       GROUP BY PlanID) AS CI
              
  IF (@intPlannedLabCount > 0)
    BEGIN
    
      -- Save Planned Labor time-phased data rows.

      INSERT @tabPLabTPD
        (RowID,
         TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         AssignmentID,
         StartDate, 
         EndDate, 
         PeriodHrs,
         PeriodCount, 
         PeriodScale)
       SELECT
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
        TimePhaseID AS TimePhaseID,
        CIStartDate AS CIStartDate,
        PlanID AS PlanID, 
        TaskID AS TaskID,
        AssignmentID AS AssignmentID,
        StartDate AS StartDate, 
        EndDate AS EndDate, 
        ROUND(ISNULL(PeriodHrs, 0), @intHrDecimals) AS PeriodHrs,
        PeriodCount AS PeriodCount, 
        PeriodScale AS PeriodScale
        FROM (SELECT -- For Childless Task Rows.
                CI.StartDate AS CIStartDate,
                TPD.TimePhaseID AS TimePhaseID,
                T.PlanID AS PlanID, 
                T.TaskID AS TaskID,
                NULL AS AssignmentID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodHrs * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                     THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                              CASE WHEN TPD.EndDate < CI.EndDate 
                                                                     THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                              TPD.StartDate, TPD.EndDate,
                                                              @strCompany)
                       ELSE PeriodHrs END AS PeriodHrs,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI 
                  LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                  INNER JOIN RPPlannedLabor AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                    AND TPD.AssignmentID IS NULL AND T.ChildrenCount = 0
                WHERE TPD.TimePhaseID IS NOT NULL
              UNION ALL
              SELECT -- For Assignment Rows.
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                A.PlanID AS PlanID, 
                A.TaskID AS TaskID,
                A.AssignmentID AS AssignmentID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodHrs * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                     THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                              CASE WHEN TPD.EndDate < CI.EndDate 
                                                                     THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                              TPD.StartDate, TPD.EndDate,
                                                              @strCompany)
                       ELSE PeriodHrs END AS PeriodHrs,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI 
                  LEFT JOIN RPAssignment AS A ON CI.PlanID = A.PlanID
                  INNER JOIN RPPlannedLabor AS TPD ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                    AND TPD.AssignmentID = A.AssignmentID
                WHERE TPD.TimePhaseID IS NOT NULL) AS X
        WHERE (PeriodHrs IS NOT NULL AND PeriodHrs != 0)
     
      -- Adjust Planned Labor time-phased data to compensate for rounding errors.
     
      UPDATE @tabPLabTPD SET PeriodHrs = (TPD.PeriodHrs + D.DeltaHrs)
        FROM @tabPLabTPD AS TPD INNER JOIN 
          (SELECT YTPD.TimePhaseID AS TimePhaseID, (YTPD.PeriodHrs - SUM(ISNULL(XTPD.PeriodHrs, 0))) AS DeltaHrs
            FROM @tabPLabTPD AS XTPD INNER JOIN RPPlannedLabor AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
            GROUP BY YTPD.TimePhaseID, YTPD.PeriodHrs) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
        WHERE RowID IN
          (SELECT RowID FROM @tabPLabTPD AS ATPD INNER JOIN
            (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabPLabTPD GROUP BY TimePhaseID) AS BTPD
                ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
                
    END -- IF (@intPlannedLabCount > 0)
    
  IF (@strExpTab = 'Y' AND @intPlannedExpCount > 0)
    BEGIN
    
      -- Save Planned Expense time-phased data rows.

      INSERT @tabPExpTPD
        (RowID,
         TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         ExpenseID,
         StartDate, 
         EndDate, 
         PeriodCost,
         PeriodBill,
         PeriodCount, 
         PeriodScale)
      SELECT 
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
        TimePhaseID AS TimePhaseID,
        CIStartDate AS CIStartDate,
        PlanID AS PlanID, 
        TaskID AS TaskID,
        ExpenseID AS ExpenseID,
        StartDate AS StartDate, 
        EndDate AS EndDate, 
        ROUND(ISNULL(PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
        ROUND(ISNULL(PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill, 
        PeriodCount AS PeriodCount, 
        PeriodScale AS PeriodScale
        FROM (SELECT 
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                T.PlanID AS PlanID, 
                T.TaskID AS TaskID,
                NULL AS ExpenseID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                      ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                      ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodCost END AS PeriodCost,
                CASE WHEN P.CalcExpBillAmtFlg = 'N'
                       THEN CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                                   THEN PeriodBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                                  THEN TPD.StartDate 
                                                                                  ELSE CI.StartDate END, 
                                                                           CASE WHEN TPD.EndDate < CI.EndDate 
                                                                                  THEN TPD.EndDate 
                                                                                  ELSE CI.EndDate END, 
                                                                           TPD.StartDate, TPD.EndDate,
                                                                           @strCompany)
                                   ELSE PeriodBill END
                       ELSE 0 END AS PeriodBill,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                  LEFT JOIN RPPlan AS P ON CI.PlanID = P.PlanID
                  INNER JOIN RPPlannedExpenses AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                    AND TPD.ExpenseID IS NULL AND T.ChildrenCount = 0
                WHERE TPD.TimePhaseID IS NOT NULL 
              UNION ALL
              SELECT 
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                E.PlanID AS PlanID, 
                E.TaskID AS TaskID,
                E.ExpenseID AS ExpenseID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                      ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                      ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodCost END AS PeriodCost,
                CASE WHEN P.CalcExpBillAmtFlg = 'N'
                       THEN CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                                   THEN PeriodBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                                  THEN TPD.StartDate 
                                                                                  ELSE CI.StartDate END, 
                                                                           CASE WHEN TPD.EndDate < CI.EndDate 
                                                                                  THEN TPD.EndDate 
                                                                                  ELSE CI.EndDate END, 
                                                                           TPD.StartDate, TPD.EndDate,
                                                                           @strCompany)
                                    ELSE PeriodBill END
                       ELSE 0 END AS PeriodBill,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN RPExpense AS E ON CI.PlanID = E.PlanID
                  LEFT JOIN RPPlan AS P ON CI.PlanID = P.PlanID
                  INNER JOIN RPPlannedExpenses AS TPD ON TPD.PlanID = E.PlanID AND TPD.TaskID = E.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                    AND TPD.ExpenseID = E.ExpenseID
                WHERE TPD.TimePhaseID IS NOT NULL) AS X
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
        
      -- Adjust Planned Expense time-phased data to compensate for rounding errors.
     
      UPDATE @tabPExpTPD SET PeriodCost = (TPD.PeriodCost + D.DeltaCost),
        PeriodBill = CASE WHEN P.CalcExpBillAmtFlg = 'N'
                            THEN (TPD.PeriodBill + D.DeltaBill)
                            ELSE 0 END
        FROM @tabPExpTPD AS TPD INNER JOIN 
          (SELECT YTPD.TimePhaseID AS TimePhaseID, 
            (YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaCost,
            (YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaBill
            FROM @tabPExpTPD AS XTPD INNER JOIN RPPlannedExpenses AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
            GROUP BY YTPD.TimePhaseID, YTPD.PeriodCost, YTPD.PeriodBill) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
          INNER JOIN RPPlan AS P ON TPD.PlanID = P.PlanID
        WHERE RowID IN
          (SELECT RowID FROM @tabPExpTPD AS ATPD INNER JOIN
            (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabPExpTPD GROUP BY TimePhaseID) AS BTPD
                ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
                
    END -- IF (@strExpTab = 'Y' AND @intPlannedExpCount > 0)

  IF (@strConTab = 'Y' AND @intPlannedConCount > 0)
    BEGIN
    
      -- Save Planned Consultant time-phased data rows.

      INSERT @tabPConTPD
        (RowID,
         TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         ConsultantID,
         StartDate, 
         EndDate, 
         PeriodCost,
         PeriodBill,
         PeriodCount, 
         PeriodScale)
      SELECT 
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
        TimePhaseID AS TimePhaseID,
        CIStartDate AS CIStartDate,
        PlanID AS PlanID, 
        TaskID AS TaskID,
        ConsultantID AS ConsultantID,
        StartDate AS StartDate, 
        EndDate AS EndDate, 
        ROUND(ISNULL(PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
        ROUND(ISNULL(PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill, 
        PeriodCount AS PeriodCount, 
        PeriodScale AS PeriodScale
        FROM (SELECT 
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                T.PlanID AS PlanID, 
                T.TaskID AS TaskID,
                NULL AS ConsultantID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                      ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                      ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodCost END AS PeriodCost,
                CASE WHEN P.CalcConBillAmtFlg = 'N'
                       THEN CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                                   THEN PeriodBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                                  THEN TPD.StartDate 
                                                                                  ELSE CI.StartDate END, 
                                                                           CASE WHEN TPD.EndDate < CI.EndDate 
                                                                                  THEN TPD.EndDate 
                                                                                  ELSE CI.EndDate END, 
                                                                           TPD.StartDate, TPD.EndDate,
                                                                           @strCompany)
                                   ELSE PeriodBill END
                       ELSE 0 END AS PeriodBill,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                  LEFT JOIN RPPlan AS P ON CI.PlanID = P.PlanID
                  INNER JOIN RPPlannedConsultant AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                    AND TPD.ConsultantID IS NULL AND T.ChildrenCount = 0
                WHERE TPD.TimePhaseID IS NOT NULL 
              UNION ALL
              SELECT 
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                C.PlanID AS PlanID, 
                C.TaskID AS TaskID,
                C.ConsultantID AS ConsultantID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                      ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                      ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodCost END AS PeriodCost,
                CASE WHEN P.CalcConBillAmtFlg = 'N'
                       THEN CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                                   THEN PeriodBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                                  THEN TPD.StartDate 
                                                                                  ELSE CI.StartDate END, 
                                                                           CASE WHEN TPD.EndDate < CI.EndDate 
                                                                                  THEN TPD.EndDate 
                                                                                  ELSE CI.EndDate END, 
                                                                           TPD.StartDate, TPD.EndDate,
                                                                           @strCompany)
                                   ELSE PeriodBill END
                       ELSE 0 END AS PeriodBill,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN RPConsultant AS C ON CI.PlanID = C.PlanID
                  LEFT JOIN RPPlan AS P ON CI.PlanID = P.PlanID
                  INNER JOIN RPPlannedConsultant AS TPD ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                    AND TPD.ConsultantID = C.ConsultantID
                WHERE TPD.TimePhaseID IS NOT NULL) AS X
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
            
      -- Adjust Planned Consultant time-phased data to compensate for rounding errors.
     
      UPDATE @tabPConTPD SET PeriodCost = (TPD.PeriodCost + D.DeltaCost),
        PeriodBill = CASE WHEN P.CalcConBillAmtFlg = 'N'
                            THEN (TPD.PeriodBill + D.DeltaBill)
                            ELSE 0 END
        FROM @tabPConTPD AS TPD INNER JOIN 
          (SELECT YTPD.TimePhaseID AS TimePhaseID, 
            (YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaCost,
            (YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaBill
            FROM @tabPConTPD AS XTPD INNER JOIN RPPlannedConsultant AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
            GROUP BY YTPD.TimePhaseID, YTPD.PeriodCost, YTPD.PeriodBill) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
          INNER JOIN RPPlan AS P ON TPD.PlanID = P.PlanID
        WHERE RowID IN
          (SELECT RowID FROM @tabPConTPD AS ATPD INNER JOIN
            (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabPConTPD GROUP BY TimePhaseID) AS BTPD
                ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
                
    END -- IF (@strConTab = 'Y' AND @intPlannedConCount > 0)
            
  IF (@strUntTab = 'Y' AND @intPlannedUntCount > 0)
    BEGIN
    
      -- Save Planned Unit time-phased data rows.

      INSERT @tabPUntTPD
        (RowID,
         TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         UnitID,
         StartDate, 
         EndDate, 
         PeriodQty,
         PeriodCount, 
         PeriodScale)
      SELECT
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
        TimePhaseID AS TimePhaseID,
        CIStartDate AS CIStartDate,
        PlanID AS PlanID, 
        TaskID AS TaskID,
        UnitID AS UnitID,
        StartDate AS StartDate, 
        EndDate AS EndDate, 
        ROUND(ISNULL(PeriodQty, 0), @intQtyDecimals) AS PeriodQty, 
        PeriodCount AS PeriodCount, 
        PeriodScale AS PeriodScale
        FROM (SELECT 
                CI.StartDate AS CIStartDate,
                TPD.TimePhaseID AS TimePhaseID,
                T.PlanID AS PlanID, 
                T.TaskID AS TaskID,
                NULL AS UnitID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodQty * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                     THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                              CASE WHEN TPD.EndDate < CI.EndDate 
                                                                     THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                              TPD.StartDate, TPD.EndDate,
                                                              @strCompany)
                       ELSE PeriodQty END AS PeriodQty,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                  INNER JOIN RPPlannedUnit AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                    AND TPD.UnitID IS NULL AND T.ChildrenCount = 0
                WHERE TPD.TimePhaseID IS NOT NULL
              UNION ALL
              SELECT 
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                U.PlanID AS PlanID, 
                U.TaskID AS TaskID,
                U.UnitID AS UnitID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodQty * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                     THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                              CASE WHEN TPD.EndDate < CI.EndDate 
                                                                     THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                              TPD.StartDate, TPD.EndDate,
                                                              @strCompany)
                       ELSE PeriodQty END AS PeriodQty,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN RPUnit AS U ON CI.PlanID = U.PlanID
                  INNER JOIN RPPlannedUnit AS TPD ON TPD.PlanID = U.PlanID AND TPD.TaskID = U.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                    AND TPD.UnitID = U.UnitID
                WHERE TPD.TimePhaseID IS NOT NULL) AS X
        WHERE (PeriodQty IS NOT NULL AND PeriodQty != 0)
     
      -- Adjust Planned Unit time-phased data to compensate for rounding errors.
     
      UPDATE @tabPUntTPD SET PeriodQty = (TPD.PeriodQty + D.DeltaQty)
        FROM @tabPUntTPD AS TPD INNER JOIN 
          (SELECT YTPD.TimePhaseID AS TimePhaseID, (YTPD.PeriodQty - SUM(ISNULL(XTPD.PeriodQty, 0))) AS DeltaQty
            FROM @tabPUntTPD AS XTPD INNER JOIN RPPlannedUnit AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
            GROUP BY YTPD.TimePhaseID, YTPD.PeriodQty) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
        WHERE RowID IN
          (SELECT RowID FROM @tabPUntTPD AS ATPD INNER JOIN
            (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabPUntTPD GROUP BY TimePhaseID) AS BTPD
                ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)        
  
    END -- IF (@strUntTab = 'Y' AND @intPlannedUntCount > 0)

  IF (@strFeesByPeriod = 'Y')
    BEGIN

      IF (@intCompCount > 0)
        BEGIN
        
          -- Save Compensation Fee time-phased data rows.

          INSERT @tabCompTPD
            (RowID,
             TimePhaseID,
             CIStartDate,
             PlanID, 
             TaskID,
             StartDate, 
             EndDate, 
             PeriodDirLabCost, 
             PeriodDirLabBill,   
             PeriodDirExpCost, 
             PeriodDirExpBill, 
             PeriodCost,
             PeriodBill)
          SELECT 
            REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
            TimePhaseID AS TimePhaseID,
            CIStartDate AS CIStartDate,
            PlanID AS PlanID, 
            TaskID AS TaskID,
            StartDate AS StartDate, 
            EndDate AS EndDate, 
            ROUND(ISNULL(PeriodDirLabCost, 0), @intAmtCostDecimals) AS PeriodDirLabCost, 
            ROUND(ISNULL(PeriodDirLabBill, 0), @intAmtCostDecimals) AS PeriodDirLabBill, 
            ROUND(ISNULL(PeriodDirExpCost, 0), @intAmtCostDecimals) AS PeriodDirExpCost, 
            ROUND(ISNULL(PeriodDirExpBill, 0), @intAmtCostDecimals) AS PeriodDirExpBill, 
            ROUND( ISNULL(PeriodDirLabCost, 0) + ISNULL(PeriodDirExpCost, 0) , @intAmtCostDecimals) AS PeriodCost, 
            ROUND( ISNULL(PeriodDirLabBill, 0) + ISNULL(PeriodDirExpBill, 0) , @intAmtBillDecimals) AS PeriodBill
            FROM (SELECT 
                    CI.StartDate AS CIStartDate, 
                    TPD.TimePhaseID AS TimePhaseID,
                    T.PlanID AS PlanID, 
                    T.TaskID AS TaskID,
                    CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                    CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,

					CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                           THEN PeriodDirLabCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                          THEN TPD.StartDate 
                                                                          ELSE CI.StartDate END, 
                                                                   CASE WHEN TPD.EndDate < CI.EndDate 
                                                                          THEN TPD.EndDate 
                                                                          ELSE CI.EndDate END, 
                                                                   TPD.StartDate, TPD.EndDate,
                                                                   @strCompany)
                           ELSE PeriodDirLabCost END AS PeriodDirLabCost,

				    CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                           THEN PeriodDirLabBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                          THEN TPD.StartDate 
                                                                          ELSE CI.StartDate END, 
                                                                   CASE WHEN TPD.EndDate < CI.EndDate 
                                                                          THEN TPD.EndDate 
                                                                          ELSE CI.EndDate END, 
                                                                   TPD.StartDate, TPD.EndDate,
                                                                   @strCompany)
                           ELSE PeriodDirLabBill END AS PeriodDirLabBill,

					CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                           THEN PeriodDirExpCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                          THEN TPD.StartDate 
                                                                          ELSE CI.StartDate END, 
                                                                   CASE WHEN TPD.EndDate < CI.EndDate 
                                                                          THEN TPD.EndDate 
                                                                          ELSE CI.EndDate END, 
                                                                   TPD.StartDate, TPD.EndDate,
                                                                   @strCompany)
                           ELSE PeriodDirExpCost END AS PeriodDirExpCost,

				    CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                           THEN PeriodDirExpBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                          THEN TPD.StartDate 
                                                                          ELSE CI.StartDate END, 
                                                                   CASE WHEN TPD.EndDate < CI.EndDate 
                                                                          THEN TPD.EndDate 
                                                                          ELSE CI.EndDate END, 
                                                                   TPD.StartDate, TPD.EndDate,
                                                                   @strCompany)
                           ELSE PeriodDirExpBill END AS PeriodDirExpBill
                    FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                      INNER JOIN RPCompensationFee AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                        AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                        AND T.ChildrenCount = 0
                    WHERE TPD.TimePhaseID IS NOT NULL) AS X
            WHERE ((PeriodDirLabCost IS NOT NULL AND PeriodDirLabCost != 0) OR (PeriodDirLabBill IS NOT NULL AND PeriodDirLabBill != 0)
			       OR (PeriodDirExpCost IS NOT NULL AND PeriodDirExpCost != 0) OR (PeriodDirExpBill IS NOT NULL AND PeriodDirExpBill != 0))
            
          -- Adjust Compensation Fee time-phased data to compensate for rounding errors.
         
          UPDATE @tabCompTPD SET 
		    PeriodDirLabCost = (TPD.PeriodDirLabCost + D.DeltaDirLabCost),
		    PeriodDirLabBill = (TPD.PeriodDirLabBill + D.DeltaDirLabBill),
		    PeriodDirExpCost = (TPD.PeriodDirExpCost + D.DeltaDirExpCost),
		    PeriodDirExpBill = (TPD.PeriodDirExpBill + D.DeltaDirExpBill),		   		   
		    PeriodCost = (TPD.PeriodDirLabCost + D.DeltaDirLabCost) +  (TPD.PeriodDirExpCost + D.DeltaDirExpCost),
            PeriodBill = (TPD.PeriodDirLabBill + D.DeltaDirLabBill) + (TPD.PeriodDirExpBill + D.DeltaDirExpBill)
            FROM @tabCompTPD AS TPD INNER JOIN 
              (SELECT YTPD.TimePhaseID AS TimePhaseID, 
                (YTPD.PeriodDirLabCost - SUM(ISNULL(XTPD.PeriodDirLabCost, 0))) AS DeltaDirLabCost,
                (YTPD.PeriodDirLabBill - SUM(ISNULL(XTPD.PeriodDirLabBill, 0))) AS DeltaDirLabBill,
                (YTPD.PeriodDirExpCost - SUM(ISNULL(XTPD.PeriodDirExpCost, 0))) AS DeltaDirExpCost,
                (YTPD.PeriodDirExpBill - SUM(ISNULL(XTPD.PeriodDirExpBill, 0))) AS DeltaDirExpBill
                FROM @tabCompTPD AS XTPD INNER JOIN RPCompensationFee AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
                GROUP BY YTPD.TimePhaseID, YTPD.PeriodDirLabCost, YTPD.PeriodDirLabBill, YTPD.PeriodDirExpCost,  YTPD.PeriodDirExpBill) AS D
              ON TPD.TimePhaseID = D.TimePhaseID
            WHERE RowID IN
              (SELECT RowID FROM @tabCompTPD AS ATPD INNER JOIN
                (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabCompTPD GROUP BY TimePhaseID) AS BTPD
                    ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
     
        END -- IF (@intCompCount > 0)

      IF (@intConsCount > 0)
        BEGIN
        
          -- Save Consultant Fee time-phased data rows.

          INSERT @tabConsTPD
            (RowID,
             TimePhaseID,
             CIStartDate,
             PlanID, 
             TaskID,
             StartDate, 
             EndDate, 
             PeriodCost,
             PeriodBill)
          SELECT 
            REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
            TimePhaseID AS TimePhaseID,
            CIStartDate AS CIStartDate,
            PlanID AS PlanID, 
            TaskID AS TaskID,
            StartDate AS StartDate, 
            EndDate AS EndDate, 
            ROUND(ISNULL(PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
            ROUND(ISNULL(PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill
            FROM (SELECT 
                    CI.StartDate AS CIStartDate, 
                    TPD.TimePhaseID AS TimePhaseID,
                    T.PlanID AS PlanID, 
                    T.TaskID AS TaskID,
                    CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                    CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                    CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                           THEN PeriodCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                          THEN TPD.StartDate 
                                                                          ELSE CI.StartDate END, 
                                                                   CASE WHEN TPD.EndDate < CI.EndDate 
                                                                          THEN TPD.EndDate 
                                                                          ELSE CI.EndDate END, 
                                                                   TPD.StartDate, TPD.EndDate,
                                                                   @strCompany)
                           ELSE PeriodCost END AS PeriodCost,
                    CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                           THEN PeriodBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                          THEN TPD.StartDate 
                                                                          ELSE CI.StartDate END, 
                                                                   CASE WHEN TPD.EndDate < CI.EndDate 
                                                                          THEN TPD.EndDate 
                                                                          ELSE CI.EndDate END, 
                                                                   TPD.StartDate, TPD.EndDate,
                                                                   @strCompany)
                           ELSE PeriodBill END AS PeriodBill
                    FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                      INNER JOIN RPConsultantFee AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                        AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                        AND T.ChildrenCount = 0
                    WHERE TPD.TimePhaseID IS NOT NULL) AS X
            WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
            
          -- Adjust Consultant Fee time-phased data to compensate for rounding errors.
         
          UPDATE @tabConsTPD SET PeriodCost = (TPD.PeriodCost + D.DeltaCost),
            PeriodBill = (TPD.PeriodBill + D.DeltaBill)
            FROM @tabConsTPD AS TPD INNER JOIN 
              (SELECT YTPD.TimePhaseID AS TimePhaseID, 
                (YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaCost,
                (YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaBill
                FROM @tabConsTPD AS XTPD INNER JOIN RPConsultantFee AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
                GROUP BY YTPD.TimePhaseID, YTPD.PeriodCost, YTPD.PeriodBill) AS D
              ON TPD.TimePhaseID = D.TimePhaseID
            WHERE RowID IN
              (SELECT RowID FROM @tabConsTPD AS ATPD INNER JOIN
                (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabConsTPD GROUP BY TimePhaseID) AS BTPD
                    ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
                    
        END -- IF (@intConsCount > 0)
                
      IF (@intReimCount > 0)
        BEGIN
        
          -- Save Reimbursable Allowance time-phased data rows.

          INSERT @tabReimTPD
            (RowID,
             TimePhaseID,
             CIStartDate,
             PlanID, 
             TaskID,
             StartDate, 
             EndDate, 
		     periodConCost,  
		     periodConBill, 
		     periodExpCost , 
             periodExpBill , 
             PeriodCost,
             PeriodBill)
          SELECT 
            REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
            TimePhaseID AS TimePhaseID,
            CIStartDate AS CIStartDate,
            PlanID AS PlanID, 
            TaskID AS TaskID,
            StartDate AS StartDate, 
            EndDate AS EndDate, 
            ROUND(ISNULL(periodConCost, 0), @intAmtCostDecimals) AS periodConCost, 
            ROUND(ISNULL(periodConBill, 0), @intAmtBillDecimals) AS periodConBill,
            ROUND(ISNULL(periodExpCost, 0), @intAmtCostDecimals) AS periodExpCost, 
            ROUND(ISNULL(periodExpBill, 0), @intAmtBillDecimals) AS periodExpBill,
            ROUND(ISNULL(periodConCost, 0) + ISNULL(periodExpCost, 0), @intAmtCostDecimals) AS PeriodCost, 
            ROUND(ISNULL(periodConBill, 0) + ISNULL(periodExpBill, 0), @intAmtBillDecimals) AS PeriodBill
            FROM (SELECT 
                    CI.StartDate AS CIStartDate, 
                    TPD.TimePhaseID AS TimePhaseID,
                    T.PlanID AS PlanID, 
                    T.TaskID AS TaskID,
                    CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                    CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                    CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                           THEN periodConCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                          THEN TPD.StartDate 
                                                                          ELSE CI.StartDate END, 
                                                                   CASE WHEN TPD.EndDate < CI.EndDate 
                                                                          THEN TPD.EndDate 
                                                                          ELSE CI.EndDate END, 
                                                                   TPD.StartDate, TPD.EndDate,
                                                                   @strCompany)
                           ELSE periodConCost END AS periodConCost,
                    CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                           THEN periodConBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                          THEN TPD.StartDate 
                                                                          ELSE CI.StartDate END, 
                                                                   CASE WHEN TPD.EndDate < CI.EndDate 
                                                                          THEN TPD.EndDate 
                                                                          ELSE CI.EndDate END, 
                                                                   TPD.StartDate, TPD.EndDate,
                                                                   @strCompany)
                           ELSE periodConBill END AS periodConBill,
                    CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                           THEN periodExpCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                          THEN TPD.StartDate 
                                                                          ELSE CI.StartDate END, 
                                                                   CASE WHEN TPD.EndDate < CI.EndDate 
                                                                          THEN TPD.EndDate 
                                                                          ELSE CI.EndDate END, 
                                                                   TPD.StartDate, TPD.EndDate,
                                                                   @strCompany)
                           ELSE periodExpCost END AS periodExpCost,
                    CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                           THEN periodExpBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                          THEN TPD.StartDate 
                                                                          ELSE CI.StartDate END, 
                                                                   CASE WHEN TPD.EndDate < CI.EndDate 
                                                                          THEN TPD.EndDate 
                                                                          ELSE CI.EndDate END, 
                                                                   TPD.StartDate, TPD.EndDate,
                                                                   @strCompany)
                           ELSE periodExpBill END AS periodExpBill
                    FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                      INNER JOIN RPReimbAllowance AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                        AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                        AND T.ChildrenCount = 0
                    WHERE TPD.TimePhaseID IS NOT NULL) AS X
            WHERE ((periodConCost IS NOT NULL AND periodConCost != 0) OR (periodConBill IS NOT NULL AND periodConBill != 0)
			        OR (periodExpCost IS NOT NULL AND periodExpCost != 0) OR (periodExpBill IS NOT NULL AND periodExpBill != 0) )
            
          -- Adjust Reimbursable Allowance time-phased data to compensate for rounding errors.
         
          UPDATE @tabReimTPD SET 
		    periodConCost = (TPD.periodConCost + D.DeltaConCost),
            periodConBill = (TPD.periodConBill + D.DeltaConBill),
			periodExpCost = (TPD.periodExpCost + D.DeltaExpCost),
            periodExpBill = (TPD.periodExpBill + D.DeltaExpBill),
		    PeriodCost = (TPD.periodConCost + D.DeltaConCost) + (TPD.periodExpCost + D.DeltaExpCost),
            PeriodBill = (TPD.periodConBill + D.DeltaConBill) +  (TPD.periodExpBill + D.DeltaExpBill)
            FROM @tabReimTPD AS TPD INNER JOIN 
              (SELECT YTPD.TimePhaseID AS TimePhaseID, 
                (YTPD.periodConCost - SUM(ISNULL(XTPD.periodConCost, 0))) AS DeltaConCost,
                (YTPD.periodConBill - SUM(ISNULL(XTPD.periodConBill, 0))) AS DeltaConBill,
                (YTPD.periodExpCost - SUM(ISNULL(XTPD.periodExpCost, 0))) AS DeltaExpCost,
                (YTPD.periodExpBill - SUM(ISNULL(XTPD.periodExpBill, 0))) AS DeltaExpBill
                FROM @tabReimTPD AS XTPD INNER JOIN RPReimbAllowance AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
                GROUP BY YTPD.TimePhaseID, YTPD.periodConCost, YTPD.periodConBill, YTPD.periodExpCost, YTPD.periodExpBill) AS D
              ON TPD.TimePhaseID = D.TimePhaseID
            WHERE RowID IN
              (SELECT RowID FROM @tabReimTPD AS ATPD INNER JOIN
                (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabReimTPD GROUP BY TimePhaseID) AS BTPD
                    ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
                    
        END -- IF (@intReimCount > 0)
                
    END -- IF (@strFeesByPeriod = 'Y')
            
  IF (@intEVCount > 0)
    BEGIN
    
      -- Save EV Percent time-phased data rows.
      -- There should be only one Task row that has EVPct children.

      INSERT @tabEVTPD
        (RowID,
         CIStartDate,
         PlanID, 
         TaskID,
         StartDate, 
         EndDate, 
         PeriodPct)
      SELECT 
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
        CIStartDate AS CIStartDate,
        PlanID AS PlanID, 
        TaskID AS TaskID,
        StartDate AS StartDate, 
        EndDate AS EndDate, 
        ROUND(ISNULL(PeriodPct, 0), 2) AS PeriodPct
        FROM (SELECT 
                CI.StartDate AS CIStartDate, 
                T.PlanID AS PlanID, 
                T.TaskID AS TaskID,
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                PeriodPct AS PeriodPct
                FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                  INNER JOIN RPEVT AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                WHERE TPD.TimePhaseID IS NOT NULL) AS X
                
    END -- IF (@intEVCount > 0)
    
  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  
  IF (@intBaselineLabCount > 0)
    BEGIN
    
      -- Save Baseline Labor time-phased data rows.

      INSERT @tabBLabTPD
        (RowID,
         TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         AssignmentID,
         StartDate, 
         EndDate, 
         PeriodHrs,
         PeriodCost,
         PeriodBill,
         PeriodRev,
         PeriodCount, 
         PeriodScale)
      SELECT 
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
        TimePhaseID AS TimePhaseID,
        CIStartDate AS CIStartDate,
        PlanID AS PlanID, 
        TaskID AS TaskID,
        AssignmentID AS AssignmentID,
        StartDate AS StartDate, 
        EndDate AS EndDate, 
        ROUND(ISNULL(PeriodHrs, 0), @intHrDecimals) AS PeriodHrs, 
        ROUND(ISNULL(PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
        ROUND(ISNULL(PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill, 
        ROUND(ISNULL(PeriodRev, 0), @intLabRevDecimals) AS PeriodRev, 
        PeriodCount AS PeriodCount, 
        PeriodScale AS PeriodScale
        FROM (SELECT 
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                T.PlanID AS PlanID, 
                T.TaskID AS TaskID,
                TPD.AssignmentID AS AssignmentID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodHrs * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                     THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                              CASE WHEN TPD.EndDate < CI.EndDate 
                                                                     THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                              TPD.StartDate, TPD.EndDate,
                                                              @strCompany)
                       ELSE PeriodHrs END AS PeriodHrs,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                      ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodCost END AS PeriodCost,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                      ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                      ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodBill END AS PeriodBill,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodRev * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                     THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                              CASE WHEN TPD.EndDate < CI.EndDate 
                                                                     THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                              TPD.StartDate, TPD.EndDate,
                                                              @strCompany)
                       ELSE PeriodRev END AS PeriodRev,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                  INNER JOIN RPBaselineLabor AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate) AS X
        WHERE (PeriodHrs IS NOT NULL AND PeriodHrs != 0)
           
      -- Adjust Baseline Labor time-phased data to compensate for rounding errors.
     
      UPDATE @tabBLabTPD SET PeriodHrs = (TPD.PeriodHrs + D.DeltaHrs),
        PeriodCost = (TPD.PeriodCost + D.DeltaCost),
        PeriodBill = (TPD.PeriodBill + D.DeltaBill),
        PeriodRev = (TPD.PeriodRev + D.DeltaRev)
        FROM @tabBLabTPD AS TPD INNER JOIN 
          (SELECT YTPD.TimePhaseID AS TimePhaseID, 
            (YTPD.PeriodHrs - SUM(ISNULL(XTPD.PeriodHrs, 0))) AS DeltaHrs,
            (YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaCost,
            (YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaBill,
            (YTPD.PeriodRev - SUM(ISNULL(XTPD.PeriodRev, 0))) AS DeltaRev
            FROM @tabBLabTPD AS XTPD INNER JOIN RPBaselineLabor AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
            GROUP BY YTPD.TimePhaseID, YTPD.PeriodHrs, YTPD.PeriodCost, YTPD.PeriodBill, YTPD.PeriodRev) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
        WHERE RowID IN
          (SELECT RowID FROM @tabBLabTPD AS ATPD INNER JOIN
            (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabBLabTPD GROUP BY TimePhaseID) AS BTPD
                ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)

    END -- IF (@intBaselineLabCount > 0)

  IF (@strExpTab = 'Y' AND @intBaselineExpCount > 0)
    BEGIN
    
      -- Save Baseline Expense time-phased data rows.

      INSERT @tabBExpTPD
        (RowID,
         TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         ExpenseID,
         StartDate, 
         EndDate, 
         PeriodCost,
         PeriodBill,
         PeriodRev,
         PeriodCount, 
         PeriodScale)
      SELECT 
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
        TimePhaseID AS TimePhaseID,
        CIStartDate AS CIStartDate,
        PlanID AS PlanID, 
        TaskID AS TaskID,
        ExpenseID AS ExpenseID,
        StartDate AS StartDate, 
        EndDate AS EndDate, 
        ROUND(ISNULL(PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
        ROUND(ISNULL(PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill, 
        ROUND(ISNULL(PeriodRev, 0), @intECURevDecimals) AS PeriodRev, 
        PeriodCount AS PeriodCount, 
        PeriodScale AS PeriodScale
        FROM (SELECT 
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                T.PlanID AS PlanID, 
                T.TaskID AS TaskID,
                TPD.ExpenseID AS ExpenseID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                      ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                      ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodCost END AS PeriodCost,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                      ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodBill END AS PeriodBill,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodRev * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                     THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                              CASE WHEN TPD.EndDate < CI.EndDate 
                                                                     THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                              TPD.StartDate, TPD.EndDate,
                                                              @strCompany)
                       ELSE PeriodRev END AS PeriodRev,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                  INNER JOIN RPBaselineExpenses AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate) AS X
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))

      -- Adjust Baseline Expense time-phased data to compensate for rounding errors.
     
      UPDATE @tabBExpTPD SET 
        PeriodCost = (TPD.PeriodCost + D.DeltaCost),
        PeriodBill = (TPD.PeriodBill + D.DeltaBill),
        PeriodRev = (TPD.PeriodRev + D.DeltaRev)
        FROM @tabBExpTPD AS TPD INNER JOIN 
          (SELECT YTPD.TimePhaseID AS TimePhaseID, 
            (YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaCost,
            (YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaBill,
            (YTPD.PeriodRev - SUM(ISNULL(XTPD.PeriodRev, 0))) AS DeltaRev
            FROM @tabBExpTPD AS XTPD INNER JOIN RPBaselineExpenses AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
            GROUP BY YTPD.TimePhaseID, YTPD.PeriodCost, YTPD.PeriodBill, YTPD.PeriodRev) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
        WHERE RowID IN
          (SELECT RowID FROM @tabBExpTPD AS ATPD INNER JOIN
            (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabBExpTPD GROUP BY TimePhaseID) AS BTPD
                ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)

    END -- IF (@strExpTab = 'Y' AND @intBaselineExpCount > 0)

  IF (@strConTab = 'Y' AND @intBaselineConCount > 0)
    BEGIN
    
      -- Save Baseline Consultant time-phased data rows.

      INSERT @tabBConTPD
        (RowID,
         TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         ConsultantID,
         StartDate, 
         EndDate, 
         PeriodCost,
         PeriodBill,
         PeriodRev,
         PeriodCount, 
         PeriodScale)
      SELECT 
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
        TimePhaseID AS TimePhaseID,
        CIStartDate AS CIStartDate,
        PlanID AS PlanID, 
        TaskID AS TaskID,
        ConsultantID AS ConsultantID,
        StartDate AS StartDate, 
        EndDate AS EndDate, 
        ROUND(ISNULL(PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
        ROUND(ISNULL(PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill, 
        ROUND(ISNULL(PeriodRev, 0), @intECURevDecimals) AS PeriodRev, 
        PeriodCount AS PeriodCount, 
        PeriodScale AS PeriodScale
        FROM (SELECT 
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                T.PlanID AS PlanID, 
                T.TaskID AS TaskID,
                TPD.ConsultantID AS ConsultantID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                      ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                      ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodCost END AS PeriodCost,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                      ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                      ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodBill END AS PeriodBill,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodRev * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                     THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                              CASE WHEN TPD.EndDate < CI.EndDate 
                                                                     THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                              TPD.StartDate, TPD.EndDate,
                                                              @strCompany)
                       ELSE PeriodRev END AS PeriodRev,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                  INNER JOIN RPBaselineConsultant AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate) AS X
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))

      -- Adjust Baseline Consultant time-phased data to compensate for rounding errors.
     
      UPDATE @tabBConTPD SET 
        PeriodCost = (TPD.PeriodCost + D.DeltaCost),
        PeriodBill = (TPD.PeriodBill + D.DeltaBill),
        PeriodRev = (TPD.PeriodRev + D.DeltaRev)
        FROM @tabBConTPD AS TPD INNER JOIN 
          (SELECT YTPD.TimePhaseID AS TimePhaseID, 
            (YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaCost,
            (YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaBill,
            (YTPD.PeriodRev - SUM(ISNULL(XTPD.PeriodRev, 0))) AS DeltaRev
            FROM @tabBConTPD AS XTPD INNER JOIN RPBaselineConsultant AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
            GROUP BY YTPD.TimePhaseID, YTPD.PeriodCost, YTPD.PeriodBill, YTPD.PeriodRev) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
        WHERE RowID IN
          (SELECT RowID FROM @tabBConTPD AS ATPD INNER JOIN
            (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabBConTPD GROUP BY TimePhaseID) AS BTPD
                ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
                
    END -- IF (@strConTab = 'Y' AND @intBaselineConCount > 0)

  IF (@strUntTab = 'Y' AND @intBaselineUntCount > 0)
    BEGIN
            
      -- Save Baseline Unit time-phased data rows.

      INSERT @tabBUntTPD
        (RowID,
         TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         UnitID,
         StartDate, 
         EndDate, 
         PeriodQty,
         PeriodCost,
         PeriodBill,
         PeriodRev,
         PeriodCount, 
         PeriodScale)
      SELECT 
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
        TimePhaseID AS TimePhaseID,
        CIStartDate AS CIStartDate,
        PlanID AS PlanID, 
        TaskID AS TaskID,
        UnitID AS UnitID,
        StartDate AS StartDate, 
        EndDate AS EndDate, 
        ROUND(ISNULL(PeriodQty, 0), @intQtyDecimals) AS PeriodQty, 
        ROUND(ISNULL(PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
        ROUND(ISNULL(PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill, 
        ROUND(ISNULL(PeriodRev, 0), @intECURevDecimals) AS PeriodRev, 
        PeriodCount AS PeriodCount, 
        PeriodScale AS PeriodScale
        FROM (SELECT 
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                T.PlanID AS PlanID, 
                T.TaskID AS TaskID,
                TPD.UnitID AS UnitID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodQty * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                     THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                              CASE WHEN TPD.EndDate < CI.EndDate 
                                                                     THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                              TPD.StartDate, TPD.EndDate,
                                                              @strCompany)
                       ELSE PeriodQty END AS PeriodQty,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                      ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                      ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodCost END AS PeriodCost,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                      ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                      ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodBill END AS PeriodBill,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodRev * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                     THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                              CASE WHEN TPD.EndDate < CI.EndDate 
                                                                     THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                              TPD.StartDate, TPD.EndDate,
                                                              @strCompany)
                       ELSE PeriodRev END AS PeriodRev,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                  INNER JOIN RPBaselineUnit AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate) AS X
        WHERE (PeriodQty IS NOT NULL AND PeriodQty != 0)
           
      -- Adjust Baseline Unit time-phased data to compensate for rounding errors.
     
      UPDATE @tabBUntTPD SET PeriodQty = (TPD.PeriodQty + D.DeltaQty),
        PeriodCost = (TPD.PeriodCost + D.DeltaCost),
        PeriodBill = (TPD.PeriodBill + D.DeltaBill),
        PeriodRev = (TPD.PeriodRev + D.DeltaRev)
        FROM @tabBUntTPD AS TPD INNER JOIN 
          (SELECT YTPD.TimePhaseID AS TimePhaseID, 
            (YTPD.PeriodQty - SUM(ISNULL(XTPD.PeriodQty, 0))) AS DeltaQty,
            (YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaCost,
            (YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaBill,
            (YTPD.PeriodRev - SUM(ISNULL(XTPD.PeriodRev, 0))) AS DeltaRev
            FROM @tabBUntTPD AS XTPD INNER JOIN RPBaselineUnit AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
            GROUP BY YTPD.TimePhaseID, YTPD.PeriodQty, YTPD.PeriodCost, YTPD.PeriodBill, YTPD.PeriodRev) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
        WHERE RowID IN
          (SELECT RowID FROM @tabBUntTPD AS ATPD INNER JOIN
            (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabBUntTPD GROUP BY TimePhaseID) AS BTPD
                ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)            

    END -- IF (@strUntTab = 'Y' AND @intBaselineUntCount > 0)

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Delete all time-phased data.
  
  IF (@intBaselineLabCount > 0) DELETE RPBaselineLabor WHERE PlanID = @strPlanID
  IF (@strExpTab = 'Y' AND @intBaselineExpCount > 0) DELETE RPBaselineExpenses WHERE PlanID = @strPlanID
  IF (@strConTab = 'Y' AND @intBaselineConCount > 0) DELETE RPBaselineConsultant WHERE PlanID = @strPlanID
  IF (@strUntTab = 'Y' AND @intBaselineUntCount > 0) DELETE RPBaselineUnit WHERE PlanID = @strPlanID

  IF (@intPlannedLabCount > 0) DELETE RPPlannedLabor WHERE PlanID = @strPlanID
  IF (@strExpTab = 'Y' AND @intPlannedExpCount > 0) DELETE RPPlannedExpenses WHERE PlanID = @strPlanID
  IF (@strConTab = 'Y' AND @intPlannedConCount > 0) DELETE RPPlannedConsultant WHERE PlanID = @strPlanID
  IF (@strUntTab = 'Y' AND @intPlannedUntCount > 0) DELETE RPPlannedUnit WHERE PlanID = @strPlanID

  IF (@strFeesByPeriod = 'Y')
    BEGIN
      IF (@intCompCount > 0) DELETE RPCompensationFee WHERE PlanID = @strPlanID
      IF (@intConsCount > 0) DELETE RPConsultantFee WHERE PlanID = @strPlanID
      IF (@intReimCount > 0) DELETE RPReimbAllowance WHERE PlanID = @strPlanID
    END -- IF (@strFeesByPeriod = 'Y')
  
  IF (@intEVCount > 0) DELETE RPEVT WHERE PlanID = @strPlanID

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Put all time-phased data from temp tables back.
  
  IF (@intPlannedLabCount > 0)
    INSERT RPPlannedLabor
          (TimePhaseID, 
           PlanID, 
           TaskID,
           AssignmentID,
           StartDate, 
           EndDate, 
           PeriodHrs, 
           CostRate,
           BillingRate,
           PeriodCount, 
           PeriodScale,
           CreateDate,
           ModDate)
      SELECT
        X.TimePhaseID, 
        X.PlanID, 
        X.TaskID,
        X.AssignmentID,
        X.StartDate, 
        X.EndDate, 
        X.PeriodHrs, 
        dbo.RP$LabRate
          (A.ResourceID,
           ISNULL(A.Category, 0),
           A.GRLBCD,
           ISNULL(A.LaborCode, T.LaborCode),
           @sintCostRtMethod,
           @intCostRtTableNo,
           @sintGRMethod,
           @intGenResTableNo,
           X.StartDate,
           X.EndDate,
           @intRtCostDecimals,
           ISNULL(EM.ProvCostRate, 0),
           ISNULL(A.CostRate, T.CostRate)) AS CostRate,
        dbo.RP$LabRate
          (A.ResourceID,
           ISNULL(A.Category, 0),
           A.GRLBCD,
           ISNULL(A.LaborCode, T.LaborCode),
           @sintBillingRtMethod,
           @intBillingRtTableNo,
           @sintGRMethod,
           @intGRBillTableNo,
           X.StartDate,
           X.EndDate,
           @intRtBillDecimals,
           ISNULL(EM.ProvBillRate, 0),
           ISNULL(A.BillingRate, T.BillingRate)) AS BillingRate,                 
        X.PeriodCount, 
        X.PeriodScale,
        LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
        LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM
          (SELECT -- Need to group together the period with same Start Date first
             REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
             PlanID, 
             TaskID,
             AssignmentID,
             MIN(StartDate) AS StartDate, 
             MAX(EndDate) AS EndDate, 
             SUM(ISNULL(PeriodHrs, 0)) AS PeriodHrs,
             PeriodCount, 
             PeriodScale
             FROM @tabPLabTPD
             GROUP BY AssignmentID, CIStartDate, TaskID, PlanID, PeriodCount, PeriodScale) AS X
          LEFT JOIN RPAssignment AS A ON X.PlanID = A.PlanID AND X.TaskID = A.TaskID AND X.AssignmentID = A.AssignmentID
          LEFT JOIN EM ON A.ResourceID = EM.Employee
          INNER JOIN RPTask AS T ON X.PlanID = T.PlanID AND X.TaskID = T.TaskID
        WHERE (PeriodHrs IS NOT NULL AND PeriodHrs != 0)
  
  IF (@strExpTab = 'Y' AND @intPlannedExpCount > 0)
    INSERT RPPlannedExpenses
          (TimePhaseID, 
           PlanID, 
           TaskID,
           ExpenseID,
           StartDate, 
           EndDate, 
           PeriodCost, 
           PeriodBill, 
           PeriodCount, 
           PeriodScale,
           CreateDate,
           ModDate)
      SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
             PlanID, 
             TaskID,
             ExpenseID,
             MIN(StartDate) AS StartDate, 
             MAX(EndDate) AS EndDate, 
             SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
             SUM(ISNULL(PeriodBill, 0)) AS PeriodBill,
             PeriodCount, 
             PeriodScale,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM @tabPExpTPD
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
        GROUP BY ExpenseID, CIStartDate, TaskID, PlanID, PeriodCount, PeriodScale
  
  IF (@strConTab = 'Y' AND @intPlannedConCount > 0)
    INSERT RPPlannedConsultant
          (TimePhaseID, 
           PlanID, 
           TaskID,
           ConsultantID,
           StartDate, 
           EndDate, 
           PeriodCost, 
           PeriodBill, 
           PeriodCount, 
           PeriodScale,
           CreateDate,
           ModDate)
      SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
             PlanID, 
             TaskID,
             ConsultantID,
             MIN(StartDate) AS StartDate, 
             MAX(EndDate) AS EndDate, 
             SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
             SUM(ISNULL(PeriodBill, 0)) AS PeriodBill,
             PeriodCount, 
             PeriodScale,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM @tabPConTPD
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
        GROUP BY ConsultantID, CIStartDate, TaskID, PlanID, PeriodCount, PeriodScale

  IF (@strUntTab = 'Y' AND @intPlannedUntCount > 0)
    INSERT RPPlannedUnit
          (TimePhaseID, 
           PlanID, 
           TaskID,
           UnitID,
           StartDate, 
           EndDate, 
           PeriodQty, 
           PeriodCount, 
           PeriodScale,
           CreateDate,
           ModDate)
      SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
             PlanID, 
             TaskID,
             UnitID,
             MIN(StartDate) AS StartDate, 
             MAX(EndDate) AS EndDate, 
             SUM(ISNULL(PeriodQty, 0)) AS PeriodQty,
             PeriodCount, 
             PeriodScale,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM @tabPUntTPD
        WHERE (PeriodQty IS NOT NULL AND PeriodQty != 0)
        GROUP BY UnitID, CIStartDate, TaskID, PlanID, PeriodCount, PeriodScale
    
  IF (@strFeesByPeriod = 'Y')
    BEGIN

      IF (@intCompCount > 0)
        INSERT RPCompensationFee
              (TimePhaseID, 
               PlanID, 
               TaskID,
               StartDate, 
               EndDate, 
			   PeriodDirLabCost, 
			   PeriodDirLabBill,   
			   PeriodDirExpCost, 
			   PeriodDirExpBill, 
               PeriodCost, 
               PeriodBill, 
               CreateDate,
                ModDate)
          SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
                 PlanID, 
                 TaskID,
                 MIN(StartDate) AS StartDate, 
                 MAX(EndDate) AS EndDate, 
                 SUM(ISNULL(PeriodDirLabCost, 0)) AS PeriodDirLabCost,
                 SUM(ISNULL(PeriodDirLabBill, 0)) AS PeriodDirLabBill,
                 SUM(ISNULL(PeriodDirExpCost, 0)) AS PeriodDirExpCost,
                 SUM(ISNULL(PeriodDirExpBill, 0)) AS PeriodDirExpBill,
                 SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
                 SUM(ISNULL(PeriodBill, 0)) AS PeriodBill,
                 LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
                 LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
            FROM @tabCompTPD
            WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
            GROUP BY TaskID, CIStartDate, PlanID
        
      IF (@intConsCount > 0)
        INSERT RPConsultantFee
              (TimePhaseID, 
               PlanID, 
               TaskID,
               StartDate, 
               EndDate, 
               PeriodCost, 
               PeriodBill, 
               CreateDate,
               ModDate)
          SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
                 PlanID, 
                 TaskID,
                 MIN(StartDate) AS StartDate, 
                 MAX(EndDate) AS EndDate, 
                 SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
                 SUM(ISNULL(PeriodBill, 0)) AS PeriodBill,
                 LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
                 LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
            FROM @tabConsTPD
            WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
            GROUP BY TaskID, CIStartDate, PlanID

      IF (@intReimCount > 0)
        INSERT RPReimbAllowance
              (TimePhaseID, 
               PlanID, 
               TaskID,
               StartDate, 
               EndDate, 
		       periodConCost,  
		       periodConBill, 
		       periodExpCost , 
               periodExpBill , 
               PeriodCost, 
               PeriodBill, 
               CreateDate,
               ModDate)
          SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
                 PlanID, 
                 TaskID,
                 MIN(StartDate) AS StartDate, 
                 MAX(EndDate) AS EndDate, 
                 SUM(ISNULL(periodConCost, 0)) AS periodConCost,
                 SUM(ISNULL(periodConBill, 0)) AS periodConBill,
                 SUM(ISNULL(periodExpCost, 0)) AS periodExpCost,
                 SUM(ISNULL(periodExpBill, 0)) AS periodExpBill,
                 SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
                 SUM(ISNULL(PeriodBill, 0)) AS PeriodBill,
                 LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
                 LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
            FROM @tabReimTPD
            WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
            GROUP BY TaskID, CIStartDate, PlanID
          
    END -- IF (@strFeesByPeriod = 'Y'
    
  -- For EV Pct, pick the highest Pct for the common periods.
  --
  --   Old Scale: |   25  |  50  |
  --   New Scale: |      50      |
  --
  --   Old Scale:    |      25      |      75      |      90      
  --   New Scale: |   25  |  25  |  75  |  75  |  90  |

  IF (@intEVCount > 0)
    INSERT RPEVT
          (TimePhaseID, 
           PlanID, 
           TaskID,
           StartDate, 
           EndDate, 
           PeriodPct, 
           CreateDate,
           ModDate)
      SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
             PlanID, 
             TaskID,
             MIN(StartDate) AS StartDate, 
             MAX(EndDate) AS EndDate, 
             MAX(ISNULL(PeriodPct, 0)) AS PeriodPct,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM @tabEVTPD
        GROUP BY TaskID, CIStartDate, PlanID

  -->>> Baseline.
  
  IF (@intBaselineLabCount > 0)
    INSERT RPBaselineLabor
          (TimePhaseID, 
           PlanID, 
           TaskID,
           AssignmentID,
           StartDate, 
           EndDate, 
           PeriodHrs, 
           PeriodCost, 
           PeriodBill, 
           PeriodRev, 
           PeriodCount, 
           PeriodScale,
           CreateDate,
           ModDate)
      SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
             PlanID, 
             TaskID,
             AssignmentID,
             MIN(StartDate) AS StartDate, 
             MAX(EndDate) AS EndDate, 
             SUM(ISNULL(PeriodHrs, 0)) AS PeriodHrs,
             SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
             SUM(ISNULL(PeriodBill, 0)) AS PeriodBill,
             SUM(ISNULL(PeriodRev, 0)) AS PeriodRev,
             PeriodCount, 
             PeriodScale,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM @tabBLabTPD
        WHERE (PeriodHrs IS NOT NULL AND PeriodHrs != 0)
        GROUP BY AssignmentID, CIStartDate, TaskID, PlanID, PeriodCount, PeriodScale
  
  IF (@strExpTab = 'Y' AND @intBaselineExpCount > 0)
    INSERT RPBaselineExpenses
          (TimePhaseID, 
           PlanID, 
           TaskID,
           ExpenseID,
           StartDate, 
           EndDate, 
           PeriodCost, 
           PeriodBill, 
           PeriodRev, 
           PeriodCount, 
           PeriodScale,
           CreateDate,
           ModDate)
      SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
             PlanID, 
             TaskID,
             ExpenseID,
             MIN(StartDate) AS StartDate, 
             MAX(EndDate) AS EndDate, 
             SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
             SUM(ISNULL(PeriodBill, 0)) AS PeriodBill,
             SUM(ISNULL(PeriodRev, 0)) AS PeriodRev,
             PeriodCount, 
             PeriodScale,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM @tabBExpTPD
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
        GROUP BY ExpenseID, CIStartDate, TaskID, PlanID, PeriodCount, PeriodScale
  
  IF (@strConTab = 'Y' AND @intBaselineConCount > 0)
    INSERT RPBaselineConsultant
          (TimePhaseID, 
           PlanID, 
           TaskID,
           ConsultantID,
           StartDate, 
           EndDate, 
           PeriodCost, 
           PeriodBill, 
           PeriodRev, 
           PeriodCount, 
           PeriodScale,
           CreateDate,
           ModDate)
      SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
             PlanID, 
             TaskID,
             ConsultantID,
             MIN(StartDate) AS StartDate, 
             MAX(EndDate) AS EndDate, 
             SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
             SUM(ISNULL(PeriodBill, 0)) AS PeriodBill,
             SUM(ISNULL(PeriodRev, 0)) AS PeriodRev,
             PeriodCount, 
             PeriodScale,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM @tabBConTPD
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
        GROUP BY ConsultantID, CIStartDate, TaskID, PlanID, PeriodCount, PeriodScale
      
  IF (@strUntTab = 'Y' AND @intBaselineUntCount > 0)
    INSERT RPBaselineUnit
          (TimePhaseID, 
           PlanID, 
           TaskID,
           UnitID,
           StartDate, 
           EndDate, 
           PeriodQty, 
           PeriodCost, 
           PeriodBill, 
           PeriodRev, 
           PeriodCount, 
           PeriodScale,
           CreateDate,
           ModDate)
      SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
             PlanID, 
             TaskID,
             UnitID,
             MIN(StartDate) AS StartDate, 
             MAX(EndDate) AS EndDate, 
             SUM(ISNULL(PeriodQty, 0)) AS PeriodQty,
             SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
             SUM(ISNULL(PeriodBill, 0)) AS PeriodBill,
             SUM(ISNULL(PeriodRev, 0)) AS PeriodRev,
             PeriodCount, 
             PeriodScale,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM @tabBUntTPD
        WHERE (PeriodQty IS NOT NULL AND PeriodQty != 0)
        GROUP BY UnitID, CIStartDate, TaskID, PlanID, PeriodCount, PeriodScale
     
  SET NOCOUNT OFF

END -- rpAlignTPD
GO
