SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_DelegationHistoryGet] ( @Username nvarchar(32), @BeginDate datetime, @EndDate datetime)
             AS EXEC spCCG_PAT_DelegationHistoryGet @Username,@BeginDate,@EndDate
GO
