SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EMAIL_Add_History]
	@msgSeqList		varchar(max),
	@msgGroupSeq	int,
	@isGroup		bit,
	@type			varchar(20),--Sent,Expired,FailedAttempt,Canceled
	@description	varchar(max)--timmed to 250 below
AS
BEGIN
	/*
	select * from CCG_EMAIL_Messages
	select * from CCG_EMAIL_HISTORY order by seq desc
	DeltekStoredProc_CCG_EMAIL_Add_History
		@msgSeqList	= '12,13',
		@msgGroupSeq = 13,
		@isGroup =1,
		@type = 'Sent',
		@description = 'Message Queued'
 	*/
	SET NOCOUNT ON;
	--DECLARE @returnVal		int;
	--DECLARE @returnMessage	varchar(max);	
	--Use Replace to handle single quotes passed in to reduce SQL injection risk

	DECLARE @sSQL		varchar(max);

	--consider a try catch around the history insert
	
	--Replace don't allow single quotes to be passed in to reduce SQL injection risk
	set @sSQL = 'INSERT INTO CCG_Email_History (MessageSeq,MessageGroupSeq,SourceApplication,Sender,ToList,CCList,BCCList,Subject,Body,Priority,MaxDelay,Type,Description) ' +
        'SELECT Seq,' + case when @isGroup = 1 then Convert(varchar(20),@msgGroupSeq) else 'NULL' end +
        ' ,SourceApplication,Sender,ToList,CCList,BCCList,' +
        case when @isGroup = 1 then 'CASE WHEN NOT GroupSubject IS NULL THEN GroupSubject ELSE SourceApplication + '' Emails'' END ' else ' Subject' end +
        ' ,Body,Priority,MaxDelay,''' +  Replace(@type,'''','') + ''',LEFT(' + case when ISNULL(@description,'') = '' then '''''' else '''' + Replace(ISNULL(@description,''),'''', '''''') + '; ''' end  + '+''Requested:'' + Convert(varchar(20),CreateDateTime),250) '  +
        ' from CCG_Email_Messages where Seq in (' + Replace(@msgSeqList,'''','') + ')';
	exec(@sSQL)
--print ISNULL(@sSQL,'NULL')
select @@ROWCOUNT

END

GO
