SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[refreshAutoNumSeqs] @navID varchar(50), @chargeType varchar(1)
as
declare
  @MappedValuesFetch		integer,
  @ExpressionsFetch 		integer,
  @AutoNumSeqLen		integer,
  @Org1Start			integer,
  @Org1Length			integer,
  @Org2Start			integer,
  @Org2Length			integer,
  @Org3Start			integer,
  @Org3Length			integer,
  @Org4Start			integer,
  @Org4Length			integer,
  @Org5Start			integer,
  @Org5Length			integer,
  @SqlStmt			NVarchar(max),
  @mvCaseQry 			Nvarchar(max),
  @wkQry 			Nvarchar(max),
  @Item				varchar(255),
  @TableName			varchar(255),
  @StartPosition		decimal(18,0),
  @LengthUsed			decimal(18,0),
  @DefaultValue			varchar(50),
  @ResetSeq			varchar(1),
  @RuleID			varchar(32),
  @Seq				varchar(32),
  @MappedValues			varchar(20),
  @myTableVar			varchar(40),
  @customTableJoin  varchar(255),
  @strAutoNumSeqStart varchar(50)
BEGIN 
   IF @navID IS NULL
       set @navID = ''
   IF @chargeType IS NULL
       set @chargeType = ' '

  set @ExpressionsFetch = 0
  set @MappedValuesFetch = 0
  set @wkQry = ''
  set @mvCaseQry = ''

	-- clear out existing Seqs for this NavID, if any exist.
	set nocount on
	DELETE FROM CFGAutoNumSeqs WHERE NavID= @navID AND ChargeType= @chargeType
	IF (SUBSTRING(@navid,1,5) = 'UDIC_')
		BEGIN
			SET @customTableJoin = ''
			SELECT @AutoNumSeqLen = AutoNumSeqLen, 
			       @myTableVar = TableName
			FROM FW_UDIC
			JOIN FW_CUSTOMCOLUMNS ON INFOCENTERAREA = UDIC_ID 
			WHERE UDIC_ID = @navID AND FW_CustomColumns.DataType = 'recordID'
		END
	ELSE 
		BEGIN
			SELECT 	@Org1Start=Org1Start,@Org1Length=Org1Length,
			@Org2Start=Org2Start,@Org2Length=Org2Length,
			@Org3Start=Org3Start,@Org3Length=Org3Length,
			@Org4Start=Org4Start,@Org4Length=Org4Length,
			@Org5Start=Org5Start,@Org5Length=Org5Length
			FROM CFGFormat
			SELECT @AutoNumSeqLen = CASE (SELECT @navID)
				WHEN 'client' 		THEN clientAutoNumSeqLen
			WHEN 'employee' 	THEN employeeAutoNumSeqLen
			WHEN 'wbs1' 		THEN wbs1AutoNumSeqLen
			WHEN 'vendor' 		THEN vendorAutoNumSeqLen
			WHEN 'mktCampaign' 	THEN mktCampaignAutoNumSeqLen
			WHEN 'equipment' 	THEN equipmentAutoNumSeqLen
			WHEN 'rp' 		THEN rpAutoNumSeqLen	END
		FROM CFGFormat
		SELECT @myTableVar = CASE (SELECT @navID)
			WHEN 'client' 		THEN 'Clendor'
			WHEN 'employee' 	THEN 'EMAllCompany'
			WHEN 'wbs1' 		THEN 'PR'
			WHEN 'vendor' 		THEN 'VE'
			WHEN 'mktCampaign' 	THEN 'MktCampaign'
			WHEN 'equipment' 	THEN 'Equipment'
			WHEN 'rp' 		THEN 'RPPlan'		END
		SELECT @customTableJoin = CASE (SELECT @navID)
			WHEN 'client' 		THEN ' LEFT JOIN ClientCustomTabFields ON ClientCustomTabFields.ClientID=Clendor.ClientID'
			WHEN 'employee' 	THEN ' LEFT JOIN EmployeeCustomTabFields ON EmployeeCustomTabFields.Employee=EMAllCompany.Employee'
			WHEN 'wbs1' 		THEN ' LEFT JOIN ProjectCustomTabFields ON ProjectCustomTabFields.WBS1=PR.WBS1 AND ProjectCustomTabFields.WBS2=PR.WBS2 AND ProjectCustomTabFields.WBS3=PR.WBS3'
			WHEN 'vendor' 		THEN ' LEFT JOIN VendorCustomTabFields ON VendorCustomTabFields.Vendor=VE.Vendor'
			WHEN 'mktCampaign' 	THEN ' LEFT JOIN MktCampaignCustomTabFields ON MktCampaignCustomTabFields.CampaignID=MktCampaign.CampaignID'
			WHEN 'equipment' 	THEN ' LEFT JOIN EquipmentCustomTabFields ON EquipmentCustomTabFields.EquipmentID=Equipment.EquipmentID'
			WHEN 'rp' 		THEN ''		END

		SELECT @strAutoNumSeqStart = CASE (SELECT @navID)
			WHEN 'wbs1' 		THEN 
				CASE (SELECT @chargeType)
					WHEN 'P' THEN 'AutoNumSeqStartPromo'
					WHEN 'H' THEN 'AutoNumSeqStartOH'
					ELSE 'AutoNumSeqStart' END
			ELSE 'AutoNumSeqStart'	END
	END

	--Print '@AutoNumSeqLen: '
	--Print @AutoNumSeqLen

	DECLARE ExpressionCursor CURSOR FOR
		SELECT Item, CASE WHEN substring(Item,1,1) = '[' THEN '' ELSE 
			substring(Item,1,charindex('.',Item)-1) END As TableName, 
			StartPosition, LengthUsed, DefaultValue, ResetSeq, RuleID, Seq,
			CASE (SELECT count(*) FROM CFGAutoNumMappedValues MV WHERE ANExp.RuleID=MV.RuleID) WHEN 0 THEN ''
			      ELSE '<Values Mapped>' END AS MappedValues
			FROM CFGAutoNumExpression ANExp, CFGFormat
			WHERE NavID=''+@navID+'' AND ChargeType=''+@chargeType+''
			ORDER BY Seq
	OPEN ExpressionCursor
	fetch next from ExpressionCursor into 
		@Item,		@TableName,
		@StartPosition,	@LengthUsed,
		@DefaultValue,	@ResetSeq,
		@RuleID,	@Seq,
		@MappedValues
	set @ExpressionsFetch = @@Fetch_Status
	WHILE (@ExpressionsFetch = 0)
	BEGIN
		if (@ResetSeq = 'Y')
		BEGIN
			set @mvCaseQry = ''
			if (@Item = '[ Char]')
				set @mvCaseQry = ' '''+@DefaultValue+''' '
			else if (@Item = '[org1]')
				set @mvCaseQry = 'substring(Organization.Org,'+convert(varchar(20),@Org1Start)+','+convert(varchar(20),@Org1Length)+')'
			else if (@Item = '[org2]')
				set @mvCaseQry = 'substring(Organization.Org,'+convert(varchar(20),@Org2Start)+','+convert(varchar(20),@Org2Length)+')'
			else if (@Item = '[org3]')
				set @mvCaseQry = 'substring(Organization.Org,'+convert(varchar(20),@Org3Start)+','+convert(varchar(20),@Org3Length)+')'
			else if (@Item = '[org4]')
				set @mvCaseQry = 'substring(Organization.Org,'+convert(varchar(20),@Org4Start)+','+convert(varchar(20),@Org4Length)+')'
			else if (@Item = '[org5]')
				set @mvCaseQry = 'substring(Organization.Org,'+convert(varchar(20),@Org5Start)+','+convert(varchar(20),@Org5Length)+')'
			else if (@Item = '[CurrentMonth]')
			BEGIN
				SELECT @mvCaseQry = CASE WHEN (Len(DatePart(month,getDate()))=1) 
					THEN ' ''0'+convert(varchar(2),DatePart(month,getDate()))+''' '
					ELSE ' '''+convert(varchar(2),DatePart(month,getDate()))+''' ' END
			END
			else if (@Item = '[CurrentYear]')
			BEGIN
				SELECT @mvCaseQry = CASE WHEN (@LengthUsed=2) 
					THEN ' '''+substring(convert(varchar(4),DatePart(year,getDate())),3,2)+''' '
					ELSE ' '''+convert(varchar(4),DatePart(year,getDate()))+''' ' END
			END
            else if (@Item = '[CurrentPdMonth]')
            BEGIN
                            SELECT @mvCaseQry = '(Select substring(CAST(CurrentPeriod As varchar),5,2) AS curPeriod FROM FW_CFGSystem)'
            END
            else if (@Item = '[CurrentPdYear]')
            BEGIN
                            SELECT @mvCaseQry = '(Select substring(CAST(CurrentPeriod As varchar),1,4) AS curYear FROM FW_CFGSystem)'
            END
            else if (@Item = '[ProjectPrimaryClientNumber]')
            BEGIN
							SELECT @mvCaseQry = ' '''+@DefaultValue+''' '
            END
			else if (@MappedValues = '<Values Mapped>')
			BEGIN
				DECLARE 
				  @mvItem		varchar(255),
				  @mvTableName		varchar(255),
				  @mvValueInVision	varchar(255),
				  @mvValueForAutoNum	varchar(255),
				  @mvRuleID		varchar(32),
				  @mvSeq		varchar(32)
	
				DECLARE MappedValuesCursor CURSOR FOR
					SELECT Item, substring(Item,1,charindex('.',Item)-1) As TableName,
						ValueInVision, ValueforAutoNum, RuleID, Seq
					FROM CFGAutoNumMappedValues
					WHERE NavID=''+@navID+'' AND RuleID=''+@RuleID+''
					ORDER BY Seq
				OPEN MappedValuesCursor
				fetch next from MappedValuesCursor into 
					@mvItem,		@mvTableName,
					@mvValueInVision,	@mvValueForAutoNum,
					@mvRuleID,		@mvSeq
				set @MappedValuesFetch = @@Fetch_Status
				WHILE (@MappedValuesFetch = 0)
				BEGIN
					if (@mvCaseQry = '')	set @mvCaseQry = 'CASE (SELECT '+@mvItem+')'
					set @mvCaseQry = @mvCaseQry + ' WHEN N'''+@mvValueInVision+''' THEN N'''+@mvValueForAutoNum+''' '
					fetch next from MappedValuesCursor into 
						@mvItem,		@mvTableName,
						@mvValueInVision,	@mvValueForAutoNum,
						@mvRuleID,		@mvSeq
					set @MappedValuesFetch = @@Fetch_Status
				END
				if (@mvCaseQry > '')	set @mvCaseQry = @mvCaseQry + 'ELSE N'''+@DefaultValue+''' END'
				--Print @mvCaseQry
				close MappedValuesCursor
				deallocate MappedValuesCursor
			END
	
			if (@wkQry > '') 	set @wkQry = @wkQry + ' + '
			if (@mvCaseQry = '')	set @mvCaseQry = @Item
			set @wkQry = @wkQry + 'IsNull(substring('+@mvCaseQry+','+convert(varchar(20),@StartPosition)+','+convert(varchar(20),@LengthUsed)
			+'),'''+@DefaultValue+''')'
			--Print @wkQry
		END
		fetch next from ExpressionCursor into 
			@Item,			@TableName,
			@StartPosition,		@LengthUsed,
			@DefaultValue,		@ResetSeq,
			@RuleID,		@Seq,
			@MappedValues
		set @ExpressionsFetch = @@Fetch_Status
	END
	
  -- Print '@wkQry: '
   --Print @wkQry
	if (@wkQry > '')
	BEGIN
	 	set @SqlStmt = 'SELECT DISTINCT NavID='''+@navID+'''' + ', '
				+ 'ChargeType='''+@ChargeType+'''' + ', '
				+ 'REPLACE('
				+ 'REPLACE('
				+ 'REPLACE('
				+@wkQry
				+','':'','''')'
				+',''.'','''')'
				+','','','''')'
				+ ' as ResetPart,'
		IF (SUBSTRING(@navid,1,5) = 'UDIC_')				
			BEGIN
				set @SqlStmt = @SqlStmt + ' LastAutoSeq = AutoNumSeqStart - 1 '
				+' FROM FW_UDIC, ' + @myTableVar
				+' WHERE FW_UDIC.UDIC_ID = '''+@navID+''''
			END
		ELSE
			BEGIN
				set @SqlStmt = @SqlStmt + ' LastAutoSeq = '+@navID+@strAutoNumSeqStart+' - 1 '
				+' FROM CFGFormat, ' + @myTableVar
				if (@myTableVar <> 'Clendor' AND @myTableVar <> 'Equipment')
				BEGIN
					set @SqlStmt = @SqlStmt +' LEFT JOIN Organization ON Organization.Org=' + @myTableVar+ '.Org'
				END
			END

		SET @SqlStmt = 'INSERT INTO CFGAutoNumSeqs ' + @SqlStmt + @customTableJoin
		
		set nocount on
		EXECUTE (@SqlStmt)
		--Print 'sqlstmt: '
		--Print @SqlStmt
	END

	close ExpressionCursor
	deallocate ExpressionCursor
	--Print 'Exiting function'

END -- refreshAutoNumSeqs
GO
