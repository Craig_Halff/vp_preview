SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_FileHistory] ( @WBS1 nvarchar(30), @Category nvarchar(50)= null, @DOCS_PR_FILEID varchar(50)= null, @RevisionHistory int= 0, @Language varchar(10)= 'en-US', @Username nvarchar(32)= null)
             AS EXEC spCCG_EI_FileHistory @WBS1,@Category,@DOCS_PR_FILEID,@RevisionHistory,@Language,@Username
GO
