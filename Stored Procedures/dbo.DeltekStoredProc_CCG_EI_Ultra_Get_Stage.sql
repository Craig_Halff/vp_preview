SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Ultra_Get_Stage]
	@wbs1	Nvarchar(32),
	@VISION_LANGUAGE	varchar(10) = null
AS BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_EI_Ultra_Get_Stage] '2002002.00','en-US'
	SET NOCOUNT ON;

	SELECT InvoiceStage,ISNULL(d.StageLabel,InvoiceStage) as InvoiceStage, Convert(varchar(25), DateAdd(minute, DateDiff(minute, getutcdate(), getdate()), DateChanged), 25), ChangedBy
		FROM CCG_EI
		LEFT JOIN CCG_EI_ConfigStagesDescriptions d on CCG_EI.InvoiceStage = d.Stage and ISNULL(@VISION_LANGUAGE,'en-US') = d.UICultureName
		WHERE wbs1 = @wbs1;
END

GO
