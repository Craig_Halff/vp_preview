SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Call_StageChange] (
	@PayableSeq		int,
	@OldStage		Nvarchar(max),
	@NewStage		Nvarchar(max),
	@Username		Nvarchar(max),
	@Role			Nvarchar(max)
) AS
BEGIN
	SET NOCOUNT ON;

	EXEC (N'spCCG_PAT_StageChange
		@PayableSeq	= ' + @PayableSeq + ',
		@OldStage	= ''' + @OldStage + ''',
		@NewStage	= ''' + @NewStage + ''',
		@Username	= N''' + @Username + ''',
		@Role		= N''' + @Role + '''');
END;

GO
