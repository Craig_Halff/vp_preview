SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_DelegationNotifications] (
	@Employee				Nvarchar(20),			-- The employee for which notifications will be checked
	@RepeatInterval			int,					-- Number of days between resending notifications (0 or -1 means no repeated emails)
	@Subject				Nvarchar(120),			-- The subject of the email to send (looks in CCG_Language_Labels when this is blank)
	@Body					Nvarchar(1024),			-- The body of the email to send (looks in CCG_Language_Labels when this is blank)
	@OverrideSenderEmail	Nvarchar(100),
	@ShowDaysOutstanding	char(1) = 'Y',
	@MsgPriority			Nvarchar(10) = NULL,
	@CCEmployee				Nvarchar(100) = NULL,
	@MessageDelay			int = 0
)
AS BEGIN
	/*
	-- Copyright (c) 2020 EleVia Software. All rights reserved.

	DECLARE	@Employee				Nvarchar(20) = '00014',
			@RepeatInterval			int = 1,
			@Subject				Nvarchar(120) = 'Test Subject',
			@Body					Nvarchar(1024) = 'Test Body: [:EM.Name] requests delegation to employee [:Delegate.Name]',
			@OverrideSenderEmail	Nvarchar(100),
			@MsgPriority			Nvarchar(10) = NULL,
			@CCEmployee				Nvarchar(100) = NULL,
			@MessageDelay			int = 0

	-- EXEC [spCCG_PAT_DelegationNotifications] '00202', 1, '', '', NULL, 'Y'
	*/

	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	BEGIN TRY
		DECLARE @Username					Nvarchar(32),
				@NotificationIntervalHours	int,			-- Hours
				@EmailLink					Nvarchar(1024),	-- 5.0 sps
				@EncTimeout					int,			-- 5.0 sps
				@Email						Nvarchar(255),
				@Language					varchar(10),
				@SupEmail					Nvarchar(255),	-- 5.0 sps
				@GUID						uniqueidentifier,
				@nowutc						datetime,
				@DelEmployeesList			Nvarchar(max),
				@DelBySupervisor			char(1),
				@DelApproval				char(1),
				@DaysOutstandingStr			Nvarchar(1024) = '[# days outstanding]'

		SELECT @EmailLink = EmailLink, @EncTimeout = EncTimeout FROM CCG_PAT_Config

		SET @NotificationIntervalHours = @RepeatInterval * 24
		SET @nowutc = GETUTCDATE()
		SET @GUID = NEWID()

		-- Create email link to PAT application
		-------------------------------------------------------
		IF IsNull(@EmailLink,N'') <> N''
		BEGIN
			SELECT @Username = IsNull(Min(Username),'') FROM SEUser WHERE Employee = @Employee and DisableLogin = 'N'
			if CHARINDEX(N'database',@EmailLink) > 0 or CHARINDEX(N'dbup',@EmailLink) > 0						-- not Cloud
				set @EmailLink = REPLACE(@EmailLink, N'[:USERENC]', N'userenc=' + dbo.fnCCG_PAT_GetE(@Username))
			else
				set @EmailLink = REPLACE(@EmailLink, N'[:USERENC]', N'user=' + dbo.fnCCG_HexScrambleCode(@Username))
			set @EmailLink = REPLACE(@EmailLink, N'[:USER]', N'user=' + dbo.fnCCG_HexScrambleCode(@Username))
			set @EmailLink = REPLACE(@EmailLink, N'[:EDENC]',   N'edenc='   + dbo.fnCCG_PAT_GetE(Convert(Nvarchar(20), DATEADD(d, @EncTimeout, getdate()),101)))
		END

		-- Get email addresses of employee and supervisor
		-------------------------------------------------------
		SELECT @Email = EM.Email, @SupEmail = Sup.Email, @Language = EM.[Language]
			FROM EM
				LEFT JOIN EM Sup ON Sup.Employee = EM.Supervisor
			WHERE EM.Employee = @Employee

		IF Len(ISNULL(@Email,N'')) < 1 BEGIN
			Print 'Employee has no email address'
			RETURN
		END
		IF Len(ISNULL(@Language,N'')) < 1 BEGIN
			Print 'Employee has no language specified'
			SET @Language = 'en-US'
		END

		-- Get delegation config settings
		-------------------------------------------------------
		SELECT @DelEmployeesList = DelegationEmployeesList, @DelApproval = DelegationApproval, @DelBySupervisor = DelegationBySupervisor
			FROM CCG_PAT_Config

		IF @DelApproval = 'N' BEGIN
			Print 'Approval is not necessary for delegation in the PAT system'
			RETURN
		END
		IF CHARINDEX(@Employee, ','+@DelEmployeesList+',') = 0 SET	@DelEmployeesList = NULL

		-- Get pending approval Email subject/body config
		-------------------------------------------------------
		IF ISNULL(@Subject,'') = ''
			SELECT @Subject = l.Label FROM CCG_Language_Labels l
				WHERE l.Id = 'Pending Delegation Approval - Subject' AND l.Group1 = 'Delegation' AND l.UICultureName = @Language
		IF ISNULL(@Body,'') = ''
			SELECT @Body = l.Label FROM CCG_Language_Labels l
				WHERE l.Id = 'Pending Delegation Approval - Body' AND l.Group1 = 'Delegation' AND l.UICultureName = @Language
		SELECT @DaysOutstandingStr = ISNULL(l.Label, @DaysOutstandingStr) FROM CCG_Language_Labels l
			WHERE l.Id = 'Days outstanding' AND l.Group1 = 'Delegation' AND l.UICultureName = @Language

		-- IF ISNULL(@Body,'') = '' SET @Body = '[:EM.Name] requests delegation to employee [:Delegate.Name]'
		IF ISNULL(@Subject,'') = '' AND ISNULL(@Body,'') = '' BEGIN
			Print 'No pending approval email body or subject has been configured for Language ''' + @Language + ''''
			Print 'Use ''Pending Delegation Approval - Subject'' and ''Pending Delegation Approval - Body'' IDs in the Language config.'
			RETURN
		END

		-- Get pending delegation approvals pertaining to this employee
		-------------------------------------------------------
		DECLARE @PendApprovals TABLE (
				Id							Nvarchar(32),
				DelEmployee					Nvarchar(20),
				DelDelegate					Nvarchar(20),
				FromDate					datetime,
				ToDate						datetime,
				MaxAmount					int,
				Dual						char(1),
				DateLastNotificationSent	datetime,
				RenotifyId					uniqueidentifier)

		INSERT INTO @PendApprovals
			SELECT d.Id, d.Employee, d.Delegate, d.FromDate, d.ToDate, d.MaxAmount, d.Dual, r.DateLastNotificationSent, r.ID as RenotifyId
				FROM CCG_PAT_Delegation d
					-- LEFT JOIN CCG_PAT_DelegationPermanentApprovals pa ON pa.Employee = d.Employee AND pa.Delegate = d.Delegate
					LEFT JOIN EM ON EM.Employee = d.Employee
					LEFT JOIN CCG_PAT_Renotify r ON r.ItemDesc = d.Id AND Stage = 'Pending Delegation' AND r.Employee = @Employee
				WHERE /* pa.Seq IS NULL AND */ d.ApprovedOn IS NULL
					AND (@DelEmployeesList IS NOT NULL OR EM.Supervisor = @Employee)
					AND (r.ID IS NULL
						OR (ISNULL(@RepeatInterval,0) > 0 AND DATEDIFF(hh, IsNull(r.DateLastNotificationSent, '1/1/2000'), GETUTCDATE()) >= @NotificationIntervalHours))

		----SELECT * FROM @PendApprovals

		-- Insert/update RENOTIFY records
		-------------------------------------------------------
		UPDATE r
			SET r.DateLastNotificationSent = @nowutc, r.ID = @GUID, EmailSubject = @Subject, EmailSubjectBatch = @Subject, EmailMessage = @Body
			FROM @PendApprovals p
				INNER JOIN CCG_PAT_Renotify r ON r.ItemDesc = p.Id AND r.Stage = 'Pending Delegation' AND r.Employee = @Employee

		INSERT INTO CCG_PAT_Renotify (ID, InsertDate, Employee, PendingSeq, PayableSeq, ItemDesc, Stage, DateChanged, ChangedBy,
				DateLastNotificationSent, EmailSubjectBatch, EmailSubject, EmailMessage)
			SELECT @GUID, @nowutc, @Employee, NULL, -1, p.Id, 'Pending Delegation', @nowutc, p.DelEmployee,
					@nowutc, @Subject, @Subject, @Body
				FROM @PendApprovals p
					LEFT JOIN CCG_PAT_Renotify r ON r.ItemDesc = p.Id AND r.Stage = 'Pending Delegation' AND r.Employee = @Employee
				WHERE r.ID IS NULL

		----SELECT * FROM CCG_PAT_Renotify Order By DateChanged Desc

		-- Create the actual email message to be sent
		-------------------------------------------------------
		INSERT INTO CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, BCCList, GroupSubject, [Subject], Body, MaxDelay, [Priority])
			SELECT DISTINCT 'Payables Approval Tracking' as SourceApplication,
					(Case When ISNULL(@OverrideSenderEmail,'') <> '' Then @OverrideSenderEmail Else Sender.Email End) as Sender,
					ToEM.Email as ToList,
					NULLIF(ToEM2.Email, '') as CCList,
					null as BCCList,
					Replace(EmailSubjectBatch, '[:ToEM.Name]', ToEM.Lastname + ', ' + ToEM.Firstname) as GroupSubject,
					Replace(EmailSubject, '[:ToEM.Name]', ToEM.Lastname + ', ' + ToEM.Firstname) as [Subject],
					Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(EmailMessage,
						'[:ToEM.Name]', ToEM.Lastname + ', ' + ToEM.Firstname),
						'[:EM.Name]', DEM.Lastname + ', ' + DEM.Firstname),
						'[:EM.LastName]', DEM.Lastname),
						'[:EM.Firstname]', DEM.Firstname),
						'[:Delegate.Name]', DDel.Lastname + ', ' + DDel.Firstname),
						'[:Delegate.LastName]', DDel.Lastname),
						'[:Delegate.Firstname]', DDel.Firstname),
						'[:LINK]', @EmailLink) + (CASE WHEN @ShowDaysOutstanding = 'Y' THEN ' ' +
							REPLACE(@DaysOutstandingStr, '#', Cast(DATEDIFF(d,
								Case
									When IsNull(r.DateLastNotificationSent, r.DateChanged) < r.DateChanged Then r.DateChanged
									Else IsNull(r.DateLastNotificationSent, r.DateChanged)
								End, GETUTCDATE()) as Nvarchar))
							ELSE '' END),
					@MessageDelay as MaxDelay, NULLIF(@MsgPriority, '') as [Priority]
				FROM @PendApprovals p
					INNER JOIN CCG_PAT_Renotify r ON r.ItemDesc = p.Id AND r.Stage = 'Pending Delegation' AND r.Employee = @Employee
					LEFT JOIN EM Sender on Sender.Employee = r.ChangedBy
					LEFT JOIN EM ToEM on ToEM.Employee = r.Employee
					LEFT JOIN EM ToEM2 on ToEM2.Employee = @CCEmployee
					LEFT JOIN EM DEM on DEm.Employee = p.DelEmployee
					LEFT JOIN EM DDel on DDel.Employee = p.DelDelegate
				WHERE r.ID = @GUID

		----SELECT * FROM CCG_Email_Messages WHERE SourceApplication = 'Payables Approval Tracking' ORDER BY CreateDateTime DESC

        -- Add history entry
		-------------------------------------------------------
		INSERT INTO CCG_PAT_History (ActionTaken, ActionDate, ActionTakenBy, ActionRecipient, PayableSeq, Stage, DelegateFor)
			SELECT N'Email Reminder Sent', @nowutc, N'DelegationNotifications Script', @Employee, r.PayableSeq, r.Stage, NULL as DelegateFor
				FROM CCG_PAT_Renotify r
				WHERE ID = @GUID

		----SELECT * FROM CCG_PAT_History WHERE ActionTaken = 'Email Reminder Sent' AND ActionTakenBy = 'DelegationNotifications Script' ORDER BY ActionDate DESC

		-- DECLARE @cnt INT = (SELECT COUNT(*) FROM @PendApprovals)
		-- PRINT 'Successfully completed. New pending approvals for this employee: ' + CAST(@cnt as Nvarchar)
		RETURN 0
	END TRY
	BEGIN CATCH
		DECLARE @res INT
		SET @res = ERROR_NUMBER()

		PRINT @res
		RETURN @res
	END CATCH
END
GO
