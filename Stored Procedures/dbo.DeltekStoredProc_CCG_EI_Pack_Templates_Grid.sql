SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Pack_Templates_Grid]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Seq, Name, Type, SourceType, Source, Status, '' as SourceDisplay, DateChanged, ChangedBy
        FROM CCG_EI_Templates
        ORDER BY Name;

	SELECT Seq, TemplateSeq, Name, Type, SourceType, Value, DateChanged, ChangedBy
		FROM CCG_EI_TemplateParameters;
END;
GO
