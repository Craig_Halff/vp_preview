SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectBillingTermsFeeValidation]
	@WBS1 varchar (32), 
	@WBS2 varchar (7), 
	@WBS3 varchar (7),
	@ConsultFee     float,
	@ReimbAllowCons float,
	@FeeDirExp      float,
	@ReimbAllowExp   float
AS
/*
Copyright 2017 (c) Central Consulting Group, Inc.  All rights reserved.
08/18/2017 David Springer
           Validate fee fields are not in conflict with Billing Type.
           Call this from Contract Details INSERT & CHANGE workflow.
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
DECLARE @BillingType  varchar (255),
		@LumpSumPhase varchar (1),
		@ErrorMessage varchar (8000)
BEGIN

	Select @BillingType = CustBillingType From ProjectCustomTabFields Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3

   If @WBS3 <> ' ' and IsNull (@BillingType, '') = ''
      Begin
   -- Get phase Billing Type
      Select @BillingType = custBillingType, @LumpSumPhase = CustLumpSumPhase
	  From ProjectCustomTabFields Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = ' '
      If @BillingType <> 'Lump Sum by Phase'
	     Begin
         Set @WBS3 = ' ' -- operate at phase level billing terms
		 End
      If @LumpSumPhase = 'Y'
	     Begin
		 Set @BillingType = 'Lump Sum by Phase'
		 If 1 < (Select Count (*) From ProjectCustomTabFields Where WBS1 = @WBS1 and CustBillingType = 'Lump Sum by Phase' and WBS3 = ' ')
			RAISERROR ('Lump Sum by Phase billing type has been used on more than one phase.                                                            ', 16, 1)
         Set @WBS2 = (Select WBS2 From ProjectCustomTabFields Where WBS1 = @WBS1 and CustBillingType = 'Lump Sum by Phase' and WBS3 = ' ')
		 End
      End

   If @WBS2 <> ' ' and IsNull (@BillingType, '') = ''
      Begin
   -- Get phase Lump Sum Phase checkbox
      Select @LumpSumPhase = CustLumpSumPhase From ProjectCustomTabFields Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = ' '
      If @LumpSumPhase = 'Y'
	     Begin
		 Set @BillingType = 'Lump Sum by Phase'
         Set @WBS2 = (Select WBS2 From ProjectCustomTabFields Where WBS1 = @WBS1 and CustBillingType = 'Lump Sum by Phase' and WBS3 = ' ')
		 End
      If @LumpSumPhase = 'N'
	     Begin
	   -- Get project Billing Type
		  Select @BillingType = custBillingType From ProjectCustomTabFields Where WBS1 = @WBS1 and WBS2 = ' '
		  If @BillingType is not null
			 Begin
			 Set @WBS2 = ' ' -- operate at project level billing terms
			 End
         End
      End

	If @BillingType like 'Cost Plus%' and @ConsultFee <> 0
		Begin
		Set @ErrorMessage = @WBS2 + ' - ' + @WBS3 + ' has a Billing Type of ' + @BillingType + ' and cannot have direct consultants.               '
		RAISERROR (@ErrorMessage, 16, 1)
		End

	If @BillingType like 'Cost Plus%' and @FeeDirExp <> 0
		Begin
		Set @ErrorMessage = @WBS2 + ' - ' + @WBS3 + ' has a Billing Type of ' + @BillingType + ' and cannot have direct expenses.               '
		RAISERROR (@ErrorMessage, 16, 1)
		End

	If @BillingType like 'Lump Sum%' and @ReimbAllowCons <> 0
		Begin
		Set @ErrorMessage = @WBS2 + ' - ' + @WBS3 + ' has a Billing Type of ' + @BillingType + ' and cannot have reimbursable consultants.               '
		RAISERROR (@ErrorMessage, 16, 1)
		End

	If @BillingType like 'Lump Sum%' and @ReimbAllowExp <> 0
		Begin
		Set @ErrorMessage = @WBS2 + ' - ' + @WBS3 + ' has a Billing Type of ' + @BillingType + ' and cannot have reimbursable expenses.               '
		RAISERROR (@ErrorMessage, 16, 1)
		End

END
GO
