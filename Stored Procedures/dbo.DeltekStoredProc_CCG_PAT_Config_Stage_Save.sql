SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_Stage_Save]
	@fieldsSQL			Nvarchar(max),
	@updateValuesSql	Nvarchar(max),
	@insertValuesSql	Nvarchar(max),
	@VISION_LANGUAGE	Nvarchar(10)
AS
BEGIN
	/*
	[DeltekStoredProc_CCG_PAT_Config_Stamp_Save]
		@fieldsSQL = 'Label, StampType, DisplayOrder, Content, PositionType, PageSet, Width, ValidRoles, Status, ModDate, ModUser',
		@updateValuesSql = '(''Another One'', ''Text'', 0, ''C:\Temp\ANOTHER'', ''TopRight'', ''All'', 25, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 28), (''Bill to budget, write off remaining'', ''Text'', 1, ''Bill to budget, write off remaining'', ''Free'', ''All'', 50, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 26), (''Approved'', ''Text'', 2, ''Approved\nby [:FIRSTNAME] [:LASTNAME]\non [:DATE] at [:TIME]'', ''TopRight'', ''First'', 20, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 15), (''Approved As-Is'', ''Text'', 3, ''Approved As-Is\nby [:FIRSTNAME] [:LASTNAME]\non [:DATE] at [:TIME]'', ''TopRight'', ''First'', 20, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 18), (''Hold Indefinitely'', ''Text'', 10, ''Hold Indefinitely'', ''TopRight'', ''First'', 20, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 16), (''Hold Until Next Month'', ''Text'', 11, ''Hold Until Next Month'', ''TopRight'', ''First'', 20, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 19), (''Hold As Noted'', ''Text'', 12, ''Hold As Noted'', ''TopRight'', ''First'', 20, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 20), (''Hold item'', ''Text'', 15, ''Hold item'', ''Free'', ''All'', 9, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 27), (''----------------'', ''Text'', 19, ''-'', ''TopRight'', ''All'', 25, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 25), (''Check'', ''Image'', 20, ''c:\temp\OK32.png'', ''Free'', ''All'', 5, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 17), (''Check - Red - Small'', ''Image'', 21, ''C:\Temp\Red Check 16x16.gif'', ''Free'', ''All'', 2, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 21), (''Check - Green - Small'', ''Image'', 22, ''C:\Temp\Green Check 16x16.png'', ''Free'', ''All'', 2, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 22), (''Accepted'', ''Image'', 23, ''C:\Temp\Accepted.jpg'', ''Free'', ''All'', 8, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 23), (''Rejected'', ''Image'', 24, ''C:\Temp\Rejected.jpg'', ''Free'', ''All'', 8, '''', ''Main'', GetDate(), ''ADMIN'', 24)',
		@insertValuesSql = ''
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;
	DECLARE @ids		TABLE (id int);
	DECLARE @i			int;
	DECLARE @newI		int;
	DECLARE @seq		Nvarchar(50);

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_PAT_RegexMatch(N'<CSV>', @fieldsSQL);
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch(N'<INSERT VALUES>|', @updateValuesSql) * 2;
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch(N'<INSERT VALUES>|', @insertValuesSql) * 4;
	IF @safeSql < 7 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS Nvarchar(4));
		RETURN;
	END;

	BEGIN TRANSACTION;

	-- Updates
	IF ISNULL(@updateValuesSql, '') <> '' BEGIN
		SET @sSQL = N'
			UPDATE cs
				SET
					[StageLabel]		= temp.[StageLabel],
					[StageDescription]	= temp.[StageDescription],
					[Status]			= temp.[Status],
					[Type]				= temp.[Type],
					[SubType]			= temp.[SubType],
					[AccountingOnly]	= temp.[AccountingOnly],
					[ValidRoles]		= temp.[ValidRoles],
					[RequireStamp]		= temp.[RequireStamp],
					[EmailSubject]		= temp.[EmailSubject],
					[EmailSubjectBatch]	= temp.[EmailSubjectBatch],
					[EmailMessage]		= temp.[EmailMessage],
					[SortOrder]			= temp.[SortOrder]
				FROM CCG_PAT_ConfigStages cs
				JOIN (
					VALUES ' + @updateValuesSql + '
				) temp (' + @fieldsSQL + ') ON temp.Stage = cs.Stage';
		--PRINT @sSQL;
		EXEC (@sSQL);
	END;

	-- Inserts
	IF ISNULL(@insertValuesSql, '') <> '' BEGIN
		SET @sSQL = N'
			INSERT INTO CCG_PAT_ConfigStages
				(' + @fieldsSQL + ')
				VALUES
				' + @insertValuesSql;
		--PRINT @sSQL;
		EXEC (@sSQL);
	END;

	-- Update the multilingual descriptions table
	SET @sSQL = N'
		DELETE a FROM CCG_PAT_ConfigStagesDescriptions a LEFT JOIN CCG_PAT_ConfigStages b ON a.Stage = b.Stage
			WHERE b.Stage IS NULL		-- No match

		MERGE CCG_PAT_ConfigStagesDescriptions WITH (HOLDLOCK) AS Target
		USING CCG_PAT_ConfigStages AS Source
			ON Target.Stage = Source.Stage AND Target.UICultureName = '''+@VISION_LANGUAGE+'''
		WHEN MATCHED THEN
			UPDATE SET
				[StageLabel]		= Source.[StageLabel],
				[StageDescription]	= Source.[StageDescription],
				[EmailSubject]		= Source.[EmailSubject],
				[EmailSubjectBatch]	= Source.[EmailSubjectBatch],
				[EmailMessage]		= Source.[EmailMessage]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (Stage, StageLabel, UICultureName, StageDescription, EmailSubject,
					EmailSubjectBatch, EmailMessage)
				VALUES (Source.Stage, Source.StageLabel, '''+@VISION_LANGUAGE+''', Source.StageDescription, Source.EmailSubject,
					Source.EmailSubjectBatch, Source.EmailMessage)
		WHEN NOT MATCHED BY SOURCE AND Target.UICultureName = '''+@VISION_LANGUAGE+''' THEN DELETE;';
	EXEC (@sSQL);

	COMMIT TRANSACTION;
END;
GO
