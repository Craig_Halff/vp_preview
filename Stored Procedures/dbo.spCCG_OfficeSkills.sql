SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_OfficeSkills] @Org varchar (25)
AS 
/* 
Copyright (c) 2019 Central Consulting Group.  All rights reserved.
12/05/2019	David Springer
			Populate UDIC_Office Skills grid
			Call this from an Employee CHANGE workflow when Status has changed
			Call this from an Employee Skills INSERT, CHANGE and DELETE workflow
*/
DECLARE @OfficeCode varchar (4) = substring (@Org, 4, 2)
BEGIN
SET NOCOUNT ON

	Delete s From UDIC_Office o, UDIC_Office_Skills s Where o.UDIC_UID = s.UDIC_UID and o.CustOfficeNumber = @OfficeCode

	Insert Into UDIC_Office_Skills
	(UDIC_UID, Seq, CustSkill, CustSkillQty)
	Select UDIC_UID, Replace (NewID(), '-', ''), Skill, Qty 
	From 
		(Select o.UDIC_UID, s.Skill, count (*) Qty
		From EM e, EMSkills s, UDIC_Office o
		Where e.Status = 'A'
		  and e.Employee = s.Employee
		  and s.PrimaryInd = 'Y'
		  and e.Org not like '02%'
		  and substring (e.Org, 4, 2) = o.CustOfficeNumber
		  and o.CustOfficeNumber = @OfficeCode
		Group by o.UDIC_UID, s.Skill
		) a

	Delete s From UDIC_Office o, UDIC_Office_Skills s Where o.UDIC_UID = s.UDIC_UID and o.CustOfficeNumber = '00'

	Insert Into UDIC_Office_Skills
	(UDIC_UID, Seq, CustSkill, CustSkillQty)
	Select UDIC_UID, Replace (NewID(), '-', ''), Skill, Qty 
	From 
		(Select o.UDIC_UID, s.Skill, count (*) Qty
		From EM e, EMSkills s, UDIC_Office o
		Where e.Status = 'A'
		  and e.Employee = s.Employee
		  and s.PrimaryInd = 'Y'
		  and e.Org not like '02%'
		  and o.CustOfficeNumber = '00'
		Group by o.UDIC_UID, s.Skill
		) a

END
GO
