SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_GetPhaseTaskRows]
	@sRole						varchar(32),
	@prnamelen					int,
	@wbs1						Nvarchar(max),		-- Can be CSV list of WBS1's
	@wbs2						Nvarchar(max),		-- Can be CSV list of WBS2's
	@ExcludeDormant				char(1) = 'N',
	@WBS3forWBS1s				char(1) = 'N',
	@quickLoad					int = 0					-- 0 or 1 
AS
BEGIN
	/*
	EXEC [dbo].[spCCG_EI_GetPhaseTaskRows] 'ACCT', 40, '2002010.00', '002', 'Y', 'N', 0
	*/
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET @wbs1 = ISNULL(@wbs1, '')
	SET @wbs2 = ISNULL(@wbs2, '')

	declare @level int = case when @wbs2 <> '' then 3 else 2 end
	declare @ColumnsActiveSql Nvarchar(max) = '', @gridCustFieldsSelectSql Nvarchar(max) = '', @custFieldsGroupSql varchar(max) = '', @roleFieldsSql varchar(max) = ''
	exec [spCCG_EI_GetSQLCustomColumns] 0, @level, @sRole, 0, @quickLoad, 
		@sqlPart = @gridCustFieldsSelectSql OUTPUT, @outerApplies = @ColumnsActiveSql OUTPUT, @sqlGroupBy = @custFieldsGroupSql OUTPUT

	DECLARE @sSQL nvarchar(max)
	DECLARE @safeSql	bit

	if CHARINDEX(',', @wbs1) > 0 set @wbs1 = ' in (N'+replace(@wbs1,''',''', ''',N''')+')'
	else if @wbs1 <> '' set @wbs1 = ' = N'''+@wbs1+''''
	if CHARINDEX(',', @wbs2) > 0 set @wbs2 = ' in (N'+replace(@wbs2,''',''', ''',N''')+')'
	else if @wbs2 <> '' set @wbs2 = ' = N'''+@wbs2+''''

	SET @sSQL = N'
		SELECT PR.WBS1, PR.WBS2, PR.WBS1 + ''|'' + PR.WBS2 as WBS2Key, PR.WBS3,
				LEFT(PR.Name, ' + Cast(@prnamelen as nvarchar(10)) + N') as Name, PR.SubLevel ' + @gridCustFieldsSelectSql + N'
			FROM PR
				LEFT JOIN CCG_EI_CustomColumns EI_Cust on EI_Cust.WBS1 = PR.WBS1 ' +
		(CASE WHEN @wbs2 = '' THEN N'
					and EI_Cust.WBS2 = PR.WBS2 and EI_Cust.WBS3 = N'' '' '
		ELSE N'
					and EI_Cust.WBS2 = PR.WBS2 and EI_Cust.WBS3 = PR.WBS3 '
		END) +
				@ColumnsActiveSql + N'
			WHERE PR.WBS1' + @wbs1 +
		(CASE WHEN @wbs2 = '' THEN N'
				and PR.WBS2 <> N'' '' '+(case when @WBS3forWBS1s='N' then ' and PR.WBS3 = N'' ''' else ' and PR.WBS3 <> N'' ''' end)
		ELSE N'
				and PR.WBS2' + @wbs2 + N' and PR.WBS3 <> N'' '' '
		END)

	IF @ExcludeDormant = 'Y' SET @sSQL = @sSQL + N'
				and PR.Status <> ''D'' '
	SET @sSQL = @sSQL + N'
			ORDER BY PR.WBS1, PR.WBS2, PR.WBS3'

	--PRINT @sSQL
	EXEC (@sSQL)
END
GO
