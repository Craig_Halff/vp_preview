SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_UltraGetOptionDetails] (
	@employeeId			Nvarchar(32),
	@type				varchar(max) = 'GridOptions'
)
AS
BEGIN
	/* Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved. */
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	select seq, Detail
		from CCG_EI_ConfigOptions
		where changedBy = @employeeId and type = @type
		order by datechanged desc
END
GO
