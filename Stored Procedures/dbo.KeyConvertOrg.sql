SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[KeyConvertOrg]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@UserName Nvarchar(32) = 'VisionKeyConvert', -- Add by DP 4.0
@Period integer = 0,
@EntityType Nvarchar(20)
as
begin -- Procedure
	set nocount on 
	declare @OldValue Nvarchar(20)
	declare @NewValue Nvarchar(20)
	Declare @Existing integer
	declare @Entity Nvarchar(20)
	declare @length integer
	declare @CvtType integer
	declare @KeysFetch integer
	declare @RetVal integer
	declare @diag smallint
	declare @Errmsg2 Nvarchar(1000),
			@custlabel	Nvarchar(40),
			@Custlabelplural Nvarchar(40)
	declare @MulticompanyEnabled Nvarchar(1)
	declare @MulticurrencyEnabled Nvarchar(1)
	declare @NewFunctionalCurrencyCode Nvarchar(3)
	declare @OldFunctionalCurrencyCode Nvarchar(3)
	declare @OldCompany Nvarchar(14)
	declare @NewCompany Nvarchar(14)
	declare @message nvarchar(max) 
	declare @OldOrgStatus Nvarchar(1)
	declare @NewOrgStatus Nvarchar(1)


	declare @temp Nvarchar(14)
	declare  @OrgLevels smallint
	declare 	@Org1Start                 smallint,
				@Org1Length                smallint,
				@Org2Start                 smallint,
				@Org2Length                smallint,
				@Org3Start                 smallint,
				@Org3Length                smallint,
				@Org4Start                 smallint,
				@Org4Length                smallint,
				@Org5Start                 smallint,
				@Org5Length                smallint
	set @diag = 0
	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'Org'
	set @length = 20
	set @Existing = 0
	set @KeysFetch = 0

	execute InsertKeyCvtDriver @Entity -- populate driver table

-- Add by DP 4.0
	delete from keyconvertDriver where entity = N'Org' and TableName = N'Organization' and ColumnName = N'Org'
	DECLARE @OldName Nvarchar(125)
	DECLARE @NewName Nvarchar(125)
	DECLARE @PKey Nvarchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--
	select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'orgLabel'
	select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'orgLabelPlural'


	select 	@Org1Start 	= Org1Start,
				@Org1Length =Org1Length,
				@Org2Start 	=Org2Start,
				@Org2Length =Org2Length,
				@Org3Start 	=Org3Start,
				@Org3Length =Org3Length,
				@Org4Start 	=Org4Start,
				@Org4Length =Org4Length,
				@Org5Start 	=Org5Start,
				@Org5Length =Org5Length,
				@OrgLevels = OrgLevels from CFGFormat


	select @MulticompanyEnabled = MulticompanyEnabled,
   		 @MulticurrencyEnabled = MulticurrencyEnabled from FW_CFGSystem

	declare KeysCursor cursor for
		select OldKey, NewKey, Pkey from KeyConvertWork where Entity = @EntityType
	open KeysCursor
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue, 
		@PKey

	set @KeysFetch = @@Fetch_Status

While (@KeysFetch = 0)
	begin
		begin tran
-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @EntityType)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),'KC','Y',  @custlabel + ' Key Convert ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, left(@custlabel + ' Key Convert', 40), @custlabel);
			end

			Set @PostControlFlag = 1
		end

		set @Existing = 0
		select @Existing =  1, @NewName = Name, @NewOrgStatus = Status from Organization where Org = @NewValue
		select @OldName = Name,@OldOrgStatus = Status from Organization where Org = @OldValue
		select @OldCompany = substring(@OldValue,@Org1Start,@Org1Length)
		select @NewCompany = substring(@newValue,@Org1Start,@Org1Length)
--
	If (@Existing = 1)
		begin
			  if (@DeleteExisting = 0)
				begin
				SET @message = dbo.GetMessage('MsgNumExistConvNotContinueOverwrite',@custlabel,@NewValue,@custlabel,'','','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end
			if (@NewValue = @OldValue)
				begin
				SET @message = dbo.GetMessage('MsgAreSameNumConvNotCont',@custlabelPlural,'','','','','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end
				
			if (@OldOrgStatus <> @NewOrgStatus)
				begin
				SET @message = dbo.GetMessage('MsgTwoOrgHasDifferentStatus','','','','','','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end

			if (@MulticurrencyEnabled = 'Y')
			  begin
				   select @NewFunctionalCurrencyCode = FunctionalCurrencyCode from cfgMainData where Company =  @newCompany
					select @OldFunctionalCurrencyCode = FunctionalCurrencyCode from cfgMainData where Company =  @oldCompany

					if (@NewFunctionalCurrencyCode <> @OldFunctionalCurrencyCode)
						begin
						SET @message = dbo.GetMessage('MsgCvtOrgCompanyCodeNotSameFunct',@newCompany,@oldcompany,@OldFunctionalCurrencyCode,@NewFunctionalCurrencyCode,'','','','','')
						RAISERROR(@message,16,3)
							if (@@Trancount > 0)
							   ROLLBACK TRANSACTION
							return(50002) -- user defined error
						end
			  end
/* Removed by DP 4.0
			  --disables constraints
			 			  exec @RetVal = KeyCvt @Entity = @Entity,
											@CvtType = @CvtType,
											@Length = 14,
											@ErrMsg = @ErrMsg2 output,
											@Diag = @Diag,
											@ConstrFlag = 3 -- UpdateTables
*/
			update Organization set
				Organization.PlugAmtRev = Organization.PlugAmtRev + old.PlugAmtRev,
				Organization.PlugAmtDirLab = Organization.PlugAmtDirLab + old.PlugAmtDirLab,
				Organization.PlugAmtDirExp = Organization.PlugAmtDirExp + old.PlugAmtDirExp,
				Organization.PlugAmtIndLab = Organization.PlugAmtIndLab + old.PlugAmtIndLab,
				Organization.PlugAmtIndExp = Organization.PlugAmtIndExp + old.PlugAmtIndExp
			from Organization old, Organization
			where Organization.org = @NewValue
					and old.org = @OldValue
-- Removed by DP
--	        Delete From Organization Where Org = @OldValue

		delete from FirmOrgAssociations where org = @OldValue
			and exists (select 'x' from FirmOrgAssociations tmp where tmp.firmID = FirmOrgAssociations.firmID and tmp.org = @NewValue)

			delete from ApprovalRoleAssignment where org = @OldValue
				and exists (select 'x' from ApprovalRoleAssignment tmp where tmp.RoleID = ApprovalRoleAssignment.RoleID and tmp.org = @NewValue)

		END --Existing
	else	--See if individual levels need to be added to CFGOrgCodes
		Begin
			select * into #TempKeyConvert from Organization where Org = @OldValue
			Update #TempKeyConvert set Org=@NewValue
			Insert Organization select * from #TempKeyConvert
			Drop Table #TempKeyConvert
			set @NewName = @OldName

			declare @count smallint
			set @count = 1

			while (@count <= @OrgLevels)
			begin
				if (@count = 1)
					set @Temp = substring(@NewValue,@Org1Start,@Org1Length)
				else if(@count = 2)
					set @Temp = substring(@NewValue,@Org2Start,@Org2Length)
				else if(@count = 3)
					set @Temp = substring(@NewValue,@Org3Start,@Org3Length)
				else if(@count = 4)
					set @Temp = substring(@NewValue,@Org4Start,@Org4Length)
				else if(@count = 5)
					set @Temp = substring(@NewValue,@Org5Start,@Org5Length)
					if not exists (select 'x' from CFGOrgCodes where (orgLevel = @Count) and (Code = @Temp))
						begin
							--print convert(Nvarchar,@count) + ' ' + @Temp

							if (@MulticompanyEnabled = 'Y' and @Count = 1)
								begin
							   	RAISERROR ('Company Code: %s does not exist.  Conversion will not continue.  To complete this convert create the company code first.',
							      16, 3,  @Temp)
									if (@@Trancount > 0)
									   ROLLBACK TRANSACTION
									return(50002) -- user defined error					
						      end
							Insert into CFGOrgCodes 
							(OrgLevel,
							 Code,
							 Label)
							Values   
							(@Count,
							 @Temp,
							'Conversion')
						end
					 else if (@MulticurrencyEnabled = 'Y')
					   begin
						   select @NewFunctionalCurrencyCode = FunctionalCurrencyCode from cfgMainData where Company =  @newCompany
							select @OldFunctionalCurrencyCode = FunctionalCurrencyCode from cfgMainData where Company =  @oldCompany

							if (@NewFunctionalCurrencyCode <> @OldFunctionalCurrencyCode)
								begin
							   	RAISERROR ('Company Code: %s does not have the same functional currency as %s.  Conversion will not continue. Old currency:%s New currency:%s.',
							      16, 3,  @newCompany,@oldcompany,@OldFunctionalCurrencyCode,@NewFunctionalCurrencyCode)
									if (@@Trancount > 0)
									   ROLLBACK TRANSACTION
									return(50002) -- user defined error
								end

					   end
					set @Count = @Count + 1
			end--while
		end	--check for subcodes


		exec @RetVal = KeyCvt @Entity = @Entity,
					 @CvtType = @CvtType,
					 @Length = 14,
					 @ErrMsg = @ErrMsg2 output,
					 @Diag = @Diag,
					 @NewValue = @NewValue,
					 @OldValue = @OldValue,
					 @ConstrFlag = 3 -- UpdateTables
-- Added by DP
	        Delete From Organization Where Org = @OldValue
--
		if (@RetVal = 0)
			begin
-- Add by DP 4.0
				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq from keyConvertWork where Pkey = @PKey
				end
--
				delete from keyConvertWork where current of keysCursor
				commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
				rollback transaction
			end
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue,
		@PKey
	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	return(0)
End -- Procedure


GO
