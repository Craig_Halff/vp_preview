SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_FleetMileageTransactionsPosted]
AS
/*
Copyright 2019 (c) Central Consulting Group.   All rights reserved.
08/08/2019	David Springer
			Update FleetMileage Posted 
			Call this from a Fleet scheduled workflow button.
*/
SET NOCOUNT ON
BEGIN

	Update m
	Set m.CustPosted = 'Y'
	From UDIC_FleetManagement f
		Join UDIC_FleetManagement_MileageLog m on m.UDIC_UID = f.UDIC_UID and m.CustPosted = 'N'
		join LedgerMisc lu on lu.Unit = f.CustUnit and lu.TransDate = m.CustDate and lu.UnitQuantity = m.CustMiles and lu.TransType = 'UN'
		Join UDIC_ProjectLowestLevel l on l.UDIC_UID = m.CustProject and l.CustWBS1 = lu.WBS1 and l.CustWBS2 = lu.WBS2 and l.CustWBS3 = lu.WBS3
		Left Join PR p on p.WBS1 = l.CustWBS1 and p.WBS2 = l.CustWBS2 and p.WBS3 = l.CustWBS3 and p.UnitTable = lu.UnitTable
END
GO
