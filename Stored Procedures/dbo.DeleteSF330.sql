SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteSF330] @SF330ID varchar(32)
AS
    delete from SF330PersonnelProjectMatrix where SF330ID = @SF330ID
    delete from SF330PersonnelMatrix where SF330ID = @SF330ID

    delete from SF330PersonnelProjects where SF330ID = @SF330ID
    delete from SF330Personnel where SF330ID = @SF330ID

    delete from SF330ProjectFirms where SF330ID = @SF330ID
    delete from SF330ProjectGraphics where SF330ID = @SF330ID
    delete from SF330Projects where SF330ID = @SF330ID

    delete from SF330FirmEmployeeDiscipline where SF330ID = @SF330ID
    delete from Sf330FirmProfile where SF330ID = @SF330ID
    delete from SF330FirmsPart2 where SF330ID = @SF330ID

    delete from SF330ProposedFirms where SF330ID = @SF330ID
GO
