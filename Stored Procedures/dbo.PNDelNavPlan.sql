SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PNDelNavPlan]
  @strPlanID varchar(32)
AS

BEGIN -- Procedure PNDelNavPlan

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete data from PN tables for a Navigator Plan.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  
  DELETE PNCalendarInterval WHERE PlanID = @strPlanID
  DELETE PNAccordionFormat WHERE PlanID = @strPlanID
  DELETE PNWBSLevelFormat WHERE PlanID = @strPlanID
  DELETE PNPlannedLabor WHERE PlanID = @strPlanID
  DELETE PNPlannedExpenses WHERE PlanID = @strPlanID
  DELETE PNPlannedConsultant WHERE PlanID = @strPlanID
  DELETE PNBaselineLabor WHERE PlanID = @strPlanID
  DELETE PNBaselineExpenses WHERE PlanID = @strPlanID
  DELETE PNBaselineConsultant WHERE PlanID = @strPlanID
  DELETE PNAssignment WHERE PlanID = @strPlanID
  DELETE PNExpense WHERE PlanID = @strPlanID
  DELETE PNConsultant WHERE PlanID = @strPlanID
  DELETE PNTask WHERE PlanID = @strPlanID
  DELETE PNPlan WHERE PlanID = @strPlanID
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF

END -- PNDelNavPlan
GO
