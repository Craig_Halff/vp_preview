SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_SPRouteHalff] ( @PayableSeq int, @Route varchar(50), @NextOrder int, @Option1 varchar(50), @Option2 varchar(50))
             AS EXEC spCCG_PAT_SPRouteHalff @PayableSeq,@Route,@NextOrder,@Option1,@Option2
GO
