SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_FleetAsset] @EquipmentID varchar (32), @FleetID varchar (32)
AS
/*
Copyright 2019 (c) Central Consulting Group.   All rights reserved.
08/08/2019	David Springer
			Update Fleet Management fields from Asset/Equipment
			Call from Asset CHANGE workflows.
*/
BEGIN
SET NOCOUNT ON

	Update f
	Set f.CustAsset = e.EquipmentNumber, 
		f.CustVIN = e.SerialNumber,
		f.CustOrganization = p.Org,
		f.CustStatus = e.AssetStatus,
		f.CustActive = e.AcquisitionDate,
		f.CustDisposed = e.DisposalDate
	From UDIC_FleetManagement f, Equipment e
		Left Join PR p on p.WBS1 = e.WBS1 and p.WBS2 = e.WBS2 and p.WBS3 = e.WBS3
	Where f.UDIC_UID = @FleetID
	  and e.EquipmentID = @EquipmentID

END
GO
