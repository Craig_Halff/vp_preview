SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_UltraGridCustomLoad]
	@Column		varchar(50)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	If @Column = 'InitialValue' OR @Column = 'Link'
		SELECT name, name FROM sys.objects
			WHERE name like '%CCG_PAT_%' AND type in (N'FN', N'IF', N'TF', N'FS', N'V', N'FT')
				and name not in ('fnCCG_PAT_projectfilter')
			ORDER BY 1
	ELSE IF @Column = 'DatabaseField'
		SELECT syscolumns.name as KeyValue, syscolumns.name as Display
            FROM sysobjects inner join syscolumns on syscolumns.id = sysobjects.id
            WHERE sysobjects.Name = 'CCG_PAT_CustomColumns' and sysobjects.xtype = 'U'
                and not syscolumns.name in ('PayableSeq', 'WBS1', 'WBS2', 'WBS3')
				and not syscolumns.name like '%_ModEmp'
            ORDER BY syscolumns.name
END;

GO
