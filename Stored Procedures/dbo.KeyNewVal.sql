SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[KeyNewVal]
	@ColumnName 	Nvarchar(100),
	@cvtType			integer, -- 0 oldVal -> NewVal; 1 --Length Change; 2 --Remove leading zeros;3 --length Change no zeros
	@Length 			integer,
   @ChangeSide	   Nvarchar(5) = 'Left', --left or right
	@RetValue 		Nvarchar(500) output,
	@Delim1			Nvarchar(1) = '',
	@DelimPos1  	integer = 0,
	@Delim2			Nvarchar(1) = '',
	@DelimPos2  	integer = 0,
	@OldDelim1		Nvarchar(1) = '',
	@OldDelimPos1  integer = 0,
	@OldDelim2		Nvarchar(1) = '',
	@OldDelimPos2  integer = 0,
	@NewValue 		Nvarchar(32) = '',
	@NewValue2		Nvarchar(7) = null,
	@NewValue3		Nvarchar(7) = null,
	@OldValue 		Nvarchar(32) = '',
	@OldValue2		Nvarchar(7) = null,
	@OldValue3		Nvarchar(7) = null
as
declare @modColName Nvarchar(200)
declare @minusVal integer
declare @ProjColName Nvarchar(40)
declare @PhaseColName Nvarchar(40)
set @minusVal = len(@Delim1 + @Delim2)

declare @dbisunicode	varchar(1)
select @dbisunicode = case data_type when 'nvarchar' then 'Y' when 'varchar' then 'N' else NULL end 
from Information_Schema.Columns where Table_Name = 'FW_CFGGridSettings' and column_name = 'ColList'

declare @NPrefix	varchar(1)
if @dbisunicode = 'Y' 
	Set @NPrefix = 'N'
else
	Set @NPrefix = ''

begin
	if (@cvtType = 1) --Length/format change
		begin
			if (@ChangeSide = 'Left')
				begin
					if (@DelimPos1 > 0 and @DelimPos2 > 0 )
						begin
							set @modColname= 'Right( replicate(''0'',30) + replace(replace(' + @ColumnName+ ',''' + @oldDelim1 + ''',''''),''' + @oldDelim2 + ''',''''),' + convert(Nvarchar,@Length -@minusVal) + ')'
							select @RetValue = 'left(' + @modColname+ ',' + convert(Nvarchar,@DelimPos1) + ' - 1) + ''' + @Delim1 + ''' + substring(' + @modColname+ ',' + convert(Nvarchar,@delimPos1) + ',' + convert(Nvarchar,@delimPos2 - @delimPos1 - 1) + ') + ''' + @Delim2 + ''' + right(' + @modColname+ ',len(' + @modColname+ ') - (' + convert(Nvarchar,@DelimPos2) + '-2)) '
							--select @RetValue = 'x'
						end
					else if (@DelimPos1 > 0)
						begin
							set @modColname= 'Right( replicate(''0'',30) + replace(replace(' + @ColumnName+ ',''' + @oldDelim1 + ''',''''),''' + @oldDelim2 + ''',''''),' + convert(Nvarchar,@Length -@minusVal) + ')'																					--len(Right( replicate('0',30) + XChargeOHCreditAccount,) - (5-1))
		--					set @modColname= 'Right( replicate(''0'',30) + replace(' + @ColumnName+ ',''' + @oldDelim1 + ''',''''),' + convert(Nvarchar,@Length -@minusVal) + ')'
							--select @RetValue = 'left(''0'' + replace(' + @ColumnName + ',''' + @oldDelim1 + ''',''''),' + convert(Nvarchar,@DelimPos1) + ' - 1) + ''' + @Delim1 + ''' + right(replace(' + @ColumnName + ',''' + @oldDelim1 + ''',''''),len(' + @columnname + ') - (' + convert(Nvarchar,@DelimPos1) + '-1))' 
							select @RetValue = 'left(' + @modColname+ ',' + convert(Nvarchar,@DelimPos1) + ' - 1) + ''' + @Delim1 + ''' + right(' + @modColname+ ',len(' + @modColname+ ') - (' + convert(Nvarchar,@DelimPos1) + '-1)) '
						end
					else
						set @RetValue = 'Right( replicate(''0'',30) + replace(' + @ColumnName + ',''' + @oldDelim1 + ''',''''),' + convert(Nvarchar,@Length) + ') ' 
				end
			if (@ChangeSide = 'Right')
				begin
					if (@DelimPos1 > 0 and @DelimPos2 > 0 )
						begin
							set @modColname= 'left(replace(replace(' + @ColumnName+ ',''' + @oldDelim1 + ''',''''),''' + @oldDelim2 + ''','''') + replicate(''0'',30),' + convert(Nvarchar,@Length -@minusVal) + ')'
							select @RetValue = 'left(' + @modColname+ ',' + convert(Nvarchar,@DelimPos1) + ' - 1) + ''' + @Delim1 + ''' + substring(' + @modColname+ ',' + convert(Nvarchar,@delimPos1) + ',' + convert(Nvarchar,@delimPos2 - @delimPos1 - 1) + ') + ''' + @Delim2 + ''' + right(' + @modColname+ ',len(' + @modColname+ ') - (' + convert(Nvarchar,@DelimPos2) + '-2)) '
							--select @RetValue = 'x'
						end
					else if (@DelimPos1 > 0)
						begin
							set @modColname= 'left( replace(replace(' + @ColumnName+ ',''' + @oldDelim1 + ''',''''),''' + @oldDelim2 + ''','''') + replicate(''0'',30),' + convert(Nvarchar,@Length -@minusVal) + ')'																					--len(Right( replicate('0',30) + XChargeOHCreditAccount,) - (5-1))
		--					set @modColname= 'Right( replicate(''0'',30) + replace(' + @ColumnName+ ',''' + @oldDelim1 + ''',''''),' + convert(Nvarchar,@Length -@minusVal) + ')'
							--select @RetValue = 'left(''0'' + replace(' + @ColumnName + ',''' + @oldDelim1 + ''',''''),' + convert(Nvarchar,@DelimPos1) + ' - 1) + ''' + @Delim1 + ''' + right(replace(' + @ColumnName + ',''' + @oldDelim1 + ''',''''),len(' + @columnname + ') - (' + convert(Nvarchar,@DelimPos1) + '-1))' 
							select @RetValue = 'left(' + @modColname+ ',' + convert(Nvarchar,@DelimPos1) + ' - 1) + ''' + @Delim1 + ''' + right(' + @modColname+ ',len(' + @modColname+ ') - (' + convert(Nvarchar,@DelimPos1) + '-1)) '
						end
					else
						set @RetValue = 'Left(replace(' + @ColumnName + ',''' + @oldDelim1 + ''','''') + replicate(''0'',30) ,' + convert(Nvarchar,@Length) + ')' 
				end
		end
	else if (@cvtType = 2) --Remove leading zeros
		begin
			set @modColName = @ColumnName
			if (@OldDelim1 <> '' or @OldDelim2 <> '')
				set @modColname= 'Replace(Replace(' + @ColumnName + ',''' + @OldDelim1 + ''',''''),''' + @OldDelim2 + ''','''')'
			set @RetValue = 'case LTrim(Replace('+  @modColname+ ',''0'','' '')) when '''' then ''0'' else replace(LTrim(Replace('+  @modColname+ ',''0'','' '')),'' '',''0'') end '
		end
	else if (@cvtType = 3) --Length change no zeros
		begin
			set @modColName = @ColumnName
			set @RetValue = 'left(' + @modcolName + ',' + convert(Nvarchar,@Length) + ')'
		end
	else -- OldValue -> New Value
		begin
			if (@NewValue2 is null)
				begin
					set @RetValue = @NPrefix + '''' +  REPLACE(@NewValue,'''','''''') + ''' WHERE ' + @ColumnName + ' = ' + @NPrefix + '''' + REPLACE(@OldValue,'''','''''') + ''''
				end
			else if(@NewValue3 is Null)
				begin
					set @ProjColName = Replace(@ColumnName,'WBS2','WBS1')
					if @ColumnName = 'Key2' set @ProjColName = Replace(@ColumnName,'Key2','Key1')					
					if (@ColumnName in ('WBS2ToPost','RevUpsetWBS2','IntercompanyTaxWBS2ToPost','PreInvWBS2ToPost'))
						begin
							set @ProjColName = 'WBS1'
							set @RetValue = @NPrefix + '''' +  REPLACE(@NewValue2,'''','''''') + ''' Where ' + @ProjColName +  ' = ' + @NPrefix + '''' + REPLACE(@OldValue,'''','''''') + ''' and isNull(' + @ColumnName +  ','' '') = ' + @NPrefix + '''' + REPLACE(@OldValue2,'''','''''') + ''''
						end
					else
						set @RetValue = @NPrefix + '''' +  REPLACE(@NewValue2,'''','''''') + ''',' + @ProjColName + ' = ' + @NPrefix + '''' + REPLACE(@NewValue,'''','''''') + ''' Where ' + @ProjColName +  ' = ' + @NPrefix + '''' + REPLACE(@OldValue,'''','''''') + ''' and isNull(' + @ColumnName +  ','' '') = ' + @NPrefix + '''' + REPLACE(@OldValue2,'''','''''') + ''''
				end
			else
				begin
					set @ProjColName = Replace(@ColumnName,'WBS3','WBS1')
					if @ColumnName = 'Key3' set @ProjColName = Replace(@ColumnName,'Key3','Key1')	
									
					set @PhaseColname =  Replace(@ColumnName,'WBS3','WBS2')
					if @ColumnName = 'Key3' set @PhaseColname =  Replace(@ColumnName,'Key3','Key2')		
								
					if (@ColumnName in ('WBS3ToPost','RevUpsetWBS3','IntercompanyTaxWBS3ToPost','PreInvWBS3ToPost'))
						begin
							set @ProjColName = 'WBS1'
							set @RetValue = @NPrefix + '''' +  REPLACE(@NewValue3,'''','''''') + ''',' + @PhaseColname + ' = ' + @NPrefix + '''' + REPLACE(@NewValue2,'''','''''') + ''' Where ' + @ProjColName +  ' = ' + @NPrefix + '''' + REPLACE(@OldValue,'''','''''') + ''' and isNull(' + @PhaseColName +  ','' '') = ' + @NPrefix + '''' + REPLACE(@OldValue2,'''','''''') + ''' and isNull(' + @ColumnName +  ','' '') = ' + @NPrefix + '''' + REPLACE(@OldValue3,'''','''''') + ''''
						end
					else
						set @RetValue = @NPrefix + '''' +  REPLACE(@NewValue3,'''','''''') + ''',' + @PhaseColname + ' = ' + @NPrefix + '''' + REPLACE(@NewValue2,'''','''''') + ''',' + @ProjColName + ' = ' + @NPrefix + '''' + REPLACE(@NewValue,'''','''''') + ''' Where ' + @ProjColName +  ' = ' + @NPrefix + '''' + REPLACE(@OldValue,'''','''''') + ''' and isNull(' + @PhaseColName +  ','' '') = ' + @NPrefix + '''' + REPLACE(@OldValue2,'''','''''') + ''' and isNull(' + @ColumnName +  ','' '') = ' + @NPrefix + '''' + REPLACE(@OldValue3,'''','''''') + ''''
				end
		end
end
GO
