SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_PendingInsert] ( @EmployeeID nvarchar(32), @PayableSeq int, @Route varchar(128)=null, @Description nvarchar(128)=null, @Parallel char(1)= 'N', @SortOrder int= 0)
             AS EXEC spCCG_PAT_PendingInsert @EmployeeID,@PayableSeq,@Route,@Description,@Parallel,@SortOrder
GO
