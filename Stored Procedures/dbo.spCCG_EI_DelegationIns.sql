SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_DelegationIns] (
	@Id				varchar(32),
	@Username		Nvarchar(32),
	@Employee		Nvarchar(20),
	@Delegate		Nvarchar(20),
	@FromDate		varchar(20),
	@ToDate			varchar(20),
	@ForClientId	varchar(32),
	@ForWBS1		Nvarchar(30),
	@Dual			char(1),
	@ApprovedBy		Nvarchar(20),
	@ApprovedOn		datetime
)
AS
BEGIN
/*
	Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved.

	exec spCCG_EI_DelegationIns '1','ADMIN','00001','00003','3/1/2017','3/8/2017',0,'N',null,null,'N'
	select * from CCG_EI_Delegation
	select * from CCG_EI_HistoryDelegation
	exec spCCG_EI_DelegationIns '2','ADMIN','00001','00003','3/1/2017','3/8/2017',0,'N','00001','2/27/2017','N'
	select * from CCG_EI_Delegation
	select * from CCG_EI_HistoryDelegation
*/
	SET NOCOUNT ON
	BEGIN TRANSACTION
	BEGIN TRY
		if isnull(@Id, '') = '' set @Id = Replace(Cast(newid() as varchar(36)),'-','')
		insert into CCG_EI_Delegation (Id,Employee,Delegate,FromDate,ToDate, ForClientId, ForWBS1, /* MaxAmount, */ Dual,ApprovedBy,ApprovedOn)
			values (@Id,@Employee,@Delegate,@FromDate,@ToDate, isnull(@ForClientId,''), isnull(@ForWBS1,N''), /* NULLIF(@MaxAmount,0), */ @Dual,@ApprovedBy,NULLIF(@ApprovedOn,''))

		insert into CCG_EI_HistoryDelegation (Id,Employee,Delegate,FromDate,ToDate, ForClientId, ForWBS1, ActionTaken,ActionDate,ActionTakenBy,ActionRecipient,Description)
			select @Id,@Employee,@Delegate,@FromDate,@ToDate, isnull(@ForClientId,''), isnull(@ForWBS1,N''), 'Created',getutcdate(),Employee,null, N'Dual = ' + @Dual
			from SEUser where Username=@Username

		if IsNull(@ApprovedBy,N'')<>N'' and @ApprovedOn is not null
			insert into CCG_EI_HistoryDelegation (Id,Employee,Delegate,FromDate,ToDate, ForClientId, ForWBS1, ActionTaken,ActionDate,ActionTakenBy,ActionRecipient,Description)
				select @Id,@Employee,@Delegate,@FromDate,@ToDate, isnull(@ForClientId,''), isnull(@ForWBS1,N''), 'Approved',getutcdate(),Employee,null,N'Approver = N' + @ApprovedBy + N', Approved On = ' + Convert(Nvarchar, @ApprovedOn, 101)
				from SEUser where Username=@Username
		select 0 as Result, @id as ErrorMessage

		COMMIT TRANSACTION
		select 0 as result, '' as errormessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		select -1 as Result, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage
	END CATCH
END
GO
