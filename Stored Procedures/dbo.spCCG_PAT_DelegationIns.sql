SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_PAT_DelegationIns] (
	@Id			varchar(32),
	@Username	Nvarchar(32),
	@Employee	Nvarchar(20),
	@Delegate	Nvarchar(20),
	@FromDate	datetime,
	@ToDate		datetime,
	@MaxAmount	money,
	@Dual		char(1),
	@ApprovedBy Nvarchar(20),
	@ApprovedOn datetime,
	@Permanent	char(1))			-- NOT USED LIKE THE OTHER PARAMETERS
AS BEGIN
/*
	Copyright (c) 2020 EleVia Software. All rights reserved.

	exec spCCG_PAT_DelegationIns '1','ADMIN','00001','00003','3/1/2017','3/8/2017',0,'N',null,null,'N'
	select * from CCG_PAT_Delegation
	select * from CCG_PAT_HistoryDelegation
	exec spCCG_PAT_DelegationIns '2','ADMIN','00001','00003','3/1/2017','3/8/2017',0,'N','00001','2/27/2017','N'
	select * from CCG_PAT_Delegation
	select * from CCG_PAT_HistoryDelegation
*/
	SET NOCOUNT ON
	BEGIN TRY
		if ISNULL(@Id, '') = '' set @Id = Replace(Cast(newid() as varchar(36)),'-','')
		insert into CCG_PAT_Delegation (Id,Employee,Delegate,FromDate,ToDate,MaxAmount,Dual,ApprovedBy,ApprovedOn)
			values (@Id,@Employee,@Delegate,@FromDate,@ToDate,NULLIF(@MaxAmount,0), @Dual,@ApprovedBy,NULLIF(@ApprovedOn,''))

		insert into CCG_PAT_HistoryDelegation (Id,Employee,Delegate,FromDate,ToDate,ActionTaken,ActionDate,ActionTakenBy,ActionRecipient,Description)
			select @Id,@Employee,@Delegate,@FromDate,@ToDate,N'Created',getutcdate(),Employee,null,N'Max Amt = ' + Cast(ISNULL(@MaxAmount,'') as Nvarchar) + ', Dual = ' + @Dual + ', Permanent = ' + @Permanent
			from SEUser where Username=@Username

		if IsNull(@ApprovedBy,'')<>'' and @ApprovedOn is not null
			insert into CCG_PAT_HistoryDelegation (Id,Employee,Delegate,FromDate,ToDate,ActionTaken,ActionDate,ActionTakenBy,ActionRecipient,Description)
				select @Id,@Employee,@Delegate,@FromDate,@ToDate,N'Approved',getutcdate(),Employee,null,N'Approver = ' + @ApprovedBy + ', Approved On = ' + Convert(Nvarchar, @ApprovedOn, 101)
				from SEUser where Username=@Username
		select 0 as Result, @id as ErrorMessage
	END TRY
	BEGIN CATCH
		select -1 as Result, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage
	END CATCH
END
GO
