SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_PAT_GetPOData] (
	@SelectType varchar(20),--KEY,PONUMUMBER,STATUS,VENDOR
	@SelectParam1 varchar(255),@SelectParam2 varchar(255),--depending on SelectType, but will contain the key(s) or status to search for
	@OutputType varchar(20),--HEADER(RS1),DETAIL(RS2),REPORTDETAIL(RS3) inclusive, meaning the more detailed type include the prior.  Each are separate resultsets.  
	@User varchar(50) = null
)
AS
BEGIN
       set nocount on
       declare @mode varchar(10)

       BEGIN TRY            
              if @SelectType = 'KEY' and exists (select [UDIC_UID] from [UDIC_PurchaseOrder] where UDIC_UID = @SelectParam1)
                     set @mode = 'PO' 
			  else if exists (select 'y' FROM [UDIC_PerDiemRequest] WHERE UDIC_UID = @SelectParam1)
					 set @mode = 'PD'
              else if @SelectType = 'KEY'
                     set @mode = 'CHECK'
              else if @SelectType = 'STATUS' and @SelectParam1 = 'BatchLoad'
                     set @mode = 'CHECK'
              else
                     set @mode = 'PO'

              --print @mode
              --exec spCCG_PAT_GetPOData 'KEY','D4F88B734B214D4380F905D313DF443E',null,'HEADER'
              --exec spCCG_PAT_GetPOData 'KEY','DA87E161BDE342E18122B23944266C0D',null,'DETAIL'
              --exec spCCG_PAT_GetPOData 'STATUS','3BE532A1A0874638B0E85D5E3CC65F7D',null,'DETAIL'
              --exec spCCG_PAT_GetPOData 'KEY','3BE532A1A0874638B0E85D5E3CC65F7D',null,'REPORTDETAIL'

            CREATE TABLE #TempSubHeaderCR (
                    RowNumber int,
                    RowOrder int,
                    Label varchar(255),
                    IsColumn varchar(1),--Y if value is columnname from main query
                    Value varchar(max),
                    CellWidth int)--null for auto								  
								                       
            CREATE TABLE #TempPropertiesCR(
                    Property varchar(255),
                    Value varchar(max))

              IF @mode = 'PO'
              BEGIN
                    SELECT [UDIC_UID] as PKey
                             ,[CustPONumber] as PONum--backwards compatable
                             ,[CustPONumber] as PONumber
                             , isnull((select top 1 Seq from CCG_PAT_Payable p where p.POPKey = @SelectParam1), -1) as POSeq --if in PAT include value from CCG_PAT_Payables.Seq
                             ,[CustDateRequested] as PODate
                             ,[CustVendor] as Vendor
                             ,'OPEN' as POStatus
                             ,RTRIM(VE.Name) as VendorName
                             ,[CustRequestedBy] as RequestedByEmp
                             ,EMAttn.FirstName + ' ' + EMAttn.LastName as RequestedByName
                             ,[CustApprovedBy] as ApprovedByEmp
                             ,EMAppr.FirstName + ' ' + EMAppr.LastName as ApprovedByName
                             ,[CustOrderedBy] as OrderedByEmp
                             ,EMOrd.FirstName + ' ' + EMOrd.LastName as OrderByName
                             ,CustDateOrdered as DateOrdered
                             ,CustDateNeeded as DateNeeded
                             ,[CustProject] as Project
                             --,LTRIM(dbo.CCG_StripHTMLTag(Replace([CustCommentsSpecialInstructions],'&nbsp;',' '),'')) as Comments
                             ,PO.[CreateUser]
                             ,PO.[CreateDate]
                             ,PO.[ModUser]
                             ,PO.[ModDate]
                             ,items.TotalPrice as TotalAmount
                             ,items.ItemCount ItemCount
						FROM [UDIC_PurchaseOrder] PO
							LEFT JOIN VE on PO.CustVendor = VE.Vendor
							LEFT JOIN EM EMAppr on EMAppr.Employee = PO.CustApprovedBy
							LEFT JOIN EM EMAttn on EMAttn.Employee = PO.[CustRequestedBy]
							LEFT JOIN EM EMOrd on EMOrd.Employee = PO.CustOrderedBy
							LEFT JOIN (select [UDIC_UID] as POPKey,Sum(CustTotal) as TotalPrice,Count(*) as ItemCount FROM [UDIC_PurchaseOrder_Items] group by [UDIC_UID]) items on PO.UDIC_UID = items.POPKey
						WHERE (@SelectType = 'KEY' and PO.UDIC_UID = @SelectParam1)
                           OR (@SelectType = 'STATUS' )
                           OR (@SelectType = 'VENDOR' and PO.CustVendor =  @SelectParam1)

                     if @OutputType in ('DETAIL') and @SelectType = 'KEY'
                     BEGIN
                           SELECT [UDIC_UID] as POPKey
								,[Seq] as ItemPkey
								,[CustDescription] as ItemDescription
								--,[CustPartModelNumber] as ItemNumber--SB VARCHAR?
								, cast([CustQuantity] as int) as Qty
								--,Cust as UOM
								,[CustAmount] as UnitPrice
								,[CustTotal] as TotalPrice
								,[CreateUser]
								,[CreateDate]
								,[ModUser]
								,[ModDate]
							FROM [UDIC_PurchaseOrder_Items] 
							WHERE (@SelectType = 'KEY' and UDIC_UID = @SelectParam1)                     
                     END
              
                     if @OutputType in ('REPORTDETAIL') and @SelectType = 'KEY'
                     BEGIN
                           --second result set is the detail rows
                           SELECT [CustDescription] as [Item Description]
								,[CustProject] as Project, 'width:120' as Project_Detail
								,[CustPhase] as Phase, 'width:80' as Phase_Detail
								,[CustTask] as Task, 'width:80' as Task_Detail
								--,[CustPartModelNumber] as ItemNumber--SB VARCHAR?
								,cast([CustQuantity] as int) as Qty, 'width:50' as Qty_Detail
								--,Cust as UOM
								,[CustAmount] as UnitPrice, 'width:120' as UnitPrice_Detail
								,[CustTotal] as TotalPrice, 'width:120' as TotalPrice_Detail
								--,[CreateUser]
								--,[CreateDate]
								--,[ModUser]
								--,[ModDate]
								FROM [UDIC_PurchaseOrder_Items]        
								WHERE (@SelectType = 'KEY' and UDIC_UID = @SelectParam1)                     

                           select * from (select top 1 'Vendor' as Label,Name,Address1,Address2,Address3,Address4,ISNULL(City + ', ','') + ISNULL(State + ' ','') + ISNULL(Zip,'') as Address5, Country as Address6,'Phone ' + Phone as Phone  
                                  FROM [UDIC_PurchaseOrder] PO inner join VE on PO.CustVendor = VE.Vendor left join VEAddress on VE.Vendor = VEAddress.Vendor 
                                  WHERE (@SelectType = 'KEY' and UDIC_UID = @SelectParam1) order by PrimaryInd desc,Accounting desc) veaddr
                           UNION ALL
                           select 'Shipping' as Label,FirmName,Address1,Address2,Address3,Address4,null as Address5,null as Address6,FirmPhone
                                  from CFGMainData c where c.Company = (select max(left(PR.Org, 2)) from [UDIC_PurchaseOrder] p inner join PR on PR.WBS1 = p.CustProject and PR.WBS2= ' ' and p.UDIC_UID = @SelectParam1) 
                           UNION ALL
                           select 'Billing' as Label,FirmName,Address1,Address2,Address3,Address4,null as Address5,null as Address6,FirmPhone
                                  from CFGMainData c where c.Company = (select max(left(PR.Org, 2)) from [UDIC_PurchaseOrder] p inner join PR on PR.WBS1 = p.CustProject and PR.WBS2= ' ' and p.UDIC_UID = @SelectParam1) 
                           
                           CREATE TABLE #TempSubHeader(
                                  RowNumber int,
                                  RowOrder int,
                                  Label varchar(255),
                                  IsColumn varchar(1),--Y if value is columnname from main query
                                  Value varchar(max),
                                  CellWidth int)--null for auto
                           insert into #TempSubHeader Values
								(1,1,'P.O. Number','Y', 'PONumber',null)
								,(1,5,'Requested Date','Y', 'PODate',null)
								,(1,5,'Need By Date','Y', 'DateNeeded',null)
								,(2,8,'Requested By','Y', 'RequestedByName',null)            
								,(2,10,'Approved By','Y', 'ApprovedByName',null)                                        
                           select * from #TempSubHeader order by 1,2
                     
                                         
                           CREATE TABLE #TempProperties(
                                  Property varchar(255),
                                  Value varchar(max))
                     
                           insert into #TempProperties Values
								('Colors.ThemeDark', '4282080913')--    c.ToARGB()    4278190335    uint
								,('Colors.ThemeLight', '4293192436')
								--('Colors.ThemeDark', 'DarkRed')--     c.ToARGB()    4278190335    uint
								--,('Colors.ThemeLight', 'Pink')               
								 ,('Colors.FontHeader', 'DarkBlue')
								,('Colors.FontTableHeader', 'Black')
								,('Colors.Font', 'Black')
								,('Font.Style', 'Arial Unicode MS')						   
								--use Image or Title for header
								--,('Header.Image', 'c:\temp\CCG-logo.png')
								--,('Header.Title', 'Halff Associates, Inc.')
								,('Header.DateText', 'Date {0}')
								,('Header.GradientColor', 'White')--if present fades from Theme light to this color, use None for no gradient.
								,('Table.TotalText', 'Total {0}')
								,('Table.FooterText', 'Please include purchase order number on all invoices and packages.')
								,('Footer.PagingTemplate', 'Page [Page #] of [TotalPages]')
								,('Report.Info.Title', 'Purchase Order')--Text in header before PO Number and in Document Properties
								,('Report.Info.Author', 'Central Consulting Group')--Text in Document Properties, default Vision username
								--,('Format.Date', 'dd-MMM-yyyy')
								--,('Format.Currency', 'c2')

						   insert into #TempPropertiesCR
								select top 1 'Header.Title', isnull(cast(orgCode.Label as varchar(100)), 'Halff Associates, Inc.')
									from [UDIC_PurchaseOrder] p 
										inner join PR on PR.WBS1 = p.CustProject and PR.WBS2 = ' ' and PR.WBS3 = ' '
										LEFT JOIN CFGOrgCodesDescriptions orgCode on orgCode.Code = left(PR.Org, 2) and orgCode.OrgLevel = 1 and orgCode.UICultureName = 'en-US'
									where p.UDIC_UID = @SelectParam1

                           select * from #TempProperties
                     
                     END
              END --mode = PO

			ELSE IF @mode = 'PD'
            BEGIN
                SELECT [UDIC_UID] as PKey
                        ,[CustNumber] as PONum--backwards compatable
                        ,[CustNumber] as PONumber
                        ,-1 as POSeq--if in PAT include value from CCG_PAT_Payables.Seq
                        ,[CustRequestDate] as PODate
                        ,[CustPayee] as Vendor
                        ,[CustPDStatus] as POStatus
                        ,RTRIM(VE.Name) as VendorName
						,CustEmployee as Emp
                        ,EM.FirstName + ' ' + EM.LastName as EmpName,
						EM_RB.FirstName + ' ' + EM_RB.LastName as RequestedByName,
						PO.CustNotes,
						PO.CustDueDate,
                        'PD:' + ISNULL(CustNotes,'') as [Description]
						,CustDueDate
						,'$' +  Convert(varchar(20),Convert(int,CustPerDiemHotel)) as CustPerdiemHotel
						,'$' +  Convert(varchar(20),Convert(int,CustPerDiemMeals)) as CustPerDiemMeals
						,'$' + Convert(varchar(20),Convert(int,CustPerDiem)) as CustPerDiem
						,CustCity
                        ,PO.[CreateUser]
                        ,PO.[CreateDate]
                        ,PO.[ModUser]
                        ,PO.[ModDate]
                        ,items.TotalPrice as TotalAmount
                        ,items.ItemCount ItemCount		
						,PO.CustFilePath as [File1]					 
                    FROM [UDIC_PerDiemRequest] PO
						LEFT JOIN VE on PO.CustPayee = VE.Vendor
						LEFT JOIN EM  on EM.Employee = PO.CustEmployee
						LEFT JOIN EM EM_RB on EM_RB.Employee = PO.CustRequestedBy
						LEFT JOIN (select [UDIC_UID] as POPKey,Sum(CustPerDiem) as TotalPrice,Count(*) as ItemCount FROM [UDIC_PerDiemRequest_PerDiemBreakdown] group by [UDIC_UID]) items on PO.UDIC_UID = items.POPKey
					WHERE (@SelectType = 'KEY' and PO.UDIC_UID = @SelectParam1)
                        OR (@SelectType = 'VENDOR' and PO.[CustPayee] =  @SelectParam1)
                     					
				--select * from [UDIC_CheckRequest_Detail]
                if @OutputType in ('DETAIL') and @SelectType = 'KEY'
                BEGIN
                    SELECT [UDIC_UID] as POPKey
							,[Seq] as ItemPkey
							,'Per Diem' as ItemDescription
							,CustProjectNumber as WBS1
							,[CustPhaseNumber] as WBS2
							,[CustTaskNumber] as WBS3
							,null as GLAccount
							,[CustPerDiem] as Amount
							,[CreateUser]
							,[CreateDate]
							,[ModUser]
							,[ModDate]
						FROM [UDIC_PerDiemRequest_PerDiemBreakdown] 
						WHERE (@SelectType = 'KEY' and UDIC_UID = @SelectParam1)                     
                END              

				if @OutputType in ('REPORTDETAIL') and @SelectType = 'KEY'
                BEGIN
                    --second result set is the detail rows

					SELECT --[UDIC_UID] as POPKey
							--,[Seq] as ItemPkey
							Convert(varchar(15), CustDay,101) as [Date]
							,CustProjectNumber as Project
							,[CustPhaseNumber] as Phase
							,[CustTaskNumber] as Task
							-- , Convert(varchar(20),Convert(int,[CustPercentofDay])) + '%' as Pct
							,[CustPerDiem] as Amount
						FROM [UDIC_PerDiemRequest_PerDiemBreakdown] 
						WHERE (@SelectType = 'KEY' and UDIC_UID = @SelectParam1)                                   

                    select * from (select top 1 'Vendor' as Label,Name,Address1,Address2,Address3,Address4,ISNULL(City + ', ','') + ISNULL(State + ' ','') + ISNULL(Zip,'') as Address5, Country as Address6,'Phone ' + Phone as Phone  
                        FROM [UDIC_PerDiemRequest] PO inner join VE on PO.[CustPayee] = VE.Vendor left join VEAddress on VE.Vendor = VEAddress.Vendor 
                        WHERE (@SelectType = 'KEY' and UDIC_UID = @SelectParam1) order by PrimaryInd desc,Accounting desc) veaddr

                    insert into #TempSubHeaderCR Values
						(1,1,'Requested By','Y', 'RequestedByName',null)
						,(1,5,'Requested Date','Y', 'PODate',null)
						,(1,8,'Employee','Y', 'EmpName',null)    
						,(1,10,'City','Y', 'CustCity',null)        
						,(2,1,'Due Date','Y', 'CustDueDate',null)          
						--,(2,5,'Hotel','Y', 'CustPerDiemHotel',null)          
						--,(2,8,'Meals','Y', 'CustPerDiemMeals',null)  
						--,(2,10,'Per Diem','Y', 'CustPerDiem',null)   
						,(3,1,'Notes','Y', 'CustNotes',null)						          
                    select * from #TempSubHeaderCR order by 1,2
                                        
                    insert into #TempPropertiesCR Values
						('Colors.ThemeDark', '4282080913')--    c.ToARGB()    4278190335    uint
						,('Colors.ThemeLight', '4293192436')
						,('Colors.FontHeader', 'DarkBlue')
						,('Colors.FontTableHeader', 'Black')
						,('Colors.Font', 'Black')
						,('Font.Style', 'Arial Unicode MS')
						--use Image or Title for header
						--,('Header.Image', 'c:\temp\CCG-logo.png')
						--,('Header.Title', 'Halff Associates, Inc.')
						,('Header.DateText', 'Date {0}')
						,('Header.GradientColor', 'White')--if present fades from Theme light to this color, use None for no gradient.
						,('Table.TotalText', 'Total {0}')
						--,('Table.FooterText', 'Please include purchase order number on all invoices and packages.')
						,('Footer.PagingTemplate', 'Page [Page #] of [TotalPages]')
						,('Report.Info.Title', 'Per Diem')--Text in header before PO Number and in Document Properties
						,('Report.Info.Author', 'Central Consulting Group')--Text in Document Properties, default Vision username
						--,('Format.Date', 'dd-MMM-yyyy')
						--,('Format.Currency', 'c2')
					insert into #TempPropertiesCR
						select top 1 'Header.Title', isnull(cast(orgCode.Label as varchar(100)), 'Halff Associates, Inc.')
							from [UDIC_PerDiemRequest] P
								LEFT JOIN [UDIC_PerDiemRequest_PerDiemBreakdown] PD on PD.UDIC_UID = P.UDIC_UID
								LEFT JOIN PR on PR.WBS1 = PD.CustProjectNumber and PR.WBS2 = ' ' and PR.WBS3 = ' '
								LEFT JOIN CFGOrgCodesDescriptions orgCode on orgCode.Code = left(PR.Org, 2) and orgCode.OrgLevel = 1 and orgCode.UICultureName = 'en-US'
							where P.UDIC_UID = @SelectParam1

                    select * from #TempPropertiesCR                     
                END
			END--else (mode = PD)
              /********************************/

              ELSE --BEGIN CHECK mode
              BEGIN
                     SELECT [UDIC_UID] as PKey
                             ,[CustNumber] as PONum--backwards compatable
                             ,[CustNumber] as PONumber
                             ,-1 as POSeq--if in PAT include value from CCG_PAT_Payables.Seq
                             ,[CustCheckRequestDate] as PODate
                             ,[CustCheckRequestPayee] as Vendor
                             ,[CustCheckRequestStatus] as POStatus
                             ,RTRIM(VE.Name) as VendorName
                             ,[CustCheckRequestRequestedBy] as RequestedByEmp
                             ,EMAttn.FirstName + ' ' + EMAttn.LastName as RequestedByName
							 , isnull(EMReturnTo.FirstName + ' ' + EMReturnTo.LastName + isnull(' ('+orgCode.Label+')', ''), VE.Name) as SendTo
							 , CustCheckRequestDueDate as DueDate
                             ,[CustCheckRequestNotes] as [Description]                       
                             --,[CustApprovedBy] as ApprovedByEmp
                             --,EMAppr.LastName + ', ' + EMAppr.FirstName as ApprovedByName
                             --,[CustOrderedBy] as OrderedByEmp
                             --,EMOrd.LastName + ', ' + EMOrd.FirstName as OrderByName
                             --,CustDateOrdered as DateOrdered
                             --,CustDateNeeded as DateNeeded
                             --,[CustProject] as Project
                             --,LTRIM(dbo.CCG_StripHTMLTag(Replace([CustCommentsSpecialInstructions],'&nbsp;',' '),'')) as Comments
                             ,PO.[CreateUser]
                             ,PO.[CreateDate]
                             ,PO.[ModUser]
                             ,PO.[ModDate]
                             ,items.TotalPrice as TotalAmount
                             ,items.ItemCount ItemCount
                             ,PO.CustCheckRequestFilePath as [File1]
                       FROM [UDIC_CheckRequest] PO
							LEFT JOIN VE on PO.[CustCheckRequestPayee] = VE.Vendor
							--LEFT JOIN EM EMAppr on EMAppr.Employee = PO.CustApprovedBy
							LEFT JOIN EM EMAttn on EMAttn.Employee = PO.[CustCheckRequestRequestedBy]
							LEFT JOIN EM EMReturnTo on EMReturnTo.Employee = PO.CustCheckRequestReturnto
							LEFT JOIN CFGOrgCodesDescriptions orgCode on orgCode.Code = substring(EMReturnTo.Org, 4, 2) and orgCode.OrgLevel = 2 and orgCode.UICultureName = 'en-US'
							--LEFT JOIN EM EMOrd on EMOrd.Employee = PO.CustOrderedBy
							LEFT JOIN (select [UDIC_UID] as POPKey,Sum(CustTotal) as TotalPrice,Count(*) as ItemCount FROM [UDIC_PurchaseOrder_Items] group by [UDIC_UID]) items on PO.UDIC_UID = items.POPKey
						WHERE (@SelectType = 'KEY' and PO.UDIC_UID = @SelectParam1)
                           OR (@SelectType = 'STATUS' and PO.[CustCheckRequestStatus] =  @SelectParam1)
                           OR (@SelectType = 'VENDOR' and PO.[CustCheckRequestPayee] =  @SelectParam1)
                     
                     if @OutputType in ('DETAIL') and @SelectType = 'KEY'
                     BEGIN
                           SELECT [UDIC_UID] as POPKey
								, [Seq] as ItemPkey
								, [CustCRDDescription] as ItemDescription
								, [CustCRDProjectNumber] as WBS1
								, [CustCRDPhase] as WBS2
								, [CustCRDTask] as WBS3
								, [CustCRDAccount] as GLAccount
								, [CustCRDAmount] as Amount
								, [CreateUser]
								, [CreateDate]
								, [ModUser]
								, [ModDate]
							FROM [UDIC_CheckRequest_Detail] 
							WHERE (@SelectType = 'KEY' and UDIC_UID = @SelectParam1)                     
                     END
              
                     if @OutputType in ('REPORTDETAIL') and @SelectType = 'KEY'
                     BEGIN
                           --second result set is the detail rows
                           SELECT --[UDIC_UID] as POPKey
								--,[Seq] as ItemPkey
								[CustCRDDescription] as Description
								,[CustCRDProjectNumber] as Project, 'width:160' as Project_Detail
								,[CustCRDPhase] as Phase
								,[CustCRDTask] as Task
								,[CustCRDAccount] as Account
								,[CustCRDAmount] as Amount
                           FROM [UDIC_CheckRequest_Detail]   
                           WHERE (@SelectType = 'KEY' and UDIC_UID = @SelectParam1)                                                

                           select * from (select top 1 'Vendor' as Label,Name,Address1,Address2,Address3,Address4,ISNULL(City + ', ','') + ISNULL(State + ' ','') + ISNULL(Zip,'') as Address5, Country as Address6,'Phone ' + Phone as Phone  
                                  FROM [UDIC_CheckRequest] PO inner join VE on PO.[CustCheckRequestPayee] = VE.Vendor left join VEAddress on VE.Vendor = VEAddress.Vendor 
                                  WHERE (@SelectType = 'KEY' and UDIC_UID = @SelectParam1) order by PrimaryInd desc,Accounting desc) veaddr
                           --UNION ALL
                           --select 'Shipping' as Label,'Apple & Bartlett, PC' as Name,'Attn: Grace C.',Address1,Address2,Address3,Address4,null as Address5,'PHONE 617-492-4410' as Phone
                           --     from CFGMainData
                           --UNION ALL
                           --select 'Billing' as Label,'Apple & Bartlett, PC' as Name,Address1,Address2,Address3,Address4,null as Address5,null as Address6,'PHONE 617-492-4410' as Phone
                           --     from CFGMainData
                       
                           insert into #TempSubHeaderCR Values
								(1,1,'Check Request #','Y', 'PONumber',null)
								,(1,5,'Request Date','Y', 'PODate',null)
								,(1,8,'Request By','Y', 'RequestedByName',null)            
								,(1,10,'Send To','Y', 'SendTo',null)            
								,(1,12,'Due Date','Y', 'DueDate',null)            
								,(2,1,'Notes','Y', 'Description',null)                                     
                           select * from #TempSubHeaderCR order by 1,2                                                
                     
                           insert into #TempPropertiesCR Values
								('Colors.ThemeDark', '4282080913')--    c.ToARGB()    4278190335    uint
								,('Colors.ThemeLight', '4293192436')
								--('Colors.ThemeDark', 'DarkRed')--     c.ToARGB()    4278190335    uint
								--,('Colors.ThemeLight', 'Pink')               
								 ,('Colors.FontHeader', 'DarkBlue')
								,('Colors.FontTableHeader', 'Black')
								,('Colors.Font', 'Black')
								,('Font.Style', 'Arial Unicode MS')
								--use Image or Title for header
								--,('Header.Image', 'c:\temp\CCG-logo.png')
								--,('Header.Title', 'Halff Associates, Inc.')
								,('Header.DateText', 'Date {0}')
								,('Header.GradientColor', 'White')--if present fades from Theme light to this color, use None for no gradient.
								,('Table.TotalText', 'Total {0}')
								--,('Table.FooterText', 'Please include purchase order number on all invoices and packages.')
								,('Footer.PagingTemplate', 'Page [Page #] of [TotalPages]')
								,('Report.Info.Title', 'Check Request')--Text in header before PO Number and in Document Properties
								,('Report.Info.Author', 'Central Consulting Group')--Text in Document Properties, default Vision username

							insert into #TempPropertiesCR 
								select top 1 'Header.Title', isnull(cast(orgCode.Label as varchar(100)), 'Halff Associates, Inc.')
									from [UDIC_CheckRequest] p 
										left join [UDIC_CheckRequest_Detail] PD on PD.UDIC_UID = p.UDIC_UID
										left join PR on PR.WBS1 = PD.CustCRDProjectNumber and PR.WBS2 = ' ' and PR.WBS3 = ' '
										LEFT JOIN CFGOrgCodesDescriptions orgCode on orgCode.Code = left(PR.Org, 2) and orgCode.OrgLevel = 1 and orgCode.UICultureName = 'en-US'
									where p.UDIC_UID = @SelectParam1

                           select * from #TempPropertiesCR                     
                     END

              END--else (mode = CHECK)

       END TRY
       BEGIN CATCH
              
              select -1 as Result, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage

       END CATCH

END
GO
