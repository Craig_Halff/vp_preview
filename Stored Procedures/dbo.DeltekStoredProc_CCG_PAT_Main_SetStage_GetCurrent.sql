SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Main_SetStage_GetCurrent] ( @seq int, @pendingseq int= 0, @modDate datetime, @oldStage varchar(200))
             AS EXEC spCCG_PAT_Main_SetStage_GetCurrent @seq,@pendingseq,@modDate,@oldStage
GO
