SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPShiftPartialTPD_RevForecast]
  @strTaskID varchar(32),
  @strFromStartDate varchar(8),
  @strFromEndDate varchar(8),
  @strToStartDate varchar(8),
  @strToEndDate varchar(8),
  @bitShiftAll bit, /* 0 = Shift using the "From" range, 1 = Shift the entire Assignment Row */
  @bitFixStartDate bit /* 1 = Hold "To" StartDate fixed, 0 = Hold "To" EndDate fixed */
AS

BEGIN -- Procedure stRPShiftPartialTPD_RevForecast

  SET NOCOUNT ON
  
  DECLARE @dtFromStartDate datetime
  DECLARE @dtFromEndDate datetime
  DECLARE @dtMinTPD datetime
  DECLARE @dtMaxTPD datetime
  DECLARE @dtBeforeDate datetime
  DECLARE @dtAfterDate datetime

  DECLARE @dtToOrgStartDate datetime
  DECLARE @dtToOrgEndDate datetime

  DECLARE @dtToTPDStartDate datetime
  DECLARE @dtToTPDEndDate datetime

  DECLARE @dtASGStartDate datetime
  DECLARE @dtASGEndDate datetime
  DECLARE @dtToASGStartDate datetime
  DECLARE @dtToASGEndDate datetime

  DECLARE @strCompany nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strUserName nvarchar(32)

  DECLARE @siGRMethod smallint
  DECLARE @siCostRtMethod smallint
  DECLARE @siBillingRtMethod smallint
  
  DECLARE @intCostRtTableNo int
  DECLARE @intBillingRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int

  DECLARE @siHrDecimals smallint
  
  DECLARE @decBeforeWD decimal(19,4) = 0
  DECLARE @decAfterWD decimal(19,4) = 0
  DECLARE @decASGOffsetWD decimal(19,4) = 0
  DECLARE @decASGDurationWD decimal(19,4) = 0

  DECLARE @intMinRowSeq int = 0
  DECLARE @intMaxRowSeq int = 0
  DECLARE @intTPDDurationWD int = 0

  -- Declare Temp tables.
  
  DECLARE @tabCalendar TABLE (
    StartDate	datetime,
    EndDate	datetime
    UNIQUE(StartDate, EndDate)
  )

  DECLARE @tabTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AT_OutlineNumber varchar(255) COLLATE database_default
    UNIQUE(PlanID, TaskID)
  )

  DECLARE @tabFromTPD TABLE(
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    StartDate datetime, 
    EndDate datetime, 
    PeriodCost decimal(19,4),
	PeriodBill decimal(19,4),
    AT_OutlineNumber varchar(255) COLLATE database_default
    UNIQUE(TimePhaseID, PlanID, TaskID, StartDate, EndDate)
  ) 

  DECLARE @tabS1TPD TABLE (
    RowSeq int,
    NewTPID varchar(32) COLLATE database_default,
    TimePhaseID varchar(32) COLLATE database_default,
    CIStartDate datetime, 
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    StartDate datetime, 
    EndDate datetime, 
    PeriodCost decimal(19,4), 
	PeriodBill decimal(19,4), 
    PeriodWD decimal(19,4),
    GapDays decimal(19,4)
    UNIQUE(RowSeq, NewTPID, PlanID, TaskID, CIStartDate, StartDate, EndDate),
    CHECK(StartDate <= EndDate)
  )

  DECLARE @tabS2TPD TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    StartDate datetime, 
    EndDate datetime, 
    PeriodCost decimal(19,4),
	PeriodBill decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, StartDate, EndDate),
    CHECK(StartDate <= EndDate)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

  SELECT 
    @siHrDecimals = HrDecimals
    FROM CFGRMSettings

  -- Set Dates

  SET @dtFromStartDate = CONVERT(datetime, @strFromStartDate)
  SET @dtFromEndDate = CONVERT(datetime, @strFromEndDate)
  SET @dtBeforeDate = DATEADD(DAY, -1, @dtFromStartDate)
  SET @dtAfterDate = DATEADD(DAY, 1, @dtFromEndDate)

  SET @dtToOrgStartDate = CONVERT(datetime, @strToStartDate)
  SET @dtToOrgEndDate = CONVERT(datetime, @strToEndDate)
  SET @dtToTPDStartDate = CONVERT(datetime, @strToStartDate)
  SET @dtToTPDEndDate = CONVERT(datetime, @strToEndDate)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabTask(
    PlanID,
    TaskID,
    AT_OutlineNumber
  )
    SELECT DISTINCT
      PlanID,
      TaskID,
      OutlineNumber AS AT_OutlineNumber
      FROM PNTask  
      WHERE
        TaskID = @strTaskID  

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get various parameters from Plan, Task, and Assigment.

  SELECT 
    @strPlanID = T.PlanID,    
    @strCompany = P.Company
    FROM @tabTask AS A
      INNER JOIN PNTask AS T ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
      INNER JOIN PNPlan AS P ON A.PlanID = P.PlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collecting Labor Time-Phased Data to be used in subsequent calculations.
  -- The TPD records come from only the Assignment record indentified by @strAssignmentID.

  INSERT @tabFromTPD(
    TimePhaseID,
    PlanID,
    TaskID,
    StartDate,
    EndDate,
    PeriodCost,
	PeriodBill,
    AT_OutlineNumber
  )
    SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID,
      TPD.TaskID AS TaskID,
      TPD.StartDate AS StartDate,
      TPD.EndDate AS EndDate,
      TPD.PeriodCost AS PeriodCost,
      TPD.PeriodBill AS PeriodBill,
      T.AT_OutlineNumber AS AT_OutlineNumber
      FROM PNPlannedRevenueLabor AS TPD
        INNER JOIN @tabTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
      WHERE TPD.PlanID = @strPlanID AND (TPD.PeriodCost <> 0 OR TPD.PeriodBill <> 0) AND 
        TPD.StartDate <= @dtFromEndDate AND TPD.EndDate >= @dtFromStartDate

  IF (@@ROWCOUNT > 0) /* If there is no TPD in the From range then do not need to do shifting of TPD. */
    BEGIN

      SELECT 
        @dtMinTPD = MIN(StartDate), 
        @dtMaxTPD = MAX(EndDate)
        FROM @tabFromTPD AS TPD

      SET @decBeforeWD = dbo.DLTK$NumWorkingDays(@dtMinTPD, @dtBeforeDate, @strCompany)
      SET @decAfterWD = dbo.DLTK$NumWorkingDays(@dtAfterDate, @dtMaxTPD, @strCompany)

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Build Calendar.
      -- Calendar has at most 3 ranges {Before, Main, After}.

      IF (@decBeforeWD > 0)
        BEGIN

          INSERT @tabCalendar(
            StartDate,
            EndDate
          )	
            SELECT 
              @dtMinTPD AS StartDate,
              @dtBeforeDate AS EndDate

        END

      INSERT @tabCalendar(
        StartDate,
        EndDate
      )	
        SELECT 
          @dtFromStartDate AS StartDate,
          @dtFromEndDate AS EndDate

      IF (@decAfterWD > 0)
        BEGIN

          INSERT @tabCalendar(
            StartDate,
            EndDate
          )	
            SELECT 
              @dtAfterDate AS StartDate,
              @dtMaxTPD AS EndDate

        END

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Split up TPD in @tabFromTPD to match with Calendar Intervals.
      -- @tabFromTPD could contain chunks that are larger than a calendar interval which need to be broken up.
      -- @tabFromTPD could contain chunks that are overlapping with the calendar interval boundaries which need to be broken off.
      -- @tabFromTPD could contain chunks that are smaller and contain within the span of a calendar interval.

      INSERT @tabS1TPD(
        RowSeq,
        NewTPID,
        TimePhaseID,
        CIStartDate,
        PlanID, 
        TaskID,
        StartDate, 
        EndDate, 
        PeriodCost,
		PeriodBill,
        PeriodWD,
        GapDays
       )
        SELECT
          RowSeq AS RowSeq,
          NewTPID AS NewTPID,
          TimePhaseID AS TimePhaseID,
          CIStartDate AS CIStartDate,
          PlanID AS PlanID, 
          TaskID AS TaskID,
          StartDate AS StartDate, 
          EndDate AS EndDate, 
		  PeriodCost AS PeriodCost,
          PeriodBill AS PeriodBill,
          PeriodWD AS PeriodWD,
          ISNULL(DATEDIFF(D, (LAG(EndDate) OVER (ORDER BY RowSeq)), StartDate), 0) AS GapDays
          FROM ( /* X2 */
            SELECT
              ROW_NUMBER() OVER (ORDER BY AT_OutlineNumber, StartDate) AS RowSeq,
              REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS NewTPID,
              TimePhaseID AS TimePhaseID,
              CIStartDate AS CIStartDate,
              PlanID AS PlanID, 
              TaskID AS TaskID,
              StartDate AS StartDate, 
              EndDate AS EndDate, 
              ROUND(ISNULL(PeriodCost, 0), @siHrDecimals) AS PeriodCost,
			  ROUND(ISNULL(PeriodBill, 0), @siHrDecimals) AS PeriodBill,
              dbo.DLTK$NumWorkingDays(StartDate, EndDate, @strCompany) AS PeriodWD
              FROM ( /* X1 */
                SELECT 
                  CI.StartDate AS CIStartDate, 
                  TPD.TimePhaseID AS TimePhaseID,
                  TPD.PlanID AS PlanID, 
                  TPD.TaskID AS TaskID,                   
                  CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                  CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                  CASE 
                    WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                    THEN PeriodCost * 
                      dbo.DLTK$ProrateRatio(
                        CASE 
                          WHEN TPD.StartDate > CI.StartDate 
                          THEN TPD.StartDate 
                          ELSE CI.StartDate 
                        END, 
                        CASE 
                          WHEN TPD.EndDate < CI.EndDate 
                          THEN TPD.EndDate 
                          ELSE CI.EndDate 
                        END, 
                        TPD.StartDate, TPD.EndDate,
                        @strCompany)
                    ELSE PeriodCost 
                  END AS PeriodCost,
				  CASE 
                    WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                    THEN PeriodBill * 
                      dbo.DLTK$ProrateRatio(
                        CASE 
                          WHEN TPD.StartDate > CI.StartDate 
                          THEN TPD.StartDate 
                          ELSE CI.StartDate 
                        END, 
                        CASE 
                          WHEN TPD.EndDate < CI.EndDate 
                          THEN TPD.EndDate 
                          ELSE CI.EndDate 
                        END, 
                        TPD.StartDate, TPD.EndDate,
                        @strCompany)
                    ELSE PeriodBill 
                  END AS PeriodBill,
                  TPD.AT_OutlineNumber AS AT_OutlineNumber
                  FROM @tabCalendar AS CI 
                    INNER JOIN @tabFromTPD AS TPD 
                      ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
              ) AS X1
              WHERE ((PeriodCost IS NOT NULL AND PeriodCost > 0) OR (PeriodBill IS NOT NULL AND PeriodBill > 0) )
          ) AS X2

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     
      -- Adjust time-phased data to compensate for rounding errors after the splitting process.

      UPDATE @tabS1TPD SET PeriodCost = (TPD.PeriodCost + DC.DeltaCost), PeriodBill = (TPD.PeriodBill + DB.DeltaBill)
        FROM @tabS1TPD AS TPD 
          INNER JOIN (
            SELECT YTPD.TimePhaseID AS TimePhaseID, 
			       (YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaCost
              FROM @tabS1TPD AS XTPD 
                INNER JOIN @tabFromTPD AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
              GROUP BY YTPD.TimePhaseID, YTPD.PeriodCost
          ) AS DC ON TPD.TimePhaseID = DC.TimePhaseID
		  INNER JOIN (
            SELECT YTPD.TimePhaseID AS TimePhaseID, 
				   (YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaBill
              FROM @tabS1TPD AS XTPD 
                INNER JOIN @tabFromTPD AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
              GROUP BY YTPD.TimePhaseID, YTPD.PeriodBill
          ) AS DB ON TPD.TimePhaseID = DB.TimePhaseID
        WHERE NewTPID IN (
          SELECT NewTPID FROM @tabS1TPD AS ATPD 
            INNER JOIN (
              SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabS1TPD GROUP BY TimePhaseID
            ) AS BTPD ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate
        )
             
    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- At this point we have a table with TPD rows aligned on the scope boundaries.

      -- Copy from @tabS1TPD those TPD rows that are outside of scope to @tabS2TPD.
      -- These TPD rows will retain the original hours as in @tabS1TPD.

      INSERT @tabS2TPD(
        TimePhaseID,
        PlanID, 
        TaskID,
        StartDate, 
        EndDate, 
        PeriodCost,
		PeriodBill
       )
         SELECT
          TPD.NewTPID AS TimePhaseID,
          TPD.PlanID AS PlanID, 
          TPD.TaskID AS TaskID,
          TPD.StartDate AS StartDate, 
          TPD.EndDate AS EndDate, 
          TPD.PeriodCost AS PeriodCost,
          TPD.PeriodBill AS PeriodBill
        FROM @tabS1TPD AS TPD
          INNER JOIN @tabTask AS A ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID  
        WHERE (TPD.EndDate < @dtFromStartDate OR TPD.StartDate > @dtFromEndDate) AND (TPD.PeriodCost > 0 OR TPD.PeriodBill > 0)

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      --> Need to adjust "To" Start and End Dates for special circumstances when Shift ALL <--
      --> Need to do this in two passes.                                                   <--
      
      --> Pass #1: 
      -->   Determine new "To" Start/End Dates for TPD. 
      -->   If there is a gap between From Start/End Dates and TPD Start/End Dates
      -->   then need to offset TPD Start/End Dates the same number of work days from From Start/End Dates.
      -->   Note that for a "Shift All" operation, the From Start/End Dates are the same as Assignment Start/End Dates.
      -->   For a "Shift Range" operation, From Start/End Dates are the dates that were sent in the input parameters of this Store Procedure.
      --> Pass #2: 
      -->   After accounted for the leading/trailing edge gap, we need to determine the "To" TPD Dates range.
      -->   If @bitFixStartDate = 1, then "To" End Date = "To" Start Date + Number of Work Days of TPD in "From" range.
      -->   If @bitFixStartDate = 0, then "To" Start Date = "To" End Date - Number of Work Days of TPD in "From" range.

      SELECT @dtToTPDStartDate =
        CASE 
          WHEN (@bitFixStartDate = 1 AND @dtFromStartDate < @dtMinTPD)
          THEN
            dbo.DLTK$AddBusinessDays(
              @dtToOrgStartDate, 
              (dbo.DLTK$NumWorkingDays(@dtFromStartDate, @dtMinTPD, @strCompany) - 1), 
              @strCompany
            )
          ELSE @dtToTPDStartDate
        END

      SELECT @dtToTPDEndDate =
        CASE 
          WHEN (@bitFixStartDate = 0 AND @dtFromEndDate > @dtMaxTPD)
          THEN
            dbo.DLTK$AddBusinessDays(
              @dtToOrgEndDate, 
              (dbo.DLTK$NumWorkingDays(@dtFromEndDate, @dtMaxTPD, @strCompany) - 1), 
              @strCompany
            )
          ELSE @dtToTPDEndDate
        END

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SET @intTPDDurationWD = (
        SELECT 
          dbo.DLTK$NumWorkingDays(MIN(StartDate), MAX(EndDate), @strCompany)
          FROM @tabS1TPD AS TPD
          WHERE TPD.StartDate <= @dtFromEndDate AND TPD.EndDate >= @dtFromStartDate AND
            ((TPD.PeriodCost IS NOT NULL AND TPD.PeriodCost > 0) OR (TPD.PeriodBill IS NOT NULL AND TPD.PeriodBill > 0))
      )

      SELECT @dtToTPDStartDate =
        CASE
          WHEN @bitFixStartDate = 0 
          THEN
            CASE
              WHEN @intTPDDurationWD = 0
              THEN @dtToTPDEndDate
              ELSE
                dbo.DLTK$AddBusinessDays(
                  @dtToTPDEndDate,
                  -1 * (@intTPDDurationWD - 1),
                  @strCompany
                )
            END
          ELSE @dtToTPDStartDate
        END

      SELECT @dtToTPDEndDate =
        CASE
          WHEN @bitFixStartDate = 1 
          THEN
            dbo.DLTK$AddBusinessDays(
              @dtToTPDStartDate,
              CASE
                WHEN @intTPDDurationWD <> 0
                THEN (@intTPDDurationWD - 1)
                ELSE 0 
              END, 
              @strCompany
            )
          ELSE @dtToTPDEndDate
        END

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Find the MIN and MAX RowSeq.
  
      SELECT 
        @intMinRowSeq = MIN(RowSeq),
        @intMaxRowSeq = MAX(RowSeq)
        FROM @tabS1TPD AS TPD
        WHERE TPD.StartDate <= @dtFromEndDate AND TPD.EndDate >= @dtFromStartDate AND
          ((TPD.PeriodCost IS NOT NULL AND TPD.PeriodCost > 0) OR (TPD.PeriodBill IS NOT NULL AND TPD.PeriodBill > 0))

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Copy rows from @tabS1TPD to @tabS2TPD with new set of {StartDate, EndDate}

    ;

      WITH NewDates AS (
        SELECT 
          TPD.RowSeq AS RowSeq,
          TPD.NewTPID AS NewTPID,
          @dtToTPDStartDate AS NewStartDate,
          CASE
            WHEN TPD.RowSeq = @intMaxRowSeq THEN @dtToTPDEndDate
            WHEN TPD.PeriodWD = 0 THEN @dtToTPDStartDate
            ELSE dbo.DLTK$AddBusinessDays(@dtToTPDStartDate, (TPD.PeriodWD - 1), @strCompany)
          END  AS NewEndDate,
          TPD.PeriodWD AS PeriodWD
        FROM @tabS1TPD AS TPD
        WHERE 
          TPD.StartDate <= @dtFromEndDate AND TPD.EndDate >= @dtFromStartDate 
          AND TPD.RowSeq = @intMinRowSeq
          AND ((TPD.PeriodCost IS NOT NULL AND TPD.PeriodCost > 0) OR (TPD.PeriodBill IS NOT NULL AND TPD.PeriodBill > 0))
        UNION ALL
        SELECT 
          TPD.RowSeq AS RowSeq,
          TPD.NewTPID AS NewTPID,
          DATEADD(D, TPD.GapDays, ND.NewEndDate) AS NewStartDate,
          CASE
            WHEN TPD.PeriodWD = 0 THEN DATEADD(D, TPD.GapDays, ND.NewEndDate)
            ELSE
              dbo.DLTK$AddBusinessDays(
                DATEADD(D, TPD.GapDays, ND.NewEndDate), 
                (TPD.PeriodWD - 1), 
                @strCompany
              )
          END AS NewEndDate,
          TPD.PeriodWD AS PeriodWD
        FROM @tabS1TPD AS TPD
          INNER JOIN NewDates AS ND
            ON TPD.RowSeq = ND.RowSeq + 1 
        WHERE 
          TPD.RowSeq <= @intMaxRowSeq
      )

      INSERT @tabS2TPD(
        TimePhaseID,
        PlanID, 
        TaskID,
        StartDate, 
        EndDate, 
        PeriodCost,
		PeriodBill
      )
        SELECT
          TPD.NewTPID AS TimePhaseID,
          TPD.PlanID AS PlanID, 
          TPD.TaskID AS TaskID,
          ND.NewStartDate AS StartDate, 
          ND.NewEndDate AS EndDate, 
          TPD.PeriodCost AS PeriodCost,
          TPD.PeriodBill AS PeriodBill
      FROM (
        SELECT
          XTPD.RowSeq AS RowSeq,
          XTPD.NewTPID AS NewTPID,
          XTPD.PlanID AS PlanID, 
          XTPD.TaskID AS TaskID,           
          XTPD.PeriodCost AS PeriodCost,
		  XTPD.PeriodBill AS PeriodBill
          FROM @tabS1TPD AS XTPD
          WHERE 
            XTPD.StartDate <= @dtFromEndDate AND XTPD.EndDate >= @dtFromStartDate 
            AND ((XTPD.PeriodCost IS NOT NULL AND XTPD.PeriodCost > 0) OR (XTPD.PeriodBill IS NOT NULL AND XTPD.PeriodBill > 0))
      ) AS TPD
        INNER JOIN NewDates AS ND ON TPD.RowSeq = ND.RowSeq AND TPD.NewTPID = ND.NewTPID
        INNER JOIN @tabTask AS A ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID  
        OPTION (MAXRECURSION 0)
        
    ;

      -- At this point we have all of the TPD at correct {StartDate, EndDate} in @tabS2TPD.
      -- Note that the new TPD at new {StartDate, EndDate} could be overlapping with existing TPD in that date range.

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      DELETE PNPlannedRevenueLabor 
        WHERE 
          PlanID = @strPlanID 
          AND (
            TimePhaseID IN (
              SELECT DISTINCT TimePhaseID FROM @tabFromTPD
            ) 
            OR (PeriodCost = 0 AND PeriodBill = 0)
          )

      INSERT PNPlannedRevenueLabor(
        TimePhaseID,
        PlanID, 
        TaskID,
        StartDate, 
        EndDate, 
        PeriodCost,
		PeriodBill,
        CreateUser,
        ModUser,
        CreateDate,
        ModDate
      )
         SELECT
          TPD.TimePhaseID AS TimePhaseID,
          TPD.PlanID AS PlanID, 
          TPD.TaskID AS TaskID,
          TPD.StartDate AS StartDate, 
          TPD.EndDate AS EndDate, 
          TPD.PeriodCost AS PeriodCost,
		  TPD.PeriodBill AS PeriodBill,
          CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stRPShiftPartialRevTPD' ELSE 'SHA_' + @strUserName END AS CreateUser,
          CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stRPShiftPartialRevTPD' ELSE 'SHA_' + @strUserName END AS ModUser,
          LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As CreateDate,
          LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As ModDate 
        FROM @tabS2TPD AS TPD
        WHERE (PeriodCost > 0 OR PeriodBill > 0)

    END /* END IF (@@ROWCOUNT > 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     
  SET NOCOUNT OFF

END -- stRPShiftPartialTPD_RevForecast
GO
