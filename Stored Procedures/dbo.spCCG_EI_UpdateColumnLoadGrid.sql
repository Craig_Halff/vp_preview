SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_UpdateColumnLoadGrid]
	@wbs1list				Nvarchar(max),
	@initialValue			varchar(max),
	@columnUpdate			varchar(500),
	@databaseField			varchar(255),
	@visionTable			varchar(100),
	@databaseModEmpField	varchar(500),
	@emptySQLValue			varchar(100),
	@valueColumnName		varchar(max),
	@outerApplyStr			varchar(max)
AS
BEGIN
	/* Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved. */
	/*
	spCCG_EI_UpdateColumnLoadGrid @wbs1list='''xxxxx'',''1999004.00'',''1999015.00'',''2002010.00'',''2002019.00'',''2003005.00'',''HALFFPR.OJ''',
		@initialValue='dbo.fnCCG_EI_tbl_HoldUntilDate',
		@columnUpdate='spCCG_EI_HoldUntilDate',
		@databaseField='HoldUntilDate',
		@visionTable='',
		@databaseModEmpField='HoldUntilDate_ModEmp',
		@emptySQLValue='''1900-01-01''',
		@valueColumnName='HoldDateValue',
		@outerApplyStr='OUTER APPLY dbo.fnCCG_EI_tbl_HoldUntilDate(PR.WBS1, PR.WBS2, PR.WBS3)'
	*/
	SET NOCOUNT ON
	declare @sSQL		Nvarchar(max)
	declare @ccValueSQL	Nvarchar(max) = (CASE when isnull(@valueColumnName,'')='' then @initialValue+N'(PR.WBS1, PR.WBS2, PR.WBS3)' else N'CC.'+@valueColumnName END)
	declare @safeSql	int

	set @wbs1list = isnull(@wbs1list, N'')

    set @sSQL = N'
		select PR.WBS1, PR.WBS2, PR.WBS3, PR.Name, '+@ccValueSQL+' as '''+@columnUpdate+''',
			eicc.'+@databaseField+', ''True'' AS ''Update'', '''' AS Result
		from PR
			inner join CCG_EI_CustomColumns eicc
				on PR.WBS1 = eicc.WBS1 and PR.WBS2 = eicc.WBS2 and PR.WBS3 = eicc.WBS3 '
			+ISNULL(NULLIF(@outerApplyStr,'')+' as CC', '')

	if isnull(@visionTable,'')<>'' AND UPPER(@visionTable) <> 'PR' -- Future use for updating a vision field directly without a SP/function
		set @sSQL = @sSQL + N'
			left join ' + @visionTable + '
				on PR.WBS1 = '+@visionTable+'.WBS1 and PR.WBS2 = '+@visionTable+'.WBS2 and PR.WBS3 = '+@visionTable+'.WBS3 '

    set @sSQL = @sSQL + N'
		where eicc.'+@databaseModEmpField+' IS NOT NULL
			and isnull(eicc.'+@databaseField+', '+@emptySQLValue+') <> isnull('+@ccValueSQL+', '+@emptySQLValue+') '

    if isnull(@wbs1list,'') <> ''
        set @sSQL = @sSQL + N'
			and PR.WBS1 IN (' + @wbs1list + ')'

    set @sSQL = @sSQL + '
		order by 1, 2, 3 '

	--PRINT @sSQL
	exec (@sSQL)
END
GO
