SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_AddlNotifications] ( @Employee nvarchar(20), @NotifyId int, @EmployeePICField varchar(100), @RepeatInterval int, @Subject nvarchar(120), @Body nvarchar(512), @OverrideSenderEmail varchar(100), @StatusToInclude varchar(10), @MsgPriority varchar(10)= NULL, @IncludeSupervisorAge int= -1)
             AS EXEC spCCG_EI_AddlNotifications @Employee,@NotifyId,@EmployeePICField,@RepeatInterval,@Subject,@Body,@OverrideSenderEmail,@StatusToInclude,@MsgPriority,@IncludeSupervisorAge
GO
