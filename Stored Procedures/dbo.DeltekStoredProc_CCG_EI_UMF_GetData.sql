SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_UMF_GetData] ( @stripHTML int, @product varchar(20), @cacheField varchar(100), @updateTable varchar(200), @updateField varchar(100), @wbs1 nvarchar(30), @wbs2 nvarchar(7), @wbs3 nvarchar(7), @payableSeq int= 0)
             AS EXEC spCCG_EI_UMF_GetData @stripHTML,@product,@cacheField,@updateTable,@updateField,@wbs1,@wbs2,@wbs3,@payableSeq
GO
