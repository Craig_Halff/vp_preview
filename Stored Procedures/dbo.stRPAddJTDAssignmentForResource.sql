SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPAddJTDAssignmentForResource]
  @resourceId nvarchar(20),
  @bitCalledFromRM bit = 0
AS
BEGIN -- Procedure stRPAddJTDAssignment

  SET NOCOUNT ON

  DECLARE @strUserName nvarchar(32) = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

  -- Set Dates   
  DECLARE @dtJTDDate datetime
  SELECT @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE()))) FROM CFGRMSettings

  DECLARE @dtETCDate datetime
  SET @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)

  DECLARE @tabLD TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,  
	PlanID varchar(32) COLLATE database_default,  
    Employee nvarchar(20) COLLATE database_default, 
	TaskID varchar(32) COLLATE database_default, 
	StartDate datetime , 
	EndDate datetime
    UNIQUE (RowID, WBS1, WBS2, WBS3, Employee)
  )
  DECLARE @assignment TABLE (
    RowID int IDENTITY(1,1),    
	PlanID varchar(32) COLLATE database_default,  
	TaskID varchar(32) COLLATE database_default, 
    ResourceID nvarchar(20) COLLATE database_default, 
    UNIQUE (RowID, PlanID, TaskID, ResourceID)
  )

  INSERT INTO @assignment (PlanID, TaskID, ResourceID) 
	SELECT PlanID, TaskID, ResourceID FROM PNAssignment WHERE ResourceID=@resourceID  
    
  INSERT @tabLD(
	WBS1,
	WBS2,
	WBS3,	    
	Employee, 
	PlanID,
	TaskID,
	StartDate,
	EndDate
	)  	
		SELECT  
			LD.WBS1,
			LD.WBS2,
			LD.WBS3,			
			LD.Employee,
			PNTask.PlanID,
			PNTask.TaskID,
			PNTask.StartDate,
			PNTask.EndDate 
			FROM LD 
			INNER JOIN PNTask ON LD.WBS1=PNTask.WBS1 AND LD.WBS2=ISNULL(PNTask.WBS2,' ') AND LD.WBS3=ISNULL(PNTask.WBS3,' ') 
			WHERE LD.Employee IS NOT NULL AND
			LD.ProjectCost = 'Y' AND LD.TransDate <= @dtJTDDate AND LD.Employee=@resourceId
			AND RegHrs + OvtHrs + SpecialOvtHrs > 0	
			AND NOT EXISTS (SELECT ' ' FROM @assignment AS A WHERE A.PlanID=PNTask.PlanID AND A.TaskID=PNTask.TaskID AND LD.Employee = A.ResourceID AND A.ResourceID IS NOT NULL)
		UNION 
		SELECT  
			TD.WBS1,
			TD.WBS2,
			TD.WBS3,			
			TD.Employee,
			PNTask.PlanID,
			PNTask.TaskID,
			PNTask.StartDate,
			PNTask.EndDate 
			FROM tkDetail AS TD -- TD table has entries at only the leaf level.				
			INNER JOIN PNTask ON TD.WBS1=PNTask.WBS1 AND TD.WBS2=ISNULL(PNTask.WBS2,' ') AND TD.WBS3=ISNULL(PNTask.WBS3,' ') 
			INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
			WHERE TD.Employee IS NOT NULL AND TD.TransDate <= @dtJTDDate  AND TD.Employee=@resourceId 
			AND RegHrs + OvtHrs + SpecialOvtHrs > 0 
			AND NOT EXISTS (SELECT ' ' FROM @assignment AS A WHERE A.PlanID=PNTask.PlanID AND A.TaskID=PNTask.TaskID AND TD.Employee = A.ResourceID AND A.ResourceID IS NOT NULL)
		UNION 
		SELECT  
			TD.WBS1,
			TD.WBS2,
			TD.WBS3,			
			TD.Employee,
			PNTask.PlanID,
			PNTask.TaskID,
			PNTask.StartDate,
			PNTask.EndDate 
			FROM tsDetail AS TD -- TD table has entries at only the leaf level.
			INNER JOIN PNTask ON TD.WBS1=PNTask.WBS1 AND TD.WBS2=ISNULL(PNTask.WBS2,' ') AND TD.WBS3=ISNULL(PNTask.WBS3,' ') 
			WHERE TD.Employee IS NOT NULL AND TD.TransDate <= @dtJTDDate AND TD.Employee=@resourceId 
			AND	RegHrs + OvtHrs + SpecialOvtHrs > 0 
			AND NOT EXISTS (SELECT ' ' FROM @assignment AS A WHERE A.PlanID=PNTask.PlanID AND A.TaskID=PNTask.TaskID AND TD.Employee = A.ResourceID AND A.ResourceID IS NOT NULL)
  
  DECLARE @tabInserted TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID VARCHAR(32) COLLATE database_default
    UNIQUE (PlanID, TaskID, AssignmentID)
  )

  BEGIN TRANSACTION 

  -- Find and insert Un-Planned resources (with JTD) into PNAssignment table.
    INSERT PNAssignment(
    PlanID,
    TaskID,
    AssignmentID,
    WBS1,
    WBS2,
    WBS3,
    ResourceID,
    GenericResourceID,
    StartDate,
    EndDate
  )
    OUTPUT
      INSERTED.PlanID,
      INSERTED.TaskID,
      INSERTED.AssignmentID
      INTO @tabInserted(
        PlanID,
        TaskID,
        AssignmentID
      )
    SELECT PlanID
		, TaskID
		, REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS AssignmentID
		, WBS1
		, WBS2
		, WBS3
		, @resourceID
		, NULL 
		, StartDate
		, EndDate
	FROM @tabLD
	GROUP BY PlanID, TaskID, WBS1, WBS2, WBS3, StartDate, EndDate

  -- Determine whether any row was inserted.

  DECLARE @intRowCount int = @@ROWCOUNT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@bitCalledFromRM = 1 AND @intRowCount > 0)
    BEGIN

      -- Find and insert Un-Planned resources (with JTD) into RPAssignment table.

      INSERT RPAssignment(
        PlanID,
        TaskID,
        AssignmentID,
        WBS1,
        WBS2,
        WBS3,
        ResourceID,
        GenericResourceID,
        StartDate,
        EndDate
      )
        SELECT  
          PA.PlanID AS PlanID,
          PA.TaskID AS TaskID,
          PA.AssignmentID AS AssignmentID,
          PA.WBS1 AS WBS1,
          PA.WBS2 AS WBS2,
          PA.WBS3 AS WBS3,
          PA.ResourceID AS ResourceID,
          PA.GenericResourceID AS GenericResourceID,
          PA.StartDate AS StartDate,
          PA.EndDate AS EndDate
          FROM PNAssignment AS PA
            INNER JOIN @tabInserted AS I
              ON PA.PlanID = I.PlanID AND PA.TaskID = I.TaskID AND PA.AssignmentID = I.AssignmentID

    END /* END IF (@bitCalledFromRM = 1) */
		
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@intRowCount > 0)
    BEGIN

		DECLARE @strVersionID varchar(32) = REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')	  
		UPDATE PNPlan SET VersionID = @strVersionID, LastPlanAction = 'SAVED' WHERE PlanID IN (SELECT PlanID FROM @tabLD)

		IF (@bitCalledFromRM = 1)
		BEGIN
		  UPDATE RPPlan SET VersionID = @strVersionID  WHERE PlanID IN (SELECT PlanID FROM @tabLD)      
		END 
           
    END /* END IF (@intRowCount > 0) */
         
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  COMMIT
 
  SET NOCOUNT OFF

END -- stRPAddJTDAssignmentForResource
GO
