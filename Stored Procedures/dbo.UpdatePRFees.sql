SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[UpdatePRFees]
    @project Nvarchar(32),
    @template Nvarchar(1)
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
BEGIN
	if (@project = '%')
-- All Projects
		Begin
		   IF @template = 'Y' OR @template = '1' OR UPPER(@template) = 'TRUE'
			BEGIN
			UPDATE PRTemplate SET 
			Fee = p2.Fee,
			ConsultFee = p2.ConsultFee,
			ReimbAllow = p2.ReimbAllow,
			FeeBillingCurrency = p2.FeeBillingCurrency,
			ConsultFeeBillingCurrency = p2.ConsultFeeBillingCurrency,
			ReimbAllowBillingCurrency = p2.ReimbAllowBillingCurrency,
			FeeFunctionalCurrency = p2.FeeFunctionalCurrency,
			ConsultFeeFunctionalCurrency = p2.ConsultFeeFunctionalCurrency,
			ReimbAllowFunctionalCurrency = p2.ReimbAllowFunctionalCurrency,
			FeeDirLab = p2.FeeDirLab,
			FeeDirExp = p2.FeeDirExp,
			ReimbAllowExp = p2.ReimbAllowExp,
			ReimbAllowCons = p2.ReimbAllowCons,
			FeeDirLabBillingCurrency = p2.FeeDirLabBillingCurrency,
			FeeDirExpBillingCurrency = p2.FeeDirExpBillingCurrency,
			ReimbAllowExpBillingCurrency = p2.ReimbAllowExpBillingCurrency,
			ReimbAllowConsBillingCurrency = p2.ReimbAllowConsBillingCurrency,
			FeeDirLabFunctionalCurrency = p2.FeeDirLabFunctionalCurrency,
			FeeDirExpFunctionalCurrency = p2.FeeDirExpFunctionalCurrency,
			ReimbAllowExpFunctionalCurrency = p2.ReimbAllowExpFunctionalCurrency,
			ReimbAllowConsFunctionalCurrency = p2.ReimbAllowConsFunctionalCurrency		
			FROM (SELECT WBS1,WBS2, IsNull(Sum(p2.Fee),0) Fee,IsNull(Sum(p2.ConsultFee),0) ConsultFee,IsNull(Sum(p2.ReimbAllow),0) ReimbAllow,IsNull(Sum(p2.FeeBillingCurrency),0) FeeBillingCurrency,
					IsNull(Sum(p2.ConsultFeeBillingCurrency),0) ConsultFeeBillingCurrency,IsNull(Sum(p2.ReimbAllowBillingCurrency),0) ReimbAllowBillingCurrency,
					IsNull(Sum(p2.FeeFunctionalCurrency),0) FeeFunctionalCurrency,IsNull(Sum(p2.ConsultFeeFunctionalCurrency),0) ConsultFeeFunctionalCurrency,
					IsNull(Sum(p2.ReimbAllowFunctionalCurrency),0) ReimbAllowFunctionalCurrency,
					IsNull(Sum(p2.FeeDirLab),0) FeeDirLab,IsNull(Sum(p2.FeeDirExp),0) FeeDirExp,
					IsNull(Sum(p2.ReimbAllowExp),0) ReimbAllowExp,IsNull(Sum(p2.ReimbAllowCons),0) ReimbAllowCons,
					IsNull(Sum(p2.FeeDirLabBillingCurrency),0) FeeDirLabBillingCurrency,IsNull(Sum(p2.FeeDirExpBillingCurrency),0) FeeDirExpBillingCurrency,
					IsNull(Sum(p2.ReimbAllowExpBillingCurrency),0) ReimbAllowExpBillingCurrency,IsNull(Sum(p2.ReimbAllowConsBillingCurrency),0) ReimbAllowConsBillingCurrency,
					IsNull(Sum(p2.FeeDirLabFunctionalCurrency),0) FeeDirLabFunctionalCurrency,IsNull(Sum(p2.FeeDirExpFunctionalCurrency),0)FeeDirExpFunctionalCurrency,
					IsNull(Sum(p2.ReimbAllowExpFunctionalCurrency),0) ReimbAllowExpFunctionalCurrency,IsNull(Sum(p2.ReimbAllowConsFunctionalCurrency),0) ReimbAllowConsFunctionalCurrency			

					FROM PRTemplate p2 WHERE p2.WBS3 <> N' '
					GROUP BY WBS1, WBS2) p2
			WHERE PRTemplate.WBS1=p2.WBS1 AND PRTemplate.WBS2=p2.WBS2 AND PRTemplate.WBS3 = N' ' AND PRTemplate.SubLevel = 'Y'

			UPDATE PRTemplate SET 
			Fee = p2.Fee,
			ConsultFee = p2.ConsultFee,
			ReimbAllow = p2.ReimbAllow,
			FeeBillingCurrency = p2.FeeBillingCurrency,
			ConsultFeeBillingCurrency = p2.ConsultFeeBillingCurrency,
			ReimbAllowBillingCurrency = p2.ReimbAllowBillingCurrency,
			FeeFunctionalCurrency = p2.FeeFunctionalCurrency,
			ConsultFeeFunctionalCurrency = p2.ConsultFeeFunctionalCurrency,
			ReimbAllowFunctionalCurrency = p2.ReimbAllowFunctionalCurrency,
			FeeDirLab = p2.FeeDirLab,
			FeeDirExp = p2.FeeDirExp,
			ReimbAllowExp = p2.ReimbAllowExp,
			ReimbAllowCons = p2.ReimbAllowCons,
			FeeDirLabBillingCurrency = p2.FeeDirLabBillingCurrency,
			FeeDirExpBillingCurrency = p2.FeeDirExpBillingCurrency,
			ReimbAllowExpBillingCurrency = p2.ReimbAllowExpBillingCurrency,
			ReimbAllowConsBillingCurrency = p2.ReimbAllowConsBillingCurrency,
			FeeDirLabFunctionalCurrency = p2.FeeDirLabFunctionalCurrency,
			FeeDirExpFunctionalCurrency = p2.FeeDirExpFunctionalCurrency,
			ReimbAllowExpFunctionalCurrency = p2.ReimbAllowExpFunctionalCurrency,
			ReimbAllowConsFunctionalCurrency = p2.ReimbAllowConsFunctionalCurrency				
			FROM (SELECT WBS1, IsNull(Sum(p2.Fee),0) Fee,IsNull(Sum(p2.ConsultFee),0) ConsultFee,IsNull(Sum(p2.ReimbAllow),0) ReimbAllow,IsNull(Sum(p2.FeeBillingCurrency),0) FeeBillingCurrency,
					IsNull(Sum(p2.ConsultFeeBillingCurrency),0) ConsultFeeBillingCurrency,IsNull(Sum(p2.ReimbAllowBillingCurrency),0) ReimbAllowBillingCurrency,
					IsNull(Sum(p2.FeeFunctionalCurrency),0) FeeFunctionalCurrency,IsNull(Sum(p2.ConsultFeeFunctionalCurrency),0) ConsultFeeFunctionalCurrency,
					IsNull(Sum(p2.ReimbAllowFunctionalCurrency),0) ReimbAllowFunctionalCurrency,
					IsNull(Sum(p2.FeeDirLab),0) FeeDirLab,IsNull(Sum(p2.FeeDirExp),0) FeeDirExp,
					IsNull(Sum(p2.ReimbAllowExp),0) ReimbAllowExp,IsNull(Sum(p2.ReimbAllowCons),0) ReimbAllowCons,
					IsNull(Sum(p2.FeeDirLabBillingCurrency),0) FeeDirLabBillingCurrency,IsNull(Sum(p2.FeeDirExpBillingCurrency),0) FeeDirExpBillingCurrency,
					IsNull(Sum(p2.ReimbAllowExpBillingCurrency),0) ReimbAllowExpBillingCurrency,IsNull(Sum(p2.ReimbAllowConsBillingCurrency),0) ReimbAllowConsBillingCurrency,
					IsNull(Sum(p2.FeeDirLabFunctionalCurrency),0) FeeDirLabFunctionalCurrency,IsNull(Sum(p2.FeeDirExpFunctionalCurrency),0)FeeDirExpFunctionalCurrency,
					IsNull(Sum(p2.ReimbAllowExpFunctionalCurrency),0) ReimbAllowExpFunctionalCurrency,IsNull(Sum(p2.ReimbAllowConsFunctionalCurrency),0) ReimbAllowConsFunctionalCurrency						
					FROM PRTemplate p2 WHERE p2.WBS2 <> N' ' AND p2.WBS3 = N' ' Group By WBS1) p2
			WHERE PRTemplate.WBS1=p2.WBS1 AND PRTemplate.WBS2 = N' ' AND PRTemplate.WBS3 = N' ' AND PRTemplate.SubLevel = 'Y'
			END
		   ELSE
			BEGIN
			UPDATE PR SET 
			Fee = p2.Fee,
			ConsultFee = p2.ConsultFee,
			ReimbAllow = p2.ReimbAllow,
			FeeBillingCurrency = p2.FeeBillingCurrency,
			ConsultFeeBillingCurrency = p2.ConsultFeeBillingCurrency,
			ReimbAllowBillingCurrency = p2.ReimbAllowBillingCurrency,
			FeeFunctionalCurrency = p2.FeeFunctionalCurrency,
			ConsultFeeFunctionalCurrency = p2.ConsultFeeFunctionalCurrency,
			ReimbAllowFunctionalCurrency = p2.ReimbAllowFunctionalCurrency,
			FeeDirLab = p2.FeeDirLab,
			FeeDirExp = p2.FeeDirExp,
			ReimbAllowExp = p2.ReimbAllowExp,
			ReimbAllowCons = p2.ReimbAllowCons,
			FeeDirLabBillingCurrency = p2.FeeDirLabBillingCurrency,
			FeeDirExpBillingCurrency = p2.FeeDirExpBillingCurrency,
			ReimbAllowExpBillingCurrency = p2.ReimbAllowExpBillingCurrency,
			ReimbAllowConsBillingCurrency = p2.ReimbAllowConsBillingCurrency,
			FeeDirLabFunctionalCurrency = p2.FeeDirLabFunctionalCurrency,
			FeeDirExpFunctionalCurrency = p2.FeeDirExpFunctionalCurrency,
			ReimbAllowExpFunctionalCurrency = p2.ReimbAllowExpFunctionalCurrency,
			ReimbAllowConsFunctionalCurrency = p2.ReimbAllowConsFunctionalCurrency				
				
			FROM (SELECT WBS1,WBS2,IsNull(Sum(p2.Fee),0) Fee,IsNull(Sum(p2.ConsultFee),0) ConsultFee,IsNull(Sum(p2.ReimbAllow),0) ReimbAllow,IsNull(Sum(p2.FeeBillingCurrency),0) FeeBillingCurrency,
					IsNull(Sum(p2.ConsultFeeBillingCurrency),0) ConsultFeeBillingCurrency,IsNull(Sum(p2.ReimbAllowBillingCurrency),0) ReimbAllowBillingCurrency,
					IsNull(Sum(p2.FeeFunctionalCurrency),0) FeeFunctionalCurrency,IsNull(Sum(p2.ConsultFeeFunctionalCurrency),0) ConsultFeeFunctionalCurrency,
					IsNull(Sum(p2.ReimbAllowFunctionalCurrency),0) ReimbAllowFunctionalCurrency,
					IsNull(Sum(p2.FeeDirLab),0) FeeDirLab,IsNull(Sum(p2.FeeDirExp),0) FeeDirExp,
					IsNull(Sum(p2.ReimbAllowExp),0) ReimbAllowExp,IsNull(Sum(p2.ReimbAllowCons),0) ReimbAllowCons,
					IsNull(Sum(p2.FeeDirLabBillingCurrency),0) FeeDirLabBillingCurrency,IsNull(Sum(p2.FeeDirExpBillingCurrency),0) FeeDirExpBillingCurrency,
					IsNull(Sum(p2.ReimbAllowExpBillingCurrency),0) ReimbAllowExpBillingCurrency,IsNull(Sum(p2.ReimbAllowConsBillingCurrency),0) ReimbAllowConsBillingCurrency,
					IsNull(Sum(p2.FeeDirLabFunctionalCurrency),0) FeeDirLabFunctionalCurrency,IsNull(Sum(p2.FeeDirExpFunctionalCurrency),0)FeeDirExpFunctionalCurrency,
					IsNull(Sum(p2.ReimbAllowExpFunctionalCurrency),0) ReimbAllowExpFunctionalCurrency,IsNull(Sum(p2.ReimbAllowConsFunctionalCurrency),0) ReimbAllowConsFunctionalCurrency						

					FROM PR p2 WHERE p2.WBS3 <> N' '
					GROUP BY WBS1, WBS2) p2
			WHERE PR.WBS1=p2.WBS1 AND PR.WBS2=p2.WBS2 AND PR.WBS3 = N' ' AND PR.SubLevel = 'Y'

			UPDATE PR SET 
			Fee = p2.Fee,
			ConsultFee = p2.ConsultFee,
			ReimbAllow = p2.ReimbAllow,
			FeeBillingCurrency = p2.FeeBillingCurrency,
			ConsultFeeBillingCurrency = p2.ConsultFeeBillingCurrency,
			ReimbAllowBillingCurrency = p2.ReimbAllowBillingCurrency,
			FeeFunctionalCurrency = p2.FeeFunctionalCurrency,
			ConsultFeeFunctionalCurrency = p2.ConsultFeeFunctionalCurrency,
			ReimbAllowFunctionalCurrency = p2.ReimbAllowFunctionalCurrency,
			FeeDirLab = p2.FeeDirLab,
			FeeDirExp = p2.FeeDirExp,
			ReimbAllowExp = p2.ReimbAllowExp,
			ReimbAllowCons = p2.ReimbAllowCons,
			FeeDirLabBillingCurrency = p2.FeeDirLabBillingCurrency,
			FeeDirExpBillingCurrency = p2.FeeDirExpBillingCurrency,
			ReimbAllowExpBillingCurrency = p2.ReimbAllowExpBillingCurrency,
			ReimbAllowConsBillingCurrency = p2.ReimbAllowConsBillingCurrency,
			FeeDirLabFunctionalCurrency = p2.FeeDirLabFunctionalCurrency,
			FeeDirExpFunctionalCurrency = p2.FeeDirExpFunctionalCurrency,
			ReimbAllowExpFunctionalCurrency = p2.ReimbAllowExpFunctionalCurrency,
			ReimbAllowConsFunctionalCurrency = p2.ReimbAllowConsFunctionalCurrency		
				
			FROM (SELECT WBS1, IsNull(Sum(p2.Fee),0) Fee,IsNull(Sum(p2.ConsultFee),0) ConsultFee,IsNull(Sum(p2.ReimbAllow),0) ReimbAllow,IsNull(Sum(p2.FeeBillingCurrency),0) FeeBillingCurrency,
					IsNull(Sum(p2.ConsultFeeBillingCurrency),0) ConsultFeeBillingCurrency,IsNull(Sum(p2.ReimbAllowBillingCurrency),0) ReimbAllowBillingCurrency,
					IsNull(Sum(p2.FeeFunctionalCurrency),0) FeeFunctionalCurrency,IsNull(Sum(p2.ConsultFeeFunctionalCurrency),0) ConsultFeeFunctionalCurrency,
					IsNull(Sum(p2.ReimbAllowFunctionalCurrency),0) ReimbAllowFunctionalCurrency,
					IsNull(Sum(p2.FeeDirLab),0) FeeDirLab,IsNull(Sum(p2.FeeDirExp),0) FeeDirExp,
					IsNull(Sum(p2.ReimbAllowExp),0) ReimbAllowExp,IsNull(Sum(p2.ReimbAllowCons),0) ReimbAllowCons,
					IsNull(Sum(p2.FeeDirLabBillingCurrency),0) FeeDirLabBillingCurrency,IsNull(Sum(p2.FeeDirExpBillingCurrency),0) FeeDirExpBillingCurrency,
					IsNull(Sum(p2.ReimbAllowExpBillingCurrency),0) ReimbAllowExpBillingCurrency,IsNull(Sum(p2.ReimbAllowConsBillingCurrency),0) ReimbAllowConsBillingCurrency,
					IsNull(Sum(p2.FeeDirLabFunctionalCurrency),0) FeeDirLabFunctionalCurrency,IsNull(Sum(p2.FeeDirExpFunctionalCurrency),0)FeeDirExpFunctionalCurrency,
					IsNull(Sum(p2.ReimbAllowExpFunctionalCurrency),0) ReimbAllowExpFunctionalCurrency,IsNull(Sum(p2.ReimbAllowConsFunctionalCurrency),0) ReimbAllowConsFunctionalCurrency						

					FROM PR p2 WHERE p2.WBS2 <> N' ' AND p2.WBS3 = N' ' Group By WBS1) p2
			WHERE PR.WBS1=p2.WBS1 AND PR.WBS2 = N' ' AND PR.WBS3 = N' ' AND PR.SubLevel = 'Y'
			END
		End
	else
		Begin
-- Specific Project
		   IF @template = 'Y' OR @template = '1' OR UPPER(@template) = 'TRUE'
			BEGIN
			UPDATE PRTemplate SET 
			Fee = p2.Fee,
			ConsultFee = p2.ConsultFee,
			ReimbAllow = p2.ReimbAllow,
			FeeBillingCurrency = p2.FeeBillingCurrency,
			ConsultFeeBillingCurrency = p2.ConsultFeeBillingCurrency,
			ReimbAllowBillingCurrency = p2.ReimbAllowBillingCurrency,
			FeeFunctionalCurrency = p2.FeeFunctionalCurrency,
			ConsultFeeFunctionalCurrency = p2.ConsultFeeFunctionalCurrency,
			ReimbAllowFunctionalCurrency = p2.ReimbAllowFunctionalCurrency,
			FeeDirLab = p2.FeeDirLab,
			FeeDirExp = p2.FeeDirExp,
			ReimbAllowExp = p2.ReimbAllowExp,
			ReimbAllowCons = p2.ReimbAllowCons,
			FeeDirLabBillingCurrency = p2.FeeDirLabBillingCurrency,
			FeeDirExpBillingCurrency = p2.FeeDirExpBillingCurrency,
			ReimbAllowExpBillingCurrency = p2.ReimbAllowExpBillingCurrency,
			ReimbAllowConsBillingCurrency = p2.ReimbAllowConsBillingCurrency,
			FeeDirLabFunctionalCurrency = p2.FeeDirLabFunctionalCurrency,
			FeeDirExpFunctionalCurrency = p2.FeeDirExpFunctionalCurrency,
			ReimbAllowExpFunctionalCurrency = p2.ReimbAllowExpFunctionalCurrency,
			ReimbAllowConsFunctionalCurrency = p2.ReimbAllowConsFunctionalCurrency		
			FROM (SELECT WBS1,WBS2,IsNull(Sum(p2.Fee),0) Fee,IsNull(Sum(p2.ConsultFee),0) ConsultFee,IsNull(Sum(p2.ReimbAllow),0) ReimbAllow,IsNull(Sum(p2.FeeBillingCurrency),0) FeeBillingCurrency,
					IsNull(Sum(p2.ConsultFeeBillingCurrency),0) ConsultFeeBillingCurrency,IsNull(Sum(p2.ReimbAllowBillingCurrency),0) ReimbAllowBillingCurrency,
					IsNull(Sum(p2.FeeFunctionalCurrency),0) FeeFunctionalCurrency,IsNull(Sum(p2.ConsultFeeFunctionalCurrency),0) ConsultFeeFunctionalCurrency,
					IsNull(Sum(p2.ReimbAllowFunctionalCurrency),0) ReimbAllowFunctionalCurrency,
					IsNull(Sum(p2.FeeDirLab),0) FeeDirLab,IsNull(Sum(p2.FeeDirExp),0) FeeDirExp,
					IsNull(Sum(p2.ReimbAllowExp),0) ReimbAllowExp,IsNull(Sum(p2.ReimbAllowCons),0) ReimbAllowCons,
					IsNull(Sum(p2.FeeDirLabBillingCurrency),0) FeeDirLabBillingCurrency,IsNull(Sum(p2.FeeDirExpBillingCurrency),0) FeeDirExpBillingCurrency,
					IsNull(Sum(p2.ReimbAllowExpBillingCurrency),0) ReimbAllowExpBillingCurrency,IsNull(Sum(p2.ReimbAllowConsBillingCurrency),0) ReimbAllowConsBillingCurrency,
					IsNull(Sum(p2.FeeDirLabFunctionalCurrency),0) FeeDirLabFunctionalCurrency,IsNull(Sum(p2.FeeDirExpFunctionalCurrency),0)FeeDirExpFunctionalCurrency,
					IsNull(Sum(p2.ReimbAllowExpFunctionalCurrency),0) ReimbAllowExpFunctionalCurrency,IsNull(Sum(p2.ReimbAllowConsFunctionalCurrency),0) ReimbAllowConsFunctionalCurrency			

					FROM PRTemplate p2 WHERE p2.WBS1 = @project AND p2.WBS2 = p2.WBS2 AND p2.WBS3 <> N' '
					GROUP BY WBS1, WBS2) p2
			WHERE PRTemplate.WBS1 = @project AND PRTemplate.WBS2 = p2.WBS2 AND PRTemplate.WBS3 = N' ' AND PRTemplate.SubLevel = 'Y'

			UPDATE PRTemplate SET 
			Fee = p2.Fee,
			ConsultFee = p2.ConsultFee,
			ReimbAllow = p2.ReimbAllow,
			FeeBillingCurrency = p2.FeeBillingCurrency,
			ConsultFeeBillingCurrency = p2.ConsultFeeBillingCurrency,
			ReimbAllowBillingCurrency = p2.ReimbAllowBillingCurrency,
			FeeFunctionalCurrency = p2.FeeFunctionalCurrency,
			ConsultFeeFunctionalCurrency = p2.ConsultFeeFunctionalCurrency,
			ReimbAllowFunctionalCurrency = p2.ReimbAllowFunctionalCurrency,
			FeeDirLab = p2.FeeDirLab,
			FeeDirExp = p2.FeeDirExp,
			ReimbAllowExp = p2.ReimbAllowExp,
			ReimbAllowCons = p2.ReimbAllowCons,
			FeeDirLabBillingCurrency = p2.FeeDirLabBillingCurrency,
			FeeDirExpBillingCurrency = p2.FeeDirExpBillingCurrency,
			ReimbAllowExpBillingCurrency = p2.ReimbAllowExpBillingCurrency,
			ReimbAllowConsBillingCurrency = p2.ReimbAllowConsBillingCurrency,
			FeeDirLabFunctionalCurrency = p2.FeeDirLabFunctionalCurrency,
			FeeDirExpFunctionalCurrency = p2.FeeDirExpFunctionalCurrency,
			ReimbAllowExpFunctionalCurrency = p2.ReimbAllowExpFunctionalCurrency,
			ReimbAllowConsFunctionalCurrency = p2.ReimbAllowConsFunctionalCurrency				
			FROM (SELECT IsNull(Sum(p2.Fee),0) Fee,IsNull(Sum(p2.ConsultFee),0) ConsultFee,IsNull(Sum(p2.ReimbAllow),0) ReimbAllow,IsNull(Sum(p2.FeeBillingCurrency),0) FeeBillingCurrency,
					IsNull(Sum(p2.ConsultFeeBillingCurrency),0) ConsultFeeBillingCurrency,IsNull(Sum(p2.ReimbAllowBillingCurrency),0) ReimbAllowBillingCurrency,
					IsNull(Sum(p2.FeeFunctionalCurrency),0) FeeFunctionalCurrency,IsNull(Sum(p2.ConsultFeeFunctionalCurrency),0) ConsultFeeFunctionalCurrency,
					IsNull(Sum(p2.ReimbAllowFunctionalCurrency),0) ReimbAllowFunctionalCurrency,
					IsNull(Sum(p2.FeeDirLab),0) FeeDirLab,IsNull(Sum(p2.FeeDirExp),0) FeeDirExp,
					IsNull(Sum(p2.ReimbAllowExp),0) ReimbAllowExp,IsNull(Sum(p2.ReimbAllowCons),0) ReimbAllowCons,
					IsNull(Sum(p2.FeeDirLabBillingCurrency),0) FeeDirLabBillingCurrency,IsNull(Sum(p2.FeeDirExpBillingCurrency),0) FeeDirExpBillingCurrency,
					IsNull(Sum(p2.ReimbAllowExpBillingCurrency),0) ReimbAllowExpBillingCurrency,IsNull(Sum(p2.ReimbAllowConsBillingCurrency),0) ReimbAllowConsBillingCurrency,
					IsNull(Sum(p2.FeeDirLabFunctionalCurrency),0) FeeDirLabFunctionalCurrency,IsNull(Sum(p2.FeeDirExpFunctionalCurrency),0)FeeDirExpFunctionalCurrency,
					IsNull(Sum(p2.ReimbAllowExpFunctionalCurrency),0) ReimbAllowExpFunctionalCurrency,IsNull(Sum(p2.ReimbAllowConsFunctionalCurrency),0) ReimbAllowConsFunctionalCurrency						
					FROM PRTemplate p2 WHERE p2.WBS1 = @project AND p2.WBS2 <> N' ' AND p2.WBS3 = N' ') p2
			WHERE PRTemplate.WBS1 = @project AND PRTemplate.WBS2 = N' ' AND PRTemplate.WBS3 = N' ' AND PRTemplate.SubLevel = 'Y'
			END
		   ELSE
			BEGIN
			UPDATE PR SET 
			Fee = p2.Fee,
			ConsultFee = p2.ConsultFee,
			ReimbAllow = p2.ReimbAllow,
			FeeBillingCurrency = p2.FeeBillingCurrency,
			ConsultFeeBillingCurrency = p2.ConsultFeeBillingCurrency,
			ReimbAllowBillingCurrency = p2.ReimbAllowBillingCurrency,
			FeeFunctionalCurrency = p2.FeeFunctionalCurrency,
			ConsultFeeFunctionalCurrency = p2.ConsultFeeFunctionalCurrency,
			ReimbAllowFunctionalCurrency = p2.ReimbAllowFunctionalCurrency,
			FeeDirLab = p2.FeeDirLab,
			FeeDirExp = p2.FeeDirExp,
			ReimbAllowExp = p2.ReimbAllowExp,
			ReimbAllowCons = p2.ReimbAllowCons,
			FeeDirLabBillingCurrency = p2.FeeDirLabBillingCurrency,
			FeeDirExpBillingCurrency = p2.FeeDirExpBillingCurrency,
			ReimbAllowExpBillingCurrency = p2.ReimbAllowExpBillingCurrency,
			ReimbAllowConsBillingCurrency = p2.ReimbAllowConsBillingCurrency,
			FeeDirLabFunctionalCurrency = p2.FeeDirLabFunctionalCurrency,
			FeeDirExpFunctionalCurrency = p2.FeeDirExpFunctionalCurrency,
			ReimbAllowExpFunctionalCurrency = p2.ReimbAllowExpFunctionalCurrency,
			ReimbAllowConsFunctionalCurrency = p2.ReimbAllowConsFunctionalCurrency				
				
			FROM (SELECT WBS1,WBS2,IsNull(Sum(p2.Fee),0) Fee,IsNull(Sum(p2.ConsultFee),0) ConsultFee,IsNull(Sum(p2.ReimbAllow),0) ReimbAllow,IsNull(Sum(p2.FeeBillingCurrency),0) FeeBillingCurrency,
					IsNull(Sum(p2.ConsultFeeBillingCurrency),0) ConsultFeeBillingCurrency,IsNull(Sum(p2.ReimbAllowBillingCurrency),0) ReimbAllowBillingCurrency,
					IsNull(Sum(p2.FeeFunctionalCurrency),0) FeeFunctionalCurrency,IsNull(Sum(p2.ConsultFeeFunctionalCurrency),0) ConsultFeeFunctionalCurrency,
					IsNull(Sum(p2.ReimbAllowFunctionalCurrency),0) ReimbAllowFunctionalCurrency,
					IsNull(Sum(p2.FeeDirLab),0) FeeDirLab,IsNull(Sum(p2.FeeDirExp),0) FeeDirExp,
					IsNull(Sum(p2.ReimbAllowExp),0) ReimbAllowExp,IsNull(Sum(p2.ReimbAllowCons),0) ReimbAllowCons,
					IsNull(Sum(p2.FeeDirLabBillingCurrency),0) FeeDirLabBillingCurrency,IsNull(Sum(p2.FeeDirExpBillingCurrency),0) FeeDirExpBillingCurrency,
					IsNull(Sum(p2.ReimbAllowExpBillingCurrency),0) ReimbAllowExpBillingCurrency,IsNull(Sum(p2.ReimbAllowConsBillingCurrency),0) ReimbAllowConsBillingCurrency,
					IsNull(Sum(p2.FeeDirLabFunctionalCurrency),0) FeeDirLabFunctionalCurrency,IsNull(Sum(p2.FeeDirExpFunctionalCurrency),0)FeeDirExpFunctionalCurrency,
					IsNull(Sum(p2.ReimbAllowExpFunctionalCurrency),0) ReimbAllowExpFunctionalCurrency,IsNull(Sum(p2.ReimbAllowConsFunctionalCurrency),0) ReimbAllowConsFunctionalCurrency						

					FROM PR p2 WHERE p2.WBS1 = @project AND p2.WBS2 = p2.WBS2 AND p2.WBS3 <> N' '
					GROUP BY WBS1, WBS2) p2
			WHERE PR.WBS1 = @project AND PR.WBS2 = p2.WBS2 AND PR.WBS3 = N' ' AND PR.SubLevel = 'Y'

			UPDATE PR SET 
			Fee = p2.Fee,
			ConsultFee = p2.ConsultFee,
			ReimbAllow = p2.ReimbAllow,
			FeeBillingCurrency = p2.FeeBillingCurrency,
			ConsultFeeBillingCurrency = p2.ConsultFeeBillingCurrency,
			ReimbAllowBillingCurrency = p2.ReimbAllowBillingCurrency,
			FeeFunctionalCurrency = p2.FeeFunctionalCurrency,
			ConsultFeeFunctionalCurrency = p2.ConsultFeeFunctionalCurrency,
			ReimbAllowFunctionalCurrency = p2.ReimbAllowFunctionalCurrency,
			FeeDirLab = p2.FeeDirLab,
			FeeDirExp = p2.FeeDirExp,
			ReimbAllowExp = p2.ReimbAllowExp,
			ReimbAllowCons = p2.ReimbAllowCons,
			FeeDirLabBillingCurrency = p2.FeeDirLabBillingCurrency,
			FeeDirExpBillingCurrency = p2.FeeDirExpBillingCurrency,
			ReimbAllowExpBillingCurrency = p2.ReimbAllowExpBillingCurrency,
			ReimbAllowConsBillingCurrency = p2.ReimbAllowConsBillingCurrency,
			FeeDirLabFunctionalCurrency = p2.FeeDirLabFunctionalCurrency,
			FeeDirExpFunctionalCurrency = p2.FeeDirExpFunctionalCurrency,
			ReimbAllowExpFunctionalCurrency = p2.ReimbAllowExpFunctionalCurrency,
			ReimbAllowConsFunctionalCurrency = p2.ReimbAllowConsFunctionalCurrency		
				
			FROM (SELECT IsNull(Sum(p2.Fee),0) Fee,IsNull(Sum(p2.ConsultFee),0) ConsultFee,IsNull(Sum(p2.ReimbAllow),0) ReimbAllow,IsNull(Sum(p2.FeeBillingCurrency),0) FeeBillingCurrency,
					IsNull(Sum(p2.ConsultFeeBillingCurrency),0) ConsultFeeBillingCurrency,IsNull(Sum(p2.ReimbAllowBillingCurrency),0) ReimbAllowBillingCurrency,
					IsNull(Sum(p2.FeeFunctionalCurrency),0) FeeFunctionalCurrency,IsNull(Sum(p2.ConsultFeeFunctionalCurrency),0) ConsultFeeFunctionalCurrency,
					IsNull(Sum(p2.ReimbAllowFunctionalCurrency),0) ReimbAllowFunctionalCurrency,
					IsNull(Sum(p2.FeeDirLab),0) FeeDirLab,IsNull(Sum(p2.FeeDirExp),0) FeeDirExp,
					IsNull(Sum(p2.ReimbAllowExp),0) ReimbAllowExp,IsNull(Sum(p2.ReimbAllowCons),0) ReimbAllowCons,
					IsNull(Sum(p2.FeeDirLabBillingCurrency),0) FeeDirLabBillingCurrency,IsNull(Sum(p2.FeeDirExpBillingCurrency),0) FeeDirExpBillingCurrency,
					IsNull(Sum(p2.ReimbAllowExpBillingCurrency),0) ReimbAllowExpBillingCurrency,IsNull(Sum(p2.ReimbAllowConsBillingCurrency),0) ReimbAllowConsBillingCurrency,
					IsNull(Sum(p2.FeeDirLabFunctionalCurrency),0) FeeDirLabFunctionalCurrency,IsNull(Sum(p2.FeeDirExpFunctionalCurrency),0)FeeDirExpFunctionalCurrency,
					IsNull(Sum(p2.ReimbAllowExpFunctionalCurrency),0) ReimbAllowExpFunctionalCurrency,IsNull(Sum(p2.ReimbAllowConsFunctionalCurrency),0) ReimbAllowConsFunctionalCurrency						

					FROM PR p2 WHERE p2.WBS1 = @project AND p2.WBS2 <> N' ' AND p2.WBS3 = N' ') p2
			WHERE PR.WBS1 = @project AND PR.WBS2 = N' ' AND PR.WBS3 = N' ' AND PR.SubLevel = 'Y'
			END
		End
END
GO
