SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_Vision_CFGUserSettings](
	@FieldName		varchar(255),
	@FieldCaption	varchar(255),--For Label and Check types, if null use Name from column setup
	@ElementType	varchar(255),--Input,Label,Check,Grid
	@TopPos			int = NULL,
	@LeftPos		int = NULL,
	@Height			int = NULL,
	@Width			int = NULL,
	@RelativeToFieldName varchar(255) = NULL,--optional fieldname of an existing field to position relative to.  When present, Top and Left pos are added to this fields values
	@TabID			varchar(255)='accountingTab',
	@InfoCenterArea	varchar(255)='Projects',--currently only Projects is supported
	@Culture		varchar(255)='en-US'
	)

AS BEGIN

	set nocount on
	declare @sql	varchar(max)
	declare @debug	char(1)
	set @debug='Y'
	
	print @FieldCaption
	set @FieldCaption = REPLACE(@FieldCaption,'''','''''')
	
	set @sql='IF NOT EXISTS (SELECT * FROM FW_CFGUserSettingsData where GridID = ''X'' and [ElementID] = ''' + @FieldName+ ''' and InfoCenterArea = ''' + @InfoCenterArea+ ''') ' +
		' INSERT INTO FW_CFGUserSettingsData ([InfoCenterArea],[TabID],[GridID],[ElementID],[ElementType],[DesignerCreated],[RecordLimit]) ' +
		' VALUES (''' + @InfoCenterArea+ ''' ,''' + @TabID+ ''' ,''X'' ,''' + @FieldName+ ''','''+ @ElementType + ''',''N'',0) ' 
	if @debug='Y' print @sql
	execute(@sql)
	
	if @RelativeToFieldName is not NULL
	BEGIN
		select @TopPos = ISNULL(TopPos,800) + @TopPos,@LeftPos = ISNULL(LeftPos,10) + @LeftPos
		from FW_CFGUserSettingsLabels where InfoCenterArea = @InfoCenterArea and TabID = @TabID and ElementID = @RelativeToFieldName and UICultureName = @Culture 
		
	END
	print @FieldCaption
	
	set @sql='IF NOT EXISTS (SELECT * FROM FW_CFGUserSettingsLabels where GridID = ''X'' and [ElementID] = ''' + @FieldName+ ''' and InfoCenterArea = ''' + @InfoCenterArea+ ''' and UICultureName ='''+ @Culture +''') ' +
		' INSERT INTO FW_CFGUserSettingsLabels ([InfoCenterArea],[TabID],[GridID],[ElementID],[UICultureName],[TopPos],[LeftPos],[Height],[Width],Caption1) ' +
		' VALUES (''' + @InfoCenterArea+ ''' ,''' + @TabID+ ''' ,''X'' ,''' + @FieldName+ ''',''' + @Culture + ''',' + ISNULL(Cast(@TopPos as varchar(10)),'NULL') + ','+ ISNULL(Cast(@LeftPos as varchar(10)),'NULL') + ','+ ISNULL(Cast(@Height as varchar(10)),'NULL')+ ','+ ISNULL(Cast(@Width as varchar(10)),'NULL')+ ',' + CASE WHEN @FieldCaption IS NULL THEN 'NULL' else '''' + @FieldCaption + '''' end + ' ) ' + 
		' ELSE UPDATE FW_CFGUserSettingsLabels ' +
		' SET [TopPos] = ' + ISNULL(Cast(@TopPos as varchar(10)),'NULL') + ',[LeftPos] = '+ ISNULL(Cast(@LeftPos as varchar(10)),'NULL') + ',[Height]= ' + ISNULL(Cast(@Height as varchar(10)),'NULL') + ',[Width] = '+ ISNULL(Cast(@Width as varchar(10)),'NULL') + ',Caption1 = ' + CASE WHEN @FieldCaption IS NULL THEN 'NULL' else '''' + @FieldCaption + '''' end +
		' WHERE GridID = ''X'' and [ElementID] = ''' + @FieldName+ ''' and InfoCenterArea = ''' + @InfoCenterArea+ ''' and UICultureName ='''+ @Culture +''' ' 
		 
	if @debug='Y' print @sql
	execute(@sql)
	print @FieldCaption
	

END

GO
