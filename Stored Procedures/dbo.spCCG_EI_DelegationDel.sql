SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_DelegationDel] (@Id varchar(32), @Username Nvarchar(32))
AS BEGIN
/*
	Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved.

	exec spCCG_EI_DelegationIns 'd','ADMIN','00001','00003','3/1/2017','3/8/2017',0,'N',null,null,'N'
	exec spCCG_EI_DelegationDel 'd','ADMIN'
	select * from CCG_EI_Delegation
	select * from CCG_EI_HistoryDelegation
*/
	set nocount on
	BEGIN TRANSACTION
	BEGIN TRY
		declare @EmployeeOrig Nvarchar(20), @DelegateOrig Nvarchar(20), @FromDateOrig datetime, @ToDateOrig datetime
		declare @DualOrig char(1), @ApprovedByOrig Nvarchar(20), @ApprovedOnOrig datetime, @ForClientIdOrig varchar(32), @ForWBS1Orig Nvarchar(30)

		select @EmployeeOrig=d.Employee, @DelegateOrig=d.Delegate, @FromDateOrig=FromDate, @ToDateOrig=ToDate,
				@DualOrig=Dual, @ApprovedByOrig=d.ApprovedBy, @ApprovedOnOrig=d.ApprovedOn, @ForClientIdOrig=ForClientId, @ForWBS1Orig=ForWBS1
			from CCG_EI_Delegation d
			where d.Id = @Id

		delete from CCG_EI_Delegation where Id=@Id

		insert into CCG_EI_HistoryDelegation (Id,Employee,Delegate,FromDate,ToDate, ForClientId, ForWBS1, ActionTaken,ActionDate,ActionTakenBy,ActionRecipient,Description)
			select @Id,@EmployeeOrig,@DelegateOrig,@FromDateOrig,@ToDateOrig, @ForClientIdOrig, @ForWBS1Orig, 'Deleted',getutcdate(),Employee,null,N'Dual = ' + @DualOrig
			from SEUser where Username=@Username

		COMMIT TRANSACTION
		select 0 as Result, '' as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		select -1 as Result, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage
	END CATCH
END
GO
