SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[spCCG_OpportunityTasksGridDelete] @OpportunityID varchar (32), @PhaseCode varchar (7)
AS
/*
Copyright 2017 Central Consulting Group.   All rights reserved.
03/14/2017 David Springer
           Delete Tasks for this phase.
           Call this from an Opportunity Phase DELETE workflow.
*/
SET NOCOUNT ON
BEGIN
   Delete From Opportunities_Tasks Where OpportunityID = @OpportunityID and CustTaskPhaseCode = @PhaseCode
END
GO
