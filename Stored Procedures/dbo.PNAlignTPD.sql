SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PNAlignTPD]
  @strPlanID VARCHAR(32)
AS

BEGIN -- Procedure PNAlignTPD

  -->>> This procedure must be called after all of the summary TPD have been deleted.

  SET NOCOUNT ON
  
  DECLARE @intHrDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intRtCostDecimals int
  DECLARE @intRtBillDecimals int
  DECLARE @intLabRevDecimals int
  DECLARE @intECURevDecimals int
  
  DECLARE @intBaselineLabCount int
  DECLARE @intBaselineExpCount int
  DECLARE @intBaselineConCount int
  DECLARE @intBaselineUntCount int
  
  DECLARE @intPlannedLabCount int
  DECLARE @intPlannedExpCount int
  DECLARE @intPlannedConCount int
  DECLARE @intPlannedUntCount int
  
  DECLARE @strCompany Nvarchar(14)
  
  DECLARE @strUnpostedFlg varchar(1)
  DECLARE @sintGRMethod smallint
  DECLARE @sintCostRtMethod smallint
  DECLARE @sintBillingRtMethod smallint
  
  DECLARE @intCostRtTableNo int
  DECLARE @intBillingRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int
  
  DECLARE @strExpTab varchar(1)
  DECLARE @strConTab varchar(1)
  
  -- Declare Temp tables.
  
  DECLARE @tabCalendarInterval
    TABLE(PlanID varchar(32) COLLATE database_default,
          StartDate datetime,
          EndDate datetime,
          PeriodScale varchar(1) COLLATE database_default
          PRIMARY KEY(PlanID, StartDate, EndDate))
          
	DECLARE @tabPLabTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         AssignmentID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodHrs decimal(19,4), 
           PeriodCount smallint, 
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 
  
	DECLARE @tabPExpTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         ExpenseID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PeriodCount smallint, 
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 

	DECLARE @tabPConTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         ConsultantID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PeriodCount smallint, 
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 

	DECLARE @tabBLabTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         AssignmentID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodHrs decimal(19,4), 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PeriodRev decimal(19,4), 
           PeriodCount smallint, 
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 
  
	DECLARE @tabBExpTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         ExpenseID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PeriodRev decimal(19,4), 
           PeriodCount smallint, 
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 

	DECLARE @tabBConTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         ConsultantID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PeriodRev decimal(19,4), 
           PeriodCount smallint, 
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get settings from PNPlan and CFGResourcePlanning.
  -- At this point, there is no way to know what was the basis used in calculation of Baseline Revenue numbers.
  -- Therefore, we decided to use the current basis to determine number of decimal digits to be used
  -- in the realignment of Baseline Revenue numbers.
  
  SELECT 
    @strCompany = P.Company,
    @strUnpostedFlg = P.UnpostedFlg,
    @sintGRMethod = P.GRMethod,
    @intGenResTableNo = P.GenResTableNo,
    @intGRBillTableNo = P.GRBillTableNo,
    @sintCostRtMethod = P.CostRtMethod,
    @sintBillingRtMethod = P.BillingRtMethod,
    @intCostRtTableNo = P.CostRtTableNo,
    @intBillingRtTableNo = P.BillingRtTableNo,
    @strExpTab = CRP.ExpTab,
    @strConTab = CRP.ConTab,
    @intHrDecimals = 2,
    @intAmtCostDecimals = 4, 
    @intAmtBillDecimals = 4, 
    @intRtCostDecimals = 4, 
    @intRtBillDecimals = 4, 
    @intLabRevDecimals = 4, 
    @intECURevDecimals = 4
    FROM PNPlan AS P, CFGResourcePlanning AS CRP 
    WHERE P.PlanID = @strPlanID AND CRP.Company = P.Company 

/*

  SELECT 
    @strCompany = P.Company,
    @strUnpostedFlg = P.UnpostedFlg,
    @sintGRMethod = P.GRMethod,
    @intGenResTableNo = P.GenResTableNo,
    @intGRBillTableNo = P.GRBillTableNo,
    @sintCostRtMethod = P.CostRtMethod,
    @sintBillingRtMethod = P.BillingRtMethod,
    @intCostRtTableNo = P.CostRtTableNo,
    @intBillingRtTableNo = P.BillingRtTableNo,
    @strExpTab = CRP.ExpTab,
    @strConTab = CRP.ConTab,
    @intHrDecimals = HrDecimals,
    @intAmtCostDecimals = CASE WHEN AmtDecimals = -1 THEN CC.DecimalPlaces ELSE AmtDecimals END, 
    @intAmtBillDecimals = CASE WHEN AmtDecimals = -1 THEN BC.DecimalPlaces ELSE AmtDecimals END, 
    @intRtCostDecimals = 4, 
    @intRtBillDecimals = 4, 
    @intLabRevDecimals = CASE WHEN P.LabMultType < 2 
                              THEN CASE WHEN AmtDecimals = -1 THEN CC.DecimalPlaces ELSE AmtDecimals END 
                              ELSE CASE WHEN AmtDecimals = -1 THEN BC.DecimalPlaces ELSE AmtDecimals END 
                         END, 
    @intECURevDecimals = CASE WHEN P.ReimbMethod = 'C' 
                              THEN CASE WHEN AmtDecimals = -1 THEN CC.DecimalPlaces ELSE AmtDecimals END 
                              ELSE CASE WHEN AmtDecimals = -1 THEN BC.DecimalPlaces ELSE AmtDecimals END 
                         END
    FROM PNPlan AS P, CFGResourcePlanning AS CRP, CFGCurrency AS CC, CFGCurrency AS BC 
    WHERE P.PlanID = @strPlanID AND CRP.Company = P.Company AND
          CC.Code = P.CostCurrencyCode AND BC.Code = P.BillingCurrencyCode 

*/

  -- Check to see if there is any time-phased data
  
    SELECT @intBaselineLabCount = COUNT(*) FROM PNBaselineLabor WHERE PlanID = @strPlanID
    SELECT @intPlannedLabCount = COUNT(*) FROM PNPlannedLabor WHERE PlanID = @strPlanID
    
    IF (@strExpTab = 'Y')
      BEGIN
        SELECT @intBaselineExpCount = COUNT(*) FROM PNBaselineExpenses WHERE PlanID = @strPlanID
        SELECT @intPlannedExpCount = COUNT(*) FROM PNPlannedExpenses WHERE PlanID = @strPlanID
      END
  
    IF (@strConTab = 'Y')
      BEGIN
        SELECT @intBaselineConCount = COUNT(*) FROM PNBaselineConsultant WHERE PlanID = @strPlanID
        SELECT @intPlannedConCount = COUNT(*) FROM PNPlannedConsultant WHERE PlanID = @strPlanID
      END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save Calendar Intervals into a temp table and add an overflow Calendar Interval.
  
  INSERT @tabCalendarInterval(PlanID, StartDate, EndDate, PeriodScale)
  SELECT PlanID, StartDate, EndDate, PeriodScale FROM
    (SELECT PlanID AS PlanID, CAST('19900101' AS datetime) AS StartDate,
			 DATEADD(d, -1, MIN(StartDate)) AS EndDate,
			 'o' AS PeriodScale
			 FROM PNCalendarInterval WHERE PlanID = @strPlanID
			 GROUP BY PlanID
		 UNION ALL
     SELECT PlanID AS PlanID, StartDate AS StartDate, EndDate AS EndDate, PeriodScale AS PeriodScale
       FROM PNCalendarInterval WHERE PlanID = @strPlanID
     UNION ALL
     SELECT PlanID AS PlanID, DATEADD(d, 1, MAX(EndDate)) AS StartDate,
       DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate,
       'o' AS PeriodScale
       FROM PNCalendarInterval WHERE PlanID = @strPlanID
       GROUP BY PlanID) AS CI

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
              
  IF (@intPlannedLabCount > 0)
    BEGIN
    
      -- Save Planned Labor time-phased data rows.

      INSERT @tabPLabTPD
        (RowID,
         TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         AssignmentID,
         StartDate, 
         EndDate, 
         PeriodHrs,
         PeriodCount, 
         PeriodScale)
       SELECT
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
        TimePhaseID AS TimePhaseID,
        CIStartDate AS CIStartDate,
        PlanID AS PlanID, 
        TaskID AS TaskID,
        AssignmentID AS AssignmentID,
        StartDate AS StartDate, 
        EndDate AS EndDate, 
        ROUND(ISNULL(PeriodHrs, 0), @intHrDecimals) AS PeriodHrs,
        PeriodCount AS PeriodCount, 
        PeriodScale AS PeriodScale
        FROM (SELECT -- For Childless Task Rows.
                CI.StartDate AS CIStartDate,
                TPD.TimePhaseID AS TimePhaseID,
                T.PlanID AS PlanID, 
                T.TaskID AS TaskID,
                NULL AS AssignmentID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodHrs * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                     THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                              CASE WHEN TPD.EndDate < CI.EndDate 
                                                                     THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                              TPD.StartDate, TPD.EndDate,
                                                              @strCompany)
                       ELSE PeriodHrs END AS PeriodHrs,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI 
                  LEFT JOIN PNTask AS T ON CI.PlanID = T.PlanID
                  INNER JOIN PNPlannedLabor AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                    AND TPD.AssignmentID IS NULL AND T.ChildrenCount = 0
                WHERE TPD.TimePhaseID IS NOT NULL
              UNION ALL
              SELECT -- For Assignment Rows.
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                A.PlanID AS PlanID, 
                A.TaskID AS TaskID,
                A.AssignmentID AS AssignmentID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodHrs * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                     THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                              CASE WHEN TPD.EndDate < CI.EndDate 
                                                                     THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                              TPD.StartDate, TPD.EndDate,
                                                              @strCompany)
                       ELSE PeriodHrs END AS PeriodHrs,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI 
                  LEFT JOIN PNAssignment AS A ON CI.PlanID = A.PlanID
                  INNER JOIN PNPlannedLabor AS TPD ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                    AND TPD.AssignmentID = A.AssignmentID
                WHERE TPD.TimePhaseID IS NOT NULL) AS X
        WHERE (PeriodHrs IS NOT NULL AND PeriodHrs != 0)
     
      -- Adjust Planned Labor time-phased data to compensate for rounding errors.
     
      UPDATE @tabPLabTPD SET PeriodHrs = (TPD.PeriodHrs + D.DeltaHrs)
        FROM @tabPLabTPD AS TPD INNER JOIN 
          (SELECT YTPD.TimePhaseID AS TimePhaseID, (YTPD.PeriodHrs - SUM(ISNULL(XTPD.PeriodHrs, 0))) AS DeltaHrs
            FROM @tabPLabTPD AS XTPD INNER JOIN PNPlannedLabor AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
            GROUP BY YTPD.TimePhaseID, YTPD.PeriodHrs) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
        WHERE RowID IN
          (SELECT RowID FROM @tabPLabTPD AS ATPD INNER JOIN
            (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabPLabTPD GROUP BY TimePhaseID) AS BTPD
                ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
                
    END -- IF (@intPlannedLabCount > 0)
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strExpTab = 'Y' AND @intPlannedExpCount > 0)
    BEGIN
    
      -- Save Planned Expense time-phased data rows.

      INSERT @tabPExpTPD
        (RowID,
         TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         ExpenseID,
         StartDate, 
         EndDate, 
         PeriodCost,
         PeriodBill,
         PeriodCount, 
         PeriodScale)
      SELECT 
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
        TimePhaseID AS TimePhaseID,
        CIStartDate AS CIStartDate,
        PlanID AS PlanID, 
        TaskID AS TaskID,
        ExpenseID AS ExpenseID,
        StartDate AS StartDate, 
        EndDate AS EndDate, 
        ROUND(ISNULL(PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
        ROUND(ISNULL(PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill, 
        PeriodCount AS PeriodCount, 
        PeriodScale AS PeriodScale
        FROM (SELECT 
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                T.PlanID AS PlanID, 
                T.TaskID AS TaskID,
                NULL AS ExpenseID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE 
                  WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                  THEN PeriodCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                               THEN TPD.StartDate 
                                                               ELSE CI.StartDate END, 
                                                          CASE WHEN TPD.EndDate < CI.EndDate 
                                                               THEN TPD.EndDate 
                                                               ELSE CI.EndDate END, 
                                                          TPD.StartDate, TPD.EndDate,
                                                          @strCompany)
                  ELSE PeriodCost 
                 END AS PeriodCost,
                 CASE 
                   WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                   THEN PeriodBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                THEN TPD.StartDate 
                                                                ELSE CI.StartDate END, 
                                                           CASE WHEN TPD.EndDate < CI.EndDate 
                                                                THEN TPD.EndDate 
                                                                ELSE CI.EndDate END, 
                                                          TPD.StartDate, TPD.EndDate,
                                                          @strCompany)
                   ELSE PeriodBill 
                END AS PeriodBill,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN PNTask AS T ON CI.PlanID = T.PlanID
                  LEFT JOIN PNPlan AS P ON CI.PlanID = P.PlanID
                  INNER JOIN PNPlannedExpenses AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                    AND TPD.ExpenseID IS NULL AND T.ChildrenCount = 0
                WHERE TPD.TimePhaseID IS NOT NULL 
              UNION ALL
              SELECT 
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                E.PlanID AS PlanID, 
                E.TaskID AS TaskID,
                E.ExpenseID AS ExpenseID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE 
                  WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                  THEN PeriodCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                               THEN TPD.StartDate 
                                                               ELSE CI.StartDate END, 
                                                          CASE WHEN TPD.EndDate < CI.EndDate 
                                                               THEN TPD.EndDate 
                                                               ELSE CI.EndDate END, 
                                                          TPD.StartDate, TPD.EndDate,
                                                          @strCompany)
                       ELSE PeriodCost 
                END AS PeriodCost,
                CASE 
                  WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                  THEN PeriodBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                               THEN TPD.StartDate 
                                                               ELSE CI.StartDate END, 
                                                          CASE WHEN TPD.EndDate < CI.EndDate 
                                                               THEN TPD.EndDate 
                                                               ELSE CI.EndDate END, 
                                                          TPD.StartDate, TPD.EndDate,
                                                          @strCompany)
                  ELSE PeriodBill 
                END AS PeriodBill,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN PNExpense AS E ON CI.PlanID = E.PlanID
                  LEFT JOIN PNPlan AS P ON CI.PlanID = P.PlanID
                  INNER JOIN PNPlannedExpenses AS TPD ON TPD.PlanID = E.PlanID AND TPD.TaskID = E.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                    AND TPD.ExpenseID = E.ExpenseID
                WHERE TPD.TimePhaseID IS NOT NULL) AS X
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
        
      -- Adjust Planned Expense time-phased data to compensate for rounding errors.
     
      UPDATE @tabPExpTPD SET 
        PeriodCost = (TPD.PeriodCost + D.DeltaCost),
        PeriodBill = (TPD.PeriodBill + D.DeltaBill)
        FROM @tabPExpTPD AS TPD INNER JOIN 
          (SELECT YTPD.TimePhaseID AS TimePhaseID, 
            (YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaCost,
            (YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaBill
            FROM @tabPExpTPD AS XTPD INNER JOIN PNPlannedExpenses AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
            GROUP BY YTPD.TimePhaseID, YTPD.PeriodCost, YTPD.PeriodBill) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
          INNER JOIN PNPlan AS P ON TPD.PlanID = P.PlanID
        WHERE RowID IN
          (SELECT RowID FROM @tabPExpTPD AS ATPD INNER JOIN
            (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabPExpTPD GROUP BY TimePhaseID) AS BTPD
                ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
                
    END -- IF (@strExpTab = 'Y' AND @intPlannedExpCount > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strConTab = 'Y' AND @intPlannedConCount > 0)
    BEGIN
    
      -- Save Planned Consultant time-phased data rows.

      INSERT @tabPConTPD
        (RowID,
         TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         ConsultantID,
         StartDate, 
         EndDate, 
         PeriodCost,
         PeriodBill,
         PeriodCount, 
         PeriodScale)
      SELECT 
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
        TimePhaseID AS TimePhaseID,
        CIStartDate AS CIStartDate,
        PlanID AS PlanID, 
        TaskID AS TaskID,
        ConsultantID AS ConsultantID,
        StartDate AS StartDate, 
        EndDate AS EndDate, 
        ROUND(ISNULL(PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
        ROUND(ISNULL(PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill, 
        PeriodCount AS PeriodCount, 
        PeriodScale AS PeriodScale
        FROM (SELECT 
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                T.PlanID AS PlanID, 
                T.TaskID AS TaskID,
                NULL AS ConsultantID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE 
                  WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                  THEN PeriodCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                               THEN TPD.StartDate 
                                                               ELSE CI.StartDate END, 
                                                          CASE WHEN TPD.EndDate < CI.EndDate 
                                                               THEN TPD.EndDate 
                                                               ELSE CI.EndDate END, 
                                                          TPD.StartDate, TPD.EndDate,
                                                          @strCompany)
                  ELSE PeriodCost 
                END AS PeriodCost,
                CASE 
                  WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                  THEN PeriodBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                               THEN TPD.StartDate 
                                                               ELSE CI.StartDate END, 
                                                          CASE WHEN TPD.EndDate < CI.EndDate 
                                                               THEN TPD.EndDate 
                                                               ELSE CI.EndDate END, 
                                                          TPD.StartDate, TPD.EndDate,
                                                          @strCompany)
                  ELSE PeriodBill 
                END AS PeriodBill,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN PNTask AS T ON CI.PlanID = T.PlanID
                  LEFT JOIN PNPlan AS P ON CI.PlanID = P.PlanID
                  INNER JOIN PNPlannedConsultant AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                    AND TPD.ConsultantID IS NULL AND T.ChildrenCount = 0
                WHERE TPD.TimePhaseID IS NOT NULL 
              UNION ALL
              SELECT 
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                C.PlanID AS PlanID, 
                C.TaskID AS TaskID,
                C.ConsultantID AS ConsultantID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE 
                  WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                  THEN PeriodCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                               THEN TPD.StartDate 
                                                               ELSE CI.StartDate END, 
                                                          CASE WHEN TPD.EndDate < CI.EndDate 
                                                               THEN TPD.EndDate 
                                                               ELSE CI.EndDate END, 
                                                          TPD.StartDate, TPD.EndDate,
                                                          @strCompany)
                  ELSE PeriodCost 
                END AS PeriodCost,
                CASE 
                  WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                  THEN PeriodBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                               THEN TPD.StartDate 
                                                               ELSE CI.StartDate END, 
                                                          CASE WHEN TPD.EndDate < CI.EndDate 
                                                               THEN TPD.EndDate 
                                                               ELSE CI.EndDate END, 
                                                          TPD.StartDate, TPD.EndDate,
                                                          @strCompany)
                  ELSE PeriodBill 
                END AS PeriodBill,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN PNConsultant AS C ON CI.PlanID = C.PlanID
                  LEFT JOIN PNPlan AS P ON CI.PlanID = P.PlanID
                  INNER JOIN PNPlannedConsultant AS TPD ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
                    AND TPD.ConsultantID = C.ConsultantID
                WHERE TPD.TimePhaseID IS NOT NULL) AS X
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
            
      -- Adjust Planned Consultant time-phased data to compensate for rounding errors.
     
      UPDATE @tabPConTPD SET 
        PeriodCost = (TPD.PeriodCost + D.DeltaCost),
        PeriodBill = (TPD.PeriodBill + D.DeltaBill)
        FROM @tabPConTPD AS TPD INNER JOIN 
          (SELECT YTPD.TimePhaseID AS TimePhaseID, 
            (YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaCost,
            (YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaBill
            FROM @tabPConTPD AS XTPD INNER JOIN PNPlannedConsultant AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
            GROUP BY YTPD.TimePhaseID, YTPD.PeriodCost, YTPD.PeriodBill) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
          INNER JOIN PNPlan AS P ON TPD.PlanID = P.PlanID
        WHERE RowID IN
          (SELECT RowID FROM @tabPConTPD AS ATPD INNER JOIN
            (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabPConTPD GROUP BY TimePhaseID) AS BTPD
                ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
                
    END -- IF (@strConTab = 'Y' AND @intPlannedConCount > 0)
            
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@intBaselineLabCount > 0)
    BEGIN
    
      -- Save Baseline Labor time-phased data rows.

      INSERT @tabBLabTPD
        (RowID,
         TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         AssignmentID,
         StartDate, 
         EndDate, 
         PeriodHrs,
         PeriodCost,
         PeriodBill,
         PeriodRev,
         PeriodCount, 
         PeriodScale)
      SELECT 
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
        TimePhaseID AS TimePhaseID,
        CIStartDate AS CIStartDate,
        PlanID AS PlanID, 
        TaskID AS TaskID,
        AssignmentID AS AssignmentID,
        StartDate AS StartDate, 
        EndDate AS EndDate, 
        ROUND(ISNULL(PeriodHrs, 0), @intHrDecimals) AS PeriodHrs, 
        ROUND(ISNULL(PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
        ROUND(ISNULL(PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill, 
        ROUND(ISNULL(PeriodRev, 0), @intLabRevDecimals) AS PeriodRev, 
        PeriodCount AS PeriodCount, 
        PeriodScale AS PeriodScale
        FROM (SELECT 
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                T.PlanID AS PlanID, 
                T.TaskID AS TaskID,
                TPD.AssignmentID AS AssignmentID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodHrs * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                     THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                              CASE WHEN TPD.EndDate < CI.EndDate 
                                                                     THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                              TPD.StartDate, TPD.EndDate,
                                                              @strCompany)
                       ELSE PeriodHrs END AS PeriodHrs,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                      ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodCost END AS PeriodCost,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                      ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                      ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodBill END AS PeriodBill,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodRev * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                     THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                              CASE WHEN TPD.EndDate < CI.EndDate 
                                                                     THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                              TPD.StartDate, TPD.EndDate,
                                                              @strCompany)
                       ELSE PeriodRev END AS PeriodRev,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN PNTask AS T ON CI.PlanID = T.PlanID
                  INNER JOIN PNBaselineLabor AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate) AS X
        WHERE (PeriodHrs IS NOT NULL AND PeriodHrs != 0)
           
      -- Adjust Baseline Labor time-phased data to compensate for rounding errors.
     
      UPDATE @tabBLabTPD SET PeriodHrs = (TPD.PeriodHrs + D.DeltaHrs),
        PeriodCost = (TPD.PeriodCost + D.DeltaCost),
        PeriodBill = (TPD.PeriodBill + D.DeltaBill),
        PeriodRev = (TPD.PeriodRev + D.DeltaRev)
        FROM @tabBLabTPD AS TPD INNER JOIN 
          (SELECT YTPD.TimePhaseID AS TimePhaseID, 
            (YTPD.PeriodHrs - SUM(ISNULL(XTPD.PeriodHrs, 0))) AS DeltaHrs,
            (YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaCost,
            (YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaBill,
            (YTPD.PeriodRev - SUM(ISNULL(XTPD.PeriodRev, 0))) AS DeltaRev
            FROM @tabBLabTPD AS XTPD INNER JOIN PNBaselineLabor AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
            GROUP BY YTPD.TimePhaseID, YTPD.PeriodHrs, YTPD.PeriodCost, YTPD.PeriodBill, YTPD.PeriodRev) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
        WHERE RowID IN
          (SELECT RowID FROM @tabBLabTPD AS ATPD INNER JOIN
            (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabBLabTPD GROUP BY TimePhaseID) AS BTPD
                ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)

    END -- IF (@intBaselineLabCount > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strExpTab = 'Y' AND @intBaselineExpCount > 0)
    BEGIN
    
      -- Save Baseline Expense time-phased data rows.

      INSERT @tabBExpTPD
        (RowID,
         TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         ExpenseID,
         StartDate, 
         EndDate, 
         PeriodCost,
         PeriodBill,
         PeriodRev,
         PeriodCount, 
         PeriodScale)
      SELECT 
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
        TimePhaseID AS TimePhaseID,
        CIStartDate AS CIStartDate,
        PlanID AS PlanID, 
        TaskID AS TaskID,
        ExpenseID AS ExpenseID,
        StartDate AS StartDate, 
        EndDate AS EndDate, 
        ROUND(ISNULL(PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
        ROUND(ISNULL(PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill, 
        ROUND(ISNULL(PeriodRev, 0), @intECURevDecimals) AS PeriodRev, 
        PeriodCount AS PeriodCount, 
        PeriodScale AS PeriodScale
        FROM (SELECT 
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                T.PlanID AS PlanID, 
                T.TaskID AS TaskID,
                TPD.ExpenseID AS ExpenseID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                      ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                      ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodCost END AS PeriodCost,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                      ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodBill END AS PeriodBill,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodRev * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                     THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                              CASE WHEN TPD.EndDate < CI.EndDate 
                                                                     THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                              TPD.StartDate, TPD.EndDate,
                                                              @strCompany)
                       ELSE PeriodRev END AS PeriodRev,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN PNTask AS T ON CI.PlanID = T.PlanID
                  INNER JOIN PNBaselineExpenses AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate) AS X
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))

      -- Adjust Baseline Expense time-phased data to compensate for rounding errors.
     
      UPDATE @tabBExpTPD SET 
        PeriodCost = (TPD.PeriodCost + D.DeltaCost),
        PeriodBill = (TPD.PeriodBill + D.DeltaBill),
        PeriodRev = (TPD.PeriodRev + D.DeltaRev)
        FROM @tabBExpTPD AS TPD INNER JOIN 
          (SELECT YTPD.TimePhaseID AS TimePhaseID, 
            (YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaCost,
            (YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaBill,
            (YTPD.PeriodRev - SUM(ISNULL(XTPD.PeriodRev, 0))) AS DeltaRev
            FROM @tabBExpTPD AS XTPD INNER JOIN PNBaselineExpenses AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
            GROUP BY YTPD.TimePhaseID, YTPD.PeriodCost, YTPD.PeriodBill, YTPD.PeriodRev) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
        WHERE RowID IN
          (SELECT RowID FROM @tabBExpTPD AS ATPD INNER JOIN
            (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabBExpTPD GROUP BY TimePhaseID) AS BTPD
                ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)

    END -- IF (@strExpTab = 'Y' AND @intBaselineExpCount > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strConTab = 'Y' AND @intBaselineConCount > 0)
    BEGIN
    
      -- Save Baseline Consultant time-phased data rows.

      INSERT @tabBConTPD
        (RowID,
         TimePhaseID,
         CIStartDate,
         PlanID, 
         TaskID,
         ConsultantID,
         StartDate, 
         EndDate, 
         PeriodCost,
         PeriodBill,
         PeriodRev,
         PeriodCount, 
         PeriodScale)
      SELECT 
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
        TimePhaseID AS TimePhaseID,
        CIStartDate AS CIStartDate,
        PlanID AS PlanID, 
        TaskID AS TaskID,
        ConsultantID AS ConsultantID,
        StartDate AS StartDate, 
        EndDate AS EndDate, 
        ROUND(ISNULL(PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
        ROUND(ISNULL(PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill, 
        ROUND(ISNULL(PeriodRev, 0), @intECURevDecimals) AS PeriodRev, 
        PeriodCount AS PeriodCount, 
        PeriodScale AS PeriodScale
        FROM (SELECT 
                CI.StartDate AS CIStartDate, 
                TPD.TimePhaseID AS TimePhaseID,
                T.PlanID AS PlanID, 
                T.TaskID AS TaskID,
                TPD.ConsultantID AS ConsultantID, 
                CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
                CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodCost * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                      ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                      ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodCost END AS PeriodCost,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodBill * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                      THEN TPD.StartDate 
                                                                      ELSE CI.StartDate END, 
                                                               CASE WHEN TPD.EndDate < CI.EndDate 
                                                                      THEN TPD.EndDate 
                                                                      ELSE CI.EndDate END, 
                                                               TPD.StartDate, TPD.EndDate,
                                                               @strCompany)
                       ELSE PeriodBill END AS PeriodBill,
                CASE WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
                       THEN PeriodRev * dbo.DLTK$ProrateRatio(CASE WHEN TPD.StartDate > CI.StartDate 
                                                                     THEN TPD.StartDate 
                                                                     ELSE CI.StartDate END, 
                                                              CASE WHEN TPD.EndDate < CI.EndDate 
                                                                     THEN TPD.EndDate 
                                                                     ELSE CI.EndDate END, 
                                                              TPD.StartDate, TPD.EndDate,
                                                              @strCompany)
                       ELSE PeriodRev END AS PeriodRev,
                1 AS PeriodCount, 
                CI.PeriodScale AS PeriodScale
                FROM @tabCalendarInterval AS CI LEFT JOIN PNTask AS T ON CI.PlanID = T.PlanID
                  INNER JOIN PNBaselineConsultant AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                    AND TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate) AS X
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))

      -- Adjust Baseline Consultant time-phased data to compensate for rounding errors.
     
      UPDATE @tabBConTPD SET 
        PeriodCost = (TPD.PeriodCost + D.DeltaCost),
        PeriodBill = (TPD.PeriodBill + D.DeltaBill),
        PeriodRev = (TPD.PeriodRev + D.DeltaRev)
        FROM @tabBConTPD AS TPD INNER JOIN 
          (SELECT YTPD.TimePhaseID AS TimePhaseID, 
            (YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaCost,
            (YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaBill,
            (YTPD.PeriodRev - SUM(ISNULL(XTPD.PeriodRev, 0))) AS DeltaRev
            FROM @tabBConTPD AS XTPD INNER JOIN PNBaselineConsultant AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
            GROUP BY YTPD.TimePhaseID, YTPD.PeriodCost, YTPD.PeriodBill, YTPD.PeriodRev) AS D
          ON TPD.TimePhaseID = D.TimePhaseID
        WHERE RowID IN
          (SELECT RowID FROM @tabBConTPD AS ATPD INNER JOIN
            (SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabBConTPD GROUP BY TimePhaseID) AS BTPD
                ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate)
                
    END -- IF (@strConTab = 'Y' AND @intBaselineConCount > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Delete all time-phased data.
  
  IF (@intBaselineLabCount > 0) DELETE PNBaselineLabor WHERE PlanID = @strPlanID
  IF (@strExpTab = 'Y' AND @intBaselineExpCount > 0) DELETE PNBaselineExpenses WHERE PlanID = @strPlanID
  IF (@strConTab = 'Y' AND @intBaselineConCount > 0) DELETE PNBaselineConsultant WHERE PlanID = @strPlanID

  IF (@intPlannedLabCount > 0) DELETE PNPlannedLabor WHERE PlanID = @strPlanID
  IF (@strExpTab = 'Y' AND @intPlannedExpCount > 0) DELETE PNPlannedExpenses WHERE PlanID = @strPlanID
  IF (@strConTab = 'Y' AND @intPlannedConCount > 0) DELETE PNPlannedConsultant WHERE PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Put all time-phased data from temp tables back.
  
  IF (@intPlannedLabCount > 0)
    INSERT PNPlannedLabor
          (TimePhaseID, 
           PlanID, 
           TaskID,
           AssignmentID,
           StartDate, 
           EndDate, 
           PeriodHrs, 
           CostRate,
           BillingRate,
           PeriodCount, 
           PeriodScale,
           CreateDate,
           ModDate)
      SELECT
        X.TimePhaseID, 
        X.PlanID, 
        X.TaskID,
        X.AssignmentID,
        X.StartDate, 
        X.EndDate, 
        X.PeriodHrs, 
        dbo.RP$LabRate
          (A.ResourceID,
           ISNULL(A.Category, 0),
           A.GRLBCD,
           ISNULL(A.LaborCode, T.LaborCode),
           @sintCostRtMethod,
           @intCostRtTableNo,
           @sintGRMethod,
           @intGenResTableNo,
           X.StartDate,
           X.EndDate,
           @intRtCostDecimals,
           ISNULL(EM.ProvCostRate, 0),
           ISNULL(A.CostRate, T.CostRate)) AS CostRate,
        dbo.RP$LabRate
          (A.ResourceID,
           ISNULL(A.Category, 0),
           A.GRLBCD,
           ISNULL(A.LaborCode, T.LaborCode),
           @sintBillingRtMethod,
           @intBillingRtTableNo,
           @sintGRMethod,
           @intGRBillTableNo,
           X.StartDate,
           X.EndDate,
           @intRtBillDecimals,
           ISNULL(EM.ProvBillRate, 0),
           ISNULL(A.BillingRate, T.BillingRate)) AS BillingRate,                 
        X.PeriodCount, 
        X.PeriodScale,
        LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
        LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM
          (SELECT -- Need to group together the period with same Start Date first
             REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
             PlanID, 
             TaskID,
             AssignmentID,
             MIN(StartDate) AS StartDate, 
             MAX(EndDate) AS EndDate, 
             SUM(ISNULL(PeriodHrs, 0)) AS PeriodHrs,
             PeriodCount, 
             PeriodScale
             FROM @tabPLabTPD
             GROUP BY AssignmentID, CIStartDate, TaskID, PlanID, PeriodCount, PeriodScale) AS X
          LEFT JOIN PNAssignment AS A ON X.PlanID = A.PlanID AND X.TaskID = A.TaskID AND X.AssignmentID = A.AssignmentID
          LEFT JOIN EM ON A.ResourceID = EM.Employee
          INNER JOIN PNTask AS T ON X.PlanID = T.PlanID AND X.TaskID = T.TaskID
        WHERE (PeriodHrs IS NOT NULL AND PeriodHrs != 0)
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strExpTab = 'Y' AND @intPlannedExpCount > 0)
    INSERT PNPlannedExpenses
          (TimePhaseID, 
           PlanID, 
           TaskID,
           ExpenseID,
           StartDate, 
           EndDate, 
           PeriodCost, 
           PeriodBill, 
           PeriodCount, 
           PeriodScale,
           CreateDate,
           ModDate)
      SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
             PlanID, 
             TaskID,
             ExpenseID,
             MIN(StartDate) AS StartDate, 
             MAX(EndDate) AS EndDate, 
             SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
             SUM(ISNULL(PeriodBill, 0)) AS PeriodBill,
             PeriodCount, 
             PeriodScale,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM @tabPExpTPD
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
        GROUP BY ExpenseID, CIStartDate, TaskID, PlanID, PeriodCount, PeriodScale

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strConTab = 'Y' AND @intPlannedConCount > 0)
    INSERT PNPlannedConsultant
          (TimePhaseID, 
           PlanID, 
           TaskID,
           ConsultantID,
           StartDate, 
           EndDate, 
           PeriodCost, 
           PeriodBill, 
           PeriodCount, 
           PeriodScale,
           CreateDate,
           ModDate)
      SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
             PlanID, 
             TaskID,
             ConsultantID,
             MIN(StartDate) AS StartDate, 
             MAX(EndDate) AS EndDate, 
             SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
             SUM(ISNULL(PeriodBill, 0)) AS PeriodBill,
             PeriodCount, 
             PeriodScale,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM @tabPConTPD
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
        GROUP BY ConsultantID, CIStartDate, TaskID, PlanID, PeriodCount, PeriodScale

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -->>> Baseline.
  
  IF (@intBaselineLabCount > 0)
    INSERT PNBaselineLabor
          (TimePhaseID, 
           PlanID, 
           TaskID,
           AssignmentID,
           StartDate, 
           EndDate, 
           PeriodHrs, 
           PeriodCost, 
           PeriodBill, 
           PeriodRev, 
           PeriodCount, 
           PeriodScale,
           CreateDate,
           ModDate)
      SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
             PlanID, 
             TaskID,
             AssignmentID,
             MIN(StartDate) AS StartDate, 
             MAX(EndDate) AS EndDate, 
             SUM(ISNULL(PeriodHrs, 0)) AS PeriodHrs,
             SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
             SUM(ISNULL(PeriodBill, 0)) AS PeriodBill,
             SUM(ISNULL(PeriodRev, 0)) AS PeriodRev,
             PeriodCount, 
             PeriodScale,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM @tabBLabTPD
        WHERE (PeriodHrs IS NOT NULL AND PeriodHrs != 0)
        GROUP BY AssignmentID, CIStartDate, TaskID, PlanID, PeriodCount, PeriodScale
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strExpTab = 'Y' AND @intBaselineExpCount > 0)
    INSERT PNBaselineExpenses
          (TimePhaseID, 
           PlanID, 
           TaskID,
           ExpenseID,
           StartDate, 
           EndDate, 
           PeriodCost, 
           PeriodBill, 
           PeriodRev, 
           PeriodCount, 
           PeriodScale,
           CreateDate,
           ModDate)
      SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
             PlanID, 
             TaskID,
             ExpenseID,
             MIN(StartDate) AS StartDate, 
             MAX(EndDate) AS EndDate, 
             SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
             SUM(ISNULL(PeriodBill, 0)) AS PeriodBill,
             SUM(ISNULL(PeriodRev, 0)) AS PeriodRev,
             PeriodCount, 
             PeriodScale,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM @tabBExpTPD
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
        GROUP BY ExpenseID, CIStartDate, TaskID, PlanID, PeriodCount, PeriodScale
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strConTab = 'Y' AND @intBaselineConCount > 0)
    INSERT PNBaselineConsultant
          (TimePhaseID, 
           PlanID, 
           TaskID,
           ConsultantID,
           StartDate, 
           EndDate, 
           PeriodCost, 
           PeriodBill, 
           PeriodRev, 
           PeriodCount, 
           PeriodScale,
           CreateDate,
           ModDate)
      SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
             PlanID, 
             TaskID,
             ConsultantID,
             MIN(StartDate) AS StartDate, 
             MAX(EndDate) AS EndDate, 
             SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,
             SUM(ISNULL(PeriodBill, 0)) AS PeriodBill,
             SUM(ISNULL(PeriodRev, 0)) AS PeriodRev,
             PeriodCount, 
             PeriodScale,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
             LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM @tabBConTPD
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
        GROUP BY ConsultantID, CIStartDate, TaskID, PlanID, PeriodCount, PeriodScale
     
  SET NOCOUNT OFF

END -- PNAlignTPD
GO
