SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_PctCmplClearEIChanges] ( @WBS1 nvarchar(32), @WBS1List nvarchar(max))
             AS EXEC spCCG_EI_PctCmplClearEIChanges @WBS1,@WBS1List
GO
