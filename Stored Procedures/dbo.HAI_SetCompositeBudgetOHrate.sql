SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[HAI_SetCompositeBudgetOHrate]
AS
/********************************************************************************
	Copyright (c) 2020 Halff Associates, Inc.  All rights reserved.

	10/01/2020	Craig H. Anderson
				Updates BudOHRate in PR table based on a 2-period rolling window
	08/17/2021	Craig H. Anderson
				Removed references to HalffImport
*********************************************************************************/
SET NOCOUNT ON;
    BEGIN
        DECLARE @OfficeRate DECIMAL(19, 4) = 187.5;
        DECLARE @FieldRate DECIMAL(19, 4) = 125.00;

        -- Current Period based on today's date
        DECLARE @period INT = DATEPART(YEAR, GETDATE()) * 100 + DATEPART(MONTH, GETDATE());

        -- Past period = max period < current period
        DECLARE @priorPeriod INT = --201901;
                    (
                        SELECT TOP (1)
                               Period
                        FROM dbo.PRSummaryMain WITH (NOLOCK)
                        WHERE Period < @period
                        ORDER BY Period DESC
                    );

        --
        IF OBJECT_ID('tempdb..#labor') IS NOT NULL DROP TABLE #labor;
        --
        SELECT l1.WBS1, l1.Period, SUM(l1.FieldSpent) AS FieldSpent, SUM(l1.OfficeSpent) AS OfficeSpent
        INTO #labor
        FROM
            (
                SELECT l.WBS1
                     , l.Period
                     , IIF(t.TeamPracticeArea = 'Construction Services', ISNULL(l.BillExt, 0), 0.0) AS FieldSpent
                     , IIF(t.TeamPracticeArea IS NULL OR t.TeamPracticeArea <> 'Construction Services'
                           , ISNULL(l.BillExt, 0)
                           , 0.0)                                                                   AS OfficeSpent
                FROM dbo.LD            AS l WITH (NOLOCK)
                LEFT OUTER JOIN DataMart.dbo.Teams AS t WITH (NOLOCK)
                    ON RIGHT(l.EmOrg, 3) = t.TeamNum
                       AND t.PrimaryRecord = 'Y'
                INNER JOIN DataMart.dbo.ProjectJTD AS p WITH (NOLOCK)
                    ON l.WBS1 = p.WBS1
                       AND p.ChargeType = 'R'
                       AND p.ProjectStatus = 'A'
            ) AS l1
        GROUP BY l1.WBS1, l1.Period;
        --
        --
        -- "TP" indiicates Two-Period
        IF OBJECT_ID('tempdb..#CompositeRates') IS NOT NULL
            DROP TABLE #CompositeRates;
        SELECT a.WBS1
             , SUM(a.TPFieldSpent)                                                         AS TPFieldSpent
             , SUM(a.TPOfficeSpent)                                                        AS TPOfficeSpent
             , SUM(a.TPFieldPctLabor)                                                      AS TPFieldPctLabor
             , SUM(a.TPOfficePctLabor)                                                     AS TPOfficePctLabor
             -- a.	Office Percentage of Labor x Office OH rate + Field Percentage of Labor x Field OH rate = Composite OH Rate

             , SUM(a.TPOfficePctLabor) * @OfficeRate + SUM(a.TPFieldPctLabor) * @FieldRate AS TPCompOHrate
        INTO #CompositeRates
        FROM
            (
                /*
			SELECT labor.WBS1
                 , SUM(labor.FieldSpent)  AS JTDFieldSpent
                 , SUM(labor.OfficeSpent) AS JTDOfficeSpent
                 , CASE
                       WHEN SUM(labor.FieldSpent) > 0
                            AND SUM(labor.OfficeSpent) > 0
                           THEN
                           SUM(labor.FieldSpent) / (SUM(labor.OfficeSpent) + SUM(labor.FieldSpent))
                       WHEN SUM(labor.FieldSpent) > 0
                            AND SUM(labor.OfficeSpent) = 0
                           THEN
                           1.0
                       WHEN SUM(labor.FieldSpent) = 0
                            AND SUM(labor.OfficeSpent) > 0
                           THEN
                           0.0
                       ELSE
                           0.0
                   END                    AS JTDFieldPctLabor
                 , CASE
                       WHEN SUM(labor.FieldSpent) > 0
                            AND SUM(labor.OfficeSpent) > 0
                           THEN
                           SUM(labor.OfficeSpent) / (SUM(labor.OfficeSpent) + SUM(labor.FieldSpent))
                       WHEN SUM(labor.FieldSpent) > 0
                            AND SUM(labor.OfficeSpent) = 0
                           THEN
                           0.0
                       WHEN SUM(labor.FieldSpent) = 0
                            AND SUM(labor.OfficeSpent) > 0
                           THEN
                           1.0
                       ELSE
                           0.0
                   END                    AS JTDOfficePctLabor
                 , 0.0                    AS TPFieldSpent
                 , 0.0                    AS TPOfficeSpent
                 , 0.0                    AS TPFieldPctLabor
                 , 0.0                    AS TPOfficePctLabor
            FROM #labor AS labor WITH (NOLOCK)
            WHERE labor.Period <= @period
            GROUP BY labor.WBS1
            UNION
			*/
                SELECT labor.WBS1
                     , 0.0                    AS JTDFieldSpent
                     , 0.0                    AS JTDOfficeSpent
                     , 0.0                    AS JTDFieldPctLabor
                     , 0.0                    AS JTDOfficePctLabor
                     , SUM(labor.FieldSpent)  AS TPFieldSpent
                     , SUM(labor.OfficeSpent) AS TPOfficeSpent
                     , CASE
                           WHEN SUM(labor.FieldSpent) > 0
                                AND SUM(labor.OfficeSpent) > 0
                               THEN
                               SUM(labor.FieldSpent) / (SUM(labor.OfficeSpent) + SUM(labor.FieldSpent))
                           WHEN SUM(labor.FieldSpent) > 0
                                AND SUM(labor.OfficeSpent) = 0
                               THEN
                               1.0
                           WHEN SUM(labor.FieldSpent) = 0
                                AND SUM(labor.OfficeSpent) > 0
                               THEN
                               0.0
                           ELSE
                               0.0
                       END                    AS TPFieldPctLabor
                     , CASE
                           WHEN SUM(labor.FieldSpent) > 0
                                AND SUM(labor.OfficeSpent) > 0
                               THEN
                               SUM(labor.OfficeSpent) / (SUM(labor.OfficeSpent) + SUM(labor.FieldSpent))
                           WHEN SUM(labor.FieldSpent) > 0
                                AND SUM(labor.OfficeSpent) = 0
                               THEN
                               0.0
                           WHEN SUM(labor.FieldSpent) = 0
                                AND SUM(labor.OfficeSpent) > 0
                               THEN
                               1.0
                           ELSE
                               0.0
                       END                    AS TPOfficePctLabor
                FROM #labor AS labor WITH (NOLOCK)
                WHERE
                    labor.Period BETWEEN @priorPeriod AND @period
                GROUP BY labor.WBS1
            ) AS a
        GROUP BY a.WBS1
        ORDER BY a.WBS1;
        --
        BEGIN TRANSACTION;
        BEGIN
            UPDATE pr
            SET PR.BudOHRate = ROUND(c.TPCompOHrate, 3)
            FROM dbo.PR                           AS pr
            INNER JOIN dbo.ProjectCustomTabFields AS f WITH (NOLOCK)
                ON pr.WBS1 = f.WBS1
                   AND f.WBS2 = ' '
            INNER JOIN #CompositeRates            AS c WITH (NOLOCK)
                ON pr.WBS1 = c.WBS1
            INNER JOIN dbo.PR                     AS p WITH (NOLOCK)
                ON pr.WBS1 = p.WBS1
                   AND p.WBS2 = ' '
            WHERE
                p.Status = 'A'
                AND p.ChargeType = 'R'
                AND f.CustManualOHRateOverride = 'N';
        END;
        COMMIT TRANSACTION;
    END;
GO
