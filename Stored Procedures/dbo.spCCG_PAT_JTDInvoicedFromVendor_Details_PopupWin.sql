SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_PAT_JTDInvoicedFromVendor_Details_PopupWin] (
	@PayableSeq int
	,@WBS1 /*N*/varchar(30) = null
	,@WBS2 /*N*/varchar(7) = null
	,@WBS3 /*N*/varchar(7) = null
	,@Username /*N*/varchar(30)
	)
AS
BEGIN
	/*
		Copyright (c) 2021 EleVia Software. All rights reserved.
	*/
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	select
		@WBS1 = nullif(@WBS1, /*N*/'')
		,@WBS2 = nullif(@WBS2, /*N*/'')
		,@WBS3 = nullif(@WBS3, /*N*/'')
		
    -- OUTPUT TABLE 1 (Column Formatting)
    select
		'A;align=right' -- Vendor
		,'D;align=right' -- Invoice Date
		,'A' -- Invoice
		,'A' -- Voucher
		,'C2' -- Amount

	-- OUTPUT TABLE 2 (Main Table)
	select
		S.VendorName as Vendor
		,S.TransDate as [Date]
		,S.InvoiceNumber as [Inv Number]
		,S.Voucher
		,sum(S.Amount) as Amount
		,'BackColor=white' as Row_Detail
	from (
		--Records from PAT not yet in Vision:
		select VE.Name as VendorName, PayableDate as TransDate, pat.PayableNumber as InvoiceNumber, pat.Voucher as Voucher, IsNull(pa.NetAmount,0) as Amount
            from CCG_PAT_Payable pat
            inner join CCG_PAT_ProjectAmount pa on pat.Seq = pa.PayableSeq
            inner join CCG_PAT_ConfigStages s on s.Stage=pat.Stage
            inner join VE on VE.Vendor=pat.Vendor
            where (pat.Seq = @PayableSeq or pat.ParentSeq = @PayableSeq)--match on single invoice or all invoices for a contract
            and (@WBS1 IS NULL or (pa.WBS1=@WBS1 and pa.WBS2=@WBS2 and pa.WBS3=@WBS3 ))
			and PayableType = 'I' and ISNULL(pat.Voucher,'') = '' and s.SubType<>'NoExport'
		UNION ALL
		-- Unposted records in Vision
		select VE.Name as VendorName, ap.TransDate, ap.Invoice as InvoiceNumber, ap.Voucher as Voucher,  (apd.NetAmount) as Amount
		from CCG_PAT_Payable pat
		inner join (select distinct PayableSeq,WBS1,WBS2,WBS3 from CCG_PAT_ProjectAmount ) pa on pat.Seq = pa.PayableSeq 
		inner join apMaster ap on pat.Vendor=ap.Vendor 
		inner join apDetail apd on apd.Batch=ap.Batch and apd.MasterPKey=ap.MasterPKey and apd.WBS1=pa.WBS1 and apd.WBS2=pa.WBS2 and apd.WBS3=pa.WBS3
		left join VE on VE.Vendor=ap.Vendor
		where (pat.Seq = @PayableSeq /*or pat.ParentSeq = @PayableSeq*/)	-- Match on single invoice or all invoices for a contract
		  and (@WBS1 IS NULL or (pa.WBS1=@WBS1 and pa.WBS2=@WBS2 and pa.WBS3=@WBS3 ))
		  and ap.Posted='N'
		UNION ALL
		--Records from Vision
		select VE.Name as VendorName, ap.TransDate, VO.Invoice as InvoiceNumber, ap.Voucher, ap.Amount
		from CCG_PAT_Payable pat
		left join (select distinct PayableSeq,WBS1,WBS2,WBS3,GLAccount from CCG_PAT_ProjectAmount  GROUP BY PayableSeq,WBS1,WBS2,WBS3,GLAccount) pa on pat.Seq = pa.PayableSeq
		left join LedgerAP ap on pat.Vendor=ap.Vendor and pa.WBS1=ap.WBS1 and pa.WBS2=ap.WBS2  and pa.WBS3=ap.WBS3 and pa.GLAccount=ap.Account
		left join VE on VE.Vendor=ap.Vendor
		left join VO on VO.Vendor=ap.Vendor and VO.Voucher=ap.Voucher
		where (pat.Seq = @PayableSeq /*or pat.ParentSeq = @PayableSeq*/)	-- Match on single invoice or all invoices for a contract
		  and (@WBS1 IS NULL or (ap.WBS1=@WBS1 and (ap.WBS2=@WBS2 or @WBS2 = ' ') and (ap.WBS3=@WBS3 or @WBS3 = ' ')))
		  and TransType = 'AP'  and PostSeq  <> '0'
		) as S
	group by
		VendorName
		,TransDate
		,InvoiceNumber
		,Voucher
	having sum(S.Amount) <> 0.00
	order by 1,2,3,4
	
	-- NOTE: total row not needed because in-app popup automatically adds a summary row.

	-- OUTPUT TABLE 3 (Advanced Options)
	select 'width=600
		height=600' as AdvancedOptions

END
GO
