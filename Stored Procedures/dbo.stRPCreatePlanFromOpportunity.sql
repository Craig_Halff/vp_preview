SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPCreatePlanFromOpportunity]
  @strOpportunityID varchar(32),
  @strUserName nvarchar(32),
  @strPlanID varchar(32) OUTPUT
AS

BEGIN -- Procedure stRPCreatePlanFromOpportunity

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- The purpose of this stored procedure is to create and publish 1.0 Plans from a given Opportunity.
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

  DECLARE @intPlanCount int

  -- If the given Opportunity already have an associated Opportunity Plan, then exit this stored procedure.

  SET @intPlanCount = 
    CASE WHEN EXISTS (
      SELECT P.PlanID FROM PNPlan AS P WHERE P.OpportunityID = @strOpportunityID AND P.WBS1 IS NULL
      UNION
      SELECT P.PlanID FROM RPPlan AS P WHERE P.OpportunityID = @strOpportunityID AND P.WBS1 IS NULL
    ) 
      THEN 1 ELSE 0 END

  IF (@intPlanCount > 0) RETURN

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  DECLARE @strCompany nvarchar(14)
  DECLARE @strDefaultCompany nvarchar(14)
  DECLARE @strDefaultFunctionalCurrencyCode nvarchar(3)
  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)
  DECLARE @strAccordionFormatID varchar(32)
  DECLARE @strEMCostCurrencyCode nvarchar(3)
  DECLARE @strEMBillCurrencyCode nvarchar(3)
  DECLARE @strGRCostCurrencyCode nvarchar(3)
  DECLARE @strGRBillCurrencyCode nvarchar(3)
  DECLARE @strCalcExpBillAmtFlg varchar(1)
  DECLARE @strCalcConBillAmtFlg varchar(1)
  DECLARE @strExpTab varchar(1)
  DECLARE @strConTab varchar(1)
  DECLARE @strBudgetType varchar(1)
  DECLARE @strReimbMethod varchar(1)
  DECLARE @strCreateModUser nvarchar(42)
  DECLARE @strErrorMsg nvarchar(max)
  DECLARE @strOppCurrencyCode nvarchar(3)
  DECLARE @strBillingCurrencyCode nvarchar(3)

  DECLARE @tiDefExpWBSLevel tinyint
  DECLARE @tiDefConWBSLevel tinyint

  DECLARE @siWBS2Length smallint
  DECLARE @siWBS3Length smallint
  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint
  DECLARE @siLCLevels smallint
  DECLARE @siLCFmtLevel smallint

  DECLARE @siGRMethod smallint
  DECLARE @siCostGRRtMethod smallint
  DECLARE @siBillGRRtMethod smallint
  DECLARE @siCostRtMethod smallint
  DECLARE @siBillRtMethod smallint
  DECLARE @siLabMultType smallint

  DECLARE @intCostRtTableNo int
  DECLARE @intBillRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int
  DECLARE @intExpBillRtMethod int 
  DECLARE @intExpBillRtTableNo int 
  DECLARE @intConBillRtMethod int 
  DECLARE @intConBillRtTableNo int 

  DECLARE @dtMinDate datetime
  DECLARE @dtMaxDate datetime
  DECLARE @dtToday datetime 
  DECLARE @dtTomorrow datetime

  DECLARE @decLabBillMultiplier decimal(19, 4)
  DECLARE @decOverheadPct decimal(19, 4)
  DECLARE @decExpBillMultiplier decimal(19, 4)
  DECLARE @decConBillMultiplier decimal(19, 4)
  DECLARE @decTargetMultCost decimal(19, 4)

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

  -- Get the current date in current local and not the UTC date.
  
  SET @dtToday = DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)
  SET @dtTomorrow = DATEADD(dd, 1, @dtToday)  
  
  -- Get Org Format

  SELECT
    @siWBS2Length = WBS2Length,
    @siWBS3Length = WBS3Length,
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length,
    @siLCLevels = LCLevels
    FROM CFGFormat

  -- Get Default Company from CFGRMSettings.

  SELECT
    @strDefaultCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN DefaultCompany ELSE ' ' END
    FROM CFGRMSettings

  -- Get Default Functional Currency Code for Default Company.

  SELECT
    @strDefaultFunctionalCurrencyCode =
      CASE
        WHEN @strMultiCurrencyEnabled = 'Y'
        THEN ISNULL(MIN(FunctionalCurrencyCode), ' ') -- Used MIN so that there always have a result row.
        ELSE ' '
      END
    FROM CFGMainData WHERE Company = @strDefaultCompany

  -- Get Opportunity data.

  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN COALESCE(CM.Company, @strDefaultCompany) ELSE ' ' END,
    @dtMinDate = COALESCE(EstStartDate, EstCompletionDate, @dtTomorrow),
    @dtMaxDate = CASE WHEN (EstCompletionDate IS NOT NULL AND EstCompletionDate >= @dtMinDate) THEN EstCompletionDate ELSE @dtMinDate END,
    @strOppCurrencyCode = COALESCE(OppCurrencyCode, @strDefaultFunctionalCurrencyCode),
    @strBillingCurrencyCode = 
      CASE 
        WHEN @strReportAtBillingInBillingCurr = 'Y' 
        THEN COALESCE(BillingCurrencyCode, @strDefaultFunctionalCurrencyCode)
        ELSE COALESCE(OppCurrencyCode, @strDefaultFunctionalCurrencyCode) 
      END
    FROM Opportunity AS O 
      LEFT JOIN (
        SELECT DISTINCT Company FROM CFGMainData
      ) AS CM
      ON SUBSTRING(O.Org, @siOrg1Start, @siOrg1Length) = CM.Company
    WHERE O.OpportunityID = @strOpportunityID

  -- Get Default Plan Settings from CFGResourcePlanning

  SELECT 
    @strBudgetType = BudgetType,
    @siLabMultType =
      CASE BudgetType
        WHEN 'C' THEN 1 /* Planned Multiplier */
        WHEN 'B' THEN 3 /* Planned Ratio */
        WHEN 'A' THEN
          CASE 
            WHEN LabMultType IN (0, 1) THEN 1 /* Planned Multiplier */
            WHEN LabMultType IN (2, 3) THEN 3 /* Planned Ratio */
          END
      END,
    @strReimbMethod = 
      CASE BudgetType
        WHEN 'C' THEN 'C' /* Cost */
        WHEN 'B' THEN 'B' /* Bill */
        WHEN 'A' THEN ReimbMethod
      END,    
    @siGRMethod = GRMethod,
    @siCostGRRtMethod = CostGRRtMethod,
    @siBillGRRtMethod = BillGRRtMethod,
    @intGenResTableNo = GenResTableNo,
    @intGRBillTableNo = GRBillTableNo,
    @siCostRtMethod = CostRtMethod,
    @siBillRtMethod = BillingRtMethod,
    @intCostRtTableNo = CostRtTableNo,
    @intBillRtTableNo = BillingRtTableNo,
    @strCalcExpBillAmtFlg = 'Y',
    @intExpBillRtMethod = ExpBillRtMethod, 
    @intExpBillRtTableNo = ExpBillRtTableNo,
    @decExpBillMultiplier = ExpBillMultiplier,
    @strCalcConBillAmtFlg = 'Y',
    @intConBillRtMethod = ConBillRtMethod, 
    @intConBillRtTableNo = ConBillRtTableNo, 
    @decConBillMultiplier = ConBillMultiplier,
    @decLabBillMultiplier = LabBillMultiplier,
    @decOverheadPct = OverheadPct,
    @decTargetMultCost = TargetMultCost,
    @strExpTab = ExpTab,
    @strConTab = ConTab,
    @tiDefExpWBSLevel = ExpWBSLevel,
    @tiDefConWBSLevel = ConWBSLevel
    FROM CFGResourcePlanning WHERE Company = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN @strCompany ELSE ' ' END

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Determining the Currency Code of Rate Tables based on Rate Methods

  SELECT @strEMCostCurrencyCode = 
    CASE 
      WHEN @siCostRtMethod = 2 THEN (SELECT CurrencyCode FROM BTRRT WHERE TableNo = @intCostRtTableNo)
      WHEN @siCostRtMethod = 3 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intCostRtTableNo)
      WHEN @siCostRtMethod = 4 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intCostRtTableNo)
    END

  SELECT @strEMBillCurrencyCode = 
    CASE 
      WHEN @siBillRtMethod = 2 THEN (SELECT CurrencyCode FROM BTRRT WHERE TableNo = @intBillRtTableNo)
      WHEN @siBillRtMethod = 3 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intBillRtTableNo)
      WHEN @siBillRtMethod = 4 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intBillRtTableNo)
    END

  SELECT @strGRCostCurrencyCode = 
    CASE 
      WHEN @siCostGRRtMethod = 0 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intGenResTableNo)
      WHEN @siCostGRRtMethod = 1 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intGenResTableNo)
    END

  SELECT @strGRBillCurrencyCode = 
    CASE 
      WHEN @siBillGRRtMethod = 0 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intGRBillTableNo)
      WHEN @siBillGRRtMethod = 1 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intGRBillTableNo)
    END

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Set @strCreateModUser

  SELECT
    @strCreateModUser = CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'stRPPlanFromOpp' ELSE N'PFO_' + @strUserName END

  BEGIN TRANSACTION

    -- Insert PNPlan record.

    SET @strPlanID = REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')
  
    INSERT PNPlan (
      PlanID,
      PlanName,
      PlanNumber,
      ProjMgr,
      Principal,
      Supervisor,
      OpportunityID,
      ClientID,
      Org,
      WBS1,
      StartDate,
      EndDate,

      Probability,

      PctCompleteFormula,
      LabMultType,
      ReimbMethod,
      TargetMultCost,
      AvailableFlg,
      UnPostedFlg,
      CommitmentFlg,
      UtilizationIncludeFlg,

      Status,
      Company,
      CostCurrencyCode,
      BillingCurrencyCode,

      BudgetType,
      CostRtMethod,
      BillingRtMethod,
      CostRtTableNo,
      BillingRtTableNo,
      LabBillMultiplier,
      GRMethod,
      CostGRRtMethod,
      BillGRRtMethod,
      GenResTableNo,
      GRBillTableNo,

      CalcExpBillAmtFlg,
      ExpBillRtMethod, 
      ExpBillRtTableNo,
      ExpBillMultiplier,

      CalcConBillAmtFlg,
      ConBillRtMethod, 
      ConBillRtTableNo,
      ConBillMultiplier,

      OverheadPct,
      ExpWBSLevel,
      ConWBSLevel,

      LastPlanAction,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        @strPlanID AS PlanID,
        ISNULL(O.Name, ' ') AS PlanName,
        O.Opportunity AS PlanNumber,
        O.ProjMgr AS ProjMgr,
        O.Principal AS Principal,
        O.Supervisor AS Supervisor,
        O.OpportunityID AS OpportunityID,
        O.ClientID AS ClientID,
        O.Org AS Org,
        NULL AS WBS1,
        @dtMinDate AS StartDate,
        @dtMaxDate AS EndDate,

        ISNULL(O.Probability, 0) AS Probability,

        2 AS PctCompleteFormula, -- JTD / (JTD + ETC)
        @siLabMultType AS LabMultType, 
        @strReimbMethod AS ReimbMethod,
        @decTargetMultCost AS TargetMultCost,
        'Y' AS AvailableFlg,
        'Y' AS UnPostedFlg,
        'Y' AS CommitmentFlg,
        'N' AS UtilizationIncludeFlg,

        ISNULL(O.Status, 'A') AS Status, -- Active if Opportunity.Status is NULL
        @strCompany Company,
        @strOppCurrencyCode AS CostCurrencyCode,
        @strBillingCurrencyCode AS BillingCurrencyCode,

        @strBudgetType AS BudgetType, -- From CFGResourcePlanning
 
        CASE WHEN @siCostRtMethod = 4 THEN 2 ELSE @siCostRtMethod END AS CostRtMethod,
        CASE WHEN @siBillRtMethod = 4 THEN 2 ELSE @siBillRtMethod END AS BillingRtMethod,

        CASE WHEN @siCostRtMethod = 4 THEN 0
        ELSE
          CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN @intCostRtTableNo
          ELSE
            CASE WHEN @strEMCostCurrencyCode = @strOppCurrencyCode THEN @intCostRtTableNo ELSE 0 END
          END
        END AS CostRtTableNo,

        CASE WHEN @siBillRtMethod = 4 THEN 0
        ELSE
          CASE 
            WHEN @strMultiCurrencyEnabled <> 'Y' 
            THEN @intBillRtTableNo
            ELSE
              CASE
                WHEN @strEMBillCurrencyCode = @strBillingCurrencyCode
                THEN @intBillRtTableNo
                ELSE 0
              END
          END
        END AS BillingRtTableNo,
 
        @decLabBillMultiplier AS LabBillMultiplier,
 
        @siGRMethod AS GRMethod,
        @siCostGRRtMethod AS CostGRRtMethod,
        @siBillGRRtMethod AS BillGRRtMethod,

        CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN @intGenResTableNo
        ELSE
          CASE WHEN @strGRCostCurrencyCode = @strOppCurrencyCode THEN @intGenResTableNo ELSE 0 END 
        END AS GenResTableNo,

        CASE 
          WHEN @strMultiCurrencyEnabled <> 'Y' 
          THEN @intGRBillTableNo
          ELSE
            CASE 
              WHEN @strGRBillCurrencyCode = @strBillingCurrencyCode
              THEN @intGRBillTableNo
              ELSE 0
            END 
        END AS GRBillTableNo,

        @strCalcExpBillAmtFlg AS CalcExpBillAmtFlg,
        @intExpBillRtMethod AS ExpBillRtMethod, 
        @intExpBillRtTableNo AS ExpBillRtTableNo,
        @decExpBillMultiplier AS ExpBillMultiplier,
 
        @strCalcConBillAmtFlg AS CalcConBillAmtFlg,
        @intConBillRtMethod AS ConBillRtMethod, 
        @intConBillRtTableNo AS ConBillRtTableNo,
        @decConBillMultiplier AS ConBillMultiplier,

        @decOverheadPct AS OverheadPct,
        @tiDefExpWBSLevel AS ExpWBSLevel,
        @tiDefConWBSLevel AS ConWBSLevel,

        'CREATED' AS LastPlanAction,
        @strCreateModUser AS CreateUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
        @strCreateModUser AS ModUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate

        FROM Opportunity AS O
        WHERE O.OpportunityID = @strOpportunityID AND
          (NOT EXISTS 
             (SELECT P.PlanID FROM PNPlan AS P WHERE P.OpportunityID = @strOpportunityID
              UNION
              SELECT P.PlanID FROM RPPlan AS P WHERE P.OpportunityID = @strOpportunityID
             )
          )

   -- If the given Opportunity already have an associated Plan, then exit this stored procedure.
 
    IF (@@ROWCOUNT = 0)
      BEGIN
        SET @strPlanID = NULL
        ROLLBACK
        RETURN
      END -- END IF (@@ROWCOUNT > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    -- Insert WBS1 PNTask record.

    INSERT PNTask (
      TaskID,
      PlanID,
      Name,
      WBS1,
      WBS2,
      WBS3,
      WBSType,
      ParentOutlineNumber,
      OutlineNumber,
      ChildrenCount,
      OutlineLevel,

      StartDate,
      EndDate,

      ProjMgr,
      ChargeType,
      ProjectType,
      ClientID,
      Status,
      Org,
      Notes,

      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TaskID,
        @strPlanID AS PlanID,
        ISNULL(O.Name, ' ') AS Name,
        '<none>' AS WBS1,
        NULL AS WBS2,
        NULL AS WBS3,
        'WBS1' AS WBSType,
        NULL AS ParentOutlineNumber,
        '001' AS OutlineNumber,
        0 AS ChildrenCount,
        0 AS OutlineLevel,

        @dtMinDate AS StartDate,
        @dtMaxDate  AS EndDate,

        O.ProjMgr AS ProjMgr,
        NULL AS ChargeType,
        NULL AS ProjectType,
        O.ClientID AS ClientID,
        ISNULL(O.Status, 'A') AS Status,
        O.Org AS Org,
        NULL AS Notes,

        @strCreateModUser AS CreateUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
        @strCreateModUser AS ModUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate

        FROM Opportunity AS O
        WHERE O.OpportunityID = @strOpportunityID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    -- Create WBS Level Format.

    INSERT PNWBSLevelFormat(
      WBSFormatID,
      PlanID,
      FmtLevel,
      WBSType,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT 
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS WBSFormatID,
        @strPlanID AS PlanID,
        1 AS FmtLevel,
        'WBS1' AS WBSType,
        @strCreateModUser AS CreateUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
        @strCreateModUser AS ModUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate

    IF(@siWBS2Length > 0)
      BEGIN

        INSERT PNWBSLevelFormat(
          WBSFormatID,
          PlanID,
          FmtLevel,
          WBSType,
          CreateUser,
          CreateDate,
          ModUser,
          ModDate
        )
          SELECT 
            REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS WBSFormatID,
            @strPlanID AS PlanID,
            2 AS FmtLevel,
            'WBS2' AS WBSType,
            @strCreateModUser AS CreateUser,
            CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
            @strCreateModUser AS ModUser,
            CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate

      END -- END IF(@siWBS2Length > 0)

    IF(@siWBS3Length > 0)
      BEGIN

        INSERT PNWBSLevelFormat(
          WBSFormatID,
          PlanID,
          FmtLevel,
          WBSType,
          CreateUser,
          CreateDate,
          ModUser,
          ModDate
        )
          SELECT 
            REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS WBSFormatID,
            @strPlanID AS PlanID,
            3 AS FmtLevel,
            'WBS3' AS WBSType,
            @strCreateModUser AS CreateUser,
            CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
            @strCreateModUser AS ModUser,
            CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate

      END -- END IF(@siWBS3Length > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Update Opportunity.PlanID

  UPDATE O SET 
    O.PlanID = @strPlanID
    FROM Opportunity AS O 
    WHERE O.OpportunityID = @strOpportunityID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Publish the PN Plan.

  EXECUTE dbo.stRPPublish @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  COMMIT

  SET NOCOUNT OFF

END -- stRPCreatePlanFromOpportunity
GO
