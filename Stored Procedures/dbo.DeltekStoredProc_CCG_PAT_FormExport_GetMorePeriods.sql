SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormExport_GetMorePeriods]
	@itemCount	INT
AS
BEGIN
	IF @itemCount > 3 SET @itemCount = 3
	SELECT TOP (4 - @itemCount) Period as Value, Convert(Nvarchar(10), Period) + ' (C)' as Display
        FROM CFGDates
        WHERE Period in (select distinct Period from CFGDatesStatus where Closed = 'Y')
        ORDER BY 1 Desc
END;
GO
