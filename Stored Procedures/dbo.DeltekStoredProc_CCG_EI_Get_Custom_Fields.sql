SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Get_Custom_Fields]
AS
BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_EI_Get_Custom_Fields]
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT syscolumns.name as KeyValue, syscolumns.name as Display
        FROM sysobjects
			INNER JOIN syscolumns on syscolumns.id = sysobjects.id
        WHERE sysobjects.Name = 'ProjectCustomTabFields'
			AND sysobjects.xtype = 'U'
			AND syscolumns.name like 'Cust%'
        ORDER BY syscolumns.name
END;
GO
