SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_SetUserLicense] ( @Username nvarchar(32), @OkdLicense varchar(1))
             AS EXEC spCCG_EI_SetUserLicense @Username,@OkdLicense
GO
