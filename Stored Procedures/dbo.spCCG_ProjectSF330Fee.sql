SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectSF330Fee] @WBS1 varchar (32)
AS
/*
Copyright 2019 (c) Central Consulting Group, Inc.  All rights reserved.
12/04/2019	David Springer
			SF330 Percents grid with SF330 Percent Code, SF330 Percent
			Call this from a Change workflow on Projects_SF330Percents table
			and a Change workflow on Project Contract Details Fee.
*/
BEGIN
SET NOCOUNT ON

	Update c
	Set c.Fees = f.Fee * s.CustSF330Percent / 100
	From Projects_SF330Percents s, PRProjectCodes c,
		(Select d.WBS1, Sum (d.FeeDirExp + d.FeeDirLab + d.ConsultFee + d.ReimbAllowCons + d.ReimbAllowExp) Fee
		From Contracts c, ContractDetails d, PR p
		Where c.WBS1 = d.WBS1
		  and c.ContractNumber = d.ContractNumber
		  and c.FeeIncludeInd = 'Y'
		  and d.WBS1 = p.WBS1
		  and d.WBS2 = p.WBS2
		  and d.WBS3 = p.WBS3
		  and p.SubLevel = 'N' -- loweest level
		Group by d.WBS1) f
	Where s.WBS1 = @WBS1
		and s.WBS1 = f.WBS1
		and s.WBS1 = c.WBS1
		and s.WBS2 = c.WBS2
		and s.WBS3 = c.WBS3
		and s.CustSF330PercentCode = c.ProjectCode
END
GO
