SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[spCCG_FleetMileageDateValidation] @Date date
AS
/*
Copyright 2019 (c) Central Consulting Group.   All rights reserved.
08/02/2019  David Springer
            Validate date is today or earlier
*/
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
BEGIN

   If @Date > convert (date, getDate())
      RAISERROR ('Date cannot be later than today.                                                                  ', 16, 1)

END
GO
