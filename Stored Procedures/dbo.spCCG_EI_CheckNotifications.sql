SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_CheckNotifications] (
	@Employee				Nvarchar(20),	/* Employee from employee-based scheduled workflow					*/
	@OverrideSubject		varchar(120),		/* if length > 0, then this becomes the email subject				*/
	@OverrideSenderEmail	varchar(100),		/* Can set to an email address to override default					*/
	@StatusToInclude		varchar(10),		/* Can be empty to include all project status's, or e.g. 'A,I' to exclude dormant */
	@MsgPriority			varchar(10) = NULL,	/* Optional: starts with L for Low or H for High else normal		*/
	@IncludeSupervisorAge	int = -1,			/* -1=never, 0=always, X=if action is X days old (5.0)				*/
	@InclWeekends			char(1) = 'Y',		/* Y = include weekends and holidays in count, N = exclude them		*/
	@RoundingHoursCutoff	int = 22			/* Number of hours at which interval rounds up to count as full day	*/
)
AS BEGIN
	/*
		Copyright (c) 2018 Central Consulting Group. All rights reserved.

		01/17/2013 JRK Updated for EI 4.3.x
		04/26/2010 JRK Updated for EI 3.0.1.x
		08/27/2010 JRK Updated to handle multiple stageflows
		10/18/2010 SPS Return success(0) or @@ERROR
		05/22/2011 SPS Send a single email per employee
		06/03/2011 SPS Added IsNull to SQL parameters in case they're null
		06/18/2011 SPS Changed to Employee-based scheduled workflow procedure - faster running
		07/08/2011 SPS Added " [re-notification]" after email subject and batch subject
		11/02/2011 SPS Redesigned script to use CCG_EI_Renotify table
		08/12/2013 SPS Added ability for renotifications to have valid EI URL links
		12/03/2013 SPS Added supervisor CC based on days old
		01/08/2018 JAM Send notifications to delegates as well
		09/12/2018 JAM Option added for including or excluding weekends/holidays
		09/25/2018 JAM Send notifications using the new CCG_EI_Pending table
		10/12/2018 JAM Rounding specification parameter added to set rounding cutoff point in hours

		select * from ccg_ei where wbs1='2002005.00'
		select emaillink from ccg_ei_config
		delete from CCG_EI_Renotify
		select count(*) from CCG_Email_Messages
		select count(*) from CCG_EI_History
		exec spCCG_EI_CheckNotifications '00001', '','', 'A', null, -1, 'N'
		select * from CCG_Email_Messages
		select count(*) from CCG_Email_Messages
		select count(*) from CCG_EI_History
		select * from CCG_Email_Messages
		select * from CCG_EI_History where wbs1='04114.02'

		select * from ccg_email_history
		select DATEDIFF(d, '2013-11-21 15:56:32.210', GETUTCDATE())
		update CCG_EI set DateLastNotificationSent='2018-09-20 16:00:00', DateChanged='2017-09-21 16:00:00' --where wbs1='2003005.00'
		update CCG_EI_Renotify set DateLastNotificationSent='2018-09-20 16:00:00'
		delete from CCG_Email_Messages
	*/
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	BEGIN TRY
		declare @Username					Nvarchar(32)
		declare @NotificationIntervalHours	int				-- Hours
		declare @EmailLink					varchar(1024)	-- 5.0 sps
		declare @EncTimeout					int				-- 5.0 sps
		declare @Email						Nvarchar(255)
		declare @Field						nvarchar(100)
		declare @WHERE						nvarchar(max)
		declare @STATUS_JOIN				nvarchar(255) = ''
		declare @STATUS_WHERE				nvarchar(255) = ''
		declare @SupEmail					Nvarchar(255)	-- 5.0 sps
		declare @Language					varchar(10)
		declare @delegation					varchar(1)
		declare @ApprovalRequired			varchar(1)
		declare @EmpCompany					varchar(14) = ''

		select @EmailLink=EmailLink, @EncTimeout=EncTimeout, @delegation = ISNULL(Delegation,'Y'), @ApprovalRequired = DelegationApproval from CCG_EI_Config

		select @NotificationIntervalHours=(MAX(isnull(Renotify,0))*24) from CCG_EI_ConfigRights
		--print 'Max Interval: ' + Convert(varchar(10),@NotificationIntervalHours)
		if IsNull(@NotificationIntervalHours,0) <= 0
		begin
			print 'No notification interval set'
			return
		end

		/* 5.0 sps */
		if @EmailLink <> ''
		begin
			select @Username=IsNull(Min(Username),N'') from SEUser where Employee=@Employee and DisableLogin='N'

			-- Determine if this is a Cloud deployment or non-Cloud (look for database connection info)
			if CHARINDEX('database',@EmailLink) > 0 or CHARINDEX('dbup',@EmailLink) > 0						-- not Cloud
				set @EmailLink = REPLACE(@EmailLink, '[:USERENC]', 'userenc=' + dbo.fnCCG_EI_GetE(@Username))
			else
				set @EmailLink = REPLACE(@EmailLink, '[:USERENC]', 'user=' + dbo.fnCCG_HexScrambleCode(@Username))
			set @EmailLink = REPLACE(@EmailLink, '[:USER]', 'user=' + dbo.fnCCG_HexScrambleCode(@Username))
			set @EmailLink = REPLACE(@EmailLink, '[:EDENC]',   'edenc='   + dbo.fnCCG_EI_GetE(Convert(varchar(20), DATEADD(d, @EncTimeout, getdate()),101)))

			--print @EmailLink
		end

		if isnull(@InclWeekends, 'Y') = 'N'
		begin
			if exists (select 'x' from CFGSystem where MulticompanyEnabled = 'Y')
			begin
				declare @sql2 nvarchar(max) = 'select @homeCompany = HomeCompany from EM where Employee = '''+@Employee+''''
			EXECUTE SP_EXECUTESQL @sql2, N'@homeCompany varchar(14) OUTPUT', @homeCompany = @EmpCompany OUTPUT
		end

			if DATEPART(dw, getdate()) in (1, 7) return																-- Weekend
			if exists (select 'x' from CFGHoliday
				Where Company = @EmpCompany and HolidayDate = DateAdd(dd, DateDiff(dd, 0, getdate()), 0)) return	-- Holiday
		end

		select @Email=Email,@Language = [Language] from EM where Employee=@Employee
		select @SupEmail=Sup.Email from EM inner join EM Sup on Sup.Employee=EM.Supervisor where EM.Employee=@Employee
		--print 'Email: ' + IsNull(@Email,'') + ', Sup Email: ' + IsNull(@SupEmail,'')
		if Len(isnull(@Email,N'')) < 1 begin
			Print 'Employee has no email address'
			return
		end
		if Len(isnull(@Language,'')) < 1 begin
			Print 'Employee has no Language'
			set @Language = 'en-US'
		end
	/*
	delete from CCG_Email_Messages
	delete from CCG_EI_History
	delete from CCG_EI_Renotify
	update ccg_ei set DateLastNotificationSent='2015-01-01', DateChanged = '2015-01-01'
	exec spCCG_EI_CheckNotifications '00003', '', '', '', '', -1
	exec spCCG_EI_CheckNotifications '00002', '', '', '', '', -1
	exec spCCG_EI_AddlNotifications '00003', 1, 'CustInvoicePrincipal', 1, '', '', '', 'A,I', 'H', -1
	exec spCCG_EI_AddlNotifications '00002', 1, 'CustInvoicePrincipal', 1, '', '', '', 'A,I', 'H', -1
	select * from CCG_EI_Renotify Order By InsertDate Desc
	select * from CCG_Email_Messages
	select * from CCG_EI_History
	*/
		if @StatusToInclude <> ''
			set @STATUS_WHERE = N' and PR.Status in (''' + Replace(Replace(@StatusToInclude,' ',''),',',''',''') + N''')'
		if @delegation <> 'N' or @STATUS_WHERE <> ''
			set @STATUS_JOIN = N' inner join PR on PR.WBS1=CCG_EI.WBS1 and PR.WBS2=N'' '' and PR.WBS3=N'' '' '

		declare @GUID uniqueidentifier, @nowutc datetime, @sql nvarchar(max), @sqlCC nvarchar(500)
		declare @daysCalculation nvarchar(1000) = 'ceiling(round(' + (case
				when isnull(@InclWeekends, 'Y') <> 'N' then 
					'(cast(datediff(minute, IsNull(DateLastNotificationSent, DateChanged), getutcdate()) as decimal(19,2)) / 60.0 - ('+cast(@RoundingHoursCutoff as varchar(5))+' - 12.0)) / 24.0 '
				else 
					'(cast(dbo.fnCCG_EI_GetBusinessDays(IsNull(DateLastNotificationSent, DateChanged), getutcdate(), '''+@EmpCompany+''') as decimal(19,2)) / 60.0 - ('+cast(@RoundingHoursCutoff as varchar(5))+' - 12.0)) / 24.0' 
			end) + ', 0))'

		set @nowutc=GETUTCDATE()
		set @GUID = NEWID()

		--print @GUID
		set @sql = N'
			insert into CCG_EI_Renotify (ID, InsertDate, Employee, WBS1, InvoiceStage, DateChanged, ChangedBy,
					DateLastNotificationSent, EmailSubjectBatch, EmailSubject, EmailMessage, CCSender, MessageDelay)
			select DISTINCT ''' + CAST(@GUID as varchar(36)) + N''', ''' + Cast(@nowutc as varchar) + N''', N''' + @Employee + ''',
				CCG_EI.WBS1, CCG_EI.InvoiceStage, CCG_EI.DateChanged, CCG_EI.ChangedBy, CCG_EI.DateLastNotificationSent,
				isnull(emailLang.EmailSubjectBatch,rights.EmailSubjectBatch),
				isnull(emailLang.EmailSubject,rights.EmailSubject),
				isnull(emailLang.EmailMessage,rights.EmailMessage), CCSender,
				roles.MessageDelay
			from CCG_EI ' + @STATUS_JOIN + '
				inner join ProjectCustomTabFields PCTF on PCTF.WBS1=CCG_EI.WBS1 and PCTF.WBS2='' ''
				inner join CCG_EI_Pending p on p.WBS1 = CCG_EI.WBS1 and isnull(p.Parallel, '''') <> ''C''
				inner join CCG_EI_ConfigRights rights on rights.StageFlow = PCTF.CustInvoiceStageFlow and rights.Stage = CCG_EI.InvoiceStage and rights.Role = p.Role
				inner join CCG_EI_ConfigRoles roles on roles.Role = p.Role and roles.Status = ''A'' --rights.Role
				left join CCG_EI_ConfigRightsDescriptions emailLang on rights.StageFlow=emailLang.StageFlow and rights.Role = emailLang.Role and rights.Stage=emailLang.Stage and emailLang.UICultureName ='''+ @Language +''' '
				-- left join EM on EM.Employee=CCG_EI.ChangedBy
			+ (case when @delegation = 'Y' then '
				left join CCG_EI_Delegation  d1 on  d1.Delegate = N'''+@Employee+''' AND (isnull( d1.ApprovedBy,N'''') <> N'''' or '''+@ApprovalRequired+''' = ''N'') AND GETDATE() between  d1.FromDate and dateadd(d, 1,  d1.ToDate) AND isnull( d1.ForWBS1,'''') = ''''     and isnull( d1.ForClientId,'''') = ''''
				left join CCG_EI_Delegation  d2 on  d2.Delegate = N'''+@Employee+''' AND (isnull( d2.ApprovedBy,N'''') <> N'''' or '''+@ApprovalRequired+''' = ''N'') AND GETDATE() between  d2.FromDate and dateadd(d, 1,  d2.ToDate) AND isnull( d2.ForWBS1,'''') = ''''     and isnull( d2.ForClientId,'''') = PR.BillingClientID
				left join CCG_EI_Delegation  d3 on  d3.Delegate = N'''+@Employee+''' AND (isnull( d3.ApprovedBy,N'''') <> N'''' or '''+@ApprovalRequired+''' = ''N'') AND GETDATE() between  d3.FromDate and dateadd(d, 1,  d3.ToDate) AND isnull( d3.ForWBS1,'''') = PR.WBS1  and isnull( d3.ForClientId,'''') = ''''
				left join CCG_EI_Delegation dt1 on dt1.Employee = N'''+@Employee+''' AND (isnull(dt1.ApprovedBy,N'''') <> N'''' or '''+@ApprovalRequired+''' = ''N'') AND GETDATE() between dt1.FromDate and dateadd(d, 1, dt1.ToDate) AND isnull(dt1.ForWBS1,'''') = ''''     and isnull(dt1.ForClientId,'''') = ''''
				left join CCG_EI_Delegation dt2 on dt2.Employee = N'''+@Employee+''' AND (isnull(dt2.ApprovedBy,N'''') <> N'''' or '''+@ApprovalRequired+''' = ''N'') AND GETDATE() between dt2.FromDate and dateadd(d, 1, dt2.ToDate) AND isnull(dt2.ForWBS1,'''') = ''''     and isnull(dt2.ForClientId,'''') = PR.BillingClientID
				left join CCG_EI_Delegation dt3 on dt3.Employee = N'''+@Employee+''' AND (isnull(dt3.ApprovedBy,N'''') <> N'''' or '''+@ApprovalRequired+''' = ''N'') AND GETDATE() between dt3.FromDate and dateadd(d, 1, dt3.ToDate) AND isnull(dt3.ForWBS1,'''') = PR.WBS1  and isnull(dt3.ForClientId,'''') = '''' '
			else '' end) + '
			where isnull(rights.Renotify, 0) > 0 AND rights.CanModify = ''Y''
				and ('+@daysCalculation+' >= rights.Renotify '
					/* Send ANOTHER notification for this WBS1 for another employee (delegate or delegator) if an automated notification has been sent within the last 3 hours (=during the same scheduled workflow session) [JAM] */
					+ (case when @delegation = 'Y' then ' or (datediff(mi, CCG_EI.DateChanged, CCG_EI.DateLastNotificationSent) > 2 '   /* must not be right after sending the initial email on stage change */
						+ 'and datediff(hh, CCG_EI.DateLastNotificationSent, getutcdate()) <= 3) ' else '' end) + '
				)' + @STATUS_WHERE

		if @delegation = 'N'
			set @sql = @sql + N'
				and p.Employee = N'''+@employee+''''
		else
			set @sql = @sql + N'
				and (
						(p.CanModify = ''Y'' and (
							   (p.Employee = d3.Employee AND d3.Employee is not null)
							OR (p.Employee = d2.Employee AND ISNULL(d3.Employee,'''')='''' AND d2.Employee is not null)
							OR (p.Employee = d1.Employee AND ISNULL(d3.Employee,'''')='''' AND ISNULL(d2.Employee,'''')='''' AND d1.Employee is not null)
					))
					OR (p.Employee = N'''+@employee+''' AND
							(ISNULL(dt3.Delegate,'''')=''''	OR dt3.Dual=''Y'')
						AND (ISNULL(dt2.Delegate,'''')='''' OR dt2.Dual=''Y'')
						AND (ISNULL(dt1.Delegate,'''')='''' OR dt1.Dual=''Y''))
				) '

		--select @sql
		exec(@sql)
	--  	exec spCCG_EI_CheckNotifications '00001', '', '', '','h'

		if @IncludeSupervisorAge > -1
			set @sqlCC = 'case when CCSender = ''Y'' then Sender.Email + '';'' else '''' end'
		else
			set @sqlCC = 'case when CCSender = ''Y'' then Sender.Email else '''' end'
		/* Check if we're to include the employee's supervisor on the email: */
		if IsNull(@SupEmail,'') <> '' and @IncludeSupervisorAge = 0	/* always */
			set @sqlCC = @sqlCC + ' + ''' + @SupEmail + ''''
		else if IsNull(@SupEmail,'') <> '' and @IncludeSupervisorAge > 0 /* only if past due ages exceeds this many days */
			set @sqlCC = @sqlCC + ' + case When DATEDIFF(d, DateChanged, GETUTCDATE()) >= ' + Cast(@IncludeSupervisorAge as varchar) +
				' Then ''' + @SupEmail + ''' Else '''' End'
		--print @sqlCC
		set @sql = N'insert into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, BCCList, GroupSubject, [Subject], Body,MaxDelay,Priority) ' +
			N' select ''Electronic Invoicing'', case When Len(''' + @OverrideSenderEmail + ''')>1 Then ''' + @OverrideSenderEmail + ''' Else Sender.Email End, ' +
			N' ''' + @Email + ''', ' + @sqlCC + ', null, ' +
			N' case When Len(''' + @OverrideSubject + ''')>1 Then ''' + @OverrideSubject + ''' Else EmailSubjectBatch + '' [re-notification]'' End, ' +
			N' Replace(Replace(Replace(EmailSubject, ''[:PR.WBS1]'', CCG_EI_Renotify.WBS1), ''[:PR.Name]'', PR.Name), ''[:EM.Name]'', ToEM.Lastname+'', '' +ToEM.Firstname) + '' [re-notification]'', ' +
			N' Replace(Replace(Replace(Replace(Replace(EmailMessage, ''[:PR.WBS1]'', CCG_EI_Renotify.WBS1), ''[:PR.Name]'', PR.Name), ''[:EM.Name]'', ToEM.Lastname+'', '' +ToEM.Firstname),''[:LINK]'',''' + @EmailLink + ''') ' +
			N' , ''[:RENOTIFYDAYS]'', '' ['' + Cast('+@daysCalculation+' as varchar) + '' day re-notification]'') ' +
			N' ,MessageDelay,case when LEFT(UPPER(''' + isnull(@MsgPriority,'NULL') + '''),1)=''N'' then null else ''' + isnull(@MsgPriority,'NULL') + ''' end ' + --hard code delay to 1,
			N' from CCG_EI_Renotify ' +
			N' left join PR on PR.WBS1=CCG_EI_Renotify.WBS1 and PR.WBS2 = '' ''' +--for PR.Name
			N' left join EM Sender on Sender.Employee=CCG_EI_Renotify.ChangedBy ' +
			N' left join EM ToEM on ToEM.Employee=CCG_EI_Renotify.Employee ' +--for EM.Name
			N' where ID=''' + CAST(@GUID as varchar(36)) + ''''
		--print @sql
		exec(@sql)

		-- Email service only logs in CCG_EMAIL_History when email is sent, CCG_EI_History entries needed for reporting
		-- Add history entry:
		set @sql = N'insert into CCG_EI_History (ActionTaken, ActionDate, ActionTakenBy, ActionRecipient, WBS1, InvoiceStage, PriorInvoiceStage) ' +
			N' select ''Email Reminder Sent'',''' + Cast(@nowutc as varchar) + ''', ' +
			N' ''Re-Notification Script'', N''' + @Employee + ''', WBS1, InvoiceStage, null ' +
			N' from CCG_EI_Renotify ' +
			N' where ID=''' + CAST(@GUID as varchar(36)) + ''''
		--print @sql
		exec(@sql)


		---- Reset DateLastNotificationDate for this employee:
		set @sql = 'update CCG_EI set DateLastNotificationSent=getutcdate() ' +
			N' from CCG_EI inner join CCG_EI_Renotify r on r.WBS1=CCG_EI.WBS1 ' +
			N' where ID=''' + CAST(@GUID as varchar(36)) + ''''
		--print @sql
		exec(@sql)

		return 0
	END TRY
	BEGIN CATCH
		declare @res int
		set @res = ERROR_NUMBER()
		--print Cursor_Status('variable', 'FieldCursor')
		if Cursor_Status('variable', 'FieldCursor') > -1
			close FieldCursor
		if Cursor_Status('variable', 'FieldCursor') > -2
			deallocate FieldCursor

		-- Try to log the error:
		insert into CCG_EI_Renotify (ID, InsertDate, Employee, WBS1, InvoiceStage, DateChanged, ChangedBy)
		values(NEWID(),GETDATE(),@Employee,CAST(@res as varchar),'',GETDATE(),'ERROR')

		return @res
		-- SQL found some error - return the message and number:
		--select -1 as Result, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage
	END CATCH
END
GO
