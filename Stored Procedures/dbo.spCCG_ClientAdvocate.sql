SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[spCCG_ClientAdvocate] @ClientID varchar (32)
AS
/*
Copyright (c) 2020 Central Consulting Group. All rights reserved.
06/12/2020	David Springer
			Write Client Manager to the employee associations grid.
			Call this from a Client INSERT & CHANGE workflow.
*/
BEGIN
SET NOCOUNT ON

--  Insert the employee if not in the grid
	Insert Into EMClientAssoc (Employee, ClientID, Relationship, CreateDate, CreateUser)
	Select CustClientAdvocate, ClientID, '01', GETDATE(), 'CCG Procedure'
	From ClientCustomTabFields c
	Where c.ClientID = @ClientID
	and not exists (Select * From EMClientAssoc Where Employee = c.CustClientAdvocate and ClientID = c.ClientID)

--  Update the association role to Client Manager if the employee is already in the grid
	If exists (Select * From ClientCustomTabFields c, EMClientAssoc a
				Where a.Employee = c.CustClientAdvocate 
				and a.ClientID = c.ClientID)
		Begin
	--  Update the existing employee to Client Manager
		Update a
		Set a.Relationship = '01'
		From ClientCustomTabFields c, EMClientAssoc a
		Where a.Employee = c.CustClientAdvocate 
		and a.ClientID = c.ClientID
		and c.ClientID = @ClientID
		End

--  Update any other employee who may have been Client Manager to null
	Update a
	Set a.Relationship = null
	From ClientCustomTabFields c, EMClientAssoc a
	Where a.Employee <> c.CustClientAdvocate 
	and a.ClientID = c.ClientID
	and c.ClientID = @ClientID
	and a.Relationship = '01'
END
GO
