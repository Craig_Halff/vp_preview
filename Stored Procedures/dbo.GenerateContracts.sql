SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[GenerateContracts]
@checkCompanies			varchar(1), -- Y Yes Check Companies, N No Check Companies
@MulticurrencyEnabled	varchar(1), -- Y Yes @MulticurrencyEnabled, N No @MulticurrencyEnabled
@SecurityRole			Nvarchar(20), 
@UserName				Nvarchar(32) = 'VisionContract',
@AdditionalWhere		Nvarchar(max) = ''
as
begin -- Procedure
	set nocount on
	declare @WBSFetch						integer
	declare @WBS1 							Nvarchar(30)
	declare @WBS2 							Nvarchar(7)
	declare @WBS3 							Nvarchar(7)
	declare @Fee							decimal(19,4)
	declare @ConsultFee						decimal(19,4)
	declare @ReimbAllow						decimal(19,4)
	declare @FeeBillingCurrency				decimal(19,4)
	declare @ConsultFeeBillingCurrency		decimal(19,4)
	declare @ReimbAllowBillingCurrency		decimal(19,4)
	declare @FeeFunctionalCurrency			decimal(19,4)
	declare @ConsultFeeFunctionalCurrency	decimal(19,4)
	declare @ReimbAllowFunctionalCurrency	decimal(19,4)
	
	declare @FeeDirLab						decimal(19,4)
	declare @FeeDirExp						decimal(19,4)
	declare @ReimbAllowExp					decimal(19,4)
	declare @ReimbAllowCons					decimal(19,4)
	declare @FeeDirLabBillingCurrency		decimal(19,4)
	declare @FeeDirExpBillingCurrency		decimal(19,4)
	declare @ReimbAllowExpBillingCurrency	decimal(19,4)
	declare @ReimbAllowConsBillingCurrency	decimal(19,4)
	declare @FeeDirLabFunctionalCurrency	decimal(19,4)
	declare @FeeDirExpFunctionalCurrency	decimal(19,4)
	declare @ReimbAllowExpFunctionalCurrency  decimal(19,4)
	declare @ReimbAllowConsFunctionalCurrency decimal(19,4)
		
	declare @BillingCurrencyCode 			Nvarchar(3)
	declare @ProjectCurrencyCode 			Nvarchar(3)
	declare @ProjectExchangeRate			decimal(19,4)
	declare @BillingExchangeRate			decimal(19,4)
	declare @Org1Len						integer
	declare @usingWBS2						integer
	declare @usingWBS3						integer
	declare @usedWBS1						Nvarchar(30)
	declare @usedContract					Nvarchar(30)
	declare @SQLString						Nvarchar(max)
	declare @FeeIncludeInd		 			Nvarchar(1)
	declare @AutoSumComp		 			Nvarchar(1)

	Set @usedWBS1 = ''
	Set @Org1Len = (select Org1Length from CFGFormat)
	Set @usingWBS2 = (select WBS2Length from CFGFormat)
	Set @usingWBS3 = (select WBS3Length from CFGFormat)
	Set @FeeIncludeInd = (select SyncProjToContractFees from FW_CFGSystem)
	Set @AutoSumComp = (select AutoSumComp from FW_CFGSystem)

	declare @tempTable as varchar(40)
	set @tempTable = '##tempPRSource'+REPLACE(CONVERT(VARCHAR(10),GETUTCDATE(),108),':','')
	print @tempTable

	set @SQLString = 'Select LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3, LEVEL3.Fee, LEVEL3.ConsultFee, LEVEL3.ReimbAllow , ' +
		'LEVEL3.FeeBillingCurrency, LEVEL3.ConsultFeeBillingCurrency, LEVEL3.ReimbAllowBillingCurrency, LEVEL3.BillingCurrencyCode, ' +
		'LEVEL3.FeeFunctionalCurrency, LEVEL3.ConsultFeeFunctionalCurrency, LEVEL3.ReimbAllowFunctionalCurrency, LEVEL3.ProjectCurrencyCode, ' +
		'LEVEL3.ProjectExchangeRate, LEVEL3.BillingExchangeRate, ' +		
		'LEVEL3.FeeDirLab, LEVEL3.FeeDirExp, LEVEL3.ReimbAllowExp, LEVEL3.ReimbAllowCons, ' +
		'LEVEL3.FeeDirLabBillingCurrency, LEVEL3.FeeDirExpBillingCurrency, LEVEL3.ReimbAllowExpBillingCurrency, LEVEL3.ReimbAllowConsBillingCurrency, ' +
		'LEVEL3.FeeDirLabFunctionalCurrency, LEVEL3.FeeDirExpFunctionalCurrency, LEVEL3.ReimbAllowExpFunctionalCurrency, LEVEL3.ReimbAllowConsFunctionalCurrency ' +				
		'into '+@tempTable+' FROM PR AS LEVEL3 ' +
		'INNER JOIN PR AS LEVEL2 ON LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 ' +
		'INNER JOIN PR ON PR.WBS1 = LEVEL2.WBS1 ' +
		case when @checkCompanies = 'N' then '' else 'LEFT Outer JOIN SECompany ON (LEFT(SECompany.Company, ' + convert(varchar,@Org1Len) + ') = LEFT(LEVEL3.Org, ' + convert(varchar,@Org1Len) + ') AND SECompany.Role = N'''+@SecurityRole+''') ' end +
		'Where ' +
		'PR.WBS2 = N'' '' AND PR.WBS3 = N'' '' AND LEVEL2.WBS3 = N'' ''  AND ' +
		case when @MulticurrencyEnabled = 'N' 
		then 
			'(LEVEL3.Fee <> 0.0 OR LEVEL3.ConsultFee <> 0.0 OR LEVEL3.ReimbAllow <> 0.0 ) ' 
		else
			'((ISNULL(LEVEL3.ProjectCurrencyCode, '''') > '''' AND (LEVEL3.Fee <> 0.0 OR LEVEL3.ConsultFee <> 0.0 OR LEVEL3.ReimbAllow <> 0.0)) ' +
			'OR (ISNULL(LEVEL3.BillingCurrencyCode, '''') > '''' AND (LEVEL3.FeeBillingCurrency <> 0.0 OR LEVEL3.ConsultFeeBillingCurrency <> 0.0 OR LEVEL3.ReimbAllowBillingCurrency <> 0.0)) ' +
			'OR LEVEL3.FeeFunctionalCurrency <> 0.0 OR LEVEL3.ConsultFeeFunctionalCurrency <> 0.0 OR LEVEL3.ReimbAllowFunctionalCurrency <> 0.0 ) ' 
		end +
		case when @checkCompanies = 'N' then '' else 'and (SECompany.Role = N'''+@SecurityRole+''') ' end +
		@AdditionalWhere +
		' Order By LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3'

	print '@SQLString'
	print @SQLString
	Execute(@SQLString)
	
	set @SQLString = 'declare WBSCursor cursor for Select * from '+@tempTable+' Order By WBS1, WBS2, WBS3'
	print '@SQLString'
	print @SQLString
	Execute(@SQLString)
	
	print 'Start open WBSCursor'
	open WBSCursor
	fetch next from WBSCursor into 
		@WBS1, @WBS2, @WBS3, @Fee, @ConsultFee, @ReimbAllow, 
		@FeeBillingCurrency, @ConsultFeeBillingCurrency, @ReimbAllowBillingCurrency, @BillingCurrencyCode, 
		@FeeFunctionalCurrency, @ConsultFeeFunctionalCurrency, @ReimbAllowFunctionalCurrency, @ProjectCurrencyCode, 
		@ProjectExchangeRate, @BillingExchangeRate,
		@FeeDirLab,						
		@FeeDirExp,						
		@ReimbAllowExp	,				
		@ReimbAllowCons,					
		@FeeDirLabBillingCurrency		,
		@FeeDirExpBillingCurrency		,
		@ReimbAllowExpBillingCurrency	,
		@ReimbAllowConsBillingCurrency	,
		@FeeDirLabFunctionalCurrency	,
		@FeeDirExpFunctionalCurrency	,
		@ReimbAllowExpFunctionalCurrency ,
		@ReimbAllowConsFunctionalCurrency 

	set @WBSFetch = @@Fetch_Status

	begin transaction
	While (@WBSFetch = 0)
	begin
		print 'Loop Through WBSCursor record'
		If (@usingWBS2<=0 Or @WBS2 <= ' ')
			begin
				--Only Project level.
				Set @usedContract = isnull(ltrim(rtrim(convert(Nvarchar,(select isnull(MAX(Convert(BigInt, ContractNumber)),0)+1 as ContractNumber From Contracts Where ISNUMERIC(ContractNumber) = 1 and not ContractNumber like '%.%' and WBS1=@WBS1 Group by WBS1)))),'1')
				print 'Insert Contracts : ' + @WBS1 + '/' + @usedContract
				Insert Into Contracts (ContractNumber, WBS1, CreateUser, ModUser, FeeIncludeInd) Select @usedContract, @WBS1, @UserName, @UserName, @FeeIncludeInd
				Set @usedWBS1 = @WBS1

				print 'Insert ContractDetails : ' + @WBS1 + '/' + @WBS2 + '/' + @WBS3 + '/' + @usedContract
				Insert Into ContractDetails (ContractNumber, WBS1, WBS2, WBS3, Fee, ConsultFee, ReimbAllow
				,FeeBillingCurrency, ConsultFeeBillingCurrency, ReimbAllowBillingCurrency
				,FeeFunctionalCurrency, ConsultFeeFunctionalCurrency, ReimbAllowFunctionalCurrency
				,Total, TotalBillingCurrency, TotalFunctionalCurrency, 
				FeeDirLab,						
				FeeDirExp,						
				ReimbAllowExp	,				
				ReimbAllowCons,					
				FeeDirLabBillingCurrency		,
				FeeDirExpBillingCurrency		,
				ReimbAllowExpBillingCurrency	,
				ReimbAllowConsBillingCurrency	,
				FeeDirLabFunctionalCurrency	,
				FeeDirExpFunctionalCurrency	,
				ReimbAllowExpFunctionalCurrency ,
				ReimbAllowConsFunctionalCurrency,
				CreateUser, ModUser) 
				Select @usedContract, @WBS1, @WBS2, @WBS3, @Fee, @ConsultFee, @ReimbAllow, 
				@FeeBillingCurrency, @ConsultFeeBillingCurrency, @ReimbAllowBillingCurrency, 
				@FeeFunctionalCurrency, @ConsultFeeFunctionalCurrency, @ReimbAllowFunctionalCurrency,
				@Fee+@ConsultFee+@ReimbAllow,
				@FeeBillingCurrency+@ConsultFeeBillingCurrency+@ReimbAllowBillingCurrency, 
				@FeeFunctionalCurrency+@ConsultFeeFunctionalCurrency+@ReimbAllowFunctionalCurrency, 
				@FeeDirLab,						
				@FeeDirExp,						
				@ReimbAllowExp	,				
				@ReimbAllowCons,					
				@FeeDirLabBillingCurrency		,
				@FeeDirExpBillingCurrency		,
				@ReimbAllowExpBillingCurrency	,
				@ReimbAllowConsBillingCurrency	,
				@FeeDirLabFunctionalCurrency	,
				@FeeDirExpFunctionalCurrency	,
				@ReimbAllowExpFunctionalCurrency ,
				@ReimbAllowConsFunctionalCurrency,								
				@UserName, @UserName
			end
		Else 
			begin
			if (@usingWBS3<=0 Or @WBS3 <= ' ')
				--Have WBS1/WBS2. Have to make sure there is no WBS3 attached--if so only use WBS3 level.
				begin
					declare @WBS3Row smallint
					if @MulticurrencyEnabled = 'N' 
						set @WBS3Row = (select COUNT(*) from PR Where WBS1=@WBS1 and WBS2=@WBS2 and WBS3>' ' 
							and (Fee <> 0.0 OR ConsultFee <> 0.0 OR ReimbAllow <> 0.0 ))
					else
						set @WBS3Row = (select COUNT(*) from PR Where WBS1=@WBS1 and WBS2=@WBS2 and WBS3>' ' 
							and ((ISNULL(ProjectCurrencyCode, '') > '' AND (Fee <> 0.0 OR ConsultFee <> 0.0 OR ReimbAllow <> 0.0))
							OR (ISNULL(BillingCurrencyCode, '') > '' AND (FeeBillingCurrency <> 0.0 OR ConsultFeeBillingCurrency <> 0.0 OR ReimbAllowBillingCurrency <> 0.0)) 
							OR FeeFunctionalCurrency <> 0.0 OR ConsultFeeFunctionalCurrency <> 0.0 OR ReimbAllowFunctionalCurrency <> 0.0 ) )
					if @WBS3Row = 0 
						--OK, no WBS3s so generate for WBS2 as is.
						begin
							if @WBS1 <> @usedWBS1 
								begin
									Set @usedContract = isnull(ltrim(rtrim(convert(Nvarchar,(select isnull(MAX(Convert(BigInt, ContractNumber)),0)+1 as ContractNumber From Contracts Where ISNUMERIC(ContractNumber) = 1 and not ContractNumber like '%.%' and WBS1=@WBS1 Group by WBS1)))),'1')
				print 'Insert Contracts : ' + @WBS1 + '/' + @usedContract
									Insert Into Contracts (ContractNumber, WBS1, CreateUser, ModUser, FeeIncludeInd) Select @usedContract, @WBS1, @UserName, @UserName, @FeeIncludeInd
									Set @usedWBS1 = @WBS1
								end
				print 'Insert ContractDetails : ' + @WBS1 + '/' + @WBS2 + '/' + @WBS3 + '/' + @usedContract
							Insert Into ContractDetails (ContractNumber, WBS1, WBS2, WBS3, Fee, ConsultFee, ReimbAllow
							,FeeBillingCurrency, ConsultFeeBillingCurrency, ReimbAllowBillingCurrency
							,FeeFunctionalCurrency, ConsultFeeFunctionalCurrency, ReimbAllowFunctionalCurrency
							,Total, TotalBillingCurrency, TotalFunctionalCurrency,
							FeeDirLab,						
							FeeDirExp,						
							ReimbAllowExp	,				
							ReimbAllowCons,					
							FeeDirLabBillingCurrency		,
							FeeDirExpBillingCurrency		,
							ReimbAllowExpBillingCurrency	,
							ReimbAllowConsBillingCurrency	,
							FeeDirLabFunctionalCurrency	,
							FeeDirExpFunctionalCurrency	,
							ReimbAllowExpFunctionalCurrency ,
							ReimbAllowConsFunctionalCurrency,							
							CreateUser, ModUser) 
							Select @usedContract, @WBS1, @WBS2, @WBS3, @Fee, @ConsultFee, @ReimbAllow, 
							@FeeBillingCurrency, @ConsultFeeBillingCurrency, @ReimbAllowBillingCurrency, 
							@FeeFunctionalCurrency, @ConsultFeeFunctionalCurrency, @ReimbAllowFunctionalCurrency,
							@Fee+@ConsultFee+@ReimbAllow,
							@FeeBillingCurrency+@ConsultFeeBillingCurrency+@ReimbAllowBillingCurrency, 
							@FeeFunctionalCurrency+@ConsultFeeFunctionalCurrency+@ReimbAllowFunctionalCurrency, 
							@FeeDirLab,						
							@FeeDirExp,						
							@ReimbAllowExp	,				
							@ReimbAllowCons,					
							@FeeDirLabBillingCurrency		,
							@FeeDirExpBillingCurrency		,
							@ReimbAllowExpBillingCurrency	,
							@ReimbAllowConsBillingCurrency	,
							@FeeDirLabFunctionalCurrency	,
							@FeeDirExpFunctionalCurrency	,
							@ReimbAllowExpFunctionalCurrency ,
							@ReimbAllowConsFunctionalCurrency,								
							@UserName, @UserName
							if not exists(select WBS1 from ContractDetails where ContractNumber=@usedContract and WBS1=@WBS1 and WBS2=' ') 
								begin
									insert ContractDetails (ContractNumber, WBS1, WBs2, wbs3) VALUES (@usedContract,@WBS1,' ',' ')
								end
						end
				end
			else
				--Have WBS1/WBS2/WBS3
				begin
					if @WBS1 <> @usedWBS1 
						begin
							Set @usedContract = isnull(ltrim(rtrim(convert(Nvarchar,(select isnull(MAX(Convert(BigInt, ContractNumber)),0)+1 as ContractNumber From Contracts Where ISNUMERIC(ContractNumber) = 1 and not ContractNumber like '%.%' and WBS1=@WBS1 Group by WBS1)))),'1')
				print 'Insert Contracts : ' + @WBS1 + '/' + @usedContract
							Insert Into Contracts (ContractNumber, WBS1, CreateUser, ModUser, FeeIncludeInd) Select @usedContract, @WBS1, @UserName, @UserName, @FeeIncludeInd
							Set @usedWBS1 = @WBS1
						end
				print 'Insert ContractDetails : ' + @WBS1 + '/' + @WBS2 + '/' + @WBS3 + '/' + @usedContract
					Insert Into ContractDetails (ContractNumber, WBS1, WBS2, WBS3, Fee, ConsultFee, ReimbAllow
					,FeeBillingCurrency, ConsultFeeBillingCurrency, ReimbAllowBillingCurrency
					,FeeFunctionalCurrency, ConsultFeeFunctionalCurrency, ReimbAllowFunctionalCurrency
					,Total, TotalBillingCurrency, TotalFunctionalCurrency, 
					FeeDirLab,						
					FeeDirExp,						
					ReimbAllowExp	,				
					ReimbAllowCons,					
					FeeDirLabBillingCurrency		,
					FeeDirExpBillingCurrency		,
					ReimbAllowExpBillingCurrency	,
					ReimbAllowConsBillingCurrency	,
					FeeDirLabFunctionalCurrency	,
					FeeDirExpFunctionalCurrency	,
					ReimbAllowExpFunctionalCurrency ,
					ReimbAllowConsFunctionalCurrency,												
					CreateUser, ModUser) 
					Select @usedContract, @WBS1, @WBS2, @WBS3, @Fee, @ConsultFee, @ReimbAllow, 
					@FeeBillingCurrency, @ConsultFeeBillingCurrency, @ReimbAllowBillingCurrency, 
					@FeeFunctionalCurrency, @ConsultFeeFunctionalCurrency, @ReimbAllowFunctionalCurrency,
					@Fee+@ConsultFee+@ReimbAllow,
					@FeeBillingCurrency+@ConsultFeeBillingCurrency+@ReimbAllowBillingCurrency, 
					@FeeFunctionalCurrency+@ConsultFeeFunctionalCurrency+@ReimbAllowFunctionalCurrency, 
					@FeeDirLab,						
					@FeeDirExp,						
					@ReimbAllowExp	,				
					@ReimbAllowCons,					
					@FeeDirLabBillingCurrency		,
					@FeeDirExpBillingCurrency		,
					@ReimbAllowExpBillingCurrency	,
					@ReimbAllowConsBillingCurrency	,
					@FeeDirLabFunctionalCurrency	,
					@FeeDirExpFunctionalCurrency	,
					@ReimbAllowExpFunctionalCurrency ,
					@ReimbAllowConsFunctionalCurrency,													
					@UserName, @UserName
					if not exists(select WBS1 from ContractDetails where ContractNumber=@usedContract and WBS1=@WBS1 and WBS2=' ') 
						begin
							insert ContractDetails (ContractNumber, WBS1, WBs2, wbs3) VALUES (@usedContract,@WBS1,' ',' ')
						end
				end
			end

		fetch next from WBSCursor into 
				@WBS1, @WBS2, @WBS3, @Fee, @ConsultFee, @ReimbAllow, 
				@FeeBillingCurrency, @ConsultFeeBillingCurrency, @ReimbAllowBillingCurrency, @BillingCurrencyCode, 
				@FeeFunctionalCurrency, @ConsultFeeFunctionalCurrency, @ReimbAllowFunctionalCurrency, @ProjectCurrencyCode, 
				@ProjectExchangeRate, @BillingExchangeRate,
				@FeeDirLab,						
				@FeeDirExp,						
				@ReimbAllowExp	,				
				@ReimbAllowCons,					
				@FeeDirLabBillingCurrency		,
				@FeeDirExpBillingCurrency		,
				@ReimbAllowExpBillingCurrency	,
				@ReimbAllowConsBillingCurrency	,
				@FeeDirLabFunctionalCurrency	,
				@FeeDirExpFunctionalCurrency	,
				@ReimbAllowExpFunctionalCurrency ,
				@ReimbAllowConsFunctionalCurrency 
			set @WBSFetch = @@Fetch_Status
	end --while
	print 'Finish WBSCursor record'
	commit Transaction

	close WBSCursor
	deallocate WBSCursor
	Execute('Drop Table '+@tempTable)

	Exec UpdateContractDetailSubTotals

	if @FeeIncludeInd = 'Y'
	Begin	
		set @SQLString = 'declare WBS1Cursor cursor for ' +
			'Select Distinct LEVEL3.WBS1 ' +				
			'FROM PR AS LEVEL3 ' +
			'INNER JOIN PR AS LEVEL2 ON LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 ' +
			'INNER JOIN PR ON PR.WBS1 = LEVEL2.WBS1 ' +
			case when @checkCompanies = 'N' then '' else 'LEFT Outer JOIN SECompany ON (LEFT(SECompany.Company, ' + convert(varchar,@Org1Len) + ') = LEFT(LEVEL3.Org, ' + convert(varchar,@Org1Len) + ') AND SECompany.Role = N'''+@SecurityRole+''') ' end +
			'Where ' +
			'PR.WBS2 = N'' '' AND PR.WBS3 = N'' '' AND LEVEL2.WBS3 = N'' ''  AND ' +
			case when @MulticurrencyEnabled = 'N' 
			then 
				'(LEVEL3.Fee <> 0.0 OR LEVEL3.ConsultFee <> 0.0 OR LEVEL3.ReimbAllow <> 0.0 ) ' 
			else
				'((ISNULL(LEVEL3.ProjectCurrencyCode, '''') > '''' AND (LEVEL3.Fee <> 0.0 OR LEVEL3.ConsultFee <> 0.0 OR LEVEL3.ReimbAllow <> 0.0)) ' +
				'OR (ISNULL(LEVEL3.BillingCurrencyCode, '''') > '''' AND (LEVEL3.FeeBillingCurrency <> 0.0 OR LEVEL3.ConsultFeeBillingCurrency <> 0.0 OR LEVEL3.ReimbAllowBillingCurrency <> 0.0)) ' +
				'OR LEVEL3.FeeFunctionalCurrency <> 0.0 OR LEVEL3.ConsultFeeFunctionalCurrency <> 0.0 OR LEVEL3.ReimbAllowFunctionalCurrency <> 0.0 ) ' 
			end +
			case when @checkCompanies = 'N' then '' else 'and (SECompany.Role = N'''+@SecurityRole+''') ' end +
			@AdditionalWhere +
			' Order By LEVEL3.WBS1'

		print '@SQLString'
		print @SQLString
		Execute(@SQLString)
		
		print 'Start open WBS1Cursor'
		open WBS1Cursor
		fetch next from WBS1Cursor into @WBS1

		set @WBSFetch = @@Fetch_Status

		begin transaction
		While (@WBSFetch = 0)
		begin
			print 'Loop Through WBS1Cursor record'

			--StrSQLSyncFeesToContracts_ForPhaseMC
			update PR
			set PR.Fee = C.Fee, PR.ConsultFee = C.ConsultFee, PR.ReimbAllow = C.ReimbAllow,
				PR.FeeBillingCurrency = C.FeeBillingCurrency, PR.ConsultFeeBillingCurrency = C.ConsultFeeBillingCurrency, PR.ReimbAllowBillingCurrency = C.ReimbAllowBillingCurrency,
				PR.FeeFunctionalCurrency = C.FeeFunctionalCurrency, PR.ConsultFeeFunctionalCurrency = C.ConsultFeeFunctionalCurrency, 
				PR.ReimbAllowFunctionalCurrency = C.ReimbAllowFunctionalCurrency,
				PR.FeeDirLab = C.FeeDirLab,
				PR.FeeDirExp = C.FeeDirExp, 
				PR.ReimbAllowExp = C.ReimbAllowExp,
				PR.ReimbAllowCons = C.ReimbAllowCons,
				PR.FeeDirLabBillingCurrency = C.FeeDirLabBillingCurrency,
				PR.FeeDirExpBillingCurrency = C.FeeDirExpBillingCurrency, 
				PR.ReimbAllowExpBillingCurrency = C.ReimbAllowExpBillingCurrency,
				PR.ReimbAllowConsBillingCurrency = C.ReimbAllowConsBillingCurrency,
				PR.FeeDirLabFunctionalCurrency = C.FeeDirLabFunctionalCurrency,
				PR.FeeDirExpFunctionalCurrency = C.FeeDirExpFunctionalCurrency, 
				PR.ReimbAllowExpFunctionalCurrency = C.ReimbAllowExpFunctionalCurrency,
				PR.ReimbAllowConsFunctionalCurrency = C.ReimbAllowConsFunctionalCurrency
			from (select CD.WBS1, CD.WBS2, Sum(CD.Fee) Fee, Sum(CD.ConsultFee) ConsultFee, Sum(CD.ReimbAllow) ReimbAllow,
				Sum(CD.FeeBillingCurrency) FeeBillingCurrency, Sum(CD.ConsultFeeBillingCurrency) ConsultFeeBillingCurrency, Sum(CD.ReimbAllowBillingCurrency) ReimbAllowBillingCurrency,
				Sum(CD.FeeFunctionalCurrency) FeeFunctionalCurrency, Sum(CD.ConsultFeeFunctionalCurrency) ConsultFeeFunctionalCurrency, 
				Sum(CD.ReimbAllowFunctionalCurrency) ReimbAllowFunctionalCurrency,
								Sum(CD.FeeDirLab) FeeDirLab, 
								Sum(CD.FeeDirExp) FeeDirExp, 
								Sum(CD.ReimbAllowExp) ReimbAllowExp,
								Sum(CD.ReimbAllowCons) ReimbAllowCons, 
								Sum(CD.FeeDirLabBillingCurrency) FeeDirLabBillingCurrency, 
								Sum(CD.FeeDirExpBillingCurrency) FeeDirExpBillingCurrency,
								Sum(CD.ReimbAllowExpBillingCurrency) ReimbAllowExpBillingCurrency, 
								Sum(CD.ReimbAllowConsBillingCurrency) ReimbAllowConsBillingCurrency, 
								Sum(CD.FeeDirLabFunctionalCurrency) FeeDirLabFunctionalCurrency,
								Sum(CD.FeeDirExpFunctionalCurrency) FeeDirExpFunctionalCurrency, 
								Sum(CD.ReimbAllowExpFunctionalCurrency) ReimbAllowExpFunctionalCurrency, 
								Sum(CD.ReimbAllowConsFunctionalCurrency) ReimbAllowConsFunctionalCurrency
			from Contracts C, ContractDetails CD 
			where C.ContractNumber = CD.ContractNumber and C.WBS1 = CD.WBS1  and C.FeeIncludeInd = N'Y'
				and CD.WBS1 = @WBS1 and  (CD.WBS2 <> N' ' and CD.WBS3 = N' ')   
			group by CD.WBS1, CD.WBS2)  C
			where C.WBS1 = PR.WBS1 and C.WBS2 = PR.WBS2 and  PR.WBS3 = N' '

			--StrSQLSyncFeesToContracts_ForTaskMC
			update PR
			set PR.Fee = C.Fee, PR.ConsultFee = C.ConsultFee, PR.ReimbAllow = C.ReimbAllow,
				PR.FeeBillingCurrency = C.FeeBillingCurrency, PR.ConsultFeeBillingCurrency = C.ConsultFeeBillingCurrency, PR.ReimbAllowBillingCurrency = C.ReimbAllowBillingCurrency,
				PR.FeeFunctionalCurrency = C.FeeFunctionalCurrency, PR.ConsultFeeFunctionalCurrency = C.ConsultFeeFunctionalCurrency, 
				PR.ReimbAllowFunctionalCurrency = C.ReimbAllowFunctionalCurrency,
				PR.FeeDirLab = C.FeeDirLab,
				PR.FeeDirExp = C.FeeDirExp, 
				PR.ReimbAllowExp = C.ReimbAllowExp,
				PR.ReimbAllowCons = C.ReimbAllowCons,
				PR.FeeDirLabBillingCurrency = C.FeeDirLabBillingCurrency,
				PR.FeeDirExpBillingCurrency = C.FeeDirExpBillingCurrency, 
				PR.ReimbAllowExpBillingCurrency = C.ReimbAllowExpBillingCurrency,
				PR.ReimbAllowConsBillingCurrency = C.ReimbAllowConsBillingCurrency,
				PR.FeeDirLabFunctionalCurrency = C.FeeDirLabFunctionalCurrency,
				PR.FeeDirExpFunctionalCurrency = C.FeeDirExpFunctionalCurrency, 
				PR.ReimbAllowExpFunctionalCurrency = C.ReimbAllowExpFunctionalCurrency,
				PR.ReimbAllowConsFunctionalCurrency = C.ReimbAllowConsFunctionalCurrency
			from (select CD.WBS1,WBS2,WBS3,Sum(CD.Fee) Fee, Sum(CD.ConsultFee) ConsultFee, Sum(CD.ReimbAllow) ReimbAllow,
											Sum(CD.FeeBillingCurrency) FeeBillingCurrency, Sum(CD.ConsultFeeBillingCurrency) ConsultFeeBillingCurrency, Sum(CD.ReimbAllowBillingCurrency) ReimbAllowBillingCurrency,
											Sum(CD.FeeFunctionalCurrency) FeeFunctionalCurrency, Sum(CD.ConsultFeeFunctionalCurrency) ConsultFeeFunctionalCurrency, 
								Sum(CD.ReimbAllowFunctionalCurrency) ReimbAllowFunctionalCurrency,
								Sum(CD.FeeDirLab) FeeDirLab, 
								Sum(CD.FeeDirExp) FeeDirExp, 
								Sum(CD.ReimbAllowExp) ReimbAllowExp,
								Sum(CD.ReimbAllowCons) ReimbAllowCons, 
								Sum(CD.FeeDirLabBillingCurrency) FeeDirLabBillingCurrency, 
								Sum(CD.FeeDirExpBillingCurrency) FeeDirExpBillingCurrency,
								Sum(CD.ReimbAllowExpBillingCurrency) ReimbAllowExpBillingCurrency, 
								Sum(CD.ReimbAllowConsBillingCurrency) ReimbAllowConsBillingCurrency, 
								Sum(CD.FeeDirLabFunctionalCurrency) FeeDirLabFunctionalCurrency,
								Sum(CD.FeeDirExpFunctionalCurrency) FeeDirExpFunctionalCurrency, 
								Sum(CD.ReimbAllowExpFunctionalCurrency) ReimbAllowExpFunctionalCurrency, 
								Sum(CD.ReimbAllowConsFunctionalCurrency) ReimbAllowConsFunctionalCurrency                 
			from Contracts C, ContractDetails CD 
			where C.ContractNumber = CD.ContractNumber  and C.WBS1 = CD.WBS1 and C.FeeIncludeInd = N'Y' and C.WBS1 = @WBS1 and not (CD.WBS2 = N' ' ) and  not ( CD.WBS3 = N' ')
			group by CD.WBS1,WBS2,WBS3)  C
			where C.WBS1 = PR.WBS1 and C.WBS2 = PR.WBS2 and C.WBS3 = PR.WBS3

			if @AutoSumComp = 'Y'
				begin
				--GetSyncFeesToContractsForProjectMCQuery
				update PR 
				set PR.Fee = C.Fee, PR.ConsultFee = C.ConsultFee, PR.ReimbAllow = C.ReimbAllow, 
					PR.FeeBillingCurrency = C.FeeBillingCurrency, PR.ConsultFeeBillingCurrency = C.ConsultFeeBillingCurrency, 
					PR.ReimbAllowBillingCurrency = C.ReimbAllowBillingCurrency, 
					PR.FeeFunctionalCurrency = C.FeeFunctionalCurrency,  
					PR.ConsultFeeFunctionalCurrency = C.ConsultFeeFunctionalCurrency, 
					PR.ReimbAllowFunctionalCurrency = C.ReimbAllowFunctionalCurrency, 
					PR.FeeDirLab = C.FeeDirLab, 
					PR.FeeDirExp = C.FeeDirExp, 
					PR.ReimbAllowExp = C.ReimbAllowExp, 
					PR.ReimbAllowCons = C.ReimbAllowCons, 
					PR.FeeDirLabBillingCurrency = C.FeeDirLabBillingCurrency, 
					PR.FeeDirExpBillingCurrency = C.FeeDirExpBillingCurrency, 
					PR.ReimbAllowExpBillingCurrency = C.ReimbAllowExpBillingCurrency, 
					PR.ReimbAllowConsBillingCurrency = C.ReimbAllowConsBillingCurrency, 
					PR.FeeDirLabFunctionalCurrency = C.FeeDirLabFunctionalCurrency, 
					PR.FeeDirExpFunctionalCurrency = C.FeeDirExpFunctionalCurrency, 
					PR.ReimbAllowExpFunctionalCurrency = C.ReimbAllowExpFunctionalCurrency, 
					PR.ReimbAllowConsFunctionalCurrency = C.ReimbAllowConsFunctionalCurrency 
				 from (select CD.WBS1,Sum(CD.Fee) Fee, Sum(CD.ConsultFee) ConsultFee, Sum(CD.ReimbAllow) ReimbAllow, 
											   Sum(CD.FeeBillingCurrency) FeeBillingCurrency, Sum(CD.ConsultFeeBillingCurrency) ConsultFeeBillingCurrency, Sum(CD.ReimbAllowBillingCurrency) ReimbAllowBillingCurrency, 
											   Sum(CD.FeeFunctionalCurrency) FeeFunctionalCurrency, Sum(CD.ConsultFeeFunctionalCurrency) ConsultFeeFunctionalCurrency, 
								 Sum(CD.ReimbAllowFunctionalCurrency) ReimbAllowFunctionalCurrency, 
								 Sum(CD.FeeDirLab) FeeDirLab, 
								 Sum(CD.FeeDirExp) FeeDirExp, 
								 Sum(CD.ReimbAllowExp) ReimbAllowExp, 
								 Sum(CD.ReimbAllowCons) ReimbAllowCons,  
								 Sum(CD.FeeDirLabBillingCurrency) FeeDirLabBillingCurrency,  
								 Sum(CD.FeeDirExpBillingCurrency) FeeDirExpBillingCurrency, 
								 Sum(CD.ReimbAllowExpBillingCurrency) ReimbAllowExpBillingCurrency,  
								 Sum(CD.ReimbAllowConsBillingCurrency) ReimbAllowConsBillingCurrency,  
								 Sum(CD.FeeDirLabFunctionalCurrency) FeeDirLabFunctionalCurrency, 
								 Sum(CD.FeeDirExpFunctionalCurrency) FeeDirExpFunctionalCurrency, 
								 Sum(CD.ReimbAllowExpFunctionalCurrency) ReimbAllowExpFunctionalCurrency, 
								 Sum(CD.ReimbAllowConsFunctionalCurrency) ReimbAllowConsFunctionalCurrency 
				from Contracts C, ContractDetails CD, PR 
				where C.ContractNumber = CD.ContractNumber and C.WBS1 = CD.WBS1  and C.FeeIncludeInd = N'Y' and PR.WBS1 = CD.WBS1 and PR.WBS2 = CD.WBS2 and PR.WBS3 = CD.WBS3 
				 and C.WBS1 = @WBS1  and  (CD.WBS2 = N' ' and CD.WBS3 = N' ')   
				group by CD.WBS1)  C
				where C.WBS1 = PR.WBS1 and PR.WBS2 = N' ' and PR.WBS3 = N' '

                EXECUTE dbo.UpdatePRFees @WBS1, 'N' 

				end
			else
				begin
				--StrSQLSyncFeesNoAutoSumup_ForProjectMC
				update PR
				set PR.Fee = C.Fee, PR.ConsultFee = C.ConsultFee, PR.ReimbAllow = C.ReimbAllow,
					PR.FeeBillingCurrency = C.FeeBillingCurrency, PR.ConsultFeeBillingCurrency = C.ConsultFeeBillingCurrency,
					PR.ReimbAllowBillingCurrency = C.ReimbAllowBillingCurrency,
					PR.FeeFunctionalCurrency = C.FeeFunctionalCurrency, 
					PR.ConsultFeeFunctionalCurrency = C.ConsultFeeFunctionalCurrency,
					PR.ReimbAllowFunctionalCurrency = C.ReimbAllowFunctionalCurrency,
					PR.FeeDirLab = C.FeeDirLab,
					PR.FeeDirExp = C.FeeDirExp, 
					PR.ReimbAllowExp = C.ReimbAllowExp,
					PR.ReimbAllowCons = C.ReimbAllowCons,
					PR.FeeDirLabBillingCurrency = C.FeeDirLabBillingCurrency,
					PR.FeeDirExpBillingCurrency = C.FeeDirExpBillingCurrency, 
					PR.ReimbAllowExpBillingCurrency = C.ReimbAllowExpBillingCurrency,
					PR.ReimbAllowConsBillingCurrency = C.ReimbAllowConsBillingCurrency,
					PR.FeeDirLabFunctionalCurrency = C.FeeDirLabFunctionalCurrency,
					PR.FeeDirExpFunctionalCurrency = C.FeeDirExpFunctionalCurrency, 
					PR.ReimbAllowExpFunctionalCurrency = C.ReimbAllowExpFunctionalCurrency,
					PR.ReimbAllowConsFunctionalCurrency = C.ReimbAllowConsFunctionalCurrency
				from (select CD.WBS1,Sum(CD.Fee) Fee, Sum(CD.ConsultFee) ConsultFee, Sum(CD.ReimbAllow) ReimbAllow,
												Sum(CD.FeeBillingCurrency) FeeBillingCurrency, Sum(CD.ConsultFeeBillingCurrency) ConsultFeeBillingCurrency, Sum(CD.ReimbAllowBillingCurrency) ReimbAllowBillingCurrency,
												Sum(CD.FeeFunctionalCurrency) FeeFunctionalCurrency, Sum(CD.ConsultFeeFunctionalCurrency) ConsultFeeFunctionalCurrency, Sum(CD.ReimbAllowFunctionalCurrency) ReimbAllowFunctionalCurrency,
									Sum(CD.FeeDirLab) FeeDirLab, 
									Sum(CD.FeeDirExp) FeeDirExp, 
									Sum(CD.ReimbAllowExp) ReimbAllowExp,
									Sum(CD.ReimbAllowCons) ReimbAllowCons, 
									Sum(CD.FeeDirLabBillingCurrency) FeeDirLabBillingCurrency, 
									Sum(CD.FeeDirExpBillingCurrency) FeeDirExpBillingCurrency,
									Sum(CD.ReimbAllowExpBillingCurrency) ReimbAllowExpBillingCurrency, 
									Sum(CD.ReimbAllowConsBillingCurrency) ReimbAllowConsBillingCurrency, 
									Sum(CD.FeeDirLabFunctionalCurrency) FeeDirLabFunctionalCurrency,
									Sum(CD.FeeDirExpFunctionalCurrency) FeeDirExpFunctionalCurrency, 
									Sum(CD.ReimbAllowExpFunctionalCurrency) ReimbAllowExpFunctionalCurrency, 
									Sum(CD.ReimbAllowConsFunctionalCurrency) ReimbAllowConsFunctionalCurrency
				from Contracts C, ContractDetails CD 
				where C.ContractNumber = CD.ContractNumber and C.WBS1 = CD.WBS1 and C.FeeIncludeInd = N'Y'
					and CD.WBS1 = @WBS1 and  ( CD.WBS2 = N' ' and CD.WBS3 = N' ' )
				group by CD.WBS1)  C
				where C.WBS1 = PR.WBS1  and  PR.WBS2 = N' ' and  PR.WBS3 = N' '

				end

			fetch next from WBS1Cursor into @WBS1
			set @WBSFetch = @@Fetch_Status
		end --while
		print 'Finish WBS1Cursor record'
		commit Transaction

		close WBS1Cursor
		deallocate WBS1Cursor		
	End

	set nocount off
	return(0)
End -- Procedure
GO
