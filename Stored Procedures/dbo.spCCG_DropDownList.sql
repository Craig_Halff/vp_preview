SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_DropDownList] @UDIC_UID varchar (32)
AS 
/* 
Copyright (c) 2021 Central Consulting Group.  All rights reserved.
09/27/2021	David Springer
			Insert new drop down list codes
			Call this from Drop Down List Control INSERT workflow
*/
DECLARE @GridID	varchar (255)
BEGIN

	If not exists 
		(Select 'x' 
		From UDIC_DropDownList c, FW_CustomColumnsData d
		Where c.UDIC_UID = @UDIC_UID
		  and c.CustHub = d.InfoCenterArea
		  and c.CustFieldName = d.Name
		  and d.DataType = 'dropdown')
		RAISERROR ('Field Name is not a custom drop down field.                                         ', 16, 1)

	Begin Transaction
	If exists 
		(Select 'x' 
		From UDIC_DropDownList c, FW_CustomColumnValuesData d
		Where c.UDIC_UID = @UDIC_UID
		  and c.CustHub = d.InfoCenterArea
		  and c.CustFieldName = d.ColName
		  and c.CustValue = d.DataValue)

		Update d
		Set d.Code = c.CustCode
		From FW_CustomColumnValuesData d, UDIC_DropDownList c
		Where c.UDIC_UID = @UDIC_UID
		  and d.InfoCenterArea = c.CustHub
		  and d.ColName = c.CustFieldName
		  and d.DataValue = c.CustValue

	Else
		Insert Into FW_CustomColumnValuesData
		(InfoCenterArea, GridID, ColName, Code, DataValue, Seq, UICultureName, CreateDate, CreateUser, ModDate, ModUser)
		Select CustHub, IsNull (d.GridID, 'X'), CustFieldName, CustCode, CustValue, IsNull (d.MaxSeq, 1), 'en-US', CreateDate, CreateUser, ModDate, ModUser
		From UDIC_DropDownList c
			Left Join
			(Select InfoCenterArea, ColName, Max (GridID) GridID, Max (Seq) + 1 MaxSeq
			From FW_CustomColumnValuesData
			Group by InfoCenterArea, ColName) d on d.InfoCenterArea = c.CustHub and d.ColName = c.CustFieldName
		Where UDIC_UID =  @UDIC_UID
	Commit Transaction

--	Update Seq, so the drop down list is in order
	Update a
	Set a.Seq = b.Seq
	From UDIC_DropDownList c, FW_CustomColumnValuesData a
		Join
		(Select InfoCenterArea, GridID, ColName, DataValue, ROW_NUMBER() over (partition by InfoCenterArea, GridID, ColName order by DataValue) Seq
		From FW_CustomColumnValuesData
		) b on b.InfoCenterArea = a.InfoCenterArea and b.GridID = a.GridID and b.ColName = a.ColName and b.DataValue = a.DataValue
	Where c.CustHub = a.InfoCenterArea
	  and c.CustFieldName = a.ColName

	Delete From UDIC_DropDownList Where UDIC_UID = @UDIC_UID

END
GO
