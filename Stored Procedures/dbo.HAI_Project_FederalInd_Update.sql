SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[HAI_Project_FederalInd_Update]
/* 
	Copyright (c) 2021 Halff Associates, Inc.  All rights reserved.
	5/14/2020	Craig H. Anderson

	Set The Federal Indicator on Project and Opportunities based on the Client Type.

	20210525	Craig H. Anderson
				Added WITH (NOLOCK) hint for read-only tables.
	20210715	David Springer revised for Vantagepoint
*/
AS
    BEGIN
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
        BEGIN TRANSACTION;

        UPDATE p
        SET p.FederalInd = IIF(c.Type = 'FEDGOV', 'Y', 'N')
        FROM dbo.PR p
           , dbo.Clendor c WITH (NOLOCK)
        WHERE
            p.ClientID = c.ClientID
            AND p.WBS2 = ' '
            AND p.FederalInd <> IIF(c.Type = 'FEDGOV', 'Y', 'N');
        COMMIT TRANSACTION;
    END;
GO
