SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE PROCEDURE [dbo].[CalcTimesheetPayRate] @activeCompany Nvarchar(14), @DorP Nvarchar(1), @strStartDate Nvarchar(80), @strEndDate Nvarchar(80), @EMWhere Nvarchar(max), @WBSWhere Nvarchar(max)
AS

DECLARE	@PayrollMult int,
	@OvtPctEnabled Nvarchar(1),
	@EnableRateTable Nvarchar(1)

DECLARE
	@bDone Nvarchar(1),
	@PayRateMeth int,
	@PayRateTableNo int,
	@PRLevel int,
	@LDStartDate datetime,
	@LDEndDate datetime,
	@LDStartPeriod int,
	@LDEndPeriod int,
	@CheckEMPayTable char(1),
	@MulticompanyEnabled char(1),
	@MulticurrencyEnabled char(1),
	@FunctionalCurrencyCode Nvarchar(3),
	@RateTableCurrencyCode Nvarchar(3),
	@blCheckRateTable char(1),
	@Org1Start smallint,
	@Org1Length smallint

DECLARE
	@DefaultPayOvtPct decimal(19,4),
	@DefaultPaySpecialOvtPct decimal(19,4),
	@Rate decimal(19,4),
	@OvtPct decimal(19,4),
	@Ovt2Pct decimal(19,4),
	@Payrate decimal(19,4), 
	@PayOvtPct decimal(19,4), 
	@PaySpecialOvtPct decimal(19,4)

DECLARE @WBS1 Nvarchar(30), 
	@WBS2 Nvarchar(7), 
	@WBS3 Nvarchar(7), 
	@LaborCode Nvarchar(20), 
	@Category int,
	@WBS1PayRateMeth int, 
	@WBS1PayRateTableNo int, 
	@WBS2PayRateMeth int, 
	@WBS2PayRateTableNo int, 
	@WBS3PayRateMeth int, 
	@WBS3PayRateTableNo int,
	@Employee Nvarchar(20), 
	@PayType Nvarchar(1), 
	@EM_PayRate decimal(19,4),
	@DefaultPayRate decimal(19,4),
	@EMPayRateMeth int,
	@EMPayRateTableNo int,
	@TransType Nvarchar(2), 
	@TransDate datetime, 
	@SelPeriod int, 
	@SelPostSeq int, 
	@SelOvtPeriod int, 
	@SelOvtPostSeq int, 
	@PKey varchar(32),
	@Period int,
	@PostSeq int,
	@RecordUpdated int

SELECT	@PayrollMult = CASE 
		WHEN PayrollFrequency = 'W' THEN 52 
		WHEN PayrollFrequency = 'B' THEN 26
		WHEN PayrollFrequency = 'S' THEN 24 
		WHEN PayrollFrequency = 'M' THEN 12
	END,
	@OvtPctEnabled = FW_CFGSystem.OvtPctEnabled, 
	@EnableRateTable = FW_CFGSystem.EnablePayRateTable,
	@FunctionalCurrencyCode = CFGMainData.FunctionalCurrencyCode,
	@MulticompanyEnabled = FW_CFGSystem.MulticompanyEnabled,
	@MulticurrencyEnabled = FW_CFGSystem.MulticurrencyEnabled,
	@Org1Start = CFGFormat.Org1Start,
	@Org1Length = CFGFormat.Org1Length
FROM	CFGPYMain, CFGMainData, FW_CFGSystem, CFGFormat
WHERE	CFGPYMain.Company = @activeCompany
	AND CFGMainData.Company = @activeCompany

IF @DorP = 'D'	-- date ranges
	SELECT	@LDStartDate = CONVERT(datetime, @strStartDate), @LDEndDate = CONVERT(datetime, @strEndDate)
ELSE	-- period ranges
	SELECT	@LDStartPeriod = CONVERT(int, @strStartDate), @LDEndPeriod = CONVERT(int, @strEndDate)

IF @EnableRateTable = 'Y'
BEGIN

IF @DorP = 'D'	-- date ranges
BEGIN
DECLARE LDCursor INSENSITIVE CURSOR FOR
SELECT
	LD.WBS1, LD.WBS2, LD.WBS3, LD.LaborCode, LD.Category, 
	PR1.PayRateMeth AS WBS1PayRateMeth, PR1.PayRateTableNo AS WBS1PayRateTableNo, 
	PR2.PayRateMeth AS WBS2PayRateMeth, PR2.PayRateTableNo AS WBS2PayRateTableNo, 
	PR3.PayRateMeth AS WBS3PayRateMeth, PR3.PayRateTableNo AS WBS3PayRateTableNo, 
	LD.Employee, EM.PayType, EM.PayRate AS EM_PayRate,
	DefaultPayRate = CASE WHEN EM.PayType = 'H' THEN EM.PayRate ELSE CONVERT(DECIMAL(19,4), EM.PayRate*@PayrollMult/2080) END,
	EM.PayOvtPct AS DefaultPayOvtPct, EM.PaySpecialOvtPct AS DefaultPaySpecialOvtPct, 
	EM.PayRateMeth AS EMPayRateMeth, EM.PayRateTableNo AS EMPayRateTableNo,
	LD.TransType, LD.TransDate, 
	LD.SelPeriod, LD.SelPostSeq, LD.SelOvtPeriod, LD.SelOvtPostSeq, 
	LD.PKey, LD.Period, LD.PostSeq
FROM
	LD,
	EMAllCompany EM,
	PR AS PR1,
	PR AS PR2,
	PR AS PR3
WHERE
	LD.TransType IN (N'TS',N'LA') AND LD.ProjectCost = 'Y' AND
	((LD.SelPeriod = 0 AND LD.SelPostSeq = 0) OR (LD.SelOvtPeriod = 0 AND LD.SelOvtPostSeq = 0)) AND
	(LD.TransDate BETWEEN @LDStartDate AND @LDEndDate) AND
	LD.Employee = EM.Employee AND 
	PR1.WBS2 = N' ' AND
	PR1.WBS3 = N' ' AND
	PR2.WBS3 = N' ' AND
	LD.WBS1 = PR1.WBS1 AND
	LD.WBS1 = PR2.WBS1 AND
	LD.WBS2 = PR2.WBS2 AND
	LD.WBS1 = PR3.WBS1 AND
	LD.WBS2 = PR3.WBS2 AND
	LD.WBS3 = PR3.WBS3
	AND ((@MulticompanyEnabled = 'Y' AND EM.EmployeeCompany = @activeCompany) OR @MulticompanyEnabled = 'N')
END
ELSE	-- period ranges
BEGIN
DECLARE LDCursor INSENSITIVE CURSOR FOR
SELECT
	LD.WBS1, LD.WBS2, LD.WBS3, LD.LaborCode, LD.Category, 
	PR1.PayRateMeth AS WBS1PayRateMeth, PR1.PayRateTableNo AS WBS1PayRateTableNo, 
	PR2.PayRateMeth AS WBS2PayRateMeth, PR2.PayRateTableNo AS WBS2PayRateTableNo, 
	PR3.PayRateMeth AS WBS3PayRateMeth, PR3.PayRateTableNo AS WBS3PayRateTableNo, 
	LD.Employee, EM.PayType, EM.PayRate AS EM_PayRate,
	DefaultPayRate = CASE WHEN EM.PayType = 'H' THEN EM.PayRate ELSE CONVERT(DECIMAL(19,4), EM.PayRate*@PayrollMult/2080) END,
	EM.PayOvtPct AS DefaultPayOvtPct, EM.PaySpecialOvtPct AS DefaultPaySpecialOvtPct, 
	EM.PayRateMeth AS EMPayRateMeth, EM.PayRateTableNo AS EMPayRateTableNo,
	LD.TransType, LD.TransDate, 
	LD.SelPeriod, LD.SelPostSeq, LD.SelOvtPeriod, LD.SelOvtPostSeq, 
	LD.PKey, LD.Period, LD.PostSeq
FROM
	LD,
	EMAllCompany EM,
	PR AS PR1,
	PR AS PR2,
	PR AS PR3
WHERE
	LD.TransType IN (N'TS',N'LA') AND LD.ProjectCost = 'Y' AND
	((LD.SelPeriod = 0 AND LD.SelPostSeq = 0) OR (LD.SelOvtPeriod = 0 AND LD.SelOvtPostSeq = 0)) AND
	(LD.Period BETWEEN @LDStartPeriod AND @LDEndPeriod) AND
	LD.Employee = EM.Employee AND 
	PR1.WBS2 = N' ' AND
	PR1.WBS3 = N' ' AND
	PR2.WBS3 = N' ' AND
	LD.WBS1 = PR1.WBS1 AND
	LD.WBS1 = PR2.WBS1 AND
	LD.WBS2 = PR2.WBS2 AND
	LD.WBS1 = PR3.WBS1 AND
	LD.WBS2 = PR3.WBS2 AND
	LD.WBS3 = PR3.WBS3
	AND ((@MulticompanyEnabled = 'Y' AND EM.EmployeeCompany = @activeCompany) OR @MulticompanyEnabled = 'N')
END

OPEN LDCursor
FETCH NEXT FROM LDCursor INTO @WBS1, @WBS2, @WBS3, @LaborCode, @Category, 
		@WBS1PayRateMeth, @WBS1PayRateTableNo, @WBS2PayRateMeth, @WBS2PayRateTableNo, @WBS3PayRateMeth, @WBS3PayRateTableNo, 
		@Employee, @PayType, @EM_PayRate,
		@DefaultPayRate, @DefaultPayOvtPct, @DefaultPaySpecialOvtPct, 
		@EMPayRateMeth, @EMPayRateTableNo,
		@TransType, @TransDate, 
		@SelPeriod, @SelPostSeq, @SelOvtPeriod, @SelOvtPostSeq, 
		@PKey, @Period, @PostSeq

WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @PRLevel = 3	-- assume at lowest level
	SELECT @CheckEMPayTable = 'N'
	IF @WBS2 = ' ' AND @WBS3 = ' '	-- highest level
		SELECT @PRLevel = 1
	ELSE
		IF @WBS3 = ' '
		    SELECT @PRLevel = 2

	SELECT @bDone = 'N'

	WHILE @bDone = 'N'
	BEGIN
		IF @CheckEMPayTable = 'N'
		BEGIN
			IF @PRLevel = 1
				SELECT @PayRateMeth = ISNULL(PayRateMeth,0), @PayRateTableNo = PayRateTableNo
				FROM PR WHERE WBS1 = @WBS1 AND WBS2 = N' ' AND WBS3 = N' '
	
			IF @PRLevel = 2
				SELECT @PayRateMeth = ISNULL(PayRateMeth,0), @PayRateTableNo = PayRateTableNo
				FROM PR WHERE WBS1 = @WBS1 AND WBS2 = @WBS2 AND WBS3 = N' '
	
			IF @PRLevel = 3
				SELECT @PayRateMeth = ISNULL(PayRateMeth,0), @PayRateTableNo = PayRateTableNo
				FROM PR WHERE WBS1 = @WBS1 AND WBS2 = @WBS2 AND WBS3 = @WBS3
		END
		ELSE
			SELECT @PayRateMeth = ISNULL(@EMPayRateMeth,0), @PayRateTableNo = @EMPayRateTableNo, @CheckEMPayTable = 'Y'

		IF @PayRateMeth = 0	-- None
			SELECT @bDone = 'N'
	

		IF @PayRateMeth = 1	-- From Labor Rate Table
		BEGIN
			SELECT @blCheckRateTable = 'Y'
                        IF @MulticurrencyEnabled = 'Y'
			BEGIN
                            SELECT @RateTableCurrencyCode = CurrencyCode FROM CostRT WHERE TableNo = @PayRateTableNo
                            SELECT @blCheckRateTable = CASE WHEN @RateTableCurrencyCode = @FunctionalCurrencyCode THEN 'Y' ELSE 'N' END
                        END
			IF @blCheckRateTable = 'Y'
			BEGIN
				SELECT TOP 1 @Rate = Rate, @OvtPct = OvtPct, @Ovt2Pct = Ovt2Pct FROM CostRTEmpls WHERE TableNo = @PayRateTableNo
				AND Employee = @Employee AND (@TransDate >= EffectiveDate OR EffectiveDate IS NULL)
				ORDER BY EffectiveDate DESC
	
				IF @@ROWCOUNT > 0
					SELECT	@bDone = 'Y', @PayRate = @Rate, 
						@PayOvtPct = CASE WHEN @OvtPctEnabled = 'Y' THEN @OvtPct ELSE @DefaultPayOvtPct END,
						@PaySpecialOvtPct = CASE WHEN @OvtPctEnabled = 'Y' THEN @Ovt2Pct ELSE @DefaultPaySpecialOvtPct END
			END
		END
	
		IF @PayRateMeth = 2	-- From Category Rate Table
		BEGIN
			SELECT @blCheckRateTable = 'Y'
                        IF @MulticurrencyEnabled = 'Y'
			BEGIN
                            SELECT @RateTableCurrencyCode = CurrencyCode FROM CostCT WHERE TableNo = @PayRateTableNo
                            SELECT @blCheckRateTable = CASE WHEN @RateTableCurrencyCode = @FunctionalCurrencyCode THEN 'Y' ELSE 'N' END
                        END
			IF @blCheckRateTable = 'Y'
			BEGIN
				SELECT TOP 1 @Rate = Rate, @OvtPct = OvtPct, @Ovt2Pct = Ovt2Pct FROM CostCTCats WHERE TableNo = @PayRateTableNo
				AND Category = @Category AND (@TransDate >= EffectiveDate OR EffectiveDate IS NULL)
				ORDER BY EffectiveDate DESC
	
				IF @@ROWCOUNT > 0
					SELECT	@bDone = 'Y', @PayRate = @Rate, 
						@PayOvtPct = CASE WHEN @OvtPctEnabled = 'Y' THEN @OvtPct ELSE @DefaultPayOvtPct END,
						@PaySpecialOvtPct = CASE WHEN @OvtPctEnabled = 'Y' THEN @Ovt2Pct ELSE @DefaultPaySpecialOvtPct END
			END
		END
	
		IF @PayRateMeth = 3	-- From Labor Code Table
		BEGIN
			SELECT @blCheckRateTable = 'Y'
                        IF @MulticurrencyEnabled = 'Y'
			BEGIN
                            SELECT @RateTableCurrencyCode = CurrencyCode FROM CostLT WHERE TableNo = @PayRateTableNo
                            SELECT @blCheckRateTable = CASE WHEN @RateTableCurrencyCode = @FunctionalCurrencyCode THEN 'Y' ELSE 'N' END
                        END
			IF @blCheckRateTable = 'Y'
			BEGIN
				SELECT TOP 1 @Rate = Rate, @OvtPct = OvtPct, @Ovt2Pct = Ovt2Pct FROM CostLTCodes WHERE TableNo = @PayRateTableNo
				AND @LaborCode LIKE LaborCodeMask
				AND (@TransDate >= EffectiveDate OR EffectiveDate IS NULL)
				ORDER BY EffectiveDate DESC, LaborCodeMask DESC
	
				IF @@ROWCOUNT > 0
					SELECT	@bDone = 'Y', @PayRate = @Rate, 
						@PayOvtPct = CASE WHEN @OvtPctEnabled = 'Y' THEN @OvtPct ELSE @DefaultPayOvtPct END,
						@PaySpecialOvtPct = CASE WHEN @OvtPctEnabled = 'Y' THEN @Ovt2Pct ELSE @DefaultPaySpecialOvtPct END
			END
		END

		IF @PayRateMeth = 4	-- Default Employee Payrate
			SELECT @bDone = 'Y', @PayRate = @DefaultPayRate, @PayOvtPct = @DefaultPayOvtPct, @PaySpecialOvtPct = @DefaultPaySpecialOvtPct

		IF @bDone = 'N'	-- No match, go up a level
		BEGIN
			IF @PRLevel = 1	-- at top level, check if employee has pay rate table assigned
				IF @CheckEMPayTable = 'N'
				BEGIN
					IF @EMPayRateMeth = 0	-- no, use employee default rate
						SELECT	@PayRate = @DefaultPayRate, @PayOvtPct = @DefaultPayOvtPct,
							@PaySpecialOvtPct = @DefaultPaySpecialOvtPct, @bDone = 'Y'
					ELSE -- check employee payrate table
						SELECT @CheckEMPayTable = 'Y'
				END
				ELSE	-- employee payrate table checked
				BEGIN
					IF @bDone = 'N'	-- No match in employee payrate table, use default
						SELECT	@PayRate = @DefaultPayRate, @PayOvtPct = @DefaultPayOvtPct, @PaySpecialOvtPct = @DefaultPaySpecialOvtPct
					SELECT @bDone = 'Y'
				END
			ELSE
				IF @PRLevel = 2
					SELECT @PRLevel = 1
				ELSE
					IF @PRLevel = 3
						SELECT @PRLevel = 2
		END
	END

	UPDATE LD SET PayRate = @PayRate, PayOvtPct = @PayOvtPct, PaySpecialOvtPct = @PaySpecialOvtPct
	WHERE PKey = @PKey AND Period = @Period AND PostSeq = @PostSeq

	FETCH NEXT FROM LDCursor INTO @WBS1, @WBS2, @WBS3, @LaborCode, @Category, 
			@WBS1PayRateMeth, @WBS1PayRateTableNo, @WBS2PayRateMeth, @WBS2PayRateTableNo, @WBS3PayRateMeth, @WBS3PayRateTableNo,
			@Employee, @PayType, @EM_PayRate,
			@DefaultPayRate, @DefaultPayOvtPct, @DefaultPaySpecialOvtPct, 
			@EMPayRateMeth, @EMPayRateTableNo,
			@TransType, @TransDate, 
			@SelPeriod, @SelPostSeq, @SelOvtPeriod, @SelOvtPostSeq, 
			@PKey, @Period, @PostSeq
END
CLOSE LDCursor
DEALLOCATE LDCursor

END
ELSE	-- EnableRateTable = 'N':  Rate table not enabled, just updated LD with default employee rates and overtime percentages
BEGIN
	IF @DorP = 'D'	-- date ranges
	BEGIN
		UPDATE	LD SET PayRate = (CASE WHEN EM.PayType = 'H' THEN EM.PayRate ELSE CONVERT(DECIMAL(19,4), EM.PayRate*@PayrollMult/2080) END),
			PayOvtPct = EM.PayOvtPct, PaySpecialOvtPct = EM.PaySpecialOvtPct
		FROM	LD JOIN EMAllCompany EM ON LD.Employee = EM.Employee
		WHERE	((LD.SelPeriod = 0 AND LD.SelPostSeq = 0) OR (LD.SelOvtPeriod = 0 AND LD.SelOvtPostSeq = 0)) AND
			LD.TransType IN (N'TS',N'LA') AND LD.ProjectCost = 'Y' AND
			(LD.TransDate BETWEEN @LDStartDate AND @LDEndDate)
			AND ((@MulticompanyEnabled = 'Y' AND EM.EmployeeCompany = @activeCompany) OR @MulticompanyEnabled = 'N')
	END
	ELSE
	BEGIN
		UPDATE	LD SET PayRate = (CASE WHEN EM.PayType = 'H' THEN EM.PayRate ELSE CONVERT(DECIMAL(19,4), EM.PayRate*@PayrollMult/2080) END),
			PayOvtPct = EM.PayOvtPct, PaySpecialOvtPct = EM.PaySpecialOvtPct
		FROM	LD JOIN EMAllCompany EM ON LD.Employee = EM.Employee
		WHERE	((LD.SelPeriod = 0 AND LD.SelPostSeq = 0) OR (LD.SelOvtPeriod = 0 AND LD.SelOvtPostSeq = 0)) AND
			LD.TransType IN (N'TS',N'LA') AND LD.ProjectCost = 'Y' AND
			(LD.Period BETWEEN @LDStartPeriod AND @LDEndPeriod)
			AND ((@MulticompanyEnabled = 'Y' AND EM.EmployeeCompany = @activeCompany) OR @MulticompanyEnabled = 'N')
	END
END

GO
