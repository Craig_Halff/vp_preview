SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spEVA__GetAppConfig](
	@app	varchar(50),
	@ccgPre	varchar(1) = null
)
AS BEGIN
	if @app not in ('ARM','EI','TR','PAT') return

	declare @sql nvarchar(max) = ''
	IF isnull(@ccgPre, '') = ''
	BEGIN
		IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'EVA_'+@app+'_Config') AND type in (N'U'))
			set @sql = N'select * from EVA_'+@app+'_Config'
		ELSE
			set @sql = N'select * from CCG_'+@app+'_Config'
	END
	ELSE BEGIN
		set @sql = (case when @ccgPre = 'Y' then N'select * from CCG_'+@app+'_Config' else N'select * from EVA_'+@app+'_Config' end)
	END
	exec (@sql)
END
GO
