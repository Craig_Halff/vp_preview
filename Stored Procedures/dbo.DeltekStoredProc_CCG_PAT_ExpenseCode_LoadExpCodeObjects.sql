SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_ExpenseCode_LoadExpCodeObjects]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SET NOCOUNT ON;

	SELECT e.type, e.Description, a.*
        FROM CFGAPExpenseCode e
            INNER JOIN CFGAPExpenseCodeAccounts a on a.Code = e.Code
        ORDER BY 3, 2
END;

GO
