SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
	Copyright (c) 2019 CCG, Inc.  All rights reserved.
	David Springer

	03/01/2021	Craig H. Anderson
				Set Unit to new unit for nearmap.
	10/21/2021	Jeremy Baummer
				Changed reference of HalffImport to VPTest
*/
CREATE PROCEDURE [dbo].[spCCG_ProjectCustNearmapLastChargeUpdate] @WBS1 VARCHAR (32)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE B
SET B.[CustNearmapLastCharge] = A.[TransDate]
FROM 
(
	SELECT [WBS1]
	,MAX([TransDate]) AS [TransDate]
	FROM [VPTest].[dbo].[LedgerMisc]
	WHERE WBS1 = @WBS1
	AND Unit LIKE '%XA01%'
	GROUP BY [WBS1]
) AS A
LEFT JOIN
(
	SELECT [WBS1]
	,[CustNearmapLastCharge]
	FROM [VPTest].[dbo].[ProjectCustomTabFields]
	WHERE WBS2 = '' AND WBS3 = ''
) AS B
ON A.WBS1 = B.WBS1
WHERE B.CustNearmapLastCharge IS NULL 
OR A.TransDate <> B.CustNearmapLastCharge

END
GO
