SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[HAI_TSR_UnbilledAgingAllOffices]
	@CompanyId varchar(2),
	@MaxTransDate varchar(25),	
	@BillingPeriod varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SET TRANSACTION isolation level READ uncommitted; 

IF EXISTS (SELECT NAME 
           FROM   sysobjects 
           WHERE  NAME = 'setContextInfo' 
                  AND type = 'P') 
  EXECUTE dbo.Setcontextinfo 
    @StrCompany = /*N*/@CompanyId, 
    @StrAuditingEnabled = 'N', 
    @strUserName = /*N*/'HIT', 
    @StrCultureName = 'en-US'; 

SELECT Isnull(Substring(pr.org, 4, 2), '')                             AS group1 
       , 
       Min(groupCFGOrgCodes2.label) 
       AS 
       groupDesc1, 
       Isnull(Substring(pr.org, 7, 3), '')                             AS group2 
       , 
       Min(groupCFGOrgCodes3.label) 
       AS groupDesc2, 
       Min(sortkey)                                                    AS 
       SortKey, 
       rectype, 
       Min(transtype)                                                  AS 
       transType, 
       Min(birptUnbilledUnion.wbs1)                                    AS wbs1, 
       Min(birptUnbilledUnion.wbs2)                                    AS wbs2, 
       Min(birptUnbilledUnion.wbs3)                                    AS wbs3, 
       CASE 
         WHEN ( billstatus = 'F' 
                AND Min(transtype) <> 'LA' ) THEN 'B*' 
         WHEN ( billstatus = 'F' 
                AND Min(transtype) = 'LA' ) THEN 'B* L' 
         WHEN ( billstatus <> 'F' 
                AND Min(transtype) = 'LA' ) THEN billstatus + '  L' 
         ELSE billstatus 
       END                                                             AS 
       billStatus, 
       CASE 
         WHEN ( ( billstatus = 'B' ) 
                 OR ( billstatus = 'F' ) ) THEN 1 
         WHEN ( billstatus = 'D' ) THEN 2 
         WHEN ( billstatus = 'H' ) THEN 3 
         WHEN ( billstatus = 'N' ) THEN 4 
         WHEN ( billstatus = 'W' ) THEN 5 
         ELSE 0 
       END                                                             AS 
       billStatusCode, 
       Min(transdate)                                                  AS 
       transDate, 
       Min(laborcode)                                                  AS 
       laborCode, 
       Min(categorydesc)                                               AS 
       Category, 
       Min(account)                                                    AS 
       account, 
       Min(birptUnbilledUnion.employee)                                AS 
       employee, 
       Min(refno)                                                      AS refNo, 
       CASE 
         WHEN ( rectype = '1' ) THEN 
           CASE 
             WHEN Isnull(Min(birptUnbilledUnion.desc1), '') <> '' THEN Rtrim( 
             Isnull(Min(birptUnbilledUnion.desc1), '')) 
             ELSE '' 
           END 
         WHEN ( rectype = '4' ) THEN 
           CASE 
             WHEN Isnull(Min(birptUnbilledUnion.desc1), '') <> '' THEN Rtrim( 
             Isnull(Min(birptUnbilledUnion.desc1), '')) 
             ELSE '' 
           END 
         ELSE CASE WHEN Isnull(Min(birptUnbilledUnion.desc1), '') <> '' THEN 
              Rtrim( 
              Isnull(Min( 
              birptUnbilledUnion.desc1), '')) + '  ' ELSE '' END + CASE WHEN 
              Isnull(Min 
              (desc2), '') <> '' THEN Isnull(Min(desc2), '') ELSE '' END 
       END                                                             AS 
       Description, 
       ''                                                              AS 
       Comment, 
       Sum(hoursqty)                                                   AS 
       hoursQty, 
       CASE 
         WHEN ( rectype = '1' 
                AND Min(birptUnbilledUnion.billrate) = 0 ) 
              AND ( Min(hoursqty) > 0 ) THEN Round( 
         Min(birptUnbilledUnion.billamount) / Min(hoursqty), Min(billingdecimals 
                                                             )) 
         ELSE Min(birptUnbilledUnion.billrate) 
       END                                                             AS 
       billRate, 
       Min(birptUnbilledUnion.billingdecimals)                         AS 
       BillingDecimals, 
       Min(unit)                                                       AS unit, 
       Min(pool)                                                       AS pool, 
       Min(category)                                                   AS 
       category, 
       Min(birptUnbilledUnion.vendor)                                  AS vendor 
       , 
       Round(Sum(birptUnbilledUnion.billamount), Min(billingdecimals)) 
       AS billAmount, 
       CASE 
         WHEN Min(transdate) IS NOT NULL THEN 
         Datediff(dd, Min(transdate), @MaxTransDate) 
         ELSE 0 
       END                                                             AS 
       daysOld, 
       Min(birptUnbilledUnion.pkey)                                    AS PKey, 
       0                                                               AS 
       jtdBill1, 
       0                                                               AS 
       jtdBill2, 
       0                                                               AS 
       jtdBill3, 
       Round(Min(birptUnbilledUnion.costrate), Min(billingdecimals))   AS 
       costRate, 
       Round(Sum(birptUnbilledUnion.costamount), Min(billingdecimals)) AS 
       costAmount, 
       Min(pr.sublevel)                                                AS 
       WBS1SubLevel, 
       Min(LEVEL2.sublevel)                                            AS 
       WBS2SubLevel, 
       Min(LEVEL3.sublevel)                                            AS 
       WBS3SubLevel, 
       Isnull(Min(WBS1PrinEM.firstname), '') + ' ' 
       + Isnull(Min(WBS1PrinEM.lastname), '')                          AS 
       WBS1Prin, 
       Isnull(Min(WBS2PrinEM.firstname), '') + ' ' 
       + Isnull(Min(WBS2PrinEM.lastname), '')                          AS 
       WBS2Prin, 
       Isnull(Min(WBS3PrinEM.firstname), '') + ' ' 
       + Isnull(Min(WBS3PrinEM.lastname), '')                          AS 
       WBS3Prin, 
       Isnull(Min(WBS1pmEM.firstname), '') + ' ' 
       + Isnull(Min(WBS1pmEM.lastname), '')                            AS WBS1PM 
       , 
       Isnull(Min(WBS2pmEM.firstname), '') + ' ' 
       + Isnull(Min(WBS2pmEM.lastname), '')                            AS WBS2PM 
       , 
       Isnull(Min(WBS3pmEM.firstname), '') + ' ' 
       + Isnull(Min(WBS3pmEM.lastname), '')                            AS WBS3PM 
       , 
       Min(WBS1BillCL.NAME) 
       AS WBS1Client, 
       Min(WBS2BillCL.NAME)                                            AS 
       WBS2Client, 
       Min(CLBill.NAME)                                                AS 
       WBS3Client, 
       Min(pr.revtype)                                                 AS 
       WBS1RevType, 
       Min(LEVEL2.revtype)                                             AS 
       WBS2RevType, 
       Min(LEVEL3.revtype)                                             AS 
       WBS3RevType, 
       ( Min(pr.fee) + Min(pr.reimballow) 
         + Min(pr.consultfee) )                                        AS 
       WBS1Comp, 
       ( Min(LEVEL2.fee) + Min(LEVEL2.reimballow) 
         + Min(LEVEL2.consultfee) )                                    AS 
       WBS2Comp, 
       ( Min(LEVEL3.fee) + Min(LEVEL3.reimballow) 
         + Min(LEVEL3.consultfee) )                                    AS 
       WBS3Comp, 
       CASE 
         WHEN Min(transdate) IS NOT NULL THEN 
           CASE 
             WHEN ( 0 = 0 
                    AND Datediff(dd, Min(transdate), @MaxTransDate) 
                        <= 30 ) 
                   OR ( Datediff(dd, Min(transdate), @MaxTransDate) 
                        >= 0 
                        AND Datediff(dd, Min(transdate), 
                            @MaxTransDate) <= 
                            30 ) 
           THEN ( CASE 
           WHEN 2 = 1 THEN Round(Sum(birptUnbilledUnion.costamount), Min( 
                           billingdecimals) 
                           ) 
           ELSE Round(Sum(birptUnbilledUnion.billamount), Min(billingdecimals)) 
           END ) 
             ELSE 0 
           END 
         ELSE 0 
       END                                                             AS 
       Age1Amount, 
       CASE 
         WHEN Min(transdate) IS NOT NULL THEN 
           CASE 
             WHEN ( 31 = 0 
                    AND Datediff(dd, Min(transdate), @MaxTransDate) 
                        <= 60 ) 
                   OR ( Datediff(dd, Min(transdate), @MaxTransDate) 
                        >= 31 
                        AND Datediff(dd, Min(transdate), 
                            @MaxTransDate) <= 
                            60 ) 
           THEN ( CASE 
           WHEN 2 = 1 THEN Round(Sum(birptUnbilledUnion.costamount), Min( 
                           billingdecimals) 
                           ) 
           ELSE Round(Sum(birptUnbilledUnion.billamount), Min(billingdecimals)) 
           END ) 
             ELSE 0 
           END 
         ELSE 0 
       END                                                             AS 
       Age2Amount, 
       CASE 
         WHEN Min(transdate) IS NOT NULL THEN 
           CASE 
             WHEN ( 61 = 0 
                    AND Datediff(dd, Min(transdate), @MaxTransDate) 
                        <= 90 ) 
                   OR ( Datediff(dd, Min(transdate), @MaxTransDate) 
                        >= 61 
                        AND Datediff(dd, Min(transdate), 
                            @MaxTransDate) <= 
                            90 ) 
           THEN ( CASE 
           WHEN 2 = 1 THEN Round(Sum(birptUnbilledUnion.costamount), Min( 
                           billingdecimals) 
                           ) 
           ELSE Round(Sum(birptUnbilledUnion.billamount), Min(billingdecimals)) 
           END ) 
             ELSE 0 
           END 
         ELSE 0 
       END                                                             AS 
       Age3Amount, 
       CASE 
         WHEN Min(transdate) IS NOT NULL THEN 
           CASE 
             WHEN ( 91 = 0 
                    AND Datediff(dd, Min(transdate), @MaxTransDate) 
                        <= 120 
                  ) 
                   OR ( Datediff(dd, Min(transdate), @MaxTransDate) 
                        >= 91 
                        AND Datediff(dd, Min(transdate), 
                            @MaxTransDate) <= 
                            120 
                      ) THEN ( CASE 
                                 WHEN 2 = 1 THEN Round(Sum( 
                                                 birptUnbilledUnion.costamount), 
                                                 Min( 
                                                 billingdecimals) 
                                                 ) 
                                 ELSE Round(Sum(birptUnbilledUnion.billamount), 
                                      Min( 
                                      billingdecimals)) 
                               END ) 
             ELSE 0 
           END 
         ELSE 0 
       END                                                             AS 
       Age4Amount, 
       CASE 
         WHEN Min(transdate) IS NOT NULL THEN 
           CASE 
             WHEN ( 121 = 0 
                    AND Datediff(dd, Min(transdate), @MaxTransDate) 
                        <= 150 
                  ) 
                   OR ( Datediff(dd, Min(transdate), @MaxTransDate) 
                        >= 121 
                        AND Datediff(dd, Min(transdate), 
                            @MaxTransDate) <= 
                            150 
                      ) THEN ( CASE 
                                 WHEN 2 = 1 THEN Round(Sum( 
                                                 birptUnbilledUnion.costamount), 
                                                 Min( 
                                                 billingdecimals) 
                                                 ) 
                                 ELSE Round(Sum(birptUnbilledUnion.billamount), 
                                      Min( 
                                      billingdecimals)) 
                               END ) 
             ELSE 0 
           END 
         ELSE 0 
       END                                                             AS 
       Age5Amount, 
       CASE 
         WHEN Min(transdate) IS NOT NULL THEN 
           CASE 
             WHEN ( 151 = 0 
                    AND Datediff(dd, Min(transdate), @MaxTransDate) 
                        <= 
                        999999 ) 
                   OR ( Datediff(dd, Min(transdate), @MaxTransDate) 
                        >= 151 
                        AND Datediff(dd, Min(transdate), 
                            @MaxTransDate) <= 
                            999999 ) THEN ( CASE 
                                              WHEN 2 = 1 THEN Round( 
                                              Sum( 
                                              birptUnbilledUnion.costamount), 
                                                              Min( 
                                              billingdecimals) 
                                              ) 
                                              ELSE 
             Round(Sum(birptUnbilledUnion.billamount), Min(billingdecimals)) 
                                            END ) 
             ELSE 0 
           END 
         ELSE 0 
       END                                                             AS 
       Age6Amount, 
       ''                                                              AS 
       currencyCodeBill, 
       ''                                                              AS 
       decimalPlacesBill, 
       ''                                                              AS 
       currencySymbolBill, 
       0                                                               AS 
       currencyCodeBillCount, 
       ''                                                              AS 
       currencyCodeCost, 
       ''                                                              AS 
       decimalPlacesCost, 
       ''                                                              AS 
       currencySymbolCost, 
       0                                                               AS 
       currencyCodeCostCount 
FROM   pr AS LEVEL3 
       INNER JOIN pr AS LEVEL2 
               ON ( LEVEL3.wbs1 = LEVEL2.wbs1 ) 
                  AND ( LEVEL3.wbs2 = LEVEL2.wbs2 ) 
       INNER JOIN pr 
               ON ( LEVEL3.wbs1 = pr.wbs1 ) 
       INNER JOIN (SELECT billCalcRateLD.pkey, 
                          LEFT(employeename + Space(40), 40) 
                          + laborcode + ' ' 
                          + CONVERT(VARCHAR, transdate, 112) AS SortKey, 
                          1                                  AS RecType, 
                          transtype, 
                          billCalcRateLD.wbs1, 
                          CASE 
                            WHEN ( 0 = 1 ) THEN ' ' 
                            ELSE billCalcRateLD.wbs2 
                          END                                AS WBS2, 
                          CASE 
                            WHEN ( 0 = 1 ) THEN ' ' 
                            ELSE billCalcRateLD.wbs3 
                          END                                AS WBS3, 
                          billingdecimals, 
                          projectdecimals, 
                          billstatus, 
                          transdate, 
                          laborcode, 
                          categorydesc, 
                          ''                                 AS Account, 
                          employee, 
                          ''                                 AS RefNo, 
                          employeename                       AS Desc1, 
                          ''                                 AS Desc2, 
                          hoursqty, 
                          ratecostcur                        AS CostRate, 
                          costamountcostcur                  AS CostAmount, 
                          ''                                 AS Unit, 
                          pool, 
                          category, 
                          ''                                 AS Vendor, 
                          comment, 
                          CASE 
                            WHEN labmeth = 0 THEN 0 
                            WHEN methodonebill = 1 THEN 
                              CASE 
                                WHEN ( separateovt = 0 
                                        OR ( ovthrs = 0 
                                             AND specialovthrs = 0 ) ) THEN 
                                ( hoursqty * baserate ) 
                                ELSE Round(reghrs * baserate, billingdecimals) 
                                     + CASE WHEN separateovt = 1 THEN Round( 
                                     ovthrs * 
                                     baserate * ( 
                                     ovtpct / 
                                     100) 
                                     , billingdecimals) + Round(specialovthrs * 
                                     baserate * ( 
                                     specialovtpct 
                                     / 
                                     100), billingdecimals) ELSE Round(ovthrs * 
                                     baserate * ovtmult 
                                     , 
                                     billingdecimals) + Round(specialovthrs * 
                                     baserate 
                                     * 
                                     specialovtmult, 
                                     billingdecimals) END 
                              END 
                            ELSE 
                              CASE 
                                WHEN ( separateovt = 0 
                                        OR ( ovthrs = 0 
                                             AND specialovthrs = 0 ) ) THEN 
                                Round(hoursqty * dbo.Unbilledroundedrate ( 
                                                 baserate, 
                                                 billingdecimals 
                                                 , 
                                                 labmeth, 
                                                 mult1, 
                                                 mult2, mult3 
                                                 ), billingdecimals) 
                                ELSE Round(reghrs * dbo.Unbilledroundedrate ( 
                                     baserate, 
                                     billingdecimals, 
                                     labmeth, 
                                     mult1, mult2, mult3), billingdecimals) 
                                     + CASE WHEN separateovt = 1 THEN Round( 
                                     ovthrs * 
                                     dbo.Unbilledroundedrate ( 
                                     baserate, billingdecimals, labmeth, mult1, 
                                     mult2, 
                                     mult3) * ( 
                                     ovtpct / 
                                     100) 
                                     , billingdecimals) + Round(specialovthrs * 
                                     dbo.Unbilledroundedrate ( 
                                     baserate, billingdecimals, labmeth, mult1, 
                                     mult2, 
                                     mult3) * ( 
                                     specialovtpct 
                                     / 100), billingdecimals) ELSE Round(ovthrs 
                                     * 
                                     dbo.Unbilledroundedrate 
                                     ( 
                                     baserate, billingdecimals, labmeth, mult1, 
                                     mult2, 
                                     mult3) * 
                                     ovtmult, 
                                     billingdecimals) + Round(specialovthrs * 
                                     dbo.Unbilledroundedrate ( 
                                     baserate, billingdecimals, labmeth, mult1, 
                                     mult2, 
                                     mult3) * 
                                     specialovtmult 
                                     , billingdecimals) END 
                              END 
                          END                                AS BillAmount, 
                          CASE 
                            WHEN ( methodonebill = 1 ) THEN 
                              CASE 
                                WHEN ( separateovt = 0 
                                        OR ( ovthrs = 0 
                                             AND specialovthrs = 0 ) ) 
                                     AND hoursqty <> 0 THEN baserate 
                                ELSE 0 
                              END 
                            ELSE 
                              CASE 
                                WHEN ( separateovt = 0 
                                        OR ( ovthrs = 0 
                                             AND specialovthrs = 0 ) ) 
                                     AND hoursqty <> 0 THEN 
                                dbo.Unbilledroundedrate (baserate, 
                                billingdecimals 
                                , labmeth, mult1, mult2, mult3 
                                ) 
                                ELSE 0 
                              END 
                          END                                AS BillRate, 
                          0                                  AS JTDBill1, 
                          0                                  AS JTDBill2, 
                          0                                  AS JTDBill3 
                   FROM   (SELECT ( billLDtoBT.reghrs + billLDtoBT.ovthrs 
                                    + billLDtoBT.specialovthrs ) AS HoursQty, 
                                  billLDtoBT.*, 
                                  CASE 
                                    WHEN Isnull(btrotempls.category, '') = '' 
                                  THEN 
                                    btlaborcats.description 
                                    ELSE (SELECT btlaborcats.description 
                                          FROM   btlaborcats 
                                          WHERE  btlaborcats.category = 
                                                 btrotempls.category) 
                                  END                            AS CategoryDesc 
                                  , 
btrrtempls.rate                AS RateTableRate, 
btrotempls.rate                AS OvRate, 
btrotempls.ratetype            AS OvRateType, 
btrotempls.category            AS OvCategory, 
btrotcats.rate                 AS ovMaxCostRate, 
btrotcats.category             AS ovMaxCostCategory, 
btrltcodes.rate                AS LaborCodeRate, 
btrctCatsQryEmp.rate           AS EmployeeCatRate, 
BTRCTCatsOT.rate               AS OverrideCatRate, 
btrctcats.rate                 AS CategoryRate, 
CASE 
  WHEN ( ( billLDtoBT.labmeth = 1 ) 
         AND Isnull(btrotempls.ratetype, ' ') = 'B' ) 
THEN 1 
  ELSE 0 
END                            AS MethodOneBill, 
CASE 
  WHEN laborcode LIKE '03' THEN 0 
  ELSE 
    CASE 
      WHEN billLDtoBT.labmeth = 1 THEN 
        CASE 
          WHEN btrotempls.rate IS NULL 
               AND NOT ( btrotcats.rate IS NOT NULL 
                         AND ( 
                   Isnull(billLDtoBT.rate, 0) > 
                   btrotcats.rate ) ) 
        THEN 
          Isnull(billLDtoBT.rate, 0) 
          WHEN btrotempls.rate IS NULL 
               AND ( btrotcats.rate IS NOT NULL 
                     AND ( Isnull(billLDtoBT.rate, 0) 
                           > 
                   btrotcats.rate ) ) THEN 
          btrotcats.rate 
          ELSE 
            CASE 
              WHEN ( btrotempls.ratetype = 'M' 
                     AND ( Isnull(billLDtoBT.rate, 0) 
                           < 
                         btrotempls.rate ) ) THEN 
              Isnull( 
              billLDtoBT.rate, 0) 
              ELSE btrotempls.rate 
            END 
        END 
      WHEN billLDtoBT.labmeth = 2 THEN 
        CASE 
          WHEN ( btrotempls.rate IS NOT NULL 
                 AND btrotempls.ratetype <> 'M' ) 
        THEN 
          btrotempls.rate 
          ELSE 
            CASE 
              WHEN btrrtempls.rate IS NOT NULL THEN 
              btrrtempls.rate 
              ELSE 0 
            END 
        END 
      WHEN billLDtoBT.labmeth = 3 THEN 
        CASE 
          WHEN btrotempls.rate = 0 
               AND btrotempls.category <> 0 
               AND BTRCTCatsRevert.rate IS NOT NULL 
        THEN 
          BTRCTCatsRevert.rate 
          WHEN ( btrotempls.rate IS NOT NULL 
                 AND btrotempls.ratetype <> 'M' ) 
               AND ( btrotempls.rate <> 0 
                      OR btrotempls.category = 0 ) 
        THEN 
          btrotempls.rate 
          ELSE 
            CASE 
              WHEN BTRCTCatsOT.rate IS NOT NULL THEN 
              BTRCTCatsOT.rate 
              ELSE 
                CASE 
                  WHEN btrctCatsQryEmp.rate IS NOT 
                       NULL THEN 
                  btrctCatsQryEmp.rate 
                  ELSE Isnull(btrctcats.rate, 0) 
                END 
            END 
        END 
      ELSE 
        CASE 
          WHEN ( btrotempls.rate IS NOT NULL 
                 AND btrotempls.ratetype <> 'M' ) 
        THEN 
          btrotempls.rate 
          ELSE 
            CASE 
              WHEN btrltcodes.rate IS NOT NULL THEN 
              btrltcodes.rate 
              ELSE 0 
            END 
        END 
    END 
END                            AS BaseRate 
FROM   (((((((SELECT ld.pkey, 
             ld.period, 
             ld.wbs1, 
             ld.wbs2, 
             ld.wbs3, 
             Isnull(ld.laborcode, '')              AS 
              LaborCode, 
             ld.transtype, 
             ld.billstatus, 
             ld.transdate, 
             ld.employee, 
             ld.NAME                               AS 
              EmployeeName, 
             ld.reghrs, 
             ld.ovthrs, 
             ld.specialovthrs, 
             ld.ovtpct, 
             ld.specialovtpct, 
             ld.ratebillingcurrency                AS 
              Rate, 
             ld.ratebillingcurrency                AS 
       RateCostCur 
              , 
             ( ld.regamtbillingcurrency 
               + ld.ovtamtbillingcurrency 
               + ld.specialovtamtbillingcurrency ) AS 
              CostAmountCostCur, 
             ( ld.regamtbillingcurrency 
               + ld.ovtamtbillingcurrency 
               + ld.specialovtamtbillingcurrency ) AS 
              CostAmount 
              , 
             ld.pool, 
             ld.category, 
             Isnull(ld.comment, '')                AS 
              Comment, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                   BTReg.labmeth 
                   ELSE BTOH.labmeth 
                 END 
               ELSE bt.labmeth 
             END                                   AS 
              LabMeth, 
             CASE 
               WHEN bt.expmeth IS NULL 
                     OR bt.expmeth = 0 THEN 
                 CASE 
                   WHEN LEVEL3.chargetype = 'R' THEN 
                   BTReg.expmeth 
                   ELSE BTOH.expmeth 
                 END 
               ELSE bt.expmeth 
             END                                   AS 
              ExpMeth, 
             CASE 
               WHEN bt.conmeth IS NULL 
                     OR bt.conmeth = 0 THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                   BTReg.conmeth 
                   ELSE BTOH.conmeth 
                 END 
               ELSE bt.conmeth 
             END                                   AS 
              ConMeth, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN LEVEL3.chargetype = 'R' THEN 
                   BTReg.ratetable 
                   ELSE BTOH.ratetable 
                 END 
               ELSE bt.ratetable 
             END                                   AS 
              RateTable, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                   BTReg.ortable 
                   ELSE BTOH.ortable 
                 END 
               ELSE bt.ortable 
             END                                   AS 
              ORTable, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN LEVEL3.chargetype = 'R' THEN 
                     CASE 
                       WHEN ( Isnull(BTReg.mult1, 0) 
                              = 0 ) THEN 
                       1 
                       ELSE BTReg.mult1 
                     END 
                   ELSE 
                     CASE 
                       WHEN ( Isnull(BTOH.mult1, 0) = 
                              0 ) THEN 1 
                       ELSE BTOH.mult1 
                     END 
                 END 
               ELSE 
                 CASE 
                   WHEN ( Isnull(bt.mult1, 0) = 0 ) 
                 THEN 1 
                   ELSE bt.mult1 
                 END 
             END                                   AS 
              Mult1, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                     CASE 
                       WHEN ( Isnull(BTReg.mult2, 0) 
                              = 0 ) THEN 
                       1 
                       ELSE BTReg.mult2 
                     END 
                   ELSE 
                     CASE 
                       WHEN ( Isnull(BTOH.mult2, 0) = 
                              0 ) THEN 1 
                       ELSE BTOH.mult2 
                     END 
                 END 
               ELSE 
                 CASE 
                   WHEN ( Isnull(bt.mult2, 0) = 0 ) 
                 THEN 1 
                   ELSE bt.mult2 
                 END 
             END                                   AS 
              Mult2, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN LEVEL3.chargetype = 'R' THEN 
                     CASE 
                       WHEN ( Isnull(BTReg.mult3, 0) 
                              = 0 ) THEN 
                       1 
                       ELSE BTReg.mult3 
                     END 
                   ELSE 
                     CASE 
                       WHEN ( Isnull(BTOH.mult3, 0) = 
                              0 ) THEN 1 
                       ELSE BTOH.mult3 
                     END 
                 END 
               ELSE 
                 CASE 
                   WHEN ( Isnull(bt.mult3, 0) = 0 ) 
                 THEN 1 
                   ELSE bt.mult3 
                 END 
             END                                   AS 
              Mult3, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                   BTReg.ovtmult 
                   ELSE BTOH.ovtmult 
                 END 
               ELSE bt.ovtmult 
             END                                   AS 
              OvtMult, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN LEVEL3.chargetype = 'R' THEN 
                   BTReg.specialovtmult 
                   ELSE BTOH.specialovtmult 
                 END 
               ELSE bt.specialovtmult 
             END                                   AS 
              SpecialOvtMult, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                   BTReg.separateovt 
                   ELSE BTOH.separateovt 
                 END 
               ELSE bt.separateovt 
             END                                   AS 
              SeparateOvt 
              , 
             CASE 
               WHEN ( bt.expmeth IS NULL 
                       OR bt.expmeth = 0 ) THEN 
                 CASE 
                   WHEN LEVEL3.chargetype = 'R' THEN 
                   BTReg.expmult 
                   ELSE BTOH. expmult 
                 END 
               ELSE bt.expmult 
             END                                   AS 
              ExpMult, 
             CASE 
               WHEN ( bt.expmeth IS NULL 
                       OR bt.expmeth = 0 ) THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                   BTReg.exptable 
                   ELSE BTOH.exptable 
                 END 
               ELSE bt.exptable 
             END                                   AS 
              ExpTable, 
             CASE 
               WHEN ( bt.conmeth IS NULL 
                       OR bt.conmeth = 0 ) THEN 
                 CASE 
                   WHEN LEVEL3.chargetype = 'R' THEN 
                   BTReg.conmult 
                   ELSE BTOH.conmult 
                 END 
               ELSE bt.conmult 
             END                                   AS 
              ConMult, 
             CASE 
               WHEN ( bt.conmeth IS NULL 
                       OR bt.conmeth = 0 ) THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                   BTReg.contable 
                   ELSE BTOH.contable 
                 END 
               ELSE bt.contable 
             END                                   AS 
              ConTable, 
             CASE 
               WHEN ( bt.unitmeth IS NULL 
                       OR bt.unitmeth = 0 ) THEN 
                 CASE 
                   WHEN LEVEL3.chargetype = 'R' THEN 
                   BTReg.unitmult 
                   ELSE BTOH.unitmult 
                 END 
               ELSE bt.unitmult 
             END                                   AS 
              UnitMult, 
             CASE 
               WHEN ( bt.unitmeth IS NULL 
                       OR bt.unitmeth = 0 ) THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                   BTReg.unitmeth 
                   ELSE BTOH.unitmeth 
                 END 
               ELSE bt.unitmeth 
             END                                   AS 
              UnitMeth, 
             CFGBillCurr.decimalplaces             AS 
              BillingDecimals, 
             CFGProjCurr.decimalplaces             AS 
              ProjectDecimals 
      FROM   btdefaults AS BTReg, 
             btdefaults AS BTOH, 
             pr AS LEVEL3 
             INNER JOIN pr AS LEVEL2 
                     ON ( LEVEL3.wbs1 = LEVEL2.wbs1 
                          AND 
             LEVEL3.wbs2 = LEVEL2.wbs2 ) 
             INNER JOIN pr 
                     ON ( LEVEL3.wbs1 = pr.wbs1 ) 
             LEFT JOIN bt 
                    ON ( LEVEL3.billwbs1 = bt.wbs1 
                         AND LEVEL3.billwbs2 = 
                             bt.wbs2 
                         AND LEVEL3.billwbs3 = 
                             bt.wbs3 ) 
             LEFT JOIN cl 
                    ON LEVEL3.clientid = cl.clientid 
             LEFT JOIN cl AS CLBill 
                    ON LEVEL3.billingclientid = 
                       CLBill.clientid 
             LEFT JOIN btbgsubs 
                    ON pr.wbs1 = btbgsubs.subwbs1 
             LEFT JOIN pr AS MainWBS1PR 
                    ON btbgsubs.mainwbs1 = 
                       MainWBS1PR.wbs1 
                       AND ' ' = MainWBS1PR.wbs2 
                       AND ' ' = MainWBS1PR.wbs3 
             LEFT JOIN fw_cfgcurrency AS CFGBillCurr 
                    ON pr.billingcurrencycode = 
                       CFGBillCurr.code 
             LEFT JOIN fw_cfgcurrency AS CFGProjCurr 
                    ON pr.projectcurrencycode = 
                       CFGProjCurr.code 
             INNER JOIN ld 
                     ON ( ld.wbs1 = LEVEL3.wbs1 
                          AND ld.wbs2 = LEVEL3.wbs2 
                          AND ld.wbs3 = LEVEL3.wbs3 ) 
      WHERE  ( ld.billstatus IN ( 'B', 'H', 'W', '' ) 
                OR ( 'B' = 'B' 
                     AND ld.billstatus = 'F' 
                     AND ld.billedperiod > @BillingPeriod ) 
             ) 
             AND ( ld.suppressbill = 'N' ) 
             AND ( ( Isnull(ld.laborcode, '') NOT 
                     LIKE '03' ) 
                    OR 1 = 1 ) 
             AND ( ld.period <= @BillingPeriod ) 
             AND ( ld.transdate <= 
                   @MaxTransDate ) 
             AND ( LEVEL3.sublevel = 'N' 
                    OR ( LEVEL3.wbs2 <> ' ' 
                         AND LEVEL3.wbs3 <> ' ' ) ) 
             AND pr.wbs2 = ' ' 
             AND LEVEL2.wbs3 = ' ' 
             AND BTReg.defaulttype = '<R>' 
             AND BTOH.defaulttype = '<O>' 
             AND BTReg.company = 
                 Substring(LEVEL3.org, 1, 2) 
             AND BTOH.company = Substring(LEVEL3.org, 
                                1, 2) 
             AND ( ( EXISTS (SELECT 'x' 
                             FROM   cfgchargetype 
                             WHERE 
                     cfgchargetype.type = 
                     pr.chargetype 
                     AND (( cfgchargetype.label = 
                            /*N*/ 
                            'Regular' 
                          ) 
                         ) 
                            ) 
                     AND ( Substring(pr.org, 1, 2) = 
                           /*N*/@CompanyId ) 
                   ) 
                   AND ( pr.status = 'A' ) ) 
             AND ( Substring(MainWBS1PR.org, 1, 2) = 
                   /*N*/@CompanyId 
                    OR ( btbgsubs.mainwbs1 IS NULL 
                         AND Substring(pr.org, 1, 2) 
                             = /*N*/@CompanyId 
                       ) ) 
      UNION ALL 
      SELECT bild.pkey, 
             bild.period, 
             bild.wbs1, 
             bild.wbs2, 
             bild.wbs3, 
             Isnull(bild.laborcode, '') 
             AS 
             LaborCode 
             , 
             bild.transtype, 
             bild.billstatus, 
             bild.transdate, 
             bild.employee, 
             bild.NAME 
             AS 
             EmployeeName, 
             bild.reghrs, 
             bild.ovthrs, 
             bild.specialovthrs, 
             bild.ovtpct, 
             bild.specialovtpct, 
             bild.ratebillingcurrency 
             AS Rate, 
             bild.ratebillingcurrency 
             AS 
             RateCostCur, 
             ( bild.regamtbillingcurrency 
               + bild.ovtamtbillingcurrency 
               + bild.specialovtamtbillingcurrency ) 
             AS 
             CostAmountCostCur, 
             ( bild.regamtbillingcurrency 
               + bild.ovtamtbillingcurrency 
               + bild.specialovtamtbillingcurrency ) 
             AS 
             CostAmount, 
             bild.pool, 
             bild.category, 
             Isnull(bild.comment, '') 
             AS Comment, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                   BTReg.labmeth 
                   ELSE BTOH.labmeth 
                 END 
               ELSE bt.labmeth 
             END 
             AS LabMeth, 
             CASE 
               WHEN bt.expmeth IS NULL 
                     OR bt.expmeth = 0 THEN 
                 CASE 
                   WHEN LEVEL3.chargetype = 'R' THEN 
                   BTReg.expmeth 
                   ELSE BTOH.expmeth 
                 END 
               ELSE bt.expmeth 
             END 
             AS ExpMeth, 
             CASE 
               WHEN bt.conmeth IS NULL 
                     OR bt.conmeth = 0 THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                   BTReg.conmeth 
                   ELSE BTOH.conmeth 
                 END 
               ELSE bt.conmeth 
             END 
             AS ConMeth, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN LEVEL3.chargetype = 'R' THEN 
                   BTReg.ratetable 
                   ELSE BTOH.ratetable 
                 END 
               ELSE bt.ratetable 
             END 
             AS 
             RateTable 
             , 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                   BTReg.ortable 
                   ELSE BTOH.ortable 
                 END 
               ELSE bt.ortable 
             END 
             AS ORTable, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN LEVEL3.chargetype = 'R' THEN 
                     CASE 
                       WHEN ( Isnull(BTReg.mult1, 0) 
                              = 0 ) THEN 
                       1 
                       ELSE BTReg.mult1 
                     END 
                   ELSE 
                     CASE 
                       WHEN ( Isnull(BTOH.mult1, 0) = 
                              0 ) THEN 1 
                       ELSE BTOH.mult1 
                     END 
                 END 
               ELSE 
                 CASE 
                   WHEN ( Isnull(bt.mult1, 0) = 0 ) 
                 THEN 1 
                   ELSE bt.mult1 
                 END 
             END 
             AS Mult1, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                     CASE 
                       WHEN ( Isnull(BTReg.mult2, 0) 
                              = 0 ) THEN 
                       1 
                       ELSE BTReg.mult2 
                     END 
                   ELSE 
                     CASE 
                       WHEN ( Isnull(BTOH.mult2, 0) = 
                              0 ) THEN 1 
                       ELSE BTOH.mult2 
                     END 
                 END 
               ELSE 
                 CASE 
                   WHEN ( Isnull(bt.mult2, 0) = 0 ) 
                 THEN 1 
                   ELSE bt.mult2 
                 END 
             END 
             AS Mult2, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN LEVEL3.chargetype = 'R' THEN 
                     CASE 
                       WHEN ( Isnull(BTReg.mult3, 0) 
                              = 0 ) THEN 
                       1 
                       ELSE BTReg.mult3 
                     END 
                   ELSE 
                     CASE 
                       WHEN ( Isnull(BTOH.mult3, 0) = 
                              0 ) THEN 1 
                       ELSE BTOH.mult3 
                     END 
                 END 
               ELSE 
                 CASE 
                   WHEN ( Isnull(bt.mult3, 0) = 0 ) 
                 THEN 1 
                   ELSE bt.mult3 
                 END 
             END 
             AS Mult3, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                   BTReg.ovtmult 
                   ELSE BTOH.ovtmult 
                 END 
               ELSE bt.ovtmult 
             END 
             AS OvtMult, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN LEVEL3.chargetype = 'R' THEN 
                   BTReg.specialovtmult 
                   ELSE BTOH.specialovtmult 
                 END 
               ELSE bt.specialovtmult 
             END 
             AS 
             SpecialOvtMult, 
             CASE 
               WHEN bt.labmeth IS NULL 
                     OR bt.labmeth = 0 THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                   BTReg.separateovt 
                   ELSE BTOH.separateovt 
                 END 
               ELSE bt.separateovt 
             END 
             AS 
             SeparateOvt, 
             CASE 
               WHEN ( bt.expmeth IS NULL 
                       OR bt.expmeth = 0 ) THEN 
                 CASE 
                   WHEN LEVEL3.chargetype = 'R' THEN 
                   BTReg.expmult 
                   ELSE BTOH. expmult 
                 END 
               ELSE bt.expmult 
             END 
             AS ExpMult, 
             CASE 
               WHEN ( bt.expmeth IS NULL 
                       OR bt.expmeth = 0 ) THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                   BTReg.exptable 
                   ELSE BTOH.exptable 
                 END 
               ELSE bt.exptable 
             END 
             AS ExpTable 
             , 
             CASE 
               WHEN ( bt.conmeth IS NULL 
                       OR bt.conmeth = 0 ) THEN 
                 CASE 
                   WHEN LEVEL3.chargetype = 'R' THEN 
                   BTReg.conmult 
                   ELSE BTOH.conmult 
                 END 
               ELSE bt.conmult 
             END 
             AS ConMult, 
             CASE 
               WHEN ( bt.conmeth IS NULL 
                       OR bt.conmeth = 0 ) THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                   BTReg.contable 
                   ELSE BTOH.contable 
                 END 
               ELSE bt.contable 
             END 
             AS ConTable 
             , 
             CASE 
               WHEN ( bt.unitmeth IS NULL 
                       OR bt.unitmeth = 0 ) THEN 
                 CASE 
                   WHEN LEVEL3.chargetype = 'R' THEN 
                   BTReg.unitmult 
                   ELSE BTOH.unitmult 
                 END 
               ELSE bt.unitmult 
             END 
             AS UnitMult 
             , 
             CASE 
               WHEN ( bt.unitmeth IS NULL 
                       OR bt.unitmeth = 0 ) THEN 
                 CASE 
                   WHEN pr.chargetype = 'R' THEN 
                   BTReg.unitmeth 
                   ELSE BTOH.unitmeth 
                 END 
               ELSE bt.unitmeth 
             END 
             AS UnitMeth 
             , 
CFGBillCurr.decimalplaces               AS 
BillingDecimals, 
CFGProjCurr.decimalplaces               AS 
ProjectDecimals 
FROM   btdefaults AS BTReg, 
btdefaults AS BTOH, 
pr AS LEVEL3 
INNER JOIN pr AS LEVEL2 
ON ( LEVEL3.wbs1 = LEVEL2.wbs1 
AND LEVEL3.wbs2 = LEVEL2.wbs2 ) 
INNER JOIN pr 
ON ( LEVEL3.wbs1 = pr.wbs1 ) 
LEFT JOIN bt 
ON ( LEVEL3.billwbs1 = bt.wbs1 
AND LEVEL3.billwbs2 = bt.wbs2 
AND LEVEL3.billwbs3 = bt.wbs3 ) 
LEFT JOIN cl 
ON LEVEL3.clientid = cl.clientid 
LEFT JOIN cl AS CLBill 
ON LEVEL3.billingclientid = CLBill.clientid 
LEFT JOIN btbgsubs 
ON pr.wbs1 = btbgsubs.subwbs1 
LEFT JOIN pr AS MainWBS1PR 
ON btbgsubs.mainwbs1 = MainWBS1PR.wbs1 
AND ' ' = MainWBS1PR.wbs2 
AND ' ' = MainWBS1PR.wbs3 
LEFT JOIN fw_cfgcurrency AS CFGBillCurr 
ON pr.billingcurrencycode = CFGBillCurr.code 
LEFT JOIN fw_cfgcurrency AS CFGProjCurr 
ON pr.projectcurrencycode = CFGProjCurr.code 
INNER JOIN bild 
ON ( bild.wbs1 = LEVEL3.wbs1 
AND bild.wbs2 = LEVEL3.wbs2 
AND bild.wbs3 = LEVEL3.wbs3 ) 
WHERE  ( bild.billstatus IN ( 'B', 'H', 'W', '' ) 
OR ( 'B' = 'B' 
AND bild.billstatus = 'F' 
AND bild.billedperiod > @BillingPeriod ) ) 
AND ( bild.suppressbill = 'N' ) 
AND ( ( Isnull(bild.laborcode, '') NOT LIKE '03' ) 
OR 1 = 1 ) 
AND ( bild.period <= @BillingPeriod ) 
AND ( bild.transdate <= @MaxTransDate ) 
AND ( LEVEL3.sublevel = 'N' 
OR ( LEVEL3.wbs2 <> ' ' 
AND LEVEL3.wbs3 <> ' ' ) ) 
AND pr.wbs2 = ' ' 
AND LEVEL2.wbs3 = ' ' 
AND BTReg.defaulttype = '<R>' 
AND BTOH.defaulttype = '<O>' 
AND BTReg.company = Substring(LEVEL3.org, 1, 2) 
AND BTOH.company = Substring(LEVEL3.org, 1, 2) 
AND ( ( EXISTS (SELECT 'x' 
FROM   cfgchargetype 
WHERE  cfgchargetype.type = 
       pr.chargetype 
       AND (( cfgchargetype.label = 
              /*N*/ 
              'Regular' 
            ) 
           ) 
) 
AND ( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
) 
AND ( pr.status = 'A' ) ) 
AND ( Substring(MainWBS1PR.org, 1, 2) = /*N*/@CompanyId 
OR ( btbgsubs.mainwbs1 IS NULL 
AND Substring(pr.org, 1, 2) = /*N*/@CompanyId 
) )) AS 
billLDtoBT 
LEFT JOIN btrrtempls 
ON ( billLDtoBT.transdate BETWEEN 
btrrtempls.startdate AND 
btrrtempls.enddate ) 
AND ( billLDtoBT.employee = btrrtempls.employee ) 
AND ( billLDtoBT.ratetable = 
btrrtempls.tableno )) 
LEFT JOIN btrotempls 
ON ( billLDtoBT.transdate BETWEEN 
btrotempls.startdate AND 
btrotempls.enddate ) 
AND ( billLDtoBT.employee = btrotempls.employee ) 
AND ( billLDtoBT.ortable = btrotempls.tableno )) 
LEFT JOIN btrotcats 
ON ( billLDtoBT.transdate BETWEEN 
btrotcats.startdate AND 
btrotcats.enddate ) 
AND ( billLDtoBT.category = btrotcats.category ) 
AND ( billLDtoBT.ortable = btrotcats.tableno )) 
LEFT JOIN btrltcodes 
ON ( billLDtoBT.transdate BETWEEN 
btrltcodes.startdate AND 
btrltcodes.enddate ) 
AND ( billLDtoBT.ratetable = btrltcodes.tableno ) 
AND ( billLDtoBT.laborcode LIKE 
btrltcodes.laborcodemask )) 
LEFT JOIN (SELECT btrctcats.tableno, 
btrctcats.category, 
btrctcats.rate, 
btrctcats.description, 
btrctcats.sortseq, 
btrctempls.employee, 
btrctcats.startdate, 
btrctcats.enddate 
FROM   btrctcats 
LEFT JOIN btrctempls 
ON ( btrctcats.tableno = 
   btrctempls.tableno ) 
 AND ( btrctcats.category = 
       btrctempls.category )) 
AS 
btrctCatsQryEmp 
ON ( billLDtoBT.transdate BETWEEN 
btrctCatsQryEmp.startdate AND 
btrctCatsQryEmp.enddate ) 
AND ( billLDtoBT.ratetable = btrctCatsQryEmp.tableno ) 
AND ( billLDtoBT.employee = btrctCatsQryEmp.employee )) 
LEFT JOIN btrctcats AS BTRCTCatsOT 
ON ( billLDtoBT.transdate BETWEEN BTRCTCatsOT.startdate AND 
BTRCTCatsOT.enddate ) 
AND ( btrotempls.category = BTRCTCatsOT.category ) 
AND ( btrotempls.tableno = BTRCTCatsOT.tableno )) 
LEFT JOIN btrctcats 
ON ( billLDtoBT.transdate BETWEEN btrctcats.startdate AND 
btrctcats.enddate ) 
AND ( billLDtoBT.category = btrctcats.category ) 
AND ( billLDtoBT.ratetable = btrctcats.tableno ) 
LEFT JOIN btrctcats AS BTRCTCatsRevert 
ON ( billLDtoBT.transdate BETWEEN BTRCTCatsRevert.startdate AND 
BTRCTCatsRevert.enddate ) 
AND ( billLDtoBT.ratetable = BTRCTCatsRevert.tableno ) 
AND ( billLDtoBT.ortable = btrotempls.tableno ) 
AND ( BTRCTCatsRevert.category = btrotempls.category ) 
LEFT JOIN btlaborcats 
ON billLDtoBT.category = btlaborcats.category) AS billCalcRateLD 
UNION ALL 
SELECT billCalcMultED.pkey, 
SortKey = CASE 
    WHEN transtype = 'UN' THEN 
    refno + unit + ' ' 
    + CONVERT(VARCHAR, transdate, 112 
    ) 
    ELSE account + ' ' + CONVERT(VARCHAR, transdate, 
         112) 
         + refno 
  END, 
CASE 
WHEN NOT( transtype = 'UN' ) 
AND ( reimbconsultant = 1 
      OR directconsultant = 1 ) THEN 2 
WHEN NOT( transtype = 'UN' ) 
AND NOT( reimbconsultant = 1 
         OR directconsultant = 1 ) THEN 3 
ELSE 4 
END                       AS RecType, 
transtype                 AS TransType, 
wbs1, 
CASE 
WHEN ( 0 = 1 ) THEN ' ' 
ELSE wbs2 
END                       AS WBS2, 
CASE 
WHEN ( 0 = 1 ) THEN ' ' 
ELSE wbs3 
END                       AS WBS3, 
billingdecimals, 
projectdecimals, 
billstatus                AS BillStatus, 
transdate, 
''                        AS LaborCode, 
''                        AS CategoryDesc, 
account                   AS Account, 
''                        AS Employee, 
refno, 
desc1                     AS Desc1, 
desc2                     AS Desc2, 
CASE 
WHEN transtype = 'UN' THEN unitquantity 
ELSE 0 
END                       AS HoursQty, 
CASE 
WHEN transtype = 'UN' THEN unitcostratecostcur 
ELSE 0 
END                       AS CostRate, 
CASE 
WHEN transtype = 'UN' THEN ( 
unitquantity * unitcostratecostcur ) 
ELSE amountcostcur 
END                       AS CostAmount, 
unit                      AS Unit, 
''                        AS Pool, 
''                        AS Category, 
vendor                    AS Vendor, 
''                        AS Comment, 
CASE 
WHEN ( transtype = 'UN' 
 AND subtype = 'B' ) THEN unitquantity 
WHEN ( transtype = 'UN' 
 AND ( subtype != 'B' 
        OR subtype IS NULL ) ) THEN 
unitquantity * unitcostrate 
ELSE amount 
END * Isnull(workmult, 0) AS BillAmount, 
CASE 
WHEN ( transtype = 'UN' 
 AND subtype = 'B' ) THEN Isnull(workmult, 0) 
WHEN ( transtype = 'UN' 
 AND subtype <> 'B' ) THEN 
unitcostrate * Isnull(workmult, 0) 
ELSE 0 
END                       AS BillRate, 
0                         AS JTDBill1, 
0                         AS JTDBill2, 
0                         AS JTDBill3 
FROM   (SELECT billEDtoBT.*, 
CASE 
  WHEN billEDtoBT.transtype = 'UN' 
       AND billEDtoBT.subtype = 'B' 
       AND Isnull(billEDtoBT.unitmeth, 0) = 1 
       AND Isnull(unitquantity, 0) <> 0 THEN ( 
  billEDtoBT.unitmult * unitbillingrate ) 
  WHEN NOT( billEDtoBT.transtype = 'UN' 
            AND billEDtoBT.subtype = 'B' ) 
       AND ( billEDtoBT.reimbconsultant = 1 
              OR billEDtoBT.directconsultant = 1 ) 
       AND ( billEDtoBT.conmeth <> 4 ) 
       AND ( Isnull(billEDtoBT.conmeth, 0) = 0 ) THEN 
  0 
  WHEN NOT( billEDtoBT.transtype = 'UN' 
            AND billEDtoBT.subtype = 'B' ) 
       AND ( billEDtoBT.reimbconsultant = 1 
              OR billEDtoBT.directconsultant = 1 ) 
       AND ( billEDtoBT.conmeth <> 4 ) 
       AND ( Isnull(billEDtoBT.conmeth, 0) = 1 ) 
       AND ( BTEAAcctsCons.multiplier IS NOT NULL ) 
THEN 
  BTEAAcctsCons.multiplier 
  WHEN NOT( billEDtoBT.transtype = 'UN' 
            AND billEDtoBT.subtype = 'B' ) 
       AND ( billEDtoBT.reimbconsultant = 1 
              OR billEDtoBT.directconsultant = 1 ) 
       AND ( billEDtoBT.conmeth <> 4 ) 
       AND ( Isnull(billEDtoBT.conmeth, 0) = 1 ) 
       AND ( BTEAAcctsCons.multiplier IS NULL ) 
       AND ( BTEAAcctsConDefault.multiplier IS NOT 
             NULL ) THEN 
  BTEAAcctsConDefault.multiplier 
  WHEN NOT( billEDtoBT.transtype = 'UN' 
            AND billEDtoBT.subtype = 'B' ) 
       AND ( billEDtoBT.reimbconsultant = 1 
              OR billEDtoBT.directconsultant = 1 ) 
       AND ( billEDtoBT.conmeth <> 4 ) 
       AND ( Isnull(billEDtoBT.conmeth, 0) = 1 ) 
       AND ( BTEAAcctsCons.multiplier IS NULL ) 
       AND ( BTEAAcctsConDefault.multiplier IS NULL ) 
       AND ( billEDtoBT.reimbconsultant = 1 ) THEN 
  billEDtoBT.conmult 
  WHEN NOT( billEDtoBT.transtype = 'UN' 
            AND billEDtoBT.subtype = 'B' ) 
       AND ( billEDtoBT.reimbconsultant = 1 
              OR billEDtoBT.directconsultant = 1 ) 
       AND ( billEDtoBT.conmeth <> 4 ) 
       AND ( Isnull(billEDtoBT.conmeth, 0) = 2 ) 
       AND ( btecCatsQryCons.multiplier IS NULL 
              OR btecCatsQryCons.multiplier = 0 ) 
       AND ( billEDtoBT.reimbconsultant = 1 ) THEN 
  billEDtoBT.conmult 
  WHEN NOT( billEDtoBT.transtype = 'UN' 
            AND billEDtoBT.subtype = 'B' ) 
       AND ( billEDtoBT.reimbconsultant = 1 
              OR billEDtoBT.directconsultant = 1 ) 
       AND ( billEDtoBT.conmeth <> 4 ) 
       AND ( Isnull(billEDtoBT.conmeth, 0) = 2 ) 
       AND NOT( btecCatsQryCons.multiplier IS NULL 
                 OR btecCatsQryCons.multiplier = 0 ) 
THEN 
  btecCatsQryCons.multiplier 
  WHEN NOT( billEDtoBT.transtype = 'UN' 
            AND billEDtoBT.subtype = 'B' ) 
       AND ( billEDtoBT.reimbconsultant = 1 
              OR billEDtoBT.directconsultant = 1 ) 
       AND ( billEDtoBT.conmeth <> 4 ) 
       AND ( Isnull(billEDtoBT.conmeth, 0) = 3 ) 
       AND ( billEDtoBT.reimbconsultant = 1 ) 
       AND ( BTEVVendsCons.multiplier IS NULL 
              OR BTEVVendsCons.multiplier = 0 ) THEN 
  billEDtoBT.conmult 
  WHEN NOT( billEDtoBT.transtype = 'UN' 
            AND billEDtoBT.subtype = 'B' ) 
       AND ( billEDtoBT.reimbconsultant = 1 
              OR billEDtoBT.directconsultant = 1 ) 
       AND ( billEDtoBT.conmeth <> 4 ) 
       AND ( Isnull(billEDtoBT.conmeth, 0) = 3 ) 
       AND ( billEDtoBT.reimbconsultant = 1 ) 
       AND NOT( BTEVVendsCons.multiplier IS NULL 
                 OR BTEVVendsCons.multiplier = 0 ) 
THEN 
  BTEVVendsCons.multiplier 
  WHEN NOT( billEDtoBT.transtype = 'UN' 
            AND billEDtoBT.subtype = 'B' ) 
       AND ( Isnull(billEDtoBT.expmeth, 0) = 1 ) 
       AND bteaaccts.multiplier IS NOT NULL THEN 
  bteaaccts.multiplier 
  WHEN NOT( billEDtoBT.transtype = 'UN' 
            AND billEDtoBT.subtype = 'B' ) 
       AND ( Isnull(billEDtoBT.expmeth, 0) = 1 ) 
       AND bteaaccts.multiplier IS NULL 
       AND BTEAAcctsExpDefault.multiplier IS NOT NULL 
THEN 
  BTEAAcctsExpDefault.multiplier 
  WHEN NOT( billEDtoBT.transtype = 'UN' 
            AND billEDtoBT.subtype = 'B' ) 
       AND ( Isnull(billEDtoBT.expmeth, 0) = 1 ) 
       AND bteaaccts.multiplier IS NULL 
       AND BTEAAcctsExpDefault.multiplier IS NULL 
       AND ( reimbursableacct = 1 ) THEN 
  billEDtoBT.expmult 
  WHEN NOT( billEDtoBT.transtype = 'UN' 
            AND billEDtoBT.subtype = 'B' ) 
       AND ( Isnull(billEDtoBT.expmeth, 0) = 2 ) 
       AND NOT( btecCatsQry.multiplier IS NULL 
                 OR btecCatsQry.multiplier = 0 ) THEN 
  btecCatsQry.multiplier 
  WHEN NOT( billEDtoBT.transtype = 'UN' 
            AND billEDtoBT.subtype = 'B' ) 
       AND ( Isnull(billEDtoBT.expmeth, 0) = 2 ) 
       AND ( btecCatsQry.multiplier IS NULL 
              OR btecCatsQry.multiplier = 0 ) 
       AND ( reimbursableacct = 1 ) THEN 
  billEDtoBT.expmult 
  WHEN NOT( billEDtoBT.transtype = 'UN' 
            AND billEDtoBT.subtype = 'B' ) 
       AND ( Isnull(billEDtoBT.expmeth, 0) = 3 ) 
       AND ( reimbursableacct = 1 ) 
       AND ( btevvends.multiplier IS NULL 
              OR btevvends.multiplier = 0 ) THEN 
  billEDtoBT.expmult 
  WHEN NOT( billEDtoBT.transtype = 'UN' 
            AND billEDtoBT.subtype = 'B' ) 
       AND ( Isnull(billEDtoBT.expmeth, 0) = 3 ) 
       AND ( reimbursableacct = 1 ) 
       AND NOT( btevvends.multiplier IS NULL 
                 OR btevvends.multiplier = 0 ) THEN 
  btevvends.multiplier 
  ELSE 0 
END AS WorkMult 
FROM   ((((((( (SELECT ledgermisc.pkey, 
              CASE 
                WHEN ca.type = 6 THEN 1 
                ELSE 0 
              END 
                            AS 
                               ReimbConsultant, 
              CASE 
                WHEN ca.type = 8 THEN 1 
                ELSE 0 
              END 
                            AS 
                               DirectConsultant, 
              CASE 
                WHEN ca.type >= 5 
                     AND ca.type < 7 THEN 1 
                ELSE 0 
              END 
                            AS 
                               ReimbursableAcct, 
              ledgermisc.period, 
              ledgermisc.wbs1, 
              ledgermisc.wbs2, 
              ledgermisc.wbs3, 
              ledgermisc.account, 
              ledgermisc.vendor, 
              ledgermisc.refno, 
              ledgermisc.transtype, 
              ledgermisc.subtype, 
              ledgermisc.billstatus, 
              ledgermisc.transdate, 
              ledgermisc.desc1, 
              ledgermisc.desc2, 
              ledgermisc.amountbillingcurrency 
                            AS 
                               Amount, 
              ledgermisc.amountbillingcurrency 
                            AS 
                               AmountCostCur, 
              ledgermisc.unit, 
              ledgermisc.unitquantity, 
              ledgermisc.unitcostratebillingcurrency 
                            AS 
                               UnitCostRate, 
              ledgermisc.unitcostratebillingcurrency 
                            AS 
                               UnitCostRateCostCur, 
              ledgermisc.unitbillingrate, 
              0 
                            AS 
                               GlobalTable, 
              ( ledgermisc.unitquantity * 
ledgermisc.unitcostratebillingcurrency ) 
AS 
UnitCostAmount, 
( ledgermisc.unitquantity * 
ledgermisc.unitcostratebillingcurrency ) 
AS 
UnitCostAmountCostCur, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.labmeth 
ELSE BTOH.labmeth 
END 
ELSE bt.labmeth 
END 
AS 
LabMeth, 
CASE 
WHEN bt.expmeth IS NULL 
OR bt.expmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.expmeth 
ELSE BTOH.expmeth 
END 
ELSE bt.expmeth 
END 
AS 
ExpMeth, 
CASE 
WHEN bt.conmeth IS NULL 
OR bt.conmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.conmeth 
ELSE BTOH.conmeth 
END 
ELSE bt.conmeth 
END 
AS 
ConMeth, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.ratetable 
ELSE BTOH.ratetable 
END 
ELSE bt.ratetable 
END 
AS 
RateTable, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.ortable 
ELSE BTOH.ortable 
END 
ELSE bt.ortable 
END 
AS 
ORTable, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
CASE 
WHEN ( Isnull(BTReg.mult1, 0) = 0 ) THEN 
1 
ELSE BTReg.mult1 
END 
ELSE 
CASE 
WHEN ( Isnull(BTOH.mult1, 0) = 0 ) THEN 1 
ELSE BTOH.mult1 
END 
END 
ELSE 
CASE 
WHEN ( Isnull(bt.mult1, 0) = 0 ) THEN 1 
ELSE bt.mult1 
END 
END 
AS 
Mult1, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN 
CASE 
WHEN ( Isnull(BTReg.mult2, 0) = 0 ) THEN 
1 
ELSE BTReg.mult2 
END 
ELSE 
CASE 
WHEN ( Isnull(BTOH.mult2, 0) = 0 ) THEN 1 
ELSE BTOH.mult2 
END 
END 
ELSE 
CASE 
WHEN ( Isnull(bt.mult2, 0) = 0 ) THEN 1 
ELSE bt.mult2 
END 
END 
AS 
Mult2, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
CASE 
WHEN ( Isnull(BTReg.mult3, 0) = 0 ) THEN 
1 
ELSE BTReg.mult3 
END 
ELSE 
CASE 
WHEN ( Isnull(BTOH.mult3, 0) = 0 ) THEN 1 
ELSE BTOH.mult3 
END 
END 
ELSE 
CASE 
WHEN ( Isnull(bt.mult3, 0) = 0 ) THEN 1 
ELSE bt.mult3 
END 
END 
AS 
Mult3, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.ovtmult 
ELSE BTOH.ovtmult 
END 
ELSE bt.ovtmult 
END 
AS 
OvtMult, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.specialovtmult 
ELSE BTOH.specialovtmult 
END 
ELSE bt.specialovtmult 
END 
AS 
SpecialOvtMult, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN 
BTReg.separateovt 
ELSE BTOH.separateovt 
END 
ELSE bt.separateovt 
END 
AS 
SeparateOvt, 
CASE 
WHEN ( bt.expmeth IS NULL 
OR bt.expmeth = 0 ) THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.expmult 
ELSE BTOH. expmult 
END 
ELSE bt.expmult 
END 
AS 
ExpMult, 
CASE 
WHEN ( bt.expmeth IS NULL 
OR bt.expmeth = 0 ) THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.exptable 
ELSE BTOH.exptable 
END 
ELSE bt.exptable 
END 
AS 
ExpTable, 
CASE 
WHEN ( bt.conmeth IS NULL 
OR bt.conmeth = 0 ) THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.conmult 
ELSE BTOH.conmult 
END 
ELSE bt.conmult 
END 
AS 
ConMult, 
CASE 
WHEN ( bt.conmeth IS NULL 
OR bt.conmeth = 0 ) THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.contable 
ELSE BTOH.contable 
END 
ELSE bt.contable 
END 
AS 
ConTable, 
CASE 
WHEN ( bt.unitmeth IS NULL 
OR bt.unitmeth = 0 ) THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.unitmult 
ELSE BTOH.unitmult 
END 
ELSE bt.unitmult 
END 
AS 
UnitMult, 
CASE 
WHEN ( bt.unitmeth IS NULL 
OR bt.unitmeth = 0 ) THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.unitmeth 
ELSE BTOH.unitmeth 
END 
ELSE bt.unitmeth 
END 
AS 
UnitMeth, 
CFGBillCurr.decimalplaces 
AS 
BillingDecimals, 
CFGProjCurr.decimalplaces 
AS 
ProjectDecimals 
FROM   btdefaults AS BTReg, 
btdefaults AS BTOH, 
pr AS LEVEL3 
INNER JOIN pr AS LEVEL2 
ON ( LEVEL3.wbs1 = LEVEL2.wbs1 
AND LEVEL3.wbs2 = LEVEL2.wbs2 ) 
INNER JOIN pr 
ON ( LEVEL3.wbs1 = pr.wbs1 ) 
LEFT JOIN bt 
ON ( LEVEL3.billwbs1 = bt.wbs1 
AND LEVEL3.billwbs2 = bt.wbs2 
AND LEVEL3.billwbs3 = bt.wbs3 ) 
LEFT JOIN cl 
ON LEVEL3.clientid = cl.clientid 
LEFT JOIN cl AS CLBill 
ON LEVEL3.billingclientid = CLBill.clientid 
LEFT JOIN btbgsubs 
ON pr.wbs1 = btbgsubs.subwbs1 
LEFT JOIN pr AS MainWBS1PR 
ON btbgsubs.mainwbs1 = MainWBS1PR.wbs1 
AND ' ' = MainWBS1PR.wbs2 
AND ' ' = MainWBS1PR.wbs3 
LEFT JOIN fw_cfgcurrency AS CFGBillCurr 
ON pr.billingcurrencycode = CFGBillCurr.code 
LEFT JOIN fw_cfgcurrency AS CFGProjCurr 
ON pr.projectcurrencycode = CFGProjCurr.code 
INNER JOIN ledgermisc 
ON ( ledgermisc.wbs1 = LEVEL3.wbs1 ) 
AND ( ledgermisc.wbs2 = LEVEL3.wbs2 ) 
AND ( ledgermisc.wbs3 = LEVEL3.wbs3 ) 
LEFT JOIN ca 
ON ( ledgermisc.account = ca.account ) 
WHERE  ( ledgermisc.billstatus IN ( 'B', 'H', 'W', '' ) 
OR ( 'B' = 'B' 
AND ledgermisc.billstatus = 'F' 
AND ledgermisc.billedperiod > @BillingPeriod ) ) 
AND ( ledgermisc.suppressbill = 'N' ) 
AND ( ledgermisc.projectcost = 'Y' ) 
AND ( ledgermisc.period <= @BillingPeriod ) 
AND ( ledgermisc.transdate <= 
@MaxTransDate ) 
AND ( LEVEL3.sublevel = 'N' 
OR ( LEVEL3.wbs2 <> ' ' 
AND LEVEL3.wbs3 <> ' ' ) ) 
AND pr.wbs2 = ' ' 
AND LEVEL2.wbs3 = ' ' 
AND BTReg.defaulttype = '<R>' 
AND BTOH.defaulttype = '<O>' 
AND BTReg.company = Substring(LEVEL3.org, 1, 2) 
AND BTOH.company = Substring(LEVEL3.org, 1, 2) 
AND ( ( EXISTS (SELECT 'x' 
FROM   cfgchargetype 
WHERE  cfgchargetype.type = 
pr.chargetype 
AND (( cfgchargetype.label = 
   /*N*/ 
   'Regular' 
 ) 
) 
) 
AND ( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
) 
AND ( pr.status = 'A' ) ) 
AND ( Substring(MainWBS1PR.org, 1, 2) = /*N*/@CompanyId 
OR ( btbgsubs.mainwbs1 IS NULL 
AND Substring(pr.org, 1, 2) = /*N*/@CompanyId 
) ) 
UNION ALL 
SELECT ledgerap.pkey, 
CASE 
WHEN ca.type = 6 THEN 1 
ELSE 0 
END 
AS 
ReimbConsultant, 
CASE 
WHEN ca.type = 8 THEN 1 
ELSE 0 
END 
AS 
DirectConsultant, 
CASE 
WHEN ca.type >= 5 
AND ca.type < 7 THEN 1 
ELSE 0 
END 
AS 
ReimbursableAcct, 
ledgerap.period, 
ledgerap.wbs1, 
ledgerap.wbs2, 
ledgerap.wbs3, 
ledgerap.account, 
ledgerap.vendor, 
ledgerap.refno, 
ledgerap.transtype, 
ledgerap.subtype, 
ledgerap.billstatus, 
ledgerap.transdate, 
ledgerap.desc1, 
ledgerap.desc2, 
ledgerap.amountbillingcurrency 
AS 
Amount, 
ledgerap.amountbillingcurrency 
AS 
AmountCostCur, 
ledgerap.unit, 
ledgerap.unitquantity, 
ledgerap.unitcostratebillingcurrency 
AS 
UnitCostRate, 
ledgerap.unitcostratebillingcurrency 
AS 
UnitCostRateCostCur, 
ledgerap.unitbillingrate, 
0 
AS 
GlobalTable, 
( ledgerap.unitquantity * 
ledgerap.unitcostratebillingcurrency ) 
AS 
UnitCostAmount, 
( ledgerap.unitquantity * 
ledgerap.unitcostratebillingcurrency ) 
AS 
UnitCostAmountCostCur, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.labmeth 
ELSE BTOH.labmeth 
END 
ELSE bt.labmeth 
END 
AS 
LabMeth, 
CASE 
WHEN bt.expmeth IS NULL 
OR bt.expmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.expmeth 
ELSE BTOH.expmeth 
END 
ELSE bt.expmeth 
END 
AS 
ExpMeth, 
CASE 
WHEN bt.conmeth IS NULL 
OR bt.conmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.conmeth 
ELSE BTOH.conmeth 
END 
ELSE bt.conmeth 
END 
AS 
ConMeth, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.ratetable 
ELSE BTOH.ratetable 
END 
ELSE bt.ratetable 
END 
AS 
RateTable, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.ortable 
ELSE BTOH.ortable 
END 
ELSE bt.ortable 
END 
AS 
ORTable, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
CASE 
WHEN ( Isnull(BTReg.mult1, 0) = 0 ) THEN 
1 
ELSE BTReg.mult1 
END 
ELSE 
CASE 
WHEN ( Isnull(BTOH.mult1, 0) = 0 ) THEN 1 
ELSE BTOH.mult1 
END 
END 
ELSE 
CASE 
WHEN ( Isnull(bt.mult1, 0) = 0 ) THEN 1 
ELSE bt.mult1 
END 
END 
AS 
Mult1 
, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN 
CASE 
WHEN ( Isnull(BTReg.mult2, 0) = 0 ) THEN 
1 
ELSE BTReg.mult2 
END 
ELSE 
CASE 
WHEN ( Isnull(BTOH.mult2, 0) = 0 ) THEN 1 
ELSE BTOH.mult2 
END 
END 
ELSE 
CASE 
WHEN ( Isnull(bt.mult2, 0) = 0 ) THEN 1 
ELSE bt.mult2 
END 
END 
AS 
Mult2 
, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
CASE 
WHEN ( Isnull(BTReg.mult3, 0) = 0 ) THEN 
1 
ELSE BTReg.mult3 
END 
ELSE 
CASE 
WHEN ( Isnull(BTOH.mult3, 0) = 0 ) THEN 1 
ELSE BTOH.mult3 
END 
END 
ELSE 
CASE 
WHEN ( Isnull(bt.mult3, 0) = 0 ) THEN 1 
ELSE bt.mult3 
END 
END 
AS 
Mult3 
, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.ovtmult 
ELSE BTOH.ovtmult 
END 
ELSE bt.ovtmult 
END 
AS 
OvtMult, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.specialovtmult 
ELSE BTOH.specialovtmult 
END 
ELSE bt.specialovtmult 
END 
AS 
SpecialOvtMult, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN 
BTReg.separateovt 
ELSE BTOH.separateovt 
END 
ELSE bt.separateovt 
END 
AS 
SeparateOvt, 
CASE 
WHEN ( bt.expmeth IS NULL 
OR bt.expmeth = 0 ) THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.expmult 
ELSE BTOH. expmult 
END 
ELSE bt.expmult 
END 
AS 
ExpMult, 
CASE 
WHEN ( bt.expmeth IS NULL 
OR bt.expmeth = 0 ) THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.exptable 
ELSE BTOH.exptable 
END 
ELSE bt.exptable 
END 
AS 
ExpTable, 
CASE 
WHEN ( bt.conmeth IS NULL 
OR bt.conmeth = 0 ) THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.conmult 
ELSE BTOH.conmult 
END 
ELSE bt.conmult 
END 
AS 
ConMult, 
CASE 
WHEN ( bt.conmeth IS NULL 
OR bt.conmeth = 0 ) THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.contable 
ELSE BTOH.contable 
END 
ELSE bt.contable 
END 
AS 
ConTable, 
CASE 
WHEN ( bt.unitmeth IS NULL 
OR bt.unitmeth = 0 ) THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.unitmult 
ELSE BTOH.unitmult 
END 
ELSE bt.unitmult 
END 
AS 
UnitMult, 
CASE 
WHEN ( bt.unitmeth IS NULL 
OR bt.unitmeth = 0 ) THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.unitmeth 
ELSE BTOH.unitmeth 
END 
ELSE bt.unitmeth 
END 
AS 
UnitMeth, 
CFGBillCurr.decimalplaces 
AS 
BillingDecimals, 
CFGProjCurr.decimalplaces 
AS 
ProjectDecimals 
FROM   btdefaults AS BTReg, 
btdefaults AS BTOH, 
pr AS LEVEL3 
INNER JOIN pr AS LEVEL2 
ON ( LEVEL3.wbs1 = LEVEL2.wbs1 
AND LEVEL3.wbs2 = LEVEL2.wbs2 ) 
INNER JOIN pr 
ON ( LEVEL3.wbs1 = pr.wbs1 ) 
LEFT JOIN bt 
ON ( LEVEL3.billwbs1 = bt.wbs1 
AND LEVEL3.billwbs2 = bt.wbs2 
AND LEVEL3.billwbs3 = bt.wbs3 ) 
LEFT JOIN cl 
ON LEVEL3.clientid = cl.clientid 
LEFT JOIN cl AS CLBill 
ON LEVEL3.billingclientid = CLBill.clientid 
LEFT JOIN btbgsubs 
ON pr.wbs1 = btbgsubs.subwbs1 
LEFT JOIN pr AS MainWBS1PR 
ON btbgsubs.mainwbs1 = MainWBS1PR.wbs1 
AND ' ' = MainWBS1PR.wbs2 
AND ' ' = MainWBS1PR.wbs3 
LEFT JOIN fw_cfgcurrency AS CFGBillCurr 
ON pr.billingcurrencycode = CFGBillCurr.code 
LEFT JOIN fw_cfgcurrency AS CFGProjCurr 
ON pr.projectcurrencycode = CFGProjCurr.code 
INNER JOIN ledgerap 
ON ( ledgerap.wbs1 = LEVEL3.wbs1 ) 
AND ( ledgerap.wbs2 = LEVEL3.wbs2 ) 
AND ( ledgerap.wbs3 = LEVEL3.wbs3 ) 
LEFT JOIN ca 
ON ( ledgerap.account = ca.account ) 
WHERE  ( ledgerap.billstatus IN ( 'B', 'H', 'W', '' ) 
OR ( 'B' = 'B' 
AND ledgerap.billstatus = 'F' 
AND ledgerap.billedperiod > @BillingPeriod ) ) 
AND ( ledgerap.suppressbill = 'N' ) 
AND ( ledgerap.projectcost = 'Y' ) 
AND ( ledgerap.period <= @BillingPeriod ) 
AND ( ledgerap.transdate <= 
@MaxTransDate ) 
AND ( LEVEL3.sublevel = 'N' 
OR ( LEVEL3.wbs2 <> ' ' 
AND LEVEL3.wbs3 <> ' ' ) ) 
AND pr.wbs2 = ' ' 
AND LEVEL2.wbs3 = ' ' 
AND BTReg.defaulttype = '<R>' 
AND BTOH.defaulttype = '<O>' 
AND BTReg.company = Substring(LEVEL3.org, 1, 2) 
AND BTOH.company = Substring(LEVEL3.org, 1, 2) 
AND ( ( EXISTS (SELECT 'x' 
FROM   cfgchargetype 
WHERE  cfgchargetype.type = 
pr.chargetype 
AND (( cfgchargetype.label = 
   /*N*/ 
   'Regular' 
 ) 
) 
) 
AND ( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
) 
AND ( pr.status = 'A' ) ) 
AND ( Substring(MainWBS1PR.org, 1, 2) = /*N*/@CompanyId 
OR ( btbgsubs.mainwbs1 IS NULL 
AND Substring(pr.org, 1, 2) = /*N*/@CompanyId 
) ) 
UNION ALL 
SELECT ledgerar.pkey, 
CASE 
WHEN ca.type = 6 THEN 1 
ELSE 0 
END 
AS 
ReimbConsultant, 
CASE 
WHEN ca.type = 8 THEN 1 
ELSE 0 
END 
AS 
DirectConsultant, 
CASE 
WHEN ca.type >= 5 
AND ca.type < 7 THEN 1 
ELSE 0 
END 
AS 
ReimbursableAcct, 
ledgerar.period, 
ledgerar.wbs1, 
ledgerar.wbs2, 
ledgerar.wbs3, 
ledgerar.account, 
ledgerar.vendor, 
ledgerar.refno, 
ledgerar.transtype, 
ledgerar.subtype, 
ledgerar.billstatus, 
ledgerar.transdate, 
ledgerar.desc1, 
ledgerar.desc2, 
ledgerar.amountbillingcurrency 
AS 
Amount, 
ledgerar.amountbillingcurrency 
AS 
AmountCostCur, 
ledgerar.unit, 
ledgerar.unitquantity, 
ledgerar.unitcostratebillingcurrency 
AS 
UnitCostRate, 
ledgerar.unitcostratebillingcurrency 
AS 
UnitCostRateCostCur, 
ledgerar.unitbillingrate, 
0 
AS 
GlobalTable, 
( ledgerar.unitquantity * 
ledgerar.unitcostratebillingcurrency ) 
AS 
UnitCostAmount, 
( ledgerar.unitquantity * 
ledgerar.unitcostratebillingcurrency ) 
AS 
UnitCostAmountCostCur, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.labmeth 
ELSE BTOH.labmeth 
END 
ELSE bt.labmeth 
END 
AS 
LabMeth, 
CASE 
WHEN bt.expmeth IS NULL 
OR bt.expmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.expmeth 
ELSE BTOH.expmeth 
END 
ELSE bt.expmeth 
END 
AS 
ExpMeth, 
CASE 
WHEN bt.conmeth IS NULL 
OR bt.conmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.conmeth 
ELSE BTOH.conmeth 
END 
ELSE bt.conmeth 
END 
AS 
ConMeth, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.ratetable 
ELSE BTOH.ratetable 
END 
ELSE bt.ratetable 
END 
AS 
RateTable, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.ortable 
ELSE BTOH.ortable 
END 
ELSE bt.ortable 
END 
AS 
ORTable, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
CASE 
WHEN ( Isnull(BTReg.mult1, 0) = 0 ) THEN 
1 
ELSE BTReg.mult1 
END 
ELSE 
CASE 
WHEN ( Isnull(BTOH.mult1, 0) = 0 ) THEN 1 
ELSE BTOH.mult1 
END 
END 
ELSE 
CASE 
WHEN ( Isnull(bt.mult1, 0) = 0 ) THEN 1 
ELSE bt.mult1 
END 
END 
AS 
Mult1 
, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN 
CASE 
WHEN ( Isnull(BTReg.mult2, 0) = 0 ) THEN 
1 
ELSE BTReg.mult2 
END 
ELSE 
CASE 
WHEN ( Isnull(BTOH.mult2, 0) = 0 ) THEN 1 
ELSE BTOH.mult2 
END 
END 
ELSE 
CASE 
WHEN ( Isnull(bt.mult2, 0) = 0 ) THEN 1 
ELSE bt.mult2 
END 
END 
AS 
Mult2 
, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
CASE 
WHEN ( Isnull(BTReg.mult3, 0) = 0 ) THEN 
1 
ELSE BTReg.mult3 
END 
ELSE 
CASE 
WHEN ( Isnull(BTOH.mult3, 0) = 0 ) THEN 1 
ELSE BTOH.mult3 
END 
END 
ELSE 
CASE 
WHEN ( Isnull(bt.mult3, 0) = 0 ) THEN 1 
ELSE bt.mult3 
END 
END 
AS 
Mult3 
, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.ovtmult 
ELSE BTOH.ovtmult 
END 
ELSE bt.ovtmult 
END 
AS 
OvtMult, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.specialovtmult 
ELSE BTOH.specialovtmult 
END 
ELSE bt.specialovtmult 
END 
AS 
SpecialOvtMult, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN 
BTReg.separateovt 
ELSE BTOH.separateovt 
END 
ELSE bt.separateovt 
END 
AS 
SeparateOvt, 
CASE 
WHEN ( bt.expmeth IS NULL 
OR bt.expmeth = 0 ) THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.expmult 
ELSE BTOH. expmult 
END 
ELSE bt.expmult 
END 
AS 
ExpMult, 
CASE 
WHEN ( bt.expmeth IS NULL 
OR bt.expmeth = 0 ) THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.exptable 
ELSE BTOH.exptable 
END 
ELSE bt.exptable 
END 
AS 
ExpTable, 
CASE 
WHEN ( bt.conmeth IS NULL 
OR bt.conmeth = 0 ) THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.conmult 
ELSE BTOH.conmult 
END 
ELSE bt.conmult 
END 
AS 
ConMult, 
CASE 
WHEN ( bt.conmeth IS NULL 
OR bt.conmeth = 0 ) THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.contable 
ELSE BTOH.contable 
END 
ELSE bt.contable 
END 
AS 
ConTable, 
CASE 
WHEN ( bt.unitmeth IS NULL 
OR bt.unitmeth = 0 ) THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.unitmult 
ELSE BTOH.unitmult 
END 
ELSE bt.unitmult 
END 
AS 
UnitMult, 
CASE 
WHEN ( bt.unitmeth IS NULL 
OR bt.unitmeth = 0 ) THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.unitmeth 
ELSE BTOH.unitmeth 
END 
ELSE bt.unitmeth 
END 
AS 
UnitMeth, 
CFGBillCurr.decimalplaces 
AS 
BillingDecimals, 
CFGProjCurr.decimalplaces 
AS 
ProjectDecimals 
FROM   btdefaults AS BTReg, 
btdefaults AS BTOH, 
pr AS LEVEL3 
INNER JOIN pr AS LEVEL2 
ON ( LEVEL3.wbs1 = LEVEL2.wbs1 
AND LEVEL3.wbs2 = LEVEL2.wbs2 ) 
INNER JOIN pr 
ON ( LEVEL3.wbs1 = pr.wbs1 ) 
LEFT JOIN bt 
ON ( LEVEL3.billwbs1 = bt.wbs1 
AND LEVEL3.billwbs2 = bt.wbs2 
AND LEVEL3.billwbs3 = bt.wbs3 ) 
LEFT JOIN cl 
ON LEVEL3.clientid = cl.clientid 
LEFT JOIN cl AS CLBill 
ON LEVEL3.billingclientid = CLBill.clientid 
LEFT JOIN btbgsubs 
ON pr.wbs1 = btbgsubs.subwbs1 
LEFT JOIN pr AS MainWBS1PR 
ON btbgsubs.mainwbs1 = MainWBS1PR.wbs1 
AND ' ' = MainWBS1PR.wbs2 
AND ' ' = MainWBS1PR.wbs3 
LEFT JOIN fw_cfgcurrency AS CFGBillCurr 
ON pr.billingcurrencycode = CFGBillCurr.code 
LEFT JOIN fw_cfgcurrency AS CFGProjCurr 
ON pr.projectcurrencycode = CFGProjCurr.code 
INNER JOIN ledgerar 
ON ( ledgerar.wbs1 = LEVEL3.wbs1 ) 
AND ( ledgerar.wbs2 = LEVEL3.wbs2 ) 
AND ( ledgerar.wbs3 = LEVEL3.wbs3 ) 
LEFT JOIN ca 
ON ( ledgerar.account = ca.account ) 
WHERE  ( ledgerar.billstatus IN ( 'B', 'H', 'W', '' ) 
OR ( 'B' = 'B' 
AND ledgerar.billstatus = 'F' 
AND ledgerar.billedperiod > @BillingPeriod ) ) 
AND ( ledgerar.suppressbill = 'N' ) 
AND ( ledgerar.projectcost = 'Y' ) 
AND ( ledgerar.period <= @BillingPeriod ) 
AND ( ledgerar.transdate <= 
@MaxTransDate ) 
AND ( LEVEL3.sublevel = 'N' 
OR ( LEVEL3.wbs2 <> ' ' 
AND LEVEL3.wbs3 <> ' ' ) ) 
AND pr.wbs2 = ' ' 
AND LEVEL2.wbs3 = ' ' 
AND BTReg.defaulttype = '<R>' 
AND BTOH.defaulttype = '<O>' 
AND BTReg.company = Substring(LEVEL3.org, 1, 2) 
AND BTOH.company = Substring(LEVEL3.org, 1, 2) 
AND ( ( EXISTS (SELECT 'x' 
FROM   cfgchargetype 
WHERE  cfgchargetype.type = 
pr.chargetype 
AND (( cfgchargetype.label = 
   /*N*/ 
   'Regular' 
 ) 
) 
) 
AND ( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
) 
AND ( pr.status = 'A' ) ) 
AND ( Substring(MainWBS1PR.org, 1, 2) = /*N*/@CompanyId 
OR ( btbgsubs.mainwbs1 IS NULL 
AND Substring(pr.org, 1, 2) = /*N*/@CompanyId 
) ) 
UNION ALL 
SELECT ledgerex.pkey, 
CASE 
WHEN ca.type = 6 THEN 1 
ELSE 0 
END 
AS 
ReimbConsultant, 
CASE 
WHEN ca.type = 8 THEN 1 
ELSE 0 
END 
AS 
DirectConsultant, 
CASE 
WHEN ca.type >= 5 
AND ca.type < 7 THEN 1 
ELSE 0 
END 
AS 
ReimbursableAcct, 
ledgerex.period, 
ledgerex.wbs1, 
ledgerex.wbs2, 
ledgerex.wbs3, 
ledgerex.account, 
ledgerex.vendor, 
ledgerex.refno, 
ledgerex.transtype, 
ledgerex.subtype, 
ledgerex.billstatus, 
ledgerex.transdate, 
ledgerex.desc1, 
ledgerex.desc2, 
ledgerex.amountbillingcurrency 
AS 
Amount, 
ledgerex.amountbillingcurrency 
AS 
AmountCostCur, 
ledgerex.unit, 
ledgerex.unitquantity, 
ledgerex.unitcostratebillingcurrency 
AS 
UnitCostRate, 
ledgerex.unitcostratebillingcurrency 
AS 
UnitCostRateCostCur, 
ledgerex.unitbillingrate, 
0 
AS 
GlobalTable, 
( ledgerex.unitquantity * 
ledgerex.unitcostratebillingcurrency ) 
AS 
UnitCostAmount, 
( ledgerex.unitquantity * 
ledgerex.unitcostratebillingcurrency ) 
AS 
UnitCostAmountCostCur, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.labmeth 
ELSE BTOH.labmeth 
END 
ELSE bt.labmeth 
END 
AS 
LabMeth, 
CASE 
WHEN bt.expmeth IS NULL 
OR bt.expmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.expmeth 
ELSE BTOH.expmeth 
END 
ELSE bt.expmeth 
END 
AS 
ExpMeth, 
CASE 
WHEN bt.conmeth IS NULL 
OR bt.conmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.conmeth 
ELSE BTOH.conmeth 
END 
ELSE bt.conmeth 
END 
AS 
ConMeth, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.ratetable 
ELSE BTOH.ratetable 
END 
ELSE bt.ratetable 
END 
AS 
RateTable, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.ortable 
ELSE BTOH.ortable 
END 
ELSE bt.ortable 
END 
AS 
ORTable, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
CASE 
WHEN ( Isnull(BTReg.mult1, 0) = 0 ) THEN 
1 
ELSE BTReg.mult1 
END 
ELSE 
CASE 
WHEN ( Isnull(BTOH.mult1, 0) = 0 ) THEN 1 
ELSE BTOH.mult1 
END 
END 
ELSE 
CASE 
WHEN ( Isnull(bt.mult1, 0) = 0 ) THEN 1 
ELSE bt.mult1 
END 
END 
AS 
Mult1 
, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN 
CASE 
WHEN ( Isnull(BTReg.mult2, 0) = 0 ) THEN 
1 
ELSE BTReg.mult2 
END 
ELSE 
CASE 
WHEN ( Isnull(BTOH.mult2, 0) = 0 ) THEN 1 
ELSE BTOH.mult2 
END 
END 
ELSE 
CASE 
WHEN ( Isnull(bt.mult2, 0) = 0 ) THEN 1 
ELSE bt.mult2 
END 
END 
AS 
Mult2 
, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
CASE 
WHEN ( Isnull(BTReg.mult3, 0) = 0 ) THEN 
1 
ELSE BTReg.mult3 
END 
ELSE 
CASE 
WHEN ( Isnull(BTOH.mult3, 0) = 0 ) THEN 1 
ELSE BTOH.mult3 
END 
END 
ELSE 
CASE 
WHEN ( Isnull(bt.mult3, 0) = 0 ) THEN 1 
ELSE bt.mult3 
END 
END 
AS 
Mult3 
, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.ovtmult 
ELSE BTOH.ovtmult 
END 
ELSE bt.ovtmult 
END 
AS 
OvtMult, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.specialovtmult 
ELSE BTOH.specialovtmult 
END 
ELSE bt.specialovtmult 
END 
AS 
SpecialOvtMult, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN 
BTReg.separateovt 
ELSE BTOH.separateovt 
END 
ELSE bt.separateovt 
END 
AS 
SeparateOvt, 
CASE 
WHEN ( bt.expmeth IS NULL 
OR bt.expmeth = 0 ) THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.expmult 
ELSE BTOH. expmult 
END 
ELSE bt.expmult 
END 
AS 
ExpMult, 
CASE 
WHEN ( bt.expmeth IS NULL 
OR bt.expmeth = 0 ) THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.exptable 
ELSE BTOH.exptable 
END 
ELSE bt.exptable 
END 
AS 
ExpTable, 
CASE 
WHEN ( bt.conmeth IS NULL 
OR bt.conmeth = 0 ) THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.conmult 
ELSE BTOH.conmult 
END 
ELSE bt.conmult 
END 
AS 
ConMult, 
CASE 
WHEN ( bt.conmeth IS NULL 
OR bt.conmeth = 0 ) THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.contable 
ELSE BTOH.contable 
END 
ELSE bt.contable 
END 
AS 
ConTable, 
CASE 
WHEN ( bt.unitmeth IS NULL 
OR bt.unitmeth = 0 ) THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.unitmult 
ELSE BTOH.unitmult 
END 
ELSE bt.unitmult 
END 
AS 
UnitMult, 
CASE 
WHEN ( bt.unitmeth IS NULL 
OR bt.unitmeth = 0 ) THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.unitmeth 
ELSE BTOH.unitmeth 
END 
ELSE bt.unitmeth 
END 
AS 
UnitMeth, 
CFGBillCurr.decimalplaces 
AS 
BillingDecimals, 
CFGProjCurr.decimalplaces 
AS 
ProjectDecimals 
FROM   btdefaults AS BTReg, 
btdefaults AS BTOH, 
pr AS LEVEL3 
INNER JOIN pr AS LEVEL2 
ON ( LEVEL3.wbs1 = LEVEL2.wbs1 
AND LEVEL3.wbs2 = LEVEL2.wbs2 ) 
INNER JOIN pr 
ON ( LEVEL3.wbs1 = pr.wbs1 ) 
LEFT JOIN bt 
ON ( LEVEL3.billwbs1 = bt.wbs1 
AND LEVEL3.billwbs2 = bt.wbs2 
AND LEVEL3.billwbs3 = bt.wbs3 ) 
LEFT JOIN cl 
ON LEVEL3.clientid = cl.clientid 
LEFT JOIN cl AS CLBill 
ON LEVEL3.billingclientid = CLBill.clientid 
LEFT JOIN btbgsubs 
ON pr.wbs1 = btbgsubs.subwbs1 
LEFT JOIN pr AS MainWBS1PR 
ON btbgsubs.mainwbs1 = MainWBS1PR.wbs1 
AND ' ' = MainWBS1PR.wbs2 
AND ' ' = MainWBS1PR.wbs3 
LEFT JOIN fw_cfgcurrency AS CFGBillCurr 
ON pr.billingcurrencycode = CFGBillCurr.code 
LEFT JOIN fw_cfgcurrency AS CFGProjCurr 
ON pr.projectcurrencycode = CFGProjCurr.code 
INNER JOIN ledgerex 
ON ( ledgerex.wbs1 = LEVEL3.wbs1 ) 
AND ( ledgerex.wbs2 = LEVEL3.wbs2 ) 
AND ( ledgerex.wbs3 = LEVEL3.wbs3 ) 
LEFT JOIN ca 
ON ( ledgerex.account = ca.account ) 
WHERE  ( ledgerex.billstatus IN ( 'B', 'H', 'W', '' ) 
OR ( 'B' = 'B' 
AND ledgerex.billstatus = 'F' 
AND ledgerex.billedperiod > @BillingPeriod ) ) 
AND ( ledgerex.suppressbill = 'N' ) 
AND ( ledgerex.projectcost = 'Y' ) 
AND ( ledgerex.period <= @BillingPeriod ) 
AND ( ledgerex.transdate <= 
@MaxTransDate ) 
AND ( LEVEL3.sublevel = 'N' 
OR ( LEVEL3.wbs2 <> ' ' 
AND LEVEL3.wbs3 <> ' ' ) ) 
AND pr.wbs2 = ' ' 
AND LEVEL2.wbs3 = ' ' 
AND BTReg.defaulttype = '<R>' 
AND BTOH.defaulttype = '<O>' 
AND BTReg.company = Substring(LEVEL3.org, 1, 2) 
AND BTOH.company = Substring(LEVEL3.org, 1, 2) 
AND ( ( EXISTS (SELECT 'x' 
FROM   cfgchargetype 
WHERE  cfgchargetype.type = 
pr.chargetype 
AND (( cfgchargetype.label = 
   /*N*/ 
   'Regular' 
 ) 
) 
) 
AND ( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
) 
AND ( pr.status = 'A' ) ) 
AND ( Substring(MainWBS1PR.org, 1, 2) = /*N*/@CompanyId 
OR ( btbgsubs.mainwbs1 IS NULL 
AND Substring(pr.org, 1, 2) = /*N*/@CompanyId 
) ) 
UNION ALL 
SELECT bied.pkey, 
CASE 
WHEN ca.type = 6 THEN 1 
ELSE 0 
END 
AS 
ReimbConsultant, 
CASE 
WHEN ca.type = 8 THEN 1 
ELSE 0 
END 
AS 
DirectConsultant, 
CASE 
WHEN ca.type >= 5 
AND ca.type < 7 THEN 1 
ELSE 0 
END 
AS 
ReimbursableAcct, 
bied.period, 
bied.wbs1, 
bied.wbs2, 
bied.wbs3, 
bied.account, 
bied.vendor, 
bied.refno, 
bied.transtype, 
bied.subtype, 
bied.billstatus, 
bied.transdate, 
bied.desc1, 
bied.desc2, 
bied.amountbillingcurrency 
AS Amount 
, 
bied.amountbillingcurrency 
AS 
AmountCostCur 
, 
bied.unit, 
bied.unitquantity, 
bied.unitcostratebillingcurrency 
AS 
UnitCostRate 
, 
bied.unitcostratebillingcurrency 
AS 
UnitCostRateCostCur, 
bied.unitbillingrate, 
0 
AS 
GlobalTable 
, 
( bied.unitquantity * 
bied.unitcostratebillingcurrency ) 
AS 
UnitCostAmount, 
( bied.unitquantity * 
bied.unitcostratebillingcurrency ) 
AS 
UnitCostAmountCostCur, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.labmeth 
ELSE BTOH.labmeth 
END 
ELSE bt.labmeth 
END 
AS 
LabMeth 
, 
CASE 
WHEN bt.expmeth IS NULL 
OR bt.expmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.expmeth 
ELSE BTOH.expmeth 
END 
ELSE bt.expmeth 
END 
AS 
ExpMeth 
, 
CASE 
WHEN bt.conmeth IS NULL 
OR bt.conmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.conmeth 
ELSE BTOH.conmeth 
END 
ELSE bt.conmeth 
END 
AS 
ConMeth 
, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.ratetable 
ELSE BTOH.ratetable 
END 
ELSE bt.ratetable 
END 
AS 
RateTable 
, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.ortable 
ELSE BTOH.ortable 
END 
ELSE bt.ortable 
END 
AS 
ORTable 
, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
CASE 
WHEN ( Isnull(BTReg.mult1, 0) = 0 ) THEN 
1 
ELSE BTReg.mult1 
END 
ELSE 
CASE 
WHEN ( Isnull(BTOH.mult1, 0) = 0 ) THEN 1 
ELSE BTOH.mult1 
END 
END 
ELSE 
CASE 
WHEN ( Isnull(bt.mult1, 0) = 0 ) THEN 1 
ELSE bt.mult1 
END 
END 
AS Mult1, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN 
CASE 
WHEN ( Isnull(BTReg.mult2, 0) = 0 ) THEN 
1 
ELSE BTReg.mult2 
END 
ELSE 
CASE 
WHEN ( Isnull(BTOH.mult2, 0) = 0 ) THEN 1 
ELSE BTOH.mult2 
END 
END 
ELSE 
CASE 
WHEN ( Isnull(bt.mult2, 0) = 0 ) THEN 1 
ELSE bt.mult2 
END 
END 
AS Mult2, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
CASE 
WHEN ( Isnull(BTReg.mult3, 0) = 0 ) THEN 
1 
ELSE BTReg.mult3 
END 
ELSE 
CASE 
WHEN ( Isnull(BTOH.mult3, 0) = 0 ) THEN 1 
ELSE BTOH.mult3 
END 
END 
ELSE 
CASE 
WHEN ( Isnull(bt.mult3, 0) = 0 ) THEN 1 
ELSE bt.mult3 
END 
END 
AS Mult3, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.ovtmult 
ELSE BTOH.ovtmult 
END 
ELSE bt.ovtmult 
END 
AS 
OvtMult 
, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.specialovtmult 
ELSE BTOH.specialovtmult 
END 
ELSE bt.specialovtmult 
END 
AS 
SpecialOvtMult, 
CASE 
WHEN bt.labmeth IS NULL 
OR bt.labmeth = 0 THEN 
CASE 
WHEN pr.chargetype = 'R' THEN 
BTReg.separateovt 
ELSE BTOH.separateovt 
END 
ELSE bt.separateovt 
END 
AS 
SeparateOvt 
, 
CASE 
WHEN ( bt.expmeth IS NULL 
OR bt.expmeth = 0 ) THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.expmult 
ELSE BTOH. expmult 
END 
ELSE bt.expmult 
END 
AS 
ExpMult 
, 
CASE 
WHEN ( bt.expmeth IS NULL 
OR bt.expmeth = 0 ) THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.exptable 
ELSE BTOH.exptable 
END 
ELSE bt.exptable 
END 
AS 
ExpTable 
, 
CASE 
WHEN ( bt.conmeth IS NULL 
OR bt.conmeth = 0 ) THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.conmult 
ELSE BTOH.conmult 
END 
ELSE bt.conmult 
END 
AS 
ConMult 
, 
CASE 
WHEN ( bt.conmeth IS NULL 
OR bt.conmeth = 0 ) THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.contable 
ELSE BTOH.contable 
END 
ELSE bt.contable 
END 
AS 
ConTable 
, 
CASE 
WHEN ( bt.unitmeth IS NULL 
OR bt.unitmeth = 0 ) THEN 
CASE 
WHEN LEVEL3.chargetype = 'R' THEN 
BTReg.unitmult 
ELSE BTOH.unitmult 
END 
ELSE bt.unitmult 
END 
AS 
UnitMult 
, 
CASE 
WHEN ( bt.unitmeth IS NULL 
OR bt.unitmeth = 0 ) THEN 
CASE 
WHEN pr.chargetype = 'R' THEN BTReg.unitmeth 
ELSE BTOH.unitmeth 
END 
ELSE bt.unitmeth 
END 
AS 
UnitMeth 
, 
CFGBillCurr.decimalplaces 
AS 
BillingDecimals, 
CFGProjCurr.decimalplaces 
AS 
ProjectDecimals 
FROM   btdefaults AS BTReg, 
btdefaults AS BTOH, 
pr AS LEVEL3 
INNER JOIN pr AS LEVEL2 
ON ( LEVEL3.wbs1 = LEVEL2.wbs1 
AND LEVEL3.wbs2 = LEVEL2.wbs2 ) 
INNER JOIN pr 
ON ( LEVEL3.wbs1 = pr.wbs1 ) 
LEFT JOIN bt 
ON ( LEVEL3.billwbs1 = bt.wbs1 
AND LEVEL3.billwbs2 = bt.wbs2 
AND LEVEL3.billwbs3 = bt.wbs3 ) 
LEFT JOIN cl 
ON LEVEL3.clientid = cl.clientid 
LEFT JOIN cl AS CLBill 
ON LEVEL3.billingclientid = CLBill.clientid 
LEFT JOIN btbgsubs 
ON pr.wbs1 = btbgsubs.subwbs1 
LEFT JOIN pr AS MainWBS1PR 
ON btbgsubs.mainwbs1 = MainWBS1PR.wbs1 
AND ' ' = MainWBS1PR.wbs2 
AND ' ' = MainWBS1PR.wbs3 
LEFT JOIN fw_cfgcurrency AS CFGBillCurr 
ON pr.billingcurrencycode = CFGBillCurr.code 
LEFT JOIN fw_cfgcurrency AS CFGProjCurr 
ON pr.projectcurrencycode = CFGProjCurr.code 
INNER JOIN bied 
ON ( bied.wbs1 = LEVEL3.wbs1 ) 
AND ( bied.wbs2 = LEVEL3.wbs2 ) 
AND ( bied.wbs3 = LEVEL3.wbs3 ) 
LEFT JOIN ca 
ON ( bied.account = ca.account ) 
WHERE  ( bied.billstatus IN ( 'B', 'H', 'W', '' ) 
OR ( 'B' = 'B' 
AND bied.billstatus = 'F' 
AND bied.billedperiod > @BillingPeriod ) ) 
AND ( bied.suppressbill = 'N' ) 
AND ( bied.projectcost = 'Y' ) 
AND ( bied.period <= @BillingPeriod ) 
AND ( bied.transdate <= @MaxTransDate ) 
AND ( LEVEL3.sublevel = 'N' 
OR ( LEVEL3.wbs2 <> ' ' 
AND LEVEL3.wbs3 <> ' ' ) ) 
AND pr.wbs2 = ' ' 
AND LEVEL2.wbs3 = ' ' 
AND BTReg.defaulttype = '<R>' 
AND BTOH.defaulttype = '<O>' 
AND BTReg.company = Substring(LEVEL3.org, 1, 2) 
AND BTOH.company = Substring(LEVEL3.org, 1, 2) 
AND ( ( EXISTS (SELECT 'x' 
FROM   cfgchargetype 
WHERE  cfgchargetype.type = 
pr.chargetype 
AND (( cfgchargetype.label = 
   /*N*/ 
   'Regular' 
 ) 
) 
) 
AND ( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
) 
AND ( pr.status = 'A' ) ) 
AND ( Substring(MainWBS1PR.org, 1, 2) = /*N*/@CompanyId 
OR ( btbgsubs.mainwbs1 IS NULL 
AND Substring(pr.org, 1, 2) = /*N*/@CompanyId 
) )) 
AS 
billEDtoBT 
LEFT JOIN btevvends 
ON ( billEDtoBT.exptable = btevvends.tableno ) 
AND ( billEDtoBT.vendor = btevvends.vendor )) 
LEFT JOIN bteaaccts 
ON ( billEDtoBT.account = bteaaccts.account ) 
AND ( billEDtoBT.exptable = bteaaccts.tableno )) 
LEFT JOIN (SELECT bteccats.tableno, 
bteccats.category, 
bteccats.description, 
bteccats.multiplier, 
bteccats.sortseq, 
btecaccts.account 
FROM   bteccats 
LEFT JOIN btecaccts 
ON ( bteccats.tableno = 
btecaccts.tableno ) 
AND ( bteccats.category = 
btecaccts.category )) 
AS 
btecCatsQry 
ON ( billEDtoBT.exptable = btecCatsQry.tableno ) 
AND ( billEDtoBT.account = btecCatsQry.account )) 
LEFT JOIN btevvends AS BTEVVendsCons 
ON ( billEDtoBT.contable = BTEVVendsCons.tableno ) 
AND ( billEDtoBT.vendor = BTEVVendsCons.vendor )) 
LEFT JOIN bteaaccts AS BTEAAcctsCons 
ON ( billEDtoBT.contable = BTEAAcctsCons.tableno ) 
AND ( billEDtoBT.account = BTEAAcctsCons.account )) 
LEFT JOIN (SELECT bteccats.tableno, 
bteccats.category, 
bteccats.description, 
bteccats.multiplier, 
bteccats.sortseq, 
btecaccts.account 
FROM   bteccats 
LEFT JOIN btecaccts 
ON ( bteccats.tableno = 
btecaccts.tableno ) 
AND ( bteccats.category = 
btecaccts.category )) AS 
btecCatsQryCons 
ON ( billEDtoBT.contable = btecCatsQryCons.tableno ) 
AND ( billEDtoBT.account = btecCatsQryCons.account )) 
LEFT JOIN bteaaccts AS BTEAAcctsConDefault 
ON ( billEDtoBT.account = BTEAAcctsConDefault.account ) 
AND ( billEDtoBT.globaltable = 
BTEAAcctsConDefault.tableno )) 
LEFT JOIN bteaaccts AS BTEAAcctsExpDefault 
ON ( billEDtoBT.account = BTEAAcctsExpDefault.account ) 
AND ( billEDtoBT.globaltable = 
BTEAAcctsExpDefault.tableno )) 
AS billCalcMultED 
WHERE  ( workmult <> 0 
OR 1 = 1 )) AS birptUnbilledUnion 
ON ( LEVEL3.wbs1 = birptUnbilledUnion.wbs1 ) 
AND ( LEVEL3.wbs2 = birptUnbilledUnion.wbs2 ) 
AND ( LEVEL3.wbs3 = birptUnbilledUnion.wbs3 ) 
LEFT JOIN em AS WBS1PrinEM 
ON pr.principal = WBS1PrinEM.employee 
LEFT JOIN em AS WBS1pmEM 
ON pr.projmgr = WBS1pmEM.employee 
LEFT JOIN em AS WBS2PrinEM 
ON LEVEL2.principal = WBS2PrinEM.employee 
LEFT JOIN em AS WBS2pmEM 
ON LEVEL2.projmgr = WBS2pmEM.employee 
LEFT JOIN em AS WBS3PrinEM 
ON LEVEL3.principal = WBS3PrinEM.employee 
LEFT JOIN em AS WBS3pmEM 
ON LEVEL3.projmgr = WBS3pmEM.employee 
LEFT JOIN cl AS WBS1BillCL 
ON pr.billingclientid = WBS1BillCL.clientid 
LEFT JOIN cl AS WBS2BillCL 
ON LEVEL2.billingclientid = WBS2BillCL.clientid 
LEFT JOIN cl 
ON LEVEL3.clientid = cl.clientid 
LEFT JOIN cl AS CLBill 
ON LEVEL3.billingclientid = CLBill.clientid 
LEFT JOIN btbgsubs 
ON pr.wbs1 = btbgsubs.subwbs1 
LEFT JOIN pr AS MainWBS1PR 
ON btbgsubs.mainwbs1 = MainWBS1PR.wbs1 
AND ' ' = MainWBS1PR.wbs2 
AND ' ' = MainWBS1PR.wbs3 
LEFT JOIN cfgorgcodes AS groupCFGOrgCodes2 
ON Substring(pr.org, 4, 2) = groupCFGOrgCodes2.code 
AND groupCFGOrgCodes2.orglevel = 2 
LEFT JOIN cfgorgcodes AS groupCFGOrgCodes3 
ON Substring(pr.org, 7, 3) = groupCFGOrgCodes3.code 
AND groupCFGOrgCodes3.orglevel = 3 
WHERE  ( pr.wbs2 = ' ' ) 
       AND ( LEVEL2.wbs3 = ' ' ) 
       AND ( ( EXISTS (SELECT 'x' 
                       FROM   cfgchargetype 
                       WHERE  cfgchargetype.type = pr.chargetype 
                              AND (( cfgchargetype.label = /*N*/'Regular' ))) 
               AND ( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) ) 
             AND ( pr.status = 'A' ) ) 
       AND ( Substring(MainWBS1PR.org, 1, 2) = /*N*/@CompanyId 
              OR ( btbgsubs.mainwbs1 IS NULL 
                   AND Substring(pr.org, 1, 2) = /*N*/@CompanyId ) ) 
GROUP  BY Isnull(Substring(pr.org, 4, 2), ''), 
          Isnull(Substring(pr.org, 7, 3), ''), 
          rectype, 
          billstatus, 
          birptUnbilledUnion.employee, 
          sortkey, 
          birptUnbilledUnion.pkey 
ORDER  BY group1, 
          group2, 
          rectype, 
          billstatus, 
          sortkey 

END
GO
