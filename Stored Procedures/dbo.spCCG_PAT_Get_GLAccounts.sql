SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_Get_GLAccounts] (
	@GLStatusActiveOnly		bit,
	@AllAccountTypes		bit,
	@Company				Nvarchar(20) = N' ',
	@RequiredAccounts		Nvarchar(max) = ''
) AS
BEGIN
	--select Account,Name from CA where status= 'A'
	--exec spCCG_PAT_Get_GLAccounts 1, 1, '00', @RequiredAccounts = '623.00, 624.00'
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	Declare @MultiCompanyEnabled char(1)
	select @MultiCompanyEnabled = MultiCompanyEnabled from FW_CFGSystem

	if @MultiCompanyEnabled = 'N'
	BEGIN
		If @AllAccountTypes = 1
			SELECT CA.Account, CA.Name + (case when CA.Status = 'I' then ' (I)' else '' end) as Name, CA.Status
				FROM CA WHERE (@GLStatusActiveOnly = 0 OR [Status] = 'A') and [Status] <> 'D' 
			UNION SELECT CA.Account, CA.Name + (case when CA.Status = 'I' then ' (I)' when CA.Status = 'D' then ' (D)' else '' end), CA.Status 
				FROM CA WHERE ', '+@RequiredAccounts+',' like '%, '+CA.Account+',%' ORDER BY 1
		ELSE
			SELECT CA.Account, CA.Name + (case when CA.Status = 'I' then ' (I)' else '' end) as Name, CA.Status
				FROM CA WHERE (@GLStatusActiveOnly = 0 OR [Status] = 'A') and [Status] <> 'D'  AND [Type] in (5,6,7,8,9) 
			UNION SELECT CA.Account, CA.Name + (case when CA.Status = 'I' then ' (I)' when CA.Status = 'D' then ' (D)' else '' end), CA.Status 
				FROM CA WHERE ', '+@RequiredAccounts+',' like '%, '+CA.Account+',%' ORDER BY 1
	END
	ELSE
	BEGIN
		If @AllAccountTypes = 1
			SELECT Distinct CA.Account, CA.Name + (case when CA.Status = 'I' then ' (I)' else '' end) as Name, CA.Status
				FROM CA LEFT JOIN CACompanyMapping m on CA.Account = m.Account
				WHERE (CA.GlobalAccount = 'Y' or m.Company = @Company) and (@GLStatusActiveOnly = 0 OR [Status] = 'A') and [Status] <> 'D' 
			UNION SELECT CA.Account, CA.Name + (case when CA.Status = 'I' then ' (I)' when CA.Status = 'D' then ' (D)' else '' end), CA.Status 
				FROM CA WHERE ', '+@RequiredAccounts+',' like '%, '+CA.Account+',%' ORDER BY 1
		ELSE
			SELECT Distinct CA.Account, CA.Name + (case when CA.Status = 'I' then ' (I)' else '' end) as Name, CA.Status
				FROM CA LEFT JOIN CACompanyMapping m on CA.Account = m.Account
				WHERE (CA.GlobalAccount = 'Y' or m.Company = @Company) AND [Type] in (5,6,7,8,9) and (@GLStatusActiveOnly = 0 OR [Status] = 'A') and [Status] <> 'D' 
			UNION SELECT CA.Account, CA.Name + (case when CA.Status = 'I' then ' (I)' when CA.Status = 'D' then ' (D)' else '' end), CA.Status 
				FROM CA WHERE ', '+@RequiredAccounts+',' like '%, '+CA.Account+',%' ORDER BY 1
	END
END
GO
