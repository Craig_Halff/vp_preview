SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_TR_TransferLabor](@Username varchar(32), @Employee varchar(32), @OrigTable varchar(4), @OrigLaborCode varchar(32), @OrigCategory varchar(8),
	@OrigPeriod varchar(6), @OrigPostSeq varchar(32), @OrigPKey varchar(32), 
	@NewPeriod  varchar(6), @NewPostSeq  varchar(32), @NewPKey  varchar(32),
	@OrigWBS1 varchar(30), @OrigWBS2 varchar(7), @OrigWBS3 varchar(7),
	@NewWBS1  varchar(30), @NewWBS2  varchar(7), @NewWBS3  varchar(7),
	@OrigRegHrs decimal(19,4), @OrigOvtHrs decimal(19,4), @OrigSpecialOvtHrs decimal(19,4),
	@NewRegHrs  decimal(19,4), @NewOvtHrs  decimal(19,4), @NewSpecialOvtHrs  decimal(19,4),
	@NewBillExt decimal(19,4),  
	@ApprovalComment varchar(255), @AutoApprove char(1) , @FNStatus varchar(2))
AS BEGIN

	BEGIN TRY
		declare @empCompany varchar(32), @directAccount varchar(20), @origOrg varchar(32), @newOrg varchar(32), @newChargeType varchar(1)
		declare @partial int, @sql varchar(max), @origTableWhereClause varchar(255)
		declare @commonColumns varchar(2000), @nonCommonColumns varchar(1000), @sqlAmts varchar(255)
		declare @regMult varchar(50), @ovtMult varchar(50), @specialOvtMult varchar(50), @emp varchar(32)
		DECLARE @employeeAccountType varchar(1)

		select @emp=Employee from SEUser where Username=@Username
		SELECT @employeeAccountType = Type FROM EM Where Employee = @Employee
		
		if @NewPKey = ''
			set @NewPKey = Left(Replace(Convert(varchar(255), newid()),'-',''),31)

		select @newOrg=Org, @newChargeType=ChargeType from PR where WBS1=@NewWBS1 and WBS2=@NewWBS2 and WBS3=@NewWBS3
		select @origOrg=Org from PR where WBS1=@OrigWBS1 and WBS2=@OrigWBS2 and WBS3=@OrigWBS3

		
		declare @Multicompany varchar(32), @Multicurrency varchar(32)
		select @Multicompany=MulticompanyEnabled, @Multicurrency=MulticurrencyEnabled from CFGSystem
		if @Multicompany='Y'
		begin
			declare @CompLength int
			select @CompLength=Org1Length from CFGFormat
			if Left(@newOrg,@CompLength) <> Left(@origOrg,@CompLength)
			begin
				RAISERROR(N'Inter-company transfers are not allowed.', 16, 1)
				return
			end
		end
		if @Multicurrency='Y'
		begin
			declare @newProjectCurrency varchar(32), @origProjectCurrency varchar(32)
			declare @newBillingCurrency varchar(32), @origBillingCurrency varchar(32)
			select @newProjectCurrency =ProjectCurrencyCode, @newBillingCurrency =BillingCurrencyCode from PR where WBS1=@NewWBS1  and WBS2=' ' and WBS3=' '
			select @origProjectCurrency=ProjectCurrencyCode, @origBillingCurrency=BillingCurrencyCode from PR where WBS1=@OrigWBS1 and WBS2=' ' and WBS3=' '
			if @newProjectCurrency <> @origProjectCurrency
			begin
				RAISERROR(N'Transfers between project currencies are not allowed.', 16, 1)
				return
			end
			if @newBillingCurrency <> @origBillingCurrency
			begin
				RAISERROR(N'Transfers between billing currencies are not allowed.', 16, 1)
				return
			end
		end

		
		if @OrigTable='BILD' and @OrigPostSeq=0
			set @NewPostSeq=0

		
		set @nonCommonColumns = 'Period,PostSeq,PKey,WBS1,WBS2,WBS3,XferWBS1,XferWBS2,XferWBS3,XferLaborCode,XferCategory,' +
			'PrOrg,ChargeType,BillStatus,RegHrs,OvtHrs,SpecialOvtHrs,BillExt,' +
			'RegAmt,OvtAmt,SpecialOvtAmt,' +
			'RegAmtProjectCurrency,OvtAmtProjectCurrency,SpecialOvtAmtProjectCurrency,' +
			'RegAmtBillingCurrency,OvtAmtBillingCurrency,SpecialOvtAmtBillingCurrency,' +
			'RegAmtEmployeeCurrency,OvtAmtEmployeeCurrency,SpecialOvtAmtEmployeeCurrency,ModUser,'
		select @commonColumns = STUFF((
			SELECT ',' + c.name
			FROM sys.columns c
			WHERE c.object_id = OBJECT_ID('BILD') and c.name in (select cols.name from sys.columns cols where cols.object_id=OBJECT_ID('LD'))
				and charindex(','+c.name+',', ','+@nonCommonColumns) < 1
			FOR XML PATH('')), 1, 1, '')

		set @origTableWhereClause = ' where Period=' + @OrigPeriod + ' and PostSeq=' + @OrigPostSeq + ' and PKey=''' + @OrigPkey + ''' '

		if @OrigRegHrs = @NewRegHrs or @OrigRegHrs = 0
			set @regMult = '1'
		else 
			set @regMult = Cast(@NewRegHrs as varchar) + ' / ' + Cast(@OrigRegHrs as varchar)
		if @OrigOvtHrs = @NewOvtHrs or @OrigOvtHrs = 0
			set @ovtMult = '1'
		else 
			set @ovtMult = Cast(@NewOvtHrs as varchar) + ' / ' + Cast(@OrigOvtHrs as varchar)
		if @OrigSpecialOvtHrs = @NewSpecialOvtHrs or @OrigSpecialOvtHrs = 0
			set @specialOvtMult = '1'
		else 
			set @specialOvtMult = Cast(@NewSpecialOvtHrs as varchar) + ' / ' + Cast(@OrigSpecialOvtHrs as varchar)

		
		set @sql = 'if not exists (select ''x'' from ' + @OrigTable + @origTableWhereClause + ' and BillStatus<>''T'') ' +
			' RAISERROR(N''The record has already been transferred or modified.'', 16, 1) '
	--	set @sql = 'if not exists (select ''x'' from ' + @OrigTable + @origTableWhereClause + ' and BillStatus in (''B'',''H'',''W'')) ' +
	--		' begin; THROW 50001, N''The record has already been transferred or modified.'', 1; end; '

		
		declare @tbl varchar(4)
		if @OrigTable='BILD' and @OrigPostSeq='0' 
			set @tbl='BILD'
		else
			set @tbl='LD'
		set @sql = @sql +
			' insert into ' + @tbl + ' (' + @nonCommonColumns + @commonColumns + ') select ' + @NewPeriod + ',' + @NewPostSeq + ',''' + @NewPKey + 'a'',' +
				'WBS1,WBS2,WBS3,XferWBS1,XferWBS2,XferWBS3,XferLaborCode,XferCategory,IsNull(PrOrg,''' + @origOrg + '''),ChargeType,''T'',' + 
				Cast(-@NewRegHrs as varchar) + ',' + Cast(-@NewOvtHrs as varchar) + ',' + Cast(-@NewSpecialOvtHrs as varchar) + 
				',Round(-BillExt * '                + @regMult + ',2),' +
				' Round(-RegAmt * '                 + @regMult + ',2), Round(-OvtAmt * '                 + @ovtMult + ',2), Round(-SpecialOvtAmt * '                 + @specialOvtMult + ',2),' +
				' Round(-RegAmtProjectCurrency * '  + @regMult + ',2), Round(-OvtAmtProjectCurrency * '  + @ovtMult + ',2), Round(-SpecialOvtAmtProjectCurrency * '  + @specialOvtMult + ',2),' +
				' Round(-RegAmtBillingCurrency * '  + @regMult + ',2), Round(-OvtAmtBillingCurrency * '  + @ovtMult + ',2), Round(-SpecialOvtAmtBillingCurrency * '  + @specialOvtMult + ',2),' +
				' Round(-RegAmtEmployeeCurrency * ' + @regMult + ',2), Round(-OvtAmtEmployeeCurrency * ' + @ovtMult + ',2), Round(-SpecialOvtAmtEmployeeCurrency * ' + @specialOvtMult + ',2),' +
				'''' + @Username + ''', ' + @commonColumns +
			' from ' + @OrigTable + @origTableWhereClause
		set @sql = @sql +
			' insert into ' + @tbl + ' (' + @nonCommonColumns + @commonColumns + ') select ' + @NewPeriod + ',' + @NewPostSeq + ',''' + @NewPKey + 'b'',' +
				'''' + @NewWBS1 + ''',''' + @NewWBS2 + ''',''' + @NewWBS3 + ''',''' + 
				@OrigWBS1 + ''',''' + @OrigWBS2 + ''',''' + @OrigWBS3 + ''',''' + @OrigLaborCode + ''',' + @OrigCategory +
				',''' + @newOrg + ''',''' + @newChargeType + ''',BillStatus,' + 
				Cast(@NewRegHrs as varchar) + ',' + Cast(@NewOvtHrs as varchar) + ',' + Cast(@NewSpecialOvtHrs as varchar) +
				',Round(BillExt * '                + @regMult + ',2),' +
				' Round(RegAmt * '                 + @regMult + ',2), Round(OvtAmt * '                 + @ovtMult + ',2), Round(SpecialOvtAmt * '                 + @specialOvtMult + ',2),' +
				' Round(RegAmtProjectCurrency * '  + @regMult + ',2), Round(OvtAmtProjectCurrency * '  + @ovtMult + ',2), Round(SpecialOvtAmtProjectCurrency * '  + @specialOvtMult + ',2),' +
				' Round(RegAmtBillingCurrency * '  + @regMult + ',2), Round(OvtAmtBillingCurrency * '  + @ovtMult + ',2), Round(SpecialOvtAmtBillingCurrency * '  + @specialOvtMult + ',2),' +
				' Round(RegAmtEmployeeCurrency * ' + @regMult + ',2), Round(OvtAmtEmployeeCurrency * ' + @ovtMult + ',2), Round(SpecialOvtAmtEmployeeCurrency * ' + @specialOvtMult + ',2),' +
				'''' + @Username + ''', ' + @commonColumns +
			' from ' + @OrigTable + @origTableWhereClause

		
		print @sql
		execute(@sql)
		set @sql = ''

		
		if @OrigRegHrs <> @NewRegHrs or @OrigOvtHrs <> @NewOvtHrs or @OrigSpecialOvtHrs <> @NewSpecialOvtHrs
		begin
			
			set @sql = ' insert into BILD (' + @nonCommonColumns + @commonColumns + ') select Period,PostSeq,''' + @NewPKey + 'c''' +
					',WBS1,WBS2,WBS3,XferWBS1,XferWBS2,XferWBS3,XferLaborCode,XferCategory,IsNull(PrOrg,''' + @newOrg + '''),ChargeType,BillStatus,' + 
					Cast(@OrigRegHrs - @NewRegHrs as varchar) + ',' + Cast(@OrigOvtHrs - @NewOvtHrs as varchar) + ',' + Cast(@OrigSpecialOvtHrs - @NewSpecialOvtHrs as varchar) + 
					',BillExt - Round(BillExt * ' + @regMult + ',2),' +
					' RegAmt  - Round(RegAmt  * ' + @regMult + ',2), OvtAmt - Round(OvtAmt * ' + @ovtMult + ',2), SpecialOvtAmt - Round(SpecialOvtAmt * '  + @specialOvtMult + ',2),' +
					' RegAmtProjectCurrency  - Round(RegAmtProjectCurrency  * ' + @regMult + ',2), OvtAmtProjectCurrency  - Round(OvtAmtProjectCurrency  * ' + @ovtMult + ',2), SpecialOvtAmtProjectCurrency  - Round(SpecialOvtAmtProjectCurrency  * ' + @specialOvtMult + ',2),' +
					' RegAmtBillingCurrency  - Round(RegAmtBillingCurrency  * ' + @regMult + ',2), OvtAmtBillingCurrency  - Round(OvtAmtBillingCurrency  * ' + @ovtMult + ',2), SpecialOvtAmtBillingCurrency  - Round(SpecialOvtAmtBillingCurrency  * ' + @specialOvtMult + ',2),' +
					' RegAmtEmployeeCurrency - Round(RegAmtEmployeeCurrency * ' + @regMult + ',2), OvtAmtEmployeeCurrency - Round(OvtAmtEmployeeCurrency * ' + @ovtMult + ',2), SpecialOvtAmtEmployeeCurrency - Round(SpecialOvtAmtEmployeeCurrency * ' + @specialOvtMult + ',2),' +
					'''' + @Username + ''', ' + @commonColumns +
				' from ' + @OrigTable + @origTableWhereClause
			
			print @sql
			execute(@sql)
			set @sql=''
		end

		
		if not exists (select 'x' from EMProjectAssoc where WBS1=@NewWBS1 and WBS2=@NewWBS2 and WBS3=@NewWBS3 and Employee=@Employee)
			insert into EMProjectAssoc (WBS1, WBS2, WBS3, Employee, Role, TeamStatus, CreateUser, CreateDate, ModUser, ModDate, RecordID)
			values (@NewWBS1, @NewWBS2, @NewWBS3, @Employee, '', 'Active',
					@Username, getutcdate(), @Username, getutcdate(), @NewPKey+'d')
		
		if @NewWBS3<>' '
		if not exists (select 'x' from EMProjectAssoc where WBS1=@NewWBS1 and WBS2=@NewWBS2 and WBS3=' ' and Employee=@Employee)
			insert into EMProjectAssoc (WBS1, WBS2, WBS3, Employee, Role, TeamStatus, CreateUser, CreateDate, ModUser, ModDate, RecordID)
			values (@NewWBS1, @NewWBS2, ' ', @Employee, '', 'Active',
					@Username, getutcdate(), @Username, getutcdate(), @NewPKey+'e')
		
		if @NewWBS2<>' '
		if not exists (select 'x' from EMProjectAssoc where WBS1=@NewWBS1 and WBS2=' ' and WBS3=' ' and Employee=@Employee)
			insert into EMProjectAssoc (WBS1, WBS2, WBS3, Employee, Role, TeamStatus, CreateUser, CreateDate, ModUser, ModDate, RecordID)
			values (@NewWBS1, ' ', ' ', @Employee, '', 'Active',
					@Username, getutcdate(), @Username, getutcdate(), @NewPKey+'f')

		
		if @OrigTable='LD' or @OrigPostSeq<>'0'
		begin
			
			select @empCompany = dbo.fnCCG_GetEmployeeCompany(@Employee)
			select @directAccount=DirectAccount from CFGEmployeeTypeAccounts where Type = @employeeAccountType AND Company = @empCompany
			set @sql = @sql +
				' insert into LedgerMisc(Period,PostSeq,PKey,WBS1,WBS2,WBS3,Account,' +
				'   Org,TransType,SubType,RefNo,TransDate,Desc1,Desc2,' +
				'   Amount,ProjectCost,AutoEntry,SuppressBill,SkipGL,TransactionAmount,TransactionCurrencyCode) ' +
				' select ' + @NewPeriod + ',' + @NewPostSeq + ',''' + @NewPKey + 'g'',null,null,null,''' + IsNull(@directAccount,'') + ''',' +
				' PrOrg,''TS'',null,''Auto'',''' + Convert(varchar, getdate(), 101) + ''',' +
				' ''Labor Posting - Billing Transfer (CCG)'',''Job Cost Adjustment (CCG)'', ' +
				' Round(-RegAmt * ' + @regMult + ',2) + Round(-OvtAmt * ' + @ovtMult + ',2) + Round(-SpecialOvtAmt * ' + @specialOvtMult + ',2),''N'',''Y'',''N'',''N'',' +
				' Round(-RegAmt * ' + @regMult + ',2) + Round(-OvtAmt * ' + @ovtMult + ',2) + Round(-SpecialOvtAmt * ' + @specialOvtMult + ',2),'' ''' +
				' from ' + @OrigTable + @origTableWhereClause
			set @sql = @sql +
				' insert into LedgerMisc(Period,PostSeq,PKey,WBS1,WBS2,WBS3,Account,' +
				'   Org,TransType,SubType,RefNo,TransDate,Desc1,Desc2,' +
				'   Amount,ProjectCost,AutoEntry,SuppressBill,SkipGL,TransactionAmount,TransactionCurrencyCode) ' +
				' select ' + @NewPeriod + ',' + @NewPostSeq + ',''' + @NewPKey + 'h'',null,null,null,''' + @directAccount + ''',' +
				' ''' + @newOrg + ''',''TS'',null,''Auto'',''' + Convert(varchar, getdate(), 101) + ''',' +
				' ''Labor Posting - Billing Transfer (CCG)'',''Job Cost Adjustment (CCG)'', ' +
				' Round(RegAmt * ' + @regMult + ',2) + Round(OvtAmt * ' + @ovtMult + ',2) + Round(SpecialOvtAmt * ' + @specialOvtMult + ',2),''N'',''Y'',''N'',''N'',' +
				' Round(RegAmt * ' + @regMult + ',2) + Round(OvtAmt * ' + @ovtMult + ',2) + Round(SpecialOvtAmt * ' + @specialOvtMult + ',2),'' ''' +
				' from ' + @OrigTable + @origTableWhereClause
		end
		
		if @OrigTable='BILD' and @OrigPostSeq<>0
			set @sql = @sql + '; delete from BILD ' + @origTableWhereClause
		else
		begin
			if exists (select 'x' from syscolumns a,sysobjects b where a.id = b.id and a.name = 'TransferredPeriod' and b.name = @OrigTable)
				set @sql = @sql + ' ; update ' + @OrigTable + ' set TransferredPeriod=' + @NewPeriod + ', TransferredBillStatus=BillStatus ' +  @origTableWhereClause
			set @sql = @sql + ' ; update ' + @OrigTable + ' set BillStatus=''T'' ' +  @origTableWhereClause
		end

		print @sql
		execute(@sql)
		set @sql = ''
		
		
		
		if @AutoApprove='Y' or (@AutoApprove='P' and @NewWBS1=@OrigWBS1)
			set @sql = 'exec spCCG_TR_InsertApproval ''' + @tbl + ''',' + Cast(@NewPeriod as varchar) + ',' + 
				Cast(@NewPostSeq as varchar) + ',''' + @NewPKey + 'b'',''' + @emp + ''',''' + @emp + ''',''' + IsNull(@ApprovalComment,'') + ''',null,null,''' + @FNStatus + ''' '

		if @OrigRegHrs <> @NewRegHrs or @OrigOvtHrs <> @NewOvtHrs or @OrigSpecialOvtHrs <> @NewSpecialOvtHrs
			set @sql = @sql + 
				' ; exec spCCG_TR_InsertApproval ''' + 'BILD' + ''',' + Cast(@OrigPeriod as varchar) + ',' + 
					Cast(@OrigPostSeq as varchar) + ',''' + @NewPKey + 'c'',''' + @emp + ''',''' + @emp + ''',''' + IsNull(@ApprovalComment,'') + ''',null,null,''' + @FNStatus + ''' '

		
		if @OrigRegHrs <> @NewRegHrs or @OrigOvtHrs <> @NewOvtHrs or @OrigSpecialOvtHrs <> @NewSpecialOvtHrs
			set @sql = @sql +
				' ; insert into CCG_TR_History (OriginalTable, OriginalPeriod, OriginalPostSeq, OriginalPKey, NewTable, NewPeriod, NewPostSeq, NewPKey, ActionStatus, ActionTaken, ActionDetail, ActionDate, ActionTakenBy, WBS1, WBS2, WBS3, OldValue, NewValue, OldValueDecimal, NewValueDecimal, ModificationComment, ApprovalComment, DateLastNotificationSent) ' +
				' select ''BILD'', OriginalPeriod, OriginalPostSeq, ''' + @NewPKey + 'c'', NewTable, NewPeriod, NewPostSeq, NewPKey, ActionStatus, ActionTaken, ActionDetail, ActionDate, ActionTakenBy, WBS1, WBS2, WBS3, OldValue, NewValue, OldValueDecimal, NewValueDecimal, ModificationComment, ApprovalComment, DateLastNotificationSent ' +
					' from CCG_TR_History where OriginalTable=''' + @tbl + ''' and OriginalPeriod=' + @OrigPeriod + ' and OriginalPostSeq=' + @OrigPostSeq + ' and OriginalPKey=''' + @OrigPkey + ''' '
		
		set @sql = @sql +
			' ; update CCG_TR_History set OriginalTable=''' + @tbl + ''', OriginalPeriod=' + Cast(@NewPeriod as varchar) + ', OriginalPostSeq=' +
				Cast(@NewPostSeq as varchar) + ', OriginalPKey=''' + @NewPKey + 'b'' ' +
				' where OriginalTable=''' + @tbl + ''' and OriginalPeriod=' + @OrigPeriod + ' and OriginalPostSeq=' + @OrigPostSeq + ' and OriginalPKey=''' + @OrigPkey + ''' '
		print @sql
		execute(@sql)
	END TRY
	BEGIN CATCH
		declare @ErrorMessage nvarchar(4000), @ErrorSeverity int, @ErrorState int
		select @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
		-- Use RAISERROR inside the CATCH block to return error information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
	END CATCH
END
GO
