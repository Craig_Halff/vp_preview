SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[HAI_spCCG_NewHireNotification]
	@Original					VARCHAR(5)
	, @Supervisor				NVARCHAR(20)
	, @FirstName				NVARCHAR(75)
	, @LastName					NVARCHAR(75)
	, @CustHireTermDate			NVARCHAR(20)
	, @BillingCategory			NVARCHAR(20)
	, @Title					NVARCHAR(50)
	, @Org						NVARCHAR(20)
	, @CustHireMentor			NVARCHAR(500)
	, @CustHireComputerSetup	NVARCHAR(500)
	, @CustHirePhoneRequired	NVARCHAR(500)
	, @CustHireOfficeSpaceLoc	NVARCHAR(500)
	, @CustHireComments			NVARCHAR(500)
AS

BEGIN
	SET NOCOUNT ON

	DECLARE @Body						NVARCHAR(MAX) = 'N'
		, @CSS							NVARCHAR(MAX) = 'N'
		, @SupervisorFName				NVARCHAR(100)
		, @SupervisorEmail				NVARCHAR(100)
		, @BillingCategoryDesc			NVARCHAR(50)
		, @CustHireMentorName			NVARCHAR(255)
		, @CustHireComputerSetupName	NVARCHAR(255)

	-- Remove extra whitespace
	SET @Original = RTRIM(LTRIM(@Original))
	SET @Supervisor = RTRIM(LTRIM(@Supervisor))
	SET @FirstName = RTRIM(LTRIM(@FirstName))
	SET @LastName = RTRIM(LTRIM(@LastName))
	SET @CustHireTermDate = RTRIM(LTRIM(@CustHireTermDate))
	SET @BillingCategory = RTRIM(LTRIM(@BillingCategory))
	SET @Title = RTRIM(LTRIM(@Title))
	SET @Org = RTRIM(LTRIM(@Org))
	SET @CustHireMentor = RTRIM(LTRIM(@CustHireMentor))
	SET @CustHireComputerSetup = RTRIM(LTRIM(@CustHireComputerSetup))
	SET @CustHirePhoneRequired = RTRIM(LTRIM(@CustHirePhoneRequired))
	SET @CustHireOfficeSpaceLoc = RTRIM(LTRIM(@CustHireOfficeSpaceLoc))
	SET @CustHireComments = RTRIM(LTRIM(@CustHireComments))

	-- Set to NULL if empty string
	IF @Supervisor = '' SET @Supervisor = NULL
	IF @FirstName = '' SET @FirstName = NULL
	IF @LastName = '' SET @LastName = NULL
	IF @CustHireTermDate = '' SET @CustHireTermDate = NULL
	IF @BillingCategory = '' SET @BillingCategory = NULL
	IF @Title = '' SET  @Title = NULL
	IF @Org = '' SET @Org = NULL
	IF @CustHireMentor = '' SET @CustHireMentor = NULL
	IF @CustHireComputerSetup = '' SET @CustHireComputerSetup = NULL
	IF @CustHirePhoneRequired = '' SET @CustHirePhoneRequired = NULL
	IF @CustHireOfficeSpaceLoc = '' SET @CustHireOfficeSpaceLoc = NULL
	IF @CustHireComments = '' SET @CustHireComments = NULL

	-- Lookups
	SELECT TOP 1 @SupervisorFName = FirstName
	,@SupervisorEmail = Email
	FROM [dbo].[EM]
	WHERE Employee = @Supervisor

	IF @BillingCategory IS NOT NULL
		SELECT TOP 1 @BillingCategoryDesc = Description
		FROM [dbo].[BTLaborCats]
		WHERE Category = @BillingCategory

	IF @CustHireMentor IS NOT NULL
		SELECT TOP 1 @CustHireMentorName = FirstName + ' ' + LastName 
		FROM [dbo].[EM]
		WHERE Employee = @CustHireMentor 

	IF @CustHireComputerSetup IS NOT NULL
		SELECT TOP 1 @CustHireComputerSetupName = FirstName + ' ' + LastName 
		FROM [dbo].[EM]
		WHERE Employee = @CustHireComputerSetup

	-- Error checking
	IF @FirstName IS NULL
		THROW 51000, 'First Name param cannot be null or blank.', 1;
	IF @LastName IS NULL
		THROW 51000, 'Last Name param cannot be null or blank.', 1;
	IF @SupervisorEmail IS NULL OR @SupervisorFName IS NULL
		THROW 51000, 'Supervisor param is blank or invalid.', 1; 

	-- CSS style rules
	SET @CSS = 
'
<style type="text/css">
	.lbl { font-weight:bold; }
	.hilite { background-color: #FFFF00;}
</style>
'

	SET @Body = 
'<p>' + @SupervisorFName + ',</p>' + 
'<p>Please confirm the following information and reply with the missing information in yellow.</p>' + 
'<table>' + 
	'<tr>' + 
		'<td class="lbl">Name:</td>' +  
		'<td>' + 
			ISNULL(@FirstName, '') + ' ' + ISNULL(@LastName, '') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Start Date:</td>' +  
		'<td>' + 
			ISNULL(@CustHireTermDate, '--') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Labor Category:</td>' +  
		'<td>' + 
			ISNULL(@BillingCategoryDesc, '--') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Title:</td>' +  
		'<td>' + 
			ISNULL(@Title, '--') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Organization:</td>' +  
		'<td>' + 
			ISNULL(@Org, '--') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Mentor:</td>' +  
		'<td>' + 
			ISNULL(@CustHireMentorName, '<i class="hilite">Employee Name</i>') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Setup computer like:</td>' +  
		'<td>' + 
			ISNULL(@CustHireComputerSetupName, '<i class="hilite">Employee Name</i>') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Phone required:</td>' +  
		'<td>' + 
			ISNULL(@CustHirePhoneRequired, '<i class="hilite">Yes or No</i>') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Office space:</td>' +  
		'<td>' + 
			ISNULL(@CustHireOfficeSpaceLoc, '<i class="hilite">Description of Location</i>') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Comments:</td>' +  
		'<td>' + 
			ISNULL(@CustHireComments, '<i class="hilite">Any Special Requirements</i>') + 
		'</td>' +  
	'</tr>' + 
'</table>' + 
'<br />' + 
'<p>Thanks,</p>' + 
'<p>Carlie Money</p>'


	--Email Recipient Information Below
	INSERT INTO CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
	VALUES 
	(
		'New Hire Notification', 
		'EmployeeNotice@halff.com', 
		'jgray@halff.com', -- @SupervisorEmail 
 		NULL, 
		CASE WHEN @Original = 'N' 
			THEN 'Reminder: ' 
			ELSE '' 
		END + 'New Hire Information Required - ' + @FirstName + ' ' + @LastName, 
		@CSS + @Body, 
		getDate(), 
		0
	)

END
GO
