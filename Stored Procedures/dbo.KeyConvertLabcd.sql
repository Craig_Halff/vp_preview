SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyConvertLabcd]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@UserName Nvarchar(32) = 'VisionKeyConvert', -- Add by DP 4.0
@Period integer = 0,
@EntityType Nvarchar(20)
as
begin -- Procedure
	set nocount on 
	declare @OldValue Nvarchar(20),
			  @OldValue2 Nvarchar(20)
	declare @NewValue Nvarchar(20)
	declare @LCLevel integer
	Declare @Existing integer
	declare @Entity Nvarchar(20)
	declare @length integer
	declare @CvtType integer
	declare @KeysFetch integer
	declare @RetVal integer
	declare @diag smallint
	declare @sqlstr Nvarchar(1000)
	declare @Errmsg2 Nvarchar(1000),
			@custlabel	Nvarchar(40),
			@Custlabelplural Nvarchar(40)
	declare @message nvarchar(max)
	set @diag = 0
	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'labcd'
	set @length = 20
	set @Existing = 0
	set @KeysFetch = 0
	execute InsertKeyCvtDriver @Entity -- populate driver table

-- Add by DP 4.0
	delete from keyconvertDriver where entity = N'labcd' and TableName = N'CFGLCCodesData'
	delete from keyconvertDriver where entity = N'labcd' and TableName = N'CFGLCCodesDescriptions'
	DECLARE @OldName Nvarchar(40)
	DECLARE @NewName Nvarchar(40)
	DECLARE @PKey Nvarchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--
	select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'labcdLabel'
	select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'labcdLabelPlural'

	declare KeysCursor cursor for
		select OldKey,OldKey2,convert(integer, OldKey2) as LCLevel, NewKey, Pkey from KeyConvertWork where Entity = @EntityType
	open KeysCursor
	fetch next from KeysCursor into 
		@OldValue,
		@OldValue2,
		@LCLevel,
		@NewValue, 
		@PKey

	set @KeysFetch = @@Fetch_Status

While (@KeysFetch = 0)
	begin
		begin tran
-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @EntityType)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),N'KC','Y',  @custlabel + ' Key Convert ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, left(@custlabel + ' Key Convert', 40), @custlabel);
			end

			Set @PostControlFlag = 1
		end
		set @Existing = 0
		select @Existing =  1, @NewName = Label from CFGLCCodes where Code = @NewValue and LCLevel = @LCLevel
		select @OldName = Label from CFGLCCodes where Code = @OldValue and LCLevel = @LCLevel
--
	
	If (@Existing = 1)
		begin
			  if (@DeleteExisting = 0)
				begin
				SET @message = dbo.GetMessage('MsgNumLevelExistConvNotContinue',@custlabel,@NewValue,@OldValue2,@custlabel,@custlabelPlural,'','','','')
				RAISERROR(@message,16,3)				
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end
			if (@NewValue = @OldValue)
				begin
				SET @message = dbo.GetMessage('MsgCvtLabcdAreTheSamevalue',@custlabelPlural,'','','','','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end

				declare 	@LC1Start                 smallint,
							@LC1Length                smallint,
							@LC2Start                 smallint,
							@LC2Length                smallint,
							@LC3Start                 smallint,
							@LC3Length                smallint,
							@LC4Start                 smallint,
							@LC4Length                smallint,
							@LC5Start                 smallint,
							@LC5Length                smallint,
							@LClevels					  smallint,
							@LCDelimiter			     Nvarchar(1),
							@retWhere					  Nvarchar(200),
							@level						  smallint,
							@UpdateValue				  Nvarchar(500),
							@UpdateValue2				  Nvarchar(500)

				select 	@LC1Start 	= 	LC1Start,
							@LC1Length =	LC1Length,
							@LC2Start 	=	LC2Start,
							@LC2Length =	LC2Length,
							@LC3Start 	=	LC3Start,
							@LC3Length =	LC3Length,
							@LC4Start 	=	LC4Start,
							@LC4Length =	LC4Length,
							@LC5Start =		LC5Start,
							@LC5Length =	LC5Length,
							@LCLevels = 	LCLevels,
							@LCDelimiter = LCDelimiter from CFGFormat
							set @level = convert(integer, @oldvalue2)
							exec KeyNewValLabcd
								@ColumnName 	= 'LaborCode',
								@RetValue 		=	@UpdateValue output,
								@retWhere 		= 	@retWhere output,
								@level 			= @level,
								@NewValue 		=@NewValue,
								@OldValue 		=@OldValue,
								@LC1Start    	=@LC1Start,
								@LC1Length   	=@LC1Length,
								@LC2Start    	=@LC2Start,
								@LC2Length   	=@LC2Length,
								@LC3Start    	=@LC3Start,
								@LC3Length   	=@LC3Length,
								@LC4Start    	=@LC4Start,
								@LC4Length   	=@LC4Length,
								@LC5Start    	=@LC5Start,
								@LC5Length   	=@LC5Length,
								@LClevels	 	=@LCLevels,
							   @Delimiter = @LCDelimiter
				set @level = convert(Nvarchar, @oldvalue2)

				set @updateValue2 = replace(@updateValue,'''' + @newValue + '''' ,'''' + @OldValue + '''')
				set @SqlStr =  'update LB   ' +
					'set lb.BillRate = lb.BillRate + old.BillRate,  ' +
						'lb.HrsBud = lb.HrsBud + old.HrsBud,  ' +
						'lb.AmtBud = lb.AmtBud + old.AmtBud,  ' +
						'lb.BillBud = lb.BillBud + old.BillBud,  ' +
						'lb.EtcHrs = lb.EtcHrs + old.EtcHrs,  ' +
						'lb.EtcAmt = lb.EtcAmt + old.EtcAmt,  ' +
						'lb.EacHrs = lb.EacHrs + old.EacHrs,  ' +
						'lb.EacAmt = lb.EacAmt + old.EacAmt  ' +
					'from lb inner join LB old on  lb.WBS1 = old.WBS1 and  ' +
						'lb.WBS2 = old.wbs2 and  ' +
						'lb.WBS3 = old.Wbs3  ' +
					'where ' +
						'lb.LaborCode = ' + replace(@updateValue,'LaborCode','lb.LaborCode') +  ' and ' +
						'old.LaborCode = ' + replace(@updateValue2,'LaborCode','lb.LaborCode')
				if (@Diag <> 0)
					print @SqlStr
				execute(@Sqlstr)
				
				set @Sqlstr = 'delete from LB where ' + @retWhere + ' ' +
					'and exists (select 1 from LB new where  new.WBS1 = lb.wbs1 and ' +
					'new.WBS2 = lb.wbs2 and ' +
					'new.wbs3 = lb.wbs3 and ' +
					'new.LaborCode = ' + replace(@updateValue,'LaborCode','lb.LaborCode') + ') '
				if (@Diag <> 0)
					print @SqlStr
				execute(@Sqlstr)
				set @sqlstr = 'delete from BTRLTCodes  where ' + replace(@retWhere,'LaborCode','LaborCodeMask') + ' and ' +
					' exists (select 1 from BTRLTCodes new where LaborCodeMask = ' + replace(@updateValue,'LaborCode','BTRLTCodes.LaborCodeMask') +
					'and BTRLTCodes.tableNo = new.tableNo)'
				if (@Diag <> 0)
					print @SqlStr
				execute(@Sqlstr)

				set @sqlstr = 'delete from CostLTCodes  where ' + replace(@retWhere,'LaborCode','LaborCodeMask') + ' and ' +
					' exists (select 1 from CostLTCodes new where LaborCodeMask = ' + replace(@updateValue,'LaborCode','CostLTCodes.LaborCodeMask') +
					'and CostLTCodes.tableNo = new.tableNo)'
				if (@Diag <> 0)
					print @SqlStr
				execute(@Sqlstr)

		END --Existing
	else 
		Begin
-- Add by DP 4.0
			select * into #TempKeyConvert from CFGLCCodesData where Code = @OldValue and LCLevel = @LCLevel
			Update #TempKeyConvert set Code=@NewValue
			Insert CFGLCCodesData select * from #TempKeyConvert
			Drop Table #TempKeyConvert

			select * into #TempKeyConvert1 from CFGLCCodesDescriptions where Code = @OldValue and LCLevel = @LCLevel
			Update #TempKeyConvert1 set Code=@NewValue
			Insert CFGLCCodesDescriptions select * from #TempKeyConvert1
			Drop Table #TempKeyConvert1
			set @NewName = @OldName
		end

		set @sqlstr = 'update EMCompany set DefaultLC' + convert(Nvarchar,@LCLevel) +  '=N''' + @newValue + ''' where DefaultLC' +convert(Nvarchar,@LCLevel) + ' = N''' + @oldValue + ''''
		if (@Diag <> 0)
			print @SqlStr
		execute(@Sqlstr)

		exec @RetVal = KeyCvt @Entity = @Entity,
					 @CvtType = @CvtType,
					 @Length = 14,
					 @ErrMsg = @ErrMsg2 output,
					 @Diag = @Diag,
					 @NewValue = @NewValue,
					 @OldValue = @OldValue,
					 @Oldvalue2 = @OldValue2,
					 @ConstrFlag = 3 -- UpdateTables

		delete from CFGLCCodesData where code = @oldValue and lclevel = @LClevel
		delete from CFGLCCodesDescriptions where code = @oldValue and lclevel = @LClevel
--
		if (@RetVal = 0)
			begin

			If (@Existing = 1)
				begin
							
--*****************************************************************************************************************
-- ****************************************
-- check duplicate tasks including labor code and merge them
-- ****************************************

				declare @PlanID		varchar(32)
				declare @OldTaskID		varchar(32)
				declare @NewTaskID		varchar(32)
				declare @ResourceID		Nvarchar(20),
						@OldAssignmentID	Nvarchar(32),
						@NewAssignmentID	Nvarchar(32)

				declare @TempFetch integer
				declare @TempFetch2 integer
				declare @WBS1	Nvarchar(30),
						@WBS2	Nvarchar(30),
						@WBS3	Nvarchar(30)
				declare @LaborCode	Nvarchar(14),
						@GenericResourceID		Nvarchar(20),
						@WBSType	Nvarchar(4)
				declare @Account		Nvarchar(13),
						@Vendor		Nvarchar(20),
						@OldExpenseID	varchar(32),
						@NewExpenseID	varchar(32)
				declare @OldConsultantID	varchar(32),
						@NewConsultantID	varchar(32)
				declare @OldUnitID	varchar(32),
						@NewUnitID	varchar(32),
						@Unit		Nvarchar(30)

				declare TaskCursor cursor for
					select A.PlanID, A.WBS1, A.WBS2, A.WBS3, A.LaborCode, A.WBSType, PNTask.TaskID as TaskID From
					(
					select PlanID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, isnull(WBSType,'') as WBSType, MIN(OutlineNumber) as OutlineNumber  
					from PNTask
					group by PlanID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(LaborCode,''), isnull(WBSType,'')
					Having count(TaskID) > 1
					) A
					Left Join PNTask on PNTask.PlanID=A.PlanID and PNTask.WBS1=A.WBS1 and isnull(PNTask.WBS2,'')=A.WBS2 and isnull(PNTask.WBS3,'')=A.WBS3 and isnull(PNTask.LaborCode,'')=A.LaborCode 
					and isnull(PNTask.WBSType,'')=A.WBSType and PNTask.OutlineNumber=A.OutlineNumber 
				open TaskCursor
				fetch next from TaskCursor into @PlanID, @WBS1, @WBS2, @WBS3, @LaborCode, @WBSType, @NewTaskID
				set @TempFetch = @@Fetch_Status
				While (@TempFetch = 0)
				begin
if (@diag = 1) Print 'Check the duplicate Tasks ' + @PlanID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+@WBSType
					declare DupTaskCursor cursor for
						select TaskID from PNTask 
						where PlanID=@PlanID and WBS1=@WBS1 
						and isnull(WBS2,'')=@WBS2 
						and isnull(WBS3,'')=@WBS3 
						and isnull(LaborCode,'')=@LaborCode 
						and isnull(WBSType,'')=@WBSType
						and TaskID <> @NewTaskID 
					open DupTaskCursor
					fetch next from DupTaskCursor into @OldTaskID
					set @TempFetch2 = @@Fetch_Status
					While (@TempFetch2 = 0)
					begin
if (@diag = 1) Print 'Merging the duplicate Tasks ' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+@WBSType
						Update RPAssignment Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPPlannedLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPBaselineLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPBaselineRevenue Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPExpense Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPPlannedExpenses Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPBaselineExpenses Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPPlannedConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPBaselineConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPUnit Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPPlannedUnit Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPBaselineUnit Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID

						Update PNAssignment Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNPlannedLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNBaselineLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNExpense Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNPlannedExpenses Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNBaselineExpenses Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNPlannedConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNBaselineConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID

						Update RPPlannedRevenueLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPBaselineRevenueLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNPlannedRevenueLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNBaselineRevenueLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID

						PRINT '>>> Start Processing PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '"'
						EXECUTE dbo.stRPDelTask @PlanID, @OldTaskID
						PRINT '<<< End Processing PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '"'

						fetch next from DupTaskCursor into @OldTaskID
						set @TempFetch2 = @@Fetch_Status
					end
					close DupTaskCursor
					deallocate DupTaskCursor

					PRINT '>>> Start Processing PlanID = "' + @PlanID + '"'
					EXECUTE dbo.stRPSyncWBSStructure @PlanID
					Execute dbo.RPDeleteSummaryTPD @PlanID, 'Y','Y','Y'
					Execute dbo.RPCalcTPD @PlanID, 'Y','Y','Y'
					Execute dbo.RPSumUpTPD @PlanID, 'Y','Y','Y'
					Execute dbo.PNDeleteSummaryTPD @PlanID, 'Y','Y','Y'
					Execute dbo.PNCalcTPD @PlanID, 'Y','Y','Y'
					Execute dbo.PNSumUpTPD @PlanID, 'Y','Y','Y'
					PRINT '<<< End Processing PlanID = "' + @PlanID + '"'

					fetch next from TaskCursor into @PlanID, @WBS1, @WBS2, @WBS3, @LaborCode, @WBSType, @NewTaskID
					set @TempFetch = @@Fetch_Status
				end
				close TaskCursor
				deallocate TaskCursor

-- Move Planned and Basedline for the duplicate RPAssignment/PNAssignment, Delete duplicate RPAssignment/PNAssignment
				declare AssignmentCursor cursor for
					select PlanID, TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, isnull(ResourceID,'') as ResourceID
					, isnull(GenericResourceID,'') as GenericResourceID, MIN(AssignmentID) as AssignmentID  
					from RPAssignment
					group by PlanID, TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(LaborCode,''), isnull(ResourceID,''), isnull(GenericResourceID,'')
					Having count(AssignmentID) > 1
				open AssignmentCursor
				fetch next from AssignmentCursor into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @LaborCode, @ResourceID, @GenericResourceID, @NewAssignmentID
				set @TempFetch = @@Fetch_Status
				While (@TempFetch = 0)
				begin
if (@diag = 1) Print 'Check the duplicate Assignment ' + @PlanID +'/' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+@ResourceID +'/'+@GenericResourceID
					declare DupAssignmentCursor cursor for
						select AssignmentID from RPAssignment 
						where PlanID=@PlanID and TaskID=@OldTaskID and WBS1=@WBS1 
						and isnull(WBS2,'')=@WBS2 
						and isnull(WBS3,'')=@WBS3 
						and isnull(LaborCode,'')=@LaborCode 
						and isnull(ResourceID,'')=@ResourceID
						and isnull(GenericResourceID,'')=@GenericResourceID
						and AssignmentID <> @NewAssignmentID
					open DupAssignmentCursor
					fetch next from DupAssignmentCursor into @OldAssignmentID
					set @TempFetch2 = @@Fetch_Status
					While (@TempFetch2 = 0)
					begin
if (@diag = 1) Print 'Merging the duplicate Assignment ' + @PlanID +'/' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+@ResourceID +'/'+@GenericResourceID
						Update RPPlannedLabor Set AssignmentID=@NewAssignmentID Where PlanID=@PlanID and TaskID=@OldTaskID and AssignmentID=@OldAssignmentID
						Update RPBaselineLabor Set AssignmentID=@NewAssignmentID Where PlanID=@PlanID and TaskID=@OldTaskID and AssignmentID=@OldAssignmentID
						Update RPBaselineRevenue Set AssignmentID=@NewAssignmentID Where PlanID=@PlanID and TaskID=@OldTaskID and AssignmentID=@OldAssignmentID

						Update PNPlannedLabor Set AssignmentID=@NewAssignmentID Where PlanID=@PlanID and TaskID=@OldTaskID and AssignmentID=@OldAssignmentID
						Update PNBaselineLabor Set AssignmentID=@NewAssignmentID Where PlanID=@PlanID and TaskID=@OldTaskID and AssignmentID=@OldAssignmentID

						PRINT '>>> Start Deleting Assignment PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '", AssignmentID = "' + @OldAssignmentID + '"'
						delete RPAssignment Where PlanID=@PlanID and TaskID=@OldTaskID and AssignmentID=@OldAssignmentID
						delete PNAssignment Where PlanID=@PlanID and TaskID=@OldTaskID and AssignmentID=@OldAssignmentID
						PRINT '<<< End Deleting Assignment PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '", AssignmentID = "' + @OldAssignmentID + '"'

						fetch next from DupAssignmentCursor into @OldAssignmentID
						set @TempFetch2 = @@Fetch_Status
					end
					close DupAssignmentCursor
					deallocate DupAssignmentCursor

					PRINT '>>> Start Processing PlanID = "' + @PlanID + '"'
					EXECUTE dbo.stRPSyncWBSStructure @PlanID
					Execute dbo.RPDeleteSummaryTPD @PlanID, 'Y','Y','Y'
					Execute dbo.RPCalcTPD @PlanID, 'Y','Y','Y'
					Execute dbo.RPSumUpTPD @PlanID, 'Y','Y','Y'
					Execute dbo.PNDeleteSummaryTPD @PlanID, 'Y','Y','Y'
					Execute dbo.PNCalcTPD @PlanID, 'Y','Y','Y'
					Execute dbo.PNSumUpTPD @PlanID, 'Y','Y','Y'
					PRINT '<<< End Processing PlanID = "' + @PlanID + '"'

					fetch next from AssignmentCursor into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @LaborCode, @ResourceID, @GenericResourceID, @NewAssignmentID
					set @TempFetch = @@Fetch_Status
				end
				close AssignmentCursor
				deallocate AssignmentCursor

				end
--*****************************************************************************************************************

-- Add by DP 4.0
				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq, OldKey2)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq, OldKey2 from keyConvertWork where Pkey = @PKey
				end
--
				delete from keyConvertWork where current of keysCursor
				commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
				rollback transaction
			end
	fetch next from KeysCursor into 
		@OldValue,
		@OldValue2,
		@LCLevel,
		@NewValue, 
		@PKey

	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	return(0)
End -- Procedure


GO
