SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_Config_Get_Version]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	SELECT version, currentperiod, appurl as visionurl, MulticompanyEnabled, MulticurrencyEnabled, 
			APExpenseCodeEnabled, APExpenseCodeRequired, BalanceSheet, DuplicateVendorInvoiceWarning
        FROM CFGSystem

	-- Check if WBS3 is being used
	SELECT top 1 'Y' as WBS3Level
		FROM PR 
		WHERE WBS1 <> N' ' and WBS2 <> N' ' and WBS3 <> N' ' and [Status] <> 'D'
END
GO
