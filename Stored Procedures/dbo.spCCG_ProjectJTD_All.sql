SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_ProjectJTD_All]
/*
Copyright (c) 2017 Central Consulting Group. All rights reserved.
08/11/2017 David Springer
           Populate project UDF fields for JTD values.
           Call this from a Project scheduled workflow and a button on the project info center.
08/18/2017 David Springer
           Added Billed Tax
12/15/2020 Craig H. Anderson
		   Added exclusions for CustUnbilledAmountBilling and CustUnbilledTransactions where SuppressBill = 'Y'
06/01/2021	Craig H. Anderson
			Added  "WITH (NOLOCK)" hints and removed HalffImport db name references.

06/21/2021	Craig H. Anderson - New version which updates values for all Active Chargeable projects.
			
*/
AS
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;
    BEGIN
        DECLARE @currentPeriod INT = DATEPART(YEAR, GETDATE()) * 100 + DATEPART(MONTH, GETDATE());
        DECLARE @firstDayOfPeriod DATE = LEFT(CAST(@currentPeriod AS VARCHAR(8)), 4) + RIGHT(CAST(@currentPeriod AS VARCHAR(8)), 2) + '01';
        DECLARE @lastDayOfPeriod DATE = EOMONTH(@firstDayOfPeriod);

        -- Reset Values that may no longer exist; others will always exist and be incrementing
        UPDATE px
            SET px.CustUnbilledAmountBilling = 0
              , px.CustUnbilledTransactions = 0
              , px.CustJTDRevenue = 0
              , px.CustYTDRevenue = 0
              , px.CustJTDARBalance = 0
              , px.CustJTDBilled = 0
              , px.CustJTDBilledTax = 0
              , px.CustYTDBilled = 0
              , px.CustJTDReceived = 0
              , px.CustYTDReceived = 0
              , px.CustJTDSpentBilling = 0
              , px.CustYTDSpentBilling = 0
              , px.CustJTDProfit = 0
              , px.CustYTDProfit = 0
              , px.CustJTDProfitPercent = 0
              , px.CustYTDProfitPercent = 0
              , px.CustJTDWorkinProcess = 0
              , px.CustJTDVariance = 0
              , px.CustYTDVariance = 0
              , px.CustJTDGoalMultiplier = 0
              , px.CustYTDGoalMultiplier = 0
              , px.CustJTDEffectiveMultiplier = 0
              , px.CustYTDEffectiveMultiplier = 0
            FROM dbo.ProjectCustomTabFields                                                             px
            INNER JOIN (SELECT WBS1 FROM dbo.PR WHERE WBS2 = ' ' AND ChargeType = 'R' AND Status = 'A') p
                ON px.WBS1 = p.WBS1;



        -- ##########   Project (WBS1) level data   ##########
        --p.CustUnbilledAmountBilling = IsNull (ap.APUnbilled, 0) + IsNull (ex.ExUnbilled, 0) + IsNull (m.MiscUnbilled, 0) + IsNull (l.LaborUnbilled, 0),
        UPDATE p
            SET p.CustUnbilledAmountBilling = ISNULL(dbo.fnCCG_EI_JTDUnbilledBillingThru(p.WBS1, p.WBS2, p.WBS3, @lastDayOfPeriod), 0.0)
              , p.CustUnbilledTransactions = ISNULL(ap.APQty, 0) + ISNULL(EX.ExQty, 0) + ISNULL(m.MiscQty, 0) + ISNULL(l.LaborQty, 0)
            FROM dbo.ProjectCustomTabFields                                                             p
            LEFT JOIN (
                SELECT WBS1
                     , SUM(BillExt) AS APUnbilled
                     , COUNT(*)     AS APQty
                    FROM dbo.LedgerAP WITH (NOLOCK)
                    WHERE
                    BillStatus IN ( 'B', 'H', 'D', 'W' )
                        AND SuppressBill <> 'Y'
                    GROUP BY WBS1
            )                                                                                           ap
                ON p.WBS1 = ap.WBS1
            LEFT JOIN (
                SELECT WBS1
                     , SUM(BillExt) AS ExUnbilled
                     , COUNT(*)     AS EXQty
                    FROM dbo.LedgerEX WITH (NOLOCK)
                    WHERE
                    BillStatus IN ( 'B', 'H', 'D', 'W' )
                        AND SuppressBill <> 'Y'
                    GROUP BY WBS1
            )                                                                                           ex
                ON p.WBS1 = ex.WBS1
            LEFT JOIN (
                SELECT WBS1
                     , SUM(BillExt) AS MiscUnbilled
                     , COUNT(*)     AS MiscQty
                    FROM dbo.LedgerMisc WITH (NOLOCK)
                    WHERE
                    BillStatus IN ( 'B', 'H', 'D', 'W' )
                        AND SuppressBill <> 'Y'
                    GROUP BY WBS1
            )                                                                                           m
                ON p.WBS1 = m.WBS1
            LEFT JOIN (
                SELECT WBS1
                     , SUM(BillExt) AS LaborUnbilled
                     , COUNT(*)     AS LaborQty
                    FROM dbo.LD WITH (NOLOCK)
                    WHERE
                    BillStatus IN ( 'B', 'H', 'D', 'W' )
                        AND SuppressBill <> 'Y'
                    GROUP BY WBS1
            )                                                                                           l
                ON p.WBS1 = l.WBS1
            INNER JOIN (SELECT WBS1 FROM dbo.PR WHERE WBS2 = ' ' AND ChargeType = 'R' AND Status = 'A') p1
                ON p.WBS1 = p1.WBS1
            WHERE p.WBS2 = ' '; -- Project level only


        UPDATE p
            SET p.CustJTDRevenue = ISNULL(s.JTDRevenue, 0)
              , p.CustYTDRevenue = ISNULL(s.YTDRevenue, 0)
              , p.CustJTDBilled = ISNULL(s.JTDBilled, 0)
              , p.CustJTDBilledTax = ISNULL(t.Tax, 0)
              , p.CustYTDBilled = ISNULL(s.YTDBilled, 0)
              , p.CustJTDReceived = ISNULL(s.JTDReceived, 0)
              , p.CustYTDReceived = ISNULL(s.YTDReceived, 0)
              , p.CustJTDARBalance = ISNULL(s.JTDBilled, 0) + ISNULL(t.Tax, 0) + ISNULL(t.Interest, 0) - ISNULL(s.JTDReceived, 0) - ISNULL(i.InterestPaid, 0)
              , p.CustJTDSpentBilling = ISNULL(s.JTDSpentBilling, 0)
              , p.CustYTDSpentBilling = ISNULL(s.YTDSpentBilling, 0)
              , p.CustJTDProfit = ISNULL(s.JTDProfit, 0)
              , p.CustYTDProfit = ISNULL(s.YTDProfit, 0)
              , p.CustJTDProfitPercent = ROUND(CASE WHEN ISNULL(s.JTDRevenue, 0) = 0 THEN 0 ELSE ISNULL(s.JTDProfit, 0) / s.JTDRevenue * 100 END, 2)
              , p.CustYTDProfitPercent = ROUND(CASE WHEN ISNULL(s.YTDRevenue, 0) = 0 THEN 0 ELSE ISNULL(s.YTDProfit, 0) / s.YTDRevenue * 100 END, 2)
              , p.CustJTDVariance = ISNULL(s.JTDRevenue, 0) - ISNULL(s.JTDSpentBilling, 0)
              , p.CustYTDVariance = ISNULL(s.YTDRevenue, 0) - ISNULL(s.YTDSpentBilling, 0)
              , p.CustJTDWorkinProcess = ISNULL(s.WIP, 0)
            FROM dbo.ProjectCustomTabFields                                                             p
            INNER JOIN (SELECT WBS1 FROM dbo.PR WHERE WBS2 = ' ' AND ChargeType = 'R' AND Status = 'A') p1
                ON p.WBS1 = p1.WBS1
            LEFT JOIN (
                SELECT WBS1
                     , SUM(Revenue)                                                                                           AS JTDRevenue
                     , SUM(CASE WHEN ROUND(Period / 100, 0, 1) = YEAR(GETDATE()) THEN Revenue ELSE 0 END)                     AS YTDRevenue
                     , SUM(Billed)                                                                                            AS JTDBilled
                     , SUM(CASE WHEN ROUND(Period / 100, 0, 1) = YEAR(GETDATE()) THEN Billed ELSE 0 END)                      AS YTDBilled
                     , SUM(Received)                                                                                          AS JTDReceived
                     , SUM(CASE WHEN ROUND(Period / 100, 0, 1) = YEAR(GETDATE()) THEN Received ELSE 0 END)                    AS YTDReceived
                     , SUM(SpentBilling)                                                                                      AS JTDSpentBilling
                     , SUM(CASE WHEN ROUND(Period / 100, 0, 1) = YEAR(GETDATE()) THEN SpentBilling ELSE 0 END)                AS YTDSpentBilling
                     , SUM(ProfitCostLessOH - Overhead)                                                                       AS JTDProfit
                     , SUM(CASE WHEN ROUND(Period / 100, 0, 1) = YEAR(GETDATE()) THEN ProfitCostLessOH - Overhead ELSE 0 END) AS YTDProfit
                     , SUM(Unbilled)                                                                                          AS WIP
                    FROM dbo.PRSummaryMain WITH (NOLOCK)
                    GROUP BY WBS1
            )                                                                                           s
                ON s.WBS1 = p.WBS1
            LEFT JOIN (
                SELECT WBS1
                     , -SUM(CASE WHEN InvoiceSection = 'T' THEN Amount ELSE 0 END) AS Tax
                     , -SUM(CASE WHEN InvoiceSection = 'I' THEN Amount ELSE 0 END) AS Interest
                    FROM dbo.LedgerAR WITH (NOLOCK)
                    WHERE
                    TransType = 'IN'
                        AND InvoiceSection IN ( 'I', 'T' )
                    GROUP BY WBS1
                    HAVING SUM(Amount) <> 0
            )                                                                                           t
                ON t.WBS1 = p.WBS1
            LEFT JOIN (
                SELECT WBS1
                     , -SUM(Amount) AS InterestPaid
                    FROM dbo.LedgerAR WITH (NOLOCK)
                    WHERE
                    TransType = 'CR'
                        AND Account = '407.00' -- Interest Revenue
                    GROUP BY WBS1
                    HAVING SUM(Amount) <> 0
            )                                                                                           i
                ON i.WBS1 = p.WBS1
            WHERE p.WBS2 = ' '; -- Project level only

        -- JTD Multipliers
        UPDATE p
            SET p.CustJTDGoalMultiplier = ROUND(ISNULL(s.JTDGoalMult, 0), 2)
              , p.CustJTDEffectiveMultiplier = ROUND(ISNULL(s.JTDEffectiveMult, 0), 2)
            FROM dbo.ProjectCustomTabFields                                                             p
            INNER JOIN (SELECT WBS1 FROM dbo.PR WHERE WBS2 = ' ' AND ChargeType = 'R' AND Status = 'A') p1
                ON p.WBS1 = p1.WBS1
            LEFT JOIN (
                SELECT WBS1
                     , SUM(LaborBilling + DirectConsBilling + DirectOtherBilling + ReimbConsBilling + ReimbOtherBilling - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost)
                       / SUM(LaborCost)                                                                                  AS JTDGoalMult
                     , SUM(Revenue - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) / SUM(LaborCost) AS JTDEffectiveMult
                    FROM dbo.PRSummarySub WITH (NOLOCK)
                    GROUP BY WBS1
                    HAVING SUM(LaborCost) <> 0
            )                                                                                           s
                ON s.WBS1 = p.WBS1
            WHERE p.WBS2 = ' '; -- Project level only

        -- YTD Multipliers
        UPDATE p
            SET p.CustYTDGoalMultiplier = ROUND(ISNULL(s.YTDGoalMult, 0), 2)
              , p.CustYTDEffectiveMultiplier = ROUND(ISNULL(s.YTDEffectiveMult, 0), 2)
            FROM dbo.ProjectCustomTabFields                                                             p
            INNER JOIN (SELECT WBS1 FROM dbo.PR WHERE WBS2 = ' ' AND ChargeType = 'R' AND Status = 'A') p1
                ON p.WBS1 = p1.WBS1
            LEFT JOIN (
                SELECT WBS1
                     , SUM(LaborBilling + DirectConsBilling + DirectOtherBilling + ReimbConsBilling + ReimbOtherBilling - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost)
                       / SUM(LaborCost)                                                                                  AS YTDGoalMult
                     , SUM(Revenue - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) / SUM(LaborCost) AS YTDEffectiveMult
                    FROM dbo.PRSummarySub WITH (NOLOCK)
                    WHERE ROUND(Period / 100, 0, 1) = YEAR(GETDATE())
                    GROUP BY WBS1
                    HAVING SUM(LaborCost) <> 0
            )                                                                                           s
                ON s.WBS1 = p.WBS1
            WHERE p.WBS2 = ' '; -- Project level only

        -- CustLastChargeDate
        UPDATE p
            SET p.CustLastChargeDate = A.MaxTransDate
            FROM dbo.ProjectCustomTabFields                                                             p
            INNER JOIN (SELECT WBS1 FROM dbo.PR WHERE WBS2 = ' ' AND ChargeType = 'R' AND Status = 'A') p1
                ON p.WBS1 = p1.WBS1
            LEFT JOIN (
                SELECT B.WBS1
                     , MAX(B.TransDate) AS MaxTransDate
                    FROM (
                        SELECT WBS1
                             , MAX(TransDate) AS TransDate
                            FROM dbo.LD WITH (NOLOCK)
                            GROUP BY WBS1
                        UNION ALL
                        SELECT WBS1
                             , MAX(TransDate) AS TransDate
                            FROM dbo.LedgerMisc WITH (NOLOCK)
                            WHERE BillStatus IS NOT NULL
                            GROUP BY WBS1
                        UNION ALL
                        SELECT WBS1
                             , MAX(TransDate) AS TransDate
                            FROM dbo.LedgerEX WITH (NOLOCK)
                            GROUP BY WBS1
                        UNION ALL
                        SELECT WBS1
                             , MAX(TransDate) AS TransDate
                            FROM.dbo.LedgerAP WITH (NOLOCK)
                            GROUP BY WBS1
                    ) AS B
                    GROUP BY B.WBS1
            )                                                                                           AS A
                ON A.WBS1 = p.WBS1
            WHERE p.WBS2 = ' ';


        -- ##########   Phase (WBS2) level data   ##########

        UPDATE p
            SET p.CustUnbilledAmountBilling = ISNULL(dbo.fnCCG_EI_JTDUnbilledBillingThru(p.WBS1, p.WBS2, p.WBS3, @lastDayOfPeriod), 0.0)
              , p.CustUnbilledTransactions = ISNULL(ap.APQty, 0) + ISNULL(EX.ExQty, 0) + ISNULL(m.MiscQty, 0) + ISNULL(l.LaborQty, 0)
            FROM dbo.ProjectCustomTabFields                                                             p
            INNER JOIN (SELECT WBS1 FROM dbo.PR WHERE WBS2 = ' ' AND ChargeType = 'R' AND Status = 'A') p1
                ON p.WBS1 = p1.WBS1
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , SUM(BillExt) AS APUnbilled
                     , COUNT(*)     AS APQty
                    FROM dbo.LedgerAP WITH (NOLOCK)
                    WHERE
                    BillStatus IN ( 'B', 'H', 'D', 'W' )
                        AND SuppressBill <> 'Y'
                    GROUP BY
                    WBS1
                  , WBS2
            )                                                                                           ap
                ON p.WBS1 = ap.WBS1
                    AND p.WBS2 = ap.WBS2
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , SUM(BillExt) AS ExUnbilled
                     , COUNT(*)     AS EXQty
                    FROM dbo.LedgerEX WITH (NOLOCK)
                    WHERE
                    BillStatus IN ( 'B', 'H', 'D', 'W' )
                        AND SuppressBill <> 'Y'
                    GROUP BY
                    WBS1
                  , WBS2
            )                                                                                           ex
                ON p.WBS1 = ex.WBS1
                    AND p.WBS2 = ex.WBS2
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , SUM(BillExt) AS MiscUnbilled
                     , COUNT(*)     AS MiscQty
                    FROM dbo.LedgerMisc WITH (NOLOCK)
                    WHERE
                    BillStatus IN ( 'B', 'H', 'D', 'W' )
                        AND SuppressBill <> 'Y'
                    GROUP BY
                    WBS1
                  , WBS2
            )                                                                                           m
                ON p.WBS1 = m.WBS1
                    AND p.WBS2 = m.WBS2
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , SUM(BillExt) AS LaborUnbilled
                     , COUNT(*)     AS LaborQty
                    FROM dbo.LD WITH (NOLOCK)
                    WHERE
                    BillStatus IN ( 'B', 'H', 'D', 'W' )
                        AND SuppressBill <> 'Y'
                    GROUP BY
                    WBS1
                  , WBS2
            )                                                                                           l
                ON p.WBS1 = l.WBS1
                    AND p.WBS2 = l.WBS2
            WHERE
            p.WBS2 <> ' ' -- Phase level only
                AND p.WBS3 = ' ';

        UPDATE p
            SET p.CustJTDRevenue = ISNULL(s.JTDRevenue, 0)
              , p.CustYTDRevenue = ISNULL(s.YTDRevenue, 0)
              , p.CustJTDBilled = ISNULL(s.JTDBilled, 0)
              , p.CustJTDBilledTax = ISNULL(t.Tax, 0)
              , p.CustYTDBilled = ISNULL(s.YTDBilled, 0)
              , p.CustJTDReceived = ISNULL(s.JTDReceived, 0)
              , p.CustYTDReceived = ISNULL(s.YTDReceived, 0)
              , p.CustJTDARBalance = ISNULL(s.JTDBilled, 0) + ISNULL(t.Tax, 0) + ISNULL(t.Interest, 0) - ISNULL(s.JTDReceived, 0) - ISNULL(i.InterestPaid, 0)
              , p.CustJTDSpentBilling = ISNULL(s.JTDSpentBilling, 0)
              , p.CustYTDSpentBilling = ISNULL(s.YTDSpentBilling, 0)
              , p.CustJTDProfit = ISNULL(s.JTDProfit, 0)
              , p.CustYTDProfit = ISNULL(s.YTDProfit, 0)
              , p.CustJTDProfitPercent = ROUND(CASE WHEN ISNULL(s.JTDRevenue, 0) = 0 THEN 0 ELSE ISNULL(s.JTDProfit, 0) / s.JTDRevenue * 100 END, 2)
              , p.CustYTDProfitPercent = ROUND(CASE WHEN ISNULL(s.YTDRevenue, 0) = 0 THEN 0 ELSE ISNULL(s.YTDProfit, 0) / s.YTDRevenue * 100 END, 2)
              , p.CustJTDVariance = ISNULL(s.JTDRevenue, 0) - ISNULL(s.JTDSpentBilling, 0)
              , p.CustYTDVariance = ISNULL(s.YTDRevenue, 0) - ISNULL(s.YTDSpentBilling, 0)
              , p.CustJTDWorkinProcess = ISNULL(s.WIP, 0)
            FROM dbo.ProjectCustomTabFields                                                             p
            INNER JOIN (SELECT WBS1 FROM dbo.PR WHERE WBS2 = ' ' AND ChargeType = 'R' AND Status = 'A') p1
                ON p.WBS1 = p1.WBS1
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , SUM(Revenue)                                                                                           AS JTDRevenue
                     , SUM(CASE WHEN ROUND(Period / 100, 0, 1) = YEAR(GETDATE()) THEN Revenue ELSE 0 END)                     AS YTDRevenue
                     , SUM(Billed)                                                                                            AS JTDBilled
                     , SUM(CASE WHEN ROUND(Period / 100, 0, 1) = YEAR(GETDATE()) THEN Billed ELSE 0 END)                      AS YTDBilled
                     , SUM(Received)                                                                                          AS JTDReceived
                     , SUM(CASE WHEN ROUND(Period / 100, 0, 1) = YEAR(GETDATE()) THEN Received ELSE 0 END)                    AS YTDReceived
                     , SUM(SpentBilling)                                                                                      AS JTDSpentBilling
                     , SUM(CASE WHEN ROUND(Period / 100, 0, 1) = YEAR(GETDATE()) THEN SpentBilling ELSE 0 END)                AS YTDSpentBilling
                     , SUM(ProfitCostLessOH - Overhead)                                                                       AS JTDProfit
                     , SUM(CASE WHEN ROUND(Period / 100, 0, 1) = YEAR(GETDATE()) THEN ProfitCostLessOH - Overhead ELSE 0 END) AS YTDProfit
                     , SUM(Unbilled)                                                                                          AS WIP
                    FROM dbo.PRSummaryMain WITH (NOLOCK)
                    GROUP BY
                    WBS1
                  , WBS2
            )                                                                                           s
                ON s.WBS1 = p.WBS1
                    AND s.WBS2 = p.WBS2
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , -SUM(CASE WHEN InvoiceSection = 'T' THEN Amount ELSE 0 END) AS Tax
                     , -SUM(CASE WHEN InvoiceSection = 'I' THEN Amount ELSE 0 END) AS Interest
                    FROM dbo.LedgerAR WITH (NOLOCK)
                    WHERE
                    TransType = 'IN'
                        AND InvoiceSection IN ( 'I', 'T' )
                    GROUP BY
                    WBS1
                  , WBS2
                    HAVING SUM(Amount) <> 0
            )                                                                                           t
                ON t.WBS1 = p.WBS1
                    AND t.WBS2 = p.WBS2
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , -SUM(Amount) AS InterestPaid
                    FROM dbo.LedgerAR WITH (NOLOCK)
                    WHERE
                    TransType = 'CR'
                        AND Account = '407.00' -- Interest Revenue
                    GROUP BY
                    WBS1
                  , WBS2
                    HAVING SUM(Amount) <> 0
            )                                                                                           i
                ON i.WBS1 = p.WBS1
                    AND i.WBS2 = p.WBS2
            WHERE
            p.WBS2 <> ' ' -- Phase level only
                AND p.WBS3 = ' ';

        -- JTD Multipliers
        UPDATE p
            SET p.CustJTDGoalMultiplier = ROUND(ISNULL(s.JTDGoalMult, 0), 2)
              , p.CustJTDEffectiveMultiplier = ROUND(ISNULL(s.JTDEffectiveMult, 0), 2)
            FROM dbo.ProjectCustomTabFields                                                             p
            INNER JOIN (SELECT WBS1 FROM dbo.PR WHERE WBS2 = ' ' AND ChargeType = 'R' AND Status = 'A') p1
                ON p.WBS1 = p1.WBS1
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , SUM(LaborBilling + DirectConsBilling + DirectOtherBilling + ReimbConsBilling + ReimbOtherBilling - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost)
                       / SUM(LaborCost)                                                                                  AS JTDGoalMult
                     , SUM(Revenue - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) / SUM(LaborCost) AS JTDEffectiveMult
                    FROM dbo.PRSummarySub WITH (NOLOCK)
                    GROUP BY
                    WBS1
                  , WBS2
                    HAVING SUM(LaborCost) <> 0
            )                                                                                           s
                ON s.WBS1 = p.WBS1
                    AND s.WBS2 = p.WBS2
            WHERE
            p.WBS2 <> ' ' -- Phase level only
                AND p.WBS3 = ' ';

        -- YTD Multipliers
        UPDATE p
            SET p.CustYTDGoalMultiplier = ROUND(ISNULL(s.YTDGoalMult, 0), 2)
              , p.CustYTDEffectiveMultiplier = ROUND(ISNULL(s.YTDEffectiveMult, 0), 2)
            FROM dbo.ProjectCustomTabFields                                                             p
            INNER JOIN (SELECT WBS1 FROM dbo.PR WHERE WBS2 = ' ' AND ChargeType = 'R' AND Status = 'A') p1
                ON p.WBS1 = p1.WBS1
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , SUM(LaborBilling + DirectConsBilling + DirectOtherBilling + ReimbConsBilling + ReimbOtherBilling - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost)
                       / SUM(LaborCost)                                                                                  AS YTDGoalMult
                     , SUM(Revenue - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) / SUM(LaborCost) AS YTDEffectiveMult
                    FROM dbo.PRSummarySub WITH (NOLOCK)
                    WHERE ROUND(Period / 100, 0, 1) = YEAR(GETDATE())
                    GROUP BY
                    WBS1
                  , WBS2
                    HAVING SUM(LaborCost) <> 0
            )                                                                                           s
                ON s.WBS1 = p.WBS1
                    AND s.WBS2 = p.WBS2
            WHERE
            p.WBS2 <> ' ' -- Phase level only
                AND p.WBS3 = ' ';

        -- CustLastChargeDate
        UPDATE p
            SET p.CustLastChargeDate = A.MaxTransDate
            FROM dbo.ProjectCustomTabFields                                                             p
            INNER JOIN (SELECT WBS1 FROM dbo.PR WHERE WBS2 = ' ' AND ChargeType = 'R' AND Status = 'A') p1
                ON p.WBS1 = p1.WBS1
            LEFT JOIN (
                SELECT B.WBS1
                     , B.WBS2
                     , MAX(B.TransDate) AS MaxTransDate
                    FROM (
                        SELECT WBS1
                             , WBS2
                             , MAX(TransDate) AS TransDate
                            FROM dbo.LD WITH (NOLOCK)
                            GROUP BY
                            WBS1
                          , WBS2
                        UNION ALL
                        SELECT WBS1
                             , WBS2
                             , MAX(TransDate) AS TransDate
                            FROM dbo.LedgerMisc WITH (NOLOCK)
                            WHERE BillStatus IS NOT NULL
                            GROUP BY
                            WBS1
                          , WBS2
                        UNION ALL
                        SELECT WBS1
                             , WBS2
                             , MAX(TransDate) AS TransDate
                            FROM dbo.LedgerEX WITH (NOLOCK)
                            GROUP BY
                            WBS1
                          , WBS2
                        UNION ALL
                        SELECT WBS1
                             , WBS2
                             , MAX(TransDate) AS TransDate
                            FROM dbo.LedgerAP WITH (NOLOCK)
                            GROUP BY
                            WBS1
                          , WBS2
                    ) AS B
                    GROUP BY
                    B.WBS1
                  , B.WBS2
            )                                                                                           AS A
                ON A.WBS1 = p.WBS1
                    AND A.WBS2 = p.WBS2
            WHERE
            p.WBS2 <> ' '
                AND p.WBS3 = ' ';

        -- ##########   Task (WBS3) level data   ##########

        UPDATE p
            SET p.CustUnbilledAmountBilling = ISNULL(dbo.fnCCG_EI_JTDUnbilledBillingThru(p.WBS1, p.WBS2, p.WBS3, @lastDayOfPeriod), 0.0)
              , p.CustUnbilledTransactions = ISNULL(ap.APQty, 0) + ISNULL(EX.ExQty, 0) + ISNULL(m.MiscQty, 0) + ISNULL(l.LaborQty, 0)
            FROM dbo.ProjectCustomTabFields                                                             p
            INNER JOIN (SELECT WBS1 FROM dbo.PR WHERE WBS2 = ' ' AND ChargeType = 'R' AND Status = 'A') p1
                ON p.WBS1 = p1.WBS1
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , WBS3
                     , SUM(BillExt) AS APUnbilled
                     , COUNT(*)     AS APQty
                    FROM dbo.LedgerAP WITH (NOLOCK)
                    WHERE
                    BillStatus IN ( 'B', 'H', 'D', 'W' )
                        AND SuppressBill <> 'Y'
                    GROUP BY
                    WBS1
                  , WBS2
                  , WBS3
            )                                                                                           ap
                ON p.WBS1 = ap.WBS1
                    AND p.WBS2 = ap.WBS2
                    AND p.WBS3 = ap.WBS3
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , WBS3
                     , SUM(BillExt) AS ExUnbilled
                     , COUNT(*)     AS EXQty
                    FROM dbo.LedgerEX WITH (NOLOCK)
                    WHERE
                    BillStatus IN ( 'B', 'H', 'D', 'W' )
                        AND SuppressBill <> 'Y'
                    GROUP BY
                    WBS1
                  , WBS2
                  , WBS3
            )                                                                                           ex
                ON p.WBS1 = ex.WBS1
                    AND p.WBS2 = ex.WBS2
                    AND p.WBS3 = ex.WBS3
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , WBS3
                     , SUM(BillExt) AS MiscUnbilled
                     , COUNT(*)     AS MiscQty
                    FROM dbo.LedgerMisc WITH (NOLOCK)
                    WHERE
                    BillStatus IN ( 'B', 'H', 'D', 'W' )
                        AND SuppressBill <> 'Y'
                    GROUP BY
                    WBS1
                  , WBS2
                  , WBS3
            )                                                                                           m
                ON p.WBS1 = m.WBS1
                    AND p.WBS2 = m.WBS2
                    AND p.WBS3 = m.WBS3
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , WBS3
                     , SUM(BillExt) AS LaborUnbilled
                     , COUNT(*)     AS LaborQty
                    FROM dbo.LD WITH (NOLOCK)
                    WHERE
                    BillStatus IN ( 'B', 'H', 'D', 'W' )
                        AND SuppressBill <> 'Y'
                    GROUP BY
                    WBS1
                  , WBS2
                  , WBS3
            )                                                                                           l
                ON p.WBS1 = l.WBS1
                    AND p.WBS2 = l.WBS2
                    AND p.WBS3 = l.WBS3
            WHERE p.WBS3 <> ' '; -- Task level only

        UPDATE p
            SET p.CustJTDRevenue = ISNULL(s.JTDRevenue, 0)
              , p.CustYTDRevenue = ISNULL(s.YTDRevenue, 0)
              , p.CustJTDBilled = ISNULL(s.JTDBilled, 0)
              , p.CustJTDBilledTax = ISNULL(t.Tax, 0)
              , p.CustYTDBilled = ISNULL(s.YTDBilled, 0)
              , p.CustJTDReceived = ISNULL(s.JTDReceived, 0)
              , p.CustYTDReceived = ISNULL(s.YTDReceived, 0)
              , p.CustJTDARBalance = ISNULL(s.JTDBilled, 0) + ISNULL(t.Tax, 0) + ISNULL(t.Interest, 0) - ISNULL(s.JTDReceived, 0) - ISNULL(i.InterestPaid, 0)
              , p.CustJTDSpentBilling = ISNULL(s.JTDSpentBilling, 0)
              , p.CustYTDSpentBilling = ISNULL(s.YTDSpentBilling, 0)
              , p.CustJTDProfit = ISNULL(s.JTDProfit, 0)
              , p.CustYTDProfit = ISNULL(s.YTDProfit, 0)
              , p.CustJTDProfitPercent = ROUND(CASE WHEN ISNULL(s.JTDRevenue, 0) = 0 THEN 0 ELSE ISNULL(s.JTDProfit, 0) / s.JTDRevenue * 100 END, 2)
              , p.CustYTDProfitPercent = ROUND(CASE WHEN ISNULL(s.YTDRevenue, 0) = 0 THEN 0 ELSE ISNULL(s.YTDProfit, 0) / s.YTDRevenue * 100 END, 2)
              , p.CustJTDVariance = ISNULL(s.JTDRevenue, 0) - ISNULL(s.JTDSpentBilling, 0)
              , p.CustYTDVariance = ISNULL(s.YTDRevenue, 0) - ISNULL(s.YTDSpentBilling, 0)
              , p.CustJTDWorkinProcess = ISNULL(s.WIP, 0)
            FROM dbo.ProjectCustomTabFields                                                             p
            INNER JOIN (SELECT WBS1 FROM dbo.PR WHERE WBS2 = ' ' AND ChargeType = 'R' AND Status = 'A') p1
                ON p.WBS1 = p1.WBS1
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , WBS3
                     , SUM(Unbilled)                                                                                          AS Unbilled
                     , SUM(Revenue)                                                                                           AS JTDRevenue
                     , SUM(CASE WHEN ROUND(Period / 100, 0, 1) = YEAR(GETDATE()) THEN Revenue ELSE 0 END)                     AS YTDRevenue
                     , SUM(Billed)                                                                                            AS JTDBilled
                     , SUM(CASE WHEN ROUND(Period / 100, 0, 1) = YEAR(GETDATE()) THEN Billed ELSE 0 END)                      AS YTDBilled
                     , SUM(Received)                                                                                          AS JTDReceived
                     , SUM(CASE WHEN ROUND(Period / 100, 0, 1) = YEAR(GETDATE()) THEN Received ELSE 0 END)                    AS YTDReceived
                     , SUM(SpentBilling)                                                                                      AS JTDSpentBilling
                     , SUM(CASE WHEN ROUND(Period / 100, 0, 1) = YEAR(GETDATE()) THEN SpentBilling ELSE 0 END)                AS YTDSpentBilling
                     , SUM(ProfitCostLessOH - Overhead)                                                                       AS JTDProfit
                     , SUM(CASE WHEN ROUND(Period / 100, 0, 1) = YEAR(GETDATE()) THEN ProfitCostLessOH - Overhead ELSE 0 END) AS YTDProfit
                     , SUM(Unbilled)                                                                                          AS WIP
                    FROM dbo.PRSummaryMain WITH (NOLOCK)
                    GROUP BY
                    WBS1
                  , WBS2
                  , WBS3
            )                                                                                           s
                ON s.WBS1 = p.WBS1
                    AND s.WBS2 = p.WBS2
                    AND s.WBS3 = p.WBS3
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , WBS3
                     , -SUM(CASE WHEN InvoiceSection = 'T' THEN Amount ELSE 0 END) AS Tax
                     , -SUM(CASE WHEN InvoiceSection = 'I' THEN Amount ELSE 0 END) AS Interest
                    FROM dbo.LedgerAR WITH (NOLOCK)
                    WHERE
                    TransType = 'IN'
                        AND InvoiceSection IN ( 'I', 'T' )
                    GROUP BY
                    WBS1
                  , WBS2
                  , WBS3
                    HAVING SUM(Amount) <> 0
            )                                                                                           t
                ON t.WBS1 = p.WBS1
                    AND t.WBS2 = p.WBS2
                    AND t.WBS3 = p.WBS3
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , WBS3
                     , -SUM(Amount) AS InterestPaid
                    FROM dbo.LedgerAR WITH (NOLOCK)
                    WHERE
                    TransType = 'CR'
                        AND Account = '407.00' -- Interest Revenue
                    GROUP BY
                    WBS1
                  , WBS2
                  , WBS3
                    HAVING SUM(Amount) <> 0
            )                                                                                           i
                ON i.WBS1 = p.WBS1
                    AND i.WBS2 = p.WBS2
                    AND i.WBS3 = p.WBS3
            WHERE p.WBS3 <> ' '; -- Task level only

        -- JTD Multipliers
        UPDATE p
            SET p.CustJTDGoalMultiplier = ROUND(ISNULL(s.JTDGoalMult, 0), 2)
              , p.CustJTDEffectiveMultiplier = ROUND(ISNULL(s.JTDEffectiveMult, 0), 2)
            FROM dbo.ProjectCustomTabFields                                                             p
            INNER JOIN (SELECT WBS1 FROM dbo.PR WHERE WBS2 = ' ' AND ChargeType = 'R' AND Status = 'A') p1
                ON p.WBS1 = p1.WBS1
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , WBS3
                     , SUM(LaborBilling + DirectConsBilling + DirectOtherBilling + ReimbConsBilling + ReimbOtherBilling - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost)
                       / SUM(LaborCost)                                                                                  AS JTDGoalMult
                     , SUM(Revenue - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) / SUM(LaborCost) AS JTDEffectiveMult
                    FROM dbo.PRSummarySub WITH (NOLOCK)
                    GROUP BY
                    WBS1
                  , WBS2
                  , WBS3
                    HAVING SUM(LaborCost) <> 0
            )                                                                                           s
                ON s.WBS1 = p.WBS1
                    AND s.WBS2 = p.WBS2
                    AND s.WBS3 = p.WBS3
            WHERE p.WBS3 <> ' '; -- Task level only

        -- YTD Multipliers
        UPDATE p
            SET p.CustYTDGoalMultiplier = ROUND(ISNULL(s.YTDGoalMult, 0), 2)
              , p.CustYTDEffectiveMultiplier = ROUND(ISNULL(s.YTDEffectiveMult, 0), 2)
            FROM dbo.ProjectCustomTabFields                                                             p
            INNER JOIN (SELECT WBS1 FROM dbo.PR WHERE WBS2 = ' ' AND ChargeType = 'R' AND Status = 'A') p1
                ON p.WBS1 = p1.WBS1
            LEFT JOIN (
                SELECT WBS1
                     , WBS2
                     , WBS3
                     , SUM(LaborBilling + DirectConsBilling + DirectOtherBilling + ReimbConsBilling + ReimbOtherBilling - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost)
                       / SUM(LaborCost)                                                                                  AS YTDGoalMult
                     , SUM(Revenue - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) / SUM(LaborCost) AS YTDEffectiveMult
                    FROM dbo.PRSummarySub WITH (NOLOCK)
                    WHERE ROUND(Period / 100, 0, 1) = YEAR(GETDATE())
                    GROUP BY
                    WBS1
                  , WBS2
                  , WBS3
                    HAVING SUM(LaborCost) <> 0
            )                                                                                           s
                ON s.WBS1 = p.WBS1
                    AND s.WBS2 = p.WBS2
                    AND s.WBS3 = p.WBS3
            WHERE p.WBS3 <> ' '; -- Task level only

        -- CustLastChargeDate
        UPDATE p
            SET p.CustLastChargeDate = A.MaxTransDate
            FROM dbo.ProjectCustomTabFields                                                             p
            INNER JOIN (SELECT WBS1 FROM dbo.PR WHERE WBS2 = ' ' AND ChargeType = 'R' AND Status = 'A') p1
                ON p.WBS1 = p1.WBS1
            LEFT JOIN (
                SELECT B.WBS1
                     , B.WBS2
                     , B.WBS3
                     , MAX(B.TransDate) AS MaxTransDate
                    FROM (
                        SELECT WBS1
                             , WBS2
                             , WBS3
                             , MAX(TransDate) AS TransDate
                            FROM dbo.LD WITH (NOLOCK)
                            GROUP BY
                            WBS1
                          , WBS2
                          , WBS3
                        UNION ALL
                        SELECT WBS1
                             , WBS2
                             , WBS3
                             , MAX(TransDate) AS TransDate
                            FROM dbo.LedgerMisc WITH (NOLOCK)
                            WHERE BillStatus IS NOT NULL
                            GROUP BY
                            WBS1
                          , WBS2
                          , WBS3
                        UNION ALL
                        SELECT WBS1
                             , WBS2
                             , WBS3
                             , MAX(TransDate) AS TransDate
                            FROM dbo.LedgerEX WITH (NOLOCK)
                            GROUP BY
                            WBS1
                          , WBS2
                          , WBS3
                        UNION ALL
                        SELECT WBS1
                             , WBS2
                             , WBS3
                             , MAX(TransDate) AS TransDate
                            FROM dbo.LedgerAP WITH (NOLOCK)
                            GROUP BY
                            WBS1
                          , WBS2
                          , WBS3
                    ) AS B
                    GROUP BY
                    B.WBS1
                  , B.WBS2
                  , B.WBS3
            )                                                                                           AS A
                ON A.WBS1 = p.WBS1
                    AND A.WBS2 = p.WBS2
                    AND A.WBS3 = p.WBS3
            WHERE
            p.WBS2 <> ' '
                AND p.WBS3 <> ' ';

    -- Fee types rollup
    --exec spCCG_ProjectJTD_UpdateFeeTypes
    END;
GO
