SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_PackageBody_Custom] ( @WBS1 varchar(30)= NULL)
             AS EXEC spCCG_EI_PackageBody_Custom @WBS1
GO
