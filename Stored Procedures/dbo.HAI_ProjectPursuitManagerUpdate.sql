SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[HAI_ProjectPursuitManagerUpdate]
	@WBS1			VARCHAR(30),
	@BillingClient	VARCHAR(32)
AS
/**************************************************************************************
			Copyright (c) 2021 Halff Associates Inc. All rights reserved.

			Insert or Update the Strategic Pursuit Manager based on the client for the 
			project. This will check if the client is TxDOT and change the Strategic 
			Pursuit Manager to the alternate for Transportation who should be the manager 
			for these projects.

			08/30/2021	Jeremy Baummer - Created
			10/06/2021	David Springer added email for Marketing Involvement
***************************************************************************************/
DECLARE @Email		varchar (255),
		@SPM		varchar (32),
		@SPMFirst	varchar (50),
		@Project	varchar (255),
		@Org		varchar (20),
		@URL		varchar(255),
		@Body		varchar (8000)
BEGIN
SET NOCOUNT ON;

Set @URL = 'https://vantagepoint.halff.com/vantagepoint/app/#!Projects/view/project/overview/' + @WBS1 + '/presentation'

--Check if the Client is TxDOT
IF (SELECT ParentID FROM dbo.Clendor AS cl WHERE @BillingClient = cl.ClientID) = '6FF82495259340708E3A4F65193646A5'
   OR @BillingClient = '6FF82495259340708E3A4F65193646A5'
	Begin
		UPDATE ctf 
		SET ctf.CustStrategicPursuitsManager = pa.CustAltStrategicPursuitManager,
			@SPM =  pa.CustAltStrategicPursuitManager
		FROM dbo.ProjectCustomTabFields ctf
			Join dbo.UDIC_PracticeArea pa on pa.CustNumber = ctf.CustPracticeArea
		WHERE ctf.WBS1 = @WBS1
		  and ctf.CustPracticeArea = 'Trans/Struct/Vis'
	End

ELSE
	Begin
		UPDATE ctf 
		SET ctf.CustStrategicPursuitsManager = pa.CustStrategicPursuitManager,
			@SPM =  pa.CustAltStrategicPursuitManager
		FROM dbo.ProjectCustomTabFields ctf
			Join dbo.UDIC_PracticeArea pa on pa.CustNumber = ctf.CustPracticeArea
		WHERE ctf.WBS1 = @WBS1
	End

If 'Y' = (Select CustCorporateMarketing From ProjectCustomTabFields Where WBS1 = @WBS1 and WBS2 = ' ')
	Begin
	Select @SPMFirst = FirstName, @Email = Email From EM Where Employee = @SPM
	Select
	 	@Project = p.Name, 
		@Org = p.Org,
		@Email = @Email + IIF (o.CustState = 'FL', ';mfleming@halff.com', '')
	From PR p, UDIC_OrganizationStructure os, UDIC_Office o
	Where p.WBS1 = @WBS1
	  and p.WBS2 = ' '
	  and p.Org = os.CustOrganizationCode
	  and substring (p.Org, 4, 2) = o.CustOfficeNumber
	  
	Set @Body = '<br>' + @SPMFirst + ',<br><br>Team ' + right(@Org,3)+' has created a new pursuit - <a href="'+@URL+'" title="Open in Vantagepoint">' + @WBS1 + ': ' + @Project + '</a> with marketing involvement requested.'

	Insert Into dbo.CCG_Email_Messages
	(SourceApplication, Sender, ToList, Subject, Body, MaxDelay)
	Values
	('SPM Notification', 'deltek@halff.com', @Email, 'New Pursuit Notification', @Body, 0)

	End

END
GO
