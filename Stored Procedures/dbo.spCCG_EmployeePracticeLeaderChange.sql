SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_EmployeePracticeLeaderChange] @Employee varchar(32), @PracticeArea varchar (32)
AS 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
/* 
Copyright (c) 2017 Central Consulting Group.  All rights reserved.
05/02/2017 David Springer
           Update project Practice Area Leader from employee Practice Leader grid.
		   Call this from an Employees_PracticeLeaders INSERT or CHANGE workflows.
*/
BEGIN

	Update px
	Set px.CustPracticeAreaLeader = @Employee
	From ProjectCustomTabFields px
	Where px.CustPracticeArea = @PracticeArea
	  and exists (Select 'x' From PR Where WBS1 = px.WBS1 and WBS2 = ' ' and Status <> 'D')

END
GO
