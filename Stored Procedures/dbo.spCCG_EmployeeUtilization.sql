SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_EmployeeUtilization]
AS 
/* 
Copyright (c) 2018 Central Consulting Group.  All rights reserved.
04/02/2018 David Springer
           Populate UDIC_Utilization
		   Call this from an Employee scheduled workflow.
04/27/2018 David Springer
           Changed benefit WBS span & working hours calc
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
	--  Sunday prior to today less 364 days to 1 year ago
DECLARE @StartDateAnnual	date,
		@StartDate4Week		date,
		@StartDate1Week		date,
		@EndDate			date
BEGIN

	-- Get max timesheet End Date < today
	Select @EndDate = max (EndDate) From tkControl Where EndDate <= getDate()
	Select @StartDateAnnual = DateAdd (day, -364, @EndDate)
	Select @StartDate4Week = DateAdd (day, -27, @EndDate)
	Select @StartDate1Week = DateAdd (day, -6, @EndDate)


	Delete From UDIC_Utilization

	Insert Into UDIC_Utilization
	(UDIC_UID, CustEmployeeNumber, CustEmployeeName, CustOrg, CustTargetRatio, CustHoursPerDay, 
		Cust1WeekHoursStandard, Cust4WeekHoursStandard, CustAnnualHoursStandard,
		Cust1WeekHoursTotal, Cust1WeekHoursBenefit, Cust1WeekHoursDirect,
		Cust4WeekHoursTotal, Cust4WeekHoursBenefit, Cust4WeekHoursDirect,
		CustAnnualHoursTotal, CustAnnualHoursBenefit, CustAnnualHoursDirect)
	Select Replace (NewID(), '-', '') ID, e.Employee, e.Employee, e.Org, e.TargetRatio, e.HoursPerDay,
		dbo.WorkDays (dbo.GreatestDate (@StartDate1Week, e.HireDate), dbo.LeastDate (IsNull (e.TerminationDate, @EndDate), @EndDate)) * e.HoursPerDay Cust1WeekStdHrs,
		dbo.WorkDays (dbo.GreatestDate (@StartDate4Week, e.HireDate), dbo.LeastDate (IsNull (e.TerminationDate, @EndDate), @EndDate)) * e.HoursPerDay Cust4WeekStdHrs,
		dbo.WorkDays (dbo.GreatestDate (@StartDateAnnual, e.HireDate), dbo.LeastDate (IsNull (e.TerminationDate, @EndDate), @EndDate)) * e.HoursPerDay CustAnnualStdHrs,
		Sum (CASE When l.TransDate >= @StartDate1Week Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) [1WeekTotalHrs],
		Sum (CASE When b.StartWBS1 is not null and l.TransDate >= @StartDate1Week Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) [1WeekBenefits],
		Sum (CASE When l.ChargeType = 'R' and l.TransDate >= @StartDate1Week Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) [1WeekBillable],
		Sum (CASE When l.TransDate >= @StartDate4Week Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) [4WeekTotalHrs],
		Sum (CASE When b.StartWBS1 is not null and l.TransDate >= @StartDate4Week Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) [4WeekBenefits],
		Sum (CASE When l.ChargeType = 'R' and l.TransDate >= @StartDate4Week Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) [4WeekBillable],
		Sum (l.RegHrs + l.OvtHrs + l.SpecialOvtHrs) AnnualTotalHrs,
		Sum (CASE When b.StartWBS1 is not null Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) AnnualBenefits,
		Sum (CASE When l.ChargeType = 'R' Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) AnnualBillable
	From EM e, LD l
		Left Join
		(Select a.StartWBS1, a.EndWBS1
		FROM CFGTimeAnalysisHeadingsData h, CFGTimeAnalysis a
		Where h.ReportColumn = a.ReportColumn
		  and h.Benefit = 'Y') b on l.WBS1 between b.StartWBS1 and b.EndWBS1
	Where l.Employee = e.Employee
	  and Left (e.Org, (Select Org1Length From CFGFormat)) in ('01', '03')
	  and l.TransDate >= @StartDateAnnual
	Group by e.Org, e.Employee, e.TargetRatio, e.HoursPerDay, e.HireDate, e.TerminationDate

	Update UDIC_Utilization
	Set Cust1WeekHoursExpected = CASE When Cust1WeekHoursTotal < Cust1WeekHoursStandard Then Cust1WeekHoursTotal Else Cust1WeekHoursStandard END,
		Cust4WeekHoursExpected = CASE When Cust4WeekHoursTotal < Cust4WeekHoursStandard Then Cust4WeekHoursTotal Else Cust4WeekHoursStandard END,
		CustAnnualHoursExpected = CASE When CustAnnualHoursTotal < CustAnnualHoursStandard Then CustAnnualHoursTotal Else CustAnnualHoursStandard END

	Update UDIC_Utilization
	Set Cust1WeekUtilization =
		Round (CASE When Cust1WeekHoursExpected - Cust1WeekHoursBenefit = 0 Then 0
			Else Cust1WeekHoursDirect / (Cust1WeekHoursExpected - Cust1WeekHoursBenefit) END * 100, 1),
		Cust4WeekUtilization =
		Round (CASE When Cust4WeekHoursExpected - Cust4WeekHoursBenefit = 0 Then 0
			Else Cust4WeekHoursDirect / (Cust4WeekHoursExpected - Cust4WeekHoursBenefit) END * 100, 1),
		CustAnnualUtilization =  
		Round (CASE When CustAnnualHoursExpected - CustAnnualHoursBenefit = 0 Then 0 
			Else CustAnnualHoursDirect / (CustAnnualHoursExpected - CustAnnualHoursBenefit) END * 100, 1)

END
GO
