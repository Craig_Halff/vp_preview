SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Load_Invoice_Groups]
	@visionMajorVersion	int,
	@VISION_LANGUAGE	varchar(10)
AS
BEGIN
	-- DEBUG TEST: EXEC [dbo].[DeltekStoredProc_CCG_EI_Config_Load_Invoice_Groups] 7, 'en-US'
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT IsNull(CascadeMenu, '') as CascadeMenu, DataValue, IsNull(InvoiceGroup, DataValue) as InvoiceGroup,
			IsNull(InvoiceGroup, ccvd.Code) as InvoiceGroupCode, ThruPeriod, Convert(varchar(10),ThruDate,126) as ThruDate, Code
		FROM FW_CustomColumnValuesData ccvd			
			LEFT JOIN CCG_EI_ConfigInvoiceGroups cig on cig.InvoiceGroup = ccvd.Code
		WHERE GridID = N'X'
			AND UICultureName = @VISION_LANGUAGE
			AND InfoCenterArea = N'Projects'
			AND ColName = N'custInvoiceGroup'
		ORDER BY Case When IsNull(CascadeMenu, '') = '' Then DataValue Else CascadeMenu End, DataValue
END


GO
