SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_Get_Invoice_Groups]
	@KEY_INVOICE_GROUP_FIELD	varchar(100),
	@VISION_LANGUAGE			varchar(100),
	@UserId						Nvarchar(32)
AS
BEGIN
	-- spCCG_EI_Get_Invoice_Groups 'custInvoiceGroup', 'fr-CA', 'ADMIN' 
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @sSQL Nvarchar(max)

	SET @sSQL = N'
		Select IsNull(CascadeMenu, '''') as CascadeMenu, isnull(ccvd.Code, ccvd2.Code) as Code, isnull(ccvd.DataValue, ccvd2.DataValue) as DataValue '

    SET @sSQL += N' 
		FROM CCG_EI_ConfigInvoiceGroups cig 
			LEFT JOIN FW_CustomColumnValuesData ccvd on cig.InvoiceGroup = ccvd.Code AND UICultureName = ''' + @VISION_LANGUAGE + N''' and GridID = ''X'' 
				and InfoCenterArea = ''Projects'' and ColName = ''' + @KEY_INVOICE_GROUP_FIELD + N''' 
            INNER JOIN (select Code, min(DataValue) as DataValue from FW_CustomColumnValuesData where GridID = ''X'' and InfoCenterArea = ''Projects'' 
				and ColName = ''' + @KEY_INVOICE_GROUP_FIELD + N''' group by Code) ccvd2 on cig.InvoiceGroup = ccvd2.Code ' +
			(CASE WHEN Exists (SELECT 'x' FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnCCG_EI_GetInvoiceGroups]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT')) THEN N'
			INNER JOIN dbo.fnCCG_EI_GetInvoiceGroups(''' + @UserId + N''') ig ON ig.InvoiceGroup = cig.InvoiceGroup '
			ELSE '' END) + N'		
		ORDER BY Case When IsNull(CascadeMenu, '''') = '''' Then isnull(ccvd.DataValue, ccvd2.DataValue) Else CascadeMenu End, DataValue '

	EXEC(@sSQL)
	--Print @sSQL
END
GO
