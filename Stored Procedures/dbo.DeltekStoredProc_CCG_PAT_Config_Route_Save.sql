SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_Route_Save]
	@fieldsRouteSQL			Nvarchar(max),
	@insertRouteSql			Nvarchar(max),
	@fieldsRouteDetailSQL	Nvarchar(max),
	@insertRouteDetailSql	Nvarchar(max),
	@VISION_LANGUAGE		Nvarchar(10)
AS
BEGIN
	/*
	[DeltekStoredProc_CCG_PAT_Config_Route_Save]
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_PAT_RegexMatch(N'<CSV>', @fieldsRouteSQL);
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch(N'<CSV>|', @fieldsRouteDetailSQL) * 2;
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch(N'<INSERT VALUES>|', @insertRouteSql) * 4;
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch(N'<INSERT VALUES>|', @insertRouteDetailSql) * 8;
	IF @safeSql < 15 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS Nvarchar(4));
		RETURN;
	END;

	BEGIN TRANSACTION;

	DELETE FROM CCG_PAT_ConfigRouteDetailDescriptions WHERE UICultureName = @VISION_LANGUAGE;
	DELETE FROM CCG_PAT_ConfigRouteDetail;
	DELETE FROM CCG_PAT_ConfigRoutes;

	-- Inserts
	IF ISNULL(@fieldsRouteSQL, N'') <> N'' AND ISNULL(@insertRouteSql, N'') <> N'' BEGIN
		SET @sSQL = N'
			INSERT INTO CCG_PAT_ConfigRoutes
				(' + @fieldsRouteSQL + ')
				VALUES
				' + @insertRouteSql;
		--PRINT @sSQL;
		EXEC (@sSQL);

		-- Update the multilingual descriptions table
		SET @sSQL = N'
			DELETE a FROM CCG_PAT_ConfigRoutesDescriptions a LEFT JOIN CCG_PAT_ConfigRoutes b ON a.Route = b.Route
				WHERE b.Route IS NULL		-- No match

			MERGE CCG_PAT_ConfigRoutesDescriptions WITH (HOLDLOCK) AS Target
			USING CCG_PAT_ConfigRoutes AS Source ON Target.Route = Source.Route AND Target.UICultureName = '''+@VISION_LANGUAGE+'''
			WHEN MATCHED THEN
				UPDATE SET
					[RouteLabel]		= Source.[RouteLabel],
					[RouteDescription]	= Source.[RouteDescription]
			WHEN NOT MATCHED BY TARGET THEN
				INSERT (Route, UICultureName, RouteLabel, RouteDescription)
					VALUES (Source.Route, '''+@VISION_LANGUAGE+''', Source.RouteLabel, Source.RouteDescription)
			WHEN NOT MATCHED BY SOURCE AND Target.UICultureName = '''+@VISION_LANGUAGE+''' THEN DELETE;';
		EXEC (@sSQL);
	END;
	IF ISNULL(@fieldsRouteDetailSQL, N'') <> N'' AND ISNULL(@insertRouteDetailSql, N'') <> N'' BEGIN
		SET @sSQL = N'
			INSERT INTO CCG_PAT_ConfigRouteDetail
				(' + @fieldsRouteDetailSQL + ')
				VALUES
				' + @insertRouteDetailSql;
		--PRINT @sSQL;
		EXEC (@sSQL);

		-- Update the multilingual descriptions table
		SET @sSQL = N'
			DELETE rd
				FROM CCG_PAT_ConfigRouteDetailDescriptions rd
					LEFT JOIN CCG_PAT_ConfigRouteDetail r ON r.Route = rd.Route AND r.RouteType = rd.RouteType AND r.RouteValue = rd.RouteValue and rd.SortOrder = r.SortOrder
				WHERE r.Route IS NULL		-- No match

			MERGE CCG_PAT_ConfigRouteDetailDescriptions WITH (HOLDLOCK) AS Target
			USING CCG_PAT_ConfigRouteDetail AS Source ON
				Target.Route = Source.Route AND Target.RouteType = Source.RouteType AND Target.RouteValue = Source.RouteValue AND Target.SortOrder = Source.SortOrder
					AND Target.UICultureName = '''+@VISION_LANGUAGE+'''
			WHEN MATCHED THEN
				UPDATE SET [Description]		= Source.[Description]
			WHEN NOT MATCHED BY TARGET THEN
				INSERT (Route, RouteType, UICultureName, RouteValue, Description, SortOrder)
					VALUES (Source.Route, Source.RouteType, '''+@VISION_LANGUAGE+''', Source.RouteValue, Source.Description, Source.SortOrder)
			WHEN NOT MATCHED BY SOURCE AND Target.UICultureName = '''+@VISION_LANGUAGE+''' THEN DELETE;';
		EXEC (@sSQL);
	END;

	COMMIT TRANSACTION;
END;
GO
