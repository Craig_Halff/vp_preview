SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_EI_GetEmployeesForDelegation]
AS BEGIN
	-- Copyright (c) 2018 EleVia Software and Central Consulting Group.  All rights reserved.
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	declare @sql varchar(max) = ''''''
	select @sql = ''''+ Replace(Replace(DelegationEmployeesList, '; ', ';'), ';', ''', ''') + '''' from CCG_EI_Config c where c.DelegationEmployeesList <> ''
	-- Is this Vision?
	if exists (select 1 from FW_CFGSystem x where left(x.Version, 2) = '7.' or left(x.Version, 2) = '6.')
		set @sql = '
			select s.Employee, max(case when s.Role in ('+@sql+') or s.Employee in ('+@sql+') then ''Y'' else ''N'' end) DelegationApprovalRights
				from SEUser s
					inner join EM on EM.Employee = s.Employee
					inner join (
						select distinct Role from SE where AccessAllNavNodes = ''Y''															-- roles with access to all nav areas
						union select distinct Role from SENavTree nt inner join FW_CustomNavtree cnt on cnt.ID = nt.NodeID
							where cnt.Page like ''%LaunchEI.aspx'' or (cnt.Page like ''%LaunchPage.aspx'' and (cnt.Args like ''%app=EI%'' or cnt.Args not like ''%app=%''))	-- roles with EI access
					) x on x.Role = s.Role
				group by s.Employee, EM.LastName, EM.FirstName
				order by EM.LastName, EM.FirstName, s.Employee '
	else -- VP2
		set @sql = '
			select s.Employee, max(case when s.Role in ('+@sql+') or s.Employee in ('+@sql+') then ''Y'' else ''N'' end) as DelegationApprovalRights
				from SEUser s 
					inner join EM on EM.Employee = s.Employee
				where DisableLogin = ''N'' and EM.Status = ''A''
				group by s.Employee, EM.LastName, EM.FirstName
				order by EM.LastName, EM.FirstName, s.Employee '
	exec(@sql)
END
GO
