SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyConvertEmployee]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@UserName Nvarchar(32) = 'VisionKeyConvert' -- Add by DP 4.0
,@Period integer = 0,
@EntityType Nvarchar(20)
as
begin -- Procedure
	set nocount on 
	declare @OldValue Nvarchar(20)
	declare @NewValue Nvarchar(20)
	Declare @Existing integer
	declare @Entity Nvarchar(20)
	declare @length integer
	declare @CvtType integer
	declare @KeysFetch integer
	declare @RetVal integer
	declare @diag smallint
	declare @maxSeq int
	declare @Errmsg2 Nvarchar(1000),
			@custlabel	Nvarchar(40),
			@Custlabelplural Nvarchar(40)
	declare @TableName 		Nvarchar(100)
	declare @Sql 				Nvarchar(1000)
	declare @message nvarchar(max) 

	set @diag = 0
	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'Employee'
	set @length = 20
	set @Existing = 0
	set @KeysFetch = 0

	execute InsertKeyCvtDriver @Entity -- populate driver table

-- Add by DP 4.0
	delete from keyconvertDriver where entity = N'Employee' and TableName = N'EMMain' and ColumnName = N'Employee'
	DECLARE @OldName Nvarchar(155)
	DECLARE @NewName Nvarchar(155)
	DECLARE @PKey Nvarchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--
	select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'employeeLabel'
	select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'employeeLabelPlural'

	declare KeysCursor cursor for
		select OldKey, NewKey, Pkey from KeyConvertWork where Entity = @EntityType
	open KeysCursor
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue, 
		@PKey

	set @KeysFetch = @@Fetch_Status

While (@KeysFetch = 0)
	begin
		begin tran
-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @EntityType)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),N'KC','Y',  @custlabel + ' Key Convert ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, left(@custlabel + ' Key Convert', 40), @custlabel);
			end

			Set @PostControlFlag = 1
		end

		set @Existing = 0
		select @Existing =  1, @NewName = EM.Lastname + (IsNull(', ' + EM.FirstName, N'')) from EMMain EM where Employee = @NewValue
		select @OldName = EM.Lastname + (IsNull(', ' + EM.FirstName, N'')) from EMMain EM where Employee = @OldValue
--

	If (@Existing = 1)
		begin
			  if (@DeleteExisting = 0)
				begin
				SET @message = dbo.GetMessage('MsgNumExistCombineEmployees',@custlabel,@NewValue,@custlabel,'','','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end
			if (@NewValue = @OldValue)
				begin
				SET @message = dbo.GetMessage('MsgAreSameNumConvNotCont',@custlabelPlural,'','','','','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end
				declare @MulticompanyEnabled Nvarchar(1)
				declare @MulticurrencyEnabled Nvarchar(1)
				declare @NewFunctionalCurrencyCode Nvarchar(3)
				declare @OldFunctionalCurrencyCode Nvarchar(3)
				--declare @oldCompay Nvarchar(14) -- from 7.5 no need to check company
				--declare @newCompay Nvarchar(14)
				select @MulticompanyEnabled = MulticompanyEnabled,
			   		 @MulticurrencyEnabled = MulticurrencyEnabled from FW_CFGSystem
				
				/* DW: from 7.5 no need to check company
				if (@MulticompanyEnabled = 'Y')
					begin
						select @oldCompay = substring(old.org,1,Org1Length),
								 @newCompay = substring(new.org,1,Org1Length) from em old,em new, cfgformat 
								where old.Employee = @OldValue and new.Employee = @NewValue
						if (@oldCompay <> @newCompay)
							begin
							SET @message = dbo.GetMessage('MsgMultiCompanyCannotMergeOldEmp',@custlabelPlural,
															@OldValue,@oldCompay,'',@NewValue,@newCompay,'','','')
							RAISERROR(@message,16,3)
								close keysCursor
								deallocate keysCursor
								if (@@Trancount > 0)
								   ROLLBACK TRANSACTION
								return(50002) -- user defined error
							end
					end
					*/
/* Removed by DP 4.0
			 			  exec @RetVal = KeyCvt @Entity = @Entity,
											@CvtType = @CvtType,
											@Length = 20,
											@ErrMsg = @ErrMsg2 output,
											@Diag = @Diag,
											@ConstrFlag = 3 -- UpdateTables
	        Delete From EM Where Employee = @OldValue
*/
/* Combine EMCompany records  */
	        Delete From EMCompany Where Employee = @OldValue 
			And EmployeeCompany in (Select EmployeeCompany  From EMCompany Where Employee = @NewValue)
			Update EMCompany Set  Employee = @NewValue  Where Employee = @OldValue 
/**/
				IF EXISTS (SELECT 'x' FROM sys.objects WHERE name = 'EmployeeCustomTabFields' AND type = 'U')
					begin
						delete from EmployeeCustomTabFields where Employee = @OldValue 
							and exists (select 'x' from EmployeeCustomTabFields where Employee = @NewValue)
					end

			Delete FROM InvoiceApprover Where InvoiceApprover.Employee = @OldValue 
            And Exists (Select 'x' from InvoiceApprover AS Tmp Where Tmp.Employee = @NewValue and tmp.WBS1 = InvoiceApprover.WBS1)

			Delete FROM CFGTransApprover Where CFGTransApprover.Employee = @OldValue 
            And Exists (Select 'x' from CFGTransApprover AS Tmp Where Tmp.Employee = @NewValue and tmp.Company = CFGTransApprover.Company)

	        Delete From BTRCTEmpls Where Employee = @OldValue 
	               And TableNo In (Select TableNo From BTRCTEmpls Where Employee = @NewValue)
	        
	        
	        Delete From BTROTEmpls Where Employee = @OldValue 
	               And TableNo In (Select TableNo From BTROTEmpls Where Employee = @NewValue)
	        
	               
	        Delete From BTRRTEmpls Where Employee = @OldValue 
	               And TableNo In (Select TableNo From BTRRTEmpls Where Employee = @NewValue)


	        Delete From CostCTEmpls Where Employee = @OldValue 
	               And TableNo In (Select TableNo From CostCTEmpls Where Employee = @NewValue)
	        
	        
	        Delete From CostRTEmpls Where Employee = @OldValue 
	               And TableNo In (Select TableNo From CostRTEmpls Where Employee = @NewValue)


	        Update EMAccrual
	               Set EMAccrual.CurrentEarned = EMAccrual.CurrentEarned + Tmp.CurrentEarned, 
	               EMAccrual.CurrentTaken = EMAccrual.CurrentTaken + Tmp.CurrentTaken, 
	               EMAccrual.HoursToAccrue = EMAccrual.HoursToAccrue + Tmp.HoursToAccrue 
				from EMAccrual Inner Join EMAccrual As Tmp On EMAccrual.Code = Tmp.Code and EMAccrual.EmployeeCompany = Tmp.EmployeeCompany
	               Where EMAccrual.Employee = @NewValue And Tmp.Employee = @OldValue

            Delete FROM EMAccrual Where EMAccrual.Employee = @OldValue 
                   And Exists (Select 'x' from EMAccrual AS Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.Code = EMAccrual.Code and EMAccrual.EmployeeCompany = Tmp.EmployeeCompany)

	        Update EMAccrualDetail
	               Set EMAccrualDetail.Earned = EMAccrualDetail.Earned + Tmp.Earned, 
	               EMAccrualDetail.Taken = EMAccrualDetail.Taken + Tmp.Taken, 
	               EMAccrualDetail.EarnedSinceLastPayroll = EMAccrualDetail.EarnedSinceLastPayroll + Tmp.EarnedSinceLastPayroll, 
	               EMAccrualDetail.TakenSinceLastPayroll = EMAccrualDetail.TakenSinceLastPayroll + Tmp.TakenSinceLastPayroll 
				from EMAccrualDetail Inner Join EMAccrualDetail As Tmp On EMAccrualDetail.Code = Tmp.Code and EMAccrualDetail.EmployeeCompany = Tmp.EmployeeCompany 
	               And EMAccrualDetail.Period = Tmp.Period And EMAccrualDetail.PostSeq = Tmp.PostSeq 
	               Where EMAccrualDetail.Employee = @NewValue And Tmp.Employee = @OldValue
	
	        	if (@@RowCount > 0)
	            Delete FROM EMAccrualDetail Where EMAccrualDetail.Employee = @OldValue 
	                   And Exists (Select 'x' from EMAccrualDetail AS Tmp Where Tmp.Employee = @NewValue 
	                   And Tmp.Code = EMAccrualDetail.Code and EMAccrualDetail.EmployeeCompany = Tmp.EmployeeCompany And Tmp.Period = EMAccrualDetail.Period 
	                   And Tmp.PostSeq = EMAccrualDetail.PostSeq)

            Delete FROM EMContactAssoc Where EMContactAssoc.Employee = @OldValue 
                   And Exists (Select 'x' from EMContactAssoc AS Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.ContactID = EMContactAssoc.ContactID)

-- Added by DP
            Delete FROM EMActivity Where EMActivity.Employee = @OldValue 
                   And Exists (Select 'x' from EMActivity AS Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.ActivityID = EMActivity.ActivityID)
            Delete FROM EMResume Where EMResume.Employee = @OldValue 
                   And Exists (Select 'x' from EMResume AS Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.ResumeCategory = EMResume.ResumeCategory)
--

            Delete FROM EMClientAssoc Where EMClientAssoc.Employee = @OldValue 
                   And Exists (Select 'x' from EMClientAssoc AS Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.ClientID = EMClientAssoc.ClientID)

			  delete from EMProjectAssoc where EMProjectAssoc.Employee = @OldValue  and
			  exists (select 'x' from EMProjectAssoc new where new.Employee = @NewValue and new.WBS1 = EMProjectAssoc.WBS1 and new.wbs2 = EMProjectAssoc.WBS2 and new.wbs3 = EMProjectAssoc.wbs3 and  new.Role = EMProjectAssoc.Role)

			  delete from EmployeeRealizationWk where EmployeeRealizationWk.Employee = @OldValue  and
			  exists (select 'x' from EmployeeRealizationWk new where new.Employee = @NewValue and new.WBS1 = EmployeeRealizationWk.WBS1 and new.wbs2 = EmployeeRealizationWk.WBS2 and new.wbs3 = EmployeeRealizationWk.wbs3 and new.PKey = EmployeeRealizationWk.PKey)
	        
			  update ExAdvance 
					Set ExAdvance.Amount = ExAdvance.Amount + Tmp.Amount
				from exAdvance Tmp inner join ExAdvance on ExAdvance.Period = Tmp.Period
				Where ExAdvance.Employee = @NewValue And Tmp.Employee = @OldValue 
				if (@@RowCount > 0)
	            Delete From ExAdvance Where ExAdvance.Employee = @OldValue 
	                   And Exists (Select 'x' From ExAdvance As Tmp Where Tmp.Employee = @NewValue 
	                   And Tmp.Period = ExAdvance.Period And Tmp.PostSeq = ExAdvance.PostSeq)

				delete from emSkills where emSkills.Employee = @OldValue
					and exists (select 'x' from emSkills tmp where tmp.Employee = @newValue and tmp.Skill = emSkills.skill)

				delete from emCitizenship where emCitizenship.Employee = @OldValue
					and exists (select 'x' from emCitizenship tmp where tmp.Employee = @newValue and tmp.CitizenshipType = emCitizenship.CitizenshipType and tmp.Country = emCitizenship.Country and ISNULL(tmp.Expiration,'') = ISNULL(EMCitizenship.Expiration,''))


				select @maxSeq = isnull(max(seq),0) 
					from emDirectDeposit
					where employee = @NewValue

				update emDirectDeposit
				set seq = seq + @MaxSeq
				where Employee = @OldValue

				/*2.0*/
            Delete From CostCTEmpls Where CostCTEmpls.Employee = @OldValue 
                   And Exists (Select 'x' From CostCTEmpls As Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.TableNo = CostCTEmpls.TableNo And Tmp.Category = CostCTEmpls.Category)

				delete from EMLocale where EMLocale.Employee = @OldValue
					and exists (select 'x' from EMLocale tmp where tmp.Employee = @newValue and tmp.Locale = EMLocale.Locale And Tmp.EmployeeCompany = EMLocale.EmployeeCompany)
/*Other amount fields in the EMPayroll table are not summed since they are scratch fields used during processing*/
				Update EMPayroll
				      Set EMPayroll.UnpaidRegHrs = EMPayroll.UnpaidRegHrs + Tmp.UnpaidRegHrs, 
				      EMPayroll.RegHrsToBePaid = EMPayroll.RegHrsToBePaid + Tmp.RegHrsToBePaid, 
				      EMPayroll.UnpaidOvtHrs = EMPayroll.UnpaidOvtHrs + Tmp.UnpaidOvtHrs, 
				      EMPayroll.OvtHrsToBePaid = EMPayroll.OvtHrsToBePaid + Tmp.OvtHrsToBePaid, 
				      EMPayroll.UnpaidSpecialOvtHrs = EMPayroll.UnpaidSpecialOvtHrs + Tmp.UnpaidSpecialOvtHrs, 
				      EMPayroll.SpecialOvtHrsToBePaid = EMPayroll.SpecialOvtHrsToBePaid + Tmp.SpecialOvtHrsToBePaid 
				From EMPayroll Inner Join EMPayroll As Tmp On EMPayroll.SelPeriod = Tmp.SelPeriod And EMPayroll.SelPostSeq = Tmp.SelPostSeq 
				      Where EMPayroll.Employee = @NewValue And Tmp.Employee = @OldValue And Tmp.EmployeeCompany = EMPayroll.EmployeeCompany
	            Delete From EMPayroll Where EMPayroll.Employee = @OldValue 
	                   And Exists (Select 'x' From EMPayroll As Tmp Where Tmp.Employee = @NewValue)


	        Update EMPayrollDetail
	               Set EMPayrollDetail.RegHrs = EMPayrollDetail.RegHrs + Tmp.RegHrs, 
	               EMPayrollDetail.OvtHrs = EMPayrollDetail.OvtHrs + Tmp.OvtHrs, 
	               EMPayrollDetail.SpecialOvtHrs = EMPayrollDetail.SpecialOvtHrs + Tmp.SpecialOvtHrs, 
	               EMPayrollDetail.RegPay = EMPayrollDetail.RegPay + Tmp.RegPay, 
	               EMPayrollDetail.OvtPay = EMPayrollDetail.OvtPay + Tmp.OvtPay, 
	               EMPayrollDetail.SpecialOvtPay = EMPayrollDetail.SpecialOvtPay + Tmp.SpecialOvtPay, 
	               EMPayrollDetail.OtherPay = EMPayrollDetail.OtherPay + Tmp.OtherPay 
				From EMPayrollDetail Inner Join EMPayrollDetail As Tmp On EMPayrollDetail.Period = Tmp.Period And EMPayrollDetail.PostSeq = Tmp.PostSeq 
	               Where EMPayrollDetail.Employee = @NewValue And Tmp.Employee = @OldValue
				if (@@RowCount > 0)
	            Delete From EMPayrollDetail Where EMPayrollDetail.Employee = @OldValue 
	                   And Exists (Select 'x' From EMPayrollDetail As Tmp Where Tmp.Employee = @NewValue 
	                   And Tmp.Period = EMPayrollDetail.Period And Tmp.PostSeq = EMPayrollDetail.PostSeq)

            Delete From EMPayrollWithholding Where EMPayrollWithholding.Employee = @OldValue 
                   And Exists (Select 'x' From EMPayrollWithholding As Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.Code = EMPayrollWithholding.Code and Tmp.EmployeeCompany = EMPayrollWithholding.EmployeeCompany)

	        Update EMPayrollWithholdingDetail 
	               Set EMPayrollWithholdingDetail.GrossPayBasis = EMPayrollWithholdingDetail.GrossPayBasis + Tmp.GrossPayBasis, 
	               EMPayrollWithholdingDetail.Amount = EMPayrollWithholdingDetail.Amount + Tmp.Amount,
	               EMPayrollWithholdingDetail.TaxablePayBasis = EMPayrollWithholdingDetail.TaxablePayBasis + tmp.TaxablePayBasis,
	               EMPayrollWithholdingDetail.AdjustedGrossPayBasis = EMPayrollWithholdingDetail.AdjustedGrossPayBasis + tmp.AdjustedGrossPayBasis,
	               EMPayrollWithholdingDetail.OtherPay1 = EMPayrollWithholdingDetail.OtherPay1 + tmp.OtherPay1,
	               EMPayrollWithholdingDetail.OtherPay2 = EMPayrollWithholdingDetail.OtherPay2 + tmp.OtherPay2,
	               EMPayrollWithholdingDetail.OtherPay3 = EMPayrollWithholdingDetail.OtherPay3 + tmp.OtherPay3,
	               EMPayrollWithholdingDetail.OtherPay4 = EMPayrollWithholdingDetail.OtherPay4 + tmp.OtherPay4,
	               EMPayrollWithholdingDetail.OtherPay5 = EMPayrollWithholdingDetail.OtherPay5 + tmp.OtherPay5,
	               EMPayrollWithholdingDetail.Amt401K = EMPayrollWithholdingDetail.Amt401K + tmp.Amt401K,
	               EMPayrollWithholdingDetail.Amt125 = EMPayrollWithholdingDetail.Amt125 + tmp.Amt125
	               from EMPayrollWithholdingDetail Inner Join EMPayrollWithholdingDetail As Tmp On 
	               EMPayrollWithholdingDetail.Code = Tmp.Code 
	               And EMPayrollWithholdingDetail.Period = Tmp.Period 
	               And EMPayrollWithholdingDetail.PostSeq = Tmp.PostSeq 
	               Where EMPayrollWithholdingDetail.Employee = @NewValue 
	               And Tmp.Employee = @OldValue
			if (@@RowCount > 0)
            Delete FROM EMPayrollWithholdingDetail Where EMPayrollWithholdingDetail.Employee = @OldValue 
                   And Exists (Select 'x' from EMPayrollWithholdingDetail AS Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.Code = EMPayrollWithholdingDetail.Code And Tmp.Period = EMPayrollWithholdingDetail.Period 
                   And Tmp.PostSeq = EMPayrollWithholdingDetail.PostSeq)

				Delete From EMPhoto Where Employee = @OldValue

            Delete FROM EMProjectAssocTemplate Where EMProjectAssocTemplate.Employee = @OldValue 
                   And Exists (Select 'x' from EMProjectAssocTemplate AS Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.WBS1 = EMProjectAssocTemplate.WBS1 
						 And Tmp.WBS2 = EMProjectAssocTemplate.WBS2 
                   And Tmp.WBS3 = EMProjectAssocTemplate.WBS3
						 And Tmp.Role = EMProjectAssocTemplate.Role)

            Delete FROM PYChecks Where PYChecks.Employee = @OldValue 
                   And Exists (Select 'x' from PYChecks AS Tmp Where Tmp.Employee = @NewValue 
						 And Tmp.Period = PYChecks.Period 
                   And Tmp.PostSeq = PYChecks.PostSeq)

            Delete FROM PYChecksA Where PYChecksA.Employee = @OldValue 
                   And Exists (Select 'x' from PYChecksA AS Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.Code = PYChecksA.Code 
						 And Tmp.Period = PYChecksA.Period 
                   And Tmp.PostSeq = PYChecksA.PostSeq)

            Delete FROM PYChecksE Where PYChecksE.Employee = @OldValue 
                   And Exists (Select 'x' from PYChecksE AS Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.Seq = PYChecksE.Seq 
						 And Tmp.Period = PYChecksE.Period 
                   And Tmp.PostSeq = PYChecksE.PostSeq)

            Delete FROM PYChecksW Where PYChecksW.Employee = @OldValue 
                   And Exists (Select 'x' from PYChecksW AS Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.Code = PYChecksW.Code 
						 And Tmp.Period = PYChecksW.Period 
                   And Tmp.PostSeq = PYChecksW.PostSeq)

            Delete FROM SF255EmployeeGraphics Where SF255EmployeeGraphics.Employee = @OldValue 
                   And Exists (Select 'x' from SF255EmployeeGraphics AS Tmp Where Tmp.Employee = @NewValue 
						 And Tmp.SF255ID = SF255EmployeeGraphics.SF255ID 
                   And Tmp.LinkID = SF255EmployeeGraphics.LinkID)
				
/* End 2.0 additions*/

/*added 4.1*/
		Delete From EMEKGroups Where EMEKGroups.Employee = @OldValue 
                   And Exists (Select 'x' From EMEKGroups As Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.EKGroup = EMEKGroups.EKGroup And Tmp.Company = EMEKGroups.Company)

		Delete From EMPayrollContribution Where EMPayrollContribution.Employee = @OldValue 
                   And Exists (Select 'x' From EMPayrollContribution As Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.Code = EMPayrollContribution.Code and Tmp.EmployeeCompany = EMPayrollContribution.EmployeeCompany)

		Update EMPayrollContributionDetail 
	               Set EMPayrollContributionDetail.GrossPayBasis = EMPayrollContributionDetail.GrossPayBasis + Tmp.GrossPayBasis, 
	               EMPayrollContributionDetail.Amount = EMPayrollContributionDetail.Amount + Tmp.Amount,
	               EMPayrollContributionDetail.TaxablePayBasis = EMPayrollContributionDetail.TaxablePayBasis + tmp.TaxablePayBasis,
	               EMPayrollContributionDetail.AdjustedGrossPayBasis = EMPayrollContributionDetail.AdjustedGrossPayBasis + tmp.AdjustedGrossPayBasis,
	               EMPayrollContributionDetail.OtherPay1 = EMPayrollContributionDetail.OtherPay1 + tmp.OtherPay1,
	               EMPayrollContributionDetail.OtherPay2 = EMPayrollContributionDetail.OtherPay2 + tmp.OtherPay2,
	               EMPayrollContributionDetail.OtherPay3 = EMPayrollContributionDetail.OtherPay3 + tmp.OtherPay3,
	               EMPayrollContributionDetail.OtherPay4 = EMPayrollContributionDetail.OtherPay4 + tmp.OtherPay4,
	               EMPayrollContributionDetail.OtherPay5 = EMPayrollContributionDetail.OtherPay5 + tmp.OtherPay5,
	               EMPayrollContributionDetail.Amt401K = EMPayrollContributionDetail.Amt401K + tmp.Amt401K,
	               EMPayrollContributionDetail.Amt125 = EMPayrollContributionDetail.Amt125 + tmp.Amt125
	               from EMPayrollContributionDetail Inner Join EMPayrollContributionDetail As Tmp On 
	               EMPayrollContributionDetail.Code = Tmp.Code 
	               And EMPayrollContributionDetail.Period = Tmp.Period 
	               And EMPayrollContributionDetail.PostSeq = Tmp.PostSeq 
	               Where EMPayrollContributionDetail.Employee = @NewValue 
	               And Tmp.Employee = @OldValue

		if (@@RowCount > 0)
			Delete FROM EMPayrollContributionDetail Where EMPayrollContributionDetail.Employee = @OldValue 
        	           And Exists (Select 'x' from EMPayrollContributionDetail AS Tmp Where Tmp.Employee = @NewValue 
                	   And Tmp.Code = EMPayrollContributionDetail.Code And Tmp.Period = EMPayrollContributionDetail.Period 
	                   And Tmp.PostSeq = EMPayrollContributionDetail.PostSeq)

		Delete From EMPayrollContributionWage Where EMPayrollContributionWage.Employee = @OldValue 
                   And Exists (Select 'x' From EMPayrollContributionWage As Tmp Where Tmp.Employee = @NewValue and Tmp.EmployeeCompany = EMPayrollContributionWage.EmployeeCompany
                   And Tmp.Code = EMPayrollContributionWage.Code And Tmp.CodeWage = EMPayrollContributionWage.CodeWage)

		Update EMPayrollContributionDetailWage 
	               Set EMPayrollContributionDetailWage.Amount = EMPayrollContributionDetailWage.Amount + Tmp.Amount
	               from EMPayrollContributionDetailWage Inner Join EMPayrollContributionDetailWage As Tmp On 
	               EMPayrollContributionDetailWage.Code = Tmp.Code 
	               And EMPayrollContributionDetailWage.CodeWage = Tmp.CodeWage 
	               And EMPayrollContributionDetailWage.Period = Tmp.Period 
	               And EMPayrollContributionDetailWage.PostSeq = Tmp.PostSeq 
	               Where EMPayrollContributionDetailWage.Employee = @NewValue 
	               And Tmp.Employee = @OldValue

		if (@@RowCount > 0)
			Delete FROM EMPayrollContributionDetailWage Where EMPayrollContributionDetailWage.Employee = @OldValue 
        	           And Exists (Select 'x' from EMPayrollContributionDetailWage AS Tmp Where Tmp.Employee = @NewValue 
                	   And Tmp.Code = EMPayrollContributionDetailWage.Code And EMPayrollContributionDetailWage.CodeWage = Tmp.CodeWage And Tmp.Period = EMPayrollContributionDetailWage.Period 
	                   And Tmp.PostSeq = EMPayrollContributionDetailWage.PostSeq)

		Delete From EMPayrollWithholdingWage Where EMPayrollWithholdingWage.Employee = @OldValue 
                   And Exists (Select 'x' From EMPayrollWithholdingWage As Tmp Where Tmp.Employee = @NewValue and Tmp.EmployeeCompany = EMPayrollWithholdingWage.EmployeeCompany 
                   And Tmp.Code = EMPayrollWithholdingWage.Code And Tmp.CodeWage = EMPayrollWithholdingWage.CodeWage)

		Update EMPayrollWithholdingDetailWage 
	               Set EMPayrollWithholdingDetailWage.Amount = EMPayrollWithholdingDetailWage.Amount + Tmp.Amount
	               from EMPayrollWithholdingDetailWage Inner Join EMPayrollWithholdingDetailWage As Tmp On 
	               EMPayrollWithholdingDetailWage.Code = Tmp.Code 
	               And EMPayrollWithholdingDetailWage.CodeWage = Tmp.CodeWage 
	               And EMPayrollWithholdingDetailWage.Period = Tmp.Period 
	               And EMPayrollWithholdingDetailWage.PostSeq = Tmp.PostSeq 
	               Where EMPayrollWithholdingDetailWage.Employee = @NewValue 
	               And Tmp.Employee = @OldValue

		if (@@RowCount > 0)
			Delete FROM EMPayrollWithholdingDetailWage Where EMPayrollWithholdingDetailWage.Employee = @OldValue 
        	           And Exists (Select 'x' from EMPayrollWithholdingDetailWage AS Tmp Where Tmp.Employee = @NewValue 
                	   And Tmp.Code = EMPayrollWithholdingDetailWage.Code And EMPayrollWithholdingDetailWage.CodeWage = Tmp.CodeWage And Tmp.Period = EMPayrollWithholdingDetailWage.Period 
	                   And Tmp.PostSeq = EMPayrollWithholdingDetailWage.PostSeq)

		Delete FROM PYChecksC Where PYChecksC.Employee = @OldValue 
                   And Exists (Select 'x' from PYChecksC AS Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.Code = PYChecksC.Code 
                   And Tmp.Period = PYChecksC.Period 
                   And Tmp.PostSeq = PYChecksC.PostSeq)
/* End 4.1 additions*/
	        
	    Delete From EMTKGroups Where Employee = @OldValue
-- Added by DP
		Delete FW_SEUserOptions From FW_SEUserOptions inner join SEUser on FW_SEUserOptions.UserName=SEUser.UserName where SEUser.Employee = @OldValue 
		Delete SEUserMyFavs From SEUserMyFavs inner join SEUser on SEUserMyFavs.UserName=SEUser.UserName where SEUser.Employee = @OldValue 
		Delete SEUserMyLinks From SEUserMyLinks inner join SEUser on SEUserMyLinks.UserName=SEUser.UserName where SEUser.Employee = @OldValue 
		Delete SEUserPasswordHistory From SEUserPasswordHistory inner join SEUser on SEUserPasswordHistory.UserName=SEUser.UserName where SEUser.Employee = @OldValue 
		Delete SF330Preferences From SF330Preferences inner join SEUser on SF330Preferences.UserName=SEUser.UserName where SEUser.Employee = @OldValue 
		Delete MergeFavorites From MergeFavorites inner join SEUser on MergeFavorites.UserName=SEUser.UserName where SEUser.Employee = @OldValue 

		Delete FROM FormW2Data Where FormW2Data.Employee = @OldValue 
                   And Exists (Select 'x' from FormW2Data AS Tmp Where Tmp.Employee = @NewValue 
		   And Tmp.Year = FormW2Data.Year 
                   And Tmp.Company = FormW2Data.Company
		   And Tmp.Sequence = FormW2Data.Sequence)

		Delete FROM EMDirectDepositDetail Where EMDirectDepositDetail.Employee = @OldValue 
                   And Exists (Select 'x' from EMDirectDepositDetail AS Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.Seq = EMDirectDepositDetail.Seq
                   And Tmp.Period = EMDirectDepositDetail.Period
		   And Tmp.PostSeq = EMDirectDepositDetail.PostSeq)

		Delete FROM laMaster Where laMaster.Employee = @OldValue 
                   And Exists (Select 'x' from laMaster AS Tmp Where Tmp.Employee = @NewValue 
		   And Tmp.Batch = laMaster.Batch)

		Delete FROM CustomProposalEmployee Where CustomProposalEmployee.Employee = @OldValue 
                   And Exists (Select 'x' from CustomProposalEmployee AS Tmp Where Tmp.Employee = @NewValue 
                   And Tmp.CustomPropID = CustomProposalEmployee.CustomPropID
		   And Tmp.SectionID = CustomProposalEmployee.SectionID)
--

		select @maxSeq = case when isnull(max(tkDetail.seq),0) > isnull(max(tmp.seq),0) then isnull(max(tkDetail.seq),0) else isnull(max(tmp.seq),0) end
				from tkDetail inner join tkDetail as tmp on tkDetail.endDate = tmp.endDate and tmp.EmployeeCompany = tkDetail.EmployeeCompany 
				where tkDetail.Employee = @NewValue and tmp.Employee =  @OldValue

				update tkDetail set
					tkDetail.seq = tkDetail.seq + @maxSeq
				from tkDetail inner join tkDetail as tmp on tkDetail.endDate = tmp.endDate and tmp.EmployeeCompany = tkDetail.EmployeeCompany 
							and tkDetail.Seq = tmp.Seq 
							and tkDetail.TransDate = tmp.transDate
				where tkDetail.Employee = @OldValue and tmp.Employee = @NewValue

				update tkCustomFields set
					tkCustomFields.seq = tkCustomFields.seq + @maxSeq
				from tkCustomFields inner join tkDetail as tmp on tkCustomFields.endDate = tmp.endDate and tmp.EmployeeCompany = tkCustomFields.EmployeeCompany 
							and tkCustomFields.Seq = tmp.Seq 
				where tkCustomFields.Employee = @OldValue and tmp.Employee = @NewValue

		select @maxSeq = isnull(max(tkUnitDetail.seq),0)
				from tkUnitDetail inner join tkUnitDetail as tmp on tkUnitDetail.endDate = tmp.endDate and tmp.EmployeeCompany = tkUnitDetail.EmployeeCompany 
				where tkUnitDetail.Employee = @NewValue and tmp.Employee =  @OldValue

				update tkUnitDetail set
					tkUnitDetail.seq = tkUnitDetail.seq + @maxSeq
				from tkUnitDetail inner join tkUnitDetail as tmp on tkUnitDetail.endDate = tmp.endDate and tmp.EmployeeCompany = tkUnitDetail.EmployeeCompany 
							and tkUnitDetail.Seq = tmp.Seq 
							and tkUnitDetail.TransDate = tmp.transDate
				where tkUnitDetail.Employee = @OldValue and tmp.Employee = @NewValue

		declare @maxRevision int
		select @maxRevision = case when isnull(max(tkRevisionMaster.Revision),0) > isnull(max(tmp.Revision),0) then isnull(max(tkRevisionMaster.Revision),0) else isnull(max(tmp.Revision),0) end
		from tkRevisionMaster inner join tkRevisionMaster as tmp on tkRevisionMaster.endDate = tmp.endDate 
		and tmp.EmployeeCompany = tkRevisionMaster.EmployeeCompany 
		where tkRevisionMaster.Employee = @NewValue and tmp.Employee =  @OldValue

		update tkRevisionMaster set tkRevisionMaster.Revision = tkRevisionMaster.Revision + @maxRevision
		from tkRevisionMaster inner join tkRevisionMaster as tmp on tkRevisionMaster.endDate = tmp.endDate 
		and tmp.EmployeeCompany = tkRevisionMaster.EmployeeCompany 
		and tkRevisionMaster.Revision = tmp.Revision 
		where tkRevisionMaster.Employee = @OldValue and tmp.Employee = @NewValue

		update tkRevisionDetail set tkRevisionDetail.Revision = tkRevisionDetail.Revision + @maxRevision
		from tkRevisionDetail inner join tkRevisionDetail as tmp on tkRevisionDetail.endDate = tmp.endDate 
		and tmp.EmployeeCompany = tkRevisionDetail.EmployeeCompany 
		and tkRevisionDetail.Revision = tmp.Revision 
		where tkRevisionDetail.Employee = @OldValue and tmp.Employee = @NewValue

				delete from tkmaster where Employee = @OldValue and 
				exists (select 'x' from tkMaster as tmp where tmp.Employee = @NewValue and tmp.EndDate = tkMaster.EndDate and tmp.EmployeeCompany = tkMaster.EmployeeCompany)

				delete from tsmaster where Employee = @OldValue and 
				exists (select 'x' from tsmaster as tmp where tmp.Employee = @NewValue and tmp.Batch = tsmaster.Batch)

				delete from EMSubscr where Employee = @oldValue and 
				exists (select 'x' from EMSubscr as tmp where tmp.Employee = @NewValue and tmp.UserName = EMSubscr.UserName)
--WorkflowEMActivity
				delete from WorkflowEMActivity where Employee = @oldValue and 
				exists (select 'x' from WorkflowEMActivity as tmp where tmp.Employee = @NewValue and tmp.ActivityID = WorkflowEMActivity.ActivityID)
--CFGPOBuyer.Employee

--CFGCreditCardSecondaryEmployee (PrimaryCode, Company, Code, Employee)
				delete from CFGCreditCardSecondaryEmployee where Employee = @oldValue and 
				exists (select 'x' from CFGCreditCardSecondaryEmployee as tmp where tmp.Employee = @NewValue and tmp.Company = CFGCreditCardSecondaryEmployee.Company
				 and tmp.PrimaryCode = CFGCreditCardSecondaryEmployee.PrimaryCode and tmp.Code = CFGCreditCardSecondaryEmployee.Code)

--Combine ekDatail and ekMaster (key Employee, ReportDate, ReportName, Seq)
			select @maxSeq = case when isnull(max(ekDetail.seq),0) > isnull(max(tmp.seq),0) then isnull(max(ekDetail.seq),0) else isnull(max(tmp.seq),0) end
			from ekDetail inner join ekDetail as tmp on ekDetail.ReportDate = tmp.ReportDate and ekDetail.ReportName = tmp.ReportName 
			where ekDetail.Employee = @NewValue and tmp.Employee =  @OldValue

			update ekDetail set
				ekDetail.seq = ekDetail.seq + @maxSeq
			from ekDetail inner join ekDetail as tmp on ekDetail.ReportDate = tmp.ReportDate and ekDetail.ReportName = tmp.ReportName
				and ekDetail.Seq = tmp.Seq 
			where ekDetail.Employee = @OldValue and tmp.Employee = @NewValue

			delete from ekmaster where Employee = @OldValue and 
			exists (select 'x' from ekmaster as tmp where tmp.Employee = @NewValue and tmp.ReportDate = ekmaster.ReportDate and tmp.ReportName = ekmaster.ReportName and tmp.EmployeeCompany = ekmaster.EmployeeCompany)

			update EKDocuments set
				EKDocuments.Seq = EKDocuments.Seq + @maxSeq
			from EKDocuments inner join ekDetail as tmp on EKDocuments.ReportDate = tmp.ReportDate and EKDocuments.ReportName = tmp.ReportName
				and EKDocuments.Seq = tmp.Seq 
			where EKDocuments.Employee = @OldValue and tmp.Employee = @NewValue

			update EKDocumentsDetail set
				EKDocumentsDetail.DetailSeq = EKDocumentsDetail.DetailSeq + @maxSeq
			from EKDocumentsDetail inner join ekDetail as tmp on EKDocumentsDetail.ReportDate = tmp.ReportDate and EKDocumentsDetail.ReportName = tmp.ReportName
				and EKDocumentsDetail.DetailSeq = tmp.Seq 
			where EKDocumentsDetail.Employee = @OldValue and tmp.Employee = @NewValue

	        Delete From CFGPOBuyer where Employee = @OldValue

				update tsDetail
				set tsDetail.pKey = tsDetail.pKey + 'x'
				 from tsdetail inner join tsdetail as tmp on tsdetail.batch = tmp.batch 
				and tsdetail.pkey = tmp.pkey
				where tsDetail.Employee = @OldValue and tmp.Employee = @NewValue

					/*handle duplicate custom tab records, possible when copying one project to another*/
				declare custTable cursor for
				select tableName from FW_CustomGrids where infocenterarea = N'Employees'
				
				open custTable
				fetch next from custTable into @TableName
				while (@@FETCH_STATUS = 0)
				begin
					set @Sql = 'Delete FROM ' + @TableName + ' WHERE ' + @TableName + '.Employee =N''' +  @OldValue + ''' and ' +
								  'exists (select ''x'' from ' + @TableName + ' new where new.Employee = N''' + @NewValue + ''' and new.seq = ' + @TableName + '.seq)'
					if (@Diag = 1)
						print @Sql
					execute(@Sql)
					fetch next from custTable into @TableName
				end
				close custTable
				deallocate custTable
				
-- RonK 07/15/09: Added Contracts for Rubik--update for merged Employees.
			-- First update non-duplicate ContractCredit records to the target Employee.
 			Update ContractCredit Set Employee = @NewValue where Employee = @OldValue and not exists 
				(select 'x' from ContractCredit new where new.Employee = @NewValue and new.WBS1 = ContractCredit.WBS1
												and new.ContractNumber = ContractCredit.ContractNumber)
			-- Note: There are no merged fields for the ContractCredit records duplicated in the Target Employee
			
			-- Next remove old ContractCredit records now merged. (If duplicated they will just be dropped)
        	Delete FROM ContractCredit WHERE Employee = @OldValue			
-- RonK 07/15/09: End Mod				


			declare @PlanID varchar(32)
			declare @WBS1	Nvarchar(30),
					@WBS2	Nvarchar(30),
					@WBS3	Nvarchar(30)
			declare @ResourceID		Nvarchar(20),
					@GenericResourceID		Nvarchar(20),
					@OldAssignmentID	Nvarchar(32),
					@NewAssignmentID	Nvarchar(32)
			declare @OldTaskID		varchar(32)
			declare @NewTaskID		varchar(32)
			declare @TempFetch integer
			declare @OldHardBooked		varchar(1)
			declare @NewHardBooked		varchar(1)

-- Move Planned and Basedline for the duplicate RPAssignment, Delete duplicate RPAssignment
			declare AssignmentCursor cursor for
				select PlanID, TaskID, max(HardBooked) as HardBooked, isnull(WBS1,'') as WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(ResourceID,'') as ResourceID, isnull(GenericResourceID,'') as GenericResourceID, MIN(AssignmentID) as AssignmentID  
				from RPAssignment where ResourceID=@OldValue and
				exists (select 'x' from RPAssignment new where new.PlanID=RPAssignment.PlanID 
					and isnull(new.wbs1,'') = isnull(RPAssignment.wbs1,'') 
					and isnull(new.wbs2,'') = isnull(RPAssignment.wbs2,'') 
					and isnull(new.wbs3,'') = isnull(RPAssignment.wbs3,'')
					and isnull(new.ResourceID,'') = @NewValue
					and isnull(new.GenericResourceID,'') = isnull(RPAssignment.GenericResourceID,''))
				group by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(ResourceID,''), isnull(GenericResourceID,'')
				Order by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(ResourceID,''), isnull(GenericResourceID,'')
			open AssignmentCursor
			fetch next from AssignmentCursor into @PlanID, @OldTaskID, @OldHardBooked, @WBS1, @WBS2, @WBS3, @ResourceID, @GenericResourceID, @OldAssignmentID
			set @TempFetch = @@Fetch_Status
			While (@TempFetch = 0)
			begin
				select @NewTaskID=TaskID, @NewAssignmentID=AssignmentID, @NewHardBooked=HardBooked from RPAssignment 
					where PlanID=@PlanID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(ResourceID,'')=@NewValue 
					and isnull(GenericResourceID,'')=isnull(@GenericResourceID,'') 

				Update RPPlannedLabor Set PlanID=@PlanID, TaskID=@NewTaskID, AssignmentID=@NewAssignmentID where AssignmentID=@OldAssignmentID
				Update RPBaselineLabor Set PlanID=@PlanID, TaskID=@NewTaskID, AssignmentID=@NewAssignmentID where AssignmentID=@OldAssignmentID

				UPDATE A SET 
					StartDate = CASE WHEN XTPD.StartDate < A.StartDate THEN XTPD.StartDate ELSE A.StartDate END, 
					EndDate = CASE WHEN XTPD.EndDate > A.EndDate THEN XTPD.EndDate ELSE A.EndDate END
					FROM RPAssignment AS A
					  INNER JOIN (
						SELECT PlanID, TaskID, AssignmentID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
						  FROM RPPlannedLabor
						  WHERE AssignmentID=@NewAssignmentID
						  GROUP BY PlanID, TaskID, AssignmentID
					  ) AS XTPD 
						ON A.PlanID = XTPD.PlanID AND A.TaskID = XTPD.TaskID AND A.AssignmentID = XTPD.AssignmentID

				if (@NewHardBooked='N' and @OldHardBooked='Y')
					Update RPAssignment Set HardBooked='Y' WHere AssignmentID=@NewAssignmentID

				delete RPAssignment Where PlanID=@PlanID and TaskID=@OldTaskID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(ResourceID,'')=@OldValue
					and isnull(GenericResourceID,'')=isnull(@GenericResourceID,'')

				fetch next from AssignmentCursor into @PlanID, @OldTaskID, @OldHardBooked, @WBS1, @WBS2, @WBS3, @ResourceID, @GenericResourceID, @OldAssignmentID
				set @TempFetch = @@Fetch_Status
			end
			close AssignmentCursor
			deallocate AssignmentCursor

-- Move Planned and Basedline for the duplicate PNAssignment, Delete duplicate PNAssignment
			declare AssignmentCursor cursor for
				select PlanID, TaskID, max(HardBooked) as HardBooked, isnull(WBS1,'') as WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(ResourceID,'') as ResourceID, isnull(GenericResourceID,'') as GenericResourceID, MIN(AssignmentID) as AssignmentID  
				from PNAssignment where ResourceID=@OldValue and
				exists (select 'x' from PNAssignment new where new.PlanID=PNAssignment.PlanID 
					and isnull(new.wbs1,'') = isnull(PNAssignment.wbs1,'') 
					and isnull(new.wbs2,'') = isnull(PNAssignment.wbs2,'') 
					and isnull(new.wbs3,'') = isnull(PNAssignment.wbs3,'')
					and isnull(new.ResourceID,'') = @NewValue
					and isnull(new.GenericResourceID,'') = isnull(PNAssignment.GenericResourceID,''))
				group by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(ResourceID,''), isnull(GenericResourceID,'')
				Order by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(ResourceID,''), isnull(GenericResourceID,'')
			open AssignmentCursor
			fetch next from AssignmentCursor into @PlanID, @OldTaskID, @OldHardBooked, @WBS1, @WBS2, @WBS3, @ResourceID, @GenericResourceID, @OldAssignmentID
			set @TempFetch = @@Fetch_Status
			While (@TempFetch = 0)
			begin
				select @NewTaskID=TaskID, @NewAssignmentID=AssignmentID, @NewHardBooked=HardBooked from PNAssignment 
					where PlanID=@PlanID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(ResourceID,'')=@NewValue 
					and isnull(GenericResourceID,'')=isnull(@GenericResourceID,'') 

				Update PNPlannedLabor Set PlanID=@PlanID, TaskID=@NewTaskID, AssignmentID=@NewAssignmentID where AssignmentID=@OldAssignmentID
				Update PNBaselineLabor Set PlanID=@PlanID, TaskID=@NewTaskID, AssignmentID=@NewAssignmentID where AssignmentID=@OldAssignmentID

				UPDATE A SET 
					StartDate = CASE WHEN XTPD.StartDate < A.StartDate THEN XTPD.StartDate ELSE A.StartDate END, 
					EndDate = CASE WHEN XTPD.EndDate > A.EndDate THEN XTPD.EndDate ELSE A.EndDate END
					FROM PNAssignment AS A
					  INNER JOIN (
						SELECT PlanID, TaskID, AssignmentID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
						  FROM PNPlannedLabor
						  WHERE AssignmentID=@NewAssignmentID
						  GROUP BY PlanID, TaskID, AssignmentID
					  ) AS XTPD 
						ON A.PlanID = XTPD.PlanID AND A.TaskID = XTPD.TaskID AND A.AssignmentID = XTPD.AssignmentID

				if (@NewHardBooked='N' and @OldHardBooked='Y')
					Update PNAssignment Set HardBooked='Y' WHere AssignmentID=@NewAssignmentID

				delete PNAssignment Where PlanID=@PlanID and TaskID=@OldTaskID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'')
					and isnull(WBS3,'')=isnull(@WBS3,'')
					and isnull(ResourceID,'')=@OldValue
					and isnull(GenericResourceID,'')=isnull(@GenericResourceID,'')

				fetch next from AssignmentCursor into @PlanID, @OldTaskID, @OldHardBooked, @WBS1, @WBS2, @WBS3, @ResourceID, @GenericResourceID, @OldAssignmentID
				set @TempFetch = @@Fetch_Status
			end
			close AssignmentCursor
			deallocate AssignmentCursor

	    END --Existing
-- Add by DP 4.0
		Else	-- Not Existing
		begin
			select * into #TempKeyConvert from EMMain where Employee = @OldValue
			Update #TempKeyConvert set Employee=@NewValue
			Insert EMMain select * from #TempKeyConvert
			Drop Table #TempKeyConvert
			set @NewName = @OldName
		end

		exec @RetVal = KeyCvt @Entity = @Entity,
					 @CvtType = @CvtType,
					 @Length = 20,
					 @ErrMsg = @ErrMsg2 output,
					 @Diag = @Diag,
					 @NewValue = @NewValue,
					 @OldValue = @OldValue,
					 @ConstrFlag = 3 -- UpdateTables
					 ,@NeedTransaction = 'N'

		--exec @RetVal = KeyCvt 'Employee', @CvtType,20,@ErrMsg2 output,'',0,'',0,0,@NewValue,@OldValue  --Equivilant of UpdateTables in Advantage
	        Delete From EMMain Where Employee = @OldValue

		--If Talent Management Web Service is enabled, update TalentModDate to track change to Employee field.
		If (Select EnableWebService FROM CFGIntegrationWS WHERE Type = 'Talent') = 'Y'
		Begin
			UPDATE EMMain SET TalentModDate = GETUTCDATE() WHERE Employee = @NewValue
		End

		--If TrafficLIVE Web Service is enabled, update TLSyncModDate to track change to Employee field.
		If (Select EnableWebService FROM CFGIntegrationWS WHERE Type = 'TL') = 'Y'
		Begin
			UPDATE EMMain SET TLSyncModDate = GETUTCDATE() WHERE Employee = @NewValue
		End

		--Update ModUser/ModDate
		UPDATE EMMain SET ModUser = @UserName, ModDate = GETUTCDATE() WHERE Employee = @NewValue

		if (@RetVal = 0)
			begin
			    -- Timekeeper approval workflows
				Update ApprovalItem SET ApplicationKey = replace(ApplicationKey, @OldValue+'|', @NewValue+'|') 
				where ApplicationID = 'TIME' AND ApprovalLevel = 'Master' AND ApplicationKey LIKE @OldValue+'|%'
				Update ApprovalItem SET ApplicationKey = replace(ApplicationKey, '|'+@OldValue+'|', '|'+@NewValue+'|') 
				where ApplicationID = 'TIME' AND ApprovalLevel = 'Detail' AND ApplicationKey LIKE '%|'+@OldValue+'|%'

				Update ApprovalWorkflow Set EmployeeAssignto = replace(EmployeeAssignto, @OldValue+'|', @NewValue+'|') 
				where EmployeeAssignto like @OldValue+'|%'
				Update ApprovalWorkflow Set EmployeeAssignto = replace(EmployeeAssignto, '|'+@OldValue, '|'+@NewValue) 
				where EmployeeAssignto like '%|'+@OldValue
				Update ApprovalWorkflow Set EmployeeAssignto = replace(EmployeeAssignto, '|'+@OldValue+'|', '|'+@NewValue+'|') 
				where EmployeeAssignto like '%|'+@OldValue+'|%'
				Update WorkflowActionCreateItem Set EmployeeAssignto = replace(EmployeeAssignto, @OldValue+'|', @NewValue+'|') 
				where EmployeeAssignto like @OldValue+'|%'
				Update WorkflowActionCreateItem Set EmployeeAssignto = replace(EmployeeAssignto, '|'+@OldValue, '|'+@NewValue) 
				where EmployeeAssignto like '%|'+@OldValue
				Update WorkflowActionCreateItem Set EmployeeAssignto = replace(EmployeeAssignto, '|'+@OldValue+'|', '|'+@NewValue+'|') 
				where EmployeeAssignto like '%|'+@OldValue+'|%'

				If (@Existing = 1)
				begin
					Update ApprovalWorkflow Set EmployeeAssigntoDisplay = @NewName
					where EmployeeAssigntoDisplay = @OldName
					Update ApprovalWorkflow Set EmployeeAssigntoDisplay = replace(EmployeeAssigntoDisplay, @OldName+'|', @NewName+'|') 
					where EmployeeAssigntoDisplay like @OldName+'|%'
					Update ApprovalWorkflow Set EmployeeAssigntoDisplay = replace(EmployeeAssigntoDisplay, '|'+@OldName, '|'+@NewName) 
					where EmployeeAssigntoDisplay like '%|'+@OldName
					Update ApprovalWorkflow Set EmployeeAssigntoDisplay = replace(EmployeeAssigntoDisplay, '|'+@OldName+'|', '|'+@NewName+'|') 
					where EmployeeAssigntoDisplay like '%|'+@OldName+'|%'
					Update WorkflowActionCreateItem Set EmployeeAssigntoDisplay = @NewName 
					where EmployeeAssigntoDisplay = @OldName
					Update WorkflowActionCreateItem Set EmployeeAssigntoDisplay = replace(EmployeeAssigntoDisplay, @OldName+'|', @NewName+'|') 
					where EmployeeAssigntoDisplay like @OldName+'|%'
					Update WorkflowActionCreateItem Set EmployeeAssigntoDisplay = replace(EmployeeAssigntoDisplay, '|'+@OldName, '|'+@NewName) 
					where EmployeeAssigntoDisplay like '%|'+@OldName
					Update WorkflowActionCreateItem Set EmployeeAssigntoDisplay = replace(EmployeeAssigntoDisplay, '|'+@OldName+'|', '|'+@NewName+'|') 
					where EmployeeAssigntoDisplay like '%|'+@OldName+'|%'
				end
-- Add by DP 4.0
				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq from keyConvertWork where Pkey = @PKey
				end
--
				delete from keyConvertWork where current of keysCursor
				commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
				rollback transaction
			end
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue,
		@PKey
	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	return(0)
End -- Procedure
GO
