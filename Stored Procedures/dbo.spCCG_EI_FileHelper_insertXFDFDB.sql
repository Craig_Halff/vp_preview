SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_FileHelper_insertXFDFDB]
      @User Nvarchar(32),
      @FILEID varchar(50),
      @XFDFFile Nvarchar(max)
AS
BEGIN
      SET NOCOUNT ON
      update  CCG_EI_XFDF set RevisionSeq = RevisionSeq +1 where DOCS_PR_FILEID = @FILEID
      insert into CCG_EI_XFDF (CreateDate,CreateUser,DOCS_PR_FILEID,XFDFFILE)
      values (GETUTCDATE(),@User, @FILEID, @XFDFFile)
END
GO
