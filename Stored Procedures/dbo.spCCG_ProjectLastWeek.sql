SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectLastWeek] @WBS1 varchar (32)
AS
/*
Copyright (c) 2018 Central Consulting Group. All rights reserved.
05/11/2018 David Springer
           Populate project Last Week 1 & 2 Spent Billing
           Call this from a Project scheduled workflow scheduled weekly
05/25/2018 David Springer
		   Added Backlog.dbo.PrjStat_BacklogDetail
05/31/2018 David Springer
           Allow either Vision transactions or Backlog to generate a grid record
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
BEGIN

-- ##########   Project (WBS1) level Week 1   ##########

	Update p
	Set p.CustLastWeek1SpentBilling = IsNull (ap.APSpent, 0) + IsNull (ex.ExSpent, 0) + IsNull (m.MiscSpent, 0) + IsNull (l.LaborSpent, 0)
	From ProjectCustomTabFields p
		Left Join (Select WBS1, Sum (BillExt) APSpent
					From LedgerAP l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1) ap on p.WBS1 = ap.WBS1
		Left Join (Select WBS1, Sum (BillExt) ExSpent
					From LedgerEX  l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1)ex on p.WBS1 = ex.WBS1
		Left Join (Select WBS1, Sum (BillExt) MiscSpent
					From LedgerMisc l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1) m on p.WBS1 = m.WBS1
		Left Join (Select WBS1, Sum (BillExt) LaborSpent
					From LD
					Where DatePart (week, TransDate - 1)  = DatePart (Week, getDate()) - 1
					  and Year (TransDate - 1) = Year (getDate())
					Group by WBS1) l on p.WBS1 = l.WBS1
	Where p.WBS1 = @WBS1
	  and p.WBS2 = ' '  -- Project level only

-- ##########   Phase (WBS2) level Week 1   ##########

	Update p
	Set p.CustLastWeek1SpentBilling = IsNull (ap.APSpent, 0) + IsNull (ex.ExSpent, 0) + IsNull (m.MiscSpent, 0) + IsNull (l.LaborSpent, 0)
	From ProjectCustomTabFields p
		Left Join (Select WBS1, WBS2, Sum (BillExt) APSpent
					From LedgerAP l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1, WBS2) ap on p.WBS1 = ap.WBS1 and p.WBS2 = ap.WBS2
		Left Join (Select WBS1, WBS2, Sum (BillExt) ExSpent
					From LedgerEX  l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1, WBS2)ex on p.WBS1 = ex.WBS1 and p.WBS2 = ex.WBS2
		Left Join (Select WBS1, WBS2, Sum (BillExt) MiscSpent
					From LedgerMisc l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1, WBS2) m on p.WBS1 = m.WBS1 and p.WBS2 = m.WBS2
		Left Join (Select WBS1, WBS2, Sum (BillExt) LaborSpent
					From LD
					Where DatePart (week, TransDate - 1)  = DatePart (Week, getDate()) - 1
					  and Year (TransDate - 1) = Year (getDate())
					Group by WBS1, WBS2) l on p.WBS1 = l.WBS1 and p.WBS2 = l.WBS2
	Where p.WBS1 = @WBS1
	  and p.WBS2 <> ' '  -- phase level only
	  and p.WBS3 = ' '

-- ##########   Task (WBS3) level Week 1   ##########

	Update p
	Set p.CustLastWeek1SpentBilling = IsNull (ap.APSpent, 0) + IsNull (ex.ExSpent, 0) + IsNull (m.MiscSpent, 0) + IsNull (l.LaborSpent, 0)
	From ProjectCustomTabFields p
		Left Join (Select WBS1, WBS2, WBS3, Sum (BillExt) APSpent
					From LedgerAP l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1, WBS2, WBS3) ap on p.WBS1 = ap.WBS1 and p.WBS2 = ap.WBS2 and p.WBS3 = ap.WBS3
		Left Join (Select WBS1, WBS2, WBS3, Sum (BillExt) ExSpent
					From LedgerEX  l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1, WBS2, WBS3)ex on p.WBS1 = ex.WBS1 and p.WBS2 = ex.WBS2 and p.WBS3 = ex.WBS3
		Left Join (Select WBS1, WBS2, WBS3, Sum (BillExt) MiscSpent
					From LedgerMisc l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1, WBS2, WBS3) m on p.WBS1 = m.WBS1 and p.WBS2 = m.WBS2 and p.WBS3 = m.WBS3
		Left Join (Select WBS1, WBS2, WBS3, Sum (BillExt) LaborSpent
					From LD
					Where DatePart (week, TransDate - 1)  = DatePart (Week, getDate()) - 1
					  and Year (TransDate - 1) = Year (getDate())
					Group by WBS1, WBS2, WBS3) l on p.WBS1 = l.WBS1 and p.WBS2 = l.WBS2 and p.WBS3 = l.WBS3
	Where p.WBS1 = @WBS1
	  and p.WBS3 <> ' ' -- task level only


-- ##########   Project (WBS1) level Week 2   ##########

	Update p
	Set p.CustLastWeek2SpentBilling = IsNull (ap.APSpent, 0) + IsNull (ex.ExSpent, 0) + IsNull (m.MiscSpent, 0) + IsNull (l.LaborSpent, 0)
	From ProjectCustomTabFields p
		Left Join (Select WBS1, Sum (BillExt) APSpent
					From LedgerAP l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1) ap on p.WBS1 = ap.WBS1
		Left Join (Select WBS1, Sum (BillExt) ExSpent
					From LedgerEX  l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1)ex on p.WBS1 = ex.WBS1
		Left Join (Select WBS1, Sum (BillExt) MiscSpent
					From LedgerMisc l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1) m on p.WBS1 = m.WBS1
		Left Join (Select WBS1, Sum (BillExt) LaborSpent
					From LD
					Where DatePart (week, TransDate - 1)  = DatePart (Week, getDate()) - 2
					  and Year (TransDate - 1) = Year (getDate())
					Group by WBS1) l on p.WBS1 = l.WBS1
	Where p.WBS1 = @WBS1
	  and p.WBS2 = ' '  -- Project level only

-- ##########   Phase (WBS2) level Week 2   ##########
	Update p
	Set p.CustLastWeek2SpentBilling = IsNull (ap.APSpent, 0) + IsNull (ex.ExSpent, 0) + IsNull (m.MiscSpent, 0) + IsNull (l.LaborSpent, 0)
	From ProjectCustomTabFields p
		Left Join (Select WBS1, WBS2, Sum (BillExt) APSpent
					From LedgerAP l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1, WBS2) ap on p.WBS1 = ap.WBS1 and p.WBS2 = ap.WBS2
		Left Join (Select WBS1, WBS2, Sum (BillExt) ExSpent
					From LedgerEX  l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1, WBS2)ex on p.WBS1 = ex.WBS1 and p.WBS2 = ex.WBS2
		Left Join (Select WBS1, WBS2, Sum (BillExt) MiscSpent
					From LedgerMisc l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1, WBS2) m on p.WBS1 = m.WBS1 and p.WBS2 = m.WBS2
		Left Join (Select WBS1, WBS2, Sum (BillExt) LaborSpent
					From LD
					Where DatePart (week, TransDate - 1)  = DatePart (Week, getDate()) - 2
					  and Year (TransDate - 1) = Year (getDate())
					Group by WBS1, WBS2) l on p.WBS1 = l.WBS1 and p.WBS2 = l.WBS2
	Where p.WBS1 = @WBS1
	  and p.WBS2 <> ' '  -- phase level only
	  and p.WBS3 = ' '

-- ##########   Task (WBS3) level Week 2   ##########

	Update p
	Set p.CustLastWeek2SpentBilling = IsNull (ap.APSpent, 0) + IsNull (ex.ExSpent, 0) + IsNull (m.MiscSpent, 0) + IsNull (l.LaborSpent, 0)
	From ProjectCustomTabFields p
		Left Join (Select WBS1, WBS2, WBS3, Sum (BillExt) APSpent
					From LedgerAP l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1, WBS2, WBS3) ap on p.WBS1 = ap.WBS1 and p.WBS2 = ap.WBS2 and p.WBS3 = ap.WBS3
		Left Join (Select WBS1, WBS2, WBS3, Sum (BillExt) ExSpent
					From LedgerEX  l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1, WBS2, WBS3)ex on p.WBS1 = ex.WBS1 and p.WBS2 = ex.WBS2 and p.WBS3 = ex.WBS3
		Left Join (Select WBS1, WBS2, WBS3, Sum (BillExt) MiscSpent
					From LedgerMisc l, CFGPostControl c
					Where l.Period = c.Period
					  and l.PostSeq = c.PostSeq
					  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
					  and Year (c.PostDate - 1) = Year (getDate())
					Group by WBS1, WBS2, WBS3) m on p.WBS1 = m.WBS1 and p.WBS2 = m.WBS2 and p.WBS3 = m.WBS3
		Left Join (Select WBS1, WBS2, WBS3, Sum (BillExt) LaborSpent
					From LD
					Where DatePart (week, TransDate - 1)  = DatePart (Week, getDate()) - 2
					  and Year (TransDate - 1) = Year (getDate())
					Group by WBS1, WBS2, WBS3) l on p.WBS1 = l.WBS1 and p.WBS2 = l.WBS2 and p.WBS3 = l.WBS3
	Where p.WBS1 = @WBS1
	  and p.WBS3 <> ' ' -- task level only

--Select * from Backlog..PrjStat_BacklogDetail
--  Spent Grid
	Delete From Projects_Spent Where WBS1 = @WBS1

--  Project Level Backlog
	Insert Into Projects_Spent
	(WBS1, WBS2, WBS3, Seq, CustSpentDescription, CustSpentTotalBacklog, CustSpentCurrentBacklog, CustSpentWeek1, CustSpentWeek2)
	Select a.WBS1, ' ' WBS2, ' ' WBS3, Replace (NewID(), '-', '') Seq, a.Description, 
			a.TotalBacklog, a.CurrentBacklog, a.Week1, a.Week1 + a.Week2
	From
		(Select x.WBS1, 
			CASE When c.Label is not null Then 'Team ' + c.Code + ' - ' + c.Label Else x.Description END Description, 
			Sum (x.TotalBacklog) TotalBacklog, Sum (x.CurrentBacklog) CurrentBacklog, Sum (x.Week1) Week1, Sum (x.Week2) Week2
		From
			(Select WBS1, null Org, 'Expense & Consultant' Description, 0 TotalBacklog, 0 CurrentBacklog, Sum (BillExt) Week1, 0 Week2
			From LedgerAP l, CFGPostControl c
			Where l.Period = c.Period
			  and l.PostSeq = c.PostSeq
			  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
			  and Year (c.PostDate - 1) = Year (getDate())
			Group by WBS1 Having  Sum (BillExt) <> 0
			UNION ALL
			Select WBS1, null, 'Expense & Consultant', 0, 0, Sum (BillExt) Week1, 0
			From LedgerEX  l, CFGPostControl c
			Where l.Period = c.Period
			  and l.PostSeq = c.PostSeq
			  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
			  and Year (c.PostDate - 1) = Year (getDate())
			Group by WBS1 Having  Sum (BillExt) <> 0
			UNION ALL
			Select WBS1, null, 'Expense & Consultant', 0, 0, Sum (BillExt) Week1, 0
			From LedgerMisc l, CFGPostControl c
			Where l.Period = c.Period
			  and l.PostSeq = c.PostSeq
			  and l.TransType <> 'UN'
			  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
			  and Year (c.PostDate - 1) = Year (getDate())
			Group by WBS1 Having  Sum (BillExt) <> 0
			UNION ALL
			Select l.WBS1, l.Org, null, 0, 0, Sum (BillExt) Week1, 0
			From LedgerMisc l, CFGPostControl c
			Where l.Period = c.Period
			  and l.PostSeq = c.PostSeq
			  and l.TransType = 'UN'
			  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
			  and Year (c.PostDate - 1) = Year (getDate())
			Group by l.WBS1, l.Org Having  Sum (BillExt) <> 0
			UNION ALL
			Select l.WBS1, l.EMOrg, null, 0, 0, Sum (BillExt) Week1, 0
			From LD l
			Where DatePart (week, TransDate - 1)  = DatePart (Week, getDate()) - 1
			  and Year (TransDate - 1) = Year (getDate())
			Group by l.WBS1, l.EMOrg Having  Sum (BillExt) <> 0
			UNION ALL
			Select WBS1, null, 'Expense & Consultant', 0, 0, 0,  Sum (BillExt) Week2
			From LedgerAP l, CFGPostControl c
			Where l.Period = c.Period
			  and l.PostSeq = c.PostSeq
			  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
			  and Year (c.PostDate - 1) = Year (getDate())
			Group by WBS1 Having  Sum (BillExt) <> 0
			UNION ALL
			Select WBS1, null, 'Expense & Consultant', 0, 0, 0, Sum (BillExt) Week2
			From LedgerEX  l, CFGPostControl c
			Where l.Period = c.Period
			  and l.PostSeq = c.PostSeq
			  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
			  and Year (c.PostDate - 1) = Year (getDate())
			Group by WBS1 Having  Sum (BillExt) <> 0
			UNION ALL
			Select WBS1, null, 'Expense & Consultant', 0, 0, 0, Sum (BillExt) Week2
			From LedgerMisc l, CFGPostControl c
			Where l.Period = c.Period
			  and l.PostSeq = c.PostSeq
			  and l.TransType <> 'UN'
			  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
			  and Year (c.PostDate - 1) = Year (getDate())
			Group by WBS1 Having  Sum (BillExt) <> 0
			UNION ALL
			Select l.WBS1, l.Org, null, 0, 0, 0, Sum (BillExt) Week2
			From LedgerMisc l, CFGPostControl c
			Where l.Period = c.Period
			  and l.PostSeq = c.PostSeq
			  and l.TransType = 'UN'
			  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
			  and Year (c.PostDate - 1) = Year (getDate())
			Group by l.WBS1, l.Org Having  Sum (BillExt) <> 0
			UNION ALL
			Select l.WBS1, l.EMOrg, null, 0, 0, 0, Sum (BillExt) Week2
			From LD l
			Where DatePart (week, TransDate - 1)  = DatePart (Week, getDate()) - 2
			  and Year (TransDate - 1) = Year (getDate())
			Group by l.WBS1, l.EMOrg Having  Sum (BillExt) <> 0
			UNION ALL
			Select d.AVO WBS1, d.TeamID, 
					CASE When d.TeamID = 'TSUB' Then 'Expense & Consultant' Else null End Description,
					Sum (d.Backlog1_4 + d.Backlog5_8 + d.BacklogOver8) TotalBacklog, Sum (d.Backlog1_4) CurrentBacklog, 0, 0
			From Backlog..PrjStat_BacklogDetail d,
				(Select AVO, Max (PeriodEnd) PeriodEnd
				From Backlog..PrjStat_BacklogDetail
				Group by AVO) m
			Where d.AVO = m.AVO
			  and d.PeriodEnd = m.PeriodEnd
	--		  and not exists (Select 'x' From Backlog..PrjStat_BacklogDetail Where AVO = d.AVO and Phase <> '' and PeriodEnd = m.PeriodEnd)
			Group by d.AVO, d.TeamID,
					CASE When d.TeamID = 'TSUB' Then 'Expense & Consultant' Else Right (d.TeamID, 2) End
			Having Sum (d.Backlog1_4 + d.Backlog5_8 + d.BacklogOver8) <> 0 or Sum (d.Backlog1_4) <> 0
			) x
			Left Join CFGOrgCodes c on c.Code = Right (x.Org, (Select Org3Length From CFGFormat)) and c.OrgLevel = '3'
		Where x.WBS1 = @WBS1
		Group by x.WBS1, CASE When c.Label is not null Then 'Team ' + c.Code + ' - ' + c.Label Else x.Description END
		) a

--  Phase Level Backlog
	Insert Into Projects_Spent
	(WBS1, WBS2, WBS3, Seq, CustSpentDescription, CustSpentTotalBacklog, CustSpentCurrentBacklog, CustSpentWeek1, CustSpentWeek2)
	Select a.WBS1, a.WBS2, ' ' WBS3, Replace (NewID(), '-', '') Seq, a.Description, 
			a.TotalBacklog, a.CurrentBacklog, a.Week1, a.Week1 + a.Week2
	From
		(Select x.WBS1, x.WBS2, 
			CASE When c.Label is not null Then 'Team ' + c.Code + ' - ' + c.Label Else x.Description END Description, 
			Sum (x.TotalBacklog) TotalBacklog, Sum (x.CurrentBacklog) CurrentBacklog, Sum (x.Week1) Week1, Sum (x.Week2) Week2
		From
			(Select WBS1, WBS2, null Org, 'Expense & Consultant' Description, 0 TotalBacklog, 0 CurrentBacklog, Sum (BillExt) Week1, 0 Week2
			From LedgerAP l, CFGPostControl c
			Where l.Period = c.Period
			  and l.PostSeq = c.PostSeq
			  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
			  and Year (c.PostDate - 1) = Year (getDate())
			Group by WBS1, WBS2 Having  Sum (BillExt) <> 0
			UNION ALL
			Select WBS1, WBS2, null, 'Expense & Consultant', 0, 0, Sum (BillExt) Week1, 0
			From LedgerEX  l, CFGPostControl c
			Where l.Period = c.Period
			  and l.PostSeq = c.PostSeq
			  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
			  and Year (c.PostDate - 1) = Year (getDate())
			Group by WBS1, WBS2 Having  Sum (BillExt) <> 0
			UNION ALL
			Select WBS1, WBS2, null, 'Expense & Consultant', 0, 0, Sum (BillExt) Week1, 0
			From LedgerMisc l, CFGPostControl c
			Where l.Period = c.Period
			  and l.PostSeq = c.PostSeq
			  and l.TransType <> 'UN'
			  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
			  and Year (c.PostDate - 1) = Year (getDate())
			Group by WBS1, WBS2 Having  Sum (BillExt) <> 0
			UNION ALL
			Select l.WBS1, l.WBS2, l.Org, null, 0, 0, Sum (BillExt) Week1, 0
			From LedgerMisc l, CFGPostControl c
			Where l.Period = c.Period
			  and l.PostSeq = c.PostSeq
			  and l.TransType = 'UN'
			  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 1
			  and Year (c.PostDate - 1) = Year (getDate())
			Group by l.WBS1, l.WBS2, l.Org Having  Sum (BillExt) <> 0
			UNION ALL
			Select l.WBS1, l.WBS2, l.EMOrg, null, 0, 0, Sum (BillExt) Week1, 0
			From LD l
			Where DatePart (week, TransDate - 1)  = DatePart (Week, getDate()) - 1
			  and Year (TransDate - 1) = Year (getDate())
			Group by l.WBS1, l.WBS2, l.EMOrg Having  Sum (BillExt) <> 0
			UNION ALL
			Select WBS1, WBS2, null, 'Expense & Consultant', 0, 0, 0, Sum (BillExt) Week2
			From LedgerAP l, CFGPostControl c
			Where l.Period = c.Period
			  and l.PostSeq = c.PostSeq
			  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
			  and Year (c.PostDate - 1) = Year (getDate())
			Group by WBS1, WBS2 Having  Sum (BillExt) <> 0
			UNION ALL
			Select WBS1, WBS2, null, 'Expense & Consultant', 0, 0, 0, Sum (BillExt) Week2
			From LedgerEX  l, CFGPostControl c
			Where l.Period = c.Period
			  and l.PostSeq = c.PostSeq
			  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
			  and Year (c.PostDate - 1) = Year (getDate())
			Group by WBS1, WBS2 Having  Sum (BillExt) <> 0
			UNION ALL
			Select WBS1, WBS2, null, 'Expense & Consultant', 0, 0, 0, Sum (BillExt) Week2
			From LedgerMisc l, CFGPostControl c
			Where l.Period = c.Period
			  and l.PostSeq = c.PostSeq
			  and l.TransType <> 'UN'
			  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
			  and Year (c.PostDate - 1) = Year (getDate())
			Group by WBS1, WBS2 Having  Sum (BillExt) <> 0
			UNION ALL
			Select l.WBS1, l.WBS2, l.Org, null, 0, 0, 0, Sum (BillExt) Week2
			From LedgerMisc l, CFGPostControl c
			Where l.Period = c.Period
			  and l.PostSeq = c.PostSeq
			  and l.TransType = 'UN'
			  and DatePart (week, c.PostDate - 1)  = DatePart (Week, getDate()) - 2
			  and Year (c.PostDate - 1) = Year (getDate())
			Group by l.WBS1, l.WBS2, l.Org Having  Sum (BillExt) <> 0
			UNION ALL
			Select l.WBS1, l.WBS2, l.EMOrg, null, 0, 0, 0, Sum (BillExt) Week2
			From LD l
			Where DatePart (week, TransDate - 1)  = DatePart (Week, getDate()) - 2
			  and Year (TransDate - 1) = Year (getDate())
			Group by l.WBS1, l.WBS2, l.EMOrg Having  Sum (BillExt) <> 0
			UNION ALL
			Select d.AVO WBS1, d.Phase WBS2, d.TeamID, 
					CASE When d.TeamID = 'TSUB' Then 'Expense & Consultant' Else null End Description,
					Sum (d.Backlog1_4 + d.Backlog5_8 + d.BacklogOver8) TotalBacklog, Sum (d.Backlog1_4) CurrentBacklog, 0, 0
			From Backlog..PrjStat_BacklogDetail d,
				(Select AVO, Phase, Max (PeriodEnd) PeriodEnd
				From Backlog..PrjStat_BacklogDetail
				Group by AVO, Phase) m
			Where d.AVO = m.AVO
			  and d.Phase = m.Phase
			  and d.PeriodEnd = m.PeriodEnd
			Group by d.AVO, d.Phase, d.TeamID,
					CASE When d.TeamID = 'TSUB' Then 'Expense & Consultant' Else null End
			Having Sum (d.Backlog1_4 + d.Backlog5_8 + d.BacklogOver8) <> 0 or Sum (d.Backlog1_4) <> 0
			) x
			Left Join CFGOrgCodes c on c.Code = Right (x.Org, (Select Org3Length From CFGFormat)) and c.OrgLevel = '3'
		Where x.WBS1 = @WBS1
		  and x.WBS2 <> ' '
		  and exists (Select 'x' From Backlog..PrjStat_BacklogDetail d, 
							  (Select AVO, Phase, Max (PeriodEnd) PeriodEnd
									From Backlog..PrjStat_BacklogDetail
									Group by AVO, Phase) m
								Where d.AVO = x.WBS1
								  and d.Phase <> ' ' 
								  and d.AVO = m.AVO
								  and d.Phase = m.Phase
								  and d.PeriodEnd = m.PeriodEnd)
		Group by x.WBS1, x.WBS2, CASE When c.Label is not null Then 'Team ' + c.Code + ' - ' + c.Label Else x.Description END
		) a

END
GO
