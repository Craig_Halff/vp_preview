SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_LoadVEAddress] (
	@vendor					Nvarchar(20)
) AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_LoadVEAddress] 'Vendor'
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	SELECT Address, Address, Case when PrimaryInd = 'Y' then Address else '' end
		FROM VEAddress
		WHERE Vendor = @vendor
		ORDER BY 2;
END;

GO
