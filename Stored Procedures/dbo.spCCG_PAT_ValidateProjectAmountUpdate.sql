SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_ValidateProjectAmountUpdate]
	@field			varchar(100),
	@wbs1			Nvarchar(30),
	@wbs2			Nvarchar(7),
	@wbs3			Nvarchar(7),
	@incInactive	bit,
	@regularOnly	bit,
	@VP2			bit = 0
AS
BEGIN
	-- EXEC [spCCG_PAT_ValidateProjectAmountUpdate] 'WBS1', 'b', 'c', 'd', 'e', 1, 1
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	DECLARE @sql Nvarchar(max)
    DECLARE @where Nvarchar(max) = N' 1 = 1 '
    DECLARE @order Nvarchar(max) = N''

    IF @field = 'WBS1' OR @field = 'WBS'
    BEGIN
        SET @where = N' (wbs1 like N''%' + @wbs1 + '%'' or name like N''%' + @wbs1 +  '%'') and wbs2 = N'' '' '
        SET @order = N'
			case
				when isnumeric(wbs1) = 1 and ISNUMERIC(''' + @wbs1 + ''') = 1 and wbs1 not like N''%[^0-9]%'' and '''+@wbs1+''' not like N''%[^0-9]%'' 
					and convert(decimal(19, 5), wbs1) = convert(decimal(19, 5), ''' + @wbs1 + ''') then 0
				else
					case
						when CHARINDEX(N''' + @wbs1 + ''', wbs1) = 0 then 999
						else CHARINDEX(N''' + @wbs1 + ''', wbs1)
					end
			end,
			case
				when CHARINDEX(N''' + @wbs1 + ''', name) = 0 then 999
				else CHARINDEX(N''' + @wbs1 + ''', name)
			end, wbs1'
    END
    else IF @field = 'WBS2'
    BEGIN
        SET @where = N' wbs1 = N''' + @wbs1 + ''' and (wbs2 like N''%' + @wbs2 + '%'' or name like N''%' + @wbs2 + '%'') and wbs3 = N'' '' '
        SET @order = N'
			case
				when isnumeric(wbs2) = 1 and ISNUMERIC(''' + @wbs2 + ''') = 1 and wbs2 not like N''%[^0-9]%'' and '''+@wbs2+''' not like N''%[^0-9]%'' 
					and convert(decimal(19, 5), wbs2) = convert(decimal(19, 5), ''' + @wbs2 + ''') then 0
				else
					case
						when CHARINDEX(N''' + @wbs2 + ''', wbs2) = 0 then 999
						else CHARINDEX(N''' + @wbs2 + ''', wbs2)
					end
			end,
			case
				when CHARINDEX(N''' + @wbs2 + ''', name) = 0 then 999
				else CHARINDEX(N''' + @wbs2 + ''', name)
			end, wbs2'
    END
    else IF @field = 'WBS3'
    BEGIN
        SET @where = N' wbs1 = N''' + @wbs1 + ''' and wbs2 = N''' + @wbs2 + ''' and (wbs3 like N''%' + @wbs3 + '%'' or name like N''%' + @wbs3 + '%'' ) '
        SET @order = N'
			case
				when isnumeric(wbs3) = 1 and ISNUMERIC(''' + @wbs3 + ''') = 1 and wbs3 not like N''%[^0-9]%'' and '''+@wbs3+''' not like N''%[^0-9]%'' 
					and convert(decimal(19, 5), wbs3) = convert(decimal(19, 5), ''' + @wbs3 + ''') then 0
				else
					case
						when CHARINDEX(N''' + @wbs3 + ''', wbs3) = 0 then 999
						else CHARINDEX(N''' + @wbs3 + ''', wbs3)
					end
			end,
			case
				when CHARINDEX(N''' + @wbs3 + ''', name) = 0 then 999
				else CHARINDEX(N''' + @wbs3 + ''', name)
			end, wbs3'
    END

	if @VP2 = 1 set @where += ' and (PR.Stage is null or PR.Stage in (select Code from CFGProjectStageData ps where ps.Step = ''W'')) ' 
    SET @sql = N'
		SELECT wbs1, wbs2, wbs3, sublevel, status, chargetype, readyForProcessing
			FROM PR
			WHERE ' + @where + ' and readyForProcessing = ''Y'' and status IN (''A'' ' +
				(CASE WHEN @incInactive = 1 THEN ', ''I''' ELSE '' END) + ') ' +
				(CASE WHEN @regularOnly = 1 THEN ' and chargetype = ''R'' ' ELSE '' END) + '
			ORDER BY ' + @order
	--PRINT @sql
	EXEC (@sql)
END
GO
