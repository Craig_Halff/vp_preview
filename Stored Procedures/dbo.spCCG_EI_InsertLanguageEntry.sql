SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_InsertLanguageEntry] (@Group1 varchar(255), @Id varchar(255),
	@Product varchar(255), @UICultureName varchar(20), @Value Nvarchar(255))
AS BEGIN
	set nocount on
	if not exists (select 'x' from CCG_Language_Labels where Id=@Id and Product=@Product and UICultureName=@UICultureName)
		insert into CCG_Language_Labels(Group1,Id,Product,UICultureName,Label) values (@Group1,@Id,@Product,@UICultureName,@Value)
	else
		UPDATE CCG_Language_Labels SET Group1 = @Group1, Label = @Value where Id=@Id and Product=@Product and UICultureName=@UICultureName
END
GO
