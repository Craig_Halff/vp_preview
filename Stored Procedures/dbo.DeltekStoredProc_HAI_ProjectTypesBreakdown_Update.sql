SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_HAI_ProjectTypesBreakdown_Update]
	@WBS1							NVARCHAR(30)
	--, @WBS2						NVARCHAR(30)
	--, @WBS3						NVARCHAR(30)
    , @Seq							NVARCHAR(32)
    , @CreateUser					NVARCHAR(32)
    , @ModUser						NVARCHAR(32)
    , @CustBreakdownProjectType		NVARCHAR(32)
    , @CustBreakdownFee				DECIMAL(19,5)
    , @CustBreakdownUnitType		NVARCHAR(255)
    , @CustBreakdownUnitValue		DECIMAL(19,5)

AS 
BEGIN
  ------------------------------------------------------------------------
  SET NOCOUNT ON;

  ------------------------------------------------------------------------

IF NOT EXISTS (
	SELECT 'x'
	FROM dbo.Projects_ProjectTypesBreakdown
	WHERE WBS1 = @WBS1
		AND WBS2 = ' ' --@WBS2
		AND WBS3 = ' ' --@WBS3
		AND Seq = @Seq
)

	INSERT INTO dbo.Projects_ProjectTypesBreakdown
	(
		WBS1
		, WBS2
		, WBS3
		, Seq
		, CreateUser
		, CreateDate
		, ModUser
		, ModDate
		, CustBreakdownProjectType
		, CustBreakdownFee
		, CustBreakdownUnitType
		, CustBreakdownUnitValue
	)
	VALUES
	(
		@WBS1					
		, ' ' --@WBS2					
		, ' ' --@WBS3					
		, REPLACE(CAST(NEWID() AS NVARCHAR(36)), '-', '')					
		, @CreateUser			
		, GETDATE()			
		, @ModUser				
		, GETDATE()				
		, @CustBreakdownProjectType		
		, @CustBreakdownFee				
		, @CustBreakdownUnitType			
		, @CustBreakdownUnitValue		
	)

ELSE

	UPDATE dbo.Projects_ProjectTypesBreakdown
	SET ModUser = @ModUser
		, ModDate = GETDATE()
		, CustBreakdownProjectType = @CustBreakdownProjectType
		, CustBreakdownFee = @CustBreakdownFee
		, CustBreakdownUnitType = @CustBreakdownUnitType
		, CustBreakdownUnitValue = @CustBreakdownUnitValue
	WHERE WBS1 = @WBS1
		AND WBS2 = ' ' --@WBS2
		AND WBS3 = ' ' --@WBS3
		AND Seq = @Seq
  ------------------------------------------------------------------------
END
GO
