SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Halff_UpdateETC_PCTComp] 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @CompanyId VARCHAR(50) = '01'
--DECLARE @Team VARCHAR(50) = '02'
DECLARE @MinPeriod INT
DECLARE @Period INT
DECLARE @Now DATETIME = GETDATE()

SET @MinPeriod = DATEPART(yyyy, @Now) * 100 
SET @Period = DATEPART(yyyy, @Now) * 100 + DATEPART(mm, @Now) 

SET TRANSACTION isolation level READ uncommitted; 

DECLARE @s1 VARCHAR(500) = 'AND (LD.Period <= ' + CAST(@Period AS VARCHAR(50)) + ') '
--DECLARE @s2 VARCHAR(500) = '(((SUBSTRING(PR.Org, 1, 2) = /*N*/''' + @CompanyId + ''') AND (SUBSTRING(PR.Org, 7, 2) = /*N*/''' + @Team + ''') AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A''))'
DECLARE @s2 VARCHAR(500) = '((EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A''))'
DECLARE @s3 VARCHAR(50) = CAST(@Period AS VARCHAR(50))

EXEC Setcurrentactivityonly 
  '4C994243374A4EF1838B04DE49058172', 
' PR LEFT JOIN CL AS CLBill ON CLBill.ClientID = PR.BillingClientID  LEFT JOIN CL ON CL.ClientID = PR.ClientID  LEFT JOIN Contacts AS BLCT ON PR.BillingContactID = BLCT.ContactID,  PR AS LEVEL2, PR AS LEVEL3 ', 
@s1, 
' ',
@s2, 
@s3, 
' ', 
'870' 


DECLARE @tabWBSList TABLE 
( 
    wbs1 NVARCHAR(30) COLLATE database_default, 
    wbs2 NVARCHAR(7) COLLATE database_default, 
    wbs3 NVARCHAR(7) COLLATE database_default 
    PRIMARY KEY(wbs1, wbs2, wbs3) 
) 

INSERT @tabWBSList 
(
	wbs1, 
    wbs2, 
    wbs3
) 
SELECT pr.wbs1, 
		level2.wbs2, 
		level3.wbs3 
FROM   pr AS level3, 
		pr AS LEVEL2, 
		pr, 
		##rwl_4c994243374a4ef1838b04de49058172 
WHERE  LEVEL3.sublevel = 'N' 
		AND pr.wbs1 = LEVEL3.wbs1 
		AND LEVEL2.wbs1 = LEVEL3.wbs1 
		AND LEVEL2.wbs2 = LEVEL3.wbs2 
		AND pr.wbs2 = /*N*/'' 
		AND pr.wbs3 = /*N*/'' 
		AND LEVEL2.wbs3 = /*N*/'' 
		AND ( ( 
			--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
			--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
			--AND 
				EXISTS (SELECT 'x' 
							FROM   cfgchargetype 
							WHERE  cfgchargetype.type = pr.chargetype 
									AND (( cfgchargetype.label = /*N*/'Regular' )) 
							) ) 
				AND ( pr.status = 'A' ) ) 
		AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
		AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
		AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
		AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 

DECLARE @tabContract TABLE 
( 
    contractwbs1                 NVARCHAR(30) COLLATE database_default, 
    contractwbs2                 NVARCHAR(7) COLLATE database_default, 
    contractwbs3                 NVARCHAR(7) COLLATE database_default, 
    contractfee                  DECIMAL(19, 4), 
    contractreimballow           DECIMAL(19, 4), 
    contractconsultfee           DECIMAL(19, 4), 
    contractfeedirectlabor       DECIMAL(19, 4), 
    contractfeedirectexpense     DECIMAL(19, 4), 
    contractreimballowexpense    DECIMAL(19, 4), 
    contractreimballowconsultant DECIMAL(19, 4) 
    PRIMARY KEY(contractwbs1, contractwbs2, contractwbs3) 
) 

INSERT @tabContract 
(
	contractwbs1, 
    contractwbs2, 
    contractwbs3, 
    contractfee, 
    contractreimballow, 
    contractconsultfee, 
    contractfeedirectlabor, 
    contractfeedirectexpense, 
    contractreimballowexpense, 
    contractreimballowconsultant
) 
SELECT contractdetails.wbs1                AS contractWBS1, 
       ' '                                 AS contractWBS2, 
       ' '                                 AS contractWBS3, 
       Sum(contractdetails.fee)            AS contractFee, 
       Sum(contractdetails.reimballow)     AS contractReimbAllow, 
       Sum(contractdetails.consultfee)     AS contractConsultFee, 
       Sum(contractdetails.feedirlab)      AS contractFeeDirectLabor, 
       Sum(contractdetails.feedirexp)      AS contractFeeDirectExpense, 
       Sum(contractdetails.reimballowexp)  AS contractReimbAllowExpense, 
       Sum(contractdetails.reimballowcons) AS contractReimbAllowConsultant 
FROM   contractdetails 
       JOIN contracts 
         ON contractdetails.wbs1 = contracts.wbs1 
            AND contractdetails.contractnumber = contracts.contractnumber 
WHERE  contracts.period <= @Period 
       AND contractdetails.wbs2 = /*N*/' ' 
       AND EXISTS (SELECT * 
                   FROM   @tabWBSList wbsList 
                   WHERE  wbsList.wbs1 = contractdetails.wbs1) 
GROUP  BY contractdetails.wbs1 
UNION ALL 
SELECT contractdetails.wbs1                AS contractWBS1, 
       contractdetails.wbs2                AS contractWBS2, 
       ' '                                 AS contractWBS3, 
       Sum(contractdetails.fee)            AS contractFee, 
       Sum(contractdetails.reimballow)     AS contractReimbAllow, 
       Sum(contractdetails.consultfee)     AS contractConsultFee, 
       Sum(contractdetails.feedirlab)      AS contractFeeDirectLabor, 
       Sum(contractdetails.feedirexp)      AS contractFeeDirectExpense, 
       Sum(contractdetails.reimballowexp)  AS contractReimbAllowExpense, 
       Sum(contractdetails.reimballowcons) AS contractReimbAllowConsultant 
FROM   contractdetails 
       JOIN contracts 
         ON contractdetails.wbs1 = contracts.wbs1 
            AND contractdetails.contractnumber = contracts.contractnumber 
WHERE  contracts.period <= @Period 
       AND NOT contractdetails.wbs2 = /*N*/' ' 
       AND EXISTS (SELECT * 
                   FROM   @tabWBSList wbsList 
                   WHERE  wbsList.wbs1 = contractdetails.wbs1 
                          AND wbsList.wbs2 = contractdetails.wbs2) 
GROUP  BY contractdetails.wbs1, 
          contractdetails.wbs2 
UNION ALL 
SELECT contractdetails.wbs1                AS contractWBS1, 
       contractdetails.wbs2                AS contractWBS2, 
       contractdetails.wbs3                AS contractWBS3, 
       Sum(contractdetails.fee)            AS contractFee, 
       Sum(contractdetails.reimballow)     AS contractReimbAllow, 
       Sum(contractdetails.consultfee)     AS contractConsultFee, 
       Sum(contractdetails.feedirlab)      AS contractFeeDirectLabor, 
       Sum(contractdetails.feedirexp)      AS contractFeeDirectExpense, 
       Sum(contractdetails.reimballowexp)  AS contractReimbAllowExpense, 
       Sum(contractdetails.reimballowcons) AS contractReimbAllowConsultant 
FROM   contractdetails 
       JOIN contracts 
         ON contractdetails.wbs1 = contracts.wbs1 
            AND contractdetails.contractnumber = contracts.contractnumber 
WHERE  contracts.period <= @Period 
       AND NOT contractdetails.wbs3 = /*N*/' ' 
       AND EXISTS (SELECT * 
                   FROM   @tabWBSList wbsList 
                   WHERE  wbsList.wbs1 = contractdetails.wbs1 
                          AND wbsList.wbs2 = contractdetails.wbs2 
                          AND wbsList.wbs3 = contractdetails.wbs3) 
GROUP  BY contractdetails.wbs1, 
          contractdetails.wbs2, 
          contractdetails.wbs3 

DECLARE @results TABLE
(
	group1			nvarchar(30)
	,group2			nvarchar(7)
	,JTDBilling		decimal(19, 4)
	,ETCWBS1		decimal(19, 0)
	,ETCWBS2		decimal(19, 0)
	,EACWBS1		decimal(19, 0)
	,EACWBS2		decimal(19, 0)
	,PctCompWBS1	decimal(19, 0)
	,PctCompWBS2	decimal(19, 0)
	PRIMARY KEY(group1, group2)
)

INSERT INTO @results

SELECT group1
,group2
,JTDBilling
,ETCWBS1
,ETCWBS2
,EACWBS1
,EACWBS2
,PctCompWBS1
,PctCompWBS2
FROM
(
	SELECT group1
	, group2
	, SUM(amtJTD_b) AS JTDBilling
	, TRY_PARSE(MAX(wbs1customCol2) AS NUMERIC(10, 0) USING 'en-US' ) AS ETCWBS1
	, TRY_PARSE(MAX(wbs2customCol2) AS NUMERIC(10, 0) USING 'en-US' ) AS ETCWBS2
	, TRY_PARSE(MAX(wbs1customCol3) AS NUMERIC(10, 0) USING 'en-US' ) AS EACWBS1
	, TRY_PARSE(MAX(wbs2customCol3) AS NUMERIC(10, 0) USING 'en-US' ) AS EACWBS2
	, TRY_PARSE(MAX(wbs1customCol4) AS NUMERIC(10, 0) USING 'en-US' ) AS PctCompWBS1
	, TRY_PARSE(MAX(wbs2customCol4) AS NUMERIC(10, 0) USING 'en-US' ) AS PctCompWBS2
	--, MAX(wbs1customCol1) AS FeeTypeWBS1
	--, MAX(wbs2customCol1) AS FeeTypeWBS2 
	FROM 
	(
		SELECT Isnull(pr.wbs1, '') 
			   AS 
			   group1, 
			   --Min(pr.NAME) 
			   --AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '') AS group2, 
			   --Min(LEVEL2.NAME) 
			   --AS groupDesc2, 
			   0 amtJTD_b,
			   Max(ProjectCustomTabFieldsWBS1.custfeetyperollup) AS wbs1customCol1, 
			   Max(ProjectCustomTabFieldsWBS1.custefforttocomplete) AS wbs1customCol2, 
			   Max(ProjectCustomTabFieldsWBS1.CustEffortatCompletion) AS wbs1customCol3, 
			   Max(ProjectCustomTabFieldsWBS1.custpercentcomplete) AS wbs1customCol4, 
			   Max(ProjectCustomTabFieldsWBS1.CustWriteoffAdjmt) AS wbs1customCol5, 
			   Max(ProjectCustomTabFieldsWBS2.custfeetyperollup) AS wbs2customCol1, 
			   Max(ProjectCustomTabFieldsWBS2.custefforttocomplete) AS wbs2customCol2, 
			   Max(ProjectCustomTabFieldsWBS2.CustEffortatCompletion) AS wbs2customCol3, 
			   Max(ProjectCustomTabFieldsWBS2.custpercentcomplete) AS wbs2customCol4, 
			   Max(ProjectCustomTabFieldsWBS2.CustWriteoffAdjmt) AS wbs2customCol5, 
			   Max(ProjectCustomTabFieldsWBS3.custfeetyperollup) AS wbs3customCol1, 
			   Max(ProjectCustomTabFieldsWBS3.custefforttocomplete) AS wbs3customCol2, 
			   Max(ProjectCustomTabFieldsWBS3.CustEffortatCompletion) AS wbs3customCol3, 
			   Max(ProjectCustomTabFieldsWBS3.custpercentcomplete) AS wbs3customCol4, 
			   Max(ProjectCustomTabFieldsWBS3.CustWriteoffAdjmt) AS wbs3customCol5 
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3 
			   LEFT JOIN projectcustomtabfields AS ProjectCustomTabFieldsWBS3 
					  ON LEVEL3.wbs1 = ProjectCustomTabFieldsWBS3.wbs1 AND LEVEL3.wbs2 = ProjectCustomTabFieldsWBS3.wbs2 AND LEVEL3.wbs3 = ProjectCustomTabFieldsWBS3.wbs3 
			   LEFT JOIN (SELECT LEVEL3.wbs1, 
								 LEVEL3.wbs2           AS MaxWBS2, 
								 Max(PRNested.maxwbs3) AS MaxWBS3, 
								 Max(CASE 
									   WHEN ( LEVEL3.wbs2 <> ' ' 
											   OR LEVEL3.sublevel = 'N' ) 
											AND LEVEL3.wbs3 = ' ' THEN 
									   Isnull(tabContract.contractfee, 0.0) 
									   ELSE -999999999.99 
									 END)              AS LevelSumContractFee, 
								 Max(CASE 
									   WHEN ( LEVEL3.wbs2 <> ' ' 
											   OR LEVEL3.sublevel = 'N' ) 
											AND LEVEL3.wbs3 = ' ' THEN 
									   Isnull(tabContract.contractreimballow, 0.0) 
									   ELSE -999999999.99 
									 END)              AS LevelSumContractReimbAllow, 
								 Max(CASE 
									   WHEN ( LEVEL3.wbs2 <> ' ' 
											   OR LEVEL3.sublevel = 'N' ) 
											AND LEVEL3.wbs3 = ' ' THEN 
									   Isnull(tabContract.contractconsultfee, 0.0) 
									   ELSE -999999999.99 
									 END)              AS LevelSumContractConsultFee, 
								 Max(CASE 
									   WHEN ( LEVEL3.wbs2 <> ' ' 
											   OR LEVEL3.sublevel = 'N' ) 
											AND LEVEL3.wbs3 = ' ' THEN 
									   Isnull(tabContract.contractfeedirectlabor, 0.0) 
									   ELSE -999999999.99 
									 END)              AS LevelSumContractFeeDirectLabor 
								 , 
								 Max(CASE 
									   WHEN ( LEVEL3.wbs2 <> ' ' 
											   OR LEVEL3.sublevel = 'N' ) 
											AND LEVEL3.wbs3 = ' ' THEN 
									   Isnull(tabContract.contractfeedirectexpense, 0.0) 
									   ELSE -999999999.99 
									 END)              AS 
								 LevelSumContractFeeDirectExpense, 
								 Max(CASE 
									   WHEN ( LEVEL3.wbs2 <> ' ' 
											   OR LEVEL3.sublevel = 'N' ) 
											AND LEVEL3.wbs3 = ' ' THEN 
		Isnull(tabContract.contractreimballowexpense, 0.0) 
		ELSE -999999999.99 
		END)              AS LevelSumContractReimbAllowExpense, 
		Max(CASE 
		WHEN ( LEVEL3.wbs2 <> ' ' 
				OR LEVEL3.sublevel = 'N' ) 
			 AND LEVEL3.wbs3 = ' ' THEN Isnull( 
		tabContract.contractreimballowconsultant, 0.0) 
		ELSE -999999999.99 
		END)              AS LevelSumContractReimbAllowConsultant 
		FROM   pr AS LEVEL3 
		INNER JOIN (SELECT LEVEL3.wbs1, 
					 LEVEL3.wbs2, 
					 Max(LEVEL3.wbs3) AS MaxWBS3 
			  FROM   pr, 
					 pr AS LEVEL2, 
					 pr AS LEVEL3, 
					 ##rwl_4c994243374a4ef1838b04de49058172 
			  WHERE  LEVEL3.wbs1 = pr.wbs1 
					 AND LEVEL3.wbs1 = LEVEL2.wbs1 
					 AND LEVEL3.wbs2 = LEVEL2.wbs2 
					 AND pr.wbs2 = ' ' 
					 AND pr.wbs3 = ' ' 
					 AND LEVEL2.wbs3 = ' ' 
					 AND ( ( 
							--( Substring(pr.org, 1, 2) = /*N*/ @CompanyId ) 
							--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
							--AND 
								EXISTS (SELECT 'x' 
										 FROM   cfgchargetype 
										 WHERE 
								 cfgchargetype.type = pr.chargetype 
								 AND (( cfgchargetype.label = /*N*/ 'Regular' ) )) ) 
						   AND ( pr.status = 'A' ) ) 
					AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
					AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
					AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
					AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
		GROUP  BY LEVEL3.wbs1, LEVEL3.wbs2) AS PRNested 
		ON LEVEL3.wbs1 = PRNested.wbs1 AND LEVEL3.wbs2 = PRNested.wbs2 
		LEFT JOIN projectcustomtabfields 
		ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3 
		LEFT JOIN @tabContract tabContract 
		ON tabContract.contractwbs1 = LEVEL3.wbs1 AND tabContract.contractwbs2 = LEVEL3.wbs2 AND tabContract.contractwbs3 = LEVEL3.wbs3 
		GROUP  BY LEVEL3.wbs1, LEVEL3.wbs2) AS WBSLevelSumQuery 
		ON LEVEL3.wbs1 = WBSLevelSumQuery.wbs1 AND LEVEL3.wbs2 = WBSLevelSumQuery.maxwbs2 AND LEVEL3.wbs3 = WBSLevelSumQuery.maxwbs3, 
		pr AS LEVEL2 
		LEFT JOIN projectcustomtabfields AS ProjectCustomTabFieldsWBS2 
		ON LEVEL2.wbs1 = ProjectCustomTabFieldsWBS2.wbs1 AND LEVEL2.wbs2 = ProjectCustomTabFieldsWBS2.wbs2 AND ProjectCustomTabFieldsWBS2.wbs3 = /*N*/'', 
		pr 
		LEFT JOIN projectcustomtabfields AS ProjectCustomTabFieldsWBS1 
		ON pr.wbs1 = ProjectCustomTabFieldsWBS1.wbs1 AND ProjectCustomTabFieldsWBS1.wbs2 = /*N*/'' AND ProjectCustomTabFieldsWBS1.wbs3 = /*N*/'' 
		LEFT JOIN cfgserviceprofile 
		ON cfgserviceprofile.code = pr.servprocode 
		LEFT JOIN fw_cfgcountry Country_PR 
		ON pr.country = Country_PR.isocountrycode, 
		##rwl_4c994243374a4ef1838b04de49058172 
		WHERE  LEVEL3.sublevel = 'N' 
			   AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
						EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
		GROUP  BY Isnull(pr.wbs1, ''), Isnull(LEVEL2.wbs2, '') 

		/***Labor***/ 
		UNION ALL 
		SELECT Isnull(pr.wbs1, '')                 AS group1, 
			   --Min(pr.NAME)                        AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '')             AS group2, 
			   --Min(LEVEL2.NAME)                    AS groupDesc2, 
			   Sum(billext)                        AS amtJTD_b,
			   ''                                  AS wbs1customCol1, 
			   ''                                  AS wbs1customCol2, 
			   ''                                  AS wbs1customCol3, 
			   ''                                  AS wbs1customCol4, 
			   ''                                  AS wbs1customCol5, 
			   ''                                  AS wbs2customCol1, 
			   ''                                  AS wbs2customCol2, 
			   ''                                  AS wbs2customCol3, 
			   ''                                  AS wbs2customCol4, 
			   ''                                  AS wbs2customCol5, 
			   ''                                  AS wbs3customCol1, 
			   ''                                  AS wbs3customCol2, 
			   ''                                  AS wbs3customCol3, 
			   ''                                  AS wbs3customCol4, 
			   ''                                  AS wbs3customCol5
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 
						 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 
						 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3, 
			   pr AS LEVEL2, 
			   pr, 
			   ##rwl_4c994243374a4ef1838b04de49058172, 
			   ld 
		WHERE  ld.wbs1 = LEVEL3.wbs1 
			   AND ld.wbs2 = LEVEL3.wbs2 
			   AND ld.wbs3 = LEVEL3.wbs3 
			   AND ld.projectcost = 'Y' 
			   AND ld.period <= @Period 
			   AND ( ld.period >= @MinPeriod OR pr.chargetype <> 'H' ) 
			   AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
							EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
		GROUP  BY Isnull(pr.wbs1, ''), Isnull(LEVEL2.wbs2, '') 

		/***Overhead***/ 
		UNION ALL 
		SELECT Isnull(pr.wbs1, '')     AS group1, 
			   --Min(pr.NAME)            AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '') AS group2, 
			   --Min(LEVEL2.NAME)        AS groupDesc2,
			   0                       amtJTD_b,
			   ''                                  AS wbs1customCol1, 
			   ''                                  AS wbs1customCol2, 
			   ''                                  AS wbs1customCol3, 
			   ''                                  AS wbs1customCol4, 
			   ''                                  AS wbs1customCol5, 
			   ''                                  AS wbs2customCol1, 
			   ''                                  AS wbs2customCol2, 
			   ''                                  AS wbs2customCol3, 
			   ''                                  AS wbs2customCol4, 
			   ''                                  AS wbs2customCol5, 
			   ''                                  AS wbs3customCol1, 
			   ''                                  AS wbs3customCol2, 
			   ''                                  AS wbs3customCol3, 
			   ''                                  AS wbs3customCol4, 
			   ''                                  AS wbs3customCol5
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3, 
			   pr AS LEVEL2, 
			   pr, 
			   ##rwl_4c994243374a4ef1838b04de49058172, 
			   prf 
		WHERE  ( ( prf.wbs1 = LEVEL3.wbs1 AND prf.wbs2 = LEVEL3.wbs2 AND prf.wbs3 = LEVEL3.wbs3 ) 
				 AND ( prf.wbs1 = LEVEL2.wbs1  AND prf.wbs2 = LEVEL2.wbs2 ) 
				 AND ( prf.wbs1 = pr.wbs1 ) 
				 AND ( pr.wbs2 = /*N*/' ' AND pr.wbs3 = /*N*/' ' AND LEVEL2.wbs3 = /*N*/' ' ) ) 
			   AND pr.chargetype = 'R' 
			   AND prf.period <= @Period 
			   AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
							EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
		GROUP  BY Isnull(pr.wbs1, ''), Isnull(LEVEL2.wbs2, '') 

		/***Direct and Reimbursable Expenses***/ 
		UNION ALL 
		SELECT Isnull(pr.wbs1, '')     AS group1, 
			   --Min(pr.NAME)            AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '') AS group2, 
			   --Min(LEVEL2.NAME)        AS groupDesc2,
			   Sum(billext)            AS amtJTD_b,
			   ''                                  AS wbs1customCol1, 
			   ''                                  AS wbs1customCol2, 
			   ''                                  AS wbs1customCol3, 
			   ''                                  AS wbs1customCol4, 
			   ''                                  AS wbs1customCol5, 
			   ''                                  AS wbs2customCol1, 
			   ''                                  AS wbs2customCol2, 
			   ''                                  AS wbs2customCol3, 
			   ''                                  AS wbs2customCol4, 
			   ''                                  AS wbs2customCol5, 
			   ''                                  AS wbs3customCol1, 
			   ''                                  AS wbs3customCol2, 
			   ''                                  AS wbs3customCol3, 
			   ''                                  AS wbs3customCol4, 
			   ''                                  AS wbs3customCol5
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3, 
			   pr AS LEVEL2, 
			   pr, 
			   ##rwl_4c994243374a4ef1838b04de49058172, 
			   ledgerap 
			   LEFT JOIN ca 
					  ON ledgerap.account = ca.account 
		WHERE  ( ( ledgerap.wbs1 = LEVEL3.wbs1 AND ledgerap.wbs2 = LEVEL3.wbs2 AND ledgerap.wbs3 = LEVEL3.wbs3 ) 
				 AND ( ledgerap.wbs1 = LEVEL2.wbs1 AND ledgerap.wbs2 = LEVEL2.wbs2 ) 
				 AND ( ledgerap.wbs1 = pr.wbs1 ) 
				 AND ( pr.wbs2 = /*N*/' '  AND pr.wbs3 = /*N*/' ' AND LEVEL2.wbs3 = /*N*/' ' ) ) 
			   AND projectcost = 'Y' 
			   AND period <= @Period 
			   AND ( period >= @MinPeriod OR pr.chargetype <> 'H' ) 
			   AND ca.type >= 5 
			   AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
							EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
		GROUP  BY Isnull(pr.wbs1, ''), Isnull(LEVEL2.wbs2, ''), 
				  CASE 
					WHEN ca.type < 7 THEN '5' 
					WHEN ca.type >= 7 AND ca.type < 9 THEN '3' 
					WHEN ca.type >= 9 AND ca.type < 10 THEN '6' 
					ELSE '7' 
				  END, 
				  CASE 
					WHEN ( ca.type = 6 ) OR ( ca.type = 8 ) THEN '0' 
					ELSE '1' 
				  END 

		/***Direct and Reimbursable Expenses***/ 
		UNION ALL 
		SELECT Isnull(pr.wbs1, '')     AS group1, 
			   --Min(pr.NAME)            AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '') AS group2, 
			   --Min(LEVEL2.NAME)        AS groupDesc2, 
			   Sum(billext)            AS amtJTD_b,
			   ''                                  AS wbs1customCol1, 
			   ''                                  AS wbs1customCol2, 
			   ''                                  AS wbs1customCol3, 
			   ''                                  AS wbs1customCol4, 
			   ''                                  AS wbs1customCol5, 
			   ''                                  AS wbs2customCol1, 
			   ''                                  AS wbs2customCol2, 
			   ''                                  AS wbs2customCol3, 
			   ''                                  AS wbs2customCol4, 
			   ''                                  AS wbs2customCol5, 
			   ''                                  AS wbs3customCol1, 
			   ''                                  AS wbs3customCol2, 
			   ''                                  AS wbs3customCol3, 
			   ''                                  AS wbs3customCol4, 
			   ''                                  AS wbs3customCol5 
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3, 
			   pr AS LEVEL2, 
			   pr, 
			   ##rwl_4c994243374a4ef1838b04de49058172, 
			   ledgerar 
			   LEFT JOIN ca 
					  ON ledgerar.account = ca.account 
		WHERE  ( ( ledgerar.wbs1 = LEVEL3.wbs1 AND ledgerar.wbs2 = LEVEL3.wbs2 AND ledgerar.wbs3 = LEVEL3.wbs3 ) 
				 AND ( ledgerar.wbs1 = LEVEL2.wbs1 AND ledgerar.wbs2 = LEVEL2.wbs2 ) 
				 AND ( ledgerar.wbs1 = pr.wbs1 ) 
				 AND ( pr.wbs2 = /*N*/' ' AND pr.wbs3 = /*N*/' ' AND LEVEL2.wbs3 = /*N*/' ' ) ) 
			   AND projectcost = 'Y' 
			   AND period <= @Period 
			   AND ( period >= @MinPeriod OR pr.chargetype <> 'H' ) 
			   AND ca.type >= 5 
			   AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
							EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
		GROUP  BY Isnull(pr.wbs1, ''), Isnull(LEVEL2.wbs2, ''), 
				  CASE 
					WHEN ca.type < 7 THEN '5' 
					WHEN ca.type >= 7 AND ca.type < 9 THEN '3' 
					WHEN ca.type >= 9 AND ca.type < 10 THEN '6' 
					ELSE '7' 
				  END, 
				  CASE 
					WHEN ( ca.type = 6 ) OR ( ca.type = 8 ) THEN '0' 
					ELSE '1' 
				  END 

		/***Direct and Reimbursable Expenses***/ 
		UNION ALL 
		SELECT Isnull(pr.wbs1, '')     AS group1, 
			   --Min(pr.NAME)            AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '') AS group2, 
			   --Min(LEVEL2.NAME)        AS groupDesc2, 
			   Sum(billext)            AS amtJTD_b,
			   ''                                  AS wbs1customCol1, 
			   ''                                  AS wbs1customCol2, 
			   ''                                  AS wbs1customCol3, 
			   ''                                  AS wbs1customCol4, 
			   ''                                  AS wbs1customCol5, 
			   ''                                  AS wbs2customCol1, 
			   ''                                  AS wbs2customCol2, 
			   ''                                  AS wbs2customCol3, 
			   ''                                  AS wbs2customCol4, 
			   ''                                  AS wbs2customCol5, 
			   ''                                  AS wbs3customCol1, 
			   ''                                  AS wbs3customCol2, 
			   ''                                  AS wbs3customCol3, 
			   ''                                  AS wbs3customCol4, 
			   ''                                  AS wbs3customCol5
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3, 
			   pr AS LEVEL2, 
			   pr, 
			   ##rwl_4c994243374a4ef1838b04de49058172, 
			   ledgerex 
			   LEFT JOIN ca 
					  ON ledgerex.account = ca.account 
		WHERE  ( ( ledgerex.wbs1 = LEVEL3.wbs1 AND ledgerex.wbs2 = LEVEL3.wbs2 AND ledgerex.wbs3 = LEVEL3.wbs3 ) 
				 AND ( ledgerex.wbs1 = LEVEL2.wbs1 AND ledgerex.wbs2 = LEVEL2.wbs2 ) 
				 AND ( ledgerex.wbs1 = pr.wbs1 ) 
				 AND ( pr.wbs2 = /*N*/' ' AND pr.wbs3 = /*N*/' ' AND LEVEL2.wbs3 = /*N*/' ' ) ) 
			   AND projectcost = 'Y' 
			   AND period <= @Period 
			   AND ( period >= @MinPeriod OR pr.chargetype <> 'H' ) 
			   AND ca.type >= 5 
			   AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
							EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
		GROUP  BY Isnull(pr.wbs1, ''), Isnull(LEVEL2.wbs2, ''), 
				  CASE 
					WHEN ca.type < 7 THEN '5' 
					WHEN ca.type >= 7 AND ca.type < 9 THEN '3' 
					WHEN ca.type >= 9 AND ca.type < 10 THEN '6' 
					ELSE '7' 
				  END, 
				  CASE 
					WHEN ( ca.type = 6 ) OR ( ca.type = 8 ) THEN '0' 
					ELSE '1' 
				  END 

		/***Direct and Reimbursable Expenses***/ 
		UNION ALL 
		SELECT Isnull(pr.wbs1, '')     AS group1, 
			   --Min(pr.NAME)            AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '') AS group2, 
			   --Min(LEVEL2.NAME)        AS groupDesc2,
			   Sum(billext)            AS amtJTD_b,
			   ''                                  AS wbs1customCol1, 
			   ''                                  AS wbs1customCol2, 
			   ''                                  AS wbs1customCol3, 
			   ''                                  AS wbs1customCol4, 
			   ''                                  AS wbs1customCol5, 
			   ''                                  AS wbs2customCol1, 
			   ''                                  AS wbs2customCol2, 
			   ''                                  AS wbs2customCol3, 
			   ''                                  AS wbs2customCol4, 
			   ''                                  AS wbs2customCol5, 
			   ''                                  AS wbs3customCol1, 
			   ''                                  AS wbs3customCol2, 
			   ''                                  AS wbs3customCol3, 
			   ''                                  AS wbs3customCol4, 
			   ''                                  AS wbs3customCol5
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3, 
			   pr AS LEVEL2, 
			   pr, 
			   ##rwl_4c994243374a4ef1838b04de49058172, 
			   ledgermisc 
			   LEFT JOIN ca 
					  ON ledgermisc.account = ca.account 
		WHERE  ( ( ledgermisc.wbs1 = LEVEL3.wbs1  AND ledgermisc.wbs2 = LEVEL3.wbs2 AND ledgermisc.wbs3 = LEVEL3.wbs3 ) 
				 AND ( ledgermisc.wbs1 = LEVEL2.wbs1 AND ledgermisc.wbs2 = LEVEL2.wbs2 ) 
				 AND ( ledgermisc.wbs1 = pr.wbs1 ) 
				 AND ( pr.wbs2 = /*N*/' ' AND pr.wbs3 = /*N*/' ' AND LEVEL2.wbs3 = /*N*/' ' ) ) 
			   AND projectcost = 'Y' 
			   AND period <= @Period 
			   AND ( period >= @MinPeriod OR pr.chargetype <> 'H' ) 
			   AND ca.type >= 5 
			   AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
							EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
		GROUP  BY Isnull(pr.wbs1, ''), 
				  Isnull(LEVEL2.wbs2, ''), 
				  CASE 
					WHEN ca.type < 7 THEN '5' 
					WHEN ca.type >= 7 AND ca.type < 9 THEN '3' 
					WHEN ca.type >= 9 AND ca.type < 10 THEN '6' 
					ELSE '7' 
				  END, 
				  CASE 
					WHEN ( ca.type = 6 ) OR ( ca.type = 8 ) THEN '0' 
					ELSE '1' 
				  END 

		/***LedgerAR: Billed and Received***/ 
		UNION ALL 
		SELECT Isnull(pr.wbs1, '')     AS group1, 
			   --Min(pr.NAME)            AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '') AS group2, 
			   --Min(LEVEL2.NAME)        AS groupDesc2,
			   0                       amtJTD_b,
			   ''                                  AS wbs1customCol1, 
			   ''                                  AS wbs1customCol2, 
			   ''                                  AS wbs1customCol3, 
			   ''                                  AS wbs1customCol4, 
			   ''                                  AS wbs1customCol5, 
			   ''                                  AS wbs2customCol1, 
			   ''                                  AS wbs2customCol2, 
			   ''                                  AS wbs2customCol3, 
			   ''                                  AS wbs2customCol4, 
			   ''                                  AS wbs2customCol5, 
			   ''                                  AS wbs3customCol1, 
			   ''                                  AS wbs3customCol2, 
			   ''                                  AS wbs3customCol3, 
			   ''                                  AS wbs3customCol4, 
			   ''                                  AS wbs3customCol5
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3, 
			   pr AS LEVEL2, 
			   pr, 
			   ##rwl_4c994243374a4ef1838b04de49058172, 
			   ledgerar 
			   LEFT JOIN ca 
					  ON ledgerar.account = ca.account 
		WHERE  ( ( ledgerar.wbs1 = LEVEL3.wbs1 AND ledgerar.wbs2 = LEVEL3.wbs2 AND ledgerar.wbs3 = LEVEL3.wbs3 ) 
				 AND ( ledgerar.wbs1 = LEVEL2.wbs1 AND ledgerar.wbs2 = LEVEL2.wbs2 ) 
				 AND ( ledgerar.wbs1 = pr.wbs1 ) 
				 AND ( pr.wbs2 = /*N*/' ' AND pr.wbs3 = /*N*/' ' AND LEVEL2.wbs3 = /*N*/' ' ) ) 
			   AND period <= @Period 
			   AND autoentry = 'N' 
			   AND ( ( transtype = /*N*/'IN' 
					   AND Isnull(subtype, ' ') <> 'I' 
					   AND Isnull(subtype, ' ') <> 'R' 
					   AND ( ca.type >= 4 OR ledgerar.account IS NULL ) 
					   AND ( NOT ( transtype = /*N*/'IN' AND linkwbs1 IS NOT NULL ) ) ) 
					  OR ( transtype = /*N*/'CR' 
						   AND ( subtype IN ( 'R', 'X' ) 
								  OR subtype = 'T' 
									 AND Isnull(invoice, /*N*/' ') = /*N*/' ' ) ) ) 
			   AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
							EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
		GROUP  BY Isnull(pr.wbs1, ''), Isnull(LEVEL2.wbs2, '')

		/***LedgerAR: AR, Revenue Columns***/ 
		UNION ALL 
		SELECT Isnull(pr.wbs1, '')     AS group1, 
			   --Min(pr.NAME)            AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '') AS group2, 
			   --Min(LEVEL2.NAME)        AS groupDesc2,
			   0                       amtJTD_b,
			   ''                                  AS wbs1customCol1, 
			   ''                                  AS wbs1customCol2, 
			   ''                                  AS wbs1customCol3, 
			   ''                                  AS wbs1customCol4, 
			   ''                                  AS wbs1customCol5, 
			   ''                                  AS wbs2customCol1, 
			   ''                                  AS wbs2customCol2, 
			   ''                                  AS wbs2customCol3, 
			   ''                                  AS wbs2customCol4, 
			   ''                                  AS wbs2customCol5, 
			   ''                                  AS wbs3customCol1, 
			   ''                                  AS wbs3customCol2, 
			   ''                                  AS wbs3customCol3, 
			   ''                                  AS wbs3customCol4, 
			   ''                                  AS wbs3customCol5
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 
						 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 
						 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3, 
			   pr AS LEVEL2, 
			   pr, 
			   ##rwl_4c994243374a4ef1838b04de49058172, 
			   ledgerar 
			   LEFT JOIN ca 
					  ON ledgerar.account = ca.account 
		WHERE  ( ( ledgerar.wbs1 = LEVEL3.wbs1 AND ledgerar.wbs2 = LEVEL3.wbs2 AND ledgerar.wbs3 = LEVEL3.wbs3 ) 
				 AND ( ledgerar.wbs1 = LEVEL2.wbs1 AND ledgerar.wbs2 = LEVEL2.wbs2 ) 
				 AND ( ledgerar.wbs1 = pr.wbs1 ) 
				 AND ( pr.wbs2 = /*N*/' ' AND pr.wbs3 = /*N*/' ' AND LEVEL2.wbs3 = /*N*/' ' ) ) 
			   AND period <= @Period 
			   AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
							EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
		GROUP  BY Isnull(pr.wbs1, ''), Isnull(LEVEL2.wbs2, '')

		/***LedgerAP: Revenue Columns***/ 
		UNION ALL 
		SELECT Isnull(pr.wbs1, '')     AS group1, 
			   --Min(pr.NAME)            AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '') AS group2, 
			   --Min(LEVEL2.NAME)        AS groupDesc2,
			   0                       amtJTD_b,
			   ''                                  AS wbs1customCol1, 
			   ''                                  AS wbs1customCol2, 
			   ''                                  AS wbs1customCol3, 
			   ''                                  AS wbs1customCol4, 
			   ''                                  AS wbs1customCol5, 
			   ''                                  AS wbs2customCol1, 
			   ''                                  AS wbs2customCol2, 
			   ''                                  AS wbs2customCol3, 
			   ''                                  AS wbs2customCol4, 
			   ''                                  AS wbs2customCol5, 
			   ''                                  AS wbs3customCol1, 
			   ''                                  AS wbs3customCol2, 
			   ''                                  AS wbs3customCol3, 
			   ''                                  AS wbs3customCol4, 
			   ''                                  AS wbs3customCol5
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 
						 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 
						 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3, 
			   pr AS LEVEL2, 
			   pr, 
			   ##rwl_4c994243374a4ef1838b04de49058172, 
			   ledgerap 
			   LEFT JOIN ca 
					  ON ledgerap.account = ca.account 
		WHERE  ( ( ledgerap.wbs1 = LEVEL3.wbs1 AND ledgerap.wbs2 = LEVEL3.wbs2 AND ledgerap.wbs3 = LEVEL3.wbs3 ) 
				 AND ( ledgerap.wbs1 = LEVEL2.wbs1 AND ledgerap.wbs2 = LEVEL2.wbs2 ) 
				 AND ( ledgerap.wbs1 = pr.wbs1 ) 
				 AND ( pr.wbs2 = /*N*/' ' AND pr.wbs3 = /*N*/' ' AND LEVEL2.wbs3 = /*N*/' ' ) ) 
			   AND period <= @Period 
			   AND ca.type = 4 
			   AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
							EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
		GROUP  BY Isnull(pr.wbs1, ''), Isnull(LEVEL2.wbs2, '')

		/***LedgerEX: Revenue Columns***/ 
		UNION ALL 
		SELECT Isnull(pr.wbs1, '')     AS group1, 
			   --Min(pr.NAME)            AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '') AS group2, 
			   --Min(LEVEL2.NAME)        AS groupDesc2,
			   0                       amtJTD_b,
			   ''                                  AS wbs1customCol1, 
			   ''                                  AS wbs1customCol2, 
			   ''                                  AS wbs1customCol3, 
			   ''                                  AS wbs1customCol4, 
			   ''                                  AS wbs1customCol5, 
			   ''                                  AS wbs2customCol1, 
			   ''                                  AS wbs2customCol2, 
			   ''                                  AS wbs2customCol3, 
			   ''                                  AS wbs2customCol4, 
			   ''                                  AS wbs2customCol5, 
			   ''                                  AS wbs3customCol1, 
			   ''                                  AS wbs3customCol2, 
			   ''                                  AS wbs3customCol3, 
			   ''                                  AS wbs3customCol4, 
			   ''                                  AS wbs3customCol5
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3, 
			   pr AS LEVEL2, 
			   pr, 
			   ##rwl_4c994243374a4ef1838b04de49058172, 
			   ledgerex 
			   LEFT JOIN ca 
					  ON ledgerex.account = ca.account 
		WHERE  ( ( ledgerex.wbs1 = LEVEL3.wbs1 AND ledgerex.wbs2 = LEVEL3.wbs2 AND ledgerex.wbs3 = LEVEL3.wbs3 ) 
				AND ( ledgerex.wbs1 = LEVEL2.wbs1 AND ledgerex.wbs2 = LEVEL2.wbs2 ) 
				AND ( ledgerex.wbs1 = pr.wbs1 ) 
				AND ( pr.wbs2 = /*N*/' ' AND pr.wbs3 = /*N*/' ' AND LEVEL2.wbs3 = /*N*/' ' ) ) 
				AND period <= @Period 
				AND ca.type = 4 
				AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
							EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
		GROUP  BY Isnull(pr.wbs1, ''), Isnull(LEVEL2.wbs2, '')
		  
		/***LedgerMisc: Revenue Columns***/ 
		UNION ALL 
		SELECT Isnull(pr.wbs1, '')     AS group1, 
			   --Min(pr.NAME)            AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '') AS group2, 
			   --Min(LEVEL2.NAME)        AS groupDesc2,
			   0                       amtJTD_b,
			   ''                                  AS wbs1customCol1, 
			   ''                                  AS wbs1customCol2, 
			   ''                                  AS wbs1customCol3, 
			   ''                                  AS wbs1customCol4, 
			   ''                                  AS wbs1customCol5, 
			   ''                                  AS wbs2customCol1, 
			   ''                                  AS wbs2customCol2, 
			   ''                                  AS wbs2customCol3, 
			   ''                                  AS wbs2customCol4, 
			   ''                                  AS wbs2customCol5, 
			   ''                                  AS wbs3customCol1, 
			   ''                                  AS wbs3customCol2, 
			   ''                                  AS wbs3customCol3, 
			   ''                                  AS wbs3customCol4, 
			   ''                                  AS wbs3customCol5
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3, 
			   pr AS LEVEL2, 
			   pr, 
			   ##rwl_4c994243374a4ef1838b04de49058172, 
			   ledgermisc 
			   LEFT JOIN ca 
					  ON ledgermisc.account = ca.account 
		WHERE  ( ( ledgermisc.wbs1 = LEVEL3.wbs1 AND ledgermisc.wbs2 = LEVEL3.wbs2 AND ledgermisc.wbs3 = LEVEL3.wbs3 ) 
				 AND ( ledgermisc.wbs1 = LEVEL2.wbs1 AND ledgermisc.wbs2 = LEVEL2.wbs2 ) 
				 AND ( ledgermisc.wbs1 = pr.wbs1 ) 
				 AND ( pr.wbs2 = /*N*/' ' AND pr.wbs3 = /*N*/' ' AND LEVEL2.wbs3 = /*N*/' ' ) ) 
			   AND period <= @Period 
			   AND ca.type = 4 
			   AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
							EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
		GROUP  BY Isnull(pr.wbs1, ''), Isnull(LEVEL2.wbs2, '')   

		/***LedgerMisc: unbilled Gain and Lose Columns***/ 
		UNION ALL 
		SELECT Isnull(pr.wbs1, '')     AS group1, 
			   --Min(pr.NAME)            AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '') AS group2, 
			   --Min(LEVEL2.NAME)        AS groupDesc2,
			   0                       amtJTD_b,
			   ''                                  AS wbs1customCol1, 
			   ''                                  AS wbs1customCol2, 
			   ''                                  AS wbs1customCol3, 
			   ''                                  AS wbs1customCol4, 
			   ''                                  AS wbs1customCol5, 
			   ''                                  AS wbs2customCol1, 
			   ''                                  AS wbs2customCol2, 
			   ''                                  AS wbs2customCol3, 
			   ''                                  AS wbs2customCol4, 
			   ''                                  AS wbs2customCol5, 
			   ''                                  AS wbs3customCol1, 
			   ''                                  AS wbs3customCol2, 
			   ''                                  AS wbs3customCol3, 
			   ''                                  AS wbs3customCol4, 
			   ''                                  AS wbs3customCol5
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3, 
			   pr AS LEVEL2, 
			   pr, 
			   ##rwl_4c994243374a4ef1838b04de49058172, 
			   ledgermisc 
			   LEFT JOIN ca 
					  ON ledgermisc.account = ca.account 
			   LEFT JOIN cfgautoposting 
					  ON cfgautoposting.company = /*N*/@CompanyId 
		WHERE  ( ( ledgermisc.wbs1 = LEVEL3.wbs1 AND ledgermisc.wbs2 = LEVEL3.wbs2 AND ledgermisc.wbs3 = LEVEL3.wbs3 ) 
				 AND ( ledgermisc.wbs1 = LEVEL2.wbs1 AND ledgermisc.wbs2 = LEVEL2.wbs2 ) 
				 AND ( ledgermisc.wbs1 = pr.wbs1 ) 
				 AND ( pr.wbs2 = /*N*/' ' AND pr.wbs3 = /*N*/' ' AND LEVEL2.wbs3 = /*N*/' ' ) ) 
			   AND period <= @Period 
			   AND ( ( cfgautoposting.unbilledservices = ledgermisc.account AND ledgermisc.gainsandlossestype = 'U' ) 
					  OR ( cfgautoposting.unbilledservices1 = ledgermisc.account AND ledgermisc.gainsandlossestype = 'U' ) 
					  OR ( cfgautoposting.unbilledservices2 = ledgermisc.account AND ledgermisc.gainsandlossestype = 'U' ) 
					  OR ( cfgautoposting.unbilledservices3 = ledgermisc.account AND ledgermisc.gainsandlossestype = 'U' ) 
					  OR ( cfgautoposting.unbilledservices4 = ledgermisc.account AND ledgermisc.gainsandlossestype = 'U' ) 
					  OR ( cfgautoposting.unbilledservices5 = ledgermisc.account AND ledgermisc.gainsandlossestype = 'U' ) ) 
			   AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
							EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
		GROUP  BY Isnull(pr.wbs1, ''), Isnull(LEVEL2.wbs2, '') 

		/***Labor Budgets***/ 
		UNION ALL 
		SELECT Isnull(pr.wbs1, '')     AS group1, 
			   --Min(pr.NAME)            AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '') AS group2, 
			   --Min(LEVEL2.NAME)        AS groupDesc2,
			   0                       amtJTD_b,
			   ''                                  AS wbs1customCol1, 
			   ''                                  AS wbs1customCol2, 
			   ''                                  AS wbs1customCol3, 
			   ''                                  AS wbs1customCol4, 
			   ''                                  AS wbs1customCol5, 
			   ''                                  AS wbs2customCol1, 
			   ''                                  AS wbs2customCol2, 
			   ''                                  AS wbs2customCol3, 
			   ''                                  AS wbs2customCol4, 
			   ''                                  AS wbs2customCol5, 
			   ''                                  AS wbs3customCol1, 
			   ''                                  AS wbs3customCol2, 
			   ''                                  AS wbs3customCol3, 
			   ''                                  AS wbs3customCol4, 
			   ''                                  AS wbs3customCol5
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3, 
			   pr AS LEVEL2, 
			   pr, 
			   ##rwl_4c994243374a4ef1838b04de49058172, 
			   lb 
		WHERE  ( ( lb.wbs1 = LEVEL3.wbs1 AND lb.wbs2 = LEVEL3.wbs2 AND lb.wbs3 = LEVEL3.wbs3 ) 
				 AND ( lb.wbs1 = LEVEL2.wbs1 AND lb.wbs2 = LEVEL2.wbs2 ) 
				 AND ( lb.wbs1 = pr.wbs1 ) 
				 AND ( pr.wbs2 = /*N*/' ' AND pr.wbs3 = /*N*/' ' AND LEVEL2.wbs3 = /*N*/' ' ) ) 
			   AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
							EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
		GROUP  BY Isnull(pr.wbs1, ''), Isnull(LEVEL2.wbs2, '') 

		/***OH Budgets***/ 
		UNION ALL 
		SELECT Isnull(pr.wbs1, '')     AS group1, 
			   --Min(pr.NAME)            AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '') AS group2, 
			   --Min(LEVEL2.NAME)        AS groupDesc2,
			   0                       amtJTD_b,
			   ''                                  AS wbs1customCol1, 
			   ''                                  AS wbs1customCol2, 
			   ''                                  AS wbs1customCol3, 
			   ''                                  AS wbs1customCol4, 
			   ''                                  AS wbs1customCol5, 
			   ''                                  AS wbs2customCol1, 
			   ''                                  AS wbs2customCol2, 
			   ''                                  AS wbs2customCol3, 
			   ''                                  AS wbs2customCol4, 
			   ''                                  AS wbs2customCol5, 
			   ''                                  AS wbs3customCol1, 
			   ''                                  AS wbs3customCol2, 
			   ''                                  AS wbs3customCol3, 
			   ''                                  AS wbs3customCol4, 
			   ''                                  AS wbs3customCol5
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3, 
			   pr AS LEVEL2, 
			   pr, 
			   ##rwl_4c994243374a4ef1838b04de49058172, 
			   lb 
		WHERE  ( ( lb.wbs1 = LEVEL3.wbs1 AND lb.wbs2 = LEVEL3.wbs2 AND lb.wbs3 = LEVEL3.wbs3 ) 
				 AND ( lb.wbs1 = LEVEL2.wbs1 AND lb.wbs2 = LEVEL2.wbs2 ) 
				 AND ( lb.wbs1 = pr.wbs1 ) 
				 AND ( pr.wbs2 = /*N*/' ' AND pr.wbs3 = /*N*/' ' AND LEVEL2.wbs3 = /*N*/' ' ) ) 
			   AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
							EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
			   AND pr.chargetype = 'R' 
		GROUP  BY Isnull(pr.wbs1, ''), Isnull(LEVEL2.wbs2, '') 

		/***Direct and Reimbursable Budgets***/ 
		UNION ALL 
		SELECT Isnull(pr.wbs1, '')     AS group1, 
			   --Min(pr.NAME)            AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '') AS group2, 
			   --Min(LEVEL2.NAME)        AS groupDesc2,
			   0                       amtJTD_b,
			   ''                                  AS wbs1customCol1, 
			   ''                                  AS wbs1customCol2, 
			   ''                                  AS wbs1customCol3, 
			   ''                                  AS wbs1customCol4, 
			   ''                                  AS wbs1customCol5, 
			   ''                                  AS wbs2customCol1, 
			   ''                                  AS wbs2customCol2, 
			   ''                                  AS wbs2customCol3, 
			   ''                                  AS wbs2customCol4, 
			   ''                                  AS wbs2customCol5, 
			   ''                                  AS wbs3customCol1, 
			   ''                                  AS wbs3customCol2, 
			   ''                                  AS wbs3customCol3, 
			   ''                                  AS wbs3customCol4, 
			   ''                                  AS wbs3customCol5
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 
						 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 
						 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3, 
			   pr AS LEVEL2, 
			   pr, 
			   ##rwl_4c994243374a4ef1838b04de49058172, 
			   eb 
			   LEFT JOIN ca 
					  ON eb.account = ca.account 
		WHERE  ( ( eb.wbs1 = LEVEL3.wbs1 AND eb.wbs2 = LEVEL3.wbs2 AND eb.wbs3 = LEVEL3.wbs3 ) 
				 AND ( eb.wbs1 = LEVEL2.wbs1 AND eb.wbs2 = LEVEL2.wbs2 ) 
				 AND ( eb.wbs1 = pr.wbs1 ) 
				 AND ( pr.wbs2 = /*N*/' ' AND pr.wbs3 = /*N*/' ' AND LEVEL2.wbs3 = /*N*/' ' ) ) 
			   AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
							EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
		GROUP  BY Isnull(pr.wbs1, ''), Isnull(LEVEL2.wbs2, ''), 
				  CASE 
					WHEN ca.type < 7 THEN '5' 
					WHEN ca.type >= 7 AND ca.type < 9 THEN '3' 
					WHEN ca.type >= 9 AND ca.type < 10 THEN '6' 
					ELSE '7' 
				  END, 
				  CASE 
					WHEN ( ca.type = 6 ) OR ( ca.type = 8 ) THEN '0' 
					ELSE '1' 
				  END 

		UNION ALL 
		SELECT Isnull(pr.wbs1, '')                   AS group1, 
			   --Min(pr.NAME)                          AS groupDesc1, 
			   Isnull(LEVEL2.wbs2, '')               AS group2, 
			   --Min(LEVEL2.NAME)                      AS groupDesc2,
			   0                                     amtJTD_b,
			   NULL									AS wbs1customCol1, 
			   ''                                  AS wbs1customCol2, 
			   ''                                  AS wbs1customCol3, 
			   ''                                  AS wbs1customCol4, 
			   ''                                  AS wbs1customCol5, 
			   ''                                  AS wbs2customCol1, 
			   ''                                  AS wbs2customCol2, 
			   ''                                  AS wbs2customCol3, 
			   ''                                  AS wbs2customCol4, 
			   ''                                  AS wbs2customCol5, 
			   ''                                  AS wbs3customCol1, 
			   ''                                  AS wbs3customCol2, 
			   ''                                  AS wbs3customCol3, 
			   ''                                  AS wbs3customCol4, 
			   ''                                  AS wbs3customCol5
		FROM   pr AS LEVEL3 
			   LEFT JOIN projectcustomtabfields 
					  ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3, 
			   pr AS LEVEL2, 
			   pr, 
			   ##rwl_4c994243374a4ef1838b04de49058172, 
			   pocommitment 
			   LEFT JOIN pomaster 
					  ON pomaster.masterpkey = pocommitment.masterpkey 
			   LEFT JOIN ca 
					  ON ca.account = pocommitment.account 
			   LEFT JOIN ve 
					  ON ve.vendor = pomaster.vendor 
		WHERE  ( ( pocommitment.wbs1 = LEVEL3.wbs1 AND pocommitment.wbs2 = LEVEL3.wbs2 AND pocommitment.wbs3 = LEVEL3.wbs3 ) 
				 AND ( pocommitment.wbs1 = LEVEL2.wbs1 AND pocommitment.wbs2 = LEVEL2.wbs2 ) 
				 AND ( pocommitment.wbs1 = pr.wbs1 ) 
				 AND ( pr.wbs2 = /*N*/' ' AND pr.wbs3 = /*N*/' ' AND LEVEL2.wbs3 = /*N*/' ' ) ) 
			   AND ( ( 
						--( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
						--AND ( Substring(pr.org, 7, 2) = /*N*/@Team ) 
						--AND 
							EXISTS (SELECT 'x' 
								   FROM   cfgchargetype 
								   WHERE  cfgchargetype.type = pr.chargetype 
										  AND (( cfgchargetype.label = /*N*/'Regular' )) 
								  ) ) 
					 AND ( pr.status = 'A' ) ) 
			   AND LEVEL3.wbs1 = ##rwl_4c994243374a4ef1838b04de49058172.wbs1 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs2 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.wbs3 = /*N*/' ' 
			   AND ##rwl_4c994243374a4ef1838b04de49058172.sessionid = '4C994243374A4EF1838B04DE49058172' 
			   AND pr.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs1 = LEVEL3.wbs1 
			   AND LEVEL2.wbs2 = LEVEL3.wbs2 
			   AND pr.wbs2 = /*N*/'' 
			   AND pr.wbs3 = /*N*/'' 
			   AND LEVEL2.wbs3 = /*N*/'' 
		GROUP  BY Isnull(pr.wbs1, ''), Isnull(LEVEL2.wbs2, ''), 
				  CASE 
					WHEN ca.type < 7 THEN '5' 
					WHEN ca.type >= 7 AND ca.type < 9 THEN '3' 
					WHEN ca.type >= 9 AND ca.type < 10 THEN '6' 
					ELSE '7' 
				  END, 
				  CASE 
					WHEN ( ca.type = 6 ) OR ( ca.type = 8 ) THEN '0' 
					ELSE '1' 
				  END 
	
	) AS alpha

	GROUP BY group1, group2
) AS beta
ORDER  BY group1, group2

UPDATE ProjectCustomTabFields 
SET CustEfforttoComplete = alpha.EfforttoComplete
, CustPercentComplete = alpha.PercentComplete
FROM
(

	-- Phase level
	SELECT group1
	,group2
	,JTDBilling
	,EACWBS2 AS EAC
	,CASE WHEN EACWBS2 IS NOT NULL THEN FORMAT((EACWBS2 - JTDBilling), '#,0;(#,0)') ELSE NULL END AS EfforttoComplete
	,CASE WHEN (EACWBS2 IS NOT NULL AND EACWBS2 != 0) 
		THEN CASE WHEN JTDBilling >= EACWBS2
			THEN '100%'
			ELSE REPLACE(FORMAT(JTDBilling / EACWBS2, 'P0'), ' ', '')  
		END
		ELSE NULL
	END AS PercentComplete
	FROM @results
	UNION
	-- Project level
	SELECT group1
	, '' AS group2
	, JTDBilling
	, EAC
	,CASE WHEN EAC IS NOT NULL THEN FORMAT((EAC - JTDBilling), '#,0;(#,0)') ELSE NULL END AS EfforttoComplete
	,CASE WHEN (EAC IS NOT NULL AND EAC != 0) 
		THEN CASE WHEN JTDBilling >= EAC
			THEN '100%'
			ELSE REPLACE(FORMAT(JTDBilling / EAC, 'P0'), ' ', '')  
		END
		ELSE NULL
	END AS PercentComplete
	FROM
	(
		SELECT group1
		,SUM(JTDBilling) AS JTDBilling
		,MAX(EACWBS1) AS EAC
		FROM @results	
		GROUP BY group1
	) AS beta
) alpha

WHERE ProjectCustomTabFields.WBS1 = group1
		AND ProjectCustomTabFields.WBS2 = group2
		AND ProjectCustomTabFields.WBS3 = ''

END
GO
