SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_PAT_AddDisplayColumn](
	@OverridePriorSettings	char(1),
	@Header					varchar(255),
	@FieldName				varchar(255),
	@ColType				char(1),
	@DrilldownColumn		char(1),
	@ColSeq					int,
	@ColDecimals			int,
	@ColWidth				int = 0,
	@ColEnabledWBS1			char(1)='Y',
	@ColEnabledWBS2			char(1)='Y',
	@ColEnabledWBS3			char(1)='Y',
	@ColStatus				char(1)='A',
	@ColViewRoles			varchar(255)='')
AS BEGIN
/*
	Copyright (c) 2011 Central Consulting Group.  All rights reserved.
	
	select * from CCG_PAT_ConfigCustomColumns
	select * from CCG_PAT_CustomColumns

	Examples:
	exec spCCG_PAT_AddDisplayColumn 'Y', 'JTD\nBilled', 'JTDBilled', 'C', 'N', 1, 0, 0
	exec spCCG_PAT_AddDisplayColumn 'Y', 'JTD\nBilled', 'JTDBilled', 'C', 'Y', 1, 0, 0
	
	exec spCCG_PAT_AddDisplayColumn 'Y', 'Fee % Compl', 'FeePctComplete', 'N', 100, 0, 0, 'Y', 'Y', 'N'
	exec spCCG_PAT_AddDisplayColumn 'Y', 'Msg',         'FeeBilling',     'N', 101, 0, 0
	
	Remove all UDCs and all associated data:
	delete from CCG_PAT_ConfigCustomColumns
	DROP TABLE CCG_PAT_CustomColumns
	CREATE TABLE [dbo].[CCG_PAT_CustomColumns](
		[WBS1] [varchar](30) NOT NULL,
		[WBS2] [varchar](7) NOT NULL,
		[WBS3] [varchar](7) NOT NULL,
	 CONSTRAINT [CCG_PAT_CustomColumnsPK] PRIMARY KEY NONCLUSTERED 
	(
		[WBS1] ASC,
		[WBS2] ASC,
		[WBS3] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
*/
	set nocount on
	declare @sql	varchar(max)
	declare @debug	char(1)
	set @debug='N'
	
	print @Header
	-- Add column into EI configuration
	if @OverridePriorSettings='Y'
		delete from CCG_PAT_ConfigCustomColumns where Label=@Header or InitialValue='dbo.fnCCG_PAT_' + @FieldName
	
	set @sql='if not exists (select ''x'' from CCG_PAT_ConfigCustomColumns where Label=''' + @Header + ''')' + CHAR(13) + CHAR(10) +
		' begin' + CHAR(13) + CHAR(10) +
		'	print ''	Adding configuration row for ' + @FieldName + '''' + CHAR(13) + CHAR(10) +
		'	insert into CCG_PAT_ConfigCustomColumns (Label, DataType, DisplayOrder, ' +
		'		InitialValue, Link,' + CHAR(13) + CHAR(10) +
		'		DatabaseField, ColumnUpdate, Width, Decimals,' + CHAR(13) + CHAR(10) +
		'		ViewRoles, EditRoles, RequiredRoles, RequiredExpression,' + CHAR(13) + CHAR(10) +
		'		EnabledWBS1, EnabledWBS2, EnabledWBS3, Status, ModDate, ModUser)' + CHAR(13) + CHAR(10) +
		'	values (''' + @Header + ''', ''' + @ColType + ''', ' + Cast(@ColSeq as varchar(10)) + CHAR(13) + CHAR(10) +
		'		, ''dbo.fnCCG_PAT_' + @FieldName + ''','
	if @DrilldownColumn='Y'
		set @sql = @sql + '''dbo.fnCCG_PAT_' + @FieldName + 'Link'',' + CHAR(13) + CHAR(10)
	else
		set @sql = @sql + ''''',' + CHAR(13) + CHAR(10)
	set @sql = @sql + 
		'		'''', '''', ' + Cast(@ColWidth as varchar(10)) + ', ' + + Cast(@ColDecimals as varchar(10)) + + ', ' + CHAR(13) + CHAR(10) +
		'		'''', '''', '''', '''',   ' + CHAR(13) + CHAR(10) +
		'		''' + @ColEnabledWBS1 + ''', ''' + @ColEnabledWBS2 + ''', ''' + @ColEnabledWBS3 + ''', ''' + @ColStatus + ''', ' + CHAR(13) + CHAR(10) +
		'		GETDATE(),''CCG'')' + CHAR(13) + CHAR(10) +
		' end'
	if @debug='Y' print @sql
	execute(@sql)
END
GO
