SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_LoadBankCode] (
	@visionMultiCompany		bit,
	@code					Nvarchar(10),
	@company				Nvarchar(14)
) AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_LoadBankCode] 1, '', ''
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max);

	SET @sql = N'
		SELECT code, Description' + (CASE WHEN @visionMultiCompany = 1 THEN '+ '' ('' + Org + '')''' ELSE '' END) + ',
			case when code = N''' + @code + ''' then code else '''' end
			FROM CFGBanks ';
    IF ISNULL(@company, N'') <> N''
		SET @sql = @sql + '
			WHERE Company = N''' + @company + ''' ';

    SET @sql = @sql + '
		ORDER BY 2';

	PRINT @sql
	EXEC(@sql);
END;

GO
