SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_AddHistoryRecord] ( @actionTaken varchar(20), @takenBy nvarchar(32), @recipient nvarchar(32), @WBS1 nvarchar(32), @invoiceStage varchar(50), @priorInvoiceStage varchar(50), @autoloadStage bit, @delegateFor nvarchar(32), @invoice nvarchar(20))
             AS EXEC spCCG_EI_AddHistoryRecord @actionTaken,@takenBy,@recipient,@WBS1,@invoiceStage,@priorInvoiceStage,@autoloadStage,@delegateFor,@invoice
GO
