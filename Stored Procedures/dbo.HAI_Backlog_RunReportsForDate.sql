SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[HAI_Backlog_RunReportsForDate] 
	@date AS DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/* CROSS OVER TO THE DATAMART DB ON THE SAME INSTANCE, AND INVOKE THE STORED PROCEDURE WITH @date */
	EXEC [DataMart].[dbo].[HAI_BacklogGenerateReportsForDate] @date
    
END
GO
