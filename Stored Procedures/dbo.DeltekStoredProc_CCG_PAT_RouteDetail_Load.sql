SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_RouteDetail_Load] ( @VISION_LANGUAGE varchar(10))
             AS EXEC spCCG_PAT_RouteDetail_Load @VISION_LANGUAGE
GO
