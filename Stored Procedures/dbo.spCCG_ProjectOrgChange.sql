SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_ProjectOrgChange] @WBS1 varchar (30), @Org varchar (20)
AS
/*
Copyright (c) 2021 Central Consulting Group. All rights reserved.
07/08/2021	David Springer
			Update Project Org because not available in column field change
			Call this from a Project change workflow when Org has changed
*/
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

	Update p
	Set p.Org = o.CustOrganization
	From PR p, UDIC_OrganizationStructure o
	Where p.WBS1 = @WBS1
	  and Left(o.CustOrganization, 2) = Left(@Org, 2)
	  and Right(o.CustOrganization, 3) = Right(@Org, 3)
	  and o.CustPrimaryTeamLocation = 'Y'
END


GO
