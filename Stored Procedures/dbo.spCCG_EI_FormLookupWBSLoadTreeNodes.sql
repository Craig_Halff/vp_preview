SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_FormLookupWBSLoadTreeNodes] (
	@exactMatch		bit,
	@filter			Nvarchar(max),
	@statusFilter	varchar(max),
	@projectFilter	Nvarchar(max)
)
AS
BEGIN
	/* Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved. */
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	declare @sql Nvarchar(max)

	if @exactMatch = 1 AND isnull(@filter, '') <> ''
		select WBS1, Name, isnull(Status, 'D'), 'N' as Sublevel, ReadyForProcessing
			from PR
			where WBS1 = @filter and WBS2 = N' ' and WBS3 = N' '
	else
	begin
		set @sql = N'
        select PR.WBS1, Name, isnull(Status, ''D''), ''N'' as Sublevel, ReadyForProcessing
			from PR	'+(case when charindex('PCTF.', @projectFilter) > 0 then '
				LEFT JOIN ProjectCustomTabFields PCTF on PCTF.WBS1 = PR.WBS1 and PCTF.WBS2 = PR.WBS2 and PCTF.WBS3 = PR.WBS3 ' else '' end)+ '
			where PR.WBS3 = N'' '' and PR.WBS2 = N'' '' and (
				PR.WBS1 = N''' + isnull(@filter, '') + '''
					or (Status ' + @statusFilter + ')) ' +
				@projectFilter + '
			order by 1'
		exec (@sql)
	end
END
GO
