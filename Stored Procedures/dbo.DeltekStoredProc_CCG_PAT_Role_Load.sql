SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Role_Load]
	@VISION_LANGUAGE	varchar(10)
AS
BEGIN
	SET NOCOUNT ON;
    SELECT T1.Role, VisionField, IsNull(T2.ColumnLabel, ISNULL(T1.ColumnLabel, '')) as ColumnLabel,
			ColumnOrder, IsNull(T2.RoleDescription, IsNull(T1.RoleDescription, '')) as RoleDescription,
			MarkupColor, HighlightColor, HidePdfTools, Status, AllowMessaging, AdvancedOptions
        FROM CCG_PAT_ConfigRoles T1
            LEFT JOIN CCG_PAT_ConfigRolesDescriptions T2
				ON T2.Role = T1.Role and T2.UICultureName = @VISION_LANGUAGE
        ORDER BY ColumnOrder;
END;
GO
