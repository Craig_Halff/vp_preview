SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpMoveDates]
  @strPlanID VARCHAR(32),
  @intNumDays int
AS

BEGIN -- Procedure rpMoveDates

  SET NOCOUNT ON
     
  -- Don't need to update Plan's Start/End Dates here
  -- since these fields were changed earlier by the Plan Insert/Update code.

  -- Update Tasks.
  
  UPDATE RPTask
    SET StartDate = DATEADD(d, @intNumDays, StartDate),
        EndDate = DATEADD(d, @intNumDays, EndDate)
    WHERE PlanID = @strPlanID
  
  -- Update Assignments.
  
  UPDATE RPAssignment
    SET StartDate = DATEADD(d, @intNumDays, StartDate),
        EndDate = DATEADD(d, @intNumDays, EndDate)
    WHERE PlanID = @strPlanID
    
  -- Update Expenses.
  
  UPDATE RPExpense
    SET StartDate = DATEADD(d, @intNumDays, StartDate),
        EndDate = DATEADD(d, @intNumDays, EndDate)
    WHERE PlanID = @strPlanID
    
  -- Update Consultants.
  
  UPDATE RPConsultant
    SET StartDate = DATEADD(d, @intNumDays, StartDate),
        EndDate = DATEADD(d, @intNumDays, EndDate)
    WHERE PlanID = @strPlanID
  
  -- Update Units.
  
  UPDATE RPUnit
    SET StartDate = DATEADD(d, @intNumDays, StartDate),
        EndDate = DATEADD(d, @intNumDays, EndDate)
    WHERE PlanID = @strPlanID
  
  -- Update Accordion Format.
  
  UPDATE RPAccordionFormat
    SET StartDate = DATEADD(d, @intNumDays, StartDate),
        EndDate = DATEADD(d, @intNumDays, EndDate)
    WHERE PlanID = @strPlanID

  -- Update Planned Labor TPD.
  
  UPDATE RPPlannedLabor
    SET StartDate = DATEADD(d, @intNumDays, StartDate),
        EndDate = DATEADD(d, @intNumDays, EndDate)
    WHERE PlanID = @strPlanID

  -- Update Planned Expense TPD.
  
  UPDATE RPPlannedExpenses
    SET StartDate = DATEADD(d, @intNumDays, StartDate),
        EndDate = DATEADD(d, @intNumDays, EndDate)
    WHERE PlanID = @strPlanID
    
  -- Update Planned Consultant TPD.
  
  UPDATE RPPlannedConsultant
    SET StartDate = DATEADD(d, @intNumDays, StartDate),
        EndDate = DATEADD(d, @intNumDays, EndDate)
    WHERE PlanID = @strPlanID
    
  -- Update Planned Unit TPD.
  
  UPDATE RPPlannedUnit
    SET StartDate = DATEADD(d, @intNumDays, StartDate),
        EndDate = DATEADD(d, @intNumDays, EndDate)
    WHERE PlanID = @strPlanID
    
  -- Update RPCompensationFee.
  
  UPDATE RPCompensationFee
    SET StartDate = DATEADD(d, @intNumDays, StartDate),
        EndDate = DATEADD(d, @intNumDays, EndDate)
    WHERE PlanID = @strPlanID

  -- Update RPConsultantFee.
  
  UPDATE RPConsultantFee
    SET StartDate = DATEADD(d, @intNumDays, StartDate),
        EndDate = DATEADD(d, @intNumDays, EndDate)
    WHERE PlanID = @strPlanID

  -- Update RPReimbAllowance.
  
  UPDATE RPReimbAllowance
    SET StartDate = DATEADD(d, @intNumDays, StartDate),
        EndDate = DATEADD(d, @intNumDays, EndDate)
    WHERE PlanID = @strPlanID

  -- Update RPEVT.
  
  UPDATE RPEVT
    SET StartDate = DATEADD(d, @intNumDays, StartDate),
        EndDate = DATEADD(d, @intNumDays, EndDate)
    WHERE PlanID = @strPlanID
    
  -- Remake Calendar Interval records.
  -- Need to delete all Calendar Interval records first to avoid PK Violation.
  
  DELETE RPCalendarInterval WHERE PlanID = @strPlanID
  
  DECLARE csrAF CURSOR FOR
    SELECT AccordionFormatID FROM RPAccordionFormat WHERE PlanID = @strPlanID
    
  DECLARE @strAccordionFormatID varchar(32)

  OPEN csrAF

  FETCH NEXT FROM csrAF INTO @strAccordionFormatID

  WHILE (@@FETCH_STATUS = 0)
    BEGIN
    
      EXECUTE rpMakeCI @strAccordionFormatID 

      FETCH NEXT FROM csrAF INTO @strAccordionFormatID

    END -- While

  CLOSE csrAF
  DEALLOCATE csrAF

  SET NOCOUNT OFF

END -- rpMoveDates
GO
