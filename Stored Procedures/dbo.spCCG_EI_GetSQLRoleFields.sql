SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_GetSQLRoleFields](@sqlPart varchar(max) output)
AS BEGIN
	-- Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved.
	/*
	declare @sqlpart varchar(max)
	EXEC [spCCG_EI_GetSQLRoleFields] @sqlPart = @sqlpart OUTPUT
	print @sqlpart
	*/
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	declare @rtnVal varchar(max) = (select top 1 SQLPart from CCG_EI_SQLPartsCache where [Type] = 'RoleFields')

	if isnull(@rtnVal,'') <> ''
	begin
		SET @sqlPart = @rtnVal
		RETURN
	end
	else begin
		set @rtnVal = ''

		declare @first bit = 1, @Role varchar(30), @VisionField varchar(max)
		declare cursorRoles cursor fast_forward for
			select Role, VisionField
				from CCG_EI_ConfigRoles
				where Status = 'A' and VisionField <> ''
				order by ColumnOrder

		open cursorRoles
		fetch next from cursorRoles into @Role, @VisionField
		while @@fetch_status=0
		begin
			if left(@VisionField, 4) = 'Cust' set @VisionField = 'PCTF.'+@VisionField
			else if left(@VisionField, 3) <> 'PR.' set @VisionField = 'PR.'+@VisionField

			set @rtnVal = @rtnVal + ', '+@VisionField+' '
			fetch next from cursorRoles into @Role, @VisionField
		end
		close cursorRoles
		deallocate cursorRoles
	end
	delete from CCG_EI_SQLPartsCache where [Role] is null and [Type] = 'RoleFields'
	insert into CCG_EI_SQLPartsCache ([Role], [Type], [SQLPart]) values (NULL, 'RoleFields', @rtnVal)

	SET @sqlPart = @rtnVal
END
GO
