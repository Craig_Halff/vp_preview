SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Get_Roles]
AS
BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_EI_Config_Get_Roles]
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	Select [Role] as [Key], [Role] as Value
		FROM SE
		ORDER BY 1;
END;
GO
