SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[HAI_UpdateCustomEAC_EACInputLevel_WriteoffAdjmt]
	@PeriodEnd NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE p
SET p.CustEffortatCompletion = b.CustEffortatCompletion,
    p.CustEACInputLevel = b.CustEACInputLevel,		
	p.CustWriteoffAdjmt = b.CustWriteoffAdjmt
FROM [ProjectCustomTabFields] p, [HAI_RevenueImportUpdatedValues] b
WHERE p.WBS1 = b.WBS1
    AND p.WBS2 = b.WBS2
    AND p.WBS3 = ' '

END
GO
