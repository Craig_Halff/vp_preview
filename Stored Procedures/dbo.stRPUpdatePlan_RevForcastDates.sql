SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPUpdatePlan_RevForcastDates]  
	@strWBS1 nvarchar(30)
AS  
  
BEGIN -- Procedure stRPUpdatePlan_RevForcastDates  

	DECLARE @strPlanID varchar(32)
	DECLARE @strAllocMethod varchar(1)
	DECLARE @tmpStartDate Datetime = NULL
	DECLARE @tmpEndDate Datetime = NULL
	DECLARE @tmpRevStartDate Datetime = NULL
	DECLARE @tmpRevEndDate Datetime = NULL

	SELECT top 1 @strAllocMethod = AllocMethod FROM PR WHERE WBS1 = @strWBS1
	SELECT @strPlanID = PlanID FROM PNPlan WHERE WBS1 = @strWBS1
		
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#tempTasksToProcess')) 
	BEGIN
	   DROP TABLE #tempTasksToProcess;
	END;
	CREATE TABLE #tempTasksToProcess (
		TaskID VARCHAR(32) COLLATE DATABASE_DEFAULT, 
		PlanID VARCHAR(32) COLLATE DATABASE_DEFAULT,
		WBS1 NVARCHAR(30) COLLATE DATABASE_DEFAULT,
		WBS2 NVARCHAR(30) COLLATE DATABASE_DEFAULT,
		WBS3 NVARCHAR(30) COLLATE DATABASE_DEFAULT,
		StartDate DATETIME, 
		EndDate DATETIME, 
		RevenueStartDate DATETIME, 
		RevenueEndDate DATETIME,
		OutlineNumber VARCHAR(255) COLLATE DATABASE_DEFAULT,
		ParentOutlineNumber VARCHAR(255) COLLATE DATABASE_DEFAULT,
		OutlineLevel INT
	)

	INSERT #tempTasksToProcess(
		TaskID,
		PlanID,
		WBS1,
		WBS2, 
		WBS3, 
		StartDate, 
		EndDate, 
		RevenueStartDate,
		RevenueEndDate,
		OutlineNumber,
		ParentOutlineNumber,
		OutlineLevel
	)
	SELECT TaskID,
		PlanID,
		WBS1,
		WBS2, 
		WBS3, 
		StartDate, 
		EndDate, 
		RevenueStartDate,
		RevenueEndDate,
		OutlineNumber,
		ParentOutlineNumber,
		OutlineLevel
	FROM PNTask WHERE PlanID = @strPlanID

	-- for nodes having wbs3 values as not null 
	
	UPDATE T  set @tmpStartDate = COALESCE(PR.StartDate,PR.EstStartDate,PR.ActCompletionDate,PR.EstCompletionDate, NULL),
					@tmpEndDate = CASE WHEN PR.ActCompletionDate >= COALESCE(PR.StartDate,'') THEN PR.ActCompletionDate 
							WHEN PR.EstCompletionDate >= COALESCE(PR.StartDate,'') THEN PR.EstCompletionDate 
						ELSE COALESCE(PR.StartDate,PR.EstStartDate,PR.ActCompletionDate,PR.EstCompletionDate, NULL) END,
						T.StartDate = @tmpStartDate,
						T.EndDate = CASE WHEN @tmpStartDate IS NOT NULL AND @tmpEndDate IS NOT NULL AND @tmpEndDate < @tmpStartDate THEN @tmpStartDate ELSE @tmpEndDate END
						,T.RevenueStartDate = CASE WHEN @strAllocMethod = 'r' AND COALESCE(PR.StartDate,PR.EstStartDate,PR.ActCompletionDate,PR.EstCompletionDate, NULL) IS NOT NULL THEN @tmpStartDate ELSE T.RevenueStartDate END,
						T.RevenueEndDate= CASE WHEN @strAllocMethod = 'r' AND COALESCE(PR.StartDate,PR.EstStartDate,PR.ActCompletionDate,PR.EstCompletionDate, NULL) IS NOT NULL THEN 
								CASE WHEN @tmpStartDate IS NOT NULL AND @tmpEndDate IS NOT NULL AND @tmpEndDate < @tmpStartDate THEN @tmpStartDate ELSE @tmpEndDate END 
							ELSE T.RevenueEndDate END 
	FROM PR
         INNER JOIN #tempTasksToProcess as T ON PR.WBS1 = T.WBS1 AND PR.WBS2 = ISNULL(T.WBS2,' ') AND PR.WBS3 = ISNULL(T.WBS3,' ')
	WHERE T.PlanID = @strPlanID and T.WBS2 IS NOT NULL AND T.WBS3 IS NOT NULL
		AND T.StartDate IS NULL AND t.EndDate IS NULL
	
	-- for parent dates working up to WBS2 then to WBS1
	UPDATE  T 
		SET @tmpStartDate =  CASE WHEN (PR.StartDate <= X2.MinDate OR X2.MinDate IS NULL) AND PR.StartDate IS NOT NULL THEN PR.StartDate 
							WHEN (PR.EstStartDate <= X2.MinDate OR  X2.MinDate IS NULL) AND PR.EstStartDate IS NOT NULL  THEN PR.EstStartDate 
						ELSE COALESCE(X2.MinDate,PR.ActCompletionDate,PR.EstCompletionDate) END ,
			@tmpEndDate = CASE WHEN (PR.ActCompletionDate >= X2.MaxDate AND PR.ActCompletionDate >= X2.MinDate OR X2.MinDate IS NULL) AND PR.ActCompletionDate IS NOT NULL THEN PR.ActCompletionDate 
							WHEN (PR.EstCompletionDate >= X2.MaxDate AND PR.EstCompletionDate >= X2.MinDate OR x2.MinDate IS NULL) AND PR.EstCompletionDate IS NOT NULL THEN PR.EstCompletionDate 
						ELSE COALESCE(X2.MaxDate,@tmpStartDate) END ,
			@tmpRevStartDate =  CASE WHEN (PR.StartDate <= X2.MinRevDate OR X2.MinRevDate IS NULL) AND PR.StartDate IS NOT NULL THEN PR.StartDate 
							WHEN (PR.EstStartDate <= X2.MinRevDate OR  X2.MinRevDate IS NULL) AND PR.EstStartDate IS NOT NULL  THEN PR.EstStartDate 
						ELSE COALESCE(X2.MinRevDate,PR.ActCompletionDate,PR.EstCompletionDate) END ,
			@tmpRevEndDate = CASE WHEN (PR.ActCompletionDate >= X2.MaxRevDate AND PR.ActCompletionDate >= X2.MinRevDate OR X2.MinRevDate IS NULL) AND PR.ActCompletionDate IS NOT NULL THEN PR.ActCompletionDate 
							WHEN (PR.EstCompletionDate >= X2.MaxRevDate AND PR.EstCompletionDate >= X2.MinRevDate OR x2.MinRevDate IS NULL) AND PR.EstCompletionDate IS NOT NULL THEN PR.EstCompletionDate 
						ELSE COALESCE(X2.MaxRevDate,@tmpStartDate) END ,
			T.StartDate = @tmpStartDate,
			T.EndDate = CASE WHEN @tmpStartDate IS NOT NULL AND @tmpEndDate IS NOT NULL AND @tmpEndDate < @tmpStartDate THEN @tmpStartDate ELSE @tmpEndDate END
			,T.RevenueStartDate = CASE WHEN @strAllocMethod = 'r' AND COALESCE(PR.StartDate,PR.EstStartDate,PR.ActCompletionDate,PR.EstCompletionDate, NULL) IS NOT NULL THEN @tmpRevStartDate ELSE T.RevenueStartDate END,
			T.RevenueEndDate= CASE WHEN @strAllocMethod = 'r' AND COALESCE(PR.StartDate,PR.EstStartDate,PR.ActCompletionDate,PR.EstCompletionDate, NULL) IS NOT NULL THEN 
					CASE WHEN @tmpRevStartDate IS NOT NULL AND @tmpRevEndDate IS NOT NULL AND @tmpRevEndDate < @tmpRevStartDate THEN @tmpRevStartDate ELSE @tmpRevEndDate END
				ELSE T.RevenueEndDate END 
	FROM PR
         INNER JOIN #tempTasksToProcess as T ON PR.WBS1 = T.WBS1 AND PR.WBS2 = T.WBS2 and PR.WBS3 = ' '
		  LEFT JOIN (SELECT COUNT(*) AS ACount, MIN(StartDate) AS MinDate, MAX(EndDate) AS MaxDate, MIN(RevenueStartDate) AS MinRevDate, MAX(RevenueEndDate) AS MaxRevDate, WBS1, WBS2 
				  FROM #tempTasksToProcess AS A WHERE A.WBS1 = @strWBS1 AND WBS2 IS NOT NULL AND WBS3 IS NOT NULL GROUP BY WBS1, WBS2) AS X2 ON X2.WBS1 = PR.WBS1 AND X2.WBS2 = PR.WBS2 
	WHERE T.PlanID = @strPlanID AND T.WBS1 = @strWBS1 AND T.WBS2 IS NOT NULL AND T.WBS3 IS NULL
	AND T.StartDate IS NULL and t.EndDate IS NULL
 
	UPDATE  T 
		SET @tmpStartDate =  CASE WHEN (T.StartDate <= X2.MinDate OR X2.MinDate IS NULL) THEN T.StartDate ELSE X2.MinDate END ,
			@tmpEndDate =	CASE WHEN (T.EndDate >= X2.MaxDate OR X2.MinDate IS NULL) THEN T.EndDate ELSE X2.MaxDate END ,
			@tmpRevStartDate =  CASE WHEN (T.RevenueStartDate <= X2.MinRevDate OR X2.MinRevDate IS NULL) THEN T.RevenueStartDate ELSE X2.MinRevDate END ,
			@tmpRevEndDate =	CASE WHEN (T.RevenueEndDate >= X2.MaxRevDate OR X2.MinRevDate IS NULL) THEN T.RevenueEndDate ELSE X2.MaxRevDate END ,
			T.StartDate = @tmpStartDate,
			T.EndDate = CASE WHEN @tmpStartDate IS NOT NULL AND @tmpEndDate IS NOT NULL AND @tmpEndDate < @tmpStartDate THEN @tmpStartDate ELSE @tmpEndDate END
			,T.RevenueStartDate = CASE WHEN @strAllocMethod = 'r' THEN  @tmpRevStartDate ELSE T.RevenueStartDate END,
			T.RevenueEndDate= CASE WHEN @strAllocMethod = 'r' THEN  
					CASE WHEN @tmpRevStartDate IS NOT NULL AND @tmpRevEndDate IS NOT NULL AND @tmpRevEndDate < @tmpRevStartDate THEN @tmpRevStartDate ELSE @tmpRevEndDate END 
				ELSE T.RevenueEndDate END
	FROM #tempTasksToProcess as T 
		  LEFT JOIN (SELECT COUNT(*) AS ACount, MIN(StartDate) AS MinDate, MAX(EndDate) AS MaxDate, MIN(RevenueStartDate) AS MinRevDate, MAX(RevenueEndDate) AS MaxRevDate, WBS1, WBS2 
				  FROM #tempTasksToProcess AS A WHERE A.WBS1 = @strWBS1 AND WBS2 IS NOT NULL AND WBS3 IS NOT NULL GROUP BY WBS1, WBS2) AS X2 ON X2.WBS1 = T.WBS1 AND X2.WBS2 = T.WBS2 
	WHERE T.PlanID = @strPlanID AND T.WBS1 = @strWBS1 AND T.WBS2 IS NOT NULL AND T.WBS3 IS NULL
   -- for parent dates working up to WBS1
	UPDATE  T 
		SET @tmpStartDate =  CASE WHEN (T.StartDate  <= X2.MinDate OR X2.MinDate IS NULL) THEN T.StartDate ELSE X2.MinDate END ,
			@tmpEndDate =	CASE WHEN (T.EndDate >= X2.MaxDate OR X2.MaxDate IS NULL) THEN T.EndDate ELSE X2.MaxDate END ,
			@tmpRevStartDate =  CASE WHEN (T.RevenueStartDate  <= X2.MinRevDate OR X2.MinRevDate IS NULL) THEN T.RevenueStartDate ELSE X2.MinRevDate END ,
			@tmpRevEndDate =	CASE WHEN (T.RevenueEndDate >= X2.MaxRevDate OR X2.MaxRevDate IS NULL) THEN T.RevenueEndDate ELSE X2.MaxRevDate END ,
			T.StartDate = @tmpStartDate,
			T.EndDate = @tmpEndDate
			,T.RevenueStartDate = CASE WHEN @strAllocMethod = 'r' THEN @tmpRevStartDate ELSE T.RevenueStartDate END,
			T.RevenueEndDate= CASE WHEN @strAllocMethod = 'r' THEN  @tmpRevEndDate ELSE T.RevenueEndDate END
	FROM #tempTasksToProcess AS T 
	LEFT JOIN (SELECT COUNT(*) AS ACount, MIN(StartDate) AS MinDate, MAX(EndDate) AS MaxDate, MIN(RevenueStartDate) AS MinRevDate, MAX(RevenueEndDate) AS MaxRevDate, WBS1 
				  FROM #tempTasksToProcess AS A WHERE A.WBS1 = @strWBS1 AND WBS2 IS NOT NULL AND WBS3 IS NULL GROUP BY WBS1 ) AS X2 ON X2.WBS1 = @strWBS1
	WHERE T.PlanID = @strPlanID AND T.WBS1 = @strWBS1 AND T.WBS2 IS NULL AND T.WBS3 IS NULL
   -- Now set all empty Plan Start and Plan End to its parent's Plan Start and Plan End dates
   DECLARE @planDates TABLE (
		PlanID VARCHAR(32),
		OutlineNumber VARCHAR(255),
		StartDate DATETIME,
		EndDate DATETIME ,
		RevenueStartDate DATETIME,
		RevenueEndDate DATETIME 
	);

	DECLARE @planParentMinMaxDates TABLE (
		PlanID VARCHAR(32),
		ParentOutlineNumber VARCHAR(255),
		StartDate DATETIME,
		EndDate DATETIME ,
		RevenueStartDate DATETIME,
		RevenueEndDate DATETIME 
	);
      
	DECLARE @iLevelTop int = 1
	DECLARE @maxiLevel int
	SELECT @maxiLevel = MAX(OutlineLevel) FROM #tempTasksToProcess WHERE PlanID = @strPlanID
	 

	WHILE (@iLevelTop <= @maxiLevel) 
	BEGIN 

			DELETE FROM @planDates;

			INSERT INTO @planDates
			SELECT PlanID, OutlineNumber,StartDate,EndDate,RevenueStartDate,RevenueEndDate
				FROM #tempTasksToProcess 
				WHERE PlanID = @strPlanID 

			UPDATE T SET
				@tmpStartDate = CASE WHEN T.RevenueStartDate IS NULL THEN x.RevenueStartDate ELSE T.RevenueStartDate END,
				@tmpEndDate = CASE WHEN T.RevenueEndDate IS NULL then x.RevenueEndDate ELSE T.RevenueEndDate END, 
				T.RevenueStartDate = CASE WHEN @strAllocMethod = 'r' THEN @tmpStartDate ELSE T.RevenueStartDate END,
				T.RevenueEndDate= CASE WHEN @strAllocMethod = 'r' THEN @tmpEndDate ELSE T.RevenueEndDate END
			FROM #tempTasksToProcess AS T
			INNER JOIN @planDates AS X ON T.PlanID = X.PlanID AND T.ParentOutlineNumber = X.OutlineNumber
			WHERE T.PlanID = @strPlanID AND T.OutlineLevel = @iLevelTop
			AND (T.RevenueStartDate IS NULL OR T.RevenueStartDate IS NULL)

		 	UPDATE T SET
				@tmpStartDate = CASE WHEN T.StartDate IS NULL THEN x.StartDate ELSE T.StartDate END,
				@tmpEndDate = CASE WHEN T.EndDate IS NULL then x.EndDate ELSE T.EndDate END,
				T.StartDate = @tmpStartDate,
				T.EndDate = @tmpEndDate 
			FROM #tempTasksToProcess AS T
			INNER JOIN @planDates AS X ON T.PlanID = X.PlanID AND T.ParentOutlineNumber = X.OutlineNumber
			WHERE T.PlanID = @strPlanID AND T.OutlineLevel = @iLevelTop
			AND (T.StartDate IS NULL OR T.EndDate IS NULL)
		--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		SET @iLevelTop = @iLevelTop + 1

		END

	--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	   
	DECLARE @iLevel int
	SELECT @iLevel = MAX(OutlineLevel) - 1 FROM #tempTasksToProcess WHERE PlanID = @strPlanID
	-- Traverse upward from bottom level.
	-- Update StartDate and EndDate of parent rows.

	WHILE (@iLevel >= 0) 
		BEGIN
		
		DELETE FROM @planParentMinMaxDates;

		INSERT INTO @planParentMinMaxDates
		SELECT PlanID, ParentOutlineNumber, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate, 
			MIN(RevenueStartDate) AS RevenueStartDate, MAX(RevenueEndDate) AS RevenueEndDate
				FROM #tempTasksToProcess 
				WHERE PlanID = @strPlanID 
				GROUP BY PlanID, ParentOutlineNumber

		DECLARE @PNTaskStartDate Datetime = NULL
		DECLARE @PNTaskEndDate Datetime = NULL
		DECLARE @PNTaskRevenueStartDate Datetime = NULL
		DECLARE @PNTaskRevenueEndDate Datetime = NULL
		 
		UPDATE T SET
			@PNTaskStartDate = CASE WHEN X.StartDate < T.StartDate THEN X.StartDate ELSE T.StartDate END, 
			@PNTaskEndDate = CASE WHEN X.EndDate > T.EndDate THEN X.EndDate ELSE T.EndDate END,
			StartDate = @PNTaskStartDate,
			EndDate = @PNTaskEndDate 
			FROM #tempTasksToProcess AS T
			INNER JOIN @planParentMinMaxDates AS X ON T.PlanID = X.PlanID AND T.OutlineNumber = X.ParentOutlineNumber
			WHERE T.PlanID = @strPlanID AND T.OutlineLevel = @iLevel
			AND (X.StartDate < T.StartDate OR X.EndDate > T.EndDate)

		UPDATE T SET
			@PNTaskRevenueStartDate = CASE WHEN X.RevenueStartDate < T.RevenueStartDate THEN X.RevenueStartDate ELSE T.RevenueStartDate END, 
			@PNTaskRevenueEndDate = CASE WHEN X.RevenueEndDate > T.RevenueEndDate THEN X.RevenueEndDate ELSE T.RevenueEndDate END, 
			RevenueStartDate = CASE WHEN @strAllocMethod = 'r' THEN  @PNTaskRevenueStartDate ELSE T.RevenueStartDate END,
			RevenueEndDate = CASE WHEN @strAllocMethod = 'r' THEN  @PNTaskRevenueEndDate ELSE T.RevenueEndDate END
			FROM #tempTasksToProcess AS T
			INNER JOIN @planParentMinMaxDates AS X ON T.PlanID = X.PlanID AND T.OutlineNumber = X.ParentOutlineNumber
			WHERE T.PlanID = @strPlanID AND T.OutlineLevel = @iLevel
			AND (X.RevenueStartDate < T.RevenueStartDate OR X.RevenueEndDate > T.RevenueEndDate)

		--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		SET @iLevel = @iLevel - 1

		END
	--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 

		UPDATE T SET T.RevenueStartDate = CASE WHEN @strAllocMethod = 'r' AND T.RevenueStartDate = NULL THEN  T.StartDate ELSE T.RevenueStartDate END,
		T.RevenueEndDate= CASE WHEN @strAllocMethod = 'r' AND T.RevenueEndDate = NULL THEN  T.EndDate ELSE T.RevenueEndDate END 
		FROM #tempTasksToProcess T  
		WHERE T.WBS1 = @strWBS1 AND (T.RevenueStartDate IS NULL OR T.RevenueEndDate IS NULL)

		UPDATE R SET R.StartDate = T.StartDate,R.EndDate=T.EndDate
		FROM RPTask R
		INNER JOIN #tempTasksToProcess T ON T.TaskID=R.TaskID
		WHERE R.PlanID = @strPlanID 
		AND ((ISNULL(R.StartDate,'') != ISNULL(T.StartDate,'')
				OR ISNULL(R.EndDate,'') != ISNULL(T.EndDate,''))) 		
		
		UPDATE R SET R.RevenueStartDate = T.RevenueStartDate,R.RevenueEndDate=T.RevenueEndDate
		FROM RPTask R
		INNER JOIN #tempTasksToProcess T ON T.TaskID=R.TaskID
		WHERE R.PlanID = @strPlanID 
		AND (ISNULL(R.RevenueStartDate,'') != ISNULL(T.RevenueStartDate,'')
				OR ISNULL(R.RevenueEndDate,'') != ISNULL(T.RevenueEndDate,''))
		 
		UPDATE T SET T.StartDate = tempTasks.StartDate,
			T.EndDate = tempTasks.EndDate, 
			T.RevenueStartDate = tempTasks.RevenueStartDate,
			T.RevenueEndDate = tempTasks.RevenueEndDate
		FROM PNTask T
		INNER JOIN #tempTasksToProcess tempTasks ON T.TaskID= tempTasks.TaskID
		WHERE T.PlanID = @strPlanID 

		IF @strAllocMethod = 'r'
		BEGIN
		   EXECUTE dbo.stRPExtendDateSpan_RevForecast @strPlanID,0
		END;  

		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#tempTasksToProcess')) 
		BEGIN
		   DROP TABLE #tempTasksToProcess;
		END;
  SET NOCOUNT OFF  
  
END 


GO
