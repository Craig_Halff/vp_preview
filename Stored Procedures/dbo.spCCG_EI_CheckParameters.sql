SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_CheckParameters] (
	@objectName		nvarchar(100)
)
AS BEGIN
	/* Copyright (c) 2018 EleVia Software and Central Consulting Group.  All rights reserved. */
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT oname as FuncName, otype as FuncType, ISNULL(MAX(case when parameter_id = 1 then d.paramName else '' end), '') as Param1,
			ISNULL(MAX(case when parameter_id = 2 then d.paramName else '' end), '') as Param2,
			ISNULL(MAX(case when parameter_id = 3 then d.paramName else '' end), '') as Param3,
			ISNULL(MAX(case when parameter_id = 4 then d.paramName else '' end), '') as Param4,
			ISNULL(MAX(case when parameter_id = 5 then d.paramName else '' end), '') as Param5,
			ISNULL(MAX(case when parameter_id = 6 then d.paramName else '' end), '') as Param6,
			ISNULL(MAX(case when parameter_id = 1 then ParamType else '' end), '') as Param1Type,
			ISNULL(MAX(case when parameter_id = 2 then ParamType else '' end), '') as Param2Type,
			ISNULL(MAX(case when parameter_id = 3 then ParamType else '' end), '') as Param3Type,
			ISNULL(MAX(case when parameter_id = 4 then ParamType else '' end), '') as Param4Type,
			ISNULL(MAX(case when parameter_id = 5 then ParamType else '' end), '') as Param5Type,
			ISNULL(MAX(case when parameter_id = 6 then ParamType else '' end), '') as Param6Type,
			ISNULL(MAX(case when parameter_id = 0 and is_output = 1 then ParamType else '' end), '') as ReturnType
		FROM (
			SELECT o.name as oname, type as otype, p.system_type_id, upper(p.name) as paramName, p.parameter_id, p.is_output,
					case system_type_id
						when 167 then 'varchar' when 231 then 'nvarchar' when 56 then 'int' when 61 then 'datetime'
						when 175 then 'char' when 106 then 'decimal' else ''
					end + (case when is_output = 1 and parameter_id > 0 then ':out' else '' end) as ParamType, max_length
				FROM sys.objects o
					LEFT JOIN sys.parameters p ON o.object_id = p.object_id
				WHERE o.object_id = OBJECT_ID(N'[' + Replace(@objectName, N'dbo.', N'dbo].[') + N']')
			) d
		GROUP BY oname, otype
END
GO
