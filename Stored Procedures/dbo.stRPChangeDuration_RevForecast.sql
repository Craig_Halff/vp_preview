SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPChangeDuration_RevForecast]
  @strRowID nvarchar(255),
  @strNewStartDate varchar(8),
  @strNewEndDate varchar(8),
  @intShiftMode int, /* 1 = Maintain Pattern, 2 = Distribute Evenly, 3 = No Redistribute */
  @bitMatchChildrenDates bit = 0 /* When 1, makes all children dates match new parent dates. */
AS

BEGIN -- Procedure stRPChangeDuration

/*

  The objective of this SP is to change the revenue durations of WBS rows in the branch that is pointed to by @strRowID.

  1. Which WBS rows will be changed are determined by the {RevenueStartDate, RevenueEndDate} range of those rows.
  2. The new Revenue Start or Revenue End Date can be reduced down to one working day for all WBS and revenue TPD rows within the selection criteria.
  3. If a row has Revenue Start Date later than NewStartDate and End Date earlier than NewEndDate then the row and its decendants will nor be affected. 

*/

  SET NOCOUNT ON
  
  DECLARE @dtOldTopStartDate datetime
  DECLARE @dtOldTopEndDate datetime
  DECLARE @dtNewStartDate datetime
  DECLARE @dtNewEndDate datetime

  DECLARE @dtMinNewDate datetime
  DECLARE @dtMaxNewDate datetime
  DECLARE @dt_MINDATE datetime = CONVERT(datetime, '')
  DECLARE @dt_MAXDATE datetime = CONVERT(datetime, '99991231')
   
  DECLARE @strCompany nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strAssignmentID varchar(32)
  DECLARE @strResourceID nvarchar(20)
  DECLARE @strGenericResourceID nvarchar(20)
  DECLARE @strResourceType varchar(1)
  DECLARE @strUserName nvarchar(32)
  DECLARE @strAECFlg varchar(1) = ''

  DECLARE @tabRevTPD TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    TimePhaseID varchar(32) COLLATE database_default,
    AT_OutlineNumber varchar(255) COLLATE database_default
    UNIQUE(PlanID, TaskID, TimePhaseID)
  )

  DECLARE @tabEvenTPD TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    TimePhaseID varchar(32) COLLATE database_default,
    TaskRevStartDate datetime,
    TaskRevEndDate datetime,
    MinTPDDate datetime,
    MaxTPDDate datetime,
    SumBill decimal(19,4),
    SumCost decimal(19,4)
    UNIQUE(PlanID, TaskID, TimePhaseID, MinTPDDate, MaxTPDDate, SumBill,SumCost)
  )
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Parse @strRowID
  
  SELECT
    @strPlanID = PlanID,
    @strTaskID = TaskID,
    @strAssignmentID = AssignmentID,
    @strResourceID = ResourceID,
    @strGenericResourceID = GenericResourceID,
    @strAECFlg = AECFlg
    FROM dbo.stRP$tabParseRowID(@strRowID)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get various parameters.

  SELECT 
    @strCompany = P.Company,
    @dtMinNewDate = T.RevenueStartDate,
    @dtMaxNewDate = T.RevenueEndDate,
    @dtOldTopStartDate = T.RevenueStartDate,
    @dtOldTopEndDate = T.RevenueEndDate
    FROM PNTask AS T
      INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID
    WHERE T.TaskID = @strTaskID

  -- Set Date limits.
  -- Only the selected duration to be shrunk to one calendar day either at the EndDate or StartDate of the selected row.  

  SET @dtNewStartDate = 
    CASE
      WHEN DATALENGTH(@strNewStartDate) = 0 THEN @dtMinNewDate
      ELSE CONVERT(datetime, @strNewStartDate)
    END

  SET @dtNewEndDate = 
    CASE
      WHEN DATALENGTH(@strNewEndDate) = 0 THEN @dtMaxNewDate
      ELSE CONVERT(datetime, @strNewEndDate)
    END
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Save off Revenue Forecast TPD rows that are in the selected branch

      INSERT @tabRevTPD(
        PlanID,
        TaskID,
        TimePhaseID,
        AT_OutlineNumber
      )
        SELECT DISTINCT
          A.PlanID,
          A.TaskID,
          A.TimePhaseID,
          AT.OutlineNumber AS AT_OutlineNumber
          FROM PNPlannedRevenueLabor AS A
            INNER JOIN PNTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
            INNER JOIN PNTask AS PT ON A.PlanID = PT.PlanID 
          WHERE
            A.PlanID = @strPlanID AND
            PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
            AT.OutlineNumber LIKE PT.OutlineNumber + '%'  AND AT.WBSType <> 'LBCD'
	      	AND (AT.ChildrenCount = 0 OR AT.TaskID IN 
			(SELECT T.TaskID FROM PNTask T 
			INNER JOIN PNTask LT ON T.PlanID = LT.PlanID 
			AND LT.ParentOutlineNumber = T.OutlineNumber AND LT.WBSType = 'LBCD' 
			WHERE T.PlanID = @strPlanID))
      
          -- Group TPD by Task.

          INSERT @tabEvenTPD(
            PlanID,
            TaskID,
            TimePhaseID,
			TaskRevStartDate,
			TaskRevEndDate,
            MinTPDDate,
            MaxTPDDate,
            SumBill,
			SumCost
          )
            SELECT DISTINCT
              TPD.PlanID AS PlanID,
              TPD.TaskID AS TaskID,
              MIN(TPD.TimePhaseID) AS TimePhaseID,
			  AT.RevenueStartDate,
			  AT.RevenueEndDate,
              MIN(TPD.StartDate) AS MinTPDDate,
              MAX(TPD.EndDate) AS MaxTPDDate,
              SUM(TPD.PeriodBill) AS SumBill,
              SUM(TPD.PeriodCost) AS SumCost
              FROM PNPlannedRevenueLabor AS TPD
                INNER JOIN PNTask AS AT ON TPD.PlanID = AT.PlanID AND TPD.TaskID = AT.TaskID
                INNER JOIN PNTask AS PT ON TPD.PlanID = PT.PlanID 
          WHERE
            TPD.PlanID = @strPlanID AND
            PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
            AT.OutlineNumber LIKE PT.OutlineNumber + '%' AND AT.WBSType <> 'LBCD'
	      	AND (AT.ChildrenCount = 0 OR AT.TaskID IN 
			(SELECT T.TaskID FROM PNTask T 
			INNER JOIN PNTask LT ON T.PlanID = LT.PlanID 
			AND LT.ParentOutlineNumber = T.OutlineNumber AND LT.WBSType = 'LBCD' 
			WHERE T.PlanID = @strPlanID))                
            GROUP BY TPD.PlanID, TPD.TaskID,AT.RevenueStartDate, AT.RevenueEndDate

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Redistribute TPD evenly only when Shift Mode is 2.

      IF (@intShiftMode = 2)
        BEGIN

          -- Delete existing TPD under Assigments collected earlier in @tabAssignment.

          DELETE PNPlannedRevenueLabor
            WHERE PlanID = @strPlanID  AND
            TimePhaseID IN (SELECT DISTINCT TimePhaseID FROM @tabRevTPD)

          -- Insert new TPD, one for each TASK in @tabRevTPD  
              INSERT PNPlannedRevenueLabor(
                TimePhaseID,
                PlanID, 
                TaskID,
                StartDate, 
                EndDate, 
                PeriodBill,
				PeriodCost,
                CreateUser,
                ModUser,
                CreateDate,
                ModDate
              )
                SELECT
                REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
                TPD.PlanID AS PlanID, 
                TPD.TaskID AS TaskID,
                CASE
                  WHEN (@bitMatchChildrenDates = 1 OR TaskRevStartDate = @dtOldTopStartDate ) THEN @dtNewStartDate
                  WHEN (@dtNewStartDate > TPD.MinTPDDate) THEN @dtNewStartDate
                  WHEN (@dtNewEndDate < TPD.MinTPDDate) THEN @dtNewEndDate
                  ELSE TPD.MinTPDDate
                END AS StartDate,
                CASE
                  WHEN (@bitMatchChildrenDates = 1 OR TaskRevEndDate = @dtOldTopEndDate) THEN @dtNewEndDate
                  WHEN (@dtNewEndDate < TPD.MaxTPDDate) THEN @dtNewEndDate
                  WHEN (@dtNewStartDate > TPD.MaxTPDDate) THEN @dtNewStartDate
                  ELSE TPD.MaxTPDDate
                END AS EndDate,
                TPD.SumBill AS PeriodBill,
				TPD.SumCost AS PeriodCost,
                CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stPNShiftDates' ELSE 'SDT_' + @strUserName END AS CreateUser,
                CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stPNShiftDates' ELSE 'SDT_' + @strUserName END AS ModUser,
                LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As CreateDate,
                LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As ModDate 
              FROM @tabEvenTPD AS TPD

        END /* END IF (@intShiftMode = 2) */
    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  
      UPDATE CT SET
        RevenueStartDate = 
          CASE
            WHEN (DATALENGTH(@strNewStartDate) <> 0 
              AND (@bitMatchChildrenDates = 1 OR CT.TaskID = ISNULL(@strTaskID, '|') OR CT.RevenueStartDate = @dtOldTopStartDate)) THEN @dtNewStartDate
            WHEN (DATALENGTH(@strNewStartDate) = 0 AND CT.StartDate <= @dtNewEndDate) THEN CT.StartDate
            WHEN (CT.RevenueStartDate > @dtNewEndDate) THEN @dtNewEndDate
            WHEN (DATALENGTH(@strNewStartDate) <> 0 AND @dtNewStartDate > CT.RevenueStartDate) THEN @dtNewStartDate
            WHEN (DATALENGTH(@strNewStartDate) <> 0 AND @dtNewStartDate <= CT.RevenueStartDate) THEN CT.RevenueStartDate
          END,

        RevenueEndDate = 
          CASE
            WHEN (DATALENGTH(@strNewEndDate) <> 0 
              AND (@bitMatchChildrenDates = 1 OR CT.TaskID = ISNULL(@strTaskID, '|') OR CT.RevenueEndDate = @dtOldTopEndDate)) THEN @dtNewEndDate
            WHEN (DATALENGTH(@strNewEndDate) = 0 AND CT.RevenueEndDate >= @dtNewStartDate) THEN CT.RevenueEndDate
            WHEN (CT.RevenueEndDate < @dtNewStartDate) THEN @dtNewStartDate
            WHEN (DATALENGTH(@strNewEndDate)<> 0 AND @dtNewEndDate < CT.RevenueEndDate) THEN @dtNewEndDate
            WHEN (DATALENGTH(@strNewEndDate)<> 0 AND @dtNewEndDate >= CT.RevenueEndDate) THEN CT.RevenueEndDate
          END,		    
		ModDate=LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19),
		ModUser=@strUserName
        FROM PNTask AS PT
          INNER JOIN PNTask AS CT ON PT.PlanID = CT.PlanID

        WHERE
          PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
          CT.OutlineNumber LIKE PT.OutlineNumber + '%'

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
  SET NOCOUNT OFF

END -- stRPChangeDuration_RevForecast
GO
