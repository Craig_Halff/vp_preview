SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[HAI_VirtualCard]
AS
/*
Copyright (c) 2018 Halff Associates Inc. All rights reserved.
08/26/2018	Joseph Sagel
			Update bank code to virtual card bank code
08/11/2021	David Springer
			Revised for Vantagepoint
*/
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

	Update vo
	Set vo.BankCode = '212.20'
	from VO vo, Clendor c, ClientCustomTabFields cx
	where vo.Vendor = c.Vendor
	  and c.ClientID = cx.ClientID
	  and cx.CustVirtualCardEnrolled = 'Y'
	  and vo.PaidPeriod = '999999'
	  and vo.Company = '01'
	  and vo.Period <> '0'
      and vo.BankCode <> '212.20'

	Update vo
	Set vo.BankCode = '100.06'
	from VO vo, Clendor c, ClientCustomTabFields cx
	where vo.Vendor = c.Vendor
	  and c.ClientID = cx.ClientID
	  and cx.CustVirtualCardEnrolled = 'N'
	  and vo.PaidPeriod = '999999'
	  and vo.Company = '01'
	  and vo.Period <> '0'
      and vo.BankCode = '212.20'

END
GO
