SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_PAT_GetVendorUDFValue]
	@CustVendorField	Nvarchar(200),
	@VendorID			Nvarchar(50)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	DECLARE @sql Nvarchar(max) = (case when not exists (select 'x' from sysobjects b where b.name = 'Clendor') then N'
		SELECT ' + @CustVendorField + ' FROM VendorCustomTabFields WHERE Vendor = N''' + @VendorID + ''''
		else
		N'
		SELECT ' + @CustVendorField + ' 
			FROM ClientCustomTabFields v
	            inner join Clendor c on c.ClientId = v.ClientId
		    WHERE c.Vendor = N''' + @VendorID + ''''
		end)
	EXEC (@sql)
END
GO
