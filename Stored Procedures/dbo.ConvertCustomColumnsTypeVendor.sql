SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ConvertCustomColumnsTypeVendor]
AS
BEGIN
	declare
	@ExpressionsFetch    INTEGER, 
	@InfoCenterArea      VARCHAR(32), 
	@Name                VARCHAR(125), 
	@datatype            VARCHAR(32), 
	@DatatypeSourceField VARCHAR(32), 
	@TableName           VARCHAR(100), 
	@ColumnName          VARCHAR(100), 
	@UpdateStatement     VARCHAR(MAX), 
	@SQL                 VARCHAR(MAX)

	print ' start ConvertCustomColumnsTypeVendor'
    SET @ExpressionsFetch = 0;
    DECLARE VendorTypeCustomColumnCursor CURSOR FOR
	SELECT FW_CustomColumnsData.InfocenterArea, Name AS ColumnName, DataType,
	    'vendor' AS DatatypeSourceField,
		CASE FW_CustomColumnsData.GridID
		WHEN 'X'
		THEN
            CASE FW_CustomColumnsData.InfoCenterArea
                WHEN 'Activity'
                THEN 'ActivityCustomTabFields'
                WHEN 'ChartOfAccounts'
                THEN 'AccountCustomTabFields'
                WHEN 'Clients'
                THEN 'ClientCustomTabFields'
				WHEN 'Firms'
				THEN 'ClientCustomTabFields'
                WHEN 'Contacts'
                THEN 'ContactCustomTabFields'
                WHEN 'Employees'
                THEN 'EmployeeCustomTabFields'
                WHEN 'Equipment'
                THEN 'EquipmentCustomTabFields'
                WHEN 'Leads'
                THEN 'LeadCustomTabFields'
                WHEN 'MktCampaigns'
                THEN 'MktCampaignCustomTabFields'
                WHEN 'Opportunities'
                THEN 'OpportunityCustomTabFields'
                WHEN 'ProjectPlan'
                THEN 'PlanningCustomTabFields'
                WHEN 'Projects'
                THEN 'ProjectCustomTabFields'
                WHEN 'TextLibrary'
                THEN 'TextLibraryCustomTabFields'
                WHEN 'Vendors'
                THEN 'VendorCustomTabFields'
                ELSE FW_CustomColumnsData.InfoCenterArea
		END 
		ELSE FW_CustomGridsData.tablename 
		end as tablename
        FROM FW_CustomColumnsData
	LEFT JOIN  FW_CustomGridsdata on FW_CustomColumnsData.InfoCenterArea = FW_CustomGridsData.InfoCenterArea AND FW_CustomColumnsData.GRIDID = FW_CustomGridsdata.GridID WHERE FW_CustomColumnsData.DataType = 'vendor' AND FW_CustomColumnsData.InfocenterArea not IN('Equipment');
    OPEN VendorTypeCustomColumnCursor;
    FETCH NEXT FROM VendorTypeCustomColumnCursor INTO @InfoCenterArea, @ColumnName, @Datatype, @DatatypeSourceField, @TableName;
    SET @ExpressionsFetch = @@Fetch_Status;
    WHILE(@ExpressionsFetch = 0)
        BEGIN
            PRINT @ExpressionsFetch;
			PRINT @TableName+'.'+@ColumnName;
            SET @UpdateStatement = 'UPDATE ['+@TableName+'] SET  ['+@ColumnName+'] = Clendor.ClientID FROM ['+@TableName+'] JOIN Clendor ON Clendor.'+@datatypeSourceField+' = ['+@TableName+'].['+@ColumnName+']';
            PRINT @UPDATESTATEMENT;
            EXECUTE (@UpdateStatement);
            FETCH NEXT FROM VendorTypeCustomColumnCursor INTO @InfoCenterArea, @ColumnName, @Datatype, @DatatypeSourceField, @TableName;
            SET @ExpressionsFetch = @@Fetch_Status;
        END;
	CLOSE VendorTypeCustomColumnCursor;
	DEALLOCATE VendorTypeCustomColumnCursor;
print ' end ConvertCustomColumnsTypeVendor'
END
GO
