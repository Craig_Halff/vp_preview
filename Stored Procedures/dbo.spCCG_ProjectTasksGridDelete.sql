SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_ProjectTasksGridDelete] @WBS1 varchar (32), @PhaseCode varchar (7)
AS
/*
Copyright 2021 Central Consulting Group.   All rights reserved.
08/16/2021	David Springer
			Delete Tasks for this phase.
			Call this from an Project Phases DELETE workflow.
*/
SET NOCOUNT ON
BEGIN
   Delete From Projects_Tasks Where WBS1 = @WBS1 and CustTaskPhaseCode = @PhaseCode
END
GO
