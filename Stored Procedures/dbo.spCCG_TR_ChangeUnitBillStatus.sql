SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_TR_ChangeUnitBillStatus](@Username varchar(32), @OrigTable varchar(12), 
	@OrigPeriod varchar(6), @OrigPostSeq varchar(32), @OrigPKey varchar(32), 
	@NewPeriod  varchar(6), @NewPostSeq  varchar(32), @NewPKey  varchar(32),
	@OrigBillStatus varchar(1), @OrigQty decimal(19,4),
	@NewBillStatus  varchar(1), @NewQty  decimal(19,4),
	@ApprovalComment varchar(255), @IncludeFuture varchar(1), @FNStatus varchar(2))
AS BEGIN

	declare @partial int, @origRecordNewBillStatus varchar(1), @sql varchar(max), @origTableWhereClause varchar(255), @emp varchar(32)
	select @emp=Employee from SEUser where Username=@Username

	if @NewPKey = ''
		set @NewPKey = Left(Replace(Convert(varchar(255), newid()),'-',''),31)
	set @origTableWhereClause = ' where Period=' + @OrigPeriod + ' and PostSeq=' + @OrigPostSeq + ' and PKey=''' + @OrigPkey + ''' '

	if @OrigQty <> @NewQty
	begin
		set @partial = 1
		set @origRecordNewBillStatus = 'R'				
	end else begin
		set @partial = 0
		set @origRecordNewBillStatus = @NewBillStatus	
	end
	
	set @sql = 'update ' + @OrigTable + ' set BillStatus=''' + @origRecordNewBillStatus + '''' +  @origTableWhereClause + ' and BillStatus=''' + @OrigBillStatus + ''' '
	
	if @partial = 1
	begin
		
		declare @commonColumns varchar(2000), @nonCommonColumns varchar(1000), @sqlAmts varchar(255)
		declare @mult varchar(50)

		set @nonCommonColumns = 'Period,PostSeq,PKey,BillStatus,BillExt,UnitBillExt,' +
			'UnitQuantity,CBAmount,Amount,TransactionAmount,AmountProjectCurrency,AmountBillingCurrency,AmountSourceCurrency,ModUser,'

		select @commonColumns = STUFF((
			SELECT ',' + c.name
			FROM sys.columns c
			WHERE c.object_id = OBJECT_ID('BIED') and c.name in (select cols.name from sys.columns cols where cols.object_id=OBJECT_ID(@OrigTable))
			  and charindex(','+c.name+',', ','+@nonCommonColumns) < 1
			FOR XML PATH('')), 1, 1, '')
		print @commonColumns
		set @mult = Cast(@NewQty as varchar) + ' / ' + Cast(@OrigQty as varchar)
		if @OrigQty = 0 set @mult = 1
		
		set @sql = @sql +
			' insert into BIED (' + @nonCommonColumns + @commonColumns + ') select ' + @NewPeriod + ',' + @NewPostSeq + ',''' + @NewPKey + 'a'',''' + @NewBillStatus + '''' + 
				', Round(BillExt     * ' + @mult + ',2)' +
				', Round(UnitBillExt * ' + @mult + ',2)' +
				', ' + Cast(@NewQty as varchar) +
				', CBAmount * ' + @mult + 
				', Amount   * ' + @mult + 
				', TransactionAmount     * ' + @mult + 
				', AmountProjectCurrency * ' + @mult + 
				', AmountBillingCurrency * ' + @mult +
				', AmountSourceCurrency  * ' + @mult +
				',''' + @Username + ''', ' + @commonColumns +
			' from ' + @OrigTable + @origTableWhereClause
		
		set @sql = @sql +
			' insert into BIED (' + @nonCommonColumns + @commonColumns + ') select ' + @NewPeriod + ',' + @NewPostSeq + ',''' + @NewPKey + 'b'',''' + @OrigBillStatus + '''' + 
				', Round(BillExt     - (BillExt     * ' + @mult + '),2)' +
				', Round(UnitBillExt - (UnitBillExt * ' + @mult + '),2)' +
				', ' + Cast(@OrigQty-@NewQty as varchar) +
				', CBAmount - (CBAmount * ' + @mult + ')' +
				', Amount   - (Amount   * ' + @mult + ')' +
				', TransactionAmount     - (TransactionAmount     * ' + @mult + ') ' +
				', AmountProjectCurrency - (AmountProjectCurrency * ' + @mult + ') ' +
				', AmountBillingCurrency - (AmountBillingCurrency * ' + @mult + ') ' +
				', AmountSourceCurrency  - (AmountSourceCurrency  * ' + @mult + ') ' +
				',''' + @Username + ''', ' + @commonColumns +
			' from ' + @OrigTable + @origTableWhereClause
	end
	
	if @partial = 0
		set @sql = @sql + ' ; exec spCCG_TR_InsertApproval ''' + @OrigTable + ''',' + Cast(@OrigPeriod as varchar) + ',' + 
			Cast(@OrigPostSeq as varchar) + ',''' + @OrigPKey + ''',null,''' + @emp + ''',''' + IsNull(@ApprovalComment,'') + ''',''' + @IncludeFuture + ''',null,''' + @FNStatus + ''' '
	else
	begin
		set @sql = @sql + 
			' ; exec spCCG_TR_InsertApproval ''' + 'BIED' + ''',' + Cast(@NewPeriod as varchar) + ',' + 
				Cast(@NewPostSeq as varchar) + ',''' + @NewPKey + 'a'',''' + @emp + ''',''' + @emp + ''',''' + IsNull(@ApprovalComment,'') + ''',''' + @IncludeFuture + ''',null,''' + @FNStatus + ''' ' +
			' ; exec spCCG_TR_InsertApproval ''' + 'BIED' + ''',' + Cast(@NewPeriod as varchar) + ',' + 
				Cast(@NewPostSeq as varchar) + ',''' + @NewPKey + 'b'',''' + @emp + ''',''' + @emp + ''',''' + IsNull(@ApprovalComment,'') + ''',null,null,''' + @FNStatus + ''' '
		
		set @sql = @sql +
			' ; update CCG_TR_History set OriginalTable=''BIED'', OriginalPeriod=' + Cast(@NewPeriod as varchar) + ', OriginalPostSeq=' +
				Cast(@NewPostSeq as varchar) + ', OriginalPKey=''' + @NewPKey + 'a'' ' +
				' where OriginalTable=''' + @OrigTable + ''' and OriginalPeriod=' + @OrigPeriod + ' and OriginalPostSeq=' + @OrigPostSeq + ' and OriginalPKey=''' + @OrigPkey + ''' '
	end
	print @sql
	execute(@sql)
END
GO
