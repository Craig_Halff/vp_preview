SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_OrgStructureSync] @UDIC_UID VARCHAR(32)
AS 
/* 
Copyright (c) 2018 Central Consulting Group.  All rights reserved.
06/29/2018	David Springer
			Update Team Leader, Director, Management Leader, Admin Assistant & Contract Admin
			in Employee, Opportunity and Project.
			Call this on a Org Structure scheduled workflow.
08/08/2019	David Springer
			Added Supervisor, EKPracticeLeader, Team Leader checkbox, Director checkbox, Operations Manager checkbox, Project Manager checkbox
12/09/2019	David Springer
			Restricted insert statements to DISTINCT to eliminate duplicates in Employee Role Log grid
09/23/2020	Marketing / BD exempted from Team Leader approver overrides
02/23/2021	Craig H. Anderson
			Modified to account for Regional Practice Leaders (exclude them)
10/25/2021  Jeremy Baummer
			Modified to remove updates to opportunities.
*/
BEGIN
SET NOCOUNT ON

--	Administrative Assistant in Project
	Update px
	Set px.CustAdministrativeAssistant = x.CustAdministrativeAssistant
	From PR p, ProjectCustomTabFields px, UDIC_OrganizationStructure x
	Where p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.WBS3 = px.WBS3
	  and x.UDIC_UID = @UDIC_UID
	  and p.Org = x.CustOrganization
	  and IsNull(px.CustAdministrativeAssistant,'1') <> IsNull(x.CustAdministrativeAssistant,'1')
	-- project level is Active
	  and exists (Select 'x' From PR Where WBS1 = p.WBS1 and WBS2 = ' ' and Status <> 'D')

--	Marketing Lead in Project
	Update px
	Set px.CustMarketingManager = x.CustMarketingManager
	From PR p, ProjectCustomTabFields px, UDIC_OrganizationStructure x
	Where p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.WBS3 = px.WBS3
	  and x.UDIC_UID = @UDIC_UID
	  and p.Org = x.CustOrganization
	  and IsNull(px.CustMarketingManager,'1') <> IsNull(x.CustMarketingManager,'1')
	-- Existing marketing manager is termed
	  and exists (Select 'x' From EM Where Employee = px.CustMarketingManager and Status = 'T')
	-- project level is Active
	  and exists (Select 'x' From PR Where WBS1 = p.WBS1 and WBS2 = ' ' and Status <> 'D')

-- ####################  Team Leader  ##########################
--	Team Leader in Employee
	Insert Into Employees_RoleLog
	(Employee, Seq, CustRole, CustDate, CustAction)
	Select Employee, Replace (NewID(), '-', '') Seq, 'Team Leader' Role, getDate(), 'Added'
	From (Select distinct Employee
		From EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
		Where x.UDIC_UID = @UDIC_UID
			and x.CustTeamLeader = ex.Employee
			and ex.CustTeamLeader = 'N'
			and not exists (Select 'x' From Employees_RoleLog Where Employee = ex.Employee and CustRole = 'Team Leader' and CustAction = 'Added')
		) a

	Update ex
	Set ex.CustTeamLeader = 'Y'
	From EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where x.UDIC_UID = @UDIC_UID
		and x.CustTeamLeader = ex.Employee
		and ex.CustTeamLeader = 'N'

	Update ex
	Set ex.CustTeamLeader = 'N'
	From EmployeeCustomTabFields ex
	Where ex.CustTeamLeader = 'Y'
	  and not exists (Select 'x' From UDIC_OrganizationStructure x Where x.CustTeamLeader = ex.Employee)

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Team Leader'
	  and ex.CustTeamLeader = 'N'

--	Team Leader in Employee as Supervisor
	Update e
	Set e.Supervisor = x.CustTeamLeader
	From EMCompany e, UDIC_OrganizationStructure x
	Where x.UDIC_UID = @UDIC_UID
	  and e.Org = x.CustOrganization
	  and (e.Status <> 'T' or DateAdd (day, -10, getDate()) < e.TerminationDate)
	  and IsNull(e.Supervisor,'1') <> IsNull(x.CustTeamLeader,'1') 

--	Team Leader in Employee as Expense Report Approver
	Update ex
	Set ex.CustEKApprover = x.CustTeamLeader
	From EM e, EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where e.Employee = ex.Employee
	  and x.UDIC_UID = @UDIC_UID
	  and e.Org = x.CustOrganization
	  and (e.Status <> 'T' or DateAdd (day, -10, getDate()) < e.TerminationDate)
	  and IsNull(ex.CustEKApprover,'1') <> IsNull(x.CustTeamLeader,'1')
	  AND right(e.Org,3) NOT IN ('992','993') -- CHA - Added to allow MArketing to have additional approvers

--	Team Leader in Project as Team Leader (Principal)
	Update p
	Set p.Principal = x.CustTeamLeader
	From PR p, UDIC_OrganizationStructure x
	Where x.UDIC_UID = @UDIC_UID
	  and p.Org = x.CustOrganization
	  and IsNull(p.Principal,'1') <> IsNull(x.CustTeamLeader,'1')
	-- project level is Active
	  and exists (Select 'x' From PR Where WBS1 = p.WBS1 and WBS2 = ' ' and Status <> 'D')

--	Team Leader in Project as Billing Team Leader
	Update px
	Set px.CustBillingTeamLeader = x.CustTeamLeader
	From PR p, ProjectCustomTabFields px, UDIC_OrganizationStructure x
	Where p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.WBS3 = px.WBS3
	  and x.UDIC_UID = @UDIC_UID
	  and p.Org = x.CustOrganization
	  and IsNull(px.CustBillingTeamLeader,'1') <> IsNull(x.CustTeamLeader,'1')
	-- project level is Active
	  and exists (Select 'x' From PR Where WBS1 = p.WBS1 and WBS2 = ' ' and (Status <> 'D' or px.CustJTDARBalance <> '0'))

-- ####################  Alternate Approver  ##########################
--	Alternate Approver in Employee as Expense Report Alternate Approver
	Update ex
	Set ex.CustEKAlternateApprover = x.CustAlternateApprover
	From EM e, EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where e.Employee = ex.Employee
	  and x.UDIC_UID = @UDIC_UID
	  and e.Org = x.CustOrganization
	  and (e.Status <> 'T' or DateAdd (day, -10, getDate()) < e.TerminationDate)
	  and IsNull(ex.CustEKAlternateApprover,'1') <> IsNull(x.CustAlternateApprover,'1')

-- ####################  Director  ##########################
--	Director in Employee
	Insert Into Employees_RoleLog
	(Employee, Seq, CustRole, CustDate, CustAction)
	Select Employee, Replace (NewID(), '-', '') Seq, 'Director' Role, getDate(), 'Added'
	From (Select distinct Employee
		From EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
		Where x.UDIC_UID = @UDIC_UID
			and x.CustDirector = ex.Employee
			and ex.CustRoleDirector = 'N'
			and not exists (Select 'x' From Employees_RoleLog Where Employee = ex.Employee and CustRole = 'Director' and CustAction = 'Added')
		) a

	Update ex
	Set ex.CustRoleDirector = 'Y'
	From EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where x.UDIC_UID = @UDIC_UID
		and x.CustDirector = ex.Employee
		and ex.CustRoleDirector = 'N'

	Update ex
	Set ex.CustRoleDirector = 'N'
	From EmployeeCustomTabFields ex
	Where ex.CustRoleDirector = 'Y'
	  and not exists (Select 'x' From UDIC_OrganizationStructure x Where x.CustDirector = ex.Employee)

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Director'
	  and ex.CustRoleDirector = 'N'

--	Director in Employee as Expense Report Director
	Update ex
	Set ex.CustEKDirector = x.CustDirector
	From EM e, EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where e.Employee = ex.Employee
	  and x.UDIC_UID = @UDIC_UID
	  and e.Org = x.CustOrganization
	  and (e.Status <> 'T' or DateAdd (day, -10, getDate()) < e.TerminationDate)
	  and IsNull(ex.CustEKDirector,'1') <> IsNull(x.CustDirector,'1')

--	Director in Project
	Update px
	Set px.CustDirector = x.CustDirector
	From PR p, ProjectCustomTabFields px, UDIC_OrganizationStructure x
	Where p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.WBS3 = px.WBS3
	  and x.UDIC_UID = @UDIC_UID
	  and p.Org = x.CustOrganization
	  and IsNull(px.CustDirector,'1') <> IsNull(x.CustDirector,'1')
	-- project level is Active
	  and exists (Select 'x' From PR Where WBS1 = p.WBS1 and WBS2 = ' ' and (Status <> 'D' or px.CustJTDARBalance <> '0'))

-- ####################  Operations Manager  ##########################
--	Operations Manager
/*	
	Update ex
	Set ex.CustOperationsManager = 'Y'
	From EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where x.UDIC_UID = @UDIC_UID
	  and x.CustOperationsManager = ex.Employee
	  and ex.CustOperationsManager = 'N'
*/
	Update ex
	Set ex.CustOperationsManager = 'N'
	From EmployeeCustomTabFields ex
	Where ex.CustOperationsManager = 'Y'
	  and not exists (Select 'x' From UDIC_OrganizationStructure x Where x.CustOperationsManager = ex.Employee)

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Operations Manager'
	  and ex.CustOperationsManager = 'N'

--	Operations Manager in Employee as Expense Report Operatons Manager
	Update ex
	Set ex.CustEKOperationsManager = x.CustOperationsManager
	From EM e, EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where e.Employee = ex.Employee
	  and x.UDIC_UID = @UDIC_UID
	  and e.Org = x.CustOrganization
	  and (e.Status <> 'T' or DateAdd (day, -10, getDate()) < e.TerminationDate)
	  and IsNull(ex.CustEKOperationsManager,'1') <> IsNull(x.CustOperationsManager,'1')

-- ####################  Team Practice Area  ##########################
--	Team Practice Area / Practice Leader in Employee
	Update ex
	Set ex.CustEKPracticeLeader = pa.Employee
	From UDIC_OrganizationStructure x, Employees_PracticeAreas pa, EM e, EmployeeCustomTabFields ex
	Where x.UDIC_UID = @UDIC_UID
	  and x.CustTeamPracticeArea = pa.CustPracticeAreaLeader
	  and pa.CustPracticeAreaLeader = ex.CustTeamPracticeArea
	  AND pa.CustPracticeAreaLeaderType = 'Primary'
	  and ex.Employee = e.Employee
	  and (e.Status <> 'T' or DateAdd (day, -10, getDate()) < e.TerminationDate)
	  and IsNull(ex.CustEKPracticeLeader,'1') <> IsNull(pa.Employee,'1')
	  and Left(e.Org,2) <> '02'

--	Team Practice Area in Project
	Update px
	Set px.CustTeamPracticeArea = x.CustTeamPracticeArea
	From PR p, ProjectCustomTabFields px, UDIC_OrganizationStructure x
	Where p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.WBS3 = px.WBS3
	  and x.UDIC_UID = @UDIC_UID
	  and p.Org = x.CustOrganization
	  and IsNull(px.CustTeamPracticeArea,'1') <> IsNull(x.CustTeamPracticeArea,'1')
	-- project level is Active
	  and exists (Select 'x' From PR Where WBS1 = p.WBS1 and WBS2 = ' ' and Status <> 'D')
	  and Left(p.Org,2) <> '02'

--	Team Practice Area in Employee
	Update ex
	Set ex.CustTeamPracticeArea = x.CustTeamPracticeArea
	From EM e, EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where e.Employee = ex.Employee
	  and x.UDIC_UID = @UDIC_UID
	  and e.Org = x.CustOrganization
	  and (e.Status <> 'T' or DateAdd (day, -10, getDate()) < e.TerminationDate)
	  and IsNull(ex.CustTeamPracticeArea,'1') <> IsNull(x.CustTeamPracticeArea,'1')
	  and Left(e.Org,2) <> '02'

-- ####################  Management Leader  ##########################

--	Management Leader in Project
	Update px
	Set px.CustManagementLeader = x.CustManagementLeader
	From PR p, ProjectCustomTabFields px, UDIC_OrganizationStructure x
	Where p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.WBS3 = px.WBS3
	  and x.UDIC_UID = @UDIC_UID
	  and p.Org = x.CustOrganization
	  and IsNull(px.CustManagementLeader,'1') <> IsNull(x.CustManagementLeader,'1')
	-- project level is Active
	  and exists (Select 'x' From PR Where WBS1 = p.WBS1 and WBS2 = ' ' and (Status <> 'D' or px.CustJTDARBalance <> '0'))

--	Management Leader in Employee
	Update ex
	Set ex.CustEKManagementLeader = x.CustManagementLeader
	From EM e, EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where e.Employee = ex.Employee
	  and x.UDIC_UID = @UDIC_UID
	  and e.Org = x.CustOrganization
	  and (e.Status <> 'T' or DateAdd (day, -10, getDate()) < e.TerminationDate)
	  and IsNull(ex.CustEKManagementLeader,'1') <> IsNull(x.CustManagementLeader,'1')

-- ####################  Biller  ##########################
--	Propagate Biller to lower levels
	UPDATE p2
	SET p2.Biller = p1.Biller
	FROM PR p1, PR p2
	WHERE p1.WBS1 = p2.WBS1
	  AND p1.WBS2 = ' '
	  AND p1.Status = 'A'
	  AND p2.WBS2 <> ' '
	  AND p1.Biller <> ISNULL (p2.Biller, '')

-- ####################  Project Manager  ##########################
--	Propagate Project Manager to lower levels
	UPDATE p2
	SET p2.ProjMgr = p1.ProjMgr
	FROM PR p1, PR p2
	WHERE p1.WBS1 = p2.WBS1
	  AND p1.WBS2 = ' '
	  AND p1.Status = 'A'
	  AND p2.WBS2 <> ' '
	  AND p1.ProjMgr <> ISNULL (p2.ProjMgr, '')

--	Reset Project Manager checkbox
	UPDATE EmployeeCustomTabFields
	SET CustRoleProjectManager = 'N'
	WHERE CustRoleProjectManager = 'Y'
	
--	Set Project Manager checkbox from active projects
	INSERT INTO Employees_RoleLog
	(Employee, Seq, CustRole, CustDate, CustAction)
	SELECT Employee, REPLACE (NEWID(), '-', '') Seq, 'Project Manager' Role, GETDATE(), 'Added'
	FROM (SELECT DISTINCT ex.Employee
		FROM EmployeeCustomTabFields ex, PR p
		WHERE ex.Employee = p.ProjMgr
			AND p.WBS2 = ' '
			AND p.ChargeType = 'R'
			AND p.Status = 'A'
			AND ex.CustRoleProjectManager = 'N'
			AND NOT EXISTS (SELECT 'x' FROM Employees_RoleLog WHERE Employee = ex.Employee AND CustRole = 'Project Manager' AND CustAction = 'Added')
		) a

	UPDATE ex
	SET ex.CustRoleProjectManager = 'Y'
	FROM EmployeeCustomTabFields ex, PR p
	WHERE ex.Employee = p.ProjMgr
	  AND p.WBS2 = ' '
	  AND p.ChargeType = 'R'
	  AND p.Status = 'A'

END
GO
