SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormRouting_DoSave_Existing]
	@Seq		int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT SortOrder, Parallel, Employee, Description, Seq
        FROM CCG_PAT_Pending p
        WHERE PayableSeq = @Seq
        ORDER BY Seq
END;

GO
