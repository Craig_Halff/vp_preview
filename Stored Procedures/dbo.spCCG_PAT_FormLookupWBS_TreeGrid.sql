SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_FormLookupWBS_TreeGrid]
	@wbs1	Nvarchar(100),
	@wbs2	Nvarchar(100),
	@VP2	bit = 0
AS
BEGIN
	-- Copyright (c) 2020 EleVia Software. All rights reserved.
	/*
	spCCG_PAT_FormLookupWBS_TreeGrid @wbs1 = '2003005.00', @wbs2 = '<> '' '' and WBS3 = '' '''
	*/
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	DECLARE @sql Nvarchar(max)

	SET @wbs1 = REPLACE(@wbs1, N'''', N'''''')
	SET @sql = N'
		SELECT WBS1, WBS2, WBS3, Name, ISNULL(Status, ''D''), Sublevel, ReadyForProcessing
            FROM PR
            WHERE WBS1 = N''' + @wbs1 + ''' and WBS2 ' + @wbs2 + ' ' 
			+ (case when @VP2 = 1 then ' and (PR.Stage is null or PR.Stage in (select Code from CFGProjectStageData ps where ps.Step = ''W'')) ' else '' end) + '
            ORDER BY 1, 2, 3'
	EXEC (@sql)
END
GO
