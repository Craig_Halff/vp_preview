SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_PackageBody] (
	@WBS1		Nvarchar(30) = NULL,
	@Invoice	Nvarchar(12) = ''
)
AS BEGIN
	-- Copyright (c) 2020 EleVia Software. All rights reserved. 

	/* Returns setup data/options mainly from the project info center that is used by
		the package building software in XML format.

		This will need to modified when pic fields are added or removed from the standard set. 

		--exec spCCG_EI_PackageBody '2003005.00'
	*/

	-- Declarations
	------------------------------------------------
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	declare @CustFinalInvPkgSubExpOrder varchar(100),
		@CustFinalInvPkgIncludeSubInvoicesMarkup varchar(20),
		@CustFinalInvPkgIncludeExpReceiptsMarkup varchar(20),
		@CustFinalInvPkgIncludeInvoiceMarkup varchar(20),
		@CustFinalInvPkgSubInvoicesMinimum decimal (19,5),
		@CustFinalInvPkgExpReceiptsMinimum decimal (19,5),
		@CustFinalInvPkgIncludeDirectExp varchar(20),
		@CustFinalInvPkgIncludeDirectSub varchar(20)

	declare @bCustFinalInvPkgSubExpOrder bit,
		@bCustFinalInvPkgIncludeSubInvoicesMarkup bit,
		@bCustFinalInvPkgIncludeExpReceiptsMarkup bit,
		@bCustFinalInvPkgIncludeInvoiceMarkup bit,
		@bCustFinalInvPkgSubInvoicesMinimum bit,
		@bCustFinalInvPkgExpReceiptsMinimum bit,
		@bCustFinalInvPkgIncludeDirectExp bit,
		@bCustFinalInvPkgIncludeDirectSub bit,
		@bCustFinalInvPkgIncludeInvoice bit,
		@bCustFinalInvPkgEmailTemplate bit,
		@bCustFinalInvPkgIncludeTimesheetData bit,
		@bCustFinalInvPkgTimesheetTemplate bit,
		@bCustFinalInvPkgIncludeSubInvoices bit,
		@bCustFinalInvPkgIncludeExpReceipts bit,
		@bProjects_FinalInvPkgUserDocuments bit
	DECLARE @sql NVARCHAR(MAX) = ''
    
	declare @vp_ddl char(1) = case when exists (select * from sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FW_CustomColumnValuesData]') AND type in (N'U')) then 'Y' else 'N' end	
	declare @ddl_email varchar(1), @ddl_temp varchar(1), @ddl_time varchar(1)
	declare @pkgTable varchar(200) = case when exists (select * from sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Projects_CustFinalInvPkgUserDocuments]') AND type in (N'U')) 
		then 'Projects_CustFinalInvPkgUserDocuments' else 'Projects_FinalInvPkgUserDocuments' end

	if @vp_ddl = 'Y'
	begin
		-- ddl for email?
		set @sql = N'set @ddl_email = case when exists (select 1 from FW_CustomColumnValuesData where InfoCenterArea = ''Projects'' and GridId = ''X'' and ColName = ''CustFinalInvPkgEmailTemplate'') then ''Y'' else ''N'' end'
		exec sp_executesql @sql, N'@ddl_email varchar(1) output', @ddl_email output		

		-- ddl for timesheet?
		set @sql = N'set @ddl_time = case when exists (select 1 from FW_CustomColumnValuesData where InfoCenterArea = ''Projects'' and GridId = ''X'' and ColName = ''CustFinalInvPkgTimesheetTemplate'') then ''Y'' else ''N'' end'
		exec sp_executesql @sql, N'@ddl_time varchar(1) output', @ddl_time output	

		-- ddl for documents list?
		set @sql = N'set @ddl_temp = case when exists (select 1 from FW_CustomColumnValuesData where InfoCenterArea = ''Projects'' and GridId like ''%FinalInvPkgUserDocuments'' and ColName = ''CustDocument'') then ''Y'' else ''N'' end'
		exec sp_executesql @sql, N'@ddl_temp varchar(1) output', @ddl_temp output		
		print @ddl_temp
	end

	set @CustFinalInvPkgIncludeInvoiceMarkup = 'N'--default

	-- Verify which UDFs exist
	------------------------------------------------
	SELECT @bCustFinalInvPkgSubExpOrder = ISNULL(MAX(CASE WHEN UPPER(colName) = UPPER('CustFinalInvPkgSubExpOrder') THEN 1 ELSE null END), 0),
			@bCustFinalInvPkgIncludeSubInvoicesMarkup = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeSubInvoicesMarkup') then 1 else null end), 0),
			@bCustFinalInvPkgIncludeExpReceiptsMarkup = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeExpReceiptsMarkup') then 1 else null end), 0),
			@bCustFinalInvPkgSubInvoicesMinimum = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgSubInvoicesMinimum') then 1 else null end), 0),
			@bCustFinalInvPkgExpReceiptsMinimum = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgExpReceiptsMinimum') then 1 else null end), 0),
			@bCustFinalInvPkgIncludeInvoiceMarkup = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeInvoiceMarkup') then 1 else null end), 0),
			@bCustFinalInvPkgIncludeDirectExp = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeDirectExp') then 1 else null end), 0),
			@bCustFinalInvPkgIncludeDirectSub = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeDirectSub') then 1 else null end), 0),
			@bCustFinalInvPkgIncludeInvoice = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeInvoice') then 1 else null end), 0),
			@bCustFinalInvPkgEmailTemplate = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgEmailTemplate') then 1 else null end), 0),
			@bCustFinalInvPkgIncludeTimesheetData = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeTimesheetData') then 1 else null end), 0),
			@bCustFinalInvPkgTimesheetTemplate = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgTimesheetTemplate') then 1 else null end), 0),
			@bCustFinalInvPkgIncludeSubInvoices = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeSubInvoices') then 1 else null end), 0),
			@bCustFinalInvPkgIncludeExpReceipts = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeExpReceipts') then 1 else null end), 0)
		FROM (
			SELECT o.name as oname, type as otype, c.name as colName, c.system_type_id
				FROM sys.objects o
					LEFT JOIN sys.columns c ON o.object_id = c.object_id
				WHERE o.object_id = OBJECT_ID(N'[' + Replace('ProjectCustomTabFields', 'dbo.', 'dbo].[') + ']')
			) d
		GROUP BY oname, otype;

	SELECT @bProjects_FinalInvPkgUserDocuments = CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END
		FROM sys.objects o
		WHERE o.object_id = OBJECT_ID(N'[' + Replace(@pkgTable, 'dbo.', 'dbo].[') + ']')

	-- Get the values for the UDFs
	------------------------------------------------
	DECLARE @T AS TABLE(
		CustFinalInvPkgSubExpOrder Nvarchar(100),
		CustFinalInvPkgIncludeSubInvoicesMarkup Nvarchar(20),
		CustFinalInvPkgIncludeExpReceiptsMarkup Nvarchar(20),
		CustFinalInvPkgSubInvoicesMinimum decimal (19,5),
		CustFinalInvPkgExpReceiptsMinimum decimal (19,5),
		CustFinalInvPkgIncludeInvoiceMarkup Nvarchar(20),
		CustFinalInvPkgIncludeDirectExp Nvarchar(20),
		CustFinalInvPkgIncludeDirectSub Nvarchar(20))

	SET @sql = '
			SELECT '+(CASE WHEN @bCustFinalInvPkgSubExpOrder=1 THEN 'CustFinalInvPkgSubExpOrder' ELSE '''INVOICE''' END)+',
					'+(CASE WHEN @bCustFinalInvPkgIncludeSubInvoicesMarkup=1 THEN 'CustFinalInvPkgIncludeSubInvoicesMarkup' ELSE '''Y''' END)+',
					'+(CASE WHEN @bCustFinalInvPkgIncludeExpReceiptsMarkup=1 THEN 'CustFinalInvPkgIncludeExpReceiptsMarkup' ELSE '''Y''' END)+',
					'+(CASE WHEN @bCustFinalInvPkgSubInvoicesMinimum=1 THEN 'CustFinalInvPkgSubInvoicesMinimum' ELSE '''0.00''' END)+',
					'+(CASE WHEN @bCustFinalInvPkgExpReceiptsMinimum=1 THEN 'CustFinalInvPkgExpReceiptsMinimum' ELSE '''0.00''' END)+',
					'+(CASE WHEN @bCustFinalInvPkgIncludeInvoiceMarkup=1 THEN 'CustFinalInvPkgIncludeInvoiceMarkup' ELSE '''Y''' END)+',
					'+(CASE WHEN @bCustFinalInvPkgIncludeDirectExp=1 THEN 'CustFinalInvPkgIncludeDirectExp' ELSE '''N''' END)+',
					'+(CASE WHEN @bCustFinalInvPkgIncludeDirectSub=1 THEN 'CustFinalInvPkgIncludeDirectSub' ELSE '''N''' END)+'
				FROM ProjectCustomTabFields
				WHERE WBS1 = N'''+@WBS1+''' and WBS2 = N'' '''
	INSERT INTO @T EXECUTE(@sql)

	--SELECT * FROM @T
	SELECT TOP 1 @CustFinalInvPkgSubExpOrder = ISNULL(CustFinalInvPkgSubExpOrder,'DEFAULT'),
			@CustFinalInvPkgIncludeSubInvoicesMarkup = ISNULL(CustFinalInvPkgIncludeSubInvoicesMarkup, ''),
			@CustFinalInvPkgIncludeExpReceiptsMarkup = ISNULL(CustFinalInvPkgIncludeExpReceiptsMarkup, ''),
			@CustFinalInvPkgSubInvoicesMinimum = ISNULL(CustFinalInvPkgSubInvoicesMinimum,0),
			@CustFinalInvPkgExpReceiptsMinimum = ISNULL(CustFinalInvPkgExpReceiptsMinimum,0),
			@CustFinalInvPkgIncludeInvoiceMarkup = ISNULL(CustFinalInvPkgIncludeInvoiceMarkup, ''),
			@CustFinalInvPkgIncludeDirectExp = ISNULL(CustFinalInvPkgIncludeDirectExp, ''),
			@CustFinalInvPkgIncludeDirectSub = ISNULL(CustFinalInvPkgIncludeDirectSub, '')
		FROM @T

	-- Create the XML Results
	---------------------------------------
	DECLARE @SQTable AS TABLE(
		contentOrder int,
		docType varchar(255),
		documentDetail varchar(255),
		documentID varchar(255),
		iterationVal Nvarchar(max), contentOrder2 int, extraParams Nvarchar(max))

	SET @sql = '
		SELECT -99 as contentOrder, ''1.0'' as docType, ''_Header_'' as documentDetail, ''_Header_'' as documentID, null, null, null --an entry that always returns and provides sp version information '

		+ (CASE WHEN @bCustFinalInvPkgEmailTemplate=1 THEN '
		------- Email Template
		UNION all
		SELECT 0 as contentOrder, ''EmailTemplate'' as docType, '+(case when @ddl_email = 'Y' then 'd.DataValue' else 'CustFinalInvPkgEmailTemplate' end)+' as documentDetail, ''CustFinalInvPkgEmailTemplate'' as documentID, null, null, null
			FROM ProjectCustomTabFields p '
				+(case when @ddl_email = 'Y' then '
				inner join FW_CustomColumnValuesData d on d.Code = p.CustFinalInvPkgEmailTemplate and d.InfoCenterArea = ''Projects'' and ColName = ''CustFinalInvPkgEmailTemplate'' and GridId = ''X''' else '' end)+'
			WHERE wbs1 = @WBS1 and wbs2 = N'' '' and ISNULL(CustFinalInvPkgEmailTemplate,'''') <> '''' '
		ELSE '' END) + '

		------- Invoice Template
		UNION all '
		+ (CASE WHEN @bCustFinalInvPkgIncludeInvoice=1 THEN '
		SELECT CustFinalInvPkgIncludeInvoice as contentOrder, ''Invoice'' as docType, null as DocumentDetail, ''CustFinalInvPkgIncludeInvoice'' as documentID, null, null, null
			FROM ProjectCustomTabFields 
			WHERE wbs1 = @WBS1 and wbs2 = N'' '' and CustFinalInvPkgIncludeInvoice > 0 '
		ELSE '
		SELECT 1 as contentOrder, ''Invoice'' as docType, null as DocumentDetail '
		END) +

		+ (CASE WHEN @bCustFinalInvPkgIncludeTimesheetData=1 AND @bCustFinalInvPkgTimesheetTemplate=1 THEN '
		------- Timesheet Template
		UNION all
		SELECT CustFinalInvPkgIncludeTimesheetData as contentOrder, ''Timesheet'' as docType, '+(case when @ddl_time = 'Y' then 'd.DataValue' else 'CustFinalInvPkgTimesheetTemplate' end)+' as DocumentDetail, ''CustFinalInvPkgIncludeTimesheetData'' as documentID, null, null, null
			FROM ProjectCustomTabFields p '
				+(case when @ddl_time = 'Y' then '
				inner join FW_CustomColumnValuesData d on d.Code = p.CustFinalInvPkgTimesheetTemplate and d.InfoCenterArea = ''Projects'' and ColName = ''CustFinalInvPkgTimesheetTemplate'' and GridId = ''X''' else '' end)+'
			WHERE wbs1 = @WBS1 and wbs2 = N'' '' and CustFinalInvPkgIncludeTimesheetData > 0 '
		ELSE '' END) +

		+ (CASE WHEN @bCustFinalInvPkgIncludeSubInvoices=1 THEN '
		------- Sub
		UNION all
		SELECT CustFinalInvPkgIncludeSubInvoices as contentOrder, ''Sub'' as docType, null as DocumentDetail, ''CustFinalInvPkgIncludeSubInvoices'' as documentID, null, null, null
			FROM ProjectCustomTabFields 
			WHERE wbs1 = @WBS1 and wbs2 = N'' '' and CustFinalInvPkgIncludeSubInvoices > 0 '
		ELSE '' END) +

		+ (CASE WHEN @bCustFinalInvPkgIncludeExpReceipts=1 THEN '
		------- Exp
		UNION all
		SELECT CustFinalInvPkgIncludeExpReceipts as contentOrder, ''Exp'' as docType, null as DocumentDetail, ''CustFinalInvPkgIncludeExpReceipts'' as documentID, null, null, null
			FROM ProjectCustomTabFields 
			WHERE wbs1 = @WBS1 and wbs2 = N'' '' and CustFinalInvPkgIncludeExpReceipts > 0 '
		ELSE '' END) +

		+ (CASE WHEN @bProjects_FinalInvPkgUserDocuments=1 THEN '
		------- User Documents
		UNION all
		SELECT CustOrder as contentOrder, CustDocType as docType, '+(case when @ddl_temp = 'Y' then 'd.DataValue' else 'CustDocument' end)+' as documentDetail, p.Seq as documentID, null, null, null
			from '+@pkgTable+' p'
				+(case when @ddl_temp = 'Y' then '
				inner join FW_CustomColumnValuesData d on d.Code = p.CustDocument and d.InfoCenterArea = ''Projects'' and ColName = ''CustDocument'' and GridId like ''%FinalInvPkgUserDocuments''' else '' end)+'
			where wbs1 = @WBS1 and wbs2 = '' '' and CustOrder > 0 '
		ELSE '' END) + '

		UNION all
		SELECT Seq, ''Template'', Name, null, null, null, null
			from CCG_EI_Templates 
			where ISNULL(@WBS1,'''') = '''' and Status = ''A'''

	SET @sql = REPLACE(@sql, '@WBS1', 'N'''+@WBS1+'''')
	--print @sql
	INSERT INTO @SQTable EXECUTE(@sql)

	-- Handle Custom "Collate" Type Templates
	if exists (select 'x' from CCG_EI_Templates where type = 'collate') 
	begin
		-- Requires XSLT X-Path selection in the form:
		--		<xsl:variable name="PARAMETER_NAME" select="package/*/content[@id='[:_contentID]' and @documentId='[:_documentid]']/parameter[@name='PARAMETER_NAME']/value"/>
		-- Templates param needs to return: TopOrder, TemplateName, IterationVal, ExtraParams

		DECLARE @SQTableCopy AS TABLE (contentOrder int, docType varchar(255), documentDetail varchar(255), documentID varchar(255), iterationVal Nvarchar(max), contentOrder2 int, extraParams Nvarchar(max))
		INSERT INTO @SQTableCopy select c.* from @SQTable c inner join CCG_EI_Templates t on c.documentDetail = t.Name where t.Type = 'Collate'
		delete c from @SQTable c inner join CCG_EI_Templates t on c.documentDetail = t.Name where t.Type = 'Collate'

		declare @contentOrder int, @docType varchar(255), @documentDetail varchar(255), @documentID varchar(255), @seq int, @val varchar(max)

		declare cursor20200230 cursor fast_forward for 
			select contentOrder, docType, documentDetail, documentID, t.Seq, tp.Value
				from @SQTableCopy c
					inner join CCG_EI_Templates t on c.documentDetail = t.Name
					inner join CCG_EI_TemplateParameters tp on tp.TemplateSeq = t.Seq
				where tp.Name = 'templates'

		BEGIN TRY
			open cursor20200230
			fetch next from cursor20200230 into @contentOrder, @docType, @documentDetail, @documentID, @seq, @val
			while @@fetch_status = 0
			begin
				--print @val
				set @val = replace(@val, '@WBS1', ''''+@wbs1+'''')
				set @val = replace(@val, '@Invoice', ''''+isnull(@Invoice, '')+'''')
				insert into @SQTable (contentOrder2, documentDetail, iterationVal, extraParams) exec (@val)
				
				update c set contentOrder = @contentOrder, docType = @docType, documentID = Convert(varchar(32), HashBytes('SHA1', CONVERT(nvarchar(4000), c.documentDetail)), 2) + replace(NEWID(), '-', '')
					from @SQTable c
					where documentID is null
		
				fetch next from cursor20200230 into @contentOrder, @docType, @documentDetail, @documentID, @seq, @val
			end
			close cursor20200230
			deallocate cursor20200230	
		END TRY
		BEGIN CATCH
			IF CURSOR_STATUS('global','cursor20200230') >= -1 BEGIN
				close cursor20200230
				deallocate cursor20200230
			END
		END CATCH
	end

	SELECT isnull(c.documentDetail,c.docType) + isnull(' ('+nullif(c.iterationVal, '')+')', '') as '@name', c.contentOrder, c.docType, c.documentID, t.Seq as templateID, ISNULL(t.Type,c.docType) as contentClass,
		CASE when c.docType in ('File','FileEdit','FileEdit') then c.documentDetail else t.Source end as templateSourceFile, t.SourceType as templateSourceType,
		(--parameters subquery for 1 to many relationship
			SELECT
				p.SourceType as '@source', p.[Type] as '@dataType', p.Name as '@name', 
				case when c.iterationVal is null then p.Value else dbo.fnCCG_All_ReplaceMultiple(p.Value, '@IterationVal='+c.iterationVal+isnull('|'+nullif(c.extraParams,'')+(case when right(rtrim(c.extraParams), 1)<>'|' then '|' else '' end),''), '|', 1) end as 'value'
			FROM (
				SELECT Seq, TemplateSeq, SourceType, [Type], Name,
					replace(replace(replace(replace(replace(replace(replace(replace(Value,
						'PCTF.CustFinalInvPkgIncludeInvoiceMarkup', isnull(@CustFinalInvPkgIncludeInvoiceMarkup, '')),
						'PCTF.CustFinalInvPkgSubExpOrder', isnull(@CustFinalInvPkgSubExpOrder, '')),
						'PCTF.CustFinalInvPkgIncludeExpReceiptsMarkup', isnull(@CustFinalInvPkgIncludeExpReceiptsMarkup, '')),
						'PCTF.CustFinalInvPkgIncludeDirectExp', isnull(@CustFinalInvPkgIncludeDirectExp, '')),
						'PCTF.CustFinalInvPkgExpReceiptsMinimum', convert(varchar(20), isnull(@CustFinalInvPkgExpReceiptsMinimum, 0))),
						'PCTF.CustFinalInvPkgIncludeSubInvoicesMarkup', isnull(@CustFinalInvPkgIncludeSubInvoicesMarkup, '')),
						'PCTF.CustFinalInvPkgIncludeDirectSub', isnull(@CustFinalInvPkgIncludeDirectSub, '')),
						'PCTF.CustFinalInvPkgSubInvoicesMinimum', convert(varchar(20), isnull(@CustFinalInvPkgSubInvoicesMinimum, 0))) as Value
					from CCG_EI_TemplateParameters tp
			) p
			WHERE t.Seq = p.TemplateSeq
				or (c.docType = 'Sub'		and p.TemplateSeq = -3)
				or (c.docType = 'Exp'		and p.TemplateSeq = -5)
				or (c.docType = 'Invoice'	and p.TemplateSeq = -7)
			FOR XML PATH('parameter'),TYPE
		)
		FROM
			@SQTable c
			LEFT JOIN CCG_EI_Templates t on c.documentDetail = t.Name
		ORDER BY 2
		FOR XML path('content'),Root('packagebody'),TYPE
END
GO
GO
