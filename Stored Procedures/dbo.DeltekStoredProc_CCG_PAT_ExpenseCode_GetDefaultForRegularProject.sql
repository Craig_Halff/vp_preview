SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_ExpenseCode_GetDefaultForRegularProject]
	@WBS1	Nvarchar(30),
	@WBS2	Nvarchar(7),
	@WBS3	Nvarchar(7)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	SELECT PR.BillByDefaultORTable, PR.BillByDefaultConsultants, PR.BillByDefaultOtherExp, PR.BillByDefault
        FROM PR
        WHERE wbs1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
END
GO
