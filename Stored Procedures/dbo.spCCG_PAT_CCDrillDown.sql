SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_CCDrillDown]
	@SP		Nvarchar(max),
	@Params	Nvarchar(max)
AS
BEGIN
	/* Copyright (c) 2020 EleVia Software.  All rights reserved. */
	SET NOCOUNT ON
	IF SUBSTRING(@SP, 1, 5) <> N'spCCG' RETURN

	DECLARE @sql Nvarchar(max) = @SP + N' ' + @Params;
	EXEC(@sql)
END
GO
