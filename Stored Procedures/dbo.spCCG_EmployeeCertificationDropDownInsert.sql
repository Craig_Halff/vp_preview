SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EmployeeCertificationDropDownInsert] @CertificationAdd VARCHAR (255)
AS 
/* 
Copyright (c) 2020 Central Consulting Group.  All rights reserved.
03/25/2020 David Springer
           Populate Certification Name drop down list
		   Call this from an Employee CHANGE workflow when Certification Add has changed
Select *
FROM dbo.FW_CustomColumnValuesData
WHERE InfoCenterArea = 'Employees'
AND GridID = 'Certifications'
AND ColName = 'CustCertificationName'
Order by Seq
*/
DECLARE @Code varchar (10)
BEGIN
SET NOCOUNT ON

    SELECT @Code = Right ('00' + ltrim (str (MAX (Code) + 1)), 3)
    FROM dbo.FW_CustomColumnValuesData
    WHERE InfoCenterArea = 'Employees'
      AND GridID = 'Certifications'
      AND ColName = 'CustCertificationName'

    IF @Code IS NOT null
        Begin
        INSERT INTO dbo.FW_CustomColumnValuesData
        (InfoCenterArea
          , GridID
          , ColName
          , UICultureName
          , Code
          , Seq
          , DataValue
          , CreateUser
          , CreateDate
        )
        Values ('Employees', 'Certifications', 'CustCertificationName', 'en-US', 
                @Code, @Code, @CertificationAdd, 'CCG Procedure', GETDATE())

        Update c
        Set c.Seq = s.Seq
        FROM dbo.FW_CustomColumnValuesData c,
            (Select Code, row_number() over (order by DataValue) Seq
            FROM dbo.FW_CustomColumnValuesData
                WHERE InfoCenterArea = 'Employees'
                  AND GridID = 'Certifications'
                  AND ColName = 'CustCertificationName'
            ) s
        WHERE c.InfoCenterArea = 'Employees'
          AND c.GridID = 'Certifications'
          AND c.ColName = 'CustCertificationName'
          and c.Code = s.Code
          and c.Seq <> s.Seq
        End

    UPDATE dbo.EmployeeCustomTabFields SET CustCertificationAdd = NULL WHERE CustCertificationAdd IS NOT null
END



GO
