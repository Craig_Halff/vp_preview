SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpRptSummary]
  @dtETCDate datetime,
  @includeETC varchar(1)
AS

BEGIN --procedure rpRptSummary start

  declare @strSelect as varchar(300)

  declare @strPlanID varchar(32)
  declare @strUnposted varchar(1)
  declare @sintGRMethod smallint

  DECLARE planInfo  CURSOR FOR Select PlanID, UnpostedFlg, GRMethod from #rptPlanList 

  BEGIN --open planInfo
     OPEN planInfo
     FETCH from planInfo INTO @strPlanID, @strUnposted, @sintGRMethod
     WHILE @@FETCH_STATUS = 0 
       begin --while @@FETCH_STATUS = 0 start

       insert #rptTaskLaborJTD
       select PlanID,
	   TaskID,
	   Max(OutlineLevel) as OutlineLevel,
	   SUM(PeriodHrs) AS JTDHours
       from dbo.RP$rptTaskJTDLabor (@strPlanID, @dtETCDate, @strUnposted, 'Y',@sintGRMethod,'N')  	
       GROUP BY PlanID, TaskID 

	   if @includeETC = 'Y'
	   begin --@includeETC = 'Y'
	   insert #rptTaskETC
	   select planID,
	   TaskID,
	   ETCLabHrs,
       ETCLabCost,
       ETCLabBill,
       ETCExpCost,
       ETCExpBill,
       ETCDirExpCost,
       ETCDirExpBill,
       ETCReimExpCost,
       ETCReimExpBill,
       ETCConCost,
       ETCConBill,
       ETCDirConCost,
       ETCDirConBill,
       ETCReimConCost,
       ETCReimConBill,
       ETCUntQty,
       ETCUntCost,
       ETCUntBill   
	   from dbo.RP$rptTaskETC(@strPlanID,dateadd(d,1,@dtETCDate))
	   end --@includeETC = 'Y'

       FETCH NEXT FROM planInfo INTO @strPlanID, @strUnposted, @sintGRMethod
       end  --while @@FETCH_STATUS = 0 end    
     CLOSE planInfo
     DEALLOCATE planInfo  

     insert #rptPlanLaborJTD
     select PlanID,
	 SUM(JTDHours) AS JTDHours
     from #rptTaskLaborJTD
     where outlineLevel = 0 
     GROUP BY PlanID

	 If @includeETC = 'Y'
	 begin --@includeETC = 'Y'
	 insert #rptPlanLaborETC
     select taskETC.PlanID,
	 SUM(ETCHours)
     from #rptTaskETC taskETC
	 inner join rpTask on rpTask.planID = taskETC.planID and rpTask.taskID = taskETC.taskID
     where outlineLevel = 0 
     GROUP BY taskETC.PlanID
	 end --@includeETC = 'Y'
  END --open planInfo

END -- procedure rpRptSummary end
GO
