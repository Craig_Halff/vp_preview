SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PNSearchIndicators]
  (@strJTDDate VARCHAR(8), -- Date must be in format: 'yyyymmdd' regardless of UI Culture.
   @strProjSQL NVARCHAR(MAX))

AS

  DECLARE @projList TABLE(
    RowNumber bigint,
		WBS1 Nvarchar(30) COLLATE database_default,
	  Company Nvarchar(14) COLLATE database_default,
    VorN char(1) COLLATE database_default,
	  PRIMARY KEY (WBS1))
        
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
          
  -- Load project list with values from XML.
  
  INSERT @projList
	EXEC sp_EXECUTESQL @strProjSQL

  --  SELECT Tbl.Col.value('WBS1[1]', 'varchar(30)'),
  --         Tbl.Col.value('Company[1]', 'varchar(14)'),
  --         dbo.PN$PlanIs_VorN(Tbl.Col.value('WBS1[1]', 'varchar(30)'))
  --  FROM @strProjectXML.nodes('//row') AS Tbl(col)
          
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
  SELECT
	  PL.WBS1 AS ProjID,
    I.FeeLabCost AS FeeLabCost,
    I.FeeLabBill AS FeeLabBill,
    I.FeeExpCost AS FeeExpCost,
    I.FeeExpBill AS FeeExpBill,
    I.FeeConCost AS FeeConCost,
    I.FeeConBill AS FeeConBill,
    I.EACLabCost AS EACLabCost,
    I.EACLabBill AS EACLabBill,
    I.EACExpCost AS EACExpCost,
    I.EACExpBill AS EACExpBill,
    I.EACConCost AS EACConCost,
    I.EACConBill AS EACConBill,
    I.ProfitLabCost AS ProfitLabCost,
    I.EACLabMult AS EACLabMult,
    I.LabTargetMult AS LabTargetMult,
    I.BudgetType AS BudgetType,
    I.CostCurrencyCode AS CostCurrencyCode,
    I.BillingCurrencyCode AS BillingCurrencyCode

  FROM @projList AS PL
    OUTER APPLY dbo.PN$tabProjIndicators(PL.WBS1, @strJTDDate) AS I
  ORDER BY PL.RowNumber

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

GO
