SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_CheckNotifications] ( @Employee nvarchar(20), @OverrideSubject varchar(120), @OverrideSenderEmail varchar(100), @StatusToInclude varchar(10), @MsgPriority varchar(10)= NULL, @IncludeSupervisorAge int= -1, @InclWeekends char(1)= 'Y', @RoundingHoursCutoff int= 22)
             AS EXEC spCCG_EI_CheckNotifications @Employee,@OverrideSubject,@OverrideSenderEmail,@StatusToInclude,@MsgPriority,@IncludeSupervisorAge,@InclWeekends,@RoundingHoursCutoff
GO
