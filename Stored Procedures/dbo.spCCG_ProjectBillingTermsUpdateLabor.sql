SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectBillingTermsUpdateLabor]
   @WBS1        varchar(32), 
   @WBS2        varchar(7), 
   @WBS3        varchar(7)
AS
/*
Copyright (c) 2020 Central Consulting Group. All rights reserved.
03/10/2020	David Springer
			Update billing terms multiplier when CustLaborFringeMultiplier or CustLaborOvhProfitMultiplier has changed
			
*/
BEGIN
SET NOCOUNT ON

-- Project level
If @WBS2 = ' '
   Begin

      Update BT
      Set Mult1 = p.CustLaborFringeMultiplier,
          Mult2 = p.CustLaborOvhProfitMultiplier
      From BT b, ProjectCustomTabFields p
      Where p.WBS1 = @WBS1
	    and p.WBS2 = ' ' -- project level Reimb Consultant Multiplier
      	and p.WBS1 = b.WBS1
		and IsNull (p.CustBillingType, 'Non-Billable') <> 'Non-Billable'

   -- Push project level Labor Multipliers to all lower levels
      Update px
      Set px.CustLaborFringeMultiplier = p.CustLaborFringeMultiplier,
	      px.CustLaborOvhProfitMultiplier = p.CustLaborOvhProfitMultiplier
      From ProjectCustomTabFields p, ProjectCustomTabFields px
      Where p.WBS1 = @WBS1
	    and p.WBS2 = ' ' -- project level billing terms
        and p.WBS1 = px.WBS1
		and px.WBS2 <> ' ' -- all lower levels of the project
		and IsNull (px.CustBillingType, 'Non-Billable') <> 'Non-Billable'

   End -- Project level

-- Phase level
If @WBS2 <> ' ' and @WBS3 = ' '
   Begin

      Update BT
      Set Mult1 = p.CustLaborFringeMultiplier,
          Mult2 = p.CustLaborOvhProfitMultiplier
      From BT b, ProjectCustomTabFields p
      Where b.WBS1 = @WBS1
	    and b.WBS2 = @WBS2
		and b.WBS1 = p.WBS1
		and b.WBS2 = p.WBS2
		and b.WBS3 = p.WBS3
		and IsNull (p.CustBillingType, 'Non-Billable') <> 'Non-Billable'

   -- Push phase level Labor Multipliers to task levels
      Update px
      Set px.CustLaborFringeMultiplier = p.CustLaborFringeMultiplier,
	      px.CustLaborOvhProfitMultiplier = p.CustLaborOvhProfitMultiplier
      From ProjectCustomTabFields px, ProjectCustomTabFields p
      Where p.WBS1 = @WBS1
        and p.WBS2 = @WBS2
		and p.WBS3 = ' '
		and p.WBS1 = px.WBS1
		and p.WBS2 = px.WBS2
		and px.WBS3 <> ' '
		and IsNull (px.CustBillingType, 'Non-Billable') <> 'Non-Billable'

   End -- Phase level

-- Task level
If @WBS3 <> ' '
   Begin
      Update BT
      Set Mult1 = p.CustLaborFringeMultiplier,
          Mult2 = p.CustLaborOvhProfitMultiplier
      From BT b, ProjectCustomTabFields p
      Where b.WBS1 = @WBS1
	    and b.WBS2 = @WBS2
	    and b.WBS3 = @WBS3
		and b.WBS1 = p.WBS1
		and b.WBS2 = p.WBS2
		and b.WBS3 = p.WBS3
		and IsNull (p.CustBillingType, 'Non-Billable') <> 'Non-Billable'

   End -- Task level

END
GO
