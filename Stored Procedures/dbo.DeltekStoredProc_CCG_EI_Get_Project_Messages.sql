SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Get_Project_Messages] ( @prnamelen int, @wbs1List nvarchar(max))
             AS EXEC spCCG_EI_Get_Project_Messages @prnamelen,@wbs1List
GO
