SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[HAI_SelectInvoiceTemplate]
    @project /*N*/ VARCHAR(12)
  , @template VARCHAR(30)
AS
    /* 
	Copyright (c) 2021 Halff Associates, Inc.  All rights reserved.
	12/04/2020	Craig H. Anderson

	Set the correct invoice template for a project.
*/
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        DECLARE @oldTemplate VARCHAR(25);
        DECLARE @newRemittanceFile VARCHAR(255);


        SET @oldTemplate = (
            SELECT TOP (1) b.Template FROM dbo.BT b WHERE b.WBS1 = @project AND b.WBS2 = ' ' ORDER BY b.WBS1
        );

        SET @newRemittanceFile = '\\HALFF.ad\Offices\ACCT\Client Invoices\Templates\RemittanceChange-2020.pdf';

        -- Set the template in the Billing Terms table (T)
        UPDATE b
        SET b.Template = @template
        FROM dbo.BT b
        WHERE
            b.WBS1 = @project
            AND b.Template <> @template;


        -- Set the Template in ProjectCustomTabField if we were called from a change in BT
        UPDATE b
        SET b.CustInvoiceTemplate = @template
        FROM dbo.ProjectCustomTabFields AS b
        WHERE
            b.WBS1 = @project
            AND b.WBS2 = ' '
            AND b.CustInvoiceTemplate <> @template;

        -- IF switching from HTT/CE to HAI, include a remittance notice in the invoice package, otherwise, remove it.
        IF @template = 'HALFF_SUMMARY'
            AND @oldTemplate IN ( 'HTT', 'CEI' )
            BEGIN
                BEGIN TRANSACTION;
                DELETE FROM dbo.Projects_FinalInvPkgUserDocuments
                WHERE
                    WBS1 = @project
                    AND CustDocument = @newRemittanceFile;

                INSERT INTO dbo.Projects_FinalInvPkgUserDocuments (
                    WBS1
                  , WBS2
                  , WBS3
                  , Seq
                  , CustOrder
                  , CustDocType
                  , CustDocument
                  , CreateUser
                  , CreateDate
                  , ModUser
                  , ModDate
                )
                SELECT p.WBS1
                     , ' ' -- WBS2
                     , ' ' -- WBS3
                     , REPLACE(NEWID(), '-', '')
                     , 1
                     , 'File'
                     , @newRemittanceFile
                     , 'ADMIN'
                     , GETUTCDATE()
                     , 'ADMIN'
                     , GETUTCDATE()
                FROM DataMart.dbo.ProjectJTD p
                WHERE
                    p.WBS1 = @project
                    AND p.ProjectStatus = 'A'
                    AND p.ChargeType = 'R';
                COMMIT TRANSACTION;
            END;
        ELSE
            BEGIN
                DELETE FROM dbo.Projects_FinalInvPkgUserDocuments
                WHERE
                    WBS1 = @project
                    AND CustDocument = @newRemittanceFile;
            END;
    END;
GO
