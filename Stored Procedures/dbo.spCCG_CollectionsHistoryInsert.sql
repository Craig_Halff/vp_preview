SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_CollectionsHistoryInsert] @UDIC_UID varchar (32)
AS 
/* 
Copyright (c) 2020 Central Consulting Group.  All rights reserved.
06/17/2020	David Springer
			Populate UDIC_Collections History grid
			Call this from a Collections CHANGE workflow when Response has changed
07/14/2020	David Springer
			Added Legal Involvement
07/16/2020 Craig Anderson
            SET CustRequestedAction to NULL if Response = 'Verified Invoice Received'
07/20/2020  Craig Anderson
            Added handling of CustRequestedActionStatus where CustResponse = 'Verified Invoice Received'
            Added clearing out of Response Notes.
*/
BEGIN
SET NOCOUNT ON;

	DECLARE @hours float = DATEPART(HOUR,GETDATE());
	DECLARE @minutes float = DATEPART(MINUTE,GETDATE());
	DECLARE @addHours float = @hours + @minutes / 60.0;

	Insert Into UDIC_Collection_History
	(UDIC_UID, Seq, CustHistoryAction, CustHistoryActionStart, CustHistoryActionOwner, 
	 CustHistoryCollectionContact, CustHistoryResponseDate, CustHistoryResponse, CustHistoryResponseNotes,
	 CustHistoryContactedbyEmployee)
	Select UDIC_UID, Replace (NewID(), '-', '') Seq, CustRequestedAction, CustRequestedActionStart, CustRequestedActionOwner,
		CustCollectionContact, DATEADD(HOUR, @addHours, CustResponseDate), CustResponse, CustResponseNotes, CustContactedbyEmployee
	From UDIC_Collection
	Where UDIC_UID = @UDIC_UID;

	UPDATE  px
	SET px.CustLegalInvolvement = 'Yes'
	FROM    ProjectCustomTabFields px
	INNER JOIN UDIC_Collection     c WITH (NOLOCK)
		ON px.WBS1 = c.CustProject
		   AND  px.WBS2 = ' '
		   AND  c.CustCollectionStatus = 'Legal Involvement'
		   AND  px.CustLegalInvolvement <> 'Yes';

    Update UDIC_Collection 
	Set CustResponseDate = null,
		CustContactedbyEmployee = NULL,
        CustResponseNotes = NULL,
        CustRequestedAction = IIF(CustResponse = 'Verified Invoice Received', NULL,CustRequestedAction),
        CustRequestedActionStatus =
            CASE
                WHEN CustResponse = 'Verified Invoice Received'
                    THEN  NULL
                ELSE CustRequestedActionStatus
            END,
	    CustResponse = NULL,
		CustRequestedActionStart = 
			CASE
                WHEN CustResponse = 'Verified Invoice Received'
                    THEN  NULL
                ELSE CustRequestedActionStart
            END
	Where UDIC_UID = @UDIC_UID;

END;
GO
