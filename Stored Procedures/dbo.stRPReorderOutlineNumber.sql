SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPReorderOutlineNumber]
  @strPlanID varchar(32),
  @bitCalledFromRM bit = 0
AS

BEGIN -- Procedure stRPReorderOutlineNumber

  SET NOCOUNT ON

  DECLARE @iMAXOulineLevel int
  DECLARE @iLevel int = 3
   --When reordering outline number, we should always reorder for RPTask
  Set @bitCalledFromRM = 1

  DECLARE @tabTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    ParentTaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int
    UNIQUE (PlanID, TaskID, ParentTaskID, WBS1, WBS2, WBS3, LaborCode, ParentOutlineNumber, OutlineNumber, OutlineLevel)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @iMAXOulineLevel = MAX(OutlineLevel) FROM PNTask WHERE PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Building a table to hold the OutlineNumber for the WBS rows.

  -- WBS1 row.

  INSERT @tabTask(
    PlanID,
    TaskID,
    ParentTaskID,
    WBS1,
    WBS2,
    WBS3,
    LaborCode,
    ParentOutlineNumber,
    OutlineNumber,
    OutlineLevel
  )
    SELECT
      T.PlanID AS PlanID,
      T.TaskID AS TaskID,
      NULL AS ParentTaskID,
      T.WBS1,
      NULL AS WBS2,
      NULL AS WBS3,
      NULL AS LaborCode,
      NULL AS ParentOutlineNumber,
      '001' AS OutlineNumber,
      0 AS OutlineLevel
      FROM PNTask AS T
      WHERE T.PlanID = @strPlanID AND T.OutlineLevel = 0 AND T.WBS2 IS NULL AND T.WBS3 IS NULL
      
  -- WBS2 or LBCD rows.

  INSERT @tabTask(
    PlanID,
    TaskID,
    ParentTaskID,
    WBS1,
    WBS2,
    WBS3,
    LaborCode,
    ParentOutlineNumber,
    OutlineNumber,
    OutlineLevel
  )
    SELECT
      X.PlanID AS PlanID,
      X.TaskID AS TaskID,
      X.ParentTaskID AS ParentTaskID,
      X.WBS1 AS WBS1,
      X.WBS2 AS WBS2,
      X.WBS3 AS WBS3,
      X.LaborCode AS LaborCode,
      '001' AS ParentOutlineNumber,
      '001' + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
      1 AS OutlineLevel
      FROM (
        SELECT
          CT.PlanID AS PlanID,
          CT.TaskID AS TaskID,
          PT.TaskID AS ParentTaskID,
          CT.WBS1 AS WBS1,
          CT.WBS2 AS WBS2,
          CT.WBS3 AS WBS3,
          CT.LaborCode AS LaborCode,
          ROW_NUMBER() OVER (PARTITION BY CT.WBS1 ORDER BY CT.WBS1, CT.WBS2, CT.WBS3, CT.LaborCode) AS RowID
          FROM PNTask AS CT
            INNER JOIN PNTask AS PT ON CT.PlanID = PT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber 
          WHERE CT.PlanID = @strPlanID AND CT.OutlineLevel = 1 
            AND ((CT.WBS2 IS NOT NULL AND CT.WBS3 IS NULL AND CT.LaborCode IS NULL) OR (CT.WBS2 IS NULL AND CT.WBS3 IS NULL AND CT.LaborCode IS NOT NULL))
      ) AS X

  -- WBS3 or LBCD rows.

  INSERT @tabTask(
    PlanID,
    TaskID,
    ParentTaskID,
    WBS1,
    WBS2,
    WBS3,
    ParentOutlineNumber,
    OutlineNumber,
    OutlineLevel
  )
    SELECT
      X.PlanID AS PlanID,
      X.TaskID AS TaskID,
      X.ParentTaskID AS ParentTaskID,
      X.WBS1,
      X.WBS2,
      X.WBS3,
      PX.OutlineNumber AS ParentOutlineNumber,
      PX.OutlineNumber + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
      2 AS OutlineLevel
      FROM (
        SELECT
          CT.PlanID AS PlanID,
          CT.TaskID AS TaskID,
          PT.TaskID AS ParentTaskID,
          CT.WBS1 AS WBS1,
          CT.WBS2 AS WBS2,
          CT.WBS3 AS WBS3,
          ROW_NUMBER() OVER (PARTITION BY CT.WBS2 ORDER BY CT.WBS1, CT.WBS2, CT.WBS3, CT.LaborCode) AS RowID
          FROM PNTask AS CT
            INNER JOIN PNTask AS PT ON CT.PlanID = PT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber AND CT.OutlineLevel = PT.OutlineLevel + 1
          WHERE CT.PlanID = @strPlanID AND CT.OutlineLevel = 2  
            AND ((CT.WBS2 IS NOT NULL AND CT.WBS3 IS NOT NULL AND CT.LaborCode IS NULL) OR (CT.WBS2 IS NOT NULL AND CT.WBS3 IS NULL AND CT.LaborCode IS NOT NULL))
      ) AS X
        INNER JOIN @tabTask AS PX ON X.PlanID = PX.PlanID AND X.ParentTaskID = PX.TaskID

  -- Rows at OutlineLevel higher than 2

  WHILE (@iLevel <= @iMAXOulineLevel) 
    BEGIN

      INSERT @tabTask(
        PlanID,
        TaskID,
        ParentTaskID,
        WBS1,
        WBS2,
        WBS3,
        LaborCode,
        ParentOutlineNumber,
        OutlineNumber,
        OutlineLevel
      )
        SELECT
          X.PlanID AS PlanID,
          X.TaskID AS TaskID,
          X.ParentTaskID AS ParentTaskID,
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.LaborCode AS LaborCode,
          PX.OutlineNumber AS ParentOutlineNumber,
          PX.OutlineNumber + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
          @iLevel AS OutlineLevel
          FROM (
            SELECT
              CT.PlanID AS PlanID,
              CT.TaskID AS TaskID,
              PT.TaskID AS ParentTaskID,
              CT.WBS1 AS WBS1,
              CT.WBS2 AS WBS2,
              CT.WBS3 AS WBS3,
              CT.LaborCode AS LaborCode,
              ROW_NUMBER() OVER (PARTITION BY PT.OutlineNumber ORDER BY CT.WBS1, CT.WBS2, CT.WBS3, CT.LaborCode, CT.OutlineNumber) AS RowID
              FROM PNTask AS CT
                INNER JOIN PNTask AS PT ON CT.PlanID = PT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber AND CT.OutlineLevel = PT.OutlineLevel + 1
              WHERE CT.PlanID = @strPlanID AND CT.OutlineLevel = @iLevel
          ) AS X
            INNER JOIN @tabTask AS PX ON X.PlanID = PX.PlanID AND X.ParentTaskID = PX.TaskID

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SET @iLevel = @iLevel + 1

    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION

    UPDATE T SET
      OutlineNumber = XT.OutlineNumber,
      ParentOutlineNumber = XT.ParentOutlineNumber
      FROM PNTask AS T
        INNER JOIN @tabTask AS XT ON T.PlanID = XT.PlanID AND T.TaskID = XT.TaskID
        WHERE T.PlanID = @strPlanID
          AND T.OutlineNumber <> XT.OutlineNumber OR T.ParentOutlineNumber <> XT.ParentOutlineNumber

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- If this SP was called from Resource Management then need to update RP tables.

  IF (@bitCalledFromRM = 1)
    BEGIN

      UPDATE T SET
        OutlineNumber = XT.OutlineNumber,
        ParentOutlineNumber = XT.ParentOutlineNumber
        FROM RPTask AS T
          INNER JOIN @tabTask AS XT ON T.PlanID = XT.PlanID AND T.TaskID = XT.TaskID
          WHERE T.PlanID = @strPlanID
            AND T.OutlineNumber <> XT.OutlineNumber OR T.ParentOutlineNumber <> XT.ParentOutlineNumber

    END /* END IF (@bitCalledFromRM = 1) */

  COMMIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF
     
END -- stRPReorderOutlineNumber
GO
