SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_DelegationGet] (
	@Username					Nvarchar(32),
	@BeginDate					varchar(20),
	@EndDate					varchar(20),
	@CurrentEmployeeId			Nvarchar(20),
	@IsSupervisorApprovalOnly	int = 0,			/* 0 or 1 */
	@ShowAllDelegations			int = 1,			/* 0 or 1 */
	@CanSelfDelegate			int = 1				/* 0 or 1 */
)
AS
BEGIN
/*
	Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved.

	select * from CCG_EI_Delegation
	exec spCCG_EI_DelegationGet 'ADMIN','','', '00001', 1, 1, 1
	exec spCCG_EI_DelegationGet 'ADMIN','','', '00001', 1, 0, 1
	exec spCCG_EI_DelegationGet 'ADMIN','2/21/2017','2/28/2017', '00001', 0, 1, 1
*/
	SET NOCOUNT ON

	-- Remove old delegation records
	declare @advOptions varchar(max) = (select top 1 AdvancedOptions from CCG_EI_Config)
	declare @noAutoCleanDelegation bit = (CASE WHEN CHARINDEX('&autocleandelegation=n&', LOWER('&'+@advOptions+'&'))>0 THEN 1 ELSE 0 END)
	if @noAutoCleanDelegation = 0 delete CCG_EI_Delegation where dateadded < dateadd(month, -3, getdate()) and ToDate < dateadd(month, -3, getdate())

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- Get the list of delegations for the currently logged in employee
	if isnull(@BeginDate,'') <> '' and isnull(@EndDate,'') <> ''
		select d.*, PR.Name as ForProject, 'N' AS Permanent, NULL as PermanentApprovedBy, NULL as PermanentApprovedOn
			from CCG_EI_Delegation d
				left join EM on EM.Employee = d.Employee
				left join PR on PR.WBS1 = d.ForWBS1 and PR.WBS2 = N' ' and PR.WBS3 = N' '
			where FromDate <= @EndDate and ToDate >= @BeginDate
				and (@ShowAllDelegations = 1
					or (@IsSupervisorApprovalOnly = 1 and EM.Supervisor = @CurrentEmployeeId)
					or (d.Employee = @CurrentEmployeeId or d.Delegate = @CurrentEmployeeId)
				)
	else
		select d.*, PR.Name as ForProject, 'N' AS Permanent, NULL as PermanentApprovedBy, NULL as PermanentApprovedOn
			from CCG_EI_Delegation d
				left join EM on EM.Employee = d.Employee
				left join PR on PR.WBS1 = d.ForWBS1 and PR.WBS2 = N' ' and PR.WBS3 = N' '
			where (@ShowAllDelegations = 1
					or (@IsSupervisorApprovalOnly = 1 and EM.Supervisor = @CurrentEmployeeId)
					or (d.Employee = @CurrentEmployeeId or d.Delegate = @CurrentEmployeeId)
				)
END
GO
