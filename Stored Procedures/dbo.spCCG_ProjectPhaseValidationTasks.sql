SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_ProjectPhaseValidationTasks] @WBS1 varchar (32), @PhaseCode varchar (32)
AS
/*
Copyright 2021 Central Consulting Group.   All rights reserved.
08/12/2021	David Springer
			Call this from a Project Phase grid CHANGE workflow when any Fee has changed.
*/
SET NOCOUNT ON
BEGIN
   If exists (Select 'x' From Projects_Tasks Where WBS1 = @WBS1 and CustTaskPhaseCode = @PhaseCode)
      RAISERROR ('Halff Custom Error:  Change Task fee, not Phase fee.                     ', 16, 1); -- 16 = severity; 1 = state

END

GO
